﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// OnlineMapsMarkerBase[]
struct OnlineMapsMarkerBaseU5BU5D_t3690434744;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// OnlineMapsVector2i
struct OnlineMapsVector2i_t2180897250;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t2364004493;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Single OnlineMapsUtils::Angle2D(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float OnlineMapsUtils_Angle2D_m2418642609 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point10, Vector2_t2243707579  ___point21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnlineMapsUtils::Angle2D(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float OnlineMapsUtils_Angle2D_m3995743537 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___point10, Vector3_t2243707580  ___point21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnlineMapsUtils::Angle2D(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)
extern "C"  float OnlineMapsUtils_Angle2D_m1653782139 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___point10, Vector3_t2243707580  ___point21, Vector3_t2243707580  ___point32, bool ___unsigned3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnlineMapsUtils::Angle2DRad(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  float OnlineMapsUtils_Angle2DRad_m913515919 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___point10, Vector3_t2243707580  ___point21, float ___offset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::ApplyColorArray(UnityEngine.Color[]&,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[]&,System.Int32,System.Int32)
extern "C"  void OnlineMapsUtils_ApplyColorArray_m555983688 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442** ___result0, int32_t ___px1, int32_t ___py2, int32_t ___width3, int32_t ___height4, ColorU5BU5D_t672350442** ___color5, int32_t ___rx6, int32_t ___ry7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::ApplyColorArray2(UnityEngine.Color[]&,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[]&)
extern "C"  void OnlineMapsUtils_ApplyColorArray2_m1928053850 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442** ___result0, int32_t ___px1, int32_t ___py2, int32_t ___width3, int32_t ___height4, ColorU5BU5D_t672350442** ___color5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double OnlineMapsUtils::Clip(System.Double,System.Double,System.Double)
extern "C"  double OnlineMapsUtils_Clip_m2608044177 (Il2CppObject * __this /* static, unused */, double ___n0, double ___minValue1, double ___maxValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::Crossing(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_Crossing_m1896305105 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___p10, Vector2_t2243707579  ___p21, Vector2_t2243707579  ___p32, Vector2_t2243707579  ___p43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::DistanceBetweenPoints(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_DistanceBetweenPoints_m1314289909 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point10, Vector2_t2243707579  ___point21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::DistanceBetweenPoints(System.Double,System.Double,System.Double,System.Double,System.Double&,System.Double&)
extern "C"  void OnlineMapsUtils_DistanceBetweenPoints_m3432485292 (Il2CppObject * __this /* static, unused */, double ___x10, double ___y11, double ___x22, double ___y23, double* ___dx4, double* ___dy5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double OnlineMapsUtils::DistanceBetweenPointsD(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  double OnlineMapsUtils_DistanceBetweenPointsD_m4043631011 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point10, Vector2_t2243707579  ___point21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::FixAngle(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_FixAngle_m3416978605 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::FlipNegative(UnityEngine.Rect)
extern "C"  void OnlineMapsUtils_FlipNegative_m2967303955 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::GetCenterPointAndZoom(OnlineMapsMarkerBase[],UnityEngine.Vector2&,System.Int32&)
extern "C"  void OnlineMapsUtils_GetCenterPointAndZoom_m3170306753 (Il2CppObject * __this /* static, unused */, OnlineMapsMarkerBaseU5BU5D_t3690434744* ___markers0, Vector2_t2243707579 * ___center1, int32_t* ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::GetCenterPointAndZoom(UnityEngine.Vector2[],UnityEngine.Vector2&,System.Int32&)
extern "C"  void OnlineMapsUtils_GetCenterPointAndZoom_m3618019590 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___positions0, Vector2_t2243707579 * ___center1, int32_t* ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::GetIntersectionPointOfTwoLines(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Int32&)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_GetIntersectionPointOfTwoLines_m2714104291 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___p110, Vector2_t2243707579  ___p121, Vector2_t2243707579  ___p212, Vector2_t2243707579  ___p223, int32_t* ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::GetIntersectionPointOfTwoLines(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32&)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_GetIntersectionPointOfTwoLines_m2269731555 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___p110, Vector3_t2243707580  ___p121, Vector3_t2243707580  ___p212, Vector3_t2243707580  ___p223, int32_t* ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::GetLatLng(OnlineMapsXML,System.String)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_GetLatLng_m68651940 (Il2CppObject * __this /* static, unused */, OnlineMapsXML_t3341520387 * ___node0, String_t* ___subNodeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW OnlineMapsUtils::GetWWW(System.String)
extern "C"  WWW_t2919945039 * OnlineMapsUtils_GetWWW_m1305707790 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color OnlineMapsUtils::HexToColor(System.String)
extern "C"  Color_t2020392075  OnlineMapsUtils_HexToColor_m3899320166 (Il2CppObject * __this /* static, unused */, String_t* ___hex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsUtils::Intersect(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool OnlineMapsUtils_Intersect_m2893035097 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___a0, Rect_t3681755626  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::LineIntersection(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_LineIntersection_m3509853330 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___p10, Vector2_t2243707579  ___p21, Vector2_t2243707579  ___p32, Vector2_t2243707579  ___p43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsUtils::same_sign(System.Single,System.Single)
extern "C"  bool OnlineMapsUtils_same_sign_m2643035686 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsUtils::IsPointInPolygon(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single,System.Single)
extern "C"  bool OnlineMapsUtils_IsPointInPolygon_m2414195315 (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___poly0, float ___x1, float ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::LatLongToMercat(System.Single,System.Single)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_LatLongToMercat_m2847378279 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::LatLongToMercat(System.Single&,System.Single&)
extern "C"  void OnlineMapsUtils_LatLongToMercat_m3311997566 (Il2CppObject * __this /* static, unused */, float* ___x0, float* ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::LatLongToMercat(System.Double&,System.Double&)
extern "C"  void OnlineMapsUtils_LatLongToMercat_m2205193020 (Il2CppObject * __this /* static, unused */, double* ___x0, double* ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsVector2i OnlineMapsUtils::LatLongToTile(System.Single,System.Single,System.Int32)
extern "C"  OnlineMapsVector2i_t2180897250 * OnlineMapsUtils_LatLongToTile_m585552574 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsVector2i OnlineMapsUtils::LatLongToTile(UnityEngine.Vector2,System.Int32)
extern "C"  OnlineMapsVector2i_t2180897250 * OnlineMapsUtils_LatLongToTile_m2201968116 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___p0, int32_t ___zoom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsVector2i OnlineMapsUtils::LatLongToTile(System.Double,System.Double,System.Int32)
extern "C"  OnlineMapsVector2i_t2180897250 * OnlineMapsUtils_LatLongToTile_m188463646 (Il2CppObject * __this /* static, unused */, double ___x0, double ___y1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::LatLongToTiled(System.Double,System.Double,System.Int32,System.Double&,System.Double&)
extern "C"  void OnlineMapsUtils_LatLongToTiled_m842368665 (Il2CppObject * __this /* static, unused */, double ___dx0, double ___dy1, int32_t ___zoom2, double* ___tx3, double* ___ty4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::LatLongToTilef(UnityEngine.Vector2,System.Int32)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_LatLongToTilef_m3592792928 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___p0, int32_t ___zoom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::LatLongToTilef(UnityEngine.Vector2,System.Int32,System.Single&,System.Single&)
extern "C"  void OnlineMapsUtils_LatLongToTilef_m310559923 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___p0, int32_t ___zoom1, float* ___fx2, float* ___fy3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::LatLongToTilef(System.Single,System.Single,System.Int32)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_LatLongToTilef_m3602770146 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::NearestPointStrict(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_NearestPointStrict_m2049095230 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, Vector2_t2243707579  ___lineStart1, Vector2_t2243707579  ___lineEnd2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double OnlineMapsUtils::Repeat(System.Double,System.Double,System.Double)
extern "C"  double OnlineMapsUtils_Repeat_m2742300492 (Il2CppObject * __this /* static, unused */, double ___n0, double ___minValue1, double ___maxValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::TileToLatLong(System.Int32,System.Int32,System.Int32)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_TileToLatLong_m3488977320 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::TileToLatLong(System.Single,System.Single,System.Int32)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_TileToLatLong_m2363134714 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsUtils::TileToLatLong(System.Double,System.Double,System.Int32,System.Double&,System.Double&)
extern "C"  void OnlineMapsUtils_TileToLatLong_m1919929301 (Il2CppObject * __this /* static, unused */, double ___tx0, double ___ty1, int32_t ___zoom2, double* ___lx3, double* ___ly4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsUtils::TileToLatLong(UnityEngine.Vector2,System.Int32)
extern "C"  Vector2_t2243707579  OnlineMapsUtils_TileToLatLong_m4139591544 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___p0, int32_t ___zoom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String OnlineMapsUtils::TileToQuadKey(System.Int32,System.Int32,System.Int32)
extern "C"  String_t* OnlineMapsUtils_TileToQuadKey_m307608553 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t ___zoom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Int32> OnlineMapsUtils::Triangulate(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  Il2CppObject* OnlineMapsUtils_Triangulate_m4009476513 (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OnlineMapsUtils::TriangulateArea(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  float OnlineMapsUtils_TriangulateArea_m1449516477 (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsUtils::TriangulateInsideTriangle(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool OnlineMapsUtils_TriangulateInsideTriangle_m4007206950 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___A0, Vector2_t2243707579  ___B1, Vector2_t2243707579  ___C2, Vector2_t2243707579  ___P3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsUtils::TriangulateSnip(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern "C"  bool OnlineMapsUtils_TriangulateSnip_m3132029563 (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___points0, int32_t ___u1, int32_t ___v2, int32_t ___w3, int32_t ___n4, Int32U5BU5D_t3030399641* ___V5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
