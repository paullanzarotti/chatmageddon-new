﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar/OnProgressFinished
struct OnProgressFinished_t739551803;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ProgressBar/OnProgressFinished::.ctor(System.Object,System.IntPtr)
extern "C"  void OnProgressFinished__ctor_m630033172 (OnProgressFinished_t739551803 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar/OnProgressFinished::Invoke(System.Single)
extern "C"  void OnProgressFinished_Invoke_m186235379 (OnProgressFinished_t739551803 * __this, float ___finishedPercent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ProgressBar/OnProgressFinished::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnProgressFinished_BeginInvoke_m573255292 (OnProgressFinished_t739551803 * __this, float ___finishedPercent0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar/OnProgressFinished::EndInvoke(System.IAsyncResult)
extern "C"  void OnProgressFinished_EndInvoke_m617642718 (OnProgressFinished_t739551803 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
