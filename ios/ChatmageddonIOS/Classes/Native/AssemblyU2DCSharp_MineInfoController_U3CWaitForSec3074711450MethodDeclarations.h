﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineInfoController/<WaitForSeconds>c__Iterator0
struct U3CWaitForSecondsU3Ec__Iterator0_t3074711450;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MineInfoController/<WaitForSeconds>c__Iterator0::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0__ctor_m3215734949 (U3CWaitForSecondsU3Ec__Iterator0_t3074711450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MineInfoController/<WaitForSeconds>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator0_MoveNext_m1982844311 (U3CWaitForSecondsU3Ec__Iterator0_t3074711450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MineInfoController/<WaitForSeconds>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m83508299 (U3CWaitForSecondsU3Ec__Iterator0_t3074711450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MineInfoController/<WaitForSeconds>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1853771763 (U3CWaitForSecondsU3Ec__Iterator0_t3074711450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoController/<WaitForSeconds>c__Iterator0::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Dispose_m625907788 (U3CWaitForSecondsU3Ec__Iterator0_t3074711450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineInfoController/<WaitForSeconds>c__Iterator0::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator0_Reset_m1172064058 (U3CWaitForSecondsU3Ec__Iterator0_t3074711450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
