﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.DpapiProtectedConfigurationProvider
struct DpapiProtectedConfigurationProvider_t1041745009;
// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.String
struct String_t;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"

// System.Void System.Configuration.DpapiProtectedConfigurationProvider::.ctor()
extern "C"  void DpapiProtectedConfigurationProvider__ctor_m1023087516 (DpapiProtectedConfigurationProvider_t1041745009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Configuration.DpapiProtectedConfigurationProvider::Decrypt(System.Xml.XmlNode)
extern "C"  XmlNode_t616554813 * DpapiProtectedConfigurationProvider_Decrypt_m2834902936 (DpapiProtectedConfigurationProvider_t1041745009 * __this, XmlNode_t616554813 * ___encrypted_node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Configuration.DpapiProtectedConfigurationProvider::Encrypt(System.Xml.XmlNode)
extern "C"  XmlNode_t616554813 * DpapiProtectedConfigurationProvider_Encrypt_m930395280 (DpapiProtectedConfigurationProvider_t1041745009 * __this, XmlNode_t616554813 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.DpapiProtectedConfigurationProvider::Initialize(System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  void DpapiProtectedConfigurationProvider_Initialize_m1064555455 (DpapiProtectedConfigurationProvider_t1041745009 * __this, String_t* ___name0, NameValueCollection_t3047564564 * ___configurationValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.DpapiProtectedConfigurationProvider::get_UseMachineProtection()
extern "C"  bool DpapiProtectedConfigurationProvider_get_UseMachineProtection_m317608620 (DpapiProtectedConfigurationProvider_t1041745009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
