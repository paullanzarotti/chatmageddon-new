﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PNVerificationNavScreen
struct PNVerificationNavScreen_t710347658;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void PNVerificationNavScreen::.ctor()
extern "C"  void PNVerificationNavScreen__ctor_m2130422837 (PNVerificationNavScreen_t710347658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationNavScreen::Start()
extern "C"  void PNVerificationNavScreen_Start_m227715813 (PNVerificationNavScreen_t710347658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationNavScreen::RegisterUIClose()
extern "C"  void PNVerificationNavScreen_RegisterUIClose_m2139243832 (PNVerificationNavScreen_t710347658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationNavScreen::VerificationUIClosing()
extern "C"  void PNVerificationNavScreen_VerificationUIClosing_m4271025883 (PNVerificationNavScreen_t710347658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void PNVerificationNavScreen_ScreenLoad_m3663988451 (PNVerificationNavScreen_t710347658 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PNVerificationNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void PNVerificationNavScreen_ScreenUnload_m3109697315 (PNVerificationNavScreen_t710347658 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PNVerificationNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool PNVerificationNavScreen_ValidateScreenNavigation_m936599718 (PNVerificationNavScreen_t710347658 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
