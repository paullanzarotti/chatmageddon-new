﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// InternetReachability/<InternetPollCheck>c__Iterator0
struct U3CInternetPollCheckU3Ec__Iterator0_t3202327804;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InternetReachability/<InternetPollCheck>c__Iterator0/<InternetPollCheck>c__AnonStorey1
struct  U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569  : public Il2CppObject
{
public:
	// System.Boolean InternetReachability/<InternetPollCheck>c__Iterator0/<InternetPollCheck>c__AnonStorey1::polled
	bool ___polled_0;
	// System.Boolean InternetReachability/<InternetPollCheck>c__Iterator0/<InternetPollCheck>c__AnonStorey1::checkingInternet
	bool ___checkingInternet_1;
	// InternetReachability/<InternetPollCheck>c__Iterator0 InternetReachability/<InternetPollCheck>c__Iterator0/<InternetPollCheck>c__AnonStorey1::<>f__ref$0
	U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_polled_0() { return static_cast<int32_t>(offsetof(U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569, ___polled_0)); }
	inline bool get_polled_0() const { return ___polled_0; }
	inline bool* get_address_of_polled_0() { return &___polled_0; }
	inline void set_polled_0(bool value)
	{
		___polled_0 = value;
	}

	inline static int32_t get_offset_of_checkingInternet_1() { return static_cast<int32_t>(offsetof(U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569, ___checkingInternet_1)); }
	inline bool get_checkingInternet_1() const { return ___checkingInternet_1; }
	inline bool* get_address_of_checkingInternet_1() { return &___checkingInternet_1; }
	inline void set_checkingInternet_1(bool value)
	{
		___checkingInternet_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569, ___U3CU3Ef__refU240_2)); }
	inline U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CInternetPollCheckU3Ec__Iterator0_t3202327804 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
