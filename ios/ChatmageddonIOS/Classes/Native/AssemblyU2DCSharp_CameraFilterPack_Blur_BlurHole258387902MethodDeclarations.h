﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_BlurHole
struct CameraFilterPack_Blur_BlurHole_t258387902;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_BlurHole::.ctor()
extern "C"  void CameraFilterPack_Blur_BlurHole__ctor_m1069892141 (CameraFilterPack_Blur_BlurHole_t258387902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_BlurHole::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_BlurHole_get_material_m1458130244 (CameraFilterPack_Blur_BlurHole_t258387902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::Start()
extern "C"  void CameraFilterPack_Blur_BlurHole_Start_m1028831997 (CameraFilterPack_Blur_BlurHole_t258387902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_BlurHole_OnRenderImage_m3898225549 (CameraFilterPack_Blur_BlurHole_t258387902 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::OnValidate()
extern "C"  void CameraFilterPack_Blur_BlurHole_OnValidate_m1229517514 (CameraFilterPack_Blur_BlurHole_t258387902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::Update()
extern "C"  void CameraFilterPack_Blur_BlurHole_Update_m3844086492 (CameraFilterPack_Blur_BlurHole_t258387902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::OnDisable()
extern "C"  void CameraFilterPack_Blur_BlurHole_OnDisable_m3735858914 (CameraFilterPack_Blur_BlurHole_t258387902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
