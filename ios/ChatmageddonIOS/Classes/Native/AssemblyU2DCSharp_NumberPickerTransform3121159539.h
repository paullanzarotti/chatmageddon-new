﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NumberPickerTransform
struct  NumberPickerTransform_t3121159539  : public MonoBehaviour_t1158329972
{
public:
	// UILabel NumberPickerTransform::pickerLabel
	UILabel_t1795115428 * ___pickerLabel_2;

public:
	inline static int32_t get_offset_of_pickerLabel_2() { return static_cast<int32_t>(offsetof(NumberPickerTransform_t3121159539, ___pickerLabel_2)); }
	inline UILabel_t1795115428 * get_pickerLabel_2() const { return ___pickerLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_pickerLabel_2() { return &___pickerLabel_2; }
	inline void set_pickerLabel_2(UILabel_t1795115428 * value)
	{
		___pickerLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___pickerLabel_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
