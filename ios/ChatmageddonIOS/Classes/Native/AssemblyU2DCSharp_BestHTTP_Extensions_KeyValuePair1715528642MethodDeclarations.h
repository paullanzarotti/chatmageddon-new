﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BestHTTP.Extensions.KeyValuePairList
struct KeyValuePairList_t1715528642;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>
struct List_1_t191583276;
// System.String
struct String_t;
// BestHTTP.Extensions.HeaderValue
struct HeaderValue_t822462144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeaderValue822462144.h"

// System.Void BestHTTP.Extensions.KeyValuePairList::.ctor()
extern "C"  void KeyValuePairList__ctor_m763360957 (KeyValuePairList_t1715528642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue> BestHTTP.Extensions.KeyValuePairList::get_Values()
extern "C"  List_1_t191583276 * KeyValuePairList_get_Values_m1695800291 (KeyValuePairList_t1715528642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.Extensions.KeyValuePairList::set_Values(System.Collections.Generic.List`1<BestHTTP.Extensions.HeaderValue>)
extern "C"  void KeyValuePairList_set_Values_m3621937006 (KeyValuePairList_t1715528642 * __this, List_1_t191583276 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.Extensions.KeyValuePairList::TryGet(System.String,BestHTTP.Extensions.HeaderValue&)
extern "C"  bool KeyValuePairList_TryGet_m3229960078 (KeyValuePairList_t1715528642 * __this, String_t* ___value0, HeaderValue_t822462144 ** ___param1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
