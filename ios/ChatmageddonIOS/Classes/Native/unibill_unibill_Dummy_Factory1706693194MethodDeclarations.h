﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// unibill.Dummy.IWindowsIAP
struct IWindowsIAP_t2654797964;
// unibill.Dummy.Product[]
struct ProductU5BU5D_t4190069746;

#include "codegen/il2cpp-codegen.h"

// unibill.Dummy.IWindowsIAP unibill.Dummy.Factory::Create(System.Boolean,unibill.Dummy.Product[])
extern "C"  Il2CppObject * Factory_Create_m4265076366 (Il2CppObject * __this /* static, unused */, bool ___mocked0, ProductU5BU5D_t4190069746* ___products1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
