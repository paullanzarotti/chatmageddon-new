﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotateButton
struct RotateButton_t2565449149;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void RotateButton::.ctor()
extern "C"  void RotateButton__ctor_m947590798 (RotateButton_t2565449149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateButton::OnButtonClick()
extern "C"  void RotateButton_OnButtonClick_m4221834837 (RotateButton_t2565449149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateButton::CheckRotate()
extern "C"  void RotateButton_CheckRotate_m2287921723 (RotateButton_t2565449149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateButton::OnButtonRotate()
extern "C"  void RotateButton_OnButtonRotate_m479426034 (RotateButton_t2565449149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateButton::OnButtonReset()
extern "C"  void RotateButton_OnButtonReset_m3464891262 (RotateButton_t2565449149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateButton::ResetButton()
extern "C"  void RotateButton_ResetButton_m2279041439 (RotateButton_t2565449149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RotateButton::Rotate()
extern "C"  Il2CppObject * RotateButton_Rotate_m525908635 (RotateButton_t2565449149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
