﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Psycho
struct CameraFilterPack_FX_Psycho_t3893392668;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Psycho::.ctor()
extern "C"  void CameraFilterPack_FX_Psycho__ctor_m2188768675 (CameraFilterPack_FX_Psycho_t3893392668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Psycho::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Psycho_get_material_m2639090794 (CameraFilterPack_FX_Psycho_t3893392668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::Start()
extern "C"  void CameraFilterPack_FX_Psycho_Start_m385893831 (CameraFilterPack_FX_Psycho_t3893392668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Psycho_OnRenderImage_m2071482719 (CameraFilterPack_FX_Psycho_t3893392668 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::OnValidate()
extern "C"  void CameraFilterPack_FX_Psycho_OnValidate_m1871639188 (CameraFilterPack_FX_Psycho_t3893392668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::Update()
extern "C"  void CameraFilterPack_FX_Psycho_Update_m3794076442 (CameraFilterPack_FX_Psycho_t3893392668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::OnDisable()
extern "C"  void CameraFilterPack_FX_Psycho_OnDisable_m1110830556 (CameraFilterPack_FX_Psycho_t3893392668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
