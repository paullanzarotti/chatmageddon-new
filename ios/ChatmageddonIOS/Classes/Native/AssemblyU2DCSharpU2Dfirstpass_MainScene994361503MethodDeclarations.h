﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainScene
struct MainScene_t994361503;

#include "codegen/il2cpp-codegen.h"

// System.Void MainScene::.ctor()
extern "C"  void MainScene__ctor_m2503248832 (MainScene_t994361503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainScene::Start()
extern "C"  void MainScene_Start_m165558864 (MainScene_t994361503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainScene::OnGUI()
extern "C"  void MainScene_OnGUI_m789724816 (MainScene_t994361503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
