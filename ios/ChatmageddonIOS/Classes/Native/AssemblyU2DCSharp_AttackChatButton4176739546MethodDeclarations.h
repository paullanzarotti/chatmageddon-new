﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackChatButton
struct AttackChatButton_t4176739546;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackChatButton::.ctor()
extern "C"  void AttackChatButton__ctor_m2371448167 (AttackChatButton_t4176739546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackChatButton::OnButtonClick()
extern "C"  void AttackChatButton_OnButtonClick_m1755998946 (AttackChatButton_t4176739546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
