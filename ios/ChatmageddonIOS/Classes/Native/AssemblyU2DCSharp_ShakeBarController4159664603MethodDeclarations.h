﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShakeBarController
struct ShakeBarController_t4159664603;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ShakeBarController::.ctor()
extern "C"  void ShakeBarController__ctor_m3724120590 (ShakeBarController_t4159664603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeBarController::StartShakeChecking()
extern "C"  void ShakeBarController_StartShakeChecking_m3807653894 (ShakeBarController_t4159664603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ShakeBarController::CheckShake()
extern "C"  Il2CppObject * ShakeBarController_CheckShake_m2112559162 (ShakeBarController_t4159664603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeBarController::UpdateProgressBar()
extern "C"  void ShakeBarController_UpdateProgressBar_m3101122735 (ShakeBarController_t4159664603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeBarController::DefendMissile()
extern "C"  void ShakeBarController_DefendMissile_m4110814222 (ShakeBarController_t4159664603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
