﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RetryConnectionButton
struct RetryConnectionButton_t1348204850;

#include "codegen/il2cpp-codegen.h"

// System.Void RetryConnectionButton::.ctor()
extern "C"  void RetryConnectionButton__ctor_m4215406339 (RetryConnectionButton_t1348204850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RetryConnectionButton::OnButtonClick()
extern "C"  void RetryConnectionButton_OnButtonClick_m3116865098 (RetryConnectionButton_t1348204850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RetryConnectionButton::SetButtonActive(System.Boolean,System.Boolean)
extern "C"  void RetryConnectionButton_SetButtonActive_m784759327 (RetryConnectionButton_t1348204850 * __this, bool ___active0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
