﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_DarkColor
struct CameraFilterPack_Colors_DarkColor_t3998561101;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_DarkColor::.ctor()
extern "C"  void CameraFilterPack_Colors_DarkColor__ctor_m640457480 (CameraFilterPack_Colors_DarkColor_t3998561101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_DarkColor::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_DarkColor_get_material_m2106906273 (CameraFilterPack_Colors_DarkColor_t3998561101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::Start()
extern "C"  void CameraFilterPack_Colors_DarkColor_Start_m3032745832 (CameraFilterPack_Colors_DarkColor_t3998561101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_DarkColor_OnRenderImage_m2690513944 (CameraFilterPack_Colors_DarkColor_t3998561101 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::OnValidate()
extern "C"  void CameraFilterPack_Colors_DarkColor_OnValidate_m2058459811 (CameraFilterPack_Colors_DarkColor_t3998561101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::Update()
extern "C"  void CameraFilterPack_Colors_DarkColor_Update_m1774522093 (CameraFilterPack_Colors_DarkColor_t3998561101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::OnDisable()
extern "C"  void CameraFilterPack_Colors_DarkColor_OnDisable_m3802244865 (CameraFilterPack_Colors_DarkColor_t3998561101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
