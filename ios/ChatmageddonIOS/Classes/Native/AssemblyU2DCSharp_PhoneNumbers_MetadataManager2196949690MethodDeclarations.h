﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.MetadataManager
struct MetadataManager_t2196949690;
// System.String
struct String_t;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.MetadataManager::.ctor()
extern "C"  void MetadataManager__ctor_m1351371871 (MetadataManager_t2196949690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.MetadataManager::LoadMedataFromFile(System.String)
extern "C"  void MetadataManager_LoadMedataFromFile_m996249331 (Il2CppObject * __this /* static, unused */, String_t* ___filePrefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.MetadataManager::GetAlternateFormatsForCountry(System.Int32)
extern "C"  PhoneMetadata_t366861403 * MetadataManager_GetAlternateFormatsForCountry_m2854305351 (Il2CppObject * __this /* static, unused */, int32_t ___countryCallingCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.MetadataManager::.cctor()
extern "C"  void MetadataManager__cctor_m2975562206 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
