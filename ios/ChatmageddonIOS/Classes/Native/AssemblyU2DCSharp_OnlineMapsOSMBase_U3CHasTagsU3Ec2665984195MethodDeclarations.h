﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMBase/<HasTags>c__AnonStorey4
struct U3CHasTagsU3Ec__AnonStorey4_t2665984195;
// OnlineMapsOSMTag
struct OnlineMapsOSMTag_t3629071465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMTag3629071465.h"

// System.Void OnlineMapsOSMBase/<HasTags>c__AnonStorey4::.ctor()
extern "C"  void U3CHasTagsU3Ec__AnonStorey4__ctor_m2906025936 (U3CHasTagsU3Ec__AnonStorey4_t2665984195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsOSMBase/<HasTags>c__AnonStorey4::<>m__0(OnlineMapsOSMTag)
extern "C"  bool U3CHasTagsU3Ec__AnonStorey4_U3CU3Em__0_m2763053480 (U3CHasTagsU3Ec__AnonStorey4_t2665984195 * __this, OnlineMapsOSMTag_t3629071465 * ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
