﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Antialiasing_FXAA
struct CameraFilterPack_Antialiasing_FXAA_t3457893842;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Antialiasing_FXAA::.ctor()
extern "C"  void CameraFilterPack_Antialiasing_FXAA__ctor_m1446216205 (CameraFilterPack_Antialiasing_FXAA_t3457893842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Antialiasing_FXAA::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Antialiasing_FXAA_get_material_m703211356 (CameraFilterPack_Antialiasing_FXAA_t3457893842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::Start()
extern "C"  void CameraFilterPack_Antialiasing_FXAA_Start_m3563196221 (CameraFilterPack_Antialiasing_FXAA_t3457893842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Antialiasing_FXAA_OnRenderImage_m1815626229 (CameraFilterPack_Antialiasing_FXAA_t3457893842 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::Update()
extern "C"  void CameraFilterPack_Antialiasing_FXAA_Update_m670617300 (CameraFilterPack_Antialiasing_FXAA_t3457893842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::OnDisable()
extern "C"  void CameraFilterPack_Antialiasing_FXAA_OnDisable_m1596561954 (CameraFilterPack_Antialiasing_FXAA_t3457893842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
