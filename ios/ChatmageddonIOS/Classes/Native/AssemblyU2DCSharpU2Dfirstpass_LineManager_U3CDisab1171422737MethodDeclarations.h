﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LineManager/<DisableLine>c__Iterator0
struct U3CDisableLineU3Ec__Iterator0_t1171422737;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LineManager/<DisableLine>c__Iterator0::.ctor()
extern "C"  void U3CDisableLineU3Ec__Iterator0__ctor_m2240095266 (U3CDisableLineU3Ec__Iterator0_t1171422737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LineManager/<DisableLine>c__Iterator0::MoveNext()
extern "C"  bool U3CDisableLineU3Ec__Iterator0_MoveNext_m887329130 (U3CDisableLineU3Ec__Iterator0_t1171422737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LineManager/<DisableLine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDisableLineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2432054488 (U3CDisableLineU3Ec__Iterator0_t1171422737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LineManager/<DisableLine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDisableLineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3013026944 (U3CDisableLineU3Ec__Iterator0_t1171422737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager/<DisableLine>c__Iterator0::Dispose()
extern "C"  void U3CDisableLineU3Ec__Iterator0_Dispose_m2552749041 (U3CDisableLineU3Ec__Iterator0_t1171422737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LineManager/<DisableLine>c__Iterator0::Reset()
extern "C"  void U3CDisableLineU3Ec__Iterator0_Reset_m246397647 (U3CDisableLineU3Ec__Iterator0_t1171422737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
