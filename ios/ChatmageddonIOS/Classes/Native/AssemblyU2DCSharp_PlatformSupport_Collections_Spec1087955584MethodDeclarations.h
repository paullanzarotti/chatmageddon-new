﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformSupport.Collections.Specialized.ReadOnlyList
struct ReadOnlyList_t1087955584;
// System.Collections.IList
struct IList_t3321498491;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void PlatformSupport.Collections.Specialized.ReadOnlyList::.ctor(System.Collections.IList)
extern "C"  void ReadOnlyList__ctor_m1060375804 (ReadOnlyList_t1087955584 * __this, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlatformSupport.Collections.Specialized.ReadOnlyList::get_Count()
extern "C"  int32_t ReadOnlyList_get_Count_m960684939 (ReadOnlyList_t1087955584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlatformSupport.Collections.Specialized.ReadOnlyList::get_IsReadOnly()
extern "C"  bool ReadOnlyList_get_IsReadOnly_m3461366416 (ReadOnlyList_t1087955584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlatformSupport.Collections.Specialized.ReadOnlyList::get_IsFixedSize()
extern "C"  bool ReadOnlyList_get_IsFixedSize_m2503089055 (ReadOnlyList_t1087955584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlatformSupport.Collections.Specialized.ReadOnlyList::get_IsSynchronized()
extern "C"  bool ReadOnlyList_get_IsSynchronized_m33011576 (ReadOnlyList_t1087955584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlatformSupport.Collections.Specialized.ReadOnlyList::get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyList_get_Item_m3952168295 (ReadOnlyList_t1087955584 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.ReadOnlyList::set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyList_set_Item_m891731944 (ReadOnlyList_t1087955584 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlatformSupport.Collections.Specialized.ReadOnlyList::get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyList_get_SyncRoot_m175045268 (ReadOnlyList_t1087955584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlatformSupport.Collections.Specialized.ReadOnlyList::Add(System.Object)
extern "C"  int32_t ReadOnlyList_Add_m2823836076 (ReadOnlyList_t1087955584 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.ReadOnlyList::Clear()
extern "C"  void ReadOnlyList_Clear_m1416858588 (ReadOnlyList_t1087955584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlatformSupport.Collections.Specialized.ReadOnlyList::Contains(System.Object)
extern "C"  bool ReadOnlyList_Contains_m697335314 (ReadOnlyList_t1087955584 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.ReadOnlyList::CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyList_CopyTo_m2596594766 (ReadOnlyList_t1087955584 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlatformSupport.Collections.Specialized.ReadOnlyList::GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyList_GetEnumerator_m3148306071 (ReadOnlyList_t1087955584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlatformSupport.Collections.Specialized.ReadOnlyList::IndexOf(System.Object)
extern "C"  int32_t ReadOnlyList_IndexOf_m3111635066 (ReadOnlyList_t1087955584 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.ReadOnlyList::Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyList_Insert_m3256126971 (ReadOnlyList_t1087955584 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.ReadOnlyList::Remove(System.Object)
extern "C"  void ReadOnlyList_Remove_m101169667 (ReadOnlyList_t1087955584 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformSupport.Collections.Specialized.ReadOnlyList::RemoveAt(System.Int32)
extern "C"  void ReadOnlyList_RemoveAt_m1131341221 (ReadOnlyList_t1087955584 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
