﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// PhoneNumbers.PhoneRegex
struct PhoneRegex_t3216508019;
// System.Collections.Generic.List`1<PhoneNumbers.NumberFormat>
struct List_1_t4105827652;
// PhoneNumbers.RegexCache
struct RegexCache_t1271678307;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.AsYouTypeFormatter
struct  AsYouTypeFormatter_t993724987  : public Il2CppObject
{
public:
	// System.String PhoneNumbers.AsYouTypeFormatter::currentOutput
	String_t* ___currentOutput_0;
	// System.Text.StringBuilder PhoneNumbers.AsYouTypeFormatter::formattingTemplate
	StringBuilder_t1221177846 * ___formattingTemplate_1;
	// System.String PhoneNumbers.AsYouTypeFormatter::currentFormattingPattern
	String_t* ___currentFormattingPattern_2;
	// System.Text.StringBuilder PhoneNumbers.AsYouTypeFormatter::accruedInput
	StringBuilder_t1221177846 * ___accruedInput_3;
	// System.Text.StringBuilder PhoneNumbers.AsYouTypeFormatter::accruedInputWithoutFormatting
	StringBuilder_t1221177846 * ___accruedInputWithoutFormatting_4;
	// System.Boolean PhoneNumbers.AsYouTypeFormatter::ableToFormat
	bool ___ableToFormat_5;
	// System.Boolean PhoneNumbers.AsYouTypeFormatter::inputHasFormatting
	bool ___inputHasFormatting_6;
	// System.Boolean PhoneNumbers.AsYouTypeFormatter::isInternationalFormatting
	bool ___isInternationalFormatting_7;
	// System.Boolean PhoneNumbers.AsYouTypeFormatter::isExpectingCountryCallingCode
	bool ___isExpectingCountryCallingCode_8;
	// PhoneNumbers.PhoneNumberUtil PhoneNumbers.AsYouTypeFormatter::phoneUtil
	PhoneNumberUtil_t4155573397 * ___phoneUtil_9;
	// System.String PhoneNumbers.AsYouTypeFormatter::defaultCountry
	String_t* ___defaultCountry_10;
	// PhoneNumbers.PhoneMetadata PhoneNumbers.AsYouTypeFormatter::defaultMetaData
	PhoneMetadata_t366861403 * ___defaultMetaData_12;
	// PhoneNumbers.PhoneMetadata PhoneNumbers.AsYouTypeFormatter::currentMetaData
	PhoneMetadata_t366861403 * ___currentMetaData_13;
	// System.String PhoneNumbers.AsYouTypeFormatter::digitPlaceholder
	String_t* ___digitPlaceholder_18;
	// System.Text.RegularExpressions.Regex PhoneNumbers.AsYouTypeFormatter::digitPattern
	Regex_t1803876613 * ___digitPattern_19;
	// System.Int32 PhoneNumbers.AsYouTypeFormatter::lastMatchPosition
	int32_t ___lastMatchPosition_20;
	// System.Int32 PhoneNumbers.AsYouTypeFormatter::originalPosition
	int32_t ___originalPosition_21;
	// System.Int32 PhoneNumbers.AsYouTypeFormatter::positionToRemember
	int32_t ___positionToRemember_22;
	// System.Text.StringBuilder PhoneNumbers.AsYouTypeFormatter::prefixBeforeNationalNumber
	StringBuilder_t1221177846 * ___prefixBeforeNationalNumber_23;
	// System.String PhoneNumbers.AsYouTypeFormatter::nationalPrefixExtracted
	String_t* ___nationalPrefixExtracted_24;
	// System.Text.StringBuilder PhoneNumbers.AsYouTypeFormatter::nationalNumber
	StringBuilder_t1221177846 * ___nationalNumber_25;
	// System.Collections.Generic.List`1<PhoneNumbers.NumberFormat> PhoneNumbers.AsYouTypeFormatter::possibleFormats
	List_1_t4105827652 * ___possibleFormats_26;
	// PhoneNumbers.RegexCache PhoneNumbers.AsYouTypeFormatter::regexCache
	RegexCache_t1271678307 * ___regexCache_27;

public:
	inline static int32_t get_offset_of_currentOutput_0() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___currentOutput_0)); }
	inline String_t* get_currentOutput_0() const { return ___currentOutput_0; }
	inline String_t** get_address_of_currentOutput_0() { return &___currentOutput_0; }
	inline void set_currentOutput_0(String_t* value)
	{
		___currentOutput_0 = value;
		Il2CppCodeGenWriteBarrier(&___currentOutput_0, value);
	}

	inline static int32_t get_offset_of_formattingTemplate_1() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___formattingTemplate_1)); }
	inline StringBuilder_t1221177846 * get_formattingTemplate_1() const { return ___formattingTemplate_1; }
	inline StringBuilder_t1221177846 ** get_address_of_formattingTemplate_1() { return &___formattingTemplate_1; }
	inline void set_formattingTemplate_1(StringBuilder_t1221177846 * value)
	{
		___formattingTemplate_1 = value;
		Il2CppCodeGenWriteBarrier(&___formattingTemplate_1, value);
	}

	inline static int32_t get_offset_of_currentFormattingPattern_2() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___currentFormattingPattern_2)); }
	inline String_t* get_currentFormattingPattern_2() const { return ___currentFormattingPattern_2; }
	inline String_t** get_address_of_currentFormattingPattern_2() { return &___currentFormattingPattern_2; }
	inline void set_currentFormattingPattern_2(String_t* value)
	{
		___currentFormattingPattern_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentFormattingPattern_2, value);
	}

	inline static int32_t get_offset_of_accruedInput_3() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___accruedInput_3)); }
	inline StringBuilder_t1221177846 * get_accruedInput_3() const { return ___accruedInput_3; }
	inline StringBuilder_t1221177846 ** get_address_of_accruedInput_3() { return &___accruedInput_3; }
	inline void set_accruedInput_3(StringBuilder_t1221177846 * value)
	{
		___accruedInput_3 = value;
		Il2CppCodeGenWriteBarrier(&___accruedInput_3, value);
	}

	inline static int32_t get_offset_of_accruedInputWithoutFormatting_4() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___accruedInputWithoutFormatting_4)); }
	inline StringBuilder_t1221177846 * get_accruedInputWithoutFormatting_4() const { return ___accruedInputWithoutFormatting_4; }
	inline StringBuilder_t1221177846 ** get_address_of_accruedInputWithoutFormatting_4() { return &___accruedInputWithoutFormatting_4; }
	inline void set_accruedInputWithoutFormatting_4(StringBuilder_t1221177846 * value)
	{
		___accruedInputWithoutFormatting_4 = value;
		Il2CppCodeGenWriteBarrier(&___accruedInputWithoutFormatting_4, value);
	}

	inline static int32_t get_offset_of_ableToFormat_5() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___ableToFormat_5)); }
	inline bool get_ableToFormat_5() const { return ___ableToFormat_5; }
	inline bool* get_address_of_ableToFormat_5() { return &___ableToFormat_5; }
	inline void set_ableToFormat_5(bool value)
	{
		___ableToFormat_5 = value;
	}

	inline static int32_t get_offset_of_inputHasFormatting_6() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___inputHasFormatting_6)); }
	inline bool get_inputHasFormatting_6() const { return ___inputHasFormatting_6; }
	inline bool* get_address_of_inputHasFormatting_6() { return &___inputHasFormatting_6; }
	inline void set_inputHasFormatting_6(bool value)
	{
		___inputHasFormatting_6 = value;
	}

	inline static int32_t get_offset_of_isInternationalFormatting_7() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___isInternationalFormatting_7)); }
	inline bool get_isInternationalFormatting_7() const { return ___isInternationalFormatting_7; }
	inline bool* get_address_of_isInternationalFormatting_7() { return &___isInternationalFormatting_7; }
	inline void set_isInternationalFormatting_7(bool value)
	{
		___isInternationalFormatting_7 = value;
	}

	inline static int32_t get_offset_of_isExpectingCountryCallingCode_8() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___isExpectingCountryCallingCode_8)); }
	inline bool get_isExpectingCountryCallingCode_8() const { return ___isExpectingCountryCallingCode_8; }
	inline bool* get_address_of_isExpectingCountryCallingCode_8() { return &___isExpectingCountryCallingCode_8; }
	inline void set_isExpectingCountryCallingCode_8(bool value)
	{
		___isExpectingCountryCallingCode_8 = value;
	}

	inline static int32_t get_offset_of_phoneUtil_9() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___phoneUtil_9)); }
	inline PhoneNumberUtil_t4155573397 * get_phoneUtil_9() const { return ___phoneUtil_9; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_phoneUtil_9() { return &___phoneUtil_9; }
	inline void set_phoneUtil_9(PhoneNumberUtil_t4155573397 * value)
	{
		___phoneUtil_9 = value;
		Il2CppCodeGenWriteBarrier(&___phoneUtil_9, value);
	}

	inline static int32_t get_offset_of_defaultCountry_10() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___defaultCountry_10)); }
	inline String_t* get_defaultCountry_10() const { return ___defaultCountry_10; }
	inline String_t** get_address_of_defaultCountry_10() { return &___defaultCountry_10; }
	inline void set_defaultCountry_10(String_t* value)
	{
		___defaultCountry_10 = value;
		Il2CppCodeGenWriteBarrier(&___defaultCountry_10, value);
	}

	inline static int32_t get_offset_of_defaultMetaData_12() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___defaultMetaData_12)); }
	inline PhoneMetadata_t366861403 * get_defaultMetaData_12() const { return ___defaultMetaData_12; }
	inline PhoneMetadata_t366861403 ** get_address_of_defaultMetaData_12() { return &___defaultMetaData_12; }
	inline void set_defaultMetaData_12(PhoneMetadata_t366861403 * value)
	{
		___defaultMetaData_12 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMetaData_12, value);
	}

	inline static int32_t get_offset_of_currentMetaData_13() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___currentMetaData_13)); }
	inline PhoneMetadata_t366861403 * get_currentMetaData_13() const { return ___currentMetaData_13; }
	inline PhoneMetadata_t366861403 ** get_address_of_currentMetaData_13() { return &___currentMetaData_13; }
	inline void set_currentMetaData_13(PhoneMetadata_t366861403 * value)
	{
		___currentMetaData_13 = value;
		Il2CppCodeGenWriteBarrier(&___currentMetaData_13, value);
	}

	inline static int32_t get_offset_of_digitPlaceholder_18() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___digitPlaceholder_18)); }
	inline String_t* get_digitPlaceholder_18() const { return ___digitPlaceholder_18; }
	inline String_t** get_address_of_digitPlaceholder_18() { return &___digitPlaceholder_18; }
	inline void set_digitPlaceholder_18(String_t* value)
	{
		___digitPlaceholder_18 = value;
		Il2CppCodeGenWriteBarrier(&___digitPlaceholder_18, value);
	}

	inline static int32_t get_offset_of_digitPattern_19() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___digitPattern_19)); }
	inline Regex_t1803876613 * get_digitPattern_19() const { return ___digitPattern_19; }
	inline Regex_t1803876613 ** get_address_of_digitPattern_19() { return &___digitPattern_19; }
	inline void set_digitPattern_19(Regex_t1803876613 * value)
	{
		___digitPattern_19 = value;
		Il2CppCodeGenWriteBarrier(&___digitPattern_19, value);
	}

	inline static int32_t get_offset_of_lastMatchPosition_20() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___lastMatchPosition_20)); }
	inline int32_t get_lastMatchPosition_20() const { return ___lastMatchPosition_20; }
	inline int32_t* get_address_of_lastMatchPosition_20() { return &___lastMatchPosition_20; }
	inline void set_lastMatchPosition_20(int32_t value)
	{
		___lastMatchPosition_20 = value;
	}

	inline static int32_t get_offset_of_originalPosition_21() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___originalPosition_21)); }
	inline int32_t get_originalPosition_21() const { return ___originalPosition_21; }
	inline int32_t* get_address_of_originalPosition_21() { return &___originalPosition_21; }
	inline void set_originalPosition_21(int32_t value)
	{
		___originalPosition_21 = value;
	}

	inline static int32_t get_offset_of_positionToRemember_22() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___positionToRemember_22)); }
	inline int32_t get_positionToRemember_22() const { return ___positionToRemember_22; }
	inline int32_t* get_address_of_positionToRemember_22() { return &___positionToRemember_22; }
	inline void set_positionToRemember_22(int32_t value)
	{
		___positionToRemember_22 = value;
	}

	inline static int32_t get_offset_of_prefixBeforeNationalNumber_23() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___prefixBeforeNationalNumber_23)); }
	inline StringBuilder_t1221177846 * get_prefixBeforeNationalNumber_23() const { return ___prefixBeforeNationalNumber_23; }
	inline StringBuilder_t1221177846 ** get_address_of_prefixBeforeNationalNumber_23() { return &___prefixBeforeNationalNumber_23; }
	inline void set_prefixBeforeNationalNumber_23(StringBuilder_t1221177846 * value)
	{
		___prefixBeforeNationalNumber_23 = value;
		Il2CppCodeGenWriteBarrier(&___prefixBeforeNationalNumber_23, value);
	}

	inline static int32_t get_offset_of_nationalPrefixExtracted_24() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___nationalPrefixExtracted_24)); }
	inline String_t* get_nationalPrefixExtracted_24() const { return ___nationalPrefixExtracted_24; }
	inline String_t** get_address_of_nationalPrefixExtracted_24() { return &___nationalPrefixExtracted_24; }
	inline void set_nationalPrefixExtracted_24(String_t* value)
	{
		___nationalPrefixExtracted_24 = value;
		Il2CppCodeGenWriteBarrier(&___nationalPrefixExtracted_24, value);
	}

	inline static int32_t get_offset_of_nationalNumber_25() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___nationalNumber_25)); }
	inline StringBuilder_t1221177846 * get_nationalNumber_25() const { return ___nationalNumber_25; }
	inline StringBuilder_t1221177846 ** get_address_of_nationalNumber_25() { return &___nationalNumber_25; }
	inline void set_nationalNumber_25(StringBuilder_t1221177846 * value)
	{
		___nationalNumber_25 = value;
		Il2CppCodeGenWriteBarrier(&___nationalNumber_25, value);
	}

	inline static int32_t get_offset_of_possibleFormats_26() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___possibleFormats_26)); }
	inline List_1_t4105827652 * get_possibleFormats_26() const { return ___possibleFormats_26; }
	inline List_1_t4105827652 ** get_address_of_possibleFormats_26() { return &___possibleFormats_26; }
	inline void set_possibleFormats_26(List_1_t4105827652 * value)
	{
		___possibleFormats_26 = value;
		Il2CppCodeGenWriteBarrier(&___possibleFormats_26, value);
	}

	inline static int32_t get_offset_of_regexCache_27() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987, ___regexCache_27)); }
	inline RegexCache_t1271678307 * get_regexCache_27() const { return ___regexCache_27; }
	inline RegexCache_t1271678307 ** get_address_of_regexCache_27() { return &___regexCache_27; }
	inline void set_regexCache_27(RegexCache_t1271678307 * value)
	{
		___regexCache_27 = value;
		Il2CppCodeGenWriteBarrier(&___regexCache_27, value);
	}
};

struct AsYouTypeFormatter_t993724987_StaticFields
{
public:
	// PhoneNumbers.PhoneMetadata PhoneNumbers.AsYouTypeFormatter::EMPTY_METADATA
	PhoneMetadata_t366861403 * ___EMPTY_METADATA_11;
	// System.Text.RegularExpressions.Regex PhoneNumbers.AsYouTypeFormatter::CHARACTER_CLASS_PATTERN
	Regex_t1803876613 * ___CHARACTER_CLASS_PATTERN_14;
	// System.Text.RegularExpressions.Regex PhoneNumbers.AsYouTypeFormatter::STANDALONE_DIGIT_PATTERN
	Regex_t1803876613 * ___STANDALONE_DIGIT_PATTERN_15;
	// PhoneNumbers.PhoneRegex PhoneNumbers.AsYouTypeFormatter::ELIGIBLE_FORMAT_PATTERN
	PhoneRegex_t3216508019 * ___ELIGIBLE_FORMAT_PATTERN_16;
	// System.Int32 PhoneNumbers.AsYouTypeFormatter::MIN_LEADING_DIGITS_LENGTH
	int32_t ___MIN_LEADING_DIGITS_LENGTH_17;

public:
	inline static int32_t get_offset_of_EMPTY_METADATA_11() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987_StaticFields, ___EMPTY_METADATA_11)); }
	inline PhoneMetadata_t366861403 * get_EMPTY_METADATA_11() const { return ___EMPTY_METADATA_11; }
	inline PhoneMetadata_t366861403 ** get_address_of_EMPTY_METADATA_11() { return &___EMPTY_METADATA_11; }
	inline void set_EMPTY_METADATA_11(PhoneMetadata_t366861403 * value)
	{
		___EMPTY_METADATA_11 = value;
		Il2CppCodeGenWriteBarrier(&___EMPTY_METADATA_11, value);
	}

	inline static int32_t get_offset_of_CHARACTER_CLASS_PATTERN_14() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987_StaticFields, ___CHARACTER_CLASS_PATTERN_14)); }
	inline Regex_t1803876613 * get_CHARACTER_CLASS_PATTERN_14() const { return ___CHARACTER_CLASS_PATTERN_14; }
	inline Regex_t1803876613 ** get_address_of_CHARACTER_CLASS_PATTERN_14() { return &___CHARACTER_CLASS_PATTERN_14; }
	inline void set_CHARACTER_CLASS_PATTERN_14(Regex_t1803876613 * value)
	{
		___CHARACTER_CLASS_PATTERN_14 = value;
		Il2CppCodeGenWriteBarrier(&___CHARACTER_CLASS_PATTERN_14, value);
	}

	inline static int32_t get_offset_of_STANDALONE_DIGIT_PATTERN_15() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987_StaticFields, ___STANDALONE_DIGIT_PATTERN_15)); }
	inline Regex_t1803876613 * get_STANDALONE_DIGIT_PATTERN_15() const { return ___STANDALONE_DIGIT_PATTERN_15; }
	inline Regex_t1803876613 ** get_address_of_STANDALONE_DIGIT_PATTERN_15() { return &___STANDALONE_DIGIT_PATTERN_15; }
	inline void set_STANDALONE_DIGIT_PATTERN_15(Regex_t1803876613 * value)
	{
		___STANDALONE_DIGIT_PATTERN_15 = value;
		Il2CppCodeGenWriteBarrier(&___STANDALONE_DIGIT_PATTERN_15, value);
	}

	inline static int32_t get_offset_of_ELIGIBLE_FORMAT_PATTERN_16() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987_StaticFields, ___ELIGIBLE_FORMAT_PATTERN_16)); }
	inline PhoneRegex_t3216508019 * get_ELIGIBLE_FORMAT_PATTERN_16() const { return ___ELIGIBLE_FORMAT_PATTERN_16; }
	inline PhoneRegex_t3216508019 ** get_address_of_ELIGIBLE_FORMAT_PATTERN_16() { return &___ELIGIBLE_FORMAT_PATTERN_16; }
	inline void set_ELIGIBLE_FORMAT_PATTERN_16(PhoneRegex_t3216508019 * value)
	{
		___ELIGIBLE_FORMAT_PATTERN_16 = value;
		Il2CppCodeGenWriteBarrier(&___ELIGIBLE_FORMAT_PATTERN_16, value);
	}

	inline static int32_t get_offset_of_MIN_LEADING_DIGITS_LENGTH_17() { return static_cast<int32_t>(offsetof(AsYouTypeFormatter_t993724987_StaticFields, ___MIN_LEADING_DIGITS_LENGTH_17)); }
	inline int32_t get_MIN_LEADING_DIGITS_LENGTH_17() const { return ___MIN_LEADING_DIGITS_LENGTH_17; }
	inline int32_t* get_address_of_MIN_LEADING_DIGITS_LENGTH_17() { return &___MIN_LEADING_DIGITS_LENGTH_17; }
	inline void set_MIN_LEADING_DIGITS_LENGTH_17(int32_t value)
	{
		___MIN_LEADING_DIGITS_LENGTH_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
