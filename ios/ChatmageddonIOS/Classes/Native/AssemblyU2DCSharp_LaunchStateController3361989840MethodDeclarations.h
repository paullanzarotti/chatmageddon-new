﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchStateController
struct LaunchStateController_t3361989840;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchState3704349340.h"

// System.Void LaunchStateController::.ctor()
extern "C"  void LaunchStateController__ctor_m3871162437 (LaunchStateController_t3361989840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchStateController::ActivateLaunchState(LaunchState)
extern "C"  void LaunchStateController_ActivateLaunchState_m2523522446 (LaunchStateController_t3361989840 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
