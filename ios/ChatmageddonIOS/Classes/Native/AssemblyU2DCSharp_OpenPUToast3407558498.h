﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenPUToast
struct  OpenPUToast_t3407558498  : public SFXButton_t792651341
{
public:
	// System.String OpenPUToast::message
	String_t* ___message_5;
	// System.String OpenPUToast::closeText
	String_t* ___closeText_6;
	// System.String OpenPUToast::title
	String_t* ___title_7;
	// UnityEngine.Texture OpenPUToast::texture
	Texture_t2243626319 * ___texture_8;
	// System.Boolean OpenPUToast::showFade
	bool ___showFade_9;
	// SFX OpenPUToast::sfx
	int32_t ___sfx_10;

public:
	inline static int32_t get_offset_of_message_5() { return static_cast<int32_t>(offsetof(OpenPUToast_t3407558498, ___message_5)); }
	inline String_t* get_message_5() const { return ___message_5; }
	inline String_t** get_address_of_message_5() { return &___message_5; }
	inline void set_message_5(String_t* value)
	{
		___message_5 = value;
		Il2CppCodeGenWriteBarrier(&___message_5, value);
	}

	inline static int32_t get_offset_of_closeText_6() { return static_cast<int32_t>(offsetof(OpenPUToast_t3407558498, ___closeText_6)); }
	inline String_t* get_closeText_6() const { return ___closeText_6; }
	inline String_t** get_address_of_closeText_6() { return &___closeText_6; }
	inline void set_closeText_6(String_t* value)
	{
		___closeText_6 = value;
		Il2CppCodeGenWriteBarrier(&___closeText_6, value);
	}

	inline static int32_t get_offset_of_title_7() { return static_cast<int32_t>(offsetof(OpenPUToast_t3407558498, ___title_7)); }
	inline String_t* get_title_7() const { return ___title_7; }
	inline String_t** get_address_of_title_7() { return &___title_7; }
	inline void set_title_7(String_t* value)
	{
		___title_7 = value;
		Il2CppCodeGenWriteBarrier(&___title_7, value);
	}

	inline static int32_t get_offset_of_texture_8() { return static_cast<int32_t>(offsetof(OpenPUToast_t3407558498, ___texture_8)); }
	inline Texture_t2243626319 * get_texture_8() const { return ___texture_8; }
	inline Texture_t2243626319 ** get_address_of_texture_8() { return &___texture_8; }
	inline void set_texture_8(Texture_t2243626319 * value)
	{
		___texture_8 = value;
		Il2CppCodeGenWriteBarrier(&___texture_8, value);
	}

	inline static int32_t get_offset_of_showFade_9() { return static_cast<int32_t>(offsetof(OpenPUToast_t3407558498, ___showFade_9)); }
	inline bool get_showFade_9() const { return ___showFade_9; }
	inline bool* get_address_of_showFade_9() { return &___showFade_9; }
	inline void set_showFade_9(bool value)
	{
		___showFade_9 = value;
	}

	inline static int32_t get_offset_of_sfx_10() { return static_cast<int32_t>(offsetof(OpenPUToast_t3407558498, ___sfx_10)); }
	inline int32_t get_sfx_10() const { return ___sfx_10; }
	inline int32_t* get_address_of_sfx_10() { return &___sfx_10; }
	inline void set_sfx_10(int32_t value)
	{
		___sfx_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
