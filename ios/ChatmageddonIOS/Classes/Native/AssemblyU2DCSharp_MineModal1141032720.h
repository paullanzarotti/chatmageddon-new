﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// MineInfoButton
struct MineInfoButton_t2712637181;
// ItemInfoController
struct ItemInfoController_t4231956141;
// ItemSwipeController
struct ItemSwipeController_t3011021799;
// UILabel
struct UILabel_t1795115428;
// System.Collections.Generic.List`1<Mine>
struct List_1_t2098562409;
// TweenRotation
struct TweenRotation_t1747194511;
// OpenAudiencePopUpButton
struct OpenAudiencePopUpButton_t2472031080;
// TweenScale
struct TweenScale_t2697902175;
// MineSelectionSwipe
struct MineSelectionSwipe_t3861158053;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineModal
struct  MineModal_t1141032720  : public ModalUI_t2568752073
{
public:
	// System.Int32 MineModal::currentItemIndex
	int32_t ___currentItemIndex_3;
	// UnityEngine.GameObject MineModal::infoObject
	GameObject_t1756533147 * ___infoObject_4;
	// UnityEngine.GameObject MineModal::itemObject
	GameObject_t1756533147 * ___itemObject_5;
	// MineInfoButton MineModal::infoButton
	MineInfoButton_t2712637181 * ___infoButton_6;
	// ItemInfoController MineModal::infoController
	ItemInfoController_t4231956141 * ___infoController_7;
	// ItemSwipeController MineModal::swipeController
	ItemSwipeController_t3011021799 * ___swipeController_8;
	// UILabel MineModal::itemNameLabel
	UILabel_t1795115428 * ___itemNameLabel_9;
	// UILabel MineModal::itemAmountLabel
	UILabel_t1795115428 * ___itemAmountLabel_10;
	// UnityEngine.GameObject MineModal::itemLight
	GameObject_t1756533147 * ___itemLight_11;
	// System.Collections.Generic.List`1<Mine> MineModal::mineItems
	List_1_t2098562409 * ___mineItems_12;
	// TweenRotation MineModal::mineRotationTween
	TweenRotation_t1747194511 * ___mineRotationTween_13;
	// OpenAudiencePopUpButton MineModal::openAudienceButton
	OpenAudiencePopUpButton_t2472031080 * ___openAudienceButton_14;
	// TweenScale MineModal::audiencePUTween
	TweenScale_t2697902175 * ___audiencePUTween_15;
	// MineAudience MineModal::chosenAudience
	int32_t ___chosenAudience_16;
	// MineSelectionSwipe MineModal::swipe
	MineSelectionSwipe_t3861158053 * ___swipe_17;
	// System.Boolean MineModal::showingInfo
	bool ___showingInfo_18;
	// System.Boolean MineModal::firstTimeLoading
	bool ___firstTimeLoading_19;
	// System.Boolean MineModal::audiencePUClosing
	bool ___audiencePUClosing_20;

public:
	inline static int32_t get_offset_of_currentItemIndex_3() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___currentItemIndex_3)); }
	inline int32_t get_currentItemIndex_3() const { return ___currentItemIndex_3; }
	inline int32_t* get_address_of_currentItemIndex_3() { return &___currentItemIndex_3; }
	inline void set_currentItemIndex_3(int32_t value)
	{
		___currentItemIndex_3 = value;
	}

	inline static int32_t get_offset_of_infoObject_4() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___infoObject_4)); }
	inline GameObject_t1756533147 * get_infoObject_4() const { return ___infoObject_4; }
	inline GameObject_t1756533147 ** get_address_of_infoObject_4() { return &___infoObject_4; }
	inline void set_infoObject_4(GameObject_t1756533147 * value)
	{
		___infoObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___infoObject_4, value);
	}

	inline static int32_t get_offset_of_itemObject_5() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___itemObject_5)); }
	inline GameObject_t1756533147 * get_itemObject_5() const { return ___itemObject_5; }
	inline GameObject_t1756533147 ** get_address_of_itemObject_5() { return &___itemObject_5; }
	inline void set_itemObject_5(GameObject_t1756533147 * value)
	{
		___itemObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemObject_5, value);
	}

	inline static int32_t get_offset_of_infoButton_6() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___infoButton_6)); }
	inline MineInfoButton_t2712637181 * get_infoButton_6() const { return ___infoButton_6; }
	inline MineInfoButton_t2712637181 ** get_address_of_infoButton_6() { return &___infoButton_6; }
	inline void set_infoButton_6(MineInfoButton_t2712637181 * value)
	{
		___infoButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___infoButton_6, value);
	}

	inline static int32_t get_offset_of_infoController_7() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___infoController_7)); }
	inline ItemInfoController_t4231956141 * get_infoController_7() const { return ___infoController_7; }
	inline ItemInfoController_t4231956141 ** get_address_of_infoController_7() { return &___infoController_7; }
	inline void set_infoController_7(ItemInfoController_t4231956141 * value)
	{
		___infoController_7 = value;
		Il2CppCodeGenWriteBarrier(&___infoController_7, value);
	}

	inline static int32_t get_offset_of_swipeController_8() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___swipeController_8)); }
	inline ItemSwipeController_t3011021799 * get_swipeController_8() const { return ___swipeController_8; }
	inline ItemSwipeController_t3011021799 ** get_address_of_swipeController_8() { return &___swipeController_8; }
	inline void set_swipeController_8(ItemSwipeController_t3011021799 * value)
	{
		___swipeController_8 = value;
		Il2CppCodeGenWriteBarrier(&___swipeController_8, value);
	}

	inline static int32_t get_offset_of_itemNameLabel_9() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___itemNameLabel_9)); }
	inline UILabel_t1795115428 * get_itemNameLabel_9() const { return ___itemNameLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_itemNameLabel_9() { return &___itemNameLabel_9; }
	inline void set_itemNameLabel_9(UILabel_t1795115428 * value)
	{
		___itemNameLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameLabel_9, value);
	}

	inline static int32_t get_offset_of_itemAmountLabel_10() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___itemAmountLabel_10)); }
	inline UILabel_t1795115428 * get_itemAmountLabel_10() const { return ___itemAmountLabel_10; }
	inline UILabel_t1795115428 ** get_address_of_itemAmountLabel_10() { return &___itemAmountLabel_10; }
	inline void set_itemAmountLabel_10(UILabel_t1795115428 * value)
	{
		___itemAmountLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___itemAmountLabel_10, value);
	}

	inline static int32_t get_offset_of_itemLight_11() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___itemLight_11)); }
	inline GameObject_t1756533147 * get_itemLight_11() const { return ___itemLight_11; }
	inline GameObject_t1756533147 ** get_address_of_itemLight_11() { return &___itemLight_11; }
	inline void set_itemLight_11(GameObject_t1756533147 * value)
	{
		___itemLight_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_11, value);
	}

	inline static int32_t get_offset_of_mineItems_12() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___mineItems_12)); }
	inline List_1_t2098562409 * get_mineItems_12() const { return ___mineItems_12; }
	inline List_1_t2098562409 ** get_address_of_mineItems_12() { return &___mineItems_12; }
	inline void set_mineItems_12(List_1_t2098562409 * value)
	{
		___mineItems_12 = value;
		Il2CppCodeGenWriteBarrier(&___mineItems_12, value);
	}

	inline static int32_t get_offset_of_mineRotationTween_13() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___mineRotationTween_13)); }
	inline TweenRotation_t1747194511 * get_mineRotationTween_13() const { return ___mineRotationTween_13; }
	inline TweenRotation_t1747194511 ** get_address_of_mineRotationTween_13() { return &___mineRotationTween_13; }
	inline void set_mineRotationTween_13(TweenRotation_t1747194511 * value)
	{
		___mineRotationTween_13 = value;
		Il2CppCodeGenWriteBarrier(&___mineRotationTween_13, value);
	}

	inline static int32_t get_offset_of_openAudienceButton_14() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___openAudienceButton_14)); }
	inline OpenAudiencePopUpButton_t2472031080 * get_openAudienceButton_14() const { return ___openAudienceButton_14; }
	inline OpenAudiencePopUpButton_t2472031080 ** get_address_of_openAudienceButton_14() { return &___openAudienceButton_14; }
	inline void set_openAudienceButton_14(OpenAudiencePopUpButton_t2472031080 * value)
	{
		___openAudienceButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___openAudienceButton_14, value);
	}

	inline static int32_t get_offset_of_audiencePUTween_15() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___audiencePUTween_15)); }
	inline TweenScale_t2697902175 * get_audiencePUTween_15() const { return ___audiencePUTween_15; }
	inline TweenScale_t2697902175 ** get_address_of_audiencePUTween_15() { return &___audiencePUTween_15; }
	inline void set_audiencePUTween_15(TweenScale_t2697902175 * value)
	{
		___audiencePUTween_15 = value;
		Il2CppCodeGenWriteBarrier(&___audiencePUTween_15, value);
	}

	inline static int32_t get_offset_of_chosenAudience_16() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___chosenAudience_16)); }
	inline int32_t get_chosenAudience_16() const { return ___chosenAudience_16; }
	inline int32_t* get_address_of_chosenAudience_16() { return &___chosenAudience_16; }
	inline void set_chosenAudience_16(int32_t value)
	{
		___chosenAudience_16 = value;
	}

	inline static int32_t get_offset_of_swipe_17() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___swipe_17)); }
	inline MineSelectionSwipe_t3861158053 * get_swipe_17() const { return ___swipe_17; }
	inline MineSelectionSwipe_t3861158053 ** get_address_of_swipe_17() { return &___swipe_17; }
	inline void set_swipe_17(MineSelectionSwipe_t3861158053 * value)
	{
		___swipe_17 = value;
		Il2CppCodeGenWriteBarrier(&___swipe_17, value);
	}

	inline static int32_t get_offset_of_showingInfo_18() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___showingInfo_18)); }
	inline bool get_showingInfo_18() const { return ___showingInfo_18; }
	inline bool* get_address_of_showingInfo_18() { return &___showingInfo_18; }
	inline void set_showingInfo_18(bool value)
	{
		___showingInfo_18 = value;
	}

	inline static int32_t get_offset_of_firstTimeLoading_19() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___firstTimeLoading_19)); }
	inline bool get_firstTimeLoading_19() const { return ___firstTimeLoading_19; }
	inline bool* get_address_of_firstTimeLoading_19() { return &___firstTimeLoading_19; }
	inline void set_firstTimeLoading_19(bool value)
	{
		___firstTimeLoading_19 = value;
	}

	inline static int32_t get_offset_of_audiencePUClosing_20() { return static_cast<int32_t>(offsetof(MineModal_t1141032720, ___audiencePUClosing_20)); }
	inline bool get_audiencePUClosing_20() const { return ___audiencePUClosing_20; }
	inline bool* get_address_of_audiencePUClosing_20() { return &___audiencePUClosing_20; }
	inline void set_audiencePUClosing_20(bool value)
	{
		___audiencePUClosing_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
