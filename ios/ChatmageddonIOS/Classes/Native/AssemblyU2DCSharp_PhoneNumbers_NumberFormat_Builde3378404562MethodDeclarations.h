﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.NumberFormat/Builder
struct Builder_t3378404562;
// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat441739224.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.NumberFormat/Builder::.ctor()
extern "C"  void Builder__ctor_m1754771831 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::get_ThisBuilder()
extern "C"  Builder_t3378404562 * Builder_get_ThisBuilder_m3470443706 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat/Builder::get_MessageBeingBuilt()
extern "C"  NumberFormat_t441739224 * Builder_get_MessageBeingBuilt_m2175702709 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::Clear()
extern "C"  Builder_t3378404562 * Builder_Clear_m2978571077 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::Clone()
extern "C"  Builder_t3378404562 * Builder_Clone_m245881295 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat/Builder::get_DefaultInstanceForType()
extern "C"  NumberFormat_t441739224 * Builder_get_DefaultInstanceForType_m3375268992 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat/Builder::Build()
extern "C"  NumberFormat_t441739224 * Builder_Build_m2506095094 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat/Builder::BuildPartial()
extern "C"  NumberFormat_t441739224 * Builder_BuildPartial_m4247450735 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::MergeFrom(PhoneNumbers.NumberFormat)
extern "C"  Builder_t3378404562 * Builder_MergeFrom_m3758021494 (Builder_t3378404562 * __this, NumberFormat_t441739224 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat/Builder::get_HasPattern()
extern "C"  bool Builder_get_HasPattern_m2487118576 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat/Builder::get_Pattern()
extern "C"  String_t* Builder_get_Pattern_m2387305899 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.NumberFormat/Builder::set_Pattern(System.String)
extern "C"  void Builder_set_Pattern_m2639642038 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::SetPattern(System.String)
extern "C"  Builder_t3378404562 * Builder_SetPattern_m2886620136 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::ClearPattern()
extern "C"  Builder_t3378404562 * Builder_ClearPattern_m2304034145 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat/Builder::get_HasFormat()
extern "C"  bool Builder_get_HasFormat_m4233720723 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat/Builder::get_Format()
extern "C"  String_t* Builder_get_Format_m2021689812 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.NumberFormat/Builder::set_Format(System.String)
extern "C"  void Builder_set_Format_m3123919271 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::SetFormat(System.String)
extern "C"  Builder_t3378404562 * Builder_SetFormat_m652141379 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::ClearFormat()
extern "C"  Builder_t3378404562 * Builder_ClearFormat_m676641804 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> PhoneNumbers.NumberFormat/Builder::get_LeadingDigitsPatternList()
extern "C"  Il2CppObject* Builder_get_LeadingDigitsPatternList_m1583418546 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.NumberFormat/Builder::get_LeadingDigitsPatternCount()
extern "C"  int32_t Builder_get_LeadingDigitsPatternCount_m1061593163 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat/Builder::GetLeadingDigitsPattern(System.Int32)
extern "C"  String_t* Builder_GetLeadingDigitsPattern_m3394524101 (Builder_t3378404562 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::SetLeadingDigitsPattern(System.Int32,System.String)
extern "C"  Builder_t3378404562 * Builder_SetLeadingDigitsPattern_m3580162673 (Builder_t3378404562 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::AddLeadingDigitsPattern(System.String)
extern "C"  Builder_t3378404562 * Builder_AddLeadingDigitsPattern_m3764295487 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::AddRangeLeadingDigitsPattern(System.Collections.Generic.IEnumerable`1<System.String>)
extern "C"  Builder_t3378404562 * Builder_AddRangeLeadingDigitsPattern_m927044783 (Builder_t3378404562 * __this, Il2CppObject* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::ClearLeadingDigitsPattern()
extern "C"  Builder_t3378404562 * Builder_ClearLeadingDigitsPattern_m1478285733 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat/Builder::get_HasNationalPrefixFormattingRule()
extern "C"  bool Builder_get_HasNationalPrefixFormattingRule_m1006441015 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat/Builder::get_NationalPrefixFormattingRule()
extern "C"  String_t* Builder_get_NationalPrefixFormattingRule_m1342646656 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.NumberFormat/Builder::set_NationalPrefixFormattingRule(System.String)
extern "C"  void Builder_set_NationalPrefixFormattingRule_m4277318499 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::SetNationalPrefixFormattingRule(System.String)
extern "C"  Builder_t3378404562 * Builder_SetNationalPrefixFormattingRule_m1826250303 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::ClearNationalPrefixFormattingRule()
extern "C"  Builder_t3378404562 * Builder_ClearNationalPrefixFormattingRule_m2170442064 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat/Builder::get_HasNationalPrefixOptionalWhenFormatting()
extern "C"  bool Builder_get_HasNationalPrefixOptionalWhenFormatting_m1749783967 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat/Builder::get_NationalPrefixOptionalWhenFormatting()
extern "C"  bool Builder_get_NationalPrefixOptionalWhenFormatting_m734178021 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.NumberFormat/Builder::set_NationalPrefixOptionalWhenFormatting(System.Boolean)
extern "C"  void Builder_set_NationalPrefixOptionalWhenFormatting_m3340801068 (Builder_t3378404562 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::SetNationalPrefixOptionalWhenFormatting(System.Boolean)
extern "C"  Builder_t3378404562 * Builder_SetNationalPrefixOptionalWhenFormatting_m2290355546 (Builder_t3378404562 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::ClearNationalPrefixOptionalWhenFormatting()
extern "C"  Builder_t3378404562 * Builder_ClearNationalPrefixOptionalWhenFormatting_m1970779222 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat/Builder::get_HasDomesticCarrierCodeFormattingRule()
extern "C"  bool Builder_get_HasDomesticCarrierCodeFormattingRule_m128684106 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat/Builder::get_DomesticCarrierCodeFormattingRule()
extern "C"  String_t* Builder_get_DomesticCarrierCodeFormattingRule_m1647365313 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.NumberFormat/Builder::set_DomesticCarrierCodeFormattingRule(System.String)
extern "C"  void Builder_set_DomesticCarrierCodeFormattingRule_m2487745168 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::SetDomesticCarrierCodeFormattingRule(System.String)
extern "C"  Builder_t3378404562 * Builder_SetDomesticCarrierCodeFormattingRule_m462175630 (Builder_t3378404562 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat/Builder::ClearDomesticCarrierCodeFormattingRule()
extern "C"  Builder_t3378404562 * Builder_ClearDomesticCarrierCodeFormattingRule_m4128813607 (Builder_t3378404562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
