﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneMetaTerritory
struct PhoneMetaTerritory_t2073820963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneMetaTerritory::.ctor()
extern "C"  void PhoneMetaTerritory__ctor_m34038588 (PhoneMetaTerritory_t2073820963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneMetaTerritory::.ctor(System.String,System.String,System.Boolean,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Boolean,System.Boolean,System.String,System.Boolean)
extern "C"  void PhoneMetaTerritory__ctor_m1014525578 (PhoneMetaTerritory_t2073820963 * __this, String_t* ___id0, String_t* ___countryCode1, bool ___mainCountryForCode2, String_t* ___leadingDigits3, String_t* ___preferredInternationalPrefix4, String_t* ___internationalPrefix5, String_t* ___nationalPrefix6, String_t* ___nationalPrefixForParsing7, String_t* ___nationalPrefixTransformRule8, String_t* ___preferredExtnPrefix9, String_t* ___nationalPrefixFormattingRule10, bool ___nationalPrefixOptionalWhenFormatting11, bool ___leadingZeroPossible12, String_t* ___carrierCodeFormattingRule13, bool ___mobileNumberPortableRegion14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
