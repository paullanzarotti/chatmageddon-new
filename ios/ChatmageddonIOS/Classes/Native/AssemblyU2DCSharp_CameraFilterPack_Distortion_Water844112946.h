﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Distortion_Water_Drop
struct  CameraFilterPack_Distortion_Water_Drop_t844112946  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Distortion_Water_Drop::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Water_Drop::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Water_Drop::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Water_Drop::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Water_Drop::CenterX
	float ___CenterX_6;
	// System.Single CameraFilterPack_Distortion_Water_Drop::CenterY
	float ___CenterY_7;
	// System.Single CameraFilterPack_Distortion_Water_Drop::WaveIntensity
	float ___WaveIntensity_8;
	// System.Int32 CameraFilterPack_Distortion_Water_Drop::NumberOfWaves
	int32_t ___NumberOfWaves_9;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_CenterX_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___CenterX_6)); }
	inline float get_CenterX_6() const { return ___CenterX_6; }
	inline float* get_address_of_CenterX_6() { return &___CenterX_6; }
	inline void set_CenterX_6(float value)
	{
		___CenterX_6 = value;
	}

	inline static int32_t get_offset_of_CenterY_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___CenterY_7)); }
	inline float get_CenterY_7() const { return ___CenterY_7; }
	inline float* get_address_of_CenterY_7() { return &___CenterY_7; }
	inline void set_CenterY_7(float value)
	{
		___CenterY_7 = value;
	}

	inline static int32_t get_offset_of_WaveIntensity_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___WaveIntensity_8)); }
	inline float get_WaveIntensity_8() const { return ___WaveIntensity_8; }
	inline float* get_address_of_WaveIntensity_8() { return &___WaveIntensity_8; }
	inline void set_WaveIntensity_8(float value)
	{
		___WaveIntensity_8 = value;
	}

	inline static int32_t get_offset_of_NumberOfWaves_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946, ___NumberOfWaves_9)); }
	inline int32_t get_NumberOfWaves_9() const { return ___NumberOfWaves_9; }
	inline int32_t* get_address_of_NumberOfWaves_9() { return &___NumberOfWaves_9; }
	inline void set_NumberOfWaves_9(int32_t value)
	{
		___NumberOfWaves_9 = value;
	}
};

struct CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields
{
public:
	// System.Single CameraFilterPack_Distortion_Water_Drop::ChangeCenterX
	float ___ChangeCenterX_10;
	// System.Single CameraFilterPack_Distortion_Water_Drop::ChangeCenterY
	float ___ChangeCenterY_11;
	// System.Single CameraFilterPack_Distortion_Water_Drop::ChangeWaveIntensity
	float ___ChangeWaveIntensity_12;
	// System.Int32 CameraFilterPack_Distortion_Water_Drop::ChangeNumberOfWaves
	int32_t ___ChangeNumberOfWaves_13;

public:
	inline static int32_t get_offset_of_ChangeCenterX_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields, ___ChangeCenterX_10)); }
	inline float get_ChangeCenterX_10() const { return ___ChangeCenterX_10; }
	inline float* get_address_of_ChangeCenterX_10() { return &___ChangeCenterX_10; }
	inline void set_ChangeCenterX_10(float value)
	{
		___ChangeCenterX_10 = value;
	}

	inline static int32_t get_offset_of_ChangeCenterY_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields, ___ChangeCenterY_11)); }
	inline float get_ChangeCenterY_11() const { return ___ChangeCenterY_11; }
	inline float* get_address_of_ChangeCenterY_11() { return &___ChangeCenterY_11; }
	inline void set_ChangeCenterY_11(float value)
	{
		___ChangeCenterY_11 = value;
	}

	inline static int32_t get_offset_of_ChangeWaveIntensity_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields, ___ChangeWaveIntensity_12)); }
	inline float get_ChangeWaveIntensity_12() const { return ___ChangeWaveIntensity_12; }
	inline float* get_address_of_ChangeWaveIntensity_12() { return &___ChangeWaveIntensity_12; }
	inline void set_ChangeWaveIntensity_12(float value)
	{
		___ChangeWaveIntensity_12 = value;
	}

	inline static int32_t get_offset_of_ChangeNumberOfWaves_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Water_Drop_t844112946_StaticFields, ___ChangeNumberOfWaves_13)); }
	inline int32_t get_ChangeNumberOfWaves_13() const { return ___ChangeNumberOfWaves_13; }
	inline int32_t* get_address_of_ChangeNumberOfWaves_13() { return &___ChangeNumberOfWaves_13; }
	inline void set_ChangeNumberOfWaves_13(int32_t value)
	{
		___ChangeNumberOfWaves_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
