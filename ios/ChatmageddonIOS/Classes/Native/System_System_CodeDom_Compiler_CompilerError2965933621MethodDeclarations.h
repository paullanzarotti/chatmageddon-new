﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.CompilerError
struct CompilerError_t2965933621;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.CodeDom.Compiler.CompilerError::.ctor()
extern "C"  void CompilerError__ctor_m3900526416 (CompilerError_t2965933621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerError::.ctor(System.String,System.Int32,System.Int32,System.String,System.String)
extern "C"  void CompilerError__ctor_m2320790344 (CompilerError_t2965933621 * __this, String_t* ___fileName0, int32_t ___line1, int32_t ___column2, String_t* ___errorNumber3, String_t* ___errorText4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.CompilerError::ToString()
extern "C"  String_t* CompilerError_ToString_m2900413393 (CompilerError_t2965933621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerError::set_Line(System.Int32)
extern "C"  void CompilerError_set_Line_m1441565244 (CompilerError_t2965933621 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerError::set_Column(System.Int32)
extern "C"  void CompilerError_set_Column_m2925988996 (CompilerError_t2965933621 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerError::set_ErrorNumber(System.String)
extern "C"  void CompilerError_set_ErrorNumber_m338118310 (CompilerError_t2965933621 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerError::set_ErrorText(System.String)
extern "C"  void CompilerError_set_ErrorText_m2420415336 (CompilerError_t2965933621 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CodeDom.Compiler.CompilerError::get_IsWarning()
extern "C"  bool CompilerError_get_IsWarning_m708880127 (CompilerError_t2965933621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerError::set_IsWarning(System.Boolean)
extern "C"  void CompilerError_set_IsWarning_m1202562838 (CompilerError_t2965933621 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerError::set_FileName(System.String)
extern "C"  void CompilerError_set_FileName_m519542208 (CompilerError_t2965933621 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
