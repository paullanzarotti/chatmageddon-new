﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneMetaNumberInfo
struct  PhoneMetaNumberInfo_t3299665448  : public Il2CppObject
{
public:
	// System.String PhoneMetaNumberInfo::nationalNumberPattern
	String_t* ___nationalNumberPattern_0;
	// System.String PhoneMetaNumberInfo::possibleNumberPattern
	String_t* ___possibleNumberPattern_1;
	// System.String PhoneMetaNumberInfo::exampleNumber
	String_t* ___exampleNumber_2;

public:
	inline static int32_t get_offset_of_nationalNumberPattern_0() { return static_cast<int32_t>(offsetof(PhoneMetaNumberInfo_t3299665448, ___nationalNumberPattern_0)); }
	inline String_t* get_nationalNumberPattern_0() const { return ___nationalNumberPattern_0; }
	inline String_t** get_address_of_nationalNumberPattern_0() { return &___nationalNumberPattern_0; }
	inline void set_nationalNumberPattern_0(String_t* value)
	{
		___nationalNumberPattern_0 = value;
		Il2CppCodeGenWriteBarrier(&___nationalNumberPattern_0, value);
	}

	inline static int32_t get_offset_of_possibleNumberPattern_1() { return static_cast<int32_t>(offsetof(PhoneMetaNumberInfo_t3299665448, ___possibleNumberPattern_1)); }
	inline String_t* get_possibleNumberPattern_1() const { return ___possibleNumberPattern_1; }
	inline String_t** get_address_of_possibleNumberPattern_1() { return &___possibleNumberPattern_1; }
	inline void set_possibleNumberPattern_1(String_t* value)
	{
		___possibleNumberPattern_1 = value;
		Il2CppCodeGenWriteBarrier(&___possibleNumberPattern_1, value);
	}

	inline static int32_t get_offset_of_exampleNumber_2() { return static_cast<int32_t>(offsetof(PhoneMetaNumberInfo_t3299665448, ___exampleNumber_2)); }
	inline String_t* get_exampleNumber_2() const { return ___exampleNumber_2; }
	inline String_t** get_address_of_exampleNumber_2() { return &___exampleNumber_2; }
	inline void set_exampleNumber_2(String_t* value)
	{
		___exampleNumber_2 = value;
		Il2CppCodeGenWriteBarrier(&___exampleNumber_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
