﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<DeleteAuthViaUDID>c__AnonStorey8<System.Object>
struct U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t3005420310;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<DeleteAuthViaUDID>c__AnonStorey8<System.Object>::.ctor()
extern "C"  void U3CDeleteAuthViaUDIDU3Ec__AnonStorey8__ctor_m3900254611_gshared (U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t3005420310 * __this, const MethodInfo* method);
#define U3CDeleteAuthViaUDIDU3Ec__AnonStorey8__ctor_m3900254611(__this, method) ((  void (*) (U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t3005420310 *, const MethodInfo*))U3CDeleteAuthViaUDIDU3Ec__AnonStorey8__ctor_m3900254611_gshared)(__this, method)
// System.Void BaseServer`1/<DeleteAuthViaUDID>c__AnonStorey8<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_U3CU3Em__0_m2889424834_gshared (U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t3005420310 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_U3CU3Em__0_m2889424834(__this, ___request0, ___response1, method) ((  void (*) (U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t3005420310 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_U3CU3Em__0_m2889424834_gshared)(__this, ___request0, ___response1, method)
