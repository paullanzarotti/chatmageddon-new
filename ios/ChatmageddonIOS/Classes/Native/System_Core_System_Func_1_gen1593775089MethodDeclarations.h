﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_1_gen348874681MethodDeclarations.h"

// System.Void System.Func`1<System.Collections.Generic.IEnumerator`1<PhoneNumbers.PhoneNumberMatch>>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m2143635226(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t1593775089 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m3570736155_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<System.Collections.Generic.IEnumerator`1<PhoneNumbers.PhoneNumberMatch>>::Invoke()
#define Func_1_Invoke_m1161414752(__this, method) ((  Il2CppObject* (*) (Func_1_t1593775089 *, const MethodInfo*))Func_1_Invoke_m846324009_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.Collections.Generic.IEnumerator`1<PhoneNumbers.PhoneNumberMatch>>::BeginInvoke(System.AsyncCallback,System.Object)
#define Func_1_BeginInvoke_m4282822125(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t1593775089 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m478378364_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<System.Collections.Generic.IEnumerator`1<PhoneNumbers.PhoneNumberMatch>>::EndInvoke(System.IAsyncResult)
#define Func_1_EndInvoke_m2307713752(__this, ___result0, method) ((  Il2CppObject* (*) (Func_1_t1593775089 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m1965878547_gshared)(__this, ___result0, method)
