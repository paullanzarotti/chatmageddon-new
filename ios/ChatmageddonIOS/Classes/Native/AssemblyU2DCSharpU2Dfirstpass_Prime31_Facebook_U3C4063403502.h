﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t1831019615;
// Prime31.Facebook
struct Facebook_t3852591386;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.Facebook/<getAppAccessToken>c__AnonStorey7
struct  U3CgetAppAccessTokenU3Ec__AnonStorey7_t4063403502  : public Il2CppObject
{
public:
	// System.Action`1<System.String> Prime31.Facebook/<getAppAccessToken>c__AnonStorey7::completionHandler
	Action_1_t1831019615 * ___completionHandler_0;
	// Prime31.Facebook Prime31.Facebook/<getAppAccessToken>c__AnonStorey7::$this
	Facebook_t3852591386 * ___U24this_1;

public:
	inline static int32_t get_offset_of_completionHandler_0() { return static_cast<int32_t>(offsetof(U3CgetAppAccessTokenU3Ec__AnonStorey7_t4063403502, ___completionHandler_0)); }
	inline Action_1_t1831019615 * get_completionHandler_0() const { return ___completionHandler_0; }
	inline Action_1_t1831019615 ** get_address_of_completionHandler_0() { return &___completionHandler_0; }
	inline void set_completionHandler_0(Action_1_t1831019615 * value)
	{
		___completionHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___completionHandler_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CgetAppAccessTokenU3Ec__AnonStorey7_t4063403502, ___U24this_1)); }
	inline Facebook_t3852591386 * get_U24this_1() const { return ___U24this_1; }
	inline Facebook_t3852591386 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Facebook_t3852591386 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
