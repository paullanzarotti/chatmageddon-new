﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.IntegerValidator
struct IntegerValidator_t2639687534;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Configuration.IntegerValidator::.ctor(System.Int32,System.Int32,System.Boolean,System.Int32)
extern "C"  void IntegerValidator__ctor_m2072671147 (IntegerValidator_t2639687534 * __this, int32_t ___minValue0, int32_t ___maxValue1, bool ___rangeIsExclusive2, int32_t ___resolution3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.IntegerValidator::.ctor(System.Int32,System.Int32,System.Boolean)
extern "C"  void IntegerValidator__ctor_m2630547332 (IntegerValidator_t2639687534 * __this, int32_t ___minValue0, int32_t ___maxValue1, bool ___rangeIsExclusive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.IntegerValidator::.ctor(System.Int32,System.Int32)
extern "C"  void IntegerValidator__ctor_m2743512885 (IntegerValidator_t2639687534 * __this, int32_t ___minValue0, int32_t ___maxValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.IntegerValidator::CanValidate(System.Type)
extern "C"  bool IntegerValidator_CanValidate_m3897253616 (IntegerValidator_t2639687534 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.IntegerValidator::Validate(System.Object)
extern "C"  void IntegerValidator_Validate_m1107880549 (IntegerValidator_t2639687534 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
