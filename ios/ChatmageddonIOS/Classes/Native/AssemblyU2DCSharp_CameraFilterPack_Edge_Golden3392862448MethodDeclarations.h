﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Edge_Golden
struct CameraFilterPack_Edge_Golden_t3392862448;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Edge_Golden::.ctor()
extern "C"  void CameraFilterPack_Edge_Golden__ctor_m3796625827 (CameraFilterPack_Edge_Golden_t3392862448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Golden::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Edge_Golden_get_material_m1582058386 (CameraFilterPack_Edge_Golden_t3392862448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::Start()
extern "C"  void CameraFilterPack_Edge_Golden_Start_m1236295343 (CameraFilterPack_Edge_Golden_t3392862448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Edge_Golden_OnRenderImage_m4061129111 (CameraFilterPack_Edge_Golden_t3392862448 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::Update()
extern "C"  void CameraFilterPack_Edge_Golden_Update_m313019922 (CameraFilterPack_Edge_Golden_t3392862448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::OnDisable()
extern "C"  void CameraFilterPack_Edge_Golden_OnDisable_m1751950636 (CameraFilterPack_Edge_Golden_t3392862448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
