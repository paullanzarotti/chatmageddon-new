﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineAudienceTabController
struct MineAudienceTabController_t1758951344;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"

// System.Void MineAudienceTabController::.ctor()
extern "C"  void MineAudienceTabController__ctor_m1687574215 (MineAudienceTabController_t1758951344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineAudienceTabController::SetActiveAudience(MineAudience)
extern "C"  void MineAudienceTabController_SetActiveAudience_m2458059488 (MineAudienceTabController_t1758951344 * __this, int32_t ___audience0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
