﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.AmazonAppStoreBillingService
struct AmazonAppStoreBillingService_t1988397396;
// Unibill.Impl.IRawAmazonAppStoreBillingInterface
struct IRawAmazonAppStoreBillingInterface_t2991800847;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TransactionDatabase201476183.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.AmazonAppStoreBillingService::.ctor(Unibill.Impl.IRawAmazonAppStoreBillingInterface,Unibill.Impl.ProductIdRemapper,Unibill.Impl.UnibillConfiguration,TransactionDatabase,Uniject.ILogger)
extern "C"  void AmazonAppStoreBillingService__ctor_m3945427587 (AmazonAppStoreBillingService_t1988397396 * __this, Il2CppObject * ___amazon0, ProductIdRemapper_t3313438456 * ___remapper1, UnibillConfiguration_t2915611853 * ___db2, TransactionDatabase_t201476183 * ___tDb3, Il2CppObject * ___logger4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::initialise(Unibill.Impl.IBillingServiceCallback)
extern "C"  void AmazonAppStoreBillingService_initialise_m3672162558 (AmazonAppStoreBillingService_t1988397396 * __this, Il2CppObject * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::purchase(System.String,System.String)
extern "C"  void AmazonAppStoreBillingService_purchase_m3912695705 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::restoreTransactions()
extern "C"  void AmazonAppStoreBillingService_restoreTransactions_m2555839209 (AmazonAppStoreBillingService_t1988397396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onSDKAvailable(System.String)
extern "C"  void AmazonAppStoreBillingService_onSDKAvailable_m1018340038 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___isSandbox0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onGetItemDataFailed()
extern "C"  void AmazonAppStoreBillingService_onGetItemDataFailed_m2428255759 (AmazonAppStoreBillingService_t1988397396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onProductListReceived(System.String)
extern "C"  void AmazonAppStoreBillingService_onProductListReceived_m1850064031 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___productListString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onUserIdRetrieved(System.String)
extern "C"  void AmazonAppStoreBillingService_onUserIdRetrieved_m918283891 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onTransactionsRestored(System.String)
extern "C"  void AmazonAppStoreBillingService_onTransactionsRestored_m3520301990 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___successString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onPurchaseFailed(System.String)
extern "C"  void AmazonAppStoreBillingService_onPurchaseFailed_m1645581453 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onPurchaseCancelled(System.String)
extern "C"  void AmazonAppStoreBillingService_onPurchaseCancelled_m3920028393 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onPurchaseSucceeded(System.String)
extern "C"  void AmazonAppStoreBillingService_onPurchaseSucceeded_m4140835245 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onPurchaseUpdateFailed()
extern "C"  void AmazonAppStoreBillingService_onPurchaseUpdateFailed_m2335978528 (AmazonAppStoreBillingService_t1988397396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AmazonAppStoreBillingService::onPurchaseUpdateSuccess(System.String)
extern "C"  void AmazonAppStoreBillingService_onPurchaseUpdateSuccess_m2944266776 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.AmazonAppStoreBillingService::hasReceipt(System.String)
extern "C"  bool AmazonAppStoreBillingService_hasReceipt_m546706994 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.AmazonAppStoreBillingService::getReceipt(System.String)
extern "C"  String_t* AmazonAppStoreBillingService_getReceipt_m2021107915 (AmazonAppStoreBillingService_t1988397396 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
