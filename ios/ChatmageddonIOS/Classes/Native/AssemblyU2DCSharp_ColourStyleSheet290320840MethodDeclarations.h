﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColourStyleSheet
struct ColourStyleSheet_t290320840;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ColourStyleSheet::.ctor()
extern "C"  void ColourStyleSheet__ctor_m2499410347 (ColourStyleSheet_t290320840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColourStyleSheet::Start()
extern "C"  void ColourStyleSheet_Start_m2757149959 (ColourStyleSheet_t290320840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColourStyleSheet::Update()
extern "C"  void ColourStyleSheet_Update_m3198061194 (ColourStyleSheet_t290320840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color ColourStyleSheet::Find(System.String)
extern "C"  Color_t2020392075  ColourStyleSheet_Find_m3754094673 (ColourStyleSheet_t290320840 * __this, String_t* ____name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
