﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ContainerFilterService
struct ContainerFilterService_t3050250756;
// System.ComponentModel.ComponentCollection
struct ComponentCollection_t737017907;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_ComponentCollection737017907.h"

// System.Void System.ComponentModel.ContainerFilterService::.ctor()
extern "C"  void ContainerFilterService__ctor_m2216195845 (ContainerFilterService_t3050250756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.ComponentCollection System.ComponentModel.ContainerFilterService::FilterComponents(System.ComponentModel.ComponentCollection)
extern "C"  ComponentCollection_t737017907 * ContainerFilterService_FilterComponents_m419579184 (ContainerFilterService_t3050250756 * __this, ComponentCollection_t737017907 * ___components0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
