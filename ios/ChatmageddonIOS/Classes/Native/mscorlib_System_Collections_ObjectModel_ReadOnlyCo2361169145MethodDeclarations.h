﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>
struct ReadOnlyCollection_1_t2361169145;
// System.Collections.Generic.IList`1<FriendNavScreen>
struct IList_1_t2716324054;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FriendNavScreen[]
struct FriendNavScreenU5BU5D_t3451088208;
// System.Collections.Generic.IEnumerator`1<FriendNavScreen>
struct IEnumerator_1_t3945874576;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2923663023_gshared (ReadOnlyCollection_1_t2361169145 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2923663023(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2923663023_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4237053365_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4237053365(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4237053365_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4127496361_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4127496361(__this, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4127496361_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2943740814_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2943740814(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2943740814_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4105177932_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4105177932(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m4105177932_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2728644474_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2728644474(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2728644474_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1421532532_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1421532532(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1421532532_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m122335645_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m122335645(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m122335645_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3388454593_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3388454593(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3388454593_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1847376512_gshared (ReadOnlyCollection_1_t2361169145 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1847376512(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1847376512_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2881980709_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2881980709(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2881980709_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m370417224_gshared (ReadOnlyCollection_1_t2361169145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m370417224(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2361169145 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m370417224_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m741822122_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m741822122(__this, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m741822122_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1916567670_gshared (ReadOnlyCollection_1_t2361169145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1916567670(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2361169145 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1916567670_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m509347790_gshared (ReadOnlyCollection_1_t2361169145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m509347790(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2361169145 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m509347790_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1233555605_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1233555605(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1233555605_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2367031645_gshared (ReadOnlyCollection_1_t2361169145 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2367031645(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2367031645_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1790489875_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1790489875(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1790489875_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3270616320_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3270616320(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3270616320_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3564719072_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3564719072(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3564719072_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3493182873_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3493182873(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3493182873_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3420722732_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3420722732(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3420722732_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1227940569_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1227940569(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1227940569_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2969583090_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2969583090(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2969583090_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3216174775_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3216174775(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3216174775_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m4117075105_gshared (ReadOnlyCollection_1_t2361169145 * __this, FriendNavScreenU5BU5D_t3451088208* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m4117075105(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2361169145 *, FriendNavScreenU5BU5D_t3451088208*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m4117075105_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m575428804_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m575428804(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m575428804_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2789278181_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2789278181(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2789278181_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1948532328_gshared (ReadOnlyCollection_1_t2361169145 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1948532328(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2361169145 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1948532328_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FriendNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m2624561088_gshared (ReadOnlyCollection_1_t2361169145 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2624561088(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2361169145 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2624561088_gshared)(__this, ___index0, method)
