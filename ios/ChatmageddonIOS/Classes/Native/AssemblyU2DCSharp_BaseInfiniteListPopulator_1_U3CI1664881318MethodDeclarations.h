﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseInfiniteListPopulator`1/<ItemIsInvisible>c__Iterator0<System.Object>
struct U3CItemIsInvisibleU3Ec__Iterator0_t1664881318;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseInfiniteListPopulator`1/<ItemIsInvisible>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CItemIsInvisibleU3Ec__Iterator0__ctor_m2449654415_gshared (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 * __this, const MethodInfo* method);
#define U3CItemIsInvisibleU3Ec__Iterator0__ctor_m2449654415(__this, method) ((  void (*) (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 *, const MethodInfo*))U3CItemIsInvisibleU3Ec__Iterator0__ctor_m2449654415_gshared)(__this, method)
// System.Boolean BaseInfiniteListPopulator`1/<ItemIsInvisible>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CItemIsInvisibleU3Ec__Iterator0_MoveNext_m873150593_gshared (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 * __this, const MethodInfo* method);
#define U3CItemIsInvisibleU3Ec__Iterator0_MoveNext_m873150593(__this, method) ((  bool (*) (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 *, const MethodInfo*))U3CItemIsInvisibleU3Ec__Iterator0_MoveNext_m873150593_gshared)(__this, method)
// System.Object BaseInfiniteListPopulator`1/<ItemIsInvisible>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CItemIsInvisibleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438887605_gshared (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 * __this, const MethodInfo* method);
#define U3CItemIsInvisibleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438887605(__this, method) ((  Il2CppObject * (*) (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 *, const MethodInfo*))U3CItemIsInvisibleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3438887605_gshared)(__this, method)
// System.Object BaseInfiniteListPopulator`1/<ItemIsInvisible>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CItemIsInvisibleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1506139309_gshared (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 * __this, const MethodInfo* method);
#define U3CItemIsInvisibleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1506139309(__this, method) ((  Il2CppObject * (*) (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 *, const MethodInfo*))U3CItemIsInvisibleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1506139309_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1/<ItemIsInvisible>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CItemIsInvisibleU3Ec__Iterator0_Dispose_m3969360132_gshared (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 * __this, const MethodInfo* method);
#define U3CItemIsInvisibleU3Ec__Iterator0_Dispose_m3969360132(__this, method) ((  void (*) (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 *, const MethodInfo*))U3CItemIsInvisibleU3Ec__Iterator0_Dispose_m3969360132_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1/<ItemIsInvisible>c__Iterator0<System.Object>::Reset()
extern "C"  void U3CItemIsInvisibleU3Ec__Iterator0_Reset_m1619838302_gshared (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 * __this, const MethodInfo* method);
#define U3CItemIsInvisibleU3Ec__Iterator0_Reset_m1619838302(__this, method) ((  void (*) (U3CItemIsInvisibleU3Ec__Iterator0_t1664881318 *, const MethodInfo*))U3CItemIsInvisibleU3Ec__Iterator0_Reset_m1619838302_gshared)(__this, method)
