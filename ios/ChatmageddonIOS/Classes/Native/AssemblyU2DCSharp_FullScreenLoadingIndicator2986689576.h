﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatmageddonLoadingIndicator
struct ChatmageddonLoadingIndicator_t472672888;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2737355296.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FullScreenLoadingIndicator
struct  FullScreenLoadingIndicator_t2986689576  : public MonoSingleton_1_t2737355296
{
public:
	// ChatmageddonLoadingIndicator FullScreenLoadingIndicator::indicator
	ChatmageddonLoadingIndicator_t472672888 * ___indicator_3;
	// TweenAlpha FullScreenLoadingIndicator::backgroundTween
	TweenAlpha_t2421518635 * ___backgroundTween_4;
	// System.Boolean FullScreenLoadingIndicator::backgroundShowing
	bool ___backgroundShowing_5;

public:
	inline static int32_t get_offset_of_indicator_3() { return static_cast<int32_t>(offsetof(FullScreenLoadingIndicator_t2986689576, ___indicator_3)); }
	inline ChatmageddonLoadingIndicator_t472672888 * get_indicator_3() const { return ___indicator_3; }
	inline ChatmageddonLoadingIndicator_t472672888 ** get_address_of_indicator_3() { return &___indicator_3; }
	inline void set_indicator_3(ChatmageddonLoadingIndicator_t472672888 * value)
	{
		___indicator_3 = value;
		Il2CppCodeGenWriteBarrier(&___indicator_3, value);
	}

	inline static int32_t get_offset_of_backgroundTween_4() { return static_cast<int32_t>(offsetof(FullScreenLoadingIndicator_t2986689576, ___backgroundTween_4)); }
	inline TweenAlpha_t2421518635 * get_backgroundTween_4() const { return ___backgroundTween_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundTween_4() { return &___backgroundTween_4; }
	inline void set_backgroundTween_4(TweenAlpha_t2421518635 * value)
	{
		___backgroundTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTween_4, value);
	}

	inline static int32_t get_offset_of_backgroundShowing_5() { return static_cast<int32_t>(offsetof(FullScreenLoadingIndicator_t2986689576, ___backgroundShowing_5)); }
	inline bool get_backgroundShowing_5() const { return ___backgroundShowing_5; }
	inline bool* get_address_of_backgroundShowing_5() { return &___backgroundShowing_5; }
	inline void set_backgroundShowing_5(bool value)
	{
		___backgroundShowing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
