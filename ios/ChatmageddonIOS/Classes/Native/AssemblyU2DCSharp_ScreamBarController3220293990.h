﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScreamMiniGame
struct ScreamMiniGame_t2794661384;
// MicService
struct MicService_t591677994;
// UIPanelProgressBar
struct UIPanelProgressBar_t2014302056;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreamBarController
struct  ScreamBarController_t3220293990  : public MonoBehaviour_t1158329972
{
public:
	// ScreamMiniGame ScreamBarController::miniGame
	ScreamMiniGame_t2794661384 * ___miniGame_2;
	// System.Single ScreamBarController::maxScreamSeconds
	float ___maxScreamSeconds_3;
	// System.Single ScreamBarController::minimumMicVolume
	float ___minimumMicVolume_4;
	// System.Single ScreamBarController::maximumMicVolume
	float ___maximumMicVolume_5;
	// MicService ScreamBarController::micChecker
	MicService_t591677994 * ___micChecker_6;
	// UIPanelProgressBar ScreamBarController::progressBar
	UIPanelProgressBar_t2014302056 * ___progressBar_7;
	// UnityEngine.Coroutine ScreamBarController::screamCheckRoutine
	Coroutine_t2299508840 * ___screamCheckRoutine_8;
	// UILabel ScreamBarController::timeLabel
	UILabel_t1795115428 * ___timeLabel_9;
	// System.Single ScreamBarController::totalTime
	float ___totalTime_10;

public:
	inline static int32_t get_offset_of_miniGame_2() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___miniGame_2)); }
	inline ScreamMiniGame_t2794661384 * get_miniGame_2() const { return ___miniGame_2; }
	inline ScreamMiniGame_t2794661384 ** get_address_of_miniGame_2() { return &___miniGame_2; }
	inline void set_miniGame_2(ScreamMiniGame_t2794661384 * value)
	{
		___miniGame_2 = value;
		Il2CppCodeGenWriteBarrier(&___miniGame_2, value);
	}

	inline static int32_t get_offset_of_maxScreamSeconds_3() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___maxScreamSeconds_3)); }
	inline float get_maxScreamSeconds_3() const { return ___maxScreamSeconds_3; }
	inline float* get_address_of_maxScreamSeconds_3() { return &___maxScreamSeconds_3; }
	inline void set_maxScreamSeconds_3(float value)
	{
		___maxScreamSeconds_3 = value;
	}

	inline static int32_t get_offset_of_minimumMicVolume_4() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___minimumMicVolume_4)); }
	inline float get_minimumMicVolume_4() const { return ___minimumMicVolume_4; }
	inline float* get_address_of_minimumMicVolume_4() { return &___minimumMicVolume_4; }
	inline void set_minimumMicVolume_4(float value)
	{
		___minimumMicVolume_4 = value;
	}

	inline static int32_t get_offset_of_maximumMicVolume_5() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___maximumMicVolume_5)); }
	inline float get_maximumMicVolume_5() const { return ___maximumMicVolume_5; }
	inline float* get_address_of_maximumMicVolume_5() { return &___maximumMicVolume_5; }
	inline void set_maximumMicVolume_5(float value)
	{
		___maximumMicVolume_5 = value;
	}

	inline static int32_t get_offset_of_micChecker_6() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___micChecker_6)); }
	inline MicService_t591677994 * get_micChecker_6() const { return ___micChecker_6; }
	inline MicService_t591677994 ** get_address_of_micChecker_6() { return &___micChecker_6; }
	inline void set_micChecker_6(MicService_t591677994 * value)
	{
		___micChecker_6 = value;
		Il2CppCodeGenWriteBarrier(&___micChecker_6, value);
	}

	inline static int32_t get_offset_of_progressBar_7() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___progressBar_7)); }
	inline UIPanelProgressBar_t2014302056 * get_progressBar_7() const { return ___progressBar_7; }
	inline UIPanelProgressBar_t2014302056 ** get_address_of_progressBar_7() { return &___progressBar_7; }
	inline void set_progressBar_7(UIPanelProgressBar_t2014302056 * value)
	{
		___progressBar_7 = value;
		Il2CppCodeGenWriteBarrier(&___progressBar_7, value);
	}

	inline static int32_t get_offset_of_screamCheckRoutine_8() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___screamCheckRoutine_8)); }
	inline Coroutine_t2299508840 * get_screamCheckRoutine_8() const { return ___screamCheckRoutine_8; }
	inline Coroutine_t2299508840 ** get_address_of_screamCheckRoutine_8() { return &___screamCheckRoutine_8; }
	inline void set_screamCheckRoutine_8(Coroutine_t2299508840 * value)
	{
		___screamCheckRoutine_8 = value;
		Il2CppCodeGenWriteBarrier(&___screamCheckRoutine_8, value);
	}

	inline static int32_t get_offset_of_timeLabel_9() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___timeLabel_9)); }
	inline UILabel_t1795115428 * get_timeLabel_9() const { return ___timeLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_9() { return &___timeLabel_9; }
	inline void set_timeLabel_9(UILabel_t1795115428 * value)
	{
		___timeLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_9, value);
	}

	inline static int32_t get_offset_of_totalTime_10() { return static_cast<int32_t>(offsetof(ScreamBarController_t3220293990, ___totalTime_10)); }
	inline float get_totalTime_10() const { return ___totalTime_10; }
	inline float* get_address_of_totalTime_10() { return &___totalTime_10; }
	inline void set_totalTime_10(float value)
	{
		___totalTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
