﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Prime31.JsonExtensions::toJson(System.Collections.IDictionary)
extern "C"  String_t* JsonExtensions_toJson_m516856193 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Prime31.JsonExtensions::listFromJson(System.String)
extern "C"  List_1_t2058570427 * JsonExtensions_listFromJson_m3329287167 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.JsonExtensions::dictionaryFromJson(System.String)
extern "C"  Dictionary_2_t309261261 * JsonExtensions_dictionaryFromJson_m2183911158 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
