﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Timer
struct  Timer_t2917042437  : public Il2CppObject
{
public:
	// System.Single Timer::myTimer
	float ___myTimer_0;
	// System.Boolean Timer::timerActive
	bool ___timerActive_1;

public:
	inline static int32_t get_offset_of_myTimer_0() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___myTimer_0)); }
	inline float get_myTimer_0() const { return ___myTimer_0; }
	inline float* get_address_of_myTimer_0() { return &___myTimer_0; }
	inline void set_myTimer_0(float value)
	{
		___myTimer_0 = value;
	}

	inline static int32_t get_offset_of_timerActive_1() { return static_cast<int32_t>(offsetof(Timer_t2917042437, ___timerActive_1)); }
	inline bool get_timerActive_1() const { return ___timerActive_1; }
	inline bool* get_address_of_timerActive_1() { return &___timerActive_1; }
	inline void set_timerActive_1(bool value)
	{
		___timerActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
