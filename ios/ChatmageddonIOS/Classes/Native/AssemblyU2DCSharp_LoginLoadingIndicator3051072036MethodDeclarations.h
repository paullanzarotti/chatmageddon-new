﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginLoadingIndicator
struct LoginLoadingIndicator_t3051072036;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoginLoadingIndicator::.ctor()
extern "C"  void LoginLoadingIndicator__ctor_m3710398779 (LoginLoadingIndicator_t3051072036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginLoadingIndicator::ShowLoadedInidication(System.Int32,System.String)
extern "C"  void LoginLoadingIndicator_ShowLoadedInidication_m730977473 (LoginLoadingIndicator_t3051072036 * __this, int32_t ___indicator0, String_t* ___loadingMessage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
