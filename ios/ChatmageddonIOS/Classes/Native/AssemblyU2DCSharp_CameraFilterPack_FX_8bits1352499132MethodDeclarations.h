﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_8bits
struct CameraFilterPack_FX_8bits_t1352499132;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_8bits::.ctor()
extern "C"  void CameraFilterPack_FX_8bits__ctor_m268091789 (CameraFilterPack_FX_8bits_t1352499132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_8bits::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_8bits_get_material_m3469653744 (CameraFilterPack_FX_8bits_t1352499132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::Start()
extern "C"  void CameraFilterPack_FX_8bits_Start_m4057298349 (CameraFilterPack_FX_8bits_t1352499132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_8bits_OnRenderImage_m338743077 (CameraFilterPack_FX_8bits_t1352499132 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::OnValidate()
extern "C"  void CameraFilterPack_FX_8bits_OnValidate_m2626806204 (CameraFilterPack_FX_8bits_t1352499132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::Update()
extern "C"  void CameraFilterPack_FX_8bits_Update_m880212746 (CameraFilterPack_FX_8bits_t1352499132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::OnDisable()
extern "C"  void CameraFilterPack_FX_8bits_OnDisable_m2398341756 (CameraFilterPack_FX_8bits_t1352499132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
