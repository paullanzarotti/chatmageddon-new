﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AttackReviewNavScreen
struct AttackReviewNavScreen_t173504869;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyActiveMissileButton
struct  BuyActiveMissileButton_t2138134254  : public SFXButton_t792651341
{
public:
	// AttackReviewNavScreen BuyActiveMissileButton::screen
	AttackReviewNavScreen_t173504869 * ___screen_5;
	// UnityEngine.Color BuyActiveMissileButton::activeColour
	Color_t2020392075  ___activeColour_6;
	// UnityEngine.Color BuyActiveMissileButton::inactiveColour
	Color_t2020392075  ___inactiveColour_7;
	// UILabel BuyActiveMissileButton::buyLabel
	UILabel_t1795115428 * ___buyLabel_8;

public:
	inline static int32_t get_offset_of_screen_5() { return static_cast<int32_t>(offsetof(BuyActiveMissileButton_t2138134254, ___screen_5)); }
	inline AttackReviewNavScreen_t173504869 * get_screen_5() const { return ___screen_5; }
	inline AttackReviewNavScreen_t173504869 ** get_address_of_screen_5() { return &___screen_5; }
	inline void set_screen_5(AttackReviewNavScreen_t173504869 * value)
	{
		___screen_5 = value;
		Il2CppCodeGenWriteBarrier(&___screen_5, value);
	}

	inline static int32_t get_offset_of_activeColour_6() { return static_cast<int32_t>(offsetof(BuyActiveMissileButton_t2138134254, ___activeColour_6)); }
	inline Color_t2020392075  get_activeColour_6() const { return ___activeColour_6; }
	inline Color_t2020392075 * get_address_of_activeColour_6() { return &___activeColour_6; }
	inline void set_activeColour_6(Color_t2020392075  value)
	{
		___activeColour_6 = value;
	}

	inline static int32_t get_offset_of_inactiveColour_7() { return static_cast<int32_t>(offsetof(BuyActiveMissileButton_t2138134254, ___inactiveColour_7)); }
	inline Color_t2020392075  get_inactiveColour_7() const { return ___inactiveColour_7; }
	inline Color_t2020392075 * get_address_of_inactiveColour_7() { return &___inactiveColour_7; }
	inline void set_inactiveColour_7(Color_t2020392075  value)
	{
		___inactiveColour_7 = value;
	}

	inline static int32_t get_offset_of_buyLabel_8() { return static_cast<int32_t>(offsetof(BuyActiveMissileButton_t2138134254, ___buyLabel_8)); }
	inline UILabel_t1795115428 * get_buyLabel_8() const { return ___buyLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_buyLabel_8() { return &___buyLabel_8; }
	inline void set_buyLabel_8(UILabel_t1795115428 * value)
	{
		___buyLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___buyLabel_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
