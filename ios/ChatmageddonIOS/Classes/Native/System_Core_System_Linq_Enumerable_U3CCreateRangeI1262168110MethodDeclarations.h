﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD
struct U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::.ctor()
extern "C"  void U3CCreateRangeIteratorU3Ec__IteratorD__ctor_m1634992811 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::System.Collections.Generic.IEnumerator<int>.get_Current()
extern "C"  int32_t U3CCreateRangeIteratorU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CintU3E_get_Current_m706114402 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateRangeIteratorU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m658298289 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateRangeIteratorU3Ec__IteratorD_System_Collections_IEnumerable_GetEnumerator_m409719138 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Int32> System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::System.Collections.Generic.IEnumerable<int>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateRangeIteratorU3Ec__IteratorD_System_Collections_Generic_IEnumerableU3CintU3E_GetEnumerator_m2601164493 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::MoveNext()
extern "C"  bool U3CCreateRangeIteratorU3Ec__IteratorD_MoveNext_m1864784237 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::Dispose()
extern "C"  void U3CCreateRangeIteratorU3Ec__IteratorD_Dispose_m979561602 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::Reset()
extern "C"  void U3CCreateRangeIteratorU3Ec__IteratorD_Reset_m2711920844 (U3CCreateRangeIteratorU3Ec__IteratorD_t1262168110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
