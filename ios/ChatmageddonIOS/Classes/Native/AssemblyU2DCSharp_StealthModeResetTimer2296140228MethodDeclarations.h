﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeResetTimer
struct StealthModeResetTimer_t2296140228;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void StealthModeResetTimer::.ctor()
extern "C"  void StealthModeResetTimer__ctor_m2536244921 (StealthModeResetTimer_t2296140228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeResetTimer::StartTimer()
extern "C"  void StealthModeResetTimer_StartTimer_m3652224158 (StealthModeResetTimer_t2296140228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeResetTimer::StopTimer()
extern "C"  void StealthModeResetTimer_StopTimer_m2829404758 (StealthModeResetTimer_t2296140228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StealthModeResetTimer::CalculateResetTime()
extern "C"  Il2CppObject * StealthModeResetTimer_CalculateResetTime_m2659798027 (StealthModeResetTimer_t2296140228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeResetTimer::SetTimeLabel(System.TimeSpan)
extern "C"  void StealthModeResetTimer_SetTimeLabel_m1482282832 (StealthModeResetTimer_t2296140228 * __this, TimeSpan_t3430258949  ___span0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
