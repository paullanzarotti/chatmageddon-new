﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackNavigationController/<FirstLoadWait>c__Iterator0
struct U3CFirstLoadWaitU3Ec__Iterator0_t2076676339;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackNavigationController/<FirstLoadWait>c__Iterator0::.ctor()
extern "C"  void U3CFirstLoadWaitU3Ec__Iterator0__ctor_m2265360056 (U3CFirstLoadWaitU3Ec__Iterator0_t2076676339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackNavigationController/<FirstLoadWait>c__Iterator0::MoveNext()
extern "C"  bool U3CFirstLoadWaitU3Ec__Iterator0_MoveNext_m1012760184 (U3CFirstLoadWaitU3Ec__Iterator0_t2076676339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackNavigationController/<FirstLoadWait>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFirstLoadWaitU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3500813072 (U3CFirstLoadWaitU3Ec__Iterator0_t2076676339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackNavigationController/<FirstLoadWait>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFirstLoadWaitU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4248884040 (U3CFirstLoadWaitU3Ec__Iterator0_t2076676339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController/<FirstLoadWait>c__Iterator0::Dispose()
extern "C"  void U3CFirstLoadWaitU3Ec__Iterator0_Dispose_m4259645705 (U3CFirstLoadWaitU3Ec__Iterator0_t2076676339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackNavigationController/<FirstLoadWait>c__Iterator0::Reset()
extern "C"  void U3CFirstLoadWaitU3Ec__Iterator0_Reset_m540656495 (U3CFirstLoadWaitU3Ec__Iterator0_t2076676339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
