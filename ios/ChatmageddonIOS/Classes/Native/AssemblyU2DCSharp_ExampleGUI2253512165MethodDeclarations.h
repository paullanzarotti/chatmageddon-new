﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExampleGUI
struct ExampleGUI_t2253512165;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ExampleGUI::.ctor()
extern "C"  void ExampleGUI__ctor_m1687372528 (ExampleGUI_t2253512165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExampleGUI::OnEnable()
extern "C"  void ExampleGUI_OnEnable_m2009812432 (ExampleGUI_t2253512165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExampleGUI::OnGUI()
extern "C"  void ExampleGUI_OnGUI_m38611400 (ExampleGUI_t2253512165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExampleGUI::FindLocation()
extern "C"  void ExampleGUI_FindLocation_m3113471482 (ExampleGUI_t2253512165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExampleGUI::<FindLocation>m__0(System.String)
extern "C"  void ExampleGUI_U3CFindLocationU3Em__0_m4155538901 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
