﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1447017461.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<Audience>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2954006471_gshared (Nullable_1_t1447017461 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2954006471(__this, ___value0, method) ((  void (*) (Nullable_1_t1447017461 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2954006471_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Audience>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m960751206_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m960751206(__this, method) ((  bool (*) (Nullable_1_t1447017461 *, const MethodInfo*))Nullable_1_get_HasValue_m960751206_gshared)(__this, method)
// T System.Nullable`1<Audience>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1353274088_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1353274088(__this, method) ((  int32_t (*) (Nullable_1_t1447017461 *, const MethodInfo*))Nullable_1_get_Value_m1353274088_gshared)(__this, method)
// System.Boolean System.Nullable`1<Audience>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1014408754_gshared (Nullable_1_t1447017461 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1014408754(__this, ___other0, method) ((  bool (*) (Nullable_1_t1447017461 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1014408754_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Audience>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m325067495_gshared (Nullable_1_t1447017461 * __this, Nullable_1_t1447017461  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m325067495(__this, ___other0, method) ((  bool (*) (Nullable_1_t1447017461 *, Nullable_1_t1447017461 , const MethodInfo*))Nullable_1_Equals_m325067495_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Audience>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m771049174_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m771049174(__this, method) ((  int32_t (*) (Nullable_1_t1447017461 *, const MethodInfo*))Nullable_1_GetHashCode_m771049174_gshared)(__this, method)
// T System.Nullable`1<Audience>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1912957201_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1912957201(__this, method) ((  int32_t (*) (Nullable_1_t1447017461 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1912957201_gshared)(__this, method)
// System.String System.Nullable`1<Audience>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3601254404_gshared (Nullable_1_t1447017461 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3601254404(__this, method) ((  String_t* (*) (Nullable_1_t1447017461 *, const MethodInfo*))Nullable_1_ToString_m3601254404_gshared)(__this, method)
