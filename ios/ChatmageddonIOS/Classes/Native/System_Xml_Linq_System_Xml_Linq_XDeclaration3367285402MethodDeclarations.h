﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XDeclaration
struct XDeclaration_t3367285402;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XDeclaration3367285402.h"

// System.Void System.Xml.Linq.XDeclaration::.ctor(System.String,System.String,System.String)
extern "C"  void XDeclaration__ctor_m617866219 (XDeclaration_t3367285402 * __this, String_t* ___version0, String_t* ___encoding1, String_t* ___standalone2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XDeclaration::.ctor(System.Xml.Linq.XDeclaration)
extern "C"  void XDeclaration__ctor_m4202021713 (XDeclaration_t3367285402 * __this, XDeclaration_t3367285402 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDeclaration::get_Encoding()
extern "C"  String_t* XDeclaration_get_Encoding_m2963864996 (XDeclaration_t3367285402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDeclaration::get_Standalone()
extern "C"  String_t* XDeclaration_get_Standalone_m1945036438 (XDeclaration_t3367285402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDeclaration::get_Version()
extern "C"  String_t* XDeclaration_get_Version_m2743306735 (XDeclaration_t3367285402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XDeclaration::ToString()
extern "C"  String_t* XDeclaration_ToString_m2249492008 (XDeclaration_t3367285402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
