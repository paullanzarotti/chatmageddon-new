﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen548570015MethodDeclarations.h"

// System.Void NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::.ctor()
#define NavigationController_2__ctor_m835481310(__this, method) ((  void (*) (NavigationController_2_t2867551090 *, const MethodInfo*))NavigationController_2__ctor_m3648062236_gshared)(__this, method)
// System.Void NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m2702842904(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t2867551090 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1507831522_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m2536960917(__this, method) ((  void (*) (NavigationController_2_t2867551090 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3154846949_gshared)(__this, method)
// System.Void NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m2416937006(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t2867551090 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1086604804_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m473839882(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t2867551090 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m680059976_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m862126735(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t2867551090 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m998082891_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m695757188(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t2867551090 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3845100182_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m1085723394(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t2867551090 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2033329720_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<PlayerProfileNavigationController,PlayerProfileNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m1632530970(__this, ___screen0, method) ((  void (*) (NavigationController_2_t2867551090 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m3772286536_gshared)(__this, ___screen0, method)
