﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "AssemblyU2DCSharp_OnlineMapsDrawingElement539447654.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsDrawingLine
struct  OnlineMapsDrawingLine_t2877694824  : public OnlineMapsDrawingElement_t539447654
{
public:
	// UnityEngine.Color OnlineMapsDrawingLine::color
	Color_t2020392075  ___color_21;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> OnlineMapsDrawingLine::points
	List_1_t1612828711 * ___points_22;
	// UnityEngine.Texture2D OnlineMapsDrawingLine::texture
	Texture2D_t3542995729 * ___texture_23;
	// System.Single OnlineMapsDrawingLine::weight
	float ___weight_24;

public:
	inline static int32_t get_offset_of_color_21() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingLine_t2877694824, ___color_21)); }
	inline Color_t2020392075  get_color_21() const { return ___color_21; }
	inline Color_t2020392075 * get_address_of_color_21() { return &___color_21; }
	inline void set_color_21(Color_t2020392075  value)
	{
		___color_21 = value;
	}

	inline static int32_t get_offset_of_points_22() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingLine_t2877694824, ___points_22)); }
	inline List_1_t1612828711 * get_points_22() const { return ___points_22; }
	inline List_1_t1612828711 ** get_address_of_points_22() { return &___points_22; }
	inline void set_points_22(List_1_t1612828711 * value)
	{
		___points_22 = value;
		Il2CppCodeGenWriteBarrier(&___points_22, value);
	}

	inline static int32_t get_offset_of_texture_23() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingLine_t2877694824, ___texture_23)); }
	inline Texture2D_t3542995729 * get_texture_23() const { return ___texture_23; }
	inline Texture2D_t3542995729 ** get_address_of_texture_23() { return &___texture_23; }
	inline void set_texture_23(Texture2D_t3542995729 * value)
	{
		___texture_23 = value;
		Il2CppCodeGenWriteBarrier(&___texture_23, value);
	}

	inline static int32_t get_offset_of_weight_24() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingLine_t2877694824, ___weight_24)); }
	inline float get_weight_24() const { return ___weight_24; }
	inline float* get_address_of_weight_24() { return &___weight_24; }
	inline void set_weight_24(float value)
	{
		___weight_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
