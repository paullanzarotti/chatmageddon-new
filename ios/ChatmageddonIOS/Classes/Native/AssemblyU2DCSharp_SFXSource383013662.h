﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_BaseAudioSource2241787848.h"
#include "AssemblyU2DCSharp_SFX1271914439.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SFXSource
struct  SFXSource_t383013662  : public BaseAudioSource_t2241787848
{
public:
	// SFX SFXSource::sfxType
	int32_t ___sfxType_10;

public:
	inline static int32_t get_offset_of_sfxType_10() { return static_cast<int32_t>(offsetof(SFXSource_t383013662, ___sfxType_10)); }
	inline int32_t get_sfxType_10() const { return ___sfxType_10; }
	inline int32_t* get_address_of_sfxType_10() { return &___sfxType_10; }
	inline void set_sfxType_10(int32_t value)
	{
		___sfxType_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
