﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen1726521459MethodDeclarations.h"

// System.Void NavigationController`2<DefendModalNavigationController,DefendNavScreen>::.ctor()
#define NavigationController_2__ctor_m3430847317(__this, method) ((  void (*) (NavigationController_2_t3412936559 *, const MethodInfo*))NavigationController_2__ctor_m321013590_gshared)(__this, method)
// System.Void NavigationController`2<DefendModalNavigationController,DefendNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m3623700867(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t3412936559 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1859701416_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<DefendModalNavigationController,DefendNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m518724780(__this, method) ((  void (*) (NavigationController_2_t3412936559 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m534062593_gshared)(__this, method)
// System.Void NavigationController`2<DefendModalNavigationController,DefendNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m1834500123(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t3412936559 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1021390126_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<DefendModalNavigationController,DefendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m3921987327(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t3412936559 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m527669130_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<DefendModalNavigationController,DefendNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m1817185638(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t3412936559 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m1209895939_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<DefendModalNavigationController,DefendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m687764803(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t3412936559 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m4012482644_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<DefendModalNavigationController,DefendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m1259505863(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t3412936559 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m4027734450_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<DefendModalNavigationController,DefendNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m1796797315(__this, ___screen0, method) ((  void (*) (NavigationController_2_t3412936559 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m1880414682_gshared)(__this, ___screen0, method)
