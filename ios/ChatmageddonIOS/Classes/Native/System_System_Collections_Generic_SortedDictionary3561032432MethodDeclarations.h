﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t3561032432;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>
struct SortedDictionary_2_t3346886819;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_SortedDictionary3212009085.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2360962727_gshared (KeyCollection_t3561032432 * __this, SortedDictionary_2_t3346886819 * ___dic0, const MethodInfo* method);
#define KeyCollection__ctor_m2360962727(__this, ___dic0, method) ((  void (*) (KeyCollection_t3561032432 *, SortedDictionary_2_t3346886819 *, const MethodInfo*))KeyCollection__ctor_m2360962727_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1452078018_gshared (KeyCollection_t3561032432 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1452078018(__this, ___item0, method) ((  void (*) (KeyCollection_t3561032432 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1452078018_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1877705257_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1877705257(__this, method) ((  void (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1877705257_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3023445058_gshared (KeyCollection_t3561032432 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3023445058(__this, ___item0, method) ((  bool (*) (KeyCollection_t3561032432 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3023445058_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2985680479_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2985680479(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2985680479_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m945847853_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m945847853(__this, method) ((  bool (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m945847853_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4130601921_gshared (KeyCollection_t3561032432 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4130601921(__this, ___item0, method) ((  bool (*) (KeyCollection_t3561032432 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4130601921_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1628337375_gshared (KeyCollection_t3561032432 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1628337375(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3561032432 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1628337375_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m633672767_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m633672767(__this, method) ((  bool (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m633672767_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4189355763_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4189355763(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4189355763_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2399335214_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2399335214(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2399335214_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2361880853_gshared (KeyCollection_t3561032432 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2361880853(__this, ___array0, ___arrayIndex1, method) ((  void (*) (KeyCollection_t3561032432 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2361880853_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1497005991_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1497005991(__this, method) ((  int32_t (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_get_Count_m1497005991_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3212009085  KeyCollection_GetEnumerator_m253781891_gshared (KeyCollection_t3561032432 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m253781891(__this, method) ((  Enumerator_t3212009085  (*) (KeyCollection_t3561032432 *, const MethodInfo*))KeyCollection_GetEnumerator_m253781891_gshared)(__this, method)
