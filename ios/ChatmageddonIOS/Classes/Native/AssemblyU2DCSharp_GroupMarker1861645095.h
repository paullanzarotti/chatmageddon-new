﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<OnlineMapsMarker3D>
struct List_1_t4240903967;
// TweenAlpha
struct TweenAlpha_t2421518635;
// System.Func`3<UnityEngine.Vector2,OnlineMapsMarker3D,UnityEngine.Vector2>
struct Func_3_t3217542043;

#include "AssemblyU2DCSharp_RadarMarkerBlip1130528275.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupMarker
struct  GroupMarker_t1861645095  : public RadarMarkerBlip_t1130528275
{
public:
	// System.Collections.Generic.List`1<OnlineMapsMarker3D> GroupMarker::group
	List_1_t4240903967 * ___group_10;
	// TweenAlpha GroupMarker::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_11;
	// UnityEngine.Vector2 GroupMarker::center
	Vector2_t2243707579  ___center_12;
	// UnityEngine.Vector2 GroupMarker::tilePosition
	Vector2_t2243707579  ___tilePosition_13;
	// System.Int32 GroupMarker::zoom
	int32_t ___zoom_14;

public:
	inline static int32_t get_offset_of_group_10() { return static_cast<int32_t>(offsetof(GroupMarker_t1861645095, ___group_10)); }
	inline List_1_t4240903967 * get_group_10() const { return ___group_10; }
	inline List_1_t4240903967 ** get_address_of_group_10() { return &___group_10; }
	inline void set_group_10(List_1_t4240903967 * value)
	{
		___group_10 = value;
		Il2CppCodeGenWriteBarrier(&___group_10, value);
	}

	inline static int32_t get_offset_of_alphaTween_11() { return static_cast<int32_t>(offsetof(GroupMarker_t1861645095, ___alphaTween_11)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_11() const { return ___alphaTween_11; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_11() { return &___alphaTween_11; }
	inline void set_alphaTween_11(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_11 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_11, value);
	}

	inline static int32_t get_offset_of_center_12() { return static_cast<int32_t>(offsetof(GroupMarker_t1861645095, ___center_12)); }
	inline Vector2_t2243707579  get_center_12() const { return ___center_12; }
	inline Vector2_t2243707579 * get_address_of_center_12() { return &___center_12; }
	inline void set_center_12(Vector2_t2243707579  value)
	{
		___center_12 = value;
	}

	inline static int32_t get_offset_of_tilePosition_13() { return static_cast<int32_t>(offsetof(GroupMarker_t1861645095, ___tilePosition_13)); }
	inline Vector2_t2243707579  get_tilePosition_13() const { return ___tilePosition_13; }
	inline Vector2_t2243707579 * get_address_of_tilePosition_13() { return &___tilePosition_13; }
	inline void set_tilePosition_13(Vector2_t2243707579  value)
	{
		___tilePosition_13 = value;
	}

	inline static int32_t get_offset_of_zoom_14() { return static_cast<int32_t>(offsetof(GroupMarker_t1861645095, ___zoom_14)); }
	inline int32_t get_zoom_14() const { return ___zoom_14; }
	inline int32_t* get_address_of_zoom_14() { return &___zoom_14; }
	inline void set_zoom_14(int32_t value)
	{
		___zoom_14 = value;
	}
};

struct GroupMarker_t1861645095_StaticFields
{
public:
	// System.Func`3<UnityEngine.Vector2,OnlineMapsMarker3D,UnityEngine.Vector2> GroupMarker::<>f__am$cache0
	Func_3_t3217542043 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(GroupMarker_t1861645095_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Func_3_t3217542043 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Func_3_t3217542043 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Func_3_t3217542043 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
