﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoreUIScaler
struct StoreUIScaler_t2133310967;

#include "codegen/il2cpp-codegen.h"

// System.Void StoreUIScaler::.ctor()
extern "C"  void StoreUIScaler__ctor_m1552450590 (StoreUIScaler_t2133310967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreUIScaler::GlobalUIScale()
extern "C"  void StoreUIScaler_GlobalUIScale_m3483144213 (StoreUIScaler_t2133310967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
