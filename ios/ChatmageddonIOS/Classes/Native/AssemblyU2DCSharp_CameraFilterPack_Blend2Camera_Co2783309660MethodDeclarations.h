﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_ColorBurn
struct CameraFilterPack_Blend2Camera_ColorBurn_t2783309660;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_ColorBurn::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_ColorBurn__ctor_m2696789851 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_ColorBurn::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_ColorBurn_get_material_m2889808892 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::Start()
extern "C"  void CameraFilterPack_Blend2Camera_ColorBurn_Start_m2363622583 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_ColorBurn_OnRenderImage_m2745053575 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_ColorBurn_OnValidate_m3779302736 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::Update()
extern "C"  void CameraFilterPack_Blend2Camera_ColorBurn_Update_m733534174 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_ColorBurn_OnEnable_m1116491575 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_ColorBurn_OnDisable_m2300672688 (CameraFilterPack_Blend2Camera_ColorBurn_t2783309660 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
