﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<DeleteAuthViaID>c__AnonStorey7<System.Object>
struct U3CDeleteAuthViaIDU3Ec__AnonStorey7_t2567761512;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<DeleteAuthViaID>c__AnonStorey7<System.Object>::.ctor()
extern "C"  void U3CDeleteAuthViaIDU3Ec__AnonStorey7__ctor_m4116486861_gshared (U3CDeleteAuthViaIDU3Ec__AnonStorey7_t2567761512 * __this, const MethodInfo* method);
#define U3CDeleteAuthViaIDU3Ec__AnonStorey7__ctor_m4116486861(__this, method) ((  void (*) (U3CDeleteAuthViaIDU3Ec__AnonStorey7_t2567761512 *, const MethodInfo*))U3CDeleteAuthViaIDU3Ec__AnonStorey7__ctor_m4116486861_gshared)(__this, method)
// System.Void BaseServer`1/<DeleteAuthViaID>c__AnonStorey7<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeleteAuthViaIDU3Ec__AnonStorey7_U3CU3Em__0_m2882347900_gshared (U3CDeleteAuthViaIDU3Ec__AnonStorey7_t2567761512 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CDeleteAuthViaIDU3Ec__AnonStorey7_U3CU3Em__0_m2882347900(__this, ___request0, ___response1, method) ((  void (*) (U3CDeleteAuthViaIDU3Ec__AnonStorey7_t2567761512 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CDeleteAuthViaIDU3Ec__AnonStorey7_U3CU3Em__0_m2882347900_gshared)(__this, ___request0, ___response1, method)
