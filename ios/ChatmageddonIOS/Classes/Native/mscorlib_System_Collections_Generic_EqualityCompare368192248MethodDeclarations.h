﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>
struct DefaultComparer_t368192248;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry3041229383.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void DefaultComparer__ctor_m2906365589_gshared (DefaultComparer_t368192248 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2906365589(__this, method) ((  void (*) (DefaultComparer_t368192248 *, const MethodInfo*))DefaultComparer__ctor_m2906365589_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4015091960_gshared (DefaultComparer_t368192248 * __this, FadeEntry_t3041229383  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4015091960(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t368192248 *, FadeEntry_t3041229383 , const MethodInfo*))DefaultComparer_GetHashCode_m4015091960_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<TypewriterEffect/FadeEntry>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m786358104_gshared (DefaultComparer_t368192248 * __this, FadeEntry_t3041229383  ___x0, FadeEntry_t3041229383  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m786358104(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t368192248 *, FadeEntry_t3041229383 , FadeEntry_t3041229383 , const MethodInfo*))DefaultComparer_Equals_m786358104_gshared)(__this, ___x0, ___y1, method)
