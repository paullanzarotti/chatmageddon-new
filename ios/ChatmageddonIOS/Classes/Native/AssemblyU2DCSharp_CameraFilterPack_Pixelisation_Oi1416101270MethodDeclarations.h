﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Pixelisation_OilPaintHQ
struct CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::.ctor()
extern "C"  void CameraFilterPack_Pixelisation_OilPaintHQ__ctor_m1401490185 (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixelisation_OilPaintHQ::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Pixelisation_OilPaintHQ_get_material_m255237904 (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::Start()
extern "C"  void CameraFilterPack_Pixelisation_OilPaintHQ_Start_m996756673 (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Pixelisation_OilPaintHQ_OnRenderImage_m1411047921 (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnValidate()
extern "C"  void CameraFilterPack_Pixelisation_OilPaintHQ_OnValidate_m1447209554 (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::Update()
extern "C"  void CameraFilterPack_Pixelisation_OilPaintHQ_Update_m736462552 (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnDisable()
extern "C"  void CameraFilterPack_Pixelisation_OilPaintHQ_OnDisable_m4168312742 (CameraFilterPack_Pixelisation_OilPaintHQ_t1416101270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
