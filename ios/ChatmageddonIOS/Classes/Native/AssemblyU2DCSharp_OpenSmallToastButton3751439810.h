﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenSmallToastButton
struct  OpenSmallToastButton_t3751439810  : public SFXButton_t792651341
{
public:
	// System.String OpenSmallToastButton::message
	String_t* ___message_5;

public:
	inline static int32_t get_offset_of_message_5() { return static_cast<int32_t>(offsetof(OpenSmallToastButton_t3751439810, ___message_5)); }
	inline String_t* get_message_5() const { return ___message_5; }
	inline String_t** get_address_of_message_5() { return &___message_5; }
	inline void set_message_5(String_t* value)
	{
		___message_5 = value;
		Il2CppCodeGenWriteBarrier(&___message_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
