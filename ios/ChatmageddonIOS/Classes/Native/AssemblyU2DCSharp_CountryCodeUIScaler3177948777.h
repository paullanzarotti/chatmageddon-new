﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountryCodeUIScaler
struct  CountryCodeUIScaler_t3177948777  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite CountryCodeUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.Transform CountryCodeUIScaler::navBackButton
	Transform_t3275118058 * ___navBackButton_15;
	// UISprite CountryCodeUIScaler::titleDivider
	UISprite_t603616735 * ___titleDivider_16;
	// UIPanel CountryCodeUIScaler::scrollViewPanel
	UIPanel_t1795085332 * ___scrollViewPanel_17;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(CountryCodeUIScaler_t3177948777, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_navBackButton_15() { return static_cast<int32_t>(offsetof(CountryCodeUIScaler_t3177948777, ___navBackButton_15)); }
	inline Transform_t3275118058 * get_navBackButton_15() const { return ___navBackButton_15; }
	inline Transform_t3275118058 ** get_address_of_navBackButton_15() { return &___navBackButton_15; }
	inline void set_navBackButton_15(Transform_t3275118058 * value)
	{
		___navBackButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___navBackButton_15, value);
	}

	inline static int32_t get_offset_of_titleDivider_16() { return static_cast<int32_t>(offsetof(CountryCodeUIScaler_t3177948777, ___titleDivider_16)); }
	inline UISprite_t603616735 * get_titleDivider_16() const { return ___titleDivider_16; }
	inline UISprite_t603616735 ** get_address_of_titleDivider_16() { return &___titleDivider_16; }
	inline void set_titleDivider_16(UISprite_t603616735 * value)
	{
		___titleDivider_16 = value;
		Il2CppCodeGenWriteBarrier(&___titleDivider_16, value);
	}

	inline static int32_t get_offset_of_scrollViewPanel_17() { return static_cast<int32_t>(offsetof(CountryCodeUIScaler_t3177948777, ___scrollViewPanel_17)); }
	inline UIPanel_t1795085332 * get_scrollViewPanel_17() const { return ___scrollViewPanel_17; }
	inline UIPanel_t1795085332 ** get_address_of_scrollViewPanel_17() { return &___scrollViewPanel_17; }
	inline void set_scrollViewPanel_17(UIPanel_t1795085332 * value)
	{
		___scrollViewPanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___scrollViewPanel_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
