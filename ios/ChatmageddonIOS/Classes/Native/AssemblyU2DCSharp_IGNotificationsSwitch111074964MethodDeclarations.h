﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IGNotificationsSwitch
struct IGNotificationsSwitch_t111074964;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IGNotificationsSwitch::.ctor()
extern "C"  void IGNotificationsSwitch__ctor_m626713589 (IGNotificationsSwitch_t111074964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IGNotificationsSwitch::OnSwitch(System.Boolean)
extern "C"  void IGNotificationsSwitch_OnSwitch_m2144219513 (IGNotificationsSwitch_t111074964 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IGNotificationsSwitch::<OnSwitch>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void IGNotificationsSwitch_U3COnSwitchU3Em__0_m3544963322 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
