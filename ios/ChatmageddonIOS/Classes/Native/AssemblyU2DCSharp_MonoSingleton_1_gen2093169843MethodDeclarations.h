﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ItemsManager>::.ctor()
#define MonoSingleton_1__ctor_m678170093(__this, method) ((  void (*) (MonoSingleton_1_t2093169843 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ItemsManager>::Awake()
#define MonoSingleton_1_Awake_m2548160540(__this, method) ((  void (*) (MonoSingleton_1_t2093169843 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ItemsManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2710564266(__this /* static, unused */, method) ((  ItemsManager_t2342504123 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ItemsManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2929012274(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ItemsManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1143824267(__this, method) ((  void (*) (MonoSingleton_1_t2093169843 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ItemsManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m572995343(__this, method) ((  void (*) (MonoSingleton_1_t2093169843 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ItemsManager>::.cctor()
#define MonoSingleton_1__cctor_m3757805854(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
