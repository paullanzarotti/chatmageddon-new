﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator0<System.Single>
struct U3CGetEnumeratorU3Ec__Iterator0_t2780791432;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator0<System.Single>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m2082773205_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m2082773205(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m2082773205_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator0<System.Single>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2844001035_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2844001035(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2844001035_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator0<System.Single>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  float U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2367323704_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2367323704(__this, method) ((  float (*) (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2367323704_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator0<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m955093575_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m955093575(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m955093575_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<System.Single>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2861584912_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2861584912(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2861584912_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator0<System.Single>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2646663650_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m2646663650(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2780791432 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m2646663650_gshared)(__this, method)
