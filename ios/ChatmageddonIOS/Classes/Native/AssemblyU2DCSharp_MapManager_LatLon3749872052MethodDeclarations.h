﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MapManager_LatLon3749872052.h"

// System.Void MapManager/LatLon::.ctor(System.Double,System.Double)
extern "C"  void LatLon__ctor_m4212126211 (LatLon_t3749872052 * __this, double ___latitude0, double ___longtitude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
