﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardLoaderUIScaler
struct  LeaderboardLoaderUIScaler_t1071922518  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite LeaderboardLoaderUIScaler::titleSeperator
	UISprite_t603616735 * ___titleSeperator_14;
	// UnityEngine.Transform LeaderboardLoaderUIScaler::navigateBackButton
	Transform_t3275118058 * ___navigateBackButton_15;
	// UISprite LeaderboardLoaderUIScaler::timeTabBackground
	UISprite_t603616735 * ___timeTabBackground_16;
	// UnityEngine.BoxCollider LeaderboardLoaderUIScaler::dailyTabButton
	BoxCollider_t22920061 * ___dailyTabButton_17;
	// UnityEngine.BoxCollider LeaderboardLoaderUIScaler::allTimeTabButton
	BoxCollider_t22920061 * ___allTimeTabButton_18;
	// UISprite LeaderboardLoaderUIScaler::timeTabSeperator
	UISprite_t603616735 * ___timeTabSeperator_19;
	// UISprite LeaderboardLoaderUIScaler::groupTabBackground
	UISprite_t603616735 * ___groupTabBackground_20;
	// UnityEngine.BoxCollider LeaderboardLoaderUIScaler::innerTabButton
	BoxCollider_t22920061 * ___innerTabButton_21;
	// UnityEngine.BoxCollider LeaderboardLoaderUIScaler::outerTabButton
	BoxCollider_t22920061 * ___outerTabButton_22;
	// UnityEngine.BoxCollider LeaderboardLoaderUIScaler::worldTabButton
	BoxCollider_t22920061 * ___worldTabButton_23;
	// UISprite LeaderboardLoaderUIScaler::groupTabSeperator
	UISprite_t603616735 * ___groupTabSeperator_24;
	// UnityEngine.Transform LeaderboardLoaderUIScaler::groupLeftSeperator
	Transform_t3275118058 * ___groupLeftSeperator_25;
	// UnityEngine.Transform LeaderboardLoaderUIScaler::groupRightSeperator
	Transform_t3275118058 * ___groupRightSeperator_26;

public:
	inline static int32_t get_offset_of_titleSeperator_14() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___titleSeperator_14)); }
	inline UISprite_t603616735 * get_titleSeperator_14() const { return ___titleSeperator_14; }
	inline UISprite_t603616735 ** get_address_of_titleSeperator_14() { return &___titleSeperator_14; }
	inline void set_titleSeperator_14(UISprite_t603616735 * value)
	{
		___titleSeperator_14 = value;
		Il2CppCodeGenWriteBarrier(&___titleSeperator_14, value);
	}

	inline static int32_t get_offset_of_navigateBackButton_15() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___navigateBackButton_15)); }
	inline Transform_t3275118058 * get_navigateBackButton_15() const { return ___navigateBackButton_15; }
	inline Transform_t3275118058 ** get_address_of_navigateBackButton_15() { return &___navigateBackButton_15; }
	inline void set_navigateBackButton_15(Transform_t3275118058 * value)
	{
		___navigateBackButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___navigateBackButton_15, value);
	}

	inline static int32_t get_offset_of_timeTabBackground_16() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___timeTabBackground_16)); }
	inline UISprite_t603616735 * get_timeTabBackground_16() const { return ___timeTabBackground_16; }
	inline UISprite_t603616735 ** get_address_of_timeTabBackground_16() { return &___timeTabBackground_16; }
	inline void set_timeTabBackground_16(UISprite_t603616735 * value)
	{
		___timeTabBackground_16 = value;
		Il2CppCodeGenWriteBarrier(&___timeTabBackground_16, value);
	}

	inline static int32_t get_offset_of_dailyTabButton_17() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___dailyTabButton_17)); }
	inline BoxCollider_t22920061 * get_dailyTabButton_17() const { return ___dailyTabButton_17; }
	inline BoxCollider_t22920061 ** get_address_of_dailyTabButton_17() { return &___dailyTabButton_17; }
	inline void set_dailyTabButton_17(BoxCollider_t22920061 * value)
	{
		___dailyTabButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___dailyTabButton_17, value);
	}

	inline static int32_t get_offset_of_allTimeTabButton_18() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___allTimeTabButton_18)); }
	inline BoxCollider_t22920061 * get_allTimeTabButton_18() const { return ___allTimeTabButton_18; }
	inline BoxCollider_t22920061 ** get_address_of_allTimeTabButton_18() { return &___allTimeTabButton_18; }
	inline void set_allTimeTabButton_18(BoxCollider_t22920061 * value)
	{
		___allTimeTabButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___allTimeTabButton_18, value);
	}

	inline static int32_t get_offset_of_timeTabSeperator_19() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___timeTabSeperator_19)); }
	inline UISprite_t603616735 * get_timeTabSeperator_19() const { return ___timeTabSeperator_19; }
	inline UISprite_t603616735 ** get_address_of_timeTabSeperator_19() { return &___timeTabSeperator_19; }
	inline void set_timeTabSeperator_19(UISprite_t603616735 * value)
	{
		___timeTabSeperator_19 = value;
		Il2CppCodeGenWriteBarrier(&___timeTabSeperator_19, value);
	}

	inline static int32_t get_offset_of_groupTabBackground_20() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___groupTabBackground_20)); }
	inline UISprite_t603616735 * get_groupTabBackground_20() const { return ___groupTabBackground_20; }
	inline UISprite_t603616735 ** get_address_of_groupTabBackground_20() { return &___groupTabBackground_20; }
	inline void set_groupTabBackground_20(UISprite_t603616735 * value)
	{
		___groupTabBackground_20 = value;
		Il2CppCodeGenWriteBarrier(&___groupTabBackground_20, value);
	}

	inline static int32_t get_offset_of_innerTabButton_21() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___innerTabButton_21)); }
	inline BoxCollider_t22920061 * get_innerTabButton_21() const { return ___innerTabButton_21; }
	inline BoxCollider_t22920061 ** get_address_of_innerTabButton_21() { return &___innerTabButton_21; }
	inline void set_innerTabButton_21(BoxCollider_t22920061 * value)
	{
		___innerTabButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___innerTabButton_21, value);
	}

	inline static int32_t get_offset_of_outerTabButton_22() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___outerTabButton_22)); }
	inline BoxCollider_t22920061 * get_outerTabButton_22() const { return ___outerTabButton_22; }
	inline BoxCollider_t22920061 ** get_address_of_outerTabButton_22() { return &___outerTabButton_22; }
	inline void set_outerTabButton_22(BoxCollider_t22920061 * value)
	{
		___outerTabButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___outerTabButton_22, value);
	}

	inline static int32_t get_offset_of_worldTabButton_23() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___worldTabButton_23)); }
	inline BoxCollider_t22920061 * get_worldTabButton_23() const { return ___worldTabButton_23; }
	inline BoxCollider_t22920061 ** get_address_of_worldTabButton_23() { return &___worldTabButton_23; }
	inline void set_worldTabButton_23(BoxCollider_t22920061 * value)
	{
		___worldTabButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___worldTabButton_23, value);
	}

	inline static int32_t get_offset_of_groupTabSeperator_24() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___groupTabSeperator_24)); }
	inline UISprite_t603616735 * get_groupTabSeperator_24() const { return ___groupTabSeperator_24; }
	inline UISprite_t603616735 ** get_address_of_groupTabSeperator_24() { return &___groupTabSeperator_24; }
	inline void set_groupTabSeperator_24(UISprite_t603616735 * value)
	{
		___groupTabSeperator_24 = value;
		Il2CppCodeGenWriteBarrier(&___groupTabSeperator_24, value);
	}

	inline static int32_t get_offset_of_groupLeftSeperator_25() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___groupLeftSeperator_25)); }
	inline Transform_t3275118058 * get_groupLeftSeperator_25() const { return ___groupLeftSeperator_25; }
	inline Transform_t3275118058 ** get_address_of_groupLeftSeperator_25() { return &___groupLeftSeperator_25; }
	inline void set_groupLeftSeperator_25(Transform_t3275118058 * value)
	{
		___groupLeftSeperator_25 = value;
		Il2CppCodeGenWriteBarrier(&___groupLeftSeperator_25, value);
	}

	inline static int32_t get_offset_of_groupRightSeperator_26() { return static_cast<int32_t>(offsetof(LeaderboardLoaderUIScaler_t1071922518, ___groupRightSeperator_26)); }
	inline Transform_t3275118058 * get_groupRightSeperator_26() const { return ___groupRightSeperator_26; }
	inline Transform_t3275118058 ** get_address_of_groupRightSeperator_26() { return &___groupRightSeperator_26; }
	inline void set_groupRightSeperator_26(Transform_t3275118058 * value)
	{
		___groupRightSeperator_26 = value;
		Il2CppCodeGenWriteBarrier(&___groupRightSeperator_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
