﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActivateUserKnockoutButton
struct  ActivateUserKnockoutButton_t3686915466  : public SFXButton_t792651341
{
public:
	// System.Int32 ActivateUserKnockoutButton::durationMins
	int32_t ___durationMins_5;
	// System.String ActivateUserKnockoutButton::userID
	String_t* ___userID_6;

public:
	inline static int32_t get_offset_of_durationMins_5() { return static_cast<int32_t>(offsetof(ActivateUserKnockoutButton_t3686915466, ___durationMins_5)); }
	inline int32_t get_durationMins_5() const { return ___durationMins_5; }
	inline int32_t* get_address_of_durationMins_5() { return &___durationMins_5; }
	inline void set_durationMins_5(int32_t value)
	{
		___durationMins_5 = value;
	}

	inline static int32_t get_offset_of_userID_6() { return static_cast<int32_t>(offsetof(ActivateUserKnockoutButton_t3686915466, ___userID_6)); }
	inline String_t* get_userID_6() const { return ___userID_6; }
	inline String_t** get_address_of_userID_6() { return &___userID_6; }
	inline void set_userID_6(String_t* value)
	{
		___userID_6 = value;
		Il2CppCodeGenWriteBarrier(&___userID_6, value);
	}
};

struct ActivateUserKnockoutButton_t3686915466_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ActivateUserKnockoutButton::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(ActivateUserKnockoutButton_t3686915466_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
