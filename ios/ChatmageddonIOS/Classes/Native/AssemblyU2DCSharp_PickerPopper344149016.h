﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// IPPickerBase
struct IPPickerBase_t4159478266;
// UIWidget
struct UIWidget_t1453041918;
// UIPanel
struct UIPanel_t1795085332;
// System.String
struct String_t;
// IPDragScrollView
struct IPDragScrollView_t145219383;
// IPUserInteraction
struct IPUserInteraction_t4192479194;
// IPCycler
struct IPCycler_t1336138445;
// TweenPanelClipRange
struct TweenPanelClipRange_t596408274;
// TweenPanelClipSoftness
struct TweenPanelClipSoftness_t2480894570;
// TweenHeight
struct TweenHeight_t161374028;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickerPopper
struct  PickerPopper_t344149016  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject PickerPopper::pickerColliderObject
	GameObject_t1756533147 * ___pickerColliderObject_2;
	// IPPickerBase PickerPopper::picker
	IPPickerBase_t4159478266 * ___picker_3;
	// UIWidget PickerPopper::pickerBackground
	UIWidget_t1453041918 * ___pickerBackground_4;
	// UIPanel PickerPopper::cyclerPanel
	UIPanel_t1795085332 * ___cyclerPanel_5;
	// UnityEngine.Vector2 PickerPopper::openCloseScaleTweenDuration
	Vector2_t2243707579  ___openCloseScaleTweenDuration_6;
	// UnityEngine.Vector2 PickerPopper::openCloseClippingTweenDuration
	Vector2_t2243707579  ___openCloseClippingTweenDuration_7;
	// UIWidget PickerPopper::widgetForBlinkConfirmation
	UIWidget_t1453041918 * ___widgetForBlinkConfirmation_8;
	// UnityEngine.Color PickerPopper::blinkColor
	Color_t2020392075  ___blinkColor_9;
	// System.Single PickerPopper::confirmBlinkDuration
	float ___confirmBlinkDuration_10;
	// UnityEngine.GameObject PickerPopper::notifyWhenCollapsing
	GameObject_t1756533147 * ___notifyWhenCollapsing_11;
	// System.String PickerPopper::message
	String_t* ___message_12;
	// IPDragScrollView PickerPopper::_dragContents
	IPDragScrollView_t145219383 * ____dragContents_13;
	// IPUserInteraction PickerPopper::_pickerInteraction
	IPUserInteraction_t4192479194 * ____pickerInteraction_14;
	// IPCycler PickerPopper::_cycler
	IPCycler_t1336138445 * ____cycler_15;
	// TweenPanelClipRange PickerPopper::_clipRangeTween
	TweenPanelClipRange_t596408274 * ____clipRangeTween_16;
	// TweenPanelClipSoftness PickerPopper::_softnessTween
	TweenPanelClipSoftness_t2480894570 * ____softnessTween_17;
	// TweenHeight PickerPopper::_scaleTween
	TweenHeight_t161374028 * ____scaleTween_18;
	// UnityEngine.BoxCollider PickerPopper::_pickerCollider
	BoxCollider_t22920061 * ____pickerCollider_19;
	// UnityEngine.BoxCollider PickerPopper::_thisCollider
	BoxCollider_t22920061 * ____thisCollider_20;
	// System.Boolean PickerPopper::_waitBeforeClosing
	bool ____waitBeforeClosing_21;
	// System.Boolean PickerPopper::_clippingIsTweening
	bool ____clippingIsTweening_22;
	// UnityEngine.Color PickerPopper::_initBlinkColor
	Color_t2020392075  ____initBlinkColor_23;
	// UnityEngine.Vector3 PickerPopper::_cyclerCachedPos
	Vector3_t2243707580  ____cyclerCachedPos_24;

public:
	inline static int32_t get_offset_of_pickerColliderObject_2() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___pickerColliderObject_2)); }
	inline GameObject_t1756533147 * get_pickerColliderObject_2() const { return ___pickerColliderObject_2; }
	inline GameObject_t1756533147 ** get_address_of_pickerColliderObject_2() { return &___pickerColliderObject_2; }
	inline void set_pickerColliderObject_2(GameObject_t1756533147 * value)
	{
		___pickerColliderObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___pickerColliderObject_2, value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___picker_3)); }
	inline IPPickerBase_t4159478266 * get_picker_3() const { return ___picker_3; }
	inline IPPickerBase_t4159478266 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(IPPickerBase_t4159478266 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier(&___picker_3, value);
	}

	inline static int32_t get_offset_of_pickerBackground_4() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___pickerBackground_4)); }
	inline UIWidget_t1453041918 * get_pickerBackground_4() const { return ___pickerBackground_4; }
	inline UIWidget_t1453041918 ** get_address_of_pickerBackground_4() { return &___pickerBackground_4; }
	inline void set_pickerBackground_4(UIWidget_t1453041918 * value)
	{
		___pickerBackground_4 = value;
		Il2CppCodeGenWriteBarrier(&___pickerBackground_4, value);
	}

	inline static int32_t get_offset_of_cyclerPanel_5() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___cyclerPanel_5)); }
	inline UIPanel_t1795085332 * get_cyclerPanel_5() const { return ___cyclerPanel_5; }
	inline UIPanel_t1795085332 ** get_address_of_cyclerPanel_5() { return &___cyclerPanel_5; }
	inline void set_cyclerPanel_5(UIPanel_t1795085332 * value)
	{
		___cyclerPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___cyclerPanel_5, value);
	}

	inline static int32_t get_offset_of_openCloseScaleTweenDuration_6() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___openCloseScaleTweenDuration_6)); }
	inline Vector2_t2243707579  get_openCloseScaleTweenDuration_6() const { return ___openCloseScaleTweenDuration_6; }
	inline Vector2_t2243707579 * get_address_of_openCloseScaleTweenDuration_6() { return &___openCloseScaleTweenDuration_6; }
	inline void set_openCloseScaleTweenDuration_6(Vector2_t2243707579  value)
	{
		___openCloseScaleTweenDuration_6 = value;
	}

	inline static int32_t get_offset_of_openCloseClippingTweenDuration_7() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___openCloseClippingTweenDuration_7)); }
	inline Vector2_t2243707579  get_openCloseClippingTweenDuration_7() const { return ___openCloseClippingTweenDuration_7; }
	inline Vector2_t2243707579 * get_address_of_openCloseClippingTweenDuration_7() { return &___openCloseClippingTweenDuration_7; }
	inline void set_openCloseClippingTweenDuration_7(Vector2_t2243707579  value)
	{
		___openCloseClippingTweenDuration_7 = value;
	}

	inline static int32_t get_offset_of_widgetForBlinkConfirmation_8() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___widgetForBlinkConfirmation_8)); }
	inline UIWidget_t1453041918 * get_widgetForBlinkConfirmation_8() const { return ___widgetForBlinkConfirmation_8; }
	inline UIWidget_t1453041918 ** get_address_of_widgetForBlinkConfirmation_8() { return &___widgetForBlinkConfirmation_8; }
	inline void set_widgetForBlinkConfirmation_8(UIWidget_t1453041918 * value)
	{
		___widgetForBlinkConfirmation_8 = value;
		Il2CppCodeGenWriteBarrier(&___widgetForBlinkConfirmation_8, value);
	}

	inline static int32_t get_offset_of_blinkColor_9() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___blinkColor_9)); }
	inline Color_t2020392075  get_blinkColor_9() const { return ___blinkColor_9; }
	inline Color_t2020392075 * get_address_of_blinkColor_9() { return &___blinkColor_9; }
	inline void set_blinkColor_9(Color_t2020392075  value)
	{
		___blinkColor_9 = value;
	}

	inline static int32_t get_offset_of_confirmBlinkDuration_10() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___confirmBlinkDuration_10)); }
	inline float get_confirmBlinkDuration_10() const { return ___confirmBlinkDuration_10; }
	inline float* get_address_of_confirmBlinkDuration_10() { return &___confirmBlinkDuration_10; }
	inline void set_confirmBlinkDuration_10(float value)
	{
		___confirmBlinkDuration_10 = value;
	}

	inline static int32_t get_offset_of_notifyWhenCollapsing_11() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___notifyWhenCollapsing_11)); }
	inline GameObject_t1756533147 * get_notifyWhenCollapsing_11() const { return ___notifyWhenCollapsing_11; }
	inline GameObject_t1756533147 ** get_address_of_notifyWhenCollapsing_11() { return &___notifyWhenCollapsing_11; }
	inline void set_notifyWhenCollapsing_11(GameObject_t1756533147 * value)
	{
		___notifyWhenCollapsing_11 = value;
		Il2CppCodeGenWriteBarrier(&___notifyWhenCollapsing_11, value);
	}

	inline static int32_t get_offset_of_message_12() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ___message_12)); }
	inline String_t* get_message_12() const { return ___message_12; }
	inline String_t** get_address_of_message_12() { return &___message_12; }
	inline void set_message_12(String_t* value)
	{
		___message_12 = value;
		Il2CppCodeGenWriteBarrier(&___message_12, value);
	}

	inline static int32_t get_offset_of__dragContents_13() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____dragContents_13)); }
	inline IPDragScrollView_t145219383 * get__dragContents_13() const { return ____dragContents_13; }
	inline IPDragScrollView_t145219383 ** get_address_of__dragContents_13() { return &____dragContents_13; }
	inline void set__dragContents_13(IPDragScrollView_t145219383 * value)
	{
		____dragContents_13 = value;
		Il2CppCodeGenWriteBarrier(&____dragContents_13, value);
	}

	inline static int32_t get_offset_of__pickerInteraction_14() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____pickerInteraction_14)); }
	inline IPUserInteraction_t4192479194 * get__pickerInteraction_14() const { return ____pickerInteraction_14; }
	inline IPUserInteraction_t4192479194 ** get_address_of__pickerInteraction_14() { return &____pickerInteraction_14; }
	inline void set__pickerInteraction_14(IPUserInteraction_t4192479194 * value)
	{
		____pickerInteraction_14 = value;
		Il2CppCodeGenWriteBarrier(&____pickerInteraction_14, value);
	}

	inline static int32_t get_offset_of__cycler_15() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____cycler_15)); }
	inline IPCycler_t1336138445 * get__cycler_15() const { return ____cycler_15; }
	inline IPCycler_t1336138445 ** get_address_of__cycler_15() { return &____cycler_15; }
	inline void set__cycler_15(IPCycler_t1336138445 * value)
	{
		____cycler_15 = value;
		Il2CppCodeGenWriteBarrier(&____cycler_15, value);
	}

	inline static int32_t get_offset_of__clipRangeTween_16() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____clipRangeTween_16)); }
	inline TweenPanelClipRange_t596408274 * get__clipRangeTween_16() const { return ____clipRangeTween_16; }
	inline TweenPanelClipRange_t596408274 ** get_address_of__clipRangeTween_16() { return &____clipRangeTween_16; }
	inline void set__clipRangeTween_16(TweenPanelClipRange_t596408274 * value)
	{
		____clipRangeTween_16 = value;
		Il2CppCodeGenWriteBarrier(&____clipRangeTween_16, value);
	}

	inline static int32_t get_offset_of__softnessTween_17() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____softnessTween_17)); }
	inline TweenPanelClipSoftness_t2480894570 * get__softnessTween_17() const { return ____softnessTween_17; }
	inline TweenPanelClipSoftness_t2480894570 ** get_address_of__softnessTween_17() { return &____softnessTween_17; }
	inline void set__softnessTween_17(TweenPanelClipSoftness_t2480894570 * value)
	{
		____softnessTween_17 = value;
		Il2CppCodeGenWriteBarrier(&____softnessTween_17, value);
	}

	inline static int32_t get_offset_of__scaleTween_18() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____scaleTween_18)); }
	inline TweenHeight_t161374028 * get__scaleTween_18() const { return ____scaleTween_18; }
	inline TweenHeight_t161374028 ** get_address_of__scaleTween_18() { return &____scaleTween_18; }
	inline void set__scaleTween_18(TweenHeight_t161374028 * value)
	{
		____scaleTween_18 = value;
		Il2CppCodeGenWriteBarrier(&____scaleTween_18, value);
	}

	inline static int32_t get_offset_of__pickerCollider_19() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____pickerCollider_19)); }
	inline BoxCollider_t22920061 * get__pickerCollider_19() const { return ____pickerCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of__pickerCollider_19() { return &____pickerCollider_19; }
	inline void set__pickerCollider_19(BoxCollider_t22920061 * value)
	{
		____pickerCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&____pickerCollider_19, value);
	}

	inline static int32_t get_offset_of__thisCollider_20() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____thisCollider_20)); }
	inline BoxCollider_t22920061 * get__thisCollider_20() const { return ____thisCollider_20; }
	inline BoxCollider_t22920061 ** get_address_of__thisCollider_20() { return &____thisCollider_20; }
	inline void set__thisCollider_20(BoxCollider_t22920061 * value)
	{
		____thisCollider_20 = value;
		Il2CppCodeGenWriteBarrier(&____thisCollider_20, value);
	}

	inline static int32_t get_offset_of__waitBeforeClosing_21() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____waitBeforeClosing_21)); }
	inline bool get__waitBeforeClosing_21() const { return ____waitBeforeClosing_21; }
	inline bool* get_address_of__waitBeforeClosing_21() { return &____waitBeforeClosing_21; }
	inline void set__waitBeforeClosing_21(bool value)
	{
		____waitBeforeClosing_21 = value;
	}

	inline static int32_t get_offset_of__clippingIsTweening_22() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____clippingIsTweening_22)); }
	inline bool get__clippingIsTweening_22() const { return ____clippingIsTweening_22; }
	inline bool* get_address_of__clippingIsTweening_22() { return &____clippingIsTweening_22; }
	inline void set__clippingIsTweening_22(bool value)
	{
		____clippingIsTweening_22 = value;
	}

	inline static int32_t get_offset_of__initBlinkColor_23() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____initBlinkColor_23)); }
	inline Color_t2020392075  get__initBlinkColor_23() const { return ____initBlinkColor_23; }
	inline Color_t2020392075 * get_address_of__initBlinkColor_23() { return &____initBlinkColor_23; }
	inline void set__initBlinkColor_23(Color_t2020392075  value)
	{
		____initBlinkColor_23 = value;
	}

	inline static int32_t get_offset_of__cyclerCachedPos_24() { return static_cast<int32_t>(offsetof(PickerPopper_t344149016, ____cyclerCachedPos_24)); }
	inline Vector3_t2243707580  get__cyclerCachedPos_24() const { return ____cyclerCachedPos_24; }
	inline Vector3_t2243707580 * get_address_of__cyclerCachedPos_24() { return &____cyclerCachedPos_24; }
	inline void set__cyclerCachedPos_24(Vector3_t2243707580  value)
	{
		____cyclerCachedPos_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
