﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>
struct U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>::.ctor()
extern "C"  void U3CSetPanelPosOverTimeU3Ec__Iterator1__ctor_m1420071547_gshared (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 * __this, const MethodInfo* method);
#define U3CSetPanelPosOverTimeU3Ec__Iterator1__ctor_m1420071547(__this, method) ((  void (*) (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 *, const MethodInfo*))U3CSetPanelPosOverTimeU3Ec__Iterator1__ctor_m1420071547_gshared)(__this, method)
// System.Boolean BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>::MoveNext()
extern "C"  bool U3CSetPanelPosOverTimeU3Ec__Iterator1_MoveNext_m3017823313_gshared (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 * __this, const MethodInfo* method);
#define U3CSetPanelPosOverTimeU3Ec__Iterator1_MoveNext_m3017823313(__this, method) ((  bool (*) (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 *, const MethodInfo*))U3CSetPanelPosOverTimeU3Ec__Iterator1_MoveNext_m3017823313_gshared)(__this, method)
// System.Object BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetPanelPosOverTimeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3747963701_gshared (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 * __this, const MethodInfo* method);
#define U3CSetPanelPosOverTimeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3747963701(__this, method) ((  Il2CppObject * (*) (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 *, const MethodInfo*))U3CSetPanelPosOverTimeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3747963701_gshared)(__this, method)
// System.Object BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetPanelPosOverTimeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2570678701_gshared (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 * __this, const MethodInfo* method);
#define U3CSetPanelPosOverTimeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2570678701(__this, method) ((  Il2CppObject * (*) (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 *, const MethodInfo*))U3CSetPanelPosOverTimeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2570678701_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>::Dispose()
extern "C"  void U3CSetPanelPosOverTimeU3Ec__Iterator1_Dispose_m3410692188_gshared (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 * __this, const MethodInfo* method);
#define U3CSetPanelPosOverTimeU3Ec__Iterator1_Dispose_m3410692188(__this, method) ((  void (*) (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 *, const MethodInfo*))U3CSetPanelPosOverTimeU3Ec__Iterator1_Dispose_m3410692188_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>::Reset()
extern "C"  void U3CSetPanelPosOverTimeU3Ec__Iterator1_Reset_m343125986_gshared (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 * __this, const MethodInfo* method);
#define U3CSetPanelPosOverTimeU3Ec__Iterator1_Reset_m343125986(__this, method) ((  void (*) (U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206 *, const MethodInfo*))U3CSetPanelPosOverTimeU3Ec__Iterator1_Reset_m343125986_gshared)(__this, method)
