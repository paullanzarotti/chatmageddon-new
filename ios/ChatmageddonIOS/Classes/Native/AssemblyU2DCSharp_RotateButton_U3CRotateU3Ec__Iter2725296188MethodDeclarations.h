﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotateButton/<Rotate>c__Iterator0
struct U3CRotateU3Ec__Iterator0_t2725296188;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RotateButton/<Rotate>c__Iterator0::.ctor()
extern "C"  void U3CRotateU3Ec__Iterator0__ctor_m95039501 (U3CRotateU3Ec__Iterator0_t2725296188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RotateButton/<Rotate>c__Iterator0::MoveNext()
extern "C"  bool U3CRotateU3Ec__Iterator0_MoveNext_m1539732363 (U3CRotateU3Ec__Iterator0_t2725296188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RotateButton/<Rotate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRotateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3636226743 (U3CRotateU3Ec__Iterator0_t2725296188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RotateButton/<Rotate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRotateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m906814415 (U3CRotateU3Ec__Iterator0_t2725296188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateButton/<Rotate>c__Iterator0::Dispose()
extern "C"  void U3CRotateU3Ec__Iterator0_Dispose_m3170729830 (U3CRotateU3Ec__Iterator0_t2725296188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotateButton/<Rotate>c__Iterator0::Reset()
extern "C"  void U3CRotateU3Ec__Iterator0_Reset_m112850024 (U3CRotateU3Ec__Iterator0_t2725296188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
