﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1881137068.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<Gender>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1929226934_gshared (Nullable_1_t1881137068 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1929226934(__this, ___value0, method) ((  void (*) (Nullable_1_t1881137068 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1929226934_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Gender>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m730290061_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m730290061(__this, method) ((  bool (*) (Nullable_1_t1881137068 *, const MethodInfo*))Nullable_1_get_HasValue_m730290061_gshared)(__this, method)
// T System.Nullable`1<Gender>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3168451933_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3168451933(__this, method) ((  int32_t (*) (Nullable_1_t1881137068 *, const MethodInfo*))Nullable_1_get_Value_m3168451933_gshared)(__this, method)
// System.Boolean System.Nullable`1<Gender>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m958922863_gshared (Nullable_1_t1881137068 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m958922863(__this, ___other0, method) ((  bool (*) (Nullable_1_t1881137068 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m958922863_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Gender>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3627789222_gshared (Nullable_1_t1881137068 * __this, Nullable_1_t1881137068  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3627789222(__this, ___other0, method) ((  bool (*) (Nullable_1_t1881137068 *, Nullable_1_t1881137068 , const MethodInfo*))Nullable_1_Equals_m3627789222_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Gender>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3942747889_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3942747889(__this, method) ((  int32_t (*) (Nullable_1_t1881137068 *, const MethodInfo*))Nullable_1_GetHashCode_m3942747889_gshared)(__this, method)
// T System.Nullable`1<Gender>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2573044256_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2573044256(__this, method) ((  int32_t (*) (Nullable_1_t1881137068 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2573044256_gshared)(__this, method)
// System.String System.Nullable`1<Gender>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2830019457_gshared (Nullable_1_t1881137068 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2830019457(__this, method) ((  String_t* (*) (Nullable_1_t1881137068 *, const MethodInfo*))Nullable_1_ToString_m2830019457_gshared)(__this, method)
