﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1947102980.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3849078273.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_R810080994.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3772873134.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_R832805900.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_R359658953.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2948722429.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1028676545.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2541139849.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3896743154.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1009038245.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2268131636.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1288494330.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3286458088.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2865463280.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3958176278.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1315869442.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato4043888430.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3695847384.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato1632016396.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato2293935318.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3004251550.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato1267912930.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3060446089.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3799708011.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Generato3263126028.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_CMa4184605658.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Cbc1613178881.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Mac3850993954.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_CfbB471961930.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Gos1934182742.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_HMa2564819335.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_ISO9911519921.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Pol3186287769.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Sip1664512494.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Macs_Vmpc976793133.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Cbc424533760.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Cc2534265899.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Cf1093564787.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ct3347056426.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ea1200038734.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ea3159821658.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc1966957103.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_GOf591890526.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Ocb427870536.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Of2804280439.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Op1145356922.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Si3693949395.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc2204846149.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc1216265862.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Modes_Gc1725665332.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings3010076608.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings_609688802.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings4046604239.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings2353579457.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings3233791910.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings2660499907.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paddings1951118083.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2995549197.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1952252333.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter508500971.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter431035336.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3120746414.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1544976430.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2841123689.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter597696274.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4026919406.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4209685231.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2298980877.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2550649858.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1082923572.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3405498240.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2732896323.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3939864474.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3475787761.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1064568751.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3632960452.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter572706344.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter303785198.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter352675504.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1215309569.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2762984431.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3740889069.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1164823134.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter602285121.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2071462699.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter521829201.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2048269886.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter964742789.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4148998567.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter206966936.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete1337840219.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter215653120.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2816354209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete4146726485.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2760900877.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4000 = { sizeof (RC564Engine_t1947102980), -1, sizeof(RC564Engine_t1947102980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4000[7] = 
{
	RC564Engine_t1947102980_StaticFields::get_offset_of_wordSize_0(),
	RC564Engine_t1947102980_StaticFields::get_offset_of_bytesPerWord_1(),
	RC564Engine_t1947102980::get_offset_of__noRounds_2(),
	RC564Engine_t1947102980::get_offset_of__S_3(),
	RC564Engine_t1947102980_StaticFields::get_offset_of_P64_4(),
	RC564Engine_t1947102980_StaticFields::get_offset_of_Q64_5(),
	RC564Engine_t1947102980::get_offset_of_forEncryption_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4001 = { sizeof (RC6Engine_t3849078273), -1, sizeof(RC6Engine_t3849078273_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4001[8] = 
{
	RC6Engine_t3849078273_StaticFields::get_offset_of_wordSize_0(),
	RC6Engine_t3849078273_StaticFields::get_offset_of_bytesPerWord_1(),
	RC6Engine_t3849078273_StaticFields::get_offset_of__noRounds_2(),
	RC6Engine_t3849078273::get_offset_of__S_3(),
	RC6Engine_t3849078273_StaticFields::get_offset_of_P32_4(),
	RC6Engine_t3849078273_StaticFields::get_offset_of_Q32_5(),
	RC6Engine_t3849078273_StaticFields::get_offset_of_LGW_6(),
	RC6Engine_t3849078273::get_offset_of_forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4002 = { sizeof (Rfc3211WrapEngine_t810080994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4002[4] = 
{
	Rfc3211WrapEngine_t810080994::get_offset_of_engine_0(),
	Rfc3211WrapEngine_t810080994::get_offset_of_param_1(),
	Rfc3211WrapEngine_t810080994::get_offset_of_forWrapping_2(),
	Rfc3211WrapEngine_t810080994::get_offset_of_rand_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4003 = { sizeof (Rfc3394WrapEngine_t3772873134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4003[4] = 
{
	Rfc3394WrapEngine_t3772873134::get_offset_of_engine_0(),
	Rfc3394WrapEngine_t3772873134::get_offset_of_param_1(),
	Rfc3394WrapEngine_t3772873134::get_offset_of_forWrapping_2(),
	Rfc3394WrapEngine_t3772873134::get_offset_of_iv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4004 = { sizeof (RsaBlindedEngine_t832805900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4004[3] = 
{
	RsaBlindedEngine_t832805900::get_offset_of_core_0(),
	RsaBlindedEngine_t832805900::get_offset_of_key_1(),
	RsaBlindedEngine_t832805900::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4005 = { sizeof (RsaCoreEngine_t359658953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4005[3] = 
{
	RsaCoreEngine_t359658953::get_offset_of_key_0(),
	RsaCoreEngine_t359658953::get_offset_of_forEncryption_1(),
	RsaCoreEngine_t359658953::get_offset_of_bitSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4006 = { sizeof (RijndaelEngine_t2948722429), -1, sizeof(RijndaelEngine_t2948722429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4006[21] = 
{
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_MAXROUNDS_0(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_MAXKC_1(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_Logtable_2(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_Alogtable_3(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_S_4(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_Si_5(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_rcon_6(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_shifts0_7(),
	RijndaelEngine_t2948722429_StaticFields::get_offset_of_shifts1_8(),
	RijndaelEngine_t2948722429::get_offset_of_BC_9(),
	RijndaelEngine_t2948722429::get_offset_of_BC_MASK_10(),
	RijndaelEngine_t2948722429::get_offset_of_ROUNDS_11(),
	RijndaelEngine_t2948722429::get_offset_of_blockBits_12(),
	RijndaelEngine_t2948722429::get_offset_of_workingKey_13(),
	RijndaelEngine_t2948722429::get_offset_of_A0_14(),
	RijndaelEngine_t2948722429::get_offset_of_A1_15(),
	RijndaelEngine_t2948722429::get_offset_of_A2_16(),
	RijndaelEngine_t2948722429::get_offset_of_A3_17(),
	RijndaelEngine_t2948722429::get_offset_of_forEncryption_18(),
	RijndaelEngine_t2948722429::get_offset_of_shifts0SC_19(),
	RijndaelEngine_t2948722429::get_offset_of_shifts1SC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4007 = { sizeof (SeedEngine_t1028676545), -1, sizeof(SeedEngine_t1028676545_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4007[8] = 
{
	0,
	SeedEngine_t1028676545_StaticFields::get_offset_of_SS0_1(),
	SeedEngine_t1028676545_StaticFields::get_offset_of_SS1_2(),
	SeedEngine_t1028676545_StaticFields::get_offset_of_SS2_3(),
	SeedEngine_t1028676545_StaticFields::get_offset_of_SS3_4(),
	SeedEngine_t1028676545_StaticFields::get_offset_of_KC_5(),
	SeedEngine_t1028676545::get_offset_of_wKey_6(),
	SeedEngine_t1028676545::get_offset_of_forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4008 = { sizeof (SeedWrapEngine_t2541139849), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4009 = { sizeof (Salsa20Engine_t3896743154), -1, sizeof(Salsa20Engine_t3896743154_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4009[13] = 
{
	Salsa20Engine_t3896743154_StaticFields::get_offset_of_DEFAULT_ROUNDS_0(),
	0,
	Salsa20Engine_t3896743154_StaticFields::get_offset_of_sigma_2(),
	Salsa20Engine_t3896743154_StaticFields::get_offset_of_tau_3(),
	Salsa20Engine_t3896743154::get_offset_of_rounds_4(),
	Salsa20Engine_t3896743154::get_offset_of_index_5(),
	Salsa20Engine_t3896743154::get_offset_of_engineState_6(),
	Salsa20Engine_t3896743154::get_offset_of_x_7(),
	Salsa20Engine_t3896743154::get_offset_of_keyStream_8(),
	Salsa20Engine_t3896743154::get_offset_of_initialised_9(),
	Salsa20Engine_t3896743154::get_offset_of_cW0_10(),
	Salsa20Engine_t3896743154::get_offset_of_cW1_11(),
	Salsa20Engine_t3896743154::get_offset_of_cW2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4010 = { sizeof (SerpentEngine_t1009038245), -1, sizeof(SerpentEngine_t1009038245_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4010[9] = 
{
	0,
	SerpentEngine_t1009038245_StaticFields::get_offset_of_ROUNDS_1(),
	SerpentEngine_t1009038245_StaticFields::get_offset_of_PHI_2(),
	SerpentEngine_t1009038245::get_offset_of_encrypting_3(),
	SerpentEngine_t1009038245::get_offset_of_wKey_4(),
	SerpentEngine_t1009038245::get_offset_of_X0_5(),
	SerpentEngine_t1009038245::get_offset_of_X1_6(),
	SerpentEngine_t1009038245::get_offset_of_X2_7(),
	SerpentEngine_t1009038245::get_offset_of_X3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4011 = { sizeof (SkipjackEngine_t2268131636), -1, sizeof(SkipjackEngine_t2268131636_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4011[7] = 
{
	0,
	SkipjackEngine_t2268131636_StaticFields::get_offset_of_ftable_1(),
	SkipjackEngine_t2268131636::get_offset_of_key0_2(),
	SkipjackEngine_t2268131636::get_offset_of_key1_3(),
	SkipjackEngine_t2268131636::get_offset_of_key2_4(),
	SkipjackEngine_t2268131636::get_offset_of_key3_5(),
	SkipjackEngine_t2268131636::get_offset_of_encrypting_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4012 = { sizeof (TeaEngine_t1288494330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4012[10] = 
{
	0,
	0,
	0,
	0,
	TeaEngine_t1288494330::get_offset_of__a_4(),
	TeaEngine_t1288494330::get_offset_of__b_5(),
	TeaEngine_t1288494330::get_offset_of__c_6(),
	TeaEngine_t1288494330::get_offset_of__d_7(),
	TeaEngine_t1288494330::get_offset_of__initialised_8(),
	TeaEngine_t1288494330::get_offset_of__forEncryption_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4013 = { sizeof (TwofishEngine_t3286458088), -1, sizeof(TwofishEngine_t3286458088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4013[45] = 
{
	TwofishEngine_t3286458088_StaticFields::get_offset_of_P_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TwofishEngine_t3286458088::get_offset_of_encrypting_36(),
	TwofishEngine_t3286458088::get_offset_of_gMDS0_37(),
	TwofishEngine_t3286458088::get_offset_of_gMDS1_38(),
	TwofishEngine_t3286458088::get_offset_of_gMDS2_39(),
	TwofishEngine_t3286458088::get_offset_of_gMDS3_40(),
	TwofishEngine_t3286458088::get_offset_of_gSubKeys_41(),
	TwofishEngine_t3286458088::get_offset_of_gSBox_42(),
	TwofishEngine_t3286458088::get_offset_of_k64Cnt_43(),
	TwofishEngine_t3286458088::get_offset_of_workingKey_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4014 = { sizeof (VmpcEngine_t2865463280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4014[5] = 
{
	VmpcEngine_t2865463280::get_offset_of_n_0(),
	VmpcEngine_t2865463280::get_offset_of_P_1(),
	VmpcEngine_t2865463280::get_offset_of_s_2(),
	VmpcEngine_t2865463280::get_offset_of_workingIV_3(),
	VmpcEngine_t2865463280::get_offset_of_workingKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4015 = { sizeof (VmpcKsa3Engine_t3958176278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4016 = { sizeof (XteaEngine_t1315869442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4016[8] = 
{
	0,
	0,
	0,
	XteaEngine_t1315869442::get_offset_of__S_3(),
	XteaEngine_t1315869442::get_offset_of__sum0_4(),
	XteaEngine_t1315869442::get_offset_of__sum1_5(),
	XteaEngine_t1315869442::get_offset_of__initialised_6(),
	XteaEngine_t1315869442::get_offset_of__forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4017 = { sizeof (DHBasicKeyPairGenerator_t4043888430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4017[1] = 
{
	DHBasicKeyPairGenerator_t4043888430::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4018 = { sizeof (DHKeyGeneratorHelper_t3695847384), -1, sizeof(DHKeyGeneratorHelper_t3695847384_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4018[1] = 
{
	DHKeyGeneratorHelper_t3695847384_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4019 = { sizeof (DHKeyPairGenerator_t1632016396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4019[1] = 
{
	DHKeyPairGenerator_t1632016396::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4020 = { sizeof (DHParametersHelper_t2293935318), -1, sizeof(DHParametersHelper_t2293935318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4020[4] = 
{
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_Six_0(),
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_primeLists_1(),
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_primeProducts_2(),
	DHParametersHelper_t2293935318_StaticFields::get_offset_of_BigPrimeProducts_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4021 = { sizeof (DsaKeyPairGenerator_t3004251550), -1, sizeof(DsaKeyPairGenerator_t3004251550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4021[2] = 
{
	DsaKeyPairGenerator_t3004251550_StaticFields::get_offset_of_One_0(),
	DsaKeyPairGenerator_t3004251550::get_offset_of_param_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4022 = { sizeof (ECKeyPairGenerator_t1267912930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4022[4] = 
{
	ECKeyPairGenerator_t1267912930::get_offset_of_algorithm_0(),
	ECKeyPairGenerator_t1267912930::get_offset_of_parameters_1(),
	ECKeyPairGenerator_t1267912930::get_offset_of_publicKeyParamSet_2(),
	ECKeyPairGenerator_t1267912930::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4023 = { sizeof (ElGamalKeyPairGenerator_t3060446089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4023[1] = 
{
	ElGamalKeyPairGenerator_t3060446089::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4024 = { sizeof (Poly1305KeyGenerator_t3799708011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4024[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4025 = { sizeof (RsaKeyPairGenerator_t3263126028), -1, sizeof(RsaKeyPairGenerator_t3263126028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4025[7] = 
{
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_SPECIAL_E_VALUES_0(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_SPECIAL_E_HIGHEST_1(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_SPECIAL_E_BITS_2(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_One_3(),
	RsaKeyPairGenerator_t3263126028_StaticFields::get_offset_of_DefaultPublicExponent_4(),
	0,
	RsaKeyPairGenerator_t3263126028::get_offset_of_parameters_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4026 = { sizeof (CMac_t4184605658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4026[11] = 
{
	0,
	0,
	CMac_t4184605658::get_offset_of_ZEROES_2(),
	CMac_t4184605658::get_offset_of_mac_3(),
	CMac_t4184605658::get_offset_of_buf_4(),
	CMac_t4184605658::get_offset_of_bufOff_5(),
	CMac_t4184605658::get_offset_of_cipher_6(),
	CMac_t4184605658::get_offset_of_macSize_7(),
	CMac_t4184605658::get_offset_of_L_8(),
	CMac_t4184605658::get_offset_of_Lu_9(),
	CMac_t4184605658::get_offset_of_Lu2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4027 = { sizeof (CbcBlockCipherMac_t1613178881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4027[5] = 
{
	CbcBlockCipherMac_t1613178881::get_offset_of_buf_0(),
	CbcBlockCipherMac_t1613178881::get_offset_of_bufOff_1(),
	CbcBlockCipherMac_t1613178881::get_offset_of_cipher_2(),
	CbcBlockCipherMac_t1613178881::get_offset_of_padding_3(),
	CbcBlockCipherMac_t1613178881::get_offset_of_macSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4028 = { sizeof (MacCFBBlockCipher_t3850993954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4028[5] = 
{
	MacCFBBlockCipher_t3850993954::get_offset_of_IV_0(),
	MacCFBBlockCipher_t3850993954::get_offset_of_cfbV_1(),
	MacCFBBlockCipher_t3850993954::get_offset_of_cfbOutV_2(),
	MacCFBBlockCipher_t3850993954::get_offset_of_blockSize_3(),
	MacCFBBlockCipher_t3850993954::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4029 = { sizeof (CfbBlockCipherMac_t471961930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4029[6] = 
{
	CfbBlockCipherMac_t471961930::get_offset_of_mac_0(),
	CfbBlockCipherMac_t471961930::get_offset_of_Buffer_1(),
	CfbBlockCipherMac_t471961930::get_offset_of_bufOff_2(),
	CfbBlockCipherMac_t471961930::get_offset_of_cipher_3(),
	CfbBlockCipherMac_t471961930::get_offset_of_padding_4(),
	CfbBlockCipherMac_t471961930::get_offset_of_macSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4030 = { sizeof (Gost28147Mac_t1934182742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4030[8] = 
{
	0,
	0,
	Gost28147Mac_t1934182742::get_offset_of_bufOff_2(),
	Gost28147Mac_t1934182742::get_offset_of_buf_3(),
	Gost28147Mac_t1934182742::get_offset_of_mac_4(),
	Gost28147Mac_t1934182742::get_offset_of_firstStep_5(),
	Gost28147Mac_t1934182742::get_offset_of_workingKey_6(),
	Gost28147Mac_t1934182742::get_offset_of_S_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4031 = { sizeof (HMac_t2564819335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4031[9] = 
{
	0,
	0,
	HMac_t2564819335::get_offset_of_digest_2(),
	HMac_t2564819335::get_offset_of_digestSize_3(),
	HMac_t2564819335::get_offset_of_blockLength_4(),
	HMac_t2564819335::get_offset_of_ipadState_5(),
	HMac_t2564819335::get_offset_of_opadState_6(),
	HMac_t2564819335::get_offset_of_inputPad_7(),
	HMac_t2564819335::get_offset_of_outputBuf_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4032 = { sizeof (ISO9797Alg3Mac_t911519921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4032[8] = 
{
	ISO9797Alg3Mac_t911519921::get_offset_of_mac_0(),
	ISO9797Alg3Mac_t911519921::get_offset_of_buf_1(),
	ISO9797Alg3Mac_t911519921::get_offset_of_bufOff_2(),
	ISO9797Alg3Mac_t911519921::get_offset_of_cipher_3(),
	ISO9797Alg3Mac_t911519921::get_offset_of_padding_4(),
	ISO9797Alg3Mac_t911519921::get_offset_of_macSize_5(),
	ISO9797Alg3Mac_t911519921::get_offset_of_lastKey2_6(),
	ISO9797Alg3Mac_t911519921::get_offset_of_lastKey3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4033 = { sizeof (Poly1305_t3186287769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4033[23] = 
{
	0,
	Poly1305_t3186287769::get_offset_of_cipher_1(),
	Poly1305_t3186287769::get_offset_of_singleByte_2(),
	Poly1305_t3186287769::get_offset_of_r0_3(),
	Poly1305_t3186287769::get_offset_of_r1_4(),
	Poly1305_t3186287769::get_offset_of_r2_5(),
	Poly1305_t3186287769::get_offset_of_r3_6(),
	Poly1305_t3186287769::get_offset_of_r4_7(),
	Poly1305_t3186287769::get_offset_of_s1_8(),
	Poly1305_t3186287769::get_offset_of_s2_9(),
	Poly1305_t3186287769::get_offset_of_s3_10(),
	Poly1305_t3186287769::get_offset_of_s4_11(),
	Poly1305_t3186287769::get_offset_of_k0_12(),
	Poly1305_t3186287769::get_offset_of_k1_13(),
	Poly1305_t3186287769::get_offset_of_k2_14(),
	Poly1305_t3186287769::get_offset_of_k3_15(),
	Poly1305_t3186287769::get_offset_of_currentBlock_16(),
	Poly1305_t3186287769::get_offset_of_currentBlockOffset_17(),
	Poly1305_t3186287769::get_offset_of_h0_18(),
	Poly1305_t3186287769::get_offset_of_h1_19(),
	Poly1305_t3186287769::get_offset_of_h2_20(),
	Poly1305_t3186287769::get_offset_of_h3_21(),
	Poly1305_t3186287769::get_offset_of_h4_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4034 = { sizeof (SipHash_t1664512494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4034[11] = 
{
	SipHash_t1664512494::get_offset_of_c_0(),
	SipHash_t1664512494::get_offset_of_d_1(),
	SipHash_t1664512494::get_offset_of_k0_2(),
	SipHash_t1664512494::get_offset_of_k1_3(),
	SipHash_t1664512494::get_offset_of_v0_4(),
	SipHash_t1664512494::get_offset_of_v1_5(),
	SipHash_t1664512494::get_offset_of_v2_6(),
	SipHash_t1664512494::get_offset_of_v3_7(),
	SipHash_t1664512494::get_offset_of_m_8(),
	SipHash_t1664512494::get_offset_of_wordPos_9(),
	SipHash_t1664512494::get_offset_of_wordCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4035 = { sizeof (VmpcMac_t976793133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4035[11] = 
{
	VmpcMac_t976793133::get_offset_of_g_0(),
	VmpcMac_t976793133::get_offset_of_n_1(),
	VmpcMac_t976793133::get_offset_of_P_2(),
	VmpcMac_t976793133::get_offset_of_s_3(),
	VmpcMac_t976793133::get_offset_of_T_4(),
	VmpcMac_t976793133::get_offset_of_workingIV_5(),
	VmpcMac_t976793133::get_offset_of_workingKey_6(),
	VmpcMac_t976793133::get_offset_of_x1_7(),
	VmpcMac_t976793133::get_offset_of_x2_8(),
	VmpcMac_t976793133::get_offset_of_x3_9(),
	VmpcMac_t976793133::get_offset_of_x4_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4036 = { sizeof (CbcBlockCipher_t424533760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4036[6] = 
{
	CbcBlockCipher_t424533760::get_offset_of_IV_0(),
	CbcBlockCipher_t424533760::get_offset_of_cbcV_1(),
	CbcBlockCipher_t424533760::get_offset_of_cbcNextV_2(),
	CbcBlockCipher_t424533760::get_offset_of_blockSize_3(),
	CbcBlockCipher_t424533760::get_offset_of_cipher_4(),
	CbcBlockCipher_t424533760::get_offset_of_encrypting_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4037 = { sizeof (CcmBlockCipher_t2534265899), -1, sizeof(CcmBlockCipher_t2534265899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4037[10] = 
{
	CcmBlockCipher_t2534265899_StaticFields::get_offset_of_BlockSize_0(),
	CcmBlockCipher_t2534265899::get_offset_of_cipher_1(),
	CcmBlockCipher_t2534265899::get_offset_of_macBlock_2(),
	CcmBlockCipher_t2534265899::get_offset_of_forEncryption_3(),
	CcmBlockCipher_t2534265899::get_offset_of_nonce_4(),
	CcmBlockCipher_t2534265899::get_offset_of_initialAssociatedText_5(),
	CcmBlockCipher_t2534265899::get_offset_of_macSize_6(),
	CcmBlockCipher_t2534265899::get_offset_of_keyParam_7(),
	CcmBlockCipher_t2534265899::get_offset_of_associatedText_8(),
	CcmBlockCipher_t2534265899::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4038 = { sizeof (CfbBlockCipher_t1093564787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4038[6] = 
{
	CfbBlockCipher_t1093564787::get_offset_of_IV_0(),
	CfbBlockCipher_t1093564787::get_offset_of_cfbV_1(),
	CfbBlockCipher_t1093564787::get_offset_of_cfbOutV_2(),
	CfbBlockCipher_t1093564787::get_offset_of_encrypting_3(),
	CfbBlockCipher_t1093564787::get_offset_of_blockSize_4(),
	CfbBlockCipher_t1093564787::get_offset_of_cipher_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4039 = { sizeof (CtsBlockCipher_t3347056426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4039[1] = 
{
	CtsBlockCipher_t3347056426::get_offset_of_blockSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4040 = { sizeof (EaxBlockCipher_t1200038734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4040[12] = 
{
	EaxBlockCipher_t1200038734::get_offset_of_cipher_0(),
	EaxBlockCipher_t1200038734::get_offset_of_forEncryption_1(),
	EaxBlockCipher_t1200038734::get_offset_of_blockSize_2(),
	EaxBlockCipher_t1200038734::get_offset_of_mac_3(),
	EaxBlockCipher_t1200038734::get_offset_of_nonceMac_4(),
	EaxBlockCipher_t1200038734::get_offset_of_associatedTextMac_5(),
	EaxBlockCipher_t1200038734::get_offset_of_macBlock_6(),
	EaxBlockCipher_t1200038734::get_offset_of_macSize_7(),
	EaxBlockCipher_t1200038734::get_offset_of_bufBlock_8(),
	EaxBlockCipher_t1200038734::get_offset_of_bufOff_9(),
	EaxBlockCipher_t1200038734::get_offset_of_cipherInitialized_10(),
	EaxBlockCipher_t1200038734::get_offset_of_initialAssociatedText_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4041 = { sizeof (Tag_t3159821658)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4041[4] = 
{
	Tag_t3159821658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4042 = { sizeof (GcmBlockCipher_t1966957103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4042[22] = 
{
	0,
	GcmBlockCipher_t1966957103::get_offset_of_cipher_1(),
	GcmBlockCipher_t1966957103::get_offset_of_multiplier_2(),
	GcmBlockCipher_t1966957103::get_offset_of_exp_3(),
	GcmBlockCipher_t1966957103::get_offset_of_forEncryption_4(),
	GcmBlockCipher_t1966957103::get_offset_of_macSize_5(),
	GcmBlockCipher_t1966957103::get_offset_of_nonce_6(),
	GcmBlockCipher_t1966957103::get_offset_of_initialAssociatedText_7(),
	GcmBlockCipher_t1966957103::get_offset_of_H_8(),
	GcmBlockCipher_t1966957103::get_offset_of_J0_9(),
	GcmBlockCipher_t1966957103::get_offset_of_bufBlock_10(),
	GcmBlockCipher_t1966957103::get_offset_of_macBlock_11(),
	GcmBlockCipher_t1966957103::get_offset_of_S_12(),
	GcmBlockCipher_t1966957103::get_offset_of_S_at_13(),
	GcmBlockCipher_t1966957103::get_offset_of_S_atPre_14(),
	GcmBlockCipher_t1966957103::get_offset_of_counter_15(),
	GcmBlockCipher_t1966957103::get_offset_of_bufOff_16(),
	GcmBlockCipher_t1966957103::get_offset_of_totalLength_17(),
	GcmBlockCipher_t1966957103::get_offset_of_atBlock_18(),
	GcmBlockCipher_t1966957103::get_offset_of_atBlockPos_19(),
	GcmBlockCipher_t1966957103::get_offset_of_atLength_20(),
	GcmBlockCipher_t1966957103::get_offset_of_atLengthPre_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4043 = { sizeof (GOfbBlockCipher_t591890526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4043[10] = 
{
	GOfbBlockCipher_t591890526::get_offset_of_IV_0(),
	GOfbBlockCipher_t591890526::get_offset_of_ofbV_1(),
	GOfbBlockCipher_t591890526::get_offset_of_ofbOutV_2(),
	GOfbBlockCipher_t591890526::get_offset_of_blockSize_3(),
	GOfbBlockCipher_t591890526::get_offset_of_cipher_4(),
	GOfbBlockCipher_t591890526::get_offset_of_firstStep_5(),
	GOfbBlockCipher_t591890526::get_offset_of_N3_6(),
	GOfbBlockCipher_t591890526::get_offset_of_N4_7(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4044 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4045 = { sizeof (OcbBlockCipher_t427870536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4045[23] = 
{
	0,
	OcbBlockCipher_t427870536::get_offset_of_hashCipher_1(),
	OcbBlockCipher_t427870536::get_offset_of_mainCipher_2(),
	OcbBlockCipher_t427870536::get_offset_of_forEncryption_3(),
	OcbBlockCipher_t427870536::get_offset_of_macSize_4(),
	OcbBlockCipher_t427870536::get_offset_of_initialAssociatedText_5(),
	OcbBlockCipher_t427870536::get_offset_of_L_6(),
	OcbBlockCipher_t427870536::get_offset_of_L_Asterisk_7(),
	OcbBlockCipher_t427870536::get_offset_of_L_Dollar_8(),
	OcbBlockCipher_t427870536::get_offset_of_KtopInput_9(),
	OcbBlockCipher_t427870536::get_offset_of_Stretch_10(),
	OcbBlockCipher_t427870536::get_offset_of_OffsetMAIN_0_11(),
	OcbBlockCipher_t427870536::get_offset_of_hashBlock_12(),
	OcbBlockCipher_t427870536::get_offset_of_mainBlock_13(),
	OcbBlockCipher_t427870536::get_offset_of_hashBlockPos_14(),
	OcbBlockCipher_t427870536::get_offset_of_mainBlockPos_15(),
	OcbBlockCipher_t427870536::get_offset_of_hashBlockCount_16(),
	OcbBlockCipher_t427870536::get_offset_of_mainBlockCount_17(),
	OcbBlockCipher_t427870536::get_offset_of_OffsetHASH_18(),
	OcbBlockCipher_t427870536::get_offset_of_Sum_19(),
	OcbBlockCipher_t427870536::get_offset_of_OffsetMAIN_20(),
	OcbBlockCipher_t427870536::get_offset_of_Checksum_21(),
	OcbBlockCipher_t427870536::get_offset_of_macBlock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4046 = { sizeof (OfbBlockCipher_t2804280439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4046[5] = 
{
	OfbBlockCipher_t2804280439::get_offset_of_IV_0(),
	OfbBlockCipher_t2804280439::get_offset_of_ofbV_1(),
	OfbBlockCipher_t2804280439::get_offset_of_ofbOutV_2(),
	OfbBlockCipher_t2804280439::get_offset_of_blockSize_3(),
	OfbBlockCipher_t2804280439::get_offset_of_cipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4047 = { sizeof (OpenPgpCfbBlockCipher_t1145356922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4047[7] = 
{
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_IV_0(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_FR_1(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_FRE_2(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_cipher_3(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_blockSize_4(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_count_5(),
	OpenPgpCfbBlockCipher_t1145356922::get_offset_of_forEncryption_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4048 = { sizeof (SicBlockCipher_t3693949395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4048[5] = 
{
	SicBlockCipher_t3693949395::get_offset_of_cipher_0(),
	SicBlockCipher_t3693949395::get_offset_of_blockSize_1(),
	SicBlockCipher_t3693949395::get_offset_of_IV_2(),
	SicBlockCipher_t3693949395::get_offset_of_counter_3(),
	SicBlockCipher_t3693949395::get_offset_of_counterOut_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4049 = { sizeof (GcmUtilities_t2204846149), -1, sizeof(GcmUtilities_t2204846149_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4049[3] = 
{
	0,
	0,
	GcmUtilities_t2204846149_StaticFields::get_offset_of_LOOKUP_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4050 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4051 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4052 = { sizeof (Tables1kGcmExponentiator_t1216265862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4052[1] = 
{
	Tables1kGcmExponentiator_t1216265862::get_offset_of_lookupPowX2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4053 = { sizeof (Tables8kGcmMultiplier_t1725665332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4053[2] = 
{
	Tables8kGcmMultiplier_t1725665332::get_offset_of_H_0(),
	Tables8kGcmMultiplier_t1725665332::get_offset_of_M_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4055 = { sizeof (ISO10126d2Padding_t3010076608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4055[1] = 
{
	ISO10126d2Padding_t3010076608::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4056 = { sizeof (ISO7816d4Padding_t609688802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4057 = { sizeof (PaddedBufferedBlockCipher_t4046604239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4057[1] = 
{
	PaddedBufferedBlockCipher_t4046604239::get_offset_of_padding_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4058 = { sizeof (Pkcs7Padding_t2353579457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4059 = { sizeof (TbcPadding_t3233791910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4060 = { sizeof (X923Padding_t2660499907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4060[1] = 
{
	X923Padding_t2660499907::get_offset_of_random_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4061 = { sizeof (ZeroBytePadding_t1951118083), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4062 = { sizeof (AeadParameters_t2995549197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4062[4] = 
{
	AeadParameters_t2995549197::get_offset_of_associatedText_0(),
	AeadParameters_t2995549197::get_offset_of_nonce_1(),
	AeadParameters_t2995549197::get_offset_of_key_2(),
	AeadParameters_t2995549197::get_offset_of_macSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4063 = { sizeof (DHKeyGenerationParameters_t1952252333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4063[1] = 
{
	DHKeyGenerationParameters_t1952252333::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4064 = { sizeof (DHKeyParameters_t508500971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4064[2] = 
{
	DHKeyParameters_t508500971::get_offset_of_parameters_1(),
	DHKeyParameters_t508500971::get_offset_of_algorithmOid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4065 = { sizeof (DHParameters_t431035336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4065[8] = 
{
	0,
	DHParameters_t431035336::get_offset_of_p_1(),
	DHParameters_t431035336::get_offset_of_g_2(),
	DHParameters_t431035336::get_offset_of_q_3(),
	DHParameters_t431035336::get_offset_of_j_4(),
	DHParameters_t431035336::get_offset_of_m_5(),
	DHParameters_t431035336::get_offset_of_l_6(),
	DHParameters_t431035336::get_offset_of_validation_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4066 = { sizeof (DHPrivateKeyParameters_t3120746414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4066[1] = 
{
	DHPrivateKeyParameters_t3120746414::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4067 = { sizeof (DHPublicKeyParameters_t1544976430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4067[1] = 
{
	DHPublicKeyParameters_t1544976430::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4068 = { sizeof (DHValidationParameters_t2841123689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4068[2] = 
{
	DHValidationParameters_t2841123689::get_offset_of_seed_0(),
	DHValidationParameters_t2841123689::get_offset_of_counter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4069 = { sizeof (DesEdeParameters_t597696274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4069[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4070 = { sizeof (DesParameters_t4026919406), -1, sizeof(DesParameters_t4026919406_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4070[3] = 
{
	0,
	0,
	DesParameters_t4026919406_StaticFields::get_offset_of_DES_weak_keys_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4071 = { sizeof (DsaKeyGenerationParameters_t4209685231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4071[1] = 
{
	DsaKeyGenerationParameters_t4209685231::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4072 = { sizeof (DsaKeyParameters_t2298980877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4072[1] = 
{
	DsaKeyParameters_t2298980877::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4073 = { sizeof (DsaParameters_t2550649858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4073[4] = 
{
	DsaParameters_t2550649858::get_offset_of_p_0(),
	DsaParameters_t2550649858::get_offset_of_q_1(),
	DsaParameters_t2550649858::get_offset_of_g_2(),
	DsaParameters_t2550649858::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4074 = { sizeof (DsaPrivateKeyParameters_t1082923572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4074[1] = 
{
	DsaPrivateKeyParameters_t1082923572::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4075 = { sizeof (DsaPublicKeyParameters_t3405498240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4075[1] = 
{
	DsaPublicKeyParameters_t3405498240::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4076 = { sizeof (DsaValidationParameters_t2732896323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4076[3] = 
{
	DsaValidationParameters_t2732896323::get_offset_of_seed_0(),
	DsaValidationParameters_t2732896323::get_offset_of_counter_1(),
	DsaValidationParameters_t2732896323::get_offset_of_usageIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4077 = { sizeof (ECDomainParameters_t3939864474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4077[5] = 
{
	ECDomainParameters_t3939864474::get_offset_of_curve_0(),
	ECDomainParameters_t3939864474::get_offset_of_seed_1(),
	ECDomainParameters_t3939864474::get_offset_of_g_2(),
	ECDomainParameters_t3939864474::get_offset_of_n_3(),
	ECDomainParameters_t3939864474::get_offset_of_h_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4078 = { sizeof (ECKeyGenerationParameters_t3475787761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4078[2] = 
{
	ECKeyGenerationParameters_t3475787761::get_offset_of_domainParams_2(),
	ECKeyGenerationParameters_t3475787761::get_offset_of_publicKeyParamSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4079 = { sizeof (ECKeyParameters_t1064568751), -1, sizeof(ECKeyParameters_t1064568751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4079[4] = 
{
	ECKeyParameters_t1064568751_StaticFields::get_offset_of_algorithms_1(),
	ECKeyParameters_t1064568751::get_offset_of_algorithm_2(),
	ECKeyParameters_t1064568751::get_offset_of_parameters_3(),
	ECKeyParameters_t1064568751::get_offset_of_publicKeyParamSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4080 = { sizeof (ECPrivateKeyParameters_t3632960452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4080[1] = 
{
	ECPrivateKeyParameters_t3632960452::get_offset_of_d_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4081 = { sizeof (ECPublicKeyParameters_t572706344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4081[1] = 
{
	ECPublicKeyParameters_t572706344::get_offset_of_q_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4082 = { sizeof (ElGamalKeyGenerationParameters_t303785198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4082[1] = 
{
	ElGamalKeyGenerationParameters_t303785198::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4083 = { sizeof (ElGamalKeyParameters_t352675504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4083[1] = 
{
	ElGamalKeyParameters_t352675504::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4084 = { sizeof (ElGamalParameters_t1215309569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4084[3] = 
{
	ElGamalParameters_t1215309569::get_offset_of_p_0(),
	ElGamalParameters_t1215309569::get_offset_of_g_1(),
	ElGamalParameters_t1215309569::get_offset_of_l_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4085 = { sizeof (ElGamalPrivateKeyParameters_t2762984431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4085[1] = 
{
	ElGamalPrivateKeyParameters_t2762984431::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4086 = { sizeof (ElGamalPublicKeyParameters_t3740889069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4086[1] = 
{
	ElGamalPublicKeyParameters_t3740889069::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4087 = { sizeof (Gost3410KeyParameters_t1164823134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4087[2] = 
{
	Gost3410KeyParameters_t1164823134::get_offset_of_parameters_1(),
	Gost3410KeyParameters_t1164823134::get_offset_of_publicKeyParamSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4088 = { sizeof (Gost3410Parameters_t602285121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4088[4] = 
{
	Gost3410Parameters_t602285121::get_offset_of_p_0(),
	Gost3410Parameters_t602285121::get_offset_of_q_1(),
	Gost3410Parameters_t602285121::get_offset_of_a_2(),
	Gost3410Parameters_t602285121::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4089 = { sizeof (Gost3410PrivateKeyParameters_t2071462699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4089[1] = 
{
	Gost3410PrivateKeyParameters_t2071462699::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4090 = { sizeof (Gost3410PublicKeyParameters_t521829201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4090[1] = 
{
	Gost3410PublicKeyParameters_t521829201::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4091 = { sizeof (Gost3410ValidationParameters_t2048269886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4091[4] = 
{
	Gost3410ValidationParameters_t2048269886::get_offset_of_x0_0(),
	Gost3410ValidationParameters_t2048269886::get_offset_of_c_1(),
	Gost3410ValidationParameters_t2048269886::get_offset_of_x0L_2(),
	Gost3410ValidationParameters_t2048269886::get_offset_of_cL_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4092 = { sizeof (Iso18033KdfParameters_t964742789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4092[1] = 
{
	Iso18033KdfParameters_t964742789::get_offset_of_seed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4093 = { sizeof (IesParameters_t4148998567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4093[3] = 
{
	IesParameters_t4148998567::get_offset_of_derivation_0(),
	IesParameters_t4148998567::get_offset_of_encoding_1(),
	IesParameters_t4148998567::get_offset_of_macKeySize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4094 = { sizeof (IesWithCipherParameters_t206966936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4094[1] = 
{
	IesWithCipherParameters_t206966936::get_offset_of_cipherKeySize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4095 = { sizeof (KdfParameters_t1337840219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4095[2] = 
{
	KdfParameters_t1337840219::get_offset_of_iv_0(),
	KdfParameters_t1337840219::get_offset_of_shared_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4096 = { sizeof (KeyParameter_t215653120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4096[1] = 
{
	KeyParameter_t215653120::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4097 = { sizeof (MqvPrivateParameters_t2816354209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4097[3] = 
{
	MqvPrivateParameters_t2816354209::get_offset_of_staticPrivateKey_0(),
	MqvPrivateParameters_t2816354209::get_offset_of_ephemeralPrivateKey_1(),
	MqvPrivateParameters_t2816354209::get_offset_of_ephemeralPublicKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4098 = { sizeof (MqvPublicParameters_t4146726485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4098[2] = 
{
	MqvPublicParameters_t4146726485::get_offset_of_staticPublicKey_0(),
	MqvPublicParameters_t4146726485::get_offset_of_ephemeralPublicKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4099 = { sizeof (ParametersWithIV_t2760900877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4099[2] = 
{
	ParametersWithIV_t2760900877::get_offset_of_parameters_0(),
	ParametersWithIV_t2760900877::get_offset_of_iv_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
