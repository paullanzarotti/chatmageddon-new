﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Dream
struct CameraFilterPack_Distortion_Dream_t823945338;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Dream::.ctor()
extern "C"  void CameraFilterPack_Distortion_Dream__ctor_m1454702775 (CameraFilterPack_Distortion_Dream_t823945338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Dream::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Dream_get_material_m2597180726 (CameraFilterPack_Distortion_Dream_t823945338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::Start()
extern "C"  void CameraFilterPack_Distortion_Dream_Start_m327761131 (CameraFilterPack_Distortion_Dream_t823945338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Dream_OnRenderImage_m848842811 (CameraFilterPack_Distortion_Dream_t823945338 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Dream_OnValidate_m269957746 (CameraFilterPack_Distortion_Dream_t823945338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::Update()
extern "C"  void CameraFilterPack_Distortion_Dream_Update_m1441922980 (CameraFilterPack_Distortion_Dream_t823945338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Dream_OnDisable_m1482717858 (CameraFilterPack_Distortion_Dream_t823945338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
