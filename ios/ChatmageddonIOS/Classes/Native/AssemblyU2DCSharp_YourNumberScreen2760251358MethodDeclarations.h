﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// YourNumberScreen
struct YourNumberScreen_t2760251358;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void YourNumberScreen::.ctor()
extern "C"  void YourNumberScreen__ctor_m1100232789 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YourNumberScreen::Start()
extern "C"  void YourNumberScreen_Start_m2226407389 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YourNumberScreen::RegisterUIClose()
extern "C"  void YourNumberScreen_RegisterUIClose_m763565588 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YourNumberScreen::YourNumberUIClosing()
extern "C"  void YourNumberScreen_YourNumberUIClosing_m184456198 (YourNumberScreen_t2760251358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YourNumberScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void YourNumberScreen_ScreenLoad_m1414592691 (YourNumberScreen_t2760251358 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void YourNumberScreen::ScreenUnload(NavigationDirection)
extern "C"  void YourNumberScreen_ScreenUnload_m2285231787 (YourNumberScreen_t2760251358 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean YourNumberScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool YourNumberScreen_ValidateScreenNavigation_m4236459530 (YourNumberScreen_t2760251358 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
