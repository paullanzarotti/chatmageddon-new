﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21603735834.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m314027741_gshared (KeyValuePair_2_t1603735834 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m314027741(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1603735834 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m314027741_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1725944715_gshared (KeyValuePair_2_t1603735834 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1725944715(__this, method) ((  int32_t (*) (KeyValuePair_2_t1603735834 *, const MethodInfo*))KeyValuePair_2_get_Key_m1725944715_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m618491520_gshared (KeyValuePair_2_t1603735834 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m618491520(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1603735834 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m618491520_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2780871275_gshared (KeyValuePair_2_t1603735834 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2780871275(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1603735834 *, const MethodInfo*))KeyValuePair_2_get_Value_m2780871275_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m207419112_gshared (KeyValuePair_2_t1603735834 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m207419112(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1603735834 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m207419112_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1695002580_gshared (KeyValuePair_2_t1603735834 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1695002580(__this, method) ((  String_t* (*) (KeyValuePair_2_t1603735834 *, const MethodInfo*))KeyValuePair_2_ToString_m1695002580_gshared)(__this, method)
