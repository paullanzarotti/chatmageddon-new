﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// System.Collections.Generic.List`1<UILabel>
struct List_1_t1164236560;
// System.Collections.Generic.List`1<UISprite>
struct List_1_t4267705163;
// System.Collections.Generic.List`1<UnityEngine.BoxCollider>
struct List_1_t3687008489;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsProfileUIScaler
struct  SettingsProfileUIScaler_t1908809364  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.Transform SettingsProfileUIScaler::profilePicture
	Transform_t3275118058 * ___profilePicture_14;
	// UnityEngine.Transform SettingsProfileUIScaler::profileNames
	Transform_t3275118058 * ___profileNames_15;
	// UnityEngine.Transform SettingsProfileUIScaler::displayLoactionSwitch
	Transform_t3275118058 * ___displayLoactionSwitch_16;
	// UnityEngine.BoxCollider SettingsProfileUIScaler::displayLocationSwitchCollider
	BoxCollider_t22920061 * ___displayLocationSwitchCollider_17;
	// UnityEngine.BoxCollider SettingsProfileUIScaler::displayLocationCollider
	BoxCollider_t22920061 * ___displayLocationCollider_18;
	// System.Collections.Generic.List`1<UILabel> SettingsProfileUIScaler::labels
	List_1_t1164236560 * ___labels_19;
	// System.Collections.Generic.List`1<UISprite> SettingsProfileUIScaler::navSprites
	List_1_t4267705163 * ___navSprites_20;
	// System.Collections.Generic.List`1<UISprite> SettingsProfileUIScaler::dividers
	List_1_t4267705163 * ___dividers_21;
	// System.Collections.Generic.List`1<UnityEngine.BoxCollider> SettingsProfileUIScaler::colliders
	List_1_t3687008489 * ___colliders_22;

public:
	inline static int32_t get_offset_of_profilePicture_14() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___profilePicture_14)); }
	inline Transform_t3275118058 * get_profilePicture_14() const { return ___profilePicture_14; }
	inline Transform_t3275118058 ** get_address_of_profilePicture_14() { return &___profilePicture_14; }
	inline void set_profilePicture_14(Transform_t3275118058 * value)
	{
		___profilePicture_14 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_14, value);
	}

	inline static int32_t get_offset_of_profileNames_15() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___profileNames_15)); }
	inline Transform_t3275118058 * get_profileNames_15() const { return ___profileNames_15; }
	inline Transform_t3275118058 ** get_address_of_profileNames_15() { return &___profileNames_15; }
	inline void set_profileNames_15(Transform_t3275118058 * value)
	{
		___profileNames_15 = value;
		Il2CppCodeGenWriteBarrier(&___profileNames_15, value);
	}

	inline static int32_t get_offset_of_displayLoactionSwitch_16() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___displayLoactionSwitch_16)); }
	inline Transform_t3275118058 * get_displayLoactionSwitch_16() const { return ___displayLoactionSwitch_16; }
	inline Transform_t3275118058 ** get_address_of_displayLoactionSwitch_16() { return &___displayLoactionSwitch_16; }
	inline void set_displayLoactionSwitch_16(Transform_t3275118058 * value)
	{
		___displayLoactionSwitch_16 = value;
		Il2CppCodeGenWriteBarrier(&___displayLoactionSwitch_16, value);
	}

	inline static int32_t get_offset_of_displayLocationSwitchCollider_17() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___displayLocationSwitchCollider_17)); }
	inline BoxCollider_t22920061 * get_displayLocationSwitchCollider_17() const { return ___displayLocationSwitchCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_displayLocationSwitchCollider_17() { return &___displayLocationSwitchCollider_17; }
	inline void set_displayLocationSwitchCollider_17(BoxCollider_t22920061 * value)
	{
		___displayLocationSwitchCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___displayLocationSwitchCollider_17, value);
	}

	inline static int32_t get_offset_of_displayLocationCollider_18() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___displayLocationCollider_18)); }
	inline BoxCollider_t22920061 * get_displayLocationCollider_18() const { return ___displayLocationCollider_18; }
	inline BoxCollider_t22920061 ** get_address_of_displayLocationCollider_18() { return &___displayLocationCollider_18; }
	inline void set_displayLocationCollider_18(BoxCollider_t22920061 * value)
	{
		___displayLocationCollider_18 = value;
		Il2CppCodeGenWriteBarrier(&___displayLocationCollider_18, value);
	}

	inline static int32_t get_offset_of_labels_19() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___labels_19)); }
	inline List_1_t1164236560 * get_labels_19() const { return ___labels_19; }
	inline List_1_t1164236560 ** get_address_of_labels_19() { return &___labels_19; }
	inline void set_labels_19(List_1_t1164236560 * value)
	{
		___labels_19 = value;
		Il2CppCodeGenWriteBarrier(&___labels_19, value);
	}

	inline static int32_t get_offset_of_navSprites_20() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___navSprites_20)); }
	inline List_1_t4267705163 * get_navSprites_20() const { return ___navSprites_20; }
	inline List_1_t4267705163 ** get_address_of_navSprites_20() { return &___navSprites_20; }
	inline void set_navSprites_20(List_1_t4267705163 * value)
	{
		___navSprites_20 = value;
		Il2CppCodeGenWriteBarrier(&___navSprites_20, value);
	}

	inline static int32_t get_offset_of_dividers_21() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___dividers_21)); }
	inline List_1_t4267705163 * get_dividers_21() const { return ___dividers_21; }
	inline List_1_t4267705163 ** get_address_of_dividers_21() { return &___dividers_21; }
	inline void set_dividers_21(List_1_t4267705163 * value)
	{
		___dividers_21 = value;
		Il2CppCodeGenWriteBarrier(&___dividers_21, value);
	}

	inline static int32_t get_offset_of_colliders_22() { return static_cast<int32_t>(offsetof(SettingsProfileUIScaler_t1908809364, ___colliders_22)); }
	inline List_1_t3687008489 * get_colliders_22() const { return ___colliders_22; }
	inline List_1_t3687008489 ** get_address_of_colliders_22() { return &___colliders_22; }
	inline void set_colliders_22(List_1_t3687008489 * value)
	{
		___colliders_22 = value;
		Il2CppCodeGenWriteBarrier(&___colliders_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
