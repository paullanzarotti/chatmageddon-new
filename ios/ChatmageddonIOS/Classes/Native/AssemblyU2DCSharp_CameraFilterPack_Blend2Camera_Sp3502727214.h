﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blend2Camera_SplitScreen
struct  CameraFilterPack_Blend2Camera_SplitScreen_t3502727214  : public MonoBehaviour_t1158329972
{
public:
	// System.String CameraFilterPack_Blend2Camera_SplitScreen::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_SplitScreen::SCShader
	Shader_t2430389951 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_SplitScreen::Camera2
	Camera_t189460977 * ___Camera2_4;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::TimeX
	float ___TimeX_5;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_SplitScreen::SCMaterial
	Material_t193706927 * ___SCMaterial_6;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::SwitchCameraToCamera2
	float ___SwitchCameraToCamera2_7;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::BlendFX
	float ___BlendFX_8;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::SplitX
	float ___SplitX_9;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::SplitY
	float ___SplitY_10;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::Smooth
	float ___Smooth_11;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::Rotation
	float ___Rotation_12;
	// System.Boolean CameraFilterPack_Blend2Camera_SplitScreen::ForceYSwap
	bool ___ForceYSwap_13;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_SplitScreen::Camera2tex
	RenderTexture_t2666733923 * ___Camera2tex_21;
	// UnityEngine.Vector2 CameraFilterPack_Blend2Camera_SplitScreen::ScreenSize
	Vector2_t2243707579  ___ScreenSize_22;

public:
	inline static int32_t get_offset_of_ShaderName_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___ShaderName_2)); }
	inline String_t* get_ShaderName_2() const { return ___ShaderName_2; }
	inline String_t** get_address_of_ShaderName_2() { return &___ShaderName_2; }
	inline void set_ShaderName_2(String_t* value)
	{
		___ShaderName_2 = value;
		Il2CppCodeGenWriteBarrier(&___ShaderName_2, value);
	}

	inline static int32_t get_offset_of_SCShader_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___SCShader_3)); }
	inline Shader_t2430389951 * get_SCShader_3() const { return ___SCShader_3; }
	inline Shader_t2430389951 ** get_address_of_SCShader_3() { return &___SCShader_3; }
	inline void set_SCShader_3(Shader_t2430389951 * value)
	{
		___SCShader_3 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_3, value);
	}

	inline static int32_t get_offset_of_Camera2_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___Camera2_4)); }
	inline Camera_t189460977 * get_Camera2_4() const { return ___Camera2_4; }
	inline Camera_t189460977 ** get_address_of_Camera2_4() { return &___Camera2_4; }
	inline void set_Camera2_4(Camera_t189460977 * value)
	{
		___Camera2_4 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2_4, value);
	}

	inline static int32_t get_offset_of_TimeX_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___TimeX_5)); }
	inline float get_TimeX_5() const { return ___TimeX_5; }
	inline float* get_address_of_TimeX_5() { return &___TimeX_5; }
	inline void set_TimeX_5(float value)
	{
		___TimeX_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}

	inline static int32_t get_offset_of_SwitchCameraToCamera2_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___SwitchCameraToCamera2_7)); }
	inline float get_SwitchCameraToCamera2_7() const { return ___SwitchCameraToCamera2_7; }
	inline float* get_address_of_SwitchCameraToCamera2_7() { return &___SwitchCameraToCamera2_7; }
	inline void set_SwitchCameraToCamera2_7(float value)
	{
		___SwitchCameraToCamera2_7 = value;
	}

	inline static int32_t get_offset_of_BlendFX_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___BlendFX_8)); }
	inline float get_BlendFX_8() const { return ___BlendFX_8; }
	inline float* get_address_of_BlendFX_8() { return &___BlendFX_8; }
	inline void set_BlendFX_8(float value)
	{
		___BlendFX_8 = value;
	}

	inline static int32_t get_offset_of_SplitX_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___SplitX_9)); }
	inline float get_SplitX_9() const { return ___SplitX_9; }
	inline float* get_address_of_SplitX_9() { return &___SplitX_9; }
	inline void set_SplitX_9(float value)
	{
		___SplitX_9 = value;
	}

	inline static int32_t get_offset_of_SplitY_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___SplitY_10)); }
	inline float get_SplitY_10() const { return ___SplitY_10; }
	inline float* get_address_of_SplitY_10() { return &___SplitY_10; }
	inline void set_SplitY_10(float value)
	{
		___SplitY_10 = value;
	}

	inline static int32_t get_offset_of_Smooth_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___Smooth_11)); }
	inline float get_Smooth_11() const { return ___Smooth_11; }
	inline float* get_address_of_Smooth_11() { return &___Smooth_11; }
	inline void set_Smooth_11(float value)
	{
		___Smooth_11 = value;
	}

	inline static int32_t get_offset_of_Rotation_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___Rotation_12)); }
	inline float get_Rotation_12() const { return ___Rotation_12; }
	inline float* get_address_of_Rotation_12() { return &___Rotation_12; }
	inline void set_Rotation_12(float value)
	{
		___Rotation_12 = value;
	}

	inline static int32_t get_offset_of_ForceYSwap_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___ForceYSwap_13)); }
	inline bool get_ForceYSwap_13() const { return ___ForceYSwap_13; }
	inline bool* get_address_of_ForceYSwap_13() { return &___ForceYSwap_13; }
	inline void set_ForceYSwap_13(bool value)
	{
		___ForceYSwap_13 = value;
	}

	inline static int32_t get_offset_of_Camera2tex_21() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___Camera2tex_21)); }
	inline RenderTexture_t2666733923 * get_Camera2tex_21() const { return ___Camera2tex_21; }
	inline RenderTexture_t2666733923 ** get_address_of_Camera2tex_21() { return &___Camera2tex_21; }
	inline void set_Camera2tex_21(RenderTexture_t2666733923 * value)
	{
		___Camera2tex_21 = value;
		Il2CppCodeGenWriteBarrier(&___Camera2tex_21, value);
	}

	inline static int32_t get_offset_of_ScreenSize_22() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214, ___ScreenSize_22)); }
	inline Vector2_t2243707579  get_ScreenSize_22() const { return ___ScreenSize_22; }
	inline Vector2_t2243707579 * get_address_of_ScreenSize_22() { return &___ScreenSize_22; }
	inline void set_ScreenSize_22(Vector2_t2243707579  value)
	{
		___ScreenSize_22 = value;
	}
};

struct CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields
{
public:
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue
	float ___ChangeValue_14;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue2
	float ___ChangeValue2_15;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue3
	float ___ChangeValue3_16;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue4
	float ___ChangeValue4_17;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue5
	float ___ChangeValue5_18;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue6
	float ___ChangeValue6_19;
	// System.Boolean CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue7
	bool ___ChangeValue7_20;

public:
	inline static int32_t get_offset_of_ChangeValue_14() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields, ___ChangeValue_14)); }
	inline float get_ChangeValue_14() const { return ___ChangeValue_14; }
	inline float* get_address_of_ChangeValue_14() { return &___ChangeValue_14; }
	inline void set_ChangeValue_14(float value)
	{
		___ChangeValue_14 = value;
	}

	inline static int32_t get_offset_of_ChangeValue2_15() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields, ___ChangeValue2_15)); }
	inline float get_ChangeValue2_15() const { return ___ChangeValue2_15; }
	inline float* get_address_of_ChangeValue2_15() { return &___ChangeValue2_15; }
	inline void set_ChangeValue2_15(float value)
	{
		___ChangeValue2_15 = value;
	}

	inline static int32_t get_offset_of_ChangeValue3_16() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields, ___ChangeValue3_16)); }
	inline float get_ChangeValue3_16() const { return ___ChangeValue3_16; }
	inline float* get_address_of_ChangeValue3_16() { return &___ChangeValue3_16; }
	inline void set_ChangeValue3_16(float value)
	{
		___ChangeValue3_16 = value;
	}

	inline static int32_t get_offset_of_ChangeValue4_17() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields, ___ChangeValue4_17)); }
	inline float get_ChangeValue4_17() const { return ___ChangeValue4_17; }
	inline float* get_address_of_ChangeValue4_17() { return &___ChangeValue4_17; }
	inline void set_ChangeValue4_17(float value)
	{
		___ChangeValue4_17 = value;
	}

	inline static int32_t get_offset_of_ChangeValue5_18() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields, ___ChangeValue5_18)); }
	inline float get_ChangeValue5_18() const { return ___ChangeValue5_18; }
	inline float* get_address_of_ChangeValue5_18() { return &___ChangeValue5_18; }
	inline void set_ChangeValue5_18(float value)
	{
		___ChangeValue5_18 = value;
	}

	inline static int32_t get_offset_of_ChangeValue6_19() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields, ___ChangeValue6_19)); }
	inline float get_ChangeValue6_19() const { return ___ChangeValue6_19; }
	inline float* get_address_of_ChangeValue6_19() { return &___ChangeValue6_19; }
	inline void set_ChangeValue6_19(float value)
	{
		___ChangeValue6_19 = value;
	}

	inline static int32_t get_offset_of_ChangeValue7_20() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blend2Camera_SplitScreen_t3502727214_StaticFields, ___ChangeValue7_20)); }
	inline bool get_ChangeValue7_20() const { return ___ChangeValue7_20; }
	inline bool* get_address_of_ChangeValue7_20() { return &___ChangeValue7_20; }
	inline void set_ChangeValue7_20(bool value)
	{
		___ChangeValue7_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
