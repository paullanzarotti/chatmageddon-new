﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPTexturePicker
struct IPTexturePicker_t4004014186;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void IPTexturePicker::.ctor()
extern "C"  void IPTexturePicker__ctor_m2479089611 (IPTexturePicker_t4004014186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture IPTexturePicker::get_CurrentTexture()
extern "C"  Texture_t2243626319 * IPTexturePicker_get_CurrentTexture_m3608943171 (IPTexturePicker_t4004014186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::set_CurrentTexture(UnityEngine.Texture)
extern "C"  void IPTexturePicker_set_CurrentTexture_m2227938740 (IPTexturePicker_t4004014186 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::ResetPickerAtContentIndex(System.Int32)
extern "C"  void IPTexturePicker_ResetPickerAtContentIndex_m2355634803 (IPTexturePicker_t4004014186 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::InsertElement(UnityEngine.Texture)
extern "C"  void IPTexturePicker_InsertElement_m990171658 (IPTexturePicker_t4004014186 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::InsertElementAtIndex(UnityEngine.Texture,System.Int32,System.Boolean)
extern "C"  void IPTexturePicker_InsertElementAtIndex_m2225254479 (IPTexturePicker_t4004014186 * __this, Texture_t2243626319 * ___texture0, int32_t ___index1, bool ___resetPicker2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::RemoveElementAtIndex(System.Int32,System.Boolean)
extern "C"  void IPTexturePicker_RemoveElementAtIndex_m491573730 (IPTexturePicker_t4004014186 * __this, int32_t ___index0, bool ___resetPicker1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPTexturePicker::GetInitIndex()
extern "C"  int32_t IPTexturePicker_GetInitIndex_m194376043 (IPTexturePicker_t4004014186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::UpdateCurrentValue()
extern "C"  void IPTexturePicker_UpdateCurrentValue_m3332608334 (IPTexturePicker_t4004014186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::UpdateWidget(System.Int32,System.Int32)
extern "C"  void IPTexturePicker_UpdateWidget_m101700696 (IPTexturePicker_t4004014186 * __this, int32_t ___widgetIndex0, int32_t ___contentIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTexturePicker::UpdateVirtualElementsCount()
extern "C"  void IPTexturePicker_UpdateVirtualElementsCount_m1582586689 (IPTexturePicker_t4004014186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
