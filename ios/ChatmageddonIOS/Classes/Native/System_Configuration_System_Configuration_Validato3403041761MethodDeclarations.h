﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ValidatorCallback
struct ValidatorCallback_t3403041761;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Configuration.ValidatorCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidatorCallback__ctor_m3792214536 (ValidatorCallback_t3403041761 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ValidatorCallback::Invoke(System.Object)
extern "C"  void ValidatorCallback_Invoke_m2470494178 (ValidatorCallback_t3403041761 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Configuration.ValidatorCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidatorCallback_BeginInvoke_m4154982075 (ValidatorCallback_t3403041761 * __this, Il2CppObject * ___o0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ValidatorCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ValidatorCallback_EndInvoke_m3443120546 (ValidatorCallback_t3403041761 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
