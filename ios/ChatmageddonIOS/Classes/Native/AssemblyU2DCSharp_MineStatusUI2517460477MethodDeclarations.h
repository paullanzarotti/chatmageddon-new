﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineStatusUI
struct MineStatusUI_t2517460477;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"

// System.Void MineStatusUI::.ctor()
extern "C"  void MineStatusUI__ctor_m3288339874 (MineStatusUI_t2517460477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineStatusUI::LoadUI(System.Boolean)
extern "C"  void MineStatusUI_LoadUI_m1532354685 (MineStatusUI_t2517460477 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineStatusUI::UnloadUI(System.Boolean)
extern "C"  void MineStatusUI_UnloadUI_m1647165208 (MineStatusUI_t2517460477 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineStatusUI::SetMineExploded()
extern "C"  void MineStatusUI_SetMineExploded_m1000046514 (MineStatusUI_t2517460477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineStatusUI::SetMineDefused()
extern "C"  void MineStatusUI_SetMineDefused_m473946309 (MineStatusUI_t2517460477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineStatusUI::SetMineCleared()
extern "C"  void MineStatusUI_SetMineCleared_m2539717649 (MineStatusUI_t2517460477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineStatusUI::SetFlamesActive(System.Boolean,System.Boolean)
extern "C"  void MineStatusUI_SetFlamesActive_m2078586156 (MineStatusUI_t2517460477 * __this, bool ___active0, bool ___withAnim1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineStatusUI::SetPointsLabels(MissileTravelState)
extern "C"  void MineStatusUI_SetPointsLabels_m2058106983 (MineStatusUI_t2517460477 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
