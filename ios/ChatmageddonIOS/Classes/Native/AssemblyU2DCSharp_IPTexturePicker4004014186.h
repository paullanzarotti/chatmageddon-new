﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Texture>
struct List_1_t1612747451;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "AssemblyU2DCSharp_IPTexturePickerBase1323538019.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPTexturePicker
struct  IPTexturePicker_t4004014186  : public IPTexturePickerBase_t1323538019
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Texture> IPTexturePicker::textures
	List_1_t1612747451 * ___textures_15;
	// System.Boolean IPTexturePicker::normalizeTextures
	bool ___normalizeTextures_16;
	// System.Single IPTexturePicker::normalizedMax
	float ___normalizedMax_17;
	// System.Int32 IPTexturePicker::initIndex
	int32_t ___initIndex_18;
	// UnityEngine.Texture IPTexturePicker::<CurrentTexture>k__BackingField
	Texture_t2243626319 * ___U3CCurrentTextureU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_textures_15() { return static_cast<int32_t>(offsetof(IPTexturePicker_t4004014186, ___textures_15)); }
	inline List_1_t1612747451 * get_textures_15() const { return ___textures_15; }
	inline List_1_t1612747451 ** get_address_of_textures_15() { return &___textures_15; }
	inline void set_textures_15(List_1_t1612747451 * value)
	{
		___textures_15 = value;
		Il2CppCodeGenWriteBarrier(&___textures_15, value);
	}

	inline static int32_t get_offset_of_normalizeTextures_16() { return static_cast<int32_t>(offsetof(IPTexturePicker_t4004014186, ___normalizeTextures_16)); }
	inline bool get_normalizeTextures_16() const { return ___normalizeTextures_16; }
	inline bool* get_address_of_normalizeTextures_16() { return &___normalizeTextures_16; }
	inline void set_normalizeTextures_16(bool value)
	{
		___normalizeTextures_16 = value;
	}

	inline static int32_t get_offset_of_normalizedMax_17() { return static_cast<int32_t>(offsetof(IPTexturePicker_t4004014186, ___normalizedMax_17)); }
	inline float get_normalizedMax_17() const { return ___normalizedMax_17; }
	inline float* get_address_of_normalizedMax_17() { return &___normalizedMax_17; }
	inline void set_normalizedMax_17(float value)
	{
		___normalizedMax_17 = value;
	}

	inline static int32_t get_offset_of_initIndex_18() { return static_cast<int32_t>(offsetof(IPTexturePicker_t4004014186, ___initIndex_18)); }
	inline int32_t get_initIndex_18() const { return ___initIndex_18; }
	inline int32_t* get_address_of_initIndex_18() { return &___initIndex_18; }
	inline void set_initIndex_18(int32_t value)
	{
		___initIndex_18 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentTextureU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(IPTexturePicker_t4004014186, ___U3CCurrentTextureU3Ek__BackingField_19)); }
	inline Texture_t2243626319 * get_U3CCurrentTextureU3Ek__BackingField_19() const { return ___U3CCurrentTextureU3Ek__BackingField_19; }
	inline Texture_t2243626319 ** get_address_of_U3CCurrentTextureU3Ek__BackingField_19() { return &___U3CCurrentTextureU3Ek__BackingField_19; }
	inline void set_U3CCurrentTextureU3Ek__BackingField_19(Texture_t2243626319 * value)
	{
		___U3CCurrentTextureU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentTextureU3Ek__BackingField_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
