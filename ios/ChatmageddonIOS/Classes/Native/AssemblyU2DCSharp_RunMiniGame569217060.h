﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LocationDistanceController
struct LocationDistanceController_t4191421708;

#include "AssemblyU2DCSharp_DefendMiniGameController1844356491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RunMiniGame
struct  RunMiniGame_t569217060  : public DefendMiniGameController_t1844356491
{
public:
	// LocationDistanceController RunMiniGame::distanceController
	LocationDistanceController_t4191421708 * ___distanceController_6;

public:
	inline static int32_t get_offset_of_distanceController_6() { return static_cast<int32_t>(offsetof(RunMiniGame_t569217060, ___distanceController_6)); }
	inline LocationDistanceController_t4191421708 * get_distanceController_6() const { return ___distanceController_6; }
	inline LocationDistanceController_t4191421708 ** get_address_of_distanceController_6() { return &___distanceController_6; }
	inline void set_distanceController_6(LocationDistanceController_t4191421708 * value)
	{
		___distanceController_6 = value;
		Il2CppCodeGenWriteBarrier(&___distanceController_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
