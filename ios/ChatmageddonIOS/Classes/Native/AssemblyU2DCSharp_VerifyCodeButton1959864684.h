﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VerificationCodeInput
struct VerificationCodeInput_t924401746;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VerifyCodeButton
struct  VerifyCodeButton_t1959864684  : public SFXButton_t792651341
{
public:
	// VerificationCodeInput VerifyCodeButton::input
	VerificationCodeInput_t924401746 * ___input_5;
	// UISprite VerifyCodeButton::buttonBackground
	UISprite_t603616735 * ___buttonBackground_6;

public:
	inline static int32_t get_offset_of_input_5() { return static_cast<int32_t>(offsetof(VerifyCodeButton_t1959864684, ___input_5)); }
	inline VerificationCodeInput_t924401746 * get_input_5() const { return ___input_5; }
	inline VerificationCodeInput_t924401746 ** get_address_of_input_5() { return &___input_5; }
	inline void set_input_5(VerificationCodeInput_t924401746 * value)
	{
		___input_5 = value;
		Il2CppCodeGenWriteBarrier(&___input_5, value);
	}

	inline static int32_t get_offset_of_buttonBackground_6() { return static_cast<int32_t>(offsetof(VerifyCodeButton_t1959864684, ___buttonBackground_6)); }
	inline UISprite_t603616735 * get_buttonBackground_6() const { return ___buttonBackground_6; }
	inline UISprite_t603616735 ** get_address_of_buttonBackground_6() { return &___buttonBackground_6; }
	inline void set_buttonBackground_6(UISprite_t603616735 * value)
	{
		___buttonBackground_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonBackground_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
