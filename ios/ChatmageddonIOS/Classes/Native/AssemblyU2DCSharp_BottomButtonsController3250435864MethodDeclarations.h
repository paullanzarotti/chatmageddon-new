﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BottomButtonsController
struct BottomButtonsController_t3250435864;

#include "codegen/il2cpp-codegen.h"

// System.Void BottomButtonsController::.ctor()
extern "C"  void BottomButtonsController__ctor_m1407377369 (BottomButtonsController_t3250435864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonsController::Start()
extern "C"  void BottomButtonsController_Start_m2269171657 (BottomButtonsController_t3250435864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonsController::ButtonsTweenFinished()
extern "C"  void BottomButtonsController_ButtonsTweenFinished_m2766891279 (BottomButtonsController_t3250435864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButtonsController::SetTargetsActive(System.Boolean)
extern "C"  void BottomButtonsController_SetTargetsActive_m2179898750 (BottomButtonsController_t3250435864 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
