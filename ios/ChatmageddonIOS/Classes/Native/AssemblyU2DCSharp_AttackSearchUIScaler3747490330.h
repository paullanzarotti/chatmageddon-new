﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UIPanel
struct UIPanel_t1795085332;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackSearchUIScaler
struct  AttackSearchUIScaler_t3747490330  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite AttackSearchUIScaler::tabBackground
	UISprite_t603616735 * ___tabBackground_14;
	// UISprite AttackSearchUIScaler::bottomSeperator
	UISprite_t603616735 * ___bottomSeperator_15;
	// UnityEngine.Transform AttackSearchUIScaler::friendTab
	Transform_t3275118058 * ___friendTab_16;
	// UnityEngine.BoxCollider AttackSearchUIScaler::friendTabCollider
	BoxCollider_t22920061 * ___friendTabCollider_17;
	// UnityEngine.Transform AttackSearchUIScaler::globalTab
	Transform_t3275118058 * ___globalTab_18;
	// UnityEngine.BoxCollider AttackSearchUIScaler::globalTabCollider
	BoxCollider_t22920061 * ___globalTabCollider_19;
	// UIPanel AttackSearchUIScaler::friendPanel
	UIPanel_t1795085332 * ___friendPanel_20;
	// UIPanel AttackSearchUIScaler::globalPanel
	UIPanel_t1795085332 * ___globalPanel_21;
	// UnityEngine.BoxCollider AttackSearchUIScaler::searchCollider
	BoxCollider_t22920061 * ___searchCollider_22;
	// UILabel AttackSearchUIScaler::searchLabel
	UILabel_t1795115428 * ___searchLabel_23;
	// UnityEngine.Transform AttackSearchUIScaler::searchButton
	Transform_t3275118058 * ___searchButton_24;
	// UISprite AttackSearchUIScaler::searchDivider
	UISprite_t603616735 * ___searchDivider_25;
	// UILabel AttackSearchUIScaler::friendsNoResultsLabel
	UILabel_t1795115428 * ___friendsNoResultsLabel_26;
	// UILabel AttackSearchUIScaler::globalNoResultsLabel
	UILabel_t1795115428 * ___globalNoResultsLabel_27;

public:
	inline static int32_t get_offset_of_tabBackground_14() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___tabBackground_14)); }
	inline UISprite_t603616735 * get_tabBackground_14() const { return ___tabBackground_14; }
	inline UISprite_t603616735 ** get_address_of_tabBackground_14() { return &___tabBackground_14; }
	inline void set_tabBackground_14(UISprite_t603616735 * value)
	{
		___tabBackground_14 = value;
		Il2CppCodeGenWriteBarrier(&___tabBackground_14, value);
	}

	inline static int32_t get_offset_of_bottomSeperator_15() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___bottomSeperator_15)); }
	inline UISprite_t603616735 * get_bottomSeperator_15() const { return ___bottomSeperator_15; }
	inline UISprite_t603616735 ** get_address_of_bottomSeperator_15() { return &___bottomSeperator_15; }
	inline void set_bottomSeperator_15(UISprite_t603616735 * value)
	{
		___bottomSeperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___bottomSeperator_15, value);
	}

	inline static int32_t get_offset_of_friendTab_16() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___friendTab_16)); }
	inline Transform_t3275118058 * get_friendTab_16() const { return ___friendTab_16; }
	inline Transform_t3275118058 ** get_address_of_friendTab_16() { return &___friendTab_16; }
	inline void set_friendTab_16(Transform_t3275118058 * value)
	{
		___friendTab_16 = value;
		Il2CppCodeGenWriteBarrier(&___friendTab_16, value);
	}

	inline static int32_t get_offset_of_friendTabCollider_17() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___friendTabCollider_17)); }
	inline BoxCollider_t22920061 * get_friendTabCollider_17() const { return ___friendTabCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_friendTabCollider_17() { return &___friendTabCollider_17; }
	inline void set_friendTabCollider_17(BoxCollider_t22920061 * value)
	{
		___friendTabCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___friendTabCollider_17, value);
	}

	inline static int32_t get_offset_of_globalTab_18() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___globalTab_18)); }
	inline Transform_t3275118058 * get_globalTab_18() const { return ___globalTab_18; }
	inline Transform_t3275118058 ** get_address_of_globalTab_18() { return &___globalTab_18; }
	inline void set_globalTab_18(Transform_t3275118058 * value)
	{
		___globalTab_18 = value;
		Il2CppCodeGenWriteBarrier(&___globalTab_18, value);
	}

	inline static int32_t get_offset_of_globalTabCollider_19() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___globalTabCollider_19)); }
	inline BoxCollider_t22920061 * get_globalTabCollider_19() const { return ___globalTabCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_globalTabCollider_19() { return &___globalTabCollider_19; }
	inline void set_globalTabCollider_19(BoxCollider_t22920061 * value)
	{
		___globalTabCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___globalTabCollider_19, value);
	}

	inline static int32_t get_offset_of_friendPanel_20() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___friendPanel_20)); }
	inline UIPanel_t1795085332 * get_friendPanel_20() const { return ___friendPanel_20; }
	inline UIPanel_t1795085332 ** get_address_of_friendPanel_20() { return &___friendPanel_20; }
	inline void set_friendPanel_20(UIPanel_t1795085332 * value)
	{
		___friendPanel_20 = value;
		Il2CppCodeGenWriteBarrier(&___friendPanel_20, value);
	}

	inline static int32_t get_offset_of_globalPanel_21() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___globalPanel_21)); }
	inline UIPanel_t1795085332 * get_globalPanel_21() const { return ___globalPanel_21; }
	inline UIPanel_t1795085332 ** get_address_of_globalPanel_21() { return &___globalPanel_21; }
	inline void set_globalPanel_21(UIPanel_t1795085332 * value)
	{
		___globalPanel_21 = value;
		Il2CppCodeGenWriteBarrier(&___globalPanel_21, value);
	}

	inline static int32_t get_offset_of_searchCollider_22() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___searchCollider_22)); }
	inline BoxCollider_t22920061 * get_searchCollider_22() const { return ___searchCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_searchCollider_22() { return &___searchCollider_22; }
	inline void set_searchCollider_22(BoxCollider_t22920061 * value)
	{
		___searchCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___searchCollider_22, value);
	}

	inline static int32_t get_offset_of_searchLabel_23() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___searchLabel_23)); }
	inline UILabel_t1795115428 * get_searchLabel_23() const { return ___searchLabel_23; }
	inline UILabel_t1795115428 ** get_address_of_searchLabel_23() { return &___searchLabel_23; }
	inline void set_searchLabel_23(UILabel_t1795115428 * value)
	{
		___searchLabel_23 = value;
		Il2CppCodeGenWriteBarrier(&___searchLabel_23, value);
	}

	inline static int32_t get_offset_of_searchButton_24() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___searchButton_24)); }
	inline Transform_t3275118058 * get_searchButton_24() const { return ___searchButton_24; }
	inline Transform_t3275118058 ** get_address_of_searchButton_24() { return &___searchButton_24; }
	inline void set_searchButton_24(Transform_t3275118058 * value)
	{
		___searchButton_24 = value;
		Il2CppCodeGenWriteBarrier(&___searchButton_24, value);
	}

	inline static int32_t get_offset_of_searchDivider_25() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___searchDivider_25)); }
	inline UISprite_t603616735 * get_searchDivider_25() const { return ___searchDivider_25; }
	inline UISprite_t603616735 ** get_address_of_searchDivider_25() { return &___searchDivider_25; }
	inline void set_searchDivider_25(UISprite_t603616735 * value)
	{
		___searchDivider_25 = value;
		Il2CppCodeGenWriteBarrier(&___searchDivider_25, value);
	}

	inline static int32_t get_offset_of_friendsNoResultsLabel_26() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___friendsNoResultsLabel_26)); }
	inline UILabel_t1795115428 * get_friendsNoResultsLabel_26() const { return ___friendsNoResultsLabel_26; }
	inline UILabel_t1795115428 ** get_address_of_friendsNoResultsLabel_26() { return &___friendsNoResultsLabel_26; }
	inline void set_friendsNoResultsLabel_26(UILabel_t1795115428 * value)
	{
		___friendsNoResultsLabel_26 = value;
		Il2CppCodeGenWriteBarrier(&___friendsNoResultsLabel_26, value);
	}

	inline static int32_t get_offset_of_globalNoResultsLabel_27() { return static_cast<int32_t>(offsetof(AttackSearchUIScaler_t3747490330, ___globalNoResultsLabel_27)); }
	inline UILabel_t1795115428 * get_globalNoResultsLabel_27() const { return ___globalNoResultsLabel_27; }
	inline UILabel_t1795115428 ** get_address_of_globalNoResultsLabel_27() { return &___globalNoResultsLabel_27; }
	inline void set_globalNoResultsLabel_27(UILabel_t1795115428 * value)
	{
		___globalNoResultsLabel_27 = value;
		Il2CppCodeGenWriteBarrier(&___globalNoResultsLabel_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
