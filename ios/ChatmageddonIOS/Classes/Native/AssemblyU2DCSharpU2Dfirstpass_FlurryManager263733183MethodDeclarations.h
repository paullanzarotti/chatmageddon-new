﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryManager
struct FlurryManager_t263733183;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FlurryManager::.cctor()
extern "C"  void FlurryManager__cctor_m574876491 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::.ctor()
extern "C"  void FlurryManager__ctor_m3796847712 (FlurryManager_t263733183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::add_spaceDidDismissEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_add_spaceDidDismissEvent_m193397769 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::remove_spaceDidDismissEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_remove_spaceDidDismissEvent_m1952293360 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::add_spaceWillLeaveApplicationEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_add_spaceWillLeaveApplicationEvent_m1750420973 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::remove_spaceWillLeaveApplicationEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_remove_spaceWillLeaveApplicationEvent_m1048204148 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::add_spaceDidFailToRenderEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_add_spaceDidFailToRenderEvent_m3663017140 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::remove_spaceDidFailToRenderEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_remove_spaceDidFailToRenderEvent_m2828004875 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::add_spaceDidFailToReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_add_spaceDidFailToReceiveAdEvent_m2967052326 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::remove_spaceDidFailToReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_remove_spaceDidFailToReceiveAdEvent_m1710913771 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::add_spaceDidReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_add_spaceDidReceiveAdEvent_m372506117 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::remove_spaceDidReceiveAdEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_remove_spaceDidReceiveAdEvent_m260480060 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::add_videoDidFinishEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_add_videoDidFinishEvent_m3875502969 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::remove_videoDidFinishEvent(System.Action`1<System.String>)
extern "C"  void FlurryManager_remove_videoDidFinishEvent_m2723584206 (Il2CppObject * __this /* static, unused */, Action_1_t1831019615 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::spaceDidDismiss(System.String)
extern "C"  void FlurryManager_spaceDidDismiss_m3000744373 (FlurryManager_t263733183 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::spaceWillLeaveApplication(System.String)
extern "C"  void FlurryManager_spaceWillLeaveApplication_m3670469145 (FlurryManager_t263733183 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::spaceDidFailToRender(System.String)
extern "C"  void FlurryManager_spaceDidFailToRender_m3176173822 (FlurryManager_t263733183 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::spaceDidFailToReceiveAd(System.String)
extern "C"  void FlurryManager_spaceDidFailToReceiveAd_m2975145152 (FlurryManager_t263733183 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::spaceDidReceiveAd(System.String)
extern "C"  void FlurryManager_spaceDidReceiveAd_m2091851409 (FlurryManager_t263733183 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryManager::videoDidFinish(System.String)
extern "C"  void FlurryManager_videoDidFinish_m614953393 (FlurryManager_t263733183 * __this, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
