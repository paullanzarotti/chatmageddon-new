﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OuterPhoneNumberButton
struct OuterPhoneNumberButton_t2854054552;

#include "codegen/il2cpp-codegen.h"

// System.Void OuterPhoneNumberButton::.ctor()
extern "C"  void OuterPhoneNumberButton__ctor_m1799236955 (OuterPhoneNumberButton_t2854054552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OuterPhoneNumberButton::SetupButton()
extern "C"  void OuterPhoneNumberButton_SetupButton_m1991504594 (OuterPhoneNumberButton_t2854054552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
