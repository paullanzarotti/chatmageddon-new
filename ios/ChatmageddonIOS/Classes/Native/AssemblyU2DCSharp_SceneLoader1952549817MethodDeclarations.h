﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneLoader
struct SceneLoader_t1952549817;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Scene4200804392.h"

// System.Void SceneLoader::.ctor()
extern "C"  void SceneLoader__ctor_m1457847494 (SceneLoader_t1952549817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::Start()
extern "C"  void SceneLoader_Start_m1230249790 (SceneLoader_t1952549817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::OnTweenFinished()
extern "C"  void SceneLoader_OnTweenFinished_m338310088 (SceneLoader_t1952549817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::LoadScene(Scene)
extern "C"  void SceneLoader_LoadScene_m1584385456 (SceneLoader_t1952549817 * __this, int32_t ___scene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::UnloadScene(Scene)
extern "C"  void SceneLoader_UnloadScene_m840660233 (SceneLoader_t1952549817 * __this, int32_t ___scene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SceneLoader::wait()
extern "C"  Il2CppObject * SceneLoader_wait_m987995661 (SceneLoader_t1952549817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::LoadSceneUI(Scene,System.Boolean)
extern "C"  void SceneLoader_LoadSceneUI_m2888874853 (SceneLoader_t1952549817 * __this, int32_t ___scene0, bool ___unload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
