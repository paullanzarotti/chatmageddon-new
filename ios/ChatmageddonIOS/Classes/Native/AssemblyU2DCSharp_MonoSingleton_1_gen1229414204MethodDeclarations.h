﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<DeviceCameraManager>::.ctor()
#define MonoSingleton_1__ctor_m4231127954(__this, method) ((  void (*) (MonoSingleton_1_t1229414204 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<DeviceCameraManager>::Awake()
#define MonoSingleton_1_Awake_m563300223(__this, method) ((  void (*) (MonoSingleton_1_t1229414204 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<DeviceCameraManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2098788901(__this /* static, unused */, method) ((  DeviceCameraManager_t1478748484 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<DeviceCameraManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2639209025(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<DeviceCameraManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m897833244(__this, method) ((  void (*) (MonoSingleton_1_t1229414204 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<DeviceCameraManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m934492796(__this, method) ((  void (*) (MonoSingleton_1_t1229414204 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<DeviceCameraManager>::.cctor()
#define MonoSingleton_1__cctor_m2859538613(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
