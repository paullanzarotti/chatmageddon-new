﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangePositionAndZoomEventsExample
struct ChangePositionAndZoomEventsExample_t1027651508;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangePositionAndZoomEventsExample::.ctor()
extern "C"  void ChangePositionAndZoomEventsExample__ctor_m2185903127 (ChangePositionAndZoomEventsExample_t1027651508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePositionAndZoomEventsExample::OnChangePosition()
extern "C"  void ChangePositionAndZoomEventsExample_OnChangePosition_m1709417161 (ChangePositionAndZoomEventsExample_t1027651508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePositionAndZoomEventsExample::OnChangeZoom()
extern "C"  void ChangePositionAndZoomEventsExample_OnChangeZoom_m3114322323 (ChangePositionAndZoomEventsExample_t1027651508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangePositionAndZoomEventsExample::Start()
extern "C"  void ChangePositionAndZoomEventsExample_Start_m3547936691 (ChangePositionAndZoomEventsExample_t1027651508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
