﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PlayerProfileNavScreen>
struct List_1_t2029789391;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1564519065.h"
#include "AssemblyU2DCSharp_PlayerProfileNavScreen2660668259.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayerProfileNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3426328962_gshared (Enumerator_t1564519065 * __this, List_1_t2029789391 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3426328962(__this, ___l0, method) ((  void (*) (Enumerator_t1564519065 *, List_1_t2029789391 *, const MethodInfo*))Enumerator__ctor_m3426328962_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayerProfileNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2062090480_gshared (Enumerator_t1564519065 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2062090480(__this, method) ((  void (*) (Enumerator_t1564519065 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2062090480_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayerProfileNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m558014918_gshared (Enumerator_t1564519065 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m558014918(__this, method) ((  Il2CppObject * (*) (Enumerator_t1564519065 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m558014918_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayerProfileNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m589093851_gshared (Enumerator_t1564519065 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m589093851(__this, method) ((  void (*) (Enumerator_t1564519065 *, const MethodInfo*))Enumerator_Dispose_m589093851_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayerProfileNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m517847084_gshared (Enumerator_t1564519065 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m517847084(__this, method) ((  void (*) (Enumerator_t1564519065 *, const MethodInfo*))Enumerator_VerifyState_m517847084_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayerProfileNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3194597232_gshared (Enumerator_t1564519065 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3194597232(__this, method) ((  bool (*) (Enumerator_t1564519065 *, const MethodInfo*))Enumerator_MoveNext_m3194597232_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayerProfileNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2477734879_gshared (Enumerator_t1564519065 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2477734879(__this, method) ((  int32_t (*) (Enumerator_t1564519065 *, const MethodInfo*))Enumerator_get_Current_m2477734879_gshared)(__this, method)
