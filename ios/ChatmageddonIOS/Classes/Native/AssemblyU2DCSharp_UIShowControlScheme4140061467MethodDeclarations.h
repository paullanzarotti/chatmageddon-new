﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIShowControlScheme
struct UIShowControlScheme_t4140061467;

#include "codegen/il2cpp-codegen.h"

// System.Void UIShowControlScheme::.ctor()
extern "C"  void UIShowControlScheme__ctor_m647218818 (UIShowControlScheme_t4140061467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIShowControlScheme::OnEnable()
extern "C"  void UIShowControlScheme_OnEnable_m1747708214 (UIShowControlScheme_t4140061467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIShowControlScheme::OnDisable()
extern "C"  void UIShowControlScheme_OnDisable_m2583083047 (UIShowControlScheme_t4140061467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIShowControlScheme::OnScheme()
extern "C"  void UIShowControlScheme_OnScheme_m362298002 (UIShowControlScheme_t4140061467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
