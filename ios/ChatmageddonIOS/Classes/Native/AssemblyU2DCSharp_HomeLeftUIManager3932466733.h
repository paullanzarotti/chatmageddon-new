﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MapButtonOptions
struct MapButtonOptions_t2133642156;
// TweenScale
struct TweenScale_t2697902175;
// ShieldProgressBar
struct ShieldProgressBar_t2832359661;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3683132453.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeLeftUIManager
struct  HomeLeftUIManager_t3932466733  : public MonoSingleton_1_t3683132453
{
public:
	// MapButtonOptions HomeLeftUIManager::mapOptions
	MapButtonOptions_t2133642156 * ___mapOptions_3;
	// TweenScale HomeLeftUIManager::uiTween
	TweenScale_t2697902175 * ___uiTween_4;
	// ShieldProgressBar HomeLeftUIManager::missileShieldBar
	ShieldProgressBar_t2832359661 * ___missileShieldBar_5;
	// ShieldProgressBar HomeLeftUIManager::mineShieldBar
	ShieldProgressBar_t2832359661 * ___mineShieldBar_6;
	// UnityEngine.Color HomeLeftUIManager::activeStateColour
	Color_t2020392075  ___activeStateColour_7;
	// UnityEngine.Color HomeLeftUIManager::inactiveStateColour
	Color_t2020392075  ___inactiveStateColour_8;
	// System.Boolean HomeLeftUIManager::tabClosing
	bool ___tabClosing_9;

public:
	inline static int32_t get_offset_of_mapOptions_3() { return static_cast<int32_t>(offsetof(HomeLeftUIManager_t3932466733, ___mapOptions_3)); }
	inline MapButtonOptions_t2133642156 * get_mapOptions_3() const { return ___mapOptions_3; }
	inline MapButtonOptions_t2133642156 ** get_address_of_mapOptions_3() { return &___mapOptions_3; }
	inline void set_mapOptions_3(MapButtonOptions_t2133642156 * value)
	{
		___mapOptions_3 = value;
		Il2CppCodeGenWriteBarrier(&___mapOptions_3, value);
	}

	inline static int32_t get_offset_of_uiTween_4() { return static_cast<int32_t>(offsetof(HomeLeftUIManager_t3932466733, ___uiTween_4)); }
	inline TweenScale_t2697902175 * get_uiTween_4() const { return ___uiTween_4; }
	inline TweenScale_t2697902175 ** get_address_of_uiTween_4() { return &___uiTween_4; }
	inline void set_uiTween_4(TweenScale_t2697902175 * value)
	{
		___uiTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___uiTween_4, value);
	}

	inline static int32_t get_offset_of_missileShieldBar_5() { return static_cast<int32_t>(offsetof(HomeLeftUIManager_t3932466733, ___missileShieldBar_5)); }
	inline ShieldProgressBar_t2832359661 * get_missileShieldBar_5() const { return ___missileShieldBar_5; }
	inline ShieldProgressBar_t2832359661 ** get_address_of_missileShieldBar_5() { return &___missileShieldBar_5; }
	inline void set_missileShieldBar_5(ShieldProgressBar_t2832359661 * value)
	{
		___missileShieldBar_5 = value;
		Il2CppCodeGenWriteBarrier(&___missileShieldBar_5, value);
	}

	inline static int32_t get_offset_of_mineShieldBar_6() { return static_cast<int32_t>(offsetof(HomeLeftUIManager_t3932466733, ___mineShieldBar_6)); }
	inline ShieldProgressBar_t2832359661 * get_mineShieldBar_6() const { return ___mineShieldBar_6; }
	inline ShieldProgressBar_t2832359661 ** get_address_of_mineShieldBar_6() { return &___mineShieldBar_6; }
	inline void set_mineShieldBar_6(ShieldProgressBar_t2832359661 * value)
	{
		___mineShieldBar_6 = value;
		Il2CppCodeGenWriteBarrier(&___mineShieldBar_6, value);
	}

	inline static int32_t get_offset_of_activeStateColour_7() { return static_cast<int32_t>(offsetof(HomeLeftUIManager_t3932466733, ___activeStateColour_7)); }
	inline Color_t2020392075  get_activeStateColour_7() const { return ___activeStateColour_7; }
	inline Color_t2020392075 * get_address_of_activeStateColour_7() { return &___activeStateColour_7; }
	inline void set_activeStateColour_7(Color_t2020392075  value)
	{
		___activeStateColour_7 = value;
	}

	inline static int32_t get_offset_of_inactiveStateColour_8() { return static_cast<int32_t>(offsetof(HomeLeftUIManager_t3932466733, ___inactiveStateColour_8)); }
	inline Color_t2020392075  get_inactiveStateColour_8() const { return ___inactiveStateColour_8; }
	inline Color_t2020392075 * get_address_of_inactiveStateColour_8() { return &___inactiveStateColour_8; }
	inline void set_inactiveStateColour_8(Color_t2020392075  value)
	{
		___inactiveStateColour_8 = value;
	}

	inline static int32_t get_offset_of_tabClosing_9() { return static_cast<int32_t>(offsetof(HomeLeftUIManager_t3932466733, ___tabClosing_9)); }
	inline bool get_tabClosing_9() const { return ___tabClosing_9; }
	inline bool* get_address_of_tabClosing_9() { return &___tabClosing_9; }
	inline void set_tabClosing_9(bool value)
	{
		___tabClosing_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
