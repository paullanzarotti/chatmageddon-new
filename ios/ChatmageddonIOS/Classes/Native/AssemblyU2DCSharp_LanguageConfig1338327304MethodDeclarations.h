﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LanguageConfig
struct LanguageConfig_t1338327304;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;
// LanguageModel
struct LanguageModel_t2428340347;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_LanguageModel2428340347.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"

// System.Void LanguageConfig::.ctor(System.Collections.Hashtable)
extern "C"  void LanguageConfig__ctor_m4071907855 (LanguageConfig_t1338327304 * __this, Hashtable_t909839986 * ___langTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LanguageConfig::GetLanguageString(System.String)
extern "C"  String_t* LanguageConfig_GetLanguageString_m2719223449 (LanguageConfig_t1338327304 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LanguageConfig::GetLanguageStringUsingModel(System.String,LanguageModel)
extern "C"  String_t* LanguageConfig_GetLanguageStringUsingModel_m2336875389 (LanguageConfig_t1338327304 * __this, String_t* ___key0, LanguageModel_t2428340347 * ___model1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LanguageConfig::RegexReplaceCallback(System.Text.RegularExpressions.Match,LanguageModel)
extern "C"  String_t* LanguageConfig_RegexReplaceCallback_m3945844937 (Il2CppObject * __this /* static, unused */, Match_t3164245899 * ___match0, LanguageModel_t2428340347 * ___model1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
