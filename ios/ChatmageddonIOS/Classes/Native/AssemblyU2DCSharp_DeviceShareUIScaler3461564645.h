﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceShareUIScaler
struct  DeviceShareUIScaler_t3461564645  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite DeviceShareUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider DeviceShareUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UISprite DeviceShareUIScaler::emailButton
	UISprite_t603616735 * ___emailButton_16;
	// UnityEngine.BoxCollider DeviceShareUIScaler::emailCollider
	BoxCollider_t22920061 * ___emailCollider_17;
	// UISprite DeviceShareUIScaler::divider3
	UISprite_t603616735 * ___divider3_18;
	// UISprite DeviceShareUIScaler::smsButton
	UISprite_t603616735 * ___smsButton_19;
	// UnityEngine.BoxCollider DeviceShareUIScaler::smsCollider
	BoxCollider_t22920061 * ___smsCollider_20;
	// UISprite DeviceShareUIScaler::divider2
	UISprite_t603616735 * ___divider2_21;
	// UISprite DeviceShareUIScaler::facebookButton
	UISprite_t603616735 * ___facebookButton_22;
	// UnityEngine.BoxCollider DeviceShareUIScaler::facebookCollider
	BoxCollider_t22920061 * ___facebookCollider_23;
	// UISprite DeviceShareUIScaler::divider1
	UISprite_t603616735 * ___divider1_24;
	// UISprite DeviceShareUIScaler::twitterButton
	UISprite_t603616735 * ___twitterButton_25;
	// UnityEngine.BoxCollider DeviceShareUIScaler::tiwtterCollider
	BoxCollider_t22920061 * ___tiwtterCollider_26;
	// UISprite DeviceShareUIScaler::cancelButton
	UISprite_t603616735 * ___cancelButton_27;
	// UnityEngine.BoxCollider DeviceShareUIScaler::cancelCollider
	BoxCollider_t22920061 * ___cancelCollider_28;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_emailButton_16() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___emailButton_16)); }
	inline UISprite_t603616735 * get_emailButton_16() const { return ___emailButton_16; }
	inline UISprite_t603616735 ** get_address_of_emailButton_16() { return &___emailButton_16; }
	inline void set_emailButton_16(UISprite_t603616735 * value)
	{
		___emailButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___emailButton_16, value);
	}

	inline static int32_t get_offset_of_emailCollider_17() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___emailCollider_17)); }
	inline BoxCollider_t22920061 * get_emailCollider_17() const { return ___emailCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_emailCollider_17() { return &___emailCollider_17; }
	inline void set_emailCollider_17(BoxCollider_t22920061 * value)
	{
		___emailCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___emailCollider_17, value);
	}

	inline static int32_t get_offset_of_divider3_18() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___divider3_18)); }
	inline UISprite_t603616735 * get_divider3_18() const { return ___divider3_18; }
	inline UISprite_t603616735 ** get_address_of_divider3_18() { return &___divider3_18; }
	inline void set_divider3_18(UISprite_t603616735 * value)
	{
		___divider3_18 = value;
		Il2CppCodeGenWriteBarrier(&___divider3_18, value);
	}

	inline static int32_t get_offset_of_smsButton_19() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___smsButton_19)); }
	inline UISprite_t603616735 * get_smsButton_19() const { return ___smsButton_19; }
	inline UISprite_t603616735 ** get_address_of_smsButton_19() { return &___smsButton_19; }
	inline void set_smsButton_19(UISprite_t603616735 * value)
	{
		___smsButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___smsButton_19, value);
	}

	inline static int32_t get_offset_of_smsCollider_20() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___smsCollider_20)); }
	inline BoxCollider_t22920061 * get_smsCollider_20() const { return ___smsCollider_20; }
	inline BoxCollider_t22920061 ** get_address_of_smsCollider_20() { return &___smsCollider_20; }
	inline void set_smsCollider_20(BoxCollider_t22920061 * value)
	{
		___smsCollider_20 = value;
		Il2CppCodeGenWriteBarrier(&___smsCollider_20, value);
	}

	inline static int32_t get_offset_of_divider2_21() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___divider2_21)); }
	inline UISprite_t603616735 * get_divider2_21() const { return ___divider2_21; }
	inline UISprite_t603616735 ** get_address_of_divider2_21() { return &___divider2_21; }
	inline void set_divider2_21(UISprite_t603616735 * value)
	{
		___divider2_21 = value;
		Il2CppCodeGenWriteBarrier(&___divider2_21, value);
	}

	inline static int32_t get_offset_of_facebookButton_22() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___facebookButton_22)); }
	inline UISprite_t603616735 * get_facebookButton_22() const { return ___facebookButton_22; }
	inline UISprite_t603616735 ** get_address_of_facebookButton_22() { return &___facebookButton_22; }
	inline void set_facebookButton_22(UISprite_t603616735 * value)
	{
		___facebookButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___facebookButton_22, value);
	}

	inline static int32_t get_offset_of_facebookCollider_23() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___facebookCollider_23)); }
	inline BoxCollider_t22920061 * get_facebookCollider_23() const { return ___facebookCollider_23; }
	inline BoxCollider_t22920061 ** get_address_of_facebookCollider_23() { return &___facebookCollider_23; }
	inline void set_facebookCollider_23(BoxCollider_t22920061 * value)
	{
		___facebookCollider_23 = value;
		Il2CppCodeGenWriteBarrier(&___facebookCollider_23, value);
	}

	inline static int32_t get_offset_of_divider1_24() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___divider1_24)); }
	inline UISprite_t603616735 * get_divider1_24() const { return ___divider1_24; }
	inline UISprite_t603616735 ** get_address_of_divider1_24() { return &___divider1_24; }
	inline void set_divider1_24(UISprite_t603616735 * value)
	{
		___divider1_24 = value;
		Il2CppCodeGenWriteBarrier(&___divider1_24, value);
	}

	inline static int32_t get_offset_of_twitterButton_25() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___twitterButton_25)); }
	inline UISprite_t603616735 * get_twitterButton_25() const { return ___twitterButton_25; }
	inline UISprite_t603616735 ** get_address_of_twitterButton_25() { return &___twitterButton_25; }
	inline void set_twitterButton_25(UISprite_t603616735 * value)
	{
		___twitterButton_25 = value;
		Il2CppCodeGenWriteBarrier(&___twitterButton_25, value);
	}

	inline static int32_t get_offset_of_tiwtterCollider_26() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___tiwtterCollider_26)); }
	inline BoxCollider_t22920061 * get_tiwtterCollider_26() const { return ___tiwtterCollider_26; }
	inline BoxCollider_t22920061 ** get_address_of_tiwtterCollider_26() { return &___tiwtterCollider_26; }
	inline void set_tiwtterCollider_26(BoxCollider_t22920061 * value)
	{
		___tiwtterCollider_26 = value;
		Il2CppCodeGenWriteBarrier(&___tiwtterCollider_26, value);
	}

	inline static int32_t get_offset_of_cancelButton_27() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___cancelButton_27)); }
	inline UISprite_t603616735 * get_cancelButton_27() const { return ___cancelButton_27; }
	inline UISprite_t603616735 ** get_address_of_cancelButton_27() { return &___cancelButton_27; }
	inline void set_cancelButton_27(UISprite_t603616735 * value)
	{
		___cancelButton_27 = value;
		Il2CppCodeGenWriteBarrier(&___cancelButton_27, value);
	}

	inline static int32_t get_offset_of_cancelCollider_28() { return static_cast<int32_t>(offsetof(DeviceShareUIScaler_t3461564645, ___cancelCollider_28)); }
	inline BoxCollider_t22920061 * get_cancelCollider_28() const { return ___cancelCollider_28; }
	inline BoxCollider_t22920061 ** get_address_of_cancelCollider_28() { return &___cancelCollider_28; }
	inline void set_cancelCollider_28(BoxCollider_t22920061 * value)
	{
		___cancelCollider_28 = value;
		Il2CppCodeGenWriteBarrier(&___cancelCollider_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
