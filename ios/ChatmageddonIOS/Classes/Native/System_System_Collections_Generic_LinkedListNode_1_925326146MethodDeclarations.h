﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_11585555208MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<System.String>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m2505599670(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t925326146 *, LinkedList_1_t2333928462 *, String_t*, const MethodInfo*))LinkedListNode_1__ctor_m231316848_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.String>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m2577927876(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t925326146 *, LinkedList_1_t2333928462 *, String_t*, LinkedListNode_1_t925326146 *, LinkedListNode_1_t925326146 *, const MethodInfo*))LinkedListNode_1__ctor_m127450634_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.String>::Detach()
#define LinkedListNode_1_Detach_m704416016(__this, method) ((  void (*) (LinkedListNode_1_t925326146 *, const MethodInfo*))LinkedListNode_1_Detach_m3110429670_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.String>::SelfReference(System.Collections.Generic.LinkedList`1<T>)
#define LinkedListNode_1_SelfReference_m3651820575(__this, ___list0, method) ((  void (*) (LinkedListNode_1_t925326146 *, LinkedList_1_t2333928462 *, const MethodInfo*))LinkedListNode_1_SelfReference_m1137067051_gshared)(__this, ___list0, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.String>::InsertBetween(System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedList`1<T>)
#define LinkedListNode_1_InsertBetween_m1879476425(__this, ___previousNode0, ___nextNode1, ___list2, method) ((  void (*) (LinkedListNode_1_t925326146 *, LinkedListNode_1_t925326146 *, LinkedListNode_1_t925326146 *, LinkedList_1_t2333928462 *, const MethodInfo*))LinkedListNode_1_InsertBetween_m2212538821_gshared)(__this, ___previousNode0, ___nextNode1, ___list2, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.String>::get_List()
#define LinkedListNode_1_get_List_m1425421018(__this, method) ((  LinkedList_1_t2333928462 * (*) (LinkedListNode_1_t925326146 *, const MethodInfo*))LinkedListNode_1_get_List_m2861065156_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.String>::get_Previous()
#define LinkedListNode_1_get_Previous_m1457257075(__this, method) ((  LinkedListNode_1_t925326146 * (*) (LinkedListNode_1_t925326146 *, const MethodInfo*))LinkedListNode_1_get_Previous_m1837352519_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.String>::get_Value()
#define LinkedListNode_1_get_Value_m3039471100(__this, method) ((  String_t* (*) (LinkedListNode_1_t925326146 *, const MethodInfo*))LinkedListNode_1_get_Value_m201900116_gshared)(__this, method)
