﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookFriendsResult
struct FacebookFriendsResult_t459691178;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookFriendsResult::.ctor()
extern "C"  void FacebookFriendsResult__ctor_m479468878 (FacebookFriendsResult_t459691178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
