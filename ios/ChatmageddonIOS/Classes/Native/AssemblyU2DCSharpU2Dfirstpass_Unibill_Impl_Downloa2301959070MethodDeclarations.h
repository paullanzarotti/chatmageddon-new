﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.DownloadManager/<monitorDownloads>c__Iterator1
struct U3CmonitorDownloadsU3Ec__Iterator1_t2301959070;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Unibill.Impl.DownloadManager/<monitorDownloads>c__Iterator1::.ctor()
extern "C"  void U3CmonitorDownloadsU3Ec__Iterator1__ctor_m234538729 (U3CmonitorDownloadsU3Ec__Iterator1_t2301959070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.DownloadManager/<monitorDownloads>c__Iterator1::MoveNext()
extern "C"  bool U3CmonitorDownloadsU3Ec__Iterator1_MoveNext_m3601136367 (U3CmonitorDownloadsU3Ec__Iterator1_t2301959070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.DownloadManager/<monitorDownloads>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CmonitorDownloadsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1630599939 (U3CmonitorDownloadsU3Ec__Iterator1_t2301959070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.DownloadManager/<monitorDownloads>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CmonitorDownloadsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m895686123 (U3CmonitorDownloadsU3Ec__Iterator1_t2301959070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<monitorDownloads>c__Iterator1::Dispose()
extern "C"  void U3CmonitorDownloadsU3Ec__Iterator1_Dispose_m959748506 (U3CmonitorDownloadsU3Ec__Iterator1_t2301959070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<monitorDownloads>c__Iterator1::Reset()
extern "C"  void U3CmonitorDownloadsU3Ec__Iterator1_Reset_m1836433420 (U3CmonitorDownloadsU3Ec__Iterator1_t2301959070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
