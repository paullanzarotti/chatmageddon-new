﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineResultsModal
struct MineResultsModal_t4210040424;
// ResultModalUI
struct ResultModalUI_t969511824;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResultModalUI969511824.h"

// System.Void MineResultsModal::.ctor()
extern "C"  void MineResultsModal__ctor_m3965476537 (MineResultsModal_t4210040424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineResultsModal::LoadResultData(ResultModalUI)
extern "C"  void MineResultsModal_LoadResultData_m614588136 (MineResultsModal_t4210040424 * __this, ResultModalUI_t969511824 * ___resultModal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineResultsModal::SetMineDefused()
extern "C"  void MineResultsModal_SetMineDefused_m2948695492 (MineResultsModal_t4210040424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineResultsModal::SetMineExploded()
extern "C"  void MineResultsModal_SetMineExploded_m3171808151 (MineResultsModal_t4210040424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineResultsModal::SetMineCleared()
extern "C"  void MineResultsModal_SetMineCleared_m1010578586 (MineResultsModal_t4210040424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
