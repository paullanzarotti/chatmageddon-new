﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsBuildingsNodeData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4109192408(__this, ___l0, method) ((  void (*) (Enumerator_t375715237 *, List_1_t840985563 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsBuildingsNodeData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m970995274(__this, method) ((  void (*) (Enumerator_t375715237 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<OnlineMapsBuildingsNodeData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1972911990(__this, method) ((  Il2CppObject * (*) (Enumerator_t375715237 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsBuildingsNodeData>::Dispose()
#define Enumerator_Dispose_m953008443(__this, method) ((  void (*) (Enumerator_t375715237 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsBuildingsNodeData>::VerifyState()
#define Enumerator_VerifyState_m3139420010(__this, method) ((  void (*) (Enumerator_t375715237 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<OnlineMapsBuildingsNodeData>::MoveNext()
#define Enumerator_MoveNext_m72952026(__this, method) ((  bool (*) (Enumerator_t375715237 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<OnlineMapsBuildingsNodeData>::get_Current()
#define Enumerator_get_Current_m2715410331(__this, method) ((  OnlineMapsBuildingsNodeData_t1471864431 * (*) (Enumerator_t375715237 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
