﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefendHoldButton
struct DefendHoldButton_t1338853911;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Light
struct Light_t494725636;
// TweenRotation
struct TweenRotation_t1747194511;

#include "AssemblyU2DCSharp_DefendMiniGameController1844356491.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoldMiniGame
struct  HoldMiniGame_t1783545348  : public DefendMiniGameController_t1844356491
{
public:
	// DefendHoldButton HoldMiniGame::holdbutton
	DefendHoldButton_t1338853911 * ___holdbutton_6;
	// UnityEngine.GameObject HoldMiniGame::modelParent
	GameObject_t1756533147 * ___modelParent_7;
	// UnityEngine.GameObject HoldMiniGame::currentModel
	GameObject_t1756533147 * ___currentModel_8;
	// UnityEngine.Light HoldMiniGame::itemLight
	Light_t494725636 * ___itemLight_9;
	// TweenRotation HoldMiniGame::leftMarker
	TweenRotation_t1747194511 * ___leftMarker_10;
	// TweenRotation HoldMiniGame::rightMarker
	TweenRotation_t1747194511 * ___rightMarker_11;

public:
	inline static int32_t get_offset_of_holdbutton_6() { return static_cast<int32_t>(offsetof(HoldMiniGame_t1783545348, ___holdbutton_6)); }
	inline DefendHoldButton_t1338853911 * get_holdbutton_6() const { return ___holdbutton_6; }
	inline DefendHoldButton_t1338853911 ** get_address_of_holdbutton_6() { return &___holdbutton_6; }
	inline void set_holdbutton_6(DefendHoldButton_t1338853911 * value)
	{
		___holdbutton_6 = value;
		Il2CppCodeGenWriteBarrier(&___holdbutton_6, value);
	}

	inline static int32_t get_offset_of_modelParent_7() { return static_cast<int32_t>(offsetof(HoldMiniGame_t1783545348, ___modelParent_7)); }
	inline GameObject_t1756533147 * get_modelParent_7() const { return ___modelParent_7; }
	inline GameObject_t1756533147 ** get_address_of_modelParent_7() { return &___modelParent_7; }
	inline void set_modelParent_7(GameObject_t1756533147 * value)
	{
		___modelParent_7 = value;
		Il2CppCodeGenWriteBarrier(&___modelParent_7, value);
	}

	inline static int32_t get_offset_of_currentModel_8() { return static_cast<int32_t>(offsetof(HoldMiniGame_t1783545348, ___currentModel_8)); }
	inline GameObject_t1756533147 * get_currentModel_8() const { return ___currentModel_8; }
	inline GameObject_t1756533147 ** get_address_of_currentModel_8() { return &___currentModel_8; }
	inline void set_currentModel_8(GameObject_t1756533147 * value)
	{
		___currentModel_8 = value;
		Il2CppCodeGenWriteBarrier(&___currentModel_8, value);
	}

	inline static int32_t get_offset_of_itemLight_9() { return static_cast<int32_t>(offsetof(HoldMiniGame_t1783545348, ___itemLight_9)); }
	inline Light_t494725636 * get_itemLight_9() const { return ___itemLight_9; }
	inline Light_t494725636 ** get_address_of_itemLight_9() { return &___itemLight_9; }
	inline void set_itemLight_9(Light_t494725636 * value)
	{
		___itemLight_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_9, value);
	}

	inline static int32_t get_offset_of_leftMarker_10() { return static_cast<int32_t>(offsetof(HoldMiniGame_t1783545348, ___leftMarker_10)); }
	inline TweenRotation_t1747194511 * get_leftMarker_10() const { return ___leftMarker_10; }
	inline TweenRotation_t1747194511 ** get_address_of_leftMarker_10() { return &___leftMarker_10; }
	inline void set_leftMarker_10(TweenRotation_t1747194511 * value)
	{
		___leftMarker_10 = value;
		Il2CppCodeGenWriteBarrier(&___leftMarker_10, value);
	}

	inline static int32_t get_offset_of_rightMarker_11() { return static_cast<int32_t>(offsetof(HoldMiniGame_t1783545348, ___rightMarker_11)); }
	inline TweenRotation_t1747194511 * get_rightMarker_11() const { return ___rightMarker_11; }
	inline TweenRotation_t1747194511 ** get_address_of_rightMarker_11() { return &___rightMarker_11; }
	inline void set_rightMarker_11(TweenRotation_t1747194511 * value)
	{
		___rightMarker_11 = value;
		Il2CppCodeGenWriteBarrier(&___rightMarker_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
