﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemSwipeController
struct ItemSwipeController_t3011021799;
// ItemSwipeController/OnLeftComplete
struct OnLeftComplete_t1529246307;
// ItemSwipeController/OnRightComplete
struct OnRightComplete_t1521737374;
// ItemSwipeController/OnHideComplete
struct OnHideComplete_t4141700430;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ItemSwipeController_OnLeftComple1529246307.h"
#include "AssemblyU2DCSharp_ItemSwipeController_OnRightCompl1521737374.h"
#include "AssemblyU2DCSharp_ItemSwipeController_OnHideComple4141700430.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void ItemSwipeController::.ctor()
extern "C"  void ItemSwipeController__ctor_m2453172310 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::add_onLeftComplete(ItemSwipeController/OnLeftComplete)
extern "C"  void ItemSwipeController_add_onLeftComplete_m707029858 (ItemSwipeController_t3011021799 * __this, OnLeftComplete_t1529246307 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::remove_onLeftComplete(ItemSwipeController/OnLeftComplete)
extern "C"  void ItemSwipeController_remove_onLeftComplete_m3844448645 (ItemSwipeController_t3011021799 * __this, OnLeftComplete_t1529246307 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::add_onRightComplete(ItemSwipeController/OnRightComplete)
extern "C"  void ItemSwipeController_add_onRightComplete_m3925304384 (ItemSwipeController_t3011021799 * __this, OnRightComplete_t1521737374 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::remove_onRightComplete(ItemSwipeController/OnRightComplete)
extern "C"  void ItemSwipeController_remove_onRightComplete_m1192221081 (ItemSwipeController_t3011021799 * __this, OnRightComplete_t1521737374 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::add_onHideComplete(ItemSwipeController/OnHideComplete)
extern "C"  void ItemSwipeController_add_onHideComplete_m1551485024 (ItemSwipeController_t3011021799 * __this, OnHideComplete_t4141700430 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::remove_onHideComplete(ItemSwipeController/OnHideComplete)
extern "C"  void ItemSwipeController_remove_onHideComplete_m698615141 (ItemSwipeController_t3011021799 * __this, OnHideComplete_t4141700430 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::TweenItemDefault()
extern "C"  void ItemSwipeController_TweenItemDefault_m382522953 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::OnTweenDefaultComplete()
extern "C"  void ItemSwipeController_OnTweenDefaultComplete_m1317096862 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::TweenItemCustom(UnityEngine.Vector3)
extern "C"  void ItemSwipeController_TweenItemCustom_m1313336866 (ItemSwipeController_t3011021799 * __this, Vector3_t2243707580  ___customPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::OnTweenCustomComplete()
extern "C"  void ItemSwipeController_OnTweenCustomComplete_m2309049382 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::TweenItemHide(System.Boolean,System.Single)
extern "C"  void ItemSwipeController_TweenItemHide_m1778621084 (ItemSwipeController_t3011021799 * __this, bool ___closing0, float ___customTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::OnTweenHideComplete()
extern "C"  void ItemSwipeController_OnTweenHideComplete_m3236706291 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::TweenItemLeft()
extern "C"  void ItemSwipeController_TweenItemLeft_m3436360233 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::OnTweenLeftComplete()
extern "C"  void ItemSwipeController_OnTweenLeftComplete_m1487776232 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::TweenItemRight()
extern "C"  void ItemSwipeController_TweenItemRight_m1035652490 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::OnTweenRightComplete()
extern "C"  void ItemSwipeController_OnTweenRightComplete_m1322019999 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::TweenItemDescription()
extern "C"  void ItemSwipeController_TweenItemDescription_m927242782 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController::DisplayItemModel()
extern "C"  void ItemSwipeController_DisplayItemModel_m1187384322 (ItemSwipeController_t3011021799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
