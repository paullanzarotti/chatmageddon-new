﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Flus1182037460.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp4151391442.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp2530143933.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp2282214205.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_ZlibE421852804.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Shar2666276944.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Inte3631556964.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Stat2290192584.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Adler6455690.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_ZlibS953065013.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib3383394762.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib3915258526.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib1899545627.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Zlib1670432928.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_Extensions957788964.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_Extensions_U2670832402.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_ExceptionHel2051805979.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeaderParser1994184764.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeaderValue822462144.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_HeartbeatMana895236645.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_KeyValuePair1715528642.h"
#include "AssemblyU2DCSharp_BestHTTP_Extensions_WWWAuthentic1921593050.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPFieldData605100868.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPFormBase1912072923.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPFormUsage2139743243.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPMultiPartForm2201706314.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_HTTPUrlEncodedFor2052977551.h"
#include "AssemblyU2DCSharp_BestHTTP_Forms_UnityForm1710299297.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPConnection2777749290.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPConnection_RetryCau2210488103.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPConnectionStates1509261476.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPManager2983460817.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPManager_U3CSendRequ3256281814.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPMethods178420096.h"
#include "AssemblyU2DCSharp_BestHTTP_SupportedProtocols1503488249.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPProtocolFactory3310674324.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPProxy2644053826.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRange1154458197.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequestStates63938943.h"
#include "AssemblyU2DCSharp_BestHTTP_OnRequestFinishedDelega3180754735.h"
#include "AssemblyU2DCSharp_BestHTTP_OnDownloadProgressDelega447146369.h"
#include "AssemblyU2DCSharp_BestHTTP_OnUploadProgressDelegat3063766470.h"
#include "AssemblyU2DCSharp_BestHTTP_OnBeforeRedirectionDeleg290558967.h"
#include "AssemblyU2DCSharp_BestHTTP_OnHeaderEnumerationDele3923304806.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest_U3CEnumerat3794713947.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest_U3CSendHead3710205996.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPUpdateDelegator1331403296.h"
#include "AssemblyU2DCSharp_BestHTTP_JSON_Json3015811848.h"
#include "AssemblyU2DCSharp_BestHTTP_Logger_DefaultLogger639890009.h"
#include "AssemblyU2DCSharp_BestHTTP_Logger_Loglevels4278436247.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Obje1310059589.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2640700633.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec3926133854.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec2314871951.h"
#include "AssemblyU2DCSharp_PlatformSupport_Collections_Spec1087955584.h"
#include "AssemblyU2DCSharp_BestHTTP_PlatformSupport_TcpClie4173427386.h"
#include "AssemblyU2DCSharp_BestHTTP_PlatformSupport_TcpClien742700320.h"
#include "AssemblyU2DCSharp_BestHTTP_PlatformSupport_TcpClie2124838760.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Generat986026348.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Stream3208800844.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Encoda3447851422.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Encoda3471733113.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Excepti782059264.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1InputS2767940265.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Null813671072.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Object564283626.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1OctetS1486532927.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Output1632877576.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Parsing912539532.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Sequence54253652.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Sequen2695581227.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Set2420705913.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Set_As1422684887.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1TaggedO990853098.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_Asn1Tags847429018.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerGenerat1021114948.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerOctetSt2849212572.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSequenc3637877645.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSequenc1792950261.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSetGener493026546.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSetPars3373663962.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerTaggedO3812856247.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerApplica3974373185.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerApplica1533940994.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerOctetStr478504981.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerOutputSt868356256.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSequence24248732.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerSet2720783323.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Asn1_BerTaggedOb282133382.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (FlushType_t1182037460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3600[6] = 
{
	FlushType_t1182037460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (CompressionLevel_t4151391442)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3601[15] = 
{
	CompressionLevel_t4151391442::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (CompressionStrategy_t2530143933)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3602[4] = 
{
	CompressionStrategy_t2530143933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (CompressionMode_t2282214205)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3603[3] = 
{
	CompressionMode_t2282214205::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (ZlibException_t421852804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (SharedUtils_t2666276944), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (InternalConstants_t3631556964), -1, sizeof(InternalConstants_t3631556964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3606[10] = 
{
	InternalConstants_t3631556964_StaticFields::get_offset_of_MAX_BITS_0(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_BL_CODES_1(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_D_CODES_2(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_LITERALS_3(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_LENGTH_CODES_4(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_L_CODES_5(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_MAX_BL_BITS_6(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_REP_3_6_7(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_REPZ_3_10_8(),
	InternalConstants_t3631556964_StaticFields::get_offset_of_REPZ_11_138_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (StaticTree_t2290192584), -1, sizeof(StaticTree_t2290192584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3607[10] = 
{
	StaticTree_t2290192584_StaticFields::get_offset_of_lengthAndLiteralsTreeCodes_0(),
	StaticTree_t2290192584_StaticFields::get_offset_of_distTreeCodes_1(),
	StaticTree_t2290192584_StaticFields::get_offset_of_Literals_2(),
	StaticTree_t2290192584_StaticFields::get_offset_of_Distances_3(),
	StaticTree_t2290192584_StaticFields::get_offset_of_BitLengths_4(),
	StaticTree_t2290192584::get_offset_of_treeCodes_5(),
	StaticTree_t2290192584::get_offset_of_extraBits_6(),
	StaticTree_t2290192584::get_offset_of_extraBase_7(),
	StaticTree_t2290192584::get_offset_of_elems_8(),
	StaticTree_t2290192584::get_offset_of_maxLength_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (Adler_t6455690), -1, sizeof(Adler_t6455690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3608[2] = 
{
	Adler_t6455690_StaticFields::get_offset_of_BASE_0(),
	Adler_t6455690_StaticFields::get_offset_of_NMAX_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (ZlibStreamFlavor_t953065013)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3609[4] = 
{
	ZlibStreamFlavor_t953065013::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (ZlibBaseStream_t3383394762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[19] = 
{
	ZlibBaseStream_t3383394762::get_offset_of__z_2(),
	ZlibBaseStream_t3383394762::get_offset_of__streamMode_3(),
	ZlibBaseStream_t3383394762::get_offset_of__flushMode_4(),
	ZlibBaseStream_t3383394762::get_offset_of__flavor_5(),
	ZlibBaseStream_t3383394762::get_offset_of__compressionMode_6(),
	ZlibBaseStream_t3383394762::get_offset_of__level_7(),
	ZlibBaseStream_t3383394762::get_offset_of__leaveOpen_8(),
	ZlibBaseStream_t3383394762::get_offset_of__workingBuffer_9(),
	ZlibBaseStream_t3383394762::get_offset_of__bufferSize_10(),
	ZlibBaseStream_t3383394762::get_offset_of_windowBitsMax_11(),
	ZlibBaseStream_t3383394762::get_offset_of__buf1_12(),
	ZlibBaseStream_t3383394762::get_offset_of__stream_13(),
	ZlibBaseStream_t3383394762::get_offset_of_Strategy_14(),
	ZlibBaseStream_t3383394762::get_offset_of_crc_15(),
	ZlibBaseStream_t3383394762::get_offset_of__GzipFileName_16(),
	ZlibBaseStream_t3383394762::get_offset_of__GzipComment_17(),
	ZlibBaseStream_t3383394762::get_offset_of__GzipMtime_18(),
	ZlibBaseStream_t3383394762::get_offset_of__gzipHeaderByteCount_19(),
	ZlibBaseStream_t3383394762::get_offset_of_nomoreinput_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (StreamMode_t3915258526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3611[4] = 
{
	StreamMode_t3915258526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (ZlibCodec_t1899545627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3612[15] = 
{
	ZlibCodec_t1899545627::get_offset_of_InputBuffer_0(),
	ZlibCodec_t1899545627::get_offset_of_NextIn_1(),
	ZlibCodec_t1899545627::get_offset_of_AvailableBytesIn_2(),
	ZlibCodec_t1899545627::get_offset_of_TotalBytesIn_3(),
	ZlibCodec_t1899545627::get_offset_of_OutputBuffer_4(),
	ZlibCodec_t1899545627::get_offset_of_NextOut_5(),
	ZlibCodec_t1899545627::get_offset_of_AvailableBytesOut_6(),
	ZlibCodec_t1899545627::get_offset_of_TotalBytesOut_7(),
	ZlibCodec_t1899545627::get_offset_of_Message_8(),
	ZlibCodec_t1899545627::get_offset_of_dstate_9(),
	ZlibCodec_t1899545627::get_offset_of_istate_10(),
	ZlibCodec_t1899545627::get_offset_of__Adler32_11(),
	ZlibCodec_t1899545627::get_offset_of_CompressLevel_12(),
	ZlibCodec_t1899545627::get_offset_of_WindowBits_13(),
	ZlibCodec_t1899545627::get_offset_of_Strategy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (ZlibConstants_t1670432928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3613[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (Extensions_t957788964), -1, sizeof(Extensions_t957788964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3614[3] = 
{
	Extensions_t957788964_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	Extensions_t957788964_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	Extensions_t957788964_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (U3CReadU3Ec__AnonStorey0_t2670832402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[1] = 
{
	U3CReadU3Ec__AnonStorey0_t2670832402::get_offset_of_block_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (ExceptionHelper_t2051805979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (HeaderParser_t1994184764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (HeaderValue_t822462144), -1, sizeof(HeaderValue_t822462144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3618[4] = 
{
	HeaderValue_t822462144::get_offset_of_U3CKeyU3Ek__BackingField_0(),
	HeaderValue_t822462144::get_offset_of_U3CValueU3Ek__BackingField_1(),
	HeaderValue_t822462144::get_offset_of_U3COptionsU3Ek__BackingField_2(),
	HeaderValue_t822462144_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (HeartbeatManager_t895236645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3620[3] = 
{
	HeartbeatManager_t895236645::get_offset_of_Heartbeats_0(),
	HeartbeatManager_t895236645::get_offset_of_UpdateArray_1(),
	HeartbeatManager_t895236645::get_offset_of_LastUpdate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (KeyValuePairList_t1715528642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[1] = 
{
	KeyValuePairList_t1715528642::get_offset_of_U3CValuesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (WWWAuthenticateHeaderParser_t1921593050), -1, sizeof(WWWAuthenticateHeaderParser_t1921593050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3622[1] = 
{
	WWWAuthenticateHeaderParser_t1921593050_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (HTTPFieldData_t605100868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[6] = 
{
	HTTPFieldData_t605100868::get_offset_of_U3CNameU3Ek__BackingField_0(),
	HTTPFieldData_t605100868::get_offset_of_U3CFileNameU3Ek__BackingField_1(),
	HTTPFieldData_t605100868::get_offset_of_U3CMimeTypeU3Ek__BackingField_2(),
	HTTPFieldData_t605100868::get_offset_of_U3CEncodingU3Ek__BackingField_3(),
	HTTPFieldData_t605100868::get_offset_of_U3CTextU3Ek__BackingField_4(),
	HTTPFieldData_t605100868::get_offset_of_U3CBinaryU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (HTTPFormBase_t1912072923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[5] = 
{
	0,
	HTTPFormBase_t1912072923::get_offset_of_U3CFieldsU3Ek__BackingField_1(),
	HTTPFormBase_t1912072923::get_offset_of_U3CIsChangedU3Ek__BackingField_2(),
	HTTPFormBase_t1912072923::get_offset_of_U3CHasBinaryU3Ek__BackingField_3(),
	HTTPFormBase_t1912072923::get_offset_of_U3CHasLongValueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (HTTPFormUsage_t2139743243)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3625[5] = 
{
	HTTPFormUsage_t2139743243::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (HTTPMultiPartForm_t2201706314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[2] = 
{
	HTTPMultiPartForm_t2201706314::get_offset_of_Boundary_5(),
	HTTPMultiPartForm_t2201706314::get_offset_of_CachedData_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (HTTPUrlEncodedForm_t2052977551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[1] = 
{
	HTTPUrlEncodedForm_t2052977551::get_offset_of_CachedData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (UnityForm_t1710299297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[1] = 
{
	UnityForm_t1710299297::get_offset_of_U3CFormU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (HTTPConnection_t2777749290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[2] = 
{
	HTTPConnection_t2777749290::get_offset_of_Client_11(),
	HTTPConnection_t2777749290::get_offset_of_Stream_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (RetryCauses_t2210488103)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3630[5] = 
{
	RetryCauses_t2210488103::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (HTTPConnectionStates_t1509261476)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3631[11] = 
{
	HTTPConnectionStates_t1509261476::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (HTTPManager_t2983460817), -1, sizeof(HTTPManager_t2983460817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3632[27] = 
{
	HTTPManager_t2983460817_StaticFields::get_offset_of_maxConnectionPerServer_0(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CKeepAliveDefaultValueU3Ek__BackingField_1(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CIsCachingDisabledU3Ek__BackingField_2(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CMaxConnectionIdleTimeU3Ek__BackingField_3(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_4(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CCookieJarSizeU3Ek__BackingField_5(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CEnablePrivateBrowsingU3Ek__BackingField_6(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_7(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CRequestTimeoutU3Ek__BackingField_8(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CRootCacheFolderProviderU3Ek__BackingField_9(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CProxyU3Ek__BackingField_10(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_heartbeats_11(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_logger_12(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CDefaultCertificateVerifyerU3Ek__BackingField_13(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CDefaultClientCredentialsProviderU3Ek__BackingField_14(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CUseAlternateSSLDefaultValueU3Ek__BackingField_15(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CMaxPathLengthU3Ek__BackingField_16(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_Connections_17(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_ActiveConnections_18(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_FreeConnections_19(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_RecycledConnections_20(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_RequestQueue_21(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_IsCallingCallbacks_22(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_Locker_23(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_24(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_25(),
	HTTPManager_t2983460817_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (U3CSendRequestImplU3Ec__AnonStorey0_t3256281814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[1] = 
{
	U3CSendRequestImplU3Ec__AnonStorey0_t3256281814::get_offset_of_conn_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (HTTPMethods_t178420096)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3634[7] = 
{
	HTTPMethods_t178420096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (SupportedProtocols_t1503488249)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3635[5] = 
{
	SupportedProtocols_t1503488249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (HTTPProtocolFactory_t3310674324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (HTTPProxy_t2644053826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3637[5] = 
{
	HTTPProxy_t2644053826::get_offset_of_U3CAddressU3Ek__BackingField_0(),
	HTTPProxy_t2644053826::get_offset_of_U3CCredentialsU3Ek__BackingField_1(),
	HTTPProxy_t2644053826::get_offset_of_U3CIsTransparentU3Ek__BackingField_2(),
	HTTPProxy_t2644053826::get_offset_of_U3CSendWholeUriU3Ek__BackingField_3(),
	HTTPProxy_t2644053826::get_offset_of_U3CNonTransparentForHTTPSU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (HTTPRange_t1154458197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3638[4] = 
{
	HTTPRange_t1154458197::get_offset_of_U3CFirstBytePosU3Ek__BackingField_0(),
	HTTPRange_t1154458197::get_offset_of_U3CLastBytePosU3Ek__BackingField_1(),
	HTTPRange_t1154458197::get_offset_of_U3CContentLengthU3Ek__BackingField_2(),
	HTTPRange_t1154458197::get_offset_of_U3CIsValidU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (HTTPRequestStates_t63938943)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3639[9] = 
{
	HTTPRequestStates_t63938943::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (OnRequestFinishedDelegate_t3180754735), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (OnDownloadProgressDelegate_t447146369), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (OnUploadProgressDelegate_t3063766470), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (OnBeforeRedirectionDelegate_t290558967), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (OnHeaderEnumerationDelegate_t3923304806), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (HTTPRequest_t138485887), -1, sizeof(HTTPRequest_t138485887_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3645[51] = 
{
	HTTPRequest_t138485887_StaticFields::get_offset_of_EOL_0(),
	HTTPRequest_t138485887_StaticFields::get_offset_of_MethodNames_1(),
	HTTPRequest_t138485887_StaticFields::get_offset_of_UploadChunkSize_2(),
	HTTPRequest_t138485887::get_offset_of_U3CUriU3Ek__BackingField_3(),
	HTTPRequest_t138485887::get_offset_of_U3CMethodTypeU3Ek__BackingField_4(),
	HTTPRequest_t138485887::get_offset_of_U3CRawDataU3Ek__BackingField_5(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadStreamU3Ek__BackingField_6(),
	HTTPRequest_t138485887::get_offset_of_U3CDisposeUploadStreamU3Ek__BackingField_7(),
	HTTPRequest_t138485887::get_offset_of_U3CUseUploadStreamLengthU3Ek__BackingField_8(),
	HTTPRequest_t138485887::get_offset_of_OnUploadProgress_9(),
	HTTPRequest_t138485887::get_offset_of_U3CCallbackU3Ek__BackingField_10(),
	HTTPRequest_t138485887::get_offset_of_OnProgress_11(),
	HTTPRequest_t138485887::get_offset_of_OnUpgraded_12(),
	HTTPRequest_t138485887::get_offset_of_U3CDisableRetryU3Ek__BackingField_13(),
	HTTPRequest_t138485887::get_offset_of_U3CIsRedirectedU3Ek__BackingField_14(),
	HTTPRequest_t138485887::get_offset_of_U3CRedirectUriU3Ek__BackingField_15(),
	HTTPRequest_t138485887::get_offset_of_U3CResponseU3Ek__BackingField_16(),
	HTTPRequest_t138485887::get_offset_of_U3CProxyResponseU3Ek__BackingField_17(),
	HTTPRequest_t138485887::get_offset_of_U3CExceptionU3Ek__BackingField_18(),
	HTTPRequest_t138485887::get_offset_of_U3CTagU3Ek__BackingField_19(),
	HTTPRequest_t138485887::get_offset_of_U3CCredentialsU3Ek__BackingField_20(),
	HTTPRequest_t138485887::get_offset_of_U3CProxyU3Ek__BackingField_21(),
	HTTPRequest_t138485887::get_offset_of_U3CMaxRedirectsU3Ek__BackingField_22(),
	HTTPRequest_t138485887::get_offset_of_U3CUseAlternateSSLU3Ek__BackingField_23(),
	HTTPRequest_t138485887::get_offset_of_U3CIsCookiesEnabledU3Ek__BackingField_24(),
	HTTPRequest_t138485887::get_offset_of_customCookies_25(),
	HTTPRequest_t138485887::get_offset_of_U3CFormUsageU3Ek__BackingField_26(),
	HTTPRequest_t138485887::get_offset_of_U3CStateU3Ek__BackingField_27(),
	HTTPRequest_t138485887::get_offset_of_U3CRedirectCountU3Ek__BackingField_28(),
	HTTPRequest_t138485887::get_offset_of_CustomCertificationValidator_29(),
	HTTPRequest_t138485887::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_30(),
	HTTPRequest_t138485887::get_offset_of_U3CTimeoutU3Ek__BackingField_31(),
	HTTPRequest_t138485887::get_offset_of_U3CEnableTimoutForStreamingU3Ek__BackingField_32(),
	HTTPRequest_t138485887::get_offset_of_U3CPriorityU3Ek__BackingField_33(),
	HTTPRequest_t138485887::get_offset_of_U3CCustomCertificateVerifyerU3Ek__BackingField_34(),
	HTTPRequest_t138485887::get_offset_of_U3CCustomClientCredentialsProviderU3Ek__BackingField_35(),
	HTTPRequest_t138485887::get_offset_of_U3CProtocolHandlerU3Ek__BackingField_36(),
	HTTPRequest_t138485887::get_offset_of_onBeforeRedirection_37(),
	HTTPRequest_t138485887::get_offset_of_U3CDownloadedU3Ek__BackingField_38(),
	HTTPRequest_t138485887::get_offset_of_U3CDownloadLengthU3Ek__BackingField_39(),
	HTTPRequest_t138485887::get_offset_of_U3CDownloadProgressChangedU3Ek__BackingField_40(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadedU3Ek__BackingField_41(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadLengthU3Ek__BackingField_42(),
	HTTPRequest_t138485887::get_offset_of_U3CUploadProgressChangedU3Ek__BackingField_43(),
	HTTPRequest_t138485887::get_offset_of_isKeepAlive_44(),
	HTTPRequest_t138485887::get_offset_of_disableCache_45(),
	HTTPRequest_t138485887::get_offset_of_streamFragmentSize_46(),
	HTTPRequest_t138485887::get_offset_of_useStreaming_47(),
	HTTPRequest_t138485887::get_offset_of_U3CHeadersU3Ek__BackingField_48(),
	HTTPRequest_t138485887::get_offset_of_FieldCollector_49(),
	HTTPRequest_t138485887::get_offset_of_FormImpl_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (U3CEnumerateHeadersU3Ec__AnonStorey0_t3794713947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3646[1] = 
{
	U3CEnumerateHeadersU3Ec__AnonStorey0_t3794713947::get_offset_of_customCookie_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (U3CSendHeadersU3Ec__AnonStorey1_t3710205996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[1] = 
{
	U3CSendHeadersU3Ec__AnonStorey1_t3710205996::get_offset_of_stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (HTTPResponse_t62748825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[25] = 
{
	0,
	0,
	0,
	HTTPResponse_t62748825::get_offset_of_U3CVersionMajorU3Ek__BackingField_3(),
	HTTPResponse_t62748825::get_offset_of_U3CVersionMinorU3Ek__BackingField_4(),
	HTTPResponse_t62748825::get_offset_of_U3CStatusCodeU3Ek__BackingField_5(),
	HTTPResponse_t62748825::get_offset_of_U3CMessageU3Ek__BackingField_6(),
	HTTPResponse_t62748825::get_offset_of_U3CIsStreamedU3Ek__BackingField_7(),
	HTTPResponse_t62748825::get_offset_of_U3CIsStreamingFinishedU3Ek__BackingField_8(),
	HTTPResponse_t62748825::get_offset_of_U3CIsFromCacheU3Ek__BackingField_9(),
	HTTPResponse_t62748825::get_offset_of_U3CHeadersU3Ek__BackingField_10(),
	HTTPResponse_t62748825::get_offset_of_U3CDataU3Ek__BackingField_11(),
	HTTPResponse_t62748825::get_offset_of_U3CIsUpgradedU3Ek__BackingField_12(),
	HTTPResponse_t62748825::get_offset_of_U3CCookiesU3Ek__BackingField_13(),
	HTTPResponse_t62748825::get_offset_of_dataAsText_14(),
	HTTPResponse_t62748825::get_offset_of_texture_15(),
	HTTPResponse_t62748825::get_offset_of_U3CIsClosedManuallyU3Ek__BackingField_16(),
	HTTPResponse_t62748825::get_offset_of_baseRequest_17(),
	HTTPResponse_t62748825::get_offset_of_Stream_18(),
	HTTPResponse_t62748825::get_offset_of_streamedFragments_19(),
	HTTPResponse_t62748825::get_offset_of_SyncRoot_20(),
	HTTPResponse_t62748825::get_offset_of_fragmentBuffer_21(),
	HTTPResponse_t62748825::get_offset_of_fragmentBufferDataLength_22(),
	HTTPResponse_t62748825::get_offset_of_cacheStream_23(),
	HTTPResponse_t62748825::get_offset_of_allFragmentSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (HTTPUpdateDelegator_t1331403296), -1, sizeof(HTTPUpdateDelegator_t1331403296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3650[6] = 
{
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_2(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CIsCreatedU3Ek__BackingField_3(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CIsThreadedU3Ek__BackingField_4(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CIsThreadRunningU3Ek__BackingField_5(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_U3CThreadFrequencyInMSU3Ek__BackingField_6(),
	HTTPUpdateDelegator_t1331403296_StaticFields::get_offset_of_IsSetupCalled_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (Json_t3015811848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (DefaultLogger_t639890009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[6] = 
{
	DefaultLogger_t639890009::get_offset_of_U3CLevelU3Ek__BackingField_0(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatVerboseU3Ek__BackingField_1(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatInfoU3Ek__BackingField_2(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatWarnU3Ek__BackingField_3(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatErrU3Ek__BackingField_4(),
	DefaultLogger_t639890009::get_offset_of_U3CFormatExU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (Loglevels_t4278436247)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3653[7] = 
{
	Loglevels_t4278436247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (NotifyCollectionChangedEventHandler_t1310059589), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3657[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (NotifyCollectionChangedAction_t2640700633)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3658[6] = 
{
	NotifyCollectionChangedAction_t2640700633::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (NotifyCollectionChangedEventArgs_t3926133854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[5] = 
{
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__action_1(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__newItems_2(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__oldItems_3(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__newStartingIndex_4(),
	NotifyCollectionChangedEventArgs_t3926133854::get_offset_of__oldStartingIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (NotifyCollectionChangedEventHandler_t2314871951), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (ReadOnlyList_t1087955584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[1] = 
{
	ReadOnlyList_t1087955584::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (TcpClient_t4173427386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3662[12] = 
{
	TcpClient_t4173427386::get_offset_of_stream_0(),
	TcpClient_t4173427386::get_offset_of_active_1(),
	TcpClient_t4173427386::get_offset_of_client_2(),
	TcpClient_t4173427386::get_offset_of_disposed_3(),
	TcpClient_t4173427386::get_offset_of_values_4(),
	TcpClient_t4173427386::get_offset_of_recv_timeout_5(),
	TcpClient_t4173427386::get_offset_of_send_timeout_6(),
	TcpClient_t4173427386::get_offset_of_recv_buffer_size_7(),
	TcpClient_t4173427386::get_offset_of_send_buffer_size_8(),
	TcpClient_t4173427386::get_offset_of_linger_state_9(),
	TcpClient_t4173427386::get_offset_of_no_delay_10(),
	TcpClient_t4173427386::get_offset_of_U3CConnectTimeoutU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (Properties_t742700320)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3663[7] = 
{
	Properties_t742700320::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (U3CConnectU3Ec__AnonStorey0_t2124838760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[1] = 
{
	U3CConnectU3Ec__AnonStorey0_t2124838760::get_offset_of_mre_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (Asn1Generator_t986026348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[1] = 
{
	Asn1Generator_t986026348::get_offset_of__out_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (Asn1StreamParser_t3208800844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[3] = 
{
	Asn1StreamParser_t3208800844::get_offset_of__in_0(),
	Asn1StreamParser_t3208800844::get_offset_of__limit_1(),
	Asn1StreamParser_t3208800844::get_offset_of_tmpBuffers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (Asn1Encodable_t3447851422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (Asn1EncodableVector_t3471733113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3672[1] = 
{
	Asn1EncodableVector_t3471733113::get_offset_of_v_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (Asn1Exception_t782059264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (Asn1InputStream_t2767940265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[2] = 
{
	Asn1InputStream_t2767940265::get_offset_of_limit_3(),
	Asn1InputStream_t2767940265::get_offset_of_tmpBuffers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (Asn1Null_t813671072), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (Asn1Object_t564283626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (Asn1OctetString_t1486532927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[1] = 
{
	Asn1OctetString_t1486532927::get_offset_of_str_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (Asn1OutputStream_t1632877576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (Asn1ParsingException_t912539532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (Asn1Sequence_t54253652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[1] = 
{
	Asn1Sequence_t54253652::get_offset_of_seq_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (Asn1SequenceParserImpl_t2695581227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[3] = 
{
	Asn1SequenceParserImpl_t2695581227::get_offset_of_outer_0(),
	Asn1SequenceParserImpl_t2695581227::get_offset_of_max_1(),
	Asn1SequenceParserImpl_t2695581227::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (Asn1Set_t2420705913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[1] = 
{
	Asn1Set_t2420705913::get_offset_of__set_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (Asn1SetParserImpl_t1422684887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3683[3] = 
{
	Asn1SetParserImpl_t1422684887::get_offset_of_outer_0(),
	Asn1SetParserImpl_t1422684887::get_offset_of_max_1(),
	Asn1SetParserImpl_t1422684887::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (Asn1TaggedObject_t990853098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3684[3] = 
{
	Asn1TaggedObject_t990853098::get_offset_of_tagNo_2(),
	Asn1TaggedObject_t990853098::get_offset_of_explicitly_3(),
	Asn1TaggedObject_t990853098::get_offset_of_obj_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (Asn1Tags_t847429018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3685[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (BerGenerator_t1021114948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3686[3] = 
{
	BerGenerator_t1021114948::get_offset_of__tagged_1(),
	BerGenerator_t1021114948::get_offset_of__isExplicit_2(),
	BerGenerator_t1021114948::get_offset_of__tagNo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (BerOctetStringParser_t2849212572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[1] = 
{
	BerOctetStringParser_t2849212572::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (BerSequenceGenerator_t3637877645), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (BerSequenceParser_t1792950261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[1] = 
{
	BerSequenceParser_t1792950261::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (BerSetGenerator_t493026546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (BerSetParser_t3373663962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[1] = 
{
	BerSetParser_t3373663962::get_offset_of__parser_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (BerTaggedObjectParser_t3812856247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[3] = 
{
	BerTaggedObjectParser_t3812856247::get_offset_of__constructed_0(),
	BerTaggedObjectParser_t3812856247::get_offset_of__tagNumber_1(),
	BerTaggedObjectParser_t3812856247::get_offset_of__parser_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (BerApplicationSpecific_t3974373185), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (BerApplicationSpecificParser_t1533940994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[2] = 
{
	BerApplicationSpecificParser_t1533940994::get_offset_of_tag_0(),
	BerApplicationSpecificParser_t1533940994::get_offset_of_parser_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (BerOctetString_t478504981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[2] = 
{
	0,
	BerOctetString_t478504981::get_offset_of_octs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (BerOutputStream_t868356256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (BerSequence_t24248732), -1, sizeof(BerSequence_t24248732_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3697[1] = 
{
	BerSequence_t24248732_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (BerSet_t2720783323), -1, sizeof(BerSet_t2720783323_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3698[1] = 
{
	BerSet_t2720783323_StaticFields::get_offset_of_Empty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (BerTaggedObject_t282133382), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
