﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindAutocompleteResult>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4007516693(__this, ___l0, method) ((  void (*) (Enumerator_t1523498896 *, List_1_t1988769222 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindAutocompleteResult>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4149654213(__this, method) ((  void (*) (Enumerator_t1523498896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<OnlineMapsFindAutocompleteResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m961911889(__this, method) ((  Il2CppObject * (*) (Enumerator_t1523498896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindAutocompleteResult>::Dispose()
#define Enumerator_Dispose_m259730254(__this, method) ((  void (*) (Enumerator_t1523498896 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnlineMapsFindAutocompleteResult>::VerifyState()
#define Enumerator_VerifyState_m520720055(__this, method) ((  void (*) (Enumerator_t1523498896 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<OnlineMapsFindAutocompleteResult>::MoveNext()
#define Enumerator_MoveNext_m3596375229(__this, method) ((  bool (*) (Enumerator_t1523498896 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<OnlineMapsFindAutocompleteResult>::get_Current()
#define Enumerator_get_Current_m2886380534(__this, method) ((  OnlineMapsFindAutocompleteResult_t2619648090 * (*) (Enumerator_t1523498896 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
