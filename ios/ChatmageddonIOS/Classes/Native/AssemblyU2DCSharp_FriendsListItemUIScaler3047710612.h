﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsListItemUIScaler
struct  FriendsListItemUIScaler_t3047710612  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite FriendsListItemUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider FriendsListItemUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UnityEngine.Transform FriendsListItemUIScaler::profilePicture
	Transform_t3275118058 * ___profilePicture_16;
	// UnityEngine.Transform FriendsListItemUIScaler::nameLabel
	Transform_t3275118058 * ___nameLabel_17;
	// UnityEngine.Transform FriendsListItemUIScaler::chatButton
	Transform_t3275118058 * ___chatButton_18;
	// UnityEngine.Transform FriendsListItemUIScaler::friendButton
	Transform_t3275118058 * ___friendButton_19;
	// UnityEngine.Transform FriendsListItemUIScaler::attackButton
	Transform_t3275118058 * ___attackButton_20;
	// UnityEngine.Transform FriendsListItemUIScaler::acceptButton
	Transform_t3275118058 * ___acceptButton_21;
	// UnityEngine.Transform FriendsListItemUIScaler::declineButton
	Transform_t3275118058 * ___declineButton_22;
	// UnityEngine.Transform FriendsListItemUIScaler::actionLabel
	Transform_t3275118058 * ___actionLabel_23;
	// UnityEngine.Transform FriendsListItemUIScaler::timeCalculator
	Transform_t3275118058 * ___timeCalculator_24;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_profilePicture_16() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___profilePicture_16)); }
	inline Transform_t3275118058 * get_profilePicture_16() const { return ___profilePicture_16; }
	inline Transform_t3275118058 ** get_address_of_profilePicture_16() { return &___profilePicture_16; }
	inline void set_profilePicture_16(Transform_t3275118058 * value)
	{
		___profilePicture_16 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_16, value);
	}

	inline static int32_t get_offset_of_nameLabel_17() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___nameLabel_17)); }
	inline Transform_t3275118058 * get_nameLabel_17() const { return ___nameLabel_17; }
	inline Transform_t3275118058 ** get_address_of_nameLabel_17() { return &___nameLabel_17; }
	inline void set_nameLabel_17(Transform_t3275118058 * value)
	{
		___nameLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_17, value);
	}

	inline static int32_t get_offset_of_chatButton_18() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___chatButton_18)); }
	inline Transform_t3275118058 * get_chatButton_18() const { return ___chatButton_18; }
	inline Transform_t3275118058 ** get_address_of_chatButton_18() { return &___chatButton_18; }
	inline void set_chatButton_18(Transform_t3275118058 * value)
	{
		___chatButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___chatButton_18, value);
	}

	inline static int32_t get_offset_of_friendButton_19() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___friendButton_19)); }
	inline Transform_t3275118058 * get_friendButton_19() const { return ___friendButton_19; }
	inline Transform_t3275118058 ** get_address_of_friendButton_19() { return &___friendButton_19; }
	inline void set_friendButton_19(Transform_t3275118058 * value)
	{
		___friendButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___friendButton_19, value);
	}

	inline static int32_t get_offset_of_attackButton_20() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___attackButton_20)); }
	inline Transform_t3275118058 * get_attackButton_20() const { return ___attackButton_20; }
	inline Transform_t3275118058 ** get_address_of_attackButton_20() { return &___attackButton_20; }
	inline void set_attackButton_20(Transform_t3275118058 * value)
	{
		___attackButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___attackButton_20, value);
	}

	inline static int32_t get_offset_of_acceptButton_21() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___acceptButton_21)); }
	inline Transform_t3275118058 * get_acceptButton_21() const { return ___acceptButton_21; }
	inline Transform_t3275118058 ** get_address_of_acceptButton_21() { return &___acceptButton_21; }
	inline void set_acceptButton_21(Transform_t3275118058 * value)
	{
		___acceptButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___acceptButton_21, value);
	}

	inline static int32_t get_offset_of_declineButton_22() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___declineButton_22)); }
	inline Transform_t3275118058 * get_declineButton_22() const { return ___declineButton_22; }
	inline Transform_t3275118058 ** get_address_of_declineButton_22() { return &___declineButton_22; }
	inline void set_declineButton_22(Transform_t3275118058 * value)
	{
		___declineButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___declineButton_22, value);
	}

	inline static int32_t get_offset_of_actionLabel_23() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___actionLabel_23)); }
	inline Transform_t3275118058 * get_actionLabel_23() const { return ___actionLabel_23; }
	inline Transform_t3275118058 ** get_address_of_actionLabel_23() { return &___actionLabel_23; }
	inline void set_actionLabel_23(Transform_t3275118058 * value)
	{
		___actionLabel_23 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_23, value);
	}

	inline static int32_t get_offset_of_timeCalculator_24() { return static_cast<int32_t>(offsetof(FriendsListItemUIScaler_t3047710612, ___timeCalculator_24)); }
	inline Transform_t3275118058 * get_timeCalculator_24() const { return ___timeCalculator_24; }
	inline Transform_t3275118058 ** get_address_of_timeCalculator_24() { return &___timeCalculator_24; }
	inline void set_timeCalculator_24(Transform_t3275118058 * value)
	{
		___timeCalculator_24 = value;
		Il2CppCodeGenWriteBarrier(&___timeCalculator_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
