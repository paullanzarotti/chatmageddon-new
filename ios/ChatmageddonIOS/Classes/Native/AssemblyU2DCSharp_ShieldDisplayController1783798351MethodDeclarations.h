﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldDisplayController
struct ShieldDisplayController_t1783798351;
// Shield
struct Shield_t3327121081;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ShieldDisplayController::.ctor()
extern "C"  void ShieldDisplayController__ctor_m2421123484 (ShieldDisplayController_t1783798351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShieldDisplayController::SetActiveShield(Shield,LaunchedMissile)
extern "C"  bool ShieldDisplayController_SetActiveShield_m638288380 (ShieldDisplayController_t1783798351 * __this, Shield_t3327121081 * ___activeShield0, LaunchedMissile_t1542515810 * ___activeMissile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShieldDisplayController::CheckIfMissileShielded(Shield,LaunchedMissile)
extern "C"  bool ShieldDisplayController_CheckIfMissileShielded_m1896545040 (ShieldDisplayController_t1783798351 * __this, Shield_t3327121081 * ___activeShield0, LaunchedMissile_t1542515810 * ___activeMissile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShieldDisplayController::CheckIfShieldOutlastsMissile()
extern "C"  bool ShieldDisplayController_CheckIfShieldOutlastsMissile_m2682482731 (ShieldDisplayController_t1783798351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldDisplayController::SetShieldIcon(System.String)
extern "C"  void ShieldDisplayController_SetShieldIcon_m3953461722 (ShieldDisplayController_t1783798351 * __this, String_t* ___internalName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
