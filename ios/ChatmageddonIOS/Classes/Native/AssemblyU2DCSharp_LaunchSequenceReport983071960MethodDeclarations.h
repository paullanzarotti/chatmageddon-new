﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchSequenceReport
struct LaunchSequenceReport_t983071960;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchSequenceReport::.ctor()
extern "C"  void LaunchSequenceReport__ctor_m3649857935 (LaunchSequenceReport_t983071960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchSequenceReport::PopulateSequenceInfo()
extern "C"  void LaunchSequenceReport_PopulateSequenceInfo_m1570184892 (LaunchSequenceReport_t983071960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
