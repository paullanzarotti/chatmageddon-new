﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.DataObjectMethodAttribute
struct DataObjectMethodAttribute_t1292727732;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_DataObjectMethodType1069349368.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.DataObjectMethodAttribute::.ctor(System.ComponentModel.DataObjectMethodType)
extern "C"  void DataObjectMethodAttribute__ctor_m3116089854 (DataObjectMethodAttribute_t1292727732 * __this, int32_t ___methodType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.DataObjectMethodAttribute::.ctor(System.ComponentModel.DataObjectMethodType,System.Boolean)
extern "C"  void DataObjectMethodAttribute__ctor_m3649213341 (DataObjectMethodAttribute_t1292727732 * __this, int32_t ___methodType0, bool ___isDefault1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.DataObjectMethodType System.ComponentModel.DataObjectMethodAttribute::get_MethodType()
extern "C"  int32_t DataObjectMethodAttribute_get_MethodType_m1024193431 (DataObjectMethodAttribute_t1292727732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectMethodAttribute::get_IsDefault()
extern "C"  bool DataObjectMethodAttribute_get_IsDefault_m3891937509 (DataObjectMethodAttribute_t1292727732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectMethodAttribute::Match(System.Object)
extern "C"  bool DataObjectMethodAttribute_Match_m601795590 (DataObjectMethodAttribute_t1292727732 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.DataObjectMethodAttribute::Equals(System.Object)
extern "C"  bool DataObjectMethodAttribute_Equals_m2562450702 (DataObjectMethodAttribute_t1292727732 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.DataObjectMethodAttribute::GetHashCode()
extern "C"  int32_t DataObjectMethodAttribute_GetHashCode_m63819810 (DataObjectMethodAttribute_t1292727732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
