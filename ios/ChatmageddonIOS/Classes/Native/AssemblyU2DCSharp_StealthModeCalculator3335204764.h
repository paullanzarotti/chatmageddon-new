﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// StealthModeCalculator/OnZeroHit
struct OnZeroHit_t2015899103;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StealthModeCalculator
struct  StealthModeCalculator_t3335204764  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color StealthModeCalculator::positiveColour
	Color_t2020392075  ___positiveColour_2;
	// UnityEngine.Color StealthModeCalculator::negativeColour
	Color_t2020392075  ___negativeColour_3;
	// UILabel StealthModeCalculator::timeLabel
	UILabel_t1795115428 * ___timeLabel_4;
	// UnityEngine.Coroutine StealthModeCalculator::calcRoutine
	Coroutine_t2299508840 * ___calcRoutine_5;
	// StealthModeCalculator/OnZeroHit StealthModeCalculator::onZeroHit
	OnZeroHit_t2015899103 * ___onZeroHit_6;

public:
	inline static int32_t get_offset_of_positiveColour_2() { return static_cast<int32_t>(offsetof(StealthModeCalculator_t3335204764, ___positiveColour_2)); }
	inline Color_t2020392075  get_positiveColour_2() const { return ___positiveColour_2; }
	inline Color_t2020392075 * get_address_of_positiveColour_2() { return &___positiveColour_2; }
	inline void set_positiveColour_2(Color_t2020392075  value)
	{
		___positiveColour_2 = value;
	}

	inline static int32_t get_offset_of_negativeColour_3() { return static_cast<int32_t>(offsetof(StealthModeCalculator_t3335204764, ___negativeColour_3)); }
	inline Color_t2020392075  get_negativeColour_3() const { return ___negativeColour_3; }
	inline Color_t2020392075 * get_address_of_negativeColour_3() { return &___negativeColour_3; }
	inline void set_negativeColour_3(Color_t2020392075  value)
	{
		___negativeColour_3 = value;
	}

	inline static int32_t get_offset_of_timeLabel_4() { return static_cast<int32_t>(offsetof(StealthModeCalculator_t3335204764, ___timeLabel_4)); }
	inline UILabel_t1795115428 * get_timeLabel_4() const { return ___timeLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_4() { return &___timeLabel_4; }
	inline void set_timeLabel_4(UILabel_t1795115428 * value)
	{
		___timeLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_4, value);
	}

	inline static int32_t get_offset_of_calcRoutine_5() { return static_cast<int32_t>(offsetof(StealthModeCalculator_t3335204764, ___calcRoutine_5)); }
	inline Coroutine_t2299508840 * get_calcRoutine_5() const { return ___calcRoutine_5; }
	inline Coroutine_t2299508840 ** get_address_of_calcRoutine_5() { return &___calcRoutine_5; }
	inline void set_calcRoutine_5(Coroutine_t2299508840 * value)
	{
		___calcRoutine_5 = value;
		Il2CppCodeGenWriteBarrier(&___calcRoutine_5, value);
	}

	inline static int32_t get_offset_of_onZeroHit_6() { return static_cast<int32_t>(offsetof(StealthModeCalculator_t3335204764, ___onZeroHit_6)); }
	inline OnZeroHit_t2015899103 * get_onZeroHit_6() const { return ___onZeroHit_6; }
	inline OnZeroHit_t2015899103 ** get_address_of_onZeroHit_6() { return &___onZeroHit_6; }
	inline void set_onZeroHit_6(OnZeroHit_t2015899103 * value)
	{
		___onZeroHit_6 = value;
		Il2CppCodeGenWriteBarrier(&___onZeroHit_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
