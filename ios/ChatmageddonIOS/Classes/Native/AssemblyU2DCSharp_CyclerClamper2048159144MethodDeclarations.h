﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CyclerClamper
struct CyclerClamper_t2048159144;

#include "codegen/il2cpp-codegen.h"

// System.Void CyclerClamper::.ctor()
extern "C"  void CyclerClamper__ctor_m1252836449 (CyclerClamper_t2048159144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CyclerClamper::Awake()
extern "C"  void CyclerClamper_Awake_m4052492376 (CyclerClamper_t2048159144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CyclerClamper::Start()
extern "C"  void CyclerClamper_Start_m3049986713 (CyclerClamper_t2048159144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CyclerClamper::OnEnable()
extern "C"  void CyclerClamper_OnEnable_m2609515193 (CyclerClamper_t2048159144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CyclerClamper::OnDisable()
extern "C"  void CyclerClamper_OnDisable_m3728037112 (CyclerClamper_t2048159144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CyclerClamper::OnPickerUpdated()
extern "C"  void CyclerClamper_OnPickerUpdated_m2173195731 (CyclerClamper_t2048159144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
