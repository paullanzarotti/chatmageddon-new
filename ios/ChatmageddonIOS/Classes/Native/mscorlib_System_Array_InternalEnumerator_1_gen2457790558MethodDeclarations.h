﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2457790558.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"

// System.Void System.Array/InternalEnumerator`1<FriendHomeNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1886975895_gshared (InternalEnumerator_1_t2457790558 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1886975895(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2457790558 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1886975895_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FriendHomeNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2015716919_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2015716919(__this, method) ((  void (*) (InternalEnumerator_1_t2457790558 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2015716919_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FriendHomeNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2338409511_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2338409511(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2457790558 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2338409511_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FriendHomeNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4213171772_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4213171772(__this, method) ((  void (*) (InternalEnumerator_1_t2457790558 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4213171772_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FriendHomeNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2431540723_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2431540723(__this, method) ((  bool (*) (InternalEnumerator_1_t2457790558 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2431540723_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FriendHomeNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2074619856_gshared (InternalEnumerator_1_t2457790558 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2074619856(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2457790558 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2074619856_gshared)(__this, method)
