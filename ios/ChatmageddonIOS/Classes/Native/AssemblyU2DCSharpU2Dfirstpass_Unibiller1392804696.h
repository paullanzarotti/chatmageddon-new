﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Biller
struct Biller_t1615588570;
// Unibill.Impl.DownloadManager
struct DownloadManager_t32803451;
// System.Action`1<UnibillState>
struct Action_1_t4073934390;
// System.Action`1<PurchasableItem>
struct Action_1_t3765153281;
// System.Action`1<PurchaseEvent>
struct Action_1_t545353811;
// System.Action`2<System.String,System.IO.DirectoryInfo>
struct Action_2_t4139768145;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`2<System.String,System.Int32>
struct Action_2_t4277199140;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibiller
struct  Unibiller_t1392804696  : public Il2CppObject
{
public:

public:
};

struct Unibiller_t1392804696_StaticFields
{
public:
	// Unibill.Biller Unibiller::biller
	Biller_t1615588570 * ___biller_0;
	// Unibill.Impl.DownloadManager Unibiller::downloadManager
	DownloadManager_t32803451 * ___downloadManager_1;
	// System.Action`1<UnibillState> Unibiller::onBillerReady
	Action_1_t4073934390 * ___onBillerReady_2;
	// System.Action`1<PurchasableItem> Unibiller::onPurchaseCancelled
	Action_1_t3765153281 * ___onPurchaseCancelled_3;
	// System.Action`1<PurchaseEvent> Unibiller::onPurchaseCompleteEvent
	Action_1_t545353811 * ___onPurchaseCompleteEvent_4;
	// System.Action`1<PurchasableItem> Unibiller::onPurchaseComplete
	Action_1_t3765153281 * ___onPurchaseComplete_5;
	// System.Action`1<PurchasableItem> Unibiller::onPurchaseFailed
	Action_1_t3765153281 * ___onPurchaseFailed_6;
	// System.Action`1<PurchasableItem> Unibiller::onPurchaseDeferred
	Action_1_t3765153281 * ___onPurchaseDeferred_7;
	// System.Action`1<PurchasableItem> Unibiller::onPurchaseRefunded
	Action_1_t3765153281 * ___onPurchaseRefunded_8;
	// System.Action`2<System.String,System.IO.DirectoryInfo> Unibiller::onDownloadCompletedEvent
	Action_2_t4139768145 * ___onDownloadCompletedEvent_9;
	// System.Action`2<System.String,System.String> Unibiller::onDownloadCompletedEventString
	Action_2_t4234541925 * ___onDownloadCompletedEventString_10;
	// System.Action`2<System.String,System.Int32> Unibiller::onDownloadProgressedEvent
	Action_2_t4277199140 * ___onDownloadProgressedEvent_11;
	// System.Action`2<System.String,System.String> Unibiller::onDownloadFailedEvent
	Action_2_t4234541925 * ___onDownloadFailedEvent_12;
	// System.Action`1<System.Boolean> Unibiller::onTransactionsRestored
	Action_1_t3627374100 * ___onTransactionsRestored_13;
	// Unibill.Impl.DownloadManager Unibiller::DownloadManager
	DownloadManager_t32803451 * ___DownloadManager_14;
	// System.Action`1<PurchasableItem> Unibiller::<>f__mg$cache0
	Action_1_t3765153281 * ___U3CU3Ef__mgU24cache0_15;
	// System.Action`1<PurchaseEvent> Unibiller::<>f__mg$cache1
	Action_1_t545353811 * ___U3CU3Ef__mgU24cache1_16;
	// System.Action`1<PurchasableItem> Unibiller::<>f__mg$cache2
	Action_1_t3765153281 * ___U3CU3Ef__mgU24cache2_17;
	// System.Action`1<PurchasableItem> Unibiller::<>f__mg$cache3
	Action_1_t3765153281 * ___U3CU3Ef__mgU24cache3_18;
	// System.Action`1<PurchasableItem> Unibiller::<>f__mg$cache4
	Action_1_t3765153281 * ___U3CU3Ef__mgU24cache4_19;
	// System.Action`1<System.Boolean> Unibiller::<>f__mg$cache5
	Action_1_t3627374100 * ___U3CU3Ef__mgU24cache5_20;

public:
	inline static int32_t get_offset_of_biller_0() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___biller_0)); }
	inline Biller_t1615588570 * get_biller_0() const { return ___biller_0; }
	inline Biller_t1615588570 ** get_address_of_biller_0() { return &___biller_0; }
	inline void set_biller_0(Biller_t1615588570 * value)
	{
		___biller_0 = value;
		Il2CppCodeGenWriteBarrier(&___biller_0, value);
	}

	inline static int32_t get_offset_of_downloadManager_1() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___downloadManager_1)); }
	inline DownloadManager_t32803451 * get_downloadManager_1() const { return ___downloadManager_1; }
	inline DownloadManager_t32803451 ** get_address_of_downloadManager_1() { return &___downloadManager_1; }
	inline void set_downloadManager_1(DownloadManager_t32803451 * value)
	{
		___downloadManager_1 = value;
		Il2CppCodeGenWriteBarrier(&___downloadManager_1, value);
	}

	inline static int32_t get_offset_of_onBillerReady_2() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onBillerReady_2)); }
	inline Action_1_t4073934390 * get_onBillerReady_2() const { return ___onBillerReady_2; }
	inline Action_1_t4073934390 ** get_address_of_onBillerReady_2() { return &___onBillerReady_2; }
	inline void set_onBillerReady_2(Action_1_t4073934390 * value)
	{
		___onBillerReady_2 = value;
		Il2CppCodeGenWriteBarrier(&___onBillerReady_2, value);
	}

	inline static int32_t get_offset_of_onPurchaseCancelled_3() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onPurchaseCancelled_3)); }
	inline Action_1_t3765153281 * get_onPurchaseCancelled_3() const { return ___onPurchaseCancelled_3; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseCancelled_3() { return &___onPurchaseCancelled_3; }
	inline void set_onPurchaseCancelled_3(Action_1_t3765153281 * value)
	{
		___onPurchaseCancelled_3 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseCancelled_3, value);
	}

	inline static int32_t get_offset_of_onPurchaseCompleteEvent_4() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onPurchaseCompleteEvent_4)); }
	inline Action_1_t545353811 * get_onPurchaseCompleteEvent_4() const { return ___onPurchaseCompleteEvent_4; }
	inline Action_1_t545353811 ** get_address_of_onPurchaseCompleteEvent_4() { return &___onPurchaseCompleteEvent_4; }
	inline void set_onPurchaseCompleteEvent_4(Action_1_t545353811 * value)
	{
		___onPurchaseCompleteEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseCompleteEvent_4, value);
	}

	inline static int32_t get_offset_of_onPurchaseComplete_5() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onPurchaseComplete_5)); }
	inline Action_1_t3765153281 * get_onPurchaseComplete_5() const { return ___onPurchaseComplete_5; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseComplete_5() { return &___onPurchaseComplete_5; }
	inline void set_onPurchaseComplete_5(Action_1_t3765153281 * value)
	{
		___onPurchaseComplete_5 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseComplete_5, value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_6() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onPurchaseFailed_6)); }
	inline Action_1_t3765153281 * get_onPurchaseFailed_6() const { return ___onPurchaseFailed_6; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseFailed_6() { return &___onPurchaseFailed_6; }
	inline void set_onPurchaseFailed_6(Action_1_t3765153281 * value)
	{
		___onPurchaseFailed_6 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseFailed_6, value);
	}

	inline static int32_t get_offset_of_onPurchaseDeferred_7() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onPurchaseDeferred_7)); }
	inline Action_1_t3765153281 * get_onPurchaseDeferred_7() const { return ___onPurchaseDeferred_7; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseDeferred_7() { return &___onPurchaseDeferred_7; }
	inline void set_onPurchaseDeferred_7(Action_1_t3765153281 * value)
	{
		___onPurchaseDeferred_7 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseDeferred_7, value);
	}

	inline static int32_t get_offset_of_onPurchaseRefunded_8() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onPurchaseRefunded_8)); }
	inline Action_1_t3765153281 * get_onPurchaseRefunded_8() const { return ___onPurchaseRefunded_8; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseRefunded_8() { return &___onPurchaseRefunded_8; }
	inline void set_onPurchaseRefunded_8(Action_1_t3765153281 * value)
	{
		___onPurchaseRefunded_8 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseRefunded_8, value);
	}

	inline static int32_t get_offset_of_onDownloadCompletedEvent_9() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onDownloadCompletedEvent_9)); }
	inline Action_2_t4139768145 * get_onDownloadCompletedEvent_9() const { return ___onDownloadCompletedEvent_9; }
	inline Action_2_t4139768145 ** get_address_of_onDownloadCompletedEvent_9() { return &___onDownloadCompletedEvent_9; }
	inline void set_onDownloadCompletedEvent_9(Action_2_t4139768145 * value)
	{
		___onDownloadCompletedEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadCompletedEvent_9, value);
	}

	inline static int32_t get_offset_of_onDownloadCompletedEventString_10() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onDownloadCompletedEventString_10)); }
	inline Action_2_t4234541925 * get_onDownloadCompletedEventString_10() const { return ___onDownloadCompletedEventString_10; }
	inline Action_2_t4234541925 ** get_address_of_onDownloadCompletedEventString_10() { return &___onDownloadCompletedEventString_10; }
	inline void set_onDownloadCompletedEventString_10(Action_2_t4234541925 * value)
	{
		___onDownloadCompletedEventString_10 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadCompletedEventString_10, value);
	}

	inline static int32_t get_offset_of_onDownloadProgressedEvent_11() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onDownloadProgressedEvent_11)); }
	inline Action_2_t4277199140 * get_onDownloadProgressedEvent_11() const { return ___onDownloadProgressedEvent_11; }
	inline Action_2_t4277199140 ** get_address_of_onDownloadProgressedEvent_11() { return &___onDownloadProgressedEvent_11; }
	inline void set_onDownloadProgressedEvent_11(Action_2_t4277199140 * value)
	{
		___onDownloadProgressedEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadProgressedEvent_11, value);
	}

	inline static int32_t get_offset_of_onDownloadFailedEvent_12() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onDownloadFailedEvent_12)); }
	inline Action_2_t4234541925 * get_onDownloadFailedEvent_12() const { return ___onDownloadFailedEvent_12; }
	inline Action_2_t4234541925 ** get_address_of_onDownloadFailedEvent_12() { return &___onDownloadFailedEvent_12; }
	inline void set_onDownloadFailedEvent_12(Action_2_t4234541925 * value)
	{
		___onDownloadFailedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___onDownloadFailedEvent_12, value);
	}

	inline static int32_t get_offset_of_onTransactionsRestored_13() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___onTransactionsRestored_13)); }
	inline Action_1_t3627374100 * get_onTransactionsRestored_13() const { return ___onTransactionsRestored_13; }
	inline Action_1_t3627374100 ** get_address_of_onTransactionsRestored_13() { return &___onTransactionsRestored_13; }
	inline void set_onTransactionsRestored_13(Action_1_t3627374100 * value)
	{
		___onTransactionsRestored_13 = value;
		Il2CppCodeGenWriteBarrier(&___onTransactionsRestored_13, value);
	}

	inline static int32_t get_offset_of_DownloadManager_14() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___DownloadManager_14)); }
	inline DownloadManager_t32803451 * get_DownloadManager_14() const { return ___DownloadManager_14; }
	inline DownloadManager_t32803451 ** get_address_of_DownloadManager_14() { return &___DownloadManager_14; }
	inline void set_DownloadManager_14(DownloadManager_t32803451 * value)
	{
		___DownloadManager_14 = value;
		Il2CppCodeGenWriteBarrier(&___DownloadManager_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_15() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___U3CU3Ef__mgU24cache0_15)); }
	inline Action_1_t3765153281 * get_U3CU3Ef__mgU24cache0_15() const { return ___U3CU3Ef__mgU24cache0_15; }
	inline Action_1_t3765153281 ** get_address_of_U3CU3Ef__mgU24cache0_15() { return &___U3CU3Ef__mgU24cache0_15; }
	inline void set_U3CU3Ef__mgU24cache0_15(Action_1_t3765153281 * value)
	{
		___U3CU3Ef__mgU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_16() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___U3CU3Ef__mgU24cache1_16)); }
	inline Action_1_t545353811 * get_U3CU3Ef__mgU24cache1_16() const { return ___U3CU3Ef__mgU24cache1_16; }
	inline Action_1_t545353811 ** get_address_of_U3CU3Ef__mgU24cache1_16() { return &___U3CU3Ef__mgU24cache1_16; }
	inline void set_U3CU3Ef__mgU24cache1_16(Action_1_t545353811 * value)
	{
		___U3CU3Ef__mgU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_17() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___U3CU3Ef__mgU24cache2_17)); }
	inline Action_1_t3765153281 * get_U3CU3Ef__mgU24cache2_17() const { return ___U3CU3Ef__mgU24cache2_17; }
	inline Action_1_t3765153281 ** get_address_of_U3CU3Ef__mgU24cache2_17() { return &___U3CU3Ef__mgU24cache2_17; }
	inline void set_U3CU3Ef__mgU24cache2_17(Action_1_t3765153281 * value)
	{
		___U3CU3Ef__mgU24cache2_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache2_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_18() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___U3CU3Ef__mgU24cache3_18)); }
	inline Action_1_t3765153281 * get_U3CU3Ef__mgU24cache3_18() const { return ___U3CU3Ef__mgU24cache3_18; }
	inline Action_1_t3765153281 ** get_address_of_U3CU3Ef__mgU24cache3_18() { return &___U3CU3Ef__mgU24cache3_18; }
	inline void set_U3CU3Ef__mgU24cache3_18(Action_1_t3765153281 * value)
	{
		___U3CU3Ef__mgU24cache3_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache3_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_19() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___U3CU3Ef__mgU24cache4_19)); }
	inline Action_1_t3765153281 * get_U3CU3Ef__mgU24cache4_19() const { return ___U3CU3Ef__mgU24cache4_19; }
	inline Action_1_t3765153281 ** get_address_of_U3CU3Ef__mgU24cache4_19() { return &___U3CU3Ef__mgU24cache4_19; }
	inline void set_U3CU3Ef__mgU24cache4_19(Action_1_t3765153281 * value)
	{
		___U3CU3Ef__mgU24cache4_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache4_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_20() { return static_cast<int32_t>(offsetof(Unibiller_t1392804696_StaticFields, ___U3CU3Ef__mgU24cache5_20)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__mgU24cache5_20() const { return ___U3CU3Ef__mgU24cache5_20; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__mgU24cache5_20() { return &___U3CU3Ef__mgU24cache5_20; }
	inline void set_U3CU3Ef__mgU24cache5_20(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__mgU24cache5_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache5_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
