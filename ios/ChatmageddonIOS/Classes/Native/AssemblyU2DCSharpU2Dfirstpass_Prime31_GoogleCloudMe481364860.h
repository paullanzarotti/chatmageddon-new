﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "P31RestKit_Prime31_MonoBehaviourGUI1135617291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.GoogleCloudMessagingUI
struct  GoogleCloudMessagingUI_t481364860  : public MonoBehaviourGUI_t1135617291
{
public:
	// System.String Prime31.GoogleCloudMessagingUI::_registrationId
	String_t* ____registrationId_18;

public:
	inline static int32_t get_offset_of__registrationId_18() { return static_cast<int32_t>(offsetof(GoogleCloudMessagingUI_t481364860, ____registrationId_18)); }
	inline String_t* get__registrationId_18() const { return ____registrationId_18; }
	inline String_t** get_address_of__registrationId_18() { return &____registrationId_18; }
	inline void set__registrationId_18(String_t* value)
	{
		____registrationId_18 = value;
		Il2CppCodeGenWriteBarrier(&____registrationId_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
