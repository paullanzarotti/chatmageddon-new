﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen3427107646.h"
#include "AssemblyU2DCSharp_LeaderboardType1980945291.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardNavigationController
struct  LeaderboardNavigationController_t1680977333  : public NavigationController_2_t3427107646
{
public:
	// UILabel LeaderboardNavigationController::dailyLabel
	UILabel_t1795115428 * ___dailyLabel_8;
	// UILabel LeaderboardNavigationController::allTimeLabel
	UILabel_t1795115428 * ___allTimeLabel_9;
	// UILabel LeaderboardNavigationController::innerLabel
	UILabel_t1795115428 * ___innerLabel_10;
	// UILabel LeaderboardNavigationController::outerLabel
	UILabel_t1795115428 * ___outerLabel_11;
	// UILabel LeaderboardNavigationController::worldLabel
	UILabel_t1795115428 * ___worldLabel_12;
	// UnityEngine.GameObject LeaderboardNavigationController::dailyObject
	GameObject_t1756533147 * ___dailyObject_13;
	// UnityEngine.GameObject LeaderboardNavigationController::allTimeObject
	GameObject_t1756533147 * ___allTimeObject_14;
	// LeaderboardType LeaderboardNavigationController::activeLeaderboardType
	int32_t ___activeLeaderboardType_15;
	// UnityEngine.Vector3 LeaderboardNavigationController::activePos
	Vector3_t2243707580  ___activePos_16;
	// UnityEngine.Vector3 LeaderboardNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_17;
	// UnityEngine.Vector3 LeaderboardNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_18;

public:
	inline static int32_t get_offset_of_dailyLabel_8() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___dailyLabel_8)); }
	inline UILabel_t1795115428 * get_dailyLabel_8() const { return ___dailyLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_dailyLabel_8() { return &___dailyLabel_8; }
	inline void set_dailyLabel_8(UILabel_t1795115428 * value)
	{
		___dailyLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___dailyLabel_8, value);
	}

	inline static int32_t get_offset_of_allTimeLabel_9() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___allTimeLabel_9)); }
	inline UILabel_t1795115428 * get_allTimeLabel_9() const { return ___allTimeLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_allTimeLabel_9() { return &___allTimeLabel_9; }
	inline void set_allTimeLabel_9(UILabel_t1795115428 * value)
	{
		___allTimeLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___allTimeLabel_9, value);
	}

	inline static int32_t get_offset_of_innerLabel_10() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___innerLabel_10)); }
	inline UILabel_t1795115428 * get_innerLabel_10() const { return ___innerLabel_10; }
	inline UILabel_t1795115428 ** get_address_of_innerLabel_10() { return &___innerLabel_10; }
	inline void set_innerLabel_10(UILabel_t1795115428 * value)
	{
		___innerLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___innerLabel_10, value);
	}

	inline static int32_t get_offset_of_outerLabel_11() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___outerLabel_11)); }
	inline UILabel_t1795115428 * get_outerLabel_11() const { return ___outerLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_outerLabel_11() { return &___outerLabel_11; }
	inline void set_outerLabel_11(UILabel_t1795115428 * value)
	{
		___outerLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___outerLabel_11, value);
	}

	inline static int32_t get_offset_of_worldLabel_12() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___worldLabel_12)); }
	inline UILabel_t1795115428 * get_worldLabel_12() const { return ___worldLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_worldLabel_12() { return &___worldLabel_12; }
	inline void set_worldLabel_12(UILabel_t1795115428 * value)
	{
		___worldLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___worldLabel_12, value);
	}

	inline static int32_t get_offset_of_dailyObject_13() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___dailyObject_13)); }
	inline GameObject_t1756533147 * get_dailyObject_13() const { return ___dailyObject_13; }
	inline GameObject_t1756533147 ** get_address_of_dailyObject_13() { return &___dailyObject_13; }
	inline void set_dailyObject_13(GameObject_t1756533147 * value)
	{
		___dailyObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___dailyObject_13, value);
	}

	inline static int32_t get_offset_of_allTimeObject_14() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___allTimeObject_14)); }
	inline GameObject_t1756533147 * get_allTimeObject_14() const { return ___allTimeObject_14; }
	inline GameObject_t1756533147 ** get_address_of_allTimeObject_14() { return &___allTimeObject_14; }
	inline void set_allTimeObject_14(GameObject_t1756533147 * value)
	{
		___allTimeObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___allTimeObject_14, value);
	}

	inline static int32_t get_offset_of_activeLeaderboardType_15() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___activeLeaderboardType_15)); }
	inline int32_t get_activeLeaderboardType_15() const { return ___activeLeaderboardType_15; }
	inline int32_t* get_address_of_activeLeaderboardType_15() { return &___activeLeaderboardType_15; }
	inline void set_activeLeaderboardType_15(int32_t value)
	{
		___activeLeaderboardType_15 = value;
	}

	inline static int32_t get_offset_of_activePos_16() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___activePos_16)); }
	inline Vector3_t2243707580  get_activePos_16() const { return ___activePos_16; }
	inline Vector3_t2243707580 * get_address_of_activePos_16() { return &___activePos_16; }
	inline void set_activePos_16(Vector3_t2243707580  value)
	{
		___activePos_16 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_17() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___leftInactivePos_17)); }
	inline Vector3_t2243707580  get_leftInactivePos_17() const { return ___leftInactivePos_17; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_17() { return &___leftInactivePos_17; }
	inline void set_leftInactivePos_17(Vector3_t2243707580  value)
	{
		___leftInactivePos_17 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_18() { return static_cast<int32_t>(offsetof(LeaderboardNavigationController_t1680977333, ___rightInactivePos_18)); }
	inline Vector3_t2243707580  get_rightInactivePos_18() const { return ___rightInactivePos_18; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_18() { return &___rightInactivePos_18; }
	inline void set_rightInactivePos_18(Vector3_t2243707580  value)
	{
		___rightInactivePos_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
