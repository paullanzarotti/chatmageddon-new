﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldShopButton
struct ShieldShopButton_t1762407207;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldShopButton::.ctor()
extern "C"  void ShieldShopButton__ctor_m1285437048 (ShieldShopButton_t1762407207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldShopButton::OnButtonClick()
extern "C"  void ShieldShopButton_OnButtonClick_m1361015227 (ShieldShopButton_t1762407207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
