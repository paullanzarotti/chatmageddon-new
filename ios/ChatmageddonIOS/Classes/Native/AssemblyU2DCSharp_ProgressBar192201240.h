﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ProgressBar/OnProgressUpdated
struct OnProgressUpdated_t2843909178;
// ProgressBar/OnProgressFinished
struct OnProgressFinished_t739551803;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar
struct  ProgressBar_t192201240  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ProgressBar::currentPercent
	float ___currentPercent_2;
	// System.Single ProgressBar::targetPercent
	float ___targetPercent_3;
	// System.Collections.IEnumerator ProgressBar::currentRoutine
	Il2CppObject * ___currentRoutine_4;
	// ProgressBar/OnProgressUpdated ProgressBar::onProgressUpdated
	OnProgressUpdated_t2843909178 * ___onProgressUpdated_5;
	// ProgressBar/OnProgressFinished ProgressBar::onProgressFinished
	OnProgressFinished_t739551803 * ___onProgressFinished_6;
	// System.Single ProgressBar::totalTime
	float ___totalTime_7;

public:
	inline static int32_t get_offset_of_currentPercent_2() { return static_cast<int32_t>(offsetof(ProgressBar_t192201240, ___currentPercent_2)); }
	inline float get_currentPercent_2() const { return ___currentPercent_2; }
	inline float* get_address_of_currentPercent_2() { return &___currentPercent_2; }
	inline void set_currentPercent_2(float value)
	{
		___currentPercent_2 = value;
	}

	inline static int32_t get_offset_of_targetPercent_3() { return static_cast<int32_t>(offsetof(ProgressBar_t192201240, ___targetPercent_3)); }
	inline float get_targetPercent_3() const { return ___targetPercent_3; }
	inline float* get_address_of_targetPercent_3() { return &___targetPercent_3; }
	inline void set_targetPercent_3(float value)
	{
		___targetPercent_3 = value;
	}

	inline static int32_t get_offset_of_currentRoutine_4() { return static_cast<int32_t>(offsetof(ProgressBar_t192201240, ___currentRoutine_4)); }
	inline Il2CppObject * get_currentRoutine_4() const { return ___currentRoutine_4; }
	inline Il2CppObject ** get_address_of_currentRoutine_4() { return &___currentRoutine_4; }
	inline void set_currentRoutine_4(Il2CppObject * value)
	{
		___currentRoutine_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentRoutine_4, value);
	}

	inline static int32_t get_offset_of_onProgressUpdated_5() { return static_cast<int32_t>(offsetof(ProgressBar_t192201240, ___onProgressUpdated_5)); }
	inline OnProgressUpdated_t2843909178 * get_onProgressUpdated_5() const { return ___onProgressUpdated_5; }
	inline OnProgressUpdated_t2843909178 ** get_address_of_onProgressUpdated_5() { return &___onProgressUpdated_5; }
	inline void set_onProgressUpdated_5(OnProgressUpdated_t2843909178 * value)
	{
		___onProgressUpdated_5 = value;
		Il2CppCodeGenWriteBarrier(&___onProgressUpdated_5, value);
	}

	inline static int32_t get_offset_of_onProgressFinished_6() { return static_cast<int32_t>(offsetof(ProgressBar_t192201240, ___onProgressFinished_6)); }
	inline OnProgressFinished_t739551803 * get_onProgressFinished_6() const { return ___onProgressFinished_6; }
	inline OnProgressFinished_t739551803 ** get_address_of_onProgressFinished_6() { return &___onProgressFinished_6; }
	inline void set_onProgressFinished_6(OnProgressFinished_t739551803 * value)
	{
		___onProgressFinished_6 = value;
		Il2CppCodeGenWriteBarrier(&___onProgressFinished_6, value);
	}

	inline static int32_t get_offset_of_totalTime_7() { return static_cast<int32_t>(offsetof(ProgressBar_t192201240, ___totalTime_7)); }
	inline float get_totalTime_7() const { return ___totalTime_7; }
	inline float* get_address_of_totalTime_7() { return &___totalTime_7; }
	inline void set_totalTime_7(float value)
	{
		___totalTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
