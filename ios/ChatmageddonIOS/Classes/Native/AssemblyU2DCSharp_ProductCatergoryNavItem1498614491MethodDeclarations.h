﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProductCatergoryNavItem
struct ProductCatergoryNavItem_t1498614491;

#include "codegen/il2cpp-codegen.h"

// System.Void ProductCatergoryNavItem::.ctor()
extern "C"  void ProductCatergoryNavItem__ctor_m3619219056 (ProductCatergoryNavItem_t1498614491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProductCatergoryNavItem::OnButtonClick()
extern "C"  void ProductCatergoryNavItem_OnButtonClick_m4048206831 (ProductCatergoryNavItem_t1498614491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
