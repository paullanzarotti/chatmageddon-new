﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigateBackwardButton
struct NavigateBackwardButton_t1567151050;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void NavigateBackwardButton::.ctor()
extern "C"  void NavigateBackwardButton__ctor_m3050283009 (NavigateBackwardButton_t1567151050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigateBackwardButton::OnButtonClick()
extern "C"  void NavigateBackwardButton_OnButtonClick_m1964118930 (NavigateBackwardButton_t1567151050 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigateBackwardButton::SetButtonDirection(ButtonDirection)
extern "C"  void NavigateBackwardButton_SetButtonDirection_m63511419 (NavigateBackwardButton_t1567151050 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
