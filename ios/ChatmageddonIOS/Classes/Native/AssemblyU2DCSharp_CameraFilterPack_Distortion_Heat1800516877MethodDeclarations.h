﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Heat
struct CameraFilterPack_Distortion_Heat_t1800516877;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Heat::.ctor()
extern "C"  void CameraFilterPack_Distortion_Heat__ctor_m4288151348 (CameraFilterPack_Distortion_Heat_t1800516877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Heat::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Heat_get_material_m3460554873 (CameraFilterPack_Distortion_Heat_t1800516877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::Start()
extern "C"  void CameraFilterPack_Distortion_Heat_Start_m324967948 (CameraFilterPack_Distortion_Heat_t1800516877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Heat_OnRenderImage_m3674054212 (CameraFilterPack_Distortion_Heat_t1800516877 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Heat_OnValidate_m3792108739 (CameraFilterPack_Distortion_Heat_t1800516877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::Update()
extern "C"  void CameraFilterPack_Distortion_Heat_Update_m3628302709 (CameraFilterPack_Distortion_Heat_t1800516877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Heat_OnDisable_m726957025 (CameraFilterPack_Distortion_Heat_t1800516877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
