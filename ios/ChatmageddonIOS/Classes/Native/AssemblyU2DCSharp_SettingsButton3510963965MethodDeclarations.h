﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsButton
struct SettingsButton_t3510963965;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsButton::.ctor()
extern "C"  void SettingsButton__ctor_m454671040 (SettingsButton_t3510963965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsButton::OnButtonClick()
extern "C"  void SettingsButton_OnButtonClick_m92240705 (SettingsButton_t3510963965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
