﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineShopButton
struct MineShopButton_t202889731;

#include "codegen/il2cpp-codegen.h"

// System.Void MineShopButton::.ctor()
extern "C"  void MineShopButton__ctor_m4293802874 (MineShopButton_t202889731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineShopButton::OnButtonClick()
extern "C"  void MineShopButton_OnButtonClick_m435184727 (MineShopButton_t202889731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
