﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FBManager`1/LoadPictureCallback<System.Object>
struct LoadPictureCallback_t2854975038;
// FBManager`1<System.Object>
struct FBManager_1_t2091981452;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBManager`1/<LoadPictureAPI>c__AnonStorey4<System.Object>
struct  U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354  : public Il2CppObject
{
public:
	// FBManager`1/LoadPictureCallback<ManagerType> FBManager`1/<LoadPictureAPI>c__AnonStorey4::callback
	LoadPictureCallback_t2854975038 * ___callback_0;
	// FBManager`1<ManagerType> FBManager`1/<LoadPictureAPI>c__AnonStorey4::$this
	FBManager_1_t2091981452 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354, ___callback_0)); }
	inline LoadPictureCallback_t2854975038 * get_callback_0() const { return ___callback_0; }
	inline LoadPictureCallback_t2854975038 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(LoadPictureCallback_t2854975038 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354, ___U24this_1)); }
	inline FBManager_1_t2091981452 * get_U24this_1() const { return ___U24this_1; }
	inline FBManager_1_t2091981452 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FBManager_1_t2091981452 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
