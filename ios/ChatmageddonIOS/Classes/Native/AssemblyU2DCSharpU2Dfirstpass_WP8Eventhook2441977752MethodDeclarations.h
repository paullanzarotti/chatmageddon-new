﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WP8Eventhook
struct WP8Eventhook_t2441977752;

#include "codegen/il2cpp-codegen.h"

// System.Void WP8Eventhook::.ctor()
extern "C"  void WP8Eventhook__ctor_m462809925 (WP8Eventhook_t2441977752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WP8Eventhook::Start()
extern "C"  void WP8Eventhook_Start_m4106386373 (WP8Eventhook_t2441977752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WP8Eventhook::OnApplicationPause(System.Boolean)
extern "C"  void WP8Eventhook_OnApplicationPause_m2299265015 (WP8Eventhook_t2441977752 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
