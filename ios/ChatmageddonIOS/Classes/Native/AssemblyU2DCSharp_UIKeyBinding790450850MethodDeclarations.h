﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIKeyBinding
struct UIKeyBinding_t790450850;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

// System.Void UIKeyBinding::.ctor()
extern "C"  void UIKeyBinding__ctor_m4030949213 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyBinding::IsBound(UnityEngine.KeyCode)
extern "C"  bool UIKeyBinding_IsBound_m2381906042 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnEnable()
extern "C"  void UIKeyBinding_OnEnable_m1948675085 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnDisable()
extern "C"  void UIKeyBinding_OnDisable_m2850546066 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::Start()
extern "C"  void UIKeyBinding_Start_m2466097869 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnSubmit()
extern "C"  void UIKeyBinding_OnSubmit_m240226284 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyBinding::IsModifierActive()
extern "C"  bool UIKeyBinding_IsModifierActive_m1457984176 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::Update()
extern "C"  void UIKeyBinding_Update_m1948624228 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnBindingPress(System.Boolean)
extern "C"  void UIKeyBinding_OnBindingPress_m2372437545 (UIKeyBinding_t790450850 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnBindingClick()
extern "C"  void UIKeyBinding_OnBindingClick_m1243771215 (UIKeyBinding_t790450850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::.cctor()
extern "C"  void UIKeyBinding__cctor_m50253916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
