﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t2637371637 ();
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t2895223116 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t362827733 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t1955031820 ();
extern "C" void DelegatePInvokeWrapper_AsyncReadHandler_t1533281179 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t3362229488 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1894833619 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1599878889 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t453000516 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1559754630 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t888270799 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t3737776727 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1824458113 ();
extern "C" void DelegatePInvokeWrapper_StreamChangeCallback_t1492982741 ();
extern "C" void DelegatePInvokeWrapper_Action_t3226471752 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336 ();
extern "C" void DelegatePInvokeWrapper_DispatcherFactory_t1307565918 ();
extern "C" void DelegatePInvokeWrapper_OnChangeCallback_t2639189684 ();
extern "C" void DelegatePInvokeWrapper_ReadDataHandler_t1814344764 ();
extern "C" void DelegatePInvokeWrapper_OnDLLLoaded_t1727406294 ();
extern "C" void DelegatePInvokeWrapper_HideUnityDelegate_t712804158 ();
extern "C" void DelegatePInvokeWrapper_InitDelegate_t3410465555 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1946318473 ();
extern "C" void DelegatePInvokeWrapper_CompressFunc_t4191606113 ();
extern "C" void DelegatePInvokeWrapper_OnTimerFinished_t1234642247 ();
extern "C" void DelegatePInvokeWrapper_Callback_t2100910411 ();
extern "C" void DelegatePInvokeWrapper_CenterOnChildStartedHandler_t5714103 ();
extern "C" void DelegatePInvokeWrapper_CyclerIndexChangeHandler_t463772902 ();
extern "C" void DelegatePInvokeWrapper_CyclerStoppedHandler_t3894542237 ();
extern "C" void DelegatePInvokeWrapper_SelectionStartedHandler_t4151988355 ();
extern "C" void DelegatePInvokeWrapper_PickerValueUpdatedHandler_t2401088127 ();
extern "C" void DelegatePInvokeWrapper_OnDragExit_t3593337772 ();
extern "C" void DelegatePInvokeWrapper_OnPickerClicked_t2482807019 ();
extern "C" void DelegatePInvokeWrapper_OnPopulationComplete_t1301655353 ();
extern "C" void DelegatePInvokeWrapper_OnHideComplete_t4141700430 ();
extern "C" void DelegatePInvokeWrapper_OnLeftComplete_t1529246307 ();
extern "C" void DelegatePInvokeWrapper_OnRightComplete_t1521737374 ();
extern "C" void DelegatePInvokeWrapper_OnZeroHit_t581944179 ();
extern "C" void DelegatePInvokeWrapper_OnZeroHit_t1439474620 ();
extern "C" void DelegatePInvokeWrapper_IndicatorHidden_t1529613817 ();
extern "C" void DelegatePInvokeWrapper_IndicatorShown_t76852832 ();
extern "C" void DelegatePInvokeWrapper_LoadFunction_t1013268478 ();
extern "C" void DelegatePInvokeWrapper_OnLocalizeNotification_t3819587589 ();
extern "C" void DelegatePInvokeWrapper_OnTimerUpdate_t2669454040 ();
extern "C" void DelegatePInvokeWrapper_OnZeroHit_t3980744303 ();
extern "C" void DelegatePInvokeWrapper_OnAnimationDrawn_t3207005891 ();
extern "C" void DelegatePInvokeWrapper_OnAnimationFinished_t3859016523 ();
extern "C" void DelegatePInvokeWrapper_OnAnimationLooped_t2655604368 ();
extern "C" void DelegatePInvokeWrapper_OnAnimationStart_t3313445917 ();
extern "C" void DelegatePInvokeWrapper_OnAnimationUpdated_t4231811366 ();
extern "C" void DelegatePInvokeWrapper_OnZeroHit_t3971658986 ();
extern "C" void DelegatePInvokeWrapper_OnProgressFinished_t739551803 ();
extern "C" void DelegatePInvokeWrapper_OnProgressUpdated_t2843909178 ();
extern "C" void DelegatePInvokeWrapper_OnPopulationComplete_t1952569739 ();
extern "C" void DelegatePInvokeWrapper_OnPopulationUpdate_t30122867 ();
extern "C" void DelegatePInvokeWrapper_ScrollStartStopHandler_t1032307121 ();
extern "C" void DelegatePInvokeWrapper_OnFinished_t3595288269 ();
extern "C" void DelegatePInvokeWrapper_OnFinished_t3890054880 ();
extern "C" void DelegatePInvokeWrapper_OnZeroHit_t2015899103 ();
extern "C" void DelegatePInvokeWrapper_GetAnyKeyFunc_t2213225645 ();
extern "C" void DelegatePInvokeWrapper_GetAxisFunc_t212136019 ();
extern "C" void DelegatePInvokeWrapper_GetKeyStateFunc_t1266514268 ();
extern "C" void DelegatePInvokeWrapper_GetTouchCountCallback_t2237370697 ();
extern "C" void DelegatePInvokeWrapper_MoveDelegate_t3125635652 ();
extern "C" void DelegatePInvokeWrapper_OnCustomInput_t3556372712 ();
extern "C" void DelegatePInvokeWrapper_OnSchemeChange_t1272113120 ();
extern "C" void DelegatePInvokeWrapper_OnScreenResize_t1327492915 ();
extern "C" void DelegatePInvokeWrapper_OnReposition_t581118304 ();
extern "C" void DelegatePInvokeWrapper_OnValidate_t2431313412 ();
extern "C" void DelegatePInvokeWrapper_OnGeometryUpdated_t754882645 ();
extern "C" void DelegatePInvokeWrapper_LegacyEvent_t3991167770 ();
extern "C" void DelegatePInvokeWrapper_OnDragFinished_t2651612482 ();
extern "C" void DelegatePInvokeWrapper_OnDragNotification_t685967913 ();
extern "C" void DelegatePInvokeWrapper_OnReposition_t1194954916 ();
extern "C" void DelegatePInvokeWrapper_Validate_t558489517 ();
extern "C" void DelegatePInvokeWrapper_HitCheck_t3590501724 ();
extern "C" void DelegatePInvokeWrapper_OnDimensionsChanged_t3620741577 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[93] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t3898244613,
	DelegatePInvokeWrapper_Swapper_t2637371637,
	DelegatePInvokeWrapper_InternalCancelHandler_t2895223116,
	DelegatePInvokeWrapper_ReadDelegate_t3184826381,
	DelegatePInvokeWrapper_WriteDelegate_t489908132,
	DelegatePInvokeWrapper_CrossContextDelegate_t754146990,
	DelegatePInvokeWrapper_CallbackHandler_t362827733,
	DelegatePInvokeWrapper_ThreadStart_t3437517264,
	DelegatePInvokeWrapper_CharGetter_t1955031820,
	DelegatePInvokeWrapper_AsyncReadHandler_t1533281179,
	DelegatePInvokeWrapper_ReadMethod_t3362229488,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745,
	DelegatePInvokeWrapper_WriteMethod_t1894833619,
	DelegatePInvokeWrapper_ReadDelegate_t1599878889,
	DelegatePInvokeWrapper_WriteDelegate_t453000516,
	DelegatePInvokeWrapper_ReadDelegate_t1559754630,
	DelegatePInvokeWrapper_WriteDelegate_t888270799,
	DelegatePInvokeWrapper_SocketAsyncCall_t3737776727,
	DelegatePInvokeWrapper_CostDelegate_t1824458113,
	DelegatePInvokeWrapper_StreamChangeCallback_t1492982741,
	DelegatePInvokeWrapper_Action_t3226471752,
	DelegatePInvokeWrapper_LogCallback_t1867914413,
	DelegatePInvokeWrapper_PCMReaderCallback_t3007145346,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033,
	DelegatePInvokeWrapper_WillRenderCanvases_t3522132132,
	DelegatePInvokeWrapper_StateChanged_t2480912210,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815,
	DelegatePInvokeWrapper_UnityAction_t4025899511,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033,
	DelegatePInvokeWrapper_WindowFunction_t3486805455,
	DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336,
	DelegatePInvokeWrapper_DispatcherFactory_t1307565918,
	DelegatePInvokeWrapper_OnChangeCallback_t2639189684,
	DelegatePInvokeWrapper_ReadDataHandler_t1814344764,
	DelegatePInvokeWrapper_OnDLLLoaded_t1727406294,
	DelegatePInvokeWrapper_HideUnityDelegate_t712804158,
	DelegatePInvokeWrapper_InitDelegate_t3410465555,
	DelegatePInvokeWrapper_OnValidateInput_t1946318473,
	DelegatePInvokeWrapper_CompressFunc_t4191606113,
	DelegatePInvokeWrapper_OnTimerFinished_t1234642247,
	DelegatePInvokeWrapper_Callback_t2100910411,
	DelegatePInvokeWrapper_CenterOnChildStartedHandler_t5714103,
	DelegatePInvokeWrapper_CyclerIndexChangeHandler_t463772902,
	DelegatePInvokeWrapper_CyclerStoppedHandler_t3894542237,
	DelegatePInvokeWrapper_SelectionStartedHandler_t4151988355,
	DelegatePInvokeWrapper_PickerValueUpdatedHandler_t2401088127,
	DelegatePInvokeWrapper_OnDragExit_t3593337772,
	DelegatePInvokeWrapper_OnPickerClicked_t2482807019,
	DelegatePInvokeWrapper_OnPopulationComplete_t1301655353,
	DelegatePInvokeWrapper_OnHideComplete_t4141700430,
	DelegatePInvokeWrapper_OnLeftComplete_t1529246307,
	DelegatePInvokeWrapper_OnRightComplete_t1521737374,
	DelegatePInvokeWrapper_OnZeroHit_t581944179,
	DelegatePInvokeWrapper_OnZeroHit_t1439474620,
	DelegatePInvokeWrapper_IndicatorHidden_t1529613817,
	DelegatePInvokeWrapper_IndicatorShown_t76852832,
	DelegatePInvokeWrapper_LoadFunction_t1013268478,
	DelegatePInvokeWrapper_OnLocalizeNotification_t3819587589,
	DelegatePInvokeWrapper_OnTimerUpdate_t2669454040,
	DelegatePInvokeWrapper_OnZeroHit_t3980744303,
	DelegatePInvokeWrapper_OnAnimationDrawn_t3207005891,
	DelegatePInvokeWrapper_OnAnimationFinished_t3859016523,
	DelegatePInvokeWrapper_OnAnimationLooped_t2655604368,
	DelegatePInvokeWrapper_OnAnimationStart_t3313445917,
	DelegatePInvokeWrapper_OnAnimationUpdated_t4231811366,
	DelegatePInvokeWrapper_OnZeroHit_t3971658986,
	DelegatePInvokeWrapper_OnProgressFinished_t739551803,
	DelegatePInvokeWrapper_OnProgressUpdated_t2843909178,
	DelegatePInvokeWrapper_OnPopulationComplete_t1952569739,
	DelegatePInvokeWrapper_OnPopulationUpdate_t30122867,
	DelegatePInvokeWrapper_ScrollStartStopHandler_t1032307121,
	DelegatePInvokeWrapper_OnFinished_t3595288269,
	DelegatePInvokeWrapper_OnFinished_t3890054880,
	DelegatePInvokeWrapper_OnZeroHit_t2015899103,
	DelegatePInvokeWrapper_GetAnyKeyFunc_t2213225645,
	DelegatePInvokeWrapper_GetAxisFunc_t212136019,
	DelegatePInvokeWrapper_GetKeyStateFunc_t1266514268,
	DelegatePInvokeWrapper_GetTouchCountCallback_t2237370697,
	DelegatePInvokeWrapper_MoveDelegate_t3125635652,
	DelegatePInvokeWrapper_OnCustomInput_t3556372712,
	DelegatePInvokeWrapper_OnSchemeChange_t1272113120,
	DelegatePInvokeWrapper_OnScreenResize_t1327492915,
	DelegatePInvokeWrapper_OnReposition_t581118304,
	DelegatePInvokeWrapper_OnValidate_t2431313412,
	DelegatePInvokeWrapper_OnGeometryUpdated_t754882645,
	DelegatePInvokeWrapper_LegacyEvent_t3991167770,
	DelegatePInvokeWrapper_OnDragFinished_t2651612482,
	DelegatePInvokeWrapper_OnDragNotification_t685967913,
	DelegatePInvokeWrapper_OnReposition_t1194954916,
	DelegatePInvokeWrapper_Validate_t558489517,
	DelegatePInvokeWrapper_HitCheck_t3590501724,
	DelegatePInvokeWrapper_OnDimensionsChanged_t3620741577,
};
