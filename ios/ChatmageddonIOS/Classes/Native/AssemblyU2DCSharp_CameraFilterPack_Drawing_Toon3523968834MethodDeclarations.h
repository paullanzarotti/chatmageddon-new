﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Toon
struct CameraFilterPack_Drawing_Toon_t3523968834;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Toon::.ctor()
extern "C"  void CameraFilterPack_Drawing_Toon__ctor_m3614349693 (CameraFilterPack_Drawing_Toon_t3523968834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Toon::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Toon_get_material_m3920261658 (CameraFilterPack_Drawing_Toon_t3523968834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::Start()
extern "C"  void CameraFilterPack_Drawing_Toon_Start_m1285276893 (CameraFilterPack_Drawing_Toon_t3523968834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Toon_OnRenderImage_m399582245 (CameraFilterPack_Drawing_Toon_t3523968834 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Toon_OnValidate_m1805938286 (CameraFilterPack_Drawing_Toon_t3523968834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::Update()
extern "C"  void CameraFilterPack_Drawing_Toon_Update_m2855900672 (CameraFilterPack_Drawing_Toon_t3523968834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Toon_OnDisable_m662427342 (CameraFilterPack_Drawing_Toon_t3523968834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
