﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FAQNavScreen
struct FAQNavScreen_t2266159941;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FAQNavScreen::.ctor()
extern "C"  void FAQNavScreen__ctor_m3826979780 (FAQNavScreen_t2266159941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQNavScreen::Start()
extern "C"  void FAQNavScreen_Start_m726246852 (FAQNavScreen_t2266159941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQNavScreen::UIClosing()
extern "C"  void FAQNavScreen_UIClosing_m1847430383 (FAQNavScreen_t2266159941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void FAQNavScreen_ScreenLoad_m1202224396 (FAQNavScreen_t2266159941 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void FAQNavScreen_ScreenUnload_m303193036 (FAQNavScreen_t2266159941 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FAQNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool FAQNavScreen_ValidateScreenNavigation_m3262295295 (FAQNavScreen_t2266159941 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
