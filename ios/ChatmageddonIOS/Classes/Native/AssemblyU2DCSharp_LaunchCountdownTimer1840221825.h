﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// LaunchCountdownTimer/OnZeroHit
struct OnZeroHit_t1439474620;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchCountdownTimer
struct  LaunchCountdownTimer_t1840221825  : public MonoBehaviour_t1158329972
{
public:
	// System.Double LaunchCountdownTimer::countdownTime
	double ___countdownTime_2;
	// UILabel LaunchCountdownTimer::timeLabel
	UILabel_t1795115428 * ___timeLabel_3;
	// UnityEngine.Coroutine LaunchCountdownTimer::calcRoutine
	Coroutine_t2299508840 * ___calcRoutine_4;
	// System.Boolean LaunchCountdownTimer::countdownPlayed
	bool ___countdownPlayed_5;
	// System.Boolean LaunchCountdownTimer::launchPlayed
	bool ___launchPlayed_6;
	// LaunchCountdownTimer/OnZeroHit LaunchCountdownTimer::onZeroHit
	OnZeroHit_t1439474620 * ___onZeroHit_7;

public:
	inline static int32_t get_offset_of_countdownTime_2() { return static_cast<int32_t>(offsetof(LaunchCountdownTimer_t1840221825, ___countdownTime_2)); }
	inline double get_countdownTime_2() const { return ___countdownTime_2; }
	inline double* get_address_of_countdownTime_2() { return &___countdownTime_2; }
	inline void set_countdownTime_2(double value)
	{
		___countdownTime_2 = value;
	}

	inline static int32_t get_offset_of_timeLabel_3() { return static_cast<int32_t>(offsetof(LaunchCountdownTimer_t1840221825, ___timeLabel_3)); }
	inline UILabel_t1795115428 * get_timeLabel_3() const { return ___timeLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_3() { return &___timeLabel_3; }
	inline void set_timeLabel_3(UILabel_t1795115428 * value)
	{
		___timeLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_3, value);
	}

	inline static int32_t get_offset_of_calcRoutine_4() { return static_cast<int32_t>(offsetof(LaunchCountdownTimer_t1840221825, ___calcRoutine_4)); }
	inline Coroutine_t2299508840 * get_calcRoutine_4() const { return ___calcRoutine_4; }
	inline Coroutine_t2299508840 ** get_address_of_calcRoutine_4() { return &___calcRoutine_4; }
	inline void set_calcRoutine_4(Coroutine_t2299508840 * value)
	{
		___calcRoutine_4 = value;
		Il2CppCodeGenWriteBarrier(&___calcRoutine_4, value);
	}

	inline static int32_t get_offset_of_countdownPlayed_5() { return static_cast<int32_t>(offsetof(LaunchCountdownTimer_t1840221825, ___countdownPlayed_5)); }
	inline bool get_countdownPlayed_5() const { return ___countdownPlayed_5; }
	inline bool* get_address_of_countdownPlayed_5() { return &___countdownPlayed_5; }
	inline void set_countdownPlayed_5(bool value)
	{
		___countdownPlayed_5 = value;
	}

	inline static int32_t get_offset_of_launchPlayed_6() { return static_cast<int32_t>(offsetof(LaunchCountdownTimer_t1840221825, ___launchPlayed_6)); }
	inline bool get_launchPlayed_6() const { return ___launchPlayed_6; }
	inline bool* get_address_of_launchPlayed_6() { return &___launchPlayed_6; }
	inline void set_launchPlayed_6(bool value)
	{
		___launchPlayed_6 = value;
	}

	inline static int32_t get_offset_of_onZeroHit_7() { return static_cast<int32_t>(offsetof(LaunchCountdownTimer_t1840221825, ___onZeroHit_7)); }
	inline OnZeroHit_t1439474620 * get_onZeroHit_7() const { return ___onZeroHit_7; }
	inline OnZeroHit_t1439474620 ** get_address_of_onZeroHit_7() { return &___onZeroHit_7; }
	inline void set_onZeroHit_7(OnZeroHit_t1439474620 * value)
	{
		___onZeroHit_7 = value;
		Il2CppCodeGenWriteBarrier(&___onZeroHit_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
