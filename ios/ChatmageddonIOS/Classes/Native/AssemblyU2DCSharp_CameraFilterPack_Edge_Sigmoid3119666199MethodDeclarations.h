﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Edge_Sigmoid
struct CameraFilterPack_Edge_Sigmoid_t3119666199;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Edge_Sigmoid::.ctor()
extern "C"  void CameraFilterPack_Edge_Sigmoid__ctor_m821495846 (CameraFilterPack_Edge_Sigmoid_t3119666199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Sigmoid::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Edge_Sigmoid_get_material_m4256115315 (CameraFilterPack_Edge_Sigmoid_t3119666199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::Start()
extern "C"  void CameraFilterPack_Edge_Sigmoid_Start_m2787396914 (CameraFilterPack_Edge_Sigmoid_t3119666199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Edge_Sigmoid_OnRenderImage_m446459194 (CameraFilterPack_Edge_Sigmoid_t3119666199 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::OnValidate()
extern "C"  void CameraFilterPack_Edge_Sigmoid_OnValidate_m4006468965 (CameraFilterPack_Edge_Sigmoid_t3119666199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::Update()
extern "C"  void CameraFilterPack_Edge_Sigmoid_Update_m2882303163 (CameraFilterPack_Edge_Sigmoid_t3119666199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::OnDisable()
extern "C"  void CameraFilterPack_Edge_Sigmoid_OnDisable_m65821043 (CameraFilterPack_Edge_Sigmoid_t3119666199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
