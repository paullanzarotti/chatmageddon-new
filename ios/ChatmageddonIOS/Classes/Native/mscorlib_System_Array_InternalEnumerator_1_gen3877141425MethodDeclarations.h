﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3877141425.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"

// System.Void System.Array/InternalEnumerator`1<FriendSearchNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3442344390_gshared (InternalEnumerator_1_t3877141425 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3442344390(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3877141425 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3442344390_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FriendSearchNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1294219654_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1294219654(__this, method) ((  void (*) (InternalEnumerator_1_t3877141425 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1294219654_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FriendSearchNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2472733776_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2472733776(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3877141425 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2472733776_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FriendSearchNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4101493547_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4101493547(__this, method) ((  void (*) (InternalEnumerator_1_t3877141425 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4101493547_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FriendSearchNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3772865278_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3772865278(__this, method) ((  bool (*) (InternalEnumerator_1_t3877141425 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3772865278_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FriendSearchNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2417651319_gshared (InternalEnumerator_1_t3877141425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2417651319(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3877141425 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2417651319_gshared)(__this, method)
