﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatContactsILP
struct ChatContactsILP_t1780035494;
// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactsNavScreen
struct  ContactsNavScreen_t1794286542  : public NavigationScreen_t2333230110
{
public:
	// ChatContactsILP ContactsNavScreen::contactsILP
	ChatContactsILP_t1780035494 * ___contactsILP_3;
	// UIInput ContactsNavScreen::searchInput
	UIInput_t860674234 * ___searchInput_4;
	// System.Boolean ContactsNavScreen::contactsUIclosing
	bool ___contactsUIclosing_5;

public:
	inline static int32_t get_offset_of_contactsILP_3() { return static_cast<int32_t>(offsetof(ContactsNavScreen_t1794286542, ___contactsILP_3)); }
	inline ChatContactsILP_t1780035494 * get_contactsILP_3() const { return ___contactsILP_3; }
	inline ChatContactsILP_t1780035494 ** get_address_of_contactsILP_3() { return &___contactsILP_3; }
	inline void set_contactsILP_3(ChatContactsILP_t1780035494 * value)
	{
		___contactsILP_3 = value;
		Il2CppCodeGenWriteBarrier(&___contactsILP_3, value);
	}

	inline static int32_t get_offset_of_searchInput_4() { return static_cast<int32_t>(offsetof(ContactsNavScreen_t1794286542, ___searchInput_4)); }
	inline UIInput_t860674234 * get_searchInput_4() const { return ___searchInput_4; }
	inline UIInput_t860674234 ** get_address_of_searchInput_4() { return &___searchInput_4; }
	inline void set_searchInput_4(UIInput_t860674234 * value)
	{
		___searchInput_4 = value;
		Il2CppCodeGenWriteBarrier(&___searchInput_4, value);
	}

	inline static int32_t get_offset_of_contactsUIclosing_5() { return static_cast<int32_t>(offsetof(ContactsNavScreen_t1794286542, ___contactsUIclosing_5)); }
	inline bool get_contactsUIclosing_5() const { return ___contactsUIclosing_5; }
	inline bool* get_address_of_contactsUIclosing_5() { return &___contactsUIclosing_5; }
	inline void set_contactsUIclosing_5(bool value)
	{
		___contactsUIclosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
