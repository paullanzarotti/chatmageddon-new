﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.ShortNumberUtil
struct ShortNumberUtil_t1108790299;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil4155573397.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.ShortNumberUtil::.ctor()
extern "C"  void ShortNumberUtil__ctor_m4058486130 (ShortNumberUtil_t1108790299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.ShortNumberUtil::.ctor(PhoneNumbers.PhoneNumberUtil)
extern "C"  void ShortNumberUtil__ctor_m3350495921 (ShortNumberUtil_t1108790299 * __this, PhoneNumberUtil_t4155573397 * ___util0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.ShortNumberUtil::ConnectsToEmergencyNumber(System.String,System.String)
extern "C"  bool ShortNumberUtil_ConnectsToEmergencyNumber_m3303280052 (ShortNumberUtil_t1108790299 * __this, String_t* ___number0, String_t* ___regionCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.ShortNumberUtil::IsEmergencyNumber(System.String,System.String)
extern "C"  bool ShortNumberUtil_IsEmergencyNumber_m3894406870 (ShortNumberUtil_t1108790299 * __this, String_t* ___number0, String_t* ___regionCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.ShortNumberUtil::MatchesEmergencyNumberHelper(System.String,System.String,System.Boolean)
extern "C"  bool ShortNumberUtil_MatchesEmergencyNumberHelper_m1942752592 (ShortNumberUtil_t1108790299 * __this, String_t* ___number0, String_t* ___regionCode1, bool ___allowPrefixMatch2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
