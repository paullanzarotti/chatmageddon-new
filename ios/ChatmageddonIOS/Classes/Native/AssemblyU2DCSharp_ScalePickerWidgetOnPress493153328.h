﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPPickerBase
struct IPPickerBase_t4159478266;
// UIWidget
struct UIWidget_t1453041918;
// TweenScale
struct TweenScale_t2697902175;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScalePickerWidgetOnPress
struct  ScalePickerWidgetOnPress_t493153328  : public MonoBehaviour_t1158329972
{
public:
	// IPPickerBase ScalePickerWidgetOnPress::picker
	IPPickerBase_t4159478266 * ___picker_2;
	// System.Single ScalePickerWidgetOnPress::duration
	float ___duration_3;
	// System.Single ScalePickerWidgetOnPress::scaleFactor
	float ___scaleFactor_4;
	// UIWidget ScalePickerWidgetOnPress::_widget
	UIWidget_t1453041918 * ____widget_5;
	// TweenScale ScalePickerWidgetOnPress::_tweenScale
	TweenScale_t2697902175 * ____tweenScale_6;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(ScalePickerWidgetOnPress_t493153328, ___picker_2)); }
	inline IPPickerBase_t4159478266 * get_picker_2() const { return ___picker_2; }
	inline IPPickerBase_t4159478266 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(IPPickerBase_t4159478266 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier(&___picker_2, value);
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(ScalePickerWidgetOnPress_t493153328, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_4() { return static_cast<int32_t>(offsetof(ScalePickerWidgetOnPress_t493153328, ___scaleFactor_4)); }
	inline float get_scaleFactor_4() const { return ___scaleFactor_4; }
	inline float* get_address_of_scaleFactor_4() { return &___scaleFactor_4; }
	inline void set_scaleFactor_4(float value)
	{
		___scaleFactor_4 = value;
	}

	inline static int32_t get_offset_of__widget_5() { return static_cast<int32_t>(offsetof(ScalePickerWidgetOnPress_t493153328, ____widget_5)); }
	inline UIWidget_t1453041918 * get__widget_5() const { return ____widget_5; }
	inline UIWidget_t1453041918 ** get_address_of__widget_5() { return &____widget_5; }
	inline void set__widget_5(UIWidget_t1453041918 * value)
	{
		____widget_5 = value;
		Il2CppCodeGenWriteBarrier(&____widget_5, value);
	}

	inline static int32_t get_offset_of__tweenScale_6() { return static_cast<int32_t>(offsetof(ScalePickerWidgetOnPress_t493153328, ____tweenScale_6)); }
	inline TweenScale_t2697902175 * get__tweenScale_6() const { return ____tweenScale_6; }
	inline TweenScale_t2697902175 ** get_address_of__tweenScale_6() { return &____tweenScale_6; }
	inline void set__tweenScale_6(TweenScale_t2697902175 * value)
	{
		____tweenScale_6 = value;
		Il2CppCodeGenWriteBarrier(&____tweenScale_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
