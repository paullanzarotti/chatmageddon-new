﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PanelTransitionManager_1_gen3816465789MethodDeclarations.h"

// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::.ctor()
#define PanelTransitionManager_1__ctor_m2765202085(__this, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, const MethodInfo*))PanelTransitionManager_1__ctor_m3470287956_gshared)(__this, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::Initialise()
#define PanelTransitionManager_1_Initialise_m3931284518(__this, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, const MethodInfo*))PanelTransitionManager_1_Initialise_m3802373897_gshared)(__this, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::AddPanel(PanelType,Panel)
#define PanelTransitionManager_1_AddPanel_m861924352(__this, ___panelType0, ___panelToAdd1, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, int32_t, Panel_t1787746694 *, const MethodInfo*))PanelTransitionManager_1_AddPanel_m2443272611_gshared)(__this, ___panelType0, ___panelToAdd1, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::RemovePanel(PanelType)
#define PanelTransitionManager_1_RemovePanel_m2505210323(__this, ___panelType0, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, int32_t, const MethodInfo*))PanelTransitionManager_1_RemovePanel_m2528126402_gshared)(__this, ___panelType0, method)
// System.Boolean PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::get_PanelActive()
#define PanelTransitionManager_1_get_PanelActive_m2412409104(__this, method) ((  bool (*) (PanelTransitionManager_1_t2526031859 *, const MethodInfo*))PanelTransitionManager_1_get_PanelActive_m3779807383_gshared)(__this, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::TogglePanel(PanelType,System.Boolean)
#define PanelTransitionManager_1_TogglePanel_m149048672(__this, ___panel0, ___switching1, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, int32_t, bool, const MethodInfo*))PanelTransitionManager_1_TogglePanel_m438349725_gshared)(__this, ___panel0, ___switching1, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::ActivatePanel(Panel)
#define PanelTransitionManager_1_ActivatePanel_m1359319864(__this, ___passed_Panel0, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, Panel_t1787746694 *, const MethodInfo*))PanelTransitionManager_1_ActivatePanel_m1159509373_gshared)(__this, ___passed_Panel0, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::DeActivatePanel(Panel)
#define PanelTransitionManager_1_DeActivatePanel_m444666005(__this, ___passed_Panel0, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, Panel_t1787746694 *, const MethodInfo*))PanelTransitionManager_1_DeActivatePanel_m413338124_gshared)(__this, ___passed_Panel0, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::DeActivateAllPanels()
#define PanelTransitionManager_1_DeActivateAllPanels_m2642741289(__this, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, const MethodInfo*))PanelTransitionManager_1_DeActivateAllPanels_m3808037922_gshared)(__this, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::ToggleUI()
#define PanelTransitionManager_1_ToggleUI_m3329011159(__this, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, const MethodInfo*))PanelTransitionManager_1_ToggleUI_m2580260392_gshared)(__this, method)
// System.Collections.IEnumerator PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::SwitchPanels()
#define PanelTransitionManager_1_SwitchPanels_m1550920642(__this, method) ((  Il2CppObject * (*) (PanelTransitionManager_1_t2526031859 *, const MethodInfo*))PanelTransitionManager_1_SwitchPanels_m1400862637_gshared)(__this, method)
// System.Void PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::TransitionObject(TweenPosition,System.Boolean)
#define PanelTransitionManager_1_TransitionObject_m589785004(__this, ___tweenPos0, ___isHiding1, method) ((  void (*) (PanelTransitionManager_1_t2526031859 *, TweenPosition_t1144714832 *, bool, const MethodInfo*))PanelTransitionManager_1_TransitionObject_m1292715937_gshared)(__this, ___tweenPos0, ___isHiding1, method)
// System.Collections.IEnumerator PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::TweenObjectForwards(TweenPosition)
#define PanelTransitionManager_1_TweenObjectForwards_m878154259(__this, ___tweenPos0, method) ((  Il2CppObject * (*) (PanelTransitionManager_1_t2526031859 *, TweenPosition_t1144714832 *, const MethodInfo*))PanelTransitionManager_1_TweenObjectForwards_m3624951864_gshared)(__this, ___tweenPos0, method)
// Panel PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::GetActivePanel()
#define PanelTransitionManager_1_GetActivePanel_m173370810(__this, method) ((  Panel_t1787746694 * (*) (PanelTransitionManager_1_t2526031859 *, const MethodInfo*))PanelTransitionManager_1_GetActivePanel_m1623511417_gshared)(__this, method)
// Panel PanelTransitionManager`1<ChatmageddonPanelTransitionManager>::GetPanel(PanelType)
#define PanelTransitionManager_1_GetPanel_m4264171628(__this, ___panelType0, method) ((  Panel_t1787746694 * (*) (PanelTransitionManager_1_t2526031859 *, int32_t, const MethodInfo*))PanelTransitionManager_1_GetPanel_m2500208523_gshared)(__this, ___panelType0, method)
