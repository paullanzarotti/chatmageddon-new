﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// User
struct User_t719925459;
// NoPointsWaitButton
struct NoPointsWaitButton_t1249750581;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoPointsWaitButton/<CalculateKnockoutTime>c__Iterator0
struct  U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588  : public Il2CppObject
{
public:
	// User NoPointsWaitButton/<CalculateKnockoutTime>c__Iterator0::user
	User_t719925459 * ___user_0;
	// System.TimeSpan NoPointsWaitButton/<CalculateKnockoutTime>c__Iterator0::<difference>__0
	TimeSpan_t3430258949  ___U3CdifferenceU3E__0_1;
	// NoPointsWaitButton NoPointsWaitButton/<CalculateKnockoutTime>c__Iterator0::$this
	NoPointsWaitButton_t1249750581 * ___U24this_2;
	// System.Object NoPointsWaitButton/<CalculateKnockoutTime>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean NoPointsWaitButton/<CalculateKnockoutTime>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 NoPointsWaitButton/<CalculateKnockoutTime>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_user_0() { return static_cast<int32_t>(offsetof(U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588, ___user_0)); }
	inline User_t719925459 * get_user_0() const { return ___user_0; }
	inline User_t719925459 ** get_address_of_user_0() { return &___user_0; }
	inline void set_user_0(User_t719925459 * value)
	{
		___user_0 = value;
		Il2CppCodeGenWriteBarrier(&___user_0, value);
	}

	inline static int32_t get_offset_of_U3CdifferenceU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588, ___U3CdifferenceU3E__0_1)); }
	inline TimeSpan_t3430258949  get_U3CdifferenceU3E__0_1() const { return ___U3CdifferenceU3E__0_1; }
	inline TimeSpan_t3430258949 * get_address_of_U3CdifferenceU3E__0_1() { return &___U3CdifferenceU3E__0_1; }
	inline void set_U3CdifferenceU3E__0_1(TimeSpan_t3430258949  value)
	{
		___U3CdifferenceU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588, ___U24this_2)); }
	inline NoPointsWaitButton_t1249750581 * get_U24this_2() const { return ___U24this_2; }
	inline NoPointsWaitButton_t1249750581 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(NoPointsWaitButton_t1249750581 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
