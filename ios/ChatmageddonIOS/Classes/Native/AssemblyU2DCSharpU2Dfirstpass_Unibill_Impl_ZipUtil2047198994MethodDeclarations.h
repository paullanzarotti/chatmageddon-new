﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.ZipUtils
struct ZipUtils_t2047198994;
// System.IO.Stream
struct Stream_t3255436806;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.ZipUtils::.ctor()
extern "C"  void ZipUtils__ctor_m3745980858 (ZipUtils_t2047198994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.ZipUtils::decompress(System.IO.Stream,System.String)
extern "C"  void ZipUtils_decompress_m116772288 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, String_t* ___outputPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.ZipUtils::Copy(System.IO.Stream,System.IO.Stream,System.Byte[])
extern "C"  void ZipUtils_Copy_m1972022810 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___source0, Stream_t3255436806 * ___destination1, ByteU5BU5D_t3397334013* ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
