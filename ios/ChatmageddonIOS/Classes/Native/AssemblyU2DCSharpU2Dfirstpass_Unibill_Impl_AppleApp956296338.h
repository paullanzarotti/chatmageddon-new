﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// System.Collections.Generic.HashSet`1<PurchasableItem>
struct HashSet_1_t2296814753;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.String
struct String_t;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IStoreKitPlugin
struct IStoreKitPlugin_t1675751103;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.AppleAppStoreBillingService
struct  AppleAppStoreBillingService_t956296338  : public Il2CppObject
{
public:
	// Unibill.Impl.IBillingServiceCallback Unibill.Impl.AppleAppStoreBillingService::biller
	Il2CppObject * ___biller_0;
	// Unibill.Impl.ProductIdRemapper Unibill.Impl.AppleAppStoreBillingService::remapper
	ProductIdRemapper_t3313438456 * ___remapper_1;
	// System.Collections.Generic.HashSet`1<PurchasableItem> Unibill.Impl.AppleAppStoreBillingService::products
	HashSet_1_t2296814753 * ___products_2;
	// System.Collections.Generic.HashSet`1<System.String> Unibill.Impl.AppleAppStoreBillingService::productsNotReturnedByStorekit
	HashSet_1_t362681087 * ___productsNotReturnedByStorekit_3;
	// System.String Unibill.Impl.AppleAppStoreBillingService::appReceipt
	String_t* ___appReceipt_4;
	// Uniject.ILogger Unibill.Impl.AppleAppStoreBillingService::logger
	Il2CppObject * ___logger_5;
	// Unibill.Impl.IStoreKitPlugin Unibill.Impl.AppleAppStoreBillingService::<storekit>k__BackingField
	Il2CppObject * ___U3CstorekitU3Ek__BackingField_6;
	// System.Boolean Unibill.Impl.AppleAppStoreBillingService::restoreInProgress
	bool ___restoreInProgress_7;

public:
	inline static int32_t get_offset_of_biller_0() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___biller_0)); }
	inline Il2CppObject * get_biller_0() const { return ___biller_0; }
	inline Il2CppObject ** get_address_of_biller_0() { return &___biller_0; }
	inline void set_biller_0(Il2CppObject * value)
	{
		___biller_0 = value;
		Il2CppCodeGenWriteBarrier(&___biller_0, value);
	}

	inline static int32_t get_offset_of_remapper_1() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___remapper_1)); }
	inline ProductIdRemapper_t3313438456 * get_remapper_1() const { return ___remapper_1; }
	inline ProductIdRemapper_t3313438456 ** get_address_of_remapper_1() { return &___remapper_1; }
	inline void set_remapper_1(ProductIdRemapper_t3313438456 * value)
	{
		___remapper_1 = value;
		Il2CppCodeGenWriteBarrier(&___remapper_1, value);
	}

	inline static int32_t get_offset_of_products_2() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___products_2)); }
	inline HashSet_1_t2296814753 * get_products_2() const { return ___products_2; }
	inline HashSet_1_t2296814753 ** get_address_of_products_2() { return &___products_2; }
	inline void set_products_2(HashSet_1_t2296814753 * value)
	{
		___products_2 = value;
		Il2CppCodeGenWriteBarrier(&___products_2, value);
	}

	inline static int32_t get_offset_of_productsNotReturnedByStorekit_3() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___productsNotReturnedByStorekit_3)); }
	inline HashSet_1_t362681087 * get_productsNotReturnedByStorekit_3() const { return ___productsNotReturnedByStorekit_3; }
	inline HashSet_1_t362681087 ** get_address_of_productsNotReturnedByStorekit_3() { return &___productsNotReturnedByStorekit_3; }
	inline void set_productsNotReturnedByStorekit_3(HashSet_1_t362681087 * value)
	{
		___productsNotReturnedByStorekit_3 = value;
		Il2CppCodeGenWriteBarrier(&___productsNotReturnedByStorekit_3, value);
	}

	inline static int32_t get_offset_of_appReceipt_4() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___appReceipt_4)); }
	inline String_t* get_appReceipt_4() const { return ___appReceipt_4; }
	inline String_t** get_address_of_appReceipt_4() { return &___appReceipt_4; }
	inline void set_appReceipt_4(String_t* value)
	{
		___appReceipt_4 = value;
		Il2CppCodeGenWriteBarrier(&___appReceipt_4, value);
	}

	inline static int32_t get_offset_of_logger_5() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___logger_5)); }
	inline Il2CppObject * get_logger_5() const { return ___logger_5; }
	inline Il2CppObject ** get_address_of_logger_5() { return &___logger_5; }
	inline void set_logger_5(Il2CppObject * value)
	{
		___logger_5 = value;
		Il2CppCodeGenWriteBarrier(&___logger_5, value);
	}

	inline static int32_t get_offset_of_U3CstorekitU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___U3CstorekitU3Ek__BackingField_6)); }
	inline Il2CppObject * get_U3CstorekitU3Ek__BackingField_6() const { return ___U3CstorekitU3Ek__BackingField_6; }
	inline Il2CppObject ** get_address_of_U3CstorekitU3Ek__BackingField_6() { return &___U3CstorekitU3Ek__BackingField_6; }
	inline void set_U3CstorekitU3Ek__BackingField_6(Il2CppObject * value)
	{
		___U3CstorekitU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstorekitU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_restoreInProgress_7() { return static_cast<int32_t>(offsetof(AppleAppStoreBillingService_t956296338, ___restoreInProgress_7)); }
	inline bool get_restoreInProgress_7() const { return ___restoreInProgress_7; }
	inline bool* get_address_of_restoreInProgress_7() { return &___restoreInProgress_7; }
	inline void set_restoreInProgress_7(bool value)
	{
		___restoreInProgress_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
