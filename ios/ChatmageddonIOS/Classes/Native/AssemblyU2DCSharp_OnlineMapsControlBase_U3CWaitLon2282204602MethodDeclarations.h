﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsControlBase/<WaitLongPress>c__Iterator2
struct U3CWaitLongPressU3Ec__Iterator2_t2282204602;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OnlineMapsControlBase/<WaitLongPress>c__Iterator2::.ctor()
extern "C"  void U3CWaitLongPressU3Ec__Iterator2__ctor_m439403293 (U3CWaitLongPressU3Ec__Iterator2_t2282204602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsControlBase/<WaitLongPress>c__Iterator2::MoveNext()
extern "C"  bool U3CWaitLongPressU3Ec__Iterator2_MoveNext_m2513966143 (U3CWaitLongPressU3Ec__Iterator2_t2282204602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsControlBase/<WaitLongPress>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitLongPressU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3821698027 (U3CWaitLongPressU3Ec__Iterator2_t2282204602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsControlBase/<WaitLongPress>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitLongPressU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m4286746371 (U3CWaitLongPressU3Ec__Iterator2_t2282204602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsControlBase/<WaitLongPress>c__Iterator2::Dispose()
extern "C"  void U3CWaitLongPressU3Ec__Iterator2_Dispose_m2330381924 (U3CWaitLongPressU3Ec__Iterator2_t2282204602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsControlBase/<WaitLongPress>c__Iterator2::Reset()
extern "C"  void U3CWaitLongPressU3Ec__Iterator2_Reset_m1792498850 (U3CWaitLongPressU3Ec__Iterator2_t2282204602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
