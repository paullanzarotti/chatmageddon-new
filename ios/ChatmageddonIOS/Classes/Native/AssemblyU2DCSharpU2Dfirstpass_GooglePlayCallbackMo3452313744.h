﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.GooglePlayBillingService
struct GooglePlayBillingService_t2494001613;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayCallbackMonoBehaviour
struct  GooglePlayCallbackMonoBehaviour_t3452313744  : public MonoBehaviour_t1158329972
{
public:
	// Unibill.Impl.GooglePlayBillingService GooglePlayCallbackMonoBehaviour::callback
	GooglePlayBillingService_t2494001613 * ___callback_2;

public:
	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(GooglePlayCallbackMonoBehaviour_t3452313744, ___callback_2)); }
	inline GooglePlayBillingService_t2494001613 * get_callback_2() const { return ___callback_2; }
	inline GooglePlayBillingService_t2494001613 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(GooglePlayBillingService_t2494001613 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
