﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileFriendButton
struct PlayerProfileFriendButton_t2898170674;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerProfileFriendButton::.ctor()
extern "C"  void PlayerProfileFriendButton__ctor_m4032939903 (PlayerProfileFriendButton_t2898170674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileFriendButton::OnButtonClick()
extern "C"  void PlayerProfileFriendButton_OnButtonClick_m492430214 (PlayerProfileFriendButton_t2898170674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
