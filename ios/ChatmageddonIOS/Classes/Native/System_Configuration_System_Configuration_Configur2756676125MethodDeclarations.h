﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationConverterBase
struct ConfigurationConverterBase_t2756676125;
// System.ComponentModel.ITypeDescriptorContext
struct ITypeDescriptorContext_t3633625151;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Configuration.ConfigurationConverterBase::.ctor()
extern "C"  void ConfigurationConverterBase__ctor_m2121451110 (ConfigurationConverterBase_t2756676125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationConverterBase::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool ConfigurationConverterBase_CanConvertFrom_m2812912026 (ConfigurationConverterBase_t2756676125 * __this, Il2CppObject * ___ctx0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationConverterBase::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool ConfigurationConverterBase_CanConvertTo_m2409092635 (ConfigurationConverterBase_t2756676125 * __this, Il2CppObject * ___ctx0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
