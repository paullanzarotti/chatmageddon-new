﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NonTimedErrorMessage
struct  NonTimedErrorMessage_t4057427095  : public SFXButton_t792651341
{
public:
	// System.String NonTimedErrorMessage::messageContent
	String_t* ___messageContent_5;

public:
	inline static int32_t get_offset_of_messageContent_5() { return static_cast<int32_t>(offsetof(NonTimedErrorMessage_t4057427095, ___messageContent_5)); }
	inline String_t* get_messageContent_5() const { return ___messageContent_5; }
	inline String_t** get_address_of_messageContent_5() { return &___messageContent_5; }
	inline void set_messageContent_5(String_t* value)
	{
		___messageContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___messageContent_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
