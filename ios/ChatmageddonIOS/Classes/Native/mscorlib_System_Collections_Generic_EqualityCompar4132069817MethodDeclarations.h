﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<PhoneNumberNavScreen>
struct EqualityComparer_1_t4132069817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<PhoneNumberNavScreen>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m423735491_gshared (EqualityComparer_1_t4132069817 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m423735491(__this, method) ((  void (*) (EqualityComparer_1_t4132069817 *, const MethodInfo*))EqualityComparer_1__ctor_m423735491_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<PhoneNumberNavScreen>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2648612180_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2648612180(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2648612180_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<PhoneNumberNavScreen>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m132300222_gshared (EqualityComparer_1_t4132069817 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m132300222(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t4132069817 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m132300222_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<PhoneNumberNavScreen>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1669181124_gshared (EqualityComparer_1_t4132069817 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1669181124(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t4132069817 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1669181124_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<PhoneNumberNavScreen>::get_Default()
extern "C"  EqualityComparer_1_t4132069817 * EqualityComparer_1_get_Default_m3015819883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m3015819883(__this /* static, unused */, method) ((  EqualityComparer_1_t4132069817 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m3015819883_gshared)(__this /* static, unused */, method)
