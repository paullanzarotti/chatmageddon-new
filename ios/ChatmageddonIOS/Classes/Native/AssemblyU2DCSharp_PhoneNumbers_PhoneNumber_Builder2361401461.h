﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumber/Builder
struct  Builder_t2361401461  : public Il2CppObject
{
public:
	// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber/Builder::result
	PhoneNumber_t814071929 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(Builder_t2361401461, ___result_0)); }
	inline PhoneNumber_t814071929 * get_result_0() const { return ___result_0; }
	inline PhoneNumber_t814071929 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(PhoneNumber_t814071929 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
