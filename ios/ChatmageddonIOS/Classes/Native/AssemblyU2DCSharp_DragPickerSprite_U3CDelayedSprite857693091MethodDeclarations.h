﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1
struct U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::.ctor()
extern "C"  void U3CDelayedSpriteAppearanceU3Ec__Iterator1__ctor_m2043335590 (U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::MoveNext()
extern "C"  bool U3CDelayedSpriteAppearanceU3Ec__Iterator1_MoveNext_m3608383994 (U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayedSpriteAppearanceU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2590211306 (U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayedSpriteAppearanceU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2371129778 (U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::Dispose()
extern "C"  void U3CDelayedSpriteAppearanceU3Ec__Iterator1_Dispose_m3266423089 (U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPickerSprite/<DelayedSpriteAppearance>c__Iterator1::Reset()
extern "C"  void U3CDelayedSpriteAppearanceU3Ec__Iterator1_Reset_m2879112823 (U3CDelayedSpriteAppearanceU3Ec__Iterator1_t857693091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
