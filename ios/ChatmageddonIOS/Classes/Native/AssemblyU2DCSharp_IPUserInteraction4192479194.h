﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPCycler
struct IPCycler_t1336138445;
// IPUserInteraction/OnPickerClicked
struct OnPickerClicked_t2482807019;
// IPUserInteraction/OnDragExit
struct OnDragExit_t3593337772;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPUserInteraction
struct  IPUserInteraction_t4192479194  : public MonoBehaviour_t1158329972
{
public:
	// IPCycler IPUserInteraction::cycler
	IPCycler_t1336138445 * ___cycler_2;
	// System.Boolean IPUserInteraction::restrictWithinPicker
	bool ___restrictWithinPicker_3;
	// System.Single IPUserInteraction::exitRecenterDelay
	float ___exitRecenterDelay_4;
	// System.Boolean IPUserInteraction::_isScrolling
	bool ____isScrolling_5;
	// System.Boolean IPUserInteraction::_isPressed
	bool ____isPressed_6;
	// System.Boolean IPUserInteraction::_isDraggingOutsideOfPicker
	bool ____isDraggingOutsideOfPicker_7;
	// IPUserInteraction/OnPickerClicked IPUserInteraction::onPickerClicked
	OnPickerClicked_t2482807019 * ___onPickerClicked_8;
	// IPUserInteraction/OnDragExit IPUserInteraction::onDragExit
	OnDragExit_t3593337772 * ___onDragExit_9;

public:
	inline static int32_t get_offset_of_cycler_2() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ___cycler_2)); }
	inline IPCycler_t1336138445 * get_cycler_2() const { return ___cycler_2; }
	inline IPCycler_t1336138445 ** get_address_of_cycler_2() { return &___cycler_2; }
	inline void set_cycler_2(IPCycler_t1336138445 * value)
	{
		___cycler_2 = value;
		Il2CppCodeGenWriteBarrier(&___cycler_2, value);
	}

	inline static int32_t get_offset_of_restrictWithinPicker_3() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ___restrictWithinPicker_3)); }
	inline bool get_restrictWithinPicker_3() const { return ___restrictWithinPicker_3; }
	inline bool* get_address_of_restrictWithinPicker_3() { return &___restrictWithinPicker_3; }
	inline void set_restrictWithinPicker_3(bool value)
	{
		___restrictWithinPicker_3 = value;
	}

	inline static int32_t get_offset_of_exitRecenterDelay_4() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ___exitRecenterDelay_4)); }
	inline float get_exitRecenterDelay_4() const { return ___exitRecenterDelay_4; }
	inline float* get_address_of_exitRecenterDelay_4() { return &___exitRecenterDelay_4; }
	inline void set_exitRecenterDelay_4(float value)
	{
		___exitRecenterDelay_4 = value;
	}

	inline static int32_t get_offset_of__isScrolling_5() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ____isScrolling_5)); }
	inline bool get__isScrolling_5() const { return ____isScrolling_5; }
	inline bool* get_address_of__isScrolling_5() { return &____isScrolling_5; }
	inline void set__isScrolling_5(bool value)
	{
		____isScrolling_5 = value;
	}

	inline static int32_t get_offset_of__isPressed_6() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ____isPressed_6)); }
	inline bool get__isPressed_6() const { return ____isPressed_6; }
	inline bool* get_address_of__isPressed_6() { return &____isPressed_6; }
	inline void set__isPressed_6(bool value)
	{
		____isPressed_6 = value;
	}

	inline static int32_t get_offset_of__isDraggingOutsideOfPicker_7() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ____isDraggingOutsideOfPicker_7)); }
	inline bool get__isDraggingOutsideOfPicker_7() const { return ____isDraggingOutsideOfPicker_7; }
	inline bool* get_address_of__isDraggingOutsideOfPicker_7() { return &____isDraggingOutsideOfPicker_7; }
	inline void set__isDraggingOutsideOfPicker_7(bool value)
	{
		____isDraggingOutsideOfPicker_7 = value;
	}

	inline static int32_t get_offset_of_onPickerClicked_8() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ___onPickerClicked_8)); }
	inline OnPickerClicked_t2482807019 * get_onPickerClicked_8() const { return ___onPickerClicked_8; }
	inline OnPickerClicked_t2482807019 ** get_address_of_onPickerClicked_8() { return &___onPickerClicked_8; }
	inline void set_onPickerClicked_8(OnPickerClicked_t2482807019 * value)
	{
		___onPickerClicked_8 = value;
		Il2CppCodeGenWriteBarrier(&___onPickerClicked_8, value);
	}

	inline static int32_t get_offset_of_onDragExit_9() { return static_cast<int32_t>(offsetof(IPUserInteraction_t4192479194, ___onDragExit_9)); }
	inline OnDragExit_t3593337772 * get_onDragExit_9() const { return ___onDragExit_9; }
	inline OnDragExit_t3593337772 ** get_address_of_onDragExit_9() { return &___onDragExit_9; }
	inline void set_onDragExit_9(OnDragExit_t3593337772 * value)
	{
		___onDragExit_9 = value;
		Il2CppCodeGenWriteBarrier(&___onDragExit_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
