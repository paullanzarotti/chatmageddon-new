﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::.ctor()
#define List_1__ctor_m1364706529(__this, method) ((  void (*) (List_1_t3205981984 *, const MethodInfo*))List_1__ctor_m2944773907_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m79436733(__this, ___collection0, method) ((  void (*) (List_1_t3205981984 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3642371895_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::.ctor(System.Int32)
#define List_1__ctor_m2994452031(__this, ___capacity0, method) ((  void (*) (List_1_t3205981984 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::.ctor(T[],System.Int32)
#define List_1__ctor_m80317105(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3205981984 *, OnlineMapsFindAutocompleteResultTermU5BU5D_t2041652477*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::.cctor()
#define List_1__cctor_m3078100661(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2678985564(__this, method) ((  Il2CppObject* (*) (List_1_t3205981984 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m29977118(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3205981984 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3202417851(__this, method) ((  Il2CppObject * (*) (List_1_t3205981984 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2717762810(__this, ___item0, method) ((  int32_t (*) (List_1_t3205981984 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3234236380(__this, ___item0, method) ((  bool (*) (List_1_t3205981984 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m4221930664(__this, ___item0, method) ((  int32_t (*) (List_1_t3205981984 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m4099517963(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3205981984 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1702984547(__this, ___item0, method) ((  void (*) (List_1_t3205981984 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3350122775(__this, method) ((  bool (*) (List_1_t3205981984 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3033785810(__this, method) ((  bool (*) (List_1_t3205981984 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3869789140(__this, method) ((  Il2CppObject * (*) (List_1_t3205981984 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1206149747(__this, method) ((  bool (*) (List_1_t3205981984 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3475254390(__this, method) ((  bool (*) (List_1_t3205981984 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1221322519(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3205981984 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3988526400(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3205981984 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Add(T)
#define List_1_Add_m1733215869(__this, ___item0, method) ((  void (*) (List_1_t3205981984 *, OnlineMapsFindAutocompleteResultTerm_t3836860852 *, const MethodInfo*))List_1_Add_m2016287831_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2817633492(__this, ___newCount0, method) ((  void (*) (List_1_t3205981984 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1868140335(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3205981984 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1338130612(__this, ___collection0, method) ((  void (*) (List_1_t3205981984 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3384198020(__this, ___enumerable0, method) ((  void (*) (List_1_t3205981984 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2212863271(__this, ___collection0, method) ((  void (*) (List_1_t3205981984 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1153521819_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::AsReadOnly()
#define List_1_AsReadOnly_m1420096142(__this, method) ((  ReadOnlyCollection_1_t4022646544 * (*) (List_1_t3205981984 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Clear()
#define List_1_Clear_m3209701179(__this, method) ((  void (*) (List_1_t3205981984 *, const MethodInfo*))List_1_Clear_m1130211784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Contains(T)
#define List_1_Contains_m1490686861(__this, ___item0, method) ((  bool (*) (List_1_t3205981984 *, OnlineMapsFindAutocompleteResultTerm_t3836860852 *, const MethodInfo*))List_1_Contains_m3004503139_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::CopyTo(T[])
#define List_1_CopyTo_m3988311282(__this, ___array0, method) ((  void (*) (List_1_t3205981984 *, OnlineMapsFindAutocompleteResultTermU5BU5D_t2041652477*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m181161235(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3205981984 *, OnlineMapsFindAutocompleteResultTermU5BU5D_t2041652477*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m952647191(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t3205981984 *, int32_t, OnlineMapsFindAutocompleteResultTermU5BU5D_t2041652477*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3378929717_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m3693329887(__this, ___match0, method) ((  bool (*) (List_1_t3205981984 *, Predicate_1_t2279830967 *, const MethodInfo*))List_1_Exists_m1496178069_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Find(System.Predicate`1<T>)
#define List_1_Find_m3568991373(__this, ___match0, method) ((  OnlineMapsFindAutocompleteResultTerm_t3836860852 * (*) (List_1_t3205981984 *, Predicate_1_t2279830967 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2127625062(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2279830967 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3733291490(__this, ___match0, method) ((  List_1_t3205981984 * (*) (List_1_t3205981984 *, Predicate_1_t2279830967 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m2531475102(__this, ___match0, method) ((  List_1_t3205981984 * (*) (List_1_t3205981984 *, Predicate_1_t2279830967 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m2423501070(__this, ___match0, method) ((  List_1_t3205981984 * (*) (List_1_t3205981984 *, Predicate_1_t2279830967 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m2701685212(__this, ___match0, method) ((  int32_t (*) (List_1_t3205981984 *, Predicate_1_t2279830967 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2508248317(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3205981984 *, int32_t, int32_t, Predicate_1_t2279830967 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2501361154(__this, ___action0, method) ((  void (*) (List_1_t3205981984 *, Action_1_t3638660234 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::GetEnumerator()
#define List_1_GetEnumerator_m2479579986(__this, method) ((  Enumerator_t2740711658  (*) (List_1_t3205981984 *, const MethodInfo*))List_1_GetEnumerator_m1278946119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::IndexOf(T)
#define List_1_IndexOf_m637995927(__this, ___item0, method) ((  int32_t (*) (List_1_t3205981984 *, OnlineMapsFindAutocompleteResultTerm_t3836860852 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3393925442(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3205981984 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3266496467(__this, ___index0, method) ((  void (*) (List_1_t3205981984 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Insert(System.Int32,T)
#define List_1_Insert_m1492932804(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3205981984 *, int32_t, OnlineMapsFindAutocompleteResultTerm_t3836860852 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3283551021(__this, ___collection0, method) ((  void (*) (List_1_t3205981984 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Remove(T)
#define List_1_Remove_m2750904762(__this, ___item0, method) ((  bool (*) (List_1_t3205981984 *, OnlineMapsFindAutocompleteResultTerm_t3836860852 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m643379290(__this, ___match0, method) ((  int32_t (*) (List_1_t3205981984 *, Predicate_1_t2279830967 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1036606216(__this, ___index0, method) ((  void (*) (List_1_t3205981984 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m2496702559(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3205981984 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Reverse()
#define List_1_Reverse_m426243932(__this, method) ((  void (*) (List_1_t3205981984 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Sort()
#define List_1_Sort_m2913513916(__this, method) ((  void (*) (List_1_t3205981984 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3886917616(__this, ___comparer0, method) ((  void (*) (List_1_t3205981984 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1970903981(__this, ___comparison0, method) ((  void (*) (List_1_t3205981984 *, Comparison_1_t803632407 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::ToArray()
#define List_1_ToArray_m2109635283(__this, method) ((  OnlineMapsFindAutocompleteResultTermU5BU5D_t2041652477* (*) (List_1_t3205981984 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::TrimExcess()
#define List_1_TrimExcess_m2903453191(__this, method) ((  void (*) (List_1_t3205981984 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::get_Capacity()
#define List_1_get_Capacity_m3467779009(__this, method) ((  int32_t (*) (List_1_t3205981984 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1003259316(__this, ___value0, method) ((  void (*) (List_1_t3205981984 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::get_Count()
#define List_1_get_Count_m1198543050(__this, method) ((  int32_t (*) (List_1_t3205981984 *, const MethodInfo*))List_1_get_Count_m2021003857_gshared)(__this, method)
// T System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::get_Item(System.Int32)
#define List_1_get_Item_m680892052(__this, ___index0, method) ((  OnlineMapsFindAutocompleteResultTerm_t3836860852 * (*) (List_1_t3205981984 *, int32_t, const MethodInfo*))List_1_get_Item_m1607739884_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<OnlineMapsFindAutocompleteResultTerm>::set_Item(System.Int32,T)
#define List_1_set_Item_m1917315575(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3205981984 *, int32_t, OnlineMapsFindAutocompleteResultTerm_t3836860852 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
