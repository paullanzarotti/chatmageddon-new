﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Bread>::.ctor()
#define List_1__ctor_m436519869(__this, method) ((  void (*) (List_1_t4121260458 *, const MethodInfo*))List_1__ctor_m2944773907_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Bread>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3930764297(__this, ___collection0, method) ((  void (*) (List_1_t4121260458 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3642371895_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Bread>::.ctor(System.Int32)
#define List_1__ctor_m1032028143(__this, ___capacity0, method) ((  void (*) (List_1_t4121260458 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Bread>::.ctor(T[],System.Int32)
#define List_1__ctor_m2207799925(__this, ___data0, ___size1, method) ((  void (*) (List_1_t4121260458 *, BreadU5BU5D_t1789099275*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<Bread>::.cctor()
#define List_1__cctor_m2633074281(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Bread>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2844268016(__this, method) ((  Il2CppObject* (*) (List_1_t4121260458 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Bread>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1632172090(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4121260458 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Bread>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m550921435(__this, method) ((  Il2CppObject * (*) (List_1_t4121260458 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3038885182(__this, ___item0, method) ((  int32_t (*) (List_1_t4121260458 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2324399592(__this, ___item0, method) ((  bool (*) (List_1_t4121260458 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3695971716(__this, ___item0, method) ((  int32_t (*) (List_1_t4121260458 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Bread>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m230518027(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4121260458 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Bread>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m4236051195(__this, ___item0, method) ((  void (*) (List_1_t4121260458 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2468014031(__this, method) ((  bool (*) (List_1_t4121260458 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4246138830(__this, method) ((  bool (*) (List_1_t4121260458 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Bread>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m212406698(__this, method) ((  Il2CppObject * (*) (List_1_t4121260458 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m4172690211(__this, method) ((  bool (*) (List_1_t4121260458 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1815709946(__this, method) ((  bool (*) (List_1_t4121260458 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Bread>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1998646435(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4121260458 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Bread>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2639780496(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4121260458 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Bread>::Add(T)
#define List_1_Add_m1835309713(__this, ___item0, method) ((  void (*) (List_1_t4121260458 *, Bread_t457172030 *, const MethodInfo*))List_1_Add_m2016287831_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Bread>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m457109992(__this, ___newCount0, method) ((  void (*) (List_1_t4121260458 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Bread>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m473929991(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4121260458 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Bread>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2955995232(__this, ___collection0, method) ((  void (*) (List_1_t4121260458 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Bread>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3143471792(__this, ___enumerable0, method) ((  void (*) (List_1_t4121260458 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Bread>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m4214047391(__this, ___collection0, method) ((  void (*) (List_1_t4121260458 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1153521819_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Bread>::AsReadOnly()
#define List_1_AsReadOnly_m1709051954(__this, method) ((  ReadOnlyCollection_1_t642957722 * (*) (List_1_t4121260458 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Bread>::Clear()
#define List_1_Clear_m2829003539(__this, method) ((  void (*) (List_1_t4121260458 *, const MethodInfo*))List_1_Clear_m1130211784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::Contains(T)
#define List_1_Contains_m3234350777(__this, ___item0, method) ((  bool (*) (List_1_t4121260458 *, Bread_t457172030 *, const MethodInfo*))List_1_Contains_m3004503139_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Bread>::CopyTo(T[])
#define List_1_CopyTo_m346780902(__this, ___array0, method) ((  void (*) (List_1_t4121260458 *, BreadU5BU5D_t1789099275*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<Bread>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4045628395(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4121260458 *, BreadU5BU5D_t1789099275*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<Bread>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m2211626423(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t4121260458 *, int32_t, BreadU5BU5D_t1789099275*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3378929717_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m1224517223(__this, ___match0, method) ((  bool (*) (List_1_t4121260458 *, Predicate_1_t3195109441 *, const MethodInfo*))List_1_Exists_m1496178069_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<Bread>::Find(System.Predicate`1<T>)
#define List_1_Find_m3360455281(__this, ___match0, method) ((  Bread_t457172030 * (*) (List_1_t4121260458 *, Predicate_1_t3195109441 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Bread>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3047434394(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3195109441 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Bread>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m3546182252(__this, ___match0, method) ((  List_1_t4121260458 * (*) (List_1_t4121260458 *, Predicate_1_t3195109441 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Bread>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m2797793952(__this, ___match0, method) ((  List_1_t4121260458 * (*) (List_1_t4121260458 *, Predicate_1_t3195109441 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<Bread>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m252390416(__this, ___match0, method) ((  List_1_t4121260458 * (*) (List_1_t4121260458 *, Predicate_1_t3195109441 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m2760495320(__this, ___match0, method) ((  int32_t (*) (List_1_t4121260458 *, Predicate_1_t3195109441 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3425884757(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4121260458 *, int32_t, int32_t, Predicate_1_t3195109441 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<Bread>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m1332974714(__this, ___action0, method) ((  void (*) (List_1_t4121260458 *, Action_1_t258971412 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Bread>::GetEnumerator()
#define List_1_GetEnumerator_m1512235926(__this, method) ((  Enumerator_t3655990132  (*) (List_1_t4121260458 *, const MethodInfo*))List_1_GetEnumerator_m1278946119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::IndexOf(T)
#define List_1_IndexOf_m2486945887(__this, ___item0, method) ((  int32_t (*) (List_1_t4121260458 *, Bread_t457172030 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Bread>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m4057110446(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4121260458 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Bread>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m4127126403(__this, ___index0, method) ((  void (*) (List_1_t4121260458 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Bread>::Insert(System.Int32,T)
#define List_1_Insert_m3427431636(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4121260458 *, int32_t, Bread_t457172030 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Bread>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m503544905(__this, ___collection0, method) ((  void (*) (List_1_t4121260458 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Bread>::Remove(T)
#define List_1_Remove_m137424546(__this, ___item0, method) ((  bool (*) (List_1_t4121260458 *, Bread_t457172030 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2667005366(__this, ___match0, method) ((  int32_t (*) (List_1_t4121260458 *, Predicate_1_t3195109441 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Bread>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m980188427(__this, ___index0, method) ((  void (*) (List_1_t4121260458 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Bread>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m3036436255(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4121260458 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Bread>::Reverse()
#define List_1_Reverse_m3535693232(__this, method) ((  void (*) (List_1_t4121260458 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Bread>::Sort()
#define List_1_Sort_m4027000780(__this, method) ((  void (*) (List_1_t4121260458 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Bread>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1927509736(__this, ___comparer0, method) ((  void (*) (List_1_t4121260458 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Bread>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3351802129(__this, ___comparison0, method) ((  void (*) (List_1_t4121260458 *, Comparison_1_t1718910881 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Bread>::ToArray()
#define List_1_ToArray_m3223527201(__this, method) ((  BreadU5BU5D_t1789099275* (*) (List_1_t4121260458 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Bread>::TrimExcess()
#define List_1_TrimExcess_m1823820911(__this, method) ((  void (*) (List_1_t4121260458 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::get_Capacity()
#define List_1_get_Capacity_m3649089729(__this, method) ((  int32_t (*) (List_1_t4121260458 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Bread>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4214239040(__this, ___value0, method) ((  void (*) (List_1_t4121260458 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Bread>::get_Count()
#define List_1_get_Count_m3294123665(__this, method) ((  int32_t (*) (List_1_t4121260458 *, const MethodInfo*))List_1_get_Count_m2021003857_gshared)(__this, method)
// T System.Collections.Generic.List`1<Bread>::get_Item(System.Int32)
#define List_1_get_Item_m2732365382(__this, ___index0, method) ((  Bread_t457172030 * (*) (List_1_t4121260458 *, int32_t, const MethodInfo*))List_1_get_Item_m1607739884_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Bread>::set_Item(System.Int32,T)
#define List_1_set_Item_m2237906271(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4121260458 *, int32_t, Bread_t457172030 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
