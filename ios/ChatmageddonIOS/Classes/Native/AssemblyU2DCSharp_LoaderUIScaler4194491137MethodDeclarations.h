﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoaderUIScaler
struct LoaderUIScaler_t4194491137;

#include "codegen/il2cpp-codegen.h"

// System.Void LoaderUIScaler::.ctor()
extern "C"  void LoaderUIScaler__ctor_m4072418528 (LoaderUIScaler_t4194491137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoaderUIScaler::GlobalUIScale()
extern "C"  void LoaderUIScaler_GlobalUIScale_m3040856651 (LoaderUIScaler_t4194491137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
