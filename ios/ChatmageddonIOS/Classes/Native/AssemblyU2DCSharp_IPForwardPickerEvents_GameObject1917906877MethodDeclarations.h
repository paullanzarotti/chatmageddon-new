﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPForwardPickerEvents/GameObjectAndMessage
struct GameObjectAndMessage_t1917906877;

#include "codegen/il2cpp-codegen.h"

// System.Void IPForwardPickerEvents/GameObjectAndMessage::.ctor()
extern "C"  void GameObjectAndMessage__ctor_m1330611470 (GameObjectAndMessage_t1917906877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
