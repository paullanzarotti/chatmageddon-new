﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ApplicationManager_U3CLogoutFromS833513242.h"
#include "AssemblyU2DCSharp_ApplicationManager_U3CScaleAndHi3742978486.h"
#include "AssemblyU2DCSharp_ApplicationManager_U3CScaleAndSh3962984058.h"
#include "AssemblyU2DCSharp_AvatarChatButton228579527.h"
#include "AssemblyU2DCSharp_AvatarChatUI2524884989.h"
#include "AssemblyU2DCSharp_AvatarUpdater2404165026.h"
#include "AssemblyU2DCSharp_AvatarUpdater_U3CCheckAvatarLoad1005977896.h"
#include "AssemblyU2DCSharp_FirstStrikeUI2168768222.h"
#include "AssemblyU2DCSharp_ChatMessage2384228687.h"
#include "AssemblyU2DCSharp_ChatThread2394323482.h"
#include "AssemblyU2DCSharp_ChatmageddonAudioManager766956996.h"
#include "AssemblyU2DCSharp_ChatmageddonSaveData4036050876.h"
#include "AssemblyU2DCSharp_ConfigManager2239702727.h"
#include "AssemblyU2DCSharp_LanguageConfig1338327304.h"
#include "AssemblyU2DCSharp_LanguageConfig_U3CGetLanguageStr3940050384.h"
#include "AssemblyU2DCSharp_LanguageModel2428340347.h"
#include "AssemblyU2DCSharp_ValidContact1479914934.h"
#include "AssemblyU2DCSharp_AddressBook411411037.h"
#include "AssemblyU2DCSharp_AddressBook_U3CLoadIosContactsU33073646581.h"
#include "AssemblyU2DCSharp_AddressBook_U3CDeleteContactsBat1321978283.h"
#include "AssemblyU2DCSharp_AddressBookContact330818429.h"
#include "AssemblyU2DCSharp_CancelButton1146598418.h"
#include "AssemblyU2DCSharp_ChooseExistingButton274093920.h"
#include "AssemblyU2DCSharp_DeviceCameraButton2936622169.h"
#include "AssemblyU2DCSharp_CameraRequestLocation942914719.h"
#include "AssemblyU2DCSharp_DeviceCameraManager1478748484.h"
#include "AssemblyU2DCSharp_TakePhotoButton1020298243.h"
#include "AssemblyU2DCSharp_DeviceCameraUIScaler3087545317.h"
#include "AssemblyU2DCSharp_CancelDeviceShareButton2424610771.h"
#include "AssemblyU2DCSharp_ShareRequestLocation2965642179.h"
#include "AssemblyU2DCSharp_ChatmageddonDeviceShareManager740212713.h"
#include "AssemblyU2DCSharp_DeviceShareButton1871825377.h"
#include "AssemblyU2DCSharp_DeviceShareUIScaler3461564645.h"
#include "AssemblyU2DCSharp_EmailShareButton2679829333.h"
#include "AssemblyU2DCSharp_FacebookShareButton509072349.h"
#include "AssemblyU2DCSharp_LableType1010844952.h"
#include "AssemblyU2DCSharp_GameLabelPopulator3519752206.h"
#include "AssemblyU2DCSharp_SMSShareButton1083588064.h"
#include "AssemblyU2DCSharp_TwitterShareButton638004942.h"
#include "AssemblyU2DCSharp_DoubleStateButton1032633262.h"
#include "AssemblyU2DCSharp_FlurryEvent3996959420.h"
#include "AssemblyU2DCSharp_FlurryController2385863972.h"
#include "AssemblyU2DCSharp_FormatDateTime1158606858.h"
#include "AssemblyU2DCSharp_FriendAvatarUpdater4187090262.h"
#include "AssemblyU2DCSharp_FriendAvatarUpdater_U3CCheckAvat3168305000.h"
#include "AssemblyU2DCSharp_Bucks3932015720.h"
#include "AssemblyU2DCSharp_Item2440468191.h"
#include "AssemblyU2DCSharp_ItemsManager2342504123.h"
#include "AssemblyU2DCSharp_CostType3548590213.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"
#include "AssemblyU2DCSharp_PurchaseableItem3351122996.h"
#include "AssemblyU2DCSharp_ItemType3455197591.h"
#include "AssemblyU2DCSharp_QuantitativeItem3036513780.h"
#include "AssemblyU2DCSharp_Fuel1015546524.h"
#include "AssemblyU2DCSharp_LaunchedItemType186177771.h"
#include "AssemblyU2DCSharp_LaunchDirection3410220992.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427.h"
#include "AssemblyU2DCSharp_MineResultStatus1141738764.h"
#include "AssemblyU2DCSharp_LaunchedMine3677282773.h"
#include "AssemblyU2DCSharp_MissileStatus1722383730.h"
#include "AssemblyU2DCSharp_MissileResultStatus3272208871.h"
#include "AssemblyU2DCSharp_WarefareState701596006.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "AssemblyU2DCSharp_MineStatus2219122381.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_ShieldType96263953.h"
#include "AssemblyU2DCSharp_ShieldProtection4135544898.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "AssemblyU2DCSharp_KnockoutCalculator371618716.h"
#include "AssemblyU2DCSharp_KnockoutCalculator_OnZeroHit581944179.h"
#include "AssemblyU2DCSharp_KnockoutCalculator_U3CCalculateK3807081895.h"
#include "AssemblyU2DCSharp_LoadingPopUp1016279926.h"
#include "AssemblyU2DCSharp_LoadingPopUpUIScaler4027306684.h"
#include "AssemblyU2DCSharp_NotificationManager2388475022.h"
#include "AssemblyU2DCSharp_NotificationManager_U3CClearOverT370995214.h"
#include "AssemblyU2DCSharp_NotificationManager_U3CCheckUnre2482171442.h"
#include "AssemblyU2DCSharp_OpenPlayerProfileButton3248004898.h"
#include "AssemblyU2DCSharp_OpenReviewPagebutton3869392897.h"
#include "AssemblyU2DCSharp_OpenURLButton170290035.h"
#include "AssemblyU2DCSharp_PlayerManager1596653588.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5300 = { sizeof (U3CLogoutFromServerU3Ec__AnonStorey2_t833513242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5300[2] = 
{
	U3CLogoutFromServerU3Ec__AnonStorey2_t833513242::get_offset_of_forced_0(),
	U3CLogoutFromServerU3Ec__AnonStorey2_t833513242::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5301 = { sizeof (U3CScaleAndHideU3Ec__Iterator0_t3742978486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5301[9] = 
{
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_gameobjects_0(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_U24locvar0_1(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_U24locvar1_2(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_scaleTime_3(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_U24locvar2_4(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_U24locvar3_5(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_U24current_6(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_U24disposing_7(),
	U3CScaleAndHideU3Ec__Iterator0_t3742978486::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5302 = { sizeof (U3CScaleAndShowU3Ec__Iterator1_t3962984058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5302[9] = 
{
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_gameobjects_0(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_U24locvar0_1(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_U24locvar1_2(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_U24locvar2_3(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_U24locvar3_4(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_scaleTime_5(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_U24current_6(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_U24disposing_7(),
	U3CScaleAndShowU3Ec__Iterator1_t3962984058::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5303 = { sizeof (AvatarChatButton_t228579527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5303[1] = 
{
	AvatarChatButton_t228579527::get_offset_of_chatUI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5304 = { sizeof (AvatarChatUI_t2524884989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5304[3] = 
{
	AvatarChatUI_t2524884989::get_offset_of_newMessageAmountLabel_2(),
	AvatarChatUI_t2524884989::get_offset_of_friend_3(),
	AvatarChatUI_t2524884989::get_offset_of_newChatMessages_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5305 = { sizeof (AvatarUpdater_t2404165026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5305[20] = 
{
	AvatarUpdater_t2404165026::get_offset_of_avatarTexture_2(),
	AvatarUpdater_t2404165026::get_offset_of_avatarOverlay_3(),
	AvatarUpdater_t2404165026::get_offset_of_avatarOutline_4(),
	AvatarUpdater_t2404165026::get_offset_of_initialsLabel_5(),
	AvatarUpdater_t2404165026::get_offset_of_player_6(),
	AvatarUpdater_t2404165026::get_offset_of_initialsOnly_7(),
	AvatarUpdater_t2404165026::get_offset_of_outlineColour_8(),
	AvatarUpdater_t2404165026::get_offset_of_outlineSize_9(),
	AvatarUpdater_t2404165026::get_offset_of_initialsOffset_10(),
	AvatarUpdater_t2404165026::get_offset_of_activeNemesis_11(),
	AvatarUpdater_t2404165026::get_offset_of_nemesisLabel_12(),
	AvatarUpdater_t2404165026::get_offset_of_nemesisColour_13(),
	AvatarUpdater_t2404165026::get_offset_of_nemesisSize_14(),
	AvatarUpdater_t2404165026::get_offset_of_nemesisOffset_15(),
	AvatarUpdater_t2404165026::get_offset_of_outlineSpriteName_16(),
	AvatarUpdater_t2404165026::get_offset_of_nemesisSpriteName_17(),
	AvatarUpdater_t2404165026::get_offset_of_showFirstStrike_18(),
	AvatarUpdater_t2404165026::get_offset_of_firstStrikeUI_19(),
	AvatarUpdater_t2404165026::get_offset_of_showChat_20(),
	AvatarUpdater_t2404165026::get_offset_of_chatUI_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5306 = { sizeof (U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5306[5] = 
{
	U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896::get_offset_of_user_0(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896::get_offset_of_U24this_1(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896::get_offset_of_U24current_2(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896::get_offset_of_U24disposing_3(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t1005977896::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5307 = { sizeof (FirstStrikeUI_t2168768222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5307[5] = 
{
	FirstStrikeUI_t2168768222::get_offset_of_overlay_2(),
	FirstStrikeUI_t2168768222::get_offset_of_firstStrikeLabel_3(),
	FirstStrikeUI_t2168768222::get_offset_of_firstStrikeAmount_4(),
	FirstStrikeUI_t2168768222::get_offset_of_prePointsString_5(),
	FirstStrikeUI_t2168768222::get_offset_of_postPointsString_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5308 = { sizeof (ChatMessage_t2384228687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5308[6] = 
{
	ChatMessage_t2384228687::get_offset_of_messageID_0(),
	ChatMessage_t2384228687::get_offset_of_threadID_1(),
	ChatMessage_t2384228687::get_offset_of_userID_2(),
	ChatMessage_t2384228687::get_offset_of_messageBody_3(),
	ChatMessage_t2384228687::get_offset_of_multichatUser_4(),
	ChatMessage_t2384228687::get_offset_of_createDateTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5309 = { sizeof (ChatThread_t2394323482), -1, sizeof(ChatThread_t2394323482_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5309[11] = 
{
	ChatThread_t2394323482::get_offset_of_threadID_0(),
	ChatThread_t2394323482::get_offset_of_threadMessages_1(),
	ChatThread_t2394323482::get_offset_of_threadUsers_2(),
	ChatThread_t2394323482::get_offset_of_messageStringList_3(),
	ChatThread_t2394323482::get_offset_of_lastMessage_4(),
	ChatThread_t2394323482::get_offset_of_lastMessageDateTime_5(),
	ChatThread_t2394323482::get_offset_of_friendName_6(),
	ChatThread_t2394323482::get_offset_of_firstTimeUpdating_7(),
	ChatThread_t2394323482::get_offset_of_multiUserChat_8(),
	ChatThread_t2394323482::get_offset_of_unreadMessageCount_9(),
	ChatThread_t2394323482_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5310 = { sizeof (ChatmageddonAudioManager_t766956996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5311 = { sizeof (ChatmageddonSaveData_t4036050876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5311[1] = 
{
	ChatmageddonSaveData_t4036050876::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5312 = { sizeof (ConfigManager_t2239702727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5312[27] = 
{
	ConfigManager_t2239702727::get_offset_of_websiteLink_3(),
	ConfigManager_t2239702727::get_offset_of_apiBaseURL_4(),
	ConfigManager_t2239702727::get_offset_of_universalFeedURL_5(),
	ConfigManager_t2239702727::get_offset_of_universalFeedDefaultPoll_6(),
	ConfigManager_t2239702727::get_offset_of_universalFeedActivePoll_7(),
	ConfigManager_t2239702727::get_offset_of_chatFeedURL_8(),
	ConfigManager_t2239702727::get_offset_of_chatFeedDefaultPoll_9(),
	ConfigManager_t2239702727::get_offset_of_chatFeedActivePoll_10(),
	ConfigManager_t2239702727::get_offset_of_stealthMinDuration_11(),
	ConfigManager_t2239702727::get_offset_of_stealthMaxDuration_12(),
	ConfigManager_t2239702727::get_offset_of_stealthDefaultDuration_13(),
	ConfigManager_t2239702727::get_offset_of_stealthDefaultStartHours_14(),
	ConfigManager_t2239702727::get_offset_of_stealthDefaultStartMins_15(),
	ConfigManager_t2239702727::get_offset_of_lockoutDurationHours_16(),
	ConfigManager_t2239702727::get_offset_of_lockoutBuyBackCost_17(),
	ConfigManager_t2239702727::get_offset_of_fuelRegenMins_18(),
	ConfigManager_t2239702727::get_offset_of_fuelMaxSlots_19(),
	ConfigManager_t2239702727::get_offset_of_backfirePoints_20(),
	ConfigManager_t2239702727::get_offset_of_mineAutoExpireHours_21(),
	ConfigManager_t2239702727::get_offset_of_mineDisarmIntroSecs_22(),
	ConfigManager_t2239702727::get_offset_of_mineDisarmGameSecs_23(),
	ConfigManager_t2239702727::get_offset_of_mineCleanupSecs_24(),
	ConfigManager_t2239702727::get_offset_of_mineMaxPoints_25(),
	ConfigManager_t2239702727::get_offset_of_minePostTriggeredProtection_26(),
	ConfigManager_t2239702727::get_offset_of_heatseekerRunDistance_27(),
	ConfigManager_t2239702727::get_offset_of_standardDefendTime_28(),
	ConfigManager_t2239702727::get_offset_of_langConfig_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5313 = { sizeof (LanguageConfig_t1338327304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5313[1] = 
{
	LanguageConfig_t1338327304::get_offset_of_languageStore_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5314 = { sizeof (U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_t3940050384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5314[1] = 
{
	U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_t3940050384::get_offset_of_model_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5315 = { sizeof (LanguageModel_t2428340347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5315[1] = 
{
	LanguageModel_t2428340347::get_offset_of_attributes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5316 = { sizeof (ValidContact_t1479914934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5316[8] = 
{
	ValidContact_t1479914934::get_offset_of_firstName_0(),
	ValidContact_t1479914934::get_offset_of_lastName_1(),
	ValidContact_t1479914934::get_offset_of_phoneNumbers_2(),
	ValidContact_t1479914934::get_offset_of_phoneNumberHashes_3(),
	ValidContact_t1479914934::get_offset_of_rawChecksum_4(),
	ValidContact_t1479914934::get_offset_of_validatedChecksum_5(),
	ValidContact_t1479914934::get_offset_of_didFind_6(),
	ValidContact_t1479914934::get_offset_of_hasChanged_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5317 = { sizeof (AddressBook_t411411037), -1, sizeof(AddressBook_t411411037_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5317[20] = 
{
	AddressBook_t411411037::get_offset_of_abContacts_2(),
	AddressBook_t411411037::get_offset_of_validContacts_3(),
	AddressBook_t411411037::get_offset_of_util_4(),
	AddressBook_t411411037_StaticFields::get_offset_of_TEST_META_DATA_FILE_PREFIX_5(),
	AddressBook_t411411037_StaticFields::get_offset_of_loadedContacts_6(),
	AddressBook_t411411037_StaticFields::get_offset_of_isBulkImport_7(),
	AddressBook_t411411037::get_offset_of_CurrentAddressBookChecksum_8(),
	AddressBook_t411411037::get_offset_of_existingContacts_9(),
	AddressBook_t411411037::get_offset_of_newContacts_10(),
	AddressBook_t411411037::get_offset_of_existingContactsKeyedByRawChecksum_11(),
	AddressBook_t411411037::get_offset_of_existingContactsKeyedByValidChecksum_12(),
	AddressBook_t411411037::get_offset_of_newContactsKeyedByRawChecksum_13(),
	AddressBook_t411411037::get_offset_of_newContactsKeyedByValidChecksum_14(),
	AddressBook_t411411037::get_offset_of_numImportBatches_15(),
	AddressBook_t411411037::get_offset_of_returnedImportBatchesCounter_16(),
	AddressBook_t411411037::get_offset_of_numDeleteBatches_17(),
	AddressBook_t411411037::get_offset_of_returnedDeleteBatchesCounter_18(),
	AddressBook_t411411037_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_19(),
	AddressBook_t411411037_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_20(),
	AddressBook_t411411037_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5318 = { sizeof (U3CLoadIosContactsU3Ec__Iterator0_t3073646581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5318[8] = 
{
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_U3CindexOffsetU3E__0_0(),
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_U3CtempU3E__1_1(),
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_index_2(),
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_contactAmount_3(),
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_U24this_4(),
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_U24current_5(),
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_U24disposing_6(),
	U3CLoadIosContactsU3Ec__Iterator0_t3073646581::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5319 = { sizeof (U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5319[2] = 
{
	U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283::get_offset_of_batchID_0(),
	U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5320 = { sizeof (AddressBookContact_t330818429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5320[3] = 
{
	AddressBookContact_t330818429::get_offset_of_name_0(),
	AddressBookContact_t330818429::get_offset_of_emails_1(),
	AddressBookContact_t330818429::get_offset_of_phoneNumbers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5321 = { sizeof (CancelButton_t1146598418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5322 = { sizeof (ChooseExistingButton_t274093920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5323 = { sizeof (DeviceCameraButton_t2936622169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5323[1] = 
{
	DeviceCameraButton_t2936622169::get_offset_of_location_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5324 = { sizeof (CameraRequestLocation_t942914719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5324[4] = 
{
	CameraRequestLocation_t942914719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5325 = { sizeof (DeviceCameraManager_t1478748484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5325[9] = 
{
	DeviceCameraManager_t1478748484::get_offset_of_panel_3(),
	DeviceCameraManager_t1478748484::get_offset_of_background_4(),
	DeviceCameraManager_t1478748484::get_offset_of_UI_5(),
	DeviceCameraManager_t1478748484::get_offset_of_dividerSprite_6(),
	DeviceCameraManager_t1478748484::get_offset_of_tpSprite_7(),
	DeviceCameraManager_t1478748484::get_offset_of_ceSprite_8(),
	DeviceCameraManager_t1478748484::get_offset_of_cancelSprite_9(),
	DeviceCameraManager_t1478748484::get_offset_of_currentLocation_10(),
	DeviceCameraManager_t1478748484::get_offset_of_cameraUIClosing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5326 = { sizeof (TakePhotoButton_t1020298243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5327 = { sizeof (DeviceCameraUIScaler_t3087545317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5327[9] = 
{
	DeviceCameraUIScaler_t3087545317::get_offset_of_background_14(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_backgroundCollider_15(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_takePhotoButton_16(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_takePhotoCollider_17(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_divider_18(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_chooseExistingButton_19(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_chooseExistingCollider_20(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_cancelButton_21(),
	DeviceCameraUIScaler_t3087545317::get_offset_of_cancelCollider_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5328 = { sizeof (CancelDeviceShareButton_t2424610771), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5329 = { sizeof (ShareRequestLocation_t2965642179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5329[3] = 
{
	ShareRequestLocation_t2965642179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5330 = { sizeof (ChatmageddonDeviceShareManager_t740212713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5330[5] = 
{
	ChatmageddonDeviceShareManager_t740212713::get_offset_of_panel_4(),
	ChatmageddonDeviceShareManager_t740212713::get_offset_of_background_5(),
	ChatmageddonDeviceShareManager_t740212713::get_offset_of_UI_6(),
	ChatmageddonDeviceShareManager_t740212713::get_offset_of_linkModel_7(),
	ChatmageddonDeviceShareManager_t740212713::get_offset_of_shareUIClosing_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5331 = { sizeof (DeviceShareButton_t1871825377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5331[1] = 
{
	DeviceShareButton_t1871825377::get_offset_of_location_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5332 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5332[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5333 = { sizeof (DeviceShareUIScaler_t3461564645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5333[15] = 
{
	DeviceShareUIScaler_t3461564645::get_offset_of_background_14(),
	DeviceShareUIScaler_t3461564645::get_offset_of_backgroundCollider_15(),
	DeviceShareUIScaler_t3461564645::get_offset_of_emailButton_16(),
	DeviceShareUIScaler_t3461564645::get_offset_of_emailCollider_17(),
	DeviceShareUIScaler_t3461564645::get_offset_of_divider3_18(),
	DeviceShareUIScaler_t3461564645::get_offset_of_smsButton_19(),
	DeviceShareUIScaler_t3461564645::get_offset_of_smsCollider_20(),
	DeviceShareUIScaler_t3461564645::get_offset_of_divider2_21(),
	DeviceShareUIScaler_t3461564645::get_offset_of_facebookButton_22(),
	DeviceShareUIScaler_t3461564645::get_offset_of_facebookCollider_23(),
	DeviceShareUIScaler_t3461564645::get_offset_of_divider1_24(),
	DeviceShareUIScaler_t3461564645::get_offset_of_twitterButton_25(),
	DeviceShareUIScaler_t3461564645::get_offset_of_tiwtterCollider_26(),
	DeviceShareUIScaler_t3461564645::get_offset_of_cancelButton_27(),
	DeviceShareUIScaler_t3461564645::get_offset_of_cancelCollider_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5334 = { sizeof (EmailShareButton_t2679829333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5335 = { sizeof (FacebookShareButton_t509072349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5336 = { sizeof (LableType_t1010844952)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5336[4] = 
{
	LableType_t1010844952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5337 = { sizeof (GameLabelPopulator_t3519752206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5337[9] = 
{
	GameLabelPopulator_t3519752206::get_offset_of_positiveColour_2(),
	GameLabelPopulator_t3519752206::get_offset_of_neutralColour_3(),
	GameLabelPopulator_t3519752206::get_offset_of_negativeColour_4(),
	GameLabelPopulator_t3519752206::get_offset_of_descriptionLabel_5(),
	GameLabelPopulator_t3519752206::get_offset_of_descriptionBackground_6(),
	GameLabelPopulator_t3519752206::get_offset_of_descriptionTween_7(),
	GameLabelPopulator_t3519752206::get_offset_of_amountLabel_8(),
	GameLabelPopulator_t3519752206::get_offset_of_amountBackground_9(),
	GameLabelPopulator_t3519752206::get_offset_of_amountTween_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5338 = { sizeof (SMSShareButton_t1083588064), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5339 = { sizeof (TwitterShareButton_t638004942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5340 = { sizeof (DoubleStateButton_t1032633262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5340[8] = 
{
	DoubleStateButton_t1032633262::get_offset_of_activeForegroundColor_5(),
	DoubleStateButton_t1032633262::get_offset_of_activeBackgroundColor_6(),
	DoubleStateButton_t1032633262::get_offset_of_inactiveForegroundColor_7(),
	DoubleStateButton_t1032633262::get_offset_of_inactiveBackgroundColor_8(),
	DoubleStateButton_t1032633262::get_offset_of_forgroundSprite_9(),
	DoubleStateButton_t1032633262::get_offset_of_backgroundSprite_10(),
	DoubleStateButton_t1032633262::get_offset_of_buttonLabel_11(),
	DoubleStateButton_t1032633262::get_offset_of_buttonActive_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5341 = { sizeof (FlurryEvent_t3996959420)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5341[48] = 
{
	FlurryEvent_t3996959420::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5342 = { sizeof (FlurryController_t2385863972), -1, sizeof(FlurryController_t2385863972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5342[1] = 
{
	FlurryController_t2385863972_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5343 = { sizeof (FormatDateTime_t1158606858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5343[1] = 
{
	FormatDateTime_t1158606858::get_offset_of_formatString_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5344 = { sizeof (FriendAvatarUpdater_t4187090262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5344[3] = 
{
	FriendAvatarUpdater_t4187090262::get_offset_of_avatarTexture_2(),
	FriendAvatarUpdater_t4187090262::get_offset_of_avatarOutline_3(),
	FriendAvatarUpdater_t4187090262::get_offset_of_initialsLabel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5345 = { sizeof (U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5345[5] = 
{
	U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000::get_offset_of_friend_0(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000::get_offset_of_U24this_1(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000::get_offset_of_U24current_2(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000::get_offset_of_U24disposing_3(),
	U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5346 = { sizeof (Bucks_t3932015720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5346[6] = 
{
	Bucks_t3932015720::get_offset_of_displayName_0(),
	Bucks_t3932015720::get_offset_of_internalName_1(),
	Bucks_t3932015720::get_offset_of_amount_2(),
	Bucks_t3932015720::get_offset_of_cost_3(),
	Bucks_t3932015720::get_offset_of_itunesID_4(),
	Bucks_t3932015720::get_offset_of_googleplayID_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5347 = { sizeof (Item_t2440468191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5347[4] = 
{
	Item_t2440468191::get_offset_of_id_0(),
	Item_t2440468191::get_offset_of_internalName_1(),
	Item_t2440468191::get_offset_of_itemName_2(),
	Item_t2440468191::get_offset_of_userID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5348 = { sizeof (ItemsManager_t2342504123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5348[10] = 
{
	ItemsManager_t2342504123::get_offset_of_bucksItems_3(),
	ItemsManager_t2342504123::get_offset_of_missileItems_4(),
	ItemsManager_t2342504123::get_offset_of_shieldItems_5(),
	ItemsManager_t2342504123::get_offset_of_mineItems_6(),
	ItemsManager_t2342504123::get_offset_of_fuelItems_7(),
	ItemsManager_t2342504123::get_offset_of_missilePurchaseableItems_8(),
	ItemsManager_t2342504123::get_offset_of_shieldPurchaseableItems_9(),
	ItemsManager_t2342504123::get_offset_of_minePurchaseableItems_10(),
	ItemsManager_t2342504123::get_offset_of_fuelPurchaseableItems_11(),
	ItemsManager_t2342504123::get_offset_of_bundlePurchaseableItems_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5349 = { sizeof (CostType_t3548590213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5349[4] = 
{
	CostType_t3548590213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5350 = { sizeof (PurchaseableCategory_t1933007175)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5350[8] = 
{
	PurchaseableCategory_t1933007175::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5351 = { sizeof (PurchaseableItem_t3351122996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5351[6] = 
{
	PurchaseableItem_t3351122996::get_offset_of_minimumUserLevel_4(),
	PurchaseableItem_t3351122996::get_offset_of_costType_5(),
	PurchaseableItem_t3351122996::get_offset_of_sortOrder_6(),
	PurchaseableItem_t3351122996::get_offset_of_category_7(),
	PurchaseableItem_t3351122996::get_offset_of_cost_8(),
	PurchaseableItem_t3351122996::get_offset_of_items_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5352 = { sizeof (ItemType_t3455197591)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5352[5] = 
{
	ItemType_t3455197591::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5353 = { sizeof (QuantitativeItem_t3036513780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5353[4] = 
{
	QuantitativeItem_t3036513780::get_offset_of_internalName_0(),
	QuantitativeItem_t3036513780::get_offset_of_itemType_1(),
	QuantitativeItem_t3036513780::get_offset_of_quanitity_2(),
	QuantitativeItem_t3036513780::get_offset_of_duration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5354 = { sizeof (Fuel_t1015546524), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5355 = { sizeof (LaunchedItemType_t186177771)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5355[4] = 
{
	LaunchedItemType_t186177771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5356 = { sizeof (LaunchDirection_t3410220992)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5356[4] = 
{
	LaunchDirection_t3410220992::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5357 = { sizeof (LaunchedItem_t3670634427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5357[6] = 
{
	LaunchedItem_t3670634427::get_offset_of_itemType_0(),
	LaunchedItem_t3670634427::get_offset_of_missileItem_1(),
	LaunchedItem_t3670634427::get_offset_of_mineItem_2(),
	LaunchedItem_t3670634427::get_offset_of_direction_3(),
	LaunchedItem_t3670634427::get_offset_of_launchedTime_4(),
	LaunchedItem_t3670634427::get_offset_of_id_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5358 = { sizeof (MineResultStatus_t1141738764)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5358[6] = 
{
	MineResultStatus_t1141738764::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5359 = { sizeof (LaunchedMine_t3677282773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5359[7] = 
{
	LaunchedMine_t3677282773::get_offset_of_resultStatus_13(),
	LaunchedMine_t3677282773::get_offset_of_direction_14(),
	LaunchedMine_t3677282773::get_offset_of_friend_15(),
	LaunchedMine_t3677282773::get_offset_of_completeTime_16(),
	LaunchedMine_t3677282773::get_offset_of_maxResultPoints_17(),
	LaunchedMine_t3677282773::get_offset_of_resultPoints_18(),
	LaunchedMine_t3677282773::get_offset_of_resultAwardTo_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5360 = { sizeof (MissileStatus_t1722383730)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5360[5] = 
{
	MissileStatus_t1722383730::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5361 = { sizeof (MissileResultStatus_t3272208871)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5361[6] = 
{
	MissileResultStatus_t3272208871::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5362 = { sizeof (WarefareState_t701596006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5362[4] = 
{
	WarefareState_t701596006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5363 = { sizeof (LaunchedMissile_t1542515810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5363[24] = 
{
	LaunchedMissile_t1542515810::get_offset_of_status_10(),
	LaunchedMissile_t1542515810::get_offset_of_resultStatus_11(),
	LaunchedMissile_t1542515810::get_offset_of_direction_12(),
	LaunchedMissile_t1542515810::get_offset_of_firstStrikePoints_13(),
	LaunchedMissile_t1542515810::get_offset_of_perfectLaunch_14(),
	LaunchedMissile_t1542515810::get_offset_of_backfired_15(),
	LaunchedMissile_t1542515810::get_offset_of_launchAccuracy_16(),
	LaunchedMissile_t1542515810::get_offset_of_launchPoints_17(),
	LaunchedMissile_t1542515810::get_offset_of_launchedTime_18(),
	LaunchedMissile_t1542515810::get_offset_of_arriveTime_19(),
	LaunchedMissile_t1542515810::get_offset_of_visibleTime_20(),
	LaunchedMissile_t1542515810::get_offset_of_defenderId_21(),
	LaunchedMissile_t1542515810::get_offset_of_friend_22(),
	LaunchedMissile_t1542515810::get_offset_of_startLocation_23(),
	LaunchedMissile_t1542515810::get_offset_of_completeTime_24(),
	LaunchedMissile_t1542515810::get_offset_of_maxResultPoints_25(),
	LaunchedMissile_t1542515810::get_offset_of_resultPoints_26(),
	LaunchedMissile_t1542515810::get_offset_of_resultAwardTo_27(),
	LaunchedMissile_t1542515810::get_offset_of_defendAttempted_28(),
	LaunchedMissile_t1542515810::get_offset_of_defendAttemptCount_29(),
	LaunchedMissile_t1542515810::get_offset_of_defendedTime_30(),
	LaunchedMissile_t1542515810::get_offset_of_defended_31(),
	LaunchedMissile_t1542515810::get_offset_of_defendTimeString_32(),
	LaunchedMissile_t1542515810::get_offset_of_scorezoneDefendedIn_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5364 = { sizeof (MineStatus_t2219122381)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5364[5] = 
{
	MineStatus_t2219122381::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5365 = { sizeof (Mine_t2729441277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5365[9] = 
{
	Mine_t2729441277::get_offset_of_status_4(),
	Mine_t2729441277::get_offset_of_damagePoints_5(),
	Mine_t2729441277::get_offset_of_ownerLevelPointsMultiplier_6(),
	Mine_t2729441277::get_offset_of_triggerRadius_7(),
	Mine_t2729441277::get_offset_of_latitude_8(),
	Mine_t2729441277::get_offset_of_longitude_9(),
	Mine_t2729441277::get_offset_of_distance_10(),
	Mine_t2729441277::get_offset_of_triggeredTime_11(),
	Mine_t2729441277::get_offset_of_triggeredById_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5366 = { sizeof (Missile_t813944928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5366[6] = 
{
	Missile_t813944928::get_offset_of_flightTime_4(),
	Missile_t813944928::get_offset_of_points_5(),
	Missile_t813944928::get_offset_of_criticalHitChance_6(),
	Missile_t813944928::get_offset_of_backfireChance_7(),
	Missile_t813944928::get_offset_of_launchNotificationDelay_8(),
	Missile_t813944928::get_offset_of_fuelUse_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5367 = { sizeof (ShieldType_t96263953)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5367[4] = 
{
	ShieldType_t96263953::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5368 = { sizeof (ShieldProtection_t4135544898)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5368[4] = 
{
	ShieldProtection_t4135544898::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5369 = { sizeof (Shield_t3327121081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5369[7] = 
{
	Shield_t3327121081::get_offset_of_shieldType_4(),
	Shield_t3327121081::get_offset_of_protectionType_5(),
	Shield_t3327121081::get_offset_of_perfectLaunchProtection_6(),
	Shield_t3327121081::get_offset_of_duration_7(),
	Shield_t3327121081::get_offset_of_active_8(),
	Shield_t3327121081::get_offset_of_activatedAt_9(),
	Shield_t3327121081::get_offset_of_deactivatedAt_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5370 = { sizeof (KnockoutCalculator_t371618716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5370[4] = 
{
	KnockoutCalculator_t371618716::get_offset_of_timeLabel_2(),
	KnockoutCalculator_t371618716::get_offset_of_calcRoutine_3(),
	KnockoutCalculator_t371618716::get_offset_of_negativeColour_4(),
	KnockoutCalculator_t371618716::get_offset_of_onZeroHit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5371 = { sizeof (OnZeroHit_t581944179), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5372 = { sizeof (U3CCalculateKnockoutTimeU3Ec__Iterator0_t3807081895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5372[5] = 
{
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t3807081895::get_offset_of_user_0(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t3807081895::get_offset_of_U24this_1(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t3807081895::get_offset_of_U24current_2(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t3807081895::get_offset_of_U24disposing_3(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t3807081895::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5373 = { sizeof (LoadingPopUp_t1016279926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5373[3] = 
{
	LoadingPopUp_t1016279926::get_offset_of_content_3(),
	LoadingPopUp_t1016279926::get_offset_of_loadingLabel_4(),
	LoadingPopUp_t1016279926::get_offset_of_fadeTween_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5374 = { sizeof (LoadingPopUpUIScaler_t4027306684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5374[3] = 
{
	LoadingPopUpUIScaler_t4027306684::get_offset_of_background_14(),
	LoadingPopUpUIScaler_t4027306684::get_offset_of_backgroundCollider_15(),
	LoadingPopUpUIScaler_t4027306684::get_offset_of_loadingLabel_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5375 = { sizeof (NotificationManager_t2388475022), -1, sizeof(NotificationManager_t2388475022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5375[7] = 
{
	NotificationManager_t2388475022::get_offset_of_initialised_3(),
	NotificationManager_t2388475022::get_offset_of_tokenSent_4(),
	NotificationManager_t2388475022::get_offset_of_activeToken_5(),
	NotificationManager_t2388475022::get_offset_of_unreadNotifications_6(),
	NotificationManager_t2388475022::get_offset_of_checkRoutine_7(),
	NotificationManager_t2388475022::get_offset_of_clearOverTimeRoutine_8(),
	NotificationManager_t2388475022_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5376 = { sizeof (U3CClearOverTimeU3Ec__Iterator0_t370995214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5376[4] = 
{
	U3CClearOverTimeU3Ec__Iterator0_t370995214::get_offset_of_U24this_0(),
	U3CClearOverTimeU3Ec__Iterator0_t370995214::get_offset_of_U24current_1(),
	U3CClearOverTimeU3Ec__Iterator0_t370995214::get_offset_of_U24disposing_2(),
	U3CClearOverTimeU3Ec__Iterator0_t370995214::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5377 = { sizeof (U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5377[5] = 
{
	U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442::get_offset_of_U3CcheckAvailabilityU3E__0_0(),
	U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442::get_offset_of_U24this_1(),
	U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442::get_offset_of_U24current_2(),
	U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442::get_offset_of_U24disposing_3(),
	U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5378 = { sizeof (OpenPlayerProfileButton_t3248004898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5378[3] = 
{
	OpenPlayerProfileButton_t3248004898::get_offset_of_activePlayer_5(),
	OpenPlayerProfileButton_t3248004898::get_offset_of_friend_6(),
	OpenPlayerProfileButton_t3248004898::get_offset_of_modalSwitch_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5379 = { sizeof (OpenReviewPagebutton_t3869392897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5380 = { sizeof (OpenURLButton_t170290035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5380[1] = 
{
	OpenURLButton_t170290035::get_offset_of_URL_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5381 = { sizeof (PlayerManager_t1596653588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5381[6] = 
{
	PlayerManager_t1596653588::get_offset_of_user_3(),
	PlayerManager_t1596653588::get_offset_of_totalFuel_4(),
	PlayerManager_t1596653588::get_offset_of_chosenMapState_5(),
	PlayerManager_t1596653588::get_offset_of_addressBookObject_6(),
	PlayerManager_t1596653588::get_offset_of_addressBook_7(),
	PlayerManager_t1596653588::get_offset_of_avatarLoadedFromServer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5382 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5382[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5383 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5383[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5384 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5384[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5385 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5385[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5386 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5386[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5387 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5387[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5388 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5388[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5389 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5389[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5390 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5390[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5391 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5391[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5392 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5392[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5393 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5393[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5394 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5394[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5395 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5395[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5396 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5396[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5397 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5397[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5398 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5398[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5399 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5399[2] = 
{
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
