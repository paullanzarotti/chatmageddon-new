﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_TV_Artefact
struct  CameraFilterPack_TV_Artefact_t2719757314  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_TV_Artefact::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// UnityEngine.Vector4 CameraFilterPack_TV_Artefact::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_3;
	// System.Single CameraFilterPack_TV_Artefact::TimeX
	float ___TimeX_4;
	// System.Single CameraFilterPack_TV_Artefact::Colorisation
	float ___Colorisation_5;
	// System.Single CameraFilterPack_TV_Artefact::Parasite
	float ___Parasite_6;
	// System.Single CameraFilterPack_TV_Artefact::Noise
	float ___Noise_7;
	// UnityEngine.Material CameraFilterPack_TV_Artefact::SCMaterial
	Material_t193706927 * ___SCMaterial_8;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Artefact_t2719757314, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_ScreenResolution_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Artefact_t2719757314, ___ScreenResolution_3)); }
	inline Vector4_t2243707581  get_ScreenResolution_3() const { return ___ScreenResolution_3; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_3() { return &___ScreenResolution_3; }
	inline void set_ScreenResolution_3(Vector4_t2243707581  value)
	{
		___ScreenResolution_3 = value;
	}

	inline static int32_t get_offset_of_TimeX_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Artefact_t2719757314, ___TimeX_4)); }
	inline float get_TimeX_4() const { return ___TimeX_4; }
	inline float* get_address_of_TimeX_4() { return &___TimeX_4; }
	inline void set_TimeX_4(float value)
	{
		___TimeX_4 = value;
	}

	inline static int32_t get_offset_of_Colorisation_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Artefact_t2719757314, ___Colorisation_5)); }
	inline float get_Colorisation_5() const { return ___Colorisation_5; }
	inline float* get_address_of_Colorisation_5() { return &___Colorisation_5; }
	inline void set_Colorisation_5(float value)
	{
		___Colorisation_5 = value;
	}

	inline static int32_t get_offset_of_Parasite_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Artefact_t2719757314, ___Parasite_6)); }
	inline float get_Parasite_6() const { return ___Parasite_6; }
	inline float* get_address_of_Parasite_6() { return &___Parasite_6; }
	inline void set_Parasite_6(float value)
	{
		___Parasite_6 = value;
	}

	inline static int32_t get_offset_of_Noise_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Artefact_t2719757314, ___Noise_7)); }
	inline float get_Noise_7() const { return ___Noise_7; }
	inline float* get_address_of_Noise_7() { return &___Noise_7; }
	inline void set_Noise_7(float value)
	{
		___Noise_7 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_Artefact_t2719757314, ___SCMaterial_8)); }
	inline Material_t193706927 * get_SCMaterial_8() const { return ___SCMaterial_8; }
	inline Material_t193706927 ** get_address_of_SCMaterial_8() { return &___SCMaterial_8; }
	inline void set_SCMaterial_8(Material_t193706927 * value)
	{
		___SCMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
