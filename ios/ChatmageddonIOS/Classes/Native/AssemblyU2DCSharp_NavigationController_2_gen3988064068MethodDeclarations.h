﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2225246313MethodDeclarations.h"

// System.Void NavigationController`2<ChatNavigationController,ChatNavScreen>::.ctor()
#define NavigationController_2__ctor_m2316323918(__this, method) ((  void (*) (NavigationController_2_t3988064068 *, const MethodInfo*))NavigationController_2__ctor_m1644053680_gshared)(__this, method)
// System.Void NavigationController`2<ChatNavigationController,ChatNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m1982405032(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t3988064068 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1767840762_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<ChatNavigationController,ChatNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m938084261(__this, method) ((  void (*) (NavigationController_2_t3988064068 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3268801571_gshared)(__this, method)
// System.Void NavigationController`2<ChatNavigationController,ChatNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3523747974(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t3988064068 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m929386684_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<ChatNavigationController,ChatNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m274520162(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t3988064068 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m599142940_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<ChatNavigationController,ChatNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m2019337471(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t3988064068 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m33970125_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<ChatNavigationController,ChatNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m673022356(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t3988064068 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1163805226_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<ChatNavigationController,ChatNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m4194676146(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t3988064068 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m605338048_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<ChatNavigationController,ChatNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m620447106(__this, ___screen0, method) ((  void (*) (NavigationController_2_t3988064068 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2027155932_gshared)(__this, ___screen0, method)
