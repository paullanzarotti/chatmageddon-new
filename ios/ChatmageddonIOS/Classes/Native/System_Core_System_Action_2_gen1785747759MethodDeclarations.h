﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"

// System.Void System.Action`2<BestHTTP.SignalR.NegotiationData,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m886943179(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1785747759 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2967680750_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<BestHTTP.SignalR.NegotiationData,System.String>::Invoke(T1,T2)
#define Action_2_Invoke_m1928510268(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1785747759 *, NegotiationData_t3059020807 *, String_t*, const MethodInfo*))Action_2_Invoke_m715425719_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<BestHTTP.SignalR.NegotiationData,System.String>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3235241667(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1785747759 *, NegotiationData_t3059020807 *, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1914861552_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<BestHTTP.SignalR.NegotiationData,System.String>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m2225522313(__this, ___result0, method) ((  void (*) (Action_2_t1785747759 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3956733788_gshared)(__this, ___result0, method)
