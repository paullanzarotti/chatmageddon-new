﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnibillError>
struct DefaultComparer_t243130020;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnibillError>::.ctor()
extern "C"  void DefaultComparer__ctor_m1048175625_gshared (DefaultComparer_t243130020 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1048175625(__this, method) ((  void (*) (DefaultComparer_t243130020 *, const MethodInfo*))DefaultComparer__ctor_m1048175625_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnibillError>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2840798694_gshared (DefaultComparer_t243130020 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2840798694(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t243130020 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m2840798694_gshared)(__this, ___x0, ___y1, method)
