﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// LanguageConfig
struct LanguageConfig_t1338327304;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1990368447.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigManager
struct  ConfigManager_t2239702727  : public MonoSingleton_1_t1990368447
{
public:
	// System.String ConfigManager::websiteLink
	String_t* ___websiteLink_3;
	// System.String ConfigManager::apiBaseURL
	String_t* ___apiBaseURL_4;
	// System.String ConfigManager::universalFeedURL
	String_t* ___universalFeedURL_5;
	// System.Single ConfigManager::universalFeedDefaultPoll
	float ___universalFeedDefaultPoll_6;
	// System.Single ConfigManager::universalFeedActivePoll
	float ___universalFeedActivePoll_7;
	// System.String ConfigManager::chatFeedURL
	String_t* ___chatFeedURL_8;
	// System.Single ConfigManager::chatFeedDefaultPoll
	float ___chatFeedDefaultPoll_9;
	// System.Single ConfigManager::chatFeedActivePoll
	float ___chatFeedActivePoll_10;
	// System.Int32 ConfigManager::stealthMinDuration
	int32_t ___stealthMinDuration_11;
	// System.Int32 ConfigManager::stealthMaxDuration
	int32_t ___stealthMaxDuration_12;
	// System.Int32 ConfigManager::stealthDefaultDuration
	int32_t ___stealthDefaultDuration_13;
	// System.Int32 ConfigManager::stealthDefaultStartHours
	int32_t ___stealthDefaultStartHours_14;
	// System.Int32 ConfigManager::stealthDefaultStartMins
	int32_t ___stealthDefaultStartMins_15;
	// System.Int32 ConfigManager::lockoutDurationHours
	int32_t ___lockoutDurationHours_16;
	// System.Int32 ConfigManager::lockoutBuyBackCost
	int32_t ___lockoutBuyBackCost_17;
	// System.Int32 ConfigManager::fuelRegenMins
	int32_t ___fuelRegenMins_18;
	// System.Int32 ConfigManager::fuelMaxSlots
	int32_t ___fuelMaxSlots_19;
	// System.Int32 ConfigManager::backfirePoints
	int32_t ___backfirePoints_20;
	// System.Int32 ConfigManager::mineAutoExpireHours
	int32_t ___mineAutoExpireHours_21;
	// System.Single ConfigManager::mineDisarmIntroSecs
	float ___mineDisarmIntroSecs_22;
	// System.Double ConfigManager::mineDisarmGameSecs
	double ___mineDisarmGameSecs_23;
	// System.Int32 ConfigManager::mineCleanupSecs
	int32_t ___mineCleanupSecs_24;
	// System.Int32 ConfigManager::mineMaxPoints
	int32_t ___mineMaxPoints_25;
	// System.Int32 ConfigManager::minePostTriggeredProtection
	int32_t ___minePostTriggeredProtection_26;
	// System.Single ConfigManager::heatseekerRunDistance
	float ___heatseekerRunDistance_27;
	// System.Single ConfigManager::standardDefendTime
	float ___standardDefendTime_28;
	// LanguageConfig ConfigManager::langConfig
	LanguageConfig_t1338327304 * ___langConfig_29;

public:
	inline static int32_t get_offset_of_websiteLink_3() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___websiteLink_3)); }
	inline String_t* get_websiteLink_3() const { return ___websiteLink_3; }
	inline String_t** get_address_of_websiteLink_3() { return &___websiteLink_3; }
	inline void set_websiteLink_3(String_t* value)
	{
		___websiteLink_3 = value;
		Il2CppCodeGenWriteBarrier(&___websiteLink_3, value);
	}

	inline static int32_t get_offset_of_apiBaseURL_4() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___apiBaseURL_4)); }
	inline String_t* get_apiBaseURL_4() const { return ___apiBaseURL_4; }
	inline String_t** get_address_of_apiBaseURL_4() { return &___apiBaseURL_4; }
	inline void set_apiBaseURL_4(String_t* value)
	{
		___apiBaseURL_4 = value;
		Il2CppCodeGenWriteBarrier(&___apiBaseURL_4, value);
	}

	inline static int32_t get_offset_of_universalFeedURL_5() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___universalFeedURL_5)); }
	inline String_t* get_universalFeedURL_5() const { return ___universalFeedURL_5; }
	inline String_t** get_address_of_universalFeedURL_5() { return &___universalFeedURL_5; }
	inline void set_universalFeedURL_5(String_t* value)
	{
		___universalFeedURL_5 = value;
		Il2CppCodeGenWriteBarrier(&___universalFeedURL_5, value);
	}

	inline static int32_t get_offset_of_universalFeedDefaultPoll_6() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___universalFeedDefaultPoll_6)); }
	inline float get_universalFeedDefaultPoll_6() const { return ___universalFeedDefaultPoll_6; }
	inline float* get_address_of_universalFeedDefaultPoll_6() { return &___universalFeedDefaultPoll_6; }
	inline void set_universalFeedDefaultPoll_6(float value)
	{
		___universalFeedDefaultPoll_6 = value;
	}

	inline static int32_t get_offset_of_universalFeedActivePoll_7() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___universalFeedActivePoll_7)); }
	inline float get_universalFeedActivePoll_7() const { return ___universalFeedActivePoll_7; }
	inline float* get_address_of_universalFeedActivePoll_7() { return &___universalFeedActivePoll_7; }
	inline void set_universalFeedActivePoll_7(float value)
	{
		___universalFeedActivePoll_7 = value;
	}

	inline static int32_t get_offset_of_chatFeedURL_8() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___chatFeedURL_8)); }
	inline String_t* get_chatFeedURL_8() const { return ___chatFeedURL_8; }
	inline String_t** get_address_of_chatFeedURL_8() { return &___chatFeedURL_8; }
	inline void set_chatFeedURL_8(String_t* value)
	{
		___chatFeedURL_8 = value;
		Il2CppCodeGenWriteBarrier(&___chatFeedURL_8, value);
	}

	inline static int32_t get_offset_of_chatFeedDefaultPoll_9() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___chatFeedDefaultPoll_9)); }
	inline float get_chatFeedDefaultPoll_9() const { return ___chatFeedDefaultPoll_9; }
	inline float* get_address_of_chatFeedDefaultPoll_9() { return &___chatFeedDefaultPoll_9; }
	inline void set_chatFeedDefaultPoll_9(float value)
	{
		___chatFeedDefaultPoll_9 = value;
	}

	inline static int32_t get_offset_of_chatFeedActivePoll_10() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___chatFeedActivePoll_10)); }
	inline float get_chatFeedActivePoll_10() const { return ___chatFeedActivePoll_10; }
	inline float* get_address_of_chatFeedActivePoll_10() { return &___chatFeedActivePoll_10; }
	inline void set_chatFeedActivePoll_10(float value)
	{
		___chatFeedActivePoll_10 = value;
	}

	inline static int32_t get_offset_of_stealthMinDuration_11() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___stealthMinDuration_11)); }
	inline int32_t get_stealthMinDuration_11() const { return ___stealthMinDuration_11; }
	inline int32_t* get_address_of_stealthMinDuration_11() { return &___stealthMinDuration_11; }
	inline void set_stealthMinDuration_11(int32_t value)
	{
		___stealthMinDuration_11 = value;
	}

	inline static int32_t get_offset_of_stealthMaxDuration_12() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___stealthMaxDuration_12)); }
	inline int32_t get_stealthMaxDuration_12() const { return ___stealthMaxDuration_12; }
	inline int32_t* get_address_of_stealthMaxDuration_12() { return &___stealthMaxDuration_12; }
	inline void set_stealthMaxDuration_12(int32_t value)
	{
		___stealthMaxDuration_12 = value;
	}

	inline static int32_t get_offset_of_stealthDefaultDuration_13() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___stealthDefaultDuration_13)); }
	inline int32_t get_stealthDefaultDuration_13() const { return ___stealthDefaultDuration_13; }
	inline int32_t* get_address_of_stealthDefaultDuration_13() { return &___stealthDefaultDuration_13; }
	inline void set_stealthDefaultDuration_13(int32_t value)
	{
		___stealthDefaultDuration_13 = value;
	}

	inline static int32_t get_offset_of_stealthDefaultStartHours_14() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___stealthDefaultStartHours_14)); }
	inline int32_t get_stealthDefaultStartHours_14() const { return ___stealthDefaultStartHours_14; }
	inline int32_t* get_address_of_stealthDefaultStartHours_14() { return &___stealthDefaultStartHours_14; }
	inline void set_stealthDefaultStartHours_14(int32_t value)
	{
		___stealthDefaultStartHours_14 = value;
	}

	inline static int32_t get_offset_of_stealthDefaultStartMins_15() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___stealthDefaultStartMins_15)); }
	inline int32_t get_stealthDefaultStartMins_15() const { return ___stealthDefaultStartMins_15; }
	inline int32_t* get_address_of_stealthDefaultStartMins_15() { return &___stealthDefaultStartMins_15; }
	inline void set_stealthDefaultStartMins_15(int32_t value)
	{
		___stealthDefaultStartMins_15 = value;
	}

	inline static int32_t get_offset_of_lockoutDurationHours_16() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___lockoutDurationHours_16)); }
	inline int32_t get_lockoutDurationHours_16() const { return ___lockoutDurationHours_16; }
	inline int32_t* get_address_of_lockoutDurationHours_16() { return &___lockoutDurationHours_16; }
	inline void set_lockoutDurationHours_16(int32_t value)
	{
		___lockoutDurationHours_16 = value;
	}

	inline static int32_t get_offset_of_lockoutBuyBackCost_17() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___lockoutBuyBackCost_17)); }
	inline int32_t get_lockoutBuyBackCost_17() const { return ___lockoutBuyBackCost_17; }
	inline int32_t* get_address_of_lockoutBuyBackCost_17() { return &___lockoutBuyBackCost_17; }
	inline void set_lockoutBuyBackCost_17(int32_t value)
	{
		___lockoutBuyBackCost_17 = value;
	}

	inline static int32_t get_offset_of_fuelRegenMins_18() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___fuelRegenMins_18)); }
	inline int32_t get_fuelRegenMins_18() const { return ___fuelRegenMins_18; }
	inline int32_t* get_address_of_fuelRegenMins_18() { return &___fuelRegenMins_18; }
	inline void set_fuelRegenMins_18(int32_t value)
	{
		___fuelRegenMins_18 = value;
	}

	inline static int32_t get_offset_of_fuelMaxSlots_19() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___fuelMaxSlots_19)); }
	inline int32_t get_fuelMaxSlots_19() const { return ___fuelMaxSlots_19; }
	inline int32_t* get_address_of_fuelMaxSlots_19() { return &___fuelMaxSlots_19; }
	inline void set_fuelMaxSlots_19(int32_t value)
	{
		___fuelMaxSlots_19 = value;
	}

	inline static int32_t get_offset_of_backfirePoints_20() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___backfirePoints_20)); }
	inline int32_t get_backfirePoints_20() const { return ___backfirePoints_20; }
	inline int32_t* get_address_of_backfirePoints_20() { return &___backfirePoints_20; }
	inline void set_backfirePoints_20(int32_t value)
	{
		___backfirePoints_20 = value;
	}

	inline static int32_t get_offset_of_mineAutoExpireHours_21() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___mineAutoExpireHours_21)); }
	inline int32_t get_mineAutoExpireHours_21() const { return ___mineAutoExpireHours_21; }
	inline int32_t* get_address_of_mineAutoExpireHours_21() { return &___mineAutoExpireHours_21; }
	inline void set_mineAutoExpireHours_21(int32_t value)
	{
		___mineAutoExpireHours_21 = value;
	}

	inline static int32_t get_offset_of_mineDisarmIntroSecs_22() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___mineDisarmIntroSecs_22)); }
	inline float get_mineDisarmIntroSecs_22() const { return ___mineDisarmIntroSecs_22; }
	inline float* get_address_of_mineDisarmIntroSecs_22() { return &___mineDisarmIntroSecs_22; }
	inline void set_mineDisarmIntroSecs_22(float value)
	{
		___mineDisarmIntroSecs_22 = value;
	}

	inline static int32_t get_offset_of_mineDisarmGameSecs_23() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___mineDisarmGameSecs_23)); }
	inline double get_mineDisarmGameSecs_23() const { return ___mineDisarmGameSecs_23; }
	inline double* get_address_of_mineDisarmGameSecs_23() { return &___mineDisarmGameSecs_23; }
	inline void set_mineDisarmGameSecs_23(double value)
	{
		___mineDisarmGameSecs_23 = value;
	}

	inline static int32_t get_offset_of_mineCleanupSecs_24() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___mineCleanupSecs_24)); }
	inline int32_t get_mineCleanupSecs_24() const { return ___mineCleanupSecs_24; }
	inline int32_t* get_address_of_mineCleanupSecs_24() { return &___mineCleanupSecs_24; }
	inline void set_mineCleanupSecs_24(int32_t value)
	{
		___mineCleanupSecs_24 = value;
	}

	inline static int32_t get_offset_of_mineMaxPoints_25() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___mineMaxPoints_25)); }
	inline int32_t get_mineMaxPoints_25() const { return ___mineMaxPoints_25; }
	inline int32_t* get_address_of_mineMaxPoints_25() { return &___mineMaxPoints_25; }
	inline void set_mineMaxPoints_25(int32_t value)
	{
		___mineMaxPoints_25 = value;
	}

	inline static int32_t get_offset_of_minePostTriggeredProtection_26() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___minePostTriggeredProtection_26)); }
	inline int32_t get_minePostTriggeredProtection_26() const { return ___minePostTriggeredProtection_26; }
	inline int32_t* get_address_of_minePostTriggeredProtection_26() { return &___minePostTriggeredProtection_26; }
	inline void set_minePostTriggeredProtection_26(int32_t value)
	{
		___minePostTriggeredProtection_26 = value;
	}

	inline static int32_t get_offset_of_heatseekerRunDistance_27() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___heatseekerRunDistance_27)); }
	inline float get_heatseekerRunDistance_27() const { return ___heatseekerRunDistance_27; }
	inline float* get_address_of_heatseekerRunDistance_27() { return &___heatseekerRunDistance_27; }
	inline void set_heatseekerRunDistance_27(float value)
	{
		___heatseekerRunDistance_27 = value;
	}

	inline static int32_t get_offset_of_standardDefendTime_28() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___standardDefendTime_28)); }
	inline float get_standardDefendTime_28() const { return ___standardDefendTime_28; }
	inline float* get_address_of_standardDefendTime_28() { return &___standardDefendTime_28; }
	inline void set_standardDefendTime_28(float value)
	{
		___standardDefendTime_28 = value;
	}

	inline static int32_t get_offset_of_langConfig_29() { return static_cast<int32_t>(offsetof(ConfigManager_t2239702727, ___langConfig_29)); }
	inline LanguageConfig_t1338327304 * get_langConfig_29() const { return ___langConfig_29; }
	inline LanguageConfig_t1338327304 ** get_address_of_langConfig_29() { return &___langConfig_29; }
	inline void set_langConfig_29(LanguageConfig_t1338327304 * value)
	{
		___langConfig_29 = value;
		Il2CppCodeGenWriteBarrier(&___langConfig_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
