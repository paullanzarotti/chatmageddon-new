﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenScale
struct TweenScale_t2697902175;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PurchasePU
struct  PurchasePU_t4126238880  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PurchasePU::puActive
	bool ___puActive_2;
	// TweenScale PurchasePU::puTween
	TweenScale_t2697902175 * ___puTween_3;
	// UILabel PurchasePU::purchaseLabel
	UILabel_t1795115428 * ___purchaseLabel_4;
	// System.Single PurchasePU::successWaitTime
	float ___successWaitTime_5;
	// System.Boolean PurchasePU::puClosing
	bool ___puClosing_6;

public:
	inline static int32_t get_offset_of_puActive_2() { return static_cast<int32_t>(offsetof(PurchasePU_t4126238880, ___puActive_2)); }
	inline bool get_puActive_2() const { return ___puActive_2; }
	inline bool* get_address_of_puActive_2() { return &___puActive_2; }
	inline void set_puActive_2(bool value)
	{
		___puActive_2 = value;
	}

	inline static int32_t get_offset_of_puTween_3() { return static_cast<int32_t>(offsetof(PurchasePU_t4126238880, ___puTween_3)); }
	inline TweenScale_t2697902175 * get_puTween_3() const { return ___puTween_3; }
	inline TweenScale_t2697902175 ** get_address_of_puTween_3() { return &___puTween_3; }
	inline void set_puTween_3(TweenScale_t2697902175 * value)
	{
		___puTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___puTween_3, value);
	}

	inline static int32_t get_offset_of_purchaseLabel_4() { return static_cast<int32_t>(offsetof(PurchasePU_t4126238880, ___purchaseLabel_4)); }
	inline UILabel_t1795115428 * get_purchaseLabel_4() const { return ___purchaseLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_purchaseLabel_4() { return &___purchaseLabel_4; }
	inline void set_purchaseLabel_4(UILabel_t1795115428 * value)
	{
		___purchaseLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseLabel_4, value);
	}

	inline static int32_t get_offset_of_successWaitTime_5() { return static_cast<int32_t>(offsetof(PurchasePU_t4126238880, ___successWaitTime_5)); }
	inline float get_successWaitTime_5() const { return ___successWaitTime_5; }
	inline float* get_address_of_successWaitTime_5() { return &___successWaitTime_5; }
	inline void set_successWaitTime_5(float value)
	{
		___successWaitTime_5 = value;
	}

	inline static int32_t get_offset_of_puClosing_6() { return static_cast<int32_t>(offsetof(PurchasePU_t4126238880, ___puClosing_6)); }
	inline bool get_puClosing_6() const { return ___puClosing_6; }
	inline bool* get_address_of_puClosing_6() { return &___puClosing_6; }
	inline void set_puClosing_6(bool value)
	{
		___puClosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
