﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateURLProfileImage>c__AnonStoreyE
struct U3CUpdateURLProfileImageU3Ec__AnonStoreyE_t1909536015;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateURLProfileImage>c__AnonStoreyE::.ctor()
extern "C"  void U3CUpdateURLProfileImageU3Ec__AnonStoreyE__ctor_m1922542926 (U3CUpdateURLProfileImageU3Ec__AnonStoreyE_t1909536015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateURLProfileImage>c__AnonStoreyE::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateURLProfileImageU3Ec__AnonStoreyE_U3CU3Em__0_m3397964595 (U3CUpdateURLProfileImageU3Ec__AnonStoreyE_t1909536015 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
