﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_SplitScreen
struct CameraFilterPack_Blend2Camera_SplitScreen_t3502727214;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_SplitScreen::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_SplitScreen__ctor_m4234201199 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_SplitScreen::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_SplitScreen_get_material_m1862557870 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::Start()
extern "C"  void CameraFilterPack_Blend2Camera_SplitScreen_Start_m2911144179 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_SplitScreen_OnRenderImage_m32624963 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_SplitScreen_OnValidate_m2671377830 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::Update()
extern "C"  void CameraFilterPack_Blend2Camera_SplitScreen_Update_m117648316 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_SplitScreen_OnEnable_m1185429843 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_SplitScreen_OnDisable_m3805320266 (CameraFilterPack_Blend2Camera_SplitScreen_t3502727214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
