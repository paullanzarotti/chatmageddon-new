﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PurchasableItem
struct PurchasableItem_t3963353899;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.WritablePurchasable
struct  WritablePurchasable_t2700975424  : public Il2CppObject
{
public:
	// PurchasableItem Unibill.Impl.WritablePurchasable::<item>k__BackingField
	PurchasableItem_t3963353899 * ___U3CitemU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CitemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WritablePurchasable_t2700975424, ___U3CitemU3Ek__BackingField_0)); }
	inline PurchasableItem_t3963353899 * get_U3CitemU3Ek__BackingField_0() const { return ___U3CitemU3Ek__BackingField_0; }
	inline PurchasableItem_t3963353899 ** get_address_of_U3CitemU3Ek__BackingField_0() { return &___U3CitemU3Ek__BackingField_0; }
	inline void set_U3CitemU3Ek__BackingField_0(PurchasableItem_t3963353899 * value)
	{
		___U3CitemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
