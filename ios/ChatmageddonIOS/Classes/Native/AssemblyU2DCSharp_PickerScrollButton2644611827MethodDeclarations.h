﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickerScrollButton
struct PickerScrollButton_t2644611827;

#include "codegen/il2cpp-codegen.h"

// System.Void PickerScrollButton::.ctor()
extern "C"  void PickerScrollButton__ctor_m3510071586 (PickerScrollButton_t2644611827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerScrollButton::Awake()
extern "C"  void PickerScrollButton_Awake_m1098498159 (PickerScrollButton_t2644611827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerScrollButton::OnPress(System.Boolean)
extern "C"  void PickerScrollButton_OnPress_m1322196051 (PickerScrollButton_t2644611827 * __this, bool ___press0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerScrollButton::OnClick()
extern "C"  void PickerScrollButton_OnClick_m2501319145 (PickerScrollButton_t2644611827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerScrollButton::Update()
extern "C"  void PickerScrollButton_Update_m1333761851 (PickerScrollButton_t2644611827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
