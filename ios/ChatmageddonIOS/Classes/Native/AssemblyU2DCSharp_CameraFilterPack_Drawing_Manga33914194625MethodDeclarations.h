﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Manga3
struct CameraFilterPack_Drawing_Manga3_t3914194625;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Manga3::.ctor()
extern "C"  void CameraFilterPack_Drawing_Manga3__ctor_m92452118 (CameraFilterPack_Drawing_Manga3_t3914194625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga3::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Manga3_get_material_m3590794989 (CameraFilterPack_Drawing_Manga3_t3914194625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::Start()
extern "C"  void CameraFilterPack_Drawing_Manga3_Start_m3579658054 (CameraFilterPack_Drawing_Manga3_t3914194625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Manga3_OnRenderImage_m2832122998 (CameraFilterPack_Drawing_Manga3_t3914194625 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Manga3_OnValidate_m3077121439 (CameraFilterPack_Drawing_Manga3_t3914194625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::Update()
extern "C"  void CameraFilterPack_Drawing_Manga3_Update_m3863255177 (CameraFilterPack_Drawing_Manga3_t3914194625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Manga3_OnDisable_m3967663685 (CameraFilterPack_Drawing_Manga3_t3914194625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
