﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectAudienceNavScreen
struct SelectAudienceNavScreen_t4274980323;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void SelectAudienceNavScreen::.ctor()
extern "C"  void SelectAudienceNavScreen__ctor_m288111238 (SelectAudienceNavScreen_t4274980323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceNavScreen::Start()
extern "C"  void SelectAudienceNavScreen_Start_m389376490 (SelectAudienceNavScreen_t4274980323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceNavScreen::UIClosing()
extern "C"  void SelectAudienceNavScreen_UIClosing_m3396948137 (SelectAudienceNavScreen_t4274980323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void SelectAudienceNavScreen_ScreenLoad_m3798189250 (SelectAudienceNavScreen_t4274980323 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void SelectAudienceNavScreen_ScreenUnload_m4026362326 (SelectAudienceNavScreen_t4274980323 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SelectAudienceNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool SelectAudienceNavScreen_ValidateScreenNavigation_m889807845 (SelectAudienceNavScreen_t4274980323 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SelectAudienceNavScreen::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * SelectAudienceNavScreen_SaveScreenContent_m814174910 (SelectAudienceNavScreen_t4274980323 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
