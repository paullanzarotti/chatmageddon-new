﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelAward
struct  LevelAward_t2605567013  : public Il2CppObject
{
public:
	// System.Int32 LevelAward::rankLevel
	int32_t ___rankLevel_0;
	// System.Int32 LevelAward::bonusAmount
	int32_t ___bonusAmount_1;
	// System.String LevelAward::rankName
	String_t* ___rankName_2;

public:
	inline static int32_t get_offset_of_rankLevel_0() { return static_cast<int32_t>(offsetof(LevelAward_t2605567013, ___rankLevel_0)); }
	inline int32_t get_rankLevel_0() const { return ___rankLevel_0; }
	inline int32_t* get_address_of_rankLevel_0() { return &___rankLevel_0; }
	inline void set_rankLevel_0(int32_t value)
	{
		___rankLevel_0 = value;
	}

	inline static int32_t get_offset_of_bonusAmount_1() { return static_cast<int32_t>(offsetof(LevelAward_t2605567013, ___bonusAmount_1)); }
	inline int32_t get_bonusAmount_1() const { return ___bonusAmount_1; }
	inline int32_t* get_address_of_bonusAmount_1() { return &___bonusAmount_1; }
	inline void set_bonusAmount_1(int32_t value)
	{
		___bonusAmount_1 = value;
	}

	inline static int32_t get_offset_of_rankName_2() { return static_cast<int32_t>(offsetof(LevelAward_t2605567013, ___rankName_2)); }
	inline String_t* get_rankName_2() const { return ___rankName_2; }
	inline String_t** get_address_of_rankName_2() { return &___rankName_2; }
	inline void set_rankName_2(String_t* value)
	{
		___rankName_2 = value;
		Il2CppCodeGenWriteBarrier(&___rankName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
