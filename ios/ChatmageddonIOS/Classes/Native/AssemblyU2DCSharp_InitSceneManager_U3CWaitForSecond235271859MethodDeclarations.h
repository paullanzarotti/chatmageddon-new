﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InitSceneManager/<WaitForSeconds>c__Iterator1
struct U3CWaitForSecondsU3Ec__Iterator1_t235271859;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void InitSceneManager/<WaitForSeconds>c__Iterator1::.ctor()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1__ctor_m1080949782 (U3CWaitForSecondsU3Ec__Iterator1_t235271859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InitSceneManager/<WaitForSeconds>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitForSecondsU3Ec__Iterator1_MoveNext_m4077602514 (U3CWaitForSecondsU3Ec__Iterator1_t235271859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InitSceneManager/<WaitForSeconds>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2172274084 (U3CWaitForSecondsU3Ec__Iterator1_t235271859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InitSceneManager/<WaitForSeconds>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSecondsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2720939276 (U3CWaitForSecondsU3Ec__Iterator1_t235271859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager/<WaitForSeconds>c__Iterator1::Dispose()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1_Dispose_m3374106493 (U3CWaitForSecondsU3Ec__Iterator1_t235271859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager/<WaitForSeconds>c__Iterator1::Reset()
extern "C"  void U3CWaitForSecondsU3Ec__Iterator1_Reset_m1631760155 (U3CWaitForSecondsU3Ec__Iterator1_t235271859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
