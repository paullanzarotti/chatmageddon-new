﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>
struct KeyCollection_t1451935634;
// System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>
struct Dictionary_2_t3263405159;
// System.Collections.Generic.IEnumerator`1<Vectrosity.Vector3Pair>
struct IEnumerator_1_t334601965;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// Vectrosity.Vector3Pair[]
struct Vector3PairU5BU5D_t3460404447;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1657941301.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2736745241_gshared (KeyCollection_t1451935634 * __this, Dictionary_2_t3263405159 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2736745241(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1451935634 *, Dictionary_2_t3263405159 *, const MethodInfo*))KeyCollection__ctor_m2736745241_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m698849407_gshared (KeyCollection_t1451935634 * __this, Vector3Pair_t2859078138  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m698849407(__this, ___item0, method) ((  void (*) (KeyCollection_t1451935634 *, Vector3Pair_t2859078138 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m698849407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m731617772_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m731617772(__this, method) ((  void (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m731617772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1150186841_gshared (KeyCollection_t1451935634 * __this, Vector3Pair_t2859078138  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1150186841(__this, ___item0, method) ((  bool (*) (KeyCollection_t1451935634 *, Vector3Pair_t2859078138 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1150186841_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2765509484_gshared (KeyCollection_t1451935634 * __this, Vector3Pair_t2859078138  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2765509484(__this, ___item0, method) ((  bool (*) (KeyCollection_t1451935634 *, Vector3Pair_t2859078138 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2765509484_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3517615270_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3517615270(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3517615270_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1234518606_gshared (KeyCollection_t1451935634 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1234518606(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1451935634 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1234518606_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m983559071_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m983559071(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m983559071_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2430613314_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2430613314(__this, method) ((  bool (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2430613314_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1103137450_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1103137450(__this, method) ((  bool (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1103137450_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2780265246_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2780265246(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2780265246_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3324008144_gshared (KeyCollection_t1451935634 * __this, Vector3PairU5BU5D_t3460404447* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3324008144(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1451935634 *, Vector3PairU5BU5D_t3460404447*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3324008144_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::GetEnumerator()
extern "C"  Enumerator_t1657941301  KeyCollection_GetEnumerator_m2160661397_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2160661397(__this, method) ((  Enumerator_t1657941301  (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_GetEnumerator_m2160661397_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Vectrosity.Vector3Pair,System.Boolean>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m687487474_gshared (KeyCollection_t1451935634 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m687487474(__this, method) ((  int32_t (*) (KeyCollection_t1451935634 *, const MethodInfo*))KeyCollection_get_Count_m687487474_gshared)(__this, method)
