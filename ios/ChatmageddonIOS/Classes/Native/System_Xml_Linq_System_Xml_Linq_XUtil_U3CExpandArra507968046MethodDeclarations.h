﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25
struct U3CExpandArrayU3Ec__Iterator25_t507968046;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::.ctor()
extern "C"  void U3CExpandArrayU3Ec__Iterator25__ctor_m1792951425 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CExpandArrayU3Ec__Iterator25_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1878832491 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExpandArrayU3Ec__Iterator25_System_Collections_IEnumerator_get_Current_m1525932195 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CExpandArrayU3Ec__Iterator25_System_Collections_IEnumerable_GetEnumerator_m2252936218 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CExpandArrayU3Ec__Iterator25_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m634432390 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::MoveNext()
extern "C"  bool U3CExpandArrayU3Ec__Iterator25_MoveNext_m942794979 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::Dispose()
extern "C"  void U3CExpandArrayU3Ec__Iterator25_Dispose_m648767970 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XUtil/<ExpandArray>c__Iterator25::Reset()
extern "C"  void U3CExpandArrayU3Ec__Iterator25_Reset_m2626269592 (U3CExpandArrayU3Ec__Iterator25_t507968046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
