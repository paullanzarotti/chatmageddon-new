﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Noise
struct CameraFilterPack_Distortion_Noise_t3048789779;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Noise::.ctor()
extern "C"  void CameraFilterPack_Distortion_Noise__ctor_m1145272354 (CameraFilterPack_Distortion_Noise_t3048789779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Noise::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Noise_get_material_m3892126495 (CameraFilterPack_Distortion_Noise_t3048789779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::Start()
extern "C"  void CameraFilterPack_Distortion_Noise_Start_m3561617582 (CameraFilterPack_Distortion_Noise_t3048789779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Noise_OnRenderImage_m4086517822 (CameraFilterPack_Distortion_Noise_t3048789779 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Noise_OnValidate_m57642345 (CameraFilterPack_Distortion_Noise_t3048789779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::Update()
extern "C"  void CameraFilterPack_Distortion_Noise_Update_m1135478423 (CameraFilterPack_Distortion_Noise_t3048789779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Noise_OnDisable_m1557726359 (CameraFilterPack_Distortion_Noise_t3048789779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
