﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_50
struct CameraFilterPack_TV_50_t2536975289;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_50::.ctor()
extern "C"  void CameraFilterPack_TV_50__ctor_m393587300 (CameraFilterPack_TV_50_t2536975289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_50::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_50_get_material_m2546058457 (CameraFilterPack_TV_50_t2536975289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::Start()
extern "C"  void CameraFilterPack_TV_50_Start_m10754852 (CameraFilterPack_TV_50_t2536975289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_50_OnRenderImage_m42299220 (CameraFilterPack_TV_50_t2536975289 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::Update()
extern "C"  void CameraFilterPack_TV_50_Update_m1439329733 (CameraFilterPack_TV_50_t2536975289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::OnDisable()
extern "C"  void CameraFilterPack_TV_50_OnDisable_m609500881 (CameraFilterPack_TV_50_t2536975289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
