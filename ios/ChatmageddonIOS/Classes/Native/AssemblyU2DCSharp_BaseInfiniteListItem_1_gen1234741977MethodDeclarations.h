﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<StatusListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m82416199(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1234741977 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<StatusListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m1961014516(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t1234741977 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<StatusListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m2835953938(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1234741977 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<StatusListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m3707676209(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1234741977 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<StatusListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m3765719772(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t1234741977 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<StatusListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m608908327(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1234741977 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<StatusListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m2713736272(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1234741977 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
