﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// Friend
struct Friend_t3555014108;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AvatarChatUI
struct  AvatarChatUI_t2524884989  : public MonoBehaviour_t1158329972
{
public:
	// UILabel AvatarChatUI::newMessageAmountLabel
	UILabel_t1795115428 * ___newMessageAmountLabel_2;
	// Friend AvatarChatUI::friend
	Friend_t3555014108 * ___friend_3;
	// System.Int32 AvatarChatUI::newChatMessages
	int32_t ___newChatMessages_4;

public:
	inline static int32_t get_offset_of_newMessageAmountLabel_2() { return static_cast<int32_t>(offsetof(AvatarChatUI_t2524884989, ___newMessageAmountLabel_2)); }
	inline UILabel_t1795115428 * get_newMessageAmountLabel_2() const { return ___newMessageAmountLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_newMessageAmountLabel_2() { return &___newMessageAmountLabel_2; }
	inline void set_newMessageAmountLabel_2(UILabel_t1795115428 * value)
	{
		___newMessageAmountLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___newMessageAmountLabel_2, value);
	}

	inline static int32_t get_offset_of_friend_3() { return static_cast<int32_t>(offsetof(AvatarChatUI_t2524884989, ___friend_3)); }
	inline Friend_t3555014108 * get_friend_3() const { return ___friend_3; }
	inline Friend_t3555014108 ** get_address_of_friend_3() { return &___friend_3; }
	inline void set_friend_3(Friend_t3555014108 * value)
	{
		___friend_3 = value;
		Il2CppCodeGenWriteBarrier(&___friend_3, value);
	}

	inline static int32_t get_offset_of_newChatMessages_4() { return static_cast<int32_t>(offsetof(AvatarChatUI_t2524884989, ___newChatMessages_4)); }
	inline int32_t get_newChatMessages_4() const { return ___newChatMessages_4; }
	inline int32_t* get_address_of_newChatMessages_4() { return &___newChatMessages_4; }
	inline void set_newChatMessages_4(int32_t value)
	{
		___newChatMessages_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
