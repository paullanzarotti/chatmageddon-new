﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Color_BrightContrastSaturation
struct  CameraFilterPack_Color_BrightContrastSaturation_t3190703103  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Color_BrightContrastSaturation::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Color_BrightContrastSaturation::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Color_BrightContrastSaturation::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::Brightness
	float ___Brightness_6;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::Saturation
	float ___Saturation_7;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::Contrast
	float ___Contrast_8;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Brightness_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103, ___Brightness_6)); }
	inline float get_Brightness_6() const { return ___Brightness_6; }
	inline float* get_address_of_Brightness_6() { return &___Brightness_6; }
	inline void set_Brightness_6(float value)
	{
		___Brightness_6 = value;
	}

	inline static int32_t get_offset_of_Saturation_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103, ___Saturation_7)); }
	inline float get_Saturation_7() const { return ___Saturation_7; }
	inline float* get_address_of_Saturation_7() { return &___Saturation_7; }
	inline void set_Saturation_7(float value)
	{
		___Saturation_7 = value;
	}

	inline static int32_t get_offset_of_Contrast_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103, ___Contrast_8)); }
	inline float get_Contrast_8() const { return ___Contrast_8; }
	inline float* get_address_of_Contrast_8() { return &___Contrast_8; }
	inline void set_Contrast_8(float value)
	{
		___Contrast_8 = value;
	}
};

struct CameraFilterPack_Color_BrightContrastSaturation_t3190703103_StaticFields
{
public:
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::ChangeBrightness
	float ___ChangeBrightness_9;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::ChangeSaturation
	float ___ChangeSaturation_10;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::ChangeContrast
	float ___ChangeContrast_11;

public:
	inline static int32_t get_offset_of_ChangeBrightness_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103_StaticFields, ___ChangeBrightness_9)); }
	inline float get_ChangeBrightness_9() const { return ___ChangeBrightness_9; }
	inline float* get_address_of_ChangeBrightness_9() { return &___ChangeBrightness_9; }
	inline void set_ChangeBrightness_9(float value)
	{
		___ChangeBrightness_9 = value;
	}

	inline static int32_t get_offset_of_ChangeSaturation_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103_StaticFields, ___ChangeSaturation_10)); }
	inline float get_ChangeSaturation_10() const { return ___ChangeSaturation_10; }
	inline float* get_address_of_ChangeSaturation_10() { return &___ChangeSaturation_10; }
	inline void set_ChangeSaturation_10(float value)
	{
		___ChangeSaturation_10 = value;
	}

	inline static int32_t get_offset_of_ChangeContrast_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Color_BrightContrastSaturation_t3190703103_StaticFields, ___ChangeContrast_11)); }
	inline float get_ChangeContrast_11() const { return ___ChangeContrast_11; }
	inline float* get_address_of_ChangeContrast_11() { return &___ChangeContrast_11; }
	inline void set_ChangeContrast_11(float value)
	{
		___ChangeContrast_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
