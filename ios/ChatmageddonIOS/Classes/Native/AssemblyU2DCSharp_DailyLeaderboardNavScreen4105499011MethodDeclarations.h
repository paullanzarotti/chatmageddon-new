﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DailyLeaderboardNavScreen
struct DailyLeaderboardNavScreen_t4105499011;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void DailyLeaderboardNavScreen::.ctor()
extern "C"  void DailyLeaderboardNavScreen__ctor_m939111056 (DailyLeaderboardNavScreen_t4105499011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DailyLeaderboardNavScreen::Start()
extern "C"  void DailyLeaderboardNavScreen_Start_m146863348 (DailyLeaderboardNavScreen_t4105499011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DailyLeaderboardNavScreen::UIClosing()
extern "C"  void DailyLeaderboardNavScreen_UIClosing_m139299105 (DailyLeaderboardNavScreen_t4105499011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DailyLeaderboardNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void DailyLeaderboardNavScreen_ScreenLoad_m3042107360 (DailyLeaderboardNavScreen_t4105499011 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DailyLeaderboardNavScreen::UpdateUI()
extern "C"  void DailyLeaderboardNavScreen_UpdateUI_m3400655459 (DailyLeaderboardNavScreen_t4105499011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DailyLeaderboardNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void DailyLeaderboardNavScreen_ScreenUnload_m3504242636 (DailyLeaderboardNavScreen_t4105499011 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DailyLeaderboardNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool DailyLeaderboardNavScreen_ValidateScreenNavigation_m2707968437 (DailyLeaderboardNavScreen_t4105499011 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
