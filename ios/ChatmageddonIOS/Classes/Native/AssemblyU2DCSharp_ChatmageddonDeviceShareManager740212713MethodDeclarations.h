﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonDeviceShareManager
struct ChatmageddonDeviceShareManager_t740212713;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ShareRequestLocation2965642179.h"

// System.Void ChatmageddonDeviceShareManager::.ctor()
extern "C"  void ChatmageddonDeviceShareManager__ctor_m3038592300 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::Start()
extern "C"  void ChatmageddonDeviceShareManager_Start_m1378085844 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::OpenUI(ShareRequestLocation)
extern "C"  void ChatmageddonDeviceShareManager_OpenUI_m517523293 (ChatmageddonDeviceShareManager_t740212713 * __this, int32_t ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::CloseUI()
extern "C"  void ChatmageddonDeviceShareManager_CloseUI_m2087350318 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::UIClosing()
extern "C"  void ChatmageddonDeviceShareManager_UIClosing_m2137184987 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::SendPresetEmail()
extern "C"  void ChatmageddonDeviceShareManager_SendPresetEmail_m780297253 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::SendPresetSMS()
extern "C"  void ChatmageddonDeviceShareManager_SendPresetSMS_m3238867258 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::SendPresetFacebookStory()
extern "C"  void ChatmageddonDeviceShareManager_SendPresetFacebookStory_m2450761078 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonDeviceShareManager::SendPresetTweet()
extern "C"  void ChatmageddonDeviceShareManager_SendPresetTweet_m1897762746 (ChatmageddonDeviceShareManager_t740212713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
