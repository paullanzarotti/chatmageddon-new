﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsXMLList
struct OnlineMapsXMLList_t1635558505;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsXMLListEnum
struct  OnlineMapsXMLListEnum_t1723955324  : public Il2CppObject
{
public:
	// OnlineMapsXMLList OnlineMapsXMLListEnum::list
	OnlineMapsXMLList_t1635558505 * ___list_0;
	// System.Int32 OnlineMapsXMLListEnum::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(OnlineMapsXMLListEnum_t1723955324, ___list_0)); }
	inline OnlineMapsXMLList_t1635558505 * get_list_0() const { return ___list_0; }
	inline OnlineMapsXMLList_t1635558505 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(OnlineMapsXMLList_t1635558505 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier(&___list_0, value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(OnlineMapsXMLListEnum_t1723955324, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
