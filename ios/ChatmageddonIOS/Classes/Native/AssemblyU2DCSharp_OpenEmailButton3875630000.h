﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenEmailButton
struct  OpenEmailButton_t3875630000  : public SFXButton_t792651341
{
public:
	// System.String OpenEmailButton::toAddress
	String_t* ___toAddress_5;
	// System.String OpenEmailButton::subject
	String_t* ___subject_6;
	// System.String OpenEmailButton::message
	String_t* ___message_7;
	// System.Boolean OpenEmailButton::isHTML
	bool ___isHTML_8;

public:
	inline static int32_t get_offset_of_toAddress_5() { return static_cast<int32_t>(offsetof(OpenEmailButton_t3875630000, ___toAddress_5)); }
	inline String_t* get_toAddress_5() const { return ___toAddress_5; }
	inline String_t** get_address_of_toAddress_5() { return &___toAddress_5; }
	inline void set_toAddress_5(String_t* value)
	{
		___toAddress_5 = value;
		Il2CppCodeGenWriteBarrier(&___toAddress_5, value);
	}

	inline static int32_t get_offset_of_subject_6() { return static_cast<int32_t>(offsetof(OpenEmailButton_t3875630000, ___subject_6)); }
	inline String_t* get_subject_6() const { return ___subject_6; }
	inline String_t** get_address_of_subject_6() { return &___subject_6; }
	inline void set_subject_6(String_t* value)
	{
		___subject_6 = value;
		Il2CppCodeGenWriteBarrier(&___subject_6, value);
	}

	inline static int32_t get_offset_of_message_7() { return static_cast<int32_t>(offsetof(OpenEmailButton_t3875630000, ___message_7)); }
	inline String_t* get_message_7() const { return ___message_7; }
	inline String_t** get_address_of_message_7() { return &___message_7; }
	inline void set_message_7(String_t* value)
	{
		___message_7 = value;
		Il2CppCodeGenWriteBarrier(&___message_7, value);
	}

	inline static int32_t get_offset_of_isHTML_8() { return static_cast<int32_t>(offsetof(OpenEmailButton_t3875630000, ___isHTML_8)); }
	inline bool get_isHTML_8() const { return ___isHTML_8; }
	inline bool* get_address_of_isHTML_8() { return &___isHTML_8; }
	inline void set_isHTML_8(bool value)
	{
		___isHTML_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
