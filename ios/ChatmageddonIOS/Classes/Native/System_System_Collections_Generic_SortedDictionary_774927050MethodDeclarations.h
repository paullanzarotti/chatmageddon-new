﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t3931121312;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_774927050.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m973000199_gshared (Enumerator_t774927050 * __this, SortedDictionary_2_t3931121312 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m973000199(__this, ___dic0, method) ((  void (*) (Enumerator_t774927050 *, SortedDictionary_2_t3931121312 *, const MethodInfo*))Enumerator__ctor_m973000199_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3408025937_gshared (Enumerator_t774927050 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3408025937(__this, method) ((  Il2CppObject * (*) (Enumerator_t774927050 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3408025937_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1033959153_gshared (Enumerator_t774927050 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1033959153(__this, method) ((  void (*) (Enumerator_t774927050 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1033959153_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3613019339_gshared (Enumerator_t774927050 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3613019339(__this, method) ((  Il2CppObject * (*) (Enumerator_t774927050 *, const MethodInfo*))Enumerator_get_Current_m3613019339_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m842463125_gshared (Enumerator_t774927050 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m842463125(__this, method) ((  bool (*) (Enumerator_t774927050 *, const MethodInfo*))Enumerator_MoveNext_m842463125_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m992973076_gshared (Enumerator_t774927050 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m992973076(__this, method) ((  void (*) (Enumerator_t774927050 *, const MethodInfo*))Enumerator_Dispose_m992973076_gshared)(__this, method)
