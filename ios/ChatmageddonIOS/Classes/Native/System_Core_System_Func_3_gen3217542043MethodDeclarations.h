﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen1022329359MethodDeclarations.h"

// System.Void System.Func`3<UnityEngine.Vector2,OnlineMapsMarker3D,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m1672610858(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3217542043 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m1411306909_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<UnityEngine.Vector2,OnlineMapsMarker3D,UnityEngine.Vector2>::Invoke(T1,T2)
#define Func_3_Invoke_m2868037599(__this, ___arg10, ___arg21, method) ((  Vector2_t2243707579  (*) (Func_3_t3217542043 *, Vector2_t2243707579 , OnlineMapsMarker3D_t576815539 *, const MethodInfo*))Func_3_Invoke_m759642380_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<UnityEngine.Vector2,OnlineMapsMarker3D,UnityEngine.Vector2>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m3636858514(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3217542043 *, Vector2_t2243707579 , OnlineMapsMarker3D_t576815539 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3837365905_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<UnityEngine.Vector2,OnlineMapsMarker3D,UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m856872160(__this, ___result0, method) ((  Vector2_t2243707579  (*) (Func_3_t3217542043 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m2937922897_gshared)(__this, ___result0, method)
