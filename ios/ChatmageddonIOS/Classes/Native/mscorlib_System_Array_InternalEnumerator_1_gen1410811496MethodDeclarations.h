﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1410811496.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"

// System.Void System.Array/InternalEnumerator`1<Unibill.Impl.BillingPlatform>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2951847528_gshared (InternalEnumerator_1_t1410811496 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2951847528(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1410811496 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2951847528_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Unibill.Impl.BillingPlatform>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2125907920_gshared (InternalEnumerator_1_t1410811496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2125907920(__this, method) ((  void (*) (InternalEnumerator_1_t1410811496 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2125907920_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Unibill.Impl.BillingPlatform>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m669096732_gshared (InternalEnumerator_1_t1410811496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m669096732(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1410811496 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m669096732_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Unibill.Impl.BillingPlatform>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2603045263_gshared (InternalEnumerator_1_t1410811496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2603045263(__this, method) ((  void (*) (InternalEnumerator_1_t1410811496 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2603045263_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Unibill.Impl.BillingPlatform>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1795290944_gshared (InternalEnumerator_1_t1410811496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1795290944(__this, method) ((  bool (*) (InternalEnumerator_1_t1410811496 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1795290944_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Unibill.Impl.BillingPlatform>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m4160822615_gshared (InternalEnumerator_1_t1410811496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4160822615(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1410811496 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4160822615_gshared)(__this, method)
