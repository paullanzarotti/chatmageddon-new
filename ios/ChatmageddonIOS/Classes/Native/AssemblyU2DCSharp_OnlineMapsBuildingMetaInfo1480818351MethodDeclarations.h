﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBuildingMetaInfo
struct OnlineMapsBuildingMetaInfo_t1480818351;

#include "codegen/il2cpp-codegen.h"

// System.Void OnlineMapsBuildingMetaInfo::.ctor()
extern "C"  void OnlineMapsBuildingMetaInfo__ctor_m1514600920 (OnlineMapsBuildingMetaInfo_t1480818351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
