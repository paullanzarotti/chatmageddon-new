﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditEmailNavScreen/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1
struct U3CSaveScreenContentU3Ec__AnonStorey1_t930415982;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EditEmailNavScreen/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__AnonStorey1__ctor_m2009540453 (U3CSaveScreenContentU3Ec__AnonStorey1_t930415982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditEmailNavScreen/<SaveScreenContent>c__Iterator0/<SaveScreenContent>c__AnonStorey1::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void U3CSaveScreenContentU3Ec__AnonStorey1_U3CU3Em__0_m2347092455 (U3CSaveScreenContentU3Ec__AnonStorey1_t930415982 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
