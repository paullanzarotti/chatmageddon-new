﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen4065141571.h"
#include "AssemblyU2DCSharp_BuildPlatform673526295.h"
#include "AssemblyU2DCSharp_Developer680803182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildManager
struct  BuildManager_t19508555  : public MonoSingleton_1_t4065141571
{
public:
	// System.String BuildManager::buildVersion
	String_t* ___buildVersion_3;
	// System.Boolean BuildManager::developmentBuild
	bool ___developmentBuild_4;
	// BuildPlatform BuildManager::buildPlatform
	int32_t ___buildPlatform_5;
	// Developer BuildManager::developer
	int32_t ___developer_6;
	// System.String BuildManager::iTunesAppId
	String_t* ___iTunesAppId_7;
	// System.Boolean BuildManager::uiScalCalculated
	bool ___uiScalCalculated_8;
	// System.Single BuildManager::devResWidth
	float ___devResWidth_9;
	// System.Single BuildManager::devResHeight
	float ___devResHeight_10;
	// System.Single BuildManager::startScaleWidth
	float ___startScaleWidth_11;
	// System.Single BuildManager::startScaleHeight
	float ___startScaleHeight_12;
	// System.Single BuildManager::calcScaleAspect
	float ___calcScaleAspect_13;
	// System.Single BuildManager::calcScaleWidth
	float ___calcScaleWidth_14;
	// System.String BuildManager::UDID
	String_t* ___UDID_15;

public:
	inline static int32_t get_offset_of_buildVersion_3() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___buildVersion_3)); }
	inline String_t* get_buildVersion_3() const { return ___buildVersion_3; }
	inline String_t** get_address_of_buildVersion_3() { return &___buildVersion_3; }
	inline void set_buildVersion_3(String_t* value)
	{
		___buildVersion_3 = value;
		Il2CppCodeGenWriteBarrier(&___buildVersion_3, value);
	}

	inline static int32_t get_offset_of_developmentBuild_4() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___developmentBuild_4)); }
	inline bool get_developmentBuild_4() const { return ___developmentBuild_4; }
	inline bool* get_address_of_developmentBuild_4() { return &___developmentBuild_4; }
	inline void set_developmentBuild_4(bool value)
	{
		___developmentBuild_4 = value;
	}

	inline static int32_t get_offset_of_buildPlatform_5() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___buildPlatform_5)); }
	inline int32_t get_buildPlatform_5() const { return ___buildPlatform_5; }
	inline int32_t* get_address_of_buildPlatform_5() { return &___buildPlatform_5; }
	inline void set_buildPlatform_5(int32_t value)
	{
		___buildPlatform_5 = value;
	}

	inline static int32_t get_offset_of_developer_6() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___developer_6)); }
	inline int32_t get_developer_6() const { return ___developer_6; }
	inline int32_t* get_address_of_developer_6() { return &___developer_6; }
	inline void set_developer_6(int32_t value)
	{
		___developer_6 = value;
	}

	inline static int32_t get_offset_of_iTunesAppId_7() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___iTunesAppId_7)); }
	inline String_t* get_iTunesAppId_7() const { return ___iTunesAppId_7; }
	inline String_t** get_address_of_iTunesAppId_7() { return &___iTunesAppId_7; }
	inline void set_iTunesAppId_7(String_t* value)
	{
		___iTunesAppId_7 = value;
		Il2CppCodeGenWriteBarrier(&___iTunesAppId_7, value);
	}

	inline static int32_t get_offset_of_uiScalCalculated_8() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___uiScalCalculated_8)); }
	inline bool get_uiScalCalculated_8() const { return ___uiScalCalculated_8; }
	inline bool* get_address_of_uiScalCalculated_8() { return &___uiScalCalculated_8; }
	inline void set_uiScalCalculated_8(bool value)
	{
		___uiScalCalculated_8 = value;
	}

	inline static int32_t get_offset_of_devResWidth_9() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___devResWidth_9)); }
	inline float get_devResWidth_9() const { return ___devResWidth_9; }
	inline float* get_address_of_devResWidth_9() { return &___devResWidth_9; }
	inline void set_devResWidth_9(float value)
	{
		___devResWidth_9 = value;
	}

	inline static int32_t get_offset_of_devResHeight_10() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___devResHeight_10)); }
	inline float get_devResHeight_10() const { return ___devResHeight_10; }
	inline float* get_address_of_devResHeight_10() { return &___devResHeight_10; }
	inline void set_devResHeight_10(float value)
	{
		___devResHeight_10 = value;
	}

	inline static int32_t get_offset_of_startScaleWidth_11() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___startScaleWidth_11)); }
	inline float get_startScaleWidth_11() const { return ___startScaleWidth_11; }
	inline float* get_address_of_startScaleWidth_11() { return &___startScaleWidth_11; }
	inline void set_startScaleWidth_11(float value)
	{
		___startScaleWidth_11 = value;
	}

	inline static int32_t get_offset_of_startScaleHeight_12() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___startScaleHeight_12)); }
	inline float get_startScaleHeight_12() const { return ___startScaleHeight_12; }
	inline float* get_address_of_startScaleHeight_12() { return &___startScaleHeight_12; }
	inline void set_startScaleHeight_12(float value)
	{
		___startScaleHeight_12 = value;
	}

	inline static int32_t get_offset_of_calcScaleAspect_13() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___calcScaleAspect_13)); }
	inline float get_calcScaleAspect_13() const { return ___calcScaleAspect_13; }
	inline float* get_address_of_calcScaleAspect_13() { return &___calcScaleAspect_13; }
	inline void set_calcScaleAspect_13(float value)
	{
		___calcScaleAspect_13 = value;
	}

	inline static int32_t get_offset_of_calcScaleWidth_14() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___calcScaleWidth_14)); }
	inline float get_calcScaleWidth_14() const { return ___calcScaleWidth_14; }
	inline float* get_address_of_calcScaleWidth_14() { return &___calcScaleWidth_14; }
	inline void set_calcScaleWidth_14(float value)
	{
		___calcScaleWidth_14 = value;
	}

	inline static int32_t get_offset_of_UDID_15() { return static_cast<int32_t>(offsetof(BuildManager_t19508555, ___UDID_15)); }
	inline String_t* get_UDID_15() const { return ___UDID_15; }
	inline String_t** get_address_of_UDID_15() { return &___UDID_15; }
	inline void set_UDID_15(String_t* value)
	{
		___UDID_15 = value;
		Il2CppCodeGenWriteBarrier(&___UDID_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
