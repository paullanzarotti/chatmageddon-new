﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<BlockListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m67909078(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2290043410 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<BlockListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m82164091(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t2290043410 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<BlockListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m1001740177(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2290043410 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<BlockListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m4237985092(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2290043410 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<BlockListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m2705620567(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t2290043410 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<BlockListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m76507334(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2290043410 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<BlockListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m4028706289(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2290043410 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
