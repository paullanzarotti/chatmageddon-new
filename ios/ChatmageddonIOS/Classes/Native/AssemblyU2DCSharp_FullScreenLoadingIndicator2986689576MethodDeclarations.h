﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FullScreenLoadingIndicator
struct FullScreenLoadingIndicator_t2986689576;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void FullScreenLoadingIndicator::.ctor()
extern "C"  void FullScreenLoadingIndicator__ctor_m3871671011 (FullScreenLoadingIndicator_t2986689576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullScreenLoadingIndicator::ShowLoadingIndicator(System.Boolean,System.Boolean,UnityEngine.Vector3)
extern "C"  void FullScreenLoadingIndicator_ShowLoadingIndicator_m1832133266 (FullScreenLoadingIndicator_t2986689576 * __this, bool ___showBackground0, bool ___instant1, Vector3_t2243707580  ___indicatorPos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullScreenLoadingIndicator::HideLoadingIndicator(System.Boolean)
extern "C"  void FullScreenLoadingIndicator_HideLoadingIndicator_m262764011 (FullScreenLoadingIndicator_t2986689576 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullScreenLoadingIndicator::ShowBackground(System.Boolean)
extern "C"  void FullScreenLoadingIndicator_ShowBackground_m695148521 (FullScreenLoadingIndicator_t2986689576 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullScreenLoadingIndicator::HideBackground(System.Boolean)
extern "C"  void FullScreenLoadingIndicator_HideBackground_m4139750042 (FullScreenLoadingIndicator_t2986689576 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullScreenLoadingIndicator::AddIndicatorToScene()
extern "C"  void FullScreenLoadingIndicator_AddIndicatorToScene_m3253925548 (FullScreenLoadingIndicator_t2986689576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FullScreenLoadingIndicator::RemoveIndicatorFromScene()
extern "C"  void FullScreenLoadingIndicator_RemoveIndicatorFromScene_m2751035924 (FullScreenLoadingIndicator_t2986689576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
