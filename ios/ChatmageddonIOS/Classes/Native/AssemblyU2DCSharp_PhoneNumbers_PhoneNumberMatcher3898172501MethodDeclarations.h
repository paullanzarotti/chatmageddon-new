﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberMatcher
struct PhoneNumberMatcher_t3898172501;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.String
struct String_t;
// PhoneNumbers.PhoneNumberMatch
struct PhoneNumberMatch_t2163858580;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String[]
struct StringU5BU5D_t1642385972;
// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;
// PhoneNumbers.PhoneNumberMatcher/CheckGroups
struct CheckGroups_t2785688338;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil4155573397.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Len3057047663.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat441739224.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberMatcher_2785688338.h"

// System.Void PhoneNumbers.PhoneNumberMatcher::.cctor()
extern "C"  void PhoneNumberMatcher__cctor_m1428689533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberMatcher::.ctor(PhoneNumbers.PhoneNumberUtil,System.String,System.String,PhoneNumbers.PhoneNumberUtil/Leniency,System.Int64)
extern "C"  void PhoneNumberMatcher__ctor_m4261392608 (PhoneNumberMatcher_t3898172501 * __this, PhoneNumberUtil_t4155573397 * ___util0, String_t* ___text1, String_t* ___country2, int32_t ___leniency3, int64_t ___maxTries4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberMatcher::Limit(System.Int32,System.Int32)
extern "C"  String_t* PhoneNumberMatcher_Limit_m2889743722 (Il2CppObject * __this /* static, unused */, int32_t ___lower0, int32_t ___upper1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberMatch PhoneNumbers.PhoneNumberMatcher::Find(System.Int32)
extern "C"  PhoneNumberMatch_t2163858580 * PhoneNumberMatcher_Find_m3491157559 (PhoneNumberMatcher_t3898172501 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberMatcher::TrimAfterFirstMatch(System.Text.RegularExpressions.Regex,System.String)
extern "C"  String_t* PhoneNumberMatcher_TrimAfterFirstMatch_m2491930998 (Il2CppObject * __this /* static, unused */, Regex_t1803876613 * ___pattern0, String_t* ___candidate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::IsLatinLetter(System.Char)
extern "C"  bool PhoneNumberMatcher_IsLatinLetter_m4085214215 (Il2CppObject * __this /* static, unused */, Il2CppChar ___letter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::IsInvalidPunctuationSymbol(System.Char)
extern "C"  bool PhoneNumberMatcher_IsInvalidPunctuationSymbol_m1690138222 (Il2CppObject * __this /* static, unused */, Il2CppChar ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberMatcher::TrimAfterUnwantedChars(System.String)
extern "C"  String_t* PhoneNumberMatcher_TrimAfterUnwantedChars_m4037105772 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberMatch PhoneNumbers.PhoneNumberMatcher::ExtractMatch(System.String,System.Int32)
extern "C"  PhoneNumberMatch_t2163858580 * PhoneNumberMatcher_ExtractMatch_m3241087876 (PhoneNumberMatcher_t3898172501 * __this, String_t* ___candidate0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberMatch PhoneNumbers.PhoneNumberMatcher::ExtractInnerMatch(System.String,System.Int32)
extern "C"  PhoneNumberMatch_t2163858580 * PhoneNumberMatcher_ExtractInnerMatch_m3184603698 (PhoneNumberMatcher_t3898172501 * __this, String_t* ___candidate0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberMatch PhoneNumbers.PhoneNumberMatcher::ParseAndVerify(System.String,System.Int32)
extern "C"  PhoneNumberMatch_t2163858580 * PhoneNumberMatcher_ParseAndVerify_m3583662913 (PhoneNumberMatcher_t3898172501 * __this, String_t* ___candidate0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::AllNumberGroupsRemainGrouped(PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumber,System.Text.StringBuilder,System.String[])
extern "C"  bool PhoneNumberMatcher_AllNumberGroupsRemainGrouped_m2171239592 (Il2CppObject * __this /* static, unused */, PhoneNumberUtil_t4155573397 * ___util0, PhoneNumber_t814071929 * ___number1, StringBuilder_t1221177846 * ___normalizedCandidate2, StringU5BU5D_t1642385972* ___formattedNumberGroups3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::AllNumberGroupsAreExactlyPresent(PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumber,System.Text.StringBuilder,System.String[])
extern "C"  bool PhoneNumberMatcher_AllNumberGroupsAreExactlyPresent_m2306920027 (Il2CppObject * __this /* static, unused */, PhoneNumberUtil_t4155573397 * ___util0, PhoneNumber_t814071929 * ___number1, StringBuilder_t1221177846 * ___normalizedCandidate2, StringU5BU5D_t1642385972* ___formattedNumberGroups3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PhoneNumbers.PhoneNumberMatcher::GetNationalNumberGroups(PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumber,PhoneNumbers.NumberFormat)
extern "C"  StringU5BU5D_t1642385972* PhoneNumberMatcher_GetNationalNumberGroups_m858690840 (Il2CppObject * __this /* static, unused */, PhoneNumberUtil_t4155573397 * ___util0, PhoneNumber_t814071929 * ___number1, NumberFormat_t441739224 * ___formattingPattern2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::CheckNumberGroupingIsValid(PhoneNumbers.PhoneNumber,System.String,PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumberMatcher/CheckGroups)
extern "C"  bool PhoneNumberMatcher_CheckNumberGroupingIsValid_m1873610404 (Il2CppObject * __this /* static, unused */, PhoneNumber_t814071929 * ___number0, String_t* ___candidate1, PhoneNumberUtil_t4155573397 * ___util2, CheckGroups_t2785688338 * ___checker3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::ContainsMoreThanOneSlash(System.String)
extern "C"  bool PhoneNumberMatcher_ContainsMoreThanOneSlash_m1298305068 (Il2CppObject * __this /* static, unused */, String_t* ___candidate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::ContainsOnlyValidXChars(PhoneNumbers.PhoneNumber,System.String,PhoneNumbers.PhoneNumberUtil)
extern "C"  bool PhoneNumberMatcher_ContainsOnlyValidXChars_m4035632504 (Il2CppObject * __this /* static, unused */, PhoneNumber_t814071929 * ___number0, String_t* ___candidate1, PhoneNumberUtil_t4155573397 * ___util2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::IsNationalPrefixPresentIfRequired(PhoneNumbers.PhoneNumber,PhoneNumbers.PhoneNumberUtil)
extern "C"  bool PhoneNumberMatcher_IsNationalPrefixPresentIfRequired_m1911456123 (Il2CppObject * __this /* static, unused */, PhoneNumber_t814071929 * ___number0, PhoneNumberUtil_t4155573397 * ___util1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberMatch PhoneNumbers.PhoneNumberMatcher::get_Current()
extern "C"  PhoneNumberMatch_t2163858580 * PhoneNumberMatcher_get_Current_m2798096655 (PhoneNumberMatcher_t3898172501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PhoneNumbers.PhoneNumberMatcher::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * PhoneNumberMatcher_System_Collections_IEnumerator_get_Current_m968051578 (PhoneNumberMatcher_t3898172501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher::MoveNext()
extern "C"  bool PhoneNumberMatcher_MoveNext_m3358671996 (PhoneNumberMatcher_t3898172501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberMatcher::Reset()
extern "C"  void PhoneNumberMatcher_Reset_m2461169831 (PhoneNumberMatcher_t3898172501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberMatcher::Dispose()
extern "C"  void PhoneNumberMatcher_Dispose_m3424185161 (PhoneNumberMatcher_t3898172501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
