﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t4014198702;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m246445300_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m246445300(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m246445300_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Vector2_t2243707579  U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3897895097_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3897895097(__this, method) ((  Vector2_t2243707579  (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m3897895097_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1114137638_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1114137638(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m1114137638_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2202352717_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2202352717(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2202352717_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m26880472_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m26880472(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m26880472_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::MoveNext()
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3036450880_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3036450880(__this, method) ((  bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3036450880_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::Dispose()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m784306447_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m784306447(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m784306447_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.Vector3,UnityEngine.Vector2>::Reset()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m773198273_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m773198273(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3073286210 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m773198273_gshared)(__this, method)
