﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardPlayerItem
struct LeaderboardPlayerItem_t2673913181;
// User
struct User_t719925459;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_User719925459.h"

// System.Void LeaderboardPlayerItem::.ctor()
extern "C"  void LeaderboardPlayerItem__ctor_m4147267860 (LeaderboardPlayerItem_t2673913181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardPlayerItem::Awake()
extern "C"  void LeaderboardPlayerItem_Awake_m1257133261 (LeaderboardPlayerItem_t2673913181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardPlayerItem::PopulateItem(User,System.Boolean)
extern "C"  void LeaderboardPlayerItem_PopulateItem_m3689497169 (LeaderboardPlayerItem_t2673913181 * __this, User_t719925459 * ___newUser0, bool ___allTimeLeaderboard1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardPlayerItem::SetPlayerRank(System.Int32)
extern "C"  void LeaderboardPlayerItem_SetPlayerRank_m2226205186 (LeaderboardPlayerItem_t2673913181 * __this, int32_t ___rank0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardPlayerItem::SetItemVisible(System.Boolean)
extern "C"  void LeaderboardPlayerItem_SetItemVisible_m629345632 (LeaderboardPlayerItem_t2673913181 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardPlayerItem::OnPanelTweenFinished()
extern "C"  void LeaderboardPlayerItem_OnPanelTweenFinished_m3856071912 (LeaderboardPlayerItem_t2673913181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
