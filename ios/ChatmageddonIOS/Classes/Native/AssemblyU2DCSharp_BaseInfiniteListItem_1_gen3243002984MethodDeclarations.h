﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<CountryCodeListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m3129177888(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3243002984 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<CountryCodeListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m92006299(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t3243002984 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<CountryCodeListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m2166784957(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3243002984 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<CountryCodeListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m953856498(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3243002984 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<CountryCodeListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m906178031(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t3243002984 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<CountryCodeListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m2157743248(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3243002984 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<CountryCodeListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m2469425717(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3243002984 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
