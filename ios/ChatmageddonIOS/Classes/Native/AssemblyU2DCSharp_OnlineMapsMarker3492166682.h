﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "AssemblyU2DCSharp_OnlineMapsMarkerBase3900955221.h"
#include "AssemblyU2DCSharp_OnlineMapsAlign3858887827.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsMarker
struct  OnlineMapsMarker_t3492166682  : public OnlineMapsMarkerBase_t3900955221
{
public:
	// OnlineMapsAlign OnlineMapsMarker::align
	int32_t ___align_17;
	// System.Boolean OnlineMapsMarker::locked
	bool ___locked_18;
	// UnityEngine.Rect OnlineMapsMarker::markerColliderRect
	Rect_t3681755626  ___markerColliderRect_19;
	// UnityEngine.Texture2D OnlineMapsMarker::texture
	Texture2D_t3542995729 * ___texture_20;
	// UnityEngine.Color[] OnlineMapsMarker::_colors
	ColorU5BU5D_t672350442* ____colors_21;
	// System.Int32 OnlineMapsMarker::_height
	int32_t ____height_22;
	// System.Single OnlineMapsMarker::_rotation
	float ____rotation_23;
	// UnityEngine.Color[] OnlineMapsMarker::_rotatedColors
	ColorU5BU5D_t672350442* ____rotatedColors_24;
	// System.Int32 OnlineMapsMarker::_textureHeight
	int32_t ____textureHeight_25;
	// System.Int32 OnlineMapsMarker::_textureWidth
	int32_t ____textureWidth_26;
	// System.Int32 OnlineMapsMarker::_width
	int32_t ____width_27;

public:
	inline static int32_t get_offset_of_align_17() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ___align_17)); }
	inline int32_t get_align_17() const { return ___align_17; }
	inline int32_t* get_address_of_align_17() { return &___align_17; }
	inline void set_align_17(int32_t value)
	{
		___align_17 = value;
	}

	inline static int32_t get_offset_of_locked_18() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ___locked_18)); }
	inline bool get_locked_18() const { return ___locked_18; }
	inline bool* get_address_of_locked_18() { return &___locked_18; }
	inline void set_locked_18(bool value)
	{
		___locked_18 = value;
	}

	inline static int32_t get_offset_of_markerColliderRect_19() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ___markerColliderRect_19)); }
	inline Rect_t3681755626  get_markerColliderRect_19() const { return ___markerColliderRect_19; }
	inline Rect_t3681755626 * get_address_of_markerColliderRect_19() { return &___markerColliderRect_19; }
	inline void set_markerColliderRect_19(Rect_t3681755626  value)
	{
		___markerColliderRect_19 = value;
	}

	inline static int32_t get_offset_of_texture_20() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ___texture_20)); }
	inline Texture2D_t3542995729 * get_texture_20() const { return ___texture_20; }
	inline Texture2D_t3542995729 ** get_address_of_texture_20() { return &___texture_20; }
	inline void set_texture_20(Texture2D_t3542995729 * value)
	{
		___texture_20 = value;
		Il2CppCodeGenWriteBarrier(&___texture_20, value);
	}

	inline static int32_t get_offset_of__colors_21() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ____colors_21)); }
	inline ColorU5BU5D_t672350442* get__colors_21() const { return ____colors_21; }
	inline ColorU5BU5D_t672350442** get_address_of__colors_21() { return &____colors_21; }
	inline void set__colors_21(ColorU5BU5D_t672350442* value)
	{
		____colors_21 = value;
		Il2CppCodeGenWriteBarrier(&____colors_21, value);
	}

	inline static int32_t get_offset_of__height_22() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ____height_22)); }
	inline int32_t get__height_22() const { return ____height_22; }
	inline int32_t* get_address_of__height_22() { return &____height_22; }
	inline void set__height_22(int32_t value)
	{
		____height_22 = value;
	}

	inline static int32_t get_offset_of__rotation_23() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ____rotation_23)); }
	inline float get__rotation_23() const { return ____rotation_23; }
	inline float* get_address_of__rotation_23() { return &____rotation_23; }
	inline void set__rotation_23(float value)
	{
		____rotation_23 = value;
	}

	inline static int32_t get_offset_of__rotatedColors_24() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ____rotatedColors_24)); }
	inline ColorU5BU5D_t672350442* get__rotatedColors_24() const { return ____rotatedColors_24; }
	inline ColorU5BU5D_t672350442** get_address_of__rotatedColors_24() { return &____rotatedColors_24; }
	inline void set__rotatedColors_24(ColorU5BU5D_t672350442* value)
	{
		____rotatedColors_24 = value;
		Il2CppCodeGenWriteBarrier(&____rotatedColors_24, value);
	}

	inline static int32_t get_offset_of__textureHeight_25() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ____textureHeight_25)); }
	inline int32_t get__textureHeight_25() const { return ____textureHeight_25; }
	inline int32_t* get_address_of__textureHeight_25() { return &____textureHeight_25; }
	inline void set__textureHeight_25(int32_t value)
	{
		____textureHeight_25 = value;
	}

	inline static int32_t get_offset_of__textureWidth_26() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ____textureWidth_26)); }
	inline int32_t get__textureWidth_26() const { return ____textureWidth_26; }
	inline int32_t* get_address_of__textureWidth_26() { return &____textureWidth_26; }
	inline void set__textureWidth_26(int32_t value)
	{
		____textureWidth_26 = value;
	}

	inline static int32_t get_offset_of__width_27() { return static_cast<int32_t>(offsetof(OnlineMapsMarker_t3492166682, ____width_27)); }
	inline int32_t get__width_27() const { return ____width_27; }
	inline int32_t* get_address_of__width_27() { return &____width_27; }
	inline void set__width_27(int32_t value)
	{
		____width_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
