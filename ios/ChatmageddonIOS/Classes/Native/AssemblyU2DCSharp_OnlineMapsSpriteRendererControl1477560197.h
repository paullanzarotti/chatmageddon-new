﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;

#include "AssemblyU2DCSharp_OnlineMapsControlBase2D1235229338.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsSpriteRendererControl
struct  OnlineMapsSpriteRendererControl_t1477560197  : public OnlineMapsControlBase2D_t1235229338
{
public:
	// UnityEngine.Collider OnlineMapsSpriteRendererControl::_cl
	Collider_t3497673348 * ____cl_33;
	// UnityEngine.Collider2D OnlineMapsSpriteRendererControl::_cl2D
	Collider2D_t646061738 * ____cl2D_34;
	// UnityEngine.SpriteRenderer OnlineMapsSpriteRendererControl::spriteRenderer
	SpriteRenderer_t1209076198 * ___spriteRenderer_35;

public:
	inline static int32_t get_offset_of__cl_33() { return static_cast<int32_t>(offsetof(OnlineMapsSpriteRendererControl_t1477560197, ____cl_33)); }
	inline Collider_t3497673348 * get__cl_33() const { return ____cl_33; }
	inline Collider_t3497673348 ** get_address_of__cl_33() { return &____cl_33; }
	inline void set__cl_33(Collider_t3497673348 * value)
	{
		____cl_33 = value;
		Il2CppCodeGenWriteBarrier(&____cl_33, value);
	}

	inline static int32_t get_offset_of__cl2D_34() { return static_cast<int32_t>(offsetof(OnlineMapsSpriteRendererControl_t1477560197, ____cl2D_34)); }
	inline Collider2D_t646061738 * get__cl2D_34() const { return ____cl2D_34; }
	inline Collider2D_t646061738 ** get_address_of__cl2D_34() { return &____cl2D_34; }
	inline void set__cl2D_34(Collider2D_t646061738 * value)
	{
		____cl2D_34 = value;
		Il2CppCodeGenWriteBarrier(&____cl2D_34, value);
	}

	inline static int32_t get_offset_of_spriteRenderer_35() { return static_cast<int32_t>(offsetof(OnlineMapsSpriteRendererControl_t1477560197, ___spriteRenderer_35)); }
	inline SpriteRenderer_t1209076198 * get_spriteRenderer_35() const { return ___spriteRenderer_35; }
	inline SpriteRenderer_t1209076198 ** get_address_of_spriteRenderer_35() { return &___spriteRenderer_35; }
	inline void set_spriteRenderer_35(SpriteRenderer_t1209076198 * value)
	{
		___spriteRenderer_35 = value;
		Il2CppCodeGenWriteBarrier(&___spriteRenderer_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
