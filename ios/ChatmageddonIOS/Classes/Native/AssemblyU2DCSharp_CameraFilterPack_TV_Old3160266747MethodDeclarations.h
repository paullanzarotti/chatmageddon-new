﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Old
struct CameraFilterPack_TV_Old_t3160266747;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Old::.ctor()
extern "C"  void CameraFilterPack_TV_Old__ctor_m2516384344 (CameraFilterPack_TV_Old_t3160266747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Old::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Old_get_material_m3330303979 (CameraFilterPack_TV_Old_t3160266747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::Start()
extern "C"  void CameraFilterPack_TV_Old_Start_m1724137132 (CameraFilterPack_TV_Old_t3160266747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Old_OnRenderImage_m3927024540 (CameraFilterPack_TV_Old_t3160266747 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::OnValidate()
extern "C"  void CameraFilterPack_TV_Old_OnValidate_m401400465 (CameraFilterPack_TV_Old_t3160266747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::Update()
extern "C"  void CameraFilterPack_TV_Old_Update_m508717283 (CameraFilterPack_TV_Old_t3160266747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::OnDisable()
extern "C"  void CameraFilterPack_TV_Old_OnDisable_m4219829443 (CameraFilterPack_TV_Old_t3160266747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
