﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MicService/<MicUpdate>c__Iterator0
struct U3CMicUpdateU3Ec__Iterator0_t2245991590;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MicService/<MicUpdate>c__Iterator0::.ctor()
extern "C"  void U3CMicUpdateU3Ec__Iterator0__ctor_m1667468303 (U3CMicUpdateU3Ec__Iterator0_t2245991590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MicService/<MicUpdate>c__Iterator0::MoveNext()
extern "C"  bool U3CMicUpdateU3Ec__Iterator0_MoveNext_m368232705 (U3CMicUpdateU3Ec__Iterator0_t2245991590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MicService/<MicUpdate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMicUpdateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3863607893 (U3CMicUpdateU3Ec__Iterator0_t2245991590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MicService/<MicUpdate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMicUpdateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1442090077 (U3CMicUpdateU3Ec__Iterator0_t2245991590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MicService/<MicUpdate>c__Iterator0::Dispose()
extern "C"  void U3CMicUpdateU3Ec__Iterator0_Dispose_m4281122116 (U3CMicUpdateU3Ec__Iterator0_t2245991590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MicService/<MicUpdate>c__Iterator0::Reset()
extern "C"  void U3CMicUpdateU3Ec__Iterator0_Reset_m2739007998 (U3CMicUpdateU3Ec__Iterator0_t2245991590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
