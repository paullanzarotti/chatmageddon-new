﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIGridSizer
struct GUIGridSizer_t636944578;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t1194435593;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2108882777;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// System.Void UnityEngine.GUIGridSizer::.ctor(UnityEngine.GUIContent[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  void GUIGridSizer__ctor_m2667770459 (GUIGridSizer_t636944578 * __this, GUIContentU5BU5D_t1194435593* ___contents0, int32_t ___xCount1, GUIStyle_t1799908754 * ___buttonStyle2, GUILayoutOptionU5BU5D_t2108882777* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUIGridSizer::GetRect(UnityEngine.GUIContent[],System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C"  Rect_t3681755626  GUIGridSizer_GetRect_m404261829 (Il2CppObject * __this /* static, unused */, GUIContentU5BU5D_t1194435593* ___contents0, int32_t ___xCount1, GUIStyle_t1799908754 * ___style2, GUILayoutOptionU5BU5D_t2108882777* ___options3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUIGridSizer::get_rows()
extern "C"  int32_t GUIGridSizer_get_rows_m593625823 (GUIGridSizer_t636944578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
