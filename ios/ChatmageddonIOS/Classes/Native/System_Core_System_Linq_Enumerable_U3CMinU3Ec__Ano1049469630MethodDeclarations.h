﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<Min>c__AnonStorey2E`1<UnityEngine.Vector3>
struct U3CMinU3Ec__AnonStorey2E_1_t1049469630;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void System.Linq.Enumerable/<Min>c__AnonStorey2E`1<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CMinU3Ec__AnonStorey2E_1__ctor_m3711901291_gshared (U3CMinU3Ec__AnonStorey2E_1_t1049469630 * __this, const MethodInfo* method);
#define U3CMinU3Ec__AnonStorey2E_1__ctor_m3711901291(__this, method) ((  void (*) (U3CMinU3Ec__AnonStorey2E_1_t1049469630 *, const MethodInfo*))U3CMinU3Ec__AnonStorey2E_1__ctor_m3711901291_gshared)(__this, method)
// System.Single System.Linq.Enumerable/<Min>c__AnonStorey2E`1<UnityEngine.Vector3>::<>m__2F(TSource,System.Single)
extern "C"  float U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m4227098510_gshared (U3CMinU3Ec__AnonStorey2E_1_t1049469630 * __this, Vector3_t2243707580  ___a0, float ___b1, const MethodInfo* method);
#define U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m4227098510(__this, ___a0, ___b1, method) ((  float (*) (U3CMinU3Ec__AnonStorey2E_1_t1049469630 *, Vector3_t2243707580 , float, const MethodInfo*))U3CMinU3Ec__AnonStorey2E_1_U3CU3Em__2F_m4227098510_gshared)(__this, ___a0, ___b1, method)
