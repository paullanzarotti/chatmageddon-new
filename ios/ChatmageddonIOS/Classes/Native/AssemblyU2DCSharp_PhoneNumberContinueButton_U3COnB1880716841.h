﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PhoneNumberContinueButton
struct PhoneNumberContinueButton_t1818508624;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumberContinueButton/<OnButtonClick>c__AnonStorey0
struct  U3COnButtonClickU3Ec__AnonStorey0_t1880716841  : public Il2CppObject
{
public:
	// System.String PhoneNumberContinueButton/<OnButtonClick>c__AnonStorey0::countyCode
	String_t* ___countyCode_0;
	// PhoneNumberContinueButton PhoneNumberContinueButton/<OnButtonClick>c__AnonStorey0::$this
	PhoneNumberContinueButton_t1818508624 * ___U24this_1;

public:
	inline static int32_t get_offset_of_countyCode_0() { return static_cast<int32_t>(offsetof(U3COnButtonClickU3Ec__AnonStorey0_t1880716841, ___countyCode_0)); }
	inline String_t* get_countyCode_0() const { return ___countyCode_0; }
	inline String_t** get_address_of_countyCode_0() { return &___countyCode_0; }
	inline void set_countyCode_0(String_t* value)
	{
		___countyCode_0 = value;
		Il2CppCodeGenWriteBarrier(&___countyCode_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnButtonClickU3Ec__AnonStorey0_t1880716841, ___U24this_1)); }
	inline PhoneNumberContinueButton_t1818508624 * get_U24this_1() const { return ___U24this_1; }
	inline PhoneNumberContinueButton_t1818508624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PhoneNumberContinueButton_t1818508624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
