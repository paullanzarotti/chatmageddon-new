﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.TempFileCollection
struct TempFileCollection_t3377240462;
// System.String
struct String_t;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.CodeDom.Compiler.TempFileCollection::.ctor()
extern "C"  void TempFileCollection__ctor_m1183749001 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::.ctor(System.String,System.Boolean)
extern "C"  void TempFileCollection__ctor_m2084608134 (TempFileCollection_t3377240462 * __this, String_t* ___tempDir0, bool ___keepFiles1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.CodeDom.Compiler.TempFileCollection::System.Collections.ICollection.get_Count()
extern "C"  int32_t TempFileCollection_System_Collections_ICollection_get_Count_m3176049490 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void TempFileCollection_System_Collections_ICollection_CopyTo_m3315299561 (TempFileCollection_t3377240462 * __this, Il2CppArray * ___array0, int32_t ___start1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.CodeDom.Compiler.TempFileCollection::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * TempFileCollection_System_Collections_ICollection_get_SyncRoot_m1569163961 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.CodeDom.Compiler.TempFileCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool TempFileCollection_System_Collections_ICollection_get_IsSynchronized_m1075267001 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::System.IDisposable.Dispose()
extern "C"  void TempFileCollection_System_IDisposable_Dispose_m1265943794 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.CodeDom.Compiler.TempFileCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * TempFileCollection_System_Collections_IEnumerable_GetEnumerator_m3939176772 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.TempFileCollection::get_BasePath()
extern "C"  String_t* TempFileCollection_get_BasePath_m1275517473 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.TempFileCollection::GetOwnTempDir()
extern "C"  String_t* TempFileCollection_GetOwnTempDir_m2684783687 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.CodeDom.Compiler.TempFileCollection::get_Count()
extern "C"  int32_t TempFileCollection_get_Count_m2039158649 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::set_KeepFiles(System.Boolean)
extern "C"  void TempFileCollection_set_KeepFiles_m17580218 (TempFileCollection_t3377240462 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.TempFileCollection::AddExtension(System.String)
extern "C"  String_t* TempFileCollection_AddExtension_m1079024257 (TempFileCollection_t3377240462 * __this, String_t* ___fileExtension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.CodeDom.Compiler.TempFileCollection::AddExtension(System.String,System.Boolean)
extern "C"  String_t* TempFileCollection_AddExtension_m534084181 (TempFileCollection_t3377240462 * __this, String_t* ___fileExtension0, bool ___keepFile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::AddFile(System.String,System.Boolean)
extern "C"  void TempFileCollection_AddFile_m1746247075 (TempFileCollection_t3377240462 * __this, String_t* ___fileName0, bool ___keepFile1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::Delete()
extern "C"  void TempFileCollection_Delete_m2462551503 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.CodeDom.Compiler.TempFileCollection::GetEnumerator()
extern "C"  Il2CppObject * TempFileCollection_GetEnumerator_m2192825801 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::Dispose(System.Boolean)
extern "C"  void TempFileCollection_Dispose_m3831317965 (TempFileCollection_t3377240462 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.TempFileCollection::Finalize()
extern "C"  void TempFileCollection_Finalize_m2233187059 (TempFileCollection_t3377240462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.CodeDom.Compiler.TempFileCollection::mkdir(System.String,System.UInt32)
extern "C"  int32_t TempFileCollection_mkdir_m1644471758 (Il2CppObject * __this /* static, unused */, String_t* ___olpath0, uint32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
