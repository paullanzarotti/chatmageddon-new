﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// ChatmageddonServer
struct ChatmageddonServer_t594474938;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonServer/<RegisterNewUser>c__AnonStorey1
struct  U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153  : public Il2CppObject
{
public:
	// System.Action`2<System.Boolean,System.String> ChatmageddonServer/<RegisterNewUser>c__AnonStorey1::callBack
	Action_2_t1865222972 * ___callBack_0;
	// ChatmageddonServer ChatmageddonServer/<RegisterNewUser>c__AnonStorey1::$this
	ChatmageddonServer_t594474938 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callBack_0() { return static_cast<int32_t>(offsetof(U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153, ___callBack_0)); }
	inline Action_2_t1865222972 * get_callBack_0() const { return ___callBack_0; }
	inline Action_2_t1865222972 ** get_address_of_callBack_0() { return &___callBack_0; }
	inline void set_callBack_0(Action_2_t1865222972 * value)
	{
		___callBack_0 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153, ___U24this_1)); }
	inline ChatmageddonServer_t594474938 * get_U24this_1() const { return ___U24this_1; }
	inline ChatmageddonServer_t594474938 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ChatmageddonServer_t594474938 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
