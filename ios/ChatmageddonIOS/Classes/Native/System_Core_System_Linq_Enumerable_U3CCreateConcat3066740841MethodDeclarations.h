﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>
struct U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::.ctor()
extern "C"  void U3CCreateConcatIteratorU3Ec__Iterator1_1__ctor_m2209438476_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1__ctor_m2209438476(__this, method) ((  void (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1__ctor_m2209438476_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3078800097_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3078800097(__this, method) ((  Il2CppObject * (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3078800097_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1072057552_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1072057552(__this, method) ((  Il2CppObject * (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1072057552_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerable_GetEnumerator_m601628501_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerable_GetEnumerator_m601628501(__this, method) ((  Il2CppObject * (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_IEnumerable_GetEnumerator_m601628501_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1102237490_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1102237490(__this, method) ((  Il2CppObject* (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1102237490_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateConcatIteratorU3Ec__Iterator1_1_MoveNext_m1871966524_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1_MoveNext_m1871966524(__this, method) ((  bool (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1_MoveNext_m1871966524_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::Dispose()
extern "C"  void U3CCreateConcatIteratorU3Ec__Iterator1_1_Dispose_m4263097911_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1_Dispose_m4263097911(__this, method) ((  void (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1_Dispose_m4263097911_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateConcatIterator>c__Iterator1`1<System.Object>::Reset()
extern "C"  void U3CCreateConcatIteratorU3Ec__Iterator1_1_Reset_m2389309517_gshared (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 * __this, const MethodInfo* method);
#define U3CCreateConcatIteratorU3Ec__Iterator1_1_Reset_m2389309517(__this, method) ((  void (*) (U3CCreateConcatIteratorU3Ec__Iterator1_1_t3066740841 *, const MethodInfo*))U3CCreateConcatIteratorU3Ec__Iterator1_1_Reset_m2389309517_gshared)(__this, method)
