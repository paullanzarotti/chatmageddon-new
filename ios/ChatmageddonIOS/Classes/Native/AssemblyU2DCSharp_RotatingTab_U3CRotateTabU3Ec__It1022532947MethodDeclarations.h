﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotatingTab/<RotateTab>c__Iterator0
struct U3CRotateTabU3Ec__Iterator0_t1022532947;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RotatingTab/<RotateTab>c__Iterator0::.ctor()
extern "C"  void U3CRotateTabU3Ec__Iterator0__ctor_m1438867638 (U3CRotateTabU3Ec__Iterator0_t1022532947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RotatingTab/<RotateTab>c__Iterator0::MoveNext()
extern "C"  bool U3CRotateTabU3Ec__Iterator0_MoveNext_m1590305010 (U3CRotateTabU3Ec__Iterator0_t1022532947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RotatingTab/<RotateTab>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRotateTabU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2186231204 (U3CRotateTabU3Ec__Iterator0_t1022532947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RotatingTab/<RotateTab>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRotateTabU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2328515212 (U3CRotateTabU3Ec__Iterator0_t1022532947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab/<RotateTab>c__Iterator0::Dispose()
extern "C"  void U3CRotateTabU3Ec__Iterator0_Dispose_m3403354781 (U3CRotateTabU3Ec__Iterator0_t1022532947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab/<RotateTab>c__Iterator0::Reset()
extern "C"  void U3CRotateTabU3Ec__Iterator0_Reset_m3704971899 (U3CRotateTabU3Ec__Iterator0_t1022532947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
