﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.List`1<System.Collections.Generic.HashSet`1<System.String>>
struct List_1_t4026769515;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Func`2<System.String,System.String>
struct Func_2_t193026957;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.MappingFileProvider
struct  MappingFileProvider_t2052216305  : public Il2CppObject
{
public:
	// System.Int32 PhoneNumbers.MappingFileProvider::numOfEntries
	int32_t ___numOfEntries_0;
	// System.Int32[] PhoneNumbers.MappingFileProvider::countryCallingCodes
	Int32U5BU5D_t3030399641* ___countryCallingCodes_1;
	// System.Collections.Generic.List`1<System.Collections.Generic.HashSet`1<System.String>> PhoneNumbers.MappingFileProvider::availableLanguages
	List_1_t4026769515 * ___availableLanguages_2;

public:
	inline static int32_t get_offset_of_numOfEntries_0() { return static_cast<int32_t>(offsetof(MappingFileProvider_t2052216305, ___numOfEntries_0)); }
	inline int32_t get_numOfEntries_0() const { return ___numOfEntries_0; }
	inline int32_t* get_address_of_numOfEntries_0() { return &___numOfEntries_0; }
	inline void set_numOfEntries_0(int32_t value)
	{
		___numOfEntries_0 = value;
	}

	inline static int32_t get_offset_of_countryCallingCodes_1() { return static_cast<int32_t>(offsetof(MappingFileProvider_t2052216305, ___countryCallingCodes_1)); }
	inline Int32U5BU5D_t3030399641* get_countryCallingCodes_1() const { return ___countryCallingCodes_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_countryCallingCodes_1() { return &___countryCallingCodes_1; }
	inline void set_countryCallingCodes_1(Int32U5BU5D_t3030399641* value)
	{
		___countryCallingCodes_1 = value;
		Il2CppCodeGenWriteBarrier(&___countryCallingCodes_1, value);
	}

	inline static int32_t get_offset_of_availableLanguages_2() { return static_cast<int32_t>(offsetof(MappingFileProvider_t2052216305, ___availableLanguages_2)); }
	inline List_1_t4026769515 * get_availableLanguages_2() const { return ___availableLanguages_2; }
	inline List_1_t4026769515 ** get_address_of_availableLanguages_2() { return &___availableLanguages_2; }
	inline void set_availableLanguages_2(List_1_t4026769515 * value)
	{
		___availableLanguages_2 = value;
		Il2CppCodeGenWriteBarrier(&___availableLanguages_2, value);
	}
};

struct MappingFileProvider_t2052216305_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PhoneNumbers.MappingFileProvider::LOCALE_NORMALIZATION_MAP
	Dictionary_2_t3943999495 * ___LOCALE_NORMALIZATION_MAP_3;
	// System.Func`2<System.String,System.String> PhoneNumbers.MappingFileProvider::<>f__am$cache0
	Func_2_t193026957 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_LOCALE_NORMALIZATION_MAP_3() { return static_cast<int32_t>(offsetof(MappingFileProvider_t2052216305_StaticFields, ___LOCALE_NORMALIZATION_MAP_3)); }
	inline Dictionary_2_t3943999495 * get_LOCALE_NORMALIZATION_MAP_3() const { return ___LOCALE_NORMALIZATION_MAP_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_LOCALE_NORMALIZATION_MAP_3() { return &___LOCALE_NORMALIZATION_MAP_3; }
	inline void set_LOCALE_NORMALIZATION_MAP_3(Dictionary_2_t3943999495 * value)
	{
		___LOCALE_NORMALIZATION_MAP_3 = value;
		Il2CppCodeGenWriteBarrier(&___LOCALE_NORMALIZATION_MAP_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(MappingFileProvider_t2052216305_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_t193026957 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_t193026957 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_t193026957 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
