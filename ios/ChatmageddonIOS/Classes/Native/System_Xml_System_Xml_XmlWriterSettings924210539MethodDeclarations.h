﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t924210539;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_System_Xml_NewLineHandling1737195169.h"
#include "System_Xml_System_Xml_NamespaceHandling1452270444.h"

// System.Void System.Xml.XmlWriterSettings::.ctor()
extern "C"  void XmlWriterSettings__ctor_m589782448 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriterSettings::Reset()
extern "C"  void XmlWriterSettings_Reset_m1023481843 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_CheckCharacters()
extern "C"  bool XmlWriterSettings_get_CheckCharacters_m3085311027 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_CloseOutput()
extern "C"  bool XmlWriterSettings_get_CloseOutput_m3532225420 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.ConformanceLevel System.Xml.XmlWriterSettings::get_ConformanceLevel()
extern "C"  int32_t XmlWriterSettings_get_ConformanceLevel_m4171034860 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriterSettings::set_ConformanceLevel(System.Xml.ConformanceLevel)
extern "C"  void XmlWriterSettings_set_ConformanceLevel_m528648687 (XmlWriterSettings_t924210539 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_Indent()
extern "C"  bool XmlWriterSettings_get_Indent_m2394532035 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriterSettings::set_Indent(System.Boolean)
extern "C"  void XmlWriterSettings_set_Indent_m1832140252 (XmlWriterSettings_t924210539 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWriterSettings::get_IndentChars()
extern "C"  String_t* XmlWriterSettings_get_IndentChars_m3783143045 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWriterSettings::get_NewLineChars()
extern "C"  String_t* XmlWriterSettings_get_NewLineChars_m1095401291 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_NewLineOnAttributes()
extern "C"  bool XmlWriterSettings_get_NewLineOnAttributes_m130380555 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.NewLineHandling System.Xml.XmlWriterSettings::get_NewLineHandling()
extern "C"  int32_t XmlWriterSettings_get_NewLineHandling_m2543665922 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_OmitXmlDeclaration()
extern "C"  bool XmlWriterSettings_get_OmitXmlDeclaration_m1678887493 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.NamespaceHandling System.Xml.XmlWriterSettings::get_NamespaceHandling()
extern "C"  int32_t XmlWriterSettings_get_NamespaceHandling_m546909538 (XmlWriterSettings_t924210539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
