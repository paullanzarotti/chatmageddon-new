﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<SavePushToken>c__AnonStorey1E<System.Object>
struct U3CSavePushTokenU3Ec__AnonStorey1E_t2263368801;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<SavePushToken>c__AnonStorey1E<System.Object>::.ctor()
extern "C"  void U3CSavePushTokenU3Ec__AnonStorey1E__ctor_m523752930_gshared (U3CSavePushTokenU3Ec__AnonStorey1E_t2263368801 * __this, const MethodInfo* method);
#define U3CSavePushTokenU3Ec__AnonStorey1E__ctor_m523752930(__this, method) ((  void (*) (U3CSavePushTokenU3Ec__AnonStorey1E_t2263368801 *, const MethodInfo*))U3CSavePushTokenU3Ec__AnonStorey1E__ctor_m523752930_gshared)(__this, method)
// System.Void BaseServer`1/<SavePushToken>c__AnonStorey1E<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CSavePushTokenU3Ec__AnonStorey1E_U3CU3Em__0_m4089374437_gshared (U3CSavePushTokenU3Ec__AnonStorey1E_t2263368801 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CSavePushTokenU3Ec__AnonStorey1E_U3CU3Em__0_m4089374437(__this, ___request0, ___response1, method) ((  void (*) (U3CSavePushTokenU3Ec__AnonStorey1E_t2263368801 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CSavePushTokenU3Ec__AnonStorey1E_U3CU3Em__0_m4089374437_gshared)(__this, ___request0, ___response1, method)
