﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraEventListener
struct EtceteraEventListener_t3310591661;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EtceteraEventListener::.ctor()
extern "C"  void EtceteraEventListener__ctor_m335372334 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::OnEnable()
extern "C"  void EtceteraEventListener_OnEnable_m1110532098 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::OnDisable()
extern "C"  void EtceteraEventListener_OnDisable_m1433618923 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::dismissingViewControllerEvent()
extern "C"  void EtceteraEventListener_dismissingViewControllerEvent_m893831381 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::imagePickerCancelled()
extern "C"  void EtceteraEventListener_imagePickerCancelled_m2557027982 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::imagePickerChoseImage(System.String)
extern "C"  void EtceteraEventListener_imagePickerChoseImage_m852493542 (EtceteraEventListener_t3310591661 * __this, String_t* ___imagePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::saveImageToPhotoAlbumSucceededEvent()
extern "C"  void EtceteraEventListener_saveImageToPhotoAlbumSucceededEvent_m511815279 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::saveImageToPhotoAlbumFailedEvent(System.String)
extern "C"  void EtceteraEventListener_saveImageToPhotoAlbumFailedEvent_m3559767625 (EtceteraEventListener_t3310591661 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::alertButtonClicked(System.String)
extern "C"  void EtceteraEventListener_alertButtonClicked_m862610531 (EtceteraEventListener_t3310591661 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::promptCancelled()
extern "C"  void EtceteraEventListener_promptCancelled_m2174838641 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::singleFieldPromptTextEntered(System.String)
extern "C"  void EtceteraEventListener_singleFieldPromptTextEntered_m3588200518 (EtceteraEventListener_t3310591661 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::twoFieldPromptTextEntered(System.String,System.String)
extern "C"  void EtceteraEventListener_twoFieldPromptTextEntered_m4015843342 (EtceteraEventListener_t3310591661 * __this, String_t* ___textOne0, String_t* ___textTwo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::remoteRegistrationSucceeded(System.String)
extern "C"  void EtceteraEventListener_remoteRegistrationSucceeded_m1959942448 (EtceteraEventListener_t3310591661 * __this, String_t* ___deviceToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::remoteRegistrationFailed(System.String)
extern "C"  void EtceteraEventListener_remoteRegistrationFailed_m571802416 (EtceteraEventListener_t3310591661 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::pushIORegistrationCompletedEvent(System.String)
extern "C"  void EtceteraEventListener_pushIORegistrationCompletedEvent_m1898891968 (EtceteraEventListener_t3310591661 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::urbanAirshipRegistrationSucceeded()
extern "C"  void EtceteraEventListener_urbanAirshipRegistrationSucceeded_m3254002082 (EtceteraEventListener_t3310591661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::urbanAirshipRegistrationFailed(System.String)
extern "C"  void EtceteraEventListener_urbanAirshipRegistrationFailed_m3085009176 (EtceteraEventListener_t3310591661 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::remoteNotificationReceived(System.Collections.IDictionary)
extern "C"  void EtceteraEventListener_remoteNotificationReceived_m2814886149 (EtceteraEventListener_t3310591661 * __this, Il2CppObject * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::remoteNotificationReceivedAtLaunch(System.Collections.IDictionary)
extern "C"  void EtceteraEventListener_remoteNotificationReceivedAtLaunch_m2075783901 (EtceteraEventListener_t3310591661 * __this, Il2CppObject * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::localNotificationWasReceivedEvent(System.Collections.IDictionary)
extern "C"  void EtceteraEventListener_localNotificationWasReceivedEvent_m2359859493 (EtceteraEventListener_t3310591661 * __this, Il2CppObject * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::localNotificationWasReceivedAtLaunchEvent(System.Collections.IDictionary)
extern "C"  void EtceteraEventListener_localNotificationWasReceivedAtLaunchEvent_m2713562845 (EtceteraEventListener_t3310591661 * __this, Il2CppObject * ___notification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::mailComposerFinished(System.String)
extern "C"  void EtceteraEventListener_mailComposerFinished_m906805949 (EtceteraEventListener_t3310591661 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraEventListener::smsComposerFinished(System.String)
extern "C"  void EtceteraEventListener_smsComposerFinished_m347353041 (EtceteraEventListener_t3310591661 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
