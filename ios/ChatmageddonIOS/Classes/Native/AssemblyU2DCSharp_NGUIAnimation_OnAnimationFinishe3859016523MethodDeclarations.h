﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIAnimation/OnAnimationFinished
struct OnAnimationFinished_t3859016523;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void NGUIAnimation/OnAnimationFinished::.ctor(System.Object,System.IntPtr)
extern "C"  void OnAnimationFinished__ctor_m1191105928 (OnAnimationFinished_t3859016523 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationFinished::Invoke()
extern "C"  void OnAnimationFinished_Invoke_m283204114 (OnAnimationFinished_t3859016523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult NGUIAnimation/OnAnimationFinished::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnAnimationFinished_BeginInvoke_m675630757 (OnAnimationFinished_t3859016523 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation/OnAnimationFinished::EndInvoke(System.IAsyncResult)
extern "C"  void OnAnimationFinished_EndInvoke_m4089404478 (OnAnimationFinished_t3859016523 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
