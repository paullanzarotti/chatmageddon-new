﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// CountdownTimer/OnTimerFinished
struct OnTimerFinished_t1234642247;

#include "AssemblyU2DCSharp_ProgressBar192201240.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountdownTimer
struct  CountdownTimer_t4123051008  : public ProgressBar_t192201240
{
public:
	// UILabel CountdownTimer::countdownLabel
	UILabel_t1795115428 * ___countdownLabel_8;
	// System.Single CountdownTimer::countdownTimeAmount
	float ___countdownTimeAmount_9;
	// CountdownTimer/OnTimerFinished CountdownTimer::onTimerFinished
	OnTimerFinished_t1234642247 * ___onTimerFinished_10;
	// System.Int32 CountdownTimer::numberOn
	int32_t ___numberOn_11;

public:
	inline static int32_t get_offset_of_countdownLabel_8() { return static_cast<int32_t>(offsetof(CountdownTimer_t4123051008, ___countdownLabel_8)); }
	inline UILabel_t1795115428 * get_countdownLabel_8() const { return ___countdownLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_countdownLabel_8() { return &___countdownLabel_8; }
	inline void set_countdownLabel_8(UILabel_t1795115428 * value)
	{
		___countdownLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___countdownLabel_8, value);
	}

	inline static int32_t get_offset_of_countdownTimeAmount_9() { return static_cast<int32_t>(offsetof(CountdownTimer_t4123051008, ___countdownTimeAmount_9)); }
	inline float get_countdownTimeAmount_9() const { return ___countdownTimeAmount_9; }
	inline float* get_address_of_countdownTimeAmount_9() { return &___countdownTimeAmount_9; }
	inline void set_countdownTimeAmount_9(float value)
	{
		___countdownTimeAmount_9 = value;
	}

	inline static int32_t get_offset_of_onTimerFinished_10() { return static_cast<int32_t>(offsetof(CountdownTimer_t4123051008, ___onTimerFinished_10)); }
	inline OnTimerFinished_t1234642247 * get_onTimerFinished_10() const { return ___onTimerFinished_10; }
	inline OnTimerFinished_t1234642247 ** get_address_of_onTimerFinished_10() { return &___onTimerFinished_10; }
	inline void set_onTimerFinished_10(OnTimerFinished_t1234642247 * value)
	{
		___onTimerFinished_10 = value;
		Il2CppCodeGenWriteBarrier(&___onTimerFinished_10, value);
	}

	inline static int32_t get_offset_of_numberOn_11() { return static_cast<int32_t>(offsetof(CountdownTimer_t4123051008, ___numberOn_11)); }
	inline int32_t get_numberOn_11() const { return ___numberOn_11; }
	inline int32_t* get_address_of_numberOn_11() { return &___numberOn_11; }
	inline void set_numberOn_11(int32_t value)
	{
		___numberOn_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
