﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModifyTooltipStyleExample
struct ModifyTooltipStyleExample_t824275140;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"

// System.Void ModifyTooltipStyleExample::.ctor()
extern "C"  void ModifyTooltipStyleExample__ctor_m3625104877 (ModifyTooltipStyleExample_t824275140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModifyTooltipStyleExample::Start()
extern "C"  void ModifyTooltipStyleExample_Start_m1127262005 (ModifyTooltipStyleExample_t824275140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModifyTooltipStyleExample::OnPrepareTooltipStyle(UnityEngine.GUIStyle&)
extern "C"  void ModifyTooltipStyleExample_OnPrepareTooltipStyle_m2447560646 (ModifyTooltipStyleExample_t824275140 * __this, GUIStyle_t1799908754 ** ___style0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
