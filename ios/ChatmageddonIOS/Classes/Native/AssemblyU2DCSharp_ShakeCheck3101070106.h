﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShakeCheck
struct  ShakeCheck_t3101070106  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ShakeCheck::accelerometerUpdateInterval
	float ___accelerometerUpdateInterval_2;
	// System.Single ShakeCheck::lowPassKernalWidthInSeconds
	float ___lowPassKernalWidthInSeconds_3;
	// System.Single ShakeCheck::shakeDetectionThreshold
	float ___shakeDetectionThreshold_4;
	// System.Single ShakeCheck::lowPassFilterFactor
	float ___lowPassFilterFactor_5;
	// UnityEngine.Vector3 ShakeCheck::lowPassValue
	Vector3_t2243707580  ___lowPassValue_6;
	// UnityEngine.Vector3 ShakeCheck::acceleration
	Vector3_t2243707580  ___acceleration_7;
	// UnityEngine.Vector3 ShakeCheck::deltaAcceleration
	Vector3_t2243707580  ___deltaAcceleration_8;
	// System.Int32 ShakeCheck::shakeCount
	int32_t ___shakeCount_9;
	// System.Boolean ShakeCheck::shaking
	bool ___shaking_10;

public:
	inline static int32_t get_offset_of_accelerometerUpdateInterval_2() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___accelerometerUpdateInterval_2)); }
	inline float get_accelerometerUpdateInterval_2() const { return ___accelerometerUpdateInterval_2; }
	inline float* get_address_of_accelerometerUpdateInterval_2() { return &___accelerometerUpdateInterval_2; }
	inline void set_accelerometerUpdateInterval_2(float value)
	{
		___accelerometerUpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_lowPassKernalWidthInSeconds_3() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___lowPassKernalWidthInSeconds_3)); }
	inline float get_lowPassKernalWidthInSeconds_3() const { return ___lowPassKernalWidthInSeconds_3; }
	inline float* get_address_of_lowPassKernalWidthInSeconds_3() { return &___lowPassKernalWidthInSeconds_3; }
	inline void set_lowPassKernalWidthInSeconds_3(float value)
	{
		___lowPassKernalWidthInSeconds_3 = value;
	}

	inline static int32_t get_offset_of_shakeDetectionThreshold_4() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___shakeDetectionThreshold_4)); }
	inline float get_shakeDetectionThreshold_4() const { return ___shakeDetectionThreshold_4; }
	inline float* get_address_of_shakeDetectionThreshold_4() { return &___shakeDetectionThreshold_4; }
	inline void set_shakeDetectionThreshold_4(float value)
	{
		___shakeDetectionThreshold_4 = value;
	}

	inline static int32_t get_offset_of_lowPassFilterFactor_5() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___lowPassFilterFactor_5)); }
	inline float get_lowPassFilterFactor_5() const { return ___lowPassFilterFactor_5; }
	inline float* get_address_of_lowPassFilterFactor_5() { return &___lowPassFilterFactor_5; }
	inline void set_lowPassFilterFactor_5(float value)
	{
		___lowPassFilterFactor_5 = value;
	}

	inline static int32_t get_offset_of_lowPassValue_6() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___lowPassValue_6)); }
	inline Vector3_t2243707580  get_lowPassValue_6() const { return ___lowPassValue_6; }
	inline Vector3_t2243707580 * get_address_of_lowPassValue_6() { return &___lowPassValue_6; }
	inline void set_lowPassValue_6(Vector3_t2243707580  value)
	{
		___lowPassValue_6 = value;
	}

	inline static int32_t get_offset_of_acceleration_7() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___acceleration_7)); }
	inline Vector3_t2243707580  get_acceleration_7() const { return ___acceleration_7; }
	inline Vector3_t2243707580 * get_address_of_acceleration_7() { return &___acceleration_7; }
	inline void set_acceleration_7(Vector3_t2243707580  value)
	{
		___acceleration_7 = value;
	}

	inline static int32_t get_offset_of_deltaAcceleration_8() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___deltaAcceleration_8)); }
	inline Vector3_t2243707580  get_deltaAcceleration_8() const { return ___deltaAcceleration_8; }
	inline Vector3_t2243707580 * get_address_of_deltaAcceleration_8() { return &___deltaAcceleration_8; }
	inline void set_deltaAcceleration_8(Vector3_t2243707580  value)
	{
		___deltaAcceleration_8 = value;
	}

	inline static int32_t get_offset_of_shakeCount_9() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___shakeCount_9)); }
	inline int32_t get_shakeCount_9() const { return ___shakeCount_9; }
	inline int32_t* get_address_of_shakeCount_9() { return &___shakeCount_9; }
	inline void set_shakeCount_9(int32_t value)
	{
		___shakeCount_9 = value;
	}

	inline static int32_t get_offset_of_shaking_10() { return static_cast<int32_t>(offsetof(ShakeCheck_t3101070106, ___shaking_10)); }
	inline bool get_shaking_10() const { return ___shaking_10; }
	inline bool* get_address_of_shaking_10() { return &___shaking_10; }
	inline void set_shaking_10(bool value)
	{
		___shaking_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
