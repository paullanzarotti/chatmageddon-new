﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackNavigateForwardsButton
struct  AttackNavigateForwardsButton_t1401782687  : public SFXButton_t792651341
{
public:
	// AttackNavScreen AttackNavigateForwardsButton::navScreen
	int32_t ___navScreen_5;
	// UnityEngine.Color AttackNavigateForwardsButton::activeColor
	Color_t2020392075  ___activeColor_6;
	// UnityEngine.Color AttackNavigateForwardsButton::inActiveColor
	Color_t2020392075  ___inActiveColor_7;
	// UISprite AttackNavigateForwardsButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_8;

public:
	inline static int32_t get_offset_of_navScreen_5() { return static_cast<int32_t>(offsetof(AttackNavigateForwardsButton_t1401782687, ___navScreen_5)); }
	inline int32_t get_navScreen_5() const { return ___navScreen_5; }
	inline int32_t* get_address_of_navScreen_5() { return &___navScreen_5; }
	inline void set_navScreen_5(int32_t value)
	{
		___navScreen_5 = value;
	}

	inline static int32_t get_offset_of_activeColor_6() { return static_cast<int32_t>(offsetof(AttackNavigateForwardsButton_t1401782687, ___activeColor_6)); }
	inline Color_t2020392075  get_activeColor_6() const { return ___activeColor_6; }
	inline Color_t2020392075 * get_address_of_activeColor_6() { return &___activeColor_6; }
	inline void set_activeColor_6(Color_t2020392075  value)
	{
		___activeColor_6 = value;
	}

	inline static int32_t get_offset_of_inActiveColor_7() { return static_cast<int32_t>(offsetof(AttackNavigateForwardsButton_t1401782687, ___inActiveColor_7)); }
	inline Color_t2020392075  get_inActiveColor_7() const { return ___inActiveColor_7; }
	inline Color_t2020392075 * get_address_of_inActiveColor_7() { return &___inActiveColor_7; }
	inline void set_inActiveColor_7(Color_t2020392075  value)
	{
		___inActiveColor_7 = value;
	}

	inline static int32_t get_offset_of_buttonSprite_8() { return static_cast<int32_t>(offsetof(AttackNavigateForwardsButton_t1401782687, ___buttonSprite_8)); }
	inline UISprite_t603616735 * get_buttonSprite_8() const { return ___buttonSprite_8; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_8() { return &___buttonSprite_8; }
	inline void set_buttonSprite_8(UISprite_t603616735 * value)
	{
		___buttonSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
