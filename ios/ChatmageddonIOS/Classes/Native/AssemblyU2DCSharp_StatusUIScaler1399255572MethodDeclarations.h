﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusUIScaler
struct StatusUIScaler_t1399255572;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusUIScaler::.ctor()
extern "C"  void StatusUIScaler__ctor_m2720782529 (StatusUIScaler_t1399255572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusUIScaler::GlobalUIScale()
extern "C"  void StatusUIScaler_GlobalUIScale_m1241686294 (StatusUIScaler_t1399255572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
