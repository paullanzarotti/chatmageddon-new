﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_EnumerableFromConst2022230559MethodDeclarations.h"

// System.Void PhoneNumbers.EnumerableFromConstructor`1<PhoneNumbers.PhoneNumberMatch>::.ctor(System.Func`1<System.Collections.Generic.IEnumerator`1<T>>)
#define EnumerableFromConstructor_1__ctor_m3406635575(__this, ___fn0, method) ((  void (*) (EnumerableFromConstructor_1_t1496639844 *, Func_1_t1593775089 *, const MethodInfo*))EnumerableFromConstructor_1__ctor_m2151719767_gshared)(__this, ___fn0, method)
// System.Collections.Generic.IEnumerator`1<T> PhoneNumbers.EnumerableFromConstructor`1<PhoneNumbers.PhoneNumberMatch>::GetEnumerator()
#define EnumerableFromConstructor_1_GetEnumerator_m2779741062(__this, method) ((  Il2CppObject* (*) (EnumerableFromConstructor_1_t1496639844 *, const MethodInfo*))EnumerableFromConstructor_1_GetEnumerator_m1562695206_gshared)(__this, method)
// System.Collections.IEnumerator PhoneNumbers.EnumerableFromConstructor`1<PhoneNumbers.PhoneNumberMatch>::System.Collections.IEnumerable.GetEnumerator()
#define EnumerableFromConstructor_1_System_Collections_IEnumerable_GetEnumerator_m2122068965(__this, method) ((  Il2CppObject * (*) (EnumerableFromConstructor_1_t1496639844 *, const MethodInfo*))EnumerableFromConstructor_1_System_Collections_IEnumerable_GetEnumerator_m1507026405_gshared)(__this, method)
