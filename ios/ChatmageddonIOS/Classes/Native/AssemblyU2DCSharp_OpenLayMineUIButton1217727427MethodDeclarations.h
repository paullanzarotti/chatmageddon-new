﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenLayMineUIButton
struct OpenLayMineUIButton_t1217727427;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenLayMineUIButton::.ctor()
extern "C"  void OpenLayMineUIButton__ctor_m3607080914 (OpenLayMineUIButton_t1217727427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenLayMineUIButton::OnButtonClick()
extern "C"  void OpenLayMineUIButton_OnButtonClick_m1031564155 (OpenLayMineUIButton_t1217727427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OpenLayMineUIButton::WaitToOpenModal()
extern "C"  Il2CppObject * OpenLayMineUIButton_WaitToOpenModal_m797271519 (OpenLayMineUIButton_t1217727427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
