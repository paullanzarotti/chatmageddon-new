﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<GroupFriendUI>
struct List_1_t1003827979;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupCircleAnimation
struct  GroupCircleAnimation_t1271941897  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GroupCircleAnimation::maxCircleRadius
	float ___maxCircleRadius_2;
	// System.Single GroupCircleAnimation::degree
	float ___degree_3;
	// System.Collections.Generic.List`1<GroupFriendUI> GroupCircleAnimation::friendsGroupUI
	List_1_t1003827979 * ___friendsGroupUI_4;

public:
	inline static int32_t get_offset_of_maxCircleRadius_2() { return static_cast<int32_t>(offsetof(GroupCircleAnimation_t1271941897, ___maxCircleRadius_2)); }
	inline float get_maxCircleRadius_2() const { return ___maxCircleRadius_2; }
	inline float* get_address_of_maxCircleRadius_2() { return &___maxCircleRadius_2; }
	inline void set_maxCircleRadius_2(float value)
	{
		___maxCircleRadius_2 = value;
	}

	inline static int32_t get_offset_of_degree_3() { return static_cast<int32_t>(offsetof(GroupCircleAnimation_t1271941897, ___degree_3)); }
	inline float get_degree_3() const { return ___degree_3; }
	inline float* get_address_of_degree_3() { return &___degree_3; }
	inline void set_degree_3(float value)
	{
		___degree_3 = value;
	}

	inline static int32_t get_offset_of_friendsGroupUI_4() { return static_cast<int32_t>(offsetof(GroupCircleAnimation_t1271941897, ___friendsGroupUI_4)); }
	inline List_1_t1003827979 * get_friendsGroupUI_4() const { return ___friendsGroupUI_4; }
	inline List_1_t1003827979 ** get_address_of_friendsGroupUI_4() { return &___friendsGroupUI_4; }
	inline void set_friendsGroupUI_4(List_1_t1003827979 * value)
	{
		___friendsGroupUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___friendsGroupUI_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
