﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchFriendsNavScreen
struct AttackSearchFriendsNavScreen_t2732178946;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackSearchFriendsNavScreen::.ctor()
extern "C"  void AttackSearchFriendsNavScreen__ctor_m2772167941 (AttackSearchFriendsNavScreen_t2732178946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchFriendsNavScreen::Start()
extern "C"  void AttackSearchFriendsNavScreen_Start_m2745482413 (AttackSearchFriendsNavScreen_t2732178946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchFriendsNavScreen::UIClosing()
extern "C"  void AttackSearchFriendsNavScreen_UIClosing_m2406257996 (AttackSearchFriendsNavScreen_t2732178946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchFriendsNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackSearchFriendsNavScreen_ScreenLoad_m2266494367 (AttackSearchFriendsNavScreen_t2732178946 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchFriendsNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackSearchFriendsNavScreen_ScreenUnload_m2095127727 (AttackSearchFriendsNavScreen_t2732178946 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackSearchFriendsNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackSearchFriendsNavScreen_ValidateScreenNavigation_m3069468758 (AttackSearchFriendsNavScreen_t2732178946 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
