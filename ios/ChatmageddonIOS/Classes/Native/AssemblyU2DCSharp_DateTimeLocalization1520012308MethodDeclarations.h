﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "AssemblyU2DCSharp_DateTimeLocalization_Language3679215353.h"
#include "mscorlib_System_String2029220233.h"

// System.Globalization.CultureInfo DateTimeLocalization::get_Culture()
extern "C"  CultureInfo_t3500843524 * DateTimeLocalization_get_Culture_m1636825113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateTimeLocalization::set_Culture(System.Globalization.CultureInfo)
extern "C"  void DateTimeLocalization_set_Culture_m1076061608 (Il2CppObject * __this /* static, unused */, CultureInfo_t3500843524 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DateTimeLocalization/Language DateTimeLocalization::get_CurrentLanguage()
extern "C"  int32_t DateTimeLocalization_get_CurrentLanguage_m1694677503 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateTimeLocalization::SetLanguage(DateTimeLocalization/Language)
extern "C"  bool DateTimeLocalization_SetLanguage_m1224468830 (Il2CppObject * __this /* static, unused */, int32_t ___language0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] DateTimeLocalization::GetDayNames(System.Boolean)
extern "C"  StringU5BU5D_t1642385972* DateTimeLocalization_GetDayNames_m3651959371 (Il2CppObject * __this /* static, unused */, bool ___abbreviated0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] DateTimeLocalization::GetMonthNames(System.Boolean)
extern "C"  StringU5BU5D_t1642385972* DateTimeLocalization_GetMonthNames_m3324750303 (Il2CppObject * __this /* static, unused */, bool ___abbreviated0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DateTimeLocalization::TrySetLanguage(System.String)
extern "C"  bool DateTimeLocalization_TrySetLanguage_m3000114286 (Il2CppObject * __this /* static, unused */, String_t* ___languageString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DateTimeLocalization::.cctor()
extern "C"  void DateTimeLocalization__cctor_m598496134 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
