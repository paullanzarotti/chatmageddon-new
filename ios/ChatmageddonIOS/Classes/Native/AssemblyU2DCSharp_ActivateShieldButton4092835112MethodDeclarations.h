﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActivateShieldButton
struct ActivateShieldButton_t4092835112;

#include "codegen/il2cpp-codegen.h"

// System.Void ActivateShieldButton::.ctor()
extern "C"  void ActivateShieldButton__ctor_m2572084619 (ActivateShieldButton_t4092835112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActivateShieldButton::OnButtonClick()
extern "C"  void ActivateShieldButton_OnButtonClick_m1229161712 (ActivateShieldButton_t4092835112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
