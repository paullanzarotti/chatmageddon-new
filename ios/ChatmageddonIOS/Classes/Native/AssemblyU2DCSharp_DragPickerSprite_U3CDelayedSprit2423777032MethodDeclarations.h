﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragPickerSprite/<DelayedSpriteAppearance>c__Iterator0
struct U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DragPickerSprite/<DelayedSpriteAppearance>c__Iterator0::.ctor()
extern "C"  void U3CDelayedSpriteAppearanceU3Ec__Iterator0__ctor_m3852457419 (U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DragPickerSprite/<DelayedSpriteAppearance>c__Iterator0::MoveNext()
extern "C"  bool U3CDelayedSpriteAppearanceU3Ec__Iterator0_MoveNext_m4107353401 (U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DragPickerSprite/<DelayedSpriteAppearance>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayedSpriteAppearanceU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1532133285 (U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DragPickerSprite/<DelayedSpriteAppearance>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayedSpriteAppearanceU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1029544429 (U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPickerSprite/<DelayedSpriteAppearance>c__Iterator0::Dispose()
extern "C"  void U3CDelayedSpriteAppearanceU3Ec__Iterator0_Dispose_m2378460406 (U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragPickerSprite/<DelayedSpriteAppearance>c__Iterator0::Reset()
extern "C"  void U3CDelayedSpriteAppearanceU3Ec__Iterator0_Reset_m2171129244 (U3CDelayedSpriteAppearanceU3Ec__Iterator0_t2423777032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
