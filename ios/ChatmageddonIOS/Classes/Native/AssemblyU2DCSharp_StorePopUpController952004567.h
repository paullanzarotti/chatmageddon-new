﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TweenAlpha
struct TweenAlpha_t2421518635;
// NotEnoughBucksPU
struct NotEnoughBucksPU_t2637555884;
// PurchasePU
struct PurchasePU_t4126238880;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen702670287.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StorePopUpController
struct  StorePopUpController_t952004567  : public MonoSingleton_1_t702670287
{
public:
	// UnityEngine.GameObject StorePopUpController::puPanel
	GameObject_t1756533147 * ___puPanel_3;
	// TweenAlpha StorePopUpController::fadeTween
	TweenAlpha_t2421518635 * ___fadeTween_4;
	// NotEnoughBucksPU StorePopUpController::nebPU
	NotEnoughBucksPU_t2637555884 * ___nebPU_5;
	// PurchasePU StorePopUpController::purchasePU
	PurchasePU_t4126238880 * ___purchasePU_6;
	// System.Boolean StorePopUpController::puClosing
	bool ___puClosing_7;

public:
	inline static int32_t get_offset_of_puPanel_3() { return static_cast<int32_t>(offsetof(StorePopUpController_t952004567, ___puPanel_3)); }
	inline GameObject_t1756533147 * get_puPanel_3() const { return ___puPanel_3; }
	inline GameObject_t1756533147 ** get_address_of_puPanel_3() { return &___puPanel_3; }
	inline void set_puPanel_3(GameObject_t1756533147 * value)
	{
		___puPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___puPanel_3, value);
	}

	inline static int32_t get_offset_of_fadeTween_4() { return static_cast<int32_t>(offsetof(StorePopUpController_t952004567, ___fadeTween_4)); }
	inline TweenAlpha_t2421518635 * get_fadeTween_4() const { return ___fadeTween_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_fadeTween_4() { return &___fadeTween_4; }
	inline void set_fadeTween_4(TweenAlpha_t2421518635 * value)
	{
		___fadeTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___fadeTween_4, value);
	}

	inline static int32_t get_offset_of_nebPU_5() { return static_cast<int32_t>(offsetof(StorePopUpController_t952004567, ___nebPU_5)); }
	inline NotEnoughBucksPU_t2637555884 * get_nebPU_5() const { return ___nebPU_5; }
	inline NotEnoughBucksPU_t2637555884 ** get_address_of_nebPU_5() { return &___nebPU_5; }
	inline void set_nebPU_5(NotEnoughBucksPU_t2637555884 * value)
	{
		___nebPU_5 = value;
		Il2CppCodeGenWriteBarrier(&___nebPU_5, value);
	}

	inline static int32_t get_offset_of_purchasePU_6() { return static_cast<int32_t>(offsetof(StorePopUpController_t952004567, ___purchasePU_6)); }
	inline PurchasePU_t4126238880 * get_purchasePU_6() const { return ___purchasePU_6; }
	inline PurchasePU_t4126238880 ** get_address_of_purchasePU_6() { return &___purchasePU_6; }
	inline void set_purchasePU_6(PurchasePU_t4126238880 * value)
	{
		___purchasePU_6 = value;
		Il2CppCodeGenWriteBarrier(&___purchasePU_6, value);
	}

	inline static int32_t get_offset_of_puClosing_7() { return static_cast<int32_t>(offsetof(StorePopUpController_t952004567, ___puClosing_7)); }
	inline bool get_puClosing_7() const { return ___puClosing_7; }
	inline bool* get_address_of_puClosing_7() { return &___puClosing_7; }
	inline void set_puClosing_7(bool value)
	{
		___puClosing_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
