﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeLeftUIManager
struct HomeLeftUIManager_t3932466733;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeLeftUIManager::.ctor()
extern "C"  void HomeLeftUIManager__ctor_m2963785854 (HomeLeftUIManager_t3932466733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeLeftUIManager::Awake()
extern "C"  void HomeLeftUIManager_Awake_m1440360845 (HomeLeftUIManager_t3932466733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeLeftUIManager::Start()
extern "C"  void HomeLeftUIManager_Start_m3406390430 (HomeLeftUIManager_t3932466733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeLeftUIManager::OnTweenFinished()
extern "C"  void HomeLeftUIManager_OnTweenFinished_m3576094884 (HomeLeftUIManager_t3932466733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeLeftUIManager::SetUIActive(System.Boolean)
extern "C"  void HomeLeftUIManager_SetUIActive_m215956821 (HomeLeftUIManager_t3932466733 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeLeftUIManager::PlayScaleTween(System.Boolean,System.Single)
extern "C"  void HomeLeftUIManager_PlayScaleTween_m3354115987 (HomeLeftUIManager_t3932466733 * __this, bool ___forward0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
