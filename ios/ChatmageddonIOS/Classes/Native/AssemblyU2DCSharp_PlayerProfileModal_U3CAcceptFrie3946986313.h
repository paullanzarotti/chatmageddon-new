﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;
// PlayerProfileModal
struct PlayerProfileModal_t3260625137;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerProfileModal/<AcceptFriendRequest>c__AnonStorey1
struct  U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313  : public Il2CppObject
{
public:
	// Friend PlayerProfileModal/<AcceptFriendRequest>c__AnonStorey1::tempFriend
	Friend_t3555014108 * ___tempFriend_0;
	// System.Boolean PlayerProfileModal/<AcceptFriendRequest>c__AnonStorey1::innerFriend
	bool ___innerFriend_1;
	// PlayerProfileModal PlayerProfileModal/<AcceptFriendRequest>c__AnonStorey1::$this
	PlayerProfileModal_t3260625137 * ___U24this_2;

public:
	inline static int32_t get_offset_of_tempFriend_0() { return static_cast<int32_t>(offsetof(U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313, ___tempFriend_0)); }
	inline Friend_t3555014108 * get_tempFriend_0() const { return ___tempFriend_0; }
	inline Friend_t3555014108 ** get_address_of_tempFriend_0() { return &___tempFriend_0; }
	inline void set_tempFriend_0(Friend_t3555014108 * value)
	{
		___tempFriend_0 = value;
		Il2CppCodeGenWriteBarrier(&___tempFriend_0, value);
	}

	inline static int32_t get_offset_of_innerFriend_1() { return static_cast<int32_t>(offsetof(U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313, ___innerFriend_1)); }
	inline bool get_innerFriend_1() const { return ___innerFriend_1; }
	inline bool* get_address_of_innerFriend_1() { return &___innerFriend_1; }
	inline void set_innerFriend_1(bool value)
	{
		___innerFriend_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CAcceptFriendRequestU3Ec__AnonStorey1_t3946986313, ___U24this_2)); }
	inline PlayerProfileModal_t3260625137 * get_U24this_2() const { return ___U24this_2; }
	inline PlayerProfileModal_t3260625137 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PlayerProfileModal_t3260625137 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
