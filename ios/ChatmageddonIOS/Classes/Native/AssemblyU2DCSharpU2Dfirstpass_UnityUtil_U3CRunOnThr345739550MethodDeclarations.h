﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityUtil/<RunOnThreadPool>c__AnonStorey1
struct U3CRunOnThreadPoolU3Ec__AnonStorey1_t345739550;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityUtil/<RunOnThreadPool>c__AnonStorey1::.ctor()
extern "C"  void U3CRunOnThreadPoolU3Ec__AnonStorey1__ctor_m3108985215 (U3CRunOnThreadPoolU3Ec__AnonStorey1_t345739550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil/<RunOnThreadPool>c__AnonStorey1::<>m__0(System.Object)
extern "C"  void U3CRunOnThreadPoolU3Ec__AnonStorey1_U3CU3Em__0_m1262046482 (U3CRunOnThreadPoolU3Ec__AnonStorey1_t345739550 * __this, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
