﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeMemberProperty
struct CodeMemberProperty_t387075564;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeMemberProperty::.ctor()
extern "C"  void CodeMemberProperty__ctor_m3699706772 (CodeMemberProperty_t387075564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
