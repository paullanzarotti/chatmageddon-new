﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayCallbackMonoBehaviour
struct GooglePlayCallbackMonoBehaviour_t3452313744;
// Unibill.Impl.GooglePlayBillingService
struct GooglePlayBillingService_t2494001613;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_GoogleP2494001613.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GooglePlayCallbackMonoBehaviour::.ctor()
extern "C"  void GooglePlayCallbackMonoBehaviour__ctor_m2171256395 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::Awake()
extern "C"  void GooglePlayCallbackMonoBehaviour_Awake_m2398290690 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::Initialise(Unibill.Impl.GooglePlayBillingService)
extern "C"  void GooglePlayCallbackMonoBehaviour_Initialise_m3710816650 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, GooglePlayBillingService_t2494001613 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onProductListReceived(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onProductListReceived_m952133056 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onBillingNotSupported()
extern "C"  void GooglePlayCallbackMonoBehaviour_onBillingNotSupported_m507849296 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onPurchaseSucceeded(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onPurchaseSucceeded_m865939308 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onPurchaseCancelled(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onPurchaseCancelled_m1326561770 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onPurchaseRefunded(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onPurchaseRefunded_m4097220110 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onPurchaseFailed(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onPurchaseFailed_m2392863052 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onTransactionsRestored(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onTransactionsRestored_m1547925703 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___successString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onInvalidPublicKey(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onInvalidPublicKey_m1254055439 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___publicKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayCallbackMonoBehaviour::onPollForConsumablesFinished(System.String)
extern "C"  void GooglePlayCallbackMonoBehaviour_onPollForConsumablesFinished_m1276332896 (GooglePlayCallbackMonoBehaviour_t3452313744 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
