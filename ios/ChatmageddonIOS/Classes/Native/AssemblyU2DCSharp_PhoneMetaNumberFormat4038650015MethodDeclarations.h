﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneMetaNumberFormat
struct PhoneMetaNumberFormat_t4038650015;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneMetaNumberFormat::.ctor()
extern "C"  void PhoneMetaNumberFormat__ctor_m2915743926 (PhoneMetaNumberFormat_t4038650015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneMetaNumberFormat::.ctor(System.String,System.String,System.Boolean,System.String)
extern "C"  void PhoneMetaNumberFormat__ctor_m3643563227 (PhoneMetaNumberFormat_t4038650015 * __this, String_t* ___pattern0, String_t* ___nationalPrefixFormattingRule1, bool ___nationalPrefixOptionalWhenFormatting2, String_t* ___carrierCodeFormattingRule3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
