﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnterPhoneNumberButton
struct EnterPhoneNumberButton_t3549119;

#include "codegen/il2cpp-codegen.h"

// System.Void EnterPhoneNumberButton::.ctor()
extern "C"  void EnterPhoneNumberButton__ctor_m1751150622 (EnterPhoneNumberButton_t3549119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnterPhoneNumberButton::OnButtonClick()
extern "C"  void EnterPhoneNumberButton_OnButtonClick_m1354183155 (EnterPhoneNumberButton_t3549119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
