﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_UISriteAnimation265093205.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteSheetNGUIAnimation
struct  SpriteSheetNGUIAnimation_t2524734547  : public UISriteAnimation_t265093205
{
public:
	// System.Int32 SpriteSheetNGUIAnimation::totalRows
	int32_t ___totalRows_13;
	// System.Int32 SpriteSheetNGUIAnimation::totalCollumns
	int32_t ___totalCollumns_14;
	// System.Int32 SpriteSheetNGUIAnimation::currentRow
	int32_t ___currentRow_15;
	// System.Int32 SpriteSheetNGUIAnimation::currentCollumn
	int32_t ___currentCollumn_16;
	// System.Single SpriteSheetNGUIAnimation::spriteWidth
	float ___spriteWidth_17;
	// System.Single SpriteSheetNGUIAnimation::spriteHeight
	float ___spriteHeight_18;

public:
	inline static int32_t get_offset_of_totalRows_13() { return static_cast<int32_t>(offsetof(SpriteSheetNGUIAnimation_t2524734547, ___totalRows_13)); }
	inline int32_t get_totalRows_13() const { return ___totalRows_13; }
	inline int32_t* get_address_of_totalRows_13() { return &___totalRows_13; }
	inline void set_totalRows_13(int32_t value)
	{
		___totalRows_13 = value;
	}

	inline static int32_t get_offset_of_totalCollumns_14() { return static_cast<int32_t>(offsetof(SpriteSheetNGUIAnimation_t2524734547, ___totalCollumns_14)); }
	inline int32_t get_totalCollumns_14() const { return ___totalCollumns_14; }
	inline int32_t* get_address_of_totalCollumns_14() { return &___totalCollumns_14; }
	inline void set_totalCollumns_14(int32_t value)
	{
		___totalCollumns_14 = value;
	}

	inline static int32_t get_offset_of_currentRow_15() { return static_cast<int32_t>(offsetof(SpriteSheetNGUIAnimation_t2524734547, ___currentRow_15)); }
	inline int32_t get_currentRow_15() const { return ___currentRow_15; }
	inline int32_t* get_address_of_currentRow_15() { return &___currentRow_15; }
	inline void set_currentRow_15(int32_t value)
	{
		___currentRow_15 = value;
	}

	inline static int32_t get_offset_of_currentCollumn_16() { return static_cast<int32_t>(offsetof(SpriteSheetNGUIAnimation_t2524734547, ___currentCollumn_16)); }
	inline int32_t get_currentCollumn_16() const { return ___currentCollumn_16; }
	inline int32_t* get_address_of_currentCollumn_16() { return &___currentCollumn_16; }
	inline void set_currentCollumn_16(int32_t value)
	{
		___currentCollumn_16 = value;
	}

	inline static int32_t get_offset_of_spriteWidth_17() { return static_cast<int32_t>(offsetof(SpriteSheetNGUIAnimation_t2524734547, ___spriteWidth_17)); }
	inline float get_spriteWidth_17() const { return ___spriteWidth_17; }
	inline float* get_address_of_spriteWidth_17() { return &___spriteWidth_17; }
	inline void set_spriteWidth_17(float value)
	{
		___spriteWidth_17 = value;
	}

	inline static int32_t get_offset_of_spriteHeight_18() { return static_cast<int32_t>(offsetof(SpriteSheetNGUIAnimation_t2524734547, ___spriteHeight_18)); }
	inline float get_spriteHeight_18() const { return ___spriteHeight_18; }
	inline float* get_address_of_spriteHeight_18() { return &___spriteHeight_18; }
	inline void set_spriteHeight_18(float value)
	{
		___spriteHeight_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
