﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Localization/LoadFunction
struct LoadFunction_t1013268478;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Localization/LoadFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void LoadFunction__ctor_m239452119 (LoadFunction_t1013268478 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Localization/LoadFunction::Invoke(System.String)
extern "C"  ByteU5BU5D_t3397334013* LoadFunction_Invoke_m3719575289 (LoadFunction_t1013268478 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Localization/LoadFunction::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoadFunction_BeginInvoke_m3062786760 (LoadFunction_t1013268478 * __this, String_t* ___path0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Localization/LoadFunction::EndInvoke(System.IAsyncResult)
extern "C"  ByteU5BU5D_t3397334013* LoadFunction_EndInvoke_m1599288477 (LoadFunction_t1013268478 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
