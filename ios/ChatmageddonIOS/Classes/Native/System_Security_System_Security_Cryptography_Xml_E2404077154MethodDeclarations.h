﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptedType
struct EncryptedType_t2404077154;
// System.Security.Cryptography.Xml.CipherData
struct CipherData_t2270233253;
// System.String
struct String_t;
// System.Security.Cryptography.Xml.EncryptionMethod
struct EncryptionMethod_t718466202;
// System.Security.Cryptography.Xml.EncryptionPropertyCollection
struct EncryptionPropertyCollection_t2509585554;
// System.Security.Cryptography.Xml.KeyInfo
struct KeyInfo_t862955101;

#include "codegen/il2cpp-codegen.h"
#include "System_Security_System_Security_Cryptography_Xml_C2270233253.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Security_System_Security_Cryptography_Xml_En718466202.h"
#include "System_Security_System_Security_Cryptography_Xml_Ke862955101.h"

// System.Void System.Security.Cryptography.Xml.EncryptedType::.ctor()
extern "C"  void EncryptedType__ctor_m2537611124 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.CipherData System.Security.Cryptography.Xml.EncryptedType::get_CipherData()
extern "C"  CipherData_t2270233253 * EncryptedType_get_CipherData_m3744675780 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedType::set_CipherData(System.Security.Cryptography.Xml.CipherData)
extern "C"  void EncryptedType_set_CipherData_m1249011041 (EncryptedType_t2404077154 * __this, CipherData_t2270233253 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedType::get_Encoding()
extern "C"  String_t* EncryptedType_get_Encoding_m937202935 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedType::set_Encoding(System.String)
extern "C"  void EncryptedType_set_Encoding_m1153089232 (EncryptedType_t2404077154 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.EncryptionMethod System.Security.Cryptography.Xml.EncryptedType::get_EncryptionMethod()
extern "C"  EncryptionMethod_t718466202 * EncryptedType_get_EncryptionMethod_m946385710 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedType::set_EncryptionMethod(System.Security.Cryptography.Xml.EncryptionMethod)
extern "C"  void EncryptedType_set_EncryptionMethod_m2344274017 (EncryptedType_t2404077154 * __this, EncryptionMethod_t718466202 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.EncryptionPropertyCollection System.Security.Cryptography.Xml.EncryptedType::get_EncryptionProperties()
extern "C"  EncryptionPropertyCollection_t2509585554 * EncryptedType_get_EncryptionProperties_m2874400550 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedType::get_Id()
extern "C"  String_t* EncryptedType_get_Id_m393019789 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedType::set_Id(System.String)
extern "C"  void EncryptedType_set_Id_m2561447608 (EncryptedType_t2404077154 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.KeyInfo System.Security.Cryptography.Xml.EncryptedType::get_KeyInfo()
extern "C"  KeyInfo_t862955101 * EncryptedType_get_KeyInfo_m1197821980 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedType::set_KeyInfo(System.Security.Cryptography.Xml.KeyInfo)
extern "C"  void EncryptedType_set_KeyInfo_m769535333 (EncryptedType_t2404077154 * __this, KeyInfo_t862955101 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedType::get_MimeType()
extern "C"  String_t* EncryptedType_get_MimeType_m2107544270 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedType::set_MimeType(System.String)
extern "C"  void EncryptedType_set_MimeType_m1576915895 (EncryptedType_t2404077154 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedType::get_Type()
extern "C"  String_t* EncryptedType_get_Type_m1653131498 (EncryptedType_t2404077154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedType::set_Type(System.String)
extern "C"  void EncryptedType_set_Type_m3835271107 (EncryptedType_t2404077154 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
