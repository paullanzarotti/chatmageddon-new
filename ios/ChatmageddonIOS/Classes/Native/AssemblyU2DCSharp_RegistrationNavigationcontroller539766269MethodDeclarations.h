﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RegistrationNavigationcontroller
struct RegistrationNavigationcontroller_t539766269;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void RegistrationNavigationcontroller::.ctor()
extern "C"  void RegistrationNavigationcontroller__ctor_m728447370 (RegistrationNavigationcontroller_t539766269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationNavigationcontroller::NavigateBackwards()
extern "C"  void RegistrationNavigationcontroller_NavigateBackwards_m3507050747 (RegistrationNavigationcontroller_t539766269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationNavigationcontroller::SetupStartScreen(RegNavScreen)
extern "C"  void RegistrationNavigationcontroller_SetupStartScreen_m750372002 (RegistrationNavigationcontroller_t539766269 * __this, int32_t ___startScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationNavigationcontroller::NavigateForwards(RegNavScreen)
extern "C"  void RegistrationNavigationcontroller_NavigateForwards_m2136050284 (RegistrationNavigationcontroller_t539766269 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RegistrationNavigationcontroller::ValidateNavigation(RegNavScreen,NavigationDirection)
extern "C"  bool RegistrationNavigationcontroller_ValidateNavigation_m1369087646 (RegistrationNavigationcontroller_t539766269 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationNavigationcontroller::LoadNavScreen(RegNavScreen,NavigationDirection,System.Boolean)
extern "C"  void RegistrationNavigationcontroller_LoadNavScreen_m1595858898 (RegistrationNavigationcontroller_t539766269 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationNavigationcontroller::UnloadNavScreen(RegNavScreen,NavigationDirection)
extern "C"  void RegistrationNavigationcontroller_UnloadNavScreen_m1060993546 (RegistrationNavigationcontroller_t539766269 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RegistrationNavigationcontroller::PopulateTitleBar(RegNavScreen)
extern "C"  void RegistrationNavigationcontroller_PopulateTitleBar_m1379871324 (RegistrationNavigationcontroller_t539766269 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
