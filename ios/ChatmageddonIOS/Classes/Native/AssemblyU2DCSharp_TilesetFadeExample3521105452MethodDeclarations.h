﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TilesetFadeExample
struct TilesetFadeExample_t3521105452;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;
// UnityEngine.Material
struct Material_t193706927;
// TilesetFadeExampleItem
struct TilesetFadeExampleItem_t1982028563;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DCSharp_TilesetFadeExampleItem1982028563.h"

// System.Void TilesetFadeExample::.ctor()
extern "C"  void TilesetFadeExample__ctor_m4250503707 (TilesetFadeExample_t3521105452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetFadeExample::Start()
extern "C"  void TilesetFadeExample_Start_m3422452695 (TilesetFadeExample_t3521105452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetFadeExample::OnChangeMaterialTexture(OnlineMapsTile,UnityEngine.Material)
extern "C"  void TilesetFadeExample_OnChangeMaterialTexture_m2901153156 (TilesetFadeExample_t3521105452 * __this, OnlineMapsTile_t21329940 * ___tile0, Material_t193706927 * ___material1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetFadeExample::Update()
extern "C"  void TilesetFadeExample_Update_m2488278346 (TilesetFadeExample_t3521105452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TilesetFadeExample::<Update>m__0(TilesetFadeExampleItem)
extern "C"  bool TilesetFadeExample_U3CUpdateU3Em__0_m1345818586 (Il2CppObject * __this /* static, unused */, TilesetFadeExampleItem_t1982028563 * ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TilesetFadeExample::<Update>m__1(TilesetFadeExampleItem)
extern "C"  bool TilesetFadeExample_U3CUpdateU3Em__1_m3048473561 (Il2CppObject * __this /* static, unused */, TilesetFadeExampleItem_t1982028563 * ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
