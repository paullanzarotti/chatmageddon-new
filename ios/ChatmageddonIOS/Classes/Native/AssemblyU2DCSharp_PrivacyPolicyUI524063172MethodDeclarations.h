﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrivacyPolicyUI
struct PrivacyPolicyUI_t524063172;

#include "codegen/il2cpp-codegen.h"

// System.Void PrivacyPolicyUI::.ctor()
extern "C"  void PrivacyPolicyUI__ctor_m483543313 (PrivacyPolicyUI_t524063172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrivacyPolicyUI::LoadUI()
extern "C"  void PrivacyPolicyUI_LoadUI_m3338964411 (PrivacyPolicyUI_t524063172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
