﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusNavigationController
struct StatusNavigationController_t3357829236;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void StatusNavigationController::.ctor()
extern "C"  void StatusNavigationController__ctor_m1568442017 (StatusNavigationController_t3357829236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusNavigationController::Start()
extern "C"  void StatusNavigationController_Start_m3092048961 (StatusNavigationController_t3357829236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusNavigationController::NavigateBackwards()
extern "C"  void StatusNavigationController_NavigateBackwards_m3229632180 (StatusNavigationController_t3357829236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusNavigationController::NavigateForwards(StatusNavScreen)
extern "C"  void StatusNavigationController_NavigateForwards_m1429615935 (StatusNavigationController_t3357829236 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusNavigationController::ValidateNavigation(StatusNavScreen,NavigationDirection)
extern "C"  bool StatusNavigationController_ValidateNavigation_m2935843683 (StatusNavigationController_t3357829236 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusNavigationController::LoadNavScreen(StatusNavScreen,NavigationDirection,System.Boolean)
extern "C"  void StatusNavigationController_LoadNavScreen_m620230903 (StatusNavigationController_t3357829236 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusNavigationController::UnloadNavScreen(StatusNavScreen,NavigationDirection)
extern "C"  void StatusNavigationController_UnloadNavScreen_m3589483683 (StatusNavigationController_t3357829236 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusNavigationController::PopulateTitleBar(StatusNavScreen)
extern "C"  void StatusNavigationController_PopulateTitleBar_m2839329983 (StatusNavigationController_t3357829236 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
