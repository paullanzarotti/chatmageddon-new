﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneLoader/<wait>c__Iterator0
struct U3CwaitU3Ec__Iterator0_t10913998;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneLoader/<wait>c__Iterator0::.ctor()
extern "C"  void U3CwaitU3Ec__Iterator0__ctor_m1271844391 (U3CwaitU3Ec__Iterator0_t10913998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneLoader/<wait>c__Iterator0::MoveNext()
extern "C"  bool U3CwaitU3Ec__Iterator0_MoveNext_m827594889 (U3CwaitU3Ec__Iterator0_t10913998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneLoader/<wait>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2417150141 (U3CwaitU3Ec__Iterator0_t10913998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneLoader/<wait>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3814311669 (U3CwaitU3Ec__Iterator0_t10913998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader/<wait>c__Iterator0::Dispose()
extern "C"  void U3CwaitU3Ec__Iterator0_Dispose_m2075184796 (U3CwaitU3Ec__Iterator0_t10913998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader/<wait>c__Iterator0::Reset()
extern "C"  void U3CwaitU3Ec__Iterator0_Reset_m4120671014 (U3CwaitU3Ec__Iterator0_t10913998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
