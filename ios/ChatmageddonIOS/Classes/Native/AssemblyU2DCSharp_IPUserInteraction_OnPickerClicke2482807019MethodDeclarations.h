﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPUserInteraction/OnPickerClicked
struct OnPickerClicked_t2482807019;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void IPUserInteraction/OnPickerClicked::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPickerClicked__ctor_m1227992452 (OnPickerClicked_t2482807019 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction/OnPickerClicked::Invoke()
extern "C"  void OnPickerClicked_Invoke_m3011789934 (OnPickerClicked_t2482807019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult IPUserInteraction/OnPickerClicked::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnPickerClicked_BeginInvoke_m47393765 (OnPickerClicked_t2482807019 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction/OnPickerClicked::EndInvoke(System.IAsyncResult)
extern "C"  void OnPickerClicked_EndInvoke_m1441784086 (OnPickerClicked_t2482807019 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
