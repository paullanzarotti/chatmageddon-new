﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineModal/<LoadDefaultModel>c__Iterator0
struct U3CLoadDefaultModelU3Ec__Iterator0_t685440682;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MineModal/<LoadDefaultModel>c__Iterator0::.ctor()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0__ctor_m3563680669 (U3CLoadDefaultModelU3Ec__Iterator0_t685440682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MineModal/<LoadDefaultModel>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadDefaultModelU3Ec__Iterator0_MoveNext_m468763347 (U3CLoadDefaultModelU3Ec__Iterator0_t685440682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MineModal/<LoadDefaultModel>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1338304435 (U3CLoadDefaultModelU3Ec__Iterator0_t685440682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MineModal/<LoadDefaultModel>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m510372859 (U3CLoadDefaultModelU3Ec__Iterator0_t685440682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal/<LoadDefaultModel>c__Iterator0::Dispose()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0_Dispose_m2812360660 (U3CLoadDefaultModelU3Ec__Iterator0_t685440682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal/<LoadDefaultModel>c__Iterator0::Reset()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0_Reset_m3376768030 (U3CLoadDefaultModelU3Ec__Iterator0_t685440682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
