﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3731121345.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"

// System.Void System.Array/InternalEnumerator`1<StatusNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m463462508_gshared (InternalEnumerator_1_t3731121345 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m463462508(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3731121345 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m463462508_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<StatusNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2916265356_gshared (InternalEnumerator_1_t3731121345 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2916265356(__this, method) ((  void (*) (InternalEnumerator_1_t3731121345 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2916265356_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<StatusNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1174497778_gshared (InternalEnumerator_1_t3731121345 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1174497778(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3731121345 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1174497778_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<StatusNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m339138135_gshared (InternalEnumerator_1_t3731121345 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m339138135(__this, method) ((  void (*) (InternalEnumerator_1_t3731121345 *, const MethodInfo*))InternalEnumerator_1_Dispose_m339138135_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<StatusNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2298743684_gshared (InternalEnumerator_1_t3731121345 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2298743684(__this, method) ((  bool (*) (InternalEnumerator_1_t3731121345 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2298743684_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<StatusNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3547635099_gshared (InternalEnumerator_1_t3731121345 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3547635099(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3731121345 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3547635099_gshared)(__this, method)
