﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExitConfirmPopUp
struct ExitConfirmPopUp_t674592142;
// ConfirmPopUpCancelButton
struct ConfirmPopUpCancelButton_t953518780;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExitStealthButton
struct  ExitStealthButton_t3936490483  : public SFXButton_t792651341
{
public:
	// ExitConfirmPopUp ExitStealthButton::popUp
	ExitConfirmPopUp_t674592142 * ___popUp_5;
	// ConfirmPopUpCancelButton ExitStealthButton::cancelButton
	ConfirmPopUpCancelButton_t953518780 * ___cancelButton_6;

public:
	inline static int32_t get_offset_of_popUp_5() { return static_cast<int32_t>(offsetof(ExitStealthButton_t3936490483, ___popUp_5)); }
	inline ExitConfirmPopUp_t674592142 * get_popUp_5() const { return ___popUp_5; }
	inline ExitConfirmPopUp_t674592142 ** get_address_of_popUp_5() { return &___popUp_5; }
	inline void set_popUp_5(ExitConfirmPopUp_t674592142 * value)
	{
		___popUp_5 = value;
		Il2CppCodeGenWriteBarrier(&___popUp_5, value);
	}

	inline static int32_t get_offset_of_cancelButton_6() { return static_cast<int32_t>(offsetof(ExitStealthButton_t3936490483, ___cancelButton_6)); }
	inline ConfirmPopUpCancelButton_t953518780 * get_cancelButton_6() const { return ___cancelButton_6; }
	inline ConfirmPopUpCancelButton_t953518780 ** get_address_of_cancelButton_6() { return &___cancelButton_6; }
	inline void set_cancelButton_6(ConfirmPopUpCancelButton_t953518780 * value)
	{
		___cancelButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___cancelButton_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
