﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotatingTab
struct RotatingTab_t2077750253;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void RotatingTab::.ctor()
extern "C"  void RotatingTab__ctor_m1357326162 (RotatingTab_t2077750253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab::StartRotation(System.Single)
extern "C"  void RotatingTab_StartRotation_m2892748685 (RotatingTab_t2077750253 * __this, float ___startSpeed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator RotatingTab::RotateTab()
extern "C"  Il2CppObject * RotatingTab_RotateTab_m1516844328 (RotatingTab_t2077750253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab::SetSpeed(System.Single)
extern "C"  void RotatingTab_SetSpeed_m702581448 (RotatingTab_t2077750253 * __this, float ___speedToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab::PauseRotation()
extern "C"  void RotatingTab_PauseRotation_m1500121038 (RotatingTab_t2077750253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab::ReverseRotation()
extern "C"  void RotatingTab_ReverseRotation_m1091425806 (RotatingTab_t2077750253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab::SetTabColour(UnityEngine.Color)
extern "C"  void RotatingTab_SetTabColour_m3817710139 (RotatingTab_t2077750253 * __this, Color_t2020392075  ___tabColour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void RotatingTab_OnTriggerEnter_m3668955606 (RotatingTab_t2077750253 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTab::OnTriggerExit(UnityEngine.Collider)
extern "C"  void RotatingTab_OnTriggerExit_m520031516 (RotatingTab_t2077750253 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
