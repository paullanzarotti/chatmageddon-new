﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendMarker
struct FriendMarker_t3038476686;
// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// Friend
struct Friend_t3555014108;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3D576815539.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"

// System.Void FriendMarker::.ctor()
extern "C"  void FriendMarker__ctor_m3421123919 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::InitFiendMarker(OnlineMapsMarker3D,Friend)
extern "C"  void FriendMarker_InitFiendMarker_m3855282294 (FriendMarker_t3038476686 * __this, OnlineMapsMarker3D_t576815539 * ___mapMarker0, Friend_t3555014108 * ___initFriend1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::OnUpdate()
extern "C"  void FriendMarker_OnUpdate_m1711545767 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::UpdateMarker(System.Int32)
extern "C"  void FriendMarker_UpdateMarker_m2342468893 (FriendMarker_t3038476686 * __this, int32_t ___zoomLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::CheckBlipColour()
extern "C"  void FriendMarker_CheckBlipColour_m1624799306 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::ShowDisplayLabel(System.Boolean)
extern "C"  void FriendMarker_ShowDisplayLabel_m3390692199 (FriendMarker_t3038476686 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::UpdateDisplayLabelPosition()
extern "C"  void FriendMarker_UpdateDisplayLabelPosition_m563492023 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::OnAlphaTweenComplete()
extern "C"  void FriendMarker_OnAlphaTweenComplete_m2526595304 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::TriggerEntered()
extern "C"  void FriendMarker_TriggerEntered_m2698325418 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::TriggerExited()
extern "C"  void FriendMarker_TriggerExited_m127597464 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendMarker::OnClick()
extern "C"  void FriendMarker_OnClick_m3367206542 (FriendMarker_t3038476686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
