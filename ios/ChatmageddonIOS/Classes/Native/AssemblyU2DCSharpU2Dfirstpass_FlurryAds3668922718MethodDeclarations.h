﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryAds
struct FlurryAds_t3668922718;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_FlurryAdSize417027784.h"

// System.Void FlurryAds::.ctor()
extern "C"  void FlurryAds__ctor_m1361161385 (FlurryAds_t3668922718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::_flurryAdsInitialize(System.Boolean)
extern "C"  void FlurryAds__flurryAdsInitialize_m4080015661 (Il2CppObject * __this /* static, unused */, bool ___enableTestAds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::enableAds(System.Boolean)
extern "C"  void FlurryAds_enableAds_m292885181 (Il2CppObject * __this /* static, unused */, bool ___enableTestAds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::_flurryAdsSetUserCookies(System.String)
extern "C"  void FlurryAds__flurryAdsSetUserCookies_m3274985442 (Il2CppObject * __this /* static, unused */, String_t* ___cookies0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::adsSetUserCookies(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FlurryAds_adsSetUserCookies_m1176195214 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3943999495 * ___cookies0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::_flurryAdsClearUserCookies()
extern "C"  void FlurryAds__flurryAdsClearUserCookies_m4145701489 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::adsClearUserCookies()
extern "C"  void FlurryAds_adsClearUserCookies_m854009624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::_flurryAdsSetKeywords(System.String)
extern "C"  void FlurryAds__flurryAdsSetKeywords_m3187071758 (Il2CppObject * __this /* static, unused */, String_t* ___keywords0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::adsSetKeywords(System.String)
extern "C"  void FlurryAds_adsSetKeywords_m137911969 (Il2CppObject * __this /* static, unused */, String_t* ___keywords0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::_flurryAdsClearKeywords()
extern "C"  void FlurryAds__flurryAdsClearKeywords_m2592542305 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::adsClearKeywords()
extern "C"  void FlurryAds_adsClearKeywords_m3615094256 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlurryAds::_flurryAdsFetchAdForSpace(System.String,System.Int32)
extern "C"  bool FlurryAds__flurryAdsFetchAdForSpace_m1326405031 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::fetchAdForSpace(System.String,FlurryAdSize)
extern "C"  void FlurryAds_fetchAdForSpace_m1092339707 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlurryAds::_flurryAdsIsAdAvailableForSpace(System.String,System.Int32)
extern "C"  bool FlurryAds__flurryAdsIsAdAvailableForSpace_m597244196 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlurryAds::isAdAvailableForSpace(System.String,FlurryAdSize)
extern "C"  bool FlurryAds_isAdAvailableForSpace_m52378926 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlurryAds::_flurryAdsFetchAndDisplayAdForSpace(System.String,System.Int32)
extern "C"  bool FlurryAds__flurryAdsFetchAndDisplayAdForSpace_m1815008476 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::fetchAndDisplayAdForSpace(System.String,FlurryAdSize)
extern "C"  void FlurryAds_fetchAndDisplayAdForSpace_m3327120844 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlurryAds::_flurryAdsDisplayAdForSpace(System.String,System.Int32)
extern "C"  bool FlurryAds__flurryAdsDisplayAdForSpace_m3464951125 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::displayAdForSpace(System.String,FlurryAdSize)
extern "C"  void FlurryAds_displayAdForSpace_m2169890885 (Il2CppObject * __this /* static, unused */, String_t* ___space0, int32_t ___adSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::_flurryAdsRemoveAdFromSpace(System.String)
extern "C"  void FlurryAds__flurryAdsRemoveAdFromSpace_m2999167593 (Il2CppObject * __this /* static, unused */, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAds::removeAdFromSpace(System.String)
extern "C"  void FlurryAds_removeAdFromSpace_m4131718784 (Il2CppObject * __this /* static, unused */, String_t* ___space0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
