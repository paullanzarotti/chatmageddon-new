﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberOfflineGeocoder/<LoadMappingFileProvider>c__AnonStorey0
struct U3CLoadMappingFileProviderU3Ec__AnonStorey0_t2973898703;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.PhoneNumberOfflineGeocoder/<LoadMappingFileProvider>c__AnonStorey0::.ctor()
extern "C"  void U3CLoadMappingFileProviderU3Ec__AnonStorey0__ctor_m4096577602 (U3CLoadMappingFileProviderU3Ec__AnonStorey0_t2973898703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberOfflineGeocoder/<LoadMappingFileProvider>c__AnonStorey0::<>m__0(System.String)
extern "C"  bool U3CLoadMappingFileProviderU3Ec__AnonStorey0_U3CU3Em__0_m1929767315 (U3CLoadMappingFileProviderU3Ec__AnonStorey0_t2973898703 * __this, String_t* ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
