﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissilePointsPopulator
struct  MissilePointsPopulator_t2262625279  : public MonoBehaviour_t1158329972
{
public:
	// UILabel MissilePointsPopulator::pointsStateLabel
	UILabel_t1795115428 * ___pointsStateLabel_2;
	// UILabel MissilePointsPopulator::pointsLabel
	UILabel_t1795115428 * ___pointsLabel_3;
	// UILabel MissilePointsPopulator::ptsLabel
	UILabel_t1795115428 * ___ptsLabel_4;
	// System.Boolean MissilePointsPopulator::stateColourUpdate
	bool ___stateColourUpdate_5;
	// System.Boolean MissilePointsPopulator::pointsColourUpdate
	bool ___pointsColourUpdate_6;
	// System.Boolean MissilePointsPopulator::ptsColourUpdate
	bool ___ptsColourUpdate_7;
	// System.Single MissilePointsPopulator::labelPadding
	float ___labelPadding_8;

public:
	inline static int32_t get_offset_of_pointsStateLabel_2() { return static_cast<int32_t>(offsetof(MissilePointsPopulator_t2262625279, ___pointsStateLabel_2)); }
	inline UILabel_t1795115428 * get_pointsStateLabel_2() const { return ___pointsStateLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_pointsStateLabel_2() { return &___pointsStateLabel_2; }
	inline void set_pointsStateLabel_2(UILabel_t1795115428 * value)
	{
		___pointsStateLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___pointsStateLabel_2, value);
	}

	inline static int32_t get_offset_of_pointsLabel_3() { return static_cast<int32_t>(offsetof(MissilePointsPopulator_t2262625279, ___pointsLabel_3)); }
	inline UILabel_t1795115428 * get_pointsLabel_3() const { return ___pointsLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_pointsLabel_3() { return &___pointsLabel_3; }
	inline void set_pointsLabel_3(UILabel_t1795115428 * value)
	{
		___pointsLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___pointsLabel_3, value);
	}

	inline static int32_t get_offset_of_ptsLabel_4() { return static_cast<int32_t>(offsetof(MissilePointsPopulator_t2262625279, ___ptsLabel_4)); }
	inline UILabel_t1795115428 * get_ptsLabel_4() const { return ___ptsLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_ptsLabel_4() { return &___ptsLabel_4; }
	inline void set_ptsLabel_4(UILabel_t1795115428 * value)
	{
		___ptsLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___ptsLabel_4, value);
	}

	inline static int32_t get_offset_of_stateColourUpdate_5() { return static_cast<int32_t>(offsetof(MissilePointsPopulator_t2262625279, ___stateColourUpdate_5)); }
	inline bool get_stateColourUpdate_5() const { return ___stateColourUpdate_5; }
	inline bool* get_address_of_stateColourUpdate_5() { return &___stateColourUpdate_5; }
	inline void set_stateColourUpdate_5(bool value)
	{
		___stateColourUpdate_5 = value;
	}

	inline static int32_t get_offset_of_pointsColourUpdate_6() { return static_cast<int32_t>(offsetof(MissilePointsPopulator_t2262625279, ___pointsColourUpdate_6)); }
	inline bool get_pointsColourUpdate_6() const { return ___pointsColourUpdate_6; }
	inline bool* get_address_of_pointsColourUpdate_6() { return &___pointsColourUpdate_6; }
	inline void set_pointsColourUpdate_6(bool value)
	{
		___pointsColourUpdate_6 = value;
	}

	inline static int32_t get_offset_of_ptsColourUpdate_7() { return static_cast<int32_t>(offsetof(MissilePointsPopulator_t2262625279, ___ptsColourUpdate_7)); }
	inline bool get_ptsColourUpdate_7() const { return ___ptsColourUpdate_7; }
	inline bool* get_address_of_ptsColourUpdate_7() { return &___ptsColourUpdate_7; }
	inline void set_ptsColourUpdate_7(bool value)
	{
		___ptsColourUpdate_7 = value;
	}

	inline static int32_t get_offset_of_labelPadding_8() { return static_cast<int32_t>(offsetof(MissilePointsPopulator_t2262625279, ___labelPadding_8)); }
	inline float get_labelPadding_8() const { return ___labelPadding_8; }
	inline float* get_address_of_labelPadding_8() { return &___labelPadding_8; }
	inline void set_labelPadding_8(float value)
	{
		___labelPadding_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
