﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ChatNavigateBackwardButton
struct ChatNavigateBackwardButton_t516150148;
// ChatNavigateForwardButton
struct ChatNavigateForwardButton_t4025092154;

#include "AssemblyU2DCSharp_NavigationController_2_gen3988064068.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatNavigationController
struct  ChatNavigationController_t880507216  : public NavigationController_2_t3988064068
{
public:
	// UILabel ChatNavigationController::titleLabel
	UILabel_t1795115428 * ___titleLabel_8;
	// UnityEngine.GameObject ChatNavigationController::threadsObject
	GameObject_t1756533147 * ___threadsObject_9;
	// UnityEngine.GameObject ChatNavigationController::messageObject
	GameObject_t1756533147 * ___messageObject_10;
	// UnityEngine.GameObject ChatNavigationController::contactsObject
	GameObject_t1756533147 * ___contactsObject_11;
	// ChatNavigateBackwardButton ChatNavigationController::mainNavigateBackwardButton
	ChatNavigateBackwardButton_t516150148 * ___mainNavigateBackwardButton_12;
	// ChatNavigateForwardButton ChatNavigationController::mainNavigateForwardButton
	ChatNavigateForwardButton_t4025092154 * ___mainNavigateForwardButton_13;
	// UnityEngine.Vector3 ChatNavigationController::activePos
	Vector3_t2243707580  ___activePos_14;
	// UnityEngine.Vector3 ChatNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_15;
	// UnityEngine.Vector3 ChatNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_16;

public:
	inline static int32_t get_offset_of_titleLabel_8() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___titleLabel_8)); }
	inline UILabel_t1795115428 * get_titleLabel_8() const { return ___titleLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_8() { return &___titleLabel_8; }
	inline void set_titleLabel_8(UILabel_t1795115428 * value)
	{
		___titleLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_8, value);
	}

	inline static int32_t get_offset_of_threadsObject_9() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___threadsObject_9)); }
	inline GameObject_t1756533147 * get_threadsObject_9() const { return ___threadsObject_9; }
	inline GameObject_t1756533147 ** get_address_of_threadsObject_9() { return &___threadsObject_9; }
	inline void set_threadsObject_9(GameObject_t1756533147 * value)
	{
		___threadsObject_9 = value;
		Il2CppCodeGenWriteBarrier(&___threadsObject_9, value);
	}

	inline static int32_t get_offset_of_messageObject_10() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___messageObject_10)); }
	inline GameObject_t1756533147 * get_messageObject_10() const { return ___messageObject_10; }
	inline GameObject_t1756533147 ** get_address_of_messageObject_10() { return &___messageObject_10; }
	inline void set_messageObject_10(GameObject_t1756533147 * value)
	{
		___messageObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___messageObject_10, value);
	}

	inline static int32_t get_offset_of_contactsObject_11() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___contactsObject_11)); }
	inline GameObject_t1756533147 * get_contactsObject_11() const { return ___contactsObject_11; }
	inline GameObject_t1756533147 ** get_address_of_contactsObject_11() { return &___contactsObject_11; }
	inline void set_contactsObject_11(GameObject_t1756533147 * value)
	{
		___contactsObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___contactsObject_11, value);
	}

	inline static int32_t get_offset_of_mainNavigateBackwardButton_12() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___mainNavigateBackwardButton_12)); }
	inline ChatNavigateBackwardButton_t516150148 * get_mainNavigateBackwardButton_12() const { return ___mainNavigateBackwardButton_12; }
	inline ChatNavigateBackwardButton_t516150148 ** get_address_of_mainNavigateBackwardButton_12() { return &___mainNavigateBackwardButton_12; }
	inline void set_mainNavigateBackwardButton_12(ChatNavigateBackwardButton_t516150148 * value)
	{
		___mainNavigateBackwardButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateBackwardButton_12, value);
	}

	inline static int32_t get_offset_of_mainNavigateForwardButton_13() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___mainNavigateForwardButton_13)); }
	inline ChatNavigateForwardButton_t4025092154 * get_mainNavigateForwardButton_13() const { return ___mainNavigateForwardButton_13; }
	inline ChatNavigateForwardButton_t4025092154 ** get_address_of_mainNavigateForwardButton_13() { return &___mainNavigateForwardButton_13; }
	inline void set_mainNavigateForwardButton_13(ChatNavigateForwardButton_t4025092154 * value)
	{
		___mainNavigateForwardButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___mainNavigateForwardButton_13, value);
	}

	inline static int32_t get_offset_of_activePos_14() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___activePos_14)); }
	inline Vector3_t2243707580  get_activePos_14() const { return ___activePos_14; }
	inline Vector3_t2243707580 * get_address_of_activePos_14() { return &___activePos_14; }
	inline void set_activePos_14(Vector3_t2243707580  value)
	{
		___activePos_14 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_15() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___leftInactivePos_15)); }
	inline Vector3_t2243707580  get_leftInactivePos_15() const { return ___leftInactivePos_15; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_15() { return &___leftInactivePos_15; }
	inline void set_leftInactivePos_15(Vector3_t2243707580  value)
	{
		___leftInactivePos_15 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_16() { return static_cast<int32_t>(offsetof(ChatNavigationController_t880507216, ___rightInactivePos_16)); }
	inline Vector3_t2243707580  get_rightInactivePos_16() const { return ___rightInactivePos_16; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_16() { return &___rightInactivePos_16; }
	inline void set_rightInactivePos_16(Vector3_t2243707580  value)
	{
		___rightInactivePos_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
