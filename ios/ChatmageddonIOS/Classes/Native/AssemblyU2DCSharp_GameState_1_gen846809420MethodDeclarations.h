﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameState`1<System.Object>
struct GameState_1_t846809420;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GameState`1<System.Object>::.ctor()
extern "C"  void GameState_1__ctor_m299346719_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1__ctor_m299346719(__this, method) ((  void (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1__ctor_m299346719_gshared)(__this, method)
// System.Void GameState`1<System.Object>::Init()
extern "C"  void GameState_1_Init_m290996569_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1_Init_m290996569(__this, method) ((  void (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1_Init_m290996569_gshared)(__this, method)
// System.Void GameState`1<System.Object>::DontDestroyOnLoad()
extern "C"  void GameState_1_DontDestroyOnLoad_m4221039203_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1_DontDestroyOnLoad_m4221039203(__this, method) ((  void (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1_DontDestroyOnLoad_m4221039203_gshared)(__this, method)
// System.Void GameState`1<System.Object>::LoadScene(System.String,System.Boolean,System.Boolean)
extern "C"  void GameState_1_LoadScene_m1399394461_gshared (GameState_1_t846809420 * __this, String_t* ___sceneName0, bool ___aSync1, bool ___garbageCollect2, const MethodInfo* method);
#define GameState_1_LoadScene_m1399394461(__this, ___sceneName0, ___aSync1, ___garbageCollect2, method) ((  void (*) (GameState_1_t846809420 *, String_t*, bool, bool, const MethodInfo*))GameState_1_LoadScene_m1399394461_gshared)(__this, ___sceneName0, ___aSync1, ___garbageCollect2, method)
// System.Void GameState`1<System.Object>::StartActiveScene()
extern "C"  void GameState_1_StartActiveScene_m2386984001_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1_StartActiveScene_m2386984001(__this, method) ((  void (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1_StartActiveScene_m2386984001_gshared)(__this, method)
// System.Void GameState`1<System.Object>::LoadLevel(System.String)
extern "C"  void GameState_1_LoadLevel_m1307252913_gshared (GameState_1_t846809420 * __this, String_t* ___level0, const MethodInfo* method);
#define GameState_1_LoadLevel_m1307252913(__this, ___level0, method) ((  void (*) (GameState_1_t846809420 *, String_t*, const MethodInfo*))GameState_1_LoadLevel_m1307252913_gshared)(__this, ___level0, method)
// System.Void GameState`1<System.Object>::PreLevelLoad()
extern "C"  void GameState_1_PreLevelLoad_m3232301818_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1_PreLevelLoad_m3232301818(__this, method) ((  void (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1_PreLevelLoad_m3232301818_gshared)(__this, method)
// System.Void GameState`1<System.Object>::LoadLevelAsync(System.String,System.Boolean)
extern "C"  void GameState_1_LoadLevelAsync_m731125564_gshared (GameState_1_t846809420 * __this, String_t* ___level0, bool ___garbageCollect1, const MethodInfo* method);
#define GameState_1_LoadLevelAsync_m731125564(__this, ___level0, ___garbageCollect1, method) ((  void (*) (GameState_1_t846809420 *, String_t*, bool, const MethodInfo*))GameState_1_LoadLevelAsync_m731125564_gshared)(__this, ___level0, ___garbageCollect1, method)
// System.Void GameState`1<System.Object>::LoadState(System.String)
extern "C"  void GameState_1_LoadState_m3432771088_gshared (GameState_1_t846809420 * __this, String_t* ___nextState0, const MethodInfo* method);
#define GameState_1_LoadState_m3432771088(__this, ___nextState0, method) ((  void (*) (GameState_1_t846809420 *, String_t*, const MethodInfo*))GameState_1_LoadState_m3432771088_gshared)(__this, ___nextState0, method)
// System.Void GameState`1<System.Object>::SetPreviousState()
extern "C"  void GameState_1_SetPreviousState_m485008655_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1_SetPreviousState_m485008655(__this, method) ((  void (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1_SetPreviousState_m485008655_gshared)(__this, method)
// System.Void GameState`1<System.Object>::SetNextState(System.String)
extern "C"  void GameState_1_SetNextState_m1387511123_gshared (GameState_1_t846809420 * __this, String_t* ___nextState0, const MethodInfo* method);
#define GameState_1_SetNextState_m1387511123(__this, ___nextState0, method) ((  void (*) (GameState_1_t846809420 *, String_t*, const MethodInfo*))GameState_1_SetNextState_m1387511123_gshared)(__this, ___nextState0, method)
// System.String GameState`1<System.Object>::getActiveScene()
extern "C"  String_t* GameState_1_getActiveScene_m4069071568_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1_getActiveScene_m4069071568(__this, method) ((  String_t* (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1_getActiveScene_m4069071568_gshared)(__this, method)
// System.String GameState`1<System.Object>::getPreviousScene()
extern "C"  String_t* GameState_1_getPreviousScene_m1851301131_gshared (GameState_1_t846809420 * __this, const MethodInfo* method);
#define GameState_1_getPreviousScene_m1851301131(__this, method) ((  String_t* (*) (GameState_1_t846809420 *, const MethodInfo*))GameState_1_getPreviousScene_m1851301131_gshared)(__this, method)
