﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SwitchPanelButton319351498.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusTabButton
struct  StatusTabButton_t3949570411  : public SwitchPanelButton_t319351498
{
public:
	// UnityEngine.Color StatusTabButton::descActiveColour
	Color_t2020392075  ___descActiveColour_6;
	// UnityEngine.Color StatusTabButton::descInactiveColour
	Color_t2020392075  ___descInactiveColour_7;
	// UnityEngine.Color StatusTabButton::amountActiveColour
	Color_t2020392075  ___amountActiveColour_8;
	// UnityEngine.Color StatusTabButton::amountInactiveColour
	Color_t2020392075  ___amountInactiveColour_9;
	// UILabel StatusTabButton::descLabel
	UILabel_t1795115428 * ___descLabel_10;
	// UILabel StatusTabButton::amountLabel
	UILabel_t1795115428 * ___amountLabel_11;

public:
	inline static int32_t get_offset_of_descActiveColour_6() { return static_cast<int32_t>(offsetof(StatusTabButton_t3949570411, ___descActiveColour_6)); }
	inline Color_t2020392075  get_descActiveColour_6() const { return ___descActiveColour_6; }
	inline Color_t2020392075 * get_address_of_descActiveColour_6() { return &___descActiveColour_6; }
	inline void set_descActiveColour_6(Color_t2020392075  value)
	{
		___descActiveColour_6 = value;
	}

	inline static int32_t get_offset_of_descInactiveColour_7() { return static_cast<int32_t>(offsetof(StatusTabButton_t3949570411, ___descInactiveColour_7)); }
	inline Color_t2020392075  get_descInactiveColour_7() const { return ___descInactiveColour_7; }
	inline Color_t2020392075 * get_address_of_descInactiveColour_7() { return &___descInactiveColour_7; }
	inline void set_descInactiveColour_7(Color_t2020392075  value)
	{
		___descInactiveColour_7 = value;
	}

	inline static int32_t get_offset_of_amountActiveColour_8() { return static_cast<int32_t>(offsetof(StatusTabButton_t3949570411, ___amountActiveColour_8)); }
	inline Color_t2020392075  get_amountActiveColour_8() const { return ___amountActiveColour_8; }
	inline Color_t2020392075 * get_address_of_amountActiveColour_8() { return &___amountActiveColour_8; }
	inline void set_amountActiveColour_8(Color_t2020392075  value)
	{
		___amountActiveColour_8 = value;
	}

	inline static int32_t get_offset_of_amountInactiveColour_9() { return static_cast<int32_t>(offsetof(StatusTabButton_t3949570411, ___amountInactiveColour_9)); }
	inline Color_t2020392075  get_amountInactiveColour_9() const { return ___amountInactiveColour_9; }
	inline Color_t2020392075 * get_address_of_amountInactiveColour_9() { return &___amountInactiveColour_9; }
	inline void set_amountInactiveColour_9(Color_t2020392075  value)
	{
		___amountInactiveColour_9 = value;
	}

	inline static int32_t get_offset_of_descLabel_10() { return static_cast<int32_t>(offsetof(StatusTabButton_t3949570411, ___descLabel_10)); }
	inline UILabel_t1795115428 * get_descLabel_10() const { return ___descLabel_10; }
	inline UILabel_t1795115428 ** get_address_of_descLabel_10() { return &___descLabel_10; }
	inline void set_descLabel_10(UILabel_t1795115428 * value)
	{
		___descLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___descLabel_10, value);
	}

	inline static int32_t get_offset_of_amountLabel_11() { return static_cast<int32_t>(offsetof(StatusTabButton_t3949570411, ___amountLabel_11)); }
	inline UILabel_t1795115428 * get_amountLabel_11() const { return ___amountLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_amountLabel_11() { return &___amountLabel_11; }
	inline void set_amountLabel_11(UILabel_t1795115428 * value)
	{
		___amountLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___amountLabel_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
