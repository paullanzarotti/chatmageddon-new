﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Oculus_NightVision5
struct CameraFilterPack_Oculus_NightVision5_t1230285942;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Oculus_NightVision5::.ctor()
extern "C"  void CameraFilterPack_Oculus_NightVision5__ctor_m1681010729 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision5::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Oculus_NightVision5_get_material_m768885952 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::ChangeFilters()
extern "C"  void CameraFilterPack_Oculus_NightVision5_ChangeFilters_m788482426 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::Start()
extern "C"  void CameraFilterPack_Oculus_NightVision5_Start_m110436129 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Oculus_NightVision5_OnRenderImage_m1802545233 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::OnValidate()
extern "C"  void CameraFilterPack_Oculus_NightVision5_OnValidate_m2353777202 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::Update()
extern "C"  void CameraFilterPack_Oculus_NightVision5_Update_m1239603640 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::OnDisable()
extern "C"  void CameraFilterPack_Oculus_NightVision5_OnDisable_m3183592902 (CameraFilterPack_Oculus_NightVision5_t1230285942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
