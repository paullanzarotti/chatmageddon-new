﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Action_2_t3603663057;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.Facebook/<getSessionPermissionsOnServer>c__AnonStorey6
struct  U3CgetSessionPermissionsOnServerU3Ec__AnonStorey6_t272072221  : public Il2CppObject
{
public:
	// System.Action`2<System.String,System.Collections.Generic.List`1<System.String>> Prime31.Facebook/<getSessionPermissionsOnServer>c__AnonStorey6::completionHandler
	Action_2_t3603663057 * ___completionHandler_0;

public:
	inline static int32_t get_offset_of_completionHandler_0() { return static_cast<int32_t>(offsetof(U3CgetSessionPermissionsOnServerU3Ec__AnonStorey6_t272072221, ___completionHandler_0)); }
	inline Action_2_t3603663057 * get_completionHandler_0() const { return ___completionHandler_0; }
	inline Action_2_t3603663057 ** get_address_of_completionHandler_0() { return &___completionHandler_0; }
	inline void set_completionHandler_0(Action_2_t3603663057 * value)
	{
		___completionHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___completionHandler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
