﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginRegistrationSceneManager
struct LoginRegistrationSceneManager_t2327503985;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoginRegistrationSceneManager::.ctor()
extern "C"  void LoginRegistrationSceneManager__ctor_m2128117250 (LoginRegistrationSceneManager_t2327503985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationSceneManager::InitScene()
extern "C"  void LoginRegistrationSceneManager_InitScene_m2316783920 (LoginRegistrationSceneManager_t2327503985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationSceneManager::StartScene()
extern "C"  void LoginRegistrationSceneManager_StartScene_m912820800 (LoginRegistrationSceneManager_t2327503985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationSceneManager::CheckLogin()
extern "C"  void LoginRegistrationSceneManager_CheckLogin_m712428405 (LoginRegistrationSceneManager_t2327503985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationSceneManager::LoadLoginStartPage()
extern "C"  void LoginRegistrationSceneManager_LoadLoginStartPage_m1383206064 (LoginRegistrationSceneManager_t2327503985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationSceneManager::<CheckLogin>m__0(System.Boolean,System.String)
extern "C"  void LoginRegistrationSceneManager_U3CCheckLoginU3Em__0_m1083214463 (LoginRegistrationSceneManager_t2327503985 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginRegistrationSceneManager::<CheckLogin>m__1(System.Boolean,System.String)
extern "C"  void LoginRegistrationSceneManager_U3CCheckLoginU3Em__1_m2020607840 (LoginRegistrationSceneManager_t2327503985 * __this, bool ___loginSuccess0, String_t* ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
