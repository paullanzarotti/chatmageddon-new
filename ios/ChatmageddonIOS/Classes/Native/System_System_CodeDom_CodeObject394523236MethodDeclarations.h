﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeObject
struct CodeObject_t394523236;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeObject::.ctor()
extern "C"  void CodeObject__ctor_m3839014120 (CodeObject_t394523236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
