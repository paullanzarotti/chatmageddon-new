﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Unibill.Impl.BillingPlatform>
struct DefaultComparer_t2173989395;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Unibill.Impl.BillingPlatform>::.ctor()
extern "C"  void DefaultComparer__ctor_m432279357_gshared (DefaultComparer_t2173989395 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m432279357(__this, method) ((  void (*) (DefaultComparer_t2173989395 *, const MethodInfo*))DefaultComparer__ctor_m432279357_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Unibill.Impl.BillingPlatform>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2133827008_gshared (DefaultComparer_t2173989395 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2133827008(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2173989395 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2133827008_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Unibill.Impl.BillingPlatform>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m728872480_gshared (DefaultComparer_t2173989395 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m728872480(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2173989395 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m728872480_gshared)(__this, ___x0, ___y1, method)
