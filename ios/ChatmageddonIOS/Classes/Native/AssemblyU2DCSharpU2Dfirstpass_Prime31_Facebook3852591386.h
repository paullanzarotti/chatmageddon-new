﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Prime31.Facebook
struct Facebook_t3852591386;

#include "P31RestKit_Prime31_P31RestKit1799911290.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.Facebook
struct  Facebook_t3852591386  : public P31RestKit_t1799911290
{
public:
	// System.String Prime31.Facebook::accessToken
	String_t* ___accessToken_5;
	// System.String Prime31.Facebook::appAccessToken
	String_t* ___appAccessToken_6;
	// System.Boolean Prime31.Facebook::useSessionBabysitter
	bool ___useSessionBabysitter_7;

public:
	inline static int32_t get_offset_of_accessToken_5() { return static_cast<int32_t>(offsetof(Facebook_t3852591386, ___accessToken_5)); }
	inline String_t* get_accessToken_5() const { return ___accessToken_5; }
	inline String_t** get_address_of_accessToken_5() { return &___accessToken_5; }
	inline void set_accessToken_5(String_t* value)
	{
		___accessToken_5 = value;
		Il2CppCodeGenWriteBarrier(&___accessToken_5, value);
	}

	inline static int32_t get_offset_of_appAccessToken_6() { return static_cast<int32_t>(offsetof(Facebook_t3852591386, ___appAccessToken_6)); }
	inline String_t* get_appAccessToken_6() const { return ___appAccessToken_6; }
	inline String_t** get_address_of_appAccessToken_6() { return &___appAccessToken_6; }
	inline void set_appAccessToken_6(String_t* value)
	{
		___appAccessToken_6 = value;
		Il2CppCodeGenWriteBarrier(&___appAccessToken_6, value);
	}

	inline static int32_t get_offset_of_useSessionBabysitter_7() { return static_cast<int32_t>(offsetof(Facebook_t3852591386, ___useSessionBabysitter_7)); }
	inline bool get_useSessionBabysitter_7() const { return ___useSessionBabysitter_7; }
	inline bool* get_address_of_useSessionBabysitter_7() { return &___useSessionBabysitter_7; }
	inline void set_useSessionBabysitter_7(bool value)
	{
		___useSessionBabysitter_7 = value;
	}
};

struct Facebook_t3852591386_StaticFields
{
public:
	// Prime31.Facebook Prime31.Facebook::_instance
	Facebook_t3852591386 * ____instance_8;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(Facebook_t3852591386_StaticFields, ____instance_8)); }
	inline Facebook_t3852591386 * get__instance_8() const { return ____instance_8; }
	inline Facebook_t3852591386 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(Facebook_t3852591386 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier(&____instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
