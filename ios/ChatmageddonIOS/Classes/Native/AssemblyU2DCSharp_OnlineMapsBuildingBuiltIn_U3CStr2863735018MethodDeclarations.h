﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsBuildingBuiltIn/<StringToColor>c__AnonStorey2
struct U3CStringToColorU3Ec__AnonStorey2_t2863735018;

#include "codegen/il2cpp-codegen.h"

// System.Void OnlineMapsBuildingBuiltIn/<StringToColor>c__AnonStorey2::.ctor()
extern "C"  void U3CStringToColorU3Ec__AnonStorey2__ctor_m4208004203 (U3CStringToColorU3Ec__AnonStorey2_t2863735018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte OnlineMapsBuildingBuiltIn/<StringToColor>c__AnonStorey2::<>m__0(System.Int32)
extern "C"  uint8_t U3CStringToColorU3Ec__AnonStorey2_U3CU3Em__0_m842328515 (U3CStringToColorU3Ec__AnonStorey2_t2863735018 * __this, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
