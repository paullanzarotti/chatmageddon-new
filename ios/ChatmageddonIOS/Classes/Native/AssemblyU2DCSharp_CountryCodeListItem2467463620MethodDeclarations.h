﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountryCodeListItem
struct CountryCodeListItem_t2467463620;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void CountryCodeListItem::.ctor()
extern "C"  void CountryCodeListItem__ctor_m739924901 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::SetUpItem()
extern "C"  void CountryCodeListItem_SetUpItem_m1618249399 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::InitItem()
extern "C"  void CountryCodeListItem_InitItem_m2733637638 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::SetVisible()
extern "C"  void CountryCodeListItem_SetVisible_m3776636657 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::SetInvisible()
extern "C"  void CountryCodeListItem_SetInvisible_m938793512 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CountryCodeListItem::verifyVisibility()
extern "C"  bool CountryCodeListItem_verifyVisibility_m3870224368 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::CheckVisibilty()
extern "C"  void CountryCodeListItem_CheckVisibilty_m4254232604 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::FixedUpdate()
extern "C"  void CountryCodeListItem_FixedUpdate_m520319052 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::OnEnable()
extern "C"  void CountryCodeListItem_OnEnable_m2035767445 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::FindScrollView()
extern "C"  void CountryCodeListItem_FindScrollView_m2070339124 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::OnPress(System.Boolean)
extern "C"  void CountryCodeListItem_OnPress_m3155613186 (CountryCodeListItem_t2467463620 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void CountryCodeListItem_OnDrag_m3416755456 (CountryCodeListItem_t2467463620 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::OnDragEnd()
extern "C"  void CountryCodeListItem_OnDragEnd_m429878367 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::OnScroll(System.Single)
extern "C"  void CountryCodeListItem_OnScroll_m1571367484 (CountryCodeListItem_t2467463620 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListItem::OnClick()
extern "C"  void CountryCodeListItem_OnClick_m3973823556 (CountryCodeListItem_t2467463620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
