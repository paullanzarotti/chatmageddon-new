﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchedItem
struct LaunchedItem_t3670634427;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// LaunchedMine
struct LaunchedMine_t3677282773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "AssemblyU2DCSharp_LaunchedMine3677282773.h"

// System.Void LaunchedItem::.ctor(LaunchedMissile)
extern "C"  void LaunchedItem__ctor_m526981076 (LaunchedItem_t3670634427 * __this, LaunchedMissile_t1542515810 * ___missile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedItem::.ctor(LaunchedMine)
extern "C"  void LaunchedItem__ctor_m2612281629 (LaunchedItem_t3670634427 * __this, LaunchedMine_t3677282773 * ___mine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
