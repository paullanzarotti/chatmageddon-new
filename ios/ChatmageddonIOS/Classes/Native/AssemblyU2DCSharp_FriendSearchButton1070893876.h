﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FriendSearchInput
struct FriendSearchInput_t390073340;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendSearchButton
struct  FriendSearchButton_t1070893876  : public SFXButton_t792651341
{
public:
	// FriendSearchInput FriendSearchButton::searchInput
	FriendSearchInput_t390073340 * ___searchInput_5;

public:
	inline static int32_t get_offset_of_searchInput_5() { return static_cast<int32_t>(offsetof(FriendSearchButton_t1070893876, ___searchInput_5)); }
	inline FriendSearchInput_t390073340 * get_searchInput_5() const { return ___searchInput_5; }
	inline FriendSearchInput_t390073340 ** get_address_of_searchInput_5() { return &___searchInput_5; }
	inline void set_searchInput_5(FriendSearchInput_t390073340 * value)
	{
		___searchInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___searchInput_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
