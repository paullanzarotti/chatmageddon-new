﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<ChatThreadsListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m1241955036(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1079408212 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<ChatThreadsListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m3794995677(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t1079408212 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatThreadsListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m1845937419(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1079408212 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatThreadsListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m1434310826(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1079408212 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatThreadsListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m4181479345(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t1079408212 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<ChatThreadsListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m3291091200(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1079408212 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatThreadsListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m3139593211(__this, method) ((  void (*) (BaseInfiniteListItem_1_t1079408212 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
