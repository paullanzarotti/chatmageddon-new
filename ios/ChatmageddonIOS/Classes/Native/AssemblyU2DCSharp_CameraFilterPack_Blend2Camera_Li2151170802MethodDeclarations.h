﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_LinearDodge
struct CameraFilterPack_Blend2Camera_LinearDodge_t2151170802;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_LinearDodge::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_LinearDodge__ctor_m2989444121 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_LinearDodge::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_LinearDodge_get_material_m1851094582 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::Start()
extern "C"  void CameraFilterPack_Blend2Camera_LinearDodge_Start_m962335417 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_LinearDodge_OnRenderImage_m3738322033 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_LinearDodge_OnValidate_m4085349414 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::Update()
extern "C"  void CameraFilterPack_Blend2Camera_LinearDodge_Update_m3802877620 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_LinearDodge_OnEnable_m520178553 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_LinearDodge_OnDisable_m771625394 (CameraFilterPack_Blend2Camera_LinearDodge_t2151170802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
