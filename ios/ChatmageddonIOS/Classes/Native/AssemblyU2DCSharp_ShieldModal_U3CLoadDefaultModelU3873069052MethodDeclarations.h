﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldModal/<LoadDefaultModel>c__Iterator0
struct U3CLoadDefaultModelU3Ec__Iterator0_t3873069052;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldModal/<LoadDefaultModel>c__Iterator0::.ctor()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0__ctor_m3590305305 (U3CLoadDefaultModelU3Ec__Iterator0_t3873069052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShieldModal/<LoadDefaultModel>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadDefaultModelU3Ec__Iterator0_MoveNext_m2299197903 (U3CLoadDefaultModelU3Ec__Iterator0_t3873069052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShieldModal/<LoadDefaultModel>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2876548751 (U3CLoadDefaultModelU3Ec__Iterator0_t3873069052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShieldModal/<LoadDefaultModel>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadDefaultModelU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m572922519 (U3CLoadDefaultModelU3Ec__Iterator0_t3873069052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal/<LoadDefaultModel>c__Iterator0::Dispose()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0_Dispose_m4201574414 (U3CLoadDefaultModelU3Ec__Iterator0_t3873069052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldModal/<LoadDefaultModel>c__Iterator0::Reset()
extern "C"  void U3CLoadDefaultModelU3Ec__Iterator0_Reset_m2754303768 (U3CLoadDefaultModelU3Ec__Iterator0_t3873069052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
