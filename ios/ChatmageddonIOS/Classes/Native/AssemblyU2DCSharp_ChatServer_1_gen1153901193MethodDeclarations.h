﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatServer_1_gen3248875550MethodDeclarations.h"

// System.Void ChatServer`1<ChatmageddonServer>::.ctor()
#define ChatServer_1__ctor_m3904915419(__this, method) ((  void (*) (ChatServer_1_t1153901193 *, const MethodInfo*))ChatServer_1__ctor_m1982702137_gshared)(__this, method)
// System.Void ChatServer`1<ChatmageddonServer>::OpenNewThread(System.Collections.Generic.List`1<System.String>,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
#define ChatServer_1_OpenNewThread_m3745828022(__this, ___userIDList0, ___message1, ___callBack2, method) ((  void (*) (ChatServer_1_t1153901193 *, List_1_t1398341365 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_OpenNewThread_m907651558_gshared)(__this, ___userIDList0, ___message1, ___callBack2, method)
// System.Void ChatServer`1<ChatmageddonServer>::RetrieveOpenThreads(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
#define ChatServer_1_RetrieveOpenThreads_m2575281113(__this, ___callBack0, method) ((  void (*) (ChatServer_1_t1153901193 *, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_RetrieveOpenThreads_m637939567_gshared)(__this, ___callBack0, method)
// System.Void ChatServer`1<ChatmageddonServer>::RetrieveOpenThreadInfo(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
#define ChatServer_1_RetrieveOpenThreadInfo_m4084664588(__this, ___threadID0, ___callBack1, method) ((  void (*) (ChatServer_1_t1153901193 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_RetrieveOpenThreadInfo_m1820464480_gshared)(__this, ___threadID0, ___callBack1, method)
// System.Void ChatServer`1<ChatmageddonServer>::RetrieveThreadMessages(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>,System.String)
#define ChatServer_1_RetrieveThreadMessages_m110068996(__this, ___threadID0, ___callBack1, ___timestamp2, method) ((  void (*) (ChatServer_1_t1153901193 *, String_t*, Action_3_t3681841185 *, String_t*, const MethodInfo*))ChatServer_1_RetrieveThreadMessages_m256300356_gshared)(__this, ___threadID0, ___callBack1, ___timestamp2, method)
// System.Void ChatServer`1<ChatmageddonServer>::RetrieveThreadMessagesForUser(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>,System.String)
#define ChatServer_1_RetrieveThreadMessagesForUser_m3946070304(__this, ___userID0, ___callBack1, ___timestamp2, method) ((  void (*) (ChatServer_1_t1153901193 *, String_t*, Action_3_t3681841185 *, String_t*, const MethodInfo*))ChatServer_1_RetrieveThreadMessagesForUser_m2574775060_gshared)(__this, ___userID0, ___callBack1, ___timestamp2, method)
// System.Void ChatServer`1<ChatmageddonServer>::AddUserToThread(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
#define ChatServer_1_AddUserToThread_m2067594995(__this, ___threadID0, ___userID1, ___callBack2, method) ((  void (*) (ChatServer_1_t1153901193 *, String_t*, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_AddUserToThread_m1040780865_gshared)(__this, ___threadID0, ___userID1, ___callBack2, method)
// System.Void ChatServer`1<ChatmageddonServer>::PostMessageToThread(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
#define ChatServer_1_PostMessageToThread_m2323617732(__this, ___threadID0, ___message1, ___callBack2, method) ((  void (*) (ChatServer_1_t1153901193 *, String_t*, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_PostMessageToThread_m1683284752_gshared)(__this, ___threadID0, ___message1, ___callBack2, method)
// System.Void ChatServer`1<ChatmageddonServer>::UpdateThreadRead(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
#define ChatServer_1_UpdateThreadRead_m252353477(__this, ___threadID0, ___callBack1, method) ((  void (*) (ChatServer_1_t1153901193 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_UpdateThreadRead_m3951537979_gshared)(__this, ___threadID0, ___callBack1, method)
// System.Void ChatServer`1<ChatmageddonServer>::DeleteThread(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
#define ChatServer_1_DeleteThread_m3797762427(__this, ___threadID0, ___callBack1, method) ((  void (*) (ChatServer_1_t1153901193 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))ChatServer_1_DeleteThread_m602312621_gshared)(__this, ___threadID0, ___callBack1, method)
