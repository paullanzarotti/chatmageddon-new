﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationPermission
struct ConfigurationPermission_t1611094853;
// System.Security.IPermission
struct IPermission_t182075948;
// System.Security.SecurityElement
struct SecurityElement_t2325568386;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionSta3557289502.h"
#include "mscorlib_System_Security_SecurityElement2325568386.h"

// System.Void System.Configuration.ConfigurationPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C"  void ConfigurationPermission__ctor_m1952749087 (ConfigurationPermission_t1611094853 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Configuration.ConfigurationPermission::Copy()
extern "C"  Il2CppObject * ConfigurationPermission_Copy_m2991027605 (ConfigurationPermission_t1611094853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationPermission::FromXml(System.Security.SecurityElement)
extern "C"  void ConfigurationPermission_FromXml_m1615470072 (ConfigurationPermission_t1611094853 * __this, SecurityElement_t2325568386 * ___securityElement0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Configuration.ConfigurationPermission::Intersect(System.Security.IPermission)
extern "C"  Il2CppObject * ConfigurationPermission_Intersect_m1553166340 (ConfigurationPermission_t1611094853 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Configuration.ConfigurationPermission::Union(System.Security.IPermission)
extern "C"  Il2CppObject * ConfigurationPermission_Union_m2985016754 (ConfigurationPermission_t1611094853 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationPermission::IsSubsetOf(System.Security.IPermission)
extern "C"  bool ConfigurationPermission_IsSubsetOf_m1751013986 (ConfigurationPermission_t1611094853 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationPermission::IsUnrestricted()
extern "C"  bool ConfigurationPermission_IsUnrestricted_m2388054376 (ConfigurationPermission_t1611094853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Configuration.ConfigurationPermission::ToXml()
extern "C"  SecurityElement_t2325568386 * ConfigurationPermission_ToXml_m4077614882 (ConfigurationPermission_t1611094853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
