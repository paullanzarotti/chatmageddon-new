﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>
struct ReadOnlyCollection_1_t1784823988;
// System.Collections.Generic.IList`1<FriendHomeNavScreen>
struct IList_1_t2139978897;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FriendHomeNavScreen[]
struct FriendHomeNavScreenU5BU5D_t1773644873;
// System.Collections.Generic.IEnumerator`1<FriendHomeNavScreen>
struct IEnumerator_1_t3369529419;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m54368980_gshared (ReadOnlyCollection_1_t1784823988 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m54368980(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m54368980_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m791954380_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m791954380(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m791954380_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m94781896_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m94781896(__this, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m94781896_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m384991601_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m384991601(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m384991601_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1924513137_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1924513137(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1924513137_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m542815141_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m542815141(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m542815141_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3790923665_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3790923665(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3790923665_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1105001670_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1105001670(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1105001670_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2146693910_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2146693910(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2146693910_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4027372039_gshared (ReadOnlyCollection_1_t1784823988 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4027372039(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4027372039_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3420546466_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3420546466(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3420546466_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m764516455_gshared (ReadOnlyCollection_1_t1784823988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m764516455(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1784823988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m764516455_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1324544567_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1324544567(__this, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1324544567_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3188942847_gshared (ReadOnlyCollection_1_t1784823988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3188942847(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1784823988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3188942847_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m403343193_gshared (ReadOnlyCollection_1_t1784823988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m403343193(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1784823988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m403343193_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3722054536_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3722054536(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3722054536_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m407084046_gshared (ReadOnlyCollection_1_t1784823988 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m407084046(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m407084046_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1698882094_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1698882094(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1698882094_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2875597623_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2875597623(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2875597623_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3189101015_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3189101015(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3189101015_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1636534540_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1636534540(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1636534540_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1784179315_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1784179315(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1784179315_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3662078048_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3662078048(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3662078048_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3761540953_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3761540953(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3761540953_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m778993814_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m778993814(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m778993814_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3218910584_gshared (ReadOnlyCollection_1_t1784823988 * __this, FriendHomeNavScreenU5BU5D_t1773644873* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3218910584(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1784823988 *, FriendHomeNavScreenU5BU5D_t1773644873*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3218910584_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2721950983_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2721950983(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2721950983_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3423175462_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3423175462(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3423175462_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3952653463_gshared (ReadOnlyCollection_1_t1784823988 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3952653463(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1784823988 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3952653463_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m3270375005_gshared (ReadOnlyCollection_1_t1784823988 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3270375005(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1784823988 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3270375005_gshared)(__this, ___index0, method)
