﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserNotificationVibrate>c__AnonStorey1C
struct U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C_t1601896396;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserNotificationVibrate>c__AnonStorey1C::.ctor()
extern "C"  void U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C__ctor_m359801445 (U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C_t1601896396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserNotificationVibrate>c__AnonStorey1C::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C_U3CU3Em__0_m374468244 (U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C_t1601896396 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
