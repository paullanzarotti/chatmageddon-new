﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// StatusILP
struct StatusILP_t1076408775;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusIncomingNavScreen
struct  StatusIncomingNavScreen_t3315318769  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject StatusIncomingNavScreen::hasIncomingUI
	GameObject_t1756533147 * ___hasIncomingUI_3;
	// UnityEngine.GameObject StatusIncomingNavScreen::noIncomingUI
	GameObject_t1756533147 * ___noIncomingUI_4;
	// StatusILP StatusIncomingNavScreen::incomingList
	StatusILP_t1076408775 * ___incomingList_5;
	// System.Boolean StatusIncomingNavScreen::uiClosing
	bool ___uiClosing_6;

public:
	inline static int32_t get_offset_of_hasIncomingUI_3() { return static_cast<int32_t>(offsetof(StatusIncomingNavScreen_t3315318769, ___hasIncomingUI_3)); }
	inline GameObject_t1756533147 * get_hasIncomingUI_3() const { return ___hasIncomingUI_3; }
	inline GameObject_t1756533147 ** get_address_of_hasIncomingUI_3() { return &___hasIncomingUI_3; }
	inline void set_hasIncomingUI_3(GameObject_t1756533147 * value)
	{
		___hasIncomingUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasIncomingUI_3, value);
	}

	inline static int32_t get_offset_of_noIncomingUI_4() { return static_cast<int32_t>(offsetof(StatusIncomingNavScreen_t3315318769, ___noIncomingUI_4)); }
	inline GameObject_t1756533147 * get_noIncomingUI_4() const { return ___noIncomingUI_4; }
	inline GameObject_t1756533147 ** get_address_of_noIncomingUI_4() { return &___noIncomingUI_4; }
	inline void set_noIncomingUI_4(GameObject_t1756533147 * value)
	{
		___noIncomingUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___noIncomingUI_4, value);
	}

	inline static int32_t get_offset_of_incomingList_5() { return static_cast<int32_t>(offsetof(StatusIncomingNavScreen_t3315318769, ___incomingList_5)); }
	inline StatusILP_t1076408775 * get_incomingList_5() const { return ___incomingList_5; }
	inline StatusILP_t1076408775 ** get_address_of_incomingList_5() { return &___incomingList_5; }
	inline void set_incomingList_5(StatusILP_t1076408775 * value)
	{
		___incomingList_5 = value;
		Il2CppCodeGenWriteBarrier(&___incomingList_5, value);
	}

	inline static int32_t get_offset_of_uiClosing_6() { return static_cast<int32_t>(offsetof(StatusIncomingNavScreen_t3315318769, ___uiClosing_6)); }
	inline bool get_uiClosing_6() const { return ___uiClosing_6; }
	inline bool* get_address_of_uiClosing_6() { return &___uiClosing_6; }
	inline void set_uiClosing_6(bool value)
	{
		___uiClosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
