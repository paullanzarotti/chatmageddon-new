﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.CSharp.CSharpCodeCompiler
struct CSharpCodeCompiler_t1800184175;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.CodeDom.Compiler.CompilerResults
struct CompilerResults_t2337889805;
// System.CodeDom.Compiler.CompilerParameters
struct CompilerParameters_t2983628483;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object
struct Il2CppObject;
// System.Diagnostics.DataReceivedEventArgs
struct DataReceivedEventArgs_t3302745294;
// System.CodeDom.Compiler.CompilerError
struct CompilerError_t2965933621;
// System.CodeDom.Compiler.TempFileCollection
struct TempFileCollection_t3377240462;

#include "codegen/il2cpp-codegen.h"
#include "System_System_CodeDom_Compiler_CompilerParameters2983628483.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_Diagnostics_DataReceivedEventArgs3302745294.h"
#include "System_System_CodeDom_Compiler_TempFileCollection3377240462.h"

// System.Void Mono.CSharp.CSharpCodeCompiler::.ctor()
extern "C"  void CSharpCodeCompiler__ctor_m1725956987 (CSharpCodeCompiler_t1800184175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.CSharp.CSharpCodeCompiler::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  void CSharpCodeCompiler__ctor_m387784841 (CSharpCodeCompiler_t1800184175 * __this, Il2CppObject* ___providerOptions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.CSharp.CSharpCodeCompiler::.cctor()
extern "C"  void CSharpCodeCompiler__cctor_m1012177936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.Compiler.CompilerResults Mono.CSharp.CSharpCodeCompiler::CompileAssemblyFromFile(System.CodeDom.Compiler.CompilerParameters,System.String)
extern "C"  CompilerResults_t2337889805 * CSharpCodeCompiler_CompileAssemblyFromFile_m387946477 (CSharpCodeCompiler_t1800184175 * __this, CompilerParameters_t2983628483 * ___options0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.Compiler.CompilerResults Mono.CSharp.CSharpCodeCompiler::CompileAssemblyFromFileBatch(System.CodeDom.Compiler.CompilerParameters,System.String[])
extern "C"  CompilerResults_t2337889805 * CSharpCodeCompiler_CompileAssemblyFromFileBatch_m2347954711 (CSharpCodeCompiler_t1800184175 * __this, CompilerParameters_t2983628483 * ___options0, StringU5BU5D_t1642385972* ___fileNames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.Compiler.CompilerResults Mono.CSharp.CSharpCodeCompiler::CompileFromFileBatch(System.CodeDom.Compiler.CompilerParameters,System.String[])
extern "C"  CompilerResults_t2337889805 * CSharpCodeCompiler_CompileFromFileBatch_m4195059585 (CSharpCodeCompiler_t1800184175 * __this, CompilerParameters_t2983628483 * ___options0, StringU5BU5D_t1642385972* ___fileNames1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.CSharp.CSharpCodeCompiler::McsStderrDataReceived(System.Object,System.Diagnostics.DataReceivedEventArgs)
extern "C"  void CSharpCodeCompiler_McsStderrDataReceived_m3434914254 (CSharpCodeCompiler_t1800184175 * __this, Il2CppObject * ___sender0, DataReceivedEventArgs_t3302745294 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.CSharp.CSharpCodeCompiler::BuildArgs(System.CodeDom.Compiler.CompilerParameters,System.String[],System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C"  String_t* CSharpCodeCompiler_BuildArgs_m3602242631 (Il2CppObject * __this /* static, unused */, CompilerParameters_t2983628483 * ___options0, StringU5BU5D_t1642385972* ___fileNames1, Il2CppObject* ___providerOptions2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.Compiler.CompilerError Mono.CSharp.CSharpCodeCompiler::CreateErrorFromString(System.String)
extern "C"  CompilerError_t2965933621 * CSharpCodeCompiler_CreateErrorFromString_m2553496745 (Il2CppObject * __this /* static, unused */, String_t* ___error_string0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.CSharp.CSharpCodeCompiler::GetTempFileNameWithExtension(System.CodeDom.Compiler.TempFileCollection,System.String,System.Boolean)
extern "C"  String_t* CSharpCodeCompiler_GetTempFileNameWithExtension_m2306894696 (Il2CppObject * __this /* static, unused */, TempFileCollection_t3377240462 * ___temp_files0, String_t* ___extension1, bool ___keepFile2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
