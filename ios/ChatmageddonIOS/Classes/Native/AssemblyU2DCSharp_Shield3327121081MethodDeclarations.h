﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shield
struct Shield_t3327121081;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ShieldType96263953.h"
#include "AssemblyU2DCSharp_ShieldProtection4135544898.h"

// System.Void Shield::.ctor(System.Collections.Hashtable)
extern "C"  void Shield__ctor_m13389376 (Shield_t3327121081 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shield::.ctor(Shield)
extern "C"  void Shield__ctor_m2554446827 (Shield_t3327121081 * __this, Shield_t3327121081 * ___existingItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shield::SetDeactivateTime(System.String)
extern "C"  void Shield_SetDeactivateTime_m1019816785 (Shield_t3327121081 * __this, String_t* ___dateTimeString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ShieldType Shield::GetShieldTypeFromString(System.String)
extern "C"  int32_t Shield_GetShieldTypeFromString_m3891406930 (Shield_t3327121081 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ShieldProtection Shield::GetShieldProtectionFromString(System.String)
extern "C"  int32_t Shield_GetShieldProtectionFromString_m3548346130 (Shield_t3327121081 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shield::CalculateShieldPorgress()
extern "C"  void Shield_CalculateShieldPorgress_m285053632 (Shield_t3327121081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
