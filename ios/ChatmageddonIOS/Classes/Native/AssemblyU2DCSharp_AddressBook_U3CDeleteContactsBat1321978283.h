﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AddressBook
struct AddressBook_t411411037;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddressBook/<DeleteContactsBatchFromServer>c__AnonStorey1
struct  U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283  : public Il2CppObject
{
public:
	// System.Int32 AddressBook/<DeleteContactsBatchFromServer>c__AnonStorey1::batchID
	int32_t ___batchID_0;
	// AddressBook AddressBook/<DeleteContactsBatchFromServer>c__AnonStorey1::$this
	AddressBook_t411411037 * ___U24this_1;

public:
	inline static int32_t get_offset_of_batchID_0() { return static_cast<int32_t>(offsetof(U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283, ___batchID_0)); }
	inline int32_t get_batchID_0() const { return ___batchID_0; }
	inline int32_t* get_address_of_batchID_0() { return &___batchID_0; }
	inline void set_batchID_0(int32_t value)
	{
		___batchID_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDeleteContactsBatchFromServerU3Ec__AnonStorey1_t1321978283, ___U24this_1)); }
	inline AddressBook_t411411037 * get_U24this_1() const { return ___U24this_1; }
	inline AddressBook_t411411037 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AddressBook_t411411037 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
