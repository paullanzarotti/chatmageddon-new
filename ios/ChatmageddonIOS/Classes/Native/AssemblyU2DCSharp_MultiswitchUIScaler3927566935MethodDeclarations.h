﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MultiswitchUIScaler
struct MultiswitchUIScaler_t3927566935;

#include "codegen/il2cpp-codegen.h"

// System.Void MultiswitchUIScaler::.ctor()
extern "C"  void MultiswitchUIScaler__ctor_m4213423058 (MultiswitchUIScaler_t3927566935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MultiswitchUIScaler::GlobalUIScale()
extern "C"  void MultiswitchUIScaler_GlobalUIScale_m1287016869 (MultiswitchUIScaler_t3927566935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
