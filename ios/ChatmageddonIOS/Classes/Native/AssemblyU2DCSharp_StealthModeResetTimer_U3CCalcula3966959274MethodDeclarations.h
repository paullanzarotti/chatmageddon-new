﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeResetTimer/<CalculateResetTime>c__Iterator0
struct U3CCalculateResetTimeU3Ec__Iterator0_t3966959274;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StealthModeResetTimer/<CalculateResetTime>c__Iterator0::.ctor()
extern "C"  void U3CCalculateResetTimeU3Ec__Iterator0__ctor_m2045278109 (U3CCalculateResetTimeU3Ec__Iterator0_t3966959274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StealthModeResetTimer/<CalculateResetTime>c__Iterator0::MoveNext()
extern "C"  bool U3CCalculateResetTimeU3Ec__Iterator0_MoveNext_m684291603 (U3CCalculateResetTimeU3Ec__Iterator0_t3966959274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StealthModeResetTimer/<CalculateResetTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCalculateResetTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m54430291 (U3CCalculateResetTimeU3Ec__Iterator0_t3966959274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StealthModeResetTimer/<CalculateResetTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCalculateResetTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3113442715 (U3CCalculateResetTimeU3Ec__Iterator0_t3966959274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeResetTimer/<CalculateResetTime>c__Iterator0::Dispose()
extern "C"  void U3CCalculateResetTimeU3Ec__Iterator0_Dispose_m1608859252 (U3CCalculateResetTimeU3Ec__Iterator0_t3966959274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeResetTimer/<CalculateResetTime>c__Iterator0::Reset()
extern "C"  void U3CCalculateResetTimeU3Ec__Iterator0_Reset_m86464830 (U3CCalculateResetTimeU3Ec__Iterator0_t3966959274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
