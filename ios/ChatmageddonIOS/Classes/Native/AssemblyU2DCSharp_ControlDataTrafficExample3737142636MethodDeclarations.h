﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ControlDataTrafficExample
struct ControlDataTrafficExample_t3737142636;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"

// System.Void ControlDataTrafficExample::.ctor()
extern "C"  void ControlDataTrafficExample__ctor_m1206645865 (ControlDataTrafficExample_t3737142636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlDataTrafficExample::OnGUI()
extern "C"  void ControlDataTrafficExample_OnGUI_m219186467 (ControlDataTrafficExample_t3737142636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlDataTrafficExample::Start()
extern "C"  void ControlDataTrafficExample_Start_m2104454065 (ControlDataTrafficExample_t3737142636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ControlDataTrafficExample::OnTileDownloaded(OnlineMapsTile)
extern "C"  void ControlDataTrafficExample_OnTileDownloaded_m2857814021 (ControlDataTrafficExample_t3737142636 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
