﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetAboutAndHelpInfo>c__AnonStorey39
struct U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetAboutAndHelpInfo>c__AnonStorey39::.ctor()
extern "C"  void U3CGetAboutAndHelpInfoU3Ec__AnonStorey39__ctor_m3624054240 (U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetAboutAndHelpInfo>c__AnonStorey39::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_U3CU3Em__0_m1961419833 (U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
