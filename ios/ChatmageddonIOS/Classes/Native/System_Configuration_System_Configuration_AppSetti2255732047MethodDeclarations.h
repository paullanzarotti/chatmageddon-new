﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.AppSettingsSection
struct AppSettingsSection_t2255732047;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t1776195828;
// System.String
struct String_t;
// System.Configuration.KeyValueConfigurationCollection
struct KeyValueConfigurationCollection_t2800862928;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Configura700320212.h"

// System.Void System.Configuration.AppSettingsSection::.ctor()
extern "C"  void AppSettingsSection__ctor_m2802383180 (AppSettingsSection_t2255732047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.AppSettingsSection::.cctor()
extern "C"  void AppSettingsSection__cctor_m3828414575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.AppSettingsSection::IsModified()
extern "C"  bool AppSettingsSection_IsModified_m949289315 (AppSettingsSection_t2255732047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.AppSettingsSection::DeserializeElement(System.Xml.XmlReader,System.Boolean)
extern "C"  void AppSettingsSection_DeserializeElement_m2865664832 (AppSettingsSection_t2255732047 * __this, XmlReader_t3675626668 * ___reader0, bool ___serializeCollectionKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.AppSettingsSection::Reset(System.Configuration.ConfigurationElement)
extern "C"  void AppSettingsSection_Reset_m560408398 (AppSettingsSection_t2255732047 * __this, ConfigurationElement_t1776195828 * ___parentSection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.AppSettingsSection::SerializeSection(System.Configuration.ConfigurationElement,System.String,System.Configuration.ConfigurationSaveMode)
extern "C"  String_t* AppSettingsSection_SerializeSection_m2404471402 (AppSettingsSection_t2255732047 * __this, ConfigurationElement_t1776195828 * ___parent0, String_t* ___name1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.AppSettingsSection::get_File()
extern "C"  String_t* AppSettingsSection_get_File_m2049681838 (AppSettingsSection_t2255732047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.AppSettingsSection::set_File(System.String)
extern "C"  void AppSettingsSection_set_File_m47834781 (AppSettingsSection_t2255732047 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.KeyValueConfigurationCollection System.Configuration.AppSettingsSection::get_Settings()
extern "C"  KeyValueConfigurationCollection_t2800862928 * AppSettingsSection_get_Settings_m1168898596 (AppSettingsSection_t2255732047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.AppSettingsSection::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * AppSettingsSection_get_Properties_m1443336267 (AppSettingsSection_t2255732047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.AppSettingsSection::GetRuntimeObject()
extern "C"  Il2CppObject * AppSettingsSection_GetRuntimeObject_m4270449620 (AppSettingsSection_t2255732047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
