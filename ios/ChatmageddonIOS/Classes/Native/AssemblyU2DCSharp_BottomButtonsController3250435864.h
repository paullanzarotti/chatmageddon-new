﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TweenPosition
struct TweenPosition_t1144714832;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BottomButtonsController
struct  BottomButtonsController_t3250435864  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BottomButtonsController::targetsButton
	GameObject_t1756533147 * ___targetsButton_2;
	// TweenPosition BottomButtonsController::buttonsTween
	TweenPosition_t1144714832 * ___buttonsTween_3;
	// TweenPosition BottomButtonsController::labelsTween
	TweenPosition_t1144714832 * ___labelsTween_4;
	// System.Boolean BottomButtonsController::targetsUIclosing
	bool ___targetsUIclosing_5;

public:
	inline static int32_t get_offset_of_targetsButton_2() { return static_cast<int32_t>(offsetof(BottomButtonsController_t3250435864, ___targetsButton_2)); }
	inline GameObject_t1756533147 * get_targetsButton_2() const { return ___targetsButton_2; }
	inline GameObject_t1756533147 ** get_address_of_targetsButton_2() { return &___targetsButton_2; }
	inline void set_targetsButton_2(GameObject_t1756533147 * value)
	{
		___targetsButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetsButton_2, value);
	}

	inline static int32_t get_offset_of_buttonsTween_3() { return static_cast<int32_t>(offsetof(BottomButtonsController_t3250435864, ___buttonsTween_3)); }
	inline TweenPosition_t1144714832 * get_buttonsTween_3() const { return ___buttonsTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_buttonsTween_3() { return &___buttonsTween_3; }
	inline void set_buttonsTween_3(TweenPosition_t1144714832 * value)
	{
		___buttonsTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonsTween_3, value);
	}

	inline static int32_t get_offset_of_labelsTween_4() { return static_cast<int32_t>(offsetof(BottomButtonsController_t3250435864, ___labelsTween_4)); }
	inline TweenPosition_t1144714832 * get_labelsTween_4() const { return ___labelsTween_4; }
	inline TweenPosition_t1144714832 ** get_address_of_labelsTween_4() { return &___labelsTween_4; }
	inline void set_labelsTween_4(TweenPosition_t1144714832 * value)
	{
		___labelsTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___labelsTween_4, value);
	}

	inline static int32_t get_offset_of_targetsUIclosing_5() { return static_cast<int32_t>(offsetof(BottomButtonsController_t3250435864, ___targetsUIclosing_5)); }
	inline bool get_targetsUIclosing_5() const { return ___targetsUIclosing_5; }
	inline bool* get_address_of_targetsUIclosing_5() { return &___targetsUIclosing_5; }
	inline void set_targetsUIclosing_5(bool value)
	{
		___targetsUIclosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
