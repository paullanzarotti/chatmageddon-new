﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeUI
struct StealthModeUI_t2601479126;

#include "codegen/il2cpp-codegen.h"

// System.Void StealthModeUI::.ctor()
extern "C"  void StealthModeUI__ctor_m1403647271 (StealthModeUI_t2601479126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::OnEnable()
extern "C"  void StealthModeUI_OnEnable_m3035393275 (StealthModeUI_t2601479126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::OnDisable()
extern "C"  void StealthModeUI_OnDisable_m814999042 (StealthModeUI_t2601479126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::Awake()
extern "C"  void StealthModeUI_Awake_m1630587046 (StealthModeUI_t2601479126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::CalculatorHitZero(System.Boolean)
extern "C"  void StealthModeUI_CalculatorHitZero_m3559485259 (StealthModeUI_t2601479126 * __this, bool ___inStealth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::LoadUI(System.Boolean)
extern "C"  void StealthModeUI_LoadUI_m4111816706 (StealthModeUI_t2601479126 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::UnloadUI(System.Boolean)
extern "C"  void StealthModeUI_UnloadUI_m1215824105 (StealthModeUI_t2601479126 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::ShowUI(System.Boolean,System.Boolean)
extern "C"  void StealthModeUI_ShowUI_m190326510 (StealthModeUI_t2601479126 * __this, bool ___instant0, bool ___reset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::HideUI(System.Boolean)
extern "C"  void StealthModeUI_HideUI_m3918476142 (StealthModeUI_t2601479126 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::BackgroundTweenFinished()
extern "C"  void StealthModeUI_BackgroundTweenFinished_m680683364 (StealthModeUI_t2601479126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeUI::ContentTweenFinished()
extern "C"  void StealthModeUI_ContentTweenFinished_m453811409 (StealthModeUI_t2601479126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
