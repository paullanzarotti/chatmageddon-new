﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XNamespace
struct XNamespace_t1613015075;
// System.String
struct String_t;
// System.Xml.Linq.XName
struct XName_t785190363;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_Linq_System_Xml_Linq_XNamespace1613015075.h"

// System.Void System.Xml.Linq.XNamespace::.ctor(System.String)
extern "C"  void XNamespace__ctor_m4013965018 (XNamespace_t1613015075 * __this, String_t* ___namespaceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XNamespace::.cctor()
extern "C"  void XNamespace__cctor_m3798812553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_None()
extern "C"  XNamespace_t1613015075 * XNamespace_get_None_m1138320793 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_Xmlns()
extern "C"  XNamespace_t1613015075 * XNamespace_get_Xmlns_m4214259751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::Get(System.String)
extern "C"  XNamespace_t1613015075 * XNamespace_Get_m1026612632 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XName System.Xml.Linq.XNamespace::GetName(System.String)
extern "C"  XName_t785190363 * XNamespace_GetName_m2299836393 (XNamespace_t1613015075 * __this, String_t* ___localName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XNamespace::get_NamespaceName()
extern "C"  String_t* XNamespace_get_NamespaceName_m3174414200 (XNamespace_t1613015075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XNamespace::Equals(System.Object)
extern "C"  bool XNamespace_Equals_m777760167 (XNamespace_t1613015075 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XNamespace::GetHashCode()
extern "C"  int32_t XNamespace_GetHashCode_m1183686781 (XNamespace_t1613015075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XNamespace::ToString()
extern "C"  String_t* XNamespace_ToString_m2854292113 (XNamespace_t1613015075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XNamespace::op_Equality(System.Xml.Linq.XNamespace,System.Xml.Linq.XNamespace)
extern "C"  bool XNamespace_op_Equality_m73486322 (Il2CppObject * __this /* static, unused */, XNamespace_t1613015075 * ___o10, XNamespace_t1613015075 * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XNamespace::op_Inequality(System.Xml.Linq.XNamespace,System.Xml.Linq.XNamespace)
extern "C"  bool XNamespace_op_Inequality_m1061703997 (Il2CppObject * __this /* static, unused */, XNamespace_t1613015075 * ___o10, XNamespace_t1613015075 * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
