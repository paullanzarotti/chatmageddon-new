﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsFindLocation
struct OnlineMapsFindLocation_t1727203720;
// System.String
struct String_t;
// OnlineMapsGoogleAPIQuery
struct OnlineMapsGoogleAPIQuery_t356009153;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void OnlineMapsFindLocation::.ctor(System.String,System.String)
extern "C"  void OnlineMapsFindLocation__ctor_m2693330939 (OnlineMapsFindLocation_t1727203720 * __this, String_t* ___address0, String_t* ___latlng1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsQueryType OnlineMapsFindLocation::get_type()
extern "C"  int32_t OnlineMapsFindLocation_get_type_m1837292047 (OnlineMapsFindLocation_t1727203720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindLocation::Find(System.String,System.String)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindLocation_Find_m4153897508 (Il2CppObject * __this /* static, unused */, String_t* ___address0, String_t* ___latlng1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindLocation::Find(UnityEngine.Vector2)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindLocation_Find_m1211934382 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___latlng0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsFindLocation::GetCoordinatesFromResult(System.String)
extern "C"  Vector2_t2243707579  OnlineMapsFindLocation_GetCoordinatesFromResult_m406658260 (Il2CppObject * __this /* static, unused */, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsFindLocation::MovePositionToResult(System.String)
extern "C"  void OnlineMapsFindLocation_MovePositionToResult_m3948329551 (Il2CppObject * __this /* static, unused */, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
