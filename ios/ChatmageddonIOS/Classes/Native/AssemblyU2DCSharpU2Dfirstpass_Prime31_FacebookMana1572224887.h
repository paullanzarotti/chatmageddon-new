﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;
// System.Action`1<Prime31.P31Error>
struct Action_1_t2658399990;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Action_1_t111060643;

#include "P31RestKit_Prime31_AbstractManager1005944057.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookManager
struct  FacebookManager_t1572224887  : public AbstractManager_t1005944057
{
public:

public:
};

struct FacebookManager_t1572224887_StaticFields
{
public:
	// System.Action Prime31.FacebookManager::sessionOpenedEvent
	Action_t3226471752 * ___sessionOpenedEvent_5;
	// System.Action Prime31.FacebookManager::preLoginSucceededEvent
	Action_t3226471752 * ___preLoginSucceededEvent_6;
	// System.Action`1<Prime31.P31Error> Prime31.FacebookManager::loginFailedEvent
	Action_1_t2658399990 * ___loginFailedEvent_7;
	// System.Action`1<System.String> Prime31.FacebookManager::dialogCompletedWithUrlEvent
	Action_1_t1831019615 * ___dialogCompletedWithUrlEvent_8;
	// System.Action`1<Prime31.P31Error> Prime31.FacebookManager::dialogFailedEvent
	Action_1_t2658399990 * ___dialogFailedEvent_9;
	// System.Action`1<System.Object> Prime31.FacebookManager::graphRequestCompletedEvent
	Action_1_t2491248677 * ___graphRequestCompletedEvent_10;
	// System.Action`1<Prime31.P31Error> Prime31.FacebookManager::graphRequestFailedEvent
	Action_1_t2658399990 * ___graphRequestFailedEvent_11;
	// System.Action`1<System.Boolean> Prime31.FacebookManager::facebookComposerCompletedEvent
	Action_1_t3627374100 * ___facebookComposerCompletedEvent_12;
	// System.Action Prime31.FacebookManager::reauthorizationSucceededEvent
	Action_t3226471752 * ___reauthorizationSucceededEvent_13;
	// System.Action`1<Prime31.P31Error> Prime31.FacebookManager::reauthorizationFailedEvent
	Action_1_t2658399990 * ___reauthorizationFailedEvent_14;
	// System.Action`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>> Prime31.FacebookManager::shareDialogSucceededEvent
	Action_1_t111060643 * ___shareDialogSucceededEvent_15;
	// System.Action`1<Prime31.P31Error> Prime31.FacebookManager::shareDialogFailedEvent
	Action_1_t2658399990 * ___shareDialogFailedEvent_16;

public:
	inline static int32_t get_offset_of_sessionOpenedEvent_5() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___sessionOpenedEvent_5)); }
	inline Action_t3226471752 * get_sessionOpenedEvent_5() const { return ___sessionOpenedEvent_5; }
	inline Action_t3226471752 ** get_address_of_sessionOpenedEvent_5() { return &___sessionOpenedEvent_5; }
	inline void set_sessionOpenedEvent_5(Action_t3226471752 * value)
	{
		___sessionOpenedEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___sessionOpenedEvent_5, value);
	}

	inline static int32_t get_offset_of_preLoginSucceededEvent_6() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___preLoginSucceededEvent_6)); }
	inline Action_t3226471752 * get_preLoginSucceededEvent_6() const { return ___preLoginSucceededEvent_6; }
	inline Action_t3226471752 ** get_address_of_preLoginSucceededEvent_6() { return &___preLoginSucceededEvent_6; }
	inline void set_preLoginSucceededEvent_6(Action_t3226471752 * value)
	{
		___preLoginSucceededEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___preLoginSucceededEvent_6, value);
	}

	inline static int32_t get_offset_of_loginFailedEvent_7() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___loginFailedEvent_7)); }
	inline Action_1_t2658399990 * get_loginFailedEvent_7() const { return ___loginFailedEvent_7; }
	inline Action_1_t2658399990 ** get_address_of_loginFailedEvent_7() { return &___loginFailedEvent_7; }
	inline void set_loginFailedEvent_7(Action_1_t2658399990 * value)
	{
		___loginFailedEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___loginFailedEvent_7, value);
	}

	inline static int32_t get_offset_of_dialogCompletedWithUrlEvent_8() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___dialogCompletedWithUrlEvent_8)); }
	inline Action_1_t1831019615 * get_dialogCompletedWithUrlEvent_8() const { return ___dialogCompletedWithUrlEvent_8; }
	inline Action_1_t1831019615 ** get_address_of_dialogCompletedWithUrlEvent_8() { return &___dialogCompletedWithUrlEvent_8; }
	inline void set_dialogCompletedWithUrlEvent_8(Action_1_t1831019615 * value)
	{
		___dialogCompletedWithUrlEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___dialogCompletedWithUrlEvent_8, value);
	}

	inline static int32_t get_offset_of_dialogFailedEvent_9() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___dialogFailedEvent_9)); }
	inline Action_1_t2658399990 * get_dialogFailedEvent_9() const { return ___dialogFailedEvent_9; }
	inline Action_1_t2658399990 ** get_address_of_dialogFailedEvent_9() { return &___dialogFailedEvent_9; }
	inline void set_dialogFailedEvent_9(Action_1_t2658399990 * value)
	{
		___dialogFailedEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___dialogFailedEvent_9, value);
	}

	inline static int32_t get_offset_of_graphRequestCompletedEvent_10() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___graphRequestCompletedEvent_10)); }
	inline Action_1_t2491248677 * get_graphRequestCompletedEvent_10() const { return ___graphRequestCompletedEvent_10; }
	inline Action_1_t2491248677 ** get_address_of_graphRequestCompletedEvent_10() { return &___graphRequestCompletedEvent_10; }
	inline void set_graphRequestCompletedEvent_10(Action_1_t2491248677 * value)
	{
		___graphRequestCompletedEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___graphRequestCompletedEvent_10, value);
	}

	inline static int32_t get_offset_of_graphRequestFailedEvent_11() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___graphRequestFailedEvent_11)); }
	inline Action_1_t2658399990 * get_graphRequestFailedEvent_11() const { return ___graphRequestFailedEvent_11; }
	inline Action_1_t2658399990 ** get_address_of_graphRequestFailedEvent_11() { return &___graphRequestFailedEvent_11; }
	inline void set_graphRequestFailedEvent_11(Action_1_t2658399990 * value)
	{
		___graphRequestFailedEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___graphRequestFailedEvent_11, value);
	}

	inline static int32_t get_offset_of_facebookComposerCompletedEvent_12() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___facebookComposerCompletedEvent_12)); }
	inline Action_1_t3627374100 * get_facebookComposerCompletedEvent_12() const { return ___facebookComposerCompletedEvent_12; }
	inline Action_1_t3627374100 ** get_address_of_facebookComposerCompletedEvent_12() { return &___facebookComposerCompletedEvent_12; }
	inline void set_facebookComposerCompletedEvent_12(Action_1_t3627374100 * value)
	{
		___facebookComposerCompletedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___facebookComposerCompletedEvent_12, value);
	}

	inline static int32_t get_offset_of_reauthorizationSucceededEvent_13() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___reauthorizationSucceededEvent_13)); }
	inline Action_t3226471752 * get_reauthorizationSucceededEvent_13() const { return ___reauthorizationSucceededEvent_13; }
	inline Action_t3226471752 ** get_address_of_reauthorizationSucceededEvent_13() { return &___reauthorizationSucceededEvent_13; }
	inline void set_reauthorizationSucceededEvent_13(Action_t3226471752 * value)
	{
		___reauthorizationSucceededEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___reauthorizationSucceededEvent_13, value);
	}

	inline static int32_t get_offset_of_reauthorizationFailedEvent_14() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___reauthorizationFailedEvent_14)); }
	inline Action_1_t2658399990 * get_reauthorizationFailedEvent_14() const { return ___reauthorizationFailedEvent_14; }
	inline Action_1_t2658399990 ** get_address_of_reauthorizationFailedEvent_14() { return &___reauthorizationFailedEvent_14; }
	inline void set_reauthorizationFailedEvent_14(Action_1_t2658399990 * value)
	{
		___reauthorizationFailedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___reauthorizationFailedEvent_14, value);
	}

	inline static int32_t get_offset_of_shareDialogSucceededEvent_15() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___shareDialogSucceededEvent_15)); }
	inline Action_1_t111060643 * get_shareDialogSucceededEvent_15() const { return ___shareDialogSucceededEvent_15; }
	inline Action_1_t111060643 ** get_address_of_shareDialogSucceededEvent_15() { return &___shareDialogSucceededEvent_15; }
	inline void set_shareDialogSucceededEvent_15(Action_1_t111060643 * value)
	{
		___shareDialogSucceededEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___shareDialogSucceededEvent_15, value);
	}

	inline static int32_t get_offset_of_shareDialogFailedEvent_16() { return static_cast<int32_t>(offsetof(FacebookManager_t1572224887_StaticFields, ___shareDialogFailedEvent_16)); }
	inline Action_1_t2658399990 * get_shareDialogFailedEvent_16() const { return ___shareDialogFailedEvent_16; }
	inline Action_1_t2658399990 ** get_address_of_shareDialogFailedEvent_16() { return &___shareDialogFailedEvent_16; }
	inline void set_shareDialogFailedEvent_16(Action_1_t2658399990 * value)
	{
		___shareDialogFailedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___shareDialogFailedEvent_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
