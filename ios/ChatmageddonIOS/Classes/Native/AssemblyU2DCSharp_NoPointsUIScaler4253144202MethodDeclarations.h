﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoPointsUIScaler
struct NoPointsUIScaler_t4253144202;

#include "codegen/il2cpp-codegen.h"

// System.Void NoPointsUIScaler::.ctor()
extern "C"  void NoPointsUIScaler__ctor_m1027340255 (NoPointsUIScaler_t4253144202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsUIScaler::GlobalUIScale()
extern "C"  void NoPointsUIScaler_GlobalUIScale_m2686559236 (NoPointsUIScaler_t4253144202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
