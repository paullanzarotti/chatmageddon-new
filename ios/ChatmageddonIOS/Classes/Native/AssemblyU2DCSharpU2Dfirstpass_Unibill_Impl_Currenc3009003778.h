﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Uniject.IStorage
struct IStorage_t1347868490;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Func`2<VirtualCurrency,System.String>
struct Func_2_t317055586;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.CurrencyManager
struct  CurrencyManager_t3009003778  : public Il2CppObject
{
public:
	// Uniject.IStorage Unibill.Impl.CurrencyManager::storage
	Il2CppObject * ___storage_0;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.CurrencyManager::config
	UnibillConfiguration_t2915611853 * ___config_1;
	// System.String[] Unibill.Impl.CurrencyManager::<Currencies>k__BackingField
	StringU5BU5D_t1642385972* ___U3CCurrenciesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_storage_0() { return static_cast<int32_t>(offsetof(CurrencyManager_t3009003778, ___storage_0)); }
	inline Il2CppObject * get_storage_0() const { return ___storage_0; }
	inline Il2CppObject ** get_address_of_storage_0() { return &___storage_0; }
	inline void set_storage_0(Il2CppObject * value)
	{
		___storage_0 = value;
		Il2CppCodeGenWriteBarrier(&___storage_0, value);
	}

	inline static int32_t get_offset_of_config_1() { return static_cast<int32_t>(offsetof(CurrencyManager_t3009003778, ___config_1)); }
	inline UnibillConfiguration_t2915611853 * get_config_1() const { return ___config_1; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_config_1() { return &___config_1; }
	inline void set_config_1(UnibillConfiguration_t2915611853 * value)
	{
		___config_1 = value;
		Il2CppCodeGenWriteBarrier(&___config_1, value);
	}

	inline static int32_t get_offset_of_U3CCurrenciesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CurrencyManager_t3009003778, ___U3CCurrenciesU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CCurrenciesU3Ek__BackingField_2() const { return ___U3CCurrenciesU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CCurrenciesU3Ek__BackingField_2() { return &___U3CCurrenciesU3Ek__BackingField_2; }
	inline void set_U3CCurrenciesU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CCurrenciesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrenciesU3Ek__BackingField_2, value);
	}
};

struct CurrencyManager_t3009003778_StaticFields
{
public:
	// System.Func`2<VirtualCurrency,System.String> Unibill.Impl.CurrencyManager::<>f__am$cache0
	Func_2_t317055586 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(CurrencyManager_t3009003778_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t317055586 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t317055586 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t317055586 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
