﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchInput
struct AttackSearchInput_t1258707860;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackSearchInput::.ctor()
extern "C"  void AttackSearchInput__ctor_m858987775 (AttackSearchInput_t1258707860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchInput::OnSearchSubmit()
extern "C"  void AttackSearchInput_OnSearchSubmit_m3214537030 (AttackSearchInput_t1258707860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchInput::OnSelect(System.Boolean)
extern "C"  void AttackSearchInput_OnSelect_m3749693441 (AttackSearchInput_t1258707860 * __this, bool ___selected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
