﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AttackHomeNav>
struct List_1_t3314011636;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2848741310.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AttackHomeNav>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m4074522769_gshared (Enumerator_t2848741310 * __this, List_1_t3314011636 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m4074522769(__this, ___l0, method) ((  void (*) (Enumerator_t2848741310 *, List_1_t3314011636 *, const MethodInfo*))Enumerator__ctor_m4074522769_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackHomeNav>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4132198497_gshared (Enumerator_t2848741310 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4132198497(__this, method) ((  void (*) (Enumerator_t2848741310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4132198497_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AttackHomeNav>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1918311009_gshared (Enumerator_t2848741310 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1918311009(__this, method) ((  Il2CppObject * (*) (Enumerator_t2848741310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1918311009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackHomeNav>::Dispose()
extern "C"  void Enumerator_Dispose_m1383484308_gshared (Enumerator_t2848741310 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1383484308(__this, method) ((  void (*) (Enumerator_t2848741310 *, const MethodInfo*))Enumerator_Dispose_m1383484308_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackHomeNav>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2713461971_gshared (Enumerator_t2848741310 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2713461971(__this, method) ((  void (*) (Enumerator_t2848741310 *, const MethodInfo*))Enumerator_VerifyState_m2713461971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AttackHomeNav>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3671422529_gshared (Enumerator_t2848741310 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3671422529(__this, method) ((  bool (*) (Enumerator_t2848741310 *, const MethodInfo*))Enumerator_MoveNext_m3671422529_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AttackHomeNav>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3045101862_gshared (Enumerator_t2848741310 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3045101862(__this, method) ((  int32_t (*) (Enumerator_t2848741310 *, const MethodInfo*))Enumerator_get_Current_m3045101862_gshared)(__this, method)
