﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<FAQListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m2587667661(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2536695735 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<FAQListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m1085831256(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t2536695735 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FAQListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m4208183194(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2536695735 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FAQListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m3025508667(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2536695735 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FAQListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m2859098232(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t2536695735 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<FAQListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m1289239793(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2536695735 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<FAQListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m1007881868(__this, method) ((  void (*) (BaseInfiniteListItem_1_t2536695735 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
