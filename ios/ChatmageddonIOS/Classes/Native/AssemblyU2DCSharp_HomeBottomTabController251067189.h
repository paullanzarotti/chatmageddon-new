﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1732909.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeBottomTabController
struct  HomeBottomTabController_t251067189  : public MonoSingleton_1_t1732909
{
public:
	// TweenPosition HomeBottomTabController::posTween
	TweenPosition_t1144714832 * ___posTween_3;
	// System.Boolean HomeBottomTabController::tabClosing
	bool ___tabClosing_4;

public:
	inline static int32_t get_offset_of_posTween_3() { return static_cast<int32_t>(offsetof(HomeBottomTabController_t251067189, ___posTween_3)); }
	inline TweenPosition_t1144714832 * get_posTween_3() const { return ___posTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_3() { return &___posTween_3; }
	inline void set_posTween_3(TweenPosition_t1144714832 * value)
	{
		___posTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_3, value);
	}

	inline static int32_t get_offset_of_tabClosing_4() { return static_cast<int32_t>(offsetof(HomeBottomTabController_t251067189, ___tabClosing_4)); }
	inline bool get_tabClosing_4() const { return ___tabClosing_4; }
	inline bool* get_address_of_tabClosing_4() { return &___tabClosing_4; }
	inline void set_tabClosing_4(bool value)
	{
		___tabClosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
