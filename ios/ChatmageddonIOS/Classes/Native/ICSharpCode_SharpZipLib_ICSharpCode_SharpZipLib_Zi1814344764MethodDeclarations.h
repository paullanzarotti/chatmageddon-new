﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler
struct ReadDataHandler_t1814344764;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ReadDataHandler__ctor_m1555072274 (ReadDataHandler_t1814344764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::Invoke(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ReadDataHandler_Invoke_m3406984789 (ReadDataHandler_t1814344764 * __this, ByteU5BU5D_t3397334013* ___b0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReadDataHandler_BeginInvoke_m115987378 (ReadDataHandler_t1814344764 * __this, ByteU5BU5D_t3397334013* ___b0, int32_t ___offset1, int32_t ___length2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::EndInvoke(System.IAsyncResult)
extern "C"  int32_t ReadDataHandler_EndInvoke_m2292870872 (ReadDataHandler_t1814344764 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
