﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.KeyValueInternalCollection
struct KeyValueInternalCollection_t919399291;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.KeyValueInternalCollection::.ctor()
extern "C"  void KeyValueInternalCollection__ctor_m2821540952 (KeyValueInternalCollection_t919399291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueInternalCollection::SetReadOnly()
extern "C"  void KeyValueInternalCollection_SetReadOnly_m2337010558 (KeyValueInternalCollection_t919399291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.KeyValueInternalCollection::Add(System.String,System.String)
extern "C"  void KeyValueInternalCollection_Add_m939505269 (KeyValueInternalCollection_t919399291 * __this, String_t* ___name0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
