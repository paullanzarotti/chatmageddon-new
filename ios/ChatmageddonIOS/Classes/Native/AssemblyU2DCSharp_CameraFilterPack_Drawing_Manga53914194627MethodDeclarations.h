﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Manga5
struct CameraFilterPack_Drawing_Manga5_t3914194627;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Manga5::.ctor()
extern "C"  void CameraFilterPack_Drawing_Manga5__ctor_m85480604 (CameraFilterPack_Drawing_Manga5_t3914194627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga5::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Manga5_get_material_m2459120439 (CameraFilterPack_Drawing_Manga5_t3914194627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::Start()
extern "C"  void CameraFilterPack_Drawing_Manga5_Start_m3572690632 (CameraFilterPack_Drawing_Manga5_t3914194627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Manga5_OnRenderImage_m3320507248 (CameraFilterPack_Drawing_Manga5_t3914194627 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Manga5_OnValidate_m1948638425 (CameraFilterPack_Drawing_Manga5_t3914194627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::Update()
extern "C"  void CameraFilterPack_Drawing_Manga5_Update_m3633047375 (CameraFilterPack_Drawing_Manga5_t3914194627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Manga5_OnDisable_m679703871 (CameraFilterPack_Drawing_Manga5_t3914194627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
