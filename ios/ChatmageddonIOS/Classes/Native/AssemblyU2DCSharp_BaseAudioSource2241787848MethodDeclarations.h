﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseAudioSource
struct BaseAudioSource_t2241787848;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseAudioSource::.ctor()
extern "C"  void BaseAudioSource__ctor_m1485628885 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::Awake()
extern "C"  void BaseAudioSource_Awake_m1494546932 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::Start()
extern "C"  void BaseAudioSource_Start_m2779387469 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BaseAudioSource::IsSourcePlaying()
extern "C"  bool BaseAudioSource_IsSourcePlaying_m1769539836 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::Mute()
extern "C"  void BaseAudioSource_Mute_m716169802 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::Play(System.Boolean,System.Boolean)
extern "C"  void BaseAudioSource_Play_m3022533621 (BaseAudioSource_t2241787848 * __this, bool ___loop0, bool ___forcePlay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BaseAudioSource::HideObjectCheck()
extern "C"  Il2CppObject * BaseAudioSource_HideObjectCheck_m68195818 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::Pause()
extern "C"  void BaseAudioSource_Pause_m2308656835 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::StopPlay()
extern "C"  void BaseAudioSource_StopPlay_m713569701 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::Loop(System.Boolean)
extern "C"  void BaseAudioSource_Loop_m3599210388 (BaseAudioSource_t2241787848 * __this, bool ___loop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::FadeIn(System.Single)
extern "C"  void BaseAudioSource_FadeIn_m514067661 (BaseAudioSource_t2241787848 * __this, float ___fadeDuration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BaseAudioSource::FadeInSource(System.Single)
extern "C"  Il2CppObject * BaseAudioSource_FadeInSource_m2895256600 (BaseAudioSource_t2241787848 * __this, float ___fadeDuration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::FadeOut(System.Single,System.Boolean)
extern "C"  void BaseAudioSource_FadeOut_m3094036459 (BaseAudioSource_t2241787848 * __this, float ___fadeDuration0, bool ___pause1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BaseAudioSource::FadeOutSource(System.Single,System.Boolean)
extern "C"  Il2CppObject * BaseAudioSource_FadeOutSource_m1609595476 (BaseAudioSource_t2241787848 * __this, float ___fadeDuration0, bool ___pause1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::StopFadeRoutines()
extern "C"  void BaseAudioSource_StopFadeRoutines_m1490736852 (BaseAudioSource_t2241787848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource::SetVolume(System.Single)
extern "C"  void BaseAudioSource_SetVolume_m1080816998 (BaseAudioSource_t2241787848 * __this, float ___newVolume0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
