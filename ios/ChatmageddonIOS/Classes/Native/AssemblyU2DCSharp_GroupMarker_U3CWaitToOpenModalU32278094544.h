﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// GroupMarker
struct GroupMarker_t1861645095;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupMarker/<WaitToOpenModal>c__Iterator0
struct  U3CWaitToOpenModalU3Ec__Iterator0_t2278094544  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Friend> GroupMarker/<WaitToOpenModal>c__Iterator0::<friendsGroup>__0
	List_1_t2924135240 * ___U3CfriendsGroupU3E__0_0;
	// GroupMarker GroupMarker/<WaitToOpenModal>c__Iterator0::$this
	GroupMarker_t1861645095 * ___U24this_1;
	// System.Object GroupMarker/<WaitToOpenModal>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean GroupMarker/<WaitToOpenModal>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GroupMarker/<WaitToOpenModal>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CfriendsGroupU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWaitToOpenModalU3Ec__Iterator0_t2278094544, ___U3CfriendsGroupU3E__0_0)); }
	inline List_1_t2924135240 * get_U3CfriendsGroupU3E__0_0() const { return ___U3CfriendsGroupU3E__0_0; }
	inline List_1_t2924135240 ** get_address_of_U3CfriendsGroupU3E__0_0() { return &___U3CfriendsGroupU3E__0_0; }
	inline void set_U3CfriendsGroupU3E__0_0(List_1_t2924135240 * value)
	{
		___U3CfriendsGroupU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfriendsGroupU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWaitToOpenModalU3Ec__Iterator0_t2278094544, ___U24this_1)); }
	inline GroupMarker_t1861645095 * get_U24this_1() const { return ___U24this_1; }
	inline GroupMarker_t1861645095 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GroupMarker_t1861645095 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitToOpenModalU3Ec__Iterator0_t2278094544, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitToOpenModalU3Ec__Iterator0_t2278094544, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitToOpenModalU3Ec__Iterator0_t2278094544, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
