﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FormatDateTime
struct FormatDateTime_t1158606858;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FormatDateTime::.ctor()
extern "C"  void FormatDateTime__ctor_m2717363253 (FormatDateTime_t1158606858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FormatDateTime::FormatDateTimeToString(System.DateTime)
extern "C"  String_t* FormatDateTime_FormatDateTimeToString_m194959188 (FormatDateTime_t1158606858 * __this, DateTime_t693205669  ___datetime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime FormatDateTime::FormatStringToDateTime(System.String)
extern "C"  DateTime_t693205669  FormatDateTime_FormatStringToDateTime_m1496533156 (FormatDateTime_t1158606858 * __this, String_t* ___datetimestring0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
