﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe151515769.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapMembe999867419.h"
#include "System_Xml_System_Xml_Serialization_XmlTypeMapping315595419.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializable277695926.h"
#include "System_Xml_System_Xml_Serialization_ClassMap1647926812.h"
#include "System_Xml_System_Xml_Serialization_ListMap1787375712.h"
#include "System_Xml_System_Xml_Serialization_EnumMap3161685173.h"
#include "System_Xml_System_Xml_Serialization_EnumMap_EnumMa3814867081.h"
#include "System_Xml_System_Xml_XQueryConvert3510797773.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler2964483403.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle1580700381.h"
#include "System_Xml_System_Xml_Schema_XmlValueGetter1685472371.h"
#include "System_Xml_System_Xml_Serialization_UnreferencedOb3651715031.h"
#include "System_Xml_System_Xml_Serialization_XmlAttributeEv1549396895.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3113026122.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati4030450962.h"
#include "System_Xml_System_Xml_Serialization_XmlSerializati3162327146.h"
#include "System_Xml_System_Xml_Serialization_XmlElementEven1680966409.h"
#include "System_Xml_System_Xml_Serialization_XmlNodeEventHa2205849959.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1278838065.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2844922008.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778800.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1327816026.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778802.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"
#include "Mono_Security_U3CModuleU3E3783534214.h"
#include "Mono_Security_Locale4255929014.h"
#include "Mono_Security_Mono_Math_BigInteger925946152.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign874893935.h"
#include "Mono_Security_Mono_Math_BigInteger_ModulusRing80355991.h"
#include "Mono_Security_Mono_Math_BigInteger_Kernel1353186455.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor1997037801.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTests3283102398.h"
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGener1053438167.h"
#include "Mono_Security_Mono_Math_Prime_Generator_SequentialS463670656.h"
#include "Mono_Security_Mono_Security_ASN1924533535.h"
#include "Mono_Security_Mono_Security_ASN1Convert3301846396.h"
#include "Mono_Security_Mono_Security_BitConverterLE2825370260.h"
#include "Mono_Security_Mono_Security_PKCS73223261922.h"
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo1443605387.h"
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData2656813772.h"
#include "Mono_Security_Mono_Security_Cryptography_ARC4Manag3379271383.h"
#include "Mono_Security_Mono_Security_Cryptography_CryptoCon4146607874.h"
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilde3965881084.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2726199179.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2Manage1421454332.h"
#include "Mono_Security_Mono_Security_Cryptography_MD41888998593.h"
#include "Mono_Security_Mono_Security_Cryptography_MD4Manage2176273562.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS13312870480.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS82103016899.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Priva92917103.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Enc1722354997.h"
#include "Mono_Security_Mono_Security_Cryptography_RC42789934315.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManage3034748747.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManaged108853709.h"
#include "Mono_Security_Mono_Security_X509_SafeBag2166702855.h"
#include "Mono_Security_Mono_Security_X509_PKCS121362584794.h"
#include "Mono_Security_Mono_Security_X509_PKCS12_DeriveByte1740753016.h"
#include "Mono_Security_Mono_Security_X509_X501349661534.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate324051957.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3592472865.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3487770522.h"
#include "Mono_Security_Mono_Security_X509_X509Chain1938971907.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFl2843686920.h"
#include "Mono_Security_Mono_Security_X509_X509Crl1699034837.h"
#include "Mono_Security_Mono_Security_X509_X509Crl_X509CrlEnt743353844.h"
#include "Mono_Security_Mono_Security_X509_X509Extension1439760127.h"
#include "Mono_Security_Mono_Security_X509_X509ExtensionColl1640144839.h"
#include "Mono_Security_Mono_Security_X509_X509Store4028973563.h"
#include "Mono_Security_Mono_Security_X509_X509StoreManager1740460066.h"
#include "Mono_Security_Mono_Security_X509_X509Stores3001420398.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Authori795428182.h"
#include "Mono_Security_Mono_Security_X509_Extensions_BasicC3608227951.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Extend3816993686.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Genera2355256240.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsag530589947.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsa1909787375.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca3880736488.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca3955735183.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Subject604050261.h"
#include "Mono_Security_Mono_Security_Cryptography_HMAC2707728663.h"
#include "Mono_Security_Mono_Security_Cryptography_MD5SHA13340472487.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLeve1706602846.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescr844791462.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Alert3405955216.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlg4212518094.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuit491456551.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui2431504453.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui3273693255.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientCon3002158488.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRec2694504884.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes3468069089.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes3595945587.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTyp859870085.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Context4285182719.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAl954949548.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1820731088.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (XmlTypeMapMemberAnyAttribute_t151515769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (XmlTypeMapMemberNamespaces_t999867419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (XmlTypeMapping_t315595419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[8] = 
{
	XmlTypeMapping_t315595419::get_offset_of_xmlType_7(),
	XmlTypeMapping_t315595419::get_offset_of_xmlTypeNamespace_8(),
	XmlTypeMapping_t315595419::get_offset_of_type_9(),
	XmlTypeMapping_t315595419::get_offset_of_baseMap_10(),
	XmlTypeMapping_t315595419::get_offset_of_multiReferenceType_11(),
	XmlTypeMapping_t315595419::get_offset_of_includeInSchema_12(),
	XmlTypeMapping_t315595419::get_offset_of_isNullable_13(),
	XmlTypeMapping_t315595419::get_offset_of__derivedTypes_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (XmlSerializableMapping_t277695926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[3] = 
{
	XmlSerializableMapping_t277695926::get_offset_of__schema_15(),
	XmlSerializableMapping_t277695926::get_offset_of__schemaType_16(),
	XmlSerializableMapping_t277695926::get_offset_of__schemaTypeName_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (ClassMap_t1647926812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[16] = 
{
	ClassMap_t1647926812::get_offset_of__elements_0(),
	ClassMap_t1647926812::get_offset_of__elementMembers_1(),
	ClassMap_t1647926812::get_offset_of__attributeMembers_2(),
	ClassMap_t1647926812::get_offset_of__attributeMembersArray_3(),
	ClassMap_t1647926812::get_offset_of__elementsByIndex_4(),
	ClassMap_t1647926812::get_offset_of__flatLists_5(),
	ClassMap_t1647926812::get_offset_of__allMembers_6(),
	ClassMap_t1647926812::get_offset_of__membersWithDefault_7(),
	ClassMap_t1647926812::get_offset_of__listMembers_8(),
	ClassMap_t1647926812::get_offset_of__defaultAnyElement_9(),
	ClassMap_t1647926812::get_offset_of__defaultAnyAttribute_10(),
	ClassMap_t1647926812::get_offset_of__namespaceDeclarations_11(),
	ClassMap_t1647926812::get_offset_of__xmlTextCollector_12(),
	ClassMap_t1647926812::get_offset_of__returnMember_13(),
	ClassMap_t1647926812::get_offset_of__ignoreMemberNamespace_14(),
	ClassMap_t1647926812::get_offset_of__canBeSimpleType_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (ListMap_t1787375712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[2] = 
{
	ListMap_t1787375712::get_offset_of__itemInfo_0(),
	ListMap_t1787375712::get_offset_of__choiceMember_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (EnumMap_t3161685173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1606[5] = 
{
	EnumMap_t3161685173::get_offset_of__members_0(),
	EnumMap_t3161685173::get_offset_of__isFlags_1(),
	EnumMap_t3161685173::get_offset_of__enumNames_2(),
	EnumMap_t3161685173::get_offset_of__xmlNames_3(),
	EnumMap_t3161685173::get_offset_of__values_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (EnumMapMember_t3814867081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[3] = 
{
	EnumMapMember_t3814867081::get_offset_of__xmlName_0(),
	EnumMapMember_t3814867081::get_offset_of__enumName_1(),
	EnumMapMember_t3814867081::get_offset_of__value_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (XQueryConvert_t3510797773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (XmlNodeChangedEventHandler_t2964483403), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (ValidationEventHandler_t1580700381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (XmlValueGetter_t1685472371), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (UnreferencedObjectEventHandler_t3651715031), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (XmlAttributeEventHandler_t1549396895), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (XmlSerializationCollectionFixupCallback_t3113026122), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (XmlSerializationFixupCallback_t4030450962), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (XmlSerializationReadCallback_t3162327146), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (XmlElementEventHandler_t1680966409), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (XmlNodeEventHandler_t2205849959), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305138), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1619[19] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D18_9(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D36_10(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D37_11(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D38_12(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D39_13(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D40_14(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D41_15(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D42_16(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D43_17(),
	U3CPrivateImplementationDetailsU3E_t1486305138_StaticFields::get_offset_of_U24U24fieldU2D44_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (U24ArrayTypeU24208_t1278838065)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24208_t1278838065 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (U24ArrayTypeU24236_t2844922008)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24236_t2844922008 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (U24ArrayTypeU2472_t3672778801)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2472_t3672778801 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (U24ArrayTypeU241532_t1327816026)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241532_t1327816026 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (U24ArrayTypeU2412_t3672778803)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778803 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (U24ArrayTypeU248_t1957337328)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337328 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (U24ArrayTypeU24256_t2038352955)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352955 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (U24ArrayTypeU241280_t628910058)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t628910058 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (U3CModuleU3E_t3783534216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (Locale_t4255929016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (BigInteger_t925946153), -1, sizeof(BigInteger_t925946153_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1630[4] = 
{
	BigInteger_t925946153::get_offset_of_length_0(),
	BigInteger_t925946153::get_offset_of_data_1(),
	BigInteger_t925946153_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t925946153_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (Sign_t874893936)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[4] = 
{
	Sign_t874893936::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (ModulusRing_t80355992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1632[2] = 
{
	ModulusRing_t80355992::get_offset_of_mod_0(),
	ModulusRing_t80355992::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (Kernel_t1353186456), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (ConfidenceFactor_t1997037802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1634[7] = 
{
	ConfidenceFactor_t1997037802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (PrimalityTests_t3283102399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (PrimeGeneratorBase_t1053438168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (SequentialSearchPrimeGeneratorBase_t463670657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (ASN1_t924533536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1638[3] = 
{
	ASN1_t924533536::get_offset_of_m_nTag_0(),
	ASN1_t924533536::get_offset_of_m_aValue_1(),
	ASN1_t924533536::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (ASN1Convert_t3301846397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (BitConverterLE_t2825370261), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (PKCS7_t3223261923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (ContentInfo_t1443605388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[2] = 
{
	ContentInfo_t1443605388::get_offset_of_contentType_0(),
	ContentInfo_t1443605388::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (EncryptedData_t2656813773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[4] = 
{
	EncryptedData_t2656813773::get_offset_of__version_0(),
	EncryptedData_t2656813773::get_offset_of__content_1(),
	EncryptedData_t2656813773::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t2656813773::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (ARC4Managed_t3379271383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[5] = 
{
	ARC4Managed_t3379271383::get_offset_of_key_12(),
	ARC4Managed_t3379271383::get_offset_of_state_13(),
	ARC4Managed_t3379271383::get_offset_of_x_14(),
	ARC4Managed_t3379271383::get_offset_of_y_15(),
	ARC4Managed_t3379271383::get_offset_of_m_disposed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (CryptoConvert_t4146607875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (KeyBuilder_t3965881085), -1, sizeof(KeyBuilder_t3965881085_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1646[1] = 
{
	KeyBuilder_t3965881085_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (MD2_t726199179), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (MD2Managed_t1421454332), -1, sizeof(MD2Managed_t1421454332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1648[6] = 
{
	MD2Managed_t1421454332::get_offset_of_state_4(),
	MD2Managed_t1421454332::get_offset_of_checksum_5(),
	MD2Managed_t1421454332::get_offset_of_buffer_6(),
	MD2Managed_t1421454332::get_offset_of_count_7(),
	MD2Managed_t1421454332::get_offset_of_x_8(),
	MD2Managed_t1421454332_StaticFields::get_offset_of_PI_SUBST_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (MD4_t1888998593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (MD4Managed_t2176273562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[5] = 
{
	MD4Managed_t2176273562::get_offset_of_state_4(),
	MD4Managed_t2176273562::get_offset_of_buffer_5(),
	MD4Managed_t2176273562::get_offset_of_count_6(),
	MD4Managed_t2176273562::get_offset_of_x_7(),
	MD4Managed_t2176273562::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (PKCS1_t3312870481), -1, sizeof(PKCS1_t3312870481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1651[4] = 
{
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t3312870481_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (PKCS8_t2103016900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (PrivateKeyInfo_t92917104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[4] = 
{
	PrivateKeyInfo_t92917104::get_offset_of__version_0(),
	PrivateKeyInfo_t92917104::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t92917104::get_offset_of__key_2(),
	PrivateKeyInfo_t92917104::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (EncryptedPrivateKeyInfo_t1722354998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[4] = 
{
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_t1722354998::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (RC4_t2789934315), -1, sizeof(RC4_t2789934315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1655[2] = 
{
	RC4_t2789934315_StaticFields::get_offset_of_s_legalBlockSizes_10(),
	RC4_t2789934315_StaticFields::get_offset_of_s_legalKeySizes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (RSAManaged_t3034748748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[13] = 
{
	RSAManaged_t3034748748::get_offset_of_isCRTpossible_2(),
	RSAManaged_t3034748748::get_offset_of_keyBlinding_3(),
	RSAManaged_t3034748748::get_offset_of_keypairGenerated_4(),
	RSAManaged_t3034748748::get_offset_of_m_disposed_5(),
	RSAManaged_t3034748748::get_offset_of_d_6(),
	RSAManaged_t3034748748::get_offset_of_p_7(),
	RSAManaged_t3034748748::get_offset_of_q_8(),
	RSAManaged_t3034748748::get_offset_of_dp_9(),
	RSAManaged_t3034748748::get_offset_of_dq_10(),
	RSAManaged_t3034748748::get_offset_of_qInv_11(),
	RSAManaged_t3034748748::get_offset_of_n_12(),
	RSAManaged_t3034748748::get_offset_of_e_13(),
	RSAManaged_t3034748748::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (KeyGeneratedEventHandler_t108853710), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (SafeBag_t2166702856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[2] = 
{
	SafeBag_t2166702856::get_offset_of__bagOID_0(),
	SafeBag_t2166702856::get_offset_of__asn1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (PKCS12_t1362584795), -1, sizeof(PKCS12_t1362584795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1659[17] = 
{
	PKCS12_t1362584795_StaticFields::get_offset_of_recommendedIterationCount_0(),
	PKCS12_t1362584795::get_offset_of__password_1(),
	PKCS12_t1362584795::get_offset_of__keyBags_2(),
	PKCS12_t1362584795::get_offset_of__secretBags_3(),
	PKCS12_t1362584795::get_offset_of__certs_4(),
	PKCS12_t1362584795::get_offset_of__keyBagsChanged_5(),
	PKCS12_t1362584795::get_offset_of__secretBagsChanged_6(),
	PKCS12_t1362584795::get_offset_of__certsChanged_7(),
	PKCS12_t1362584795::get_offset_of__iterations_8(),
	PKCS12_t1362584795::get_offset_of__safeBags_9(),
	PKCS12_t1362584795::get_offset_of__rng_10(),
	PKCS12_t1362584795_StaticFields::get_offset_of_password_max_length_11(),
	PKCS12_t1362584795_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_12(),
	PKCS12_t1362584795_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_13(),
	PKCS12_t1362584795_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_14(),
	PKCS12_t1362584795_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_15(),
	PKCS12_t1362584795_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (DeriveBytes_t1740753017), -1, sizeof(DeriveBytes_t1740753017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1660[7] = 
{
	DeriveBytes_t1740753017_StaticFields::get_offset_of_keyDiversifier_0(),
	DeriveBytes_t1740753017_StaticFields::get_offset_of_ivDiversifier_1(),
	DeriveBytes_t1740753017_StaticFields::get_offset_of_macDiversifier_2(),
	DeriveBytes_t1740753017::get_offset_of__hashName_3(),
	DeriveBytes_t1740753017::get_offset_of__iterations_4(),
	DeriveBytes_t1740753017::get_offset_of__password_5(),
	DeriveBytes_t1740753017::get_offset_of__salt_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (X501_t349661535), -1, sizeof(X501_t349661535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1661[15] = 
{
	X501_t349661535_StaticFields::get_offset_of_countryName_0(),
	X501_t349661535_StaticFields::get_offset_of_organizationName_1(),
	X501_t349661535_StaticFields::get_offset_of_organizationalUnitName_2(),
	X501_t349661535_StaticFields::get_offset_of_commonName_3(),
	X501_t349661535_StaticFields::get_offset_of_localityName_4(),
	X501_t349661535_StaticFields::get_offset_of_stateOrProvinceName_5(),
	X501_t349661535_StaticFields::get_offset_of_streetAddress_6(),
	X501_t349661535_StaticFields::get_offset_of_domainComponent_7(),
	X501_t349661535_StaticFields::get_offset_of_userid_8(),
	X501_t349661535_StaticFields::get_offset_of_email_9(),
	X501_t349661535_StaticFields::get_offset_of_dnQualifier_10(),
	X501_t349661535_StaticFields::get_offset_of_title_11(),
	X501_t349661535_StaticFields::get_offset_of_surname_12(),
	X501_t349661535_StaticFields::get_offset_of_givenName_13(),
	X501_t349661535_StaticFields::get_offset_of_initial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (X509Certificate_t324051958), -1, sizeof(X509Certificate_t324051958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1662[26] = 
{
	X509Certificate_t324051958::get_offset_of_decoder_0(),
	X509Certificate_t324051958::get_offset_of_m_encodedcert_1(),
	X509Certificate_t324051958::get_offset_of_m_from_2(),
	X509Certificate_t324051958::get_offset_of_m_until_3(),
	X509Certificate_t324051958::get_offset_of_issuer_4(),
	X509Certificate_t324051958::get_offset_of_m_issuername_5(),
	X509Certificate_t324051958::get_offset_of_m_keyalgo_6(),
	X509Certificate_t324051958::get_offset_of_m_keyalgoparams_7(),
	X509Certificate_t324051958::get_offset_of_subject_8(),
	X509Certificate_t324051958::get_offset_of_m_subject_9(),
	X509Certificate_t324051958::get_offset_of_m_publickey_10(),
	X509Certificate_t324051958::get_offset_of_signature_11(),
	X509Certificate_t324051958::get_offset_of_m_signaturealgo_12(),
	X509Certificate_t324051958::get_offset_of_m_signaturealgoparams_13(),
	X509Certificate_t324051958::get_offset_of_certhash_14(),
	X509Certificate_t324051958::get_offset_of__rsa_15(),
	X509Certificate_t324051958::get_offset_of__dsa_16(),
	X509Certificate_t324051958::get_offset_of_version_17(),
	X509Certificate_t324051958::get_offset_of_serialnumber_18(),
	X509Certificate_t324051958::get_offset_of_issuerUniqueID_19(),
	X509Certificate_t324051958::get_offset_of_subjectUniqueID_20(),
	X509Certificate_t324051958::get_offset_of_extensions_21(),
	X509Certificate_t324051958_StaticFields::get_offset_of_encoding_error_22(),
	X509Certificate_t324051958_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_23(),
	X509Certificate_t324051958_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_24(),
	X509Certificate_t324051958_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (X509CertificateCollection_t3592472866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (X509CertificateEnumerator_t3487770523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[1] = 
{
	X509CertificateEnumerator_t3487770523::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (X509Chain_t1938971908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[5] = 
{
	X509Chain_t1938971908::get_offset_of_roots_0(),
	X509Chain_t1938971908::get_offset_of_certs_1(),
	X509Chain_t1938971908::get_offset_of__root_2(),
	X509Chain_t1938971908::get_offset_of__chain_3(),
	X509Chain_t1938971908::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (X509ChainStatusFlags_t2843686920)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1666[8] = 
{
	X509ChainStatusFlags_t2843686920::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (X509Crl_t1699034837), -1, sizeof(X509Crl_t1699034837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1667[11] = 
{
	X509Crl_t1699034837::get_offset_of_issuer_0(),
	X509Crl_t1699034837::get_offset_of_version_1(),
	X509Crl_t1699034837::get_offset_of_thisUpdate_2(),
	X509Crl_t1699034837::get_offset_of_nextUpdate_3(),
	X509Crl_t1699034837::get_offset_of_entries_4(),
	X509Crl_t1699034837::get_offset_of_signatureOID_5(),
	X509Crl_t1699034837::get_offset_of_signature_6(),
	X509Crl_t1699034837::get_offset_of_extensions_7(),
	X509Crl_t1699034837::get_offset_of_encoded_8(),
	X509Crl_t1699034837::get_offset_of_hash_value_9(),
	X509Crl_t1699034837_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (X509CrlEntry_t743353844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[3] = 
{
	X509CrlEntry_t743353844::get_offset_of_sn_0(),
	X509CrlEntry_t743353844::get_offset_of_revocationDate_1(),
	X509CrlEntry_t743353844::get_offset_of_extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (X509Extension_t1439760128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[3] = 
{
	X509Extension_t1439760128::get_offset_of_extnOid_0(),
	X509Extension_t1439760128::get_offset_of_extnCritical_1(),
	X509Extension_t1439760128::get_offset_of_extnValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (X509ExtensionCollection_t1640144840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[1] = 
{
	X509ExtensionCollection_t1640144840::get_offset_of_readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (X509Store_t4028973564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[5] = 
{
	X509Store_t4028973564::get_offset_of__storePath_0(),
	X509Store_t4028973564::get_offset_of__certificates_1(),
	X509Store_t4028973564::get_offset_of__crls_2(),
	X509Store_t4028973564::get_offset_of__crl_3(),
	X509Store_t4028973564::get_offset_of__name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (X509StoreManager_t1740460067), -1, sizeof(X509StoreManager_t1740460067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1672[2] = 
{
	X509StoreManager_t1740460067_StaticFields::get_offset_of__userStore_0(),
	X509StoreManager_t1740460067_StaticFields::get_offset_of__machineStore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (X509Stores_t3001420399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[2] = 
{
	X509Stores_t3001420399::get_offset_of__storePath_0(),
	X509Stores_t3001420399::get_offset_of__trusted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (AuthorityKeyIdentifierExtension_t795428182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[1] = 
{
	AuthorityKeyIdentifierExtension_t795428182::get_offset_of_aki_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (BasicConstraintsExtension_t3608227952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[2] = 
{
	BasicConstraintsExtension_t3608227952::get_offset_of_cA_3(),
	BasicConstraintsExtension_t3608227952::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (ExtendedKeyUsageExtension_t3816993686), -1, sizeof(ExtendedKeyUsageExtension_t3816993686_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1676[2] = 
{
	ExtendedKeyUsageExtension_t3816993686::get_offset_of_keyPurpose_3(),
	ExtendedKeyUsageExtension_t3816993686_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (GeneralNames_t2355256240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[5] = 
{
	GeneralNames_t2355256240::get_offset_of_rfc822Name_0(),
	GeneralNames_t2355256240::get_offset_of_dnsName_1(),
	GeneralNames_t2355256240::get_offset_of_directoryNames_2(),
	GeneralNames_t2355256240::get_offset_of_uris_3(),
	GeneralNames_t2355256240::get_offset_of_ipAddr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (KeyUsages_t530589947)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1678[11] = 
{
	KeyUsages_t530589947::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (KeyUsageExtension_t1909787375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[1] = 
{
	KeyUsageExtension_t1909787375::get_offset_of_kubits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (NetscapeCertTypeExtension_t3880736488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[1] = 
{
	NetscapeCertTypeExtension_t3880736488::get_offset_of_ctbits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (CertTypes_t3955735183)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1681[8] = 
{
	CertTypes_t3955735183::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (SubjectAltNameExtension_t604050261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[1] = 
{
	SubjectAltNameExtension_t604050261::get_offset_of__names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (HMAC_t2707728663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[4] = 
{
	HMAC_t2707728663::get_offset_of_hash_5(),
	HMAC_t2707728663::get_offset_of_hashing_6(),
	HMAC_t2707728663::get_offset_of_innerPad_7(),
	HMAC_t2707728663::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (MD5SHA1_t3340472487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[3] = 
{
	MD5SHA1_t3340472487::get_offset_of_md5_4(),
	MD5SHA1_t3340472487::get_offset_of_sha_5(),
	MD5SHA1_t3340472487::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (AlertLevel_t1706602846)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1685[3] = 
{
	AlertLevel_t1706602846::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (AlertDescription_t844791462)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1686[25] = 
{
	AlertDescription_t844791462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (Alert_t3405955216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[2] = 
{
	Alert_t3405955216::get_offset_of_level_0(),
	Alert_t3405955216::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (CipherAlgorithmType_t4212518094)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1688[8] = 
{
	CipherAlgorithmType_t4212518094::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (CipherSuite_t491456551), -1, sizeof(CipherSuite_t491456551_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1689[21] = 
{
	CipherSuite_t491456551_StaticFields::get_offset_of_EmptyArray_0(),
	CipherSuite_t491456551::get_offset_of_code_1(),
	CipherSuite_t491456551::get_offset_of_name_2(),
	CipherSuite_t491456551::get_offset_of_cipherAlgorithmType_3(),
	CipherSuite_t491456551::get_offset_of_hashAlgorithmType_4(),
	CipherSuite_t491456551::get_offset_of_exchangeAlgorithmType_5(),
	CipherSuite_t491456551::get_offset_of_isExportable_6(),
	CipherSuite_t491456551::get_offset_of_cipherMode_7(),
	CipherSuite_t491456551::get_offset_of_keyMaterialSize_8(),
	CipherSuite_t491456551::get_offset_of_keyBlockSize_9(),
	CipherSuite_t491456551::get_offset_of_expandedKeyMaterialSize_10(),
	CipherSuite_t491456551::get_offset_of_effectiveKeyBits_11(),
	CipherSuite_t491456551::get_offset_of_ivSize_12(),
	CipherSuite_t491456551::get_offset_of_blockSize_13(),
	CipherSuite_t491456551::get_offset_of_context_14(),
	CipherSuite_t491456551::get_offset_of_encryptionAlgorithm_15(),
	CipherSuite_t491456551::get_offset_of_encryptionCipher_16(),
	CipherSuite_t491456551::get_offset_of_decryptionAlgorithm_17(),
	CipherSuite_t491456551::get_offset_of_decryptionCipher_18(),
	CipherSuite_t491456551::get_offset_of_clientHMAC_19(),
	CipherSuite_t491456551::get_offset_of_serverHMAC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (CipherSuiteCollection_t2431504453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[2] = 
{
	CipherSuiteCollection_t2431504453::get_offset_of_cipherSuites_0(),
	CipherSuiteCollection_t2431504453::get_offset_of_protocol_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (CipherSuiteFactory_t3273693255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (ClientContext_t3002158488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[2] = 
{
	ClientContext_t3002158488::get_offset_of_sslStream_30(),
	ClientContext_t3002158488::get_offset_of_clientHelloProtocol_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (ClientRecordProtocol_t2694504884), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (ClientSessionInfo_t3468069089), -1, sizeof(ClientSessionInfo_t3468069089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1694[6] = 
{
	ClientSessionInfo_t3468069089_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t3468069089::get_offset_of_disposed_1(),
	ClientSessionInfo_t3468069089::get_offset_of_validuntil_2(),
	ClientSessionInfo_t3468069089::get_offset_of_host_3(),
	ClientSessionInfo_t3468069089::get_offset_of_sid_4(),
	ClientSessionInfo_t3468069089::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (ClientSessionCache_t3595945587), -1, sizeof(ClientSessionCache_t3595945587_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1695[2] = 
{
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t3595945587_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (ContentType_t859870085)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1696[5] = 
{
	ContentType_t859870085::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (Context_t4285182719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[30] = 
{
	Context_t4285182719::get_offset_of_securityProtocol_0(),
	Context_t4285182719::get_offset_of_sessionId_1(),
	Context_t4285182719::get_offset_of_compressionMethod_2(),
	Context_t4285182719::get_offset_of_serverSettings_3(),
	Context_t4285182719::get_offset_of_clientSettings_4(),
	Context_t4285182719::get_offset_of_current_5(),
	Context_t4285182719::get_offset_of_negotiating_6(),
	Context_t4285182719::get_offset_of_read_7(),
	Context_t4285182719::get_offset_of_write_8(),
	Context_t4285182719::get_offset_of_supportedCiphers_9(),
	Context_t4285182719::get_offset_of_lastHandshakeMsg_10(),
	Context_t4285182719::get_offset_of_handshakeState_11(),
	Context_t4285182719::get_offset_of_abbreviatedHandshake_12(),
	Context_t4285182719::get_offset_of_receivedConnectionEnd_13(),
	Context_t4285182719::get_offset_of_sentConnectionEnd_14(),
	Context_t4285182719::get_offset_of_protocolNegotiated_15(),
	Context_t4285182719::get_offset_of_writeSequenceNumber_16(),
	Context_t4285182719::get_offset_of_readSequenceNumber_17(),
	Context_t4285182719::get_offset_of_clientRandom_18(),
	Context_t4285182719::get_offset_of_serverRandom_19(),
	Context_t4285182719::get_offset_of_randomCS_20(),
	Context_t4285182719::get_offset_of_randomSC_21(),
	Context_t4285182719::get_offset_of_masterSecret_22(),
	Context_t4285182719::get_offset_of_clientWriteKey_23(),
	Context_t4285182719::get_offset_of_serverWriteKey_24(),
	Context_t4285182719::get_offset_of_clientWriteIV_25(),
	Context_t4285182719::get_offset_of_serverWriteIV_26(),
	Context_t4285182719::get_offset_of_handshakeMessages_27(),
	Context_t4285182719::get_offset_of_random_28(),
	Context_t4285182719::get_offset_of_recordProtocol_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (ExchangeAlgorithmType_t954949548)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1698[6] = 
{
	ExchangeAlgorithmType_t954949548::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (HandshakeState_t1820731088)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[4] = 
{
	HandshakeState_t1820731088::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
