﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,System.Object>
struct NavigationController_2_t577351051;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,System.Object>::.ctor()
extern "C"  void NavigationController_2__ctor_m1930283657_gshared (NavigationController_2_t577351051 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m1930283657(__this, method) ((  void (*) (NavigationController_2_t577351051 *, const MethodInfo*))NavigationController_2__ctor_m1930283657_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,System.Object>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1631311415_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m1631311415(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t577351051 *, Il2CppObject *, const MethodInfo*))NavigationController_2_SetupStartScreen_m1631311415_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,System.Object>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1673772046_gshared (NavigationController_2_t577351051 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m1673772046(__this, method) ((  void (*) (NavigationController_2_t577351051 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m1673772046_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,System.Object>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m871857639_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m871857639(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t577351051 *, Il2CppObject *, const MethodInfo*))NavigationController_2_NavigateForwards_m871857639_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,System.Object>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m284324339_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m284324339(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t577351051 *, Il2CppObject *, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m284324339_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,System.Object>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m189672568_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m189672568(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t577351051 *, Il2CppObject *, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m189672568_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,System.Object>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1853810855_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m1853810855(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t577351051 *, Il2CppObject *, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1853810855_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,System.Object>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2840335907_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m2840335907(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t577351051 *, Il2CppObject *, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2840335907_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,System.Object>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m3757624735_gshared (NavigationController_2_t577351051 * __this, Il2CppObject * ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m3757624735(__this, ___screen0, method) ((  void (*) (NavigationController_2_t577351051 *, Il2CppObject *, const MethodInfo*))NavigationController_2_PopulateTitleBar_m3757624735_gshared)(__this, ___screen0, method)
