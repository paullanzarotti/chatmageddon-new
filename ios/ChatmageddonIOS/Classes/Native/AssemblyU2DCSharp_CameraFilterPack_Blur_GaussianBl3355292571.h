﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Blur_GaussianBlur
struct  CameraFilterPack_Blur_GaussianBlur_t3355292571  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Blur_GaussianBlur::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_GaussianBlur::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Blur_GaussianBlur::Size
	float ___Size_4;
	// UnityEngine.Vector4 CameraFilterPack_Blur_GaussianBlur::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_Blur_GaussianBlur::SCMaterial
	Material_t193706927 * ___SCMaterial_6;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_GaussianBlur_t3355292571, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_GaussianBlur_t3355292571, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_Size_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_GaussianBlur_t3355292571, ___Size_4)); }
	inline float get_Size_4() const { return ___Size_4; }
	inline float* get_address_of_Size_4() { return &___Size_4; }
	inline void set_Size_4(float value)
	{
		___Size_4 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_GaussianBlur_t3355292571, ___ScreenResolution_5)); }
	inline Vector4_t2243707581  get_ScreenResolution_5() const { return ___ScreenResolution_5; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_5() { return &___ScreenResolution_5; }
	inline void set_ScreenResolution_5(Vector4_t2243707581  value)
	{
		___ScreenResolution_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_GaussianBlur_t3355292571, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}
};

struct CameraFilterPack_Blur_GaussianBlur_t3355292571_StaticFields
{
public:
	// System.Single CameraFilterPack_Blur_GaussianBlur::ChangeSize
	float ___ChangeSize_7;

public:
	inline static int32_t get_offset_of_ChangeSize_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Blur_GaussianBlur_t3355292571_StaticFields, ___ChangeSize_7)); }
	inline float get_ChangeSize_7() const { return ___ChangeSize_7; }
	inline float* get_address_of_ChangeSize_7() { return &___ChangeSize_7; }
	inline void set_ChangeSize_7(float value)
	{
		___ChangeSize_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
