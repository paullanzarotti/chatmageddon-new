﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AddressBook/<LoadIosContacts>c__Iterator0
struct U3CLoadIosContactsU3Ec__Iterator0_t3073646581;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AddressBook/<LoadIosContacts>c__Iterator0::.ctor()
extern "C"  void U3CLoadIosContactsU3Ec__Iterator0__ctor_m909864726 (U3CLoadIosContactsU3Ec__Iterator0_t3073646581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AddressBook/<LoadIosContacts>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadIosContactsU3Ec__Iterator0_MoveNext_m2599228478 (U3CLoadIosContactsU3Ec__Iterator0_t3073646581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AddressBook/<LoadIosContacts>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadIosContactsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1847954448 (U3CLoadIosContactsU3Ec__Iterator0_t3073646581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AddressBook/<LoadIosContacts>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadIosContactsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m361605112 (U3CLoadIosContactsU3Ec__Iterator0_t3073646581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook/<LoadIosContacts>c__Iterator0::Dispose()
extern "C"  void U3CLoadIosContactsU3Ec__Iterator0_Dispose_m3465324303 (U3CLoadIosContactsU3Ec__Iterator0_t3073646581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AddressBook/<LoadIosContacts>c__Iterator0::Reset()
extern "C"  void U3CLoadIosContactsU3Ec__Iterator0_Reset_m1510336353 (U3CLoadIosContactsU3Ec__Iterator0_t3073646581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
