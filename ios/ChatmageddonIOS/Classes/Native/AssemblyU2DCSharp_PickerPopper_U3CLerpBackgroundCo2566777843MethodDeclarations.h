﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickerPopper/<LerpBackgroundColor>c__Iterator3
struct U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PickerPopper/<LerpBackgroundColor>c__Iterator3::.ctor()
extern "C"  void U3CLerpBackgroundColorU3Ec__Iterator3__ctor_m1214240046 (U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickerPopper/<LerpBackgroundColor>c__Iterator3::MoveNext()
extern "C"  bool U3CLerpBackgroundColorU3Ec__Iterator3_MoveNext_m152423922 (U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<LerpBackgroundColor>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLerpBackgroundColorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1253025786 (U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<LerpBackgroundColor>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLerpBackgroundColorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3974539650 (U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<LerpBackgroundColor>c__Iterator3::Dispose()
extern "C"  void U3CLerpBackgroundColorU3Ec__Iterator3_Dispose_m305522745 (U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<LerpBackgroundColor>c__Iterator3::Reset()
extern "C"  void U3CLerpBackgroundColorU3Ec__Iterator3_Reset_m2014262159 (U3CLerpBackgroundColorU3Ec__Iterator3_t2566777843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
