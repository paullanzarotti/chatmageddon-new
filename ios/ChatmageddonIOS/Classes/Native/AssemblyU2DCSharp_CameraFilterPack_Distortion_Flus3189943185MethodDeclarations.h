﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Flush
struct CameraFilterPack_Distortion_Flush_t3189943185;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Flush::.ctor()
extern "C"  void CameraFilterPack_Distortion_Flush__ctor_m1216516026 (CameraFilterPack_Distortion_Flush_t3189943185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Flush::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Flush_get_material_m563855113 (CameraFilterPack_Distortion_Flush_t3189943185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::Start()
extern "C"  void CameraFilterPack_Distortion_Flush_Start_m3324239106 (CameraFilterPack_Distortion_Flush_t3189943185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Flush_OnRenderImage_m1387633170 (CameraFilterPack_Distortion_Flush_t3189943185 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Flush_OnValidate_m1522453935 (CameraFilterPack_Distortion_Flush_t3189943185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::Update()
extern "C"  void CameraFilterPack_Distortion_Flush_Update_m2026445501 (CameraFilterPack_Distortion_Flush_t3189943185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Flush_OnDisable_m3362344785 (CameraFilterPack_Distortion_Flush_t3189943185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
