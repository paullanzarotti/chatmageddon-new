﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackInventoryNavScreen
struct AttackInventoryNavScreen_t33214553;
// Missile
struct Missile_t813944928;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PurchaseableItem
struct PurchaseableItem_t3351122996;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_PurchaseableItem3351122996.h"

// System.Void AttackInventoryNavScreen::.ctor()
extern "C"  void AttackInventoryNavScreen__ctor_m1441986532 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::Start()
extern "C"  void AttackInventoryNavScreen_Start_m3257998724 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::UIClosing()
extern "C"  void AttackInventoryNavScreen_UIClosing_m3394242595 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackInventoryNavScreen_ScreenLoad_m4062543624 (AttackInventoryNavScreen_t33214553 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackInventoryNavScreen_ScreenUnload_m1443174064 (AttackInventoryNavScreen_t33214553 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackInventoryNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackInventoryNavScreen_ValidateScreenNavigation_m2981806347 (AttackInventoryNavScreen_t33214553 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::LoadMissileInventory()
extern "C"  void AttackInventoryNavScreen_LoadMissileInventory_m3402843650 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackInventoryNavScreen::MissileItemsContains(Missile)
extern "C"  bool AttackInventoryNavScreen_MissileItemsContains_m2474956221 (AttackInventoryNavScreen_t33214553 * __this, Missile_t813944928 * ___missile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::LoadDefaultItem()
extern "C"  void AttackInventoryNavScreen_LoadDefaultItem_m3021332194 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AttackInventoryNavScreen::LoadDefaultModel()
extern "C"  Il2CppObject * AttackInventoryNavScreen_LoadDefaultModel_m1829295640 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::OnDefaultItemShow(System.Boolean)
extern "C"  void AttackInventoryNavScreen_OnDefaultItemShow_m1797332495 (AttackInventoryNavScreen_t33214553 * __this, bool ___closed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::LoadNextItem()
extern "C"  void AttackInventoryNavScreen_LoadNextItem_m595100384 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::LoadPreviousItem()
extern "C"  void AttackInventoryNavScreen_LoadPreviousItem_m4146097068 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::TweenItem(System.Boolean)
extern "C"  void AttackInventoryNavScreen_TweenItem_m2406600987 (AttackInventoryNavScreen_t33214553 * __this, bool ___right0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::OnTweenItemComplete()
extern "C"  void AttackInventoryNavScreen_OnTweenItemComplete_m1605962292 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::LoadItem(PurchaseableItem)
extern "C"  void AttackInventoryNavScreen_LoadItem_m4008000999 (AttackInventoryNavScreen_t33214553 * __this, PurchaseableItem_t3351122996 * ___missile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::AlphaTweenLabels(System.Boolean)
extern "C"  void AttackInventoryNavScreen_AlphaTweenLabels_m2866927791 (AttackInventoryNavScreen_t33214553 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::SetAmountLabel(System.Boolean)
extern "C"  void AttackInventoryNavScreen_SetAmountLabel_m1368393023 (AttackInventoryNavScreen_t33214553 * __this, bool ___inifinity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::SetItemLockedData()
extern "C"  void AttackInventoryNavScreen_SetItemLockedData_m1557232509 (AttackInventoryNavScreen_t33214553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::SetLightActive(System.Boolean)
extern "C"  void AttackInventoryNavScreen_SetLightActive_m993500879 (AttackInventoryNavScreen_t33214553 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryNavScreen::ShowItemDescription(System.Boolean)
extern "C"  void AttackInventoryNavScreen_ShowItemDescription_m2663537131 (AttackInventoryNavScreen_t33214553 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
