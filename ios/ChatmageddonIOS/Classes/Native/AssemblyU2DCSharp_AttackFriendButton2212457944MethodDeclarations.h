﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackFriendButton
struct AttackFriendButton_t2212457944;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackFriendButton::.ctor()
extern "C"  void AttackFriendButton__ctor_m3032723855 (AttackFriendButton_t2212457944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackFriendButton::OnButtonClick()
extern "C"  void AttackFriendButton_OnButtonClick_m1984634620 (AttackFriendButton_t2212457944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
