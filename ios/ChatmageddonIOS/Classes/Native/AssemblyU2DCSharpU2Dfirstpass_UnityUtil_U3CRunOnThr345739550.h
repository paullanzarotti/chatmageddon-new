﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityUtil/<RunOnThreadPool>c__AnonStorey1
struct  U3CRunOnThreadPoolU3Ec__AnonStorey1_t345739550  : public Il2CppObject
{
public:
	// System.Action UnityUtil/<RunOnThreadPool>c__AnonStorey1::runnable
	Action_t3226471752 * ___runnable_0;

public:
	inline static int32_t get_offset_of_runnable_0() { return static_cast<int32_t>(offsetof(U3CRunOnThreadPoolU3Ec__AnonStorey1_t345739550, ___runnable_0)); }
	inline Action_t3226471752 * get_runnable_0() const { return ___runnable_0; }
	inline Action_t3226471752 ** get_address_of_runnable_0() { return &___runnable_0; }
	inline void set_runnable_0(Action_t3226471752 * value)
	{
		___runnable_0 = value;
		Il2CppCodeGenWriteBarrier(&___runnable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
