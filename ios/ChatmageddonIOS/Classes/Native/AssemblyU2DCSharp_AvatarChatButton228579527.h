﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AvatarChatUI
struct AvatarChatUI_t2524884989;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AvatarChatButton
struct  AvatarChatButton_t228579527  : public SFXButton_t792651341
{
public:
	// AvatarChatUI AvatarChatButton::chatUI
	AvatarChatUI_t2524884989 * ___chatUI_5;

public:
	inline static int32_t get_offset_of_chatUI_5() { return static_cast<int32_t>(offsetof(AvatarChatButton_t228579527, ___chatUI_5)); }
	inline AvatarChatUI_t2524884989 * get_chatUI_5() const { return ___chatUI_5; }
	inline AvatarChatUI_t2524884989 ** get_address_of_chatUI_5() { return &___chatUI_5; }
	inline void set_chatUI_5(AvatarChatUI_t2524884989 * value)
	{
		___chatUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___chatUI_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
