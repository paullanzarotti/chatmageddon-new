﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType IComparable_1_t991353265_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t991353265_0_0_0_Types[] = { &IComparable_1_t991353265_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t991353265_0_0_0 = { 1, GenInst_IComparable_1_t991353265_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1363496211_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1363496211_0_0_0_Types[] = { &IEquatable_1_t1363496211_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1363496211_0_0_0 = { 1, GenInst_IEquatable_1_t1363496211_0_0_0_Types };
extern const Il2CppType ValueType_t3507792607_0_0_0;
static const Il2CppType* GenInst_ValueType_t3507792607_0_0_0_Types[] = { &ValueType_t3507792607_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3507792607_0_0_0 = { 1, GenInst_ValueType_t3507792607_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType IFormattable_t1523031934_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1523031934_0_0_0_Types[] = { &IFormattable_t1523031934_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1523031934_0_0_0 = { 1, GenInst_IFormattable_t1523031934_0_0_0_Types };
extern const Il2CppType IComparable_1_t3903716671_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3903716671_0_0_0_Types[] = { &IComparable_1_t3903716671_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3903716671_0_0_0 = { 1, GenInst_IComparable_1_t3903716671_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4275859617_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4275859617_0_0_0_Types[] = { &IEquatable_1_t4275859617_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4275859617_0_0_0 = { 1, GenInst_IEquatable_1_t4275859617_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType IComparable_1_t1614887608_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1614887608_0_0_0_Types[] = { &IComparable_1_t1614887608_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1614887608_0_0_0 = { 1, GenInst_IComparable_1_t1614887608_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1987030554_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1987030554_0_0_0_Types[] = { &IEquatable_1_t1987030554_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1987030554_0_0_0 = { 1, GenInst_IEquatable_1_t1987030554_0_0_0_Types };
extern const Il2CppType IComparable_1_t3981521244_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3981521244_0_0_0_Types[] = { &IComparable_1_t3981521244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3981521244_0_0_0 = { 1, GenInst_IComparable_1_t3981521244_0_0_0_Types };
extern const Il2CppType IEquatable_1_t58696894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t58696894_0_0_0_Types[] = { &IEquatable_1_t58696894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t58696894_0_0_0 = { 1, GenInst_IEquatable_1_t58696894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1219976363_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1219976363_0_0_0_Types[] = { &IComparable_1_t1219976363_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1219976363_0_0_0 = { 1, GenInst_IComparable_1_t1219976363_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1592119309_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1592119309_0_0_0_Types[] = { &IEquatable_1_t1592119309_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1592119309_0_0_0 = { 1, GenInst_IEquatable_1_t1592119309_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType IComparable_1_t3908349155_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3908349155_0_0_0_Types[] = { &IComparable_1_t3908349155_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3908349155_0_0_0 = { 1, GenInst_IComparable_1_t3908349155_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4280492101_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4280492101_0_0_0_Types[] = { &IEquatable_1_t4280492101_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4280492101_0_0_0 = { 1, GenInst_IEquatable_1_t4280492101_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
extern const Il2CppType IComparable_1_t2818721834_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2818721834_0_0_0_Types[] = { &IComparable_1_t2818721834_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2818721834_0_0_0 = { 1, GenInst_IComparable_1_t2818721834_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3190864780_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3190864780_0_0_0_Types[] = { &IEquatable_1_t3190864780_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3190864780_0_0_0 = { 1, GenInst_IEquatable_1_t3190864780_0_0_0_Types };
extern const Il2CppType IComparable_1_t446068841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t446068841_0_0_0_Types[] = { &IComparable_1_t446068841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t446068841_0_0_0 = { 1, GenInst_IComparable_1_t446068841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t818211787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t818211787_0_0_0_Types[] = { &IEquatable_1_t818211787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t818211787_0_0_0 = { 1, GenInst_IEquatable_1_t818211787_0_0_0_Types };
extern const Il2CppType IComparable_1_t1578117841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1578117841_0_0_0_Types[] = { &IComparable_1_t1578117841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1578117841_0_0_0 = { 1, GenInst_IComparable_1_t1578117841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1950260787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1950260787_0_0_0_Types[] = { &IEquatable_1_t1950260787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1950260787_0_0_0 = { 1, GenInst_IEquatable_1_t1950260787_0_0_0_Types };
extern const Il2CppType IComparable_1_t2286256772_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2286256772_0_0_0_Types[] = { &IComparable_1_t2286256772_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2286256772_0_0_0 = { 1, GenInst_IComparable_1_t2286256772_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2658399718_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2658399718_0_0_0_Types[] = { &IEquatable_1_t2658399718_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2658399718_0_0_0 = { 1, GenInst_IEquatable_1_t2658399718_0_0_0_Types };
extern const Il2CppType IComparable_1_t2740917260_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2740917260_0_0_0_Types[] = { &IComparable_1_t2740917260_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2740917260_0_0_0 = { 1, GenInst_IComparable_1_t2740917260_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3113060206_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3113060206_0_0_0_Types[] = { &IEquatable_1_t3113060206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3113060206_0_0_0 = { 1, GenInst_IEquatable_1_t3113060206_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t2430923913_0_0_0;
static const Il2CppType* GenInst__EventInfo_t2430923913_0_0_0_Types[] = { &_EventInfo_t2430923913_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t2430923913_0_0_0 = { 1, GenInst__EventInfo_t2430923913_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
extern const Il2CppType Link_t2723257478_0_0_0;
static const Il2CppType* GenInst_Link_t2723257478_0_0_0_Types[] = { &Link_t2723257478_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType UriScheme_t683497865_0_0_0;
static const Il2CppType* GenInst_UriScheme_t683497865_0_0_0_Types[] = { &UriScheme_t683497865_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t683497865_0_0_0 = { 1, GenInst_UriScheme_t683497865_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType Assembly_t4268412390_0_0_0;
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0 = { 1, GenInst_Assembly_t4268412390_0_0_0_Types };
extern const Il2CppType _Assembly_t2937922309_0_0_0;
static const Il2CppType* GenInst__Assembly_t2937922309_0_0_0_Types[] = { &_Assembly_t2937922309_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t2937922309_0_0_0 = { 1, GenInst__Assembly_t2937922309_0_0_0_Types };
extern const Il2CppType IEvidenceFactory_t1747265420_0_0_0;
static const Il2CppType* GenInst_IEvidenceFactory_t1747265420_0_0_0_Types[] = { &IEvidenceFactory_t1747265420_0_0_0 };
extern const Il2CppGenericInst GenInst_IEvidenceFactory_t1747265420_0_0_0 = { 1, GenInst_IEvidenceFactory_t1747265420_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType Guid_t2533601593_0_0_0;
static const Il2CppType* GenInst_Guid_t2533601593_0_0_0_Types[] = { &Guid_t2533601593_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2533601593_0_0_0 = { 1, GenInst_Guid_t2533601593_0_0_0_Types };
extern const Il2CppType IComparable_1_t1362446645_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1362446645_0_0_0_Types[] = { &IComparable_1_t1362446645_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1362446645_0_0_0 = { 1, GenInst_IComparable_1_t1362446645_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1734589591_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1734589591_0_0_0_Types[] = { &IEquatable_1_t1734589591_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1734589591_0_0_0 = { 1, GenInst_IEquatable_1_t1734589591_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType TermInfoStrings_t1425267120_0_0_0;
static const Il2CppType* GenInst_TermInfoStrings_t1425267120_0_0_0_Types[] = { &TermInfoStrings_t1425267120_0_0_0 };
extern const Il2CppGenericInst GenInst_TermInfoStrings_t1425267120_0_0_0 = { 1, GenInst_TermInfoStrings_t1425267120_0_0_0_Types };
extern const Il2CppType Enum_t2459695545_0_0_0;
static const Il2CppType* GenInst_Enum_t2459695545_0_0_0_Types[] = { &Enum_t2459695545_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2459695545_0_0_0 = { 1, GenInst_Enum_t2459695545_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType Slot_t2022531261_0_0_0;
static const Il2CppType* GenInst_Slot_t2022531261_0_0_0_Types[] = { &Slot_t2022531261_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
extern const Il2CppType Slot_t2267560602_0_0_0;
static const Il2CppType* GenInst_Slot_t2267560602_0_0_0_Types[] = { &Slot_t2267560602_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Calendar_t585061108_0_0_0;
static const Il2CppType* GenInst_Calendar_t585061108_0_0_0_Types[] = { &Calendar_t585061108_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
extern const Il2CppType CultureInfo_t3500843524_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t3500843524_0_0_0_Types[] = { &CultureInfo_t3500843524_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t3500843524_0_0_0 = { 1, GenInst_CultureInfo_t3500843524_0_0_0_Types };
extern const Il2CppType IFormatProvider_t2849799027_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t2849799027_0_0_0_Types[] = { &IFormatProvider_t2849799027_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t2849799027_0_0_0 = { 1, GenInst_IFormatProvider_t2849799027_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType MonoResource_t3127387157_0_0_0;
static const Il2CppType* GenInst_MonoResource_t3127387157_0_0_0_Types[] = { &MonoResource_t3127387157_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t3127387157_0_0_0 = { 1, GenInst_MonoResource_t3127387157_0_0_0_Types };
extern const Il2CppType RefEmitPermissionSet_t2708608433_0_0_0;
static const Il2CppType* GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types[] = { &RefEmitPermissionSet_t2708608433_0_0_0 };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t2708608433_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType LocalBuilder_t2116499186_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t2116499186_0_0_0_Types[] = { &LocalBuilder_t2116499186_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t2116499186_0_0_0 = { 1, GenInst_LocalBuilder_t2116499186_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t61912499_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t61912499_0_0_0_Types[] = { &_LocalBuilder_t61912499_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t61912499_0_0_0 = { 1, GenInst__LocalBuilder_t61912499_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t1749284021_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t1749284021_0_0_0_Types[] = { &LocalVariableInfo_t1749284021_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t1749284021_0_0_0 = { 1, GenInst_LocalVariableInfo_t1749284021_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { &PropertyBuilder_t3694255912_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { &_PropertyBuilder_t3341912621_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { &ResourceInfo_t3933049236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { &ResourceCacheItem_t333236149_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IComparable_1_t2525044892_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2525044892_0_0_0_Types[] = { &IComparable_1_t2525044892_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2525044892_0_0_0 = { 1, GenInst_IComparable_1_t2525044892_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2897187838_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2897187838_0_0_0_Types[] = { &IEquatable_1_t2897187838_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2897187838_0_0_0 = { 1, GenInst_IEquatable_1_t2897187838_0_0_0_Types };
extern const Il2CppType IComparable_1_t2556540300_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2556540300_0_0_0_Types[] = { &IComparable_1_t2556540300_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2556540300_0_0_0 = { 1, GenInst_IComparable_1_t2556540300_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2928683246_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2928683246_0_0_0_Types[] = { &IEquatable_1_t2928683246_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2928683246_0_0_0 = { 1, GenInst_IEquatable_1_t2928683246_0_0_0_Types };
extern const Il2CppType IComparable_1_t967130876_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t967130876_0_0_0_Types[] = { &IComparable_1_t967130876_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t967130876_0_0_0 = { 1, GenInst_IComparable_1_t967130876_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1339273822_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1339273822_0_0_0_Types[] = { &IEquatable_1_t1339273822_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1339273822_0_0_0 = { 1, GenInst_IEquatable_1_t1339273822_0_0_0_Types };
extern const Il2CppType TypeTag_t141209596_0_0_0;
static const Il2CppType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { &TypeTag_t141209596_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType KeyContainerPermissionAccessEntry_t41069825_0_0_0;
static const Il2CppType* GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0_Types[] = { &KeyContainerPermissionAccessEntry_t41069825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0 = { 1, GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType CodeConnectAccess_t3638993531_0_0_0;
static const Il2CppType* GenInst_CodeConnectAccess_t3638993531_0_0_0_Types[] = { &CodeConnectAccess_t3638993531_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeConnectAccess_t3638993531_0_0_0 = { 1, GenInst_CodeConnectAccess_t3638993531_0_0_0_Types };
extern const Il2CppType EncodingInfo_t2546178797_0_0_0;
static const Il2CppType* GenInst_EncodingInfo_t2546178797_0_0_0_Types[] = { &EncodingInfo_t2546178797_0_0_0 };
extern const Il2CppGenericInst GenInst_EncodingInfo_t2546178797_0_0_0 = { 1, GenInst_EncodingInfo_t2546178797_0_0_0_Types };
extern const Il2CppType WaitHandle_t677569169_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t677569169_0_0_0_Types[] = { &WaitHandle_t677569169_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t677569169_0_0_0 = { 1, GenInst_WaitHandle_t677569169_0_0_0_Types };
extern const Il2CppType IDisposable_t2427283555_0_0_0;
static const Il2CppType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { &IDisposable_t2427283555_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1285298191_0_0_0_Types[] = { &MarshalByRefObject_t1285298191_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1285298191_0_0_0 = { 1, GenInst_MarshalByRefObject_t1285298191_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType XmlSchemaAttribute_t4015859774_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types[] = { &XmlSchemaAttribute_t4015859774_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAttribute_t4015859774_0_0_0 = { 1, GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types };
extern const Il2CppType XmlSchemaAnnotated_t2082486936_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types[] = { &XmlSchemaAnnotated_t2082486936_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAnnotated_t2082486936_0_0_0 = { 1, GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types };
extern const Il2CppType XmlSchemaObject_t2050913741_0_0_0;
static const Il2CppType* GenInst_XmlSchemaObject_t2050913741_0_0_0_Types[] = { &XmlSchemaObject_t2050913741_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaObject_t2050913741_0_0_0 = { 1, GenInst_XmlSchemaObject_t2050913741_0_0_0_Types };
extern const Il2CppType XsdIdentityPath_t2037874_0_0_0;
static const Il2CppType* GenInst_XsdIdentityPath_t2037874_0_0_0_Types[] = { &XsdIdentityPath_t2037874_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityPath_t2037874_0_0_0 = { 1, GenInst_XsdIdentityPath_t2037874_0_0_0_Types };
extern const Il2CppType XsdIdentityField_t2563516441_0_0_0;
static const Il2CppType* GenInst_XsdIdentityField_t2563516441_0_0_0_Types[] = { &XsdIdentityField_t2563516441_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityField_t2563516441_0_0_0 = { 1, GenInst_XsdIdentityField_t2563516441_0_0_0_Types };
extern const Il2CppType XsdIdentityStep_t452377251_0_0_0;
static const Il2CppType* GenInst_XsdIdentityStep_t452377251_0_0_0_Types[] = { &XsdIdentityStep_t452377251_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityStep_t452377251_0_0_0 = { 1, GenInst_XsdIdentityStep_t452377251_0_0_0_Types };
extern const Il2CppType XmlSchemaException_t4082200141_0_0_0;
static const Il2CppType* GenInst_XmlSchemaException_t4082200141_0_0_0_Types[] = { &XmlSchemaException_t4082200141_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaException_t4082200141_0_0_0 = { 1, GenInst_XmlSchemaException_t4082200141_0_0_0_Types };
extern const Il2CppType SystemException_t3877406272_0_0_0;
static const Il2CppType* GenInst_SystemException_t3877406272_0_0_0_Types[] = { &SystemException_t3877406272_0_0_0 };
extern const Il2CppGenericInst GenInst_SystemException_t3877406272_0_0_0 = { 1, GenInst_SystemException_t3877406272_0_0_0_Types };
extern const Il2CppType Exception_t1927440687_0_0_0;
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_Types[] = { &Exception_t1927440687_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0 = { 1, GenInst_Exception_t1927440687_0_0_0_Types };
extern const Il2CppType _Exception_t3026971024_0_0_0;
static const Il2CppType* GenInst__Exception_t3026971024_0_0_0_Types[] = { &_Exception_t3026971024_0_0_0 };
extern const Il2CppGenericInst GenInst__Exception_t3026971024_0_0_0 = { 1, GenInst__Exception_t3026971024_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1430411454_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1430411454_0_0_0_Types[] = { &KeyValuePair_2_t1430411454_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1430411454_0_0_0 = { 1, GenInst_KeyValuePair_2_t1430411454_0_0_0_Types };
extern const Il2CppType DTDNode_t1758286970_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t1758286970_0_0_0_Types[] = { &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t1758286970_0_0_0 = { 1, GenInst_DTDNode_t1758286970_0_0_0_Types };
extern const Il2CppType AttributeSlot_t1499247213_0_0_0;
static const Il2CppType* GenInst_AttributeSlot_t1499247213_0_0_0_Types[] = { &AttributeSlot_t1499247213_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeSlot_t1499247213_0_0_0 = { 1, GenInst_AttributeSlot_t1499247213_0_0_0_Types };
extern const Il2CppType Entry_t2583369454_0_0_0;
static const Il2CppType* GenInst_Entry_t2583369454_0_0_0_Types[] = { &Entry_t2583369454_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t2583369454_0_0_0 = { 1, GenInst_Entry_t2583369454_0_0_0_Types };
extern const Il2CppType XmlNode_t616554813_0_0_0;
static const Il2CppType* GenInst_XmlNode_t616554813_0_0_0_Types[] = { &XmlNode_t616554813_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t616554813_0_0_0 = { 1, GenInst_XmlNode_t616554813_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t845515791_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t845515791_0_0_0_Types[] = { &IXPathNavigable_t845515791_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t845515791_0_0_0 = { 1, GenInst_IXPathNavigable_t845515791_0_0_0_Types };
extern const Il2CppType NsDecl_t3210081295_0_0_0;
static const Il2CppType* GenInst_NsDecl_t3210081295_0_0_0_Types[] = { &NsDecl_t3210081295_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t3210081295_0_0_0 = { 1, GenInst_NsDecl_t3210081295_0_0_0_Types };
extern const Il2CppType NsScope_t2513625351_0_0_0;
static const Il2CppType* GenInst_NsScope_t2513625351_0_0_0_Types[] = { &NsScope_t2513625351_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t2513625351_0_0_0 = { 1, GenInst_NsScope_t2513625351_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t3353594030_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types[] = { &XmlAttributeTokenInfo_t3353594030_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t254587324_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t254587324_0_0_0_Types[] = { &XmlTokenInfo_t254587324_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t254587324_0_0_0 = { 1, GenInst_XmlTokenInfo_t254587324_0_0_0_Types };
extern const Il2CppType TagName_t2340974457_0_0_0;
static const Il2CppType* GenInst_TagName_t2340974457_0_0_0_Types[] = { &TagName_t2340974457_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t2340974457_0_0_0 = { 1, GenInst_TagName_t2340974457_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t3709371029_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t3709371029_0_0_0_Types[] = { &XmlNodeInfo_t3709371029_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t3709371029_0_0_0 = { 1, GenInst_XmlNodeInfo_t3709371029_0_0_0_Types };
extern const Il2CppType XPathNavigator_t3981235968_0_0_0;
static const Il2CppType* GenInst_XPathNavigator_t3981235968_0_0_0_Types[] = { &XPathNavigator_t3981235968_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathNavigator_t3981235968_0_0_0 = { 1, GenInst_XPathNavigator_t3981235968_0_0_0_Types };
extern const Il2CppType IXmlNamespaceResolver_t3928241465_0_0_0;
static const Il2CppType* GenInst_IXmlNamespaceResolver_t3928241465_0_0_0_Types[] = { &IXmlNamespaceResolver_t3928241465_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlNamespaceResolver_t3928241465_0_0_0 = { 1, GenInst_IXmlNamespaceResolver_t3928241465_0_0_0_Types };
extern const Il2CppType XPathItem_t3130801258_0_0_0;
static const Il2CppType* GenInst_XPathItem_t3130801258_0_0_0_Types[] = { &XPathItem_t3130801258_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathItem_t3130801258_0_0_0 = { 1, GenInst_XPathItem_t3130801258_0_0_0_Types };
extern const Il2CppType XPathResultType_t1521569578_0_0_0;
static const Il2CppType* GenInst_XPathResultType_t1521569578_0_0_0_Types[] = { &XPathResultType_t1521569578_0_0_0 };
extern const Il2CppGenericInst GenInst_XPathResultType_t1521569578_0_0_0 = { 1, GenInst_XPathResultType_t1521569578_0_0_0_Types };
extern const Il2CppType XmlAttribute_t175731005_0_0_0;
static const Il2CppType* GenInst_XmlAttribute_t175731005_0_0_0_Types[] = { &XmlAttribute_t175731005_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t175731005_0_0_0 = { 1, GenInst_XmlAttribute_t175731005_0_0_0_Types };
extern const Il2CppType IHasXmlChildNode_t2048545686_0_0_0;
static const Il2CppType* GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types[] = { &IHasXmlChildNode_t2048545686_0_0_0 };
extern const Il2CppGenericInst GenInst_IHasXmlChildNode_t2048545686_0_0_0 = { 1, GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types };
extern const Il2CppType XmlQualifiedName_t1944712516_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0 = { 1, GenInst_XmlQualifiedName_t1944712516_0_0_0_Types };
extern const Il2CppType XmlSchema_t880472818_0_0_0;
static const Il2CppType* GenInst_XmlSchema_t880472818_0_0_0_Types[] = { &XmlSchema_t880472818_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchema_t880472818_0_0_0 = { 1, GenInst_XmlSchema_t880472818_0_0_0_Types };
extern const Il2CppType Regex_t1803876613_0_0_0;
static const Il2CppType* GenInst_Regex_t1803876613_0_0_0_Types[] = { &Regex_t1803876613_0_0_0 };
extern const Il2CppGenericInst GenInst_Regex_t1803876613_0_0_0 = { 1, GenInst_Regex_t1803876613_0_0_0_Types };
extern const Il2CppType XmlSchemaSimpleType_t248156492_0_0_0;
static const Il2CppType* GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types[] = { &XmlSchemaSimpleType_t248156492_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaSimpleType_t248156492_0_0_0 = { 1, GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types };
extern const Il2CppType XmlSchemaType_t1795078578_0_0_0;
static const Il2CppType* GenInst_XmlSchemaType_t1795078578_0_0_0_Types[] = { &XmlSchemaType_t1795078578_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaType_t1795078578_0_0_0 = { 1, GenInst_XmlSchemaType_t1795078578_0_0_0_Types };
extern const Il2CppType GenerationResult_t2691853749_0_0_0;
static const Il2CppType* GenInst_GenerationResult_t2691853749_0_0_0_Types[] = { &GenerationResult_t2691853749_0_0_0 };
extern const Il2CppGenericInst GenInst_GenerationResult_t2691853749_0_0_0 = { 1, GenInst_GenerationResult_t2691853749_0_0_0_Types };
extern const Il2CppType XmlMapping_t1597064667_0_0_0;
static const Il2CppType* GenInst_XmlMapping_t1597064667_0_0_0_Types[] = { &XmlMapping_t1597064667_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlMapping_t1597064667_0_0_0 = { 1, GenInst_XmlMapping_t1597064667_0_0_0_Types };
extern const Il2CppType EnumMapMember_t3814867081_0_0_0;
static const Il2CppType* GenInst_EnumMapMember_t3814867081_0_0_0_Types[] = { &EnumMapMember_t3814867081_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumMapMember_t3814867081_0_0_0 = { 1, GenInst_EnumMapMember_t3814867081_0_0_0_Types };
extern const Il2CppType Hook_t3980190809_0_0_0;
static const Il2CppType* GenInst_Hook_t3980190809_0_0_0_Types[] = { &Hook_t3980190809_0_0_0 };
extern const Il2CppGenericInst GenInst_Hook_t3980190809_0_0_0 = { 1, GenInst_Hook_t3980190809_0_0_0_Types };
extern const Il2CppType XmlMemberMapping_t2912176015_0_0_0;
static const Il2CppType* GenInst_XmlMemberMapping_t2912176015_0_0_0_Types[] = { &XmlMemberMapping_t2912176015_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlMemberMapping_t2912176015_0_0_0 = { 1, GenInst_XmlMemberMapping_t2912176015_0_0_0_Types };
extern const Il2CppType XmlIncludeAttribute_t3199808027_0_0_0;
static const Il2CppType* GenInst_XmlIncludeAttribute_t3199808027_0_0_0_Types[] = { &XmlIncludeAttribute_t3199808027_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlIncludeAttribute_t3199808027_0_0_0 = { 1, GenInst_XmlIncludeAttribute_t3199808027_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType SerializerData_t3606741380_0_0_0;
static const Il2CppType* GenInst_SerializerData_t3606741380_0_0_0_Types[] = { &SerializerData_t3606741380_0_0_0 };
extern const Il2CppGenericInst GenInst_SerializerData_t3606741380_0_0_0 = { 1, GenInst_SerializerData_t3606741380_0_0_0_Types };
extern const Il2CppType XmlTypeMapMemberAttribute_t3329971455_0_0_0;
static const Il2CppType* GenInst_XmlTypeMapMemberAttribute_t3329971455_0_0_0_Types[] = { &XmlTypeMapMemberAttribute_t3329971455_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTypeMapMemberAttribute_t3329971455_0_0_0 = { 1, GenInst_XmlTypeMapMemberAttribute_t3329971455_0_0_0_Types };
extern const Il2CppType XmlTypeMapMember_t3057402259_0_0_0;
static const Il2CppType* GenInst_XmlTypeMapMember_t3057402259_0_0_0_Types[] = { &XmlTypeMapMember_t3057402259_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTypeMapMember_t3057402259_0_0_0 = { 1, GenInst_XmlTypeMapMember_t3057402259_0_0_0_Types };
extern const Il2CppType XmlTypeMapElementInfo_t3381496463_0_0_0;
static const Il2CppType* GenInst_XmlTypeMapElementInfo_t3381496463_0_0_0_Types[] = { &XmlTypeMapElementInfo_t3381496463_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTypeMapElementInfo_t3381496463_0_0_0 = { 1, GenInst_XmlTypeMapElementInfo_t3381496463_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
extern const Il2CppType XmlElement_t2877111883_0_0_0;
static const Il2CppType* GenInst_XmlElement_t2877111883_0_0_0_Types[] = { &XmlElement_t2877111883_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlElement_t2877111883_0_0_0 = { 1, GenInst_XmlElement_t2877111883_0_0_0_Types };
extern const Il2CppType XmlLinkedNode_t1287616130_0_0_0;
static const Il2CppType* GenInst_XmlLinkedNode_t1287616130_0_0_0_Types[] = { &XmlLinkedNode_t1287616130_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlLinkedNode_t1287616130_0_0_0 = { 1, GenInst_XmlLinkedNode_t1287616130_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType Node_t2499136326_0_0_0;
static const Il2CppType* GenInst_Node_t2499136326_0_0_0_Types[] = { &Node_t2499136326_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2499136326_0_0_0 = { 1, GenInst_Node_t2499136326_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t4250402154_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t4250402154_0_0_0_Types[] = { &PropertyDescriptor_t4250402154_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t4250402154_0_0_0 = { 1, GenInst_PropertyDescriptor_t4250402154_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t3749827553_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t3749827553_0_0_0_Types[] = { &MemberDescriptor_t3749827553_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t3749827553_0_0_0 = { 1, GenInst_MemberDescriptor_t3749827553_0_0_0_Types };
extern const Il2CppType IComponent_t1000253244_0_0_0;
static const Il2CppType* GenInst_IComponent_t1000253244_0_0_0_Types[] = { &IComponent_t1000253244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComponent_t1000253244_0_0_0 = { 1, GenInst_IComponent_t1000253244_0_0_0_Types };
extern const Il2CppType EventDescriptor_t962731901_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t962731901_0_0_0_Types[] = { &EventDescriptor_t962731901_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t962731901_0_0_0 = { 1, GenInst_EventDescriptor_t962731901_0_0_0_Types };
extern const Il2CppType ListSortDescription_t3194554012_0_0_0;
static const Il2CppType* GenInst_ListSortDescription_t3194554012_0_0_0_Types[] = { &ListSortDescription_t3194554012_0_0_0 };
extern const Il2CppGenericInst GenInst_ListSortDescription_t3194554012_0_0_0 = { 1, GenInst_ListSortDescription_t3194554012_0_0_0_Types };
extern const Il2CppType EditPosition_t3912732320_0_0_0;
static const Il2CppType* GenInst_EditPosition_t3912732320_0_0_0_Types[] = { &EditPosition_t3912732320_0_0_0 };
extern const Il2CppGenericInst GenInst_EditPosition_t3912732320_0_0_0 = { 1, GenInst_EditPosition_t3912732320_0_0_0_Types };
extern const Il2CppType PropertyTabScope_t2485003348_0_0_0;
static const Il2CppType* GenInst_PropertyTabScope_t2485003348_0_0_0_Types[] = { &PropertyTabScope_t2485003348_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyTabScope_t2485003348_0_0_0 = { 1, GenInst_PropertyTabScope_t2485003348_0_0_0_Types };
extern const Il2CppType AttributeU5BU5D_t4255796347_0_0_0;
static const Il2CppType* GenInst_AttributeU5BU5D_t4255796347_0_0_0_Types[] = { &AttributeU5BU5D_t4255796347_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeU5BU5D_t4255796347_0_0_0 = { 1, GenInst_AttributeU5BU5D_t4255796347_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2743332604_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t2438624375_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types[] = { &TypeDescriptionProvider_t2438624375_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t2438624375_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LinkedList_1_t2743332604_0_0_0_Types[] = { &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2743332604_0_0_0 = { 1, GenInst_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2438035723_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2438035723_0_0_0_Types[] = { &KeyValuePair_2_t2438035723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2438035723_0_0_0 = { 1, GenInst_KeyValuePair_2_t2438035723_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t2012978780_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0 = { 1, GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3261256129_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3261256129_0_0_0_Types[] = { &KeyValuePair_2_t3261256129_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3261256129_0_0_0 = { 1, GenInst_KeyValuePair_2_t3261256129_0_0_0_Types };
extern const Il2CppType Cookie_t3154017544_0_0_0;
static const Il2CppType* GenInst_Cookie_t3154017544_0_0_0_Types[] = { &Cookie_t3154017544_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t3154017544_0_0_0 = { 1, GenInst_Cookie_t3154017544_0_0_0_Types };
extern const Il2CppType IPAddress_t1399971723_0_0_0;
static const Il2CppType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { &IPAddress_t1399971723_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { &ArraySegment_1_t2594217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType DynamicMethod_t3307743052_0_0_0;
static const Il2CppType* GenInst_DynamicMethod_t3307743052_0_0_0_Types[] = { &DynamicMethod_t3307743052_0_0_0 };
extern const Il2CppGenericInst GenInst_DynamicMethod_t3307743052_0_0_0 = { 1, GenInst_DynamicMethod_t3307743052_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
extern const Il2CppType Label_t4243202660_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1008373517_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1008373517_0_0_0_Types[] = { &KeyValuePair_2_t1008373517_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1008373517_0_0_0 = { 1, GenInst_KeyValuePair_2_t1008373517_0_0_0_Types };
static const Il2CppType* GenInst_Label_t4243202660_0_0_0_Types[] = { &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t4243202660_0_0_0 = { 1, GenInst_Label_t4243202660_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &KeyValuePair_2_t1008373517_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
extern const Il2CppType Mark_t2724874473_0_0_0;
static const Il2CppType* GenInst_Mark_t2724874473_0_0_0_Types[] = { &Mark_t2724874473_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
extern const Il2CppType UriScheme_t1876590943_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { &UriScheme_t1876590943_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
extern const Il2CppType ConfigurationElement_t1776195828_0_0_0;
static const Il2CppType* GenInst_ConfigurationElement_t1776195828_0_0_0_Types[] = { &ConfigurationElement_t1776195828_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationElement_t1776195828_0_0_0 = { 1, GenInst_ConfigurationElement_t1776195828_0_0_0_Types };
extern const Il2CppType ConfigurationProperty_t2048066811_0_0_0;
static const Il2CppType* GenInst_ConfigurationProperty_t2048066811_0_0_0_Types[] = { &ConfigurationProperty_t2048066811_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationProperty_t2048066811_0_0_0 = { 1, GenInst_ConfigurationProperty_t2048066811_0_0_0_Types };
extern const Il2CppType ConfigurationSection_t2600766927_0_0_0;
static const Il2CppType* GenInst_ConfigurationSection_t2600766927_0_0_0_Types[] = { &ConfigurationSection_t2600766927_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationSection_t2600766927_0_0_0 = { 1, GenInst_ConfigurationSection_t2600766927_0_0_0_Types };
extern const Il2CppType ConfigurationSectionGroup_t2230982736_0_0_0;
static const Il2CppType* GenInst_ConfigurationSectionGroup_t2230982736_0_0_0_Types[] = { &ConfigurationSectionGroup_t2230982736_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationSectionGroup_t2230982736_0_0_0 = { 1, GenInst_ConfigurationSectionGroup_t2230982736_0_0_0_Types };
extern const Il2CppType PropertyInformation_t2089433965_0_0_0;
static const Il2CppType* GenInst_PropertyInformation_t2089433965_0_0_0_Types[] = { &PropertyInformation_t2089433965_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInformation_t2089433965_0_0_0 = { 1, GenInst_PropertyInformation_t2089433965_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType Link_t865133271_0_0_0;
static const Il2CppType* GenInst_Link_t865133271_0_0_0_Types[] = { &Link_t865133271_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { &AchievementDescription_t3110978151_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { &IAchievementDescription_t3498529102_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
extern const Il2CppType UserProfile_t3365630962_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { &UserProfile_t3365630962_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { &IUserProfile_t4108565527_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { &GcLeaderboard_t453887929_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4083280315_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { &IAchievementU5BU5D_t2709554645_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
extern const Il2CppType IAchievement_t1752291260_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { &IAchievement_t1752291260_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { &GcAchievementData_t1754866149_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
extern const Il2CppType Achievement_t1333316625_0_0_0;
static const Il2CppType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { &Achievement_t1333316625_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { &IScoreU5BU5D_t3237304636_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
extern const Il2CppType IScore_t513966369_0_0_0;
static const Il2CppType* GenInst_IScore_t513966369_0_0_0_Types[] = { &IScore_t513966369_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { &GcScoreData_t3676783238_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
extern const Il2CppType Score_t2307748940_0_0_0;
static const Il2CppType* GenInst_Score_t2307748940_0_0_0_Types[] = { &Score_t2307748940_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { &IUserProfileU5BU5D_t3461248430_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
extern const Il2CppType Material_t193706927_0_0_0;
static const Il2CppType* GenInst_Material_t193706927_0_0_0_Types[] = { &Material_t193706927_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t193706927_0_0_0 = { 1, GenInst_Material_t193706927_0_0_0_Types };
extern const Il2CppType Vector2_t2243707579_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType Plane_t3727654732_0_0_0;
static const Il2CppType* GenInst_Plane_t3727654732_0_0_0_Types[] = { &Plane_t3727654732_0_0_0 };
extern const Il2CppGenericInst GenInst_Plane_t3727654732_0_0_0 = { 1, GenInst_Plane_t3727654732_0_0_0_Types };
extern const Il2CppType Touch_t407273883_0_0_0;
static const Il2CppType* GenInst_Touch_t407273883_0_0_0_Types[] = { &Touch_t407273883_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t407273883_0_0_0 = { 1, GenInst_Touch_t407273883_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Vector3_t2243707580_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType Vector4_t2243707581_0_0_0;
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType Color32_t874517518_0_0_0;
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
extern const Il2CppType Color_t2020392075_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType Playable_t3667545548_0_0_0;
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0 = { 1, GenInst_Playable_t3667545548_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType Collider2D_t646061738_0_0_0;
static const Il2CppType* GenInst_Collider2D_t646061738_0_0_0_Types[] = { &Collider2D_t646061738_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider2D_t646061738_0_0_0 = { 1, GenInst_Collider2D_t646061738_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType UIVertex_t1204258818_0_0_0;
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUIContent_t4210063000_0_0_0;
static const Il2CppType* GenInst_GUIContent_t4210063000_0_0_0_Types[] = { &GUIContent_t4210063000_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIContent_t4210063000_0_0_0 = { 1, GenInst_GUIContent_t4210063000_0_0_0_Types };
extern const Il2CppType Rect_t3681755626_0_0_0;
static const Il2CppType* GenInst_Rect_t3681755626_0_0_0_Types[] = { &Rect_t3681755626_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t3681755626_0_0_0 = { 1, GenInst_Rect_t3681755626_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType Texture_t2243626319_0_0_0;
static const Il2CppType* GenInst_Texture_t2243626319_0_0_0_Types[] = { &Texture_t2243626319_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t2243626319_0_0_0 = { 1, GenInst_Texture_t2243626319_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t3120781045_0_0_0_Types[] = { &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t3120781045_0_0_0 = { 1, GenInst_LayoutCache_t3120781045_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
extern const Il2CppType Event_t3028476042_0_0_0;
extern const Il2CppType TextEditOp_t3138797698_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t488203048_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0 = { 1, GenInst_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0 = { 1, GenInst_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_Types[] = { &Event_t3028476042_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0 = { 1, GenInst_Event_t3028476042_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3799506081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3799506081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3799506081_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
extern const Il2CppType List_1_t61287617_0_0_0;
static const Il2CppType* GenInst_List_1_t61287617_0_0_0_Types[] = { &List_1_t61287617_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t61287617_0_0_0 = { 1, GenInst_List_1_t61287617_0_0_0_Types };
extern const Il2CppType DispatcherKey_t708950850_0_0_0;
extern const Il2CppType Dispatcher_t2240407071_0_0_0;
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0, &Dispatcher_t2240407071_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0 = { 2, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0 = { 1, GenInst_DispatcherKey_t708950850_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0, &Dispatcher_t2240407071_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dispatcher_t2240407071_0_0_0_Types[] = { &Dispatcher_t2240407071_0_0_0 };
extern const Il2CppGenericInst GenInst_Dispatcher_t2240407071_0_0_0 = { 1, GenInst_Dispatcher_t2240407071_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3846770086_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3846770086_0_0_0_Types[] = { &KeyValuePair_2_t3846770086_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3846770086_0_0_0 = { 1, GenInst_KeyValuePair_2_t3846770086_0_0_0_Types };
extern const Il2CppType UrlSchemes_t2066630273_0_0_0;
static const Il2CppType* GenInst_UrlSchemes_t2066630273_0_0_0_Types[] = { &UrlSchemes_t2066630273_0_0_0 };
extern const Il2CppGenericInst GenInst_UrlSchemes_t2066630273_0_0_0 = { 1, GenInst_UrlSchemes_t2066630273_0_0_0_Types };
extern const Il2CppType OnChangeCallback_t2639189684_0_0_0;
static const Il2CppType* GenInst_OnChangeCallback_t2639189684_0_0_0_Types[] = { &OnChangeCallback_t2639189684_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeCallback_t2639189684_0_0_0 = { 1, GenInst_OnChangeCallback_t2639189684_0_0_0_Types };
extern const Il2CppType ZipEntry_t1764014695_0_0_0;
static const Il2CppType* GenInst_ZipEntry_t1764014695_0_0_0_Types[] = { &ZipEntry_t1764014695_0_0_0 };
extern const Il2CppGenericInst GenInst_ZipEntry_t1764014695_0_0_0 = { 1, GenInst_ZipEntry_t1764014695_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
extern const Il2CppType Action_t3226471752_0_0_0;
static const Il2CppType* GenInst_Action_t3226471752_0_0_0_Types[] = { &Action_t3226471752_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
extern const Il2CppType Action_2_t2572051853_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0_Types[] = { &String_t_0_0_0, &Action_2_t2572051853_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0 = { 2, GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Action_2_t2572051853_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t2572051853_0_0_0_Types[] = { &Action_2_t2572051853_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2572051853_0_0_0 = { 1, GenInst_Action_2_t2572051853_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2244176337_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2244176337_0_0_0_Types[] = { &KeyValuePair_2_t2244176337_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2244176337_0_0_0 = { 1, GenInst_KeyValuePair_2_t2244176337_0_0_0_Types };
extern const Il2CppType JsonContextType_t3787516849_0_0_0;
static const Il2CppType* GenInst_JsonContextType_t3787516849_0_0_0_Types[] = { &JsonContextType_t3787516849_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonContextType_t3787516849_0_0_0 = { 1, GenInst_JsonContextType_t3787516849_0_0_0_Types };
extern const Il2CppType MemberMap_t574666045_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0_Types[] = { &String_t_0_0_0, &MemberMap_t574666045_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0 = { 2, GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &MemberMap_t574666045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t246790529_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t246790529_0_0_0_Types[] = { &KeyValuePair_2_t246790529_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t246790529_0_0_0 = { 1, GenInst_KeyValuePair_2_t246790529_0_0_0_Types };
extern const Il2CppType SafeDictionary_2_t1260879230_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0_Types[] = { &Type_t_0_0_0, &SafeDictionary_2_t1260879230_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0 = { 2, GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &SafeDictionary_2_t1260879230_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t955582349_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t955582349_0_0_0_Types[] = { &KeyValuePair_2_t955582349_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t955582349_0_0_0 = { 1, GenInst_KeyValuePair_2_t955582349_0_0_0_Types };
extern const Il2CppType CtorDelegate_t3614558424_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0_Types[] = { &Type_t_0_0_0, &CtorDelegate_t3614558424_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0 = { 2, GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &CtorDelegate_t3614558424_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3309261543_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3309261543_0_0_0_Types[] = { &KeyValuePair_2_t3309261543_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3309261543_0_0_0 = { 1, GenInst_KeyValuePair_2_t3309261543_0_0_0_Types };
extern const Il2CppType Product_t1158373411_0_0_0;
static const Il2CppType* GenInst_Product_t1158373411_0_0_0_Types[] = { &Product_t1158373411_0_0_0 };
extern const Il2CppGenericInst GenInst_Product_t1158373411_0_0_0 = { 1, GenInst_Product_t1158373411_0_0_0_Types };
extern const Il2CppType FacebookUnityPlatform_t1867507902_0_0_0;
static const Il2CppType* GenInst_FacebookUnityPlatform_t1867507902_0_0_0_Types[] = { &FacebookUnityPlatform_t1867507902_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookUnityPlatform_t1867507902_0_0_0 = { 1, GenInst_FacebookUnityPlatform_t1867507902_0_0_0_Types };
extern const Il2CppType ILoginResult_t403585443_0_0_0;
static const Il2CppType* GenInst_ILoginResult_t403585443_0_0_0_Types[] = { &ILoginResult_t403585443_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoginResult_t403585443_0_0_0 = { 1, GenInst_ILoginResult_t403585443_0_0_0_Types };
extern const Il2CppType IAppRequestResult_t1874118006_0_0_0;
static const Il2CppType* GenInst_IAppRequestResult_t1874118006_0_0_0_Types[] = { &IAppRequestResult_t1874118006_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppRequestResult_t1874118006_0_0_0 = { 1, GenInst_IAppRequestResult_t1874118006_0_0_0_Types };
extern const Il2CppType OGActionType_t1978093408_0_0_0;
static const Il2CppType* GenInst_OGActionType_t1978093408_0_0_0_Types[] = { &OGActionType_t1978093408_0_0_0 };
extern const Il2CppGenericInst GenInst_OGActionType_t1978093408_0_0_0 = { 1, GenInst_OGActionType_t1978093408_0_0_0_Types };
extern const Il2CppType IShareResult_t830127229_0_0_0;
static const Il2CppType* GenInst_IShareResult_t830127229_0_0_0_Types[] = { &IShareResult_t830127229_0_0_0 };
extern const Il2CppGenericInst GenInst_IShareResult_t830127229_0_0_0 = { 1, GenInst_IShareResult_t830127229_0_0_0_Types };
extern const Il2CppType IGraphResult_t3984946686_0_0_0;
static const Il2CppType* GenInst_IGraphResult_t3984946686_0_0_0_Types[] = { &IGraphResult_t3984946686_0_0_0 };
extern const Il2CppGenericInst GenInst_IGraphResult_t3984946686_0_0_0 = { 1, GenInst_IGraphResult_t3984946686_0_0_0_Types };
extern const Il2CppType IAppLinkResult_t3542744145_0_0_0;
static const Il2CppType* GenInst_IAppLinkResult_t3542744145_0_0_0_Types[] = { &IAppLinkResult_t3542744145_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppLinkResult_t3542744145_0_0_0 = { 1, GenInst_IAppLinkResult_t3542744145_0_0_0_Types };
extern const Il2CppType IGroupCreateResult_t2512549813_0_0_0;
static const Il2CppType* GenInst_IGroupCreateResult_t2512549813_0_0_0_Types[] = { &IGroupCreateResult_t2512549813_0_0_0 };
extern const Il2CppGenericInst GenInst_IGroupCreateResult_t2512549813_0_0_0 = { 1, GenInst_IGroupCreateResult_t2512549813_0_0_0_Types };
extern const Il2CppType IGroupJoinResult_t1571908141_0_0_0;
static const Il2CppType* GenInst_IGroupJoinResult_t1571908141_0_0_0_Types[] = { &IGroupJoinResult_t1571908141_0_0_0 };
extern const Il2CppGenericInst GenInst_IGroupJoinResult_t1571908141_0_0_0 = { 1, GenInst_IGroupJoinResult_t1571908141_0_0_0_Types };
extern const Il2CppType IPayResult_t2860116854_0_0_0;
static const Il2CppType* GenInst_IPayResult_t2860116854_0_0_0_Types[] = { &IPayResult_t2860116854_0_0_0 };
extern const Il2CppGenericInst GenInst_IPayResult_t2860116854_0_0_0 = { 1, GenInst_IPayResult_t2860116854_0_0_0_Types };
extern const Il2CppType IAppInviteResult_t3529555166_0_0_0;
static const Il2CppType* GenInst_IAppInviteResult_t3529555166_0_0_0_Types[] = { &IAppInviteResult_t3529555166_0_0_0 };
extern const Il2CppGenericInst GenInst_IAppInviteResult_t3529555166_0_0_0 = { 1, GenInst_IAppInviteResult_t3529555166_0_0_0_Types };
extern const Il2CppType IAccessTokenRefreshResult_t2724304088_0_0_0;
static const Il2CppType* GenInst_IAccessTokenRefreshResult_t2724304088_0_0_0_Types[] = { &IAccessTokenRefreshResult_t2724304088_0_0_0 };
extern const Il2CppGenericInst GenInst_IAccessTokenRefreshResult_t2724304088_0_0_0 = { 1, GenInst_IAccessTokenRefreshResult_t2724304088_0_0_0_Types };
extern const Il2CppType ResultContainer_t2148006712_0_0_0;
static const Il2CppType* GenInst_ResultContainer_t2148006712_0_0_0_Types[] = { &ResultContainer_t2148006712_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultContainer_t2148006712_0_0_0 = { 1, GenInst_ResultContainer_t2148006712_0_0_0_Types };
extern const Il2CppType IResult_t3447678270_0_0_0;
static const Il2CppType* GenInst_IResult_t3447678270_0_0_0_Types[] = { &IResult_t3447678270_0_0_0 };
extern const Il2CppGenericInst GenInst_IResult_t3447678270_0_0_0 = { 1, GenInst_IResult_t3447678270_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types[] = { &Il2CppObject_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_String_t_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType XAttribute_t3858477518_0_0_0;
static const Il2CppType* GenInst_XAttribute_t3858477518_0_0_0_Types[] = { &XAttribute_t3858477518_0_0_0 };
extern const Il2CppGenericInst GenInst_XAttribute_t3858477518_0_0_0 = { 1, GenInst_XAttribute_t3858477518_0_0_0_Types };
extern const Il2CppType XObject_t3550811009_0_0_0;
static const Il2CppType* GenInst_XObject_t3550811009_0_0_0_Types[] = { &XObject_t3550811009_0_0_0 };
extern const Il2CppGenericInst GenInst_XObject_t3550811009_0_0_0 = { 1, GenInst_XObject_t3550811009_0_0_0_Types };
extern const Il2CppType IXmlLineInfo_t135184468_0_0_0;
static const Il2CppType* GenInst_IXmlLineInfo_t135184468_0_0_0_Types[] = { &IXmlLineInfo_t135184468_0_0_0 };
extern const Il2CppGenericInst GenInst_IXmlLineInfo_t135184468_0_0_0 = { 1, GenInst_IXmlLineInfo_t135184468_0_0_0_Types };
extern const Il2CppType XNode_t2707504214_0_0_0;
static const Il2CppType* GenInst_XNode_t2707504214_0_0_0_Types[] = { &XNode_t2707504214_0_0_0 };
extern const Il2CppGenericInst GenInst_XNode_t2707504214_0_0_0 = { 1, GenInst_XNode_t2707504214_0_0_0_Types };
extern const Il2CppType XElement_t553821050_0_0_0;
static const Il2CppType* GenInst_XElement_t553821050_0_0_0_Types[] = { &XElement_t553821050_0_0_0 };
extern const Il2CppGenericInst GenInst_XElement_t553821050_0_0_0 = { 1, GenInst_XElement_t553821050_0_0_0_Types };
extern const Il2CppType XName_t785190363_0_0_0;
static const Il2CppType* GenInst_XName_t785190363_0_0_0_Types[] = { &XName_t785190363_0_0_0 };
extern const Il2CppGenericInst GenInst_XName_t785190363_0_0_0 = { 1, GenInst_XName_t785190363_0_0_0_Types };
extern const Il2CppType XNamespace_t1613015075_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_Types[] = { &String_t_0_0_0, &XNamespace_t1613015075_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0 = { 2, GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &XNamespace_t1613015075_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_XNamespace_t1613015075_0_0_0_Types[] = { &XNamespace_t1613015075_0_0_0 };
extern const Il2CppGenericInst GenInst_XNamespace_t1613015075_0_0_0 = { 1, GenInst_XNamespace_t1613015075_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1285139559_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1285139559_0_0_0_Types[] = { &KeyValuePair_2_t1285139559_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1285139559_0_0_0 = { 1, GenInst_KeyValuePair_2_t1285139559_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XName_t785190363_0_0_0_Types[] = { &String_t_0_0_0, &XName_t785190363_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XName_t785190363_0_0_0 = { 2, GenInst_String_t_0_0_0_XName_t785190363_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &XName_t785190363_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t457314847_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t457314847_0_0_0_Types[] = { &KeyValuePair_2_t457314847_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t457314847_0_0_0 = { 1, GenInst_KeyValuePair_2_t457314847_0_0_0_Types };
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { &BaseInputModule_t1295781545_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
extern const Il2CppType RaycastResult_t21186376_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { &IDeselectHandler_t3182198310_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { &IEventSystemHandler_t2741188318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
extern const Il2CppType List_1_t2110309450_0_0_0;
static const Il2CppType* GenInst_List_1_t2110309450_0_0_0_Types[] = { &List_1_t2110309450_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
extern const Il2CppType List_1_t2058570427_0_0_0;
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_Types[] = { &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
extern const Il2CppType List_1_t3188497603_0_0_0;
static const Il2CppType* GenInst_List_1_t3188497603_0_0_0_Types[] = { &List_1_t3188497603_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { &ISelectHandler_t2812555161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { &BaseRaycaster_t2336171397_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
extern const Il2CppType Entry_t3365010046_0_0_0;
static const Il2CppType* GenInst_Entry_t3365010046_0_0_0_Types[] = { &Entry_t3365010046_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { &BaseEventData_t2681005625_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { &IPointerEnterHandler_t193164956_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { &IPointerExitHandler_t461019860_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { &IPointerDownHandler_t3929046918_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { &IPointerUpHandler_t1847764461_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { &IPointerClickHandler_t96169666_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { &IInitializePotentialDragHandler_t3350809087_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { &IBeginDragHandler_t3135127860_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { &IDragHandler_t2583993319_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { &IEndDragHandler_t1349123600_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { &IDropHandler_t2390101210_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { &IScrollHandler_t3834677510_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { &IUpdateSelectedHandler_t3778909353_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { &IMoveHandler_t2611925506_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { &ISubmitHandler_t525803901_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { &ICancelHandler_t1980319651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
extern const Il2CppType Transform_t3275118058_0_0_0;
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
extern const Il2CppType GameObject_t1756533147_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType BaseInput_t621514313_0_0_0;
static const Il2CppType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { &BaseInput_t621514313_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { &UIBehaviour_t3960014691_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
extern const Il2CppType ButtonState_t2688375492_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { &ButtonState_t2688375492_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { &ColorBlock_t2652774230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
extern const Il2CppType OptionData_t2420267500_0_0_0;
static const Il2CppType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { &OptionData_t2420267500_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { &DropdownItem_t4139978805_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
extern const Il2CppType FloatTween_t2986189219_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { &FloatTween_t2986189219_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
extern const Il2CppType Sprite_t309593783_0_0_0;
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Types[] = { &Sprite_t309593783_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
extern const Il2CppType Canvas_t209405766_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_Types[] = { &Canvas_t209405766_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
extern const Il2CppType List_1_t3873494194_0_0_0;
static const Il2CppType* GenInst_List_1_t3873494194_0_0_0_Types[] = { &List_1_t3873494194_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType Text_t356221433_0_0_0;
static const Il2CppType* GenInst_Text_t356221433_0_0_0_Types[] = { &Text_t356221433_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t2984649583_0_0_0_Types[] = { &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2984649583_0_0_0 = { 1, GenInst_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
extern const Il2CppType ColorTween_t3438117476_0_0_0;
static const Il2CppType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { &ColorTween_t3438117476_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
extern const Il2CppType Graphic_t2426225576_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t286373651_0_0_0_Types[] = { &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t286373651_0_0_0 = { 1, GenInst_IndexedSet_1_t286373651_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
extern const Il2CppType Type_t3352948571_0_0_0;
static const Il2CppType* GenInst_Type_t3352948571_0_0_0_Types[] = { &Type_t3352948571_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
extern const Il2CppType FillMethod_t1640962579_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { &FillMethod_t1640962579_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
extern const Il2CppType ContentType_t1028629049_0_0_0;
static const Il2CppType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { &ContentType_t1028629049_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
extern const Il2CppType LineType_t2931319356_0_0_0;
static const Il2CppType* GenInst_LineType_t2931319356_0_0_0_Types[] = { &LineType_t2931319356_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
extern const Il2CppType InputType_t1274231802_0_0_0;
static const Il2CppType* GenInst_InputType_t1274231802_0_0_0_Types[] = { &InputType_t1274231802_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { &TouchScreenKeyboardType_t875112366_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { &CharacterValidation_t3437478890_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
extern const Il2CppType Mask_t2977958238_0_0_0;
static const Il2CppType* GenInst_Mask_t2977958238_0_0_0_Types[] = { &Mask_t2977958238_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
extern const Il2CppType List_1_t2347079370_0_0_0;
static const Il2CppType* GenInst_List_1_t2347079370_0_0_0_Types[] = { &List_1_t2347079370_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { &RectMask2D_t1156185964_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
extern const Il2CppType List_1_t525307096_0_0_0;
static const Il2CppType* GenInst_List_1_t525307096_0_0_0_Types[] = { &List_1_t525307096_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
extern const Il2CppType Navigation_t1571958496_0_0_0;
static const Il2CppType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { &Navigation_t1571958496_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
extern const Il2CppType IClippable_t1941276057_0_0_0;
static const Il2CppType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { &IClippable_t1941276057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
extern const Il2CppType Direction_t3696775921_0_0_0;
static const Il2CppType* GenInst_Direction_t3696775921_0_0_0_Types[] = { &Direction_t3696775921_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
extern const Il2CppType Selectable_t1490392188_0_0_0;
static const Il2CppType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { &Selectable_t1490392188_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
extern const Il2CppType Transition_t605142169_0_0_0;
static const Il2CppType* GenInst_Transition_t605142169_0_0_0_Types[] = { &Transition_t605142169_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
extern const Il2CppType SpriteState_t1353336012_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { &SpriteState_t1353336012_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { &CanvasGroup_t3296560743_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
extern const Il2CppType Direction_t1525323322_0_0_0;
static const Il2CppType* GenInst_Direction_t1525323322_0_0_0_Types[] = { &Direction_t1525323322_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
extern const Il2CppType MatEntry_t3157325053_0_0_0;
static const Il2CppType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { &MatEntry_t3157325053_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
extern const Il2CppType Toggle_t3976754468_0_0_0;
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IClipper_t900477982_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Types[] = { &IClipper_t900477982_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
extern const Il2CppType AspectMode_t1166448724_0_0_0;
static const Il2CppType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { &AspectMode_t1166448724_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
extern const Il2CppType FitMode_t4030874534_0_0_0;
static const Il2CppType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { &FitMode_t4030874534_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
extern const Il2CppType RectTransform_t3349966182_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { &RectTransform_t3349966182_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { &LayoutRebuilder_t2155218138_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType List_1_t1612828712_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828712_0_0_0_Types[] = { &List_1_t1612828712_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
extern const Il2CppType List_1_t243638650_0_0_0;
static const Il2CppType* GenInst_List_1_t243638650_0_0_0_Types[] = { &List_1_t243638650_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
extern const Il2CppType List_1_t1612828711_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828711_0_0_0_Types[] = { &List_1_t1612828711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
extern const Il2CppType List_1_t1612828713_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828713_0_0_0_Types[] = { &List_1_t1612828713_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
extern const Il2CppType List_1_t1440998580_0_0_0;
static const Il2CppType* GenInst_List_1_t1440998580_0_0_0_Types[] = { &List_1_t1440998580_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
extern const Il2CppType List_1_t573379950_0_0_0;
static const Il2CppType* GenInst_List_1_t573379950_0_0_0_Types[] = { &List_1_t573379950_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
extern const Il2CppType RenderTexture_t2666733923_0_0_0;
static const Il2CppType* GenInst_RenderTexture_t2666733923_0_0_0_Types[] = { &RenderTexture_t2666733923_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTexture_t2666733923_0_0_0 = { 1, GenInst_RenderTexture_t2666733923_0_0_0_Types };
extern const Il2CppType Texture2D_t3542995729_0_0_0;
static const Il2CppType* GenInst_Texture2D_t3542995729_0_0_0_Types[] = { &Texture2D_t3542995729_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t3542995729_0_0_0 = { 1, GenInst_Texture2D_t3542995729_0_0_0_Types };
extern const Il2CppType WWW_t2919945039_0_0_0;
static const Il2CppType* GenInst_WWW_t2919945039_0_0_0_Types[] = { &WWW_t2919945039_0_0_0 };
extern const Il2CppGenericInst GenInst_WWW_t2919945039_0_0_0 = { 1, GenInst_WWW_t2919945039_0_0_0_Types };
extern const Il2CppType Contact_t4014196408_0_0_0;
static const Il2CppType* GenInst_Contact_t4014196408_0_0_0_Types[] = { &Contact_t4014196408_0_0_0 };
extern const Il2CppGenericInst GenInst_Contact_t4014196408_0_0_0 = { 1, GenInst_Contact_t4014196408_0_0_0_Types };
extern const Il2CppType IDictionary_t596158605_0_0_0;
static const Il2CppType* GenInst_IDictionary_t596158605_0_0_0_Types[] = { &IDictionary_t596158605_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_t596158605_0_0_0 = { 1, GenInst_IDictionary_t596158605_0_0_0_Types };
extern const Il2CppType FacebookFriendsResult_t459691178_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FacebookFriendsResult_t459691178_0_0_0_Types[] = { &String_t_0_0_0, &FacebookFriendsResult_t459691178_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FacebookFriendsResult_t459691178_0_0_0 = { 2, GenInst_String_t_0_0_0_FacebookFriendsResult_t459691178_0_0_0_Types };
extern const Il2CppType List_1_t1398341365_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1398341365_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_Types };
extern const Il2CppType FacebookMeResult_t187696501_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FacebookMeResult_t187696501_0_0_0_Types[] = { &String_t_0_0_0, &FacebookMeResult_t187696501_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FacebookMeResult_t187696501_0_0_0 = { 2, GenInst_String_t_0_0_0_FacebookMeResult_t187696501_0_0_0_Types };
extern const Il2CppType FacebookBatchRequest_t2176192103_0_0_0;
static const Il2CppType* GenInst_FacebookBatchRequest_t2176192103_0_0_0_Types[] = { &FacebookBatchRequest_t2176192103_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookBatchRequest_t2176192103_0_0_0 = { 1, GenInst_FacebookBatchRequest_t2176192103_0_0_0_Types };
extern const Il2CppType Dictionary_2_t309261261_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t309261261_0_0_0_Types[] = { &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t309261261_0_0_0 = { 1, GenInst_Dictionary_2_t309261261_0_0_0_Types };
extern const Il2CppType Nullable_1_t3251239280_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t3251239280_0_0_0_Types[] = { &Nullable_1_t3251239280_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t3251239280_0_0_0 = { 1, GenInst_Nullable_1_t3251239280_0_0_0_Types };
extern const Il2CppType FacebookFriend_t3865174844_0_0_0;
static const Il2CppType* GenInst_FacebookFriend_t3865174844_0_0_0_Types[] = { &FacebookFriend_t3865174844_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookFriend_t3865174844_0_0_0 = { 1, GenInst_FacebookFriend_t3865174844_0_0_0_Types };
extern const Il2CppType P31Error_t2856600608_0_0_0;
static const Il2CppType* GenInst_P31Error_t2856600608_0_0_0_Types[] = { &P31Error_t2856600608_0_0_0 };
extern const Il2CppGenericInst GenInst_P31Error_t2856600608_0_0_0 = { 1, GenInst_P31Error_t2856600608_0_0_0_Types };
extern const Il2CppType RuntimePlatform_t1869584967_0_0_0;
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0 = { 1, GenInst_RuntimePlatform_t1869584967_0_0_0_Types };
extern const Il2CppType VectorLine_t3390220087_0_0_0;
static const Il2CppType* GenInst_VectorLine_t3390220087_0_0_0_Types[] = { &VectorLine_t3390220087_0_0_0 };
extern const Il2CppGenericInst GenInst_VectorLine_t3390220087_0_0_0 = { 1, GenInst_VectorLine_t3390220087_0_0_0_Types };
extern const Il2CppType Vector2U5BU5D_t686124026_0_0_0;
static const Il2CppType* GenInst_Vector2U5BU5D_t686124026_0_0_0_Types[] = { &Vector2U5BU5D_t686124026_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2U5BU5D_t686124026_0_0_0 = { 1, GenInst_Vector2U5BU5D_t686124026_0_0_0_Types };
extern const Il2CppType CapInfo_t3093272726_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0_Types[] = { &String_t_0_0_0, &CapInfo_t3093272726_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0 = { 2, GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &CapInfo_t3093272726_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_CapInfo_t3093272726_0_0_0_Types[] = { &CapInfo_t3093272726_0_0_0 };
extern const Il2CppGenericInst GenInst_CapInfo_t3093272726_0_0_0 = { 1, GenInst_CapInfo_t3093272726_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2765397210_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2765397210_0_0_0_Types[] = { &KeyValuePair_2_t2765397210_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2765397210_0_0_0 = { 1, GenInst_KeyValuePair_2_t2765397210_0_0_0_Types };
extern const Il2CppType VectorPoints_t440342714_0_0_0;
static const Il2CppType* GenInst_VectorPoints_t440342714_0_0_0_Types[] = { &VectorPoints_t440342714_0_0_0 };
extern const Il2CppGenericInst GenInst_VectorPoints_t440342714_0_0_0 = { 1, GenInst_VectorPoints_t440342714_0_0_0_Types };
extern const Il2CppType Vector3Pair_t2859078138_0_0_0;
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1020750381_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1020750381_0_0_0_Types[] = { &KeyValuePair_2_t1020750381_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1020750381_0_0_0 = { 1, GenInst_KeyValuePair_2_t1020750381_0_0_0_Types };
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0 = { 1, GenInst_Vector3Pair_t2859078138_0_0_0_Types };
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Vector3Pair_t2859078138_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0, &Boolean_t3825574718_0_0_0, &Vector3Pair_t2859078138_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Vector3Pair_t2859078138_0_0_0 = { 3, GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Vector3Pair_t2859078138_0_0_0_Types };
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1020750381_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1020750381_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1020750381_0_0_0 = { 3, GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1020750381_0_0_0_Types };
extern const Il2CppType RefInt_t2938871354_0_0_0;
static const Il2CppType* GenInst_RefInt_t2938871354_0_0_0_Types[] = { &RefInt_t2938871354_0_0_0 };
extern const Il2CppGenericInst GenInst_RefInt_t2938871354_0_0_0 = { 1, GenInst_RefInt_t2938871354_0_0_0_Types };
extern const Il2CppType Mesh_t1356156583_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0_Types[] = { &String_t_0_0_0, &Mesh_t1356156583_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0 = { 2, GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Mesh_t1356156583_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Mesh_t1356156583_0_0_0_Types[] = { &Mesh_t1356156583_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_t1356156583_0_0_0 = { 1, GenInst_Mesh_t1356156583_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1028281067_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1028281067_0_0_0_Types[] = { &KeyValuePair_2_t1028281067_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1028281067_0_0_0 = { 1, GenInst_KeyValuePair_2_t1028281067_0_0_0_Types };
extern const Il2CppType PurchasableItem_t3963353899_0_0_0;
static const Il2CppType* GenInst_PurchasableItem_t3963353899_0_0_0_Types[] = { &PurchasableItem_t3963353899_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchasableItem_t3963353899_0_0_0 = { 1, GenInst_PurchasableItem_t3963353899_0_0_0_Types };
static const Il2CppType* GenInst_PurchasableItem_t3963353899_0_0_0_String_t_0_0_0_Types[] = { &PurchasableItem_t3963353899_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchasableItem_t3963353899_0_0_0_String_t_0_0_0 = { 2, GenInst_PurchasableItem_t3963353899_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType PurchaseEvent_t743554429_0_0_0;
static const Il2CppType* GenInst_PurchaseEvent_t743554429_0_0_0_Types[] = { &PurchaseEvent_t743554429_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchaseEvent_t743554429_0_0_0 = { 1, GenInst_PurchaseEvent_t743554429_0_0_0_Types };
extern const Il2CppType PostParameter_t415229629_0_0_0;
static const Il2CppType* GenInst_PostParameter_t415229629_0_0_0_Types[] = { &PostParameter_t415229629_0_0_0 };
extern const Il2CppGenericInst GenInst_PostParameter_t415229629_0_0_0 = { 1, GenInst_PostParameter_t415229629_0_0_0_Types };
extern const Il2CppType UnibillError_t1753859787_0_0_0;
static const Il2CppType* GenInst_UnibillError_t1753859787_0_0_0_Types[] = { &UnibillError_t1753859787_0_0_0 };
extern const Il2CppGenericInst GenInst_UnibillError_t1753859787_0_0_0 = { 1, GenInst_UnibillError_t1753859787_0_0_0_Types };
static const Il2CppType* GenInst_PurchasableItem_t3963353899_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &PurchasableItem_t3963353899_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchasableItem_t3963353899_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_PurchasableItem_t3963353899_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_PurchasableItem_t3963353899_0_0_0_Product_t1158373411_0_0_0_Types[] = { &PurchasableItem_t3963353899_0_0_0, &Product_t1158373411_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchasableItem_t3963353899_0_0_0_Product_t1158373411_0_0_0 = { 2, GenInst_PurchasableItem_t3963353899_0_0_0_Product_t1158373411_0_0_0_Types };
extern const Il2CppType VirtualCurrency_t1115497880_0_0_0;
static const Il2CppType* GenInst_VirtualCurrency_t1115497880_0_0_0_String_t_0_0_0_Types[] = { &VirtualCurrency_t1115497880_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualCurrency_t1115497880_0_0_0_String_t_0_0_0 = { 2, GenInst_VirtualCurrency_t1115497880_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_VirtualCurrency_t1115497880_0_0_0_Types[] = { &VirtualCurrency_t1115497880_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualCurrency_t1115497880_0_0_0 = { 1, GenInst_VirtualCurrency_t1115497880_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0_Types[] = { &String_t_0_0_0, &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0 = { 2, GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2369073723_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2369073723_0_0_0_Types[] = { &KeyValuePair_2_t2369073723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2369073723_0_0_0 = { 1, GenInst_KeyValuePair_2_t2369073723_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Decimal_t724701077_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Decimal_t724701077_0_0_0, &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Decimal_t724701077_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_KeyValuePair_2_t2369073723_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Decimal_t724701077_0_0_0, &KeyValuePair_2_t2369073723_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_KeyValuePair_2_t2369073723_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_KeyValuePair_2_t2369073723_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Decimal_t724701077_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t396825561_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t396825561_0_0_0_Types[] = { &KeyValuePair_2_t396825561_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t396825561_0_0_0 = { 1, GenInst_KeyValuePair_2_t396825561_0_0_0_Types };
extern const Il2CppType PostRequest_t4132869030_0_0_0;
static const Il2CppType* GenInst_PostRequest_t4132869030_0_0_0_Types[] = { &PostRequest_t4132869030_0_0_0 };
extern const Il2CppGenericInst GenInst_PostRequest_t4132869030_0_0_0 = { 1, GenInst_PostRequest_t4132869030_0_0_0_Types };
extern const Il2CppType BillingPlatform_t552059234_0_0_0;
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0 = { 2, GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2335466038_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2335466038_0_0_0_Types[] = { &KeyValuePair_2_t2335466038_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2335466038_0_0_0 = { 1, GenInst_KeyValuePair_2_t2335466038_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0 = { 1, GenInst_BillingPlatform_t552059234_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_BillingPlatform_t552059234_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &Il2CppObject_0_0_0, &BillingPlatform_t552059234_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_BillingPlatform_t552059234_0_0_0 = { 3, GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_BillingPlatform_t552059234_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2335466038_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2335466038_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2335466038_0_0_0 = { 3, GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2335466038_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1675236976_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1675236976_0_0_0_Types[] = { &KeyValuePair_2_t1675236976_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1675236976_0_0_0 = { 1, GenInst_KeyValuePair_2_t1675236976_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0 = { 2, GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &Dictionary_2_t309261261_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4250245300_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4250245300_0_0_0_Types[] = { &KeyValuePair_2_t4250245300_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4250245300_0_0_0 = { 1, GenInst_KeyValuePair_2_t4250245300_0_0_0_Types };
extern const Il2CppType ProductDefinition_t1519653988_0_0_0;
static const Il2CppType* GenInst_ProductDefinition_t1519653988_0_0_0_Types[] = { &ProductDefinition_t1519653988_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductDefinition_t1519653988_0_0_0 = { 1, GenInst_ProductDefinition_t1519653988_0_0_0_Types };
extern const Il2CppType UnibillState_t4272135008_0_0_0;
static const Il2CppType* GenInst_UnibillState_t4272135008_0_0_0_Types[] = { &UnibillState_t4272135008_0_0_0 };
extern const Il2CppGenericInst GenInst_UnibillState_t4272135008_0_0_0 = { 1, GenInst_UnibillState_t4272135008_0_0_0_Types };
extern const Il2CppType DirectoryInfo_t1934446453_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DirectoryInfo_t1934446453_0_0_0_Types[] = { &String_t_0_0_0, &DirectoryInfo_t1934446453_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DirectoryInfo_t1934446453_0_0_0 = { 2, GenInst_String_t_0_0_0_DirectoryInfo_t1934446453_0_0_0_Types };
extern const Il2CppType HeaderValue_t822462144_0_0_0;
static const Il2CppType* GenInst_HeaderValue_t822462144_0_0_0_Types[] = { &HeaderValue_t822462144_0_0_0 };
extern const Il2CppGenericInst GenInst_HeaderValue_t822462144_0_0_0 = { 1, GenInst_HeaderValue_t822462144_0_0_0_Types };
extern const Il2CppType Digest_t59399582_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_Types[] = { &String_t_0_0_0, &Digest_t59399582_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Digest_t59399582_0_0_0 = { 2, GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Digest_t59399582_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Digest_t59399582_0_0_0_Types[] = { &Digest_t59399582_0_0_0 };
extern const Il2CppGenericInst GenInst_Digest_t59399582_0_0_0 = { 1, GenInst_Digest_t59399582_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4026491362_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4026491362_0_0_0_Types[] = { &KeyValuePair_2_t4026491362_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4026491362_0_0_0 = { 1, GenInst_KeyValuePair_2_t4026491362_0_0_0_Types };
extern const Il2CppType HTTPCacheFileInfo_t2858191078_0_0_0;
static const Il2CppType* GenInst_HTTPCacheFileInfo_t2858191078_0_0_0_Types[] = { &HTTPCacheFileInfo_t2858191078_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPCacheFileInfo_t2858191078_0_0_0 = { 1, GenInst_HTTPCacheFileInfo_t2858191078_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t1398341365_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t1398341365_0_0_0_Types[] = { &List_1_t1398341365_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1398341365_0_0_0 = { 1, GenInst_List_1_t1398341365_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1070465849_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1070465849_0_0_0_Types[] = { &KeyValuePair_2_t1070465849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1070465849_0_0_0 = { 1, GenInst_KeyValuePair_2_t1070465849_0_0_0_Types };
extern const Il2CppType Uri_t19570940_0_0_0;
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Types[] = { &Uri_t19570940_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0 = { 1, GenInst_Uri_t19570940_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3930922740_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3930922740_0_0_0_Types[] = { &KeyValuePair_2_t3930922740_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3930922740_0_0_0 = { 1, GenInst_KeyValuePair_2_t3930922740_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0 = { 2, GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types };
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Uri_t19570940_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4099664523_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4099664523_0_0_0_Types[] = { &KeyValuePair_2_t4099664523_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4099664523_0_0_0 = { 1, GenInst_KeyValuePair_2_t4099664523_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2387876582_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2387876582_0_0_0_Types[] = { &KeyValuePair_2_t2387876582_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387876582_0_0_0 = { 1, GenInst_KeyValuePair_2_t2387876582_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2387876582_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &HTTPCacheFileInfo_t2858191078_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2556618365_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2556618365_0_0_0_Types[] = { &KeyValuePair_2_t2556618365_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2556618365_0_0_0 = { 1, GenInst_KeyValuePair_2_t2556618365_0_0_0_Types };
extern const Il2CppType Stream_t3255436806_0_0_0;
static const Il2CppType* GenInst_Stream_t3255436806_0_0_0_Types[] = { &Stream_t3255436806_0_0_0 };
extern const Il2CppGenericInst GenInst_Stream_t3255436806_0_0_0 = { 1, GenInst_Stream_t3255436806_0_0_0_Types };
extern const Il2CppType Cookie_t4162804382_0_0_0;
static const Il2CppType* GenInst_Cookie_t4162804382_0_0_0_Types[] = { &Cookie_t4162804382_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t4162804382_0_0_0 = { 1, GenInst_Cookie_t4162804382_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Config_t3381668151_0_0_0;
static const Il2CppType* GenInst_Config_t3381668151_0_0_0_Types[] = { &Config_t3381668151_0_0_0 };
extern const Il2CppGenericInst GenInst_Config_t3381668151_0_0_0 = { 1, GenInst_Config_t3381668151_0_0_0_Types };
extern const Il2CppType Int32U5BU5D_t3030399641_0_0_0;
static const Il2CppType* GenInst_Int32U5BU5D_t3030399641_0_0_0_Types[] = { &Int32U5BU5D_t3030399641_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t3030399641_0_0_0 = { 1, GenInst_Int32U5BU5D_t3030399641_0_0_0_Types };
extern const Il2CppType IHeartbeat_t3217346319_0_0_0;
static const Il2CppType* GenInst_IHeartbeat_t3217346319_0_0_0_Types[] = { &IHeartbeat_t3217346319_0_0_0 };
extern const Il2CppGenericInst GenInst_IHeartbeat_t3217346319_0_0_0 = { 1, GenInst_IHeartbeat_t3217346319_0_0_0_Types };
extern const Il2CppType HTTPFieldData_t605100868_0_0_0;
static const Il2CppType* GenInst_HTTPFieldData_t605100868_0_0_0_Types[] = { &HTTPFieldData_t605100868_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPFieldData_t605100868_0_0_0 = { 1, GenInst_HTTPFieldData_t605100868_0_0_0_Types };
extern const Il2CppType List_1_t2151311861_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2151311861_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_Types };
extern const Il2CppType ConnectionBase_t2782190729_0_0_0;
static const Il2CppType* GenInst_ConnectionBase_t2782190729_0_0_0_Types[] = { &ConnectionBase_t2782190729_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionBase_t2782190729_0_0_0 = { 1, GenInst_ConnectionBase_t2782190729_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2151311861_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2151311861_0_0_0_Types[] = { &List_1_t2151311861_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2151311861_0_0_0 = { 1, GenInst_List_1_t2151311861_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1823436345_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1823436345_0_0_0_Types[] = { &KeyValuePair_2_t1823436345_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1823436345_0_0_0 = { 1, GenInst_KeyValuePair_2_t1823436345_0_0_0_Types };
extern const Il2CppType HTTPRequest_t138485887_0_0_0;
static const Il2CppType* GenInst_HTTPRequest_t138485887_0_0_0_Types[] = { &HTTPRequest_t138485887_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPRequest_t138485887_0_0_0 = { 1, GenInst_HTTPRequest_t138485887_0_0_0_Types };
extern const Il2CppType X509Chain_t777637347_0_0_0;
static const Il2CppType* GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &HTTPRequest_t138485887_0_0_0, &X509Certificate_t283079845_0_0_0, &X509Chain_t777637347_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0 = { 4, GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Asn1Encodable_t3447851422_0_0_0;
static const Il2CppType* GenInst_Asn1Encodable_t3447851422_0_0_0_Types[] = { &Asn1Encodable_t3447851422_0_0_0 };
extern const Il2CppGenericInst GenInst_Asn1Encodable_t3447851422_0_0_0 = { 1, GenInst_Asn1Encodable_t3447851422_0_0_0_Types };
extern const Il2CppType IAsn1Convertible_t983765413_0_0_0;
static const Il2CppType* GenInst_IAsn1Convertible_t983765413_0_0_0_Types[] = { &IAsn1Convertible_t983765413_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsn1Convertible_t983765413_0_0_0 = { 1, GenInst_IAsn1Convertible_t983765413_0_0_0_Types };
extern const Il2CppType DerEnumerated_t514019671_0_0_0;
static const Il2CppType* GenInst_DerEnumerated_t514019671_0_0_0_Types[] = { &DerEnumerated_t514019671_0_0_0 };
extern const Il2CppGenericInst GenInst_DerEnumerated_t514019671_0_0_0 = { 1, GenInst_DerEnumerated_t514019671_0_0_0_Types };
extern const Il2CppType Asn1Object_t564283626_0_0_0;
static const Il2CppType* GenInst_Asn1Object_t564283626_0_0_0_Types[] = { &Asn1Object_t564283626_0_0_0 };
extern const Il2CppGenericInst GenInst_Asn1Object_t564283626_0_0_0 = { 1, GenInst_Asn1Object_t564283626_0_0_0_Types };
extern const Il2CppType DerObjectIdentifier_t3495876513_0_0_0;
static const Il2CppType* GenInst_DerObjectIdentifier_t3495876513_0_0_0_Types[] = { &DerObjectIdentifier_t3495876513_0_0_0 };
extern const Il2CppGenericInst GenInst_DerObjectIdentifier_t3495876513_0_0_0 = { 1, GenInst_DerObjectIdentifier_t3495876513_0_0_0_Types };
extern const Il2CppType BigInteger_t4268922522_0_0_0;
static const Il2CppType* GenInst_BigInteger_t4268922522_0_0_0_Types[] = { &BigInteger_t4268922522_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t4268922522_0_0_0 = { 1, GenInst_BigInteger_t4268922522_0_0_0_Types };
extern const Il2CppType DistributionPoint_t769724552_0_0_0;
static const Il2CppType* GenInst_DistributionPoint_t769724552_0_0_0_Types[] = { &DistributionPoint_t769724552_0_0_0 };
extern const Il2CppGenericInst GenInst_DistributionPoint_t769724552_0_0_0 = { 1, GenInst_DistributionPoint_t769724552_0_0_0_Types };
extern const Il2CppType CrlEntry_t4200172927_0_0_0;
static const Il2CppType* GenInst_CrlEntry_t4200172927_0_0_0_Types[] = { &CrlEntry_t4200172927_0_0_0 };
extern const Il2CppGenericInst GenInst_CrlEntry_t4200172927_0_0_0 = { 1, GenInst_CrlEntry_t4200172927_0_0_0_Types };
extern const Il2CppType GeneralName_t294965175_0_0_0;
static const Il2CppType* GenInst_GeneralName_t294965175_0_0_0_Types[] = { &GeneralName_t294965175_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneralName_t294965175_0_0_0 = { 1, GenInst_GeneralName_t294965175_0_0_0_Types };
extern const Il2CppType IAsn1Choice_t4205079803_0_0_0;
static const Il2CppType* GenInst_IAsn1Choice_t4205079803_0_0_0_Types[] = { &IAsn1Choice_t4205079803_0_0_0 };
extern const Il2CppGenericInst GenInst_IAsn1Choice_t4205079803_0_0_0 = { 1, GenInst_IAsn1Choice_t4205079803_0_0_0_Types };
extern const Il2CppType UInt32U5BU5D_t59386216_0_0_0;
static const Il2CppType* GenInst_UInt32U5BU5D_t59386216_0_0_0_Types[] = { &UInt32U5BU5D_t59386216_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32U5BU5D_t59386216_0_0_0 = { 1, GenInst_UInt32U5BU5D_t59386216_0_0_0_Types };
extern const Il2CppType Int64U5BU5D_t717125112_0_0_0;
static const Il2CppType* GenInst_Int64U5BU5D_t717125112_0_0_0_Types[] = { &Int64U5BU5D_t717125112_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64U5BU5D_t717125112_0_0_0 = { 1, GenInst_Int64U5BU5D_t717125112_0_0_0_Types };
extern const Il2CppType UInt32U5BU5DU5BU5D_t1156922361_0_0_0;
static const Il2CppType* GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0_Types[] = { &UInt32U5BU5DU5BU5D_t1156922361_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0 = { 1, GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0_Types };
extern const Il2CppType ServerName_t2635557658_0_0_0;
static const Il2CppType* GenInst_ServerName_t2635557658_0_0_0_Types[] = { &ServerName_t2635557658_0_0_0 };
extern const Il2CppGenericInst GenInst_ServerName_t2635557658_0_0_0 = { 1, GenInst_ServerName_t2635557658_0_0_0_Types };
extern const Il2CppType X509CertificateStructure_t3705285294_0_0_0;
static const Il2CppType* GenInst_X509CertificateStructure_t3705285294_0_0_0_Types[] = { &X509CertificateStructure_t3705285294_0_0_0 };
extern const Il2CppGenericInst GenInst_X509CertificateStructure_t3705285294_0_0_0 = { 1, GenInst_X509CertificateStructure_t3705285294_0_0_0_Types };
extern const Il2CppType ECPoint_t626351532_0_0_0;
static const Il2CppType* GenInst_ECPoint_t626351532_0_0_0_Types[] = { &ECPoint_t626351532_0_0_0 };
extern const Il2CppGenericInst GenInst_ECPoint_t626351532_0_0_0 = { 1, GenInst_ECPoint_t626351532_0_0_0_Types };
extern const Il2CppType ECFieldElement_t1092946118_0_0_0;
static const Il2CppType* GenInst_ECFieldElement_t1092946118_0_0_0_Types[] = { &ECFieldElement_t1092946118_0_0_0 };
extern const Il2CppGenericInst GenInst_ECFieldElement_t1092946118_0_0_0 = { 1, GenInst_ECFieldElement_t1092946118_0_0_0_Types };
extern const Il2CppType WNafPreCompInfo_t485024160_0_0_0;
static const Il2CppType* GenInst_WNafPreCompInfo_t485024160_0_0_0_Types[] = { &WNafPreCompInfo_t485024160_0_0_0 };
extern const Il2CppGenericInst GenInst_WNafPreCompInfo_t485024160_0_0_0 = { 1, GenInst_WNafPreCompInfo_t485024160_0_0_0_Types };
extern const Il2CppType PreCompInfo_t1123315090_0_0_0;
static const Il2CppType* GenInst_PreCompInfo_t1123315090_0_0_0_Types[] = { &PreCompInfo_t1123315090_0_0_0 };
extern const Il2CppGenericInst GenInst_PreCompInfo_t1123315090_0_0_0 = { 1, GenInst_PreCompInfo_t1123315090_0_0_0_Types };
extern const Il2CppType LongArray_t194261203_0_0_0;
static const Il2CppType* GenInst_LongArray_t194261203_0_0_0_Types[] = { &LongArray_t194261203_0_0_0 };
extern const Il2CppGenericInst GenInst_LongArray_t194261203_0_0_0 = { 1, GenInst_LongArray_t194261203_0_0_0_Types };
extern const Il2CppType ZTauElement_t2571810054_0_0_0;
static const Il2CppType* GenInst_ZTauElement_t2571810054_0_0_0_Types[] = { &ZTauElement_t2571810054_0_0_0 };
extern const Il2CppGenericInst GenInst_ZTauElement_t2571810054_0_0_0 = { 1, GenInst_ZTauElement_t2571810054_0_0_0_Types };
extern const Il2CppType SByteU5BU5D_t3472287392_0_0_0;
static const Il2CppType* GenInst_SByteU5BU5D_t3472287392_0_0_0_Types[] = { &SByteU5BU5D_t3472287392_0_0_0 };
extern const Il2CppGenericInst GenInst_SByteU5BU5D_t3472287392_0_0_0 = { 1, GenInst_SByteU5BU5D_t3472287392_0_0_0_Types };
extern const Il2CppType AbstractF2mPoint_t883694769_0_0_0;
static const Il2CppType* GenInst_AbstractF2mPoint_t883694769_0_0_0_Types[] = { &AbstractF2mPoint_t883694769_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractF2mPoint_t883694769_0_0_0 = { 1, GenInst_AbstractF2mPoint_t883694769_0_0_0_Types };
extern const Il2CppType ECPointBase_t3119694375_0_0_0;
static const Il2CppType* GenInst_ECPointBase_t3119694375_0_0_0_Types[] = { &ECPointBase_t3119694375_0_0_0 };
extern const Il2CppGenericInst GenInst_ECPointBase_t3119694375_0_0_0 = { 1, GenInst_ECPointBase_t3119694375_0_0_0_Types };
extern const Il2CppType Config_t1249383685_0_0_0;
static const Il2CppType* GenInst_Config_t1249383685_0_0_0_Types[] = { &Config_t1249383685_0_0_0 };
extern const Il2CppGenericInst GenInst_Config_t1249383685_0_0_0 = { 1, GenInst_Config_t1249383685_0_0_0_Types };
extern const Il2CppType OnEventDelegate_t790674770_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_Types[] = { &String_t_0_0_0, &OnEventDelegate_t790674770_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0 = { 2, GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &OnEventDelegate_t790674770_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_OnEventDelegate_t790674770_0_0_0_Types[] = { &OnEventDelegate_t790674770_0_0_0 };
extern const Il2CppGenericInst GenInst_OnEventDelegate_t790674770_0_0_0 = { 1, GenInst_OnEventDelegate_t790674770_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t462799254_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t462799254_0_0_0_Types[] = { &KeyValuePair_2_t462799254_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t462799254_0_0_0 = { 1, GenInst_KeyValuePair_2_t462799254_0_0_0_Types };
extern const Il2CppType EventSourceResponse_t2287402344_0_0_0;
extern const Il2CppType Message_t1650395211_0_0_0;
static const Il2CppType* GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0_Types[] = { &EventSourceResponse_t2287402344_0_0_0, &Message_t1650395211_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0 = { 2, GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0_Types };
static const Il2CppType* GenInst_EventSourceResponse_t2287402344_0_0_0_Types[] = { &EventSourceResponse_t2287402344_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSourceResponse_t2287402344_0_0_0 = { 1, GenInst_EventSourceResponse_t2287402344_0_0_0_Types };
static const Il2CppType* GenInst_Message_t1650395211_0_0_0_Types[] = { &Message_t1650395211_0_0_0 };
extern const Il2CppGenericInst GenInst_Message_t1650395211_0_0_0 = { 1, GenInst_Message_t1650395211_0_0_0_Types };
extern const Il2CppType Hub_t272719679_0_0_0;
static const Il2CppType* GenInst_Hub_t272719679_0_0_0_Types[] = { &Hub_t272719679_0_0_0 };
extern const Il2CppGenericInst GenInst_Hub_t272719679_0_0_0 = { 1, GenInst_Hub_t272719679_0_0_0_Types };
extern const Il2CppType IHub_t3409721544_0_0_0;
static const Il2CppType* GenInst_IHub_t3409721544_0_0_0_Types[] = { &IHub_t3409721544_0_0_0 };
extern const Il2CppGenericInst GenInst_IHub_t3409721544_0_0_0 = { 1, GenInst_IHub_t3409721544_0_0_0_Types };
extern const Il2CppType IServerMessage_t2384143743_0_0_0;
static const Il2CppType* GenInst_IServerMessage_t2384143743_0_0_0_Types[] = { &IServerMessage_t2384143743_0_0_0 };
extern const Il2CppGenericInst GenInst_IServerMessage_t2384143743_0_0_0 = { 1, GenInst_IServerMessage_t2384143743_0_0_0_Types };
extern const Il2CppType NegotiationData_t3059020807_0_0_0;
static const Il2CppType* GenInst_NegotiationData_t3059020807_0_0_0_Types[] = { &NegotiationData_t3059020807_0_0_0 };
extern const Il2CppGenericInst GenInst_NegotiationData_t3059020807_0_0_0 = { 1, GenInst_NegotiationData_t3059020807_0_0_0_Types };
static const Il2CppType* GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0_Types[] = { &NegotiationData_t3059020807_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0 = { 2, GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ClientMessage_t624279968_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t322707255_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t322707255_0_0_0_Types[] = { &KeyValuePair_2_t322707255_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322707255_0_0_0 = { 1, GenInst_KeyValuePair_2_t322707255_0_0_0_Types };
static const Il2CppType* GenInst_ClientMessage_t624279968_0_0_0_Types[] = { &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientMessage_t624279968_0_0_0 = { 1, GenInst_ClientMessage_t624279968_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &ClientMessage_t624279968_0_0_0, &KeyValuePair_2_t322707255_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0 = { 3, GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types };
extern const Il2CppType OnMethodCallCallbackDelegate_t3483117754_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types[] = { &String_t_0_0_0, &OnMethodCallCallbackDelegate_t3483117754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0 = { 2, GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &OnMethodCallCallbackDelegate_t3483117754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types[] = { &OnMethodCallCallbackDelegate_t3483117754_0_0_0 };
extern const Il2CppGenericInst GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0 = { 1, GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3155242238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3155242238_0_0_0_Types[] = { &KeyValuePair_2_t3155242238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3155242238_0_0_0 = { 1, GenInst_KeyValuePair_2_t3155242238_0_0_0_Types };
extern const Il2CppType SocketIOCallback_t88619200_0_0_0;
static const Il2CppType* GenInst_SocketIOCallback_t88619200_0_0_0_Types[] = { &SocketIOCallback_t88619200_0_0_0 };
extern const Il2CppGenericInst GenInst_SocketIOCallback_t88619200_0_0_0 = { 1, GenInst_SocketIOCallback_t88619200_0_0_0_Types };
extern const Il2CppType List_1_t3426161967_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3426161967_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_Types };
extern const Il2CppType EventDescriptor_t4057040835_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t4057040835_0_0_0_Types[] = { &EventDescriptor_t4057040835_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t4057040835_0_0_0 = { 1, GenInst_EventDescriptor_t4057040835_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t3426161967_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t3426161967_0_0_0_Types[] = { &List_1_t3426161967_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3426161967_0_0_0 = { 1, GenInst_List_1_t3426161967_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3098286451_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3098286451_0_0_0_Types[] = { &KeyValuePair_2_t3098286451_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3098286451_0_0_0 = { 1, GenInst_KeyValuePair_2_t3098286451_0_0_0_Types };
extern const Il2CppType HandshakeData_t1703965475_0_0_0;
static const Il2CppType* GenInst_HandshakeData_t1703965475_0_0_0_Types[] = { &HandshakeData_t1703965475_0_0_0 };
extern const Il2CppGenericInst GenInst_HandshakeData_t1703965475_0_0_0 = { 1, GenInst_HandshakeData_t1703965475_0_0_0_Types };
static const Il2CppType* GenInst_HandshakeData_t1703965475_0_0_0_String_t_0_0_0_Types[] = { &HandshakeData_t1703965475_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_HandshakeData_t1703965475_0_0_0_String_t_0_0_0 = { 2, GenInst_HandshakeData_t1703965475_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0 = { 2, GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0_Types };
extern const Il2CppType SocketIOAckCallback_t53599143_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SocketIOAckCallback_t53599143_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &SocketIOAckCallback_t53599143_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_SocketIOAckCallback_t53599143_0_0_0_Types[] = { &SocketIOAckCallback_t53599143_0_0_0 };
extern const Il2CppGenericInst GenInst_SocketIOAckCallback_t53599143_0_0_0 = { 1, GenInst_SocketIOAckCallback_t53599143_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1113737296_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1113737296_0_0_0_Types[] = { &KeyValuePair_2_t1113737296_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1113737296_0_0_0 = { 1, GenInst_KeyValuePair_2_t1113737296_0_0_0_Types };
extern const Il2CppType Socket_t2716624701_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_Types[] = { &String_t_0_0_0, &Socket_t2716624701_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0 = { 2, GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Socket_t2716624701_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Socket_t2716624701_0_0_0_Types[] = { &Socket_t2716624701_0_0_0 };
extern const Il2CppGenericInst GenInst_Socket_t2716624701_0_0_0 = { 1, GenInst_Socket_t2716624701_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2388749185_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2388749185_0_0_0_Types[] = { &KeyValuePair_2_t2388749185_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2388749185_0_0_0 = { 1, GenInst_KeyValuePair_2_t2388749185_0_0_0_Types };
extern const Il2CppType Packet_t1309324146_0_0_0;
static const Il2CppType* GenInst_Packet_t1309324146_0_0_0_Types[] = { &Packet_t1309324146_0_0_0 };
extern const Il2CppGenericInst GenInst_Packet_t1309324146_0_0_0 = { 1, GenInst_Packet_t1309324146_0_0_0_Types };
extern const Il2CppType IExtension_t2171905938_0_0_0;
static const Il2CppType* GenInst_IExtension_t2171905938_0_0_0_Types[] = { &IExtension_t2171905938_0_0_0 };
extern const Il2CppGenericInst GenInst_IExtension_t2171905938_0_0_0 = { 1, GenInst_IExtension_t2171905938_0_0_0_Types };
extern const Il2CppType WebSocketFrame_t4163283394_0_0_0;
static const Il2CppType* GenInst_WebSocketFrame_t4163283394_0_0_0_Types[] = { &WebSocketFrame_t4163283394_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketFrame_t4163283394_0_0_0 = { 1, GenInst_WebSocketFrame_t4163283394_0_0_0_Types };
extern const Il2CppType WebSocketFrameReader_t549273869_0_0_0;
static const Il2CppType* GenInst_WebSocketFrameReader_t549273869_0_0_0_Types[] = { &WebSocketFrameReader_t549273869_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketFrameReader_t549273869_0_0_0 = { 1, GenInst_WebSocketFrameReader_t549273869_0_0_0_Types };
extern const Il2CppType WebSocketResponse_t3376763264_0_0_0;
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0 = { 2, GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0 = { 2, GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0_Types };
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &UInt16_t986882611_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0 = { 3, GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t986882611_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0_Types[] = { &WebSocketResponse_t3376763264_0_0_0, &WebSocketFrameReader_t549273869_0_0_0 };
extern const Il2CppGenericInst GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0 = { 2, GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0_Types };
extern const Il2CppType JsonData_t269267574_0_0_0;
static const Il2CppType* GenInst_JsonData_t269267574_0_0_0_Types[] = { &JsonData_t269267574_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonData_t269267574_0_0_0 = { 1, GenInst_JsonData_t269267574_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t269267574_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4236359354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4236359354_0_0_0_Types[] = { &KeyValuePair_2_t4236359354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4236359354_0_0_0 = { 1, GenInst_KeyValuePair_2_t4236359354_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &JsonData_t269267574_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType PropertyMetadata_t3693826136_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 2, GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1043231486_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1043231486_0_0_0_Types[] = { &KeyValuePair_2_t1043231486_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1043231486_0_0_0 = { 1, GenInst_KeyValuePair_2_t1043231486_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t3693826136_0_0_0_Types[] = { &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3693826136_0_0_0 = { 1, GenInst_PropertyMetadata_t3693826136_0_0_0_Types };
extern const Il2CppType ExporterFunc_t408878057_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t408878057_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0 = { 2, GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_Types };
static const Il2CppType* GenInst_ExporterFunc_t408878057_0_0_0_Types[] = { &ExporterFunc_t408878057_0_0_0 };
extern const Il2CppGenericInst GenInst_ExporterFunc_t408878057_0_0_0 = { 1, GenInst_ExporterFunc_t408878057_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2914292212_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t2914292212_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_Types };
extern const Il2CppType ImporterFunc_t2977850894_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t2977850894_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0 = { 2, GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_Types };
static const Il2CppType* GenInst_ImporterFunc_t2977850894_0_0_0_Types[] = { &ImporterFunc_t2977850894_0_0_0 };
extern const Il2CppGenericInst GenInst_ImporterFunc_t2977850894_0_0_0 = { 1, GenInst_ImporterFunc_t2977850894_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t2914292212_0_0_0_Types[] = { &IDictionary_2_t2914292212_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2914292212_0_0_0 = { 1, GenInst_IDictionary_2_t2914292212_0_0_0_Types };
extern const Il2CppType ArrayMetadata_t2008834462_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 2, GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3653207108_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3653207108_0_0_0_Types[] = { &KeyValuePair_2_t3653207108_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653207108_0_0_0 = { 1, GenInst_KeyValuePair_2_t3653207108_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t2008834462_0_0_0_Types[] = { &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t2008834462_0_0_0 = { 1, GenInst_ArrayMetadata_t2008834462_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3266987655_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3266987655_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3266987655_0_0_0_Types[] = { &IDictionary_2_t3266987655_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3266987655_0_0_0 = { 1, GenInst_IDictionary_2_t3266987655_0_0_0_Types };
extern const Il2CppType ObjectMetadata_t3995922398_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 2, GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1345327748_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1345327748_0_0_0_Types[] = { &KeyValuePair_2_t1345327748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1345327748_0_0_0 = { 1, GenInst_KeyValuePair_2_t1345327748_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t3995922398_0_0_0_Types[] = { &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t3995922398_0_0_0 = { 1, GenInst_ObjectMetadata_t3995922398_0_0_0_Types };
extern const Il2CppType IList_1_t4234766737_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t4234766737_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0 = { 2, GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_Types };
static const Il2CppType* GenInst_IList_1_t4234766737_0_0_0_Types[] = { &IList_1_t4234766737_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4234766737_0_0_0 = { 1, GenInst_IList_1_t4234766737_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &KeyValuePair_2_t3653207108_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ArrayMetadata_t2008834462_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1703537581_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1703537581_0_0_0_Types[] = { &KeyValuePair_2_t1703537581_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1703537581_0_0_0 = { 1, GenInst_KeyValuePair_2_t1703537581_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t3266987655_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2961690774_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2961690774_0_0_0_Types[] = { &KeyValuePair_2_t2961690774_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2961690774_0_0_0 = { 1, GenInst_KeyValuePair_2_t2961690774_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &KeyValuePair_2_t1345327748_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ObjectMetadata_t3995922398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3690625517_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3690625517_0_0_0_Types[] = { &KeyValuePair_2_t3690625517_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3690625517_0_0_0 = { 1, GenInst_KeyValuePair_2_t3690625517_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IList_1_t4234766737_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3929469856_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3929469856_0_0_0_Types[] = { &KeyValuePair_2_t3929469856_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3929469856_0_0_0 = { 1, GenInst_KeyValuePair_2_t3929469856_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ExporterFunc_t408878057_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t103581176_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t103581176_0_0_0_Types[] = { &KeyValuePair_2_t103581176_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t103581176_0_0_0 = { 1, GenInst_KeyValuePair_2_t103581176_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t2914292212_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2608995331_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2608995331_0_0_0_Types[] = { &KeyValuePair_2_t2608995331_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2608995331_0_0_0 = { 1, GenInst_KeyValuePair_2_t2608995331_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &KeyValuePair_2_t1043231486_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &PropertyMetadata_t3693826136_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3365950620_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3365950620_0_0_0_Types[] = { &KeyValuePair_2_t3365950620_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3365950620_0_0_0 = { 1, GenInst_KeyValuePair_2_t3365950620_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &MethodInfo_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3025249456_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3025249456_0_0_0_Types[] = { &KeyValuePair_2_t3025249456_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3025249456_0_0_0 = { 1, GenInst_KeyValuePair_2_t3025249456_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &ImporterFunc_t2977850894_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2672554013_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2672554013_0_0_0_Types[] = { &KeyValuePair_2_t2672554013_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2672554013_0_0_0 = { 1, GenInst_KeyValuePair_2_t2672554013_0_0_0_Types };
extern const Il2CppType IDictionary_2_t37308697_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IDictionary_2_t37308697_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32U5BU5D_t3030399641_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t37308697_0_0_0_Types[] = { &IDictionary_2_t37308697_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t37308697_0_0_0 = { 1, GenInst_IDictionary_2_t37308697_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &IDictionary_2_t37308697_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1097446850_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1097446850_0_0_0_Types[] = { &KeyValuePair_2_t1097446850_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1097446850_0_0_0 = { 1, GenInst_KeyValuePair_2_t1097446850_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32U5BU5D_t3030399641_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4090537794_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4090537794_0_0_0_Types[] = { &KeyValuePair_2_t4090537794_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4090537794_0_0_0 = { 1, GenInst_KeyValuePair_2_t4090537794_0_0_0_Types };
extern const Il2CppType WriterContext_t4137194742_0_0_0;
static const Il2CppType* GenInst_WriterContext_t4137194742_0_0_0_Types[] = { &WriterContext_t4137194742_0_0_0 };
extern const Il2CppGenericInst GenInst_WriterContext_t4137194742_0_0_0 = { 1, GenInst_WriterContext_t4137194742_0_0_0_Types };
extern const Il2CppType StateHandler_t387387051_0_0_0;
static const Il2CppType* GenInst_StateHandler_t387387051_0_0_0_Types[] = { &StateHandler_t387387051_0_0_0 };
extern const Il2CppGenericInst GenInst_StateHandler_t387387051_0_0_0 = { 1, GenInst_StateHandler_t387387051_0_0_0_Types };
extern const Il2CppType MulticastDelegate_t3201952435_0_0_0;
static const Il2CppType* GenInst_MulticastDelegate_t3201952435_0_0_0_Types[] = { &MulticastDelegate_t3201952435_0_0_0 };
extern const Il2CppGenericInst GenInst_MulticastDelegate_t3201952435_0_0_0 = { 1, GenInst_MulticastDelegate_t3201952435_0_0_0_Types };
extern const Il2CppType SampleDescriptor_t3285910703_0_0_0;
static const Il2CppType* GenInst_SampleDescriptor_t3285910703_0_0_0_Types[] = { &SampleDescriptor_t3285910703_0_0_0 };
extern const Il2CppGenericInst GenInst_SampleDescriptor_t3285910703_0_0_0 = { 1, GenInst_SampleDescriptor_t3285910703_0_0_0_Types };
extern const Il2CppType MessageTypes_t223757197_0_0_0;
static const Il2CppType* GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0_Types[] = { &MessageTypes_t223757197_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0 = { 2, GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0_Types[] = { &MessageTypes_t223757197_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypes_t223757197_0_0_0_Types[] = { &MessageTypes_t223757197_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypes_t223757197_0_0_0 = { 1, GenInst_MessageTypes_t223757197_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType1_2_t377947286_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &U3CU3E__AnonType1_2_t377947286_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3943999495_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3943999495_0_0_0_Types[] = { &Dictionary_2_t3943999495_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3943999495_0_0_0 = { 1, GenInst_Dictionary_2_t3943999495_0_0_0_Types };
extern const Il2CppType OnlineMapsTile_t21329940_0_0_0;
static const Il2CppType* GenInst_OnlineMapsTile_t21329940_0_0_0_Types[] = { &OnlineMapsTile_t21329940_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsTile_t21329940_0_0_0 = { 1, GenInst_OnlineMapsTile_t21329940_0_0_0_Types };
extern const Il2CppType OnlineMapsMarker_t3492166682_0_0_0;
static const Il2CppType* GenInst_OnlineMapsMarker_t3492166682_0_0_0_Types[] = { &OnlineMapsMarker_t3492166682_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker_t3492166682_0_0_0 = { 1, GenInst_OnlineMapsMarker_t3492166682_0_0_0_Types };
extern const Il2CppType OnlineMapsMarkerBase_t3900955221_0_0_0;
static const Il2CppType* GenInst_OnlineMapsMarkerBase_t3900955221_0_0_0_Types[] = { &OnlineMapsMarkerBase_t3900955221_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarkerBase_t3900955221_0_0_0 = { 1, GenInst_OnlineMapsMarkerBase_t3900955221_0_0_0_Types };
extern const Il2CppType OnlineMapsFindAutocompleteResult_t2619648090_0_0_0;
static const Il2CppType* GenInst_OnlineMapsFindAutocompleteResult_t2619648090_0_0_0_Types[] = { &OnlineMapsFindAutocompleteResult_t2619648090_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsFindAutocompleteResult_t2619648090_0_0_0 = { 1, GenInst_OnlineMapsFindAutocompleteResult_t2619648090_0_0_0_Types };
extern const Il2CppType OnlineMapsDirectionStep_t2252483185_0_0_0;
static const Il2CppType* GenInst_OnlineMapsDirectionStep_t2252483185_0_0_0_Types[] = { &OnlineMapsDirectionStep_t2252483185_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsDirectionStep_t2252483185_0_0_0 = { 1, GenInst_OnlineMapsDirectionStep_t2252483185_0_0_0_Types };
extern const Il2CppType OnlineMapsFindPlacesResult_t686120750_0_0_0;
static const Il2CppType* GenInst_OnlineMapsFindPlacesResult_t686120750_0_0_0_Types[] = { &OnlineMapsFindPlacesResult_t686120750_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsFindPlacesResult_t686120750_0_0_0 = { 1, GenInst_OnlineMapsFindPlacesResult_t686120750_0_0_0_Types };
extern const Il2CppType OnlineMapsGetElevationResult_t3115336078_0_0_0;
static const Il2CppType* GenInst_OnlineMapsGetElevationResult_t3115336078_0_0_0_Types[] = { &OnlineMapsGetElevationResult_t3115336078_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsGetElevationResult_t3115336078_0_0_0 = { 1, GenInst_OnlineMapsGetElevationResult_t3115336078_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType OnlineMapsOSMTag_t3629071465_0_0_0;
static const Il2CppType* GenInst_OnlineMapsOSMTag_t3629071465_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &OnlineMapsOSMTag_t3629071465_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsOSMTag_t3629071465_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_OnlineMapsOSMTag_t3629071465_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType OnlineMapsOSMNode_t3383990403_0_0_0;
static const Il2CppType* GenInst_OnlineMapsOSMNode_t3383990403_0_0_0_Types[] = { &OnlineMapsOSMNode_t3383990403_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsOSMNode_t3383990403_0_0_0 = { 1, GenInst_OnlineMapsOSMNode_t3383990403_0_0_0_Types };
extern const Il2CppType OnlineMapsOSMWay_t3319895272_0_0_0;
static const Il2CppType* GenInst_OnlineMapsOSMWay_t3319895272_0_0_0_Types[] = { &OnlineMapsOSMWay_t3319895272_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsOSMWay_t3319895272_0_0_0 = { 1, GenInst_OnlineMapsOSMWay_t3319895272_0_0_0_Types };
extern const Il2CppType OnlineMapsOSMRelation_t1871982797_0_0_0;
static const Il2CppType* GenInst_OnlineMapsOSMRelation_t1871982797_0_0_0_Types[] = { &OnlineMapsOSMRelation_t1871982797_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsOSMRelation_t1871982797_0_0_0 = { 1, GenInst_OnlineMapsOSMRelation_t1871982797_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsOSMTag_t3629071465_0_0_0_Types[] = { &OnlineMapsOSMTag_t3629071465_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsOSMTag_t3629071465_0_0_0 = { 1, GenInst_OnlineMapsOSMTag_t3629071465_0_0_0_Types };
extern const Il2CppType OnlineMapsMarker3D_t576815539_0_0_0;
static const Il2CppType* GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Types[] = { &OnlineMapsMarker3D_t576815539_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker3D_t576815539_0_0_0 = { 1, GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Types };
extern const Il2CppType TilesetFadeExampleItem_t1982028563_0_0_0;
static const Il2CppType* GenInst_TilesetFadeExampleItem_t1982028563_0_0_0_Types[] = { &TilesetFadeExampleItem_t1982028563_0_0_0 };
extern const Il2CppGenericInst GenInst_TilesetFadeExampleItem_t1982028563_0_0_0 = { 1, GenInst_TilesetFadeExampleItem_t1982028563_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsTile_t21329940_0_0_0_Material_t193706927_0_0_0_Types[] = { &OnlineMapsTile_t21329940_0_0_0, &Material_t193706927_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsTile_t21329940_0_0_0_Material_t193706927_0_0_0 = { 2, GenInst_OnlineMapsTile_t21329940_0_0_0_Material_t193706927_0_0_0_Types };
static const Il2CppType* GenInst_TilesetFadeExampleItem_t1982028563_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &TilesetFadeExampleItem_t1982028563_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_TilesetFadeExampleItem_t1982028563_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_TilesetFadeExampleItem_t1982028563_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType MapManager_t3593696545_0_0_0;
static const Il2CppType* GenInst_MapManager_t3593696545_0_0_0_Types[] = { &MapManager_t3593696545_0_0_0 };
extern const Il2CppGenericInst GenInst_MapManager_t3593696545_0_0_0 = { 1, GenInst_MapManager_t3593696545_0_0_0_Types };
extern const Il2CppType OnlineMapsDrawingElement_t539447654_0_0_0;
static const Il2CppType* GenInst_OnlineMapsDrawingElement_t539447654_0_0_0_Types[] = { &OnlineMapsDrawingElement_t539447654_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsDrawingElement_t539447654_0_0_0 = { 1, GenInst_OnlineMapsDrawingElement_t539447654_0_0_0_Types };
extern const Il2CppType OnlineMapsEvents_t850667009_0_0_0;
static const Il2CppType* GenInst_OnlineMapsEvents_t850667009_0_0_0_Types[] = { &OnlineMapsEvents_t850667009_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsEvents_t850667009_0_0_0 = { 1, GenInst_OnlineMapsEvents_t850667009_0_0_0_Types };
extern const Il2CppType OnlineMapsMarkerBillboard_t495103289_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &OnlineMapsMarkerBillboard_t495103289_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &OnlineMapsMarkerBillboard_t495103289_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsMarkerBillboard_t495103289_0_0_0_Types[] = { &OnlineMapsMarkerBillboard_t495103289_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarkerBillboard_t495103289_0_0_0 = { 1, GenInst_OnlineMapsMarkerBillboard_t495103289_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1555241442_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1555241442_0_0_0_Types[] = { &KeyValuePair_2_t1555241442_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1555241442_0_0_0 = { 1, GenInst_KeyValuePair_2_t1555241442_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &OnlineMapsMarker3D_t576815539_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType TilesetFlatMarker_t3550885087_0_0_0;
static const Il2CppType* GenInst_TilesetFlatMarker_t3550885087_0_0_0_Types[] = { &TilesetFlatMarker_t3550885087_0_0_0 };
extern const Il2CppGenericInst GenInst_TilesetFlatMarker_t3550885087_0_0_0 = { 1, GenInst_TilesetFlatMarker_t3550885087_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int16_t4041245914_0_0_0_Types[] = { &String_t_0_0_0, &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int16_t4041245914_0_0_0 = { 2, GenInst_String_t_0_0_0_Int16_t4041245914_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsMarker_t3492166682_0_0_0_OnlineMapsMarker_t3492166682_0_0_0_Types[] = { &OnlineMapsMarker_t3492166682_0_0_0, &OnlineMapsMarker_t3492166682_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker_t3492166682_0_0_0_OnlineMapsMarker_t3492166682_0_0_0 = { 2, GenInst_OnlineMapsMarker_t3492166682_0_0_0_OnlineMapsMarker_t3492166682_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsMarker_t3492166682_0_0_0_Single_t2076509932_0_0_0_Types[] = { &OnlineMapsMarker_t3492166682_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker_t3492166682_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_OnlineMapsMarker_t3492166682_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType TilesetSortedMarker_t2480816995_0_0_0;
static const Il2CppType* GenInst_TilesetSortedMarker_t2480816995_0_0_0_Single_t2076509932_0_0_0_Types[] = { &TilesetSortedMarker_t2480816995_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_TilesetSortedMarker_t2480816995_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_TilesetSortedMarker_t2480816995_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_TilesetSortedMarker_t2480816995_0_0_0_Types[] = { &TilesetSortedMarker_t2480816995_0_0_0 };
extern const Il2CppGenericInst GenInst_TilesetSortedMarker_t2480816995_0_0_0 = { 1, GenInst_TilesetSortedMarker_t2480816995_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsMarker_t3492166682_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &OnlineMapsMarker_t3492166682_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker_t3492166682_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_OnlineMapsMarker_t3492166682_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType OnlineMapsFindAutocompleteResultTerm_t3836860852_0_0_0;
static const Il2CppType* GenInst_OnlineMapsFindAutocompleteResultTerm_t3836860852_0_0_0_Types[] = { &OnlineMapsFindAutocompleteResultTerm_t3836860852_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsFindAutocompleteResultTerm_t3836860852_0_0_0 = { 1, GenInst_OnlineMapsFindAutocompleteResultTerm_t3836860852_0_0_0_Types };
extern const Il2CppType OnlineMapsFindPlacesResultPhoto_t2950683050_0_0_0;
static const Il2CppType* GenInst_OnlineMapsFindPlacesResultPhoto_t2950683050_0_0_0_Types[] = { &OnlineMapsFindPlacesResultPhoto_t2950683050_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsFindPlacesResultPhoto_t2950683050_0_0_0 = { 1, GenInst_OnlineMapsFindPlacesResultPhoto_t2950683050_0_0_0_Types };
extern const Il2CppType OnlineMapsGoogleAPIQuery_t356009153_0_0_0;
static const Il2CppType* GenInst_OnlineMapsGoogleAPIQuery_t356009153_0_0_0_Types[] = { &OnlineMapsGoogleAPIQuery_t356009153_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsGoogleAPIQuery_t356009153_0_0_0 = { 1, GenInst_OnlineMapsGoogleAPIQuery_t356009153_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0_Types[] = { &String_t_0_0_0, &OnlineMapsOSMNode_t3383990403_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0 = { 2, GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &OnlineMapsOSMNode_t3383990403_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3056114887_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3056114887_0_0_0_Types[] = { &KeyValuePair_2_t3056114887_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3056114887_0_0_0 = { 1, GenInst_KeyValuePair_2_t3056114887_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsOSMNode_t3383990403_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &OnlineMapsOSMNode_t3383990403_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsOSMNode_t3383990403_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_OnlineMapsOSMNode_t3383990403_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType OnlineMapsOSMRelationMember_t1040319881_0_0_0;
static const Il2CppType* GenInst_OnlineMapsOSMRelationMember_t1040319881_0_0_0_Types[] = { &OnlineMapsOSMRelationMember_t1040319881_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsOSMRelationMember_t1040319881_0_0_0 = { 1, GenInst_OnlineMapsOSMRelationMember_t1040319881_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsTile_t21329940_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &OnlineMapsTile_t21329940_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsTile_t21329940_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_OnlineMapsTile_t21329940_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsTile_t21329940_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &OnlineMapsTile_t21329940_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsTile_t21329940_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_OnlineMapsTile_t21329940_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsDrawingElement_t539447654_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &OnlineMapsDrawingElement_t539447654_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsDrawingElement_t539447654_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_OnlineMapsDrawingElement_t539447654_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType OnlineMapsBufferZoom_t2072536377_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &OnlineMapsBufferZoom_t2072536377_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &OnlineMapsBufferZoom_t2072536377_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsBufferZoom_t2072536377_0_0_0_Types[] = { &OnlineMapsBufferZoom_t2072536377_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsBufferZoom_t2072536377_0_0_0 = { 1, GenInst_OnlineMapsBufferZoom_t2072536377_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132674530_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132674530_0_0_0_Types[] = { &KeyValuePair_2_t3132674530_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132674530_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132674530_0_0_0_Types };
extern const Il2CppType OnlineMapsBuildingBase_t650727021_0_0_0;
static const Il2CppType* GenInst_OnlineMapsBuildingBase_t650727021_0_0_0_Types[] = { &OnlineMapsBuildingBase_t650727021_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsBuildingBase_t650727021_0_0_0 = { 1, GenInst_OnlineMapsBuildingBase_t650727021_0_0_0_Types };
extern const Il2CppType OnlineMapsBuildingMetaInfo_t1480818351_0_0_0;
static const Il2CppType* GenInst_OnlineMapsBuildingMetaInfo_t1480818351_0_0_0_Types[] = { &OnlineMapsBuildingMetaInfo_t1480818351_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsBuildingMetaInfo_t1480818351_0_0_0 = { 1, GenInst_OnlineMapsBuildingMetaInfo_t1480818351_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 3, GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType OnlineMapsBuildingMaterial_t302108695_0_0_0;
static const Il2CppType* GenInst_OnlineMapsBuildingMaterial_t302108695_0_0_0_Types[] = { &OnlineMapsBuildingMaterial_t302108695_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsBuildingMaterial_t302108695_0_0_0 = { 1, GenInst_OnlineMapsBuildingMaterial_t302108695_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Byte_t3683104436_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Byte_t3683104436_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0_Types[] = { &String_t_0_0_0, &OnlineMapsBuildingBase_t650727021_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0 = { 2, GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &OnlineMapsBuildingBase_t650727021_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t322851505_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t322851505_0_0_0_Types[] = { &KeyValuePair_2_t322851505_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322851505_0_0_0 = { 1, GenInst_KeyValuePair_2_t322851505_0_0_0_Types };
extern const Il2CppType OnlineMapsBuildingsNodeData_t1471864431_0_0_0;
static const Il2CppType* GenInst_OnlineMapsBuildingsNodeData_t1471864431_0_0_0_Types[] = { &OnlineMapsBuildingsNodeData_t1471864431_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsBuildingsNodeData_t1471864431_0_0_0 = { 1, GenInst_OnlineMapsBuildingsNodeData_t1471864431_0_0_0_Types };
extern const Il2CppType List_1_t1621604317_0_0_0;
static const Il2CppType* GenInst_List_1_t1621604317_0_0_0_Types[] = { &List_1_t1621604317_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1621604317_0_0_0 = { 1, GenInst_List_1_t1621604317_0_0_0_Types };
extern const Il2CppType Component_t3519836246_0_0_0;
static const Il2CppType* GenInst_Component_t3519836246_0_0_0_Types[] = { &Component_t3519836246_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3519836246_0_0_0 = { 1, GenInst_Component_t3519836246_0_0_0_Types };
extern const Il2CppType CodeU5BU5D_t4050074539_0_0_0;
static const Il2CppType* GenInst_CodeU5BU5D_t4050074539_0_0_0_Types[] = { &CodeU5BU5D_t4050074539_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeU5BU5D_t4050074539_0_0_0 = { 1, GenInst_CodeU5BU5D_t4050074539_0_0_0_Types };
extern const Il2CppType Code_t3472511518_0_0_0;
static const Il2CppType* GenInst_Code_t3472511518_0_0_0_Types[] = { &Code_t3472511518_0_0_0 };
extern const Il2CppGenericInst GenInst_Code_t3472511518_0_0_0 = { 1, GenInst_Code_t3472511518_0_0_0_Types };
extern const Il2CppType OnlineMapsXML_t3341520387_0_0_0;
static const Il2CppType* GenInst_OnlineMapsXML_t3341520387_0_0_0_Types[] = { &OnlineMapsXML_t3341520387_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsXML_t3341520387_0_0_0 = { 1, GenInst_OnlineMapsXML_t3341520387_0_0_0_Types };
extern const Il2CppType EventDelegate_t3496309181_0_0_0;
static const Il2CppType* GenInst_EventDelegate_t3496309181_0_0_0_Types[] = { &EventDelegate_t3496309181_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDelegate_t3496309181_0_0_0 = { 1, GenInst_EventDelegate_t3496309181_0_0_0_Types };
extern const Il2CppType FadeEntry_t3041229383_0_0_0;
static const Il2CppType* GenInst_FadeEntry_t3041229383_0_0_0_Types[] = { &FadeEntry_t3041229383_0_0_0 };
extern const Il2CppGenericInst GenInst_FadeEntry_t3041229383_0_0_0 = { 1, GenInst_FadeEntry_t3041229383_0_0_0_Types };
extern const Il2CppType UIButton_t3377238306_0_0_0;
static const Il2CppType* GenInst_UIButton_t3377238306_0_0_0_Types[] = { &UIButton_t3377238306_0_0_0 };
extern const Il2CppGenericInst GenInst_UIButton_t3377238306_0_0_0 = { 1, GenInst_UIButton_t3377238306_0_0_0_Types };
extern const Il2CppType UIButtonColor_t3793385709_0_0_0;
static const Il2CppType* GenInst_UIButtonColor_t3793385709_0_0_0_Types[] = { &UIButtonColor_t3793385709_0_0_0 };
extern const Il2CppGenericInst GenInst_UIButtonColor_t3793385709_0_0_0 = { 1, GenInst_UIButtonColor_t3793385709_0_0_0_Types };
extern const Il2CppType UIWidgetContainer_t701016325_0_0_0;
static const Il2CppType* GenInst_UIWidgetContainer_t701016325_0_0_0_Types[] = { &UIWidgetContainer_t701016325_0_0_0 };
extern const Il2CppGenericInst GenInst_UIWidgetContainer_t701016325_0_0_0 = { 1, GenInst_UIWidgetContainer_t701016325_0_0_0_Types };
extern const Il2CppType UIDragDropItem_t4109477862_0_0_0;
static const Il2CppType* GenInst_UIDragDropItem_t4109477862_0_0_0_Types[] = { &UIDragDropItem_t4109477862_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDragDropItem_t4109477862_0_0_0 = { 1, GenInst_UIDragDropItem_t4109477862_0_0_0_Types };
extern const Il2CppType UIKeyBinding_t790450850_0_0_0;
static const Il2CppType* GenInst_UIKeyBinding_t790450850_0_0_0_Types[] = { &UIKeyBinding_t790450850_0_0_0 };
extern const Il2CppGenericInst GenInst_UIKeyBinding_t790450850_0_0_0 = { 1, GenInst_UIKeyBinding_t790450850_0_0_0_Types };
extern const Il2CppType UIKeyNavigation_t1158973079_0_0_0;
static const Il2CppType* GenInst_UIKeyNavigation_t1158973079_0_0_0_Types[] = { &UIKeyNavigation_t1158973079_0_0_0 };
extern const Il2CppGenericInst GenInst_UIKeyNavigation_t1158973079_0_0_0 = { 1, GenInst_UIKeyNavigation_t1158973079_0_0_0_Types };
extern const Il2CppType UITweener_t2986641582_0_0_0;
static const Il2CppType* GenInst_UITweener_t2986641582_0_0_0_Types[] = { &UITweener_t2986641582_0_0_0 };
extern const Il2CppGenericInst GenInst_UITweener_t2986641582_0_0_0 = { 1, GenInst_UITweener_t2986641582_0_0_0_Types };
extern const Il2CppType UILabel_t1795115428_0_0_0;
static const Il2CppType* GenInst_UILabel_t1795115428_0_0_0_Types[] = { &UILabel_t1795115428_0_0_0 };
extern const Il2CppGenericInst GenInst_UILabel_t1795115428_0_0_0 = { 1, GenInst_UILabel_t1795115428_0_0_0_Types };
extern const Il2CppType UIPlaySound_t2984775557_0_0_0;
static const Il2CppType* GenInst_UIPlaySound_t2984775557_0_0_0_Types[] = { &UIPlaySound_t2984775557_0_0_0 };
extern const Il2CppGenericInst GenInst_UIPlaySound_t2984775557_0_0_0 = { 1, GenInst_UIPlaySound_t2984775557_0_0_0_Types };
extern const Il2CppType UIWidget_t1453041918_0_0_0;
static const Il2CppType* GenInst_UIWidget_t1453041918_0_0_0_Types[] = { &UIWidget_t1453041918_0_0_0 };
extern const Il2CppGenericInst GenInst_UIWidget_t1453041918_0_0_0 = { 1, GenInst_UIWidget_t1453041918_0_0_0_Types };
extern const Il2CppType UIRect_t4127168124_0_0_0;
static const Il2CppType* GenInst_UIRect_t4127168124_0_0_0_Types[] = { &UIRect_t4127168124_0_0_0 };
extern const Il2CppGenericInst GenInst_UIRect_t4127168124_0_0_0 = { 1, GenInst_UIRect_t4127168124_0_0_0_Types };
extern const Il2CppType Collider_t3497673348_0_0_0;
static const Il2CppType* GenInst_Collider_t3497673348_0_0_0_Types[] = { &Collider_t3497673348_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t3497673348_0_0_0 = { 1, GenInst_Collider_t3497673348_0_0_0_Types };
extern const Il2CppType UIToggle_t3036740318_0_0_0;
static const Il2CppType* GenInst_UIToggle_t3036740318_0_0_0_Types[] = { &UIToggle_t3036740318_0_0_0 };
extern const Il2CppGenericInst GenInst_UIToggle_t3036740318_0_0_0 = { 1, GenInst_UIToggle_t3036740318_0_0_0_Types };
extern const Il2CppType UIScrollView_t3033954930_0_0_0;
static const Il2CppType* GenInst_UIScrollView_t3033954930_0_0_0_Types[] = { &UIScrollView_t3033954930_0_0_0 };
extern const Il2CppGenericInst GenInst_UIScrollView_t3033954930_0_0_0 = { 1, GenInst_UIScrollView_t3033954930_0_0_0_Types };
extern const Il2CppType Bounds_t3033363703_0_0_0;
static const Il2CppType* GenInst_Bounds_t3033363703_0_0_0_Types[] = { &Bounds_t3033363703_0_0_0 };
extern const Il2CppGenericInst GenInst_Bounds_t3033363703_0_0_0 = { 1, GenInst_Bounds_t3033363703_0_0_0_Types };
extern const Il2CppType UIPanel_t1795085332_0_0_0;
static const Il2CppType* GenInst_UIPanel_t1795085332_0_0_0_Types[] = { &UIPanel_t1795085332_0_0_0 };
extern const Il2CppGenericInst GenInst_UIPanel_t1795085332_0_0_0 = { 1, GenInst_UIPanel_t1795085332_0_0_0_Types };
extern const Il2CppType BMGlyph_t3903496831_0_0_0;
static const Il2CppType* GenInst_BMGlyph_t3903496831_0_0_0_Types[] = { &BMGlyph_t3903496831_0_0_0 };
extern const Il2CppGenericInst GenInst_BMGlyph_t3903496831_0_0_0 = { 1, GenInst_BMGlyph_t3903496831_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &BMGlyph_t3903496831_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &BMGlyph_t3903496831_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t668667688_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t668667688_0_0_0_Types[] = { &KeyValuePair_2_t668667688_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t668667688_0_0_0 = { 1, GenInst_KeyValuePair_2_t668667688_0_0_0_Types };
extern const Il2CppType Parameter_t3376276275_0_0_0;
static const Il2CppType* GenInst_Parameter_t3376276275_0_0_0_Types[] = { &Parameter_t3376276275_0_0_0 };
extern const Il2CppGenericInst GenInst_Parameter_t3376276275_0_0_0 = { 1, GenInst_Parameter_t3376276275_0_0_0_Types };
extern const Il2CppType StringU5BU5D_t1642385972_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0_Types[] = { &String_t_0_0_0, &StringU5BU5D_t1642385972_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0 = { 2, GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &StringU5BU5D_t1642385972_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_StringU5BU5D_t1642385972_0_0_0_Types[] = { &StringU5BU5D_t1642385972_0_0_0 };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0 = { 1, GenInst_StringU5BU5D_t1642385972_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1314510456_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1314510456_0_0_0_Types[] = { &KeyValuePair_2_t1314510456_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1314510456_0_0_0 = { 1, GenInst_KeyValuePair_2_t1314510456_0_0_0_Types };
extern const Il2CppType KeyCode_t2283395152_0_0_0;
static const Il2CppType* GenInst_KeyCode_t2283395152_0_0_0_Types[] = { &KeyCode_t2283395152_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2283395152_0_0_0 = { 1, GenInst_KeyCode_t2283395152_0_0_0_Types };
extern const Il2CppType AudioListener_t1996719162_0_0_0;
static const Il2CppType* GenInst_AudioListener_t1996719162_0_0_0_Types[] = { &AudioListener_t1996719162_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioListener_t1996719162_0_0_0 = { 1, GenInst_AudioListener_t1996719162_0_0_0_Types };
extern const Il2CppType UICamera_t1496819779_0_0_0;
static const Il2CppType* GenInst_UICamera_t1496819779_0_0_0_Types[] = { &UICamera_t1496819779_0_0_0 };
extern const Il2CppGenericInst GenInst_UICamera_t1496819779_0_0_0 = { 1, GenInst_UICamera_t1496819779_0_0_0_Types };
extern const Il2CppType UIRoot_t389944298_0_0_0;
static const Il2CppType* GenInst_UIRoot_t389944298_0_0_0_Types[] = { &UIRoot_t389944298_0_0_0 };
extern const Il2CppGenericInst GenInst_UIRoot_t389944298_0_0_0 = { 1, GenInst_UIRoot_t389944298_0_0_0_Types };
extern const Il2CppType UIDrawCall_t3291843512_0_0_0;
static const Il2CppType* GenInst_UIDrawCall_t3291843512_0_0_0_Types[] = { &UIDrawCall_t3291843512_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDrawCall_t3291843512_0_0_0 = { 1, GenInst_UIDrawCall_t3291843512_0_0_0_Types };
extern const Il2CppType UISpriteData_t2862501359_0_0_0;
static const Il2CppType* GenInst_UISpriteData_t2862501359_0_0_0_Types[] = { &UISpriteData_t2862501359_0_0_0 };
extern const Il2CppGenericInst GenInst_UISpriteData_t2862501359_0_0_0 = { 1, GenInst_UISpriteData_t2862501359_0_0_0_Types };
extern const Il2CppType Sprite_t134787095_0_0_0;
static const Il2CppType* GenInst_Sprite_t134787095_0_0_0_Types[] = { &Sprite_t134787095_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t134787095_0_0_0 = { 1, GenInst_Sprite_t134787095_0_0_0_Types };
extern const Il2CppType UISprite_t603616735_0_0_0;
static const Il2CppType* GenInst_UISprite_t603616735_0_0_0_Types[] = { &UISprite_t603616735_0_0_0 };
extern const Il2CppGenericInst GenInst_UISprite_t603616735_0_0_0 = { 1, GenInst_UISprite_t603616735_0_0_0_Types };
extern const Il2CppType UIBasicSprite_t754925213_0_0_0;
static const Il2CppType* GenInst_UIBasicSprite_t754925213_0_0_0_Types[] = { &UIBasicSprite_t754925213_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBasicSprite_t754925213_0_0_0 = { 1, GenInst_UIBasicSprite_t754925213_0_0_0_Types };
extern const Il2CppType UIFont_t389944949_0_0_0;
static const Il2CppType* GenInst_UIFont_t389944949_0_0_0_Types[] = { &UIFont_t389944949_0_0_0 };
extern const Il2CppGenericInst GenInst_UIFont_t389944949_0_0_0 = { 1, GenInst_UIFont_t389944949_0_0_0_Types };
extern const Il2CppType MouseOrTouch_t2470076277_0_0_0;
static const Il2CppType* GenInst_MouseOrTouch_t2470076277_0_0_0_Types[] = { &MouseOrTouch_t2470076277_0_0_0 };
extern const Il2CppGenericInst GenInst_MouseOrTouch_t2470076277_0_0_0 = { 1, GenInst_MouseOrTouch_t2470076277_0_0_0_Types };
extern const Il2CppType DepthEntry_t974746545_0_0_0;
static const Il2CppType* GenInst_DepthEntry_t974746545_0_0_0_Types[] = { &DepthEntry_t974746545_0_0_0 };
extern const Il2CppGenericInst GenInst_DepthEntry_t974746545_0_0_0 = { 1, GenInst_DepthEntry_t974746545_0_0_0_Types };
extern const Il2CppType BMSymbol_t1865486779_0_0_0;
static const Il2CppType* GenInst_BMSymbol_t1865486779_0_0_0_Types[] = { &BMSymbol_t1865486779_0_0_0 };
extern const Il2CppGenericInst GenInst_BMSymbol_t1865486779_0_0_0 = { 1, GenInst_BMSymbol_t1865486779_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4232308010_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4232308010_0_0_0_Types[] = { &KeyValuePair_2_t4232308010_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4232308010_0_0_0 = { 1, GenInst_KeyValuePair_2_t4232308010_0_0_0_Types };
extern const Il2CppType Paragraph_t2587095060_0_0_0;
static const Il2CppType* GenInst_Paragraph_t2587095060_0_0_0_Types[] = { &Paragraph_t2587095060_0_0_0 };
extern const Il2CppGenericInst GenInst_Paragraph_t2587095060_0_0_0 = { 1, GenInst_Paragraph_t2587095060_0_0_0_Types };
extern const Il2CppType BetterList_1_t2807483702_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0_Types[] = { &String_t_0_0_0, &BetterList_1_t2807483702_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0 = { 2, GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &BetterList_1_t2807483702_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_BetterList_1_t2807483702_0_0_0_Types[] = { &BetterList_1_t2807483702_0_0_0 };
extern const Il2CppGenericInst GenInst_BetterList_1_t2807483702_0_0_0 = { 1, GenInst_BetterList_1_t2807483702_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2479608186_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2479608186_0_0_0_Types[] = { &KeyValuePair_2_t2479608186_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2479608186_0_0_0 = { 1, GenInst_KeyValuePair_2_t2479608186_0_0_0_Types };
extern const Il2CppType UITexture_t2537039969_0_0_0;
static const Il2CppType* GenInst_UITexture_t2537039969_0_0_0_Types[] = { &UITexture_t2537039969_0_0_0 };
extern const Il2CppGenericInst GenInst_UITexture_t2537039969_0_0_0 = { 1, GenInst_UITexture_t2537039969_0_0_0_Types };
extern const Il2CppType ApplicationManager_t2110631419_0_0_0;
static const Il2CppType* GenInst_ApplicationManager_t2110631419_0_0_0_Types[] = { &ApplicationManager_t2110631419_0_0_0 };
extern const Il2CppGenericInst GenInst_ApplicationManager_t2110631419_0_0_0 = { 1, GenInst_ApplicationManager_t2110631419_0_0_0_Types };
extern const Il2CppType ChatmageddonAudioManager_t766956996_0_0_0;
static const Il2CppType* GenInst_ChatmageddonAudioManager_t766956996_0_0_0_Types[] = { &ChatmageddonAudioManager_t766956996_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonAudioManager_t766956996_0_0_0 = { 1, GenInst_ChatmageddonAudioManager_t766956996_0_0_0_Types };
extern const Il2CppType SFXSource_t383013662_0_0_0;
static const Il2CppType* GenInst_SFXSource_t383013662_0_0_0_Types[] = { &SFXSource_t383013662_0_0_0 };
extern const Il2CppGenericInst GenInst_SFXSource_t383013662_0_0_0 = { 1, GenInst_SFXSource_t383013662_0_0_0_Types };
extern const Il2CppType MusicSource_t904181206_0_0_0;
static const Il2CppType* GenInst_MusicSource_t904181206_0_0_0_Types[] = { &MusicSource_t904181206_0_0_0 };
extern const Il2CppGenericInst GenInst_MusicSource_t904181206_0_0_0 = { 1, GenInst_MusicSource_t904181206_0_0_0_Types };
extern const Il2CppType ChatmageddonFBManager_t234400784_0_0_0;
static const Il2CppType* GenInst_ChatmageddonFBManager_t234400784_0_0_0_Types[] = { &ChatmageddonFBManager_t234400784_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonFBManager_t234400784_0_0_0 = { 1, GenInst_ChatmageddonFBManager_t234400784_0_0_0_Types };
extern const Il2CppType Hashtable_t909839986_0_0_0;
static const Il2CppType* GenInst_Hashtable_t909839986_0_0_0_Types[] = { &Hashtable_t909839986_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t909839986_0_0_0 = { 1, GenInst_Hashtable_t909839986_0_0_0_Types };
extern const Il2CppType InternetReachability_t3219127626_0_0_0;
static const Il2CppType* GenInst_InternetReachability_t3219127626_0_0_0_Types[] = { &InternetReachability_t3219127626_0_0_0 };
extern const Il2CppGenericInst GenInst_InternetReachability_t3219127626_0_0_0 = { 1, GenInst_InternetReachability_t3219127626_0_0_0_Types };
extern const Il2CppType LockoutManager_t4122842952_0_0_0;
static const Il2CppType* GenInst_LockoutManager_t4122842952_0_0_0_Types[] = { &LockoutManager_t4122842952_0_0_0 };
extern const Il2CppGenericInst GenInst_LockoutManager_t4122842952_0_0_0 = { 1, GenInst_LockoutManager_t4122842952_0_0_0_Types };
extern const Il2CppType GlobalManagersController_t3516162077_0_0_0;
static const Il2CppType* GenInst_GlobalManagersController_t3516162077_0_0_0_Types[] = { &GlobalManagersController_t3516162077_0_0_0 };
extern const Il2CppGenericInst GenInst_GlobalManagersController_t3516162077_0_0_0 = { 1, GenInst_GlobalManagersController_t3516162077_0_0_0_Types };
extern const Il2CppType PlayerManager_t1596653588_0_0_0;
static const Il2CppType* GenInst_PlayerManager_t1596653588_0_0_0_Types[] = { &PlayerManager_t1596653588_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerManager_t1596653588_0_0_0 = { 1, GenInst_PlayerManager_t1596653588_0_0_0_Types };
extern const Il2CppType FeedManager_t2709865823_0_0_0;
static const Il2CppType* GenInst_FeedManager_t2709865823_0_0_0_Types[] = { &FeedManager_t2709865823_0_0_0 };
extern const Il2CppGenericInst GenInst_FeedManager_t2709865823_0_0_0 = { 1, GenInst_FeedManager_t2709865823_0_0_0_Types };
extern const Il2CppType NotificationManager_t2388475022_0_0_0;
static const Il2CppType* GenInst_NotificationManager_t2388475022_0_0_0_Types[] = { &NotificationManager_t2388475022_0_0_0 };
extern const Il2CppGenericInst GenInst_NotificationManager_t2388475022_0_0_0 = { 1, GenInst_NotificationManager_t2388475022_0_0_0_Types };
extern const Il2CppType InventoryManager_t38269895_0_0_0;
static const Il2CppType* GenInst_InventoryManager_t38269895_0_0_0_Types[] = { &InventoryManager_t38269895_0_0_0 };
extern const Il2CppGenericInst GenInst_InventoryManager_t38269895_0_0_0 = { 1, GenInst_InventoryManager_t38269895_0_0_0_Types };
extern const Il2CppType ItemsManager_t2342504123_0_0_0;
static const Il2CppType* GenInst_ItemsManager_t2342504123_0_0_0_Types[] = { &ItemsManager_t2342504123_0_0_0 };
extern const Il2CppGenericInst GenInst_ItemsManager_t2342504123_0_0_0 = { 1, GenInst_ItemsManager_t2342504123_0_0_0_Types };
extern const Il2CppType ModalManager_t4136513312_0_0_0;
static const Il2CppType* GenInst_ModalManager_t4136513312_0_0_0_Types[] = { &ModalManager_t4136513312_0_0_0 };
extern const Il2CppGenericInst GenInst_ModalManager_t4136513312_0_0_0 = { 1, GenInst_ModalManager_t4136513312_0_0_0_Types };
extern const Il2CppType ShieldManager_t3551654236_0_0_0;
static const Il2CppType* GenInst_ShieldManager_t3551654236_0_0_0_Types[] = { &ShieldManager_t3551654236_0_0_0 };
extern const Il2CppGenericInst GenInst_ShieldManager_t3551654236_0_0_0 = { 1, GenInst_ShieldManager_t3551654236_0_0_0_Types };
extern const Il2CppType MineManager_t2821846902_0_0_0;
static const Il2CppType* GenInst_MineManager_t2821846902_0_0_0_Types[] = { &MineManager_t2821846902_0_0_0 };
extern const Il2CppGenericInst GenInst_MineManager_t2821846902_0_0_0 = { 1, GenInst_MineManager_t2821846902_0_0_0_Types };
extern const Il2CppType ChatmageddonSaveData_t4036050876_0_0_0;
static const Il2CppType* GenInst_ChatmageddonSaveData_t4036050876_0_0_0_Types[] = { &ChatmageddonSaveData_t4036050876_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonSaveData_t4036050876_0_0_0 = { 1, GenInst_ChatmageddonSaveData_t4036050876_0_0_0_Types };
extern const Il2CppType BuildManager_t19508555_0_0_0;
static const Il2CppType* GenInst_BuildManager_t19508555_0_0_0_Types[] = { &BuildManager_t19508555_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildManager_t19508555_0_0_0 = { 1, GenInst_BuildManager_t19508555_0_0_0_Types };
extern const Il2CppType ChatmageddonGamestate_t808689860_0_0_0;
static const Il2CppType* GenInst_ChatmageddonGamestate_t808689860_0_0_0_Types[] = { &ChatmageddonGamestate_t808689860_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonGamestate_t808689860_0_0_0 = { 1, GenInst_ChatmageddonGamestate_t808689860_0_0_0_Types };
extern const Il2CppType ChatmageddonServer_t594474938_0_0_0;
static const Il2CppType* GenInst_ChatmageddonServer_t594474938_0_0_0_Types[] = { &ChatmageddonServer_t594474938_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonServer_t594474938_0_0_0 = { 1, GenInst_ChatmageddonServer_t594474938_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Hashtable_t909839986_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Hashtable_t909839986_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Hashtable_t909839986_0_0_0_String_t_0_0_0 = { 3, GenInst_Boolean_t3825574718_0_0_0_Hashtable_t909839986_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType HomeSceneManager_t2690266116_0_0_0;
static const Il2CppType* GenInst_HomeSceneManager_t2690266116_0_0_0_Types[] = { &HomeSceneManager_t2690266116_0_0_0 };
extern const Il2CppGenericInst GenInst_HomeSceneManager_t2690266116_0_0_0 = { 1, GenInst_HomeSceneManager_t2690266116_0_0_0_Types };
extern const Il2CppType SettingsSceneManager_t3860594814_0_0_0;
static const Il2CppType* GenInst_SettingsSceneManager_t3860594814_0_0_0_Types[] = { &SettingsSceneManager_t3860594814_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsSceneManager_t3860594814_0_0_0 = { 1, GenInst_SettingsSceneManager_t3860594814_0_0_0_Types };
extern const Il2CppType ChatSceneManager_t555354597_0_0_0;
static const Il2CppType* GenInst_ChatSceneManager_t555354597_0_0_0_Types[] = { &ChatSceneManager_t555354597_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatSceneManager_t555354597_0_0_0 = { 1, GenInst_ChatSceneManager_t555354597_0_0_0_Types };
extern const Il2CppType LeaderboardSceneManager_t2883297036_0_0_0;
static const Il2CppType* GenInst_LeaderboardSceneManager_t2883297036_0_0_0_Types[] = { &LeaderboardSceneManager_t2883297036_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardSceneManager_t2883297036_0_0_0 = { 1, GenInst_LeaderboardSceneManager_t2883297036_0_0_0_Types };
extern const Il2CppType LoginRegistrationSceneManager_t2327503985_0_0_0;
static const Il2CppType* GenInst_LoginRegistrationSceneManager_t2327503985_0_0_0_Types[] = { &LoginRegistrationSceneManager_t2327503985_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginRegistrationSceneManager_t2327503985_0_0_0 = { 1, GenInst_LoginRegistrationSceneManager_t2327503985_0_0_0_Types };
extern const Il2CppType LoginSuccessSceneManager_t2204684339_0_0_0;
static const Il2CppType* GenInst_LoginSuccessSceneManager_t2204684339_0_0_0_Types[] = { &LoginSuccessSceneManager_t2204684339_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginSuccessSceneManager_t2204684339_0_0_0 = { 1, GenInst_LoginSuccessSceneManager_t2204684339_0_0_0_Types };
extern const Il2CppType ToastManager_t1002293494_0_0_0;
static const Il2CppType* GenInst_ToastManager_t1002293494_0_0_0_Types[] = { &ToastManager_t1002293494_0_0_0 };
extern const Il2CppGenericInst GenInst_ToastManager_t1002293494_0_0_0 = { 1, GenInst_ToastManager_t1002293494_0_0_0_Types };
extern const Il2CppType ConfigManager_t2239702727_0_0_0;
static const Il2CppType* GenInst_ConfigManager_t2239702727_0_0_0_Types[] = { &ConfigManager_t2239702727_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigManager_t2239702727_0_0_0 = { 1, GenInst_ConfigManager_t2239702727_0_0_0_Types };
extern const Il2CppType StatusListItem_t459202613_0_0_0;
static const Il2CppType* GenInst_StatusListItem_t459202613_0_0_0_Types[] = { &StatusListItem_t459202613_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusListItem_t459202613_0_0_0 = { 1, GenInst_StatusListItem_t459202613_0_0_0_Types };
extern const Il2CppType ChatManager_t2792590695_0_0_0;
static const Il2CppType* GenInst_ChatManager_t2792590695_0_0_0_Types[] = { &ChatManager_t2792590695_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatManager_t2792590695_0_0_0 = { 1, GenInst_ChatManager_t2792590695_0_0_0_Types };
extern const Il2CppType StatusNavigationController_t3357829236_0_0_0;
static const Il2CppType* GenInst_StatusNavigationController_t3357829236_0_0_0_Types[] = { &StatusNavigationController_t3357829236_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusNavigationController_t3357829236_0_0_0 = { 1, GenInst_StatusNavigationController_t3357829236_0_0_0_Types };
extern const Il2CppType FriendHomeNavigationController_t1091974581_0_0_0;
static const Il2CppType* GenInst_FriendHomeNavigationController_t1091974581_0_0_0_Types[] = { &FriendHomeNavigationController_t1091974581_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendHomeNavigationController_t1091974581_0_0_0 = { 1, GenInst_FriendHomeNavigationController_t1091974581_0_0_0_Types };
extern const Il2CppType AttackHomeNavigationController_t3047766999_0_0_0;
static const Il2CppType* GenInst_AttackHomeNavigationController_t3047766999_0_0_0_Types[] = { &AttackHomeNavigationController_t3047766999_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackHomeNavigationController_t3047766999_0_0_0 = { 1, GenInst_AttackHomeNavigationController_t3047766999_0_0_0_Types };
extern const Il2CppType UIAtlas_t1304615221_0_0_0;
static const Il2CppType* GenInst_UIAtlas_t1304615221_0_0_0_Types[] = { &UIAtlas_t1304615221_0_0_0 };
extern const Il2CppGenericInst GenInst_UIAtlas_t1304615221_0_0_0 = { 1, GenInst_UIAtlas_t1304615221_0_0_0_Types };
extern const Il2CppType SettingsManager_t2519859232_0_0_0;
static const Il2CppType* GenInst_SettingsManager_t2519859232_0_0_0_Types[] = { &SettingsManager_t2519859232_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsManager_t2519859232_0_0_0 = { 1, GenInst_SettingsManager_t2519859232_0_0_0_Types };
extern const Il2CppType SettingsNavigationController_t2581944457_0_0_0;
static const Il2CppType* GenInst_SettingsNavigationController_t2581944457_0_0_0_Types[] = { &SettingsNavigationController_t2581944457_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsNavigationController_t2581944457_0_0_0 = { 1, GenInst_SettingsNavigationController_t2581944457_0_0_0_Types };
extern const Il2CppType ChatThread_t2394323482_0_0_0;
static const Il2CppType* GenInst_ChatThread_t2394323482_0_0_0_Types[] = { &ChatThread_t2394323482_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatThread_t2394323482_0_0_0 = { 1, GenInst_ChatThread_t2394323482_0_0_0_Types };
extern const Il2CppType Friend_t3555014108_0_0_0;
static const Il2CppType* GenInst_Friend_t3555014108_0_0_0_Types[] = { &Friend_t3555014108_0_0_0 };
extern const Il2CppGenericInst GenInst_Friend_t3555014108_0_0_0 = { 1, GenInst_Friend_t3555014108_0_0_0_Types };
extern const Il2CppType ChatMessage_t2384228687_0_0_0;
static const Il2CppType* GenInst_ChatMessage_t2384228687_0_0_0_Types[] = { &ChatMessage_t2384228687_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatMessage_t2384228687_0_0_0 = { 1, GenInst_ChatMessage_t2384228687_0_0_0_Types };
static const Il2CppType* GenInst_ChatMessage_t2384228687_0_0_0_DateTime_t693205669_0_0_0_Types[] = { &ChatMessage_t2384228687_0_0_0, &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatMessage_t2384228687_0_0_0_DateTime_t693205669_0_0_0 = { 2, GenInst_ChatMessage_t2384228687_0_0_0_DateTime_t693205669_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DateTime_t693205669_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DateTime_t693205669_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType Audience_t3183951146_0_0_0;
static const Il2CppType* GenInst_Audience_t3183951146_0_0_0_Types[] = { &Audience_t3183951146_0_0_0 };
extern const Il2CppGenericInst GenInst_Audience_t3183951146_0_0_0 = { 1, GenInst_Audience_t3183951146_0_0_0_Types };
extern const Il2CppType Gender_t3618070753_0_0_0;
static const Il2CppType* GenInst_Gender_t3618070753_0_0_0_Types[] = { &Gender_t3618070753_0_0_0 };
extern const Il2CppGenericInst GenInst_Gender_t3618070753_0_0_0 = { 1, GenInst_Gender_t3618070753_0_0_0_Types };
extern const Il2CppType ValidContact_t1479914934_0_0_0;
static const Il2CppType* GenInst_ValidContact_t1479914934_0_0_0_Types[] = { &ValidContact_t1479914934_0_0_0 };
extern const Il2CppGenericInst GenInst_ValidContact_t1479914934_0_0_0 = { 1, GenInst_ValidContact_t1479914934_0_0_0_Types };
static const Il2CppType* GenInst_ValidContact_t1479914934_0_0_0_String_t_0_0_0_Types[] = { &ValidContact_t1479914934_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ValidContact_t1479914934_0_0_0_String_t_0_0_0 = { 2, GenInst_ValidContact_t1479914934_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_ValidContact_t1479914934_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &ValidContact_t1479914934_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_ValidContact_t1479914934_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_ValidContact_t1479914934_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &List_1_t1398341365_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &List_1_t1398341365_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2458479518_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2458479518_0_0_0_Types[] = { &KeyValuePair_2_t2458479518_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2458479518_0_0_0 = { 1, GenInst_KeyValuePair_2_t2458479518_0_0_0_Types };
extern const Il2CppType AttackManager_t3475553125_0_0_0;
static const Il2CppType* GenInst_AttackManager_t3475553125_0_0_0_Types[] = { &AttackManager_t3475553125_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackManager_t3475553125_0_0_0 = { 1, GenInst_AttackManager_t3475553125_0_0_0_Types };
extern const Il2CppType FriendManager_t36140827_0_0_0;
static const Il2CppType* GenInst_FriendManager_t36140827_0_0_0_Types[] = { &FriendManager_t36140827_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendManager_t36140827_0_0_0 = { 1, GenInst_FriendManager_t36140827_0_0_0_Types };
extern const Il2CppType DeviceCameraManager_t1478748484_0_0_0;
static const Il2CppType* GenInst_DeviceCameraManager_t1478748484_0_0_0_Types[] = { &DeviceCameraManager_t1478748484_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceCameraManager_t1478748484_0_0_0 = { 1, GenInst_DeviceCameraManager_t1478748484_0_0_0_Types };
extern const Il2CppType RegistrationManager_t1533517116_0_0_0;
static const Il2CppType* GenInst_RegistrationManager_t1533517116_0_0_0_Types[] = { &RegistrationManager_t1533517116_0_0_0 };
extern const Il2CppGenericInst GenInst_RegistrationManager_t1533517116_0_0_0 = { 1, GenInst_RegistrationManager_t1533517116_0_0_0_Types };
extern const Il2CppType ChatmageddonDeviceShareManager_t740212713_0_0_0;
static const Il2CppType* GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0_Types[] = { &ChatmageddonDeviceShareManager_t740212713_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0 = { 1, GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0_Types };
extern const Il2CppType ShareRequestLocation_t2965642179_0_0_0;
static const Il2CppType* GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0_ShareRequestLocation_t2965642179_0_0_0_Types[] = { &ChatmageddonDeviceShareManager_t740212713_0_0_0, &ShareRequestLocation_t2965642179_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0_ShareRequestLocation_t2965642179_0_0_0 = { 2, GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0_ShareRequestLocation_t2965642179_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ShareRequestLocation_t2965642179_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ShareRequestLocation_t2965642179_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ShareRequestLocation_t2965642179_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ShareRequestLocation_t2965642179_0_0_0_Types };
extern const Il2CppType ChatmageddonTwitterManager_t3244794565_0_0_0;
static const Il2CppType* GenInst_ChatmageddonTwitterManager_t3244794565_0_0_0_Types[] = { &ChatmageddonTwitterManager_t3244794565_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonTwitterManager_t3244794565_0_0_0 = { 1, GenInst_ChatmageddonTwitterManager_t3244794565_0_0_0_Types };
extern const Il2CppType FlurryController_t2385863972_0_0_0;
static const Il2CppType* GenInst_FlurryController_t2385863972_0_0_0_Types[] = { &FlurryController_t2385863972_0_0_0 };
extern const Il2CppGenericInst GenInst_FlurryController_t2385863972_0_0_0 = { 1, GenInst_FlurryController_t2385863972_0_0_0_Types };
extern const Il2CppType FormatDateTime_t1158606858_0_0_0;
static const Il2CppType* GenInst_FormatDateTime_t1158606858_0_0_0_Types[] = { &FormatDateTime_t1158606858_0_0_0 };
extern const Il2CppGenericInst GenInst_FormatDateTime_t1158606858_0_0_0 = { 1, GenInst_FormatDateTime_t1158606858_0_0_0_Types };
extern const Il2CppType ChatmageddonPurchaseManager_t1221205491_0_0_0;
static const Il2CppType* GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0_Types[] = { &ChatmageddonPurchaseManager_t1221205491_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0 = { 1, GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0_Types };
extern const Il2CppType ShopPurchasableItems_t522663808_0_0_0;
static const Il2CppType* GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0_ShopPurchasableItems_t522663808_0_0_0_Types[] = { &ChatmageddonPurchaseManager_t1221205491_0_0_0, &ShopPurchasableItems_t522663808_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0_ShopPurchasableItems_t522663808_0_0_0 = { 2, GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0_ShopPurchasableItems_t522663808_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ShopPurchasableItems_t522663808_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ShopPurchasableItems_t522663808_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ShopPurchasableItems_t522663808_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ShopPurchasableItems_t522663808_0_0_0_Types };
extern const Il2CppType UnibillManager_t3725383138_0_0_0;
static const Il2CppType* GenInst_UnibillManager_t3725383138_0_0_0_Types[] = { &UnibillManager_t3725383138_0_0_0 };
extern const Il2CppGenericInst GenInst_UnibillManager_t3725383138_0_0_0 = { 1, GenInst_UnibillManager_t3725383138_0_0_0_Types };
extern const Il2CppType Bucks_t3932015720_0_0_0;
static const Il2CppType* GenInst_Bucks_t3932015720_0_0_0_Types[] = { &Bucks_t3932015720_0_0_0 };
extern const Il2CppGenericInst GenInst_Bucks_t3932015720_0_0_0 = { 1, GenInst_Bucks_t3932015720_0_0_0_Types };
extern const Il2CppType Missile_t813944928_0_0_0;
static const Il2CppType* GenInst_Missile_t813944928_0_0_0_Types[] = { &Missile_t813944928_0_0_0 };
extern const Il2CppGenericInst GenInst_Missile_t813944928_0_0_0 = { 1, GenInst_Missile_t813944928_0_0_0_Types };
extern const Il2CppType Shield_t3327121081_0_0_0;
static const Il2CppType* GenInst_Shield_t3327121081_0_0_0_Types[] = { &Shield_t3327121081_0_0_0 };
extern const Il2CppGenericInst GenInst_Shield_t3327121081_0_0_0 = { 1, GenInst_Shield_t3327121081_0_0_0_Types };
extern const Il2CppType Mine_t2729441277_0_0_0;
static const Il2CppType* GenInst_Mine_t2729441277_0_0_0_Types[] = { &Mine_t2729441277_0_0_0 };
extern const Il2CppGenericInst GenInst_Mine_t2729441277_0_0_0 = { 1, GenInst_Mine_t2729441277_0_0_0_Types };
extern const Il2CppType Fuel_t1015546524_0_0_0;
static const Il2CppType* GenInst_Fuel_t1015546524_0_0_0_Types[] = { &Fuel_t1015546524_0_0_0 };
extern const Il2CppGenericInst GenInst_Fuel_t1015546524_0_0_0 = { 1, GenInst_Fuel_t1015546524_0_0_0_Types };
extern const Il2CppType PurchaseableItem_t3351122996_0_0_0;
static const Il2CppType* GenInst_PurchaseableItem_t3351122996_0_0_0_Types[] = { &PurchaseableItem_t3351122996_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchaseableItem_t3351122996_0_0_0 = { 1, GenInst_PurchaseableItem_t3351122996_0_0_0_Types };
extern const Il2CppType QuantitativeItem_t3036513780_0_0_0;
static const Il2CppType* GenInst_QuantitativeItem_t3036513780_0_0_0_Types[] = { &QuantitativeItem_t3036513780_0_0_0 };
extern const Il2CppGenericInst GenInst_QuantitativeItem_t3036513780_0_0_0 = { 1, GenInst_QuantitativeItem_t3036513780_0_0_0_Types };
extern const Il2CppType LoadingPopUp_t1016279926_0_0_0;
static const Il2CppType* GenInst_LoadingPopUp_t1016279926_0_0_0_Types[] = { &LoadingPopUp_t1016279926_0_0_0 };
extern const Il2CppGenericInst GenInst_LoadingPopUp_t1016279926_0_0_0 = { 1, GenInst_LoadingPopUp_t1016279926_0_0_0_Types };
extern const Il2CppType ChatmageddonPanelTransitionManager_t1399015365_0_0_0;
static const Il2CppType* GenInst_ChatmageddonPanelTransitionManager_t1399015365_0_0_0_Types[] = { &ChatmageddonPanelTransitionManager_t1399015365_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatmageddonPanelTransitionManager_t1399015365_0_0_0 = { 1, GenInst_ChatmageddonPanelTransitionManager_t1399015365_0_0_0_Types };
extern const Il2CppType PanelType_t482769230_0_0_0;
extern const Il2CppType Panel_t1787746694_0_0_0;
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &Panel_t1787746694_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0 = { 2, GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1603735834_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1603735834_0_0_0_Types[] = { &KeyValuePair_2_t1603735834_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1603735834_0_0_0 = { 1, GenInst_KeyValuePair_2_t1603735834_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Types[] = { &PanelType_t482769230_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0 = { 1, GenInst_PanelType_t482769230_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_PanelType_t482769230_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &Il2CppObject_0_0_0, &PanelType_t482769230_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_PanelType_t482769230_0_0_0 = { 3, GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_PanelType_t482769230_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1603735834_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1603735834_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1603735834_0_0_0 = { 3, GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1603735834_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &Panel_t1787746694_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Panel_t1787746694_0_0_0_Types[] = { &Panel_t1787746694_0_0_0 };
extern const Il2CppGenericInst GenInst_Panel_t1787746694_0_0_0 = { 1, GenInst_Panel_t1787746694_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t702033233_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t702033233_0_0_0_Types[] = { &KeyValuePair_2_t702033233_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t702033233_0_0_0 = { 1, GenInst_KeyValuePair_2_t702033233_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 3, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType List_1_t2207118780_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_List_1_t2207118780_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &List_1_t2207118780_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_List_1_t2207118780_0_0_0_String_t_0_0_0 = { 3, GenInst_Boolean_t3825574718_0_0_0_List_1_t2207118780_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType FAQ_t2837997648_0_0_0;
static const Il2CppType* GenInst_FAQ_t2837997648_0_0_0_Types[] = { &FAQ_t2837997648_0_0_0 };
extern const Il2CppGenericInst GenInst_FAQ_t2837997648_0_0_0 = { 1, GenInst_FAQ_t2837997648_0_0_0_Types };
extern const Il2CppType HeaderManager_t49185160_0_0_0;
static const Il2CppType* GenInst_HeaderManager_t49185160_0_0_0_Types[] = { &HeaderManager_t49185160_0_0_0 };
extern const Il2CppGenericInst GenInst_HeaderManager_t49185160_0_0_0 = { 1, GenInst_HeaderManager_t49185160_0_0_0_Types };
extern const Il2CppType HomeLeftUIManager_t3932466733_0_0_0;
static const Il2CppType* GenInst_HomeLeftUIManager_t3932466733_0_0_0_Types[] = { &HomeLeftUIManager_t3932466733_0_0_0 };
extern const Il2CppGenericInst GenInst_HomeLeftUIManager_t3932466733_0_0_0 = { 1, GenInst_HomeLeftUIManager_t3932466733_0_0_0_Types };
extern const Il2CppType Bread_t457172030_0_0_0;
static const Il2CppType* GenInst_Bread_t457172030_0_0_0_Types[] = { &Bread_t457172030_0_0_0 };
extern const Il2CppGenericInst GenInst_Bread_t457172030_0_0_0 = { 1, GenInst_Bread_t457172030_0_0_0_Types };
extern const Il2CppType TweenAlpha_t2421518635_0_0_0;
static const Il2CppType* GenInst_TweenAlpha_t2421518635_0_0_0_Types[] = { &TweenAlpha_t2421518635_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenAlpha_t2421518635_0_0_0 = { 1, GenInst_TweenAlpha_t2421518635_0_0_0_Types };
extern const Il2CppType VibrationController_t2976556198_0_0_0;
static const Il2CppType* GenInst_VibrationController_t2976556198_0_0_0_Types[] = { &VibrationController_t2976556198_0_0_0 };
extern const Il2CppGenericInst GenInst_VibrationController_t2976556198_0_0_0 = { 1, GenInst_VibrationController_t2976556198_0_0_0_Types };
extern const Il2CppType Feed_t3408144164_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0_Types[] = { &String_t_0_0_0, &Feed_t3408144164_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0 = { 2, GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Feed_t3408144164_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Feed_t3408144164_0_0_0_Types[] = { &Feed_t3408144164_0_0_0 };
extern const Il2CppGenericInst GenInst_Feed_t3408144164_0_0_0 = { 1, GenInst_Feed_t3408144164_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3080268648_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3080268648_0_0_0_Types[] = { &KeyValuePair_2_t3080268648_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3080268648_0_0_0 = { 1, GenInst_KeyValuePair_2_t3080268648_0_0_0_Types };
extern const Il2CppType StatusManager_t2720247037_0_0_0;
static const Il2CppType* GenInst_StatusManager_t2720247037_0_0_0_Types[] = { &StatusManager_t2720247037_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusManager_t2720247037_0_0_0 = { 1, GenInst_StatusManager_t2720247037_0_0_0_Types };
extern const Il2CppType LaunchedItem_t3670634427_0_0_0;
static const Il2CppType* GenInst_LaunchedItem_t3670634427_0_0_0_Types[] = { &LaunchedItem_t3670634427_0_0_0 };
extern const Il2CppGenericInst GenInst_LaunchedItem_t3670634427_0_0_0 = { 1, GenInst_LaunchedItem_t3670634427_0_0_0_Types };
static const Il2CppType* GenInst_Friend_t3555014108_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Friend_t3555014108_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Friend_t3555014108_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Friend_t3555014108_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Friend_t3555014108_0_0_0_String_t_0_0_0_Types[] = { &Friend_t3555014108_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Friend_t3555014108_0_0_0_String_t_0_0_0 = { 2, GenInst_Friend_t3555014108_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType PrefabResource_t926725558_0_0_0;
static const Il2CppType* GenInst_PrefabResource_t926725558_0_0_0_Types[] = { &PrefabResource_t926725558_0_0_0 };
extern const Il2CppGenericInst GenInst_PrefabResource_t926725558_0_0_0 = { 1, GenInst_PrefabResource_t926725558_0_0_0_Types };
extern const Il2CppType ChatContactsListItem_t4002571236_0_0_0;
static const Il2CppType* GenInst_ChatContactsListItem_t4002571236_0_0_0_Types[] = { &ChatContactsListItem_t4002571236_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatContactsListItem_t4002571236_0_0_0 = { 1, GenInst_ChatContactsListItem_t4002571236_0_0_0_Types };
extern const Il2CppType ChatContact_t3760700014_0_0_0;
static const Il2CppType* GenInst_ChatContact_t3760700014_0_0_0_String_t_0_0_0_Types[] = { &ChatContact_t3760700014_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatContact_t3760700014_0_0_0_String_t_0_0_0 = { 2, GenInst_ChatContact_t3760700014_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_ChatContact_t3760700014_0_0_0_Types[] = { &ChatContact_t3760700014_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatContact_t3760700014_0_0_0 = { 1, GenInst_ChatContact_t3760700014_0_0_0_Types };
extern const Il2CppType ChatNavigationController_t880507216_0_0_0;
static const Il2CppType* GenInst_ChatNavigationController_t880507216_0_0_0_Types[] = { &ChatNavigationController_t880507216_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatNavigationController_t880507216_0_0_0 = { 1, GenInst_ChatNavigationController_t880507216_0_0_0_Types };
extern const Il2CppType ChatNavScreen_t42377261_0_0_0;
static const Il2CppType* GenInst_ChatNavigationController_t880507216_0_0_0_ChatNavScreen_t42377261_0_0_0_Types[] = { &ChatNavigationController_t880507216_0_0_0, &ChatNavScreen_t42377261_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatNavigationController_t880507216_0_0_0_ChatNavScreen_t42377261_0_0_0 = { 2, GenInst_ChatNavigationController_t880507216_0_0_0_ChatNavScreen_t42377261_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ChatNavScreen_t42377261_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ChatNavScreen_t42377261_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ChatNavScreen_t42377261_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ChatNavScreen_t42377261_0_0_0_Types };
static const Il2CppType* GenInst_ChatNavScreen_t42377261_0_0_0_Types[] = { &ChatNavScreen_t42377261_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatNavScreen_t42377261_0_0_0 = { 1, GenInst_ChatNavScreen_t42377261_0_0_0_Types };
extern const Il2CppType FullScreenLoadingIndicator_t2986689576_0_0_0;
static const Il2CppType* GenInst_FullScreenLoadingIndicator_t2986689576_0_0_0_Types[] = { &FullScreenLoadingIndicator_t2986689576_0_0_0 };
extern const Il2CppGenericInst GenInst_FullScreenLoadingIndicator_t2986689576_0_0_0 = { 1, GenInst_FullScreenLoadingIndicator_t2986689576_0_0_0_Types };
extern const Il2CppType ChatMessageListItem_t2425051210_0_0_0;
static const Il2CppType* GenInst_ChatMessageListItem_t2425051210_0_0_0_Types[] = { &ChatMessageListItem_t2425051210_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatMessageListItem_t2425051210_0_0_0 = { 1, GenInst_ChatMessageListItem_t2425051210_0_0_0_Types };
extern const Il2CppType ChatThreadsListItem_t303868848_0_0_0;
static const Il2CppType* GenInst_ChatThreadsListItem_t303868848_0_0_0_Types[] = { &ChatThreadsListItem_t303868848_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatThreadsListItem_t303868848_0_0_0 = { 1, GenInst_ChatThreadsListItem_t303868848_0_0_0_Types };
static const Il2CppType* GenInst_ChatThread_t2394323482_0_0_0_DateTime_t693205669_0_0_0_Types[] = { &ChatThread_t2394323482_0_0_0, &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatThread_t2394323482_0_0_0_DateTime_t693205669_0_0_0 = { 2, GenInst_ChatThread_t2394323482_0_0_0_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType AttackNavigationController_t2145483014_0_0_0;
static const Il2CppType* GenInst_AttackNavigationController_t2145483014_0_0_0_Types[] = { &AttackNavigationController_t2145483014_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackNavigationController_t2145483014_0_0_0 = { 1, GenInst_AttackNavigationController_t2145483014_0_0_0_Types };
extern const Il2CppType AttackNavScreen_t36759459_0_0_0;
static const Il2CppType* GenInst_AttackNavigationController_t2145483014_0_0_0_AttackNavScreen_t36759459_0_0_0_Types[] = { &AttackNavigationController_t2145483014_0_0_0, &AttackNavScreen_t36759459_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackNavigationController_t2145483014_0_0_0_AttackNavScreen_t36759459_0_0_0 = { 2, GenInst_AttackNavigationController_t2145483014_0_0_0_AttackNavScreen_t36759459_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_AttackNavScreen_t36759459_0_0_0_Types[] = { &Il2CppObject_0_0_0, &AttackNavScreen_t36759459_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_AttackNavScreen_t36759459_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_AttackNavScreen_t36759459_0_0_0_Types };
static const Il2CppType* GenInst_AttackNavScreen_t36759459_0_0_0_Types[] = { &AttackNavScreen_t36759459_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackNavScreen_t36759459_0_0_0 = { 1, GenInst_AttackNavScreen_t36759459_0_0_0_Types };
extern const Il2CppType AttackHomeNav_t3944890504_0_0_0;
static const Il2CppType* GenInst_AttackHomeNavigationController_t3047766999_0_0_0_AttackHomeNav_t3944890504_0_0_0_Types[] = { &AttackHomeNavigationController_t3047766999_0_0_0, &AttackHomeNav_t3944890504_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackHomeNavigationController_t3047766999_0_0_0_AttackHomeNav_t3944890504_0_0_0 = { 2, GenInst_AttackHomeNavigationController_t3047766999_0_0_0_AttackHomeNav_t3944890504_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_AttackHomeNav_t3944890504_0_0_0_Types[] = { &Il2CppObject_0_0_0, &AttackHomeNav_t3944890504_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_AttackHomeNav_t3944890504_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_AttackHomeNav_t3944890504_0_0_0_Types };
static const Il2CppType* GenInst_AttackHomeNav_t3944890504_0_0_0_Types[] = { &AttackHomeNav_t3944890504_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackHomeNav_t3944890504_0_0_0 = { 1, GenInst_AttackHomeNav_t3944890504_0_0_0_Types };
extern const Il2CppType AttackSearchNavigationController_t4133786906_0_0_0;
static const Il2CppType* GenInst_AttackSearchNavigationController_t4133786906_0_0_0_Types[] = { &AttackSearchNavigationController_t4133786906_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackSearchNavigationController_t4133786906_0_0_0 = { 1, GenInst_AttackSearchNavigationController_t4133786906_0_0_0_Types };
extern const Il2CppType AttackSearchNav_t4257884637_0_0_0;
static const Il2CppType* GenInst_AttackSearchNavigationController_t4133786906_0_0_0_AttackSearchNav_t4257884637_0_0_0_Types[] = { &AttackSearchNavigationController_t4133786906_0_0_0, &AttackSearchNav_t4257884637_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackSearchNavigationController_t4133786906_0_0_0_AttackSearchNav_t4257884637_0_0_0 = { 2, GenInst_AttackSearchNavigationController_t4133786906_0_0_0_AttackSearchNav_t4257884637_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_AttackSearchNav_t4257884637_0_0_0_Types[] = { &Il2CppObject_0_0_0, &AttackSearchNav_t4257884637_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_AttackSearchNav_t4257884637_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_AttackSearchNav_t4257884637_0_0_0_Types };
static const Il2CppType* GenInst_AttackSearchNav_t4257884637_0_0_0_Types[] = { &AttackSearchNav_t4257884637_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackSearchNav_t4257884637_0_0_0 = { 1, GenInst_AttackSearchNav_t4257884637_0_0_0_Types };
extern const Il2CppType ContactsPhoneNumberManager_t4168190569_0_0_0;
static const Il2CppType* GenInst_ContactsPhoneNumberManager_t4168190569_0_0_0_Types[] = { &ContactsPhoneNumberManager_t4168190569_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactsPhoneNumberManager_t4168190569_0_0_0 = { 1, GenInst_ContactsPhoneNumberManager_t4168190569_0_0_0_Types };
extern const Il2CppType AttackListItem_t3405986821_0_0_0;
static const Il2CppType* GenInst_AttackListItem_t3405986821_0_0_0_Types[] = { &AttackListItem_t3405986821_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackListItem_t3405986821_0_0_0 = { 1, GenInst_AttackListItem_t3405986821_0_0_0_Types };
extern const Il2CppType StoreManager_t650776524_0_0_0;
static const Il2CppType* GenInst_StoreManager_t650776524_0_0_0_Types[] = { &StoreManager_t650776524_0_0_0 };
extern const Il2CppGenericInst GenInst_StoreManager_t650776524_0_0_0 = { 1, GenInst_StoreManager_t650776524_0_0_0_Types };
extern const Il2CppType HomeCenterUIManager_t2267606293_0_0_0;
static const Il2CppType* GenInst_HomeCenterUIManager_t2267606293_0_0_0_Types[] = { &HomeCenterUIManager_t2267606293_0_0_0 };
extern const Il2CppGenericInst GenInst_HomeCenterUIManager_t2267606293_0_0_0 = { 1, GenInst_HomeCenterUIManager_t2267606293_0_0_0_Types };
extern const Il2CppType FriendHomeNavScreen_t1599038296_0_0_0;
static const Il2CppType* GenInst_FriendHomeNavigationController_t1091974581_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0_Types[] = { &FriendHomeNavigationController_t1091974581_0_0_0, &FriendHomeNavScreen_t1599038296_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendHomeNavigationController_t1091974581_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0 = { 2, GenInst_FriendHomeNavigationController_t1091974581_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0_Types[] = { &Il2CppObject_0_0_0, &FriendHomeNavScreen_t1599038296_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0_Types };
static const Il2CppType* GenInst_FriendHomeNavScreen_t1599038296_0_0_0_Types[] = { &FriendHomeNavScreen_t1599038296_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendHomeNavScreen_t1599038296_0_0_0 = { 1, GenInst_FriendHomeNavScreen_t1599038296_0_0_0_Types };
extern const Il2CppType FriendsNavigationController_t702141233_0_0_0;
static const Il2CppType* GenInst_FriendsNavigationController_t702141233_0_0_0_Types[] = { &FriendsNavigationController_t702141233_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendsNavigationController_t702141233_0_0_0 = { 1, GenInst_FriendsNavigationController_t702141233_0_0_0_Types };
extern const Il2CppType FriendNavScreen_t2175383453_0_0_0;
static const Il2CppType* GenInst_FriendsNavigationController_t702141233_0_0_0_FriendNavScreen_t2175383453_0_0_0_Types[] = { &FriendsNavigationController_t702141233_0_0_0, &FriendNavScreen_t2175383453_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendsNavigationController_t702141233_0_0_0_FriendNavScreen_t2175383453_0_0_0 = { 2, GenInst_FriendsNavigationController_t702141233_0_0_0_FriendNavScreen_t2175383453_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_FriendNavScreen_t2175383453_0_0_0_Types[] = { &Il2CppObject_0_0_0, &FriendNavScreen_t2175383453_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_FriendNavScreen_t2175383453_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_FriendNavScreen_t2175383453_0_0_0_Types };
static const Il2CppType* GenInst_FriendNavScreen_t2175383453_0_0_0_Types[] = { &FriendNavScreen_t2175383453_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendNavScreen_t2175383453_0_0_0 = { 1, GenInst_FriendNavScreen_t2175383453_0_0_0_Types };
extern const Il2CppType FriendsSearchNavigationController_t1013932095_0_0_0;
static const Il2CppType* GenInst_FriendsSearchNavigationController_t1013932095_0_0_0_Types[] = { &FriendsSearchNavigationController_t1013932095_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendsSearchNavigationController_t1013932095_0_0_0 = { 1, GenInst_FriendsSearchNavigationController_t1013932095_0_0_0_Types };
extern const Il2CppType FriendSearchNavScreen_t3018389163_0_0_0;
static const Il2CppType* GenInst_FriendsSearchNavigationController_t1013932095_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0_Types[] = { &FriendsSearchNavigationController_t1013932095_0_0_0, &FriendSearchNavScreen_t3018389163_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendsSearchNavigationController_t1013932095_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0 = { 2, GenInst_FriendsSearchNavigationController_t1013932095_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0_Types[] = { &Il2CppObject_0_0_0, &FriendSearchNavScreen_t3018389163_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0_Types };
static const Il2CppType* GenInst_FriendSearchNavScreen_t3018389163_0_0_0_Types[] = { &FriendSearchNavScreen_t3018389163_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendSearchNavScreen_t3018389163_0_0_0 = { 1, GenInst_FriendSearchNavScreen_t3018389163_0_0_0_Types };
extern const Il2CppType FriendsListItem_t521220246_0_0_0;
static const Il2CppType* GenInst_FriendsListItem_t521220246_0_0_0_Types[] = { &FriendsListItem_t521220246_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendsListItem_t521220246_0_0_0 = { 1, GenInst_FriendsListItem_t521220246_0_0_0_Types };
extern const Il2CppType MultiPhoneNumberListItem_t177127779_0_0_0;
static const Il2CppType* GenInst_MultiPhoneNumberListItem_t177127779_0_0_0_Types[] = { &MultiPhoneNumberListItem_t177127779_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiPhoneNumberListItem_t177127779_0_0_0 = { 1, GenInst_MultiPhoneNumberListItem_t177127779_0_0_0_Types };
extern const Il2CppType OtherFriendsListItem_t1916582308_0_0_0;
static const Il2CppType* GenInst_OtherFriendsListItem_t1916582308_0_0_0_Types[] = { &OtherFriendsListItem_t1916582308_0_0_0 };
extern const Il2CppGenericInst GenInst_OtherFriendsListItem_t1916582308_0_0_0 = { 1, GenInst_OtherFriendsListItem_t1916582308_0_0_0_Types };
extern const Il2CppType ContactsNaviagationController_t22981102_0_0_0;
extern const Il2CppType PhoneNumberNavScreen_t1263467250_0_0_0;
static const Il2CppType* GenInst_ContactsNaviagationController_t22981102_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0_Types[] = { &ContactsNaviagationController_t22981102_0_0_0, &PhoneNumberNavScreen_t1263467250_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactsNaviagationController_t22981102_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0 = { 2, GenInst_ContactsNaviagationController_t22981102_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PhoneNumberNavScreen_t1263467250_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0_Types };
static const Il2CppType* GenInst_PhoneNumberNavScreen_t1263467250_0_0_0_Types[] = { &PhoneNumberNavScreen_t1263467250_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneNumberNavScreen_t1263467250_0_0_0 = { 1, GenInst_PhoneNumberNavScreen_t1263467250_0_0_0_Types };
static const Il2CppType* GenInst_ContactsNaviagationController_t22981102_0_0_0_Types[] = { &ContactsNaviagationController_t22981102_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactsNaviagationController_t22981102_0_0_0 = { 1, GenInst_ContactsNaviagationController_t22981102_0_0_0_Types };
extern const Il2CppType ErrorMessagePopUp_t1211465891_0_0_0;
static const Il2CppType* GenInst_ErrorMessagePopUp_t1211465891_0_0_0_Types[] = { &ErrorMessagePopUp_t1211465891_0_0_0 };
extern const Il2CppGenericInst GenInst_ErrorMessagePopUp_t1211465891_0_0_0 = { 1, GenInst_ErrorMessagePopUp_t1211465891_0_0_0_Types };
extern const Il2CppType List_1_t2696242213_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2696242213_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2696242213_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2696242213_0_0_0_Types[] = { &List_1_t2696242213_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2696242213_0_0_0 = { 1, GenInst_List_1_t2696242213_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2368366697_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2368366697_0_0_0_Types[] = { &KeyValuePair_2_t2368366697_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2368366697_0_0_0 = { 1, GenInst_KeyValuePair_2_t2368366697_0_0_0_Types };
extern const Il2CppType HomeRightUIManager_t3565483256_0_0_0;
static const Il2CppType* GenInst_HomeRightUIManager_t3565483256_0_0_0_Types[] = { &HomeRightUIManager_t3565483256_0_0_0 };
extern const Il2CppGenericInst GenInst_HomeRightUIManager_t3565483256_0_0_0 = { 1, GenInst_HomeRightUIManager_t3565483256_0_0_0_Types };
extern const Il2CppType HomeTabController_t1746680606_0_0_0;
static const Il2CppType* GenInst_HomeTabController_t1746680606_0_0_0_Types[] = { &HomeTabController_t1746680606_0_0_0 };
extern const Il2CppGenericInst GenInst_HomeTabController_t1746680606_0_0_0 = { 1, GenInst_HomeTabController_t1746680606_0_0_0_Types };
extern const Il2CppType HomeBottomTabController_t251067189_0_0_0;
static const Il2CppType* GenInst_HomeBottomTabController_t251067189_0_0_0_Types[] = { &HomeBottomTabController_t251067189_0_0_0 };
extern const Il2CppGenericInst GenInst_HomeBottomTabController_t251067189_0_0_0 = { 1, GenInst_HomeBottomTabController_t251067189_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &OnlineMapsMarker3D_t576815539_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0 = { 3, GenInst_Vector2_t2243707579_0_0_0_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Il2CppObject_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0 = { 3, GenInst_Vector2_t2243707579_0_0_0_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType SwitchMapStateButton_t2972050837_0_0_0;
static const Il2CppType* GenInst_SwitchMapStateButton_t2972050837_0_0_0_Types[] = { &SwitchMapStateButton_t2972050837_0_0_0 };
extern const Il2CppGenericInst GenInst_SwitchMapStateButton_t2972050837_0_0_0 = { 1, GenInst_SwitchMapStateButton_t2972050837_0_0_0_Types };
extern const Il2CppType DefendModalNavigationController_t472169731_0_0_0;
static const Il2CppType* GenInst_DefendModalNavigationController_t472169731_0_0_0_Types[] = { &DefendModalNavigationController_t472169731_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendModalNavigationController_t472169731_0_0_0 = { 1, GenInst_DefendModalNavigationController_t472169731_0_0_0_Types };
extern const Il2CppType DefendNavScreen_t3838619703_0_0_0;
static const Il2CppType* GenInst_DefendModalNavigationController_t472169731_0_0_0_DefendNavScreen_t3838619703_0_0_0_Types[] = { &DefendModalNavigationController_t472169731_0_0_0, &DefendNavScreen_t3838619703_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendModalNavigationController_t472169731_0_0_0_DefendNavScreen_t3838619703_0_0_0 = { 2, GenInst_DefendModalNavigationController_t472169731_0_0_0_DefendNavScreen_t3838619703_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_DefendNavScreen_t3838619703_0_0_0_Types[] = { &Il2CppObject_0_0_0, &DefendNavScreen_t3838619703_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_DefendNavScreen_t3838619703_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_DefendNavScreen_t3838619703_0_0_0_Types };
static const Il2CppType* GenInst_DefendNavScreen_t3838619703_0_0_0_Types[] = { &DefendNavScreen_t3838619703_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendNavScreen_t3838619703_0_0_0 = { 1, GenInst_DefendNavScreen_t3838619703_0_0_0_Types };
extern const Il2CppType GroupFriendUI_t1634706847_0_0_0;
static const Il2CppType* GenInst_GroupFriendUI_t1634706847_0_0_0_Types[] = { &GroupFriendUI_t1634706847_0_0_0 };
extern const Il2CppGenericInst GenInst_GroupFriendUI_t1634706847_0_0_0 = { 1, GenInst_GroupFriendUI_t1634706847_0_0_0_Types };
extern const Il2CppType MineAudienceTab_t1025879672_0_0_0;
static const Il2CppType* GenInst_MineAudienceTab_t1025879672_0_0_0_Types[] = { &MineAudienceTab_t1025879672_0_0_0 };
extern const Il2CppGenericInst GenInst_MineAudienceTab_t1025879672_0_0_0 = { 1, GenInst_MineAudienceTab_t1025879672_0_0_0_Types };
extern const Il2CppType List_1_t2098562409_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2098562409_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2098562409_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2098562409_0_0_0_Types[] = { &List_1_t2098562409_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2098562409_0_0_0 = { 1, GenInst_List_1_t2098562409_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1770686893_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1770686893_0_0_0_Types[] = { &KeyValuePair_2_t1770686893_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1770686893_0_0_0 = { 1, GenInst_KeyValuePair_2_t1770686893_0_0_0_Types };
extern const Il2CppType PlayerProfileNavigationController_t1388421560_0_0_0;
static const Il2CppType* GenInst_PlayerProfileNavigationController_t1388421560_0_0_0_Types[] = { &PlayerProfileNavigationController_t1388421560_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerProfileNavigationController_t1388421560_0_0_0 = { 1, GenInst_PlayerProfileNavigationController_t1388421560_0_0_0_Types };
extern const Il2CppType PlayerProfileNavScreen_t2660668259_0_0_0;
static const Il2CppType* GenInst_PlayerProfileNavigationController_t1388421560_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0_Types[] = { &PlayerProfileNavigationController_t1388421560_0_0_0, &PlayerProfileNavScreen_t2660668259_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerProfileNavigationController_t1388421560_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0 = { 2, GenInst_PlayerProfileNavigationController_t1388421560_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0_Types[] = { &Il2CppObject_0_0_0, &PlayerProfileNavScreen_t2660668259_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0_Types };
static const Il2CppType* GenInst_PlayerProfileNavScreen_t2660668259_0_0_0_Types[] = { &PlayerProfileNavScreen_t2660668259_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerProfileNavScreen_t2660668259_0_0_0 = { 1, GenInst_PlayerProfileNavScreen_t2660668259_0_0_0_Types };
extern const Il2CppType SettingsNavScreen_t2267127234_0_0_0;
static const Il2CppType* GenInst_SettingsNavigationController_t2581944457_0_0_0_SettingsNavScreen_t2267127234_0_0_0_Types[] = { &SettingsNavigationController_t2581944457_0_0_0, &SettingsNavScreen_t2267127234_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsNavigationController_t2581944457_0_0_0_SettingsNavScreen_t2267127234_0_0_0 = { 2, GenInst_SettingsNavigationController_t2581944457_0_0_0_SettingsNavScreen_t2267127234_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_SettingsNavScreen_t2267127234_0_0_0_Types[] = { &Il2CppObject_0_0_0, &SettingsNavScreen_t2267127234_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_SettingsNavScreen_t2267127234_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_SettingsNavScreen_t2267127234_0_0_0_Types };
static const Il2CppType* GenInst_SettingsNavScreen_t2267127234_0_0_0_Types[] = { &SettingsNavScreen_t2267127234_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsNavScreen_t2267127234_0_0_0 = { 1, GenInst_SettingsNavScreen_t2267127234_0_0_0_Types };
extern const Il2CppType ProfileNavigationController_t2708779591_0_0_0;
extern const Il2CppType ProfileNavScreen_t1126657854_0_0_0;
static const Il2CppType* GenInst_ProfileNavigationController_t2708779591_0_0_0_ProfileNavScreen_t1126657854_0_0_0_Types[] = { &ProfileNavigationController_t2708779591_0_0_0, &ProfileNavScreen_t1126657854_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileNavigationController_t2708779591_0_0_0_ProfileNavScreen_t1126657854_0_0_0 = { 2, GenInst_ProfileNavigationController_t2708779591_0_0_0_ProfileNavScreen_t1126657854_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileNavScreen_t1126657854_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileNavScreen_t1126657854_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileNavScreen_t1126657854_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ProfileNavScreen_t1126657854_0_0_0_Types };
static const Il2CppType* GenInst_ProfileNavScreen_t1126657854_0_0_0_Types[] = { &ProfileNavScreen_t1126657854_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileNavScreen_t1126657854_0_0_0 = { 1, GenInst_ProfileNavScreen_t1126657854_0_0_0_Types };
static const Il2CppType* GenInst_ProfileNavigationController_t2708779591_0_0_0_Types[] = { &ProfileNavigationController_t2708779591_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileNavigationController_t2708779591_0_0_0 = { 1, GenInst_ProfileNavigationController_t2708779591_0_0_0_Types };
extern const Il2CppType StatusNavScreen_t2872369083_0_0_0;
static const Il2CppType* GenInst_StatusNavigationController_t3357829236_0_0_0_StatusNavScreen_t2872369083_0_0_0_Types[] = { &StatusNavigationController_t3357829236_0_0_0, &StatusNavScreen_t2872369083_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusNavigationController_t3357829236_0_0_0_StatusNavScreen_t2872369083_0_0_0 = { 2, GenInst_StatusNavigationController_t3357829236_0_0_0_StatusNavScreen_t2872369083_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_StatusNavScreen_t2872369083_0_0_0_Types[] = { &Il2CppObject_0_0_0, &StatusNavScreen_t2872369083_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_StatusNavScreen_t2872369083_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_StatusNavScreen_t2872369083_0_0_0_Types };
static const Il2CppType* GenInst_StatusNavScreen_t2872369083_0_0_0_Types[] = { &StatusNavScreen_t2872369083_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusNavScreen_t2872369083_0_0_0 = { 1, GenInst_StatusNavScreen_t2872369083_0_0_0_Types };
extern const Il2CppType StorePopUpController_t952004567_0_0_0;
static const Il2CppType* GenInst_StorePopUpController_t952004567_0_0_0_Types[] = { &StorePopUpController_t952004567_0_0_0 };
extern const Il2CppGenericInst GenInst_StorePopUpController_t952004567_0_0_0 = { 1, GenInst_StorePopUpController_t952004567_0_0_0_Types };
extern const Il2CppType List_1_t183066060_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t183066060_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t183066060_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t183066060_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t183066060_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t183066060_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t183066060_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t183066060_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t183066060_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t183066060_0_0_0_Types[] = { &List_1_t183066060_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t183066060_0_0_0 = { 1, GenInst_List_1_t183066060_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4150157840_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4150157840_0_0_0_Types[] = { &KeyValuePair_2_t4150157840_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4150157840_0_0_0 = { 1, GenInst_KeyValuePair_2_t4150157840_0_0_0_Types };
extern const Il2CppType LeaderboardNavigationController_t1680977333_0_0_0;
static const Il2CppType* GenInst_LeaderboardNavigationController_t1680977333_0_0_0_Types[] = { &LeaderboardNavigationController_t1680977333_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardNavigationController_t1680977333_0_0_0 = { 1, GenInst_LeaderboardNavigationController_t1680977333_0_0_0_Types };
extern const Il2CppType LeaderboardNavScreen_t2356043712_0_0_0;
static const Il2CppType* GenInst_LeaderboardNavigationController_t1680977333_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0_Types[] = { &LeaderboardNavigationController_t1680977333_0_0_0, &LeaderboardNavScreen_t2356043712_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardNavigationController_t1680977333_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0 = { 2, GenInst_LeaderboardNavigationController_t1680977333_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0_Types[] = { &Il2CppObject_0_0_0, &LeaderboardNavScreen_t2356043712_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0_Types };
static const Il2CppType* GenInst_LeaderboardNavScreen_t2356043712_0_0_0_Types[] = { &LeaderboardNavScreen_t2356043712_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardNavScreen_t2356043712_0_0_0 = { 1, GenInst_LeaderboardNavScreen_t2356043712_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1428657631_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1428657631_0_0_0_Types[] = { &KeyValuePair_2_t1428657631_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1428657631_0_0_0 = { 1, GenInst_KeyValuePair_2_t1428657631_0_0_0_Types };
extern const Il2CppType InitSceneManager_t1690944945_0_0_0;
static const Il2CppType* GenInst_InitSceneManager_t1690944945_0_0_0_Types[] = { &InitSceneManager_t1690944945_0_0_0 };
extern const Il2CppGenericInst GenInst_InitSceneManager_t1690944945_0_0_0 = { 1, GenInst_InitSceneManager_t1690944945_0_0_0_Types };
extern const Il2CppType ConnectionPopUp_t470643858_0_0_0;
static const Il2CppType* GenInst_ConnectionPopUp_t470643858_0_0_0_Types[] = { &ConnectionPopUp_t470643858_0_0_0 };
extern const Il2CppGenericInst GenInst_ConnectionPopUp_t470643858_0_0_0 = { 1, GenInst_ConnectionPopUp_t470643858_0_0_0_Types };
extern const Il2CppType LeaderboardListItem_t3112309392_0_0_0;
static const Il2CppType* GenInst_LeaderboardListItem_t3112309392_0_0_0_Types[] = { &LeaderboardListItem_t3112309392_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardListItem_t3112309392_0_0_0 = { 1, GenInst_LeaderboardListItem_t3112309392_0_0_0_Types };
extern const Il2CppType User_t719925459_0_0_0;
static const Il2CppType* GenInst_User_t719925459_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &User_t719925459_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_User_t719925459_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_User_t719925459_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_User_t719925459_0_0_0_String_t_0_0_0_Types[] = { &User_t719925459_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_User_t719925459_0_0_0_String_t_0_0_0 = { 2, GenInst_User_t719925459_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_User_t719925459_0_0_0_Types[] = { &User_t719925459_0_0_0 };
extern const Il2CppGenericInst GenInst_User_t719925459_0_0_0 = { 1, GenInst_User_t719925459_0_0_0_Types };
extern const Il2CppType LeaderboardManager_t1534320874_0_0_0;
static const Il2CppType* GenInst_LeaderboardManager_t1534320874_0_0_0_Types[] = { &LeaderboardManager_t1534320874_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardManager_t1534320874_0_0_0 = { 1, GenInst_LeaderboardManager_t1534320874_0_0_0_Types };
extern const Il2CppType LoginBottomUIManager_t3738095009_0_0_0;
static const Il2CppType* GenInst_LoginBottomUIManager_t3738095009_0_0_0_Types[] = { &LoginBottomUIManager_t3738095009_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginBottomUIManager_t3738095009_0_0_0 = { 1, GenInst_LoginBottomUIManager_t3738095009_0_0_0_Types };
extern const Il2CppType RegistrationNavigationcontroller_t539766269_0_0_0;
static const Il2CppType* GenInst_RegistrationNavigationcontroller_t539766269_0_0_0_Types[] = { &RegistrationNavigationcontroller_t539766269_0_0_0 };
extern const Il2CppGenericInst GenInst_RegistrationNavigationcontroller_t539766269_0_0_0 = { 1, GenInst_RegistrationNavigationcontroller_t539766269_0_0_0_Types };
extern const Il2CppType RegNavScreen_t667173359_0_0_0;
static const Il2CppType* GenInst_RegistrationNavigationcontroller_t539766269_0_0_0_RegNavScreen_t667173359_0_0_0_Types[] = { &RegistrationNavigationcontroller_t539766269_0_0_0, &RegNavScreen_t667173359_0_0_0 };
extern const Il2CppGenericInst GenInst_RegistrationNavigationcontroller_t539766269_0_0_0_RegNavScreen_t667173359_0_0_0 = { 2, GenInst_RegistrationNavigationcontroller_t539766269_0_0_0_RegNavScreen_t667173359_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_RegNavScreen_t667173359_0_0_0_Types[] = { &Il2CppObject_0_0_0, &RegNavScreen_t667173359_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_RegNavScreen_t667173359_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_RegNavScreen_t667173359_0_0_0_Types };
static const Il2CppType* GenInst_RegNavScreen_t667173359_0_0_0_Types[] = { &RegNavScreen_t667173359_0_0_0 };
extern const Il2CppGenericInst GenInst_RegNavScreen_t667173359_0_0_0 = { 1, GenInst_RegNavScreen_t667173359_0_0_0_Types };
extern const Il2CppType CountryCodeListManager_t203952622_0_0_0;
static const Il2CppType* GenInst_CountryCodeListManager_t203952622_0_0_0_Types[] = { &CountryCodeListManager_t203952622_0_0_0 };
extern const Il2CppGenericInst GenInst_CountryCodeListManager_t203952622_0_0_0 = { 1, GenInst_CountryCodeListManager_t203952622_0_0_0_Types };
extern const Il2CppType CountryCodeListItem_t2467463620_0_0_0;
static const Il2CppType* GenInst_CountryCodeListItem_t2467463620_0_0_0_Types[] = { &CountryCodeListItem_t2467463620_0_0_0 };
extern const Il2CppGenericInst GenInst_CountryCodeListItem_t2467463620_0_0_0 = { 1, GenInst_CountryCodeListItem_t2467463620_0_0_0_Types };
extern const Il2CppType RegInput_t2119160278_0_0_0;
static const Il2CppType* GenInst_RegInput_t2119160278_0_0_0_Types[] = { &RegInput_t2119160278_0_0_0 };
extern const Il2CppGenericInst GenInst_RegInput_t2119160278_0_0_0 = { 1, GenInst_RegInput_t2119160278_0_0_0_Types };
extern const Il2CppType LoginLoadingIndicator_t3051072036_0_0_0;
static const Il2CppType* GenInst_LoginLoadingIndicator_t3051072036_0_0_0_Types[] = { &LoginLoadingIndicator_t3051072036_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginLoadingIndicator_t3051072036_0_0_0 = { 1, GenInst_LoginLoadingIndicator_t3051072036_0_0_0_Types };
extern const Il2CppType MineDefuseSwipe_t2955699161_0_0_0;
static const Il2CppType* GenInst_MineDefuseSwipe_t2955699161_0_0_0_Types[] = { &MineDefuseSwipe_t2955699161_0_0_0 };
extern const Il2CppGenericInst GenInst_MineDefuseSwipe_t2955699161_0_0_0 = { 1, GenInst_MineDefuseSwipe_t2955699161_0_0_0_Types };
extern const Il2CppType AttemptTab_t3964708364_0_0_0;
static const Il2CppType* GenInst_AttemptTab_t3964708364_0_0_0_Types[] = { &AttemptTab_t3964708364_0_0_0 };
extern const Il2CppGenericInst GenInst_AttemptTab_t3964708364_0_0_0 = { 1, GenInst_AttemptTab_t3964708364_0_0_0_Types };
extern const Il2CppType LauncherSceneManager_t2478891917_0_0_0;
static const Il2CppType* GenInst_LauncherSceneManager_t2478891917_0_0_0_Types[] = { &LauncherSceneManager_t2478891917_0_0_0 };
extern const Il2CppGenericInst GenInst_LauncherSceneManager_t2478891917_0_0_0 = { 1, GenInst_LauncherSceneManager_t2478891917_0_0_0_Types };
extern const Il2CppType BlockListItem_t1514504046_0_0_0;
static const Il2CppType* GenInst_BlockListItem_t1514504046_0_0_0_Types[] = { &BlockListItem_t1514504046_0_0_0 };
extern const Il2CppGenericInst GenInst_BlockListItem_t1514504046_0_0_0 = { 1, GenInst_BlockListItem_t1514504046_0_0_0_Types };
extern const Il2CppType FAQListItem_t1761156371_0_0_0;
static const Il2CppType* GenInst_FAQListItem_t1761156371_0_0_0_Types[] = { &FAQListItem_t1761156371_0_0_0 };
extern const Il2CppGenericInst GenInst_FAQListItem_t1761156371_0_0_0 = { 1, GenInst_FAQListItem_t1761156371_0_0_0_Types };
extern const Il2CppType BoxCollider_t22920061_0_0_0;
static const Il2CppType* GenInst_BoxCollider_t22920061_0_0_0_Types[] = { &BoxCollider_t22920061_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider_t22920061_0_0_0 = { 1, GenInst_BoxCollider_t22920061_0_0_0_Types };
extern const Il2CppType TweenPosition_t1144714832_0_0_0;
static const Il2CppType* GenInst_TweenPosition_t1144714832_0_0_0_Types[] = { &TweenPosition_t1144714832_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenPosition_t1144714832_0_0_0 = { 1, GenInst_TweenPosition_t1144714832_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1872368772_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1872368772_0_0_0_Types[] = { &IEquatable_1_t1872368772_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1872368772_0_0_0 = { 1, GenInst_IEquatable_1_t1872368772_0_0_0_Types };
extern const Il2CppType ColourStyleSheet_t290320840_0_0_0;
static const Il2CppType* GenInst_ColourStyleSheet_t290320840_0_0_0_Types[] = { &ColourStyleSheet_t290320840_0_0_0 };
extern const Il2CppGenericInst GenInst_ColourStyleSheet_t290320840_0_0_0 = { 1, GenInst_ColourStyleSheet_t290320840_0_0_0_Types };
extern const Il2CppType SliderNumberLabel_t290801982_0_0_0;
static const Il2CppType* GenInst_SliderNumberLabel_t290801982_0_0_0_Types[] = { &SliderNumberLabel_t290801982_0_0_0 };
extern const Il2CppGenericInst GenInst_SliderNumberLabel_t290801982_0_0_0 = { 1, GenInst_SliderNumberLabel_t290801982_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &GameObject_t1756533147_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2816671300_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2816671300_0_0_0_Types[] = { &KeyValuePair_2_t2816671300_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2816671300_0_0_0 = { 1, GenInst_KeyValuePair_2_t2816671300_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3888080226_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3888080226_0_0_0_Types[] = { &KeyValuePair_2_t3888080226_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3888080226_0_0_0 = { 1, GenInst_KeyValuePair_2_t3888080226_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector3_t2243707580_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_KeyValuePair_2_t3888080226_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector3_t2243707580_0_0_0, &KeyValuePair_2_t3888080226_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_KeyValuePair_2_t3888080226_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_KeyValuePair_2_t3888080226_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0, &Vector3_t2243707580_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3331398246_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3331398246_0_0_0_Types[] = { &KeyValuePair_2_t3331398246_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3331398246_0_0_0 = { 1, GenInst_KeyValuePair_2_t3331398246_0_0_0_Types };
extern const Il2CppType FacebookFriend_t758270532_0_0_0;
static const Il2CppType* GenInst_FacebookFriend_t758270532_0_0_0_Types[] = { &FacebookFriend_t758270532_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookFriend_t758270532_0_0_0 = { 1, GenInst_FacebookFriend_t758270532_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3089358386_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3089358386_0_0_0_Types[] = { &KeyValuePair_2_t3089358386_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3089358386_0_0_0 = { 1, GenInst_KeyValuePair_2_t3089358386_0_0_0_Types };
extern const Il2CppType NumberFormat_t441739224_0_0_0;
static const Il2CppType* GenInst_NumberFormat_t441739224_0_0_0_Types[] = { &NumberFormat_t441739224_0_0_0 };
extern const Il2CppGenericInst GenInst_NumberFormat_t441739224_0_0_0 = { 1, GenInst_NumberFormat_t441739224_0_0_0_Types };
extern const Il2CppType PhoneMetadata_t366861403_0_0_0;
static const Il2CppType* GenInst_PhoneMetadata_t366861403_0_0_0_Types[] = { &PhoneMetadata_t366861403_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneMetadata_t366861403_0_0_0 = { 1, GenInst_PhoneMetadata_t366861403_0_0_0_Types };
extern const Il2CppType Link_t247561424_0_0_0;
static const Il2CppType* GenInst_Link_t247561424_0_0_0_Types[] = { &Link_t247561424_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t247561424_0_0_0 = { 1, GenInst_Link_t247561424_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t3943999495_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0 = { 2, GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Dictionary_2_t3943999495_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3616123979_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3616123979_0_0_0_Types[] = { &KeyValuePair_2_t3616123979_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3616123979_0_0_0 = { 1, GenInst_KeyValuePair_2_t3616123979_0_0_0_Types };
extern const Il2CppType HashSet_1_t362681087_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t362681087_0_0_0_Types[] = { &HashSet_1_t362681087_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t362681087_0_0_0 = { 1, GenInst_HashSet_1_t362681087_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_HashSet_1_t362681087_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &HashSet_1_t362681087_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_HashSet_1_t362681087_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_HashSet_1_t362681087_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1422819240_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1422819240_0_0_0_Types[] = { &KeyValuePair_2_t1422819240_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1422819240_0_0_0 = { 1, GenInst_KeyValuePair_2_t1422819240_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PhoneMetadata_t366861403_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PhoneMetadata_t366861403_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1426999556_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1426999556_0_0_0_Types[] = { &KeyValuePair_2_t1426999556_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1426999556_0_0_0 = { 1, GenInst_KeyValuePair_2_t1426999556_0_0_0_Types };
extern const Il2CppType PhoneMetaTerritory_t2073820963_0_0_0;
static const Il2CppType* GenInst_PhoneMetaTerritory_t2073820963_0_0_0_Types[] = { &PhoneMetaTerritory_t2073820963_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneMetaTerritory_t2073820963_0_0_0 = { 1, GenInst_PhoneMetaTerritory_t2073820963_0_0_0_Types };
extern const Il2CppType PhoneMetaNumberFormat_t4038650015_0_0_0;
static const Il2CppType* GenInst_PhoneMetaNumberFormat_t4038650015_0_0_0_Types[] = { &PhoneMetaNumberFormat_t4038650015_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneMetaNumberFormat_t4038650015_0_0_0 = { 1, GenInst_PhoneMetaNumberFormat_t4038650015_0_0_0_Types };
extern const Il2CppType PhoneNumberMatch_t2163858580_0_0_0;
static const Il2CppType* GenInst_PhoneNumberMatch_t2163858580_0_0_0_Types[] = { &PhoneNumberMatch_t2163858580_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneNumberMatch_t2163858580_0_0_0 = { 1, GenInst_PhoneNumberMatch_t2163858580_0_0_0_Types };
extern const Il2CppType AreaCodeMap_t3230759372_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0_Types[] = { &String_t_0_0_0, &AreaCodeMap_t3230759372_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0 = { 2, GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &AreaCodeMap_t3230759372_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_AreaCodeMap_t3230759372_0_0_0_Types[] = { &AreaCodeMap_t3230759372_0_0_0 };
extern const Il2CppGenericInst GenInst_AreaCodeMap_t3230759372_0_0_0 = { 1, GenInst_AreaCodeMap_t3230759372_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2902883856_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2902883856_0_0_0_Types[] = { &KeyValuePair_2_t2902883856_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2902883856_0_0_0 = { 1, GenInst_KeyValuePair_2_t2902883856_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2812303849_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2812303849_0_0_0_Types[] = { &KeyValuePair_2_t2812303849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2812303849_0_0_0 = { 1, GenInst_KeyValuePair_2_t2812303849_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 3, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t2812303849_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &Char_t3454481338_0_0_0, &KeyValuePair_2_t2812303849_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t2812303849_0_0_0 = { 3, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t2812303849_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0_Types[] = { &String_t_0_0_0, &PhoneMetadata_t366861403_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0 = { 2, GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &PhoneMetadata_t366861403_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38985887_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38985887_0_0_0_Types[] = { &KeyValuePair_2_t38985887_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38985887_0_0_0 = { 1, GenInst_KeyValuePair_2_t38985887_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_String_t_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_String_t_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType IEnumerator_1_t3934349703_0_0_0;
static const Il2CppType* GenInst_IEnumerator_1_t3934349703_0_0_0_Types[] = { &IEnumerator_1_t3934349703_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerator_1_t3934349703_0_0_0 = { 1, GenInst_IEnumerator_1_t3934349703_0_0_0_Types };
extern const Il2CppType IEnumerator_1_t164973122_0_0_0;
static const Il2CppType* GenInst_IEnumerator_1_t164973122_0_0_0_Types[] = { &IEnumerator_1_t164973122_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerator_1_t164973122_0_0_0 = { 1, GenInst_IEnumerator_1_t164973122_0_0_0_Types };
extern const Il2CppType Entry_t1519365992_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0_Types[] = { &String_t_0_0_0, &Entry_t1519365992_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0 = { 2, GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Entry_t1519365992_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Entry_t1519365992_0_0_0_Types[] = { &Entry_t1519365992_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t1519365992_0_0_0 = { 1, GenInst_Entry_t1519365992_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1191490476_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1191490476_0_0_0_Types[] = { &KeyValuePair_2_t1191490476_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1191490476_0_0_0 = { 1, GenInst_KeyValuePair_2_t1191490476_0_0_0_Types };
static const Il2CppType* GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &XElement_t553821050_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0, &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0, &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0, &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0, &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3546416104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types[] = { &Array_Sort_m3546416104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3546416104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m940423571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m940423571_gp_0_0_0_0_Types[] = { &Array_compare_m940423571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m940423571_gp_0_0_0_0 = { 1, GenInst_Array_compare_m940423571_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3895203923_gp_0_0_0_0, &ShimEnumerator_t3895203923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { &DefaultComparer_t1766400012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType List_1_ConvertAll_m343121476_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_List_1_ConvertAll_m343121476_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0, &List_1_ConvertAll_m343121476_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0_List_1_ConvertAll_m343121476_gp_0_0_0_0 = { 2, GenInst_List_1_t1169184319_gp_0_0_0_0_List_1_ConvertAll_m343121476_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_List_1_ConvertAll_m343121476_gp_0_0_0_0_Types[] = { &List_1_ConvertAll_m343121476_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_ConvertAll_m343121476_gp_0_0_0_0 = { 1, GenInst_List_1_ConvertAll_m343121476_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1001032761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { &DefaultComparer_t3074655092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3556217344_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types[] = { &LinkedList_1_t3556217344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3556217344_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4145643798_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types[] = { &Enumerator_t4145643798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4145643798_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2172356692_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t2172356692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Intern_m3121628056_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0_Types[] = { &RBTree_Intern_m3121628056_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0 = { 1, GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Remove_m2480195356_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0_Types[] = { &RBTree_Remove_m2480195356_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0 = { 1, GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_Lookup_m3378201690_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0_Types[] = { &RBTree_Lookup_m3378201690_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0 = { 1, GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0_Types };
extern const Il2CppType RBTree_find_key_m2107656200_gp_0_0_0_0;
static const Il2CppType* GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0_Types[] = { &RBTree_find_key_m2107656200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0 = { 1, GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { &Queue_1_t1458930734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { &Enumerator_t4000919638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t3426860295_gp_0_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types };
extern const Il2CppType SortedDictionary_2_t3426860295_gp_1_0_0_0;
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { &SortedDictionary_2_t3426860295_gp_0_0_0_0, &SortedDictionary_2_t3426860295_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 2, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4073929546_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4073929546_0_0_0_Types[] = { &KeyValuePair_2_t4073929546_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4073929546_0_0_0 = { 1, GenInst_KeyValuePair_2_t4073929546_0_0_0_Types };
extern const Il2CppType Node_t1473068371_gp_0_0_0_0;
extern const Il2CppType Node_t1473068371_gp_1_0_0_0;
static const Il2CppType* GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types[] = { &Node_t1473068371_gp_0_0_0_0, &Node_t1473068371_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0 = { 2, GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types };
extern const Il2CppType NodeHelper_t357096749_gp_0_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types[] = { &NodeHelper_t357096749_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0 = { 1, GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types };
extern const Il2CppType NodeHelper_t357096749_gp_1_0_0_0;
static const Il2CppType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types[] = { &NodeHelper_t357096749_gp_0_0_0_0, &NodeHelper_t357096749_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0 = { 2, GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types };
extern const Il2CppType ValueCollection_t2735575576_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2735575576_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { &ValueCollection_t2735575576_gp_0_0_0_0, &ValueCollection_t2735575576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { &ValueCollection_t2735575576_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1353355221_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1353355221_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { &Enumerator_t1353355221_gp_0_0_0_0, &Enumerator_t1353355221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { &Enumerator_t1353355221_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types };
extern const Il2CppType KeyCollection_t3620397270_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3620397270_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types[] = { &KeyCollection_t3620397270_gp_0_0_0_0, &KeyCollection_t3620397270_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types[] = { &KeyCollection_t3620397270_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4275149413_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4275149413_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types[] = { &Enumerator_t4275149413_gp_0_0_0_0, &Enumerator_t4275149413_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types[] = { &Enumerator_t4275149413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t824916987_gp_0_0_0_0;
extern const Il2CppType Enumerator_t824916987_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types[] = { &Enumerator_t824916987_gp_0_0_0_0, &Enumerator_t824916987_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0 = { 2, GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t404405498_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t404405498_0_0_0_Types[] = { &KeyValuePair_2_t404405498_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t404405498_0_0_0 = { 1, GenInst_KeyValuePair_2_t404405498_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType BindingList_1_t257543824_gp_0_0_0_0;
static const Il2CppType* GenInst_BindingList_1_t257543824_gp_0_0_0_0_Types[] = { &BindingList_1_t257543824_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BindingList_1_t257543824_gp_0_0_0_0 = { 1, GenInst_BindingList_1_t257543824_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Aggregate_m297629787_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Aggregate_m297629787_gp_0_0_0_0_Types[] = { &Enumerable_Aggregate_m297629787_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m297629787_gp_0_0_0_0 = { 1, GenInst_Enumerable_Aggregate_m297629787_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Aggregate_m297629787_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Aggregate_m297629787_gp_1_0_0_0_Enumerable_Aggregate_m297629787_gp_0_0_0_0_Enumerable_Aggregate_m297629787_gp_1_0_0_0_Types[] = { &Enumerable_Aggregate_m297629787_gp_1_0_0_0, &Enumerable_Aggregate_m297629787_gp_0_0_0_0, &Enumerable_Aggregate_m297629787_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Aggregate_m297629787_gp_1_0_0_0_Enumerable_Aggregate_m297629787_gp_0_0_0_0_Enumerable_Aggregate_m297629787_gp_1_0_0_0 = { 3, GenInst_Enumerable_Aggregate_m297629787_gp_1_0_0_0_Enumerable_Aggregate_m297629787_gp_0_0_0_0_Enumerable_Aggregate_m297629787_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_All_m2363499768_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Types[] = { &Enumerable_All_m2363499768_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m2363499768_gp_0_0_0_0 = { 1, GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_All_m2363499768_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m665396702_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types[] = { &Enumerable_Any_m665396702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m665396702_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m2739389357_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Concat_m3276894131_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Concat_m3276894131_gp_0_0_0_0_Types[] = { &Enumerable_Concat_m3276894131_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Concat_m3276894131_gp_0_0_0_0 = { 1, GenInst_Enumerable_Concat_m3276894131_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateConcatIterator_m2343633649_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateConcatIterator_m2343633649_gp_0_0_0_0_Types[] = { &Enumerable_CreateConcatIterator_m2343633649_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateConcatIterator_m2343633649_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateConcatIterator_m2343633649_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m1284016302_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m1284016302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m4622279_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m4622279_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m1561720045_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types[] = { &Enumerable_Count_m1561720045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m136242780_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Types[] = { &Enumerable_Count_m136242780_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m136242780_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Count_m136242780_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_First_m4120844597_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Types[] = { &Enumerable_First_m4120844597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m4120844597_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_First_m4120844597_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1693250038_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types[] = { &Enumerable_First_m1693250038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1693250038_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Last_m1426884230_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Last_m1426884230_gp_0_0_0_0_Types[] = { &Enumerable_Last_m1426884230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m1426884230_gp_0_0_0_0 = { 1, GenInst_Enumerable_Last_m1426884230_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Iterate_m2058622934_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0_Types[] = { &Enumerable_Iterate_m2058622934_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0 = { 1, GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Iterate_m2058622934_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0_Types[] = { &Enumerable_Iterate_m2058622934_gp_0_0_0_0, &Enumerable_Iterate_m2058622934_gp_1_0_0_0, &Enumerable_Iterate_m2058622934_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0 = { 3, GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Min_m1749901801_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Types[] = { &Enumerable_Min_m1749901801_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0 = { 1, GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Enumerable_Min_m1749901801_gp_0_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Enumerable_Min_m1749901801_gp_0_0_0_0, &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 3, GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType Enumerable_OfType_m192007909_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0_Types[] = { &Enumerable_OfType_m192007909_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0 = { 1, GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0_Types[] = { &Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0, &Enumerable_OrderBy_m920500904_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0, &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0_Types[] = { &Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderByDescending_m2247647662_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0_Enumerable_OrderByDescending_m2247647662_gp_1_0_0_0_Types[] = { &Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0, &Enumerable_OrderByDescending_m2247647662_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0_Enumerable_OrderByDescending_m2247647662_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0_Enumerable_OrderByDescending_m2247647662_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0_Types[] = { &Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0_Types[] = { &Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0, &Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0_Types[] = { &Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0, &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Skip_m3101762585_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0_Types[] = { &Enumerable_Skip_m3101762585_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0 = { 1, GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0_Types[] = { &Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Sum_m3284247776_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Types[] = { &Enumerable_Sum_m3284247776_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0 = { 1, GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Enumerable_Sum_m3284247776_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m575340852_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0_Types[] = { &Enumerable_ThenBy_m575340852_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m575340852_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0_Enumerable_ThenBy_m575340852_gp_1_0_0_0_Types[] = { &Enumerable_ThenBy_m575340852_gp_0_0_0_0, &Enumerable_ThenBy_m575340852_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0_Enumerable_ThenBy_m575340852_gp_1_0_0_0 = { 2, GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0_Enumerable_ThenBy_m575340852_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m1785535793_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0_Types[] = { &Enumerable_ThenBy_m1785535793_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m1785535793_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0_Enumerable_ThenBy_m1785535793_gp_1_0_0_0_Types[] = { &Enumerable_ThenBy_m1785535793_gp_0_0_0_0, &Enumerable_ThenBy_m1785535793_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0_Enumerable_ThenBy_m1785535793_gp_1_0_0_0 = { 2, GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0_Enumerable_ThenBy_m1785535793_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ThenBy_m1785535793_gp_1_0_0_0_Types[] = { &Enumerable_ThenBy_m1785535793_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m1785535793_gp_1_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m1785535793_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m2343256994_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m2343256994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m261161385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m261161385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CCreateConcatIteratorU3Ec__Iterator1_1_t2976092573_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateConcatIteratorU3Ec__Iterator1_1_t2976092573_gp_0_0_0_0_Types[] = { &U3CCreateConcatIteratorU3Ec__Iterator1_1_t2976092573_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateConcatIteratorU3Ec__Iterator1_1_t2976092573_gp_0_0_0_0 = { 1, GenInst_U3CCreateConcatIteratorU3Ec__Iterator1_1_t2976092573_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0_Types[] = { &U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0 = { 1, GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0_Types[] = { &U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0 = { 1, GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CMinU3Ec__AnonStorey2E_1_t2538447045_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CMinU3Ec__AnonStorey2E_1_t2538447045_gp_0_0_0_0_Single_t2076509932_0_0_0_Types[] = { &U3CMinU3Ec__AnonStorey2E_1_t2538447045_gp_0_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CMinU3Ec__AnonStorey2E_1_t2538447045_gp_0_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_U3CMinU3Ec__AnonStorey2E_1_t2538447045_gp_0_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t641749975_gp_0_0_0_0;
extern const Il2CppType IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t641749975_gp_0_0_0_0, &IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0 = { 2, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t641749975_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t753306046_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t753306046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t753306046_gp_0_0_0_0, &OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0 = { 2, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0, &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1290221672_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types[] = { &QuickSort_1_t1290221672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1290221672_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t4088581714_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types[] = { &SortContext_1_t4088581714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t4088581714_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0, &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { &HashSet_1_t2624254809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { &Enumerator_t2109956843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { &PrimeHelper_t3424417428_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2621570726_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2621570726_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m3101579087_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m3101579087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m3999848894_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m4171325764_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m2530741872_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types[] = { &Object_Instantiate_m2530741872_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types[] = { &GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 = { 1, GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types };
extern const Il2CppType AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0;
static const Il2CppType* GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types[] = { &AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 = { 1, GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types };
extern const Il2CppType CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types[] = { &CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 = { 1, GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { &UnityAction_1_t2490859068_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
extern const Il2CppType List_1_t2244783541_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2244783541_gp_0_0_0_0_Types[] = { &List_1_t2244783541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2244783541_gp_0_0_0_0 = { 1, GenInst_List_1_t2244783541_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t264254312_0_0_0;
static const Il2CppType* GenInst_List_1_t264254312_0_0_0_Types[] = { &List_1_t264254312_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t264254312_0_0_0 = { 1, GenInst_List_1_t264254312_0_0_0_Types };
extern const Il2CppType ActionExtensions_fire_m398400524_gp_0_0_0_0;
static const Il2CppType* GenInst_ActionExtensions_fire_m398400524_gp_0_0_0_0_Types[] = { &ActionExtensions_fire_m398400524_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionExtensions_fire_m398400524_gp_0_0_0_0 = { 1, GenInst_ActionExtensions_fire_m398400524_gp_0_0_0_0_Types };
extern const Il2CppType Json_decode_m444439411_gp_0_0_0_0;
static const Il2CppType* GenInst_Json_decode_m444439411_gp_0_0_0_0_Types[] = { &Json_decode_m444439411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Json_decode_m444439411_gp_0_0_0_0 = { 1, GenInst_Json_decode_m444439411_gp_0_0_0_0_Types };
extern const Il2CppType Json_decodeObject_m2379391988_gp_0_0_0_0;
static const Il2CppType* GenInst_Json_decodeObject_m2379391988_gp_0_0_0_0_Types[] = { &Json_decodeObject_m2379391988_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Json_decodeObject_m2379391988_gp_0_0_0_0 = { 1, GenInst_Json_decodeObject_m2379391988_gp_0_0_0_0_Types };
extern const Il2CppType ObjectDecoder_decode_m1381048909_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectDecoder_decode_m1381048909_gp_0_0_0_0_Types[] = { &ObjectDecoder_decode_m1381048909_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectDecoder_decode_m1381048909_gp_0_0_0_0 = { 1, GenInst_ObjectDecoder_decode_m1381048909_gp_0_0_0_0_Types };
extern const Il2CppType ObjectDecoder_decode_m1878805481_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectDecoder_decode_m1878805481_gp_0_0_0_0_Types[] = { &ObjectDecoder_decode_m1878805481_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectDecoder_decode_m1878805481_gp_0_0_0_0 = { 1, GenInst_ObjectDecoder_decode_m1878805481_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2611711602_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2611711602_0_0_0_Types[] = { &KeyValuePair_2_t2611711602_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2611711602_0_0_0 = { 1, GenInst_KeyValuePair_2_t2611711602_0_0_0_Types };
extern const Il2CppType SafeDictionary_2_t812589197_gp_0_0_0_0;
extern const Il2CppType SafeDictionary_2_t812589197_gp_1_0_0_0;
static const Il2CppType* GenInst_SafeDictionary_2_t812589197_gp_0_0_0_0_SafeDictionary_2_t812589197_gp_1_0_0_0_Types[] = { &SafeDictionary_2_t812589197_gp_0_0_0_0, &SafeDictionary_2_t812589197_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SafeDictionary_2_t812589197_gp_0_0_0_0_SafeDictionary_2_t812589197_gp_1_0_0_0 = { 2, GenInst_SafeDictionary_2_t812589197_gp_0_0_0_0_SafeDictionary_2_t812589197_gp_1_0_0_0_Types };
extern const Il2CppType CallbackManager_AddFacebookDelegate_m958781935_gp_0_0_0_0;
static const Il2CppType* GenInst_CallbackManager_AddFacebookDelegate_m958781935_gp_0_0_0_0_Types[] = { &CallbackManager_AddFacebookDelegate_m958781935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackManager_AddFacebookDelegate_m958781935_gp_0_0_0_0 = { 1, GenInst_CallbackManager_AddFacebookDelegate_m958781935_gp_0_0_0_0_Types };
extern const Il2CppType CallbackManager_TryCallCallback_m3670554012_gp_0_0_0_0;
static const Il2CppType* GenInst_CallbackManager_TryCallCallback_m3670554012_gp_0_0_0_0_Types[] = { &CallbackManager_TryCallCallback_m3670554012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CallbackManager_TryCallCallback_m3670554012_gp_0_0_0_0 = { 1, GenInst_CallbackManager_TryCallCallback_m3670554012_gp_0_0_0_0_Types };
extern const Il2CppType ComponentFactory_GetComponent_m440979758_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentFactory_GetComponent_m440979758_gp_0_0_0_0_Types[] = { &ComponentFactory_GetComponent_m440979758_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactory_GetComponent_m440979758_gp_0_0_0_0 = { 1, GenInst_ComponentFactory_GetComponent_m440979758_gp_0_0_0_0_Types };
extern const Il2CppType ComponentFactory_AddComponent_m3589982647_gp_0_0_0_0;
static const Il2CppType* GenInst_ComponentFactory_AddComponent_m3589982647_gp_0_0_0_0_Types[] = { &ComponentFactory_AddComponent_m3589982647_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactory_AddComponent_m3589982647_gp_0_0_0_0 = { 1, GenInst_ComponentFactory_AddComponent_m3589982647_gp_0_0_0_0_Types };
extern const Il2CppType MethodArguments_AddNullablePrimitive_m1387850230_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodArguments_AddNullablePrimitive_m1387850230_gp_0_0_0_0_Types[] = { &MethodArguments_AddNullablePrimitive_m1387850230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodArguments_AddNullablePrimitive_m1387850230_gp_0_0_0_0 = { 1, GenInst_MethodArguments_AddNullablePrimitive_m1387850230_gp_0_0_0_0_Types };
extern const Il2CppType MethodArguments_AddList_m1635305002_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodArguments_AddList_m1635305002_gp_0_0_0_0_Types[] = { &MethodArguments_AddList_m1635305002_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodArguments_AddList_m1635305002_gp_0_0_0_0 = { 1, GenInst_MethodArguments_AddList_m1635305002_gp_0_0_0_0_Types };
extern const Il2CppType MethodCall_1_t4238425506_gp_0_0_0_0;
static const Il2CppType* GenInst_MethodCall_1_t4238425506_gp_0_0_0_0_Types[] = { &MethodCall_1_t4238425506_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodCall_1_t4238425506_gp_0_0_0_0 = { 1, GenInst_MethodCall_1_t4238425506_gp_0_0_0_0_Types };
extern const Il2CppType CanvasUIMethodCall_1_t2448137672_gp_0_0_0_0;
static const Il2CppType* GenInst_CanvasUIMethodCall_1_t2448137672_gp_0_0_0_0_Types[] = { &CanvasUIMethodCall_1_t2448137672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasUIMethodCall_1_t2448137672_gp_0_0_0_0 = { 1, GenInst_CanvasUIMethodCall_1_t2448137672_gp_0_0_0_0_Types };
extern const Il2CppType JavaMethodCall_1_t1992182800_gp_0_0_0_0;
static const Il2CppType* GenInst_JavaMethodCall_1_t1992182800_gp_0_0_0_0_Types[] = { &JavaMethodCall_1_t1992182800_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JavaMethodCall_1_t1992182800_gp_0_0_0_0 = { 1, GenInst_JavaMethodCall_1_t1992182800_gp_0_0_0_0_Types };
extern const Il2CppType IOSFacebook_AddCallback_m3462239408_gp_0_0_0_0;
static const Il2CppType* GenInst_IOSFacebook_AddCallback_m3462239408_gp_0_0_0_0_Types[] = { &IOSFacebook_AddCallback_m3462239408_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebook_AddCallback_m3462239408_gp_0_0_0_0 = { 1, GenInst_IOSFacebook_AddCallback_m3462239408_gp_0_0_0_0_Types };
extern const Il2CppType Utilities_GetValueOrDefault_m3032348298_gp_0_0_0_0;
static const Il2CppType* GenInst_Utilities_GetValueOrDefault_m3032348298_gp_0_0_0_0_Types[] = { &Utilities_GetValueOrDefault_m3032348298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utilities_GetValueOrDefault_m3032348298_gp_0_0_0_0 = { 1, GenInst_Utilities_GetValueOrDefault_m3032348298_gp_0_0_0_0_Types };
extern const Il2CppType Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0;
extern const Il2CppType Utilities_AddAllKVPFrom_m2028366180_gp_1_0_0_0;
static const Il2CppType* GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0_Utilities_AddAllKVPFrom_m2028366180_gp_1_0_0_0_Types[] = { &Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0, &Utilities_AddAllKVPFrom_m2028366180_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0_Utilities_AddAllKVPFrom_m2028366180_gp_1_0_0_0 = { 2, GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0_Utilities_AddAllKVPFrom_m2028366180_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0_Types[] = { &Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0 = { 1, GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m1961163955_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2584777480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { &ListPool_1_t1984115411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2000868992_0_0_0;
static const Il2CppType* GenInst_List_1_t2000868992_0_0_0_Types[] = { &List_1_t2000868992_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { &ObjectPool_1_t4265859154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
extern const Il2CppType UnityUtil_getAnyComponentsOfType_m3238176900_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityUtil_getAnyComponentsOfType_m3238176900_gp_0_0_0_0_Types[] = { &UnityUtil_getAnyComponentsOfType_m3238176900_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_getAnyComponentsOfType_m3238176900_gp_0_0_0_0 = { 1, GenInst_UnityUtil_getAnyComponentsOfType_m3238176900_gp_0_0_0_0_Types };
extern const Il2CppType UnityUtil_loadResourceInstanceOfType_m607627357_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityUtil_loadResourceInstanceOfType_m607627357_gp_0_0_0_0_Types[] = { &UnityUtil_loadResourceInstanceOfType_m607627357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_loadResourceInstanceOfType_m607627357_gp_0_0_0_0 = { 1, GenInst_UnityUtil_loadResourceInstanceOfType_m607627357_gp_0_0_0_0_Types };
extern const Il2CppType ObservableDictionary_2_t1567152659_gp_0_0_0_0;
extern const Il2CppType ObservableDictionary_2_t1567152659_gp_1_0_0_0;
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_0_0_0_0, &ObservableDictionary_2_t1567152659_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0 = { 2, GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0 = { 1, GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0 = { 1, GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t512346522_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t512346522_0_0_0_Types[] = { &KeyValuePair_2_t512346522_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t512346522_0_0_0 = { 1, GenInst_KeyValuePair_2_t512346522_0_0_0_Types };
static const Il2CppType* GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &ObservableDictionary_2_t1567152659_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0_Types[] = { &JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0 = { 1, GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0_Types };
extern const Il2CppType JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0;
extern const Il2CppType JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0;
static const Il2CppType* GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0_Types[] = { &JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0, &JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0 = { 2, GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0_Types };
extern const Il2CppType U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0_Types[] = { &U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0 = { 1, GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0_Types };
extern const Il2CppType U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0;
extern const Il2CppType U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0_Types[] = { &U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0, &U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0 = { 2, GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0_Types };
extern const Il2CppType OnlineMapsXML_Find_m2276820904_gp_0_0_0_0;
static const Il2CppType* GenInst_OnlineMapsXML_Find_m2276820904_gp_0_0_0_0_Types[] = { &OnlineMapsXML_Find_m2276820904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsXML_Find_m2276820904_gp_0_0_0_0 = { 1, GenInst_OnlineMapsXML_Find_m2276820904_gp_0_0_0_0_Types };
extern const Il2CppType OnlineMapsXML_Get_m603060637_gp_0_0_0_0;
static const Il2CppType* GenInst_OnlineMapsXML_Get_m603060637_gp_0_0_0_0_Types[] = { &OnlineMapsXML_Get_m603060637_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsXML_Get_m603060637_gp_0_0_0_0 = { 1, GenInst_OnlineMapsXML_Get_m603060637_gp_0_0_0_0_Types };
extern const Il2CppType OnlineMapsXML_Get_m2532915309_gp_0_0_0_0;
static const Il2CppType* GenInst_OnlineMapsXML_Get_m2532915309_gp_0_0_0_0_Types[] = { &OnlineMapsXML_Get_m2532915309_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsXML_Get_m2532915309_gp_0_0_0_0 = { 1, GenInst_OnlineMapsXML_Get_m2532915309_gp_0_0_0_0_Types };
extern const Il2CppType OnlineMapsXML_Value_m791613976_gp_0_0_0_0;
static const Il2CppType* GenInst_OnlineMapsXML_Value_m791613976_gp_0_0_0_0_Types[] = { &OnlineMapsXML_Value_m791613976_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsXML_Value_m791613976_gp_0_0_0_0 = { 1, GenInst_OnlineMapsXML_Value_m791613976_gp_0_0_0_0_Types };
extern const Il2CppType BetterList_1_t2993062629_gp_0_0_0_0;
static const Il2CppType* GenInst_BetterList_1_t2993062629_gp_0_0_0_0_Types[] = { &BetterList_1_t2993062629_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BetterList_1_t2993062629_gp_0_0_0_0 = { 1, GenInst_BetterList_1_t2993062629_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t4219084783_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4219084783_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t4219084783_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4219084783_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4219084783_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddChild_m203049429_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddChild_m203049429_gp_0_0_0_0_Types[] = { &NGUITools_AddChild_m203049429_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddChild_m203049429_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddChild_m203049429_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddChild_m124544632_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddChild_m124544632_gp_0_0_0_0_Types[] = { &NGUITools_AddChild_m124544632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddChild_m124544632_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddChild_m124544632_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddWidget_m3299507383_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddWidget_m3299507383_gp_0_0_0_0_Types[] = { &NGUITools_AddWidget_m3299507383_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddWidget_m3299507383_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddWidget_m3299507383_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddWidget_m2193511042_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddWidget_m2193511042_gp_0_0_0_0_Types[] = { &NGUITools_AddWidget_m2193511042_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddWidget_m2193511042_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddWidget_m2193511042_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_FindInParents_m904571355_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_FindInParents_m904571355_gp_0_0_0_0_Types[] = { &NGUITools_FindInParents_m904571355_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_FindInParents_m904571355_gp_0_0_0_0 = { 1, GenInst_NGUITools_FindInParents_m904571355_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_FindInParents_m230326540_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_FindInParents_m230326540_gp_0_0_0_0_Types[] = { &NGUITools_FindInParents_m230326540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_FindInParents_m230326540_gp_0_0_0_0 = { 1, GenInst_NGUITools_FindInParents_m230326540_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_AddMissingComponent_m3184960056_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_AddMissingComponent_m3184960056_gp_0_0_0_0_Types[] = { &NGUITools_AddMissingComponent_m3184960056_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_AddMissingComponent_m3184960056_gp_0_0_0_0 = { 1, GenInst_NGUITools_AddMissingComponent_m3184960056_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_Execute_m2896051690_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_Execute_m2896051690_gp_0_0_0_0_Types[] = { &NGUITools_Execute_m2896051690_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_Execute_m2896051690_gp_0_0_0_0 = { 1, GenInst_NGUITools_Execute_m2896051690_gp_0_0_0_0_Types };
extern const Il2CppType NGUITools_ExecuteAll_m3760296385_gp_0_0_0_0;
static const Il2CppType* GenInst_NGUITools_ExecuteAll_m3760296385_gp_0_0_0_0_Types[] = { &NGUITools_ExecuteAll_m3760296385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUITools_ExecuteAll_m3760296385_gp_0_0_0_0 = { 1, GenInst_NGUITools_ExecuteAll_m3760296385_gp_0_0_0_0_Types };
extern const Il2CppType UITweener_Begin_m1126580952_gp_0_0_0_0;
static const Il2CppType* GenInst_UITweener_Begin_m1126580952_gp_0_0_0_0_Types[] = { &UITweener_Begin_m1126580952_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UITweener_Begin_m1126580952_gp_0_0_0_0 = { 1, GenInst_UITweener_Begin_m1126580952_gp_0_0_0_0_Types };
extern const Il2CppType IPCycler_MakeWidgets_m159957440_gp_0_0_0_0;
static const Il2CppType* GenInst_IPCycler_MakeWidgets_m159957440_gp_0_0_0_0_Types[] = { &IPCycler_MakeWidgets_m159957440_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IPCycler_MakeWidgets_m159957440_gp_0_0_0_0 = { 1, GenInst_IPCycler_MakeWidgets_m159957440_gp_0_0_0_0_Types };
extern const Il2CppType DeviceShareManager_2_t116377866_gp_0_0_0_0;
static const Il2CppType* GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0_Types[] = { &DeviceShareManager_2_t116377866_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0 = { 1, GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0_Types };
extern const Il2CppType DeviceShareManager_2_t116377866_gp_1_0_0_0;
static const Il2CppType* GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0_DeviceShareManager_2_t116377866_gp_1_0_0_0_Types[] = { &DeviceShareManager_2_t116377866_gp_0_0_0_0, &DeviceShareManager_2_t116377866_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0_DeviceShareManager_2_t116377866_gp_1_0_0_0 = { 2, GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0_DeviceShareManager_2_t116377866_gp_1_0_0_0_Types };
extern const Il2CppType BaseServer_1_t4274267663_gp_0_0_0_0;
static const Il2CppType* GenInst_BaseServer_1_t4274267663_gp_0_0_0_0_Types[] = { &BaseServer_1_t4274267663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseServer_1_t4274267663_gp_0_0_0_0 = { 1, GenInst_BaseServer_1_t4274267663_gp_0_0_0_0_Types };
extern const Il2CppType U3CCheckPersistantLoginU3Ec__AnonStorey0_t2587949953_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCheckPersistantLoginU3Ec__AnonStorey0_t2587949953_gp_0_0_0_0_Types[] = { &U3CCheckPersistantLoginU3Ec__AnonStorey0_t2587949953_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCheckPersistantLoginU3Ec__AnonStorey0_t2587949953_gp_0_0_0_0 = { 1, GenInst_U3CCheckPersistantLoginU3Ec__AnonStorey0_t2587949953_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveAllActiveAuthU3Ec__AnonStorey1_t1497602663_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveAllActiveAuthU3Ec__AnonStorey1_t1497602663_gp_0_0_0_0_Types[] = { &U3CRetrieveAllActiveAuthU3Ec__AnonStorey1_t1497602663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveAllActiveAuthU3Ec__AnonStorey1_t1497602663_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveAllActiveAuthU3Ec__AnonStorey1_t1497602663_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveUserFacebookAuthU3Ec__AnonStorey2_t2271550450_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveUserFacebookAuthU3Ec__AnonStorey2_t2271550450_gp_0_0_0_0_Types[] = { &U3CRetrieveUserFacebookAuthU3Ec__AnonStorey2_t2271550450_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveUserFacebookAuthU3Ec__AnonStorey2_t2271550450_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveUserFacebookAuthU3Ec__AnonStorey2_t2271550450_gp_0_0_0_0_Types };
extern const Il2CppType U3CLoginViaTwitterU3Ec__AnonStorey3_t2886528624_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLoginViaTwitterU3Ec__AnonStorey3_t2886528624_gp_0_0_0_0_Types[] = { &U3CLoginViaTwitterU3Ec__AnonStorey3_t2886528624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLoginViaTwitterU3Ec__AnonStorey3_t2886528624_gp_0_0_0_0 = { 1, GenInst_U3CLoginViaTwitterU3Ec__AnonStorey3_t2886528624_gp_0_0_0_0_Types };
extern const Il2CppType U3CLoginViaGoogleU3Ec__AnonStorey4_t1026245177_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLoginViaGoogleU3Ec__AnonStorey4_t1026245177_gp_0_0_0_0_Types[] = { &U3CLoginViaGoogleU3Ec__AnonStorey4_t1026245177_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLoginViaGoogleU3Ec__AnonStorey4_t1026245177_gp_0_0_0_0 = { 1, GenInst_U3CLoginViaGoogleU3Ec__AnonStorey4_t1026245177_gp_0_0_0_0_Types };
extern const Il2CppType U3CLoginViaFacebookU3Ec__AnonStorey5_t2316794845_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLoginViaFacebookU3Ec__AnonStorey5_t2316794845_gp_0_0_0_0_Types[] = { &U3CLoginViaFacebookU3Ec__AnonStorey5_t2316794845_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLoginViaFacebookU3Ec__AnonStorey5_t2316794845_gp_0_0_0_0 = { 1, GenInst_U3CLoginViaFacebookU3Ec__AnonStorey5_t2316794845_gp_0_0_0_0_Types };
extern const Il2CppType U3CLoginViaUsernameU3Ec__AnonStorey6_t3039520268_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLoginViaUsernameU3Ec__AnonStorey6_t3039520268_gp_0_0_0_0_Types[] = { &U3CLoginViaUsernameU3Ec__AnonStorey6_t3039520268_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLoginViaUsernameU3Ec__AnonStorey6_t3039520268_gp_0_0_0_0 = { 1, GenInst_U3CLoginViaUsernameU3Ec__AnonStorey6_t3039520268_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeleteAuthViaIDU3Ec__AnonStorey7_t1409908404_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeleteAuthViaIDU3Ec__AnonStorey7_t1409908404_gp_0_0_0_0_Types[] = { &U3CDeleteAuthViaIDU3Ec__AnonStorey7_t1409908404_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeleteAuthViaIDU3Ec__AnonStorey7_t1409908404_gp_0_0_0_0 = { 1, GenInst_U3CDeleteAuthViaIDU3Ec__AnonStorey7_t1409908404_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t1268384018_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t1268384018_gp_0_0_0_0_Types[] = { &U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t1268384018_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t1268384018_gp_0_0_0_0 = { 1, GenInst_U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t1268384018_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeleteTwitterAuthU3Ec__AnonStorey9_t2609573364_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeleteTwitterAuthU3Ec__AnonStorey9_t2609573364_gp_0_0_0_0_Types[] = { &U3CDeleteTwitterAuthU3Ec__AnonStorey9_t2609573364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeleteTwitterAuthU3Ec__AnonStorey9_t2609573364_gp_0_0_0_0 = { 1, GenInst_U3CDeleteTwitterAuthU3Ec__AnonStorey9_t2609573364_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t417802369_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t417802369_gp_0_0_0_0_Types[] = { &U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t417802369_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t417802369_gp_0_0_0_0 = { 1, GenInst_U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t417802369_gp_0_0_0_0_Types };
extern const Il2CppType U3CLogoutU3Ec__AnonStoreyB_t1763411585_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLogoutU3Ec__AnonStoreyB_t1763411585_gp_0_0_0_0_Types[] = { &U3CLogoutU3Ec__AnonStoreyB_t1763411585_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLogoutU3Ec__AnonStoreyB_t1763411585_gp_0_0_0_0 = { 1, GenInst_U3CLogoutU3Ec__AnonStoreyB_t1763411585_gp_0_0_0_0_Types };
extern const Il2CppType U3CResetPasswordU3Ec__AnonStoreyC_t2840379088_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CResetPasswordU3Ec__AnonStoreyC_t2840379088_gp_0_0_0_0_Types[] = { &U3CResetPasswordU3Ec__AnonStoreyC_t2840379088_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CResetPasswordU3Ec__AnonStoreyC_t2840379088_gp_0_0_0_0 = { 1, GenInst_U3CResetPasswordU3Ec__AnonStoreyC_t2840379088_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t2337793304_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t2337793304_gp_0_0_0_0_Types[] = { &U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t2337793304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t2337793304_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t2337793304_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t2162634300_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t2162634300_gp_0_0_0_0_Types[] = { &U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t2162634300_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t2162634300_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t2162634300_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveUsersPendindFriendRequestsU3Ec__AnonStoreyF_t4120878747_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveUsersPendindFriendRequestsU3Ec__AnonStoreyF_t4120878747_gp_0_0_0_0_Types[] = { &U3CRetrieveUsersPendindFriendRequestsU3Ec__AnonStoreyF_t4120878747_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveUsersPendindFriendRequestsU3Ec__AnonStoreyF_t4120878747_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveUsersPendindFriendRequestsU3Ec__AnonStoreyF_t4120878747_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2590691728_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2590691728_gp_0_0_0_0_Types[] = { &U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2590691728_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2590691728_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2590691728_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t3361226555_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t3361226555_gp_0_0_0_0_Types[] = { &U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t3361226555_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t3361226555_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t3361226555_gp_0_0_0_0_Types };
extern const Il2CppType U3CAddNewFriendU3Ec__AnonStorey12_t2594874173_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAddNewFriendU3Ec__AnonStorey12_t2594874173_gp_0_0_0_0_Types[] = { &U3CAddNewFriendU3Ec__AnonStorey12_t2594874173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAddNewFriendU3Ec__AnonStorey12_t2594874173_gp_0_0_0_0 = { 1, GenInst_U3CAddNewFriendU3Ec__AnonStorey12_t2594874173_gp_0_0_0_0_Types };
extern const Il2CppType U3CRemoveExistingFriendU3Ec__AnonStorey13_t1942826340_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRemoveExistingFriendU3Ec__AnonStorey13_t1942826340_gp_0_0_0_0_Types[] = { &U3CRemoveExistingFriendU3Ec__AnonStorey13_t1942826340_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRemoveExistingFriendU3Ec__AnonStorey13_t1942826340_gp_0_0_0_0 = { 1, GenInst_U3CRemoveExistingFriendU3Ec__AnonStorey13_t1942826340_gp_0_0_0_0_Types };
extern const Il2CppType U3CAcceptFriendRequestU3Ec__AnonStorey14_t1031038011_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAcceptFriendRequestU3Ec__AnonStorey14_t1031038011_gp_0_0_0_0_Types[] = { &U3CAcceptFriendRequestU3Ec__AnonStorey14_t1031038011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAcceptFriendRequestU3Ec__AnonStorey14_t1031038011_gp_0_0_0_0 = { 1, GenInst_U3CAcceptFriendRequestU3Ec__AnonStorey14_t1031038011_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeclineFriendRequestU3Ec__AnonStorey15_t3374200632_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeclineFriendRequestU3Ec__AnonStorey15_t3374200632_gp_0_0_0_0_Types[] = { &U3CDeclineFriendRequestU3Ec__AnonStorey15_t3374200632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeclineFriendRequestU3Ec__AnonStorey15_t3374200632_gp_0_0_0_0 = { 1, GenInst_U3CDeclineFriendRequestU3Ec__AnonStorey15_t3374200632_gp_0_0_0_0_Types };
extern const Il2CppType U3CRevokeFriendRequestU3Ec__AnonStorey16_t3843201513_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRevokeFriendRequestU3Ec__AnonStorey16_t3843201513_gp_0_0_0_0_Types[] = { &U3CRevokeFriendRequestU3Ec__AnonStorey16_t3843201513_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRevokeFriendRequestU3Ec__AnonStorey16_t3843201513_gp_0_0_0_0 = { 1, GenInst_U3CRevokeFriendRequestU3Ec__AnonStorey16_t3843201513_gp_0_0_0_0_Types };
extern const Il2CppType U3CImportContatcsU3Ec__AnonStorey17_t4076402405_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CImportContatcsU3Ec__AnonStorey17_t4076402405_gp_0_0_0_0_Types[] = { &U3CImportContatcsU3Ec__AnonStorey17_t4076402405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CImportContatcsU3Ec__AnonStorey17_t4076402405_gp_0_0_0_0 = { 1, GenInst_U3CImportContatcsU3Ec__AnonStorey17_t4076402405_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeleteAllContactsU3Ec__AnonStorey18_t3160876367_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeleteAllContactsU3Ec__AnonStorey18_t3160876367_gp_0_0_0_0_Types[] = { &U3CDeleteAllContactsU3Ec__AnonStorey18_t3160876367_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeleteAllContactsU3Ec__AnonStorey18_t3160876367_gp_0_0_0_0 = { 1, GenInst_U3CDeleteAllContactsU3Ec__AnonStorey18_t3160876367_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeleteSingleContactU3Ec__AnonStorey19_t3571938214_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeleteSingleContactU3Ec__AnonStorey19_t3571938214_gp_0_0_0_0_Types[] = { &U3CDeleteSingleContactU3Ec__AnonStorey19_t3571938214_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeleteSingleContactU3Ec__AnonStorey19_t3571938214_gp_0_0_0_0 = { 1, GenInst_U3CDeleteSingleContactU3Ec__AnonStorey19_t3571938214_gp_0_0_0_0_Types };
extern const Il2CppType U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t3127642584_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t3127642584_gp_0_0_0_0_Types[] = { &U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t3127642584_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t3127642584_gp_0_0_0_0 = { 1, GenInst_U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t3127642584_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetExistingUserU3Ec__AnonStorey1B_t4228972032_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetExistingUserU3Ec__AnonStorey1B_t4228972032_gp_0_0_0_0_Types[] = { &U3CGetExistingUserU3Ec__AnonStorey1B_t4228972032_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetExistingUserU3Ec__AnonStorey1B_t4228972032_gp_0_0_0_0 = { 1, GenInst_U3CGetExistingUserU3Ec__AnonStorey1B_t4228972032_gp_0_0_0_0_Types };
extern const Il2CppType U3CRegisterNewUserU3Ec__AnonStorey1C_t3612730071_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRegisterNewUserU3Ec__AnonStorey1C_t3612730071_gp_0_0_0_0_Types[] = { &U3CRegisterNewUserU3Ec__AnonStorey1C_t3612730071_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRegisterNewUserU3Ec__AnonStorey1C_t3612730071_gp_0_0_0_0 = { 1, GenInst_U3CRegisterNewUserU3Ec__AnonStorey1C_t3612730071_gp_0_0_0_0_Types };
extern const Il2CppType U3CUpdateExistingUserU3Ec__AnonStorey1D_t1768280839_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CUpdateExistingUserU3Ec__AnonStorey1D_t1768280839_gp_0_0_0_0_Types[] = { &U3CUpdateExistingUserU3Ec__AnonStorey1D_t1768280839_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CUpdateExistingUserU3Ec__AnonStorey1D_t1768280839_gp_0_0_0_0 = { 1, GenInst_U3CUpdateExistingUserU3Ec__AnonStorey1D_t1768280839_gp_0_0_0_0_Types };
extern const Il2CppType U3CSavePushTokenU3Ec__AnonStorey1E_t911289941_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSavePushTokenU3Ec__AnonStorey1E_t911289941_gp_0_0_0_0_Types[] = { &U3CSavePushTokenU3Ec__AnonStorey1E_t911289941_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSavePushTokenU3Ec__AnonStorey1E_t911289941_gp_0_0_0_0 = { 1, GenInst_U3CSavePushTokenU3Ec__AnonStorey1E_t911289941_gp_0_0_0_0_Types };
extern const Il2CppType U3CLoadImageFromURLU3Ec__AnonStorey1F_t2208707420_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLoadImageFromURLU3Ec__AnonStorey1F_t2208707420_gp_0_0_0_0_Types[] = { &U3CLoadImageFromURLU3Ec__AnonStorey1F_t2208707420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLoadImageFromURLU3Ec__AnonStorey1F_t2208707420_gp_0_0_0_0 = { 1, GenInst_U3CLoadImageFromURLU3Ec__AnonStorey1F_t2208707420_gp_0_0_0_0_Types };
extern const Il2CppType ChatServer_1_t2802672986_gp_0_0_0_0;
static const Il2CppType* GenInst_ChatServer_1_t2802672986_gp_0_0_0_0_Types[] = { &ChatServer_1_t2802672986_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatServer_1_t2802672986_gp_0_0_0_0 = { 1, GenInst_ChatServer_1_t2802672986_gp_0_0_0_0_Types };
extern const Il2CppType U3COpenNewThreadU3Ec__AnonStorey0_t3278697514_gp_0_0_0_0;
static const Il2CppType* GenInst_U3COpenNewThreadU3Ec__AnonStorey0_t3278697514_gp_0_0_0_0_Types[] = { &U3COpenNewThreadU3Ec__AnonStorey0_t3278697514_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3COpenNewThreadU3Ec__AnonStorey0_t3278697514_gp_0_0_0_0 = { 1, GenInst_U3COpenNewThreadU3Ec__AnonStorey0_t3278697514_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveOpenThreadsU3Ec__AnonStorey1_t3799062252_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveOpenThreadsU3Ec__AnonStorey1_t3799062252_gp_0_0_0_0_Types[] = { &U3CRetrieveOpenThreadsU3Ec__AnonStorey1_t3799062252_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveOpenThreadsU3Ec__AnonStorey1_t3799062252_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveOpenThreadsU3Ec__AnonStorey1_t3799062252_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t1275715504_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t1275715504_gp_0_0_0_0_Types[] = { &U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t1275715504_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t1275715504_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t1275715504_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t1603105993_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t1603105993_gp_0_0_0_0_Types[] = { &U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t1603105993_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t1603105993_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t1603105993_gp_0_0_0_0_Types };
extern const Il2CppType U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t2099807180_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t2099807180_gp_0_0_0_0_Types[] = { &U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t2099807180_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t2099807180_gp_0_0_0_0 = { 1, GenInst_U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t2099807180_gp_0_0_0_0_Types };
extern const Il2CppType U3CAddUserToThreadU3Ec__AnonStorey5_t1890844300_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CAddUserToThreadU3Ec__AnonStorey5_t1890844300_gp_0_0_0_0_Types[] = { &U3CAddUserToThreadU3Ec__AnonStorey5_t1890844300_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CAddUserToThreadU3Ec__AnonStorey5_t1890844300_gp_0_0_0_0 = { 1, GenInst_U3CAddUserToThreadU3Ec__AnonStorey5_t1890844300_gp_0_0_0_0_Types };
extern const Il2CppType U3CPostMessageToThreadU3Ec__AnonStorey6_t499261174_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CPostMessageToThreadU3Ec__AnonStorey6_t499261174_gp_0_0_0_0_Types[] = { &U3CPostMessageToThreadU3Ec__AnonStorey6_t499261174_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CPostMessageToThreadU3Ec__AnonStorey6_t499261174_gp_0_0_0_0 = { 1, GenInst_U3CPostMessageToThreadU3Ec__AnonStorey6_t499261174_gp_0_0_0_0_Types };
extern const Il2CppType U3CUpdateThreadReadU3Ec__AnonStorey7_t311204856_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CUpdateThreadReadU3Ec__AnonStorey7_t311204856_gp_0_0_0_0_Types[] = { &U3CUpdateThreadReadU3Ec__AnonStorey7_t311204856_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CUpdateThreadReadU3Ec__AnonStorey7_t311204856_gp_0_0_0_0 = { 1, GenInst_U3CUpdateThreadReadU3Ec__AnonStorey7_t311204856_gp_0_0_0_0_Types };
extern const Il2CppType U3CDeleteThreadU3Ec__AnonStorey8_t2511843973_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDeleteThreadU3Ec__AnonStorey8_t2511843973_gp_0_0_0_0_Types[] = { &U3CDeleteThreadU3Ec__AnonStorey8_t2511843973_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDeleteThreadU3Ec__AnonStorey8_t2511843973_gp_0_0_0_0 = { 1, GenInst_U3CDeleteThreadU3Ec__AnonStorey8_t2511843973_gp_0_0_0_0_Types };
extern const Il2CppType EnumSwitch_1_t1206431522_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumSwitch_1_t1206431522_gp_0_0_0_0_Types[] = { &EnumSwitch_1_t1206431522_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumSwitch_1_t1206431522_gp_0_0_0_0 = { 1, GenInst_EnumSwitch_1_t1206431522_gp_0_0_0_0_Types };
extern const Il2CppType BasePurchaseManager_2_t3333410625_gp_0_0_0_0;
static const Il2CppType* GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0_Types[] = { &BasePurchaseManager_2_t3333410625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0 = { 1, GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0_Types };
extern const Il2CppType BasePurchaseManager_2_t3333410625_gp_1_0_0_0;
static const Il2CppType* GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0_BasePurchaseManager_2_t3333410625_gp_1_0_0_0_Types[] = { &BasePurchaseManager_2_t3333410625_gp_0_0_0_0, &BasePurchaseManager_2_t3333410625_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0_BasePurchaseManager_2_t3333410625_gp_1_0_0_0 = { 2, GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0_BasePurchaseManager_2_t3333410625_gp_1_0_0_0_Types };
extern const Il2CppType AudioManager_1_t2880363018_gp_0_0_0_0;
static const Il2CppType* GenInst_AudioManager_1_t2880363018_gp_0_0_0_0_Types[] = { &AudioManager_1_t2880363018_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioManager_1_t2880363018_gp_0_0_0_0 = { 1, GenInst_AudioManager_1_t2880363018_gp_0_0_0_0_Types };
extern const Il2CppType BaseInfiniteListItem_1_t4091055671_gp_0_0_0_0;
static const Il2CppType* GenInst_BaseInfiniteListItem_1_t4091055671_gp_0_0_0_0_Types[] = { &BaseInfiniteListItem_1_t4091055671_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInfiniteListItem_1_t4091055671_gp_0_0_0_0 = { 1, GenInst_BaseInfiniteListItem_1_t4091055671_gp_0_0_0_0_Types };
extern const Il2CppType BaseInfiniteListPopulator_1_t409764030_gp_0_0_0_0;
static const Il2CppType* GenInst_BaseInfiniteListPopulator_1_t409764030_gp_0_0_0_0_Types[] = { &BaseInfiniteListPopulator_1_t409764030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInfiniteListPopulator_1_t409764030_gp_0_0_0_0 = { 1, GenInst_BaseInfiniteListPopulator_1_t409764030_gp_0_0_0_0_Types };
extern const Il2CppType ListMovementDirection_t356946411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListMovementDirection_t356946411_gp_0_0_0_0_Types[] = { &ListMovementDirection_t356946411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListMovementDirection_t356946411_gp_0_0_0_0 = { 1, GenInst_ListMovementDirection_t356946411_gp_0_0_0_0_Types };
extern const Il2CppType U3CItemIsInvisibleU3Ec__Iterator0_t1925290786_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CItemIsInvisibleU3Ec__Iterator0_t1925290786_gp_0_0_0_0_Types[] = { &U3CItemIsInvisibleU3Ec__Iterator0_t1925290786_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CItemIsInvisibleU3Ec__Iterator0_t1925290786_gp_0_0_0_0 = { 1, GenInst_U3CItemIsInvisibleU3Ec__Iterator0_t1925290786_gp_0_0_0_0_Types };
extern const Il2CppType U3CSetPanelPosOverTimeU3Ec__Iterator1_t1729400738_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSetPanelPosOverTimeU3Ec__Iterator1_t1729400738_gp_0_0_0_0_Types[] = { &U3CSetPanelPosOverTimeU3Ec__Iterator1_t1729400738_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSetPanelPosOverTimeU3Ec__Iterator1_t1729400738_gp_0_0_0_0 = { 1, GenInst_U3CSetPanelPosOverTimeU3Ec__Iterator1_t1729400738_gp_0_0_0_0_Types };
extern const Il2CppType BaseSceneManager_1_t1474365667_gp_0_0_0_0;
static const Il2CppType* GenInst_BaseSceneManager_1_t1474365667_gp_0_0_0_0_Types[] = { &BaseSceneManager_1_t1474365667_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseSceneManager_1_t1474365667_gp_0_0_0_0 = { 1, GenInst_BaseSceneManager_1_t1474365667_gp_0_0_0_0_Types };
extern const Il2CppType U3CSceneLoadU3Ec__Iterator0_t664871687_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSceneLoadU3Ec__Iterator0_t664871687_gp_0_0_0_0_Types[] = { &U3CSceneLoadU3Ec__Iterator0_t664871687_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSceneLoadU3Ec__Iterator0_t664871687_gp_0_0_0_0 = { 1, GenInst_U3CSceneLoadU3Ec__Iterator0_t664871687_gp_0_0_0_0_Types };
extern const Il2CppType GameState_1_t3222403128_gp_0_0_0_0;
static const Il2CppType* GenInst_GameState_1_t3222403128_gp_0_0_0_0_Types[] = { &GameState_1_t3222403128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameState_1_t3222403128_gp_0_0_0_0 = { 1, GenInst_GameState_1_t3222403128_gp_0_0_0_0_Types };
extern const Il2CppType MonoSingleton_1_t3179822507_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoSingleton_1_t3179822507_gp_0_0_0_0_Types[] = { &MonoSingleton_1_t3179822507_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoSingleton_1_t3179822507_gp_0_0_0_0 = { 1, GenInst_MonoSingleton_1_t3179822507_gp_0_0_0_0_Types };
extern const Il2CppType NavigationController_2_t2720678560_gp_0_0_0_0;
static const Il2CppType* GenInst_NavigationController_2_t2720678560_gp_0_0_0_0_Types[] = { &NavigationController_2_t2720678560_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NavigationController_2_t2720678560_gp_0_0_0_0 = { 1, GenInst_NavigationController_2_t2720678560_gp_0_0_0_0_Types };
extern const Il2CppType NavigationController_2_t2720678560_gp_1_0_0_0;
static const Il2CppType* GenInst_NavigationController_2_t2720678560_gp_1_0_0_0_Types[] = { &NavigationController_2_t2720678560_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NavigationController_2_t2720678560_gp_1_0_0_0 = { 1, GenInst_NavigationController_2_t2720678560_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_NavigationController_2_t2720678560_gp_0_0_0_0_NavigationController_2_t2720678560_gp_1_0_0_0_Types[] = { &NavigationController_2_t2720678560_gp_0_0_0_0, &NavigationController_2_t2720678560_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NavigationController_2_t2720678560_gp_0_0_0_0_NavigationController_2_t2720678560_gp_1_0_0_0 = { 2, GenInst_NavigationController_2_t2720678560_gp_0_0_0_0_NavigationController_2_t2720678560_gp_1_0_0_0_Types };
extern const Il2CppType PanelTransitionManager_1_t1801572561_gp_0_0_0_0;
static const Il2CppType* GenInst_PanelTransitionManager_1_t1801572561_gp_0_0_0_0_Types[] = { &PanelTransitionManager_1_t1801572561_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelTransitionManager_1_t1801572561_gp_0_0_0_0 = { 1, GenInst_PanelTransitionManager_1_t1801572561_gp_0_0_0_0_Types };
extern const Il2CppType U3CSwitchPanelsU3Ec__Iterator0_t1828045924_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSwitchPanelsU3Ec__Iterator0_t1828045924_gp_0_0_0_0_Types[] = { &U3CSwitchPanelsU3Ec__Iterator0_t1828045924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSwitchPanelsU3Ec__Iterator0_t1828045924_gp_0_0_0_0 = { 1, GenInst_U3CSwitchPanelsU3Ec__Iterator0_t1828045924_gp_0_0_0_0_Types };
extern const Il2CppType U3CTweenObjectForwardsU3Ec__Iterator1_t1210270656_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CTweenObjectForwardsU3Ec__Iterator1_t1210270656_gp_0_0_0_0_Types[] = { &U3CTweenObjectForwardsU3Ec__Iterator1_t1210270656_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CTweenObjectForwardsU3Ec__Iterator1_t1210270656_gp_0_0_0_0 = { 1, GenInst_U3CTweenObjectForwardsU3Ec__Iterator1_t1210270656_gp_0_0_0_0_Types };
extern const Il2CppType FBManager_1_t2471193464_gp_0_0_0_0;
static const Il2CppType* GenInst_FBManager_1_t2471193464_gp_0_0_0_0_Types[] = { &FBManager_1_t2471193464_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_FBManager_1_t2471193464_gp_0_0_0_0 = { 1, GenInst_FBManager_1_t2471193464_gp_0_0_0_0_Types };
extern const Il2CppType LoadPictureCallback_t2054799482_gp_0_0_0_0;
static const Il2CppType* GenInst_LoadPictureCallback_t2054799482_gp_0_0_0_0_Types[] = { &LoadPictureCallback_t2054799482_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LoadPictureCallback_t2054799482_gp_0_0_0_0 = { 1, GenInst_LoadPictureCallback_t2054799482_gp_0_0_0_0_Types };
extern const Il2CppType U3CPostHighScoreU3Ec__AnonStorey2_t3067682344_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CPostHighScoreU3Ec__AnonStorey2_t3067682344_gp_0_0_0_0_Types[] = { &U3CPostHighScoreU3Ec__AnonStorey2_t3067682344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CPostHighScoreU3Ec__AnonStorey2_t3067682344_gp_0_0_0_0 = { 1, GenInst_U3CPostHighScoreU3Ec__AnonStorey2_t3067682344_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t311754389_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t311754389_gp_0_0_0_0_Types[] = { &U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t311754389_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t311754389_gp_0_0_0_0 = { 1, GenInst_U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t311754389_gp_0_0_0_0_Types };
extern const Il2CppType U3CCheckScoreUpdateU3Ec__Iterator0_t1608651375_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCheckScoreUpdateU3Ec__Iterator0_t1608651375_gp_0_0_0_0_Types[] = { &U3CCheckScoreUpdateU3Ec__Iterator0_t1608651375_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCheckScoreUpdateU3Ec__Iterator0_t1608651375_gp_0_0_0_0 = { 1, GenInst_U3CCheckScoreUpdateU3Ec__Iterator0_t1608651375_gp_0_0_0_0_Types };
extern const Il2CppType U3CLoadPictureAPIU3Ec__AnonStorey4_t3748439766_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLoadPictureAPIU3Ec__AnonStorey4_t3748439766_gp_0_0_0_0_Types[] = { &U3CLoadPictureAPIU3Ec__AnonStorey4_t3748439766_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLoadPictureAPIU3Ec__AnonStorey4_t3748439766_gp_0_0_0_0 = { 1, GenInst_U3CLoadPictureAPIU3Ec__AnonStorey4_t3748439766_gp_0_0_0_0_Types };
extern const Il2CppType U3CLoadPictureEnumeratorU3Ec__Iterator1_t2410595175_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CLoadPictureEnumeratorU3Ec__Iterator1_t2410595175_gp_0_0_0_0_Types[] = { &U3CLoadPictureEnumeratorU3Ec__Iterator1_t2410595175_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CLoadPictureEnumeratorU3Ec__Iterator1_t2410595175_gp_0_0_0_0 = { 1, GenInst_U3CLoadPictureEnumeratorU3Ec__Iterator1_t2410595175_gp_0_0_0_0_Types };
extern const Il2CppType BaseTwitterManager_1_t2537397012_gp_0_0_0_0;
static const Il2CppType* GenInst_BaseTwitterManager_1_t2537397012_gp_0_0_0_0_Types[] = { &BaseTwitterManager_1_t2537397012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseTwitterManager_1_t2537397012_gp_0_0_0_0 = { 1, GenInst_BaseTwitterManager_1_t2537397012_gp_0_0_0_0_Types };
extern const Il2CppType RequestType_t4084858450_gp_0_0_0_0;
static const Il2CppType* GenInst_RequestType_t4084858450_gp_0_0_0_0_Types[] = { &RequestType_t4084858450_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RequestType_t4084858450_gp_0_0_0_0 = { 1, GenInst_RequestType_t4084858450_gp_0_0_0_0_Types };
extern const Il2CppType U3CCheckLogingSuccessU3Ec__Iterator0_t2600799761_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCheckLogingSuccessU3Ec__Iterator0_t2600799761_gp_0_0_0_0_Types[] = { &U3CCheckLogingSuccessU3Ec__Iterator0_t2600799761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCheckLogingSuccessU3Ec__Iterator0_t2600799761_gp_0_0_0_0 = { 1, GenInst_U3CCheckLogingSuccessU3Ec__Iterator0_t2600799761_gp_0_0_0_0_Types };
extern const Il2CppType ListExtension_ConvertAll_m3788713098_gp_0_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0_Types[] = { &Char_t3454481338_0_0_0, &ListExtension_ConvertAll_m3788713098_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0_Types[] = { &ListExtension_ConvertAll_m3788713098_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0 = { 1, GenInst_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerator_1_t105163639_0_0_0;
static const Il2CppType* GenInst_IEnumerator_1_t105163639_0_0_0_Types[] = { &IEnumerator_1_t105163639_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerator_1_t105163639_0_0_0 = { 1, GenInst_IEnumerator_1_t105163639_0_0_0_Types };
extern const Il2CppType EnumerableFromConstructor_1_t1982007363_gp_0_0_0_0;
static const Il2CppType* GenInst_EnumerableFromConstructor_1_t1982007363_gp_0_0_0_0_Types[] = { &EnumerableFromConstructor_1_t1982007363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EnumerableFromConstructor_1_t1982007363_gp_0_0_0_0 = { 1, GenInst_EnumerableFromConstructor_1_t1982007363_gp_0_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0;
extern const Il2CppType U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0, &U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 = { 2, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0 = { 1, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0 = { 1, GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0;
extern const Il2CppType U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0, &U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 = { 2, GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_Types[] = { &U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0 = { 1, GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0 = { 1, GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0_Types };
extern const Il2CppType U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0;
extern const Il2CppType U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0;
extern const Il2CppType U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0;
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0, &U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0, &U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 = { 3, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0 = { 1, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0 = { 1, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types[] = { &U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0 = { 1, GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { &DefaultExecutionOrder_t2717914595_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
extern const Il2CppType FacebookSettings_t2167659529_0_0_0;
static const Il2CppType* GenInst_FacebookSettings_t2167659529_0_0_0_Types[] = { &FacebookSettings_t2167659529_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookSettings_t2167659529_0_0_0 = { 1, GenInst_FacebookSettings_t2167659529_0_0_0_Types };
extern const Il2CppType LifecycleHelper_t337468626_0_0_0;
static const Il2CppType* GenInst_LifecycleHelper_t337468626_0_0_0_Types[] = { &LifecycleHelper_t337468626_0_0_0 };
extern const Il2CppGenericInst GenInst_LifecycleHelper_t337468626_0_0_0 = { 1, GenInst_LifecycleHelper_t337468626_0_0_0_Types };
extern const Il2CppType ThreadingCallbackHelper_t3782680035_0_0_0;
static const Il2CppType* GenInst_ThreadingCallbackHelper_t3782680035_0_0_0_Types[] = { &ThreadingCallbackHelper_t3782680035_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadingCallbackHelper_t3782680035_0_0_0 = { 1, GenInst_ThreadingCallbackHelper_t3782680035_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2603311978_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2603311978_0_0_0_Types[] = { &IDictionary_2_t2603311978_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2603311978_0_0_0 = { 1, GenInst_IDictionary_2_t2603311978_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2981576340_0_0_0_Types[] = { &IEnumerable_1_t2981576340_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2981576340_0_0_0 = { 1, GenInst_IEnumerable_1_t2981576340_0_0_0_Types };
extern const Il2CppType ArcadeFacebookGameObject_t1372893397_0_0_0;
static const Il2CppType* GenInst_ArcadeFacebookGameObject_t1372893397_0_0_0_Types[] = { &ArcadeFacebookGameObject_t1372893397_0_0_0 };
extern const Il2CppGenericInst GenInst_ArcadeFacebookGameObject_t1372893397_0_0_0 = { 1, GenInst_ArcadeFacebookGameObject_t1372893397_0_0_0_Types };
extern const Il2CppType AsyncRequestString_t2888107224_0_0_0;
static const Il2CppType* GenInst_AsyncRequestString_t2888107224_0_0_0_Types[] = { &AsyncRequestString_t2888107224_0_0_0 };
extern const Il2CppGenericInst GenInst_AsyncRequestString_t2888107224_0_0_0 = { 1, GenInst_AsyncRequestString_t2888107224_0_0_0_Types };
extern const Il2CppType IList_1_t3230389896_0_0_0;
static const Il2CppType* GenInst_IList_1_t3230389896_0_0_0_Types[] = { &IList_1_t3230389896_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3230389896_0_0_0 = { 1, GenInst_IList_1_t3230389896_0_0_0_Types };
extern const Il2CppType JsBridge_t2435492180_0_0_0;
static const Il2CppType* GenInst_JsBridge_t2435492180_0_0_0_Types[] = { &JsBridge_t2435492180_0_0_0 };
extern const Il2CppGenericInst GenInst_JsBridge_t2435492180_0_0_0 = { 1, GenInst_JsBridge_t2435492180_0_0_0_Types };
extern const Il2CppType CanvasFacebookGameObject_t1732848805_0_0_0;
static const Il2CppType* GenInst_CanvasFacebookGameObject_t1732848805_0_0_0_Types[] = { &CanvasFacebookGameObject_t1732848805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasFacebookGameObject_t1732848805_0_0_0 = { 1, GenInst_CanvasFacebookGameObject_t1732848805_0_0_0_Types };
extern const Il2CppType EditorFacebookGameObject_t63110230_0_0_0;
static const Il2CppType* GenInst_EditorFacebookGameObject_t63110230_0_0_0_Types[] = { &EditorFacebookGameObject_t63110230_0_0_0 };
extern const Il2CppGenericInst GenInst_EditorFacebookGameObject_t63110230_0_0_0 = { 1, GenInst_EditorFacebookGameObject_t63110230_0_0_0_Types };
extern const Il2CppType MockLoginDialog_t531765055_0_0_0;
static const Il2CppType* GenInst_MockLoginDialog_t531765055_0_0_0_Types[] = { &MockLoginDialog_t531765055_0_0_0 };
extern const Il2CppGenericInst GenInst_MockLoginDialog_t531765055_0_0_0 = { 1, GenInst_MockLoginDialog_t531765055_0_0_0_Types };
extern const Il2CppType MockShareDialog_t1542668653_0_0_0;
static const Il2CppType* GenInst_MockShareDialog_t1542668653_0_0_0_Types[] = { &MockShareDialog_t1542668653_0_0_0 };
extern const Il2CppGenericInst GenInst_MockShareDialog_t1542668653_0_0_0 = { 1, GenInst_MockShareDialog_t1542668653_0_0_0_Types };
extern const Il2CppType EmptyMockDialog_t2854396319_0_0_0;
static const Il2CppType* GenInst_EmptyMockDialog_t2854396319_0_0_0_Types[] = { &EmptyMockDialog_t2854396319_0_0_0 };
extern const Il2CppGenericInst GenInst_EmptyMockDialog_t2854396319_0_0_0 = { 1, GenInst_EmptyMockDialog_t2854396319_0_0_0_Types };
extern const Il2CppType EditorFacebookLoader_t2109505584_0_0_0;
static const Il2CppType* GenInst_EditorFacebookLoader_t2109505584_0_0_0_Types[] = { &EditorFacebookLoader_t2109505584_0_0_0 };
extern const Il2CppGenericInst GenInst_EditorFacebookLoader_t2109505584_0_0_0 = { 1, GenInst_EditorFacebookLoader_t2109505584_0_0_0_Types };
extern const Il2CppType CanvasFacebookLoader_t1985905999_0_0_0;
static const Il2CppType* GenInst_CanvasFacebookLoader_t1985905999_0_0_0_Types[] = { &CanvasFacebookLoader_t1985905999_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasFacebookLoader_t1985905999_0_0_0 = { 1, GenInst_CanvasFacebookLoader_t1985905999_0_0_0_Types };
extern const Il2CppType IOSFacebookLoader_t3879535236_0_0_0;
static const Il2CppType* GenInst_IOSFacebookLoader_t3879535236_0_0_0_Types[] = { &IOSFacebookLoader_t3879535236_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebookLoader_t3879535236_0_0_0 = { 1, GenInst_IOSFacebookLoader_t3879535236_0_0_0_Types };
extern const Il2CppType AndroidFacebookLoader_t3776634754_0_0_0;
static const Il2CppType* GenInst_AndroidFacebookLoader_t3776634754_0_0_0_Types[] = { &AndroidFacebookLoader_t3776634754_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidFacebookLoader_t3776634754_0_0_0 = { 1, GenInst_AndroidFacebookLoader_t3776634754_0_0_0_Types };
extern const Il2CppType ArcadeFacebookLoader_t3930015791_0_0_0;
static const Il2CppType* GenInst_ArcadeFacebookLoader_t3930015791_0_0_0_Types[] = { &ArcadeFacebookLoader_t3930015791_0_0_0 };
extern const Il2CppGenericInst GenInst_ArcadeFacebookLoader_t3930015791_0_0_0 = { 1, GenInst_ArcadeFacebookLoader_t3930015791_0_0_0_Types };
extern const Il2CppType AndroidFacebookGameObject_t2149209708_0_0_0;
static const Il2CppType* GenInst_AndroidFacebookGameObject_t2149209708_0_0_0_Types[] = { &AndroidFacebookGameObject_t2149209708_0_0_0 };
extern const Il2CppGenericInst GenInst_AndroidFacebookGameObject_t2149209708_0_0_0 = { 1, GenInst_AndroidFacebookGameObject_t2149209708_0_0_0_Types };
extern const Il2CppType IOSFacebookGameObject_t3391486318_0_0_0;
static const Il2CppType* GenInst_IOSFacebookGameObject_t3391486318_0_0_0_Types[] = { &IOSFacebookGameObject_t3391486318_0_0_0 };
extern const Il2CppGenericInst GenInst_IOSFacebookGameObject_t3391486318_0_0_0 = { 1, GenInst_IOSFacebookGameObject_t3391486318_0_0_0_Types };
extern const Il2CppType EventSystem_t3466835263_0_0_0;
static const Il2CppType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { &EventSystem_t3466835263_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { &AxisEventData_t1524870173_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { &SpriteRenderer_t1209076198_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { &GraphicRaycaster_t410733016_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
extern const Il2CppType Image_t2042527209_0_0_0;
static const Il2CppType* GenInst_Image_t2042527209_0_0_0_Types[] = { &Image_t2042527209_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
extern const Il2CppType Button_t2872111280_0_0_0;
static const Il2CppType* GenInst_Button_t2872111280_0_0_0_Types[] = { &Button_t2872111280_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
extern const Il2CppType Dropdown_t1985816271_0_0_0;
static const Il2CppType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { &Dropdown_t1985816271_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { &CanvasRenderer_t261436805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
extern const Il2CppType Corner_t1077473318_0_0_0;
static const Il2CppType* GenInst_Corner_t1077473318_0_0_0_Types[] = { &Corner_t1077473318_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
extern const Il2CppType Axis_t1431825778_0_0_0;
static const Il2CppType* GenInst_Axis_t1431825778_0_0_0_Types[] = { &Axis_t1431825778_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
extern const Il2CppType Constraint_t3558160636_0_0_0;
static const Il2CppType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { &Constraint_t3558160636_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { &SubmitEvent_t907918422_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { &OnChangeEvent_t2863344003_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { &OnValidateInput_t1946318473_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { &LayoutElement_t2808691390_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
extern const Il2CppType RectOffset_t3387826427_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
extern const Il2CppType TextAnchor_t112990806_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { &TextAnchor_t112990806_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { &AnimationTriggers_t3244928895_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
extern const Il2CppType Animator_t69676727_0_0_0;
static const Il2CppType* GenInst_Animator_t69676727_0_0_0_Types[] = { &Animator_t69676727_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
extern const Il2CppType List_1_t3383317540_0_0_0;
static const Il2CppType* GenInst_List_1_t3383317540_0_0_0_Types[] = { &List_1_t3383317540_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3383317540_0_0_0 = { 1, GenInst_List_1_t3383317540_0_0_0_Types };
extern const Il2CppType Renderer_t257310565_0_0_0;
static const Il2CppType* GenInst_Renderer_t257310565_0_0_0_Types[] = { &Renderer_t257310565_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0 = { 1, GenInst_Renderer_t257310565_0_0_0_Types };
static const Il2CppType* GenInst_FacebookFriendsResult_t459691178_0_0_0_Types[] = { &FacebookFriendsResult_t459691178_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookFriendsResult_t459691178_0_0_0 = { 1, GenInst_FacebookFriendsResult_t459691178_0_0_0_Types };
static const Il2CppType* GenInst_FacebookMeResult_t187696501_0_0_0_Types[] = { &FacebookMeResult_t187696501_0_0_0 };
extern const Il2CppGenericInst GenInst_FacebookMeResult_t187696501_0_0_0 = { 1, GenInst_FacebookMeResult_t187696501_0_0_0_Types };
extern const Il2CppType MeshRenderer_t1268241104_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t1268241104_0_0_0_Types[] = { &MeshRenderer_t1268241104_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1268241104_0_0_0 = { 1, GenInst_MeshRenderer_t1268241104_0_0_0_Types };
extern const Il2CppType PurchaseType_t1639241305_0_0_0;
static const Il2CppType* GenInst_PurchaseType_t1639241305_0_0_0_Types[] = { &PurchaseType_t1639241305_0_0_0 };
extern const Il2CppGenericInst GenInst_PurchaseType_t1639241305_0_0_0 = { 1, GenInst_PurchaseType_t1639241305_0_0_0_Types };
extern const Il2CppType WP8Eventhook_t2441977752_0_0_0;
static const Il2CppType* GenInst_WP8Eventhook_t2441977752_0_0_0_Types[] = { &WP8Eventhook_t2441977752_0_0_0 };
extern const Il2CppGenericInst GenInst_WP8Eventhook_t2441977752_0_0_0 = { 1, GenInst_WP8Eventhook_t2441977752_0_0_0_Types };
extern const Il2CppType Win8Eventhook_t3380410125_0_0_0;
static const Il2CppType* GenInst_Win8Eventhook_t3380410125_0_0_0_Types[] = { &Win8Eventhook_t3380410125_0_0_0 };
extern const Il2CppGenericInst GenInst_Win8Eventhook_t3380410125_0_0_0 = { 1, GenInst_Win8Eventhook_t3380410125_0_0_0_Types };
extern const Il2CppType UnityLevelLoadListener_t2593383553_0_0_0;
static const Il2CppType* GenInst_UnityLevelLoadListener_t2593383553_0_0_0_Types[] = { &UnityLevelLoadListener_t2593383553_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityLevelLoadListener_t2593383553_0_0_0 = { 1, GenInst_UnityLevelLoadListener_t2593383553_0_0_0_Types };
extern const Il2CppType RemoteConfigFetcher_t2652256879_0_0_0;
static const Il2CppType* GenInst_RemoteConfigFetcher_t2652256879_0_0_0_Types[] = { &RemoteConfigFetcher_t2652256879_0_0_0 };
extern const Il2CppGenericInst GenInst_RemoteConfigFetcher_t2652256879_0_0_0 = { 1, GenInst_RemoteConfigFetcher_t2652256879_0_0_0_Types };
extern const Il2CppType AppleAppStoreCallbackMonoBehaviour_t2011772955_0_0_0;
static const Il2CppType* GenInst_AppleAppStoreCallbackMonoBehaviour_t2011772955_0_0_0_Types[] = { &AppleAppStoreCallbackMonoBehaviour_t2011772955_0_0_0 };
extern const Il2CppGenericInst GenInst_AppleAppStoreCallbackMonoBehaviour_t2011772955_0_0_0 = { 1, GenInst_AppleAppStoreCallbackMonoBehaviour_t2011772955_0_0_0_Types };
extern const Il2CppType SamsungAppsMode_t2745005863_0_0_0;
static const Il2CppType* GenInst_SamsungAppsMode_t2745005863_0_0_0_Types[] = { &SamsungAppsMode_t2745005863_0_0_0 };
extern const Il2CppGenericInst GenInst_SamsungAppsMode_t2745005863_0_0_0 = { 1, GenInst_SamsungAppsMode_t2745005863_0_0_0_Types };
extern const Il2CppType UnityUtil_t671748753_0_0_0;
static const Il2CppType* GenInst_UnityUtil_t671748753_0_0_0_Types[] = { &UnityUtil_t671748753_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityUtil_t671748753_0_0_0 = { 1, GenInst_UnityUtil_t671748753_0_0_0_Types };
extern const Il2CppType MeshFilter_t3026937449_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3026937449_0_0_0_Types[] = { &MeshFilter_t3026937449_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3026937449_0_0_0 = { 1, GenInst_MeshFilter_t3026937449_0_0_0_Types };
extern const Il2CppType LineManager_t3088676951_0_0_0;
static const Il2CppType* GenInst_LineManager_t3088676951_0_0_0_Types[] = { &LineManager_t3088676951_0_0_0 };
extern const Il2CppGenericInst GenInst_LineManager_t3088676951_0_0_0 = { 1, GenInst_LineManager_t3088676951_0_0_0_Types };
extern const Il2CppType RegistrationCompleteScreen_t222205918_0_0_0;
static const Il2CppType* GenInst_RegistrationCompleteScreen_t222205918_0_0_0_Types[] = { &RegistrationCompleteScreen_t222205918_0_0_0 };
extern const Il2CppGenericInst GenInst_RegistrationCompleteScreen_t222205918_0_0_0 = { 1, GenInst_RegistrationCompleteScreen_t222205918_0_0_0_Types };
extern const Il2CppType ActiveAnimation_t4137610604_0_0_0;
static const Il2CppType* GenInst_ActiveAnimation_t4137610604_0_0_0_Types[] = { &ActiveAnimation_t4137610604_0_0_0 };
extern const Il2CppGenericInst GenInst_ActiveAnimation_t4137610604_0_0_0 = { 1, GenInst_ActiveAnimation_t4137610604_0_0_0_Types };
extern const Il2CppType StatusIncomingNavScreen_t3315318769_0_0_0;
static const Il2CppType* GenInst_StatusIncomingNavScreen_t3315318769_0_0_0_Types[] = { &StatusIncomingNavScreen_t3315318769_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusIncomingNavScreen_t3315318769_0_0_0 = { 1, GenInst_StatusIncomingNavScreen_t3315318769_0_0_0_Types };
extern const Il2CppType StatusOutgoingNavScreen_t2187904061_0_0_0;
static const Il2CppType* GenInst_StatusOutgoingNavScreen_t2187904061_0_0_0_Types[] = { &StatusOutgoingNavScreen_t2187904061_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusOutgoingNavScreen_t2187904061_0_0_0 = { 1, GenInst_StatusOutgoingNavScreen_t2187904061_0_0_0_Types };
extern const Il2CppType PlayerProfileModal_t3260625137_0_0_0;
static const Il2CppType* GenInst_PlayerProfileModal_t3260625137_0_0_0_Types[] = { &PlayerProfileModal_t3260625137_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerProfileModal_t3260625137_0_0_0 = { 1, GenInst_PlayerProfileModal_t3260625137_0_0_0_Types };
extern const Il2CppType ResultModalUI_t969511824_0_0_0;
static const Il2CppType* GenInst_ResultModalUI_t969511824_0_0_0_Types[] = { &ResultModalUI_t969511824_0_0_0 };
extern const Il2CppGenericInst GenInst_ResultModalUI_t969511824_0_0_0 = { 1, GenInst_ResultModalUI_t969511824_0_0_0_Types };
extern const Il2CppType MapGroupModal_t1530151518_0_0_0;
static const Il2CppType* GenInst_MapGroupModal_t1530151518_0_0_0_Types[] = { &MapGroupModal_t1530151518_0_0_0 };
extern const Il2CppGenericInst GenInst_MapGroupModal_t1530151518_0_0_0 = { 1, GenInst_MapGroupModal_t1530151518_0_0_0_Types };
extern const Il2CppType FriendsInnerNavScreen_t2659475988_0_0_0;
static const Il2CppType* GenInst_FriendsInnerNavScreen_t2659475988_0_0_0_Types[] = { &FriendsInnerNavScreen_t2659475988_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendsInnerNavScreen_t2659475988_0_0_0 = { 1, GenInst_FriendsInnerNavScreen_t2659475988_0_0_0_Types };
extern const Il2CppType FriendsOuterNavScreen_t1899896515_0_0_0;
static const Il2CppType* GenInst_FriendsOuterNavScreen_t1899896515_0_0_0_Types[] = { &FriendsOuterNavScreen_t1899896515_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendsOuterNavScreen_t1899896515_0_0_0 = { 1, GenInst_FriendsOuterNavScreen_t1899896515_0_0_0_Types };
extern const Il2CppType AttackHomeInnerNavScreen_t1310676258_0_0_0;
static const Il2CppType* GenInst_AttackHomeInnerNavScreen_t1310676258_0_0_0_Types[] = { &AttackHomeInnerNavScreen_t1310676258_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackHomeInnerNavScreen_t1310676258_0_0_0 = { 1, GenInst_AttackHomeInnerNavScreen_t1310676258_0_0_0_Types };
extern const Il2CppType AttackHomeOuterNavScreen_t2515452089_0_0_0;
static const Il2CppType* GenInst_AttackHomeOuterNavScreen_t2515452089_0_0_0_Types[] = { &AttackHomeOuterNavScreen_t2515452089_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackHomeOuterNavScreen_t2515452089_0_0_0 = { 1, GenInst_AttackHomeOuterNavScreen_t2515452089_0_0_0_Types };
extern const Il2CppType NavigationScreen_t2333230110_0_0_0;
static const Il2CppType* GenInst_NavigationScreen_t2333230110_0_0_0_Types[] = { &NavigationScreen_t2333230110_0_0_0 };
extern const Il2CppGenericInst GenInst_NavigationScreen_t2333230110_0_0_0 = { 1, GenInst_NavigationScreen_t2333230110_0_0_0_Types };
extern const Il2CppType AudioSource_t1135106623_0_0_0;
static const Il2CppType* GenInst_AudioSource_t1135106623_0_0_0_Types[] = { &AudioSource_t1135106623_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t1135106623_0_0_0 = { 1, GenInst_AudioSource_t1135106623_0_0_0_Types };
extern const Il2CppType HTTPUpdateDelegator_t1331403296_0_0_0;
static const Il2CppType* GenInst_HTTPUpdateDelegator_t1331403296_0_0_0_Types[] = { &HTTPUpdateDelegator_t1331403296_0_0_0 };
extern const Il2CppGenericInst GenInst_HTTPUpdateDelegator_t1331403296_0_0_0 = { 1, GenInst_HTTPUpdateDelegator_t1331403296_0_0_0_Types };
extern const Il2CppType GUITexture_t1909122990_0_0_0;
static const Il2CppType* GenInst_GUITexture_t1909122990_0_0_0_Types[] = { &GUITexture_t1909122990_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t1909122990_0_0_0 = { 1, GenInst_GUITexture_t1909122990_0_0_0_Types };
extern const Il2CppType TweenColor_t3390486518_0_0_0;
static const Il2CppType* GenInst_TweenColor_t3390486518_0_0_0_Types[] = { &TweenColor_t3390486518_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenColor_t3390486518_0_0_0 = { 1, GenInst_TweenColor_t3390486518_0_0_0_Types };
extern const Il2CppType List_1_t849036066_0_0_0;
static const Il2CppType* GenInst_List_1_t849036066_0_0_0_Types[] = { &List_1_t849036066_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t849036066_0_0_0 = { 1, GenInst_List_1_t849036066_0_0_0_Types };
extern const Il2CppType ThreadsNavScreen_t4034840106_0_0_0;
static const Il2CppType* GenInst_ThreadsNavScreen_t4034840106_0_0_0_Types[] = { &ThreadsNavScreen_t4034840106_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadsNavScreen_t4034840106_0_0_0 = { 1, GenInst_ThreadsNavScreen_t4034840106_0_0_0_Types };
extern const Il2CppType SceneLoader_t1952549817_0_0_0;
static const Il2CppType* GenInst_SceneLoader_t1952549817_0_0_0_Types[] = { &SceneLoader_t1952549817_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneLoader_t1952549817_0_0_0 = { 1, GenInst_SceneLoader_t1952549817_0_0_0_Types };
extern const Il2CppType TweenScale_t2697902175_0_0_0;
static const Il2CppType* GenInst_TweenScale_t2697902175_0_0_0_Types[] = { &TweenScale_t2697902175_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenScale_t2697902175_0_0_0 = { 1, GenInst_TweenScale_t2697902175_0_0_0_Types };
extern const Il2CppType DefendMiniGameController_t1844356491_0_0_0;
static const Il2CppType* GenInst_DefendMiniGameController_t1844356491_0_0_0_Types[] = { &DefendMiniGameController_t1844356491_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendMiniGameController_t1844356491_0_0_0 = { 1, GenInst_DefendMiniGameController_t1844356491_0_0_0_Types };
extern const Il2CppType DefendInfoNavScreen_t2323813817_0_0_0;
static const Il2CppType* GenInst_DefendInfoNavScreen_t2323813817_0_0_0_Types[] = { &DefendInfoNavScreen_t2323813817_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendInfoNavScreen_t2323813817_0_0_0 = { 1, GenInst_DefendInfoNavScreen_t2323813817_0_0_0_Types };
extern const Il2CppType DefendGameNavScreen_t2935542205_0_0_0;
static const Il2CppType* GenInst_DefendGameNavScreen_t2935542205_0_0_0_Types[] = { &DefendGameNavScreen_t2935542205_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendGameNavScreen_t2935542205_0_0_0 = { 1, GenInst_DefendGameNavScreen_t2935542205_0_0_0_Types };
extern const Il2CppType OnlineMaps_t1893290312_0_0_0;
static const Il2CppType* GenInst_OnlineMaps_t1893290312_0_0_0_Types[] = { &OnlineMaps_t1893290312_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMaps_t1893290312_0_0_0 = { 1, GenInst_OnlineMaps_t1893290312_0_0_0_Types };
extern const Il2CppType ModalUI_t2568752073_0_0_0;
static const Il2CppType* GenInst_ModalUI_t2568752073_0_0_0_Types[] = { &ModalUI_t2568752073_0_0_0 };
extern const Il2CppGenericInst GenInst_ModalUI_t2568752073_0_0_0 = { 1, GenInst_ModalUI_t2568752073_0_0_0_Types };
static const Il2CppType* GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &OnlineMapsMarker3D_t576815539_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType FriendMarker_t3038476686_0_0_0;
static const Il2CppType* GenInst_FriendMarker_t3038476686_0_0_0_Types[] = { &FriendMarker_t3038476686_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendMarker_t3038476686_0_0_0 = { 1, GenInst_FriendMarker_t3038476686_0_0_0_Types };
extern const Il2CppType ChatLoader_t2661087257_0_0_0;
static const Il2CppType* GenInst_ChatLoader_t2661087257_0_0_0_Types[] = { &ChatLoader_t2661087257_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatLoader_t2661087257_0_0_0 = { 1, GenInst_ChatLoader_t2661087257_0_0_0_Types };
extern const Il2CppType LaunchAnimationController_t825305345_0_0_0;
static const Il2CppType* GenInst_LaunchAnimationController_t825305345_0_0_0_Types[] = { &LaunchAnimationController_t825305345_0_0_0 };
extern const Il2CppGenericInst GenInst_LaunchAnimationController_t825305345_0_0_0 = { 1, GenInst_LaunchAnimationController_t825305345_0_0_0_Types };
extern const Il2CppType UIPopupList_t109953940_0_0_0;
static const Il2CppType* GenInst_UIPopupList_t109953940_0_0_0_Types[] = { &UIPopupList_t109953940_0_0_0 };
extern const Il2CppGenericInst GenInst_UIPopupList_t109953940_0_0_0 = { 1, GenInst_UIPopupList_t109953940_0_0_0_Types };
extern const Il2CppType LaunchRoutine_t3791963153_0_0_0;
static const Il2CppType* GenInst_LaunchRoutine_t3791963153_0_0_0_Types[] = { &LaunchRoutine_t3791963153_0_0_0 };
extern const Il2CppGenericInst GenInst_LaunchRoutine_t3791963153_0_0_0 = { 1, GenInst_LaunchRoutine_t3791963153_0_0_0_Types };
extern const Il2CppType TextAsset_t3973159845_0_0_0;
static const Il2CppType* GenInst_TextAsset_t3973159845_0_0_0_Types[] = { &TextAsset_t3973159845_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAsset_t3973159845_0_0_0 = { 1, GenInst_TextAsset_t3973159845_0_0_0_Types };
extern const Il2CppType LockoutUI_t1337294023_0_0_0;
static const Il2CppType* GenInst_LockoutUI_t1337294023_0_0_0_Types[] = { &LockoutUI_t1337294023_0_0_0 };
extern const Il2CppGenericInst GenInst_LockoutUI_t1337294023_0_0_0 = { 1, GenInst_LockoutUI_t1337294023_0_0_0_Types };
extern const Il2CppType MineMarker_t2486518043_0_0_0;
static const Il2CppType* GenInst_MineMarker_t2486518043_0_0_0_Types[] = { &MineMarker_t2486518043_0_0_0 };
extern const Il2CppGenericInst GenInst_MineMarker_t2486518043_0_0_0 = { 1, GenInst_MineMarker_t2486518043_0_0_0_Types };
extern const Il2CppType PlayerMarker_t2200525527_0_0_0;
static const Il2CppType* GenInst_PlayerMarker_t2200525527_0_0_0_Types[] = { &PlayerMarker_t2200525527_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerMarker_t2200525527_0_0_0 = { 1, GenInst_PlayerMarker_t2200525527_0_0_0_Types };
extern const Il2CppType RadarMarkerBlip_t1130528275_0_0_0;
static const Il2CppType* GenInst_RadarMarkerBlip_t1130528275_0_0_0_Types[] = { &RadarMarkerBlip_t1130528275_0_0_0 };
extern const Il2CppGenericInst GenInst_RadarMarkerBlip_t1130528275_0_0_0 = { 1, GenInst_RadarMarkerBlip_t1130528275_0_0_0_Types };
extern const Il2CppType GroupMarker_t1861645095_0_0_0;
static const Il2CppType* GenInst_GroupMarker_t1861645095_0_0_0_Types[] = { &GroupMarker_t1861645095_0_0_0 };
extern const Il2CppGenericInst GenInst_GroupMarker_t1861645095_0_0_0 = { 1, GenInst_GroupMarker_t1861645095_0_0_0_Types };
extern const Il2CppType CameraFilterPack_Blur_GaussianBlur_t3355292571_0_0_0;
static const Il2CppType* GenInst_CameraFilterPack_Blur_GaussianBlur_t3355292571_0_0_0_Types[] = { &CameraFilterPack_Blur_GaussianBlur_t3355292571_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraFilterPack_Blur_GaussianBlur_t3355292571_0_0_0 = { 1, GenInst_CameraFilterPack_Blur_GaussianBlur_t3355292571_0_0_0_Types };
extern const Il2CppType OnlineMapsControlBase3D_t2801313279_0_0_0;
static const Il2CppType* GenInst_OnlineMapsControlBase3D_t2801313279_0_0_0_Types[] = { &OnlineMapsControlBase3D_t2801313279_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsControlBase3D_t2801313279_0_0_0 = { 1, GenInst_OnlineMapsControlBase3D_t2801313279_0_0_0_Types };
extern const Il2CppType MineMiniGameController_t2791970086_0_0_0;
static const Il2CppType* GenInst_MineMiniGameController_t2791970086_0_0_0_Types[] = { &MineMiniGameController_t2791970086_0_0_0 };
extern const Il2CppGenericInst GenInst_MineMiniGameController_t2791970086_0_0_0 = { 1, GenInst_MineMiniGameController_t2791970086_0_0_0_Types };
extern const Il2CppType AttackInventoryNavScreen_t33214553_0_0_0;
static const Il2CppType* GenInst_AttackInventoryNavScreen_t33214553_0_0_0_Types[] = { &AttackInventoryNavScreen_t33214553_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackInventoryNavScreen_t33214553_0_0_0 = { 1, GenInst_AttackInventoryNavScreen_t33214553_0_0_0_Types };
extern const Il2CppType AttackReviewNavScreen_t173504869_0_0_0;
static const Il2CppType* GenInst_AttackReviewNavScreen_t173504869_0_0_0_Types[] = { &AttackReviewNavScreen_t173504869_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackReviewNavScreen_t173504869_0_0_0 = { 1, GenInst_AttackReviewNavScreen_t173504869_0_0_0_Types };
extern const Il2CppType ModalPanel_t3020707755_0_0_0;
static const Il2CppType* GenInst_ModalPanel_t3020707755_0_0_0_Types[] = { &ModalPanel_t3020707755_0_0_0 };
extern const Il2CppGenericInst GenInst_ModalPanel_t3020707755_0_0_0 = { 1, GenInst_ModalPanel_t3020707755_0_0_0_Types };
extern const Il2CppType DefendModal_t2645228609_0_0_0;
static const Il2CppType* GenInst_DefendModal_t2645228609_0_0_0_Types[] = { &DefendModal_t2645228609_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendModal_t2645228609_0_0_0 = { 1, GenInst_DefendModal_t2645228609_0_0_0_Types };
extern const Il2CppType AwardModal_t1856366994_0_0_0;
static const Il2CppType* GenInst_AwardModal_t1856366994_0_0_0_Types[] = { &AwardModal_t1856366994_0_0_0 };
extern const Il2CppGenericInst GenInst_AwardModal_t1856366994_0_0_0 = { 1, GenInst_AwardModal_t1856366994_0_0_0_Types };
extern const Il2CppType LongNotificationModal_t834782082_0_0_0;
static const Il2CppType* GenInst_LongNotificationModal_t834782082_0_0_0_Types[] = { &LongNotificationModal_t834782082_0_0_0 };
extern const Il2CppGenericInst GenInst_LongNotificationModal_t834782082_0_0_0 = { 1, GenInst_LongNotificationModal_t834782082_0_0_0_Types };
extern const Il2CppType NGUIDebug_t1735550160_0_0_0;
static const Il2CppType* GenInst_NGUIDebug_t1735550160_0_0_0_Types[] = { &NGUIDebug_t1735550160_0_0_0 };
extern const Il2CppGenericInst GenInst_NGUIDebug_t1735550160_0_0_0 = { 1, GenInst_NGUIDebug_t1735550160_0_0_0_Types };
extern const Il2CppType BoxCollider2D_t948534547_0_0_0;
static const Il2CppType* GenInst_BoxCollider2D_t948534547_0_0_0_Types[] = { &BoxCollider2D_t948534547_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider2D_t948534547_0_0_0 = { 1, GenInst_BoxCollider2D_t948534547_0_0_0_Types };
extern const Il2CppType UIAnchor_t624210015_0_0_0;
static const Il2CppType* GenInst_UIAnchor_t624210015_0_0_0_Types[] = { &UIAnchor_t624210015_0_0_0 };
extern const Il2CppGenericInst GenInst_UIAnchor_t624210015_0_0_0 = { 1, GenInst_UIAnchor_t624210015_0_0_0_Types };
extern const Il2CppType OnlineMapsControlBase_t473237564_0_0_0;
static const Il2CppType* GenInst_OnlineMapsControlBase_t473237564_0_0_0_Types[] = { &OnlineMapsControlBase_t473237564_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsControlBase_t473237564_0_0_0 = { 1, GenInst_OnlineMapsControlBase_t473237564_0_0_0_Types };
extern const Il2CppType OnlineMapsBuildingBuiltIn_t544190665_0_0_0;
static const Il2CppType* GenInst_OnlineMapsBuildingBuiltIn_t544190665_0_0_0_Types[] = { &OnlineMapsBuildingBuiltIn_t544190665_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsBuildingBuiltIn_t544190665_0_0_0 = { 1, GenInst_OnlineMapsBuildingBuiltIn_t544190665_0_0_0_Types };
extern const Il2CppType MeshCollider_t2718867283_0_0_0;
static const Il2CppType* GenInst_MeshCollider_t2718867283_0_0_0_Types[] = { &MeshCollider_t2718867283_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshCollider_t2718867283_0_0_0 = { 1, GenInst_MeshCollider_t2718867283_0_0_0_Types };
extern const Il2CppType OnlineMapsMarkerInstanceBase_t538187336_0_0_0;
static const Il2CppType* GenInst_OnlineMapsMarkerInstanceBase_t538187336_0_0_0_Types[] = { &OnlineMapsMarkerInstanceBase_t538187336_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarkerInstanceBase_t538187336_0_0_0 = { 1, GenInst_OnlineMapsMarkerInstanceBase_t538187336_0_0_0_Types };
extern const Il2CppType OnlineMapsMarker3DInstance_t3567168358_0_0_0;
static const Il2CppType* GenInst_OnlineMapsMarker3DInstance_t3567168358_0_0_0_Types[] = { &OnlineMapsMarker3DInstance_t3567168358_0_0_0 };
extern const Il2CppGenericInst GenInst_OnlineMapsMarker3DInstance_t3567168358_0_0_0 = { 1, GenInst_OnlineMapsMarker3DInstance_t3567168358_0_0_0_Types };
extern const Il2CppType RawImage_t2749640213_0_0_0;
static const Il2CppType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { &RawImage_t2749640213_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
extern const Il2CppType AddressBook_t411411037_0_0_0;
static const Il2CppType* GenInst_AddressBook_t411411037_0_0_0_Types[] = { &AddressBook_t411411037_0_0_0 };
extern const Il2CppGenericInst GenInst_AddressBook_t411411037_0_0_0 = { 1, GenInst_AddressBook_t411411037_0_0_0_Types };
extern const Il2CppType PPStatsNavScreen_t3294811412_0_0_0;
static const Il2CppType* GenInst_PPStatsNavScreen_t3294811412_0_0_0_Types[] = { &PPStatsNavScreen_t3294811412_0_0_0 };
extern const Il2CppGenericInst GenInst_PPStatsNavScreen_t3294811412_0_0_0 = { 1, GenInst_PPStatsNavScreen_t3294811412_0_0_0_Types };
extern const Il2CppType LoginCenterUI_t890505802_0_0_0;
static const Il2CppType* GenInst_LoginCenterUI_t890505802_0_0_0_Types[] = { &LoginCenterUI_t890505802_0_0_0 };
extern const Il2CppGenericInst GenInst_LoginCenterUI_t890505802_0_0_0 = { 1, GenInst_LoginCenterUI_t890505802_0_0_0_Types };
extern const Il2CppType MissileResultsModal_t1615487503_0_0_0;
static const Il2CppType* GenInst_MissileResultsModal_t1615487503_0_0_0_Types[] = { &MissileResultsModal_t1615487503_0_0_0 };
extern const Il2CppGenericInst GenInst_MissileResultsModal_t1615487503_0_0_0 = { 1, GenInst_MissileResultsModal_t1615487503_0_0_0_Types };
extern const Il2CppType MineResultsModal_t4210040424_0_0_0;
static const Il2CppType* GenInst_MineResultsModal_t4210040424_0_0_0_Types[] = { &MineResultsModal_t4210040424_0_0_0 };
extern const Il2CppGenericInst GenInst_MineResultsModal_t4210040424_0_0_0 = { 1, GenInst_MineResultsModal_t4210040424_0_0_0_Types };
extern const Il2CppType SceneLoaderUI_t1806459353_0_0_0;
static const Il2CppType* GenInst_SceneLoaderUI_t1806459353_0_0_0_Types[] = { &SceneLoaderUI_t1806459353_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneLoaderUI_t1806459353_0_0_0 = { 1, GenInst_SceneLoaderUI_t1806459353_0_0_0_Types };
extern const Il2CppType SettingsHomeContent_t249573869_0_0_0;
static const Il2CppType* GenInst_SettingsHomeContent_t249573869_0_0_0_Types[] = { &SettingsHomeContent_t249573869_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsHomeContent_t249573869_0_0_0 = { 1, GenInst_SettingsHomeContent_t249573869_0_0_0_Types };
extern const Il2CppType SettingsProfileNavScreen_t3587426313_0_0_0;
static const Il2CppType* GenInst_SettingsProfileNavScreen_t3587426313_0_0_0_Types[] = { &SettingsProfileNavScreen_t3587426313_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsProfileNavScreen_t3587426313_0_0_0 = { 1, GenInst_SettingsProfileNavScreen_t3587426313_0_0_0_Types };
extern const Il2CppType BlockListNavScreen_t522977310_0_0_0;
static const Il2CppType* GenInst_BlockListNavScreen_t522977310_0_0_0_Types[] = { &BlockListNavScreen_t522977310_0_0_0 };
extern const Il2CppGenericInst GenInst_BlockListNavScreen_t522977310_0_0_0 = { 1, GenInst_BlockListNavScreen_t522977310_0_0_0_Types };
extern const Il2CppType SettingsProfileContent_t3280006997_0_0_0;
static const Il2CppType* GenInst_SettingsProfileContent_t3280006997_0_0_0_Types[] = { &SettingsProfileContent_t3280006997_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsProfileContent_t3280006997_0_0_0 = { 1, GenInst_SettingsProfileContent_t3280006997_0_0_0_Types };
extern const Il2CppType SpringPanel_t2962744957_0_0_0;
static const Il2CppType* GenInst_SpringPanel_t2962744957_0_0_0_Types[] = { &SpringPanel_t2962744957_0_0_0 };
extern const Il2CppGenericInst GenInst_SpringPanel_t2962744957_0_0_0 = { 1, GenInst_SpringPanel_t2962744957_0_0_0_Types };
extern const Il2CppType SpringPosition_t434217086_0_0_0;
static const Il2CppType* GenInst_SpringPosition_t434217086_0_0_0_Types[] = { &SpringPosition_t434217086_0_0_0 };
extern const Il2CppGenericInst GenInst_SpringPosition_t434217086_0_0_0 = { 1, GenInst_SpringPosition_t434217086_0_0_0_Types };
extern const Il2CppType StealthModeNavScreen_t2705902227_0_0_0;
static const Il2CppType* GenInst_StealthModeNavScreen_t2705902227_0_0_0_Types[] = { &StealthModeNavScreen_t2705902227_0_0_0 };
extern const Il2CppGenericInst GenInst_StealthModeNavScreen_t2705902227_0_0_0 = { 1, GenInst_StealthModeNavScreen_t2705902227_0_0_0_Types };
extern const Il2CppType ShieldSlider_t1483508804_0_0_0;
static const Il2CppType* GenInst_ShieldSlider_t1483508804_0_0_0_Types[] = { &ShieldSlider_t1483508804_0_0_0 };
extern const Il2CppGenericInst GenInst_ShieldSlider_t1483508804_0_0_0 = { 1, GenInst_ShieldSlider_t1483508804_0_0_0_Types };
extern const Il2CppType ProductCatergoryNavItem_t1498614491_0_0_0;
static const Il2CppType* GenInst_ProductCatergoryNavItem_t1498614491_0_0_0_Types[] = { &ProductCatergoryNavItem_t1498614491_0_0_0 };
extern const Il2CppGenericInst GenInst_ProductCatergoryNavItem_t1498614491_0_0_0 = { 1, GenInst_ProductCatergoryNavItem_t1498614491_0_0_0_Types };
extern const Il2CppType NumberPickerTransform_t3121159539_0_0_0;
static const Il2CppType* GenInst_NumberPickerTransform_t3121159539_0_0_0_Types[] = { &NumberPickerTransform_t3121159539_0_0_0 };
extern const Il2CppGenericInst GenInst_NumberPickerTransform_t3121159539_0_0_0 = { 1, GenInst_NumberPickerTransform_t3121159539_0_0_0_Types };
extern const Il2CppType ToastUI_t270783635_0_0_0;
static const Il2CppType* GenInst_ToastUI_t270783635_0_0_0_Types[] = { &ToastUI_t270783635_0_0_0 };
extern const Il2CppGenericInst GenInst_ToastUI_t270783635_0_0_0 = { 1, GenInst_ToastUI_t270783635_0_0_0_Types };
extern const Il2CppType Toaster_t2428368812_0_0_0;
static const Il2CppType* GenInst_Toaster_t2428368812_0_0_0_Types[] = { &Toaster_t2428368812_0_0_0 };
extern const Il2CppGenericInst GenInst_Toaster_t2428368812_0_0_0 = { 1, GenInst_Toaster_t2428368812_0_0_0_Types };
extern const Il2CppType Light_t494725636_0_0_0;
static const Il2CppType* GenInst_Light_t494725636_0_0_0_Types[] = { &Light_t494725636_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t494725636_0_0_0 = { 1, GenInst_Light_t494725636_0_0_0_Types };
extern const Il2CppType TweenFOV_t612833154_0_0_0;
static const Il2CppType* GenInst_TweenFOV_t612833154_0_0_0_Types[] = { &TweenFOV_t612833154_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenFOV_t612833154_0_0_0 = { 1, GenInst_TweenFOV_t612833154_0_0_0_Types };
extern const Il2CppType UITable_t3717403602_0_0_0;
static const Il2CppType* GenInst_UITable_t3717403602_0_0_0_Types[] = { &UITable_t3717403602_0_0_0 };
extern const Il2CppGenericInst GenInst_UITable_t3717403602_0_0_0 = { 1, GenInst_UITable_t3717403602_0_0_0_Types };
extern const Il2CppType TweenHeight_t161374028_0_0_0;
static const Il2CppType* GenInst_TweenHeight_t161374028_0_0_0_Types[] = { &TweenHeight_t161374028_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenHeight_t161374028_0_0_0 = { 1, GenInst_TweenHeight_t161374028_0_0_0_Types };
extern const Il2CppType TweenOrthoSize_t2754360798_0_0_0;
static const Il2CppType* GenInst_TweenOrthoSize_t2754360798_0_0_0_Types[] = { &TweenOrthoSize_t2754360798_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenOrthoSize_t2754360798_0_0_0 = { 1, GenInst_TweenOrthoSize_t2754360798_0_0_0_Types };
extern const Il2CppType TweenRotation_t1747194511_0_0_0;
static const Il2CppType* GenInst_TweenRotation_t1747194511_0_0_0_Types[] = { &TweenRotation_t1747194511_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRotation_t1747194511_0_0_0 = { 1, GenInst_TweenRotation_t1747194511_0_0_0_Types };
extern const Il2CppType TweenTransform_t3901935067_0_0_0;
static const Il2CppType* GenInst_TweenTransform_t3901935067_0_0_0_Types[] = { &TweenTransform_t3901935067_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenTransform_t3901935067_0_0_0 = { 1, GenInst_TweenTransform_t3901935067_0_0_0_Types };
extern const Il2CppType TweenVolume_t1729630735_0_0_0;
static const Il2CppType* GenInst_TweenVolume_t1729630735_0_0_0_Types[] = { &TweenVolume_t1729630735_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenVolume_t1729630735_0_0_0 = { 1, GenInst_TweenVolume_t1729630735_0_0_0_Types };
extern const Il2CppType TweenWidth_t511657453_0_0_0;
static const Il2CppType* GenInst_TweenWidth_t511657453_0_0_0_Types[] = { &TweenWidth_t511657453_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenWidth_t511657453_0_0_0 = { 1, GenInst_TweenWidth_t511657453_0_0_0_Types };
extern const Il2CppType UI2DSprite_t1082505957_0_0_0;
static const Il2CppType* GenInst_UI2DSprite_t1082505957_0_0_0_Types[] = { &UI2DSprite_t1082505957_0_0_0 };
extern const Il2CppGenericInst GenInst_UI2DSprite_t1082505957_0_0_0 = { 1, GenInst_UI2DSprite_t1082505957_0_0_0_Types };
extern const Il2CppType Animation_t2068071072_0_0_0;
static const Il2CppType* GenInst_Animation_t2068071072_0_0_0_Types[] = { &Animation_t2068071072_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t2068071072_0_0_0 = { 1, GenInst_Animation_t2068071072_0_0_0_Types };
extern const Il2CppType UIInput_t860674234_0_0_0;
static const Il2CppType* GenInst_UIInput_t860674234_0_0_0_Types[] = { &UIInput_t860674234_0_0_0 };
extern const Il2CppGenericInst GenInst_UIInput_t860674234_0_0_0 = { 1, GenInst_UIInput_t860674234_0_0_0_Types };
extern const Il2CppType Rigidbody_t4233889191_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { &Rigidbody_t4233889191_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
extern const Il2CppType UIGrid_t2420180906_0_0_0;
static const Il2CppType* GenInst_UIGrid_t2420180906_0_0_0_Types[] = { &UIGrid_t2420180906_0_0_0 };
extern const Il2CppGenericInst GenInst_UIGrid_t2420180906_0_0_0 = { 1, GenInst_UIGrid_t2420180906_0_0_0_Types };
extern const Il2CppType UIWrapContent_t1931723019_0_0_0;
static const Il2CppType* GenInst_UIWrapContent_t1931723019_0_0_0_Types[] = { &UIWrapContent_t1931723019_0_0_0 };
extern const Il2CppGenericInst GenInst_UIWrapContent_t1931723019_0_0_0 = { 1, GenInst_UIWrapContent_t1931723019_0_0_0_Types };
extern const Il2CppType UICenterOnChild_t1687745660_0_0_0;
static const Il2CppType* GenInst_UICenterOnChild_t1687745660_0_0_0_Types[] = { &UICenterOnChild_t1687745660_0_0_0 };
extern const Il2CppGenericInst GenInst_UICenterOnChild_t1687745660_0_0_0 = { 1, GenInst_UICenterOnChild_t1687745660_0_0_0_Types };
extern const Il2CppType UIDraggableCamera_t2562792962_0_0_0;
static const Il2CppType* GenInst_UIDraggableCamera_t2562792962_0_0_0_Types[] = { &UIDraggableCamera_t2562792962_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDraggableCamera_t2562792962_0_0_0 = { 1, GenInst_UIDraggableCamera_t2562792962_0_0_0_Types };
extern const Il2CppType UIDragScrollView_t2942595320_0_0_0;
static const Il2CppType* GenInst_UIDragScrollView_t2942595320_0_0_0_Types[] = { &UIDragScrollView_t2942595320_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDragScrollView_t2942595320_0_0_0 = { 1, GenInst_UIDragScrollView_t2942595320_0_0_0_Types };
extern const Il2CppType UIDragDropContainer_t4036588756_0_0_0;
static const Il2CppType* GenInst_UIDragDropContainer_t4036588756_0_0_0_Types[] = { &UIDragDropContainer_t4036588756_0_0_0 };
extern const Il2CppGenericInst GenInst_UIDragDropContainer_t4036588756_0_0_0 = { 1, GenInst_UIDragDropContainer_t4036588756_0_0_0_Types };
extern const Il2CppType UIEventListener_t3982099366_0_0_0;
static const Il2CppType* GenInst_UIEventListener_t3982099366_0_0_0_Types[] = { &UIEventListener_t3982099366_0_0_0 };
extern const Il2CppGenericInst GenInst_UIEventListener_t3982099366_0_0_0 = { 1, GenInst_UIEventListener_t3982099366_0_0_0_Types };
extern const Il2CppType UIOrthoCamera_t1890393437_0_0_0;
static const Il2CppType* GenInst_UIOrthoCamera_t1890393437_0_0_0_Types[] = { &UIOrthoCamera_t1890393437_0_0_0 };
extern const Il2CppGenericInst GenInst_UIOrthoCamera_t1890393437_0_0_0 = { 1, GenInst_UIOrthoCamera_t1890393437_0_0_0_Types };
extern const Il2CppType UIProgressBar_t3824507790_0_0_0;
static const Il2CppType* GenInst_UIProgressBar_t3824507790_0_0_0_Types[] = { &UIProgressBar_t3824507790_0_0_0 };
extern const Il2CppGenericInst GenInst_UIProgressBar_t3824507790_0_0_0 = { 1, GenInst_UIProgressBar_t3824507790_0_0_0_Types };
extern const Il2CppType UISlider_t2191058247_0_0_0;
static const Il2CppType* GenInst_UISlider_t2191058247_0_0_0_Types[] = { &UISlider_t2191058247_0_0_0 };
extern const Il2CppGenericInst GenInst_UISlider_t2191058247_0_0_0 = { 1, GenInst_UISlider_t2191058247_0_0_0_Types };
static const Il2CppType* GenInst_AttackHomeNav_t3944890504_0_0_0_AttackHomeNav_t3944890504_0_0_0_Types[] = { &AttackHomeNav_t3944890504_0_0_0, &AttackHomeNav_t3944890504_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackHomeNav_t3944890504_0_0_0_AttackHomeNav_t3944890504_0_0_0 = { 2, GenInst_AttackHomeNav_t3944890504_0_0_0_AttackHomeNav_t3944890504_0_0_0_Types };
static const Il2CppType* GenInst_AttackNavScreen_t36759459_0_0_0_AttackNavScreen_t36759459_0_0_0_Types[] = { &AttackNavScreen_t36759459_0_0_0, &AttackNavScreen_t36759459_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackNavScreen_t36759459_0_0_0_AttackNavScreen_t36759459_0_0_0 = { 2, GenInst_AttackNavScreen_t36759459_0_0_0_AttackNavScreen_t36759459_0_0_0_Types };
static const Il2CppType* GenInst_AttackSearchNav_t4257884637_0_0_0_AttackSearchNav_t4257884637_0_0_0_Types[] = { &AttackSearchNav_t4257884637_0_0_0, &AttackSearchNav_t4257884637_0_0_0 };
extern const Il2CppGenericInst GenInst_AttackSearchNav_t4257884637_0_0_0_AttackSearchNav_t4257884637_0_0_0 = { 2, GenInst_AttackSearchNav_t4257884637_0_0_0_AttackSearchNav_t4257884637_0_0_0_Types };
static const Il2CppType* GenInst_ChatNavScreen_t42377261_0_0_0_ChatNavScreen_t42377261_0_0_0_Types[] = { &ChatNavScreen_t42377261_0_0_0, &ChatNavScreen_t42377261_0_0_0 };
extern const Il2CppGenericInst GenInst_ChatNavScreen_t42377261_0_0_0_ChatNavScreen_t42377261_0_0_0 = { 2, GenInst_ChatNavScreen_t42377261_0_0_0_ChatNavScreen_t42377261_0_0_0_Types };
static const Il2CppType* GenInst_DefendNavScreen_t3838619703_0_0_0_DefendNavScreen_t3838619703_0_0_0_Types[] = { &DefendNavScreen_t3838619703_0_0_0, &DefendNavScreen_t3838619703_0_0_0 };
extern const Il2CppGenericInst GenInst_DefendNavScreen_t3838619703_0_0_0_DefendNavScreen_t3838619703_0_0_0 = { 2, GenInst_DefendNavScreen_t3838619703_0_0_0_DefendNavScreen_t3838619703_0_0_0_Types };
static const Il2CppType* GenInst_FriendHomeNavScreen_t1599038296_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0_Types[] = { &FriendHomeNavScreen_t1599038296_0_0_0, &FriendHomeNavScreen_t1599038296_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendHomeNavScreen_t1599038296_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0 = { 2, GenInst_FriendHomeNavScreen_t1599038296_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0_Types };
static const Il2CppType* GenInst_FriendNavScreen_t2175383453_0_0_0_FriendNavScreen_t2175383453_0_0_0_Types[] = { &FriendNavScreen_t2175383453_0_0_0, &FriendNavScreen_t2175383453_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendNavScreen_t2175383453_0_0_0_FriendNavScreen_t2175383453_0_0_0 = { 2, GenInst_FriendNavScreen_t2175383453_0_0_0_FriendNavScreen_t2175383453_0_0_0_Types };
static const Il2CppType* GenInst_FriendSearchNavScreen_t3018389163_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0_Types[] = { &FriendSearchNavScreen_t3018389163_0_0_0, &FriendSearchNavScreen_t3018389163_0_0_0 };
extern const Il2CppGenericInst GenInst_FriendSearchNavScreen_t3018389163_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0 = { 2, GenInst_FriendSearchNavScreen_t3018389163_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0_Types };
static const Il2CppType* GenInst_LeaderboardNavScreen_t2356043712_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0_Types[] = { &LeaderboardNavScreen_t2356043712_0_0_0, &LeaderboardNavScreen_t2356043712_0_0_0 };
extern const Il2CppGenericInst GenInst_LeaderboardNavScreen_t2356043712_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0 = { 2, GenInst_LeaderboardNavScreen_t2356043712_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types[] = { &PropertyMetadata_t3693826136_0_0_0, &PropertyMetadata_t3693826136_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0 = { 2, GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0_Types };
static const Il2CppType* GenInst_PhoneNumberNavScreen_t1263467250_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0_Types[] = { &PhoneNumberNavScreen_t1263467250_0_0_0, &PhoneNumberNavScreen_t1263467250_0_0_0 };
extern const Il2CppGenericInst GenInst_PhoneNumberNavScreen_t1263467250_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0 = { 2, GenInst_PhoneNumberNavScreen_t1263467250_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0_Types };
static const Il2CppType* GenInst_PlayerProfileNavScreen_t2660668259_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0_Types[] = { &PlayerProfileNavScreen_t2660668259_0_0_0, &PlayerProfileNavScreen_t2660668259_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerProfileNavScreen_t2660668259_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0 = { 2, GenInst_PlayerProfileNavScreen_t2660668259_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0_Types };
static const Il2CppType* GenInst_ProfileNavScreen_t1126657854_0_0_0_ProfileNavScreen_t1126657854_0_0_0_Types[] = { &ProfileNavScreen_t1126657854_0_0_0, &ProfileNavScreen_t1126657854_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileNavScreen_t1126657854_0_0_0_ProfileNavScreen_t1126657854_0_0_0 = { 2, GenInst_ProfileNavScreen_t1126657854_0_0_0_ProfileNavScreen_t1126657854_0_0_0_Types };
static const Il2CppType* GenInst_RegNavScreen_t667173359_0_0_0_RegNavScreen_t667173359_0_0_0_Types[] = { &RegNavScreen_t667173359_0_0_0, &RegNavScreen_t667173359_0_0_0 };
extern const Il2CppGenericInst GenInst_RegNavScreen_t667173359_0_0_0_RegNavScreen_t667173359_0_0_0 = { 2, GenInst_RegNavScreen_t667173359_0_0_0_RegNavScreen_t667173359_0_0_0_Types };
static const Il2CppType* GenInst_SettingsNavScreen_t2267127234_0_0_0_SettingsNavScreen_t2267127234_0_0_0_Types[] = { &SettingsNavScreen_t2267127234_0_0_0, &SettingsNavScreen_t2267127234_0_0_0 };
extern const Il2CppGenericInst GenInst_SettingsNavScreen_t2267127234_0_0_0_SettingsNavScreen_t2267127234_0_0_0 = { 2, GenInst_SettingsNavScreen_t2267127234_0_0_0_SettingsNavScreen_t2267127234_0_0_0_Types };
static const Il2CppType* GenInst_StatusNavScreen_t2872369083_0_0_0_StatusNavScreen_t2872369083_0_0_0_Types[] = { &StatusNavScreen_t2872369083_0_0_0, &StatusNavScreen_t2872369083_0_0_0 };
extern const Il2CppGenericInst GenInst_StatusNavScreen_t2872369083_0_0_0_StatusNavScreen_t2872369083_0_0_0 = { 2, GenInst_StatusNavScreen_t2872369083_0_0_0_StatusNavScreen_t2872369083_0_0_0_Types };
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0, &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0, &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0 = { 2, GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0, &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0, &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_UnibillError_t1753859787_0_0_0_UnibillError_t1753859787_0_0_0_Types[] = { &UnibillError_t1753859787_0_0_0, &UnibillError_t1753859787_0_0_0 };
extern const Il2CppGenericInst GenInst_UnibillError_t1753859787_0_0_0_UnibillError_t1753859787_0_0_0 = { 2, GenInst_UnibillError_t1753859787_0_0_0_UnibillError_t1753859787_0_0_0_Types };
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0, &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0 = { 2, GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0, &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0, &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0, &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0 = { 2, GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types };
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0, &RuntimePlatform_t1869584967_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0 = { 2, GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0, &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0, &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0, &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const Il2CppType* GenInst_PanelType_t482769230_0_0_0_PanelType_t482769230_0_0_0_Types[] = { &PanelType_t482769230_0_0_0, &PanelType_t482769230_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelType_t482769230_0_0_0_PanelType_t482769230_0_0_0 = { 2, GenInst_PanelType_t482769230_0_0_0_PanelType_t482769230_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1603735834_0_0_0_KeyValuePair_2_t1603735834_0_0_0_Types[] = { &KeyValuePair_2_t1603735834_0_0_0, &KeyValuePair_2_t1603735834_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1603735834_0_0_0_KeyValuePair_2_t1603735834_0_0_0 = { 2, GenInst_KeyValuePair_2_t1603735834_0_0_0_KeyValuePair_2_t1603735834_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1603735834_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1603735834_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1603735834_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1603735834_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2812303849_0_0_0_KeyValuePair_2_t2812303849_0_0_0_Types[] = { &KeyValuePair_2_t2812303849_0_0_0, &KeyValuePair_2_t2812303849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2812303849_0_0_0_KeyValuePair_2_t2812303849_0_0_0 = { 2, GenInst_KeyValuePair_2_t2812303849_0_0_0_KeyValuePair_2_t2812303849_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2812303849_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2812303849_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2812303849_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2812303849_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types[] = { &KeyValuePair_2_t1008373517_0_0_0, &KeyValuePair_2_t1008373517_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0 = { 2, GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1008373517_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0_Types[] = { &Label_t4243202660_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types[] = { &Label_t4243202660_0_0_0, &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0 = { 2, GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types[] = { &ArrayMetadata_t2008834462_0_0_0, &ArrayMetadata_t2008834462_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0 = { 2, GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0_Types };
static const Il2CppType* GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types[] = { &ArrayMetadata_t2008834462_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types[] = { &KeyValuePair_2_t3653207108_0_0_0, &KeyValuePair_2_t3653207108_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3653207108_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types[] = { &ObjectMetadata_t3995922398_0_0_0, &ObjectMetadata_t3995922398_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0 = { 2, GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0_Types };
static const Il2CppType* GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types[] = { &ObjectMetadata_t3995922398_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types[] = { &KeyValuePair_2_t1345327748_0_0_0, &KeyValuePair_2_t1345327748_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0 = { 2, GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1345327748_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types[] = { &PropertyMetadata_t3693826136_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types[] = { &KeyValuePair_2_t1043231486_0_0_0, &KeyValuePair_2_t1043231486_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0 = { 2, GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1043231486_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2369073723_0_0_0_KeyValuePair_2_t2369073723_0_0_0_Types[] = { &KeyValuePair_2_t2369073723_0_0_0, &KeyValuePair_2_t2369073723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2369073723_0_0_0_KeyValuePair_2_t2369073723_0_0_0 = { 2, GenInst_KeyValuePair_2_t2369073723_0_0_0_KeyValuePair_2_t2369073723_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2369073723_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2369073723_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2369073723_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2369073723_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0, &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0 = { 2, GenInst_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0_Types };
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0_Types[] = { &Decimal_t724701077_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3888080226_0_0_0_KeyValuePair_2_t3888080226_0_0_0_Types[] = { &KeyValuePair_2_t3888080226_0_0_0, &KeyValuePair_2_t3888080226_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3888080226_0_0_0_KeyValuePair_2_t3888080226_0_0_0 = { 2, GenInst_KeyValuePair_2_t3888080226_0_0_0_KeyValuePair_2_t3888080226_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3888080226_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3888080226_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3888080226_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3888080226_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types[] = { &ClientMessage_t624279968_0_0_0, &ClientMessage_t624279968_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0 = { 2, GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0_Types };
static const Il2CppType* GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0_Types[] = { &ClientMessage_t624279968_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types[] = { &KeyValuePair_2_t322707255_0_0_0, &KeyValuePair_2_t322707255_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0 = { 2, GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t322707255_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0, &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0 = { 2, GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types[] = { &KeyValuePair_2_t2387876582_0_0_0, &KeyValuePair_2_t2387876582_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0 = { 2, GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2387876582_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2335466038_0_0_0_KeyValuePair_2_t2335466038_0_0_0_Types[] = { &KeyValuePair_2_t2335466038_0_0_0, &KeyValuePair_2_t2335466038_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2335466038_0_0_0_KeyValuePair_2_t2335466038_0_0_0 = { 2, GenInst_KeyValuePair_2_t2335466038_0_0_0_KeyValuePair_2_t2335466038_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2335466038_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2335466038_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2335466038_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2335466038_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_BillingPlatform_t552059234_0_0_0_BillingPlatform_t552059234_0_0_0_Types[] = { &BillingPlatform_t552059234_0_0_0, &BillingPlatform_t552059234_0_0_0 };
extern const Il2CppGenericInst GenInst_BillingPlatform_t552059234_0_0_0_BillingPlatform_t552059234_0_0_0 = { 2, GenInst_BillingPlatform_t552059234_0_0_0_BillingPlatform_t552059234_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1020750381_0_0_0_KeyValuePair_2_t1020750381_0_0_0_Types[] = { &KeyValuePair_2_t1020750381_0_0_0, &KeyValuePair_2_t1020750381_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1020750381_0_0_0_KeyValuePair_2_t1020750381_0_0_0 = { 2, GenInst_KeyValuePair_2_t1020750381_0_0_0_KeyValuePair_2_t1020750381_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1020750381_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1020750381_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1020750381_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1020750381_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Il2CppObject_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Vector3Pair_t2859078138_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Vector3Pair_t2859078138_0_0_0_Vector3Pair_t2859078138_0_0_0_Types[] = { &Vector3Pair_t2859078138_0_0_0, &Vector3Pair_t2859078138_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3Pair_t2859078138_0_0_0_Vector3Pair_t2859078138_0_0_0 = { 2, GenInst_Vector3Pair_t2859078138_0_0_0_Vector3Pair_t2859078138_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType PanelContainer_t1029301983_0_0_0;
static const Il2CppType* GenInst_PanelContainer_t1029301983_0_0_0_Types[] = { &PanelContainer_t1029301983_0_0_0 };
extern const Il2CppGenericInst GenInst_PanelContainer_t1029301983_0_0_0 = { 1, GenInst_PanelContainer_t1029301983_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1805] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IComparable_1_t991353265_0_0_0,
	&GenInst_IEquatable_1_t1363496211_0_0_0,
	&GenInst_ValueType_t3507792607_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_IFormattable_t1523031934_0_0_0,
	&GenInst_IComparable_1_t3903716671_0_0_0,
	&GenInst_IEquatable_1_t4275859617_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_IComparable_1_t1614887608_0_0_0,
	&GenInst_IEquatable_1_t1987030554_0_0_0,
	&GenInst_IComparable_1_t3981521244_0_0_0,
	&GenInst_IEquatable_1_t58696894_0_0_0,
	&GenInst_IComparable_1_t1219976363_0_0_0,
	&GenInst_IEquatable_1_t1592119309_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_IComparable_1_t3908349155_0_0_0,
	&GenInst_IEquatable_1_t4280492101_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_IComparable_1_t2818721834_0_0_0,
	&GenInst_IEquatable_1_t3190864780_0_0_0,
	&GenInst_IComparable_1_t446068841_0_0_0,
	&GenInst_IEquatable_1_t818211787_0_0_0,
	&GenInst_IComparable_1_t1578117841_0_0_0,
	&GenInst_IEquatable_1_t1950260787_0_0_0,
	&GenInst_IComparable_1_t2286256772_0_0_0,
	&GenInst_IEquatable_1_t2658399718_0_0_0,
	&GenInst_IComparable_1_t2740917260_0_0_0,
	&GenInst_IEquatable_1_t3113060206_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t2430923913_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_UriScheme_t683497865_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0,
	&GenInst__Assembly_t2937922309_0_0_0,
	&GenInst_IEvidenceFactory_t1747265420_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_Guid_t2533601593_0_0_0,
	&GenInst_IComparable_1_t1362446645_0_0_0,
	&GenInst_IEquatable_1_t1734589591_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_TermInfoStrings_t1425267120_0_0_0,
	&GenInst_Enum_t2459695545_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_CultureInfo_t3500843524_0_0_0,
	&GenInst_IFormatProvider_t2849799027_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_MonoResource_t3127387157_0_0_0,
	&GenInst_RefEmitPermissionSet_t2708608433_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_LocalBuilder_t2116499186_0_0_0,
	&GenInst__LocalBuilder_t61912499_0_0_0,
	&GenInst_LocalVariableInfo_t1749284021_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IComparable_1_t2525044892_0_0_0,
	&GenInst_IEquatable_1_t2897187838_0_0_0,
	&GenInst_IComparable_1_t2556540300_0_0_0,
	&GenInst_IEquatable_1_t2928683246_0_0_0,
	&GenInst_IComparable_1_t967130876_0_0_0,
	&GenInst_IEquatable_1_t1339273822_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_CodeConnectAccess_t3638993531_0_0_0,
	&GenInst_EncodingInfo_t2546178797_0_0_0,
	&GenInst_WaitHandle_t677569169_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_MarshalByRefObject_t1285298191_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_XmlSchemaAttribute_t4015859774_0_0_0,
	&GenInst_XmlSchemaAnnotated_t2082486936_0_0_0,
	&GenInst_XmlSchemaObject_t2050913741_0_0_0,
	&GenInst_XsdIdentityPath_t2037874_0_0_0,
	&GenInst_XsdIdentityField_t2563516441_0_0_0,
	&GenInst_XsdIdentityStep_t452377251_0_0_0,
	&GenInst_XmlSchemaException_t4082200141_0_0_0,
	&GenInst_SystemException_t3877406272_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0,
	&GenInst__Exception_t3026971024_0_0_0,
	&GenInst_KeyValuePair_2_t1430411454_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0,
	&GenInst_DTDNode_t1758286970_0_0_0,
	&GenInst_AttributeSlot_t1499247213_0_0_0,
	&GenInst_Entry_t2583369454_0_0_0,
	&GenInst_XmlNode_t616554813_0_0_0,
	&GenInst_IXPathNavigable_t845515791_0_0_0,
	&GenInst_NsDecl_t3210081295_0_0_0,
	&GenInst_NsScope_t2513625351_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0,
	&GenInst_XmlTokenInfo_t254587324_0_0_0,
	&GenInst_TagName_t2340974457_0_0_0,
	&GenInst_XmlNodeInfo_t3709371029_0_0_0,
	&GenInst_XPathNavigator_t3981235968_0_0_0,
	&GenInst_IXmlNamespaceResolver_t3928241465_0_0_0,
	&GenInst_XPathItem_t3130801258_0_0_0,
	&GenInst_XPathResultType_t1521569578_0_0_0,
	&GenInst_XmlAttribute_t175731005_0_0_0,
	&GenInst_IHasXmlChildNode_t2048545686_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0,
	&GenInst_XmlSchema_t880472818_0_0_0,
	&GenInst_Regex_t1803876613_0_0_0,
	&GenInst_XmlSchemaSimpleType_t248156492_0_0_0,
	&GenInst_XmlSchemaType_t1795078578_0_0_0,
	&GenInst_GenerationResult_t2691853749_0_0_0,
	&GenInst_XmlMapping_t1597064667_0_0_0,
	&GenInst_EnumMapMember_t3814867081_0_0_0,
	&GenInst_Hook_t3980190809_0_0_0,
	&GenInst_XmlMemberMapping_t2912176015_0_0_0,
	&GenInst_XmlIncludeAttribute_t3199808027_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_SerializerData_t3606741380_0_0_0,
	&GenInst_XmlTypeMapMemberAttribute_t3329971455_0_0_0,
	&GenInst_XmlTypeMapMember_t3057402259_0_0_0,
	&GenInst_XmlTypeMapElementInfo_t3381496463_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_XmlElement_t2877111883_0_0_0,
	&GenInst_XmlLinkedNode_t1287616130_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_Node_t2499136326_0_0_0,
	&GenInst_PropertyDescriptor_t4250402154_0_0_0,
	&GenInst_MemberDescriptor_t3749827553_0_0_0,
	&GenInst_IComponent_t1000253244_0_0_0,
	&GenInst_EventDescriptor_t962731901_0_0_0,
	&GenInst_ListSortDescription_t3194554012_0_0_0,
	&GenInst_EditPosition_t3912732320_0_0_0,
	&GenInst_PropertyTabScope_t2485003348_0_0_0,
	&GenInst_AttributeU5BU5D_t4255796347_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_TypeDescriptionProvider_t2438624375_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LinkedList_1_t2743332604_0_0_0,
	&GenInst_KeyValuePair_2_t2438035723_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3261256129_0_0_0,
	&GenInst_Cookie_t3154017544_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_DynamicMethod_t3307743052_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0,
	&GenInst_KeyValuePair_2_t1008373517_0_0_0,
	&GenInst_Label_t4243202660_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_ConfigurationElement_t1776195828_0_0_0,
	&GenInst_ConfigurationProperty_t2048066811_0_0_0,
	&GenInst_ConfigurationSection_t2600766927_0_0_0,
	&GenInst_ConfigurationSectionGroup_t2230982736_0_0_0,
	&GenInst_PropertyInformation_t2089433965_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_Material_t193706927_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Plane_t3727654732_0_0_0,
	&GenInst_Touch_t407273883_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_Collider2D_t646061738_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUIContent_t4210063000_0_0_0,
	&GenInst_Rect_t3681755626_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_Texture_t2243626319_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LayoutCache_t3120781045_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_Event_t3028476042_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_List_1_t61287617_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dispatcher_t2240407071_0_0_0,
	&GenInst_KeyValuePair_2_t3846770086_0_0_0,
	&GenInst_UrlSchemes_t2066630273_0_0_0,
	&GenInst_OnChangeCallback_t2639189684_0_0_0,
	&GenInst_ZipEntry_t1764014695_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0,
	&GenInst_String_t_0_0_0_Action_2_t2572051853_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Action_2_t2572051853_0_0_0,
	&GenInst_KeyValuePair_2_t2244176337_0_0_0,
	&GenInst_JsonContextType_t3787516849_0_0_0,
	&GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0,
	&GenInst_String_t_0_0_0_MemberMap_t574666045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t246790529_0_0_0,
	&GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0,
	&GenInst_Type_t_0_0_0_SafeDictionary_2_t1260879230_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t955582349_0_0_0,
	&GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0,
	&GenInst_Type_t_0_0_0_CtorDelegate_t3614558424_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3309261543_0_0_0,
	&GenInst_Product_t1158373411_0_0_0,
	&GenInst_FacebookUnityPlatform_t1867507902_0_0_0,
	&GenInst_ILoginResult_t403585443_0_0_0,
	&GenInst_IAppRequestResult_t1874118006_0_0_0,
	&GenInst_OGActionType_t1978093408_0_0_0,
	&GenInst_IShareResult_t830127229_0_0_0,
	&GenInst_IGraphResult_t3984946686_0_0_0,
	&GenInst_IAppLinkResult_t3542744145_0_0_0,
	&GenInst_IGroupCreateResult_t2512549813_0_0_0,
	&GenInst_IGroupJoinResult_t1571908141_0_0_0,
	&GenInst_IPayResult_t2860116854_0_0_0,
	&GenInst_IAppInviteResult_t3529555166_0_0_0,
	&GenInst_IAccessTokenRefreshResult_t2724304088_0_0_0,
	&GenInst_ResultContainer_t2148006712_0_0_0,
	&GenInst_IResult_t3447678270_0_0_0,
	&GenInst_Il2CppObject_0_0_0_String_t_0_0_0,
	&GenInst_XAttribute_t3858477518_0_0_0,
	&GenInst_XObject_t3550811009_0_0_0,
	&GenInst_IXmlLineInfo_t135184468_0_0_0,
	&GenInst_XNode_t2707504214_0_0_0,
	&GenInst_XElement_t553821050_0_0_0,
	&GenInst_XName_t785190363_0_0_0,
	&GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0,
	&GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_XNamespace_t1613015075_0_0_0,
	&GenInst_KeyValuePair_2_t1285139559_0_0_0,
	&GenInst_String_t_0_0_0_XName_t785190363_0_0_0,
	&GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t457314847_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HashSet_1_t2984649583_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IndexedSet_1_t286373651_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_RenderTexture_t2666733923_0_0_0,
	&GenInst_Texture2D_t3542995729_0_0_0,
	&GenInst_WWW_t2919945039_0_0_0,
	&GenInst_Contact_t4014196408_0_0_0,
	&GenInst_IDictionary_t596158605_0_0_0,
	&GenInst_String_t_0_0_0_FacebookFriendsResult_t459691178_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0,
	&GenInst_String_t_0_0_0_FacebookMeResult_t187696501_0_0_0,
	&GenInst_FacebookBatchRequest_t2176192103_0_0_0,
	&GenInst_Dictionary_2_t309261261_0_0_0,
	&GenInst_Nullable_1_t3251239280_0_0_0,
	&GenInst_FacebookFriend_t3865174844_0_0_0,
	&GenInst_P31Error_t2856600608_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0,
	&GenInst_VectorLine_t3390220087_0_0_0,
	&GenInst_Vector2U5BU5D_t686124026_0_0_0,
	&GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0,
	&GenInst_String_t_0_0_0_CapInfo_t3093272726_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_CapInfo_t3093272726_0_0_0,
	&GenInst_KeyValuePair_2_t2765397210_0_0_0,
	&GenInst_VectorPoints_t440342714_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1020750381_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Vector3Pair_t2859078138_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1020750381_0_0_0,
	&GenInst_RefInt_t2938871354_0_0_0,
	&GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0,
	&GenInst_String_t_0_0_0_Mesh_t1356156583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Mesh_t1356156583_0_0_0,
	&GenInst_KeyValuePair_2_t1028281067_0_0_0,
	&GenInst_PurchasableItem_t3963353899_0_0_0,
	&GenInst_PurchasableItem_t3963353899_0_0_0_String_t_0_0_0,
	&GenInst_PurchaseEvent_t743554429_0_0_0,
	&GenInst_PostParameter_t415229629_0_0_0,
	&GenInst_UnibillError_t1753859787_0_0_0,
	&GenInst_PurchasableItem_t3963353899_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_PurchasableItem_t3963353899_0_0_0_Product_t1158373411_0_0_0,
	&GenInst_VirtualCurrency_t1115497880_0_0_0_String_t_0_0_0,
	&GenInst_VirtualCurrency_t1115497880_0_0_0,
	&GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0,
	&GenInst_KeyValuePair_2_t2369073723_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Decimal_t724701077_0_0_0_KeyValuePair_2_t2369073723_0_0_0,
	&GenInst_String_t_0_0_0_Decimal_t724701077_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t396825561_0_0_0,
	&GenInst_PostRequest_t4132869030_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2335466038_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_BillingPlatform_t552059234_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2335466038_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1675236976_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4250245300_0_0_0,
	&GenInst_ProductDefinition_t1519653988_0_0_0,
	&GenInst_UnibillState_t4272135008_0_0_0,
	&GenInst_String_t_0_0_0_DirectoryInfo_t1934446453_0_0_0,
	&GenInst_HeaderValue_t822462144_0_0_0,
	&GenInst_String_t_0_0_0_Digest_t59399582_0_0_0,
	&GenInst_String_t_0_0_0_Digest_t59399582_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Digest_t59399582_0_0_0,
	&GenInst_KeyValuePair_2_t4026491362_0_0_0,
	&GenInst_HTTPCacheFileInfo_t2858191078_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t1398341365_0_0_0,
	&GenInst_KeyValuePair_2_t1070465849_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Uri_t19570940_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3930922740_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0,
	&GenInst_Uri_t19570940_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4099664523_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2387876582_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_UInt64_t2909196914_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2387876582_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_HTTPCacheFileInfo_t2858191078_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2556618365_0_0_0,
	&GenInst_Stream_t3255436806_0_0_0,
	&GenInst_Cookie_t4162804382_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Config_t3381668151_0_0_0,
	&GenInst_Int32U5BU5D_t3030399641_0_0_0,
	&GenInst_IHeartbeat_t3217346319_0_0_0,
	&GenInst_HTTPFieldData_t605100868_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0,
	&GenInst_ConnectionBase_t2782190729_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2151311861_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2151311861_0_0_0,
	&GenInst_KeyValuePair_2_t1823436345_0_0_0,
	&GenInst_HTTPRequest_t138485887_0_0_0,
	&GenInst_HTTPRequest_t138485887_0_0_0_X509Certificate_t283079845_0_0_0_X509Chain_t777637347_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Asn1Encodable_t3447851422_0_0_0,
	&GenInst_IAsn1Convertible_t983765413_0_0_0,
	&GenInst_DerEnumerated_t514019671_0_0_0,
	&GenInst_Asn1Object_t564283626_0_0_0,
	&GenInst_DerObjectIdentifier_t3495876513_0_0_0,
	&GenInst_BigInteger_t4268922522_0_0_0,
	&GenInst_DistributionPoint_t769724552_0_0_0,
	&GenInst_CrlEntry_t4200172927_0_0_0,
	&GenInst_GeneralName_t294965175_0_0_0,
	&GenInst_IAsn1Choice_t4205079803_0_0_0,
	&GenInst_UInt32U5BU5D_t59386216_0_0_0,
	&GenInst_Int64U5BU5D_t717125112_0_0_0,
	&GenInst_UInt32U5BU5DU5BU5D_t1156922361_0_0_0,
	&GenInst_ServerName_t2635557658_0_0_0,
	&GenInst_X509CertificateStructure_t3705285294_0_0_0,
	&GenInst_ECPoint_t626351532_0_0_0,
	&GenInst_ECFieldElement_t1092946118_0_0_0,
	&GenInst_WNafPreCompInfo_t485024160_0_0_0,
	&GenInst_PreCompInfo_t1123315090_0_0_0,
	&GenInst_LongArray_t194261203_0_0_0,
	&GenInst_ZTauElement_t2571810054_0_0_0,
	&GenInst_SByteU5BU5D_t3472287392_0_0_0,
	&GenInst_AbstractF2mPoint_t883694769_0_0_0,
	&GenInst_ECPointBase_t3119694375_0_0_0,
	&GenInst_Config_t1249383685_0_0_0,
	&GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0,
	&GenInst_String_t_0_0_0_OnEventDelegate_t790674770_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_OnEventDelegate_t790674770_0_0_0,
	&GenInst_KeyValuePair_2_t462799254_0_0_0,
	&GenInst_EventSourceResponse_t2287402344_0_0_0_Message_t1650395211_0_0_0,
	&GenInst_EventSourceResponse_t2287402344_0_0_0,
	&GenInst_Message_t1650395211_0_0_0,
	&GenInst_Hub_t272719679_0_0_0,
	&GenInst_IHub_t3409721544_0_0_0,
	&GenInst_IServerMessage_t2384143743_0_0_0,
	&GenInst_NegotiationData_t3059020807_0_0_0,
	&GenInst_NegotiationData_t3059020807_0_0_0_String_t_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0,
	&GenInst_KeyValuePair_2_t322707255_0_0_0,
	&GenInst_ClientMessage_t624279968_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_UInt64_t2909196914_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_ClientMessage_t624279968_0_0_0_KeyValuePair_2_t322707255_0_0_0,
	&GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0,
	&GenInst_String_t_0_0_0_OnMethodCallCallbackDelegate_t3483117754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_OnMethodCallCallbackDelegate_t3483117754_0_0_0,
	&GenInst_KeyValuePair_2_t3155242238_0_0_0,
	&GenInst_SocketIOCallback_t88619200_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0,
	&GenInst_EventDescriptor_t4057040835_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t3426161967_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t3426161967_0_0_0,
	&GenInst_KeyValuePair_2_t3098286451_0_0_0,
	&GenInst_HandshakeData_t1703965475_0_0_0,
	&GenInst_HandshakeData_t1703965475_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t309261261_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_SocketIOAckCallback_t53599143_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_SocketIOAckCallback_t53599143_0_0_0,
	&GenInst_KeyValuePair_2_t1113737296_0_0_0,
	&GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0,
	&GenInst_String_t_0_0_0_Socket_t2716624701_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Socket_t2716624701_0_0_0,
	&GenInst_KeyValuePair_2_t2388749185_0_0_0,
	&GenInst_Packet_t1309324146_0_0_0,
	&GenInst_IExtension_t2171905938_0_0_0,
	&GenInst_WebSocketFrame_t4163283394_0_0_0,
	&GenInst_WebSocketFrameReader_t549273869_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_String_t_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_UInt16_t986882611_0_0_0_String_t_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t986882611_0_0_0_Il2CppObject_0_0_0,
	&GenInst_WebSocketResponse_t3376763264_0_0_0_WebSocketFrameReader_t549273869_0_0_0,
	&GenInst_JsonData_t269267574_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0,
	&GenInst_KeyValuePair_2_t4236359354_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t269267574_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_KeyValuePair_2_t1043231486_0_0_0,
	&GenInst_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0,
	&GenInst_ExporterFunc_t408878057_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0,
	&GenInst_ImporterFunc_t2977850894_0_0_0,
	&GenInst_IDictionary_2_t2914292212_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_KeyValuePair_2_t3653207108_0_0_0,
	&GenInst_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0,
	&GenInst_IDictionary_2_t3266987655_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_KeyValuePair_2_t1345327748_0_0_0,
	&GenInst_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0,
	&GenInst_IList_1_t4234766737_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ArrayMetadata_t2008834462_0_0_0_KeyValuePair_2_t3653207108_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t2008834462_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1703537581_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2961690774_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ObjectMetadata_t3995922398_0_0_0_KeyValuePair_2_t1345327748_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t3995922398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3690625517_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t4234766737_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3929469856_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t408878057_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t103581176_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t2914292212_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2608995331_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PropertyMetadata_t3693826136_0_0_0_KeyValuePair_2_t1043231486_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3693826136_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3365950620_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3025249456_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t2977850894_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2672554013_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0,
	&GenInst_IDictionary_2_t37308697_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IDictionary_2_t37308697_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1097446850_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32U5BU5D_t3030399641_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4090537794_0_0_0,
	&GenInst_WriterContext_t4137194742_0_0_0,
	&GenInst_StateHandler_t387387051_0_0_0,
	&GenInst_MulticastDelegate_t3201952435_0_0_0,
	&GenInst_SampleDescriptor_t3285910703_0_0_0,
	&GenInst_MessageTypes_t223757197_0_0_0_String_t_0_0_0,
	&GenInst_MessageTypes_t223757197_0_0_0_Il2CppObject_0_0_0,
	&GenInst_MessageTypes_t223757197_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_U3CU3E__AnonType1_2_t377947286_0_0_0,
	&GenInst_Dictionary_2_t3943999495_0_0_0,
	&GenInst_OnlineMapsTile_t21329940_0_0_0,
	&GenInst_OnlineMapsMarker_t3492166682_0_0_0,
	&GenInst_OnlineMapsMarkerBase_t3900955221_0_0_0,
	&GenInst_OnlineMapsFindAutocompleteResult_t2619648090_0_0_0,
	&GenInst_OnlineMapsDirectionStep_t2252483185_0_0_0,
	&GenInst_OnlineMapsFindPlacesResult_t686120750_0_0_0,
	&GenInst_OnlineMapsGetElevationResult_t3115336078_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_OnlineMapsOSMTag_t3629071465_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_OnlineMapsOSMNode_t3383990403_0_0_0,
	&GenInst_OnlineMapsOSMWay_t3319895272_0_0_0,
	&GenInst_OnlineMapsOSMRelation_t1871982797_0_0_0,
	&GenInst_OnlineMapsOSMTag_t3629071465_0_0_0,
	&GenInst_OnlineMapsMarker3D_t576815539_0_0_0,
	&GenInst_TilesetFadeExampleItem_t1982028563_0_0_0,
	&GenInst_OnlineMapsTile_t21329940_0_0_0_Material_t193706927_0_0_0,
	&GenInst_TilesetFadeExampleItem_t1982028563_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MapManager_t3593696545_0_0_0,
	&GenInst_OnlineMapsDrawingElement_t539447654_0_0_0,
	&GenInst_OnlineMapsEvents_t850667009_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_OnlineMapsMarkerBillboard_t495103289_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_OnlineMapsMarkerBillboard_t495103289_0_0_0,
	&GenInst_KeyValuePair_2_t1555241442_0_0_0,
	&GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_TilesetFlatMarker_t3550885087_0_0_0,
	&GenInst_String_t_0_0_0_Int16_t4041245914_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int16_t4041245914_0_0_0,
	&GenInst_OnlineMapsMarker_t3492166682_0_0_0_OnlineMapsMarker_t3492166682_0_0_0,
	&GenInst_OnlineMapsMarker_t3492166682_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_TilesetSortedMarker_t2480816995_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_TilesetSortedMarker_t2480816995_0_0_0,
	&GenInst_OnlineMapsMarker_t3492166682_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_OnlineMapsFindAutocompleteResultTerm_t3836860852_0_0_0,
	&GenInst_OnlineMapsFindPlacesResultPhoto_t2950683050_0_0_0,
	&GenInst_OnlineMapsGoogleAPIQuery_t356009153_0_0_0,
	&GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0,
	&GenInst_String_t_0_0_0_OnlineMapsOSMNode_t3383990403_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3056114887_0_0_0,
	&GenInst_OnlineMapsOSMNode_t3383990403_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_OnlineMapsOSMRelationMember_t1040319881_0_0_0,
	&GenInst_OnlineMapsTile_t21329940_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_OnlineMapsTile_t21329940_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_OnlineMapsDrawingElement_t539447654_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_OnlineMapsBufferZoom_t2072536377_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_OnlineMapsBufferZoom_t2072536377_0_0_0,
	&GenInst_KeyValuePair_2_t3132674530_0_0_0,
	&GenInst_OnlineMapsBuildingBase_t650727021_0_0_0,
	&GenInst_OnlineMapsBuildingMetaInfo_t1480818351_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_OnlineMapsBuildingMaterial_t302108695_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0,
	&GenInst_String_t_0_0_0_OnlineMapsBuildingBase_t650727021_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t322851505_0_0_0,
	&GenInst_OnlineMapsBuildingsNodeData_t1471864431_0_0_0,
	&GenInst_List_1_t1621604317_0_0_0,
	&GenInst_Component_t3519836246_0_0_0,
	&GenInst_CodeU5BU5D_t4050074539_0_0_0,
	&GenInst_Code_t3472511518_0_0_0,
	&GenInst_OnlineMapsXML_t3341520387_0_0_0,
	&GenInst_EventDelegate_t3496309181_0_0_0,
	&GenInst_FadeEntry_t3041229383_0_0_0,
	&GenInst_UIButton_t3377238306_0_0_0,
	&GenInst_UIButtonColor_t3793385709_0_0_0,
	&GenInst_UIWidgetContainer_t701016325_0_0_0,
	&GenInst_UIDragDropItem_t4109477862_0_0_0,
	&GenInst_UIKeyBinding_t790450850_0_0_0,
	&GenInst_UIKeyNavigation_t1158973079_0_0_0,
	&GenInst_UITweener_t2986641582_0_0_0,
	&GenInst_UILabel_t1795115428_0_0_0,
	&GenInst_UIPlaySound_t2984775557_0_0_0,
	&GenInst_UIWidget_t1453041918_0_0_0,
	&GenInst_UIRect_t4127168124_0_0_0,
	&GenInst_Collider_t3497673348_0_0_0,
	&GenInst_UIToggle_t3036740318_0_0_0,
	&GenInst_UIScrollView_t3033954930_0_0_0,
	&GenInst_Bounds_t3033363703_0_0_0,
	&GenInst_UIPanel_t1795085332_0_0_0,
	&GenInst_BMGlyph_t3903496831_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_BMGlyph_t3903496831_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t668667688_0_0_0,
	&GenInst_Parameter_t3376276275_0_0_0,
	&GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0,
	&GenInst_String_t_0_0_0_StringU5BU5D_t1642385972_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0,
	&GenInst_KeyValuePair_2_t1314510456_0_0_0,
	&GenInst_KeyCode_t2283395152_0_0_0,
	&GenInst_AudioListener_t1996719162_0_0_0,
	&GenInst_UICamera_t1496819779_0_0_0,
	&GenInst_UIRoot_t389944298_0_0_0,
	&GenInst_UIDrawCall_t3291843512_0_0_0,
	&GenInst_UISpriteData_t2862501359_0_0_0,
	&GenInst_Sprite_t134787095_0_0_0,
	&GenInst_UISprite_t603616735_0_0_0,
	&GenInst_UIBasicSprite_t754925213_0_0_0,
	&GenInst_UIFont_t389944949_0_0_0,
	&GenInst_MouseOrTouch_t2470076277_0_0_0,
	&GenInst_DepthEntry_t974746545_0_0_0,
	&GenInst_BMSymbol_t1865486779_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4232308010_0_0_0,
	&GenInst_Paragraph_t2587095060_0_0_0,
	&GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0,
	&GenInst_String_t_0_0_0_BetterList_1_t2807483702_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_BetterList_1_t2807483702_0_0_0,
	&GenInst_KeyValuePair_2_t2479608186_0_0_0,
	&GenInst_UITexture_t2537039969_0_0_0,
	&GenInst_ApplicationManager_t2110631419_0_0_0,
	&GenInst_ChatmageddonAudioManager_t766956996_0_0_0,
	&GenInst_SFXSource_t383013662_0_0_0,
	&GenInst_MusicSource_t904181206_0_0_0,
	&GenInst_ChatmageddonFBManager_t234400784_0_0_0,
	&GenInst_Hashtable_t909839986_0_0_0,
	&GenInst_InternetReachability_t3219127626_0_0_0,
	&GenInst_LockoutManager_t4122842952_0_0_0,
	&GenInst_GlobalManagersController_t3516162077_0_0_0,
	&GenInst_PlayerManager_t1596653588_0_0_0,
	&GenInst_FeedManager_t2709865823_0_0_0,
	&GenInst_NotificationManager_t2388475022_0_0_0,
	&GenInst_InventoryManager_t38269895_0_0_0,
	&GenInst_ItemsManager_t2342504123_0_0_0,
	&GenInst_ModalManager_t4136513312_0_0_0,
	&GenInst_ShieldManager_t3551654236_0_0_0,
	&GenInst_MineManager_t2821846902_0_0_0,
	&GenInst_ChatmageddonSaveData_t4036050876_0_0_0,
	&GenInst_BuildManager_t19508555_0_0_0,
	&GenInst_ChatmageddonGamestate_t808689860_0_0_0,
	&GenInst_ChatmageddonServer_t594474938_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Hashtable_t909839986_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_HomeSceneManager_t2690266116_0_0_0,
	&GenInst_SettingsSceneManager_t3860594814_0_0_0,
	&GenInst_ChatSceneManager_t555354597_0_0_0,
	&GenInst_LeaderboardSceneManager_t2883297036_0_0_0,
	&GenInst_LoginRegistrationSceneManager_t2327503985_0_0_0,
	&GenInst_LoginSuccessSceneManager_t2204684339_0_0_0,
	&GenInst_ToastManager_t1002293494_0_0_0,
	&GenInst_ConfigManager_t2239702727_0_0_0,
	&GenInst_StatusListItem_t459202613_0_0_0,
	&GenInst_ChatManager_t2792590695_0_0_0,
	&GenInst_StatusNavigationController_t3357829236_0_0_0,
	&GenInst_FriendHomeNavigationController_t1091974581_0_0_0,
	&GenInst_AttackHomeNavigationController_t3047766999_0_0_0,
	&GenInst_UIAtlas_t1304615221_0_0_0,
	&GenInst_SettingsManager_t2519859232_0_0_0,
	&GenInst_SettingsNavigationController_t2581944457_0_0_0,
	&GenInst_ChatThread_t2394323482_0_0_0,
	&GenInst_Friend_t3555014108_0_0_0,
	&GenInst_ChatMessage_t2384228687_0_0_0,
	&GenInst_ChatMessage_t2384228687_0_0_0_DateTime_t693205669_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DateTime_t693205669_0_0_0,
	&GenInst_Audience_t3183951146_0_0_0,
	&GenInst_Gender_t3618070753_0_0_0,
	&GenInst_ValidContact_t1479914934_0_0_0,
	&GenInst_ValidContact_t1479914934_0_0_0_String_t_0_0_0,
	&GenInst_ValidContact_t1479914934_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_List_1_t1398341365_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2458479518_0_0_0,
	&GenInst_AttackManager_t3475553125_0_0_0,
	&GenInst_FriendManager_t36140827_0_0_0,
	&GenInst_DeviceCameraManager_t1478748484_0_0_0,
	&GenInst_RegistrationManager_t1533517116_0_0_0,
	&GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0,
	&GenInst_ChatmageddonDeviceShareManager_t740212713_0_0_0_ShareRequestLocation_t2965642179_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ShareRequestLocation_t2965642179_0_0_0,
	&GenInst_ChatmageddonTwitterManager_t3244794565_0_0_0,
	&GenInst_FlurryController_t2385863972_0_0_0,
	&GenInst_FormatDateTime_t1158606858_0_0_0,
	&GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0,
	&GenInst_ChatmageddonPurchaseManager_t1221205491_0_0_0_ShopPurchasableItems_t522663808_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ShopPurchasableItems_t522663808_0_0_0,
	&GenInst_UnibillManager_t3725383138_0_0_0,
	&GenInst_Bucks_t3932015720_0_0_0,
	&GenInst_Missile_t813944928_0_0_0,
	&GenInst_Shield_t3327121081_0_0_0,
	&GenInst_Mine_t2729441277_0_0_0,
	&GenInst_Fuel_t1015546524_0_0_0,
	&GenInst_PurchaseableItem_t3351122996_0_0_0,
	&GenInst_QuantitativeItem_t3036513780_0_0_0,
	&GenInst_LoadingPopUp_t1016279926_0_0_0,
	&GenInst_ChatmageddonPanelTransitionManager_t1399015365_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1603735834_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_PanelType_t482769230_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1603735834_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_Panel_t1787746694_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Panel_t1787746694_0_0_0,
	&GenInst_KeyValuePair_2_t702033233_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_List_1_t2207118780_0_0_0_String_t_0_0_0,
	&GenInst_FAQ_t2837997648_0_0_0,
	&GenInst_HeaderManager_t49185160_0_0_0,
	&GenInst_HomeLeftUIManager_t3932466733_0_0_0,
	&GenInst_Bread_t457172030_0_0_0,
	&GenInst_TweenAlpha_t2421518635_0_0_0,
	&GenInst_VibrationController_t2976556198_0_0_0,
	&GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0,
	&GenInst_String_t_0_0_0_Feed_t3408144164_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Feed_t3408144164_0_0_0,
	&GenInst_KeyValuePair_2_t3080268648_0_0_0,
	&GenInst_StatusManager_t2720247037_0_0_0,
	&GenInst_LaunchedItem_t3670634427_0_0_0,
	&GenInst_Friend_t3555014108_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Friend_t3555014108_0_0_0_String_t_0_0_0,
	&GenInst_PrefabResource_t926725558_0_0_0,
	&GenInst_ChatContactsListItem_t4002571236_0_0_0,
	&GenInst_ChatContact_t3760700014_0_0_0_String_t_0_0_0,
	&GenInst_ChatContact_t3760700014_0_0_0,
	&GenInst_ChatNavigationController_t880507216_0_0_0,
	&GenInst_ChatNavigationController_t880507216_0_0_0_ChatNavScreen_t42377261_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ChatNavScreen_t42377261_0_0_0,
	&GenInst_ChatNavScreen_t42377261_0_0_0,
	&GenInst_FullScreenLoadingIndicator_t2986689576_0_0_0,
	&GenInst_ChatMessageListItem_t2425051210_0_0_0,
	&GenInst_ChatThreadsListItem_t303868848_0_0_0,
	&GenInst_ChatThread_t2394323482_0_0_0_DateTime_t693205669_0_0_0,
	&GenInst_AttackNavigationController_t2145483014_0_0_0,
	&GenInst_AttackNavigationController_t2145483014_0_0_0_AttackNavScreen_t36759459_0_0_0,
	&GenInst_Il2CppObject_0_0_0_AttackNavScreen_t36759459_0_0_0,
	&GenInst_AttackNavScreen_t36759459_0_0_0,
	&GenInst_AttackHomeNavigationController_t3047766999_0_0_0_AttackHomeNav_t3944890504_0_0_0,
	&GenInst_Il2CppObject_0_0_0_AttackHomeNav_t3944890504_0_0_0,
	&GenInst_AttackHomeNav_t3944890504_0_0_0,
	&GenInst_AttackSearchNavigationController_t4133786906_0_0_0,
	&GenInst_AttackSearchNavigationController_t4133786906_0_0_0_AttackSearchNav_t4257884637_0_0_0,
	&GenInst_Il2CppObject_0_0_0_AttackSearchNav_t4257884637_0_0_0,
	&GenInst_AttackSearchNav_t4257884637_0_0_0,
	&GenInst_ContactsPhoneNumberManager_t4168190569_0_0_0,
	&GenInst_AttackListItem_t3405986821_0_0_0,
	&GenInst_StoreManager_t650776524_0_0_0,
	&GenInst_HomeCenterUIManager_t2267606293_0_0_0,
	&GenInst_FriendHomeNavigationController_t1091974581_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0,
	&GenInst_Il2CppObject_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0,
	&GenInst_FriendHomeNavScreen_t1599038296_0_0_0,
	&GenInst_FriendsNavigationController_t702141233_0_0_0,
	&GenInst_FriendsNavigationController_t702141233_0_0_0_FriendNavScreen_t2175383453_0_0_0,
	&GenInst_Il2CppObject_0_0_0_FriendNavScreen_t2175383453_0_0_0,
	&GenInst_FriendNavScreen_t2175383453_0_0_0,
	&GenInst_FriendsSearchNavigationController_t1013932095_0_0_0,
	&GenInst_FriendsSearchNavigationController_t1013932095_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0,
	&GenInst_Il2CppObject_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0,
	&GenInst_FriendSearchNavScreen_t3018389163_0_0_0,
	&GenInst_FriendsListItem_t521220246_0_0_0,
	&GenInst_MultiPhoneNumberListItem_t177127779_0_0_0,
	&GenInst_OtherFriendsListItem_t1916582308_0_0_0,
	&GenInst_ContactsNaviagationController_t22981102_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0,
	&GenInst_PhoneNumberNavScreen_t1263467250_0_0_0,
	&GenInst_ContactsNaviagationController_t22981102_0_0_0,
	&GenInst_ErrorMessagePopUp_t1211465891_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2696242213_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2696242213_0_0_0,
	&GenInst_KeyValuePair_2_t2368366697_0_0_0,
	&GenInst_HomeRightUIManager_t3565483256_0_0_0,
	&GenInst_HomeTabController_t1746680606_0_0_0,
	&GenInst_HomeBottomTabController_t251067189_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_SwitchMapStateButton_t2972050837_0_0_0,
	&GenInst_DefendModalNavigationController_t472169731_0_0_0,
	&GenInst_DefendModalNavigationController_t472169731_0_0_0_DefendNavScreen_t3838619703_0_0_0,
	&GenInst_Il2CppObject_0_0_0_DefendNavScreen_t3838619703_0_0_0,
	&GenInst_DefendNavScreen_t3838619703_0_0_0,
	&GenInst_GroupFriendUI_t1634706847_0_0_0,
	&GenInst_MineAudienceTab_t1025879672_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2098562409_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2098562409_0_0_0,
	&GenInst_KeyValuePair_2_t1770686893_0_0_0,
	&GenInst_PlayerProfileNavigationController_t1388421560_0_0_0,
	&GenInst_PlayerProfileNavigationController_t1388421560_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0,
	&GenInst_Il2CppObject_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0,
	&GenInst_PlayerProfileNavScreen_t2660668259_0_0_0,
	&GenInst_SettingsNavigationController_t2581944457_0_0_0_SettingsNavScreen_t2267127234_0_0_0,
	&GenInst_Il2CppObject_0_0_0_SettingsNavScreen_t2267127234_0_0_0,
	&GenInst_SettingsNavScreen_t2267127234_0_0_0,
	&GenInst_ProfileNavigationController_t2708779591_0_0_0_ProfileNavScreen_t1126657854_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileNavScreen_t1126657854_0_0_0,
	&GenInst_ProfileNavScreen_t1126657854_0_0_0,
	&GenInst_ProfileNavigationController_t2708779591_0_0_0,
	&GenInst_StatusNavigationController_t3357829236_0_0_0_StatusNavScreen_t2872369083_0_0_0,
	&GenInst_Il2CppObject_0_0_0_StatusNavScreen_t2872369083_0_0_0,
	&GenInst_StatusNavScreen_t2872369083_0_0_0,
	&GenInst_StorePopUpController_t952004567_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t183066060_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t183066060_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t183066060_0_0_0,
	&GenInst_KeyValuePair_2_t4150157840_0_0_0,
	&GenInst_LeaderboardNavigationController_t1680977333_0_0_0,
	&GenInst_LeaderboardNavigationController_t1680977333_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0,
	&GenInst_Il2CppObject_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0,
	&GenInst_LeaderboardNavScreen_t2356043712_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_String_t_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1428657631_0_0_0,
	&GenInst_InitSceneManager_t1690944945_0_0_0,
	&GenInst_ConnectionPopUp_t470643858_0_0_0,
	&GenInst_LeaderboardListItem_t3112309392_0_0_0,
	&GenInst_User_t719925459_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_User_t719925459_0_0_0_String_t_0_0_0,
	&GenInst_User_t719925459_0_0_0,
	&GenInst_LeaderboardManager_t1534320874_0_0_0,
	&GenInst_LoginBottomUIManager_t3738095009_0_0_0,
	&GenInst_RegistrationNavigationcontroller_t539766269_0_0_0,
	&GenInst_RegistrationNavigationcontroller_t539766269_0_0_0_RegNavScreen_t667173359_0_0_0,
	&GenInst_Il2CppObject_0_0_0_RegNavScreen_t667173359_0_0_0,
	&GenInst_RegNavScreen_t667173359_0_0_0,
	&GenInst_CountryCodeListManager_t203952622_0_0_0,
	&GenInst_CountryCodeListItem_t2467463620_0_0_0,
	&GenInst_RegInput_t2119160278_0_0_0,
	&GenInst_LoginLoadingIndicator_t3051072036_0_0_0,
	&GenInst_MineDefuseSwipe_t2955699161_0_0_0,
	&GenInst_AttemptTab_t3964708364_0_0_0,
	&GenInst_LauncherSceneManager_t2478891917_0_0_0,
	&GenInst_BlockListItem_t1514504046_0_0_0,
	&GenInst_FAQListItem_t1761156371_0_0_0,
	&GenInst_BoxCollider_t22920061_0_0_0,
	&GenInst_TweenPosition_t1144714832_0_0_0,
	&GenInst_IEquatable_1_t1872368772_0_0_0,
	&GenInst_ColourStyleSheet_t290320840_0_0_0,
	&GenInst_SliderNumberLabel_t290801982_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_GameObject_t1756533147_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2816671300_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_KeyValuePair_2_t3888080226_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector3_t2243707580_0_0_0_KeyValuePair_2_t3888080226_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_Vector3_t2243707580_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3331398246_0_0_0,
	&GenInst_FacebookFriend_t758270532_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_NumberFormat_t441739224_0_0_0,
	&GenInst_PhoneMetadata_t366861403_0_0_0,
	&GenInst_Link_t247561424_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0,
	&GenInst_String_t_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3616123979_0_0_0,
	&GenInst_HashSet_1_t362681087_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_HashSet_1_t362681087_0_0_0,
	&GenInst_KeyValuePair_2_t1422819240_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1426999556_0_0_0,
	&GenInst_PhoneMetaTerritory_t2073820963_0_0_0,
	&GenInst_PhoneMetaNumberFormat_t4038650015_0_0_0,
	&GenInst_PhoneNumberMatch_t2163858580_0_0_0,
	&GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0,
	&GenInst_String_t_0_0_0_AreaCodeMap_t3230759372_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_AreaCodeMap_t3230759372_0_0_0,
	&GenInst_KeyValuePair_2_t2902883856_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_KeyValuePair_2_t2812303849_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t2812303849_0_0_0,
	&GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0,
	&GenInst_String_t_0_0_0_PhoneMetadata_t366861403_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t38985887_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_String_t_0_0_0,
	&GenInst_IEnumerator_1_t3934349703_0_0_0,
	&GenInst_IEnumerator_1_t164973122_0_0_0,
	&GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0,
	&GenInst_String_t_0_0_0_Entry_t1519365992_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Entry_t1519365992_0_0_0,
	&GenInst_KeyValuePair_2_t1191490476_0_0_0,
	&GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_Sort_m3546416104_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_compare_m940423571_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0_List_1_ConvertAll_m343121476_gp_0_0_0_0,
	&GenInst_List_1_ConvertAll_m343121476_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3556217344_gp_0_0_0_0,
	&GenInst_Enumerator_t4145643798_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0,
	&GenInst_RBTree_Intern_m3121628056_gp_0_0_0_0,
	&GenInst_RBTree_Remove_m2480195356_gp_0_0_0_0,
	&GenInst_RBTree_Lookup_m3378201690_gp_0_0_0_0,
	&GenInst_RBTree_find_key_m2107656200_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4073929546_0_0_0,
	&GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0,
	&GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t404405498_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_BindingList_1_t257543824_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m297629787_gp_0_0_0_0,
	&GenInst_Enumerable_Aggregate_m297629787_gp_1_0_0_0_Enumerable_Aggregate_m297629787_gp_0_0_0_0_Enumerable_Aggregate_m297629787_gp_1_0_0_0,
	&GenInst_Enumerable_All_m2363499768_gp_0_0_0_0,
	&GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Any_m665396702_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Concat_m3276894131_gp_0_0_0_0,
	&GenInst_Enumerable_CreateConcatIterator_m2343633649_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m136242780_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_First_m4120844597_gp_0_0_0_0,
	&GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_First_m1693250038_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1672962002_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Last_m1426884230_gp_0_0_0_0,
	&GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0,
	&GenInst_Enumerable_Iterate_m2058622934_gp_0_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0_Enumerable_Iterate_m2058622934_gp_1_0_0_0,
	&GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0,
	&GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Enumerable_Min_m1749901801_gp_0_0_0_0_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0,
	&GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0,
	&GenInst_Enumerable_OrderByDescending_m2247647662_gp_0_0_0_0_Enumerable_OrderByDescending_m2247647662_gp_1_0_0_0,
	&GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0,
	&GenInst_Enumerable_OrderByDescending_m1210394609_gp_0_0_0_0_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0,
	&GenInst_Enumerable_OrderByDescending_m1210394609_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_Skip_m3101762585_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSkipIterator_m3940565531_gp_0_0_0_0,
	&GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0,
	&GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0,
	&GenInst_Enumerable_ThenBy_m575340852_gp_0_0_0_0_Enumerable_ThenBy_m575340852_gp_1_0_0_0,
	&GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0,
	&GenInst_Enumerable_ThenBy_m1785535793_gp_0_0_0_0_Enumerable_ThenBy_m1785535793_gp_1_0_0_0,
	&GenInst_Enumerable_ThenBy_m1785535793_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCreateConcatIteratorU3Ec__Iterator1_1_t2976092573_gp_0_0_0_0,
	&GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSkipIteratorU3Ec__Iterator16_1_t284123852_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CMinU3Ec__AnonStorey2E_1_t2538447045_gp_0_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0,
	&GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3528599513_gp_0_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m420282348_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1290221672_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0,
	&GenInst_SortContext_1_t4088581714_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0,
	&GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0,
	&GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_List_1_t2244783541_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0,
	&GenInst_List_1_t264254312_0_0_0,
	&GenInst_ActionExtensions_fire_m398400524_gp_0_0_0_0,
	&GenInst_Json_decode_m444439411_gp_0_0_0_0,
	&GenInst_Json_decodeObject_m2379391988_gp_0_0_0_0,
	&GenInst_ObjectDecoder_decode_m1381048909_gp_0_0_0_0,
	&GenInst_ObjectDecoder_decode_m1878805481_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2611711602_0_0_0,
	&GenInst_SafeDictionary_2_t812589197_gp_0_0_0_0_SafeDictionary_2_t812589197_gp_1_0_0_0,
	&GenInst_CallbackManager_AddFacebookDelegate_m958781935_gp_0_0_0_0,
	&GenInst_CallbackManager_TryCallCallback_m3670554012_gp_0_0_0_0,
	&GenInst_ComponentFactory_GetComponent_m440979758_gp_0_0_0_0,
	&GenInst_ComponentFactory_AddComponent_m3589982647_gp_0_0_0_0,
	&GenInst_MethodArguments_AddNullablePrimitive_m1387850230_gp_0_0_0_0,
	&GenInst_MethodArguments_AddList_m1635305002_gp_0_0_0_0,
	&GenInst_MethodCall_1_t4238425506_gp_0_0_0_0,
	&GenInst_CanvasUIMethodCall_1_t2448137672_gp_0_0_0_0,
	&GenInst_JavaMethodCall_1_t1992182800_gp_0_0_0_0,
	&GenInst_IOSFacebook_AddCallback_m3462239408_gp_0_0_0_0,
	&GenInst_Utilities_GetValueOrDefault_m3032348298_gp_0_0_0_0,
	&GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0_Utilities_AddAllKVPFrom_m2028366180_gp_1_0_0_0,
	&GenInst_Utilities_AddAllKVPFrom_m2028366180_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_UnityUtil_getAnyComponentsOfType_m3238176900_gp_0_0_0_0,
	&GenInst_UnityUtil_loadResourceInstanceOfType_m607627357_gp_0_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_ObservableDictionary_2_t1567152659_gp_1_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t512346522_0_0_0,
	&GenInst_ObservableDictionary_2_t1567152659_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_JsonMapper_RegisterExporter_m2960481954_gp_0_0_0_0,
	&GenInst_JsonMapper_RegisterImporter_m1043896970_gp_0_0_0_0_JsonMapper_RegisterImporter_m1043896970_gp_1_0_0_0,
	&GenInst_U3CRegisterExporterU3Ec__AnonStorey0_1_t2856363167_gp_0_0_0_0,
	&GenInst_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_0_0_0_0_U3CRegisterImporterU3Ec__AnonStorey1_2_t3452082012_gp_1_0_0_0,
	&GenInst_OnlineMapsXML_Find_m2276820904_gp_0_0_0_0,
	&GenInst_OnlineMapsXML_Get_m603060637_gp_0_0_0_0,
	&GenInst_OnlineMapsXML_Get_m2532915309_gp_0_0_0_0,
	&GenInst_OnlineMapsXML_Value_m791613976_gp_0_0_0_0,
	&GenInst_BetterList_1_t2993062629_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t4219084783_gp_0_0_0_0,
	&GenInst_NGUITools_AddChild_m203049429_gp_0_0_0_0,
	&GenInst_NGUITools_AddChild_m124544632_gp_0_0_0_0,
	&GenInst_NGUITools_AddWidget_m3299507383_gp_0_0_0_0,
	&GenInst_NGUITools_AddWidget_m2193511042_gp_0_0_0_0,
	&GenInst_NGUITools_FindInParents_m904571355_gp_0_0_0_0,
	&GenInst_NGUITools_FindInParents_m230326540_gp_0_0_0_0,
	&GenInst_NGUITools_AddMissingComponent_m3184960056_gp_0_0_0_0,
	&GenInst_NGUITools_Execute_m2896051690_gp_0_0_0_0,
	&GenInst_NGUITools_ExecuteAll_m3760296385_gp_0_0_0_0,
	&GenInst_UITweener_Begin_m1126580952_gp_0_0_0_0,
	&GenInst_IPCycler_MakeWidgets_m159957440_gp_0_0_0_0,
	&GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0,
	&GenInst_DeviceShareManager_2_t116377866_gp_0_0_0_0_DeviceShareManager_2_t116377866_gp_1_0_0_0,
	&GenInst_BaseServer_1_t4274267663_gp_0_0_0_0,
	&GenInst_U3CCheckPersistantLoginU3Ec__AnonStorey0_t2587949953_gp_0_0_0_0,
	&GenInst_U3CRetrieveAllActiveAuthU3Ec__AnonStorey1_t1497602663_gp_0_0_0_0,
	&GenInst_U3CRetrieveUserFacebookAuthU3Ec__AnonStorey2_t2271550450_gp_0_0_0_0,
	&GenInst_U3CLoginViaTwitterU3Ec__AnonStorey3_t2886528624_gp_0_0_0_0,
	&GenInst_U3CLoginViaGoogleU3Ec__AnonStorey4_t1026245177_gp_0_0_0_0,
	&GenInst_U3CLoginViaFacebookU3Ec__AnonStorey5_t2316794845_gp_0_0_0_0,
	&GenInst_U3CLoginViaUsernameU3Ec__AnonStorey6_t3039520268_gp_0_0_0_0,
	&GenInst_U3CDeleteAuthViaIDU3Ec__AnonStorey7_t1409908404_gp_0_0_0_0,
	&GenInst_U3CDeleteAuthViaUDIDU3Ec__AnonStorey8_t1268384018_gp_0_0_0_0,
	&GenInst_U3CDeleteTwitterAuthU3Ec__AnonStorey9_t2609573364_gp_0_0_0_0,
	&GenInst_U3CDeleteFacebookAuthU3Ec__AnonStoreyA_t417802369_gp_0_0_0_0,
	&GenInst_U3CLogoutU3Ec__AnonStoreyB_t1763411585_gp_0_0_0_0,
	&GenInst_U3CResetPasswordU3Ec__AnonStoreyC_t2840379088_gp_0_0_0_0,
	&GenInst_U3CRetrieveUsersFriendsListU3Ec__AnonStoreyD_t2337793304_gp_0_0_0_0,
	&GenInst_U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t2162634300_gp_0_0_0_0,
	&GenInst_U3CRetrieveUsersPendindFriendRequestsU3Ec__AnonStoreyF_t4120878747_gp_0_0_0_0,
	&GenInst_U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2590691728_gp_0_0_0_0,
	&GenInst_U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t3361226555_gp_0_0_0_0,
	&GenInst_U3CAddNewFriendU3Ec__AnonStorey12_t2594874173_gp_0_0_0_0,
	&GenInst_U3CRemoveExistingFriendU3Ec__AnonStorey13_t1942826340_gp_0_0_0_0,
	&GenInst_U3CAcceptFriendRequestU3Ec__AnonStorey14_t1031038011_gp_0_0_0_0,
	&GenInst_U3CDeclineFriendRequestU3Ec__AnonStorey15_t3374200632_gp_0_0_0_0,
	&GenInst_U3CRevokeFriendRequestU3Ec__AnonStorey16_t3843201513_gp_0_0_0_0,
	&GenInst_U3CImportContatcsU3Ec__AnonStorey17_t4076402405_gp_0_0_0_0,
	&GenInst_U3CDeleteAllContactsU3Ec__AnonStorey18_t3160876367_gp_0_0_0_0,
	&GenInst_U3CDeleteSingleContactU3Ec__AnonStorey19_t3571938214_gp_0_0_0_0,
	&GenInst_U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t3127642584_gp_0_0_0_0,
	&GenInst_U3CGetExistingUserU3Ec__AnonStorey1B_t4228972032_gp_0_0_0_0,
	&GenInst_U3CRegisterNewUserU3Ec__AnonStorey1C_t3612730071_gp_0_0_0_0,
	&GenInst_U3CUpdateExistingUserU3Ec__AnonStorey1D_t1768280839_gp_0_0_0_0,
	&GenInst_U3CSavePushTokenU3Ec__AnonStorey1E_t911289941_gp_0_0_0_0,
	&GenInst_U3CLoadImageFromURLU3Ec__AnonStorey1F_t2208707420_gp_0_0_0_0,
	&GenInst_ChatServer_1_t2802672986_gp_0_0_0_0,
	&GenInst_U3COpenNewThreadU3Ec__AnonStorey0_t3278697514_gp_0_0_0_0,
	&GenInst_U3CRetrieveOpenThreadsU3Ec__AnonStorey1_t3799062252_gp_0_0_0_0,
	&GenInst_U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t1275715504_gp_0_0_0_0,
	&GenInst_U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t1603105993_gp_0_0_0_0,
	&GenInst_U3CRetrieveThreadMessagesForUserU3Ec__AnonStorey4_t2099807180_gp_0_0_0_0,
	&GenInst_U3CAddUserToThreadU3Ec__AnonStorey5_t1890844300_gp_0_0_0_0,
	&GenInst_U3CPostMessageToThreadU3Ec__AnonStorey6_t499261174_gp_0_0_0_0,
	&GenInst_U3CUpdateThreadReadU3Ec__AnonStorey7_t311204856_gp_0_0_0_0,
	&GenInst_U3CDeleteThreadU3Ec__AnonStorey8_t2511843973_gp_0_0_0_0,
	&GenInst_EnumSwitch_1_t1206431522_gp_0_0_0_0,
	&GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0,
	&GenInst_BasePurchaseManager_2_t3333410625_gp_0_0_0_0_BasePurchaseManager_2_t3333410625_gp_1_0_0_0,
	&GenInst_AudioManager_1_t2880363018_gp_0_0_0_0,
	&GenInst_BaseInfiniteListItem_1_t4091055671_gp_0_0_0_0,
	&GenInst_BaseInfiniteListPopulator_1_t409764030_gp_0_0_0_0,
	&GenInst_ListMovementDirection_t356946411_gp_0_0_0_0,
	&GenInst_U3CItemIsInvisibleU3Ec__Iterator0_t1925290786_gp_0_0_0_0,
	&GenInst_U3CSetPanelPosOverTimeU3Ec__Iterator1_t1729400738_gp_0_0_0_0,
	&GenInst_BaseSceneManager_1_t1474365667_gp_0_0_0_0,
	&GenInst_U3CSceneLoadU3Ec__Iterator0_t664871687_gp_0_0_0_0,
	&GenInst_GameState_1_t3222403128_gp_0_0_0_0,
	&GenInst_MonoSingleton_1_t3179822507_gp_0_0_0_0,
	&GenInst_NavigationController_2_t2720678560_gp_0_0_0_0,
	&GenInst_NavigationController_2_t2720678560_gp_1_0_0_0,
	&GenInst_NavigationController_2_t2720678560_gp_0_0_0_0_NavigationController_2_t2720678560_gp_1_0_0_0,
	&GenInst_PanelTransitionManager_1_t1801572561_gp_0_0_0_0,
	&GenInst_U3CSwitchPanelsU3Ec__Iterator0_t1828045924_gp_0_0_0_0,
	&GenInst_U3CTweenObjectForwardsU3Ec__Iterator1_t1210270656_gp_0_0_0_0,
	&GenInst_FBManager_1_t2471193464_gp_0_0_0_0,
	&GenInst_LoadPictureCallback_t2054799482_gp_0_0_0_0,
	&GenInst_U3CPostHighScoreU3Ec__AnonStorey2_t3067682344_gp_0_0_0_0,
	&GenInst_U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t311754389_gp_0_0_0_0,
	&GenInst_U3CCheckScoreUpdateU3Ec__Iterator0_t1608651375_gp_0_0_0_0,
	&GenInst_U3CLoadPictureAPIU3Ec__AnonStorey4_t3748439766_gp_0_0_0_0,
	&GenInst_U3CLoadPictureEnumeratorU3Ec__Iterator1_t2410595175_gp_0_0_0_0,
	&GenInst_BaseTwitterManager_1_t2537397012_gp_0_0_0_0,
	&GenInst_RequestType_t4084858450_gp_0_0_0_0,
	&GenInst_U3CCheckLogingSuccessU3Ec__Iterator0_t2600799761_gp_0_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0,
	&GenInst_ListExtension_ConvertAll_m3788713098_gp_0_0_0_0,
	&GenInst_IEnumerator_1_t105163639_0_0_0,
	&GenInst_EnumerableFromConstructor_1_t1982007363_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType0_2_t3193949424_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType1_2_t3193949521_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType1_2_t3193949521_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_0_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_1_0_0_0,
	&GenInst_U3CU3E__AnonType2_3_t3193949487_gp_2_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_FacebookSettings_t2167659529_0_0_0,
	&GenInst_LifecycleHelper_t337468626_0_0_0,
	&GenInst_ThreadingCallbackHelper_t3782680035_0_0_0,
	&GenInst_IDictionary_2_t2603311978_0_0_0,
	&GenInst_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_ArcadeFacebookGameObject_t1372893397_0_0_0,
	&GenInst_AsyncRequestString_t2888107224_0_0_0,
	&GenInst_IList_1_t3230389896_0_0_0,
	&GenInst_JsBridge_t2435492180_0_0_0,
	&GenInst_CanvasFacebookGameObject_t1732848805_0_0_0,
	&GenInst_EditorFacebookGameObject_t63110230_0_0_0,
	&GenInst_MockLoginDialog_t531765055_0_0_0,
	&GenInst_MockShareDialog_t1542668653_0_0_0,
	&GenInst_EmptyMockDialog_t2854396319_0_0_0,
	&GenInst_EditorFacebookLoader_t2109505584_0_0_0,
	&GenInst_CanvasFacebookLoader_t1985905999_0_0_0,
	&GenInst_IOSFacebookLoader_t3879535236_0_0_0,
	&GenInst_AndroidFacebookLoader_t3776634754_0_0_0,
	&GenInst_ArcadeFacebookLoader_t3930015791_0_0_0,
	&GenInst_AndroidFacebookGameObject_t2149209708_0_0_0,
	&GenInst_IOSFacebookGameObject_t3391486318_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_List_1_t3383317540_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0,
	&GenInst_FacebookFriendsResult_t459691178_0_0_0,
	&GenInst_FacebookMeResult_t187696501_0_0_0,
	&GenInst_MeshRenderer_t1268241104_0_0_0,
	&GenInst_PurchaseType_t1639241305_0_0_0,
	&GenInst_WP8Eventhook_t2441977752_0_0_0,
	&GenInst_Win8Eventhook_t3380410125_0_0_0,
	&GenInst_UnityLevelLoadListener_t2593383553_0_0_0,
	&GenInst_RemoteConfigFetcher_t2652256879_0_0_0,
	&GenInst_AppleAppStoreCallbackMonoBehaviour_t2011772955_0_0_0,
	&GenInst_SamsungAppsMode_t2745005863_0_0_0,
	&GenInst_UnityUtil_t671748753_0_0_0,
	&GenInst_MeshFilter_t3026937449_0_0_0,
	&GenInst_LineManager_t3088676951_0_0_0,
	&GenInst_RegistrationCompleteScreen_t222205918_0_0_0,
	&GenInst_ActiveAnimation_t4137610604_0_0_0,
	&GenInst_StatusIncomingNavScreen_t3315318769_0_0_0,
	&GenInst_StatusOutgoingNavScreen_t2187904061_0_0_0,
	&GenInst_PlayerProfileModal_t3260625137_0_0_0,
	&GenInst_ResultModalUI_t969511824_0_0_0,
	&GenInst_MapGroupModal_t1530151518_0_0_0,
	&GenInst_FriendsInnerNavScreen_t2659475988_0_0_0,
	&GenInst_FriendsOuterNavScreen_t1899896515_0_0_0,
	&GenInst_AttackHomeInnerNavScreen_t1310676258_0_0_0,
	&GenInst_AttackHomeOuterNavScreen_t2515452089_0_0_0,
	&GenInst_NavigationScreen_t2333230110_0_0_0,
	&GenInst_AudioSource_t1135106623_0_0_0,
	&GenInst_HTTPUpdateDelegator_t1331403296_0_0_0,
	&GenInst_GUITexture_t1909122990_0_0_0,
	&GenInst_TweenColor_t3390486518_0_0_0,
	&GenInst_List_1_t849036066_0_0_0,
	&GenInst_ThreadsNavScreen_t4034840106_0_0_0,
	&GenInst_SceneLoader_t1952549817_0_0_0,
	&GenInst_TweenScale_t2697902175_0_0_0,
	&GenInst_DefendMiniGameController_t1844356491_0_0_0,
	&GenInst_DefendInfoNavScreen_t2323813817_0_0_0,
	&GenInst_DefendGameNavScreen_t2935542205_0_0_0,
	&GenInst_OnlineMaps_t1893290312_0_0_0,
	&GenInst_ModalUI_t2568752073_0_0_0,
	&GenInst_OnlineMapsMarker3D_t576815539_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_FriendMarker_t3038476686_0_0_0,
	&GenInst_ChatLoader_t2661087257_0_0_0,
	&GenInst_LaunchAnimationController_t825305345_0_0_0,
	&GenInst_UIPopupList_t109953940_0_0_0,
	&GenInst_LaunchRoutine_t3791963153_0_0_0,
	&GenInst_TextAsset_t3973159845_0_0_0,
	&GenInst_LockoutUI_t1337294023_0_0_0,
	&GenInst_MineMarker_t2486518043_0_0_0,
	&GenInst_PlayerMarker_t2200525527_0_0_0,
	&GenInst_RadarMarkerBlip_t1130528275_0_0_0,
	&GenInst_GroupMarker_t1861645095_0_0_0,
	&GenInst_CameraFilterPack_Blur_GaussianBlur_t3355292571_0_0_0,
	&GenInst_OnlineMapsControlBase3D_t2801313279_0_0_0,
	&GenInst_MineMiniGameController_t2791970086_0_0_0,
	&GenInst_AttackInventoryNavScreen_t33214553_0_0_0,
	&GenInst_AttackReviewNavScreen_t173504869_0_0_0,
	&GenInst_ModalPanel_t3020707755_0_0_0,
	&GenInst_DefendModal_t2645228609_0_0_0,
	&GenInst_AwardModal_t1856366994_0_0_0,
	&GenInst_LongNotificationModal_t834782082_0_0_0,
	&GenInst_NGUIDebug_t1735550160_0_0_0,
	&GenInst_BoxCollider2D_t948534547_0_0_0,
	&GenInst_UIAnchor_t624210015_0_0_0,
	&GenInst_OnlineMapsControlBase_t473237564_0_0_0,
	&GenInst_OnlineMapsBuildingBuiltIn_t544190665_0_0_0,
	&GenInst_MeshCollider_t2718867283_0_0_0,
	&GenInst_OnlineMapsMarkerInstanceBase_t538187336_0_0_0,
	&GenInst_OnlineMapsMarker3DInstance_t3567168358_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_AddressBook_t411411037_0_0_0,
	&GenInst_PPStatsNavScreen_t3294811412_0_0_0,
	&GenInst_LoginCenterUI_t890505802_0_0_0,
	&GenInst_MissileResultsModal_t1615487503_0_0_0,
	&GenInst_MineResultsModal_t4210040424_0_0_0,
	&GenInst_SceneLoaderUI_t1806459353_0_0_0,
	&GenInst_SettingsHomeContent_t249573869_0_0_0,
	&GenInst_SettingsProfileNavScreen_t3587426313_0_0_0,
	&GenInst_BlockListNavScreen_t522977310_0_0_0,
	&GenInst_SettingsProfileContent_t3280006997_0_0_0,
	&GenInst_SpringPanel_t2962744957_0_0_0,
	&GenInst_SpringPosition_t434217086_0_0_0,
	&GenInst_StealthModeNavScreen_t2705902227_0_0_0,
	&GenInst_ShieldSlider_t1483508804_0_0_0,
	&GenInst_ProductCatergoryNavItem_t1498614491_0_0_0,
	&GenInst_NumberPickerTransform_t3121159539_0_0_0,
	&GenInst_ToastUI_t270783635_0_0_0,
	&GenInst_Toaster_t2428368812_0_0_0,
	&GenInst_Light_t494725636_0_0_0,
	&GenInst_TweenFOV_t612833154_0_0_0,
	&GenInst_UITable_t3717403602_0_0_0,
	&GenInst_TweenHeight_t161374028_0_0_0,
	&GenInst_TweenOrthoSize_t2754360798_0_0_0,
	&GenInst_TweenRotation_t1747194511_0_0_0,
	&GenInst_TweenTransform_t3901935067_0_0_0,
	&GenInst_TweenVolume_t1729630735_0_0_0,
	&GenInst_TweenWidth_t511657453_0_0_0,
	&GenInst_UI2DSprite_t1082505957_0_0_0,
	&GenInst_Animation_t2068071072_0_0_0,
	&GenInst_UIInput_t860674234_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_UIGrid_t2420180906_0_0_0,
	&GenInst_UIWrapContent_t1931723019_0_0_0,
	&GenInst_UICenterOnChild_t1687745660_0_0_0,
	&GenInst_UIDraggableCamera_t2562792962_0_0_0,
	&GenInst_UIDragScrollView_t2942595320_0_0_0,
	&GenInst_UIDragDropContainer_t4036588756_0_0_0,
	&GenInst_UIEventListener_t3982099366_0_0_0,
	&GenInst_UIOrthoCamera_t1890393437_0_0_0,
	&GenInst_UIProgressBar_t3824507790_0_0_0,
	&GenInst_UISlider_t2191058247_0_0_0,
	&GenInst_AttackHomeNav_t3944890504_0_0_0_AttackHomeNav_t3944890504_0_0_0,
	&GenInst_AttackNavScreen_t36759459_0_0_0_AttackNavScreen_t36759459_0_0_0,
	&GenInst_AttackSearchNav_t4257884637_0_0_0_AttackSearchNav_t4257884637_0_0_0,
	&GenInst_ChatNavScreen_t42377261_0_0_0_ChatNavScreen_t42377261_0_0_0,
	&GenInst_DefendNavScreen_t3838619703_0_0_0_DefendNavScreen_t3838619703_0_0_0,
	&GenInst_FriendHomeNavScreen_t1599038296_0_0_0_FriendHomeNavScreen_t1599038296_0_0_0,
	&GenInst_FriendNavScreen_t2175383453_0_0_0_FriendNavScreen_t2175383453_0_0_0,
	&GenInst_FriendSearchNavScreen_t3018389163_0_0_0_FriendSearchNavScreen_t3018389163_0_0_0,
	&GenInst_LeaderboardNavScreen_t2356043712_0_0_0_LeaderboardNavScreen_t2356043712_0_0_0,
	&GenInst_PropertyMetadata_t3693826136_0_0_0_PropertyMetadata_t3693826136_0_0_0,
	&GenInst_PhoneNumberNavScreen_t1263467250_0_0_0_PhoneNumberNavScreen_t1263467250_0_0_0,
	&GenInst_PlayerProfileNavScreen_t2660668259_0_0_0_PlayerProfileNavScreen_t2660668259_0_0_0,
	&GenInst_ProfileNavScreen_t1126657854_0_0_0_ProfileNavScreen_t1126657854_0_0_0,
	&GenInst_RegNavScreen_t667173359_0_0_0_RegNavScreen_t667173359_0_0_0,
	&GenInst_SettingsNavScreen_t2267127234_0_0_0_SettingsNavScreen_t2267127234_0_0_0,
	&GenInst_StatusNavScreen_t2872369083_0_0_0_StatusNavScreen_t2872369083_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0_Int16_t4041245914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_UnibillError_t1753859787_0_0_0_UnibillError_t1753859787_0_0_0,
	&GenInst_Color_t2020392075_0_0_0_Color_t2020392075_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0_RuntimePlatform_t1869584967_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_PanelType_t482769230_0_0_0_PanelType_t482769230_0_0_0,
	&GenInst_KeyValuePair_2_t1603735834_0_0_0_KeyValuePair_2_t1603735834_0_0_0,
	&GenInst_KeyValuePair_2_t1603735834_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2812303849_0_0_0_KeyValuePair_2_t2812303849_0_0_0,
	&GenInst_KeyValuePair_2_t2812303849_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0,
	&GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0,
	&GenInst_ArrayMetadata_t2008834462_0_0_0_ArrayMetadata_t2008834462_0_0_0,
	&GenInst_ArrayMetadata_t2008834462_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3653207108_0_0_0_KeyValuePair_2_t3653207108_0_0_0,
	&GenInst_KeyValuePair_2_t3653207108_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ObjectMetadata_t3995922398_0_0_0_ObjectMetadata_t3995922398_0_0_0,
	&GenInst_ObjectMetadata_t3995922398_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1345327748_0_0_0_KeyValuePair_2_t1345327748_0_0_0,
	&GenInst_KeyValuePair_2_t1345327748_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PropertyMetadata_t3693826136_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1043231486_0_0_0_KeyValuePair_2_t1043231486_0_0_0,
	&GenInst_KeyValuePair_2_t1043231486_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2369073723_0_0_0_KeyValuePair_2_t2369073723_0_0_0,
	&GenInst_KeyValuePair_2_t2369073723_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0_Decimal_t724701077_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t3888080226_0_0_0_KeyValuePair_2_t3888080226_0_0_0,
	&GenInst_KeyValuePair_2_t3888080226_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ClientMessage_t624279968_0_0_0_ClientMessage_t624279968_0_0_0,
	&GenInst_ClientMessage_t624279968_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t322707255_0_0_0_KeyValuePair_2_t322707255_0_0_0,
	&GenInst_KeyValuePair_2_t322707255_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0_UInt64_t2909196914_0_0_0,
	&GenInst_KeyValuePair_2_t2387876582_0_0_0_KeyValuePair_2_t2387876582_0_0_0,
	&GenInst_KeyValuePair_2_t2387876582_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2335466038_0_0_0_KeyValuePair_2_t2335466038_0_0_0,
	&GenInst_KeyValuePair_2_t2335466038_0_0_0_Il2CppObject_0_0_0,
	&GenInst_BillingPlatform_t552059234_0_0_0_BillingPlatform_t552059234_0_0_0,
	&GenInst_KeyValuePair_2_t1020750381_0_0_0_KeyValuePair_2_t1020750381_0_0_0,
	&GenInst_KeyValuePair_2_t1020750381_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Vector3Pair_t2859078138_0_0_0_Vector3Pair_t2859078138_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_PanelContainer_t1029301983_0_0_0,
};
