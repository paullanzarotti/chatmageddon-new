﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// CountryCodeILP
struct CountryCodeILP_t3216166666;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen4249585638.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountryCodeListManager
struct  CountryCodeListManager_t203952622  : public MonoSingleton_1_t4249585638
{
public:
	// TweenPosition CountryCodeListManager::posTween
	TweenPosition_t1144714832 * ___posTween_3;
	// CountryCodeILP CountryCodeListManager::codeILP
	CountryCodeILP_t3216166666 * ___codeILP_4;
	// UnityEngine.GameObject CountryCodeListManager::backgroundPanel
	GameObject_t1756533147 * ___backgroundPanel_5;
	// UnityEngine.GameObject CountryCodeListManager::topContent
	GameObject_t1756533147 * ___topContent_6;
	// UnityEngine.GameObject CountryCodeListManager::bottomContent
	GameObject_t1756533147 * ___bottomContent_7;
	// UnityEngine.GameObject CountryCodeListManager::titleBar
	GameObject_t1756533147 * ___titleBar_8;
	// UnityEngine.GameObject CountryCodeListManager::background
	GameObject_t1756533147 * ___background_9;
	// System.Boolean CountryCodeListManager::UIClosing
	bool ___UIClosing_10;

public:
	inline static int32_t get_offset_of_posTween_3() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___posTween_3)); }
	inline TweenPosition_t1144714832 * get_posTween_3() const { return ___posTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_3() { return &___posTween_3; }
	inline void set_posTween_3(TweenPosition_t1144714832 * value)
	{
		___posTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_3, value);
	}

	inline static int32_t get_offset_of_codeILP_4() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___codeILP_4)); }
	inline CountryCodeILP_t3216166666 * get_codeILP_4() const { return ___codeILP_4; }
	inline CountryCodeILP_t3216166666 ** get_address_of_codeILP_4() { return &___codeILP_4; }
	inline void set_codeILP_4(CountryCodeILP_t3216166666 * value)
	{
		___codeILP_4 = value;
		Il2CppCodeGenWriteBarrier(&___codeILP_4, value);
	}

	inline static int32_t get_offset_of_backgroundPanel_5() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___backgroundPanel_5)); }
	inline GameObject_t1756533147 * get_backgroundPanel_5() const { return ___backgroundPanel_5; }
	inline GameObject_t1756533147 ** get_address_of_backgroundPanel_5() { return &___backgroundPanel_5; }
	inline void set_backgroundPanel_5(GameObject_t1756533147 * value)
	{
		___backgroundPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundPanel_5, value);
	}

	inline static int32_t get_offset_of_topContent_6() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___topContent_6)); }
	inline GameObject_t1756533147 * get_topContent_6() const { return ___topContent_6; }
	inline GameObject_t1756533147 ** get_address_of_topContent_6() { return &___topContent_6; }
	inline void set_topContent_6(GameObject_t1756533147 * value)
	{
		___topContent_6 = value;
		Il2CppCodeGenWriteBarrier(&___topContent_6, value);
	}

	inline static int32_t get_offset_of_bottomContent_7() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___bottomContent_7)); }
	inline GameObject_t1756533147 * get_bottomContent_7() const { return ___bottomContent_7; }
	inline GameObject_t1756533147 ** get_address_of_bottomContent_7() { return &___bottomContent_7; }
	inline void set_bottomContent_7(GameObject_t1756533147 * value)
	{
		___bottomContent_7 = value;
		Il2CppCodeGenWriteBarrier(&___bottomContent_7, value);
	}

	inline static int32_t get_offset_of_titleBar_8() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___titleBar_8)); }
	inline GameObject_t1756533147 * get_titleBar_8() const { return ___titleBar_8; }
	inline GameObject_t1756533147 ** get_address_of_titleBar_8() { return &___titleBar_8; }
	inline void set_titleBar_8(GameObject_t1756533147 * value)
	{
		___titleBar_8 = value;
		Il2CppCodeGenWriteBarrier(&___titleBar_8, value);
	}

	inline static int32_t get_offset_of_background_9() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___background_9)); }
	inline GameObject_t1756533147 * get_background_9() const { return ___background_9; }
	inline GameObject_t1756533147 ** get_address_of_background_9() { return &___background_9; }
	inline void set_background_9(GameObject_t1756533147 * value)
	{
		___background_9 = value;
		Il2CppCodeGenWriteBarrier(&___background_9, value);
	}

	inline static int32_t get_offset_of_UIClosing_10() { return static_cast<int32_t>(offsetof(CountryCodeListManager_t203952622, ___UIClosing_10)); }
	inline bool get_UIClosing_10() const { return ___UIClosing_10; }
	inline bool* get_address_of_UIClosing_10() { return &___UIClosing_10; }
	inline void set_UIClosing_10(bool value)
	{
		___UIClosing_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
