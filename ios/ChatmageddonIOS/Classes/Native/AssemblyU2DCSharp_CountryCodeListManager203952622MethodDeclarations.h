﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountryCodeListManager
struct CountryCodeListManager_t203952622;

#include "codegen/il2cpp-codegen.h"

// System.Void CountryCodeListManager::.ctor()
extern "C"  void CountryCodeListManager__ctor_m1074325875 (CountryCodeListManager_t203952622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListManager::Start()
extern "C"  void CountryCodeListManager_Start_m1105062351 (CountryCodeListManager_t203952622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListManager::LoadList()
extern "C"  void CountryCodeListManager_LoadList_m3390385167 (CountryCodeListManager_t203952622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListManager::OpenUI()
extern "C"  void CountryCodeListManager_OpenUI_m2040526951 (CountryCodeListManager_t203952622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListManager::CloseUI()
extern "C"  void CountryCodeListManager_CloseUI_m610416377 (CountryCodeListManager_t203952622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListManager::onPosTweenFinish()
extern "C"  void CountryCodeListManager_onPosTweenFinish_m4097706630 (CountryCodeListManager_t203952622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountryCodeListManager::FinishUIClose()
extern "C"  void CountryCodeListManager_FinishUIClose_m2427464214 (CountryCodeListManager_t203952622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
