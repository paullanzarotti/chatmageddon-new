﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.WritablePurchasable
struct WritablePurchasable_t2700975424;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseType1639241305.h"

// System.Void Unibill.Impl.WritablePurchasable::.ctor(PurchasableItem)
extern "C"  void WritablePurchasable__ctor_m3330344887 (WritablePurchasable_t2700975424 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem Unibill.Impl.WritablePurchasable::get_item()
extern "C"  PurchasableItem_t3963353899 * WritablePurchasable_get_item_m553370792 (WritablePurchasable_t2700975424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WritablePurchasable::set_item(PurchasableItem)
extern "C"  void WritablePurchasable_set_item_m1326700631 (WritablePurchasable_t2700975424 * __this, PurchasableItem_t3963353899 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.WritablePurchasable::get_Id()
extern "C"  String_t* WritablePurchasable_get_Id_m1664980871 (WritablePurchasable_t2700975424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WritablePurchasable::set_Id(System.String)
extern "C"  void WritablePurchasable_set_Id_m3718737532 (WritablePurchasable_t2700975424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchaseType Unibill.Impl.WritablePurchasable::get_PurchaseType()
extern "C"  int32_t WritablePurchasable_get_PurchaseType_m4167932918 (WritablePurchasable_t2700975424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WritablePurchasable::set_PurchaseType(PurchaseType)
extern "C"  void WritablePurchasable_set_PurchaseType_m3140062699 (WritablePurchasable_t2700975424 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.WritablePurchasable::get_description()
extern "C"  String_t* WritablePurchasable_get_description_m2779549058 (WritablePurchasable_t2700975424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WritablePurchasable::set_description(System.String)
extern "C"  void WritablePurchasable_set_description_m1736774339 (WritablePurchasable_t2700975424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.WritablePurchasable::get_name()
extern "C"  String_t* WritablePurchasable_get_name_m2585888861 (WritablePurchasable_t2700975424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WritablePurchasable::set_name(System.String)
extern "C"  void WritablePurchasable_set_name_m1294683958 (WritablePurchasable_t2700975424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.WritablePurchasable::get_downloadableContentId()
extern "C"  String_t* WritablePurchasable_get_downloadableContentId_m3990269160 (WritablePurchasable_t2700975424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.WritablePurchasable::set_downloadableContentId(System.String)
extern "C"  void WritablePurchasable_set_downloadableContentId_m1070177299 (WritablePurchasable_t2700975424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
