﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookBinding/<graphRequest>c__AnonStorey1
struct U3CgraphRequestU3Ec__AnonStorey1_t3312924124;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookBinding/<graphRequest>c__AnonStorey1::.ctor()
extern "C"  void U3CgraphRequestU3Ec__AnonStorey1__ctor_m2428481985 (U3CgraphRequestU3Ec__AnonStorey1_t3312924124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
