﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::.ctor()
#define Queue_1__ctor_m3941010682(__this, method) ((  void (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m2455046297(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3952525865 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m1169835057(__this, method) ((  bool (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m1041828813(__this, method) ((  Il2CppObject * (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m911957665(__this, method) ((  Il2CppObject* (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m965740444(__this, method) ((  Il2CppObject * (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m1264315374(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3952525865 *, PostRequestU5BU5D_t3717946115*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::Dequeue()
#define Queue_1_Dequeue_m2994083792(__this, method) ((  PostRequest_t4132869030 * (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::Peek()
#define Queue_1_Peek_m1359640693(__this, method) ((  PostRequest_t4132869030 * (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::Enqueue(T)
#define Queue_1_Enqueue_m3172990617(__this, ___item0, method) ((  void (*) (Queue_1_t3952525865 *, PostRequest_t4132869030 *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m656596442(__this, ___new_size0, method) ((  void (*) (Queue_1_t3952525865 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::get_Count()
#define Queue_1_get_Count_m2878866632(__this, method) ((  int32_t (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<Unibill.Impl.HTTPClient/PostRequest>::GetEnumerator()
#define Queue_1_GetEnumerator_m1475961856(__this, method) ((  Enumerator_t167621649  (*) (Queue_1_t3952525865 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
