﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_SwitchPanelButton319351498.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackNavigateBackwardsButton
struct  AttackNavigateBackwardsButton_t4019147023  : public SwitchPanelButton_t319351498
{
public:
	// UISprite AttackNavigateBackwardsButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_6;

public:
	inline static int32_t get_offset_of_buttonSprite_6() { return static_cast<int32_t>(offsetof(AttackNavigateBackwardsButton_t4019147023, ___buttonSprite_6)); }
	inline UISprite_t603616735 * get_buttonSprite_6() const { return ___buttonSprite_6; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_6() { return &___buttonSprite_6; }
	inline void set_buttonSprite_6(UISprite_t603616735 * value)
	{
		___buttonSprite_6 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
