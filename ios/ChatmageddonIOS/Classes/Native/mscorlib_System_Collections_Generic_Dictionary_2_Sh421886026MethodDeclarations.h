﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>
struct ShimEnumerator_t421886026;
// System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>
struct Dictionary_2_t316761205;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1068665908_gshared (ShimEnumerator_t421886026 * __this, Dictionary_2_t316761205 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1068665908(__this, ___host0, method) ((  void (*) (ShimEnumerator_t421886026 *, Dictionary_2_t316761205 *, const MethodInfo*))ShimEnumerator__ctor_m1068665908_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3162823503_gshared (ShimEnumerator_t421886026 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3162823503(__this, method) ((  bool (*) (ShimEnumerator_t421886026 *, const MethodInfo*))ShimEnumerator_MoveNext_m3162823503_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m4079776131_gshared (ShimEnumerator_t421886026 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m4079776131(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t421886026 *, const MethodInfo*))ShimEnumerator_get_Entry_m4079776131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m923175312_gshared (ShimEnumerator_t421886026 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m923175312(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t421886026 *, const MethodInfo*))ShimEnumerator_get_Key_m923175312_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3806163270_gshared (ShimEnumerator_t421886026 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3806163270(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t421886026 *, const MethodInfo*))ShimEnumerator_get_Value_m3806163270_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1009098230_gshared (ShimEnumerator_t421886026 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1009098230(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t421886026 *, const MethodInfo*))ShimEnumerator_get_Current_m1009098230_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Decimal>::Reset()
extern "C"  void ShimEnumerator_Reset_m1177401150_gshared (ShimEnumerator_t421886026 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1177401150(__this, method) ((  void (*) (ShimEnumerator_t421886026 *, const MethodInfo*))ShimEnumerator_Reset_m1177401150_gshared)(__this, method)
