﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPCycler/CyclerStoppedHandler
struct CyclerStoppedHandler_t3894542237;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void IPCycler/CyclerStoppedHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void CyclerStoppedHandler__ctor_m3711038764 (CyclerStoppedHandler_t3894542237 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler/CyclerStoppedHandler::Invoke()
extern "C"  void CyclerStoppedHandler_Invoke_m2767721702 (CyclerStoppedHandler_t3894542237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult IPCycler/CyclerStoppedHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CyclerStoppedHandler_BeginInvoke_m4047808059 (CyclerStoppedHandler_t3894542237 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler/CyclerStoppedHandler::EndInvoke(System.IAsyncResult)
extern "C"  void CyclerStoppedHandler_EndInvoke_m3783553214 (CyclerStoppedHandler_t3894542237 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
