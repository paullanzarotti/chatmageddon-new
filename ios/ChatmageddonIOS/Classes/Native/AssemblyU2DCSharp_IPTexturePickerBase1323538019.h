﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UITexture[]
struct UITextureU5BU5D_t4048678140;

#include "AssemblyU2DCSharp_IPPickerBase4159478266.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPTexturePickerBase
struct  IPTexturePickerBase_t1323538019  : public IPPickerBase_t4159478266
{
public:
	// UnityEngine.Shader IPTexturePickerBase::shader
	Shader_t2430389951 * ___shader_13;
	// UITexture[] IPTexturePickerBase::uiTextures
	UITextureU5BU5D_t4048678140* ___uiTextures_14;

public:
	inline static int32_t get_offset_of_shader_13() { return static_cast<int32_t>(offsetof(IPTexturePickerBase_t1323538019, ___shader_13)); }
	inline Shader_t2430389951 * get_shader_13() const { return ___shader_13; }
	inline Shader_t2430389951 ** get_address_of_shader_13() { return &___shader_13; }
	inline void set_shader_13(Shader_t2430389951 * value)
	{
		___shader_13 = value;
		Il2CppCodeGenWriteBarrier(&___shader_13, value);
	}

	inline static int32_t get_offset_of_uiTextures_14() { return static_cast<int32_t>(offsetof(IPTexturePickerBase_t1323538019, ___uiTextures_14)); }
	inline UITextureU5BU5D_t4048678140* get_uiTextures_14() const { return ___uiTextures_14; }
	inline UITextureU5BU5D_t4048678140** get_address_of_uiTextures_14() { return &___uiTextures_14; }
	inline void set_uiTextures_14(UITextureU5BU5D_t4048678140* value)
	{
		___uiTextures_14 = value;
		Il2CppCodeGenWriteBarrier(&___uiTextures_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
