﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<DefendNavScreen>
struct List_1_t3207740835;
// System.Collections.Generic.IEnumerable`1<DefendNavScreen>
struct IEnumerable_1_t4130746748;
// DefendNavScreen[]
struct DefendNavScreenU5BU5D_t1548979470;
// System.Collections.Generic.IEnumerator`1<DefendNavScreen>
struct IEnumerator_1_t1314143530;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<DefendNavScreen>
struct ICollection_1_t495727712;
// System.Collections.ObjectModel.ReadOnlyCollection`1<DefendNavScreen>
struct ReadOnlyCollection_1_t4024405395;
// System.Predicate`1<DefendNavScreen>
struct Predicate_1_t2281589818;
// System.Action`1<DefendNavScreen>
struct Action_1_t3640419085;
// System.Collections.Generic.IComparer`1<DefendNavScreen>
struct IComparer_1_t1793082825;
// System.Comparison`1<DefendNavScreen>
struct Comparison_1_t805391258;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2742470509.h"

// System.Void System.Collections.Generic.List`1<DefendNavScreen>::.ctor()
extern "C"  void List_1__ctor_m3901692575_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1__ctor_m3901692575(__this, method) ((  void (*) (List_1_t3207740835 *, const MethodInfo*))List_1__ctor_m3901692575_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m433966246_gshared (List_1_t3207740835 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m433966246(__this, ___collection0, method) ((  void (*) (List_1_t3207740835 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m433966246_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4042574312_gshared (List_1_t3207740835 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4042574312(__this, ___capacity0, method) ((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1__ctor_m4042574312_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m3067750104_gshared (List_1_t3207740835 * __this, DefendNavScreenU5BU5D_t1548979470* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m3067750104(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3207740835 *, DefendNavScreenU5BU5D_t1548979470*, int32_t, const MethodInfo*))List_1__ctor_m3067750104_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::.cctor()
extern "C"  void List_1__cctor_m3182555556_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3182555556(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3182555556_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3902817807_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3902817807(__this, method) ((  Il2CppObject* (*) (List_1_t3207740835 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3902817807_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m219678819_gshared (List_1_t3207740835 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m219678819(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3207740835 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m219678819_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3300432846_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3300432846(__this, method) ((  Il2CppObject * (*) (List_1_t3207740835 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3300432846_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3853970675_gshared (List_1_t3207740835 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3853970675(__this, ___item0, method) ((  int32_t (*) (List_1_t3207740835 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3853970675_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2512080427_gshared (List_1_t3207740835 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2512080427(__this, ___item0, method) ((  bool (*) (List_1_t3207740835 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2512080427_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m858714437_gshared (List_1_t3207740835 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m858714437(__this, ___item0, method) ((  int32_t (*) (List_1_t3207740835 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m858714437_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m4280884252_gshared (List_1_t3207740835 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m4280884252(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3207740835 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m4280884252_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3934062778_gshared (List_1_t3207740835 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3934062778(__this, ___item0, method) ((  void (*) (List_1_t3207740835 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3934062778_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1570036298_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1570036298(__this, method) ((  bool (*) (List_1_t3207740835 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1570036298_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m390304683_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m390304683(__this, method) ((  bool (*) (List_1_t3207740835 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m390304683_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2003298019_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2003298019(__this, method) ((  Il2CppObject * (*) (List_1_t3207740835 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2003298019_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m21135888_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m21135888(__this, method) ((  bool (*) (List_1_t3207740835 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m21135888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1114577815_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1114577815(__this, method) ((  bool (*) (List_1_t3207740835 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1114577815_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1246478748_gshared (List_1_t3207740835 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1246478748(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1246478748_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m570389749_gshared (List_1_t3207740835 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m570389749(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3207740835 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m570389749_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Add(T)
extern "C"  void List_1_Add_m3169189584_gshared (List_1_t3207740835 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3169189584(__this, ___item0, method) ((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_Add_m3169189584_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2676016271_gshared (List_1_t3207740835 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2676016271(__this, ___newCount0, method) ((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2676016271_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m3824339768_gshared (List_1_t3207740835 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m3824339768(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3207740835 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m3824339768_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2169221727_gshared (List_1_t3207740835 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2169221727(__this, ___collection0, method) ((  void (*) (List_1_t3207740835 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2169221727_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3005472383_gshared (List_1_t3207740835 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3005472383(__this, ___enumerable0, method) ((  void (*) (List_1_t3207740835 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3005472383_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3862576358_gshared (List_1_t3207740835 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3862576358(__this, ___collection0, method) ((  void (*) (List_1_t3207740835 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3862576358_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<DefendNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4024405395 * List_1_AsReadOnly_m1054551551_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1054551551(__this, method) ((  ReadOnlyCollection_1_t4024405395 * (*) (List_1_t3207740835 *, const MethodInfo*))List_1_AsReadOnly_m1054551551_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Clear()
extern "C"  void List_1_Clear_m4151531916_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_Clear_m4151531916(__this, method) ((  void (*) (List_1_t3207740835 *, const MethodInfo*))List_1_Clear_m4151531916_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m2377537218_gshared (List_1_t3207740835 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m2377537218(__this, ___item0, method) ((  bool (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_Contains_m2377537218_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m4027112895_gshared (List_1_t3207740835 * __this, DefendNavScreenU5BU5D_t1548979470* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m4027112895(__this, ___array0, method) ((  void (*) (List_1_t3207740835 *, DefendNavScreenU5BU5D_t1548979470*, const MethodInfo*))List_1_CopyTo_m4027112895_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m339631972_gshared (List_1_t3207740835 * __this, DefendNavScreenU5BU5D_t1548979470* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m339631972(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3207740835 *, DefendNavScreenU5BU5D_t1548979470*, int32_t, const MethodInfo*))List_1_CopyTo_m339631972_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m491423814_gshared (List_1_t3207740835 * __this, int32_t ___index0, DefendNavScreenU5BU5D_t1548979470* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m491423814(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t3207740835 *, int32_t, DefendNavScreenU5BU5D_t1548979470*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m491423814_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m4293449026_gshared (List_1_t3207740835 * __this, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_Exists_m4293449026(__this, ___match0, method) ((  bool (*) (List_1_t3207740835 *, Predicate_1_t2281589818 *, const MethodInfo*))List_1_Exists_m4293449026_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<DefendNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2461100292_gshared (List_1_t3207740835 * __this, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_Find_m2461100292(__this, ___match0, method) ((  int32_t (*) (List_1_t3207740835 *, Predicate_1_t2281589818 *, const MethodInfo*))List_1_Find_m2461100292_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1025479851_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1025479851(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2281589818 *, const MethodInfo*))List_1_CheckMatch_m1025479851_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<DefendNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3207740835 * List_1_FindAll_m1337835351_gshared (List_1_t3207740835 * __this, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m1337835351(__this, ___match0, method) ((  List_1_t3207740835 * (*) (List_1_t3207740835 *, Predicate_1_t2281589818 *, const MethodInfo*))List_1_FindAll_m1337835351_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<DefendNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3207740835 * List_1_FindAllStackBits_m1898545569_gshared (List_1_t3207740835 * __this, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1898545569(__this, ___match0, method) ((  List_1_t3207740835 * (*) (List_1_t3207740835 *, Predicate_1_t2281589818 *, const MethodInfo*))List_1_FindAllStackBits_m1898545569_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<DefendNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3207740835 * List_1_FindAllList_m2508174817_gshared (List_1_t3207740835 * __this, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2508174817(__this, ___match0, method) ((  List_1_t3207740835 * (*) (List_1_t3207740835 *, Predicate_1_t2281589818 *, const MethodInfo*))List_1_FindAllList_m2508174817_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m3775418979_gshared (List_1_t3207740835 * __this, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m3775418979(__this, ___match0, method) ((  int32_t (*) (List_1_t3207740835 *, Predicate_1_t2281589818 *, const MethodInfo*))List_1_FindIndex_m3775418979_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3358260820_gshared (List_1_t3207740835 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2281589818 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3358260820(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3207740835 *, int32_t, int32_t, Predicate_1_t2281589818 *, const MethodInfo*))List_1_GetIndex_m3358260820_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m2126824989_gshared (List_1_t3207740835 * __this, Action_1_t3640419085 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m2126824989(__this, ___action0, method) ((  void (*) (List_1_t3207740835 *, Action_1_t3640419085 *, const MethodInfo*))List_1_ForEach_m2126824989_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<DefendNavScreen>::GetEnumerator()
extern "C"  Enumerator_t2742470509  List_1_GetEnumerator_m799925263_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m799925263(__this, method) ((  Enumerator_t2742470509  (*) (List_1_t3207740835 *, const MethodInfo*))List_1_GetEnumerator_m799925263_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1599209914_gshared (List_1_t3207740835 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1599209914(__this, ___item0, method) ((  int32_t (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_IndexOf_m1599209914_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2138379567_gshared (List_1_t3207740835 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2138379567(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3207740835 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2138379567_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m810075876_gshared (List_1_t3207740835 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m810075876(__this, ___index0, method) ((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_CheckIndex_m810075876_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2787857689_gshared (List_1_t3207740835 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m2787857689(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3207740835 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2787857689_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1709943758_gshared (List_1_t3207740835 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1709943758(__this, ___collection0, method) ((  void (*) (List_1_t3207740835 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1709943758_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<DefendNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m149159809_gshared (List_1_t3207740835 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m149159809(__this, ___item0, method) ((  bool (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_Remove_m149159809_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2247855635_gshared (List_1_t3207740835 * __this, Predicate_1_t2281589818 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2247855635(__this, ___match0, method) ((  int32_t (*) (List_1_t3207740835 *, Predicate_1_t2281589818 *, const MethodInfo*))List_1_RemoveAll_m2247855635_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3545350405_gshared (List_1_t3207740835 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3545350405(__this, ___index0, method) ((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3545350405_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m905688744_gshared (List_1_t3207740835 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m905688744(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3207740835 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m905688744_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m579243271_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_Reverse_m579243271(__this, method) ((  void (*) (List_1_t3207740835 *, const MethodInfo*))List_1_Reverse_m579243271_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Sort()
extern "C"  void List_1_Sort_m929940281_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_Sort_m929940281(__this, method) ((  void (*) (List_1_t3207740835 *, const MethodInfo*))List_1_Sort_m929940281_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m678910645_gshared (List_1_t3207740835 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m678910645(__this, ___comparer0, method) ((  void (*) (List_1_t3207740835 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m678910645_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m267910204_gshared (List_1_t3207740835 * __this, Comparison_1_t805391258 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m267910204(__this, ___comparison0, method) ((  void (*) (List_1_t3207740835 *, Comparison_1_t805391258 *, const MethodInfo*))List_1_Sort_m267910204_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<DefendNavScreen>::ToArray()
extern "C"  DefendNavScreenU5BU5D_t1548979470* List_1_ToArray_m1287068612_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_ToArray_m1287068612(__this, method) ((  DefendNavScreenU5BU5D_t1548979470* (*) (List_1_t3207740835 *, const MethodInfo*))List_1_ToArray_m1287068612_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1952938790_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1952938790(__this, method) ((  void (*) (List_1_t3207740835 *, const MethodInfo*))List_1_TrimExcess_m1952938790_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1917782144_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1917782144(__this, method) ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))List_1_get_Capacity_m1917782144_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4293217855_gshared (List_1_t3207740835 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4293217855(__this, ___value0, method) ((  void (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4293217855_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<DefendNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m2700104499_gshared (List_1_t3207740835 * __this, const MethodInfo* method);
#define List_1_get_Count_m2700104499(__this, method) ((  int32_t (*) (List_1_t3207740835 *, const MethodInfo*))List_1_get_Count_m2700104499_gshared)(__this, method)
// T System.Collections.Generic.List`1<DefendNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1607829529_gshared (List_1_t3207740835 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1607829529(__this, ___index0, method) ((  int32_t (*) (List_1_t3207740835 *, int32_t, const MethodInfo*))List_1_get_Item_m1607829529_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<DefendNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3909203654_gshared (List_1_t3207740835 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3909203654(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3207740835 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3909203654_gshared)(__this, ___index0, ___value1, method)
