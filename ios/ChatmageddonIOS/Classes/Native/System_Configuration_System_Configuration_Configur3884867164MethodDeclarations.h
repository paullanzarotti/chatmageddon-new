﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationSaveEventArgs
struct ConfigurationSaveEventArgs_t3884867164;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Configuration.ConfigurationSaveEventArgs::.ctor(System.String,System.Boolean,System.Exception,System.Object)
extern "C"  void ConfigurationSaveEventArgs__ctor_m507911104 (ConfigurationSaveEventArgs_t3884867164 * __this, String_t* ___streamPath0, bool ___start1, Exception_t1927440687 * ___ex2, Il2CppObject * ___context3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationSaveEventArgs::get_StreamPath()
extern "C"  String_t* ConfigurationSaveEventArgs_get_StreamPath_m938463566 (ConfigurationSaveEventArgs_t3884867164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSaveEventArgs::set_StreamPath(System.String)
extern "C"  void ConfigurationSaveEventArgs_set_StreamPath_m450521949 (ConfigurationSaveEventArgs_t3884867164 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationSaveEventArgs::get_Start()
extern "C"  bool ConfigurationSaveEventArgs_get_Start_m2227512084 (ConfigurationSaveEventArgs_t3884867164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSaveEventArgs::set_Start(System.Boolean)
extern "C"  void ConfigurationSaveEventArgs_set_Start_m966930989 (ConfigurationSaveEventArgs_t3884867164 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ConfigurationSaveEventArgs::get_Context()
extern "C"  Il2CppObject * ConfigurationSaveEventArgs_get_Context_m444690816 (ConfigurationSaveEventArgs_t3884867164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSaveEventArgs::set_Context(System.Object)
extern "C"  void ConfigurationSaveEventArgs_set_Context_m4025393599 (ConfigurationSaveEventArgs_t3884867164 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ConfigurationSaveEventArgs::get_Failed()
extern "C"  bool ConfigurationSaveEventArgs_get_Failed_m3028077201 (ConfigurationSaveEventArgs_t3884867164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSaveEventArgs::set_Failed(System.Boolean)
extern "C"  void ConfigurationSaveEventArgs_set_Failed_m1367360390 (ConfigurationSaveEventArgs_t3884867164 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Configuration.ConfigurationSaveEventArgs::get_Exception()
extern "C"  Exception_t1927440687 * ConfigurationSaveEventArgs_get_Exception_m169505546 (ConfigurationSaveEventArgs_t3884867164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationSaveEventArgs::set_Exception(System.Exception)
extern "C"  void ConfigurationSaveEventArgs_set_Exception_m3069188265 (ConfigurationSaveEventArgs_t3884867164 * __this, Exception_t1927440687 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
