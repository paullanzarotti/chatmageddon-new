﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EmailShareButton
struct EmailShareButton_t2679829333;

#include "codegen/il2cpp-codegen.h"

// System.Void EmailShareButton::.ctor()
extern "C"  void EmailShareButton__ctor_m3946067094 (EmailShareButton_t2679829333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EmailShareButton::OnButtonClick()
extern "C"  void EmailShareButton_OnButtonClick_m4067772829 (EmailShareButton_t2679829333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
