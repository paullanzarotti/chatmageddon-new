﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,AttackSearchNav>
struct NavigationController_2_t2145786393;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,AttackSearchNav>::.ctor()
extern "C"  void NavigationController_2__ctor_m2534835378_gshared (NavigationController_2_t2145786393 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m2534835378(__this, method) ((  void (*) (NavigationController_2_t2145786393 *, const MethodInfo*))NavigationController_2__ctor_m2534835378_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,AttackSearchNav>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m3679967428_gshared (NavigationController_2_t2145786393 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m3679967428(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t2145786393 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m3679967428_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,AttackSearchNav>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3208631707_gshared (NavigationController_2_t2145786393 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m3208631707(__this, method) ((  void (*) (NavigationController_2_t2145786393 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3208631707_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,AttackSearchNav>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m346844242_gshared (NavigationController_2_t2145786393 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m346844242(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t2145786393 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m346844242_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,AttackSearchNav>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m1184525474_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m1184525474(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t2145786393 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m1184525474_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,AttackSearchNav>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m579444669_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m579444669(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t2145786393 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m579444669_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,AttackSearchNav>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1577172356_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m1577172356(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t2145786393 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1577172356_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,AttackSearchNav>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m1887059318_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m1887059318(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t2145786393 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m1887059318_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,AttackSearchNav>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m898731946_gshared (NavigationController_2_t2145786393 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m898731946(__this, ___screen0, method) ((  void (*) (NavigationController_2_t2145786393 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m898731946_gshared)(__this, ___screen0, method)
