﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<RemoveExistingFriend>c__AnonStorey13<System.Object>
struct U3CRemoveExistingFriendU3Ec__AnonStorey13_t1193638296;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<RemoveExistingFriend>c__AnonStorey13<System.Object>::.ctor()
extern "C"  void U3CRemoveExistingFriendU3Ec__AnonStorey13__ctor_m1087726813_gshared (U3CRemoveExistingFriendU3Ec__AnonStorey13_t1193638296 * __this, const MethodInfo* method);
#define U3CRemoveExistingFriendU3Ec__AnonStorey13__ctor_m1087726813(__this, method) ((  void (*) (U3CRemoveExistingFriendU3Ec__AnonStorey13_t1193638296 *, const MethodInfo*))U3CRemoveExistingFriendU3Ec__AnonStorey13__ctor_m1087726813_gshared)(__this, method)
// System.Void BaseServer`1/<RemoveExistingFriend>c__AnonStorey13<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRemoveExistingFriendU3Ec__AnonStorey13_U3CU3Em__0_m197485196_gshared (U3CRemoveExistingFriendU3Ec__AnonStorey13_t1193638296 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRemoveExistingFriendU3Ec__AnonStorey13_U3CU3Em__0_m197485196(__this, ___request0, ___response1, method) ((  void (*) (U3CRemoveExistingFriendU3Ec__AnonStorey13_t1193638296 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRemoveExistingFriendU3Ec__AnonStorey13_U3CU3Em__0_m197485196_gshared)(__this, ___request0, ___response1, method)
