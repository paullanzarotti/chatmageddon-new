﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// PhoneNumbers.AreaCodeMapStorageStrategy
struct AreaCodeMapStorageStrategy_t3709015996;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.AreaCodeMap
struct  AreaCodeMap_t3230759372  : public Il2CppObject
{
public:
	// PhoneNumbers.PhoneNumberUtil PhoneNumbers.AreaCodeMap::phoneUtil
	PhoneNumberUtil_t4155573397 * ___phoneUtil_0;
	// PhoneNumbers.AreaCodeMapStorageStrategy PhoneNumbers.AreaCodeMap::areaCodeMapStorage
	AreaCodeMapStorageStrategy_t3709015996 * ___areaCodeMapStorage_1;

public:
	inline static int32_t get_offset_of_phoneUtil_0() { return static_cast<int32_t>(offsetof(AreaCodeMap_t3230759372, ___phoneUtil_0)); }
	inline PhoneNumberUtil_t4155573397 * get_phoneUtil_0() const { return ___phoneUtil_0; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_phoneUtil_0() { return &___phoneUtil_0; }
	inline void set_phoneUtil_0(PhoneNumberUtil_t4155573397 * value)
	{
		___phoneUtil_0 = value;
		Il2CppCodeGenWriteBarrier(&___phoneUtil_0, value);
	}

	inline static int32_t get_offset_of_areaCodeMapStorage_1() { return static_cast<int32_t>(offsetof(AreaCodeMap_t3230759372, ___areaCodeMapStorage_1)); }
	inline AreaCodeMapStorageStrategy_t3709015996 * get_areaCodeMapStorage_1() const { return ___areaCodeMapStorage_1; }
	inline AreaCodeMapStorageStrategy_t3709015996 ** get_address_of_areaCodeMapStorage_1() { return &___areaCodeMapStorage_1; }
	inline void set_areaCodeMapStorage_1(AreaCodeMapStorageStrategy_t3709015996 * value)
	{
		___areaCodeMapStorage_1 = value;
		Il2CppCodeGenWriteBarrier(&___areaCodeMapStorage_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
