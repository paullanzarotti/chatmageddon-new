﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InventoryItemButton
struct InventoryItemButton_t3515382031;

#include "codegen/il2cpp-codegen.h"

// System.Void InventoryItemButton::.ctor()
extern "C"  void InventoryItemButton__ctor_m2263505934 (InventoryItemButton_t3515382031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryItemButton::OnButtonReset()
extern "C"  void InventoryItemButton_OnButtonReset_m2692204282 (InventoryItemButton_t3515382031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InventoryItemButton::OnButtonRotate()
extern "C"  void InventoryItemButton_OnButtonRotate_m2664780202 (InventoryItemButton_t3515382031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
