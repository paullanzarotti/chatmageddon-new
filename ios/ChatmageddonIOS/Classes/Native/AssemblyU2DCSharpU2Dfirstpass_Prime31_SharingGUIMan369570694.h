﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action
struct Action_t3226471752;

#include "P31RestKit_Prime31_MonoBehaviourGUI1135617291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.SharingGUIManager
struct  SharingGUIManager_t369570694  : public MonoBehaviourGUI_t1135617291
{
public:

public:
};

struct SharingGUIManager_t369570694_StaticFields
{
public:
	// System.String Prime31.SharingGUIManager::screenshotFilename
	String_t* ___screenshotFilename_18;
	// System.Action`1<System.String> Prime31.SharingGUIManager::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_19;
	// System.Action Prime31.SharingGUIManager::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_20;

public:
	inline static int32_t get_offset_of_screenshotFilename_18() { return static_cast<int32_t>(offsetof(SharingGUIManager_t369570694_StaticFields, ___screenshotFilename_18)); }
	inline String_t* get_screenshotFilename_18() const { return ___screenshotFilename_18; }
	inline String_t** get_address_of_screenshotFilename_18() { return &___screenshotFilename_18; }
	inline void set_screenshotFilename_18(String_t* value)
	{
		___screenshotFilename_18 = value;
		Il2CppCodeGenWriteBarrier(&___screenshotFilename_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_19() { return static_cast<int32_t>(offsetof(SharingGUIManager_t369570694_StaticFields, ___U3CU3Ef__amU24cache0_19)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_19() const { return ___U3CU3Ef__amU24cache0_19; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_19() { return &___U3CU3Ef__amU24cache0_19; }
	inline void set_U3CU3Ef__amU24cache0_19(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_20() { return static_cast<int32_t>(offsetof(SharingGUIManager_t369570694_StaticFields, ___U3CU3Ef__amU24cache1_20)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_20() const { return ___U3CU3Ef__amU24cache1_20; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_20() { return &___U3CU3Ef__amU24cache1_20; }
	inline void set_U3CU3Ef__amU24cache1_20(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
