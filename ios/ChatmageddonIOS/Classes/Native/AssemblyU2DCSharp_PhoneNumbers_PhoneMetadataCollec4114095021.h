﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneMetadataCollection
struct PhoneMetadataCollection_t4114095021;
// System.Collections.Generic.List`1<PhoneNumbers.PhoneMetadata>
struct List_1_t4030949831;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneMetadataCollection
struct  PhoneMetadataCollection_t4114095021  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<PhoneNumbers.PhoneMetadata> PhoneNumbers.PhoneMetadataCollection::metadata_
	List_1_t4030949831 * ___metadata__2;

public:
	inline static int32_t get_offset_of_metadata__2() { return static_cast<int32_t>(offsetof(PhoneMetadataCollection_t4114095021, ___metadata__2)); }
	inline List_1_t4030949831 * get_metadata__2() const { return ___metadata__2; }
	inline List_1_t4030949831 ** get_address_of_metadata__2() { return &___metadata__2; }
	inline void set_metadata__2(List_1_t4030949831 * value)
	{
		___metadata__2 = value;
		Il2CppCodeGenWriteBarrier(&___metadata__2, value);
	}
};

struct PhoneMetadataCollection_t4114095021_StaticFields
{
public:
	// PhoneNumbers.PhoneMetadataCollection PhoneNumbers.PhoneMetadataCollection::defaultInstance
	PhoneMetadataCollection_t4114095021 * ___defaultInstance_0;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(PhoneMetadataCollection_t4114095021_StaticFields, ___defaultInstance_0)); }
	inline PhoneMetadataCollection_t4114095021 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline PhoneMetadataCollection_t4114095021 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(PhoneMetadataCollection_t4114095021 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
