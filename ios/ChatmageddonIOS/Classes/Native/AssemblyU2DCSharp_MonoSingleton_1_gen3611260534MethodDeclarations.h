﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<SettingsSceneManager>::.ctor()
#define MonoSingleton_1__ctor_m3678154042(__this, method) ((  void (*) (MonoSingleton_1_t3611260534 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<SettingsSceneManager>::Awake()
#define MonoSingleton_1_Awake_m3864305465(__this, method) ((  void (*) (MonoSingleton_1_t3611260534 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<SettingsSceneManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m582986279(__this /* static, unused */, method) ((  SettingsSceneManager_t3860594814 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<SettingsSceneManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4098546763(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<SettingsSceneManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1468077240(__this, method) ((  void (*) (MonoSingleton_1_t3611260534 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<SettingsSceneManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1321949124(__this, method) ((  void (*) (MonoSingleton_1_t3611260534 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<SettingsSceneManager>::.cctor()
#define MonoSingleton_1__cctor_m3960316187(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
