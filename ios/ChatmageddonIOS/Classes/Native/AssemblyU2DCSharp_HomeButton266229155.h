﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenScale
struct TweenScale_t2697902175;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_Scene4200804392.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeButton
struct  HomeButton_t266229155  : public SFXButton_t792651341
{
public:
	// Scene HomeButton::loadingFromScene
	int32_t ___loadingFromScene_5;
	// TweenScale HomeButton::dailyTween
	TweenScale_t2697902175 * ___dailyTween_6;
	// TweenScale HomeButton::allTimeTween
	TweenScale_t2697902175 * ___allTimeTween_7;

public:
	inline static int32_t get_offset_of_loadingFromScene_5() { return static_cast<int32_t>(offsetof(HomeButton_t266229155, ___loadingFromScene_5)); }
	inline int32_t get_loadingFromScene_5() const { return ___loadingFromScene_5; }
	inline int32_t* get_address_of_loadingFromScene_5() { return &___loadingFromScene_5; }
	inline void set_loadingFromScene_5(int32_t value)
	{
		___loadingFromScene_5 = value;
	}

	inline static int32_t get_offset_of_dailyTween_6() { return static_cast<int32_t>(offsetof(HomeButton_t266229155, ___dailyTween_6)); }
	inline TweenScale_t2697902175 * get_dailyTween_6() const { return ___dailyTween_6; }
	inline TweenScale_t2697902175 ** get_address_of_dailyTween_6() { return &___dailyTween_6; }
	inline void set_dailyTween_6(TweenScale_t2697902175 * value)
	{
		___dailyTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___dailyTween_6, value);
	}

	inline static int32_t get_offset_of_allTimeTween_7() { return static_cast<int32_t>(offsetof(HomeButton_t266229155, ___allTimeTween_7)); }
	inline TweenScale_t2697902175 * get_allTimeTween_7() const { return ___allTimeTween_7; }
	inline TweenScale_t2697902175 ** get_address_of_allTimeTween_7() { return &___allTimeTween_7; }
	inline void set_allTimeTween_7(TweenScale_t2697902175 * value)
	{
		___allTimeTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___allTimeTween_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
