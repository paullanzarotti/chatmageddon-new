﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FAQILP
struct FAQILP_t1146604377;
// FAQ
struct FAQ_t2837997648;
// UILabel
struct UILabel_t1795115428;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen2536695735.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FAQListItem
struct  FAQListItem_t1761156371  : public BaseInfiniteListItem_1_t2536695735
{
public:
	// FAQILP FAQListItem::listPopulator
	FAQILP_t1146604377 * ___listPopulator_10;
	// FAQ FAQListItem::faqItem
	FAQ_t2837997648 * ___faqItem_11;
	// UILabel FAQListItem::questionLabel
	UILabel_t1795115428 * ___questionLabel_12;
	// UIScrollView FAQListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_13;
	// UIScrollView FAQListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_14;
	// UnityEngine.Transform FAQListItem::mTrans
	Transform_t3275118058 * ___mTrans_15;
	// UIScrollView FAQListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_16;
	// System.Boolean FAQListItem::mAutoFind
	bool ___mAutoFind_17;
	// System.Boolean FAQListItem::mStarted
	bool ___mStarted_18;

public:
	inline static int32_t get_offset_of_listPopulator_10() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___listPopulator_10)); }
	inline FAQILP_t1146604377 * get_listPopulator_10() const { return ___listPopulator_10; }
	inline FAQILP_t1146604377 ** get_address_of_listPopulator_10() { return &___listPopulator_10; }
	inline void set_listPopulator_10(FAQILP_t1146604377 * value)
	{
		___listPopulator_10 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_10, value);
	}

	inline static int32_t get_offset_of_faqItem_11() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___faqItem_11)); }
	inline FAQ_t2837997648 * get_faqItem_11() const { return ___faqItem_11; }
	inline FAQ_t2837997648 ** get_address_of_faqItem_11() { return &___faqItem_11; }
	inline void set_faqItem_11(FAQ_t2837997648 * value)
	{
		___faqItem_11 = value;
		Il2CppCodeGenWriteBarrier(&___faqItem_11, value);
	}

	inline static int32_t get_offset_of_questionLabel_12() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___questionLabel_12)); }
	inline UILabel_t1795115428 * get_questionLabel_12() const { return ___questionLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_questionLabel_12() { return &___questionLabel_12; }
	inline void set_questionLabel_12(UILabel_t1795115428 * value)
	{
		___questionLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___questionLabel_12, value);
	}

	inline static int32_t get_offset_of_scrollView_13() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___scrollView_13)); }
	inline UIScrollView_t3033954930 * get_scrollView_13() const { return ___scrollView_13; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_13() { return &___scrollView_13; }
	inline void set_scrollView_13(UIScrollView_t3033954930 * value)
	{
		___scrollView_13 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_13, value);
	}

	inline static int32_t get_offset_of_draggablePanel_14() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___draggablePanel_14)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_14() const { return ___draggablePanel_14; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_14() { return &___draggablePanel_14; }
	inline void set_draggablePanel_14(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_14 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_14, value);
	}

	inline static int32_t get_offset_of_mTrans_15() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___mTrans_15)); }
	inline Transform_t3275118058 * get_mTrans_15() const { return ___mTrans_15; }
	inline Transform_t3275118058 ** get_address_of_mTrans_15() { return &___mTrans_15; }
	inline void set_mTrans_15(Transform_t3275118058 * value)
	{
		___mTrans_15 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_15, value);
	}

	inline static int32_t get_offset_of_mScroll_16() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___mScroll_16)); }
	inline UIScrollView_t3033954930 * get_mScroll_16() const { return ___mScroll_16; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_16() { return &___mScroll_16; }
	inline void set_mScroll_16(UIScrollView_t3033954930 * value)
	{
		___mScroll_16 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_16, value);
	}

	inline static int32_t get_offset_of_mAutoFind_17() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___mAutoFind_17)); }
	inline bool get_mAutoFind_17() const { return ___mAutoFind_17; }
	inline bool* get_address_of_mAutoFind_17() { return &___mAutoFind_17; }
	inline void set_mAutoFind_17(bool value)
	{
		___mAutoFind_17 = value;
	}

	inline static int32_t get_offset_of_mStarted_18() { return static_cast<int32_t>(offsetof(FAQListItem_t1761156371, ___mStarted_18)); }
	inline bool get_mStarted_18() const { return ___mStarted_18; }
	inline bool* get_address_of_mStarted_18() { return &___mStarted_18; }
	inline void set_mStarted_18(bool value)
	{
		___mStarted_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
