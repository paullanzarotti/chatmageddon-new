﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsOSMTag
struct OnlineMapsOSMTag_t3629071465;
// OnlineMapsOSMBase/<HasTags>c__AnonStorey4
struct U3CHasTagsU3Ec__AnonStorey4_t2665984195;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsOSMBase/<HasTags>c__AnonStorey4/<HasTags>c__AnonStorey5
struct  U3CHasTagsU3Ec__AnonStorey5_t516336015  : public Il2CppObject
{
public:
	// OnlineMapsOSMTag OnlineMapsOSMBase/<HasTags>c__AnonStorey4/<HasTags>c__AnonStorey5::tag
	OnlineMapsOSMTag_t3629071465 * ___tag_0;
	// OnlineMapsOSMBase/<HasTags>c__AnonStorey4 OnlineMapsOSMBase/<HasTags>c__AnonStorey4/<HasTags>c__AnonStorey5::<>f__ref$4
	U3CHasTagsU3Ec__AnonStorey4_t2665984195 * ___U3CU3Ef__refU244_1;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(U3CHasTagsU3Ec__AnonStorey5_t516336015, ___tag_0)); }
	inline OnlineMapsOSMTag_t3629071465 * get_tag_0() const { return ___tag_0; }
	inline OnlineMapsOSMTag_t3629071465 ** get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(OnlineMapsOSMTag_t3629071465 * value)
	{
		___tag_0 = value;
		Il2CppCodeGenWriteBarrier(&___tag_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_1() { return static_cast<int32_t>(offsetof(U3CHasTagsU3Ec__AnonStorey5_t516336015, ___U3CU3Ef__refU244_1)); }
	inline U3CHasTagsU3Ec__AnonStorey4_t2665984195 * get_U3CU3Ef__refU244_1() const { return ___U3CU3Ef__refU244_1; }
	inline U3CHasTagsU3Ec__AnonStorey4_t2665984195 ** get_address_of_U3CU3Ef__refU244_1() { return &___U3CU3Ef__refU244_1; }
	inline void set_U3CU3Ef__refU244_1(U3CHasTagsU3Ec__AnonStorey4_t2665984195 * value)
	{
		___U3CU3Ef__refU244_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU244_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
