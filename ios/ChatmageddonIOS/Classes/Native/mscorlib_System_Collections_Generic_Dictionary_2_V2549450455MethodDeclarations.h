﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>
struct ValueCollection_t2549450455;
// System.Collections.Generic.Dictionary`2<PanelType,System.Object>
struct Dictionary_2_t3846390612;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1237956080.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2130100390_gshared (ValueCollection_t2549450455 * __this, Dictionary_2_t3846390612 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2130100390(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2549450455 *, Dictionary_2_t3846390612 *, const MethodInfo*))ValueCollection__ctor_m2130100390_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3438430544_gshared (ValueCollection_t2549450455 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3438430544(__this, ___item0, method) ((  void (*) (ValueCollection_t2549450455 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3438430544_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1058040647_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1058040647(__this, method) ((  void (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1058040647_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m694834480_gshared (ValueCollection_t2549450455 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m694834480(__this, ___item0, method) ((  bool (*) (ValueCollection_t2549450455 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m694834480_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m979932759_gshared (ValueCollection_t2549450455 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m979932759(__this, ___item0, method) ((  bool (*) (ValueCollection_t2549450455 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m979932759_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1918810493_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1918810493(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1918810493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2215146529_gshared (ValueCollection_t2549450455 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2215146529(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2549450455 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2215146529_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m908029848_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m908029848(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m908029848_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3483533139_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3483533139(__this, method) ((  bool (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3483533139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3925226765_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3925226765(__this, method) ((  bool (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3925226765_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3186168593_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3186168593(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3186168593_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1621950423_gshared (ValueCollection_t2549450455 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1621950423(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2549450455 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1621950423_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1237956080  ValueCollection_GetEnumerator_m271368956_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m271368956(__this, method) ((  Enumerator_t1237956080  (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_GetEnumerator_m271368956_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m250972581_gshared (ValueCollection_t2549450455 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m250972581(__this, method) ((  int32_t (*) (ValueCollection_t2549450455 *, const MethodInfo*))ValueCollection_get_Count_m250972581_gshared)(__this, method)
