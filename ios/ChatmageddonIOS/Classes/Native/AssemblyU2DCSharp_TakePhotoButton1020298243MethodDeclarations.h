﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TakePhotoButton
struct TakePhotoButton_t1020298243;

#include "codegen/il2cpp-codegen.h"

// System.Void TakePhotoButton::.ctor()
extern "C"  void TakePhotoButton__ctor_m2842076540 (TakePhotoButton_t1020298243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TakePhotoButton::OnButtonClick()
extern "C"  void TakePhotoButton_OnButtonClick_m2890459899 (TakePhotoButton_t1020298243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
