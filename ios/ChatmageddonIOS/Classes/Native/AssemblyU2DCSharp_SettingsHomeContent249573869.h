﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AvatarUpdater
struct AvatarUpdater_t2404165026;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsHomeContent
struct  SettingsHomeContent_t249573869  : public MonoBehaviour_t1158329972
{
public:
	// AvatarUpdater SettingsHomeContent::avatar
	AvatarUpdater_t2404165026 * ___avatar_2;
	// UILabel SettingsHomeContent::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_3;
	// UILabel SettingsHomeContent::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_4;
	// UILabel SettingsHomeContent::userNameLabel
	UILabel_t1795115428 * ___userNameLabel_5;
	// UILabel SettingsHomeContent::SMStatusLabel
	UILabel_t1795115428 * ___SMStatusLabel_6;
	// UILabel SettingsHomeContent::SMTimeLabel
	UILabel_t1795115428 * ___SMTimeLabel_7;
	// UILabel SettingsHomeContent::SMDurationLabel
	UILabel_t1795115428 * ___SMDurationLabel_8;

public:
	inline static int32_t get_offset_of_avatar_2() { return static_cast<int32_t>(offsetof(SettingsHomeContent_t249573869, ___avatar_2)); }
	inline AvatarUpdater_t2404165026 * get_avatar_2() const { return ___avatar_2; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_2() { return &___avatar_2; }
	inline void set_avatar_2(AvatarUpdater_t2404165026 * value)
	{
		___avatar_2 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_2, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_3() { return static_cast<int32_t>(offsetof(SettingsHomeContent_t249573869, ___firstNameLabel_3)); }
	inline UILabel_t1795115428 * get_firstNameLabel_3() const { return ___firstNameLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_3() { return &___firstNameLabel_3; }
	inline void set_firstNameLabel_3(UILabel_t1795115428 * value)
	{
		___firstNameLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_3, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_4() { return static_cast<int32_t>(offsetof(SettingsHomeContent_t249573869, ___lastNameLabel_4)); }
	inline UILabel_t1795115428 * get_lastNameLabel_4() const { return ___lastNameLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_4() { return &___lastNameLabel_4; }
	inline void set_lastNameLabel_4(UILabel_t1795115428 * value)
	{
		___lastNameLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_4, value);
	}

	inline static int32_t get_offset_of_userNameLabel_5() { return static_cast<int32_t>(offsetof(SettingsHomeContent_t249573869, ___userNameLabel_5)); }
	inline UILabel_t1795115428 * get_userNameLabel_5() const { return ___userNameLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_userNameLabel_5() { return &___userNameLabel_5; }
	inline void set_userNameLabel_5(UILabel_t1795115428 * value)
	{
		___userNameLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___userNameLabel_5, value);
	}

	inline static int32_t get_offset_of_SMStatusLabel_6() { return static_cast<int32_t>(offsetof(SettingsHomeContent_t249573869, ___SMStatusLabel_6)); }
	inline UILabel_t1795115428 * get_SMStatusLabel_6() const { return ___SMStatusLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_SMStatusLabel_6() { return &___SMStatusLabel_6; }
	inline void set_SMStatusLabel_6(UILabel_t1795115428 * value)
	{
		___SMStatusLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___SMStatusLabel_6, value);
	}

	inline static int32_t get_offset_of_SMTimeLabel_7() { return static_cast<int32_t>(offsetof(SettingsHomeContent_t249573869, ___SMTimeLabel_7)); }
	inline UILabel_t1795115428 * get_SMTimeLabel_7() const { return ___SMTimeLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_SMTimeLabel_7() { return &___SMTimeLabel_7; }
	inline void set_SMTimeLabel_7(UILabel_t1795115428 * value)
	{
		___SMTimeLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___SMTimeLabel_7, value);
	}

	inline static int32_t get_offset_of_SMDurationLabel_8() { return static_cast<int32_t>(offsetof(SettingsHomeContent_t249573869, ___SMDurationLabel_8)); }
	inline UILabel_t1795115428 * get_SMDurationLabel_8() const { return ___SMDurationLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_SMDurationLabel_8() { return &___SMDurationLabel_8; }
	inline void set_SMDurationLabel_8(UILabel_t1795115428 * value)
	{
		___SMDurationLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___SMDurationLabel_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
