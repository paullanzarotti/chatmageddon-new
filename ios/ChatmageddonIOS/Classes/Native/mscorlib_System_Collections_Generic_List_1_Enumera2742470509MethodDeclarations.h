﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<DefendNavScreen>
struct List_1_t3207740835;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2742470509.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DefendNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m4234720072_gshared (Enumerator_t2742470509 * __this, List_1_t3207740835 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m4234720072(__this, ___l0, method) ((  void (*) (Enumerator_t2742470509 *, List_1_t3207740835 *, const MethodInfo*))Enumerator__ctor_m4234720072_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DefendNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m450729402_gshared (Enumerator_t2742470509 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m450729402(__this, method) ((  void (*) (Enumerator_t2742470509 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m450729402_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DefendNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m655200750_gshared (Enumerator_t2742470509 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m655200750(__this, method) ((  Il2CppObject * (*) (Enumerator_t2742470509 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m655200750_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DefendNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m2905088299_gshared (Enumerator_t2742470509 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2905088299(__this, method) ((  void (*) (Enumerator_t2742470509 *, const MethodInfo*))Enumerator_Dispose_m2905088299_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DefendNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m916339426_gshared (Enumerator_t2742470509 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m916339426(__this, method) ((  void (*) (Enumerator_t2742470509 *, const MethodInfo*))Enumerator_VerifyState_m916339426_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DefendNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1879471642_gshared (Enumerator_t2742470509 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1879471642(__this, method) ((  bool (*) (Enumerator_t2742470509 *, const MethodInfo*))Enumerator_MoveNext_m1879471642_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DefendNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m294790739_gshared (Enumerator_t2742470509 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m294790739(__this, method) ((  int32_t (*) (Enumerator_t2742470509 *, const MethodInfo*))Enumerator_get_Current_m294790739_gshared)(__this, method)
