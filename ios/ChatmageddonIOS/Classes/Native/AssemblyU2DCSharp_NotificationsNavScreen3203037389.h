﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IGNotificationsSwitch
struct IGNotificationsSwitch_t111074964;
// IGNotificationSoundSwitch
struct IGNotificationSoundSwitch_t1960044298;
// IGNotificationVibrationSwitch
struct IGNotificationVibrationSwitch_t507453717;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationsNavScreen
struct  NotificationsNavScreen_t3203037389  : public NavigationScreen_t2333230110
{
public:
	// IGNotificationsSwitch NotificationsNavScreen::notificationSwitch
	IGNotificationsSwitch_t111074964 * ___notificationSwitch_3;
	// IGNotificationSoundSwitch NotificationsNavScreen::soundSwitch
	IGNotificationSoundSwitch_t1960044298 * ___soundSwitch_4;
	// IGNotificationVibrationSwitch NotificationsNavScreen::vibrationSwitch
	IGNotificationVibrationSwitch_t507453717 * ___vibrationSwitch_5;
	// System.Boolean NotificationsNavScreen::UIclosing
	bool ___UIclosing_6;

public:
	inline static int32_t get_offset_of_notificationSwitch_3() { return static_cast<int32_t>(offsetof(NotificationsNavScreen_t3203037389, ___notificationSwitch_3)); }
	inline IGNotificationsSwitch_t111074964 * get_notificationSwitch_3() const { return ___notificationSwitch_3; }
	inline IGNotificationsSwitch_t111074964 ** get_address_of_notificationSwitch_3() { return &___notificationSwitch_3; }
	inline void set_notificationSwitch_3(IGNotificationsSwitch_t111074964 * value)
	{
		___notificationSwitch_3 = value;
		Il2CppCodeGenWriteBarrier(&___notificationSwitch_3, value);
	}

	inline static int32_t get_offset_of_soundSwitch_4() { return static_cast<int32_t>(offsetof(NotificationsNavScreen_t3203037389, ___soundSwitch_4)); }
	inline IGNotificationSoundSwitch_t1960044298 * get_soundSwitch_4() const { return ___soundSwitch_4; }
	inline IGNotificationSoundSwitch_t1960044298 ** get_address_of_soundSwitch_4() { return &___soundSwitch_4; }
	inline void set_soundSwitch_4(IGNotificationSoundSwitch_t1960044298 * value)
	{
		___soundSwitch_4 = value;
		Il2CppCodeGenWriteBarrier(&___soundSwitch_4, value);
	}

	inline static int32_t get_offset_of_vibrationSwitch_5() { return static_cast<int32_t>(offsetof(NotificationsNavScreen_t3203037389, ___vibrationSwitch_5)); }
	inline IGNotificationVibrationSwitch_t507453717 * get_vibrationSwitch_5() const { return ___vibrationSwitch_5; }
	inline IGNotificationVibrationSwitch_t507453717 ** get_address_of_vibrationSwitch_5() { return &___vibrationSwitch_5; }
	inline void set_vibrationSwitch_5(IGNotificationVibrationSwitch_t507453717 * value)
	{
		___vibrationSwitch_5 = value;
		Il2CppCodeGenWriteBarrier(&___vibrationSwitch_5, value);
	}

	inline static int32_t get_offset_of_UIclosing_6() { return static_cast<int32_t>(offsetof(NotificationsNavScreen_t3203037389, ___UIclosing_6)); }
	inline bool get_UIclosing_6() const { return ___UIclosing_6; }
	inline bool* get_address_of_UIclosing_6() { return &___UIclosing_6; }
	inline void set_UIclosing_6(bool value)
	{
		___UIclosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
