﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Prime31.FacebookFriend>
struct List_1_t3234295976;
// Prime31.FacebookResultsPaging
struct FacebookResultsPaging_t3277319338;

#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookBaseDT79607460.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookFriendsResult
struct  FacebookFriendsResult_t459691178  : public FacebookBaseDTO_t79607460
{
public:
	// System.Collections.Generic.List`1<Prime31.FacebookFriend> Prime31.FacebookFriendsResult::data
	List_1_t3234295976 * ___data_0;
	// Prime31.FacebookResultsPaging Prime31.FacebookFriendsResult::paging
	FacebookResultsPaging_t3277319338 * ___paging_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(FacebookFriendsResult_t459691178, ___data_0)); }
	inline List_1_t3234295976 * get_data_0() const { return ___data_0; }
	inline List_1_t3234295976 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(List_1_t3234295976 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of_paging_1() { return static_cast<int32_t>(offsetof(FacebookFriendsResult_t459691178, ___paging_1)); }
	inline FacebookResultsPaging_t3277319338 * get_paging_1() const { return ___paging_1; }
	inline FacebookResultsPaging_t3277319338 ** get_address_of_paging_1() { return &___paging_1; }
	inline void set_paging_1(FacebookResultsPaging_t3277319338 * value)
	{
		___paging_1 = value;
		Il2CppCodeGenWriteBarrier(&___paging_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
