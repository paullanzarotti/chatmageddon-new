﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OtherFriendListItemUIScaler
struct  OtherFriendListItemUIScaler_t173824305  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite OtherFriendListItemUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider OtherFriendListItemUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UnityEngine.Transform OtherFriendListItemUIScaler::firstNameLabel
	Transform_t3275118058 * ___firstNameLabel_16;
	// UnityEngine.Transform OtherFriendListItemUIScaler::inviteButton
	Transform_t3275118058 * ___inviteButton_17;
	// UISprite OtherFriendListItemUIScaler::divider
	UISprite_t603616735 * ___divider_18;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(OtherFriendListItemUIScaler_t173824305, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(OtherFriendListItemUIScaler_t173824305, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_16() { return static_cast<int32_t>(offsetof(OtherFriendListItemUIScaler_t173824305, ___firstNameLabel_16)); }
	inline Transform_t3275118058 * get_firstNameLabel_16() const { return ___firstNameLabel_16; }
	inline Transform_t3275118058 ** get_address_of_firstNameLabel_16() { return &___firstNameLabel_16; }
	inline void set_firstNameLabel_16(Transform_t3275118058 * value)
	{
		___firstNameLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_16, value);
	}

	inline static int32_t get_offset_of_inviteButton_17() { return static_cast<int32_t>(offsetof(OtherFriendListItemUIScaler_t173824305, ___inviteButton_17)); }
	inline Transform_t3275118058 * get_inviteButton_17() const { return ___inviteButton_17; }
	inline Transform_t3275118058 ** get_address_of_inviteButton_17() { return &___inviteButton_17; }
	inline void set_inviteButton_17(Transform_t3275118058 * value)
	{
		___inviteButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___inviteButton_17, value);
	}

	inline static int32_t get_offset_of_divider_18() { return static_cast<int32_t>(offsetof(OtherFriendListItemUIScaler_t173824305, ___divider_18)); }
	inline UISprite_t603616735 * get_divider_18() const { return ___divider_18; }
	inline UISprite_t603616735 ** get_address_of_divider_18() { return &___divider_18; }
	inline void set_divider_18(UISprite_t603616735 * value)
	{
		___divider_18 = value;
		Il2CppCodeGenWriteBarrier(&___divider_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
