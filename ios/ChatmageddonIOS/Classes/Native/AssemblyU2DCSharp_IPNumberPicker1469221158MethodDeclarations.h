﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPNumberPicker
struct IPNumberPicker_t1469221158;

#include "codegen/il2cpp-codegen.h"

// System.Void IPNumberPicker::.ctor()
extern "C"  void IPNumberPicker__ctor_m2618681595 (IPNumberPicker_t1469221158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPNumberPicker::get_CurrentValue()
extern "C"  int32_t IPNumberPicker_get_CurrentValue_m3978886024 (IPNumberPicker_t1469221158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPNumberPicker::set_CurrentValue(System.Int32)
extern "C"  void IPNumberPicker_set_CurrentValue_m678533697 (IPNumberPicker_t1469221158 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPNumberPicker::ResetPickerAtValue(System.Int32)
extern "C"  void IPNumberPicker_ResetPickerAtValue_m2579121175 (IPNumberPicker_t1469221158 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPNumberPicker::GetInitIndex()
extern "C"  int32_t IPNumberPicker_GetInitIndex_m3874659507 (IPNumberPicker_t1469221158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPNumberPicker::UpdateCurrentValue()
extern "C"  void IPNumberPicker_UpdateCurrentValue_m416288430 (IPNumberPicker_t1469221158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPNumberPicker::UpdateWidget(System.Int32,System.Int32)
extern "C"  void IPNumberPicker_UpdateWidget_m3194201292 (IPNumberPicker_t1469221158 * __this, int32_t ___widgetIndex0, int32_t ___contentIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPNumberPicker::UpdateVirtualElementsCount()
extern "C"  void IPNumberPicker_UpdateVirtualElementsCount_m1700537749 (IPNumberPicker_t1469221158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPNumberPicker::VirtualIndexToValue(System.Int32)
extern "C"  int32_t IPNumberPicker_VirtualIndexToValue_m1438926071 (IPNumberPicker_t1469221158 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPNumberPicker::ValueToIndex(System.Int32)
extern "C"  int32_t IPNumberPicker_ValueToIndex_m3356069822 (IPNumberPicker_t1469221158 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
