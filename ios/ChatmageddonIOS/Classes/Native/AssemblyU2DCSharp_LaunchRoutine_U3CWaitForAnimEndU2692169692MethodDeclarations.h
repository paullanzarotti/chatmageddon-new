﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchRoutine/<WaitForAnimEnd>c__Iterator3
struct U3CWaitForAnimEndU3Ec__Iterator3_t2692169692;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchRoutine/<WaitForAnimEnd>c__Iterator3::.ctor()
extern "C"  void U3CWaitForAnimEndU3Ec__Iterator3__ctor_m1394524683 (U3CWaitForAnimEndU3Ec__Iterator3_t2692169692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaunchRoutine/<WaitForAnimEnd>c__Iterator3::MoveNext()
extern "C"  bool U3CWaitForAnimEndU3Ec__Iterator3_MoveNext_m639027501 (U3CWaitForAnimEndU3Ec__Iterator3_t2692169692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<WaitForAnimEnd>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForAnimEndU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1990021601 (U3CWaitForAnimEndU3Ec__Iterator3_t2692169692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<WaitForAnimEnd>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForAnimEndU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m177510441 (U3CWaitForAnimEndU3Ec__Iterator3_t2692169692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<WaitForAnimEnd>c__Iterator3::Dispose()
extern "C"  void U3CWaitForAnimEndU3Ec__Iterator3_Dispose_m1452190914 (U3CWaitForAnimEndU3Ec__Iterator3_t2692169692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<WaitForAnimEnd>c__Iterator3::Reset()
extern "C"  void U3CWaitForAnimEndU3Ec__Iterator3_Reset_m3727148492 (U3CWaitForAnimEndU3Ec__Iterator3_t2692169692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
