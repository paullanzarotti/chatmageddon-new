﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<UIKeyBinding>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m748378439(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3528388261 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UIKeyBinding>::Invoke(T)
#define Predicate_1_Invoke_m1171893319(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3528388261 *, UIKeyBinding_t790450850 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UIKeyBinding>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m3569546066(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3528388261 *, UIKeyBinding_t790450850 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UIKeyBinding>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1988060361(__this, ___result0, method) ((  bool (*) (Predicate_1_t3528388261 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
