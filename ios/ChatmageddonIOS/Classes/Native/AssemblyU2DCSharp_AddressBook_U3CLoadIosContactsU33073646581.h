﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Contact>
struct List_1_t3383317540;
// AddressBook
struct AddressBook_t411411037;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddressBook/<LoadIosContacts>c__Iterator0
struct  U3CLoadIosContactsU3Ec__Iterator0_t3073646581  : public Il2CppObject
{
public:
	// System.Int32 AddressBook/<LoadIosContacts>c__Iterator0::<indexOffset>__0
	int32_t ___U3CindexOffsetU3E__0_0;
	// System.Collections.Generic.List`1<Contact> AddressBook/<LoadIosContacts>c__Iterator0::<temp>__1
	List_1_t3383317540 * ___U3CtempU3E__1_1;
	// System.Int32 AddressBook/<LoadIosContacts>c__Iterator0::index
	int32_t ___index_2;
	// System.Int64 AddressBook/<LoadIosContacts>c__Iterator0::contactAmount
	int64_t ___contactAmount_3;
	// AddressBook AddressBook/<LoadIosContacts>c__Iterator0::$this
	AddressBook_t411411037 * ___U24this_4;
	// System.Object AddressBook/<LoadIosContacts>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean AddressBook/<LoadIosContacts>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 AddressBook/<LoadIosContacts>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CindexOffsetU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___U3CindexOffsetU3E__0_0)); }
	inline int32_t get_U3CindexOffsetU3E__0_0() const { return ___U3CindexOffsetU3E__0_0; }
	inline int32_t* get_address_of_U3CindexOffsetU3E__0_0() { return &___U3CindexOffsetU3E__0_0; }
	inline void set_U3CindexOffsetU3E__0_0(int32_t value)
	{
		___U3CindexOffsetU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E__1_1() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___U3CtempU3E__1_1)); }
	inline List_1_t3383317540 * get_U3CtempU3E__1_1() const { return ___U3CtempU3E__1_1; }
	inline List_1_t3383317540 ** get_address_of_U3CtempU3E__1_1() { return &___U3CtempU3E__1_1; }
	inline void set_U3CtempU3E__1_1(List_1_t3383317540 * value)
	{
		___U3CtempU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtempU3E__1_1, value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_contactAmount_3() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___contactAmount_3)); }
	inline int64_t get_contactAmount_3() const { return ___contactAmount_3; }
	inline int64_t* get_address_of_contactAmount_3() { return &___contactAmount_3; }
	inline void set_contactAmount_3(int64_t value)
	{
		___contactAmount_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___U24this_4)); }
	inline AddressBook_t411411037 * get_U24this_4() const { return ___U24this_4; }
	inline AddressBook_t411411037 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(AddressBook_t411411037 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadIosContactsU3Ec__Iterator0_t3073646581, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
