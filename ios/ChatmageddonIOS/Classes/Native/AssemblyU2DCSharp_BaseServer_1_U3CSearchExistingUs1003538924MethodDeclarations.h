﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<SearchExistingUserBase>c__AnonStorey1A<System.Object>
struct U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t1003538924;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<SearchExistingUserBase>c__AnonStorey1A<System.Object>::.ctor()
extern "C"  void U3CSearchExistingUserBaseU3Ec__AnonStorey1A__ctor_m4022611017_gshared (U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t1003538924 * __this, const MethodInfo* method);
#define U3CSearchExistingUserBaseU3Ec__AnonStorey1A__ctor_m4022611017(__this, method) ((  void (*) (U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t1003538924 *, const MethodInfo*))U3CSearchExistingUserBaseU3Ec__AnonStorey1A__ctor_m4022611017_gshared)(__this, method)
// System.Void BaseServer`1/<SearchExistingUserBase>c__AnonStorey1A<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CSearchExistingUserBaseU3Ec__AnonStorey1A_U3CU3Em__0_m2091390288_gshared (U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t1003538924 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CSearchExistingUserBaseU3Ec__AnonStorey1A_U3CU3Em__0_m2091390288(__this, ___request0, ___response1, method) ((  void (*) (U3CSearchExistingUserBaseU3Ec__AnonStorey1A_t1003538924 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CSearchExistingUserBaseU3Ec__AnonStorey1A_U3CU3Em__0_m2091390288_gshared)(__this, ___request0, ___response1, method)
