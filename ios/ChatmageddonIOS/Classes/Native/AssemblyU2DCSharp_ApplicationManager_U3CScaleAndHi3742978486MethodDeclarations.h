﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ApplicationManager/<ScaleAndHide>c__Iterator0
struct U3CScaleAndHideU3Ec__Iterator0_t3742978486;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ApplicationManager/<ScaleAndHide>c__Iterator0::.ctor()
extern "C"  void U3CScaleAndHideU3Ec__Iterator0__ctor_m4001393129 (U3CScaleAndHideU3Ec__Iterator0_t3742978486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ApplicationManager/<ScaleAndHide>c__Iterator0::MoveNext()
extern "C"  bool U3CScaleAndHideU3Ec__Iterator0_MoveNext_m1163220867 (U3CScaleAndHideU3Ec__Iterator0_t3742978486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ApplicationManager/<ScaleAndHide>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CScaleAndHideU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2391685031 (U3CScaleAndHideU3Ec__Iterator0_t3742978486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ApplicationManager/<ScaleAndHide>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CScaleAndHideU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m862289231 (U3CScaleAndHideU3Ec__Iterator0_t3742978486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager/<ScaleAndHide>c__Iterator0::Dispose()
extern "C"  void U3CScaleAndHideU3Ec__Iterator0_Dispose_m844004072 (U3CScaleAndHideU3Ec__Iterator0_t3742978486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ApplicationManager/<ScaleAndHide>c__Iterator0::Reset()
extern "C"  void U3CScaleAndHideU3Ec__Iterator0_Reset_m3700418710 (U3CScaleAndHideU3Ec__Iterator0_t3742978486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
