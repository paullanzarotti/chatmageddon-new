﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_StatusSwipeComponent1744404785.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusDoubleStateButton
struct  StatusDoubleStateButton_t3675410548  : public StatusSwipeComponent_t1744404785
{
public:
	// UnityEngine.Color StatusDoubleStateButton::activeForegroundColor
	Color_t2020392075  ___activeForegroundColor_11;
	// UnityEngine.Color StatusDoubleStateButton::activeBackgroundColor
	Color_t2020392075  ___activeBackgroundColor_12;
	// UnityEngine.Color StatusDoubleStateButton::inactiveForegroundColor
	Color_t2020392075  ___inactiveForegroundColor_13;
	// UnityEngine.Color StatusDoubleStateButton::inactiveBackgroundColor
	Color_t2020392075  ___inactiveBackgroundColor_14;
	// UISprite StatusDoubleStateButton::forgroundSprite
	UISprite_t603616735 * ___forgroundSprite_15;
	// UISprite StatusDoubleStateButton::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_16;
	// UILabel StatusDoubleStateButton::buttonLabel
	UILabel_t1795115428 * ___buttonLabel_17;
	// System.Boolean StatusDoubleStateButton::buttonActive
	bool ___buttonActive_18;

public:
	inline static int32_t get_offset_of_activeForegroundColor_11() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___activeForegroundColor_11)); }
	inline Color_t2020392075  get_activeForegroundColor_11() const { return ___activeForegroundColor_11; }
	inline Color_t2020392075 * get_address_of_activeForegroundColor_11() { return &___activeForegroundColor_11; }
	inline void set_activeForegroundColor_11(Color_t2020392075  value)
	{
		___activeForegroundColor_11 = value;
	}

	inline static int32_t get_offset_of_activeBackgroundColor_12() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___activeBackgroundColor_12)); }
	inline Color_t2020392075  get_activeBackgroundColor_12() const { return ___activeBackgroundColor_12; }
	inline Color_t2020392075 * get_address_of_activeBackgroundColor_12() { return &___activeBackgroundColor_12; }
	inline void set_activeBackgroundColor_12(Color_t2020392075  value)
	{
		___activeBackgroundColor_12 = value;
	}

	inline static int32_t get_offset_of_inactiveForegroundColor_13() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___inactiveForegroundColor_13)); }
	inline Color_t2020392075  get_inactiveForegroundColor_13() const { return ___inactiveForegroundColor_13; }
	inline Color_t2020392075 * get_address_of_inactiveForegroundColor_13() { return &___inactiveForegroundColor_13; }
	inline void set_inactiveForegroundColor_13(Color_t2020392075  value)
	{
		___inactiveForegroundColor_13 = value;
	}

	inline static int32_t get_offset_of_inactiveBackgroundColor_14() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___inactiveBackgroundColor_14)); }
	inline Color_t2020392075  get_inactiveBackgroundColor_14() const { return ___inactiveBackgroundColor_14; }
	inline Color_t2020392075 * get_address_of_inactiveBackgroundColor_14() { return &___inactiveBackgroundColor_14; }
	inline void set_inactiveBackgroundColor_14(Color_t2020392075  value)
	{
		___inactiveBackgroundColor_14 = value;
	}

	inline static int32_t get_offset_of_forgroundSprite_15() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___forgroundSprite_15)); }
	inline UISprite_t603616735 * get_forgroundSprite_15() const { return ___forgroundSprite_15; }
	inline UISprite_t603616735 ** get_address_of_forgroundSprite_15() { return &___forgroundSprite_15; }
	inline void set_forgroundSprite_15(UISprite_t603616735 * value)
	{
		___forgroundSprite_15 = value;
		Il2CppCodeGenWriteBarrier(&___forgroundSprite_15, value);
	}

	inline static int32_t get_offset_of_backgroundSprite_16() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___backgroundSprite_16)); }
	inline UISprite_t603616735 * get_backgroundSprite_16() const { return ___backgroundSprite_16; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_16() { return &___backgroundSprite_16; }
	inline void set_backgroundSprite_16(UISprite_t603616735 * value)
	{
		___backgroundSprite_16 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_16, value);
	}

	inline static int32_t get_offset_of_buttonLabel_17() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___buttonLabel_17)); }
	inline UILabel_t1795115428 * get_buttonLabel_17() const { return ___buttonLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_buttonLabel_17() { return &___buttonLabel_17; }
	inline void set_buttonLabel_17(UILabel_t1795115428 * value)
	{
		___buttonLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabel_17, value);
	}

	inline static int32_t get_offset_of_buttonActive_18() { return static_cast<int32_t>(offsetof(StatusDoubleStateButton_t3675410548, ___buttonActive_18)); }
	inline bool get_buttonActive_18() const { return ___buttonActive_18; }
	inline bool* get_address_of_buttonActive_18() { return &___buttonActive_18; }
	inline void set_buttonActive_18(bool value)
	{
		___buttonActive_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
