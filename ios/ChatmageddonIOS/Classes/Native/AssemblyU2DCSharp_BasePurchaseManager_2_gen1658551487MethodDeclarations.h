﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BasePurchaseManager`2<System.Object,ShopPurchasableItems>
struct BasePurchaseManager_2_t1658551487;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ShopPurchasableItems522663808.h"

// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::.ctor()
extern "C"  void BasePurchaseManager_2__ctor_m1131248398_gshared (BasePurchaseManager_2_t1658551487 * __this, const MethodInfo* method);
#define BasePurchaseManager_2__ctor_m1131248398(__this, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, const MethodInfo*))BasePurchaseManager_2__ctor_m1131248398_gshared)(__this, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::Init()
extern "C"  void BasePurchaseManager_2_Init_m619327910_gshared (BasePurchaseManager_2_t1658551487 * __this, const MethodInfo* method);
#define BasePurchaseManager_2_Init_m619327910(__this, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, const MethodInfo*))BasePurchaseManager_2_Init_m619327910_gshared)(__this, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::PurchaseUnibillItem(PurchasableItem)
extern "C"  void BasePurchaseManager_2_PurchaseUnibillItem_m643615934_gshared (BasePurchaseManager_2_t1658551487 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_PurchaseUnibillItem_m643615934(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, PurchasableItem_t3963353899 *, const MethodInfo*))BasePurchaseManager_2_PurchaseUnibillItem_m643615934_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::UnibillPurchaseSuccess(PurchasableItem,System.String)
extern "C"  void BasePurchaseManager_2_UnibillPurchaseSuccess_m1466479594_gshared (BasePurchaseManager_2_t1658551487 * __this, PurchasableItem_t3963353899 * ___item0, String_t* ___receipt1, const MethodInfo* method);
#define BasePurchaseManager_2_UnibillPurchaseSuccess_m1466479594(__this, ___item0, ___receipt1, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, PurchasableItem_t3963353899 *, String_t*, const MethodInfo*))BasePurchaseManager_2_UnibillPurchaseSuccess_m1466479594_gshared)(__this, ___item0, ___receipt1, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::UnibillPurchaseFailed(PurchasableItem)
extern "C"  void BasePurchaseManager_2_UnibillPurchaseFailed_m3742805378_gshared (BasePurchaseManager_2_t1658551487 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_UnibillPurchaseFailed_m3742805378(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, PurchasableItem_t3963353899 *, const MethodInfo*))BasePurchaseManager_2_UnibillPurchaseFailed_m3742805378_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::ProcessItem(System.String,System.String)
extern "C"  void BasePurchaseManager_2_ProcessItem_m640918206_gshared (BasePurchaseManager_2_t1658551487 * __this, String_t* ___id0, String_t* ___receipt1, const MethodInfo* method);
#define BasePurchaseManager_2_ProcessItem_m640918206(__this, ___id0, ___receipt1, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, String_t*, String_t*, const MethodInfo*))BasePurchaseManager_2_ProcessItem_m640918206_gshared)(__this, ___id0, ___receipt1, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::ProcessFailedItem(System.String)
extern "C"  void BasePurchaseManager_2_ProcessFailedItem_m3360315917_gshared (BasePurchaseManager_2_t1658551487 * __this, String_t* ___id0, const MethodInfo* method);
#define BasePurchaseManager_2_ProcessFailedItem_m3360315917(__this, ___id0, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, String_t*, const MethodInfo*))BasePurchaseManager_2_ProcessFailedItem_m3360315917_gshared)(__this, ___id0, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::PurchaseItem(itemType)
extern "C"  void BasePurchaseManager_2_PurchaseItem_m926136925_gshared (BasePurchaseManager_2_t1658551487 * __this, int32_t ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_PurchaseItem_m926136925(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, int32_t, const MethodInfo*))BasePurchaseManager_2_PurchaseItem_m926136925_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<System.Object,ShopPurchasableItems>::PurchaseItemByID(System.String)
extern "C"  void BasePurchaseManager_2_PurchaseItemByID_m4138142978_gshared (BasePurchaseManager_2_t1658551487 * __this, String_t* ___itemID0, const MethodInfo* method);
#define BasePurchaseManager_2_PurchaseItemByID_m4138142978(__this, ___itemID0, method) ((  void (*) (BasePurchaseManager_2_t1658551487 *, String_t*, const MethodInfo*))BasePurchaseManager_2_PurchaseItemByID_m4138142978_gshared)(__this, ___itemID0, method)
// System.String BasePurchaseManager`2<System.Object,ShopPurchasableItems>::GetPurchaseLocalizedString(itemType)
extern "C"  String_t* BasePurchaseManager_2_GetPurchaseLocalizedString_m1960358353_gshared (BasePurchaseManager_2_t1658551487 * __this, int32_t ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_GetPurchaseLocalizedString_m1960358353(__this, ___item0, method) ((  String_t* (*) (BasePurchaseManager_2_t1658551487 *, int32_t, const MethodInfo*))BasePurchaseManager_2_GetPurchaseLocalizedString_m1960358353_gshared)(__this, ___item0, method)
// System.String BasePurchaseManager`2<System.Object,ShopPurchasableItems>::GetPurchaseLocalizedStringByID(System.String)
extern "C"  String_t* BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m3465251108_gshared (BasePurchaseManager_2_t1658551487 * __this, String_t* ___itemID0, const MethodInfo* method);
#define BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m3465251108(__this, ___itemID0, method) ((  String_t* (*) (BasePurchaseManager_2_t1658551487 *, String_t*, const MethodInfo*))BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m3465251108_gshared)(__this, ___itemID0, method)
