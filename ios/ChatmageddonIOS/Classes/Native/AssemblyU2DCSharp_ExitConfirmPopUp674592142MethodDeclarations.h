﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExitConfirmPopUp
struct ExitConfirmPopUp_t674592142;

#include "codegen/il2cpp-codegen.h"

// System.Void ExitConfirmPopUp::.ctor()
extern "C"  void ExitConfirmPopUp__ctor_m640887399 (ExitConfirmPopUp_t674592142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitConfirmPopUp::Awake()
extern "C"  void ExitConfirmPopUp_Awake_m1272298970 (ExitConfirmPopUp_t674592142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitConfirmPopUp::LoadPopUp()
extern "C"  void ExitConfirmPopUp_LoadPopUp_m4086681107 (ExitConfirmPopUp_t674592142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitConfirmPopUp::UnloadPopUp()
extern "C"  void ExitConfirmPopUp_UnloadPopUp_m1750613170 (ExitConfirmPopUp_t674592142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExitConfirmPopUp::OnBackgroundFinished()
extern "C"  void ExitConfirmPopUp_OnBackgroundFinished_m1890336710 (ExitConfirmPopUp_t674592142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
