﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationOkButton
struct NotificationOkButton_t3431423113;

#include "codegen/il2cpp-codegen.h"

// System.Void NotificationOkButton::.ctor()
extern "C"  void NotificationOkButton__ctor_m1174167864 (NotificationOkButton_t3431423113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationOkButton::OnButtonClick()
extern "C"  void NotificationOkButton_OnButtonClick_m490977681 (NotificationOkButton_t3431423113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
