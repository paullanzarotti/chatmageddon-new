﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<UnibillManager>::.ctor()
#define MonoSingleton_1__ctor_m3795133234(__this, method) ((  void (*) (MonoSingleton_1_t3476048858 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<UnibillManager>::Awake()
#define MonoSingleton_1_Awake_m2322269947(__this, method) ((  void (*) (MonoSingleton_1_t3476048858 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<UnibillManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m1550792109(__this /* static, unused */, method) ((  UnibillManager_t3725383138 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<UnibillManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4196810385(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<UnibillManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m4005890928(__this, method) ((  void (*) (MonoSingleton_1_t3476048858 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<UnibillManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m3125854464(__this, method) ((  void (*) (MonoSingleton_1_t3476048858 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<UnibillManager>::.cctor()
#define MonoSingleton_1__cctor_m1646691581(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
