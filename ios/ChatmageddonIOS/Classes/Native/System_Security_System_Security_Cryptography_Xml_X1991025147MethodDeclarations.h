﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDsigXsltTransform
struct XmlDsigXsltTransform_t1991025147;
// System.Xml.XmlNodeList
struct XmlNodeList_t497326455;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Void System.Security.Cryptography.Xml.XmlDsigXsltTransform::.ctor()
extern "C"  void XmlDsigXsltTransform__ctor_m313710377 (XmlDsigXsltTransform_t1991025147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigXsltTransform::.ctor(System.Boolean)
extern "C"  void XmlDsigXsltTransform__ctor_m3127990518 (XmlDsigXsltTransform_t1991025147 * __this, bool ___includeComments0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigXsltTransform::GetInnerXml()
extern "C"  XmlNodeList_t497326455 * XmlDsigXsltTransform_GetInnerXml_m1115381564 (XmlDsigXsltTransform_t1991025147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigXsltTransform::LoadInnerXml(System.Xml.XmlNodeList)
extern "C"  void XmlDsigXsltTransform_LoadInnerXml_m243882859 (XmlDsigXsltTransform_t1991025147 * __this, XmlNodeList_t497326455 * ___nodeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
