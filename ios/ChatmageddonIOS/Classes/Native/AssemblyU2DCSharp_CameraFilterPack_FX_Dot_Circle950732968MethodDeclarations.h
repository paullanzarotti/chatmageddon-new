﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Dot_Circle
struct CameraFilterPack_FX_Dot_Circle_t950732968;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Dot_Circle::.ctor()
extern "C"  void CameraFilterPack_FX_Dot_Circle__ctor_m3472391703 (CameraFilterPack_FX_Dot_Circle_t950732968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Dot_Circle::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Dot_Circle_get_material_m3963901014 (CameraFilterPack_FX_Dot_Circle_t950732968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::Start()
extern "C"  void CameraFilterPack_FX_Dot_Circle_Start_m1663776211 (CameraFilterPack_FX_Dot_Circle_t950732968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Dot_Circle_OnRenderImage_m914234595 (CameraFilterPack_FX_Dot_Circle_t950732968 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::OnValidate()
extern "C"  void CameraFilterPack_FX_Dot_Circle_OnValidate_m2214677536 (CameraFilterPack_FX_Dot_Circle_t950732968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::Update()
extern "C"  void CameraFilterPack_FX_Dot_Circle_Update_m2111639462 (CameraFilterPack_FX_Dot_Circle_t950732968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::OnDisable()
extern "C"  void CameraFilterPack_FX_Dot_Circle_OnDisable_m1404613080 (CameraFilterPack_FX_Dot_Circle_t950732968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
