﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMBase/<HasTagValue>c__AnonStorey3
struct U3CHasTagValueU3Ec__AnonStorey3_t620528620;
// OnlineMapsOSMTag
struct OnlineMapsOSMTag_t3629071465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMTag3629071465.h"

// System.Void OnlineMapsOSMBase/<HasTagValue>c__AnonStorey3::.ctor()
extern "C"  void U3CHasTagValueU3Ec__AnonStorey3__ctor_m54530949 (U3CHasTagValueU3Ec__AnonStorey3_t620528620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsOSMBase/<HasTagValue>c__AnonStorey3::<>m__0(OnlineMapsOSMTag)
extern "C"  bool U3CHasTagValueU3Ec__AnonStorey3_U3CU3Em__0_m1310591561 (U3CHasTagValueU3Ec__AnonStorey3_t620528620 * __this, OnlineMapsOSMTag_t3629071465 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
