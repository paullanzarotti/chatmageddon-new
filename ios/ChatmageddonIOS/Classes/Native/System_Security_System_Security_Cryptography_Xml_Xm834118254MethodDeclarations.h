﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.XmlDsigExcC14NTransform
struct XmlDsigExcC14NTransform_t834118254;
// System.String
struct String_t;
// System.Xml.XmlNodeList
struct XmlNodeList_t497326455;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"

// System.Void System.Security.Cryptography.Xml.XmlDsigExcC14NTransform::.ctor()
extern "C"  void XmlDsigExcC14NTransform__ctor_m3063238304 (XmlDsigExcC14NTransform_t834118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigExcC14NTransform::.ctor(System.Boolean)
extern "C"  void XmlDsigExcC14NTransform__ctor_m1873268091 (XmlDsigExcC14NTransform_t834118254 * __this, bool ___includeComments0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigExcC14NTransform::.ctor(System.Boolean,System.String)
extern "C"  void XmlDsigExcC14NTransform__ctor_m1098688877 (XmlDsigExcC14NTransform_t834118254 * __this, bool ___includeComments0, String_t* ___inclusiveNamespacesPrefixList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeList System.Security.Cryptography.Xml.XmlDsigExcC14NTransform::GetInnerXml()
extern "C"  XmlNodeList_t497326455 * XmlDsigExcC14NTransform_GetInnerXml_m1068674237 (XmlDsigExcC14NTransform_t834118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.XmlDsigExcC14NTransform::LoadInnerXml(System.Xml.XmlNodeList)
extern "C"  void XmlDsigExcC14NTransform_LoadInnerXml_m824851252 (XmlDsigExcC14NTransform_t834118254 * __this, XmlNodeList_t497326455 * ___nodeList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
