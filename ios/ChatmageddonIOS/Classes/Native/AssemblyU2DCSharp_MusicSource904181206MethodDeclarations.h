﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MusicSource
struct MusicSource_t904181206;

#include "codegen/il2cpp-codegen.h"

// System.Void MusicSource::.ctor()
extern "C"  void MusicSource__ctor_m2515116799 (MusicSource_t904181206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
