﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VisibilityControlStatic/<WaitCheck>c__Iterator0
struct U3CWaitCheckU3Ec__Iterator0_t23299524;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VisibilityControlStatic/<WaitCheck>c__Iterator0::.ctor()
extern "C"  void U3CWaitCheckU3Ec__Iterator0__ctor_m1011817141 (U3CWaitCheckU3Ec__Iterator0_t23299524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VisibilityControlStatic/<WaitCheck>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitCheckU3Ec__Iterator0_MoveNext_m2826533899 (U3CWaitCheckU3Ec__Iterator0_t23299524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VisibilityControlStatic/<WaitCheck>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitCheckU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m906149511 (U3CWaitCheckU3Ec__Iterator0_t23299524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VisibilityControlStatic/<WaitCheck>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitCheckU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1975844463 (U3CWaitCheckU3Ec__Iterator0_t23299524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlStatic/<WaitCheck>c__Iterator0::Dispose()
extern "C"  void U3CWaitCheckU3Ec__Iterator0_Dispose_m3801062720 (U3CWaitCheckU3Ec__Iterator0_t23299524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VisibilityControlStatic/<WaitCheck>c__Iterator0::Reset()
extern "C"  void U3CWaitCheckU3Ec__Iterator0_Reset_m12360450 (U3CWaitCheckU3Ec__Iterator0_t23299524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
