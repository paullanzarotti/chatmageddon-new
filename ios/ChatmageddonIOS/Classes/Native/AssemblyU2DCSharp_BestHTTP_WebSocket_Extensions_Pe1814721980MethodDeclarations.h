﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BestHTTP.WebSocket.Extensions.PerMessageCompression
struct PerMessageCompression_t1814721980;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.WebSocket.WebSocketResponse
struct WebSocketResponse_t3376763264;
// BestHTTP.WebSocket.Frames.WebSocketFrame
struct WebSocketFrame_t4163283394;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Comp4151391442.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_WebSocketResp3376763264.h"
#include "AssemblyU2DCSharp_BestHTTP_WebSocket_Frames_WebSoc4163283394.h"

// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::.ctor()
extern "C"  void PerMessageCompression__ctor_m1244799342 (PerMessageCompression_t1814721980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::.ctor(BestHTTP.Decompression.Zlib.CompressionLevel,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32)
extern "C"  void PerMessageCompression__ctor_m1589524725 (PerMessageCompression_t1814721980 * __this, int32_t ___level0, bool ___clientNoContextTakeover1, bool ___serverNoContextTakeover2, int32_t ___desiredClientMaxWindowBits3, int32_t ___desiredServerMaxWindowBits4, int32_t ___minDatalengthToCompress5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.WebSocket.Extensions.PerMessageCompression::get_ClientNoContextTakeover()
extern "C"  bool PerMessageCompression_get_ClientNoContextTakeover_m2971218763 (PerMessageCompression_t1814721980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::set_ClientNoContextTakeover(System.Boolean)
extern "C"  void PerMessageCompression_set_ClientNoContextTakeover_m2171063570 (PerMessageCompression_t1814721980 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.WebSocket.Extensions.PerMessageCompression::get_ServerNoContextTakeover()
extern "C"  bool PerMessageCompression_get_ServerNoContextTakeover_m2485038007 (PerMessageCompression_t1814721980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::set_ServerNoContextTakeover(System.Boolean)
extern "C"  void PerMessageCompression_set_ServerNoContextTakeover_m4048237334 (PerMessageCompression_t1814721980 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::get_ClientMaxWindowBits()
extern "C"  int32_t PerMessageCompression_get_ClientMaxWindowBits_m2781083992 (PerMessageCompression_t1814721980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::set_ClientMaxWindowBits(System.Int32)
extern "C"  void PerMessageCompression_set_ClientMaxWindowBits_m1499745657 (PerMessageCompression_t1814721980 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::get_ServerMaxWindowBits()
extern "C"  int32_t PerMessageCompression_get_ServerMaxWindowBits_m4238602596 (PerMessageCompression_t1814721980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::set_ServerMaxWindowBits(System.Int32)
extern "C"  void PerMessageCompression_set_ServerMaxWindowBits_m3123152477 (PerMessageCompression_t1814721980 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BestHTTP.Decompression.Zlib.CompressionLevel BestHTTP.WebSocket.Extensions.PerMessageCompression::get_Level()
extern "C"  int32_t PerMessageCompression_get_Level_m207174884 (PerMessageCompression_t1814721980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::set_Level(BestHTTP.Decompression.Zlib.CompressionLevel)
extern "C"  void PerMessageCompression_set_Level_m2862530807 (PerMessageCompression_t1814721980 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BestHTTP.WebSocket.Extensions.PerMessageCompression::get_MinimumDataLegthToCompress()
extern "C"  int32_t PerMessageCompression_get_MinimumDataLegthToCompress_m1388210790 (PerMessageCompression_t1814721980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::set_MinimumDataLegthToCompress(System.Int32)
extern "C"  void PerMessageCompression_set_MinimumDataLegthToCompress_m630864789 (PerMessageCompression_t1814721980 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::AddNegotiation(BestHTTP.HTTPRequest)
extern "C"  void PerMessageCompression_AddNegotiation_m3793921541 (PerMessageCompression_t1814721980 * __this, HTTPRequest_t138485887 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BestHTTP.WebSocket.Extensions.PerMessageCompression::ParseNegotiation(BestHTTP.WebSocket.WebSocketResponse)
extern "C"  bool PerMessageCompression_ParseNegotiation_m2283491397 (PerMessageCompression_t1814721980 * __this, WebSocketResponse_t3376763264 * ___resp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte BestHTTP.WebSocket.Extensions.PerMessageCompression::GetFrameHeader(BestHTTP.WebSocket.Frames.WebSocketFrame,System.Byte)
extern "C"  uint8_t PerMessageCompression_GetFrameHeader_m2257970602 (PerMessageCompression_t1814721980 * __this, WebSocketFrame_t4163283394 * ___writer0, uint8_t ___inFlag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] BestHTTP.WebSocket.Extensions.PerMessageCompression::Encode(BestHTTP.WebSocket.Frames.WebSocketFrame)
extern "C"  ByteU5BU5D_t3397334013* PerMessageCompression_Encode_m2716561971 (PerMessageCompression_t1814721980 * __this, WebSocketFrame_t4163283394 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] BestHTTP.WebSocket.Extensions.PerMessageCompression::Decode(System.Byte,System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* PerMessageCompression_Decode_m4200040668 (PerMessageCompression_t1814721980 * __this, uint8_t ___header0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] BestHTTP.WebSocket.Extensions.PerMessageCompression::Compress(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* PerMessageCompression_Compress_m1717276409 (PerMessageCompression_t1814721980 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] BestHTTP.WebSocket.Extensions.PerMessageCompression::Decompress(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* PerMessageCompression_Decompress_m2818278498 (PerMessageCompression_t1814721980 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BestHTTP.WebSocket.Extensions.PerMessageCompression::.cctor()
extern "C"  void PerMessageCompression__cctor_m2396174655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
