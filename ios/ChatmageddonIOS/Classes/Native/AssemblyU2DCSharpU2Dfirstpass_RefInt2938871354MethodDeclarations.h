﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RefInt
struct RefInt_t2938871354;

#include "codegen/il2cpp-codegen.h"

// System.Void RefInt::.ctor(System.Int32)
extern "C"  void RefInt__ctor_m3359105882 (RefInt_t2938871354 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
