﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsOtherNavScreen
struct FriendsOtherNavScreen_t264443870;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendsOtherNavScreen::.ctor()
extern "C"  void FriendsOtherNavScreen__ctor_m3985597011 (FriendsOtherNavScreen_t264443870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOtherNavScreen::Start()
extern "C"  void FriendsOtherNavScreen_Start_m1177166671 (FriendsOtherNavScreen_t264443870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOtherNavScreen::UIClosing()
extern "C"  void FriendsOtherNavScreen_UIClosing_m3333710316 (FriendsOtherNavScreen_t264443870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOtherNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void FriendsOtherNavScreen_ScreenLoad_m500233413 (FriendsOtherNavScreen_t264443870 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOtherNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void FriendsOtherNavScreen_ScreenUnload_m2727922153 (FriendsOtherNavScreen_t264443870 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsOtherNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool FriendsOtherNavScreen_ValidateScreenNavigation_m135290030 (FriendsOtherNavScreen_t264443870 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
