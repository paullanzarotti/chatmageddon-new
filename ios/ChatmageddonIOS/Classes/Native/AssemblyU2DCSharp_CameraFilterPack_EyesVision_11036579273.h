﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_EyesVision_1
struct  CameraFilterPack_EyesVision_1_t1036579273  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_EyesVision_1::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_EyesVision_1::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_EyesVision_1::_EyeWave
	float ____EyeWave_4;
	// System.Single CameraFilterPack_EyesVision_1::_EyeSpeed
	float ____EyeSpeed_5;
	// System.Single CameraFilterPack_EyesVision_1::_EyeMove
	float ____EyeMove_6;
	// System.Single CameraFilterPack_EyesVision_1::_EyeBlink
	float ____EyeBlink_7;
	// UnityEngine.Material CameraFilterPack_EyesVision_1::SCMaterial
	Material_t193706927 * ___SCMaterial_8;
	// UnityEngine.Texture2D CameraFilterPack_EyesVision_1::Texture2
	Texture2D_t3542995729 * ___Texture2_9;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of__EyeWave_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ____EyeWave_4)); }
	inline float get__EyeWave_4() const { return ____EyeWave_4; }
	inline float* get_address_of__EyeWave_4() { return &____EyeWave_4; }
	inline void set__EyeWave_4(float value)
	{
		____EyeWave_4 = value;
	}

	inline static int32_t get_offset_of__EyeSpeed_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ____EyeSpeed_5)); }
	inline float get__EyeSpeed_5() const { return ____EyeSpeed_5; }
	inline float* get_address_of__EyeSpeed_5() { return &____EyeSpeed_5; }
	inline void set__EyeSpeed_5(float value)
	{
		____EyeSpeed_5 = value;
	}

	inline static int32_t get_offset_of__EyeMove_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ____EyeMove_6)); }
	inline float get__EyeMove_6() const { return ____EyeMove_6; }
	inline float* get_address_of__EyeMove_6() { return &____EyeMove_6; }
	inline void set__EyeMove_6(float value)
	{
		____EyeMove_6 = value;
	}

	inline static int32_t get_offset_of__EyeBlink_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ____EyeBlink_7)); }
	inline float get__EyeBlink_7() const { return ____EyeBlink_7; }
	inline float* get_address_of__EyeBlink_7() { return &____EyeBlink_7; }
	inline void set__EyeBlink_7(float value)
	{
		____EyeBlink_7 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ___SCMaterial_8)); }
	inline Material_t193706927 * get_SCMaterial_8() const { return ___SCMaterial_8; }
	inline Material_t193706927 ** get_address_of_SCMaterial_8() { return &___SCMaterial_8; }
	inline void set_SCMaterial_8(Material_t193706927 * value)
	{
		___SCMaterial_8 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_8, value);
	}

	inline static int32_t get_offset_of_Texture2_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_EyesVision_1_t1036579273, ___Texture2_9)); }
	inline Texture2D_t3542995729 * get_Texture2_9() const { return ___Texture2_9; }
	inline Texture2D_t3542995729 ** get_address_of_Texture2_9() { return &___Texture2_9; }
	inline void set_Texture2_9(Texture2D_t3542995729 * value)
	{
		___Texture2_9 = value;
		Il2CppCodeGenWriteBarrier(&___Texture2_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
