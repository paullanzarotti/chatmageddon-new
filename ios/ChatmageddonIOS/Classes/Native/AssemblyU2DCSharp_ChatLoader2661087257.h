﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_SceneLoaderUI1806459353.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatLoader
struct  ChatLoader_t2661087257  : public SceneLoaderUI_t1806459353
{
public:
	// UILabel ChatLoader::chatTitle
	UILabel_t1795115428 * ___chatTitle_2;
	// UISprite ChatLoader::backButtonSprite
	UISprite_t603616735 * ___backButtonSprite_3;
	// UISprite ChatLoader::forwardButtonSprite
	UISprite_t603616735 * ___forwardButtonSprite_4;
	// UnityEngine.Transform ChatLoader::newMessageButton
	Transform_t3275118058 * ___newMessageButton_5;
	// System.Boolean ChatLoader::newMessage
	bool ___newMessage_6;

public:
	inline static int32_t get_offset_of_chatTitle_2() { return static_cast<int32_t>(offsetof(ChatLoader_t2661087257, ___chatTitle_2)); }
	inline UILabel_t1795115428 * get_chatTitle_2() const { return ___chatTitle_2; }
	inline UILabel_t1795115428 ** get_address_of_chatTitle_2() { return &___chatTitle_2; }
	inline void set_chatTitle_2(UILabel_t1795115428 * value)
	{
		___chatTitle_2 = value;
		Il2CppCodeGenWriteBarrier(&___chatTitle_2, value);
	}

	inline static int32_t get_offset_of_backButtonSprite_3() { return static_cast<int32_t>(offsetof(ChatLoader_t2661087257, ___backButtonSprite_3)); }
	inline UISprite_t603616735 * get_backButtonSprite_3() const { return ___backButtonSprite_3; }
	inline UISprite_t603616735 ** get_address_of_backButtonSprite_3() { return &___backButtonSprite_3; }
	inline void set_backButtonSprite_3(UISprite_t603616735 * value)
	{
		___backButtonSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___backButtonSprite_3, value);
	}

	inline static int32_t get_offset_of_forwardButtonSprite_4() { return static_cast<int32_t>(offsetof(ChatLoader_t2661087257, ___forwardButtonSprite_4)); }
	inline UISprite_t603616735 * get_forwardButtonSprite_4() const { return ___forwardButtonSprite_4; }
	inline UISprite_t603616735 ** get_address_of_forwardButtonSprite_4() { return &___forwardButtonSprite_4; }
	inline void set_forwardButtonSprite_4(UISprite_t603616735 * value)
	{
		___forwardButtonSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___forwardButtonSprite_4, value);
	}

	inline static int32_t get_offset_of_newMessageButton_5() { return static_cast<int32_t>(offsetof(ChatLoader_t2661087257, ___newMessageButton_5)); }
	inline Transform_t3275118058 * get_newMessageButton_5() const { return ___newMessageButton_5; }
	inline Transform_t3275118058 ** get_address_of_newMessageButton_5() { return &___newMessageButton_5; }
	inline void set_newMessageButton_5(Transform_t3275118058 * value)
	{
		___newMessageButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___newMessageButton_5, value);
	}

	inline static int32_t get_offset_of_newMessage_6() { return static_cast<int32_t>(offsetof(ChatLoader_t2661087257, ___newMessage_6)); }
	inline bool get_newMessage_6() const { return ___newMessage_6; }
	inline bool* get_address_of_newMessage_6() { return &___newMessage_6; }
	inline void set_newMessage_6(bool value)
	{
		___newMessage_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
