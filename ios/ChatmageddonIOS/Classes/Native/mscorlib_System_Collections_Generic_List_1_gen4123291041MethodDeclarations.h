﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<StatusListItem>::.ctor()
#define List_1__ctor_m2171868874(__this, method) ((  void (*) (List_1_t4123291041 *, const MethodInfo*))List_1__ctor_m2944773907_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2225234792(__this, ___collection0, method) ((  void (*) (List_1_t4123291041 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3642371895_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::.ctor(System.Int32)
#define List_1__ctor_m3923422962(__this, ___capacity0, method) ((  void (*) (List_1_t4123291041 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::.ctor(T[],System.Int32)
#define List_1__ctor_m3542714106(__this, ___data0, ___size1, method) ((  void (*) (List_1_t4123291041 *, StatusListItemU5BU5D_t869095256*, int32_t, const MethodInfo*))List_1__ctor_m3410056391_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::.cctor()
#define List_1__cctor_m1760631678(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<StatusListItem>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3342813257(__this, method) ((  Il2CppObject* (*) (List_1_t4123291041 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2191932905(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4123291041 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<StatusListItem>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3001776584(__this, method) ((  Il2CppObject * (*) (List_1_t4123291041 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3192449969(__this, ___item0, method) ((  int32_t (*) (List_1_t4123291041 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1345335113(__this, ___item0, method) ((  bool (*) (List_1_t4123291041 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m593845351(__this, ___item0, method) ((  int32_t (*) (List_1_t4123291041 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1947274498(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4123291041 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3534985656(__this, ___item0, method) ((  void (*) (List_1_t4123291041 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1793571896(__this, method) ((  bool (*) (List_1_t4123291041 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1175590301(__this, method) ((  bool (*) (List_1_t4123291041 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<StatusListItem>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1776602333(__this, method) ((  Il2CppObject * (*) (List_1_t4123291041 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3236335114(__this, method) ((  bool (*) (List_1_t4123291041 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1687270505(__this, method) ((  bool (*) (List_1_t4123291041 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m294201088(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4123291041 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2518599255(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4123291041 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Add(T)
#define List_1_Add_m1147007974(__this, ___item0, method) ((  void (*) (List_1_t4123291041 *, StatusListItem_t459202613 *, const MethodInfo*))List_1_Add_m2016287831_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m484776489(__this, ___newCount0, method) ((  void (*) (List_1_t4123291041 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m2511991578(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4123291041 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3993835489(__this, ___collection0, method) ((  void (*) (List_1_t4123291041 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1300227825(__this, ___enumerable0, method) ((  void (*) (List_1_t4123291041 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m331655416(__this, ___collection0, method) ((  void (*) (List_1_t4123291041 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1153521819_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<StatusListItem>::AsReadOnly()
#define List_1_AsReadOnly_m2502106137(__this, method) ((  ReadOnlyCollection_1_t644988305 * (*) (List_1_t4123291041 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Clear()
#define List_1_Clear_m884302602(__this, method) ((  void (*) (List_1_t4123291041 *, const MethodInfo*))List_1_Clear_m1130211784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::Contains(T)
#define List_1_Contains_m303509196(__this, ___item0, method) ((  bool (*) (List_1_t4123291041 *, StatusListItem_t459202613 *, const MethodInfo*))List_1_Contains_m3004503139_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::CopyTo(T[])
#define List_1_CopyTo_m3670891049(__this, ___array0, method) ((  void (*) (List_1_t4123291041 *, StatusListItemU5BU5D_t869095256*, const MethodInfo*))List_1_CopyTo_m626830950_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3598770334(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4123291041 *, StatusListItemU5BU5D_t869095256*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m1743422212(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t4123291041 *, int32_t, StatusListItemU5BU5D_t869095256*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3378929717_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m3145163328(__this, ___match0, method) ((  bool (*) (List_1_t4123291041 *, Predicate_1_t3197140024 *, const MethodInfo*))List_1_Exists_m1496178069_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<StatusListItem>::Find(System.Predicate`1<T>)
#define List_1_Find_m1754863008(__this, ___match0, method) ((  StatusListItem_t459202613 * (*) (List_1_t4123291041 *, Predicate_1_t3197140024 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1144395577(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3197140024 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<StatusListItem>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m4095547317(__this, ___match0, method) ((  List_1_t4123291041 * (*) (List_1_t4123291041 *, Predicate_1_t3197140024 *, const MethodInfo*))List_1_FindAll_m1864150752_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<StatusListItem>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m982489535(__this, ___match0, method) ((  List_1_t4123291041 * (*) (List_1_t4123291041 *, Predicate_1_t3197140024 *, const MethodInfo*))List_1_FindAllStackBits_m1687626136_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<StatusListItem>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m1522214031(__this, ___match0, method) ((  List_1_t4123291041 * (*) (List_1_t4123291041 *, Predicate_1_t3197140024 *, const MethodInfo*))List_1_FindAllList_m3013325848_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1303274505(__this, ___match0, method) ((  int32_t (*) (List_1_t4123291041 *, Predicate_1_t3197140024 *, const MethodInfo*))List_1_FindIndex_m552623364_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1799457106(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4123291041 *, int32_t, int32_t, Predicate_1_t3197140024 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m1410011187(__this, ___action0, method) ((  void (*) (List_1_t4123291041 *, Action_1_t261001995 *, const MethodInfo*))List_1_ForEach_m4266746626_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<StatusListItem>::GetEnumerator()
#define List_1_GetEnumerator_m924669149(__this, method) ((  Enumerator_t3658020715  (*) (List_1_t4123291041 *, const MethodInfo*))List_1_GetEnumerator_m1278946119_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::IndexOf(T)
#define List_1_IndexOf_m2623618048(__this, ___item0, method) ((  int32_t (*) (List_1_t4123291041 *, StatusListItem_t459202613 *, const MethodInfo*))List_1_IndexOf_m1888505567_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m421504717(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4123291041 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1008578186(__this, ___index0, method) ((  void (*) (List_1_t4123291041 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Insert(System.Int32,T)
#define List_1_Insert_m4092853295(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4123291041 *, int32_t, StatusListItem_t459202613 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2065523748(__this, ___collection0, method) ((  void (*) (List_1_t4123291041 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<StatusListItem>::Remove(T)
#define List_1_Remove_m154050843(__this, ___item0, method) ((  bool (*) (List_1_t4123291041 *, StatusListItem_t459202613 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3088661081(__this, ___match0, method) ((  int32_t (*) (List_1_t4123291041 *, Predicate_1_t3197140024 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m989956723(__this, ___index0, method) ((  void (*) (List_1_t4123291041 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m481009014(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4123291041 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Reverse()
#define List_1_Reverse_m1057395565(__this, method) ((  void (*) (List_1_t4123291041 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Sort()
#define List_1_Sort_m3597195219(__this, method) ((  void (*) (List_1_t4123291041 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m306726399(__this, ___comparer0, method) ((  void (*) (List_1_t4123291041 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1932873654(__this, ___comparison0, method) ((  void (*) (List_1_t4123291041 *, Comparison_1_t1720941464 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<StatusListItem>::ToArray()
#define List_1_ToArray_m4032239772(__this, method) ((  StatusListItemU5BU5D_t869095256* (*) (List_1_t4123291041 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::TrimExcess()
#define List_1_TrimExcess_m1600409916(__this, method) ((  void (*) (List_1_t4123291041 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::get_Capacity()
#define List_1_get_Capacity_m213534370(__this, method) ((  int32_t (*) (List_1_t4123291041 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4253669601(__this, ___value0, method) ((  void (*) (List_1_t4123291041 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<StatusListItem>::get_Count()
#define List_1_get_Count_m703733304(__this, method) ((  int32_t (*) (List_1_t4123291041 *, const MethodInfo*))List_1_get_Count_m2021003857_gshared)(__this, method)
// T System.Collections.Generic.List`1<StatusListItem>::get_Item(System.Int32)
#define List_1_get_Item_m165153529(__this, ___index0, method) ((  StatusListItem_t459202613 * (*) (List_1_t4123291041 *, int32_t, const MethodInfo*))List_1_get_Item_m1607739884_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<StatusListItem>::set_Item(System.Int32,T)
#define List_1_set_Item_m967746796(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4123291041 *, int32_t, StatusListItem_t459202613 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
