﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_Rainbow
struct CameraFilterPack_Gradients_Rainbow_t3966350505;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_Rainbow::.ctor()
extern "C"  void CameraFilterPack_Gradients_Rainbow__ctor_m502674178 (CameraFilterPack_Gradients_Rainbow_t3966350505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Rainbow::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_Rainbow_get_material_m147465701 (CameraFilterPack_Gradients_Rainbow_t3966350505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::Start()
extern "C"  void CameraFilterPack_Gradients_Rainbow_Start_m1865495882 (CameraFilterPack_Gradients_Rainbow_t3966350505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_Rainbow_OnRenderImage_m3501030498 (CameraFilterPack_Gradients_Rainbow_t3966350505 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::Update()
extern "C"  void CameraFilterPack_Gradients_Rainbow_Update_m1765900649 (CameraFilterPack_Gradients_Rainbow_t3966350505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::OnDisable()
extern "C"  void CameraFilterPack_Gradients_Rainbow_OnDisable_m1307255245 (CameraFilterPack_Gradients_Rainbow_t3966350505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
