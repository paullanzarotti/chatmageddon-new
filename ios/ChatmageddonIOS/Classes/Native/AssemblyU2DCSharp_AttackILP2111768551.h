﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3965135000.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackILP
struct  AttackILP_t2111768551  : public BaseInfiniteListPopulator_1_t3965135000
{
public:
	// UILabel AttackILP::noResultsLabel
	UILabel_t1795115428 * ___noResultsLabel_33;
	// System.Int32 AttackILP::maxNameLength
	int32_t ___maxNameLength_34;
	// System.Boolean AttackILP::listLoaded
	bool ___listLoaded_35;
	// System.Collections.ArrayList AttackILP::attackArray
	ArrayList_t4252133567 * ___attackArray_36;
	// System.Boolean AttackILP::outerTargetList
	bool ___outerTargetList_37;
	// System.Boolean AttackILP::searchList
	bool ___searchList_38;
	// System.Boolean AttackILP::globalSearchList
	bool ___globalSearchList_39;
	// System.Boolean AttackILP::resetSearch
	bool ___resetSearch_40;

public:
	inline static int32_t get_offset_of_noResultsLabel_33() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___noResultsLabel_33)); }
	inline UILabel_t1795115428 * get_noResultsLabel_33() const { return ___noResultsLabel_33; }
	inline UILabel_t1795115428 ** get_address_of_noResultsLabel_33() { return &___noResultsLabel_33; }
	inline void set_noResultsLabel_33(UILabel_t1795115428 * value)
	{
		___noResultsLabel_33 = value;
		Il2CppCodeGenWriteBarrier(&___noResultsLabel_33, value);
	}

	inline static int32_t get_offset_of_maxNameLength_34() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___maxNameLength_34)); }
	inline int32_t get_maxNameLength_34() const { return ___maxNameLength_34; }
	inline int32_t* get_address_of_maxNameLength_34() { return &___maxNameLength_34; }
	inline void set_maxNameLength_34(int32_t value)
	{
		___maxNameLength_34 = value;
	}

	inline static int32_t get_offset_of_listLoaded_35() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___listLoaded_35)); }
	inline bool get_listLoaded_35() const { return ___listLoaded_35; }
	inline bool* get_address_of_listLoaded_35() { return &___listLoaded_35; }
	inline void set_listLoaded_35(bool value)
	{
		___listLoaded_35 = value;
	}

	inline static int32_t get_offset_of_attackArray_36() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___attackArray_36)); }
	inline ArrayList_t4252133567 * get_attackArray_36() const { return ___attackArray_36; }
	inline ArrayList_t4252133567 ** get_address_of_attackArray_36() { return &___attackArray_36; }
	inline void set_attackArray_36(ArrayList_t4252133567 * value)
	{
		___attackArray_36 = value;
		Il2CppCodeGenWriteBarrier(&___attackArray_36, value);
	}

	inline static int32_t get_offset_of_outerTargetList_37() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___outerTargetList_37)); }
	inline bool get_outerTargetList_37() const { return ___outerTargetList_37; }
	inline bool* get_address_of_outerTargetList_37() { return &___outerTargetList_37; }
	inline void set_outerTargetList_37(bool value)
	{
		___outerTargetList_37 = value;
	}

	inline static int32_t get_offset_of_searchList_38() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___searchList_38)); }
	inline bool get_searchList_38() const { return ___searchList_38; }
	inline bool* get_address_of_searchList_38() { return &___searchList_38; }
	inline void set_searchList_38(bool value)
	{
		___searchList_38 = value;
	}

	inline static int32_t get_offset_of_globalSearchList_39() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___globalSearchList_39)); }
	inline bool get_globalSearchList_39() const { return ___globalSearchList_39; }
	inline bool* get_address_of_globalSearchList_39() { return &___globalSearchList_39; }
	inline void set_globalSearchList_39(bool value)
	{
		___globalSearchList_39 = value;
	}

	inline static int32_t get_offset_of_resetSearch_40() { return static_cast<int32_t>(offsetof(AttackILP_t2111768551, ___resetSearch_40)); }
	inline bool get_resetSearch_40() const { return ___resetSearch_40; }
	inline bool* get_address_of_resetSearch_40() { return &___resetSearch_40; }
	inline void set_resetSearch_40(bool value)
	{
		___resetSearch_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
