﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnibillError>
struct List_1_t1122980919;
// System.Collections.Generic.IEnumerable`1<UnibillError>
struct IEnumerable_1_t2045986832;
// UnibillError[]
struct UnibillErrorU5BU5D_t4092036522;
// System.Collections.Generic.IEnumerator`1<UnibillError>
struct IEnumerator_1_t3524350910;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnibillError>
struct ICollection_1_t2705935092;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnibillError>
struct ReadOnlyCollection_1_t1939645479;
// System.Predicate`1<UnibillError>
struct Predicate_1_t196829902;
// System.Action`1<UnibillError>
struct Action_1_t1555659169;
// System.Collections.Generic.IComparer`1<UnibillError>
struct IComparer_1_t4003290205;
// System.Comparison`1<UnibillError>
struct Comparison_1_t3015598638;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat657710593.h"

// System.Void System.Collections.Generic.List`1<UnibillError>::.ctor()
extern "C"  void List_1__ctor_m2645845486_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1__ctor_m2645845486(__this, method) ((  void (*) (List_1_t1122980919 *, const MethodInfo*))List_1__ctor_m2645845486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2246611914_gshared (List_1_t1122980919 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2246611914(__this, ___collection0, method) ((  void (*) (List_1_t1122980919 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2246611914_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m809677012_gshared (List_1_t1122980919 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m809677012(__this, ___capacity0, method) ((  void (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1__ctor_m809677012_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m2264210812_gshared (List_1_t1122980919 * __this, UnibillErrorU5BU5D_t4092036522* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m2264210812(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1122980919 *, UnibillErrorU5BU5D_t4092036522*, int32_t, const MethodInfo*))List_1__ctor_m2264210812_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::.cctor()
extern "C"  void List_1__cctor_m3062201856_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3062201856(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3062201856_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnibillError>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1422495783_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1422495783(__this, method) ((  Il2CppObject* (*) (List_1_t1122980919 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1422495783_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m110278699_gshared (List_1_t1122980919 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m110278699(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1122980919 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m110278699_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnibillError>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m272835218_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m272835218(__this, method) ((  Il2CppObject * (*) (List_1_t1122980919 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m272835218_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m238491611_gshared (List_1_t1122980919 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m238491611(__this, ___item0, method) ((  int32_t (*) (List_1_t1122980919 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m238491611_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1008606995_gshared (List_1_t1122980919 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1008606995(__this, ___item0, method) ((  bool (*) (List_1_t1122980919 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1008606995_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m876387441_gshared (List_1_t1122980919 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m876387441(__this, ___item0, method) ((  int32_t (*) (List_1_t1122980919 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m876387441_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2435019232_gshared (List_1_t1122980919 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2435019232(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1122980919 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2435019232_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m675582358_gshared (List_1_t1122980919 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m675582358(__this, ___item0, method) ((  void (*) (List_1_t1122980919 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m675582358_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m486417422_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m486417422(__this, method) ((  bool (*) (List_1_t1122980919 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m486417422_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1421992691_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1421992691(__this, method) ((  bool (*) (List_1_t1122980919 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1421992691_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnibillError>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m75912223_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m75912223(__this, method) ((  Il2CppObject * (*) (List_1_t1122980919 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m75912223_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3301313588_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3301313588(__this, method) ((  bool (*) (List_1_t1122980919 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3301313588_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3767599647_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3767599647(__this, method) ((  bool (*) (List_1_t1122980919 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3767599647_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2592527778_gshared (List_1_t1122980919 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2592527778(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2592527778_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m4159314517_gshared (List_1_t1122980919 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m4159314517(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1122980919 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m4159314517_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Add(T)
extern "C"  void List_1_Add_m2994502066_gshared (List_1_t1122980919 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2994502066(__this, ___item0, method) ((  void (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_Add_m2994502066_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1959098823_gshared (List_1_t1122980919 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1959098823(__this, ___newCount0, method) ((  void (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1959098823_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1473524604_gshared (List_1_t1122980919 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1473524604(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1122980919 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1473524604_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1980775423_gshared (List_1_t1122980919 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1980775423(__this, ___collection0, method) ((  void (*) (List_1_t1122980919 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1980775423_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m870878991_gshared (List_1_t1122980919 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m870878991(__this, ___enumerable0, method) ((  void (*) (List_1_t1122980919 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m870878991_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1758056954_gshared (List_1_t1122980919 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1758056954(__this, ___collection0, method) ((  void (*) (List_1_t1122980919 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1758056954_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnibillError>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1939645479 * List_1_AsReadOnly_m2005339727_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2005339727(__this, method) ((  ReadOnlyCollection_1_t1939645479 * (*) (List_1_t1122980919 *, const MethodInfo*))List_1_AsReadOnly_m2005339727_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Clear()
extern "C"  void List_1_Clear_m2547683752_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_Clear_m2547683752(__this, method) ((  void (*) (List_1_t1122980919 *, const MethodInfo*))List_1_Clear_m2547683752_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::Contains(T)
extern "C"  bool List_1_Contains_m4100484886_gshared (List_1_t1122980919 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m4100484886(__this, ___item0, method) ((  bool (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_Contains_m4100484886_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1855395495_gshared (List_1_t1122980919 * __this, UnibillErrorU5BU5D_t4092036522* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1855395495(__this, ___array0, method) ((  void (*) (List_1_t1122980919 *, UnibillErrorU5BU5D_t4092036522*, const MethodInfo*))List_1_CopyTo_m1855395495_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3969304000_gshared (List_1_t1122980919 * __this, UnibillErrorU5BU5D_t4092036522* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3969304000(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1122980919 *, UnibillErrorU5BU5D_t4092036522*, int32_t, const MethodInfo*))List_1_CopyTo_m3969304000_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m2087991010_gshared (List_1_t1122980919 * __this, int32_t ___index0, UnibillErrorU5BU5D_t4092036522* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m2087991010(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t1122980919 *, int32_t, UnibillErrorU5BU5D_t4092036522*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m2087991010_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m4221280502_gshared (List_1_t1122980919 * __this, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_Exists_m4221280502(__this, ___match0, method) ((  bool (*) (List_1_t1122980919 *, Predicate_1_t196829902 *, const MethodInfo*))List_1_Exists_m4221280502_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<UnibillError>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1564505122_gshared (List_1_t1122980919 * __this, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_Find_m1564505122(__this, ___match0, method) ((  int32_t (*) (List_1_t1122980919 *, Predicate_1_t196829902 *, const MethodInfo*))List_1_Find_m1564505122_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3257902779_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3257902779(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t196829902 *, const MethodInfo*))List_1_CheckMatch_m3257902779_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnibillError>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t1122980919 * List_1_FindAll_m1623638123_gshared (List_1_t1122980919 * __this, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m1623638123(__this, ___match0, method) ((  List_1_t1122980919 * (*) (List_1_t1122980919 *, Predicate_1_t196829902 *, const MethodInfo*))List_1_FindAll_m1623638123_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnibillError>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t1122980919 * List_1_FindAllStackBits_m1262999861_gshared (List_1_t1122980919 * __this, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1262999861(__this, ___match0, method) ((  List_1_t1122980919 * (*) (List_1_t1122980919 *, Predicate_1_t196829902 *, const MethodInfo*))List_1_FindAllStackBits_m1262999861_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnibillError>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t1122980919 * List_1_FindAllList_m1691566533_gshared (List_1_t1122980919 * __this, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m1691566533(__this, ___match0, method) ((  List_1_t1122980919 * (*) (List_1_t1122980919 *, Predicate_1_t196829902 *, const MethodInfo*))List_1_FindAllList_m1691566533_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m2168283603_gshared (List_1_t1122980919 * __this, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m2168283603(__this, ___match0, method) ((  int32_t (*) (List_1_t1122980919 *, Predicate_1_t196829902 *, const MethodInfo*))List_1_FindIndex_m2168283603_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m827546236_gshared (List_1_t1122980919 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t196829902 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m827546236(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1122980919 *, int32_t, int32_t, Predicate_1_t196829902 *, const MethodInfo*))List_1_GetIndex_m827546236_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1156605365_gshared (List_1_t1122980919 * __this, Action_1_t1555659169 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1156605365(__this, ___action0, method) ((  void (*) (List_1_t1122980919 *, Action_1_t1555659169 *, const MethodInfo*))List_1_ForEach_m1156605365_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnibillError>::GetEnumerator()
extern "C"  Enumerator_t657710593  List_1_GetEnumerator_m1651415423_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1651415423(__this, method) ((  Enumerator_t657710593  (*) (List_1_t1122980919 *, const MethodInfo*))List_1_GetEnumerator_m1651415423_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m556771382_gshared (List_1_t1122980919 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m556771382(__this, ___item0, method) ((  int32_t (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_IndexOf_m556771382_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3188637839_gshared (List_1_t1122980919 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3188637839(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1122980919 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3188637839_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2268249480_gshared (List_1_t1122980919 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2268249480(__this, ___index0, method) ((  void (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2268249480_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m314726737_gshared (List_1_t1122980919 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m314726737(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1122980919 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m314726737_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3523326882_gshared (List_1_t1122980919 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3523326882(__this, ___collection0, method) ((  void (*) (List_1_t1122980919 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3523326882_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnibillError>::Remove(T)
extern "C"  bool List_1_Remove_m1671415505_gshared (List_1_t1122980919 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m1671415505(__this, ___item0, method) ((  bool (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_Remove_m1671415505_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2343767427_gshared (List_1_t1122980919 * __this, Predicate_1_t196829902 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2343767427(__this, ___match0, method) ((  int32_t (*) (List_1_t1122980919 *, Predicate_1_t196829902 *, const MethodInfo*))List_1_RemoveAll_m2343767427_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3734363573_gshared (List_1_t1122980919 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3734363573(__this, ___index0, method) ((  void (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3734363573_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1375412948_gshared (List_1_t1122980919 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1375412948(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1122980919 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1375412948_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Reverse()
extern "C"  void List_1_Reverse_m2705074287_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_Reverse_m2705074287(__this, method) ((  void (*) (List_1_t1122980919 *, const MethodInfo*))List_1_Reverse_m2705074287_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Sort()
extern "C"  void List_1_Sort_m435666609_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_Sort_m435666609(__this, method) ((  void (*) (List_1_t1122980919 *, const MethodInfo*))List_1_Sort_m435666609_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4097017117_gshared (List_1_t1122980919 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4097017117(__this, ___comparer0, method) ((  void (*) (List_1_t1122980919 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4097017117_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1522189496_gshared (List_1_t1122980919 * __this, Comparison_1_t3015598638 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1522189496(__this, ___comparison0, method) ((  void (*) (List_1_t1122980919 *, Comparison_1_t3015598638 *, const MethodInfo*))List_1_Sort_m1522189496_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnibillError>::ToArray()
extern "C"  UnibillErrorU5BU5D_t4092036522* List_1_ToArray_m555931760_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_ToArray_m555931760(__this, method) ((  UnibillErrorU5BU5D_t4092036522* (*) (List_1_t1122980919 *, const MethodInfo*))List_1_ToArray_m555931760_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1909654810_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1909654810(__this, method) ((  void (*) (List_1_t1122980919 *, const MethodInfo*))List_1_TrimExcess_m1909654810_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2608908344_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2608908344(__this, method) ((  int32_t (*) (List_1_t1122980919 *, const MethodInfo*))List_1_get_Capacity_m2608908344_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m797694271_gshared (List_1_t1122980919 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m797694271(__this, ___value0, method) ((  void (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_set_Capacity_m797694271_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnibillError>::get_Count()
extern "C"  int32_t List_1_get_Count_m1951175884_gshared (List_1_t1122980919 * __this, const MethodInfo* method);
#define List_1_get_Count_m1951175884(__this, method) ((  int32_t (*) (List_1_t1122980919 *, const MethodInfo*))List_1_get_Count_m1951175884_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnibillError>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1743526697_gshared (List_1_t1122980919 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1743526697(__this, ___index0, method) ((  int32_t (*) (List_1_t1122980919 *, int32_t, const MethodInfo*))List_1_get_Item_m1743526697_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnibillError>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2073537834_gshared (List_1_t1122980919 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m2073537834(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1122980919 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m2073537834_gshared)(__this, ___index0, ___value1, method)
