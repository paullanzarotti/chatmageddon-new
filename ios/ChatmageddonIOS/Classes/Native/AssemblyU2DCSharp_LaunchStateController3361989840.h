﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenAlpha
struct TweenAlpha_t2421518635;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchStateController
struct  LaunchStateController_t3361989840  : public MonoBehaviour_t1158329972
{
public:
	// TweenAlpha LaunchStateController::initateTween
	TweenAlpha_t2421518635 * ___initateTween_2;
	// TweenAlpha LaunchStateController::engageTween
	TweenAlpha_t2421518635 * ___engageTween_3;
	// TweenAlpha LaunchStateController::launchTween
	TweenAlpha_t2421518635 * ___launchTween_4;

public:
	inline static int32_t get_offset_of_initateTween_2() { return static_cast<int32_t>(offsetof(LaunchStateController_t3361989840, ___initateTween_2)); }
	inline TweenAlpha_t2421518635 * get_initateTween_2() const { return ___initateTween_2; }
	inline TweenAlpha_t2421518635 ** get_address_of_initateTween_2() { return &___initateTween_2; }
	inline void set_initateTween_2(TweenAlpha_t2421518635 * value)
	{
		___initateTween_2 = value;
		Il2CppCodeGenWriteBarrier(&___initateTween_2, value);
	}

	inline static int32_t get_offset_of_engageTween_3() { return static_cast<int32_t>(offsetof(LaunchStateController_t3361989840, ___engageTween_3)); }
	inline TweenAlpha_t2421518635 * get_engageTween_3() const { return ___engageTween_3; }
	inline TweenAlpha_t2421518635 ** get_address_of_engageTween_3() { return &___engageTween_3; }
	inline void set_engageTween_3(TweenAlpha_t2421518635 * value)
	{
		___engageTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___engageTween_3, value);
	}

	inline static int32_t get_offset_of_launchTween_4() { return static_cast<int32_t>(offsetof(LaunchStateController_t3361989840, ___launchTween_4)); }
	inline TweenAlpha_t2421518635 * get_launchTween_4() const { return ___launchTween_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_launchTween_4() { return &___launchTween_4; }
	inline void set_launchTween_4(TweenAlpha_t2421518635 * value)
	{
		___launchTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___launchTween_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
