﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3338879407MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PanelType,Panel,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m561693977(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2454139580 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m3410144461_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PanelType,Panel,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m35756533(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2454139580 *, int32_t, Panel_t1787746694 *, const MethodInfo*))Transform_1_Invoke_m1153697081_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PanelType,Panel,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1372826000(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2454139580 *, int32_t, Panel_t1787746694 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m4181367104_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PanelType,Panel,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m670750463(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2454139580 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1268706543_gshared)(__this, ___result0, method)
