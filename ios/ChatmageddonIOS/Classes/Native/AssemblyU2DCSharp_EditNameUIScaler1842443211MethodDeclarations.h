﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditNameUIScaler
struct EditNameUIScaler_t1842443211;

#include "codegen/il2cpp-codegen.h"

// System.Void EditNameUIScaler::.ctor()
extern "C"  void EditNameUIScaler__ctor_m2115795624 (EditNameUIScaler_t1842443211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditNameUIScaler::GlobalUIScale()
extern "C"  void EditNameUIScaler_GlobalUIScale_m1996728509 (EditNameUIScaler_t1842443211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
