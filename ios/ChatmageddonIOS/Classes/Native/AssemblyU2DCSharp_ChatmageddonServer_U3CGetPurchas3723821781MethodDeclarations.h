﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetPurchasableItems>c__AnonStorey32
struct U3CGetPurchasableItemsU3Ec__AnonStorey32_t3723821781;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetPurchasableItems>c__AnonStorey32::.ctor()
extern "C"  void U3CGetPurchasableItemsU3Ec__AnonStorey32__ctor_m2218000576 (U3CGetPurchasableItemsU3Ec__AnonStorey32_t3723821781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetPurchasableItems>c__AnonStorey32::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetPurchasableItemsU3Ec__AnonStorey32_U3CU3Em__0_m750439385 (U3CGetPurchasableItemsU3Ec__AnonStorey32_t3723821781 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
