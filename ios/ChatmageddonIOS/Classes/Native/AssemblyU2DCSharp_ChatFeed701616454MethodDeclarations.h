﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatFeed
struct ChatFeed_t701616454;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharp_ChatUpdateType1225593925.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatFeed::.ctor()
extern "C"  void ChatFeed__ctor_m1740520347 (ChatFeed_t701616454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::InitFeed()
extern "C"  void ChatFeed_InitFeed_m2610493279 (ChatFeed_t701616454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::GetFeedInfo()
extern "C"  void ChatFeed_GetFeedInfo_m3208220089 (ChatFeed_t701616454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::ProcessFeedInfo(System.Collections.Hashtable)
extern "C"  void ChatFeed_ProcessFeedInfo_m69788292 (ChatFeed_t701616454 * __this, Hashtable_t909839986 * ___infoTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChatUpdateType ChatFeed::GetTypeFromString(System.String)
extern "C"  int32_t ChatFeed_GetTypeFromString_m4189322196 (ChatFeed_t701616454 * __this, String_t* ___typeString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::ProcessUpdate(ChatUpdateType,System.Collections.Hashtable)
extern "C"  void ChatFeed_ProcessUpdate_m700265458 (ChatFeed_t701616454 * __this, int32_t ___type0, Hashtable_t909839986 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::NewMessage(System.Collections.Hashtable)
extern "C"  void ChatFeed_NewMessage_m2917927700 (ChatFeed_t701616454 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::<GetFeedInfo>__BaseCallProxy0()
extern "C"  void ChatFeed_U3CGetFeedInfoU3E__BaseCallProxy0_m2805176780 (ChatFeed_t701616454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::<GetFeedInfo>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatFeed_U3CGetFeedInfoU3Em__0_m2359428611 (ChatFeed_t701616454 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatFeed::<NewMessage>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatFeed_U3CNewMessageU3Em__1_m4149701395 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
