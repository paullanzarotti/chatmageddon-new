﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_BlueScreen
struct CameraFilterPack_Blend2Camera_BlueScreen_t3336107930;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_BlueScreen::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_BlueScreen__ctor_m245749679 (CameraFilterPack_Blend2Camera_BlueScreen_t3336107930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_BlueScreen::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_BlueScreen_get_material_m2016924760 (CameraFilterPack_Blend2Camera_BlueScreen_t3336107930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::Start()
extern "C"  void CameraFilterPack_Blend2Camera_BlueScreen_Start_m4253937723 (CameraFilterPack_Blend2Camera_BlueScreen_t3336107930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_BlueScreen_OnRenderImage_m3472731107 (CameraFilterPack_Blend2Camera_BlueScreen_t3336107930 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::Update()
extern "C"  void CameraFilterPack_Blend2Camera_BlueScreen_Update_m2181415208 (CameraFilterPack_Blend2Camera_BlueScreen_t3336107930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_BlueScreen_OnEnable_m3074937179 (CameraFilterPack_Blend2Camera_BlueScreen_t3336107930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_BlueScreen_OnDisable_m96871606 (CameraFilterPack_Blend2Camera_BlueScreen_t3336107930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
