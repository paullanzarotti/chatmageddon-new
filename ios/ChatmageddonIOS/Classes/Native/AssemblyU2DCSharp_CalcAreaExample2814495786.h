﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// OnlineMaps
struct OnlineMaps_t1893290312;
// System.Collections.Generic.List`1<OnlineMapsMarker>
struct List_1_t2861287814;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// OnlineMapsDrawingPoly
struct OnlineMapsDrawingPoly_t2218936412;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CalcAreaExample
struct  CalcAreaExample_t2814495786  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D CalcAreaExample::markerTexture
	Texture2D_t3542995729 * ___markerTexture_2;
	// OnlineMaps CalcAreaExample::api
	OnlineMaps_t1893290312 * ___api_3;
	// System.Boolean CalcAreaExample::changed
	bool ___changed_4;
	// System.Collections.Generic.List`1<OnlineMapsMarker> CalcAreaExample::markers
	List_1_t2861287814 * ___markers_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> CalcAreaExample::markerPositions
	List_1_t1612828711 * ___markerPositions_6;
	// OnlineMapsDrawingPoly CalcAreaExample::polygon
	OnlineMapsDrawingPoly_t2218936412 * ___polygon_7;

public:
	inline static int32_t get_offset_of_markerTexture_2() { return static_cast<int32_t>(offsetof(CalcAreaExample_t2814495786, ___markerTexture_2)); }
	inline Texture2D_t3542995729 * get_markerTexture_2() const { return ___markerTexture_2; }
	inline Texture2D_t3542995729 ** get_address_of_markerTexture_2() { return &___markerTexture_2; }
	inline void set_markerTexture_2(Texture2D_t3542995729 * value)
	{
		___markerTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___markerTexture_2, value);
	}

	inline static int32_t get_offset_of_api_3() { return static_cast<int32_t>(offsetof(CalcAreaExample_t2814495786, ___api_3)); }
	inline OnlineMaps_t1893290312 * get_api_3() const { return ___api_3; }
	inline OnlineMaps_t1893290312 ** get_address_of_api_3() { return &___api_3; }
	inline void set_api_3(OnlineMaps_t1893290312 * value)
	{
		___api_3 = value;
		Il2CppCodeGenWriteBarrier(&___api_3, value);
	}

	inline static int32_t get_offset_of_changed_4() { return static_cast<int32_t>(offsetof(CalcAreaExample_t2814495786, ___changed_4)); }
	inline bool get_changed_4() const { return ___changed_4; }
	inline bool* get_address_of_changed_4() { return &___changed_4; }
	inline void set_changed_4(bool value)
	{
		___changed_4 = value;
	}

	inline static int32_t get_offset_of_markers_5() { return static_cast<int32_t>(offsetof(CalcAreaExample_t2814495786, ___markers_5)); }
	inline List_1_t2861287814 * get_markers_5() const { return ___markers_5; }
	inline List_1_t2861287814 ** get_address_of_markers_5() { return &___markers_5; }
	inline void set_markers_5(List_1_t2861287814 * value)
	{
		___markers_5 = value;
		Il2CppCodeGenWriteBarrier(&___markers_5, value);
	}

	inline static int32_t get_offset_of_markerPositions_6() { return static_cast<int32_t>(offsetof(CalcAreaExample_t2814495786, ___markerPositions_6)); }
	inline List_1_t1612828711 * get_markerPositions_6() const { return ___markerPositions_6; }
	inline List_1_t1612828711 ** get_address_of_markerPositions_6() { return &___markerPositions_6; }
	inline void set_markerPositions_6(List_1_t1612828711 * value)
	{
		___markerPositions_6 = value;
		Il2CppCodeGenWriteBarrier(&___markerPositions_6, value);
	}

	inline static int32_t get_offset_of_polygon_7() { return static_cast<int32_t>(offsetof(CalcAreaExample_t2814495786, ___polygon_7)); }
	inline OnlineMapsDrawingPoly_t2218936412 * get_polygon_7() const { return ___polygon_7; }
	inline OnlineMapsDrawingPoly_t2218936412 ** get_address_of_polygon_7() { return &___polygon_7; }
	inline void set_polygon_7(OnlineMapsDrawingPoly_t2218936412 * value)
	{
		___polygon_7 = value;
		Il2CppCodeGenWriteBarrier(&___polygon_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
