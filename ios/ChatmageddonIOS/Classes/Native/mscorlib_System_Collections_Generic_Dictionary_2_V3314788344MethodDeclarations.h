﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>
struct ValueCollection_t3314788344;
// System.Collections.Generic.Dictionary`2<System.Object,System.Decimal>
struct Dictionary_2_t316761205;
// System.Collections.Generic.IEnumerator`1<System.Decimal>
struct IEnumerator_1_t2495192200;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Decimal[]
struct DecimalU5BU5D_t624008824;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2003293969.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2978730160_gshared (ValueCollection_t3314788344 * __this, Dictionary_2_t316761205 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2978730160(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3314788344 *, Dictionary_2_t316761205 *, const MethodInfo*))ValueCollection__ctor_m2978730160_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3555081710_gshared (ValueCollection_t3314788344 * __this, Decimal_t724701077  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3555081710(__this, ___item0, method) ((  void (*) (ValueCollection_t3314788344 *, Decimal_t724701077 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3555081710_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3847838873_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3847838873(__this, method) ((  void (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3847838873_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2125951286_gshared (ValueCollection_t3314788344 * __this, Decimal_t724701077  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2125951286(__this, ___item0, method) ((  bool (*) (ValueCollection_t3314788344 *, Decimal_t724701077 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2125951286_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m315837893_gshared (ValueCollection_t3314788344 * __this, Decimal_t724701077  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m315837893(__this, ___item0, method) ((  bool (*) (ValueCollection_t3314788344 *, Decimal_t724701077 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m315837893_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1103358091_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1103358091(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1103358091_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m618479283_gshared (ValueCollection_t3314788344 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m618479283(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3314788344 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m618479283_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2554823098_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2554823098(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2554823098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4067432401_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4067432401(__this, method) ((  bool (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4067432401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3490313731_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3490313731(__this, method) ((  bool (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3490313731_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3115208567_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3115208567(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3115208567_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3289163821_gshared (ValueCollection_t3314788344 * __this, DecimalU5BU5D_t624008824* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3289163821(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3314788344 *, DecimalU5BU5D_t624008824*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3289163821_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::GetEnumerator()
extern "C"  Enumerator_t2003293969  ValueCollection_GetEnumerator_m1421054152_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1421054152(__this, method) ((  Enumerator_t2003293969  (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_GetEnumerator_m1421054152_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Decimal>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m375045227_gshared (ValueCollection_t3314788344 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m375045227(__this, method) ((  int32_t (*) (ValueCollection_t3314788344 *, const MethodInfo*))ValueCollection_get_Count_m375045227_gshared)(__this, method)
