﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_CircularProgressBar2980095063.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldBarController
struct  ShieldBarController_t264539450  : public CircularProgressBar_t2980095063
{
public:
	// UnityEngine.Color ShieldBarController::bronzeColour
	Color_t2020392075  ___bronzeColour_13;
	// UnityEngine.Color ShieldBarController::silverColour
	Color_t2020392075  ___silverColour_14;
	// UnityEngine.Color ShieldBarController::goldColour
	Color_t2020392075  ___goldColour_15;
	// UnityEngine.Color ShieldBarController::inactiveColour
	Color_t2020392075  ___inactiveColour_16;
	// UISprite ShieldBarController::shieldIcon
	UISprite_t603616735 * ___shieldIcon_17;
	// UILabel ShieldBarController::timeLabel
	UILabel_t1795115428 * ___timeLabel_18;

public:
	inline static int32_t get_offset_of_bronzeColour_13() { return static_cast<int32_t>(offsetof(ShieldBarController_t264539450, ___bronzeColour_13)); }
	inline Color_t2020392075  get_bronzeColour_13() const { return ___bronzeColour_13; }
	inline Color_t2020392075 * get_address_of_bronzeColour_13() { return &___bronzeColour_13; }
	inline void set_bronzeColour_13(Color_t2020392075  value)
	{
		___bronzeColour_13 = value;
	}

	inline static int32_t get_offset_of_silverColour_14() { return static_cast<int32_t>(offsetof(ShieldBarController_t264539450, ___silverColour_14)); }
	inline Color_t2020392075  get_silverColour_14() const { return ___silverColour_14; }
	inline Color_t2020392075 * get_address_of_silverColour_14() { return &___silverColour_14; }
	inline void set_silverColour_14(Color_t2020392075  value)
	{
		___silverColour_14 = value;
	}

	inline static int32_t get_offset_of_goldColour_15() { return static_cast<int32_t>(offsetof(ShieldBarController_t264539450, ___goldColour_15)); }
	inline Color_t2020392075  get_goldColour_15() const { return ___goldColour_15; }
	inline Color_t2020392075 * get_address_of_goldColour_15() { return &___goldColour_15; }
	inline void set_goldColour_15(Color_t2020392075  value)
	{
		___goldColour_15 = value;
	}

	inline static int32_t get_offset_of_inactiveColour_16() { return static_cast<int32_t>(offsetof(ShieldBarController_t264539450, ___inactiveColour_16)); }
	inline Color_t2020392075  get_inactiveColour_16() const { return ___inactiveColour_16; }
	inline Color_t2020392075 * get_address_of_inactiveColour_16() { return &___inactiveColour_16; }
	inline void set_inactiveColour_16(Color_t2020392075  value)
	{
		___inactiveColour_16 = value;
	}

	inline static int32_t get_offset_of_shieldIcon_17() { return static_cast<int32_t>(offsetof(ShieldBarController_t264539450, ___shieldIcon_17)); }
	inline UISprite_t603616735 * get_shieldIcon_17() const { return ___shieldIcon_17; }
	inline UISprite_t603616735 ** get_address_of_shieldIcon_17() { return &___shieldIcon_17; }
	inline void set_shieldIcon_17(UISprite_t603616735 * value)
	{
		___shieldIcon_17 = value;
		Il2CppCodeGenWriteBarrier(&___shieldIcon_17, value);
	}

	inline static int32_t get_offset_of_timeLabel_18() { return static_cast<int32_t>(offsetof(ShieldBarController_t264539450, ___timeLabel_18)); }
	inline UILabel_t1795115428 * get_timeLabel_18() const { return ___timeLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_18() { return &___timeLabel_18; }
	inline void set_timeLabel_18(UILabel_t1795115428 * value)
	{
		___timeLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
