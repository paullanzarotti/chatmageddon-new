﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserLastName>c__AnonStorey14
struct U3CUpdateUserLastNameU3Ec__AnonStorey14_t3340540426;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserLastName>c__AnonStorey14::.ctor()
extern "C"  void U3CUpdateUserLastNameU3Ec__AnonStorey14__ctor_m681046145 (U3CUpdateUserLastNameU3Ec__AnonStorey14_t3340540426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserLastName>c__AnonStorey14::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserLastNameU3Ec__AnonStorey14_U3CU3Em__0_m4211675934 (U3CUpdateUserLastNameU3Ec__AnonStorey14_t3340540426 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
