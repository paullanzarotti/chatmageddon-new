﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModalUI
struct ModalUI_t2568752073;

#include "codegen/il2cpp-codegen.h"

// System.Void ModalUI::.ctor()
extern "C"  void ModalUI__ctor_m1899658786 (ModalUI_t2568752073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalUI::OnUIActivate()
extern "C"  void ModalUI_OnUIActivate_m3846873850 (ModalUI_t2568752073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalUI::OnUIDeactivate()
extern "C"  void ModalUI_OnUIDeactivate_m2023711297 (ModalUI_t2568752073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalUI::OnUICleared()
extern "C"  void ModalUI_OnUICleared_m3043742517 (ModalUI_t2568752073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalUI::SetUIScene()
extern "C"  void ModalUI_SetUIScene_m3360526794 (ModalUI_t2568752073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
