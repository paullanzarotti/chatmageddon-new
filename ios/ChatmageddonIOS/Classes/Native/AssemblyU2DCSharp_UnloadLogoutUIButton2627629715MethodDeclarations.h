﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnloadLogoutUIButton
struct UnloadLogoutUIButton_t2627629715;

#include "codegen/il2cpp-codegen.h"

// System.Void UnloadLogoutUIButton::.ctor()
extern "C"  void UnloadLogoutUIButton__ctor_m493960578 (UnloadLogoutUIButton_t2627629715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnloadLogoutUIButton::OnButtonClick()
extern "C"  void UnloadLogoutUIButton_OnButtonClick_m396652151 (UnloadLogoutUIButton_t2627629715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
