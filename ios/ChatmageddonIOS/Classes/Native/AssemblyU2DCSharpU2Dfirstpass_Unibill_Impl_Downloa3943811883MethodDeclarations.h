﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.DownloadManager/<onDownloadFailedPermanently>c__AnonStorey4
struct U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883;

#include "codegen/il2cpp-codegen.h"

// System.Void Unibill.Impl.DownloadManager/<onDownloadFailedPermanently>c__AnonStorey4::.ctor()
extern "C"  void U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4__ctor_m3059936530 (U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<onDownloadFailedPermanently>c__AnonStorey4::<>m__0()
extern "C"  void U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_U3CU3Em__0_m4264905675 (U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
