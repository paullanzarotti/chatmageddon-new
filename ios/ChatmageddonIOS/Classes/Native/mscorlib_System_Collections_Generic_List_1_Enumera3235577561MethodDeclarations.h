﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AttackNavScreen>
struct List_1_t3700847887;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3235577561.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AttackNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3574119824_gshared (Enumerator_t3235577561 * __this, List_1_t3700847887 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3574119824(__this, ___l0, method) ((  void (*) (Enumerator_t3235577561 *, List_1_t3700847887 *, const MethodInfo*))Enumerator__ctor_m3574119824_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2521466066_gshared (Enumerator_t3235577561 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2521466066(__this, method) ((  void (*) (Enumerator_t3235577561 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2521466066_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AttackNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3120774454_gshared (Enumerator_t3235577561 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3120774454(__this, method) ((  Il2CppObject * (*) (Enumerator_t3235577561 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3120774454_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m1733241827_gshared (Enumerator_t3235577561 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1733241827(__this, method) ((  void (*) (Enumerator_t3235577561 *, const MethodInfo*))Enumerator_Dispose_m1733241827_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttackNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2356986766_gshared (Enumerator_t3235577561 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2356986766(__this, method) ((  void (*) (Enumerator_t3235577561 *, const MethodInfo*))Enumerator_VerifyState_m2356986766_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AttackNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1816643586_gshared (Enumerator_t3235577561 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1816643586(__this, method) ((  bool (*) (Enumerator_t3235577561 *, const MethodInfo*))Enumerator_MoveNext_m1816643586_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AttackNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2033465563_gshared (Enumerator_t3235577561 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2033465563(__this, method) ((  int32_t (*) (Enumerator_t3235577561 *, const MethodInfo*))Enumerator_get_Current_m2033465563_gshared)(__this, method)
