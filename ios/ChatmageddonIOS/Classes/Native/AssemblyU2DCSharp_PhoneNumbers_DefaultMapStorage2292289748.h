﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Func`2<System.String,System.Int32>
struct Func_2_t235684172;

#include "AssemblyU2DCSharp_PhoneNumbers_AreaCodeMapStorageS3709015996.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.DefaultMapStorage
struct  DefaultMapStorage_t2292289748  : public AreaCodeMapStorageStrategy_t3709015996
{
public:
	// System.Int32[] PhoneNumbers.DefaultMapStorage::phoneNumberPrefixes
	Int32U5BU5D_t3030399641* ___phoneNumberPrefixes_2;
	// System.String[] PhoneNumbers.DefaultMapStorage::descriptions
	StringU5BU5D_t1642385972* ___descriptions_3;

public:
	inline static int32_t get_offset_of_phoneNumberPrefixes_2() { return static_cast<int32_t>(offsetof(DefaultMapStorage_t2292289748, ___phoneNumberPrefixes_2)); }
	inline Int32U5BU5D_t3030399641* get_phoneNumberPrefixes_2() const { return ___phoneNumberPrefixes_2; }
	inline Int32U5BU5D_t3030399641** get_address_of_phoneNumberPrefixes_2() { return &___phoneNumberPrefixes_2; }
	inline void set_phoneNumberPrefixes_2(Int32U5BU5D_t3030399641* value)
	{
		___phoneNumberPrefixes_2 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumberPrefixes_2, value);
	}

	inline static int32_t get_offset_of_descriptions_3() { return static_cast<int32_t>(offsetof(DefaultMapStorage_t2292289748, ___descriptions_3)); }
	inline StringU5BU5D_t1642385972* get_descriptions_3() const { return ___descriptions_3; }
	inline StringU5BU5D_t1642385972** get_address_of_descriptions_3() { return &___descriptions_3; }
	inline void set_descriptions_3(StringU5BU5D_t1642385972* value)
	{
		___descriptions_3 = value;
		Il2CppCodeGenWriteBarrier(&___descriptions_3, value);
	}
};

struct DefaultMapStorage_t2292289748_StaticFields
{
public:
	// System.Func`2<System.String,System.Int32> PhoneNumbers.DefaultMapStorage::<>f__am$cache0
	Func_2_t235684172 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(DefaultMapStorage_t2292289748_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_t235684172 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_t235684172 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_t235684172 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
