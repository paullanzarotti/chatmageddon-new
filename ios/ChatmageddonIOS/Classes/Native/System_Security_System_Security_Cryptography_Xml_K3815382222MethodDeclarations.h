﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyInfoEncryptedKey
struct KeyInfoEncryptedKey_t3815382222;
// System.Security.Cryptography.Xml.EncryptedKey
struct EncryptedKey_t3186175257;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "System_Security_System_Security_Cryptography_Xml_E3186175257.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.KeyInfoEncryptedKey::.ctor()
extern "C"  void KeyInfoEncryptedKey__ctor_m1802297260 (KeyInfoEncryptedKey_t3815382222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoEncryptedKey::.ctor(System.Security.Cryptography.Xml.EncryptedKey)
extern "C"  void KeyInfoEncryptedKey__ctor_m121109539 (KeyInfoEncryptedKey_t3815382222 * __this, EncryptedKey_t3186175257 * ___ek0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.EncryptedKey System.Security.Cryptography.Xml.KeyInfoEncryptedKey::get_EncryptedKey()
extern "C"  EncryptedKey_t3186175257 * KeyInfoEncryptedKey_get_EncryptedKey_m2698960556 (KeyInfoEncryptedKey_t3815382222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoEncryptedKey::set_EncryptedKey(System.Security.Cryptography.Xml.EncryptedKey)
extern "C"  void KeyInfoEncryptedKey_set_EncryptedKey_m135092031 (KeyInfoEncryptedKey_t3815382222 * __this, EncryptedKey_t3186175257 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.KeyInfoEncryptedKey::GetXml()
extern "C"  XmlElement_t2877111883 * KeyInfoEncryptedKey_GetXml_m1633627981 (KeyInfoEncryptedKey_t3815382222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.KeyInfoEncryptedKey::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * KeyInfoEncryptedKey_GetXml_m2743755715 (KeyInfoEncryptedKey_t3815382222 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoEncryptedKey::LoadXml(System.Xml.XmlElement)
extern "C"  void KeyInfoEncryptedKey_LoadXml_m2753106668 (KeyInfoEncryptedKey_t3815382222 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
