﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.RBTree/Node
struct Node_t2499136326;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.RBTree/Node::.ctor()
extern "C"  void Node__ctor_m3246611644 (Node_t2499136326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.RBTree/Node::get_IsBlack()
extern "C"  bool Node_get_IsBlack_m2486375420 (Node_t2499136326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.RBTree/Node::set_IsBlack(System.Boolean)
extern "C"  void Node_set_IsBlack_m3482698563 (Node_t2499136326 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Collections.Generic.RBTree/Node::get_Size()
extern "C"  uint32_t Node_get_Size_m3884002931 (Node_t2499136326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.RBTree/Node::set_Size(System.UInt32)
extern "C"  void Node_set_Size_m2428994664 (Node_t2499136326 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Collections.Generic.RBTree/Node::FixSize()
extern "C"  uint32_t Node_FixSize_m4010501621 (Node_t2499136326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
