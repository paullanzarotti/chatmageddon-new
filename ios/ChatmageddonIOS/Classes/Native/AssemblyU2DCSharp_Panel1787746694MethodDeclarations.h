﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Panel
struct Panel_t1787746694;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Panel::.ctor()
extern "C"  void Panel__ctor_m1830699833 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Panel::get_panelActive()
extern "C"  bool Panel_get_panelActive_m2753537268 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Panel::TogglePanel()
extern "C"  void Panel_TogglePanel_m1989136485 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Panel::OpenPanel()
extern "C"  void Panel_OpenPanel_m967030359 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Panel::ClosePanel()
extern "C"  void Panel_ClosePanel_m1609359589 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Panel::CheckIfTweening()
extern "C"  bool Panel_CheckIfTweening_m3570359093 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Panel::OpenPanelCoroutine()
extern "C"  Il2CppObject * Panel_OpenPanelCoroutine_m3143862635 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Panel::ClosePanelCoroutine()
extern "C"  Il2CppObject * Panel_ClosePanelCoroutine_m3561487709 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Panel::OnCloseFinished()
extern "C"  void Panel_OnCloseFinished_m2746184868 (Panel_t1787746694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Panel::PopulateView(System.String)
extern "C"  void Panel_PopulateView_m1130033072 (Panel_t1787746694 * __this, String_t* ___playerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
