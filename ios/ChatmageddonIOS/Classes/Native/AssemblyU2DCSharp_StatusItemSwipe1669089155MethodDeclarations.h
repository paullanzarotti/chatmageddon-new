﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusItemSwipe
struct StatusItemSwipe_t1669089155;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void StatusItemSwipe::.ctor()
extern "C"  void StatusItemSwipe__ctor_m256556180 (StatusItemSwipe_t1669089155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusItemSwipe::Start()
extern "C"  void StatusItemSwipe_Start_m1154366984 (StatusItemSwipe_t1669089155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusItemSwipe::PosTweenFinished()
extern "C"  void StatusItemSwipe_PosTweenFinished_m4229628377 (StatusItemSwipe_t1669089155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusItemSwipe::ValidateSwipeLeft()
extern "C"  bool StatusItemSwipe_ValidateSwipeLeft_m2024861539 (StatusItemSwipe_t1669089155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusItemSwipe::OnSwipeLeft()
extern "C"  void StatusItemSwipe_OnSwipeLeft_m1969716400 (StatusItemSwipe_t1669089155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusItemSwipe::ValidateSwipeRight()
extern "C"  bool StatusItemSwipe_ValidateSwipeRight_m1172925654 (StatusItemSwipe_t1669089155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusItemSwipe::OnSwipeRight()
extern "C"  void StatusItemSwipe_OnSwipeRight_m4015644943 (StatusItemSwipe_t1669089155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusItemSwipe::OnDragUp(UnityEngine.Vector2)
extern "C"  void StatusItemSwipe_OnDragUp_m878101696 (StatusItemSwipe_t1669089155 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
