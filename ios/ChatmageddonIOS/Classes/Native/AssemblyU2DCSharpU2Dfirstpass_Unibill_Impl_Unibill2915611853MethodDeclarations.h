﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// System.String
struct String_t;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.Collections.Generic.List`1<Unibill.ProductDefinition>
struct List_1_t888775120;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// PurchasableItem
struct PurchasableItem_t3963353899;
// VirtualCurrency
struct VirtualCurrency_t1115497880;
// System.Collections.Generic.List`1<PurchasableItem>
struct List_1_t3332475031;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Samsung2745005863.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void Unibill.Impl.UnibillConfiguration::.ctor(System.String,UnityEngine.RuntimePlatform,Uniject.ILogger,System.Collections.Generic.List`1<Unibill.ProductDefinition>)
extern "C"  void UnibillConfiguration__ctor_m3090382601 (UnibillConfiguration_t2915611853 * __this, String_t* ___json0, int32_t ___runtimePlatform1, Il2CppObject * ___logger2, List_1_t888775120 * ___runtimeProducts3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.BillingPlatform Unibill.Impl.UnibillConfiguration::get_CurrentPlatform()
extern "C"  int32_t UnibillConfiguration_get_CurrentPlatform_m4037513048 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_CurrentPlatform(Unibill.Impl.BillingPlatform)
extern "C"  void UnibillConfiguration_set_CurrentPlatform_m1609252739 (UnibillConfiguration_t2915611853 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnibillConfiguration::get_iOSSKU()
extern "C"  String_t* UnibillConfiguration_get_iOSSKU_m4042142969 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_iOSSKU(System.String)
extern "C"  void UnibillConfiguration_set_iOSSKU_m414286886 (UnibillConfiguration_t2915611853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnibillConfiguration::get_macAppStoreSKU()
extern "C"  String_t* UnibillConfiguration_get_macAppStoreSKU_m955546377 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_macAppStoreSKU(System.String)
extern "C"  void UnibillConfiguration_set_macAppStoreSKU_m931408424 (UnibillConfiguration_t2915611853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.BillingPlatform Unibill.Impl.UnibillConfiguration::get_AndroidBillingPlatform()
extern "C"  int32_t UnibillConfiguration_get_AndroidBillingPlatform_m3380558669 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_AndroidBillingPlatform(Unibill.Impl.BillingPlatform)
extern "C"  void UnibillConfiguration_set_AndroidBillingPlatform_m4100085162 (UnibillConfiguration_t2915611853 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnibillConfiguration::get_GooglePlayPublicKey()
extern "C"  String_t* UnibillConfiguration_get_GooglePlayPublicKey_m2203634566 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_GooglePlayPublicKey(System.String)
extern "C"  void UnibillConfiguration_set_GooglePlayPublicKey_m1600510417 (UnibillConfiguration_t2915611853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::get_AmazonSandboxEnabled()
extern "C"  bool UnibillConfiguration_get_AmazonSandboxEnabled_m3521250704 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_AmazonSandboxEnabled(System.Boolean)
extern "C"  void UnibillConfiguration_set_AmazonSandboxEnabled_m1544433081 (UnibillConfiguration_t2915611853 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::get_WP8SandboxEnabled()
extern "C"  bool UnibillConfiguration_get_WP8SandboxEnabled_m461959047 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_WP8SandboxEnabled(System.Boolean)
extern "C"  void UnibillConfiguration_set_WP8SandboxEnabled_m1279071348 (UnibillConfiguration_t2915611853 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::get_UseHostedConfig()
extern "C"  bool UnibillConfiguration_get_UseHostedConfig_m3080187480 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_UseHostedConfig(System.Boolean)
extern "C"  void UnibillConfiguration_set_UseHostedConfig_m3944337775 (UnibillConfiguration_t2915611853 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnibillConfiguration::get_HostedConfigUrl()
extern "C"  String_t* UnibillConfiguration_get_HostedConfigUrl_m783478905 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_HostedConfigUrl(System.String)
extern "C"  void UnibillConfiguration_set_HostedConfigUrl_m1130590292 (UnibillConfiguration_t2915611853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnibillConfiguration::get_UnibillAnalyticsAppId()
extern "C"  String_t* UnibillConfiguration_get_UnibillAnalyticsAppId_m2007170196 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_UnibillAnalyticsAppId(System.String)
extern "C"  void UnibillConfiguration_set_UnibillAnalyticsAppId_m2882083741 (UnibillConfiguration_t2915611853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnibillConfiguration::get_UnibillAnalyticsAppSecret()
extern "C"  String_t* UnibillConfiguration_get_UnibillAnalyticsAppSecret_m2379924029 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_UnibillAnalyticsAppSecret(System.String)
extern "C"  void UnibillConfiguration_set_UnibillAnalyticsAppSecret_m3935399302 (UnibillConfiguration_t2915611853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::get_UseWin8_1Sandbox()
extern "C"  bool UnibillConfiguration_get_UseWin8_1Sandbox_m5980812 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_UseWin8_1Sandbox(System.Boolean)
extern "C"  void UnibillConfiguration_set_UseWin8_1Sandbox_m1091154531 (UnibillConfiguration_t2915611853 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.SamsungAppsMode Unibill.Impl.UnibillConfiguration::get_SamsungAppsMode()
extern "C"  int32_t UnibillConfiguration_get_SamsungAppsMode_m1644672800 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_SamsungAppsMode(Unibill.Impl.SamsungAppsMode)
extern "C"  void UnibillConfiguration_set_SamsungAppsMode_m3948689085 (UnibillConfiguration_t2915611853 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.UnibillConfiguration::get_SamsungItemGroupId()
extern "C"  String_t* UnibillConfiguration_get_SamsungItemGroupId_m2692425658 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::set_SamsungItemGroupId(System.String)
extern "C"  void UnibillConfiguration_set_SamsungItemGroupId_m2094791997 (UnibillConfiguration_t2915611853 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.UnibillConfiguration::loadCurrencies(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void UnibillConfiguration_loadCurrencies_m3557259177 (UnibillConfiguration_t2915611853 * __this, Dictionary_2_t309261261 * ___root0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem Unibill.Impl.UnibillConfiguration::AddItem()
extern "C"  PurchasableItem_t3963353899 * UnibillConfiguration_AddItem_m4043034967 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.UnibillConfiguration::Serialize()
extern "C"  Dictionary_2_t309261261 * UnibillConfiguration_Serialize_m3723008727 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem Unibill.Impl.UnibillConfiguration::getItemById(System.String)
extern "C"  PurchasableItem_t3963353899 * UnibillConfiguration_getItemById_m3521631290 (UnibillConfiguration_t2915611853 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VirtualCurrency Unibill.Impl.UnibillConfiguration::getCurrency(System.String)
extern "C"  VirtualCurrency_t1115497880 * UnibillConfiguration_getCurrency_m3473119907 (UnibillConfiguration_t2915611853 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::get_AllPurchasableItems()
extern "C"  List_1_t3332475031 * UnibillConfiguration_get_AllPurchasableItems_m2677107411 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::get_AllNonConsumablePurchasableItems()
extern "C"  List_1_t3332475031 * UnibillConfiguration_get_AllNonConsumablePurchasableItems_m2743393195 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::get_AllConsumablePurchasableItems()
extern "C"  List_1_t3332475031 * UnibillConfiguration_get_AllConsumablePurchasableItems_m2149643808 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::get_AllSubscriptions()
extern "C"  List_1_t3332475031 * UnibillConfiguration_get_AllSubscriptions_m4020962385 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PurchasableItem> Unibill.Impl.UnibillConfiguration::get_AllNonSubscriptionPurchasableItems()
extern "C"  List_1_t3332475031 * UnibillConfiguration_get_AllNonSubscriptionPurchasableItems_m1499146295 (UnibillConfiguration_t2915611853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::tryGetBoolean(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  bool UnibillConfiguration_tryGetBoolean_m1110360389 (UnibillConfiguration_t2915611853 * __this, String_t* ___name0, Dictionary_2_t309261261 * ___root1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::<get_AllNonConsumablePurchasableItems>m__0(PurchasableItem)
extern "C"  bool UnibillConfiguration_U3Cget_AllNonConsumablePurchasableItemsU3Em__0_m392992293 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::<get_AllConsumablePurchasableItems>m__1(PurchasableItem)
extern "C"  bool UnibillConfiguration_U3Cget_AllConsumablePurchasableItemsU3Em__1_m3654721111 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::<get_AllSubscriptions>m__2(PurchasableItem)
extern "C"  bool UnibillConfiguration_U3Cget_AllSubscriptionsU3Em__2_m2519948737 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.UnibillConfiguration::<get_AllNonSubscriptionPurchasableItems>m__3(PurchasableItem)
extern "C"  bool UnibillConfiguration_U3Cget_AllNonSubscriptionPurchasableItemsU3Em__3_m3447400286 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
