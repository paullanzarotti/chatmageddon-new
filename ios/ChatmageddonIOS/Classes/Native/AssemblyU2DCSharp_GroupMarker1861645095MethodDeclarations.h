﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GroupMarker
struct GroupMarker_t1861645095;
// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3D576815539.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void GroupMarker::.ctor()
extern "C"  void GroupMarker__ctor_m546648184 (GroupMarker_t1861645095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupMarker::InitGroup(OnlineMapsMarker3D)
extern "C"  void GroupMarker_InitGroup_m4044539166 (GroupMarker_t1861645095 * __this, OnlineMapsMarker3D_t576815539 * ___mapMarker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupMarker::UpdateMarker(System.Int32)
extern "C"  void GroupMarker_UpdateMarker_m1531202740 (GroupMarker_t1861645095 * __this, int32_t ___zoomLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupMarker::TriggerEntered()
extern "C"  void GroupMarker_TriggerEntered_m3877140663 (GroupMarker_t1861645095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupMarker::OnClick()
extern "C"  void GroupMarker_OnClick_m2824891329 (GroupMarker_t1861645095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GroupMarker::WaitToOpenModal()
extern "C"  Il2CppObject * GroupMarker_WaitToOpenModal_m3936836011 (GroupMarker_t1861645095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupMarker::AddMarker(OnlineMapsMarker3D)
extern "C"  void GroupMarker_AddMarker_m733683662 (GroupMarker_t1861645095 * __this, OnlineMapsMarker3D_t576815539 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 GroupMarker::<AddMarker>m__0(UnityEngine.Vector2,OnlineMapsMarker3D)
extern "C"  Vector2_t2243707579  GroupMarker_U3CAddMarkerU3Em__0_m2558104132 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___current0, OnlineMapsMarker3D_t576815539 * ___m1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
