﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PurchaseableItem
struct PurchaseableItem_t3351122996;
// BuyItemButton
struct BuyItemButton_t2593913003;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyItemButton/<PurchaseStaticItem>c__AnonStorey1
struct  U3CPurchaseStaticItemU3Ec__AnonStorey1_t2538291704  : public Il2CppObject
{
public:
	// PurchaseableItem BuyItemButton/<PurchaseStaticItem>c__AnonStorey1::cPurchaseableItem
	PurchaseableItem_t3351122996 * ___cPurchaseableItem_0;
	// BuyItemButton BuyItemButton/<PurchaseStaticItem>c__AnonStorey1::$this
	BuyItemButton_t2593913003 * ___U24this_1;

public:
	inline static int32_t get_offset_of_cPurchaseableItem_0() { return static_cast<int32_t>(offsetof(U3CPurchaseStaticItemU3Ec__AnonStorey1_t2538291704, ___cPurchaseableItem_0)); }
	inline PurchaseableItem_t3351122996 * get_cPurchaseableItem_0() const { return ___cPurchaseableItem_0; }
	inline PurchaseableItem_t3351122996 ** get_address_of_cPurchaseableItem_0() { return &___cPurchaseableItem_0; }
	inline void set_cPurchaseableItem_0(PurchaseableItem_t3351122996 * value)
	{
		___cPurchaseableItem_0 = value;
		Il2CppCodeGenWriteBarrier(&___cPurchaseableItem_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPurchaseStaticItemU3Ec__AnonStorey1_t2538291704, ___U24this_1)); }
	inline BuyItemButton_t2593913003 * get_U24this_1() const { return ___U24this_1; }
	inline BuyItemButton_t2593913003 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BuyItemButton_t2593913003 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
