﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Old_Movie_2
struct CameraFilterPack_TV_Old_Movie_2_t256438953;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Old_Movie_2::.ctor()
extern "C"  void CameraFilterPack_TV_Old_Movie_2__ctor_m2488790414 (CameraFilterPack_TV_Old_Movie_2_t256438953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Old_Movie_2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Old_Movie_2_get_material_m386931861 (CameraFilterPack_TV_Old_Movie_2_t256438953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie_2::Start()
extern "C"  void CameraFilterPack_TV_Old_Movie_2_Start_m555608238 (CameraFilterPack_TV_Old_Movie_2_t256438953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Old_Movie_2_OnRenderImage_m3944709054 (CameraFilterPack_TV_Old_Movie_2_t256438953 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie_2::OnValidate()
extern "C"  void CameraFilterPack_TV_Old_Movie_2_OnValidate_m3085178183 (CameraFilterPack_TV_Old_Movie_2_t256438953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie_2::Update()
extern "C"  void CameraFilterPack_TV_Old_Movie_2_Update_m214432657 (CameraFilterPack_TV_Old_Movie_2_t256438953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie_2::OnDisable()
extern "C"  void CameraFilterPack_TV_Old_Movie_2_OnDisable_m3261558973 (CameraFilterPack_TV_Old_Movie_2_t256438953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
