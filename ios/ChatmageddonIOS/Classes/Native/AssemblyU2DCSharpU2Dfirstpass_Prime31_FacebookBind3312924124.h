﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookBinding/<graphRequest>c__AnonStorey1
struct  U3CgraphRequestU3Ec__AnonStorey1_t3312924124  : public Il2CppObject
{
public:
	// System.String Prime31.FacebookBinding/<graphRequest>c__AnonStorey1::graphPath
	String_t* ___graphPath_0;
	// System.String Prime31.FacebookBinding/<graphRequest>c__AnonStorey1::httpMethod
	String_t* ___httpMethod_1;

public:
	inline static int32_t get_offset_of_graphPath_0() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey1_t3312924124, ___graphPath_0)); }
	inline String_t* get_graphPath_0() const { return ___graphPath_0; }
	inline String_t** get_address_of_graphPath_0() { return &___graphPath_0; }
	inline void set_graphPath_0(String_t* value)
	{
		___graphPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___graphPath_0, value);
	}

	inline static int32_t get_offset_of_httpMethod_1() { return static_cast<int32_t>(offsetof(U3CgraphRequestU3Ec__AnonStorey1_t3312924124, ___httpMethod_1)); }
	inline String_t* get_httpMethod_1() const { return ___httpMethod_1; }
	inline String_t** get_address_of_httpMethod_1() { return &___httpMethod_1; }
	inline void set_httpMethod_1(String_t* value)
	{
		___httpMethod_1 = value;
		Il2CppCodeGenWriteBarrier(&___httpMethod_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
