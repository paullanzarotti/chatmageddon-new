﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneMetaPhoneNumbers
struct PhoneMetaPhoneNumbers_t2910735851;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneMetaPhoneNumbers::.ctor()
extern "C"  void PhoneMetaPhoneNumbers__ctor_m2701767786 (PhoneMetaPhoneNumbers_t2910735851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneMetaPhoneNumbers PhoneMetaPhoneNumbers::Load(System.String)
extern "C"  PhoneMetaPhoneNumbers_t2910735851 * PhoneMetaPhoneNumbers_Load_m3060538362 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneMetaPhoneNumbers PhoneMetaPhoneNumbers::LoadFromText(System.String)
extern "C"  PhoneMetaPhoneNumbers_t2910735851 * PhoneMetaPhoneNumbers_LoadFromText_m4267970095 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
