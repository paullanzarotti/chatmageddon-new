﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StatusListItem
struct StatusListItem_t459202613;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// MissileTrajectoryDisplay
struct MissileTrajectoryDisplay_t1441887151;
// MissileArriveTimer
struct MissileArriveTimer_t44929408;
// UISprite
struct UISprite_t603616735;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileTravelController
struct  MissileTravelController_t534821896  : public MonoBehaviour_t1158329972
{
public:
	// MissileTravelState MissileTravelController::currentState
	int32_t ___currentState_2;
	// MissileTravelState MissileTravelController::previousState
	int32_t ___previousState_3;
	// StatusListItem MissileTravelController::listItem
	StatusListItem_t459202613 * ___listItem_4;
	// LaunchedMissile MissileTravelController::missileItem
	LaunchedMissile_t1542515810 * ___missileItem_5;
	// MissileTrajectoryDisplay MissileTravelController::trajectory
	MissileTrajectoryDisplay_t1441887151 * ___trajectory_6;
	// MissileArriveTimer MissileTravelController::arriveTimer
	MissileArriveTimer_t44929408 * ___arriveTimer_7;
	// UISprite MissileTravelController::userLogoBackground
	UISprite_t603616735 * ___userLogoBackground_8;
	// System.Double MissileTravelController::totalTimeAsSeconds
	double ___totalTimeAsSeconds_9;
	// System.Double MissileTravelController::progressTimeAsSeconds
	double ___progressTimeAsSeconds_10;

public:
	inline static int32_t get_offset_of_currentState_2() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___currentState_2)); }
	inline int32_t get_currentState_2() const { return ___currentState_2; }
	inline int32_t* get_address_of_currentState_2() { return &___currentState_2; }
	inline void set_currentState_2(int32_t value)
	{
		___currentState_2 = value;
	}

	inline static int32_t get_offset_of_previousState_3() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___previousState_3)); }
	inline int32_t get_previousState_3() const { return ___previousState_3; }
	inline int32_t* get_address_of_previousState_3() { return &___previousState_3; }
	inline void set_previousState_3(int32_t value)
	{
		___previousState_3 = value;
	}

	inline static int32_t get_offset_of_listItem_4() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___listItem_4)); }
	inline StatusListItem_t459202613 * get_listItem_4() const { return ___listItem_4; }
	inline StatusListItem_t459202613 ** get_address_of_listItem_4() { return &___listItem_4; }
	inline void set_listItem_4(StatusListItem_t459202613 * value)
	{
		___listItem_4 = value;
		Il2CppCodeGenWriteBarrier(&___listItem_4, value);
	}

	inline static int32_t get_offset_of_missileItem_5() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___missileItem_5)); }
	inline LaunchedMissile_t1542515810 * get_missileItem_5() const { return ___missileItem_5; }
	inline LaunchedMissile_t1542515810 ** get_address_of_missileItem_5() { return &___missileItem_5; }
	inline void set_missileItem_5(LaunchedMissile_t1542515810 * value)
	{
		___missileItem_5 = value;
		Il2CppCodeGenWriteBarrier(&___missileItem_5, value);
	}

	inline static int32_t get_offset_of_trajectory_6() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___trajectory_6)); }
	inline MissileTrajectoryDisplay_t1441887151 * get_trajectory_6() const { return ___trajectory_6; }
	inline MissileTrajectoryDisplay_t1441887151 ** get_address_of_trajectory_6() { return &___trajectory_6; }
	inline void set_trajectory_6(MissileTrajectoryDisplay_t1441887151 * value)
	{
		___trajectory_6 = value;
		Il2CppCodeGenWriteBarrier(&___trajectory_6, value);
	}

	inline static int32_t get_offset_of_arriveTimer_7() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___arriveTimer_7)); }
	inline MissileArriveTimer_t44929408 * get_arriveTimer_7() const { return ___arriveTimer_7; }
	inline MissileArriveTimer_t44929408 ** get_address_of_arriveTimer_7() { return &___arriveTimer_7; }
	inline void set_arriveTimer_7(MissileArriveTimer_t44929408 * value)
	{
		___arriveTimer_7 = value;
		Il2CppCodeGenWriteBarrier(&___arriveTimer_7, value);
	}

	inline static int32_t get_offset_of_userLogoBackground_8() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___userLogoBackground_8)); }
	inline UISprite_t603616735 * get_userLogoBackground_8() const { return ___userLogoBackground_8; }
	inline UISprite_t603616735 ** get_address_of_userLogoBackground_8() { return &___userLogoBackground_8; }
	inline void set_userLogoBackground_8(UISprite_t603616735 * value)
	{
		___userLogoBackground_8 = value;
		Il2CppCodeGenWriteBarrier(&___userLogoBackground_8, value);
	}

	inline static int32_t get_offset_of_totalTimeAsSeconds_9() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___totalTimeAsSeconds_9)); }
	inline double get_totalTimeAsSeconds_9() const { return ___totalTimeAsSeconds_9; }
	inline double* get_address_of_totalTimeAsSeconds_9() { return &___totalTimeAsSeconds_9; }
	inline void set_totalTimeAsSeconds_9(double value)
	{
		___totalTimeAsSeconds_9 = value;
	}

	inline static int32_t get_offset_of_progressTimeAsSeconds_10() { return static_cast<int32_t>(offsetof(MissileTravelController_t534821896, ___progressTimeAsSeconds_10)); }
	inline double get_progressTimeAsSeconds_10() const { return ___progressTimeAsSeconds_10; }
	inline double* get_address_of_progressTimeAsSeconds_10() { return &___progressTimeAsSeconds_10; }
	inline void set_progressTimeAsSeconds_10(double value)
	{
		___progressTimeAsSeconds_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
