﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m904326150_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m904326150(__this, method) ((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m904326150_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  int32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2921628691_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2921628691(__this, method) ((  int32_t (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2921628691_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m1938324600_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m1938324600(__this, method) ((  Il2CppObject * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m1938324600_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m1874917311_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m1874917311(__this, method) ((  Il2CppObject * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m1874917311_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3932226178_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3932226178(__this, method) ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3932226178_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::MoveNext()
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2577937910_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2577937910(__this, method) ((  bool (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m2577937910_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::Dispose()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m464556617_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m464556617(__this, method) ((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m464556617_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Int32>::Reset()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m4153446011_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m4153446011(__this, method) ((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3504167023 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m4153446011_gshared)(__this, method)
