﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_Therma
struct CameraFilterPack_Gradients_Therma_t1493044570;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_Therma::.ctor()
extern "C"  void CameraFilterPack_Gradients_Therma__ctor_m3067463025 (CameraFilterPack_Gradients_Therma_t1493044570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Therma::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_Therma_get_material_m3282275262 (CameraFilterPack_Gradients_Therma_t1493044570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::Start()
extern "C"  void CameraFilterPack_Gradients_Therma_Start_m3455193505 (CameraFilterPack_Gradients_Therma_t1493044570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_Therma_OnRenderImage_m4082245785 (CameraFilterPack_Gradients_Therma_t1493044570 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::Update()
extern "C"  void CameraFilterPack_Gradients_Therma_Update_m609682364 (CameraFilterPack_Gradients_Therma_t1493044570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::OnDisable()
extern "C"  void CameraFilterPack_Gradients_Therma_OnDisable_m1445543722 (CameraFilterPack_Gradients_Therma_t1493044570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
