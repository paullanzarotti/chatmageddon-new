﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsHomeNavScreen
struct FriendsHomeNavScreen_t1258257937;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendsHomeNavScreen::.ctor()
extern "C"  void FriendsHomeNavScreen__ctor_m2803944110 (FriendsHomeNavScreen_t1258257937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsHomeNavScreen::Start()
extern "C"  void FriendsHomeNavScreen_Start_m2399423646 (FriendsHomeNavScreen_t1258257937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsHomeNavScreen::UIClosing()
extern "C"  void FriendsHomeNavScreen_UIClosing_m3446236643 (FriendsHomeNavScreen_t1258257937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsHomeNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void FriendsHomeNavScreen_ScreenLoad_m1347390182 (FriendsHomeNavScreen_t1258257937 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsHomeNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void FriendsHomeNavScreen_ScreenUnload_m930840022 (FriendsHomeNavScreen_t1258257937 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsHomeNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool FriendsHomeNavScreen_ValidateScreenNavigation_m2222475027 (FriendsHomeNavScreen_t1258257937 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
