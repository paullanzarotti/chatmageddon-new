﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RadarMarkerBlip
struct RadarMarkerBlip_t1130528275;
// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3D576815539.h"
#include "AssemblyU2DCSharp_OnlineMapsMarkerBase3900955221.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void RadarMarkerBlip::.ctor()
extern "C"  void RadarMarkerBlip__ctor_m3442057292 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::Init(OnlineMapsMarker3D)
extern "C"  void RadarMarkerBlip_Init_m1305837677 (RadarMarkerBlip_t1130528275 * __this, OnlineMapsMarker3D_t576815539 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::UpdateCollision()
extern "C"  void RadarMarkerBlip_UpdateCollision_m303174193 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::UpdateScale()
extern "C"  void RadarMarkerBlip_UpdateScale_m451785799 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::ClickEffect(OnlineMapsMarkerBase)
extern "C"  void RadarMarkerBlip_ClickEffect_m3909329292 (RadarMarkerBlip_t1130528275 * __this, OnlineMapsMarkerBase_t3900955221 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::OnClick()
extern "C"  void RadarMarkerBlip_OnClick_m4096681789 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::Update()
extern "C"  void RadarMarkerBlip_Update_m3958725439 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::OnUpdate()
extern "C"  void RadarMarkerBlip_OnUpdate_m3613939194 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void RadarMarkerBlip_OnTriggerEnter_m530158544 (RadarMarkerBlip_t1130528275 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::OnTriggerExit(UnityEngine.Collider)
extern "C"  void RadarMarkerBlip_OnTriggerExit_m2662767890 (RadarMarkerBlip_t1130528275 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::TriggerEntered()
extern "C"  void RadarMarkerBlip_TriggerEntered_m3325468963 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::TriggerExited()
extern "C"  void RadarMarkerBlip_TriggerExited_m340064911 (RadarMarkerBlip_t1130528275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadarMarkerBlip::UpdateMarker(System.Int32)
extern "C"  void RadarMarkerBlip_UpdateMarker_m952896888 (RadarMarkerBlip_t1130528275 * __this, int32_t ___zoomLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
