﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Uniject.Impl.UnityLevelLoadListener
struct UnityLevelLoadListener_t2593383553;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void Uniject.Impl.UnityLevelLoadListener::.ctor()
extern "C"  void UnityLevelLoadListener__ctor_m1258738416 (UnityLevelLoadListener_t2593383553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLevelLoadListener::registerListener(System.Action)
extern "C"  void UnityLevelLoadListener_registerListener_m133504858 (UnityLevelLoadListener_t2593383553 * __this, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLevelLoadListener::Start()
extern "C"  void UnityLevelLoadListener_Start_m3342379276 (UnityLevelLoadListener_t2593383553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Uniject.Impl.UnityLevelLoadListener::OnLevelWasLoaded(System.Int32)
extern "C"  void UnityLevelLoadListener_OnLevelWasLoaded_m4092706920 (UnityLevelLoadListener_t2593383553 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
