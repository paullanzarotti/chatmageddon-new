﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwitchMapStateButton
struct  SwitchMapStateButton_t2972050837  : public SFXButton_t792651341
{
public:
	// MapState SwitchMapStateButton::mapState
	int32_t ___mapState_5;
	// System.Boolean SwitchMapStateButton::deactivated
	bool ___deactivated_6;
	// TweenPosition SwitchMapStateButton::posTween
	TweenPosition_t1144714832 * ___posTween_7;
	// System.Boolean SwitchMapStateButton::buttonActive
	bool ___buttonActive_8;
	// UISprite SwitchMapStateButton::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_9;
	// UISprite SwitchMapStateButton::buttonSprite
	UISprite_t603616735 * ___buttonSprite_10;
	// System.Boolean SwitchMapStateButton::isActiveState
	bool ___isActiveState_11;

public:
	inline static int32_t get_offset_of_mapState_5() { return static_cast<int32_t>(offsetof(SwitchMapStateButton_t2972050837, ___mapState_5)); }
	inline int32_t get_mapState_5() const { return ___mapState_5; }
	inline int32_t* get_address_of_mapState_5() { return &___mapState_5; }
	inline void set_mapState_5(int32_t value)
	{
		___mapState_5 = value;
	}

	inline static int32_t get_offset_of_deactivated_6() { return static_cast<int32_t>(offsetof(SwitchMapStateButton_t2972050837, ___deactivated_6)); }
	inline bool get_deactivated_6() const { return ___deactivated_6; }
	inline bool* get_address_of_deactivated_6() { return &___deactivated_6; }
	inline void set_deactivated_6(bool value)
	{
		___deactivated_6 = value;
	}

	inline static int32_t get_offset_of_posTween_7() { return static_cast<int32_t>(offsetof(SwitchMapStateButton_t2972050837, ___posTween_7)); }
	inline TweenPosition_t1144714832 * get_posTween_7() const { return ___posTween_7; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_7() { return &___posTween_7; }
	inline void set_posTween_7(TweenPosition_t1144714832 * value)
	{
		___posTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_7, value);
	}

	inline static int32_t get_offset_of_buttonActive_8() { return static_cast<int32_t>(offsetof(SwitchMapStateButton_t2972050837, ___buttonActive_8)); }
	inline bool get_buttonActive_8() const { return ___buttonActive_8; }
	inline bool* get_address_of_buttonActive_8() { return &___buttonActive_8; }
	inline void set_buttonActive_8(bool value)
	{
		___buttonActive_8 = value;
	}

	inline static int32_t get_offset_of_backgroundSprite_9() { return static_cast<int32_t>(offsetof(SwitchMapStateButton_t2972050837, ___backgroundSprite_9)); }
	inline UISprite_t603616735 * get_backgroundSprite_9() const { return ___backgroundSprite_9; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_9() { return &___backgroundSprite_9; }
	inline void set_backgroundSprite_9(UISprite_t603616735 * value)
	{
		___backgroundSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_9, value);
	}

	inline static int32_t get_offset_of_buttonSprite_10() { return static_cast<int32_t>(offsetof(SwitchMapStateButton_t2972050837, ___buttonSprite_10)); }
	inline UISprite_t603616735 * get_buttonSprite_10() const { return ___buttonSprite_10; }
	inline UISprite_t603616735 ** get_address_of_buttonSprite_10() { return &___buttonSprite_10; }
	inline void set_buttonSprite_10(UISprite_t603616735 * value)
	{
		___buttonSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSprite_10, value);
	}

	inline static int32_t get_offset_of_isActiveState_11() { return static_cast<int32_t>(offsetof(SwitchMapStateButton_t2972050837, ___isActiveState_11)); }
	inline bool get_isActiveState_11() const { return ___isActiveState_11; }
	inline bool* get_address_of_isActiveState_11() { return &___isActiveState_11; }
	inline void set_isActiveState_11(bool value)
	{
		___isActiveState_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
