﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XNode
struct XNode_t2707504214;
// System.String
struct String_t;
// System.Xml.XmlReader
struct XmlReader_t3675626668;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"
#include "System_Xml_Linq_System_Xml_Linq_SaveOptions2961005337.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_Linq_System_Xml_Linq_LoadOptions53198144.h"

// System.Void System.Xml.Linq.XNode::.ctor()
extern "C"  void XNode__ctor_m3635466201 (XNode_t2707504214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XNode::.cctor()
extern "C"  void XNode__cctor_m1243525384 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XNode::set_PreviousNode(System.Xml.Linq.XNode)
extern "C"  void XNode_set_PreviousNode_m1918335957 (XNode_t2707504214 * __this, XNode_t2707504214 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNode System.Xml.Linq.XNode::get_NextNode()
extern "C"  XNode_t2707504214 * XNode_get_NextNode_m1164634702 (XNode_t2707504214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XNode::set_NextNode(System.Xml.Linq.XNode)
extern "C"  void XNode_set_NextNode_m2612555387 (XNode_t2707504214 * __this, XNode_t2707504214 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XNode::ToString(System.Xml.Linq.SaveOptions)
extern "C"  String_t* XNode_ToString_m3539772875 (XNode_t2707504214 * __this, int32_t ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNode System.Xml.Linq.XNode::ReadFrom(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern "C"  XNode_t2707504214 * XNode_ReadFrom_m1777325942 (Il2CppObject * __this /* static, unused */, XmlReader_t3675626668 * ___r0, int32_t ___options1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XNode::ToString()
extern "C"  String_t* XNode_ToString_m418147946 (XNode_t2707504214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
