﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsControlBase
struct OnlineMapsControlBase_t473237564;
// System.Action
struct Action_t3226471752;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// OnlineMaps
struct OnlineMaps_t1893290312;
// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsControlBase
struct  OnlineMapsControlBase_t473237564  : public MonoBehaviour_t1158329972
{
public:
	// System.Action OnlineMapsControlBase::OnMapClick
	Action_t3226471752 * ___OnMapClick_4;
	// System.Action OnlineMapsControlBase::OnMapDoubleClick
	Action_t3226471752 * ___OnMapDoubleClick_5;
	// System.Action OnlineMapsControlBase::OnMapDrag
	Action_t3226471752 * ___OnMapDrag_6;
	// System.Action OnlineMapsControlBase::OnMapLongPress
	Action_t3226471752 * ___OnMapLongPress_7;
	// System.Action OnlineMapsControlBase::OnMapPress
	Action_t3226471752 * ___OnMapPress_8;
	// System.Action OnlineMapsControlBase::OnMapRelease
	Action_t3226471752 * ___OnMapRelease_9;
	// System.Action OnlineMapsControlBase::OnMapZoom
	Action_t3226471752 * ___OnMapZoom_10;
	// UnityEngine.Texture2D OnlineMapsControlBase::activeTexture
	Texture2D_t3542995729 * ___activeTexture_11;
	// System.Boolean OnlineMapsControlBase::allowAddMarkerByM
	bool ___allowAddMarkerByM_12;
	// System.Boolean OnlineMapsControlBase::allowZoom
	bool ___allowZoom_13;
	// System.Boolean OnlineMapsControlBase::allowUserControl
	bool ___allowUserControl_14;
	// System.Boolean OnlineMapsControlBase::isMapDrag
	bool ___isMapDrag_15;
	// System.Boolean OnlineMapsControlBase::invertTouchZoom
	bool ___invertTouchZoom_16;
	// System.Boolean OnlineMapsControlBase::zoomInOnDoubleClick
	bool ___zoomInOnDoubleClick_17;
	// UnityEngine.Rect OnlineMapsControlBase::_screenRect
	Rect_t3681755626  ____screenRect_18;
	// OnlineMaps OnlineMapsControlBase::api
	OnlineMaps_t1893290312 * ___api_19;
	// System.Single OnlineMapsControlBase::lastGestureDistance
	float ___lastGestureDistance_20;
	// UnityEngine.Vector2 OnlineMapsControlBase::lastMousePosition
	Vector2_t2243707579  ___lastMousePosition_21;
	// System.Int32 OnlineMapsControlBase::lastTouchCount
	int32_t ___lastTouchCount_22;
	// OnlineMapsMarkerBase OnlineMapsControlBase::_dragMarker
	OnlineMapsMarkerBase_t3900955221 * ____dragMarker_23;
	// System.Int64[] OnlineMapsControlBase::lastClickTimes
	Int64U5BU5D_t717125112* ___lastClickTimes_24;
	// UnityEngine.Vector3 OnlineMapsControlBase::pressPoint
	Vector3_t2243707580  ___pressPoint_25;
	// System.Collections.IEnumerator OnlineMapsControlBase::longPressEnumenator
	Il2CppObject * ___longPressEnumenator_26;
	// UnityEngine.Vector2 OnlineMapsControlBase::lastGestureCenter
	Vector2_t2243707579  ___lastGestureCenter_27;
	// System.Double OnlineMapsControlBase::lastPositionLng
	double ___lastPositionLng_28;
	// System.Double OnlineMapsControlBase::lastPositionLat
	double ___lastPositionLat_29;
	// UnityEngine.Coroutine OnlineMapsControlBase::dragVelocityRoutine
	Coroutine_t2299508840 * ___dragVelocityRoutine_30;
	// System.Double OnlineMapsControlBase::startPressX
	double ___startPressX_31;
	// System.Double OnlineMapsControlBase::startPressY
	double ___startPressY_32;

public:
	inline static int32_t get_offset_of_OnMapClick_4() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___OnMapClick_4)); }
	inline Action_t3226471752 * get_OnMapClick_4() const { return ___OnMapClick_4; }
	inline Action_t3226471752 ** get_address_of_OnMapClick_4() { return &___OnMapClick_4; }
	inline void set_OnMapClick_4(Action_t3226471752 * value)
	{
		___OnMapClick_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapClick_4, value);
	}

	inline static int32_t get_offset_of_OnMapDoubleClick_5() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___OnMapDoubleClick_5)); }
	inline Action_t3226471752 * get_OnMapDoubleClick_5() const { return ___OnMapDoubleClick_5; }
	inline Action_t3226471752 ** get_address_of_OnMapDoubleClick_5() { return &___OnMapDoubleClick_5; }
	inline void set_OnMapDoubleClick_5(Action_t3226471752 * value)
	{
		___OnMapDoubleClick_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapDoubleClick_5, value);
	}

	inline static int32_t get_offset_of_OnMapDrag_6() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___OnMapDrag_6)); }
	inline Action_t3226471752 * get_OnMapDrag_6() const { return ___OnMapDrag_6; }
	inline Action_t3226471752 ** get_address_of_OnMapDrag_6() { return &___OnMapDrag_6; }
	inline void set_OnMapDrag_6(Action_t3226471752 * value)
	{
		___OnMapDrag_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapDrag_6, value);
	}

	inline static int32_t get_offset_of_OnMapLongPress_7() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___OnMapLongPress_7)); }
	inline Action_t3226471752 * get_OnMapLongPress_7() const { return ___OnMapLongPress_7; }
	inline Action_t3226471752 ** get_address_of_OnMapLongPress_7() { return &___OnMapLongPress_7; }
	inline void set_OnMapLongPress_7(Action_t3226471752 * value)
	{
		___OnMapLongPress_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapLongPress_7, value);
	}

	inline static int32_t get_offset_of_OnMapPress_8() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___OnMapPress_8)); }
	inline Action_t3226471752 * get_OnMapPress_8() const { return ___OnMapPress_8; }
	inline Action_t3226471752 ** get_address_of_OnMapPress_8() { return &___OnMapPress_8; }
	inline void set_OnMapPress_8(Action_t3226471752 * value)
	{
		___OnMapPress_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapPress_8, value);
	}

	inline static int32_t get_offset_of_OnMapRelease_9() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___OnMapRelease_9)); }
	inline Action_t3226471752 * get_OnMapRelease_9() const { return ___OnMapRelease_9; }
	inline Action_t3226471752 ** get_address_of_OnMapRelease_9() { return &___OnMapRelease_9; }
	inline void set_OnMapRelease_9(Action_t3226471752 * value)
	{
		___OnMapRelease_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapRelease_9, value);
	}

	inline static int32_t get_offset_of_OnMapZoom_10() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___OnMapZoom_10)); }
	inline Action_t3226471752 * get_OnMapZoom_10() const { return ___OnMapZoom_10; }
	inline Action_t3226471752 ** get_address_of_OnMapZoom_10() { return &___OnMapZoom_10; }
	inline void set_OnMapZoom_10(Action_t3226471752 * value)
	{
		___OnMapZoom_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapZoom_10, value);
	}

	inline static int32_t get_offset_of_activeTexture_11() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___activeTexture_11)); }
	inline Texture2D_t3542995729 * get_activeTexture_11() const { return ___activeTexture_11; }
	inline Texture2D_t3542995729 ** get_address_of_activeTexture_11() { return &___activeTexture_11; }
	inline void set_activeTexture_11(Texture2D_t3542995729 * value)
	{
		___activeTexture_11 = value;
		Il2CppCodeGenWriteBarrier(&___activeTexture_11, value);
	}

	inline static int32_t get_offset_of_allowAddMarkerByM_12() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___allowAddMarkerByM_12)); }
	inline bool get_allowAddMarkerByM_12() const { return ___allowAddMarkerByM_12; }
	inline bool* get_address_of_allowAddMarkerByM_12() { return &___allowAddMarkerByM_12; }
	inline void set_allowAddMarkerByM_12(bool value)
	{
		___allowAddMarkerByM_12 = value;
	}

	inline static int32_t get_offset_of_allowZoom_13() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___allowZoom_13)); }
	inline bool get_allowZoom_13() const { return ___allowZoom_13; }
	inline bool* get_address_of_allowZoom_13() { return &___allowZoom_13; }
	inline void set_allowZoom_13(bool value)
	{
		___allowZoom_13 = value;
	}

	inline static int32_t get_offset_of_allowUserControl_14() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___allowUserControl_14)); }
	inline bool get_allowUserControl_14() const { return ___allowUserControl_14; }
	inline bool* get_address_of_allowUserControl_14() { return &___allowUserControl_14; }
	inline void set_allowUserControl_14(bool value)
	{
		___allowUserControl_14 = value;
	}

	inline static int32_t get_offset_of_isMapDrag_15() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___isMapDrag_15)); }
	inline bool get_isMapDrag_15() const { return ___isMapDrag_15; }
	inline bool* get_address_of_isMapDrag_15() { return &___isMapDrag_15; }
	inline void set_isMapDrag_15(bool value)
	{
		___isMapDrag_15 = value;
	}

	inline static int32_t get_offset_of_invertTouchZoom_16() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___invertTouchZoom_16)); }
	inline bool get_invertTouchZoom_16() const { return ___invertTouchZoom_16; }
	inline bool* get_address_of_invertTouchZoom_16() { return &___invertTouchZoom_16; }
	inline void set_invertTouchZoom_16(bool value)
	{
		___invertTouchZoom_16 = value;
	}

	inline static int32_t get_offset_of_zoomInOnDoubleClick_17() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___zoomInOnDoubleClick_17)); }
	inline bool get_zoomInOnDoubleClick_17() const { return ___zoomInOnDoubleClick_17; }
	inline bool* get_address_of_zoomInOnDoubleClick_17() { return &___zoomInOnDoubleClick_17; }
	inline void set_zoomInOnDoubleClick_17(bool value)
	{
		___zoomInOnDoubleClick_17 = value;
	}

	inline static int32_t get_offset_of__screenRect_18() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ____screenRect_18)); }
	inline Rect_t3681755626  get__screenRect_18() const { return ____screenRect_18; }
	inline Rect_t3681755626 * get_address_of__screenRect_18() { return &____screenRect_18; }
	inline void set__screenRect_18(Rect_t3681755626  value)
	{
		____screenRect_18 = value;
	}

	inline static int32_t get_offset_of_api_19() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___api_19)); }
	inline OnlineMaps_t1893290312 * get_api_19() const { return ___api_19; }
	inline OnlineMaps_t1893290312 ** get_address_of_api_19() { return &___api_19; }
	inline void set_api_19(OnlineMaps_t1893290312 * value)
	{
		___api_19 = value;
		Il2CppCodeGenWriteBarrier(&___api_19, value);
	}

	inline static int32_t get_offset_of_lastGestureDistance_20() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___lastGestureDistance_20)); }
	inline float get_lastGestureDistance_20() const { return ___lastGestureDistance_20; }
	inline float* get_address_of_lastGestureDistance_20() { return &___lastGestureDistance_20; }
	inline void set_lastGestureDistance_20(float value)
	{
		___lastGestureDistance_20 = value;
	}

	inline static int32_t get_offset_of_lastMousePosition_21() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___lastMousePosition_21)); }
	inline Vector2_t2243707579  get_lastMousePosition_21() const { return ___lastMousePosition_21; }
	inline Vector2_t2243707579 * get_address_of_lastMousePosition_21() { return &___lastMousePosition_21; }
	inline void set_lastMousePosition_21(Vector2_t2243707579  value)
	{
		___lastMousePosition_21 = value;
	}

	inline static int32_t get_offset_of_lastTouchCount_22() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___lastTouchCount_22)); }
	inline int32_t get_lastTouchCount_22() const { return ___lastTouchCount_22; }
	inline int32_t* get_address_of_lastTouchCount_22() { return &___lastTouchCount_22; }
	inline void set_lastTouchCount_22(int32_t value)
	{
		___lastTouchCount_22 = value;
	}

	inline static int32_t get_offset_of__dragMarker_23() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ____dragMarker_23)); }
	inline OnlineMapsMarkerBase_t3900955221 * get__dragMarker_23() const { return ____dragMarker_23; }
	inline OnlineMapsMarkerBase_t3900955221 ** get_address_of__dragMarker_23() { return &____dragMarker_23; }
	inline void set__dragMarker_23(OnlineMapsMarkerBase_t3900955221 * value)
	{
		____dragMarker_23 = value;
		Il2CppCodeGenWriteBarrier(&____dragMarker_23, value);
	}

	inline static int32_t get_offset_of_lastClickTimes_24() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___lastClickTimes_24)); }
	inline Int64U5BU5D_t717125112* get_lastClickTimes_24() const { return ___lastClickTimes_24; }
	inline Int64U5BU5D_t717125112** get_address_of_lastClickTimes_24() { return &___lastClickTimes_24; }
	inline void set_lastClickTimes_24(Int64U5BU5D_t717125112* value)
	{
		___lastClickTimes_24 = value;
		Il2CppCodeGenWriteBarrier(&___lastClickTimes_24, value);
	}

	inline static int32_t get_offset_of_pressPoint_25() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___pressPoint_25)); }
	inline Vector3_t2243707580  get_pressPoint_25() const { return ___pressPoint_25; }
	inline Vector3_t2243707580 * get_address_of_pressPoint_25() { return &___pressPoint_25; }
	inline void set_pressPoint_25(Vector3_t2243707580  value)
	{
		___pressPoint_25 = value;
	}

	inline static int32_t get_offset_of_longPressEnumenator_26() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___longPressEnumenator_26)); }
	inline Il2CppObject * get_longPressEnumenator_26() const { return ___longPressEnumenator_26; }
	inline Il2CppObject ** get_address_of_longPressEnumenator_26() { return &___longPressEnumenator_26; }
	inline void set_longPressEnumenator_26(Il2CppObject * value)
	{
		___longPressEnumenator_26 = value;
		Il2CppCodeGenWriteBarrier(&___longPressEnumenator_26, value);
	}

	inline static int32_t get_offset_of_lastGestureCenter_27() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___lastGestureCenter_27)); }
	inline Vector2_t2243707579  get_lastGestureCenter_27() const { return ___lastGestureCenter_27; }
	inline Vector2_t2243707579 * get_address_of_lastGestureCenter_27() { return &___lastGestureCenter_27; }
	inline void set_lastGestureCenter_27(Vector2_t2243707579  value)
	{
		___lastGestureCenter_27 = value;
	}

	inline static int32_t get_offset_of_lastPositionLng_28() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___lastPositionLng_28)); }
	inline double get_lastPositionLng_28() const { return ___lastPositionLng_28; }
	inline double* get_address_of_lastPositionLng_28() { return &___lastPositionLng_28; }
	inline void set_lastPositionLng_28(double value)
	{
		___lastPositionLng_28 = value;
	}

	inline static int32_t get_offset_of_lastPositionLat_29() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___lastPositionLat_29)); }
	inline double get_lastPositionLat_29() const { return ___lastPositionLat_29; }
	inline double* get_address_of_lastPositionLat_29() { return &___lastPositionLat_29; }
	inline void set_lastPositionLat_29(double value)
	{
		___lastPositionLat_29 = value;
	}

	inline static int32_t get_offset_of_dragVelocityRoutine_30() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___dragVelocityRoutine_30)); }
	inline Coroutine_t2299508840 * get_dragVelocityRoutine_30() const { return ___dragVelocityRoutine_30; }
	inline Coroutine_t2299508840 ** get_address_of_dragVelocityRoutine_30() { return &___dragVelocityRoutine_30; }
	inline void set_dragVelocityRoutine_30(Coroutine_t2299508840 * value)
	{
		___dragVelocityRoutine_30 = value;
		Il2CppCodeGenWriteBarrier(&___dragVelocityRoutine_30, value);
	}

	inline static int32_t get_offset_of_startPressX_31() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___startPressX_31)); }
	inline double get_startPressX_31() const { return ___startPressX_31; }
	inline double* get_address_of_startPressX_31() { return &___startPressX_31; }
	inline void set_startPressX_31(double value)
	{
		___startPressX_31 = value;
	}

	inline static int32_t get_offset_of_startPressY_32() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564, ___startPressY_32)); }
	inline double get_startPressY_32() const { return ___startPressY_32; }
	inline double* get_address_of_startPressY_32() { return &___startPressY_32; }
	inline void set_startPressY_32(double value)
	{
		___startPressY_32 = value;
	}
};

struct OnlineMapsControlBase_t473237564_StaticFields
{
public:
	// System.Single OnlineMapsControlBase::longPressDelay
	float ___longPressDelay_2;
	// OnlineMapsControlBase OnlineMapsControlBase::_instance
	OnlineMapsControlBase_t473237564 * ____instance_3;

public:
	inline static int32_t get_offset_of_longPressDelay_2() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564_StaticFields, ___longPressDelay_2)); }
	inline float get_longPressDelay_2() const { return ___longPressDelay_2; }
	inline float* get_address_of_longPressDelay_2() { return &___longPressDelay_2; }
	inline void set_longPressDelay_2(float value)
	{
		___longPressDelay_2 = value;
	}

	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase_t473237564_StaticFields, ____instance_3)); }
	inline OnlineMapsControlBase_t473237564 * get__instance_3() const { return ____instance_3; }
	inline OnlineMapsControlBase_t473237564 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(OnlineMapsControlBase_t473237564 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
