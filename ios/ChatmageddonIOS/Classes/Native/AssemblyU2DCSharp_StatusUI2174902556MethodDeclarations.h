﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusUI
struct StatusUI_t2174902556;
// StatusListItem
struct StatusListItem_t459202613;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StatusListItem459202613.h"

// System.Void StatusUI::.ctor()
extern "C"  void StatusUI__ctor_m2908587531 (StatusUI_t2174902556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusUI::SetStartUI(StatusListItem)
extern "C"  void StatusUI_SetStartUI_m2183494726 (StatusUI_t2174902556 * __this, StatusListItem_t459202613 * ___newitem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusUI::SetItem(StatusListItem)
extern "C"  void StatusUI_SetItem_m1239623827 (StatusUI_t2174902556 * __this, StatusListItem_t459202613 * ___newitem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusUI::LoadUI(System.Boolean)
extern "C"  void StatusUI_LoadUI_m2253769956 (StatusUI_t2174902556 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusUI::UnloadUI(System.Boolean)
extern "C"  void StatusUI_UnloadUI_m4129354261 (StatusUI_t2174902556 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
