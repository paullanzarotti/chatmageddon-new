﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsHomeUIScaler
struct  SettingsHomeUIScaler_t3836684136  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.BoxCollider SettingsHomeUIScaler::topBarrier
	BoxCollider_t22920061 * ___topBarrier_14;
	// UnityEngine.Transform SettingsHomeUIScaler::profilePicture
	Transform_t3275118058 * ___profilePicture_15;
	// UnityEngine.Transform SettingsHomeUIScaler::profileNames
	Transform_t3275118058 * ___profileNames_16;
	// UISprite SettingsHomeUIScaler::topDivider
	UISprite_t603616735 * ___topDivider_17;
	// UILabel SettingsHomeUIScaler::updateProfileLabel
	UILabel_t1795115428 * ___updateProfileLabel_18;
	// UISprite SettingsHomeUIScaler::updateProfileNav
	UISprite_t603616735 * ___updateProfileNav_19;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::updateProfileCollider
	BoxCollider_t22920061 * ___updateProfileCollider_20;
	// UISprite SettingsHomeUIScaler::updateProfileDivider
	UISprite_t603616735 * ___updateProfileDivider_21;
	// UnityEngine.Transform SettingsHomeUIScaler::stealthMode
	Transform_t3275118058 * ___stealthMode_22;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::stealthModeCollider
	BoxCollider_t22920061 * ___stealthModeCollider_23;
	// UISprite SettingsHomeUIScaler::stealthModeDivider
	UISprite_t603616735 * ___stealthModeDivider_24;
	// UILabel SettingsHomeUIScaler::updateStealthModeLabel
	UILabel_t1795115428 * ___updateStealthModeLabel_25;
	// UISprite SettingsHomeUIScaler::updateStealthModeNav
	UISprite_t603616735 * ___updateStealthModeNav_26;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::updateStealthModeCollider
	BoxCollider_t22920061 * ___updateStealthModeCollider_27;
	// UISprite SettingsHomeUIScaler::updateStealthModeDivider
	UISprite_t603616735 * ___updateStealthModeDivider_28;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::middleCollider
	BoxCollider_t22920061 * ___middleCollider_29;
	// UISprite SettingsHomeUIScaler::aAndBTopDivider
	UISprite_t603616735 * ___aAndBTopDivider_30;
	// UILabel SettingsHomeUIScaler::aAndBLabel
	UILabel_t1795115428 * ___aAndBLabel_31;
	// UISprite SettingsHomeUIScaler::aAndBNav
	UISprite_t603616735 * ___aAndBNav_32;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::aAndBCollider
	BoxCollider_t22920061 * ___aAndBCollider_33;
	// UISprite SettingsHomeUIScaler::aAndBBottomDivider
	UISprite_t603616735 * ___aAndBBottomDivider_34;
	// UILabel SettingsHomeUIScaler::notificationsLabel
	UILabel_t1795115428 * ___notificationsLabel_35;
	// UISprite SettingsHomeUIScaler::notificationsNav
	UISprite_t603616735 * ___notificationsNav_36;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::notificationsCollider
	BoxCollider_t22920061 * ___notificationsCollider_37;
	// UISprite SettingsHomeUIScaler::notificationsDivider
	UISprite_t603616735 * ___notificationsDivider_38;
	// UILabel SettingsHomeUIScaler::audioLabel
	UILabel_t1795115428 * ___audioLabel_39;
	// UISprite SettingsHomeUIScaler::audioNav
	UISprite_t603616735 * ___audioNav_40;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::audioCollider
	BoxCollider_t22920061 * ___audioCollider_41;
	// UISprite SettingsHomeUIScaler::audioDivider
	UISprite_t603616735 * ___audioDivider_42;
	// UILabel SettingsHomeUIScaler::tellAFriendLabel
	UILabel_t1795115428 * ___tellAFriendLabel_43;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::tellAFriendCollider
	BoxCollider_t22920061 * ___tellAFriendCollider_44;
	// UISprite SettingsHomeUIScaler::tellAFriendDivider
	UISprite_t603616735 * ___tellAFriendDivider_45;
	// UILabel SettingsHomeUIScaler::rateThisAppLabel
	UILabel_t1795115428 * ___rateThisAppLabel_46;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::rateThisAppCollider
	BoxCollider_t22920061 * ___rateThisAppCollider_47;
	// UISprite SettingsHomeUIScaler::rateThisAppDivider
	UISprite_t603616735 * ___rateThisAppDivider_48;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::bottomCollider
	BoxCollider_t22920061 * ___bottomCollider_49;
	// UILabel SettingsHomeUIScaler::logoutLabel
	UILabel_t1795115428 * ___logoutLabel_50;
	// UISprite SettingsHomeUIScaler::logoutNav
	UISprite_t603616735 * ___logoutNav_51;
	// UnityEngine.BoxCollider SettingsHomeUIScaler::logoutCollider
	BoxCollider_t22920061 * ___logoutCollider_52;
	// UISprite SettingsHomeUIScaler::logoutDivider
	UISprite_t603616735 * ___logoutDivider_53;

public:
	inline static int32_t get_offset_of_topBarrier_14() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___topBarrier_14)); }
	inline BoxCollider_t22920061 * get_topBarrier_14() const { return ___topBarrier_14; }
	inline BoxCollider_t22920061 ** get_address_of_topBarrier_14() { return &___topBarrier_14; }
	inline void set_topBarrier_14(BoxCollider_t22920061 * value)
	{
		___topBarrier_14 = value;
		Il2CppCodeGenWriteBarrier(&___topBarrier_14, value);
	}

	inline static int32_t get_offset_of_profilePicture_15() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___profilePicture_15)); }
	inline Transform_t3275118058 * get_profilePicture_15() const { return ___profilePicture_15; }
	inline Transform_t3275118058 ** get_address_of_profilePicture_15() { return &___profilePicture_15; }
	inline void set_profilePicture_15(Transform_t3275118058 * value)
	{
		___profilePicture_15 = value;
		Il2CppCodeGenWriteBarrier(&___profilePicture_15, value);
	}

	inline static int32_t get_offset_of_profileNames_16() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___profileNames_16)); }
	inline Transform_t3275118058 * get_profileNames_16() const { return ___profileNames_16; }
	inline Transform_t3275118058 ** get_address_of_profileNames_16() { return &___profileNames_16; }
	inline void set_profileNames_16(Transform_t3275118058 * value)
	{
		___profileNames_16 = value;
		Il2CppCodeGenWriteBarrier(&___profileNames_16, value);
	}

	inline static int32_t get_offset_of_topDivider_17() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___topDivider_17)); }
	inline UISprite_t603616735 * get_topDivider_17() const { return ___topDivider_17; }
	inline UISprite_t603616735 ** get_address_of_topDivider_17() { return &___topDivider_17; }
	inline void set_topDivider_17(UISprite_t603616735 * value)
	{
		___topDivider_17 = value;
		Il2CppCodeGenWriteBarrier(&___topDivider_17, value);
	}

	inline static int32_t get_offset_of_updateProfileLabel_18() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateProfileLabel_18)); }
	inline UILabel_t1795115428 * get_updateProfileLabel_18() const { return ___updateProfileLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_updateProfileLabel_18() { return &___updateProfileLabel_18; }
	inline void set_updateProfileLabel_18(UILabel_t1795115428 * value)
	{
		___updateProfileLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___updateProfileLabel_18, value);
	}

	inline static int32_t get_offset_of_updateProfileNav_19() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateProfileNav_19)); }
	inline UISprite_t603616735 * get_updateProfileNav_19() const { return ___updateProfileNav_19; }
	inline UISprite_t603616735 ** get_address_of_updateProfileNav_19() { return &___updateProfileNav_19; }
	inline void set_updateProfileNav_19(UISprite_t603616735 * value)
	{
		___updateProfileNav_19 = value;
		Il2CppCodeGenWriteBarrier(&___updateProfileNav_19, value);
	}

	inline static int32_t get_offset_of_updateProfileCollider_20() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateProfileCollider_20)); }
	inline BoxCollider_t22920061 * get_updateProfileCollider_20() const { return ___updateProfileCollider_20; }
	inline BoxCollider_t22920061 ** get_address_of_updateProfileCollider_20() { return &___updateProfileCollider_20; }
	inline void set_updateProfileCollider_20(BoxCollider_t22920061 * value)
	{
		___updateProfileCollider_20 = value;
		Il2CppCodeGenWriteBarrier(&___updateProfileCollider_20, value);
	}

	inline static int32_t get_offset_of_updateProfileDivider_21() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateProfileDivider_21)); }
	inline UISprite_t603616735 * get_updateProfileDivider_21() const { return ___updateProfileDivider_21; }
	inline UISprite_t603616735 ** get_address_of_updateProfileDivider_21() { return &___updateProfileDivider_21; }
	inline void set_updateProfileDivider_21(UISprite_t603616735 * value)
	{
		___updateProfileDivider_21 = value;
		Il2CppCodeGenWriteBarrier(&___updateProfileDivider_21, value);
	}

	inline static int32_t get_offset_of_stealthMode_22() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___stealthMode_22)); }
	inline Transform_t3275118058 * get_stealthMode_22() const { return ___stealthMode_22; }
	inline Transform_t3275118058 ** get_address_of_stealthMode_22() { return &___stealthMode_22; }
	inline void set_stealthMode_22(Transform_t3275118058 * value)
	{
		___stealthMode_22 = value;
		Il2CppCodeGenWriteBarrier(&___stealthMode_22, value);
	}

	inline static int32_t get_offset_of_stealthModeCollider_23() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___stealthModeCollider_23)); }
	inline BoxCollider_t22920061 * get_stealthModeCollider_23() const { return ___stealthModeCollider_23; }
	inline BoxCollider_t22920061 ** get_address_of_stealthModeCollider_23() { return &___stealthModeCollider_23; }
	inline void set_stealthModeCollider_23(BoxCollider_t22920061 * value)
	{
		___stealthModeCollider_23 = value;
		Il2CppCodeGenWriteBarrier(&___stealthModeCollider_23, value);
	}

	inline static int32_t get_offset_of_stealthModeDivider_24() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___stealthModeDivider_24)); }
	inline UISprite_t603616735 * get_stealthModeDivider_24() const { return ___stealthModeDivider_24; }
	inline UISprite_t603616735 ** get_address_of_stealthModeDivider_24() { return &___stealthModeDivider_24; }
	inline void set_stealthModeDivider_24(UISprite_t603616735 * value)
	{
		___stealthModeDivider_24 = value;
		Il2CppCodeGenWriteBarrier(&___stealthModeDivider_24, value);
	}

	inline static int32_t get_offset_of_updateStealthModeLabel_25() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateStealthModeLabel_25)); }
	inline UILabel_t1795115428 * get_updateStealthModeLabel_25() const { return ___updateStealthModeLabel_25; }
	inline UILabel_t1795115428 ** get_address_of_updateStealthModeLabel_25() { return &___updateStealthModeLabel_25; }
	inline void set_updateStealthModeLabel_25(UILabel_t1795115428 * value)
	{
		___updateStealthModeLabel_25 = value;
		Il2CppCodeGenWriteBarrier(&___updateStealthModeLabel_25, value);
	}

	inline static int32_t get_offset_of_updateStealthModeNav_26() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateStealthModeNav_26)); }
	inline UISprite_t603616735 * get_updateStealthModeNav_26() const { return ___updateStealthModeNav_26; }
	inline UISprite_t603616735 ** get_address_of_updateStealthModeNav_26() { return &___updateStealthModeNav_26; }
	inline void set_updateStealthModeNav_26(UISprite_t603616735 * value)
	{
		___updateStealthModeNav_26 = value;
		Il2CppCodeGenWriteBarrier(&___updateStealthModeNav_26, value);
	}

	inline static int32_t get_offset_of_updateStealthModeCollider_27() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateStealthModeCollider_27)); }
	inline BoxCollider_t22920061 * get_updateStealthModeCollider_27() const { return ___updateStealthModeCollider_27; }
	inline BoxCollider_t22920061 ** get_address_of_updateStealthModeCollider_27() { return &___updateStealthModeCollider_27; }
	inline void set_updateStealthModeCollider_27(BoxCollider_t22920061 * value)
	{
		___updateStealthModeCollider_27 = value;
		Il2CppCodeGenWriteBarrier(&___updateStealthModeCollider_27, value);
	}

	inline static int32_t get_offset_of_updateStealthModeDivider_28() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___updateStealthModeDivider_28)); }
	inline UISprite_t603616735 * get_updateStealthModeDivider_28() const { return ___updateStealthModeDivider_28; }
	inline UISprite_t603616735 ** get_address_of_updateStealthModeDivider_28() { return &___updateStealthModeDivider_28; }
	inline void set_updateStealthModeDivider_28(UISprite_t603616735 * value)
	{
		___updateStealthModeDivider_28 = value;
		Il2CppCodeGenWriteBarrier(&___updateStealthModeDivider_28, value);
	}

	inline static int32_t get_offset_of_middleCollider_29() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___middleCollider_29)); }
	inline BoxCollider_t22920061 * get_middleCollider_29() const { return ___middleCollider_29; }
	inline BoxCollider_t22920061 ** get_address_of_middleCollider_29() { return &___middleCollider_29; }
	inline void set_middleCollider_29(BoxCollider_t22920061 * value)
	{
		___middleCollider_29 = value;
		Il2CppCodeGenWriteBarrier(&___middleCollider_29, value);
	}

	inline static int32_t get_offset_of_aAndBTopDivider_30() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___aAndBTopDivider_30)); }
	inline UISprite_t603616735 * get_aAndBTopDivider_30() const { return ___aAndBTopDivider_30; }
	inline UISprite_t603616735 ** get_address_of_aAndBTopDivider_30() { return &___aAndBTopDivider_30; }
	inline void set_aAndBTopDivider_30(UISprite_t603616735 * value)
	{
		___aAndBTopDivider_30 = value;
		Il2CppCodeGenWriteBarrier(&___aAndBTopDivider_30, value);
	}

	inline static int32_t get_offset_of_aAndBLabel_31() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___aAndBLabel_31)); }
	inline UILabel_t1795115428 * get_aAndBLabel_31() const { return ___aAndBLabel_31; }
	inline UILabel_t1795115428 ** get_address_of_aAndBLabel_31() { return &___aAndBLabel_31; }
	inline void set_aAndBLabel_31(UILabel_t1795115428 * value)
	{
		___aAndBLabel_31 = value;
		Il2CppCodeGenWriteBarrier(&___aAndBLabel_31, value);
	}

	inline static int32_t get_offset_of_aAndBNav_32() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___aAndBNav_32)); }
	inline UISprite_t603616735 * get_aAndBNav_32() const { return ___aAndBNav_32; }
	inline UISprite_t603616735 ** get_address_of_aAndBNav_32() { return &___aAndBNav_32; }
	inline void set_aAndBNav_32(UISprite_t603616735 * value)
	{
		___aAndBNav_32 = value;
		Il2CppCodeGenWriteBarrier(&___aAndBNav_32, value);
	}

	inline static int32_t get_offset_of_aAndBCollider_33() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___aAndBCollider_33)); }
	inline BoxCollider_t22920061 * get_aAndBCollider_33() const { return ___aAndBCollider_33; }
	inline BoxCollider_t22920061 ** get_address_of_aAndBCollider_33() { return &___aAndBCollider_33; }
	inline void set_aAndBCollider_33(BoxCollider_t22920061 * value)
	{
		___aAndBCollider_33 = value;
		Il2CppCodeGenWriteBarrier(&___aAndBCollider_33, value);
	}

	inline static int32_t get_offset_of_aAndBBottomDivider_34() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___aAndBBottomDivider_34)); }
	inline UISprite_t603616735 * get_aAndBBottomDivider_34() const { return ___aAndBBottomDivider_34; }
	inline UISprite_t603616735 ** get_address_of_aAndBBottomDivider_34() { return &___aAndBBottomDivider_34; }
	inline void set_aAndBBottomDivider_34(UISprite_t603616735 * value)
	{
		___aAndBBottomDivider_34 = value;
		Il2CppCodeGenWriteBarrier(&___aAndBBottomDivider_34, value);
	}

	inline static int32_t get_offset_of_notificationsLabel_35() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___notificationsLabel_35)); }
	inline UILabel_t1795115428 * get_notificationsLabel_35() const { return ___notificationsLabel_35; }
	inline UILabel_t1795115428 ** get_address_of_notificationsLabel_35() { return &___notificationsLabel_35; }
	inline void set_notificationsLabel_35(UILabel_t1795115428 * value)
	{
		___notificationsLabel_35 = value;
		Il2CppCodeGenWriteBarrier(&___notificationsLabel_35, value);
	}

	inline static int32_t get_offset_of_notificationsNav_36() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___notificationsNav_36)); }
	inline UISprite_t603616735 * get_notificationsNav_36() const { return ___notificationsNav_36; }
	inline UISprite_t603616735 ** get_address_of_notificationsNav_36() { return &___notificationsNav_36; }
	inline void set_notificationsNav_36(UISprite_t603616735 * value)
	{
		___notificationsNav_36 = value;
		Il2CppCodeGenWriteBarrier(&___notificationsNav_36, value);
	}

	inline static int32_t get_offset_of_notificationsCollider_37() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___notificationsCollider_37)); }
	inline BoxCollider_t22920061 * get_notificationsCollider_37() const { return ___notificationsCollider_37; }
	inline BoxCollider_t22920061 ** get_address_of_notificationsCollider_37() { return &___notificationsCollider_37; }
	inline void set_notificationsCollider_37(BoxCollider_t22920061 * value)
	{
		___notificationsCollider_37 = value;
		Il2CppCodeGenWriteBarrier(&___notificationsCollider_37, value);
	}

	inline static int32_t get_offset_of_notificationsDivider_38() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___notificationsDivider_38)); }
	inline UISprite_t603616735 * get_notificationsDivider_38() const { return ___notificationsDivider_38; }
	inline UISprite_t603616735 ** get_address_of_notificationsDivider_38() { return &___notificationsDivider_38; }
	inline void set_notificationsDivider_38(UISprite_t603616735 * value)
	{
		___notificationsDivider_38 = value;
		Il2CppCodeGenWriteBarrier(&___notificationsDivider_38, value);
	}

	inline static int32_t get_offset_of_audioLabel_39() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___audioLabel_39)); }
	inline UILabel_t1795115428 * get_audioLabel_39() const { return ___audioLabel_39; }
	inline UILabel_t1795115428 ** get_address_of_audioLabel_39() { return &___audioLabel_39; }
	inline void set_audioLabel_39(UILabel_t1795115428 * value)
	{
		___audioLabel_39 = value;
		Il2CppCodeGenWriteBarrier(&___audioLabel_39, value);
	}

	inline static int32_t get_offset_of_audioNav_40() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___audioNav_40)); }
	inline UISprite_t603616735 * get_audioNav_40() const { return ___audioNav_40; }
	inline UISprite_t603616735 ** get_address_of_audioNav_40() { return &___audioNav_40; }
	inline void set_audioNav_40(UISprite_t603616735 * value)
	{
		___audioNav_40 = value;
		Il2CppCodeGenWriteBarrier(&___audioNav_40, value);
	}

	inline static int32_t get_offset_of_audioCollider_41() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___audioCollider_41)); }
	inline BoxCollider_t22920061 * get_audioCollider_41() const { return ___audioCollider_41; }
	inline BoxCollider_t22920061 ** get_address_of_audioCollider_41() { return &___audioCollider_41; }
	inline void set_audioCollider_41(BoxCollider_t22920061 * value)
	{
		___audioCollider_41 = value;
		Il2CppCodeGenWriteBarrier(&___audioCollider_41, value);
	}

	inline static int32_t get_offset_of_audioDivider_42() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___audioDivider_42)); }
	inline UISprite_t603616735 * get_audioDivider_42() const { return ___audioDivider_42; }
	inline UISprite_t603616735 ** get_address_of_audioDivider_42() { return &___audioDivider_42; }
	inline void set_audioDivider_42(UISprite_t603616735 * value)
	{
		___audioDivider_42 = value;
		Il2CppCodeGenWriteBarrier(&___audioDivider_42, value);
	}

	inline static int32_t get_offset_of_tellAFriendLabel_43() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___tellAFriendLabel_43)); }
	inline UILabel_t1795115428 * get_tellAFriendLabel_43() const { return ___tellAFriendLabel_43; }
	inline UILabel_t1795115428 ** get_address_of_tellAFriendLabel_43() { return &___tellAFriendLabel_43; }
	inline void set_tellAFriendLabel_43(UILabel_t1795115428 * value)
	{
		___tellAFriendLabel_43 = value;
		Il2CppCodeGenWriteBarrier(&___tellAFriendLabel_43, value);
	}

	inline static int32_t get_offset_of_tellAFriendCollider_44() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___tellAFriendCollider_44)); }
	inline BoxCollider_t22920061 * get_tellAFriendCollider_44() const { return ___tellAFriendCollider_44; }
	inline BoxCollider_t22920061 ** get_address_of_tellAFriendCollider_44() { return &___tellAFriendCollider_44; }
	inline void set_tellAFriendCollider_44(BoxCollider_t22920061 * value)
	{
		___tellAFriendCollider_44 = value;
		Il2CppCodeGenWriteBarrier(&___tellAFriendCollider_44, value);
	}

	inline static int32_t get_offset_of_tellAFriendDivider_45() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___tellAFriendDivider_45)); }
	inline UISprite_t603616735 * get_tellAFriendDivider_45() const { return ___tellAFriendDivider_45; }
	inline UISprite_t603616735 ** get_address_of_tellAFriendDivider_45() { return &___tellAFriendDivider_45; }
	inline void set_tellAFriendDivider_45(UISprite_t603616735 * value)
	{
		___tellAFriendDivider_45 = value;
		Il2CppCodeGenWriteBarrier(&___tellAFriendDivider_45, value);
	}

	inline static int32_t get_offset_of_rateThisAppLabel_46() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___rateThisAppLabel_46)); }
	inline UILabel_t1795115428 * get_rateThisAppLabel_46() const { return ___rateThisAppLabel_46; }
	inline UILabel_t1795115428 ** get_address_of_rateThisAppLabel_46() { return &___rateThisAppLabel_46; }
	inline void set_rateThisAppLabel_46(UILabel_t1795115428 * value)
	{
		___rateThisAppLabel_46 = value;
		Il2CppCodeGenWriteBarrier(&___rateThisAppLabel_46, value);
	}

	inline static int32_t get_offset_of_rateThisAppCollider_47() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___rateThisAppCollider_47)); }
	inline BoxCollider_t22920061 * get_rateThisAppCollider_47() const { return ___rateThisAppCollider_47; }
	inline BoxCollider_t22920061 ** get_address_of_rateThisAppCollider_47() { return &___rateThisAppCollider_47; }
	inline void set_rateThisAppCollider_47(BoxCollider_t22920061 * value)
	{
		___rateThisAppCollider_47 = value;
		Il2CppCodeGenWriteBarrier(&___rateThisAppCollider_47, value);
	}

	inline static int32_t get_offset_of_rateThisAppDivider_48() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___rateThisAppDivider_48)); }
	inline UISprite_t603616735 * get_rateThisAppDivider_48() const { return ___rateThisAppDivider_48; }
	inline UISprite_t603616735 ** get_address_of_rateThisAppDivider_48() { return &___rateThisAppDivider_48; }
	inline void set_rateThisAppDivider_48(UISprite_t603616735 * value)
	{
		___rateThisAppDivider_48 = value;
		Il2CppCodeGenWriteBarrier(&___rateThisAppDivider_48, value);
	}

	inline static int32_t get_offset_of_bottomCollider_49() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___bottomCollider_49)); }
	inline BoxCollider_t22920061 * get_bottomCollider_49() const { return ___bottomCollider_49; }
	inline BoxCollider_t22920061 ** get_address_of_bottomCollider_49() { return &___bottomCollider_49; }
	inline void set_bottomCollider_49(BoxCollider_t22920061 * value)
	{
		___bottomCollider_49 = value;
		Il2CppCodeGenWriteBarrier(&___bottomCollider_49, value);
	}

	inline static int32_t get_offset_of_logoutLabel_50() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___logoutLabel_50)); }
	inline UILabel_t1795115428 * get_logoutLabel_50() const { return ___logoutLabel_50; }
	inline UILabel_t1795115428 ** get_address_of_logoutLabel_50() { return &___logoutLabel_50; }
	inline void set_logoutLabel_50(UILabel_t1795115428 * value)
	{
		___logoutLabel_50 = value;
		Il2CppCodeGenWriteBarrier(&___logoutLabel_50, value);
	}

	inline static int32_t get_offset_of_logoutNav_51() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___logoutNav_51)); }
	inline UISprite_t603616735 * get_logoutNav_51() const { return ___logoutNav_51; }
	inline UISprite_t603616735 ** get_address_of_logoutNav_51() { return &___logoutNav_51; }
	inline void set_logoutNav_51(UISprite_t603616735 * value)
	{
		___logoutNav_51 = value;
		Il2CppCodeGenWriteBarrier(&___logoutNav_51, value);
	}

	inline static int32_t get_offset_of_logoutCollider_52() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___logoutCollider_52)); }
	inline BoxCollider_t22920061 * get_logoutCollider_52() const { return ___logoutCollider_52; }
	inline BoxCollider_t22920061 ** get_address_of_logoutCollider_52() { return &___logoutCollider_52; }
	inline void set_logoutCollider_52(BoxCollider_t22920061 * value)
	{
		___logoutCollider_52 = value;
		Il2CppCodeGenWriteBarrier(&___logoutCollider_52, value);
	}

	inline static int32_t get_offset_of_logoutDivider_53() { return static_cast<int32_t>(offsetof(SettingsHomeUIScaler_t3836684136, ___logoutDivider_53)); }
	inline UISprite_t603616735 * get_logoutDivider_53() const { return ___logoutDivider_53; }
	inline UISprite_t603616735 ** get_address_of_logoutDivider_53() { return &___logoutDivider_53; }
	inline void set_logoutDivider_53(UISprite_t603616735 * value)
	{
		___logoutDivider_53 = value;
		Il2CppCodeGenWriteBarrier(&___logoutDivider_53, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
