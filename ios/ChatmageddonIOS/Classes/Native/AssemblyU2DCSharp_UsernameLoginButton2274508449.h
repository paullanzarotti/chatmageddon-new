﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UsernameLoginButton
struct  UsernameLoginButton_t2274508449  : public SFXButton_t792651341
{
public:
	// UnityEngine.GameObject UsernameLoginButton::loginUI
	GameObject_t1756533147 * ___loginUI_5;
	// UnityEngine.GameObject UsernameLoginButton::closeLoginButton
	GameObject_t1756533147 * ___closeLoginButton_6;
	// UIInput UsernameLoginButton::usernameInput
	UIInput_t860674234 * ___usernameInput_7;
	// UIInput UsernameLoginButton::passwordInput
	UIInput_t860674234 * ___passwordInput_8;
	// System.Boolean UsernameLoginButton::unloaded
	bool ___unloaded_9;
	// System.Boolean UsernameLoginButton::userLoginSuccess
	bool ___userLoginSuccess_10;

public:
	inline static int32_t get_offset_of_loginUI_5() { return static_cast<int32_t>(offsetof(UsernameLoginButton_t2274508449, ___loginUI_5)); }
	inline GameObject_t1756533147 * get_loginUI_5() const { return ___loginUI_5; }
	inline GameObject_t1756533147 ** get_address_of_loginUI_5() { return &___loginUI_5; }
	inline void set_loginUI_5(GameObject_t1756533147 * value)
	{
		___loginUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___loginUI_5, value);
	}

	inline static int32_t get_offset_of_closeLoginButton_6() { return static_cast<int32_t>(offsetof(UsernameLoginButton_t2274508449, ___closeLoginButton_6)); }
	inline GameObject_t1756533147 * get_closeLoginButton_6() const { return ___closeLoginButton_6; }
	inline GameObject_t1756533147 ** get_address_of_closeLoginButton_6() { return &___closeLoginButton_6; }
	inline void set_closeLoginButton_6(GameObject_t1756533147 * value)
	{
		___closeLoginButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___closeLoginButton_6, value);
	}

	inline static int32_t get_offset_of_usernameInput_7() { return static_cast<int32_t>(offsetof(UsernameLoginButton_t2274508449, ___usernameInput_7)); }
	inline UIInput_t860674234 * get_usernameInput_7() const { return ___usernameInput_7; }
	inline UIInput_t860674234 ** get_address_of_usernameInput_7() { return &___usernameInput_7; }
	inline void set_usernameInput_7(UIInput_t860674234 * value)
	{
		___usernameInput_7 = value;
		Il2CppCodeGenWriteBarrier(&___usernameInput_7, value);
	}

	inline static int32_t get_offset_of_passwordInput_8() { return static_cast<int32_t>(offsetof(UsernameLoginButton_t2274508449, ___passwordInput_8)); }
	inline UIInput_t860674234 * get_passwordInput_8() const { return ___passwordInput_8; }
	inline UIInput_t860674234 ** get_address_of_passwordInput_8() { return &___passwordInput_8; }
	inline void set_passwordInput_8(UIInput_t860674234 * value)
	{
		___passwordInput_8 = value;
		Il2CppCodeGenWriteBarrier(&___passwordInput_8, value);
	}

	inline static int32_t get_offset_of_unloaded_9() { return static_cast<int32_t>(offsetof(UsernameLoginButton_t2274508449, ___unloaded_9)); }
	inline bool get_unloaded_9() const { return ___unloaded_9; }
	inline bool* get_address_of_unloaded_9() { return &___unloaded_9; }
	inline void set_unloaded_9(bool value)
	{
		___unloaded_9 = value;
	}

	inline static int32_t get_offset_of_userLoginSuccess_10() { return static_cast<int32_t>(offsetof(UsernameLoginButton_t2274508449, ___userLoginSuccess_10)); }
	inline bool get_userLoginSuccess_10() const { return ___userLoginSuccess_10; }
	inline bool* get_address_of_userLoginSuccess_10() { return &___userLoginSuccess_10; }
	inline void set_userLoginSuccess_10(bool value)
	{
		___userLoginSuccess_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
