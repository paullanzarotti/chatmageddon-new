﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<FriendsNavigationController>::.ctor()
#define MonoSingleton_1__ctor_m1093475703(__this, method) ((  void (*) (MonoSingleton_1_t452806953 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendsNavigationController>::Awake()
#define MonoSingleton_1_Awake_m1724853482(__this, method) ((  void (*) (MonoSingleton_1_t452806953 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<FriendsNavigationController>::get_Instance()
#define MonoSingleton_1_get_Instance_m3881016018(__this /* static, unused */, method) ((  FriendsNavigationController_t702141233 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<FriendsNavigationController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2205796656(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<FriendsNavigationController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2737688369(__this, method) ((  void (*) (MonoSingleton_1_t452806953 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendsNavigationController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1311389473(__this, method) ((  void (*) (MonoSingleton_1_t452806953 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<FriendsNavigationController>::.cctor()
#define MonoSingleton_1__cctor_m1503509464(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
