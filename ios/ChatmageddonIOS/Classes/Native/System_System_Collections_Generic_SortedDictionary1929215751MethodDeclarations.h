﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Object>
struct Node_t1929215751;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.RBTree/Node
struct Node_t2499136326;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Object>::.ctor(TKey)
extern "C"  void Node__ctor_m1387298521_gshared (Node_t1929215751 * __this, int32_t ___key0, const MethodInfo* method);
#define Node__ctor_m1387298521(__this, ___key0, method) ((  void (*) (Node_t1929215751 *, int32_t, const MethodInfo*))Node__ctor_m1387298521_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void Node__ctor_m1596926594_gshared (Node_t1929215751 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Node__ctor_m1596926594(__this, ___key0, ___value1, method) ((  void (*) (Node_t1929215751 *, int32_t, Il2CppObject *, const MethodInfo*))Node__ctor_m1596926594_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Object>::SwapValue(System.Collections.Generic.RBTree/Node)
extern "C"  void Node_SwapValue_m1870921446_gshared (Node_t1929215751 * __this, Node_t2499136326 * ___other0, const MethodInfo* method);
#define Node_SwapValue_m1870921446(__this, ___other0, method) ((  void (*) (Node_t1929215751 *, Node_t2499136326 *, const MethodInfo*))Node_SwapValue_m1870921446_gshared)(__this, ___other0, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Object>::AsKV()
extern "C"  KeyValuePair_2_t3749587448  Node_AsKV_m2459827919_gshared (Node_t1929215751 * __this, const MethodInfo* method);
#define Node_AsKV_m2459827919(__this, method) ((  KeyValuePair_2_t3749587448  (*) (Node_t1929215751 *, const MethodInfo*))Node_AsKV_m2459827919_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Object>::AsDE()
extern "C"  DictionaryEntry_t3048875398  Node_AsDE_m4001353572_gshared (Node_t1929215751 * __this, const MethodInfo* method);
#define Node_AsDE_m4001353572(__this, method) ((  DictionaryEntry_t3048875398  (*) (Node_t1929215751 *, const MethodInfo*))Node_AsDE_m4001353572_gshared)(__this, method)
