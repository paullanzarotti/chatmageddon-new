﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPPickerBase
struct IPPickerBase_t4159478266;
// TweenScale
struct TweenScale_t2697902175;
// UIWidget
struct UIWidget_t1453041918;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_UITweener_Method1694901606.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenCenterWidgetScale
struct  TweenCenterWidgetScale_t549921132  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TweenCenterWidgetScale::scaleFactor
	float ___scaleFactor_2;
	// System.Single TweenCenterWidgetScale::duration
	float ___duration_3;
	// UITweener/Method TweenCenterWidgetScale::tweenMethod
	int32_t ___tweenMethod_4;
	// IPPickerBase TweenCenterWidgetScale::_picker
	IPPickerBase_t4159478266 * ____picker_5;
	// TweenScale TweenCenterWidgetScale::_tweenScale
	TweenScale_t2697902175 * ____tweenScale_6;
	// UIWidget TweenCenterWidgetScale::_centerWidget
	UIWidget_t1453041918 * ____centerWidget_7;

public:
	inline static int32_t get_offset_of_scaleFactor_2() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScale_t549921132, ___scaleFactor_2)); }
	inline float get_scaleFactor_2() const { return ___scaleFactor_2; }
	inline float* get_address_of_scaleFactor_2() { return &___scaleFactor_2; }
	inline void set_scaleFactor_2(float value)
	{
		___scaleFactor_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScale_t549921132, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_tweenMethod_4() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScale_t549921132, ___tweenMethod_4)); }
	inline int32_t get_tweenMethod_4() const { return ___tweenMethod_4; }
	inline int32_t* get_address_of_tweenMethod_4() { return &___tweenMethod_4; }
	inline void set_tweenMethod_4(int32_t value)
	{
		___tweenMethod_4 = value;
	}

	inline static int32_t get_offset_of__picker_5() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScale_t549921132, ____picker_5)); }
	inline IPPickerBase_t4159478266 * get__picker_5() const { return ____picker_5; }
	inline IPPickerBase_t4159478266 ** get_address_of__picker_5() { return &____picker_5; }
	inline void set__picker_5(IPPickerBase_t4159478266 * value)
	{
		____picker_5 = value;
		Il2CppCodeGenWriteBarrier(&____picker_5, value);
	}

	inline static int32_t get_offset_of__tweenScale_6() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScale_t549921132, ____tweenScale_6)); }
	inline TweenScale_t2697902175 * get__tweenScale_6() const { return ____tweenScale_6; }
	inline TweenScale_t2697902175 ** get_address_of__tweenScale_6() { return &____tweenScale_6; }
	inline void set__tweenScale_6(TweenScale_t2697902175 * value)
	{
		____tweenScale_6 = value;
		Il2CppCodeGenWriteBarrier(&____tweenScale_6, value);
	}

	inline static int32_t get_offset_of__centerWidget_7() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScale_t549921132, ____centerWidget_7)); }
	inline UIWidget_t1453041918 * get__centerWidget_7() const { return ____centerWidget_7; }
	inline UIWidget_t1453041918 ** get_address_of__centerWidget_7() { return &____centerWidget_7; }
	inline void set__centerWidget_7(UIWidget_t1453041918 * value)
	{
		____centerWidget_7 = value;
		Il2CppCodeGenWriteBarrier(&____centerWidget_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
