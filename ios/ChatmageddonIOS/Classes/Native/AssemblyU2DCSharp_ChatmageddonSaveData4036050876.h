﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayerPrefSaveData
struct PlayerPrefSaveData_t170835669;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3786716596.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonSaveData
struct  ChatmageddonSaveData_t4036050876  : public MonoSingleton_1_t3786716596
{
public:
	// PlayerPrefSaveData ChatmageddonSaveData::data
	PlayerPrefSaveData_t170835669 * ___data_3;

public:
	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(ChatmageddonSaveData_t4036050876, ___data_3)); }
	inline PlayerPrefSaveData_t170835669 * get_data_3() const { return ___data_3; }
	inline PlayerPrefSaveData_t170835669 ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(PlayerPrefSaveData_t170835669 * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier(&___data_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
