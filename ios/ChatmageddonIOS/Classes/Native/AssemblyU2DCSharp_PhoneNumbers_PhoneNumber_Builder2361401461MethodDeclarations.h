﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumber/Builder
struct Builder_t2361401461;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Types_C2831594294.h"

// System.Void PhoneNumbers.PhoneNumber/Builder::.ctor()
extern "C"  void Builder__ctor_m2889927476 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::get_ThisBuilder()
extern "C"  Builder_t2361401461 * Builder_get_ThisBuilder_m1952405480 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber/Builder::get_MessageBeingBuilt()
extern "C"  PhoneNumber_t814071929 * Builder_get_MessageBeingBuilt_m1135713017 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::Clear()
extern "C"  Builder_t2361401461 * Builder_Clear_m2129798517 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::Clone()
extern "C"  Builder_t2361401461 * Builder_Clone_m346131883 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber/Builder::get_DefaultInstanceForType()
extern "C"  PhoneNumber_t814071929 * Builder_get_DefaultInstanceForType_m886029602 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber/Builder::Build()
extern "C"  PhoneNumber_t814071929 * Builder_Build_m4116093520 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber/Builder::BuildPartial()
extern "C"  PhoneNumber_t814071929 * Builder_BuildPartial_m3143672819 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::MergeFrom(PhoneNumbers.PhoneNumber)
extern "C"  Builder_t2361401461 * Builder_MergeFrom_m1194524623 (Builder_t2361401461 * __this, PhoneNumber_t814071929 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_HasCountryCode()
extern "C"  bool Builder_get_HasCountryCode_m3002855854 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneNumber/Builder::get_CountryCode()
extern "C"  int32_t Builder_get_CountryCode_m3010595194 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber/Builder::set_CountryCode(System.Int32)
extern "C"  void Builder_set_CountryCode_m1149441749 (Builder_t2361401461 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::SetCountryCode(System.Int32)
extern "C"  Builder_t2361401461 * Builder_SetCountryCode_m514563272 (Builder_t2361401461 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::ClearCountryCode()
extern "C"  Builder_t2361401461 * Builder_ClearCountryCode_m3791294926 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_HasNationalNumber()
extern "C"  bool Builder_get_HasNationalNumber_m1278803966 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 PhoneNumbers.PhoneNumber/Builder::get_NationalNumber()
extern "C"  uint64_t Builder_get_NationalNumber_m2476926960 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber/Builder::set_NationalNumber(System.UInt64)
extern "C"  void Builder_set_NationalNumber_m1259001019 (Builder_t2361401461 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::SetNationalNumber(System.UInt64)
extern "C"  Builder_t2361401461 * Builder_SetNationalNumber_m2971499406 (Builder_t2361401461 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::ClearNationalNumber()
extern "C"  Builder_t2361401461 * Builder_ClearNationalNumber_m2410951426 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_HasExtension()
extern "C"  bool Builder_get_HasExtension_m2504235672 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumber/Builder::get_Extension()
extern "C"  String_t* Builder_get_Extension_m2273983105 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber/Builder::set_Extension(System.String)
extern "C"  void Builder_set_Extension_m849020328 (Builder_t2361401461 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::SetExtension(System.String)
extern "C"  Builder_t2361401461 * Builder_SetExtension_m337848373 (Builder_t2361401461 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::ClearExtension()
extern "C"  Builder_t2361401461 * Builder_ClearExtension_m186952092 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_HasItalianLeadingZero()
extern "C"  bool Builder_get_HasItalianLeadingZero_m2042688599 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_ItalianLeadingZero()
extern "C"  bool Builder_get_ItalianLeadingZero_m3915832629 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber/Builder::set_ItalianLeadingZero(System.Boolean)
extern "C"  void Builder_set_ItalianLeadingZero_m1241739138 (Builder_t2361401461 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::SetItalianLeadingZero(System.Boolean)
extern "C"  Builder_t2361401461 * Builder_SetItalianLeadingZero_m2667607029 (Builder_t2361401461 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::ClearItalianLeadingZero()
extern "C"  Builder_t2361401461 * Builder_ClearItalianLeadingZero_m13271197 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_HasRawInput()
extern "C"  bool Builder_get_HasRawInput_m432273625 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumber/Builder::get_RawInput()
extern "C"  String_t* Builder_get_RawInput_m2696532048 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber/Builder::set_RawInput(System.String)
extern "C"  void Builder_set_RawInput_m1651849529 (Builder_t2361401461 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::SetRawInput(System.String)
extern "C"  Builder_t2361401461 * Builder_SetRawInput_m4213420520 (Builder_t2361401461 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::ClearRawInput()
extern "C"  Builder_t2361401461 * Builder_ClearRawInput_m1884107495 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_HasCountryCodeSource()
extern "C"  bool Builder_get_HasCountryCodeSource_m1171359351 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Types/CountryCodeSource PhoneNumbers.PhoneNumber/Builder::get_CountryCodeSource()
extern "C"  int32_t Builder_get_CountryCodeSource_m455980430 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber/Builder::set_CountryCodeSource(PhoneNumbers.PhoneNumber/Types/CountryCodeSource)
extern "C"  void Builder_set_CountryCodeSource_m2842373887 (Builder_t2361401461 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::SetCountryCodeSource(PhoneNumbers.PhoneNumber/Types/CountryCodeSource)
extern "C"  Builder_t2361401461 * Builder_SetCountryCodeSource_m1501865070 (Builder_t2361401461 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::ClearCountryCodeSource()
extern "C"  Builder_t2361401461 * Builder_ClearCountryCodeSource_m159748381 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumber/Builder::get_HasPreferredDomesticCarrierCode()
extern "C"  bool Builder_get_HasPreferredDomesticCarrierCode_m3997538137 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumber/Builder::get_PreferredDomesticCarrierCode()
extern "C"  String_t* Builder_get_PreferredDomesticCarrierCode_m2322995680 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumber/Builder::set_PreferredDomesticCarrierCode(System.String)
extern "C"  void Builder_set_PreferredDomesticCarrierCode_m3235822201 (Builder_t2361401461 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::SetPreferredDomesticCarrierCode(System.String)
extern "C"  Builder_t2361401461 * Builder_SetPreferredDomesticCarrierCode_m3800060808 (Builder_t2361401461 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumber/Builder PhoneNumbers.PhoneNumber/Builder::ClearPreferredDomesticCarrierCode()
extern "C"  Builder_t2361401461 * Builder_ClearPreferredDomesticCarrierCode_m1653813087 (Builder_t2361401461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
