﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoPointsWaitButton
struct NoPointsWaitButton_t1249750581;
// NoPointsWaitButton/OnZeroHit
struct OnZeroHit_t3971658986;
// User
struct User_t719925459;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton_OnZeroHit3971658986.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void NoPointsWaitButton::.ctor()
extern "C"  void NoPointsWaitButton__ctor_m3777619072 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::add_onZeroHit(NoPointsWaitButton/OnZeroHit)
extern "C"  void NoPointsWaitButton_add_onZeroHit_m1102814586 (NoPointsWaitButton_t1249750581 * __this, OnZeroHit_t3971658986 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::remove_onZeroHit(NoPointsWaitButton/OnZeroHit)
extern "C"  void NoPointsWaitButton_remove_onZeroHit_m3018186249 (NoPointsWaitButton_t1249750581 * __this, OnZeroHit_t3971658986 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::Awake()
extern "C"  void NoPointsWaitButton_Awake_m114813413 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::OnButtonClick()
extern "C"  void NoPointsWaitButton_OnButtonClick_m709115033 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::DisplayMissileDefendedToast()
extern "C"  void NoPointsWaitButton_DisplayMissileDefendedToast_m682990282 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::StartCalculator(User)
extern "C"  void NoPointsWaitButton_StartCalculator_m1858778849 (NoPointsWaitButton_t1249750581 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::StopCalculator()
extern "C"  void NoPointsWaitButton_StopCalculator_m908928262 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NoPointsWaitButton::CalculateKnockoutTime(User)
extern "C"  Il2CppObject * NoPointsWaitButton_CalculateKnockoutTime_m3880034308 (NoPointsWaitButton_t1249750581 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan NoPointsWaitButton::GetKnockoutTimeSpan(User)
extern "C"  TimeSpan_t3430258949  NoPointsWaitButton_GetKnockoutTimeSpan_m2052793211 (NoPointsWaitButton_t1249750581 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::SetTimeLabel(System.TimeSpan)
extern "C"  void NoPointsWaitButton_SetTimeLabel_m1218474551 (NoPointsWaitButton_t1249750581 * __this, TimeSpan_t3430258949  ___span0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::SetTimeLabelZero()
extern "C"  void NoPointsWaitButton_SetTimeLabelZero_m3708579135 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::SendZeroHitEvent()
extern "C"  void NoPointsWaitButton_SendZeroHitEvent_m2831413611 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::ShowExitButton(System.Boolean)
extern "C"  void NoPointsWaitButton_ShowExitButton_m957422178 (NoPointsWaitButton_t1249750581 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::WaitTimeTweenFinished()
extern "C"  void NoPointsWaitButton_WaitTimeTweenFinished_m1363972817 (NoPointsWaitButton_t1249750581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoPointsWaitButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void NoPointsWaitButton_U3COnButtonClickU3Em__0_m1780878847 (NoPointsWaitButton_t1249750581 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
