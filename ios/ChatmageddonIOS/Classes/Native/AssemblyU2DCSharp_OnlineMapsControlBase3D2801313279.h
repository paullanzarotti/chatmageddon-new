﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3226471752;
// UnityEngine.Camera
struct Camera_t189460977;
// OnlineMapsMarker3D[]
struct OnlineMapsMarker3DU5BU5D_t1514879522;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Renderer
struct Renderer_t257310565;
// System.Collections.Generic.Dictionary`2<System.Int32,OnlineMapsMarkerBillboard>
struct Dictionary_2_t3797896220;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Func`2<OnlineMapsMarker3D,System.Boolean>
struct Func_2_t1861874992;

#include "AssemblyU2DCSharp_OnlineMapsControlBase473237564.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker2DMode2893795757.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsControlBase3D
struct  OnlineMapsControlBase3D_t2801313279  : public OnlineMapsControlBase_t473237564
{
public:
	// System.Action OnlineMapsControlBase3D::OnCameraControl
	Action_t3226471752 * ___OnCameraControl_33;
	// UnityEngine.Camera OnlineMapsControlBase3D::activeCamera
	Camera_t189460977 * ___activeCamera_34;
	// System.Boolean OnlineMapsControlBase3D::allowDefaultMarkerEvents
	bool ___allowDefaultMarkerEvents_35;
	// System.Boolean OnlineMapsControlBase3D::allowCameraControl
	bool ___allowCameraControl_36;
	// System.Single OnlineMapsControlBase3D::cameraDistance
	float ___cameraDistance_37;
	// UnityEngine.Vector2 OnlineMapsControlBase3D::cameraRotation
	Vector2_t2243707579  ___cameraRotation_38;
	// UnityEngine.Vector2 OnlineMapsControlBase3D::cameraSpeed
	Vector2_t2243707579  ___cameraSpeed_39;
	// OnlineMapsMarker2DMode OnlineMapsControlBase3D::marker2DMode
	int32_t ___marker2DMode_40;
	// System.Single OnlineMapsControlBase3D::marker2DSize
	float ___marker2DSize_41;
	// OnlineMapsMarker3D[] OnlineMapsControlBase3D::markers3D
	OnlineMapsMarker3DU5BU5D_t1514879522* ___markers3D_42;
	// System.Single OnlineMapsControlBase3D::marker3DScale
	float ___marker3DScale_43;
	// System.Boolean OnlineMapsControlBase3D::allowAddMarker3DByN
	bool ___allowAddMarker3DByN_44;
	// UnityEngine.GameObject OnlineMapsControlBase3D::markersGameObject
	GameObject_t1756533147 * ___markersGameObject_45;
	// UnityEngine.Mesh OnlineMapsControlBase3D::markersMesh
	Mesh_t1356156583 * ___markersMesh_46;
	// UnityEngine.Renderer OnlineMapsControlBase3D::markersRenderer
	Renderer_t257310565 * ___markersRenderer_47;
	// System.Boolean OnlineMapsControlBase3D::isCameraControl
	bool ___isCameraControl_48;
	// System.Collections.Generic.Dictionary`2<System.Int32,OnlineMapsMarkerBillboard> OnlineMapsControlBase3D::markerBillboards
	Dictionary_2_t3797896220 * ___markerBillboards_49;
	// UnityEngine.Collider OnlineMapsControlBase3D::_cl
	Collider_t3497673348 * ____cl_50;
	// UnityEngine.Renderer OnlineMapsControlBase3D::_renderer
	Renderer_t257310565 * ____renderer_51;

public:
	inline static int32_t get_offset_of_OnCameraControl_33() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___OnCameraControl_33)); }
	inline Action_t3226471752 * get_OnCameraControl_33() const { return ___OnCameraControl_33; }
	inline Action_t3226471752 ** get_address_of_OnCameraControl_33() { return &___OnCameraControl_33; }
	inline void set_OnCameraControl_33(Action_t3226471752 * value)
	{
		___OnCameraControl_33 = value;
		Il2CppCodeGenWriteBarrier(&___OnCameraControl_33, value);
	}

	inline static int32_t get_offset_of_activeCamera_34() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___activeCamera_34)); }
	inline Camera_t189460977 * get_activeCamera_34() const { return ___activeCamera_34; }
	inline Camera_t189460977 ** get_address_of_activeCamera_34() { return &___activeCamera_34; }
	inline void set_activeCamera_34(Camera_t189460977 * value)
	{
		___activeCamera_34 = value;
		Il2CppCodeGenWriteBarrier(&___activeCamera_34, value);
	}

	inline static int32_t get_offset_of_allowDefaultMarkerEvents_35() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___allowDefaultMarkerEvents_35)); }
	inline bool get_allowDefaultMarkerEvents_35() const { return ___allowDefaultMarkerEvents_35; }
	inline bool* get_address_of_allowDefaultMarkerEvents_35() { return &___allowDefaultMarkerEvents_35; }
	inline void set_allowDefaultMarkerEvents_35(bool value)
	{
		___allowDefaultMarkerEvents_35 = value;
	}

	inline static int32_t get_offset_of_allowCameraControl_36() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___allowCameraControl_36)); }
	inline bool get_allowCameraControl_36() const { return ___allowCameraControl_36; }
	inline bool* get_address_of_allowCameraControl_36() { return &___allowCameraControl_36; }
	inline void set_allowCameraControl_36(bool value)
	{
		___allowCameraControl_36 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_37() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___cameraDistance_37)); }
	inline float get_cameraDistance_37() const { return ___cameraDistance_37; }
	inline float* get_address_of_cameraDistance_37() { return &___cameraDistance_37; }
	inline void set_cameraDistance_37(float value)
	{
		___cameraDistance_37 = value;
	}

	inline static int32_t get_offset_of_cameraRotation_38() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___cameraRotation_38)); }
	inline Vector2_t2243707579  get_cameraRotation_38() const { return ___cameraRotation_38; }
	inline Vector2_t2243707579 * get_address_of_cameraRotation_38() { return &___cameraRotation_38; }
	inline void set_cameraRotation_38(Vector2_t2243707579  value)
	{
		___cameraRotation_38 = value;
	}

	inline static int32_t get_offset_of_cameraSpeed_39() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___cameraSpeed_39)); }
	inline Vector2_t2243707579  get_cameraSpeed_39() const { return ___cameraSpeed_39; }
	inline Vector2_t2243707579 * get_address_of_cameraSpeed_39() { return &___cameraSpeed_39; }
	inline void set_cameraSpeed_39(Vector2_t2243707579  value)
	{
		___cameraSpeed_39 = value;
	}

	inline static int32_t get_offset_of_marker2DMode_40() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___marker2DMode_40)); }
	inline int32_t get_marker2DMode_40() const { return ___marker2DMode_40; }
	inline int32_t* get_address_of_marker2DMode_40() { return &___marker2DMode_40; }
	inline void set_marker2DMode_40(int32_t value)
	{
		___marker2DMode_40 = value;
	}

	inline static int32_t get_offset_of_marker2DSize_41() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___marker2DSize_41)); }
	inline float get_marker2DSize_41() const { return ___marker2DSize_41; }
	inline float* get_address_of_marker2DSize_41() { return &___marker2DSize_41; }
	inline void set_marker2DSize_41(float value)
	{
		___marker2DSize_41 = value;
	}

	inline static int32_t get_offset_of_markers3D_42() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___markers3D_42)); }
	inline OnlineMapsMarker3DU5BU5D_t1514879522* get_markers3D_42() const { return ___markers3D_42; }
	inline OnlineMapsMarker3DU5BU5D_t1514879522** get_address_of_markers3D_42() { return &___markers3D_42; }
	inline void set_markers3D_42(OnlineMapsMarker3DU5BU5D_t1514879522* value)
	{
		___markers3D_42 = value;
		Il2CppCodeGenWriteBarrier(&___markers3D_42, value);
	}

	inline static int32_t get_offset_of_marker3DScale_43() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___marker3DScale_43)); }
	inline float get_marker3DScale_43() const { return ___marker3DScale_43; }
	inline float* get_address_of_marker3DScale_43() { return &___marker3DScale_43; }
	inline void set_marker3DScale_43(float value)
	{
		___marker3DScale_43 = value;
	}

	inline static int32_t get_offset_of_allowAddMarker3DByN_44() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___allowAddMarker3DByN_44)); }
	inline bool get_allowAddMarker3DByN_44() const { return ___allowAddMarker3DByN_44; }
	inline bool* get_address_of_allowAddMarker3DByN_44() { return &___allowAddMarker3DByN_44; }
	inline void set_allowAddMarker3DByN_44(bool value)
	{
		___allowAddMarker3DByN_44 = value;
	}

	inline static int32_t get_offset_of_markersGameObject_45() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___markersGameObject_45)); }
	inline GameObject_t1756533147 * get_markersGameObject_45() const { return ___markersGameObject_45; }
	inline GameObject_t1756533147 ** get_address_of_markersGameObject_45() { return &___markersGameObject_45; }
	inline void set_markersGameObject_45(GameObject_t1756533147 * value)
	{
		___markersGameObject_45 = value;
		Il2CppCodeGenWriteBarrier(&___markersGameObject_45, value);
	}

	inline static int32_t get_offset_of_markersMesh_46() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___markersMesh_46)); }
	inline Mesh_t1356156583 * get_markersMesh_46() const { return ___markersMesh_46; }
	inline Mesh_t1356156583 ** get_address_of_markersMesh_46() { return &___markersMesh_46; }
	inline void set_markersMesh_46(Mesh_t1356156583 * value)
	{
		___markersMesh_46 = value;
		Il2CppCodeGenWriteBarrier(&___markersMesh_46, value);
	}

	inline static int32_t get_offset_of_markersRenderer_47() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___markersRenderer_47)); }
	inline Renderer_t257310565 * get_markersRenderer_47() const { return ___markersRenderer_47; }
	inline Renderer_t257310565 ** get_address_of_markersRenderer_47() { return &___markersRenderer_47; }
	inline void set_markersRenderer_47(Renderer_t257310565 * value)
	{
		___markersRenderer_47 = value;
		Il2CppCodeGenWriteBarrier(&___markersRenderer_47, value);
	}

	inline static int32_t get_offset_of_isCameraControl_48() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___isCameraControl_48)); }
	inline bool get_isCameraControl_48() const { return ___isCameraControl_48; }
	inline bool* get_address_of_isCameraControl_48() { return &___isCameraControl_48; }
	inline void set_isCameraControl_48(bool value)
	{
		___isCameraControl_48 = value;
	}

	inline static int32_t get_offset_of_markerBillboards_49() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ___markerBillboards_49)); }
	inline Dictionary_2_t3797896220 * get_markerBillboards_49() const { return ___markerBillboards_49; }
	inline Dictionary_2_t3797896220 ** get_address_of_markerBillboards_49() { return &___markerBillboards_49; }
	inline void set_markerBillboards_49(Dictionary_2_t3797896220 * value)
	{
		___markerBillboards_49 = value;
		Il2CppCodeGenWriteBarrier(&___markerBillboards_49, value);
	}

	inline static int32_t get_offset_of__cl_50() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ____cl_50)); }
	inline Collider_t3497673348 * get__cl_50() const { return ____cl_50; }
	inline Collider_t3497673348 ** get_address_of__cl_50() { return &____cl_50; }
	inline void set__cl_50(Collider_t3497673348 * value)
	{
		____cl_50 = value;
		Il2CppCodeGenWriteBarrier(&____cl_50, value);
	}

	inline static int32_t get_offset_of__renderer_51() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279, ____renderer_51)); }
	inline Renderer_t257310565 * get__renderer_51() const { return ____renderer_51; }
	inline Renderer_t257310565 ** get_address_of__renderer_51() { return &____renderer_51; }
	inline void set__renderer_51(Renderer_t257310565 * value)
	{
		____renderer_51 = value;
		Il2CppCodeGenWriteBarrier(&____renderer_51, value);
	}
};

struct OnlineMapsControlBase3D_t2801313279_StaticFields
{
public:
	// System.Func`2<OnlineMapsMarker3D,System.Boolean> OnlineMapsControlBase3D::<>f__am$cache0
	Func_2_t1861874992 * ___U3CU3Ef__amU24cache0_52;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_52() { return static_cast<int32_t>(offsetof(OnlineMapsControlBase3D_t2801313279_StaticFields, ___U3CU3Ef__amU24cache0_52)); }
	inline Func_2_t1861874992 * get_U3CU3Ef__amU24cache0_52() const { return ___U3CU3Ef__amU24cache0_52; }
	inline Func_2_t1861874992 ** get_address_of_U3CU3Ef__amU24cache0_52() { return &___U3CU3Ef__amU24cache0_52; }
	inline void set_U3CU3Ef__amU24cache0_52(Func_2_t1861874992 * value)
	{
		___U3CU3Ef__amU24cache0_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_52, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
