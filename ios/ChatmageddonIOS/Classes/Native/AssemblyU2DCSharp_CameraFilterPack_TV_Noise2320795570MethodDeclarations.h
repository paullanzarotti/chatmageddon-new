﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Noise
struct CameraFilterPack_TV_Noise_t2320795570;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Noise::.ctor()
extern "C"  void CameraFilterPack_TV_Noise__ctor_m930748543 (CameraFilterPack_TV_Noise_t2320795570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Noise::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Noise_get_material_m1093647694 (CameraFilterPack_TV_Noise_t2320795570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::Start()
extern "C"  void CameraFilterPack_TV_Noise_Start_m1683934883 (CameraFilterPack_TV_Noise_t2320795570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Noise_OnRenderImage_m1352820019 (CameraFilterPack_TV_Noise_t2320795570 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::OnValidate()
extern "C"  void CameraFilterPack_TV_Noise_OnValidate_m3836378474 (CameraFilterPack_TV_Noise_t2320795570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::Update()
extern "C"  void CameraFilterPack_TV_Noise_Update_m3000501500 (CameraFilterPack_TV_Noise_t2320795570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::OnDisable()
extern "C"  void CameraFilterPack_TV_Noise_OnDisable_m3106719690 (CameraFilterPack_TV_Noise_t2320795570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
