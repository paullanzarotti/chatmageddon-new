﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationManager/<CheckUnreadNotifications>c__Iterator1
struct U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void NotificationManager/<CheckUnreadNotifications>c__Iterator1::.ctor()
extern "C"  void U3CCheckUnreadNotificationsU3Ec__Iterator1__ctor_m116244885 (U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NotificationManager/<CheckUnreadNotifications>c__Iterator1::MoveNext()
extern "C"  bool U3CCheckUnreadNotificationsU3Ec__Iterator1_MoveNext_m433209019 (U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NotificationManager/<CheckUnreadNotifications>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckUnreadNotificationsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3489444699 (U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NotificationManager/<CheckUnreadNotifications>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckUnreadNotificationsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3435610771 (U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager/<CheckUnreadNotifications>c__Iterator1::Dispose()
extern "C"  void U3CCheckUnreadNotificationsU3Ec__Iterator1_Dispose_m3263668492 (U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationManager/<CheckUnreadNotifications>c__Iterator1::Reset()
extern "C"  void U3CCheckUnreadNotificationsU3Ec__Iterator1_Reset_m4230768230 (U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
