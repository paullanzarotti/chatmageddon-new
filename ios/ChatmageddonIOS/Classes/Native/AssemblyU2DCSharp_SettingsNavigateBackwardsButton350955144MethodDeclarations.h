﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsNavigateBackwardsButton
struct SettingsNavigateBackwardsButton_t350955144;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void SettingsNavigateBackwardsButton::.ctor()
extern "C"  void SettingsNavigateBackwardsButton__ctor_m100564395 (SettingsNavigateBackwardsButton_t350955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigateBackwardsButton::OnClick()
extern "C"  void SettingsNavigateBackwardsButton_OnClick_m3805735504 (SettingsNavigateBackwardsButton_t350955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigateBackwardsButton::PlaySfx()
extern "C"  void SettingsNavigateBackwardsButton_PlaySfx_m3875232326 (SettingsNavigateBackwardsButton_t350955144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigateBackwardsButton::SetButtonDirection(ButtonDirection)
extern "C"  void SettingsNavigateBackwardsButton_SetButtonDirection_m3546795985 (SettingsNavigateBackwardsButton_t350955144 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigateBackwardsButton::<OnClick>m__0(System.Boolean)
extern "C"  void SettingsNavigateBackwardsButton_U3COnClickU3Em__0_m852393186 (Il2CppObject * __this /* static, unused */, bool ___isSaved0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
