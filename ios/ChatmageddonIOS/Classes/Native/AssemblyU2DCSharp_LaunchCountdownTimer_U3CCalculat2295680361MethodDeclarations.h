﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchCountdownTimer/<CalculateCountdownTime>c__Iterator0
struct U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchCountdownTimer/<CalculateCountdownTime>c__Iterator0::.ctor()
extern "C"  void U3CCalculateCountdownTimeU3Ec__Iterator0__ctor_m371779266 (U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaunchCountdownTimer/<CalculateCountdownTime>c__Iterator0::MoveNext()
extern "C"  bool U3CCalculateCountdownTimeU3Ec__Iterator0_MoveNext_m2053881378 (U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchCountdownTimer/<CalculateCountdownTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCalculateCountdownTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m588356580 (U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchCountdownTimer/<CalculateCountdownTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCalculateCountdownTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2808474364 (U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer/<CalculateCountdownTime>c__Iterator0::Dispose()
extern "C"  void U3CCalculateCountdownTimeU3Ec__Iterator0_Dispose_m3382651107 (U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchCountdownTimer/<CalculateCountdownTime>c__Iterator0::Reset()
extern "C"  void U3CCalculateCountdownTimeU3Ec__Iterator0_Reset_m2978980693 (U3CCalculateCountdownTimeU3Ec__Iterator0_t2295680361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
