﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginCenterUIScaler
struct LoginCenterUIScaler_t3891368654;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginCenterUIScaler::.ctor()
extern "C"  void LoginCenterUIScaler__ctor_m1642126897 (LoginCenterUIScaler_t3891368654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginCenterUIScaler::GlobalUIScale()
extern "C"  void LoginCenterUIScaler_GlobalUIScale_m2979585048 (LoginCenterUIScaler_t3891368654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
