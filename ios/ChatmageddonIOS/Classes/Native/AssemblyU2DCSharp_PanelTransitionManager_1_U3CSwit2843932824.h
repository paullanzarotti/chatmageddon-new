﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PanelTransitionManager`1<System.Object>
struct PanelTransitionManager_1_t3816465789;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PanelTransitionManager`1/<SwitchPanels>c__Iterator0<System.Object>
struct  U3CSwitchPanelsU3Ec__Iterator0_t2843932824  : public Il2CppObject
{
public:
	// PanelTransitionManager`1<ManagerType> PanelTransitionManager`1/<SwitchPanels>c__Iterator0::$this
	PanelTransitionManager_1_t3816465789 * ___U24this_0;
	// System.Object PanelTransitionManager`1/<SwitchPanels>c__Iterator0::$current
	Il2CppObject * ___U24current_1;
	// System.Boolean PanelTransitionManager`1/<SwitchPanels>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PanelTransitionManager`1/<SwitchPanels>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CSwitchPanelsU3Ec__Iterator0_t2843932824, ___U24this_0)); }
	inline PanelTransitionManager_1_t3816465789 * get_U24this_0() const { return ___U24this_0; }
	inline PanelTransitionManager_1_t3816465789 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PanelTransitionManager_1_t3816465789 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_0, value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSwitchPanelsU3Ec__Iterator0_t2843932824, ___U24current_1)); }
	inline Il2CppObject * get_U24current_1() const { return ___U24current_1; }
	inline Il2CppObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(Il2CppObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_1, value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSwitchPanelsU3Ec__Iterator0_t2843932824, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSwitchPanelsU3Ec__Iterator0_t2843932824, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
