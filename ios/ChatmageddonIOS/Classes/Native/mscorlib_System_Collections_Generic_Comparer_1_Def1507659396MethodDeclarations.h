﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<FriendSearchNavScreen>
struct DefaultComparer_t1507659396;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<FriendSearchNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m3199992409_gshared (DefaultComparer_t1507659396 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3199992409(__this, method) ((  void (*) (DefaultComparer_t1507659396 *, const MethodInfo*))DefaultComparer__ctor_m3199992409_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<FriendSearchNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1790892990_gshared (DefaultComparer_t1507659396 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1790892990(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1507659396 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m1790892990_gshared)(__this, ___x0, ___y1, method)
