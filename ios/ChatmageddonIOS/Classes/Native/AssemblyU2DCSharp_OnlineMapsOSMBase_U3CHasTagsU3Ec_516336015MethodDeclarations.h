﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMBase/<HasTags>c__AnonStorey4/<HasTags>c__AnonStorey5
struct U3CHasTagsU3Ec__AnonStorey5_t516336015;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void OnlineMapsOSMBase/<HasTags>c__AnonStorey4/<HasTags>c__AnonStorey5::.ctor()
extern "C"  void U3CHasTagsU3Ec__AnonStorey5__ctor_m1108980698 (U3CHasTagsU3Ec__AnonStorey5_t516336015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsOSMBase/<HasTags>c__AnonStorey4/<HasTags>c__AnonStorey5::<>m__0(System.String)
extern "C"  bool U3CHasTagsU3Ec__AnonStorey5_U3CU3Em__0_m3191024915 (U3CHasTagsU3Ec__AnonStorey5_t516336015 * __this, String_t* ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
