﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContactsNaviagationController
struct ContactsNaviagationController_t22981102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void ContactsNaviagationController::.ctor()
extern "C"  void ContactsNaviagationController__ctor_m1801388423 (ContactsNaviagationController_t22981102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNaviagationController::NavigateBackwards()
extern "C"  void ContactsNaviagationController_NavigateBackwards_m1717057174 (ContactsNaviagationController_t22981102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNaviagationController::SetupStartScreen(PhoneNumberNavScreen)
extern "C"  void ContactsNaviagationController_SetupStartScreen_m2251898700 (ContactsNaviagationController_t22981102 * __this, int32_t ___startScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNaviagationController::NavigateForwards(PhoneNumberNavScreen)
extern "C"  void ContactsNaviagationController_NavigateForwards_m1166592362 (ContactsNaviagationController_t22981102 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ContactsNaviagationController::ValidateNavigation(PhoneNumberNavScreen,NavigationDirection)
extern "C"  bool ContactsNaviagationController_ValidateNavigation_m537318472 (ContactsNaviagationController_t22981102 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNaviagationController::LoadNavScreen(PhoneNumberNavScreen,NavigationDirection,System.Boolean)
extern "C"  void ContactsNaviagationController_LoadNavScreen_m2027734686 (ContactsNaviagationController_t22981102 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNaviagationController::UnloadNavScreen(PhoneNumberNavScreen,NavigationDirection)
extern "C"  void ContactsNaviagationController_UnloadNavScreen_m3003791214 (ContactsNaviagationController_t22981102 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNaviagationController::PopulateTitleBar(PhoneNumberNavScreen)
extern "C"  void ContactsNaviagationController_PopulateTitleBar_m1387305618 (ContactsNaviagationController_t22981102 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
