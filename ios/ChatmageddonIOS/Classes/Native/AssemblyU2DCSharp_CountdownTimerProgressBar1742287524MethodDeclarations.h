﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountdownTimerProgressBar
struct CountdownTimerProgressBar_t1742287524;

#include "codegen/il2cpp-codegen.h"

// System.Void CountdownTimerProgressBar::.ctor()
extern "C"  void CountdownTimerProgressBar__ctor_m4096475021 (CountdownTimerProgressBar_t1742287524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimerProgressBar::TransitionToPercent(System.Single,System.Single)
extern "C"  void CountdownTimerProgressBar_TransitionToPercent_m744759898 (CountdownTimerProgressBar_t1742287524 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimerProgressBar::ProgressUpdated()
extern "C"  void CountdownTimerProgressBar_ProgressUpdated_m2446786633 (CountdownTimerProgressBar_t1742287524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimerProgressBar::ProgressFinished()
extern "C"  void CountdownTimerProgressBar_ProgressFinished_m4208386062 (CountdownTimerProgressBar_t1742287524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimerProgressBar::StartCountDownTimer()
extern "C"  void CountdownTimerProgressBar_StartCountDownTimer_m1713165311 (CountdownTimerProgressBar_t1742287524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
