﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.ReferenceList
struct ReferenceList_t1582251325;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.Xml.EncryptedReference
struct EncryptedReference_t2364822029;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Security_System_Security_Cryptography_Xml_E2364822029.h"
#include "mscorlib_System_Array3829468939.h"

// System.Void System.Security.Cryptography.Xml.ReferenceList::.ctor()
extern "C"  void ReferenceList__ctor_m2572236123 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.Xml.ReferenceList::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReferenceList_System_Collections_IList_get_Item_m2930326530 (ReferenceList_t1582251325 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.ReferenceList::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReferenceList_System_Collections_IList_set_Item_m3796845333 (ReferenceList_t1582251325 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.ReferenceList::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReferenceList_System_Collections_IList_get_IsFixedSize_m1784317870 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.ReferenceList::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReferenceList_System_Collections_IList_get_IsReadOnly_m3822558291 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.ReferenceList::get_Count()
extern "C"  int32_t ReferenceList_get_Count_m929972095 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.ReferenceList::get_IsSynchronized()
extern "C"  bool ReferenceList_get_IsSynchronized_m4111608148 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.EncryptedReference System.Security.Cryptography.Xml.ReferenceList::get_ItemOf(System.Int32)
extern "C"  EncryptedReference_t2364822029 * ReferenceList_get_ItemOf_m2190035305 (ReferenceList_t1582251325 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.ReferenceList::set_ItemOf(System.Int32,System.Security.Cryptography.Xml.EncryptedReference)
extern "C"  void ReferenceList_set_ItemOf_m4149779670 (ReferenceList_t1582251325 * __this, int32_t ___index0, EncryptedReference_t2364822029 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.Xml.ReferenceList::get_SyncRoot()
extern "C"  Il2CppObject * ReferenceList_get_SyncRoot_m371810834 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.ReferenceList::Add(System.Object)
extern "C"  int32_t ReferenceList_Add_m363760544 (ReferenceList_t1582251325 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.ReferenceList::Clear()
extern "C"  void ReferenceList_Clear_m1025808146 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.Xml.ReferenceList::Contains(System.Object)
extern "C"  bool ReferenceList_Contains_m4002092718 (ReferenceList_t1582251325 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.ReferenceList::CopyTo(System.Array,System.Int32)
extern "C"  void ReferenceList_CopyTo_m1352749896 (ReferenceList_t1582251325 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Cryptography.Xml.ReferenceList::GetEnumerator()
extern "C"  Il2CppObject * ReferenceList_GetEnumerator_m98855483 (ReferenceList_t1582251325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.ReferenceList::IndexOf(System.Object)
extern "C"  int32_t ReferenceList_IndexOf_m244267566 (ReferenceList_t1582251325 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.ReferenceList::Insert(System.Int32,System.Object)
extern "C"  void ReferenceList_Insert_m1870617323 (ReferenceList_t1582251325 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.ReferenceList::Remove(System.Object)
extern "C"  void ReferenceList_Remove_m994836859 (ReferenceList_t1582251325 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.ReferenceList::RemoveAt(System.Int32)
extern "C"  void ReferenceList_RemoveAt_m966040549 (ReferenceList_t1582251325 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
