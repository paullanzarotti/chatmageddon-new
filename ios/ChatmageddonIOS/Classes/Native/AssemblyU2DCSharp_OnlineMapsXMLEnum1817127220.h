﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsXML
struct OnlineMapsXML_t3341520387;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsXMLEnum
struct  OnlineMapsXMLEnum_t1817127220  : public Il2CppObject
{
public:
	// OnlineMapsXML OnlineMapsXMLEnum::el
	OnlineMapsXML_t3341520387 * ___el_0;
	// System.Int32 OnlineMapsXMLEnum::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_el_0() { return static_cast<int32_t>(offsetof(OnlineMapsXMLEnum_t1817127220, ___el_0)); }
	inline OnlineMapsXML_t3341520387 * get_el_0() const { return ___el_0; }
	inline OnlineMapsXML_t3341520387 ** get_address_of_el_0() { return &___el_0; }
	inline void set_el_0(OnlineMapsXML_t3341520387 * value)
	{
		___el_0 = value;
		Il2CppCodeGenWriteBarrier(&___el_0, value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(OnlineMapsXMLEnum_t1817127220, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
