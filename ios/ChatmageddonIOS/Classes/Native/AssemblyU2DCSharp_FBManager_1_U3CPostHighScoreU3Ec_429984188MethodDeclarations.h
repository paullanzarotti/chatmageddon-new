﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBManager`1/<PostHighScore>c__AnonStorey2<System.Object>
struct U3CPostHighScoreU3Ec__AnonStorey2_t429984188;
// Facebook.Unity.ILoginResult
struct ILoginResult_t403585443;

#include "codegen/il2cpp-codegen.h"

// System.Void FBManager`1/<PostHighScore>c__AnonStorey2<System.Object>::.ctor()
extern "C"  void U3CPostHighScoreU3Ec__AnonStorey2__ctor_m3859885075_gshared (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * __this, const MethodInfo* method);
#define U3CPostHighScoreU3Ec__AnonStorey2__ctor_m3859885075(__this, method) ((  void (*) (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 *, const MethodInfo*))U3CPostHighScoreU3Ec__AnonStorey2__ctor_m3859885075_gshared)(__this, method)
// System.Void FBManager`1/<PostHighScore>c__AnonStorey2<System.Object>::<>m__0(Facebook.Unity.ILoginResult)
extern "C"  void U3CPostHighScoreU3Ec__AnonStorey2_U3CU3Em__0_m1136012094_gshared (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define U3CPostHighScoreU3Ec__AnonStorey2_U3CU3Em__0_m1136012094(__this, ___result0, method) ((  void (*) (U3CPostHighScoreU3Ec__AnonStorey2_t429984188 *, Il2CppObject *, const MethodInfo*))U3CPostHighScoreU3Ec__AnonStorey2_U3CU3Em__0_m1136012094_gshared)(__this, ___result0, method)
