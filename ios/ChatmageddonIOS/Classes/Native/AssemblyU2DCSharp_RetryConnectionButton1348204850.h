﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenColor
struct TweenColor_t3390486518;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RetryConnectionButton
struct  RetryConnectionButton_t1348204850  : public SFXButton_t792651341
{
public:
	// TweenColor RetryConnectionButton::buttonTween
	TweenColor_t3390486518 * ___buttonTween_5;

public:
	inline static int32_t get_offset_of_buttonTween_5() { return static_cast<int32_t>(offsetof(RetryConnectionButton_t1348204850, ___buttonTween_5)); }
	inline TweenColor_t3390486518 * get_buttonTween_5() const { return ___buttonTween_5; }
	inline TweenColor_t3390486518 ** get_address_of_buttonTween_5() { return &___buttonTween_5; }
	inline void set_buttonTween_5(TweenColor_t3390486518 * value)
	{
		___buttonTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___buttonTween_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
