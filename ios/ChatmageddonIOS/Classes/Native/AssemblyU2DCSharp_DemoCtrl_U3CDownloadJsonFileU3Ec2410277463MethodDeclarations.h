﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoCtrl/<DownloadJsonFile>c__Iterator1
struct U3CDownloadJsonFileU3Ec__Iterator1_t2410277463;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DemoCtrl/<DownloadJsonFile>c__Iterator1::.ctor()
extern "C"  void U3CDownloadJsonFileU3Ec__Iterator1__ctor_m3497100224 (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DemoCtrl/<DownloadJsonFile>c__Iterator1::MoveNext()
extern "C"  bool U3CDownloadJsonFileU3Ec__Iterator1_MoveNext_m1475652924 (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DemoCtrl/<DownloadJsonFile>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadJsonFileU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2024049970 (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DemoCtrl/<DownloadJsonFile>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadJsonFileU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1753610954 (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl/<DownloadJsonFile>c__Iterator1::Dispose()
extern "C"  void U3CDownloadJsonFileU3Ec__Iterator1_Dispose_m2649244065 (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl/<DownloadJsonFile>c__Iterator1::Reset()
extern "C"  void U3CDownloadJsonFileU3Ec__Iterator1_Reset_m2380503747 (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl/<DownloadJsonFile>c__Iterator1::<>m__0(System.String)
extern "C"  void U3CDownloadJsonFileU3Ec__Iterator1_U3CU3Em__0_m3929811423 (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463 * __this, String_t* ___fileContents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
