﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Runtime.Internal.IOSImpl
struct IOSImpl_t3175828673;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Fabric.Runtime.Internal.IOSImpl::.ctor()
extern "C"  void IOSImpl__ctor_m3251266098 (IOSImpl_t3175828673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Runtime.Internal.IOSImpl::fabric_symbol_for_linker()
extern "C"  String_t* IOSImpl_fabric_symbol_for_linker_m89643027 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Runtime.Internal.IOSImpl::Initialize()
extern "C"  String_t* IOSImpl_Initialize_m2731455311 (IOSImpl_t3175828673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
