﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_NGUIAnimation2962118329.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UISriteAnimation
struct  UISriteAnimation_t265093205  : public NGUIAnimation_t2962118329
{
public:
	// UISprite UISriteAnimation::animSprite
	UISprite_t603616735 * ___animSprite_12;

public:
	inline static int32_t get_offset_of_animSprite_12() { return static_cast<int32_t>(offsetof(UISriteAnimation_t265093205, ___animSprite_12)); }
	inline UISprite_t603616735 * get_animSprite_12() const { return ___animSprite_12; }
	inline UISprite_t603616735 ** get_address_of_animSprite_12() { return &___animSprite_12; }
	inline void set_animSprite_12(UISprite_t603616735 * value)
	{
		___animSprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___animSprite_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
