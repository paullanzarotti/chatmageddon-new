﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchAccuracyController
struct LaunchAccuracyController_t4092159714;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LaunchAccuracyController::.ctor()
extern "C"  void LaunchAccuracyController__ctor_m198860411 (LaunchAccuracyController_t4092159714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAccuracyController::Awake()
extern "C"  void LaunchAccuracyController_Awake_m2610568878 (LaunchAccuracyController_t4092159714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAccuracyController::SetAccuracyLabel(System.String)
extern "C"  void LaunchAccuracyController_SetAccuracyLabel_m4033868642 (LaunchAccuracyController_t4092159714 * __this, String_t* ___percentage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAccuracyController::StartAnimation()
extern "C"  void LaunchAccuracyController_StartAnimation_m3646250969 (LaunchAccuracyController_t4092159714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAccuracyController::PosTweenFinished()
extern "C"  void LaunchAccuracyController_PosTweenFinished_m3047904718 (LaunchAccuracyController_t4092159714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchAccuracyController::FlashRoutine()
extern "C"  Il2CppObject * LaunchAccuracyController_FlashRoutine_m3207653807 (LaunchAccuracyController_t4092159714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAccuracyController::ResetAnimation()
extern "C"  void LaunchAccuracyController_ResetAnimation_m241538802 (LaunchAccuracyController_t4092159714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
