﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Runtime.Internal.Impl
struct Impl_t1344545172;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Fabric.Runtime.Internal.Impl::.ctor()
extern "C"  void Impl__ctor_m3717974263 (Impl_t1344545172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Fabric.Runtime.Internal.Impl Fabric.Runtime.Internal.Impl::Make()
extern "C"  Impl_t1344545172 * Impl_Make_m3840723966 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Runtime.Internal.Impl::Initialize()
extern "C"  String_t* Impl_Initialize_m2945986778 (Impl_t1344545172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
