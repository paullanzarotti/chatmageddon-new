﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultModalUI
struct ResultModalUI_t969511824;

#include "AssemblyU2DCSharp_DoubleStateButton1032633262.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileResultsAttackButton
struct  MissileResultsAttackButton_t3918897298  : public DoubleStateButton_t1032633262
{
public:
	// ResultModalUI MissileResultsAttackButton::modal
	ResultModalUI_t969511824 * ___modal_13;

public:
	inline static int32_t get_offset_of_modal_13() { return static_cast<int32_t>(offsetof(MissileResultsAttackButton_t3918897298, ___modal_13)); }
	inline ResultModalUI_t969511824 * get_modal_13() const { return ___modal_13; }
	inline ResultModalUI_t969511824 ** get_address_of_modal_13() { return &___modal_13; }
	inline void set_modal_13(ResultModalUI_t969511824 * value)
	{
		___modal_13 = value;
		Il2CppCodeGenWriteBarrier(&___modal_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
