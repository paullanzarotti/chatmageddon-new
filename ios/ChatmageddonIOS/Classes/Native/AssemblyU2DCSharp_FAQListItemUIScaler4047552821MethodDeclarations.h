﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FAQListItemUIScaler
struct FAQListItemUIScaler_t4047552821;

#include "codegen/il2cpp-codegen.h"

// System.Void FAQListItemUIScaler::.ctor()
extern "C"  void FAQListItemUIScaler__ctor_m3355549696 (FAQListItemUIScaler_t4047552821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FAQListItemUIScaler::GlobalUIScale()
extern "C"  void FAQListItemUIScaler_GlobalUIScale_m1114591143 (FAQListItemUIScaler_t4047552821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
