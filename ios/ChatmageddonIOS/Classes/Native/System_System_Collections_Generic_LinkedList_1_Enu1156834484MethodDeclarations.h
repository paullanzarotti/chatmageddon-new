﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1817063546MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.String>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Enumerator__ctor_m4168706819(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t1156834484 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Enumerator__ctor_m1529567823_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.String>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m2107238715(__this, ___parent0, method) ((  void (*) (Enumerator_t1156834484 *, LinkedList_1_t2333928462 *, const MethodInfo*))Enumerator__ctor_m1586864815_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3842558326(__this, method) ((  Il2CppObject * (*) (Enumerator_t1156834484 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2866076434(__this, method) ((  void (*) (Enumerator_t1156834484 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1061591080_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.String>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3571513375(__this, ___info0, ___context1, method) ((  void (*) (Enumerator_t1156834484 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3579581747_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.String>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
#define Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m227292826(__this, ___sender0, method) ((  void (*) (Enumerator_t1156834484 *, Il2CppObject *, const MethodInfo*))Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2669336772_gshared)(__this, ___sender0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m363886619(__this, method) ((  String_t* (*) (Enumerator_t1156834484 *, const MethodInfo*))Enumerator_get_Current_m3158498407_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m1899175354(__this, method) ((  bool (*) (Enumerator_t1156834484 *, const MethodInfo*))Enumerator_MoveNext_m1957727328_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m622259939(__this, method) ((  void (*) (Enumerator_t1156834484 *, const MethodInfo*))Enumerator_Dispose_m3217456423_gshared)(__this, method)
