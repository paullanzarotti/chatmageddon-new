﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsListItemUIScaler
struct FriendsListItemUIScaler_t3047710612;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsListItemUIScaler::.ctor()
extern "C"  void FriendsListItemUIScaler__ctor_m3371244639 (FriendsListItemUIScaler_t3047710612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsListItemUIScaler::GlobalUIScale()
extern "C"  void FriendsListItemUIScaler_GlobalUIScale_m2340666226 (FriendsListItemUIScaler_t3047710612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
