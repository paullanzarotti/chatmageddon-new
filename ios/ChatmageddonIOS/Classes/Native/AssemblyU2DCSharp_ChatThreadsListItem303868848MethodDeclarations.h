﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatThreadsListItem
struct ChatThreadsListItem_t303868848;
// ChatThread
struct ChatThread_t2394323482;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatThread2394323482.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void ChatThreadsListItem::.ctor()
extern "C"  void ChatThreadsListItem__ctor_m1483808803 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::SetUpItem()
extern "C"  void ChatThreadsListItem_SetUpItem_m56095477 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::PopulateItem(ChatThread)
extern "C"  void ChatThreadsListItem_PopulateItem_m2492529552 (ChatThreadsListItem_t303868848 * __this, ChatThread_t2394323482 * ___thread0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::UpdateAvatar()
extern "C"  void ChatThreadsListItem_UpdateAvatar_m987852099 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::SetLatestMessage()
extern "C"  void ChatThreadsListItem_SetLatestMessage_m3444142761 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatThreadsListItem::FormatDateString(System.String)
extern "C"  String_t* ChatThreadsListItem_FormatDateString_m2193021976 (ChatThreadsListItem_t303868848 * __this, String_t* ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::SetNewMessage()
extern "C"  void ChatThreadsListItem_SetNewMessage_m4240027570 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::SetVisible()
extern "C"  void ChatThreadsListItem_SetVisible_m2324680467 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::SetInvisible()
extern "C"  void ChatThreadsListItem_SetInvisible_m1513587848 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatThreadsListItem::verifyVisibility()
extern "C"  bool ChatThreadsListItem_verifyVisibility_m1955446644 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::CheckVisibilty()
extern "C"  void ChatThreadsListItem_CheckVisibilty_m1381113852 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::FixedUpdate()
extern "C"  void ChatThreadsListItem_FixedUpdate_m630694656 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::OnEnable()
extern "C"  void ChatThreadsListItem_OnEnable_m3187408759 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::FindScrollView()
extern "C"  void ChatThreadsListItem_FindScrollView_m2569062744 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::OnPress(System.Boolean)
extern "C"  void ChatThreadsListItem_OnPress_m3710767990 (ChatThreadsListItem_t303868848 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::Drag(UnityEngine.Vector2)
extern "C"  void ChatThreadsListItem_Drag_m2978329171 (ChatThreadsListItem_t303868848 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::OnDragEnd()
extern "C"  void ChatThreadsListItem_OnDragEnd_m2190328997 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::OnScroll(System.Single)
extern "C"  void ChatThreadsListItem_OnScroll_m3742766280 (ChatThreadsListItem_t303868848 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThreadsListItem::OnClick()
extern "C"  void ChatThreadsListItem_OnClick_m2817461816 (ChatThreadsListItem_t303868848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
