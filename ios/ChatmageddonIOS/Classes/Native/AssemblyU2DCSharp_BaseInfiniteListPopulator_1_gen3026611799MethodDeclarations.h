﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3248597474MethodDeclarations.h"

// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::.ctor()
#define BaseInfiniteListPopulator_1__ctor_m1683769701(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1__ctor_m2641845277_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::Awake()
#define BaseInfiniteListPopulator_1_Awake_m4093761736(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_Awake_m758246746_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::ResetPanel()
#define BaseInfiniteListPopulator_1_ResetPanel_m2791307836(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_ResetPanel_m2816930074_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m384380272(__this, ___item0, ___dataIndex1, ___carouselIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, Transform_t3275118058 *, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422_gshared)(__this, ___item0, ___dataIndex1, ___carouselIndex2, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::SetStartIndex(System.Int32)
#define BaseInfiniteListPopulator_1_SetStartIndex_m1558254990(__this, ___inStartIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_SetStartIndex_m814882572_gshared)(__this, ___inStartIndex0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::SetOriginalData(System.Collections.ArrayList)
#define BaseInfiniteListPopulator_1_SetOriginalData_m1934967205(__this, ___inDataList0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, ArrayList_t4252133567 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetOriginalData_m3487250981_gshared)(__this, ___inDataList0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::SetSectionIndices(System.Collections.Generic.List`1<System.Int32>)
#define BaseInfiniteListPopulator_1_SetSectionIndices_m3186573948(__this, ___inSectionsIndices0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, List_1_t1440998580 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658_gshared)(__this, ___inSectionsIndices0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::RefreshTableView()
#define BaseInfiniteListPopulator_1_RefreshTableView_m2050987395(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshTableView_m3255066675_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::InitTableView(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableView_m2859644485(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableView_m3216652517_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::InitTableViewImp(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableViewImp_m3465031613(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::RepositionList()
#define BaseInfiniteListPopulator_1_RepositionList_m3387566245(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_RepositionList_m111137877_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::UponPostRepositionReq()
#define BaseInfiniteListPopulator_1_UponPostRepositionReq_m1654599461(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_InitListItemWithIndex_m4035160339(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, Transform_t3275118058 *, int32_t, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259_gshared)(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::PrepareListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m2459603218(__this, ___item0, ___newIndex1, ___oldIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, Transform_t3275118058 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::PrepareCarouselListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m4155767281(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, Transform_t3275118058 *, int32_t, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<CountryCodeListItem>::ItemIsInvisible(System.Int32)
#define BaseInfiniteListPopulator_1_ItemIsInvisible_m3759963364(__this, ___itemNumber0, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::SetItemInvisible(Item,System.Boolean)
#define BaseInfiniteListPopulator_1_SetItemInvisible_m3933604281(__this, ___item0, ___forward1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, CountryCodeListItem_t2467463620 *, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353_gshared)(__this, ___item0, ___forward1, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::UpdateList(System.Int32)
#define BaseInfiniteListPopulator_1_UpdateList_m795850607(__this, ___itemNumber0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateList_m2236403487_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::UpdateListDirection(BaseInfiniteListPopulator`1/ListMovementDirection<Item>,System.Int32)
#define BaseInfiniteListPopulator_1_UpdateListDirection_m175905176(__this, ___direction0, ___itemNumber1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970_gshared)(__this, ___direction0, ___itemNumber1, method)
// System.Int32 BaseInfiniteListPopulator`1<CountryCodeListItem>::GetJumpIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1792520060(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498_gshared)(__this, ___itemDataIndex0, method)
// System.Int32 BaseInfiniteListPopulator`1<CountryCodeListItem>::GetRealIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetRealIndexForItem_m3276902468(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::itemIsPressed(System.Int32,System.Boolean)
#define BaseInfiniteListPopulator_1_itemIsPressed_m1675444564(__this, ___itemDataIndex0, ___isDown1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, bool, const MethodInfo*))BaseInfiniteListPopulator_1_itemIsPressed_m852036530_gshared)(__this, ___itemDataIndex0, ___isDown1, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::itemClicked(System.Int32)
#define BaseInfiniteListPopulator_1_itemClicked_m1905430398(__this, ___itemDataIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_itemClicked_m3174637120_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::StopScrollViewMomentum()
#define BaseInfiniteListPopulator_1_StopScrollViewMomentum_m2799149537(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289_gshared)(__this, method)
// UnityEngine.Transform BaseInfiniteListPopulator`1<CountryCodeListItem>::GetItemFromPool(System.Int32)
#define BaseInfiniteListPopulator_1_GetItemFromPool_m3373190245(__this, ___i0, method) ((  Transform_t3275118058 * (*) (BaseInfiniteListPopulator_1_t3026611799 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773_gshared)(__this, ___i0, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::RefreshPool()
#define BaseInfiniteListPopulator_1_RefreshPool_m4082966954(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshPool_m4033485912_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::SetScrollToBottom()
#define BaseInfiniteListPopulator_1_SetScrollToBottom_m2823955372(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::SetScrollPosition(System.Single,System.Boolean)
#define BaseInfiniteListPopulator_1_SetScrollPosition_m1314430533(__this, ___exactPosition0, ___up1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, float, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381_gshared)(__this, ___exactPosition0, ___up1, method)
// System.Void BaseInfiniteListPopulator`1<CountryCodeListItem>::SetScrollPositionOverTime(System.Single)
#define BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3333722541(__this, ___exactPosition0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3026611799 *, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221_gshared)(__this, ___exactPosition0, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<CountryCodeListItem>::SetPanelPosOverTime(System.Single,System.Single)
#define BaseInfiniteListPopulator_1_SetPanelPosOverTime_m3700710122(__this, ___seconds0, ___position1, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t3026611799 *, float, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792_gshared)(__this, ___seconds0, ___position1, method)
