﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginSuccessSceneManager
struct LoginSuccessSceneManager_t2204684339;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void LoginSuccessSceneManager::.ctor()
extern "C"  void LoginSuccessSceneManager__ctor_m242966250 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::InitScene()
extern "C"  void LoginSuccessSceneManager_InitScene_m1951547592 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::StartScene()
extern "C"  void LoginSuccessSceneManager_StartScene_m3531874852 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::CheckInternetConnection(System.Action)
extern "C"  void LoginSuccessSceneManager_CheckInternetConnection_m52787314 (LoginSuccessSceneManager_t2204684339 * __this, Action_t3226471752 * ___loadFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::RetryLatestLoad()
extern "C"  void LoginSuccessSceneManager_RetryLatestLoad_m2391548263 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::NotifyAppStart()
extern "C"  void LoginSuccessSceneManager_NotifyAppStart_m1843205336 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadWarefareInfo()
extern "C"  void LoginSuccessSceneManager_LoadWarefareInfo_m915241243 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadStoreInfo()
extern "C"  void LoginSuccessSceneManager_LoadStoreInfo_m1792071481 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadInventoryInfo()
extern "C"  void LoginSuccessSceneManager_LoadInventoryInfo_m540843326 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::AddStandardMissile()
extern "C"  void LoginSuccessSceneManager_AddStandardMissile_m870107622 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadFriendingInfo()
extern "C"  void LoginSuccessSceneManager_LoadFriendingInfo_m3057250182 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadOutgoingPendingFriendingInfo()
extern "C"  void LoginSuccessSceneManager_LoadOutgoingPendingFriendingInfo_m709855713 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadIncomingPendingFriendingInfo()
extern "C"  void LoginSuccessSceneManager_LoadIncomingPendingFriendingInfo_m1604441929 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoginSuccessSceneManager::ContactWait()
extern "C"  Il2CppObject * LoginSuccessSceneManager_ContactWait_m2312451071 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadUserFriendsList()
extern "C"  void LoginSuccessSceneManager_LoadUserFriendsList_m220236754 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadStatusInfo()
extern "C"  void LoginSuccessSceneManager_LoadStatusInfo_m651989050 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadChatInfo()
extern "C"  void LoginSuccessSceneManager_LoadChatInfo_m2180445622 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadBlockedList()
extern "C"  void LoginSuccessSceneManager_LoadBlockedList_m1715763386 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::LoadFeeds()
extern "C"  void LoginSuccessSceneManager_LoadFeeds_m2810185145 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoginSuccessSceneManager::WaitForAnimation()
extern "C"  Il2CppObject * LoginSuccessSceneManager_WaitForAnimation_m104096722 (LoginSuccessSceneManager_t2204684339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<NotifyAppStart>m__0(System.Boolean,System.String)
extern "C"  void LoginSuccessSceneManager_U3CNotifyAppStartU3Em__0_m3124645948 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadStoreInfo>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadStoreInfoU3Em__1_m1892015936 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadInventoryInfo>m__2(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadInventoryInfoU3Em__2_m3129265042 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<AddStandardMissile>m__3(System.Boolean)
extern "C"  void LoginSuccessSceneManager_U3CAddStandardMissileU3Em__3_m2166881835 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadOutgoingPendingFriendingInfo>m__4(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadOutgoingPendingFriendingInfoU3Em__4_m558881379 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___friendsInfo1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadIncomingPendingFriendingInfo>m__5(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadIncomingPendingFriendingInfoU3Em__5_m2518993930 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___friendsInfo1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadUserFriendsList>m__6(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadUserFriendsListU3Em__6_m954799422 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___friendsInfo1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadStatusInfo>m__7(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadStatusInfoU3Em__7_m2761072963 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadChatInfo>m__8(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadChatInfoU3Em__8_m1992494480 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___threadsInfo1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager::<LoadBlockedList>m__9(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LoginSuccessSceneManager_U3CLoadBlockedListU3Em__9_m3766568265 (LoginSuccessSceneManager_t2204684339 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
