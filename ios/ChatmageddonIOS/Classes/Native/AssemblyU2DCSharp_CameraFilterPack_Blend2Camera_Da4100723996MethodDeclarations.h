﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_DarkerColor
struct CameraFilterPack_Blend2Camera_DarkerColor_t4100723996;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_DarkerColor::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_DarkerColor__ctor_m8734619 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_DarkerColor::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_DarkerColor_get_material_m2145231276 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::Start()
extern "C"  void CameraFilterPack_Blend2Camera_DarkerColor_Start_m1950546999 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_DarkerColor_OnRenderImage_m3835213319 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_DarkerColor_OnValidate_m333266448 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::Update()
extern "C"  void CameraFilterPack_Blend2Camera_DarkerColor_Update_m1835571486 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_DarkerColor_OnEnable_m3546001079 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_DarkerColor_OnDisable_m1952490864 (CameraFilterPack_Blend2Camera_DarkerColor_t4100723996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
