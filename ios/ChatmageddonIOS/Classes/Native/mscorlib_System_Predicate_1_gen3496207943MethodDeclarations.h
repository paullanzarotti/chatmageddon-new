﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"

// System.Void System.Predicate`1<FacebookFriend>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m36921099(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t3496207943 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m2289454599_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<FacebookFriend>::Invoke(T)
#define Predicate_1_Invoke_m4135549195(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3496207943 *, FacebookFriend_t758270532 *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<FacebookFriend>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m280663084(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t3496207943 *, FacebookFriend_t758270532 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m3556950370_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<FacebookFriend>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m1327218229(__this, ___result0, method) ((  bool (*) (Predicate_1_t3496207943 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3656575065_gshared)(__this, ___result0, method)
