﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyReference
struct KeyReference_t1620770554;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.Xml.KeyReference::.ctor()
extern "C"  void KeyReference__ctor_m4003025578 (KeyReference_t1620770554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
