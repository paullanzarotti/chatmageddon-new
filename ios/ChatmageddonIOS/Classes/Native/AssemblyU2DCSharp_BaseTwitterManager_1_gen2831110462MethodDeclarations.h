﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseTwitterManager_1_gen2275765192MethodDeclarations.h"

// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::.ctor()
#define BaseTwitterManager_1__ctor_m4095921522(__this, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1__ctor_m2106005101_gshared)(__this, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::Start()
#define BaseTwitterManager_1_Start_m1913835610(__this, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1_Start_m782943989_gshared)(__this, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::Initialise(System.String,System.String)
#define BaseTwitterManager_1_Initialise_m2209719929(__this, ___consumerKey0, ___consumerSecret1, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_Initialise_m967028770_gshared)(__this, ___consumerKey0, ___consumerSecret1, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::PerformAPIRequest(BaseTwitterManager`1/RequestType<ManagerType>,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
#define BaseTwitterManager_1_PerformAPIRequest_m2525603938(__this, ___type0, ___path1, ___requestParameters2, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, int32_t, String_t*, Dictionary_2_t3943999495 *, const MethodInfo*))BaseTwitterManager_1_PerformAPIRequest_m34965517_gshared)(__this, ___type0, ___path1, ___requestParameters2, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::Login(TwitterLoginEntrance)
#define BaseTwitterManager_1_Login_m2337697825(__this, ___loginEntrance0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, int32_t, const MethodInfo*))BaseTwitterManager_1_Login_m2724261672_gshared)(__this, ___loginEntrance0, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::Logout()
#define BaseTwitterManager_1_Logout_m3648811430(__this, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1_Logout_m446240679_gshared)(__this, method)
// System.Boolean BaseTwitterManager`1<ChatmageddonTwitterManager>::IsLoggedIn()
#define BaseTwitterManager_1_IsLoggedIn_m1004722047(__this, method) ((  bool (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1_IsLoggedIn_m3542423240_gshared)(__this, method)
// System.String BaseTwitterManager`1<ChatmageddonTwitterManager>::GetLoggedInUsername()
#define BaseTwitterManager_1_GetLoggedInUsername_m476832788(__this, method) ((  String_t* (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1_GetLoggedInUsername_m3874150437_gshared)(__this, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::PostStatusUpdate(System.String)
#define BaseTwitterManager_1_PostStatusUpdate_m4033869151(__this, ___status0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, const MethodInfo*))BaseTwitterManager_1_PostStatusUpdate_m3539375508_gshared)(__this, ___status0, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::PostImageStatusUpdate(System.String,System.String)
#define BaseTwitterManager_1_PostImageStatusUpdate_m3884111848(__this, ___status0, ___imagePath1, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_PostImageStatusUpdate_m2760878927_gshared)(__this, ___status0, ___imagePath1, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::ShowTweetComposer(System.String)
#define BaseTwitterManager_1_ShowTweetComposer_m542168020(__this, ___status0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, const MethodInfo*))BaseTwitterManager_1_ShowTweetComposer_m3417481457_gshared)(__this, ___status0, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::ShowTweetComposer(System.String,System.String)
#define BaseTwitterManager_1_ShowTweetComposer_m1780957780(__this, ___status0, ___imagePath1, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_ShowTweetComposer_m3875334979_gshared)(__this, ___status0, ___imagePath1, method)
// System.Action`1<System.String> BaseTwitterManager`1<ChatmageddonTwitterManager>::LoginSuccess()
#define BaseTwitterManager_1_LoginSuccess_m1896418115(__this, method) ((  Action_1_t1831019615 * (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1_LoginSuccess_m256501760_gshared)(__this, method)
// System.Collections.IEnumerator BaseTwitterManager`1<ChatmageddonTwitterManager>::CheckLogingSuccess()
#define BaseTwitterManager_1_CheckLogingSuccess_m3101257495(__this, method) ((  Il2CppObject * (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1_CheckLogingSuccess_m2776892846_gshared)(__this, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::TwitterLoginSuccess()
#define BaseTwitterManager_1_TwitterLoginSuccess_m3752304237(__this, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, const MethodInfo*))BaseTwitterManager_1_TwitterLoginSuccess_m3082522860_gshared)(__this, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::TwitterRequestSuccessEvent(System.Object)
#define BaseTwitterManager_1_TwitterRequestSuccessEvent_m3904311765(__this, ___requestObject0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, Il2CppObject *, const MethodInfo*))BaseTwitterManager_1_TwitterRequestSuccessEvent_m2359635270_gshared)(__this, ___requestObject0, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::TwitterRequestSuccess(System.String,System.String)
#define BaseTwitterManager_1_TwitterRequestSuccess_m1949095555(__this, ___code0, ___message1, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, String_t*, const MethodInfo*))BaseTwitterManager_1_TwitterRequestSuccess_m3075721710_gshared)(__this, ___code0, ___message1, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::TwitterRequestFailedEvent(System.String)
#define BaseTwitterManager_1_TwitterRequestFailedEvent_m749237249(__this, ___requestString0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, const MethodInfo*))BaseTwitterManager_1_TwitterRequestFailedEvent_m1998231770_gshared)(__this, ___requestString0, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::TwitterRequestFailed(System.String)
#define BaseTwitterManager_1_TwitterRequestFailed_m3547998901(__this, ___failString0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, const MethodInfo*))BaseTwitterManager_1_TwitterRequestFailed_m1151393876_gshared)(__this, ___failString0, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::<Start>m__0(System.Object)
#define BaseTwitterManager_1_U3CStartU3Em__0_m3625722997(__this, ___requestObject0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, Il2CppObject *, const MethodInfo*))BaseTwitterManager_1_U3CStartU3Em__0_m2718740694_gshared)(__this, ___requestObject0, method)
// System.Void BaseTwitterManager`1<ChatmageddonTwitterManager>::<Start>m__1(System.String)
#define BaseTwitterManager_1_U3CStartU3Em__1_m3363248554(__this, ___requestString0, method) ((  void (*) (BaseTwitterManager_1_t2831110462 *, String_t*, const MethodInfo*))BaseTwitterManager_1_U3CStartU3Em__1_m3162299589_gshared)(__this, ___requestString0, method)
