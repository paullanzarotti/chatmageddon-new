﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPPickerBase/PickerValueUpdatedHandler
struct PickerValueUpdatedHandler_t2401088127;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void IPPickerBase/PickerValueUpdatedHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PickerValueUpdatedHandler__ctor_m1734479446 (PickerValueUpdatedHandler_t2401088127 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase/PickerValueUpdatedHandler::Invoke()
extern "C"  void PickerValueUpdatedHandler_Invoke_m693584972 (PickerValueUpdatedHandler_t2401088127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult IPPickerBase/PickerValueUpdatedHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PickerValueUpdatedHandler_BeginInvoke_m3910263153 (PickerValueUpdatedHandler_t2401088127 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerBase/PickerValueUpdatedHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PickerValueUpdatedHandler_EndInvoke_m4149272336 (PickerValueUpdatedHandler_t2401088127 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
