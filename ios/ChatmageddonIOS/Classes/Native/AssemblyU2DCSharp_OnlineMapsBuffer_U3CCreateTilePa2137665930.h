﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBuffer/<CreateTileParent>c__AnonStorey0
struct  U3CCreateTileParentU3Ec__AnonStorey0_t2137665930  : public Il2CppObject
{
public:
	// System.Int32 OnlineMapsBuffer/<CreateTileParent>c__AnonStorey0::px
	int32_t ___px_0;
	// System.Int32 OnlineMapsBuffer/<CreateTileParent>c__AnonStorey0::py
	int32_t ___py_1;

public:
	inline static int32_t get_offset_of_px_0() { return static_cast<int32_t>(offsetof(U3CCreateTileParentU3Ec__AnonStorey0_t2137665930, ___px_0)); }
	inline int32_t get_px_0() const { return ___px_0; }
	inline int32_t* get_address_of_px_0() { return &___px_0; }
	inline void set_px_0(int32_t value)
	{
		___px_0 = value;
	}

	inline static int32_t get_offset_of_py_1() { return static_cast<int32_t>(offsetof(U3CCreateTileParentU3Ec__AnonStorey0_t2137665930, ___py_1)); }
	inline int32_t get_py_1() const { return ___py_1; }
	inline int32_t* get_address_of_py_1() { return &___py_1; }
	inline void set_py_1(int32_t value)
	{
		___py_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
