﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction1525323322.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2111116400.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis375128448.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1353336012.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial1630303189.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE3157325053.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityScript_Lang_U3CModuleU3E3783534214.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions2406697300.h"
#include "Facebook_Unity_Android_U3CModuleU3E3783534214.h"
#include "Facebook_Unity_Android_Facebook_Unity_Android_Andro950021864.h"
#include "Facebook_Unity_IOS_U3CModuleU3E3783534214.h"
#include "Facebook_Unity_IOS_Facebook_Unity_IOS_IOSWrapper294756590.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_BlurEffect519166744.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ColorCorrectionEffect618837202.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ContrastStretchEffec3732070570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GlowEffect1329942340.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GrayscaleEffect1203934418.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ImageEffectBase898523515.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ImageEffects2410519413.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MotionBlur1035271213.h"
#include "AssemblyU2DCSharpU2Dfirstpass_NoiseEffect3733883223.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SSAOEffect1790065191.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SSAOEffect_SSAOSampl2315017899.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SepiaToneEffect2695985059.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TwirlEffect2412422509.h"
#include "AssemblyU2DCSharpU2Dfirstpass_VortexEffect3754663651.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ELANManager1808384203.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EnumTimeType570234944.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ELANNotification786863861.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MainScene994361503.h"
#include "AssemblyU2DCSharpU2Dfirstpass_P31RemoteNotificatio2463552857.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UIInterfaceOrientatio177348777.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PhotoPromptType3400315462.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraBinding1171909520.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraBinding_U3Ctak72106313.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraBinding_U3Ct3494249130.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraBinding_U3Cg2412087682.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraBinding_U3Cs3784894394.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraBinding_U3Cs4033442107.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraBinding_U3Cr1159577394.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Contact4014196408.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraManager1020444134.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraManager_U3Ct1429582119.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraManager_U3Cr4257737484.h"
#include "AssemblyU2DCSharpU2Dfirstpass_CubeRotator965177170.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EtceteraEventListene3310591661.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (Slider_t297367283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[15] = 
{
	Slider_t297367283::get_offset_of_m_FillRect_16(),
	Slider_t297367283::get_offset_of_m_HandleRect_17(),
	Slider_t297367283::get_offset_of_m_Direction_18(),
	Slider_t297367283::get_offset_of_m_MinValue_19(),
	Slider_t297367283::get_offset_of_m_MaxValue_20(),
	Slider_t297367283::get_offset_of_m_WholeNumbers_21(),
	Slider_t297367283::get_offset_of_m_Value_22(),
	Slider_t297367283::get_offset_of_m_OnValueChanged_23(),
	Slider_t297367283::get_offset_of_m_FillImage_24(),
	Slider_t297367283::get_offset_of_m_FillTransform_25(),
	Slider_t297367283::get_offset_of_m_FillContainerRect_26(),
	Slider_t297367283::get_offset_of_m_HandleTransform_27(),
	Slider_t297367283::get_offset_of_m_HandleContainerRect_28(),
	Slider_t297367283::get_offset_of_m_Offset_29(),
	Slider_t297367283::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (Direction_t1525323322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3301[5] = 
{
	Direction_t1525323322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (SliderEvent_t2111116400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (Axis_t375128448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3303[3] = 
{
	Axis_t375128448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (SpriteState_t1353336012)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3305[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3307[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3309[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3311[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3312[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3318[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3320[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3321[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3322[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3324[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3326[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3327[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3328[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3339[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3340[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3346[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3351[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3357[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (U3CModuleU3E_t3783534231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (Extensions_t2406697300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (U3CModuleU3E_t3783534232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (AndroidWrapper_t950021864), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (U3CModuleU3E_t3783534233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (IOSWrapper_t294756590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (U3CModuleU3E_t3783534234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (BlurEffect_t519166744), -1, sizeof(BlurEffect_t519166744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3366[4] = 
{
	BlurEffect_t519166744::get_offset_of_iterations_2(),
	BlurEffect_t519166744::get_offset_of_blurSpread_3(),
	BlurEffect_t519166744::get_offset_of_blurShader_4(),
	BlurEffect_t519166744_StaticFields::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (ColorCorrectionEffect_t618837202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3367[1] = 
{
	ColorCorrectionEffect_t618837202::get_offset_of_textureRamp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (ContrastStretchEffect_t3732070570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[13] = 
{
	ContrastStretchEffect_t3732070570::get_offset_of_adaptationSpeed_2(),
	ContrastStretchEffect_t3732070570::get_offset_of_limitMinimum_3(),
	ContrastStretchEffect_t3732070570::get_offset_of_limitMaximum_4(),
	ContrastStretchEffect_t3732070570::get_offset_of_adaptRenderTex_5(),
	ContrastStretchEffect_t3732070570::get_offset_of_curAdaptIndex_6(),
	ContrastStretchEffect_t3732070570::get_offset_of_shaderLum_7(),
	ContrastStretchEffect_t3732070570::get_offset_of_m_materialLum_8(),
	ContrastStretchEffect_t3732070570::get_offset_of_shaderReduce_9(),
	ContrastStretchEffect_t3732070570::get_offset_of_m_materialReduce_10(),
	ContrastStretchEffect_t3732070570::get_offset_of_shaderAdapt_11(),
	ContrastStretchEffect_t3732070570::get_offset_of_m_materialAdapt_12(),
	ContrastStretchEffect_t3732070570::get_offset_of_shaderApply_13(),
	ContrastStretchEffect_t3732070570::get_offset_of_m_materialApply_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (GlowEffect_t1329942340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3369[10] = 
{
	GlowEffect_t1329942340::get_offset_of_glowIntensity_2(),
	GlowEffect_t1329942340::get_offset_of_blurIterations_3(),
	GlowEffect_t1329942340::get_offset_of_blurSpread_4(),
	GlowEffect_t1329942340::get_offset_of_glowTint_5(),
	GlowEffect_t1329942340::get_offset_of_compositeShader_6(),
	GlowEffect_t1329942340::get_offset_of_m_CompositeMaterial_7(),
	GlowEffect_t1329942340::get_offset_of_blurShader_8(),
	GlowEffect_t1329942340::get_offset_of_m_BlurMaterial_9(),
	GlowEffect_t1329942340::get_offset_of_downsampleShader_10(),
	GlowEffect_t1329942340::get_offset_of_m_DownsampleMaterial_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (GrayscaleEffect_t1203934418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[2] = 
{
	GrayscaleEffect_t1203934418::get_offset_of_textureRamp_4(),
	GrayscaleEffect_t1203934418::get_offset_of_rampOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (ImageEffectBase_t898523515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[2] = 
{
	ImageEffectBase_t898523515::get_offset_of_shader_2(),
	ImageEffectBase_t898523515::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (ImageEffects_t2410519413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (MotionBlur_t1035271213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[3] = 
{
	MotionBlur_t1035271213::get_offset_of_blurAmount_4(),
	MotionBlur_t1035271213::get_offset_of_extraBlur_5(),
	MotionBlur_t1035271213::get_offset_of_accumTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (NoiseEffect_t3733883223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[18] = 
{
	NoiseEffect_t3733883223::get_offset_of_monochrome_2(),
	NoiseEffect_t3733883223::get_offset_of_rgbFallback_3(),
	NoiseEffect_t3733883223::get_offset_of_grainIntensityMin_4(),
	NoiseEffect_t3733883223::get_offset_of_grainIntensityMax_5(),
	NoiseEffect_t3733883223::get_offset_of_grainSize_6(),
	NoiseEffect_t3733883223::get_offset_of_scratchIntensityMin_7(),
	NoiseEffect_t3733883223::get_offset_of_scratchIntensityMax_8(),
	NoiseEffect_t3733883223::get_offset_of_scratchFPS_9(),
	NoiseEffect_t3733883223::get_offset_of_scratchJitter_10(),
	NoiseEffect_t3733883223::get_offset_of_grainTexture_11(),
	NoiseEffect_t3733883223::get_offset_of_scratchTexture_12(),
	NoiseEffect_t3733883223::get_offset_of_shaderRGB_13(),
	NoiseEffect_t3733883223::get_offset_of_shaderYUV_14(),
	NoiseEffect_t3733883223::get_offset_of_m_MaterialRGB_15(),
	NoiseEffect_t3733883223::get_offset_of_m_MaterialYUV_16(),
	NoiseEffect_t3733883223::get_offset_of_scratchTimeLeft_17(),
	NoiseEffect_t3733883223::get_offset_of_scratchX_18(),
	NoiseEffect_t3733883223::get_offset_of_scratchY_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (SSAOEffect_t1790065191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[11] = 
{
	SSAOEffect_t1790065191::get_offset_of_m_Radius_2(),
	SSAOEffect_t1790065191::get_offset_of_m_SampleCount_3(),
	SSAOEffect_t1790065191::get_offset_of_m_OcclusionIntensity_4(),
	SSAOEffect_t1790065191::get_offset_of_m_Blur_5(),
	SSAOEffect_t1790065191::get_offset_of_m_Downsampling_6(),
	SSAOEffect_t1790065191::get_offset_of_m_OcclusionAttenuation_7(),
	SSAOEffect_t1790065191::get_offset_of_m_MinZ_8(),
	SSAOEffect_t1790065191::get_offset_of_m_SSAOShader_9(),
	SSAOEffect_t1790065191::get_offset_of_m_SSAOMaterial_10(),
	SSAOEffect_t1790065191::get_offset_of_m_RandomTexture_11(),
	SSAOEffect_t1790065191::get_offset_of_m_Supported_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (SSAOSamples_t2315017899)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3376[4] = 
{
	SSAOSamples_t2315017899::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (SepiaToneEffect_t2695985059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (TwirlEffect_t2412422509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[3] = 
{
	TwirlEffect_t2412422509::get_offset_of_radius_4(),
	TwirlEffect_t2412422509::get_offset_of_angle_5(),
	TwirlEffect_t2412422509::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (VortexEffect_t3754663651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[3] = 
{
	VortexEffect_t3754663651::get_offset_of_radius_4(),
	VortexEffect_t3754663651::get_offset_of_angle_5(),
	VortexEffect_t3754663651::get_offset_of_center_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (ELANManager_t1808384203), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (EnumTimeType_t570234944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3381[5] = 
{
	EnumTimeType_t570234944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (ELANNotification_t786863861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3382[12] = 
{
	ELANNotification_t786863861::get_offset_of_ID_2(),
	ELANNotification_t786863861::get_offset_of_fullClassName_3(),
	ELANNotification_t786863861::get_offset_of_title_4(),
	ELANNotification_t786863861::get_offset_of_message_5(),
	ELANNotification_t786863861::get_offset_of_delayTypeTime_6(),
	ELANNotification_t786863861::get_offset_of_delay_7(),
	ELANNotification_t786863861::get_offset_of_repetitionTypeTime_8(),
	ELANNotification_t786863861::get_offset_of_repetition_9(),
	ELANNotification_t786863861::get_offset_of_advancedNotification_10(),
	ELANNotification_t786863861::get_offset_of_useVibration_11(),
	ELANNotification_t786863861::get_offset_of_useSound_12(),
	ELANNotification_t786863861::get_offset_of_sendOnStart_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (MainScene_t994361503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[18] = 
{
	MainScene_t994361503::get_offset_of_notification_2(),
	MainScene_t994361503::get_offset_of_parametrizedNotification1_3(),
	MainScene_t994361503::get_offset_of_parametrizedNotification2_4(),
	MainScene_t994361503::get_offset_of_parametrizedNotification3_5(),
	MainScene_t994361503::get_offset_of_parametrizedNotification4_6(),
	MainScene_t994361503::get_offset_of_parametrizedNotification5_7(),
	MainScene_t994361503::get_offset_of_sendParametrizedNotification1_8(),
	MainScene_t994361503::get_offset_of_sendParametrizedNotification2_9(),
	MainScene_t994361503::get_offset_of_sendParametrizedNotification3_10(),
	MainScene_t994361503::get_offset_of_sendParametrizedNotification4_11(),
	MainScene_t994361503::get_offset_of_sendParametrizedNotification5_12(),
	MainScene_t994361503::get_offset_of_delay_13(),
	MainScene_t994361503::get_offset_of_rep_14(),
	MainScene_t994361503::get_offset_of_message_15(),
	MainScene_t994361503::get_offset_of_errorRepetition_16(),
	MainScene_t994361503::get_offset_of_errorDelay_17(),
	MainScene_t994361503::get_offset_of_errorType_18(),
	MainScene_t994361503::get_offset_of_logMessage_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (P31RemoteNotificationType_t2463552857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3384[5] = 
{
	P31RemoteNotificationType_t2463552857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (UIInterfaceOrientation_t177348777)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3385[5] = 
{
	UIInterfaceOrientation_t177348777::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (PhotoPromptType_t3400315462)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3386[4] = 
{
	PhotoPromptType_t3400315462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (EtceteraBinding_t1171909520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (U3CtakeScreenShotU3Ec__Iterator0_t72106313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[6] = 
{
	U3CtakeScreenShotU3Ec__Iterator0_t72106313::get_offset_of_filename_0(),
	U3CtakeScreenShotU3Ec__Iterator0_t72106313::get_offset_of_completionHandler_1(),
	U3CtakeScreenShotU3Ec__Iterator0_t72106313::get_offset_of_U24current_2(),
	U3CtakeScreenShotU3Ec__Iterator0_t72106313::get_offset_of_U24disposing_3(),
	U3CtakeScreenShotU3Ec__Iterator0_t72106313::get_offset_of_U24PC_4(),
	U3CtakeScreenShotU3Ec__Iterator0_t72106313::get_offset_of_U24locvar0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (U3CtakeScreenShotU3Ec__AnonStorey4_t3494249130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[2] = 
{
	U3CtakeScreenShotU3Ec__AnonStorey4_t3494249130::get_offset_of_filename_0(),
	U3CtakeScreenShotU3Ec__AnonStorey4_t3494249130::get_offset_of_completionHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[5] = 
{
	U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682::get_offset_of_U3CtexU3E__0_0(),
	U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682::get_offset_of_completionHandler_1(),
	U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682::get_offset_of_U24current_2(),
	U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682::get_offset_of_U24disposing_3(),
	U3CgetScreenShotTextureU3Ec__Iterator1_t2412087682::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[8] = 
{
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_toAddress_0(),
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_subject_1(),
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_body_2(),
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_isHTML_3(),
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_U24current_4(),
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_U24disposing_5(),
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_U24PC_6(),
	U3CshowMailComposerWithScreenshotU3Ec__Iterator2_t3784894394::get_offset_of_U24locvar0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[4] = 
{
	U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107::get_offset_of_toAddress_0(),
	U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107::get_offset_of_subject_1(),
	U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107::get_offset_of_body_2(),
	U3CshowMailComposerWithScreenshotU3Ec__AnonStorey5_t4033442107::get_offset_of_isHTML_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3393[13] = 
{
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U3CurlU3E__0_0(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U3CparametersU3E__1_1(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_gameThriveAppId_2(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_deviceToken_3(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_additionalParameters_4(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U3CjsonU3E__2_5(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U3CbytesU3E__3_6(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U3CheadersU3E__4_7(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U3CwwwU3E__5_8(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_completionHandler_9(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U24current_10(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U24disposing_11(),
	U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (Contact_t4014196408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3394[3] = 
{
	Contact_t4014196408::get_offset_of_name_0(),
	Contact_t4014196408::get_offset_of_emails_1(),
	Contact_t4014196408::get_offset_of_phoneNumbers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (EtceteraManager_t1020444134), -1, sizeof(EtceteraManager_t1020444134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3395[23] = 
{
	EtceteraManager_t1020444134_StaticFields::get_offset_of_dismissingViewControllerEvent_5(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_imagePickerCancelledEvent_6(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_imagePickerChoseImageEvent_7(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_saveImageToPhotoAlbumSucceededEvent_8(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_saveImageToPhotoAlbumFailedEvent_9(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_alertButtonClickedEvent_10(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_promptCancelledEvent_11(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_singleFieldPromptTextEnteredEvent_12(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_twoFieldPromptTextEnteredEvent_13(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_remoteRegistrationSucceededEvent_14(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_remoteRegistrationFailedEvent_15(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_urbanAirshipRegistrationSucceededEvent_16(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_urbanAirshipRegistrationFailedEvent_17(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_pushIORegistrationCompletedEvent_18(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_remoteNotificationReceivedEvent_19(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_remoteNotificationReceivedAtLaunchEvent_20(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_localNotificationWasReceivedEvent_21(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_localNotificationWasReceivedAtLaunchEvent_22(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_mailComposerFinishedEvent_23(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_smsComposerFinishedEvent_24(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_U3CdeviceTokenU3Ek__BackingField_25(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_pushIOApiKey_26(),
	EtceteraManager_t1020444134_StaticFields::get_offset_of_pushIOCategories_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3396[8] = 
{
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_U3CwwwU3E__0_0(),
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_filePath_1(),
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_errorDel_2(),
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_U3CtexU3E__1_3(),
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_del_4(),
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_U24current_5(),
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_U24disposing_6(),
	U3CtextureFromFileAtPathU3Ec__Iterator0_t1429582119::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[5] = 
{
	U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484::get_offset_of_U3CurlU3E__0_0(),
	U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484::get_offset_of_U3CwwwU3E__1_1(),
	U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484::get_offset_of_U24current_2(),
	U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484::get_offset_of_U24disposing_3(),
	U3CregisterDeviceWithPushIOU3Ec__Iterator1_t4257737484::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (CubeRotator_t965177170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3398[3] = 
{
	CubeRotator_t965177170::get_offset_of_speed_2(),
	CubeRotator_t965177170::get_offset_of_cube_3(),
	CubeRotator_t965177170::get_offset_of_shouldRotate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (EtceteraEventListener_t3310591661), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
