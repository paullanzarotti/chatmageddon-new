﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectAudienceSwitch
struct SelectAudienceSwitch_t3539699246;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"

// System.Void SelectAudienceSwitch::.ctor()
extern "C"  void SelectAudienceSwitch__ctor_m1418610533 (SelectAudienceSwitch_t3539699246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectAudienceSwitch::OnSwitch(Audience)
extern "C"  void SelectAudienceSwitch_OnSwitch_m713414858 (SelectAudienceSwitch_t3539699246 * __this, int32_t ___newState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
