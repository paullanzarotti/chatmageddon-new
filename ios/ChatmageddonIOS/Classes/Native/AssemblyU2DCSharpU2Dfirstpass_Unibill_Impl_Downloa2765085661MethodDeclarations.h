﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.DownloadManager/<checkDownloads>c__Iterator0
struct U3CcheckDownloadsU3Ec__Iterator0_t2765085661;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Unibill.Impl.DownloadManager/<checkDownloads>c__Iterator0::.ctor()
extern "C"  void U3CcheckDownloadsU3Ec__Iterator0__ctor_m67958808 (U3CcheckDownloadsU3Ec__Iterator0_t2765085661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.DownloadManager/<checkDownloads>c__Iterator0::MoveNext()
extern "C"  bool U3CcheckDownloadsU3Ec__Iterator0_MoveNext_m1652166384 (U3CcheckDownloadsU3Ec__Iterator0_t2765085661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.DownloadManager/<checkDownloads>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CcheckDownloadsU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1048058882 (U3CcheckDownloadsU3Ec__Iterator0_t2765085661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.DownloadManager/<checkDownloads>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CcheckDownloadsU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3238429290 (U3CcheckDownloadsU3Ec__Iterator0_t2765085661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<checkDownloads>c__Iterator0::Dispose()
extern "C"  void U3CcheckDownloadsU3Ec__Iterator0_Dispose_m1486293161 (U3CcheckDownloadsU3Ec__Iterator0_t2765085661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<checkDownloads>c__Iterator0::Reset()
extern "C"  void U3CcheckDownloadsU3Ec__Iterator0_Reset_m2241030347 (U3CcheckDownloadsU3Ec__Iterator0_t2765085661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
