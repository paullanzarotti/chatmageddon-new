﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.AmazonAppStoreBillingService
struct AmazonAppStoreBillingService_t1988397396;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AmazonAppStoreCallbackMonoBehaviour
struct  AmazonAppStoreCallbackMonoBehaviour_t1679328205  : public MonoBehaviour_t1158329972
{
public:
	// Unibill.Impl.AmazonAppStoreBillingService AmazonAppStoreCallbackMonoBehaviour::amazon
	AmazonAppStoreBillingService_t1988397396 * ___amazon_2;

public:
	inline static int32_t get_offset_of_amazon_2() { return static_cast<int32_t>(offsetof(AmazonAppStoreCallbackMonoBehaviour_t1679328205, ___amazon_2)); }
	inline AmazonAppStoreBillingService_t1988397396 * get_amazon_2() const { return ___amazon_2; }
	inline AmazonAppStoreBillingService_t1988397396 ** get_address_of_amazon_2() { return &___amazon_2; }
	inline void set_amazon_2(AmazonAppStoreBillingService_t1988397396 * value)
	{
		___amazon_2 = value;
		Il2CppCodeGenWriteBarrier(&___amazon_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
