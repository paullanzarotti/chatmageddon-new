﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserLoginButton
struct UserLoginButton_t3500827568;

#include "codegen/il2cpp-codegen.h"

// System.Void UserLoginButton::.ctor()
extern "C"  void UserLoginButton__ctor_m2784767299 (UserLoginButton_t3500827568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserLoginButton::Start()
extern "C"  void UserLoginButton_Start_m112400055 (UserLoginButton_t3500827568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserLoginButton::OnButtonClick()
extern "C"  void UserLoginButton_OnButtonClick_m568155012 (UserLoginButton_t3500827568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserLoginButton::ActivateStartPage(System.Boolean)
extern "C"  void UserLoginButton_ActivateStartPage_m3499998890 (UserLoginButton_t3500827568 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserLoginButton::LoadRemainingUI()
extern "C"  void UserLoginButton_LoadRemainingUI_m1942796173 (UserLoginButton_t3500827568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
