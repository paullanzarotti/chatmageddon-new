﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<OnlineMapsTile>
struct List_1_t3685418368;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsBufferZoom
struct  OnlineMapsBufferZoom_t2072536377  : public Il2CppObject
{
public:
	// System.Int32 OnlineMapsBufferZoom::id
	int32_t ___id_0;
	// System.Collections.Generic.List`1<OnlineMapsTile> OnlineMapsBufferZoom::tiles
	List_1_t3685418368 * ___tiles_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(OnlineMapsBufferZoom_t2072536377, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_tiles_1() { return static_cast<int32_t>(offsetof(OnlineMapsBufferZoom_t2072536377, ___tiles_1)); }
	inline List_1_t3685418368 * get_tiles_1() const { return ___tiles_1; }
	inline List_1_t3685418368 ** get_address_of_tiles_1() { return &___tiles_1; }
	inline void set_tiles_1(List_1_t3685418368 * value)
	{
		___tiles_1 = value;
		Il2CppCodeGenWriteBarrier(&___tiles_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
