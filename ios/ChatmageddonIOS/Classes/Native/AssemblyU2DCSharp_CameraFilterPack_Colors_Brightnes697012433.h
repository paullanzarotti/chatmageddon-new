﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Colors_Brightness
struct  CameraFilterPack_Colors_Brightness_t697012433  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Colors_Brightness::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_Brightness::_Brightness
	float ____Brightness_3;
	// UnityEngine.Material CameraFilterPack_Colors_Brightness::SCMaterial
	Material_t193706927 * ___SCMaterial_4;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Brightness_t697012433, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of__Brightness_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Brightness_t697012433, ____Brightness_3)); }
	inline float get__Brightness_3() const { return ____Brightness_3; }
	inline float* get_address_of__Brightness_3() { return &____Brightness_3; }
	inline void set__Brightness_3(float value)
	{
		____Brightness_3 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Brightness_t697012433, ___SCMaterial_4)); }
	inline Material_t193706927 * get_SCMaterial_4() const { return ___SCMaterial_4; }
	inline Material_t193706927 ** get_address_of_SCMaterial_4() { return &___SCMaterial_4; }
	inline void set_SCMaterial_4(Material_t193706927 * value)
	{
		___SCMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_4, value);
	}
};

struct CameraFilterPack_Colors_Brightness_t697012433_StaticFields
{
public:
	// System.Single CameraFilterPack_Colors_Brightness::ChangeBrightness
	float ___ChangeBrightness_5;

public:
	inline static int32_t get_offset_of_ChangeBrightness_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Colors_Brightness_t697012433_StaticFields, ___ChangeBrightness_5)); }
	inline float get_ChangeBrightness_5() const { return ___ChangeBrightness_5; }
	inline float* get_address_of_ChangeBrightness_5() { return &___ChangeBrightness_5; }
	inline void set_ChangeBrightness_5(float value)
	{
		___ChangeBrightness_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
