﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatMessage
struct ChatMessage_t2384228687;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatMessage::.ctor(System.Collections.Hashtable)
extern "C"  void ChatMessage__ctor_m971371436 (ChatMessage_t2384228687 * __this, Hashtable_t909839986 * ___messageData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessage::.ctor(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void ChatMessage__ctor_m263856110 (ChatMessage_t2384228687 * __this, String_t* ___messageid0, String_t* ___threadid1, String_t* ___userid2, String_t* ___messagebody3, String_t* ___createddatetime4, String_t* ____multichatUser5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessage::SetCreatedTimeToNow()
extern "C"  void ChatMessage_SetCreatedTimeToNow_m1142018124 (ChatMessage_t2384228687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatMessage::getUserID()
extern "C"  String_t* ChatMessage_getUserID_m3743219757 (ChatMessage_t2384228687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
