﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LanguageConfig/<GetLanguageStringUsingModel>c__AnonStorey0
struct U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_t3940050384;
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"

// System.Void LanguageConfig/<GetLanguageStringUsingModel>c__AnonStorey0::.ctor()
extern "C"  void U3CGetLanguageStringUsingModelU3Ec__AnonStorey0__ctor_m1470981265 (U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_t3940050384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LanguageConfig/<GetLanguageStringUsingModel>c__AnonStorey0::<>m__0(System.Text.RegularExpressions.Match)
extern "C"  String_t* U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_U3CU3Em__0_m2739322565 (U3CGetLanguageStringUsingModelU3Ec__AnonStorey0_t3940050384 * __this, Match_t3164245899 * ___match0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
