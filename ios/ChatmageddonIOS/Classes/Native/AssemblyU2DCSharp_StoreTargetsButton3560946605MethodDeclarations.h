﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoreTargetsButton
struct StoreTargetsButton_t3560946605;

#include "codegen/il2cpp-codegen.h"

// System.Void StoreTargetsButton::.ctor()
extern "C"  void StoreTargetsButton__ctor_m3647566108 (StoreTargetsButton_t3560946605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreTargetsButton::SetButtonActive(System.Int32,System.Boolean)
extern "C"  void StoreTargetsButton_SetButtonActive_m472768106 (StoreTargetsButton_t3560946605 * __this, int32_t ___inventoryAmount0, bool ___locked1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreTargetsButton::OnButtonClick()
extern "C"  void StoreTargetsButton_OnButtonClick_m631931509 (StoreTargetsButton_t3560946605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreTargetsButton::SetMissileForAttack()
extern "C"  void StoreTargetsButton_SetMissileForAttack_m4082982969 (StoreTargetsButton_t3560946605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
