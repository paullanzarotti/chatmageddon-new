﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.RawBillingPlatformProvider
struct RawBillingPlatformProvider_t1857346121;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.IRawGooglePlayInterface
struct IRawGooglePlayInterface_t4240979409;
// Unibill.Impl.IRawAmazonAppStoreBillingInterface
struct IRawAmazonAppStoreBillingInterface_t2991800847;
// Unibill.Impl.IStoreKitPlugin
struct IStoreKitPlugin_t1675751103;
// Unibill.Impl.IRawSamsungAppsBillingService
struct IRawSamsungAppsBillingService_t1919646871;
// Uniject.ILevelLoadListener
struct ILevelLoadListener_t3681507369;
// Unibill.Impl.IHTTPClient
struct IHTTPClient_t1282637506;
// Uniject.IUtil
struct IUtil_t2188430191;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"

// System.Void Unibill.Impl.RawBillingPlatformProvider::.ctor(Unibill.Impl.UnibillConfiguration)
extern "C"  void RawBillingPlatformProvider__ctor_m3237980681 (RawBillingPlatformProvider_t1857346121 * __this, UnibillConfiguration_t2915611853 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IRawGooglePlayInterface Unibill.Impl.RawBillingPlatformProvider::getGooglePlay()
extern "C"  Il2CppObject * RawBillingPlatformProvider_getGooglePlay_m943489957 (RawBillingPlatformProvider_t1857346121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IRawAmazonAppStoreBillingInterface Unibill.Impl.RawBillingPlatformProvider::getAmazon()
extern "C"  Il2CppObject * RawBillingPlatformProvider_getAmazon_m483593930 (RawBillingPlatformProvider_t1857346121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IStoreKitPlugin Unibill.Impl.RawBillingPlatformProvider::getStorekit()
extern "C"  Il2CppObject * RawBillingPlatformProvider_getStorekit_m3113268585 (RawBillingPlatformProvider_t1857346121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IRawSamsungAppsBillingService Unibill.Impl.RawBillingPlatformProvider::getSamsung()
extern "C"  Il2CppObject * RawBillingPlatformProvider_getSamsung_m2369420022 (RawBillingPlatformProvider_t1857346121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Uniject.ILevelLoadListener Unibill.Impl.RawBillingPlatformProvider::getLevelLoadListener()
extern "C"  Il2CppObject * RawBillingPlatformProvider_getLevelLoadListener_m4006726769 (RawBillingPlatformProvider_t1857346121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IHTTPClient Unibill.Impl.RawBillingPlatformProvider::getHTTPClient(Uniject.IUtil)
extern "C"  Il2CppObject * RawBillingPlatformProvider_getHTTPClient_m1832815469 (RawBillingPlatformProvider_t1857346121 * __this, Il2CppObject * ___util0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
