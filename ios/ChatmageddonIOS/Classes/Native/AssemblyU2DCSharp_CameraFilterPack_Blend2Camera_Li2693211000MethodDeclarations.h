﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_LighterColor
struct CameraFilterPack_Blend2Camera_LighterColor_t2693211000;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_LighterColor::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_LighterColor__ctor_m1002268305 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_LighterColor::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_LighterColor_get_material_m3524406730 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::Start()
extern "C"  void CameraFilterPack_Blend2Camera_LighterColor_Start_m1404837017 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_LighterColor_OnRenderImage_m3754896201 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_LighterColor_OnValidate_m3953175592 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::Update()
extern "C"  void CameraFilterPack_Blend2Camera_LighterColor_Update_m877841954 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_LighterColor_OnEnable_m2466050041 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_LighterColor_OnDisable_m1210078132 (CameraFilterPack_Blend2Camera_LighterColor_t2693211000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
