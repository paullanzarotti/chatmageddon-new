﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsSearchNavigationController
struct FriendsSearchNavigationController_t1013932095;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendSearchNavScreen3018389163.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendsSearchNavigationController::.ctor()
extern "C"  void FriendsSearchNavigationController__ctor_m2467088830 (FriendsSearchNavigationController_t1013932095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavigationController::Start()
extern "C"  void FriendsSearchNavigationController_Start_m138013786 (FriendsSearchNavigationController_t1013932095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavigationController::NavigateBackwards()
extern "C"  void FriendsSearchNavigationController_NavigateBackwards_m1872431589 (FriendsSearchNavigationController_t1013932095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavigationController::NavigateForwards(FriendSearchNavScreen)
extern "C"  void FriendsSearchNavigationController_NavigateForwards_m2690917326 (FriendsSearchNavigationController_t1013932095 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsSearchNavigationController::ValidateNavigation(FriendSearchNavScreen,NavigationDirection)
extern "C"  bool FriendsSearchNavigationController_ValidateNavigation_m1383284354 (FriendsSearchNavigationController_t1013932095 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavigationController::LoadNavScreen(FriendSearchNavScreen,NavigationDirection,System.Boolean)
extern "C"  void FriendsSearchNavigationController_LoadNavScreen_m1085582476 (FriendsSearchNavigationController_t1013932095 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavigationController::UnloadNavScreen(FriendSearchNavScreen,NavigationDirection)
extern "C"  void FriendsSearchNavigationController_UnloadNavScreen_m886174178 (FriendsSearchNavigationController_t1013932095 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsSearchNavigationController::PopulateTitleBar(FriendSearchNavScreen)
extern "C"  void FriendsSearchNavigationController_PopulateTitleBar_m4054096778 (FriendsSearchNavigationController_t1013932095 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
