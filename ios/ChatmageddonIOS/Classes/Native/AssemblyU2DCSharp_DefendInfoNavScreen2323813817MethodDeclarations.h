﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendInfoNavScreen
struct DefendInfoNavScreen_t2323813817;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void DefendInfoNavScreen::.ctor()
extern "C"  void DefendInfoNavScreen__ctor_m2116037828 (DefendInfoNavScreen_t2323813817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::Start()
extern "C"  void DefendInfoNavScreen_Start_m4003746964 (DefendInfoNavScreen_t2323813817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::UIClosing()
extern "C"  void DefendInfoNavScreen_UIClosing_m113523387 (DefendInfoNavScreen_t2323813817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void DefendInfoNavScreen_ScreenLoad_m1984634484 (DefendInfoNavScreen_t2323813817 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void DefendInfoNavScreen_ScreenUnload_m3141076804 (DefendInfoNavScreen_t2323813817 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::PopulateUI()
extern "C"  void DefendInfoNavScreen_PopulateUI_m261433570 (DefendInfoNavScreen_t2323813817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DefendInfoNavScreen::WaitForSeconds(System.Single)
extern "C"  Il2CppObject * DefendInfoNavScreen_WaitForSeconds_m70754860 (DefendInfoNavScreen_t2323813817 * __this, float ___amountToWait0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::UpdateActionLabel()
extern "C"  void DefendInfoNavScreen_UpdateActionLabel_m2543156853 (DefendInfoNavScreen_t2323813817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::UpdateModel()
extern "C"  void DefendInfoNavScreen_UpdateModel_m2290092142 (DefendInfoNavScreen_t2323813817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendInfoNavScreen::ModalClosing()
extern "C"  void DefendInfoNavScreen_ModalClosing_m346166310 (DefendInfoNavScreen_t2323813817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
