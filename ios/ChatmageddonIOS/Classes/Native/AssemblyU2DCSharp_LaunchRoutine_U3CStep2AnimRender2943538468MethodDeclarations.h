﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchRoutine/<Step2AnimRenderTween>c__Iterator1
struct U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchRoutine/<Step2AnimRenderTween>c__Iterator1::.ctor()
extern "C"  void U3CStep2AnimRenderTweenU3Ec__Iterator1__ctor_m665030181 (U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaunchRoutine/<Step2AnimRenderTween>c__Iterator1::MoveNext()
extern "C"  bool U3CStep2AnimRenderTweenU3Ec__Iterator1_MoveNext_m1984790975 (U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<Step2AnimRenderTween>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStep2AnimRenderTweenU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4178015431 (U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<Step2AnimRenderTween>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStep2AnimRenderTweenU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3202652607 (U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<Step2AnimRenderTween>c__Iterator1::Dispose()
extern "C"  void U3CStep2AnimRenderTweenU3Ec__Iterator1_Dispose_m3352496078 (U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<Step2AnimRenderTween>c__Iterator1::Reset()
extern "C"  void U3CStep2AnimRenderTweenU3Ec__Iterator1_Reset_m2104628500 (U3CStep2AnimRenderTweenU3Ec__Iterator1_t2943538468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
