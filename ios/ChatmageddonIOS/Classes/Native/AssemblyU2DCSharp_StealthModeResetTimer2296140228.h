﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StealthModeResetTimer
struct  StealthModeResetTimer_t2296140228  : public MonoBehaviour_t1158329972
{
public:
	// UILabel StealthModeResetTimer::timeLabel
	UILabel_t1795115428 * ___timeLabel_2;
	// UILabel StealthModeResetTimer::descriptionLabel
	UILabel_t1795115428 * ___descriptionLabel_3;
	// UnityEngine.Coroutine StealthModeResetTimer::calcRoutine
	Coroutine_t2299508840 * ___calcRoutine_4;

public:
	inline static int32_t get_offset_of_timeLabel_2() { return static_cast<int32_t>(offsetof(StealthModeResetTimer_t2296140228, ___timeLabel_2)); }
	inline UILabel_t1795115428 * get_timeLabel_2() const { return ___timeLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_2() { return &___timeLabel_2; }
	inline void set_timeLabel_2(UILabel_t1795115428 * value)
	{
		___timeLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_2, value);
	}

	inline static int32_t get_offset_of_descriptionLabel_3() { return static_cast<int32_t>(offsetof(StealthModeResetTimer_t2296140228, ___descriptionLabel_3)); }
	inline UILabel_t1795115428 * get_descriptionLabel_3() const { return ___descriptionLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_descriptionLabel_3() { return &___descriptionLabel_3; }
	inline void set_descriptionLabel_3(UILabel_t1795115428 * value)
	{
		___descriptionLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionLabel_3, value);
	}

	inline static int32_t get_offset_of_calcRoutine_4() { return static_cast<int32_t>(offsetof(StealthModeResetTimer_t2296140228, ___calcRoutine_4)); }
	inline Coroutine_t2299508840 * get_calcRoutine_4() const { return ___calcRoutine_4; }
	inline Coroutine_t2299508840 ** get_address_of_calcRoutine_4() { return &___calcRoutine_4; }
	inline void set_calcRoutine_4(Coroutine_t2299508840 * value)
	{
		___calcRoutine_4 = value;
		Il2CppCodeGenWriteBarrier(&___calcRoutine_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
