﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsDrawingPoly
struct OnlineMapsDrawingPoly_t2218936412;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// OnlineMapsVector2i
struct OnlineMapsVector2i_t2180897250;
// OnlineMapsTileSetControl
struct OnlineMapsTileSetControl_t3368302803;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsVector2i2180897250.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl3368302803.h"

// System.Void OnlineMapsDrawingPoly::.ctor()
extern "C"  void OnlineMapsDrawingPoly__ctor_m3746760621 (OnlineMapsDrawingPoly_t2218936412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingPoly::.ctor(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  void OnlineMapsDrawingPoly__ctor_m3610918187 (OnlineMapsDrawingPoly_t2218936412 * __this, List_1_t1612828711 * ___points0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingPoly::.ctor(System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Color)
extern "C"  void OnlineMapsDrawingPoly__ctor_m552035861 (OnlineMapsDrawingPoly_t2218936412 * __this, List_1_t1612828711 * ___points0, Color_t2020392075  ___borderColor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingPoly::.ctor(System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Color,System.Single)
extern "C"  void OnlineMapsDrawingPoly__ctor_m2833841028 (OnlineMapsDrawingPoly_t2218936412 * __this, List_1_t1612828711 * ___points0, Color_t2020392075  ___borderColor1, float ___borderWeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingPoly::.ctor(System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Color,System.Single,UnityEngine.Color)
extern "C"  void OnlineMapsDrawingPoly__ctor_m83604546 (OnlineMapsDrawingPoly_t2218936412 * __this, List_1_t1612828711 * ___points0, Color_t2020392075  ___borderColor1, float ___borderWeight2, Color_t2020392075  ___backgroundColor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMapsDrawingPoly::get_center()
extern "C"  Vector2_t2243707579  OnlineMapsDrawingPoly_get_center_m3262732566 (OnlineMapsDrawingPoly_t2218936412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingPoly::Draw(UnityEngine.Color[],OnlineMapsVector2i,System.Int32,System.Int32,System.Int32)
extern "C"  void OnlineMapsDrawingPoly_Draw_m558844342 (OnlineMapsDrawingPoly_t2218936412 * __this, ColorU5BU5D_t672350442* ___buffer0, OnlineMapsVector2i_t2180897250 * ___bufferPosition1, int32_t ___bufferWidth2, int32_t ___bufferHeight3, int32_t ___zoom4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsDrawingPoly::DrawOnTileset(OnlineMapsTileSetControl)
extern "C"  void OnlineMapsDrawingPoly_DrawOnTileset_m1106524147 (OnlineMapsDrawingPoly_t2218936412 * __this, OnlineMapsTileSetControl_t3368302803 * ___control0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsDrawingPoly::HitTest(UnityEngine.Vector2,System.Int32)
extern "C"  bool OnlineMapsDrawingPoly_HitTest_m386327015 (OnlineMapsDrawingPoly_t2218936412 * __this, Vector2_t2243707579  ___positionLatLng0, int32_t ___zoom1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
