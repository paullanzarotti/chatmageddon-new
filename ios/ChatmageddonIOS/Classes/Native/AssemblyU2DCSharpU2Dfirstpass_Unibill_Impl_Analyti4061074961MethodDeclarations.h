﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.AnalyticsReporter
struct AnalyticsReporter_t4061074961;
// Unibill.Biller
struct Biller_t1615588570;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.IHTTPClient
struct IHTTPClient_t1282637506;
// Uniject.IStorage
struct IStorage_t1347868490;
// Uniject.IUtil
struct IUtil_t2188430191;
// Uniject.ILevelLoadListener
struct ILevelLoadListener_t3681507369;
// System.String
struct String_t;
// PurchaseEvent
struct PurchaseEvent_t743554429;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Biller1615588570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseEvent743554429.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Analyti4278177091.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void Unibill.Impl.AnalyticsReporter::.ctor(Unibill.Biller,Unibill.Impl.UnibillConfiguration,Unibill.Impl.IHTTPClient,Uniject.IStorage,Uniject.IUtil,Uniject.ILevelLoadListener)
extern "C"  void AnalyticsReporter__ctor_m451658555 (AnalyticsReporter_t4061074961 * __this, Biller_t1615588570 * ___biller0, UnibillConfiguration_t2915611853 * ___config1, Il2CppObject * ___client2, Il2CppObject * ___storage3, Il2CppObject * ___util4, Il2CppObject * ___listener5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::onLevelLoad()
extern "C"  void AnalyticsReporter_onLevelLoad_m3666238650 (AnalyticsReporter_t4061074961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.AnalyticsReporter::getUserId(Uniject.IStorage)
extern "C"  String_t* AnalyticsReporter_getUserId_m609583364 (AnalyticsReporter_t4061074961 * __this, Il2CppObject * ___storage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::onSucceeded(PurchaseEvent)
extern "C"  void AnalyticsReporter_onSucceeded_m3845136166 (AnalyticsReporter_t4061074961 * __this, PurchaseEvent_t743554429 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::onCancelled(PurchaseEvent)
extern "C"  void AnalyticsReporter_onCancelled_m349970644 (AnalyticsReporter_t4061074961 * __this, PurchaseEvent_t743554429 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::onEvent(Unibill.Impl.AnalyticsReporter/EventType,PurchasableItem,System.String)
extern "C"  void AnalyticsReporter_onEvent_m46565938 (AnalyticsReporter_t4061074961 * __this, int32_t ___e0, PurchasableItem_t3963353899 * ___item1, String_t* ___receipt2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::onEvent(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnalyticsReporter_onEvent_m326783149 (AnalyticsReporter_t4061074961 * __this, Dictionary_2_t309261261 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.AnalyticsReporter::encodeLevelChange()
extern "C"  Dictionary_2_t309261261 * AnalyticsReporter_encodeLevelChange_m3893521749 (AnalyticsReporter_t4061074961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.AnalyticsReporter::formatTimestamp(System.DateTime)
extern "C"  String_t* AnalyticsReporter_formatTimestamp_m1478699977 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___timestamp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.AnalyticsReporter::getBaseRequest(Unibill.Impl.AnalyticsReporter/EventType)
extern "C"  Dictionary_2_t309261261 * AnalyticsReporter_getBaseRequest_m2778351378 (AnalyticsReporter_t4061074961 * __this, int32_t ___eventType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.AnalyticsReporter::encodeConfig()
extern "C"  Dictionary_2_t309261261 * AnalyticsReporter_encodeConfig_m783992623 (AnalyticsReporter_t4061074961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.AnalyticsReporter::encodeItem(PurchasableItem,System.String)
extern "C"  Dictionary_2_t309261261 * AnalyticsReporter_encodeItem_m2452716355 (AnalyticsReporter_t4061074961 * __this, PurchasableItem_t3963353899 * ___item0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.AnalyticsReporter::encodeDeviceInfo()
extern "C"  Dictionary_2_t309261261 * AnalyticsReporter_encodeDeviceInfo_m914963195 (AnalyticsReporter_t4061074961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::<AnalyticsReporter>m__0(PurchasableItem)
extern "C"  void AnalyticsReporter_U3CAnalyticsReporterU3Em__0_m1335683472 (AnalyticsReporter_t4061074961 * __this, PurchasableItem_t3963353899 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::<AnalyticsReporter>m__1(PurchasableItem)
extern "C"  void AnalyticsReporter_U3CAnalyticsReporterU3Em__1_m3745827407 (AnalyticsReporter_t4061074961 * __this, PurchasableItem_t3963353899 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::<AnalyticsReporter>m__2(System.Boolean)
extern "C"  void AnalyticsReporter_U3CAnalyticsReporterU3Em__2_m1932723038 (AnalyticsReporter_t4061074961 * __this, bool ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::<AnalyticsReporter>m__3(System.Boolean)
extern "C"  void AnalyticsReporter_U3CAnalyticsReporterU3Em__3_m252297887 (AnalyticsReporter_t4061074961 * __this, bool ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.AnalyticsReporter::<AnalyticsReporter>m__4()
extern "C"  void AnalyticsReporter_U3CAnalyticsReporterU3Em__4_m3909220447 (AnalyticsReporter_t4061074961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
