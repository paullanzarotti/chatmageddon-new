﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3671056111.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22812303849.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2156639896_gshared (InternalEnumerator_1_t3671056111 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2156639896(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3671056111 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2156639896_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1263051440_gshared (InternalEnumerator_1_t3671056111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1263051440(__this, method) ((  void (*) (InternalEnumerator_1_t3671056111 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1263051440_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m989015812_gshared (InternalEnumerator_1_t3671056111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m989015812(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3671056111 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m989015812_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m121937711_gshared (InternalEnumerator_1_t3671056111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m121937711(__this, method) ((  void (*) (InternalEnumerator_1_t3671056111 *, const MethodInfo*))InternalEnumerator_1_Dispose_m121937711_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1852964992_gshared (InternalEnumerator_1_t3671056111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1852964992(__this, method) ((  bool (*) (InternalEnumerator_1_t3671056111 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1852964992_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Char,System.Char>>::get_Current()
extern "C"  KeyValuePair_2_t2812303849  InternalEnumerator_1_get_Current_m586885887_gshared (InternalEnumerator_1_t3671056111 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m586885887(__this, method) ((  KeyValuePair_2_t2812303849  (*) (InternalEnumerator_1_t3671056111 *, const MethodInfo*))InternalEnumerator_1_get_Current_m586885887_gshared)(__this, method)
