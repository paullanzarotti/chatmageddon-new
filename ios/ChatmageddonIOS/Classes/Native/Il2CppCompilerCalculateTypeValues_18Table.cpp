﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_CodeDom_CodeEntryPointMethod2358493612.h"
#include "System_System_CodeDom_CodeMemberEvent1412924361.h"
#include "System_System_CodeDom_CodeMemberField487462747.h"
#include "System_System_CodeDom_CodeMemberMethod3029799204.h"
#include "System_System_CodeDom_CodeMemberProperty387075564.h"
#include "System_System_CodeDom_CodeObject394523236.h"
#include "System_System_CodeDom_CodeSnippetTypeMember3205083186.h"
#include "System_System_CodeDom_CodeTypeConstructor1021880337.h"
#include "System_System_CodeDom_CodeTypeDeclaration3088034193.h"
#include "System_System_CodeDom_CodeTypeMember3928690109.h"
#include "System_System_CodeDom_Compiler_CodeDomProvider2842833816.h"
#include "System_System_CodeDom_Compiler_CodeGenerator1245030040.h"
#include "System_System_CodeDom_Compiler_CodeGenerator_Visit4003864182.h"
#include "System_System_CodeDom_Compiler_CompilerErrorCollec2852289537.h"
#include "System_System_CodeDom_Compiler_CompilerError2965933621.h"
#include "System_System_CodeDom_Compiler_CompilerParameters2983628483.h"
#include "System_System_CodeDom_Compiler_CompilerResults2337889805.h"
#include "System_System_CodeDom_Compiler_GeneratedCodeAttrib4009325960.h"
#include "System_System_CodeDom_Compiler_TempFileCollection3377240462.h"
#include "System_System_CodeDom_MemberAttributes1656695347.h"
#include "System_System_Collections_Generic_RBTree1544615604.h"
#include "System_System_Collections_Generic_RBTree_Node2499136326.h"
#include "System_System_Collections_Generic_RBTree_NodeEnumer648190100.h"
#include "System_System_Collections_Specialized_CollectionsU1732617381.h"
#include "System_System_Collections_Specialized_HybridDiction290043810.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_ProcessStrin2580846352.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "System_System_Collections_Specialized_StringEnumera441637433.h"
#include "System_System_ComponentModel_AddingNewEventArgs3938289828.h"
#include "System_System_ComponentModel_AmbientValueAttribute1570695927.h"
#include "System_System_ComponentModel_ArrayConverter2804512129.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayPr599180064.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938.h"
#include "System_System_ComponentModel_AsyncOperation1185541675.h"
#include "System_System_ComponentModel_AsyncOperationManager3553158318.h"
#include "System_System_ComponentModel_AttributeProviderAttri780297797.h"
#include "System_System_ComponentModel_AttributeCollection1925812292.h"
#include "System_System_ComponentModel_BackgroundWorker4230068110.h"
#include "System_System_ComponentModel_BackgroundWorker_Proce308732489.h"
#include "System_System_ComponentModel_BaseNumberConverter1130358776.h"
#include "System_System_ComponentModel_BindableAttribute3544708709.h"
#include "System_System_ComponentModel_BindableSupport1735231582.h"
#include "System_System_ComponentModel_BindingDirection2271780566.h"
#include "System_System_ComponentModel_BooleanConverter284715810.h"
#include "System_System_ComponentModel_BrowsableAttribute2487167291.h"
#include "System_System_ComponentModel_ByteConverter1265255600.h"
#include "System_System_ComponentModel_CancelEventArgs1976499267.h"
#include "System_System_ComponentModel_CategoryAttribute540457070.h"
#include "System_System_ComponentModel_CharConverter437233350.h"
#include "System_System_ComponentModel_CollectionChangeActio3495844100.h"
#include "System_System_ComponentModel_CollectionChangeEvent1734749345.h"
#include "System_System_ComponentModel_CollectionConverter2459375096.h"
#include "System_System_ComponentModel_ComplexBindingProperti527181054.h"
#include "System_System_ComponentModel_ComponentCollection737017907.h"
#include "System_System_ComponentModel_ComponentConverter3121608223.h"
#include "System_System_ComponentModel_Component2826673791.h"
#include "System_System_ComponentModel_ComponentEditor3060735964.h"
#include "System_System_ComponentModel_ComponentResourceMana1797694556.h"
#include "System_System_ComponentModel_Container3759338679.h"
#include "System_System_ComponentModel_Container_DefaultSite2367623411.h"
#include "System_System_ComponentModel_ContainerFilterServic3050250756.h"
#include "System_System_ComponentModel_CultureInfoConverter2239982248.h"
#include "System_System_ComponentModel_CultureInfoConverter_2714931249.h"
#include "System_System_ComponentModel_CustomTypeDescriptor1720788626.h"
#include "System_System_ComponentModel_DataObjectAttribute1431615805.h"
#include "System_System_ComponentModel_DataObjectFieldAttribu580021281.h"
#include "System_System_ComponentModel_DataObjectMethodAttri1292727732.h"
#include "System_System_ComponentModel_DataObjectMethodType1069349368.h"
#include "System_System_ComponentModel_DateTimeConverter2436647419.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (CodeEntryPointMethod_t2358493612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (CodeMemberEvent_t1412924361), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (CodeMemberField_t487462747), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (CodeMemberMethod_t3029799204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (CodeMemberProperty_t387075564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (CodeObject_t394523236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (CodeSnippetTypeMember_t3205083186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (CodeTypeConstructor_t1021880337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (CodeTypeDeclaration_t3088034193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	CodeTypeDeclaration_t3088034193::get_offset_of_attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (CodeTypeMember_t3928690109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[2] = 
{
	CodeTypeMember_t3928690109::get_offset_of_name_0(),
	CodeTypeMember_t3928690109::get_offset_of_attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (CodeDomProvider_t2842833816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (CodeGenerator_t1245030040), -1, sizeof(CodeGenerator_t1245030040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[2] = 
{
	CodeGenerator_t1245030040::get_offset_of_visitor_0(),
	CodeGenerator_t1245030040_StaticFields::get_offset_of_memberTypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (Visitor_t4003864182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	Visitor_t4003864182::get_offset_of_g_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (CompilerErrorCollection_t2852289537), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (CompilerError_t2965933621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[6] = 
{
	CompilerError_t2965933621::get_offset_of_fileName_0(),
	CompilerError_t2965933621::get_offset_of_line_1(),
	CompilerError_t2965933621::get_offset_of_column_2(),
	CompilerError_t2965933621::get_offset_of_errorNumber_3(),
	CompilerError_t2965933621::get_offset_of_errorText_4(),
	CompilerError_t2965933621::get_offset_of_isWarning_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (CompilerParameters_t2983628483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[14] = 
{
	CompilerParameters_t2983628483::get_offset_of_compilerOptions_0(),
	CompilerParameters_t2983628483::get_offset_of_evidence_1(),
	CompilerParameters_t2983628483::get_offset_of_generateExecutable_2(),
	CompilerParameters_t2983628483::get_offset_of_generateInMemory_3(),
	CompilerParameters_t2983628483::get_offset_of_includeDebugInformation_4(),
	CompilerParameters_t2983628483::get_offset_of_outputAssembly_5(),
	CompilerParameters_t2983628483::get_offset_of_referencedAssemblies_6(),
	CompilerParameters_t2983628483::get_offset_of_tempFiles_7(),
	CompilerParameters_t2983628483::get_offset_of_treatWarningsAsErrors_8(),
	CompilerParameters_t2983628483::get_offset_of_userToken_9(),
	CompilerParameters_t2983628483::get_offset_of_warningLevel_10(),
	CompilerParameters_t2983628483::get_offset_of_win32Resource_11(),
	CompilerParameters_t2983628483::get_offset_of_embedded_resources_12(),
	CompilerParameters_t2983628483::get_offset_of_linked_resources_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (CompilerResults_t2337889805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[6] = 
{
	CompilerResults_t2337889805::get_offset_of_compiledAssembly_0(),
	CompilerResults_t2337889805::get_offset_of_errors_1(),
	CompilerResults_t2337889805::get_offset_of_nativeCompilerReturnValue_2(),
	CompilerResults_t2337889805::get_offset_of_output_3(),
	CompilerResults_t2337889805::get_offset_of_pathToAssembly_4(),
	CompilerResults_t2337889805::get_offset_of_tempFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (GeneratedCodeAttribute_t4009325960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[2] = 
{
	GeneratedCodeAttribute_t4009325960::get_offset_of_tool_0(),
	GeneratedCodeAttribute_t4009325960::get_offset_of_version_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (TempFileCollection_t3377240462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[6] = 
{
	TempFileCollection_t3377240462::get_offset_of_filehash_0(),
	TempFileCollection_t3377240462::get_offset_of_tempdir_1(),
	TempFileCollection_t3377240462::get_offset_of_keepfiles_2(),
	TempFileCollection_t3377240462::get_offset_of_basepath_3(),
	TempFileCollection_t3377240462::get_offset_of_rnd_4(),
	TempFileCollection_t3377240462::get_offset_of_ownTempDir_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (MemberAttributes_t1656695347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1822[17] = 
{
	MemberAttributes_t1656695347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (RBTree_t1544615604), -1, 0, sizeof(RBTree_t1544615604_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	RBTree_t1544615604::get_offset_of_root_0(),
	RBTree_t1544615604::get_offset_of_hlp_1(),
	RBTree_t1544615604::get_offset_of_version_2(),
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (Node_t2499136326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[3] = 
{
	Node_t2499136326::get_offset_of_left_0(),
	Node_t2499136326::get_offset_of_right_1(),
	Node_t2499136326::get_offset_of_size_black_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (NodeEnumerator_t648190100)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[3] = 
{
	NodeEnumerator_t648190100::get_offset_of_tree_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NodeEnumerator_t648190100::get_offset_of_version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NodeEnumerator_t648190100::get_offset_of_pennants_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1836[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (CollectionsUtil_t1732617381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (HybridDictionary_t290043810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[3] = 
{
	HybridDictionary_t290043810::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t290043810::get_offset_of_hashtable_1(),
	HybridDictionary_t290043810::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[4] = 
{
	ListDictionary_t3458713452::get_offset_of_count_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_head_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (DictionaryNodeEnumerator_t1923170152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[4] = 
{
	DictionaryNodeEnumerator_t1923170152::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (DictionaryNodeCollection_t528898270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[2] = 
{
	DictionaryNodeCollection_t528898270::get_offset_of_dict_0(),
	DictionaryNodeCollection_t528898270::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (DictionaryNodeCollectionEnumerator_t2037848305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[2] = 
{
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (NameObjectCollectionBase_t2034248631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[10] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2034248631::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (_Item_t3244489099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[2] = 
{
	_Item_t3244489099::get_offset_of_key_0(),
	_Item_t3244489099::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (_KeysEnumerator_t1718269396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[2] = 
{
	_KeysEnumerator_t1718269396::get_offset_of_m_collection_0(),
	_KeysEnumerator_t1718269396::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[1] = 
{
	KeysCollection_t633582367::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[2] = 
{
	NameValueCollection_t3047564564::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t3047564564::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (ProcessStringDictionary_t2580846352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[1] = 
{
	ProcessStringDictionary_t2580846352::get_offset_of_table_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (StringCollection_t352985975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[1] = 
{
	StringCollection_t352985975::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (StringDictionary_t1070889667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[1] = 
{
	StringDictionary_t1070889667::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (StringEnumerator_t441637433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[1] = 
{
	StringEnumerator_t441637433::get_offset_of_enumerable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (AddingNewEventArgs_t3938289828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[1] = 
{
	AddingNewEventArgs_t3938289828::get_offset_of_obj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (AmbientValueAttribute_t1570695927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[1] = 
{
	AmbientValueAttribute_t1570695927::get_offset_of_AmbientValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (ArrayConverter_t2804512129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (ArrayPropertyDescriptor_t599180064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[2] = 
{
	ArrayPropertyDescriptor_t599180064::get_offset_of_index_6(),
	ArrayPropertyDescriptor_t599180064::get_offset_of_array_type_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (AsyncCompletedEventArgs_t83270938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[3] = 
{
	AsyncCompletedEventArgs_t83270938::get_offset_of__error_1(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__cancelled_2(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__userState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (AsyncOperation_t1185541675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[3] = 
{
	AsyncOperation_t1185541675::get_offset_of_ctx_0(),
	AsyncOperation_t1185541675::get_offset_of_state_1(),
	AsyncOperation_t1185541675::get_offset_of_done_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (AsyncOperationManager_t3553158318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (AttributeProviderAttribute_t780297797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[2] = 
{
	AttributeProviderAttribute_t780297797::get_offset_of_type_name_0(),
	AttributeProviderAttribute_t780297797::get_offset_of_property_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (AttributeCollection_t1925812292), -1, sizeof(AttributeCollection_t1925812292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1866[2] = 
{
	AttributeCollection_t1925812292::get_offset_of_attrList_0(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (BackgroundWorker_t4230068110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[7] = 
{
	BackgroundWorker_t4230068110::get_offset_of_async_4(),
	BackgroundWorker_t4230068110::get_offset_of_cancel_pending_5(),
	BackgroundWorker_t4230068110::get_offset_of_report_progress_6(),
	BackgroundWorker_t4230068110::get_offset_of_support_cancel_7(),
	BackgroundWorker_t4230068110::get_offset_of_DoWork_8(),
	BackgroundWorker_t4230068110::get_offset_of_ProgressChanged_9(),
	BackgroundWorker_t4230068110::get_offset_of_RunWorkerCompleted_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (ProcessWorkerEventHandler_t308732489), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (BaseNumberConverter_t1130358776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[1] = 
{
	BaseNumberConverter_t1130358776::get_offset_of_InnerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (BindableAttribute_t3544708709), -1, sizeof(BindableAttribute_t3544708709_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1870[5] = 
{
	BindableAttribute_t3544708709::get_offset_of_bindable_0(),
	BindableAttribute_t3544708709::get_offset_of_direction_1(),
	BindableAttribute_t3544708709_StaticFields::get_offset_of_No_2(),
	BindableAttribute_t3544708709_StaticFields::get_offset_of_Yes_3(),
	BindableAttribute_t3544708709_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (BindableSupport_t1735231582)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1871[4] = 
{
	BindableSupport_t1735231582::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (BindingDirection_t2271780566)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1872[3] = 
{
	BindingDirection_t2271780566::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (BooleanConverter_t284715810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (BrowsableAttribute_t2487167291), -1, sizeof(BrowsableAttribute_t2487167291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1875[4] = 
{
	BrowsableAttribute_t2487167291::get_offset_of_browsable_0(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Default_1(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_No_2(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (ByteConverter_t1265255600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (CancelEventArgs_t1976499267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	CancelEventArgs_t1976499267::get_offset_of_cancel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (CategoryAttribute_t540457070), -1, sizeof(CategoryAttribute_t540457070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1878[17] = 
{
	CategoryAttribute_t540457070::get_offset_of_category_0(),
	CategoryAttribute_t540457070::get_offset_of_IsLocalized_1(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_action_2(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_appearance_3(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_behaviour_4(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_data_5(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_def_6(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_design_7(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_drag_drop_8(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_focus_9(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_format_10(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_key_11(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_layout_12(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_mouse_13(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_window_style_14(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_async_15(),
	CategoryAttribute_t540457070_StaticFields::get_offset_of_lockobj_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (CharConverter_t437233350), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (CollectionChangeAction_t3495844100)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[4] = 
{
	CollectionChangeAction_t3495844100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (CollectionChangeEventArgs_t1734749345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[2] = 
{
	CollectionChangeEventArgs_t1734749345::get_offset_of_changeAction_1(),
	CollectionChangeEventArgs_t1734749345::get_offset_of_theElement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (CollectionConverter_t2459375096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (ComplexBindingPropertiesAttribute_t527181054), -1, sizeof(ComplexBindingPropertiesAttribute_t527181054_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1883[3] = 
{
	ComplexBindingPropertiesAttribute_t527181054::get_offset_of_data_source_0(),
	ComplexBindingPropertiesAttribute_t527181054::get_offset_of_data_member_1(),
	ComplexBindingPropertiesAttribute_t527181054_StaticFields::get_offset_of_Default_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ComponentCollection_t737017907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (ComponentConverter_t3121608223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (Component_t2826673791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[3] = 
{
	Component_t2826673791::get_offset_of_event_handlers_1(),
	Component_t2826673791::get_offset_of_mySite_2(),
	Component_t2826673791::get_offset_of_disposedEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (ComponentEditor_t3060735964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (ComponentResourceManager_t1797694556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (Container_t3759338679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[1] = 
{
	Container_t3759338679::get_offset_of_c_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (DefaultSite_t2367623411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[3] = 
{
	DefaultSite_t2367623411::get_offset_of_component_0(),
	DefaultSite_t2367623411::get_offset_of_container_1(),
	DefaultSite_t2367623411::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (ContainerFilterService_t3050250756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (CultureInfoConverter_t2239982248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[1] = 
{
	CultureInfoConverter_t2239982248::get_offset_of__standardValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (CultureInfoComparer_t2714931249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (CustomTypeDescriptor_t1720788626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[1] = 
{
	CustomTypeDescriptor_t1720788626::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (DataObjectAttribute_t1431615805), -1, sizeof(DataObjectAttribute_t1431615805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1895[4] = 
{
	DataObjectAttribute_t1431615805_StaticFields::get_offset_of_DataObject_0(),
	DataObjectAttribute_t1431615805_StaticFields::get_offset_of_Default_1(),
	DataObjectAttribute_t1431615805_StaticFields::get_offset_of_NonDataObject_2(),
	DataObjectAttribute_t1431615805::get_offset_of__isDataObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (DataObjectFieldAttribute_t580021281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[4] = 
{
	DataObjectFieldAttribute_t580021281::get_offset_of_primary_key_0(),
	DataObjectFieldAttribute_t580021281::get_offset_of_is_identity_1(),
	DataObjectFieldAttribute_t580021281::get_offset_of_is_nullable_2(),
	DataObjectFieldAttribute_t580021281::get_offset_of_length_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (DataObjectMethodAttribute_t1292727732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[2] = 
{
	DataObjectMethodAttribute_t1292727732::get_offset_of__methodType_0(),
	DataObjectMethodAttribute_t1292727732::get_offset_of__isDefault_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (DataObjectMethodType_t1069349368)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[6] = 
{
	DataObjectMethodType_t1069349368::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (DateTimeConverter_t2436647419), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
