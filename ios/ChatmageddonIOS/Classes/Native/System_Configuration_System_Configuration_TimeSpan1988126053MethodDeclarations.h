﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.TimeSpanValidatorAttribute
struct TimeSpanValidatorAttribute_t1988126053;
// System.String
struct String_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void System.Configuration.TimeSpanValidatorAttribute::.ctor()
extern "C"  void TimeSpanValidatorAttribute__ctor_m2433330042 (TimeSpanValidatorAttribute_t1988126053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.TimeSpanValidatorAttribute::get_MaxValueString()
extern "C"  String_t* TimeSpanValidatorAttribute_get_MaxValueString_m3957197068 (TimeSpanValidatorAttribute_t1988126053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.TimeSpanValidatorAttribute::set_MaxValueString(System.String)
extern "C"  void TimeSpanValidatorAttribute_set_MaxValueString_m954731083 (TimeSpanValidatorAttribute_t1988126053 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.TimeSpanValidatorAttribute::get_MinValueString()
extern "C"  String_t* TimeSpanValidatorAttribute_get_MinValueString_m2397403898 (TimeSpanValidatorAttribute_t1988126053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.TimeSpanValidatorAttribute::set_MinValueString(System.String)
extern "C"  void TimeSpanValidatorAttribute_set_MinValueString_m2630531397 (TimeSpanValidatorAttribute_t1988126053 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Configuration.TimeSpanValidatorAttribute::get_MaxValue()
extern "C"  TimeSpan_t3430258949  TimeSpanValidatorAttribute_get_MaxValue_m3877594065 (TimeSpanValidatorAttribute_t1988126053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Configuration.TimeSpanValidatorAttribute::get_MinValue()
extern "C"  TimeSpan_t3430258949  TimeSpanValidatorAttribute_get_MinValue_m1305386491 (TimeSpanValidatorAttribute_t1988126053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.TimeSpanValidatorAttribute::get_ExcludeRange()
extern "C"  bool TimeSpanValidatorAttribute_get_ExcludeRange_m3849118326 (TimeSpanValidatorAttribute_t1988126053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.TimeSpanValidatorAttribute::set_ExcludeRange(System.Boolean)
extern "C"  void TimeSpanValidatorAttribute_set_ExcludeRange_m1411622761 (TimeSpanValidatorAttribute_t1988126053 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.TimeSpanValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * TimeSpanValidatorAttribute_get_ValidatorInstance_m3912769897 (TimeSpanValidatorAttribute_t1988126053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
