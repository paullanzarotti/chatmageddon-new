﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveMarkersExample
struct MoveMarkersExample_t2707127286;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveMarkersExample::.ctor()
extern "C"  void MoveMarkersExample__ctor_m1138574663 (MoveMarkersExample_t2707127286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveMarkersExample::Start()
extern "C"  void MoveMarkersExample_Start_m2957218307 (MoveMarkersExample_t2707127286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveMarkersExample::Update()
extern "C"  void MoveMarkersExample_Update_m594881216 (MoveMarkersExample_t2707127286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
