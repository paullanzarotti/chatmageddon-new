﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonServer/<VerifyMobileNumber>c__AnonStorey3
struct  U3CVerifyMobileNumberU3Ec__AnonStorey3_t164903227  : public Il2CppObject
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ChatmageddonServer/<VerifyMobileNumber>c__AnonStorey3::callBack
	Action_3_t3681841185 * ___callBack_0;

public:
	inline static int32_t get_offset_of_callBack_0() { return static_cast<int32_t>(offsetof(U3CVerifyMobileNumberU3Ec__AnonStorey3_t164903227, ___callBack_0)); }
	inline Action_3_t3681841185 * get_callBack_0() const { return ___callBack_0; }
	inline Action_3_t3681841185 ** get_address_of_callBack_0() { return &___callBack_0; }
	inline void set_callBack_0(Action_3_t3681841185 * value)
	{
		___callBack_0 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
