﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoaderUIScaler
struct  LoaderUIScaler_t4194491137  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite LoaderUIScaler::background
	UISprite_t603616735 * ___background_14;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(LoaderUIScaler_t4194491137, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
