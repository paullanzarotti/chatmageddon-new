﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UITexture
struct UITexture_t2537039969;
// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIComponentStyle
struct  GUIComponentStyle_t3347948829  : public MonoBehaviour_t1158329972
{
public:
	// UISprite GUIComponentStyle::componentSprite
	UISprite_t603616735 * ___componentSprite_2;
	// UITexture GUIComponentStyle::componentTexture
	UITexture_t2537039969 * ___componentTexture_3;
	// UILabel GUIComponentStyle::componentLabel
	UILabel_t1795115428 * ___componentLabel_4;
	// System.String GUIComponentStyle::spriteStyleName
	String_t* ___spriteStyleName_5;
	// System.String GUIComponentStyle::textureName
	String_t* ___textureName_6;
	// System.String GUIComponentStyle::labelSpriteName
	String_t* ___labelSpriteName_7;

public:
	inline static int32_t get_offset_of_componentSprite_2() { return static_cast<int32_t>(offsetof(GUIComponentStyle_t3347948829, ___componentSprite_2)); }
	inline UISprite_t603616735 * get_componentSprite_2() const { return ___componentSprite_2; }
	inline UISprite_t603616735 ** get_address_of_componentSprite_2() { return &___componentSprite_2; }
	inline void set_componentSprite_2(UISprite_t603616735 * value)
	{
		___componentSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___componentSprite_2, value);
	}

	inline static int32_t get_offset_of_componentTexture_3() { return static_cast<int32_t>(offsetof(GUIComponentStyle_t3347948829, ___componentTexture_3)); }
	inline UITexture_t2537039969 * get_componentTexture_3() const { return ___componentTexture_3; }
	inline UITexture_t2537039969 ** get_address_of_componentTexture_3() { return &___componentTexture_3; }
	inline void set_componentTexture_3(UITexture_t2537039969 * value)
	{
		___componentTexture_3 = value;
		Il2CppCodeGenWriteBarrier(&___componentTexture_3, value);
	}

	inline static int32_t get_offset_of_componentLabel_4() { return static_cast<int32_t>(offsetof(GUIComponentStyle_t3347948829, ___componentLabel_4)); }
	inline UILabel_t1795115428 * get_componentLabel_4() const { return ___componentLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_componentLabel_4() { return &___componentLabel_4; }
	inline void set_componentLabel_4(UILabel_t1795115428 * value)
	{
		___componentLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___componentLabel_4, value);
	}

	inline static int32_t get_offset_of_spriteStyleName_5() { return static_cast<int32_t>(offsetof(GUIComponentStyle_t3347948829, ___spriteStyleName_5)); }
	inline String_t* get_spriteStyleName_5() const { return ___spriteStyleName_5; }
	inline String_t** get_address_of_spriteStyleName_5() { return &___spriteStyleName_5; }
	inline void set_spriteStyleName_5(String_t* value)
	{
		___spriteStyleName_5 = value;
		Il2CppCodeGenWriteBarrier(&___spriteStyleName_5, value);
	}

	inline static int32_t get_offset_of_textureName_6() { return static_cast<int32_t>(offsetof(GUIComponentStyle_t3347948829, ___textureName_6)); }
	inline String_t* get_textureName_6() const { return ___textureName_6; }
	inline String_t** get_address_of_textureName_6() { return &___textureName_6; }
	inline void set_textureName_6(String_t* value)
	{
		___textureName_6 = value;
		Il2CppCodeGenWriteBarrier(&___textureName_6, value);
	}

	inline static int32_t get_offset_of_labelSpriteName_7() { return static_cast<int32_t>(offsetof(GUIComponentStyle_t3347948829, ___labelSpriteName_7)); }
	inline String_t* get_labelSpriteName_7() const { return ___labelSpriteName_7; }
	inline String_t** get_address_of_labelSpriteName_7() { return &___labelSpriteName_7; }
	inline void set_labelSpriteName_7(String_t* value)
	{
		___labelSpriteName_7 = value;
		Il2CppCodeGenWriteBarrier(&___labelSpriteName_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
