﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,DefendNavScreen>
struct NavigationController_2_t1726521459;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,DefendNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m321013590_gshared (NavigationController_2_t1726521459 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m321013590(__this, method) ((  void (*) (NavigationController_2_t1726521459 *, const MethodInfo*))NavigationController_2__ctor_m321013590_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,DefendNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1859701416_gshared (NavigationController_2_t1726521459 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m1859701416(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t1726521459 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1859701416_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,DefendNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m534062593_gshared (NavigationController_2_t1726521459 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m534062593(__this, method) ((  void (*) (NavigationController_2_t1726521459 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m534062593_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,DefendNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1021390126_gshared (NavigationController_2_t1726521459 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m1021390126(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t1726521459 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1021390126_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,DefendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m527669130_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m527669130(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t1726521459 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m527669130_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,DefendNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m1209895939_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m1209895939(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t1726521459 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m1209895939_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,DefendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m4012482644_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m4012482644(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t1726521459 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m4012482644_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,DefendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m4027734450_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m4027734450(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t1726521459 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m4027734450_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,DefendNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m1880414682_gshared (NavigationController_2_t1726521459 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m1880414682(__this, ___screen0, method) ((  void (*) (NavigationController_2_t1726521459 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m1880414682_gshared)(__this, ___screen0, method)
