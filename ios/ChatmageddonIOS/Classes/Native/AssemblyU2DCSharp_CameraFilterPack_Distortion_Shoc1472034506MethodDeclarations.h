﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_ShockWave
struct CameraFilterPack_Distortion_ShockWave_t1472034506;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_ShockWave::.ctor()
extern "C"  void CameraFilterPack_Distortion_ShockWave__ctor_m1329223617 (CameraFilterPack_Distortion_ShockWave_t1472034506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_ShockWave::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_ShockWave_get_material_m1245333374 (CameraFilterPack_Distortion_ShockWave_t1472034506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::Start()
extern "C"  void CameraFilterPack_Distortion_ShockWave_Start_m3286507281 (CameraFilterPack_Distortion_ShockWave_t1472034506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_ShockWave_OnRenderImage_m4196192649 (CameraFilterPack_Distortion_ShockWave_t1472034506 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::OnValidate()
extern "C"  void CameraFilterPack_Distortion_ShockWave_OnValidate_m1060464318 (CameraFilterPack_Distortion_ShockWave_t1472034506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::Update()
extern "C"  void CameraFilterPack_Distortion_ShockWave_Update_m359764876 (CameraFilterPack_Distortion_ShockWave_t1472034506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::OnDisable()
extern "C"  void CameraFilterPack_Distortion_ShockWave_OnDisable_m3684983770 (CameraFilterPack_Distortion_ShockWave_t1472034506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
