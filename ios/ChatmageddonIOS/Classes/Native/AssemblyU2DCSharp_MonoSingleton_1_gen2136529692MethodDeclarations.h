﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<FlurryController>::.ctor()
#define MonoSingleton_1__ctor_m801333200(__this, method) ((  void (*) (MonoSingleton_1_t2136529692 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<FlurryController>::Awake()
#define MonoSingleton_1_Awake_m1052351231(__this, method) ((  void (*) (MonoSingleton_1_t2136529692 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<FlurryController>::get_Instance()
#define MonoSingleton_1_get_Instance_m2166063373(__this /* static, unused */, method) ((  FlurryController_t2385863972 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<FlurryController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m46061225(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<FlurryController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2432465586(__this, method) ((  void (*) (MonoSingleton_1_t2136529692 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<FlurryController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1224157722(__this, method) ((  void (*) (MonoSingleton_1_t2136529692 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<FlurryController>::.cctor()
#define MonoSingleton_1__cctor_m949462949(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
