﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImageEffectBase
struct ImageEffectBase_t898523515;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"

// System.Void ImageEffectBase::.ctor()
extern "C"  void ImageEffectBase__ctor_m3746019910 (ImageEffectBase_t898523515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageEffectBase::Start()
extern "C"  void ImageEffectBase_Start_m312878582 (ImageEffectBase_t898523515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material ImageEffectBase::get_material()
extern "C"  Material_t193706927 * ImageEffectBase_get_material_m4089051505 (ImageEffectBase_t898523515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageEffectBase::OnDisable()
extern "C"  void ImageEffectBase_OnDisable_m683909301 (ImageEffectBase_t898523515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
