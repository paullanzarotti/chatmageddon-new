﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.MappingFileProvider
struct MappingFileProvider_t2052216305;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>
struct SortedDictionary_2_t1020118611;
// System.String
struct String_t;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"

// System.Void PhoneNumbers.MappingFileProvider::.cctor()
extern "C"  void MappingFileProvider__cctor_m308320445 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.MappingFileProvider::.ctor()
extern "C"  void MappingFileProvider__ctor_m1537004668 (MappingFileProvider_t2052216305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.MappingFileProvider::ReadFileConfigs(System.Collections.Generic.SortedDictionary`2<System.Int32,System.Collections.Generic.HashSet`1<System.String>>)
extern "C"  void MappingFileProvider_ReadFileConfigs_m78986328 (MappingFileProvider_t2052216305 * __this, SortedDictionary_2_t1020118611 * ___availableDataFiles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.MappingFileProvider::ToString()
extern "C"  String_t* MappingFileProvider_ToString_m2214385957 (MappingFileProvider_t2052216305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.MappingFileProvider::GetFileName(System.Int32,System.String,System.String,System.String)
extern "C"  String_t* MappingFileProvider_GetFileName_m2300544539 (MappingFileProvider_t2052216305 * __this, int32_t ___countryCallingCode0, String_t* ___language1, String_t* ___script2, String_t* ___region3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.MappingFileProvider::FindBestMatchingLanguageCode(System.Collections.Generic.HashSet`1<System.String>,System.String,System.String,System.String)
extern "C"  String_t* MappingFileProvider_FindBestMatchingLanguageCode_m647333354 (MappingFileProvider_t2052216305 * __this, HashSet_1_t362681087 * ___setOfLangs0, String_t* ___language1, String_t* ___script2, String_t* ___region3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.MappingFileProvider::OnlyOneOfScriptOrRegionIsEmpty(System.String,System.String)
extern "C"  bool MappingFileProvider_OnlyOneOfScriptOrRegionIsEmpty_m1257296784 (MappingFileProvider_t2052216305 * __this, String_t* ___script0, String_t* ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder PhoneNumbers.MappingFileProvider::ConstructFullLocale(System.String,System.String,System.String)
extern "C"  StringBuilder_t1221177846 * MappingFileProvider_ConstructFullLocale_m2832287501 (MappingFileProvider_t2052216305 * __this, String_t* ___language0, String_t* ___script1, String_t* ___region2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.MappingFileProvider::AppendSubsequentLocalePart(System.String,System.Text.StringBuilder)
extern "C"  void MappingFileProvider_AppendSubsequentLocalePart_m3082675342 (MappingFileProvider_t2052216305 * __this, String_t* ___subsequentLocalePart0, StringBuilder_t1221177846 * ___fullLocale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.MappingFileProvider::<ToString>m__0(System.String)
extern "C"  String_t* MappingFileProvider_U3CToStringU3Em__0_m3344797572 (Il2CppObject * __this /* static, unused */, String_t* ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
