﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenEmailButton
struct OpenEmailButton_t3875630000;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenEmailButton::.ctor()
extern "C"  void OpenEmailButton__ctor_m2705759641 (OpenEmailButton_t3875630000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenEmailButton::OnButtonClick()
extern "C"  void OpenEmailButton_OnButtonClick_m3561619012 (OpenEmailButton_t3875630000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
