﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>
struct Dictionary_2_t3263405159;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va654970627.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4206125322_gshared (Enumerator_t654970627 * __this, Dictionary_2_t3263405159 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4206125322(__this, ___host0, method) ((  void (*) (Enumerator_t654970627 *, Dictionary_2_t3263405159 *, const MethodInfo*))Enumerator__ctor_m4206125322_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4179254427_gshared (Enumerator_t654970627 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4179254427(__this, method) ((  Il2CppObject * (*) (Enumerator_t654970627 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4179254427_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m337816743_gshared (Enumerator_t654970627 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m337816743(__this, method) ((  void (*) (Enumerator_t654970627 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m337816743_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m1146605142_gshared (Enumerator_t654970627 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1146605142(__this, method) ((  void (*) (Enumerator_t654970627 *, const MethodInfo*))Enumerator_Dispose_m1146605142_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1500004099_gshared (Enumerator_t654970627 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1500004099(__this, method) ((  bool (*) (Enumerator_t654970627 *, const MethodInfo*))Enumerator_MoveNext_m1500004099_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::get_Current()
extern "C"  bool Enumerator_get_Current_m541897177_gshared (Enumerator_t654970627 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m541897177(__this, method) ((  bool (*) (Enumerator_t654970627 *, const MethodInfo*))Enumerator_get_Current_m541897177_gshared)(__this, method)
