﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Vision_Tunnel
struct CameraFilterPack_Vision_Tunnel_t582645848;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Vision_Tunnel::.ctor()
extern "C"  void CameraFilterPack_Vision_Tunnel__ctor_m4259981071 (CameraFilterPack_Vision_Tunnel_t582645848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Tunnel::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Vision_Tunnel_get_material_m3137912806 (CameraFilterPack_Vision_Tunnel_t582645848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::Start()
extern "C"  void CameraFilterPack_Vision_Tunnel_Start_m4231949571 (CameraFilterPack_Vision_Tunnel_t582645848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Vision_Tunnel_OnRenderImage_m1044510515 (CameraFilterPack_Vision_Tunnel_t582645848 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::OnValidate()
extern "C"  void CameraFilterPack_Vision_Tunnel_OnValidate_m1647100648 (CameraFilterPack_Vision_Tunnel_t582645848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::Update()
extern "C"  void CameraFilterPack_Vision_Tunnel_Update_m755307222 (CameraFilterPack_Vision_Tunnel_t582645848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::OnDisable()
extern "C"  void CameraFilterPack_Vision_Tunnel_OnDisable_m1298109088 (CameraFilterPack_Vision_Tunnel_t582645848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
