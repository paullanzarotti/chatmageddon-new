﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen2320304550.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FAQILP
struct  FAQILP_t1146604377  : public BaseInfiniteListPopulator_1_t2320304550
{
public:
	// System.Int32 FAQILP::maxNameLength
	int32_t ___maxNameLength_33;
	// System.Boolean FAQILP::listLoaded
	bool ___listLoaded_34;
	// System.Collections.ArrayList FAQILP::FAQArray
	ArrayList_t4252133567 * ___FAQArray_35;

public:
	inline static int32_t get_offset_of_maxNameLength_33() { return static_cast<int32_t>(offsetof(FAQILP_t1146604377, ___maxNameLength_33)); }
	inline int32_t get_maxNameLength_33() const { return ___maxNameLength_33; }
	inline int32_t* get_address_of_maxNameLength_33() { return &___maxNameLength_33; }
	inline void set_maxNameLength_33(int32_t value)
	{
		___maxNameLength_33 = value;
	}

	inline static int32_t get_offset_of_listLoaded_34() { return static_cast<int32_t>(offsetof(FAQILP_t1146604377, ___listLoaded_34)); }
	inline bool get_listLoaded_34() const { return ___listLoaded_34; }
	inline bool* get_address_of_listLoaded_34() { return &___listLoaded_34; }
	inline void set_listLoaded_34(bool value)
	{
		___listLoaded_34 = value;
	}

	inline static int32_t get_offset_of_FAQArray_35() { return static_cast<int32_t>(offsetof(FAQILP_t1146604377, ___FAQArray_35)); }
	inline ArrayList_t4252133567 * get_FAQArray_35() const { return ___FAQArray_35; }
	inline ArrayList_t4252133567 ** get_address_of_FAQArray_35() { return &___FAQArray_35; }
	inline void set_FAQArray_35(ArrayList_t4252133567 * value)
	{
		___FAQArray_35 = value;
		Il2CppCodeGenWriteBarrier(&___FAQArray_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
