﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_EnumSwitch_1_gen2186774145.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectAudienceSwitch
struct  SelectAudienceSwitch_t3539699246  : public EnumSwitch_1_t2186774145
{
public:
	// UnityEngine.GameObject SelectAudienceSwitch::selectionObject
	GameObject_t1756533147 * ___selectionObject_3;
	// UnityEngine.Vector3 SelectAudienceSwitch::selectionObjectPosition
	Vector3_t2243707580  ___selectionObjectPosition_4;
	// UnityEngine.GameObject SelectAudienceSwitch::everyoneObject
	GameObject_t1756533147 * ___everyoneObject_5;
	// UnityEngine.GameObject SelectAudienceSwitch::contactsObject
	GameObject_t1756533147 * ___contactsObject_6;
	// UnityEngine.GameObject SelectAudienceSwitch::nobodyObject
	GameObject_t1756533147 * ___nobodyObject_7;

public:
	inline static int32_t get_offset_of_selectionObject_3() { return static_cast<int32_t>(offsetof(SelectAudienceSwitch_t3539699246, ___selectionObject_3)); }
	inline GameObject_t1756533147 * get_selectionObject_3() const { return ___selectionObject_3; }
	inline GameObject_t1756533147 ** get_address_of_selectionObject_3() { return &___selectionObject_3; }
	inline void set_selectionObject_3(GameObject_t1756533147 * value)
	{
		___selectionObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectionObject_3, value);
	}

	inline static int32_t get_offset_of_selectionObjectPosition_4() { return static_cast<int32_t>(offsetof(SelectAudienceSwitch_t3539699246, ___selectionObjectPosition_4)); }
	inline Vector3_t2243707580  get_selectionObjectPosition_4() const { return ___selectionObjectPosition_4; }
	inline Vector3_t2243707580 * get_address_of_selectionObjectPosition_4() { return &___selectionObjectPosition_4; }
	inline void set_selectionObjectPosition_4(Vector3_t2243707580  value)
	{
		___selectionObjectPosition_4 = value;
	}

	inline static int32_t get_offset_of_everyoneObject_5() { return static_cast<int32_t>(offsetof(SelectAudienceSwitch_t3539699246, ___everyoneObject_5)); }
	inline GameObject_t1756533147 * get_everyoneObject_5() const { return ___everyoneObject_5; }
	inline GameObject_t1756533147 ** get_address_of_everyoneObject_5() { return &___everyoneObject_5; }
	inline void set_everyoneObject_5(GameObject_t1756533147 * value)
	{
		___everyoneObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___everyoneObject_5, value);
	}

	inline static int32_t get_offset_of_contactsObject_6() { return static_cast<int32_t>(offsetof(SelectAudienceSwitch_t3539699246, ___contactsObject_6)); }
	inline GameObject_t1756533147 * get_contactsObject_6() const { return ___contactsObject_6; }
	inline GameObject_t1756533147 ** get_address_of_contactsObject_6() { return &___contactsObject_6; }
	inline void set_contactsObject_6(GameObject_t1756533147 * value)
	{
		___contactsObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___contactsObject_6, value);
	}

	inline static int32_t get_offset_of_nobodyObject_7() { return static_cast<int32_t>(offsetof(SelectAudienceSwitch_t3539699246, ___nobodyObject_7)); }
	inline GameObject_t1756533147 * get_nobodyObject_7() const { return ___nobodyObject_7; }
	inline GameObject_t1756533147 ** get_address_of_nobodyObject_7() { return &___nobodyObject_7; }
	inline void set_nobodyObject_7(GameObject_t1756533147 * value)
	{
		___nobodyObject_7 = value;
		Il2CppCodeGenWriteBarrier(&___nobodyObject_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
