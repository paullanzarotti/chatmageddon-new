﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ComplexBindingPropertiesAttribute
struct ComplexBindingPropertiesAttribute_t527181054;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.ComplexBindingPropertiesAttribute::.ctor(System.String,System.String)
extern "C"  void ComplexBindingPropertiesAttribute__ctor_m2450738275 (ComplexBindingPropertiesAttribute_t527181054 * __this, String_t* ___dataSource0, String_t* ___dataMember1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ComplexBindingPropertiesAttribute::.ctor(System.String)
extern "C"  void ComplexBindingPropertiesAttribute__ctor_m1622540409 (ComplexBindingPropertiesAttribute_t527181054 * __this, String_t* ___dataSource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ComplexBindingPropertiesAttribute::.ctor()
extern "C"  void ComplexBindingPropertiesAttribute__ctor_m2637144123 (ComplexBindingPropertiesAttribute_t527181054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ComplexBindingPropertiesAttribute::.cctor()
extern "C"  void ComplexBindingPropertiesAttribute__cctor_m3400401352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.ComplexBindingPropertiesAttribute::get_DataMember()
extern "C"  String_t* ComplexBindingPropertiesAttribute_get_DataMember_m3675985723 (ComplexBindingPropertiesAttribute_t527181054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.ComplexBindingPropertiesAttribute::get_DataSource()
extern "C"  String_t* ComplexBindingPropertiesAttribute_get_DataSource_m2395966714 (ComplexBindingPropertiesAttribute_t527181054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ComplexBindingPropertiesAttribute::Equals(System.Object)
extern "C"  bool ComplexBindingPropertiesAttribute_Equals_m2850239604 (ComplexBindingPropertiesAttribute_t527181054 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.ComplexBindingPropertiesAttribute::GetHashCode()
extern "C"  int32_t ComplexBindingPropertiesAttribute_GetHashCode_m1294595548 (ComplexBindingPropertiesAttribute_t527181054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
