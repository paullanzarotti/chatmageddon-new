﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumberUIScaler
struct PhoneNumberUIScaler_t4195264923;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumberUIScaler::.ctor()
extern "C"  void PhoneNumberUIScaler__ctor_m1927671446 (PhoneNumberUIScaler_t4195264923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberUIScaler::GlobalUIScale()
extern "C"  void PhoneNumberUIScaler_GlobalUIScale_m723130361 (PhoneNumberUIScaler_t4195264923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
