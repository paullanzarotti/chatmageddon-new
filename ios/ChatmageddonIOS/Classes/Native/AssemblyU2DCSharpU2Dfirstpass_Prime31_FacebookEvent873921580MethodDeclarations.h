﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookEventListener
struct FacebookEventListener_t873921580;
// Prime31.P31Error
struct P31Error_t2856600608;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "P31RestKit_Prime31_P31Error2856600608.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Prime31.FacebookEventListener::.ctor()
extern "C"  void FacebookEventListener__ctor_m132983402 (FacebookEventListener_t873921580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::OnEnable()
extern "C"  void FacebookEventListener_OnEnable_m1052723482 (FacebookEventListener_t873921580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::OnDisable()
extern "C"  void FacebookEventListener_OnDisable_m921624049 (FacebookEventListener_t873921580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::sessionOpenedEvent()
extern "C"  void FacebookEventListener_sessionOpenedEvent_m1830751915 (FacebookEventListener_t873921580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::loginFailedEvent(Prime31.P31Error)
extern "C"  void FacebookEventListener_loginFailedEvent_m299036825 (FacebookEventListener_t873921580 * __this, P31Error_t2856600608 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::dialogCompletedWithUrlEvent(System.String)
extern "C"  void FacebookEventListener_dialogCompletedWithUrlEvent_m4154230726 (FacebookEventListener_t873921580 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::dialogFailedEvent(Prime31.P31Error)
extern "C"  void FacebookEventListener_dialogFailedEvent_m453353962 (FacebookEventListener_t873921580 * __this, P31Error_t2856600608 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::facebokDialogCompleted()
extern "C"  void FacebookEventListener_facebokDialogCompleted_m1560198712 (FacebookEventListener_t873921580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::graphRequestCompletedEvent(System.Object)
extern "C"  void FacebookEventListener_graphRequestCompletedEvent_m1429479522 (FacebookEventListener_t873921580 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::facebookCustomRequestFailed(Prime31.P31Error)
extern "C"  void FacebookEventListener_facebookCustomRequestFailed_m2259514540 (FacebookEventListener_t873921580 * __this, P31Error_t2856600608 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::facebookComposerCompletedEvent(System.Boolean)
extern "C"  void FacebookEventListener_facebookComposerCompletedEvent_m1036128332 (FacebookEventListener_t873921580 * __this, bool ___didSucceed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::reauthorizationSucceededEvent()
extern "C"  void FacebookEventListener_reauthorizationSucceededEvent_m2079581387 (FacebookEventListener_t873921580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::reauthorizationFailedEvent(Prime31.P31Error)
extern "C"  void FacebookEventListener_reauthorizationFailedEvent_m1044845566 (FacebookEventListener_t873921580 * __this, P31Error_t2856600608 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::shareDialogFailedEvent(Prime31.P31Error)
extern "C"  void FacebookEventListener_shareDialogFailedEvent_m3473477759 (FacebookEventListener_t873921580 * __this, P31Error_t2856600608 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookEventListener::shareDialogSucceededEvent(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void FacebookEventListener_shareDialogSucceededEvent_m4138969229 (FacebookEventListener_t873921580 * __this, Dictionary_2_t309261261 * ___dict0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
