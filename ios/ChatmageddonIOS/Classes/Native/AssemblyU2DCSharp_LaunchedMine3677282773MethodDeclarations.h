﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchedMine
struct LaunchedMine_t3677282773;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MineResultStatus1141738764.h"
#include "AssemblyU2DCSharp_WarefareState701596006.h"

// System.Void LaunchedMine::.ctor(System.Collections.Hashtable)
extern "C"  void LaunchedMine__ctor_m1515173176 (LaunchedMine_t3677282773 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMine::UpdateMine(System.Collections.Hashtable)
extern "C"  void LaunchedMine_UpdateMine_m360357800 (LaunchedMine_t3677282773 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMine::SetFriend(System.String,System.Collections.Hashtable)
extern "C"  void LaunchedMine_SetFriend_m235616940 (LaunchedMine_t3677282773 * __this, String_t* ___userID0, Hashtable_t909839986 * ___itemHash1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMine::AddResult(System.Collections.Hashtable)
extern "C"  void LaunchedMine_AddResult_m4134986394 (LaunchedMine_t3677282773 * __this, Hashtable_t909839986 * ___resultData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MineResultStatus LaunchedMine::GetResultStatusFromString(System.String)
extern "C"  int32_t LaunchedMine_GetResultStatusFromString_m2383324655 (LaunchedMine_t3677282773 * __this, String_t* ___statusString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WarefareState LaunchedMine::GetWarfareStateFromString(System.String)
extern "C"  int32_t LaunchedMine_GetWarfareStateFromString_m4284233021 (LaunchedMine_t3677282773 * __this, String_t* ___stateString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchedMine::<SetFriend>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void LaunchedMine_U3CSetFriendU3Em__0_m2723399498 (LaunchedMine_t3677282773 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
