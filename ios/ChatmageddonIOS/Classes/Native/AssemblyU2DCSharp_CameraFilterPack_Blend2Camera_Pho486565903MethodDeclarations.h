﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_PhotoshopFilters
struct CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters__ctor_m2181727004 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_PhotoshopFilters::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_PhotoshopFilters_get_material_m2562476839 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::ChangeFilters()
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters_ChangeFilters_m3108870411 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::Start()
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters_Start_m2139255152 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnRenderImage_m3742252744 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnValidate_m1592902337 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::Update()
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters_Update_m1399435347 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnEnable_m3163986384 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnDisable_m1656313571 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t486565903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
