﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchAnimationController
struct LaunchAnimationController_t825305345;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchAnimationController::.ctor()
extern "C"  void LaunchAnimationController__ctor_m1091741270 (LaunchAnimationController_t825305345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAnimationController::StartAnimation(System.Boolean)
extern "C"  void LaunchAnimationController_StartAnimation_m128849357 (LaunchAnimationController_t825305345 * __this, bool ___restart0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAnimationController::StopAnimation()
extern "C"  void LaunchAnimationController_StopAnimation_m2650424008 (LaunchAnimationController_t825305345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAnimationController::SetAnimationParameters()
extern "C"  void LaunchAnimationController_SetAnimationParameters_m3179582842 (LaunchAnimationController_t825305345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAnimationController::SetAnimMissile()
extern "C"  void LaunchAnimationController_SetAnimMissile_m3878139621 (LaunchAnimationController_t825305345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAnimationController::SetMissileTimeOfDay()
extern "C"  void LaunchAnimationController_SetMissileTimeOfDay_m2193629440 (LaunchAnimationController_t825305345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAnimationController::LoadSkyboxes()
extern "C"  void LaunchAnimationController_LoadSkyboxes_m2496901934 (LaunchAnimationController_t825305345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchAnimationController::ActivateGlitchEffect(System.Boolean)
extern "C"  void LaunchAnimationController_ActivateGlitchEffect_m3199364416 (LaunchAnimationController_t825305345 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
