﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EULAUI
struct  EULAUI_t3168190395  : public MonoBehaviour_t1158329972
{
public:
	// UILabel EULAUI::eulaText
	UILabel_t1795115428 * ___eulaText_2;
	// UnityEngine.BoxCollider EULAUI::uiCollider
	BoxCollider_t22920061 * ___uiCollider_3;
	// System.Single EULAUI::sectionSplit
	float ___sectionSplit_4;

public:
	inline static int32_t get_offset_of_eulaText_2() { return static_cast<int32_t>(offsetof(EULAUI_t3168190395, ___eulaText_2)); }
	inline UILabel_t1795115428 * get_eulaText_2() const { return ___eulaText_2; }
	inline UILabel_t1795115428 ** get_address_of_eulaText_2() { return &___eulaText_2; }
	inline void set_eulaText_2(UILabel_t1795115428 * value)
	{
		___eulaText_2 = value;
		Il2CppCodeGenWriteBarrier(&___eulaText_2, value);
	}

	inline static int32_t get_offset_of_uiCollider_3() { return static_cast<int32_t>(offsetof(EULAUI_t3168190395, ___uiCollider_3)); }
	inline BoxCollider_t22920061 * get_uiCollider_3() const { return ___uiCollider_3; }
	inline BoxCollider_t22920061 ** get_address_of_uiCollider_3() { return &___uiCollider_3; }
	inline void set_uiCollider_3(BoxCollider_t22920061 * value)
	{
		___uiCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___uiCollider_3, value);
	}

	inline static int32_t get_offset_of_sectionSplit_4() { return static_cast<int32_t>(offsetof(EULAUI_t3168190395, ___sectionSplit_4)); }
	inline float get_sectionSplit_4() const { return ___sectionSplit_4; }
	inline float* get_address_of_sectionSplit_4() { return &___sectionSplit_4; }
	inline void set_sectionSplit_4(float value)
	{
		___sectionSplit_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
