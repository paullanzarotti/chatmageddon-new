﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LongNotificationUIScaler
struct LongNotificationUIScaler_t4012764123;

#include "codegen/il2cpp-codegen.h"

// System.Void LongNotificationUIScaler::.ctor()
extern "C"  void LongNotificationUIScaler__ctor_m157665190 (LongNotificationUIScaler_t4012764123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationUIScaler::GlobalUIScale()
extern "C"  void LongNotificationUIScaler_GlobalUIScale_m115914941 (LongNotificationUIScaler_t4012764123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
