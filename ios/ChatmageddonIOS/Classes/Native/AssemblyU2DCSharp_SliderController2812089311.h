﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// SliderNumberLabel[]
struct SliderNumberLabelU5BU5D_t450552843;
// SliderControllerSlider
struct SliderControllerSlider_t2300726312;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_SliderDirection603010170.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SliderController
struct  SliderController_t2812089311  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform SliderController::background
	Transform_t3275118058 * ___background_2;
	// UnityEngine.Transform SliderController::slider
	Transform_t3275118058 * ___slider_3;
	// UnityEngine.Transform SliderController::scaleRoot
	Transform_t3275118058 * ___scaleRoot_4;
	// SliderNumberLabel[] SliderController::labels
	SliderNumberLabelU5BU5D_t450552843* ___labels_5;
	// SliderControllerSlider SliderController::slideComp
	SliderControllerSlider_t2300726312 * ___slideComp_6;
	// SliderDirection SliderController::slideDirection
	int32_t ___slideDirection_7;
	// UILabel SliderController::startingLabel
	UILabel_t1795115428 * ___startingLabel_8;
	// UILabel SliderController::endLabel
	UILabel_t1795115428 * ___endLabel_9;
	// System.Int32 SliderController::startingPoint
	int32_t ___startingPoint_10;
	// System.Int32 SliderController::points
	int32_t ___points_11;
	// System.Int32 SliderController::closestPoint
	int32_t ___closestPoint_12;

public:
	inline static int32_t get_offset_of_background_2() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___background_2)); }
	inline Transform_t3275118058 * get_background_2() const { return ___background_2; }
	inline Transform_t3275118058 ** get_address_of_background_2() { return &___background_2; }
	inline void set_background_2(Transform_t3275118058 * value)
	{
		___background_2 = value;
		Il2CppCodeGenWriteBarrier(&___background_2, value);
	}

	inline static int32_t get_offset_of_slider_3() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___slider_3)); }
	inline Transform_t3275118058 * get_slider_3() const { return ___slider_3; }
	inline Transform_t3275118058 ** get_address_of_slider_3() { return &___slider_3; }
	inline void set_slider_3(Transform_t3275118058 * value)
	{
		___slider_3 = value;
		Il2CppCodeGenWriteBarrier(&___slider_3, value);
	}

	inline static int32_t get_offset_of_scaleRoot_4() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___scaleRoot_4)); }
	inline Transform_t3275118058 * get_scaleRoot_4() const { return ___scaleRoot_4; }
	inline Transform_t3275118058 ** get_address_of_scaleRoot_4() { return &___scaleRoot_4; }
	inline void set_scaleRoot_4(Transform_t3275118058 * value)
	{
		___scaleRoot_4 = value;
		Il2CppCodeGenWriteBarrier(&___scaleRoot_4, value);
	}

	inline static int32_t get_offset_of_labels_5() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___labels_5)); }
	inline SliderNumberLabelU5BU5D_t450552843* get_labels_5() const { return ___labels_5; }
	inline SliderNumberLabelU5BU5D_t450552843** get_address_of_labels_5() { return &___labels_5; }
	inline void set_labels_5(SliderNumberLabelU5BU5D_t450552843* value)
	{
		___labels_5 = value;
		Il2CppCodeGenWriteBarrier(&___labels_5, value);
	}

	inline static int32_t get_offset_of_slideComp_6() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___slideComp_6)); }
	inline SliderControllerSlider_t2300726312 * get_slideComp_6() const { return ___slideComp_6; }
	inline SliderControllerSlider_t2300726312 ** get_address_of_slideComp_6() { return &___slideComp_6; }
	inline void set_slideComp_6(SliderControllerSlider_t2300726312 * value)
	{
		___slideComp_6 = value;
		Il2CppCodeGenWriteBarrier(&___slideComp_6, value);
	}

	inline static int32_t get_offset_of_slideDirection_7() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___slideDirection_7)); }
	inline int32_t get_slideDirection_7() const { return ___slideDirection_7; }
	inline int32_t* get_address_of_slideDirection_7() { return &___slideDirection_7; }
	inline void set_slideDirection_7(int32_t value)
	{
		___slideDirection_7 = value;
	}

	inline static int32_t get_offset_of_startingLabel_8() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___startingLabel_8)); }
	inline UILabel_t1795115428 * get_startingLabel_8() const { return ___startingLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_startingLabel_8() { return &___startingLabel_8; }
	inline void set_startingLabel_8(UILabel_t1795115428 * value)
	{
		___startingLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___startingLabel_8, value);
	}

	inline static int32_t get_offset_of_endLabel_9() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___endLabel_9)); }
	inline UILabel_t1795115428 * get_endLabel_9() const { return ___endLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_endLabel_9() { return &___endLabel_9; }
	inline void set_endLabel_9(UILabel_t1795115428 * value)
	{
		___endLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___endLabel_9, value);
	}

	inline static int32_t get_offset_of_startingPoint_10() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___startingPoint_10)); }
	inline int32_t get_startingPoint_10() const { return ___startingPoint_10; }
	inline int32_t* get_address_of_startingPoint_10() { return &___startingPoint_10; }
	inline void set_startingPoint_10(int32_t value)
	{
		___startingPoint_10 = value;
	}

	inline static int32_t get_offset_of_points_11() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___points_11)); }
	inline int32_t get_points_11() const { return ___points_11; }
	inline int32_t* get_address_of_points_11() { return &___points_11; }
	inline void set_points_11(int32_t value)
	{
		___points_11 = value;
	}

	inline static int32_t get_offset_of_closestPoint_12() { return static_cast<int32_t>(offsetof(SliderController_t2812089311, ___closestPoint_12)); }
	inline int32_t get_closestPoint_12() const { return ___closestPoint_12; }
	inline int32_t* get_address_of_closestPoint_12() { return &___closestPoint_12; }
	inline void set_closestPoint_12(int32_t value)
	{
		___closestPoint_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
