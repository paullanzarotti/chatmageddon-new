﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Nullable`1<System.DateTime>>
struct Action_1_t3053038662;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.Facebook/<extendAccessToken>c__AnonStorey4
struct  U3CextendAccessTokenU3Ec__AnonStorey4_t2614090744  : public Il2CppObject
{
public:
	// System.Action`1<System.Nullable`1<System.DateTime>> Prime31.Facebook/<extendAccessToken>c__AnonStorey4::completionHandler
	Action_1_t3053038662 * ___completionHandler_0;

public:
	inline static int32_t get_offset_of_completionHandler_0() { return static_cast<int32_t>(offsetof(U3CextendAccessTokenU3Ec__AnonStorey4_t2614090744, ___completionHandler_0)); }
	inline Action_1_t3053038662 * get_completionHandler_0() const { return ___completionHandler_0; }
	inline Action_1_t3053038662 ** get_address_of_completionHandler_0() { return &___completionHandler_0; }
	inline void set_completionHandler_0(Action_1_t3053038662 * value)
	{
		___completionHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___completionHandler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
