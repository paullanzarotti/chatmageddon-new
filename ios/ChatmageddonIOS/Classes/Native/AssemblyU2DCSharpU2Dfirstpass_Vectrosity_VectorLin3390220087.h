﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t851691520;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t3873494194;
// UnityEngine.Camera
struct Camera_t189460977;
// LineManager
struct LineManager_t3088676951;
// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo>
struct Dictionary_2_t713084692;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Joins3488019013.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_EndCap4262812865.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vectrosity.VectorLine
struct  VectorLine_t3390220087  : public Il2CppObject
{
public:
	// UnityEngine.UIVertex[] Vectrosity.VectorLine::m_UIVertices
	UIVertexU5BU5D_t3048644023* ___m_UIVertices_0;
	// UnityEngine.UIVertex[] Vectrosity.VectorLine::m_capVertices
	UIVertexU5BU5D_t3048644023* ___m_capVertices_1;
	// UnityEngine.UIVertex[] Vectrosity.VectorLine::m_fillVertices
	UIVertexU5BU5D_t3048644023* ___m_fillVertices_2;
	// UnityEngine.GameObject Vectrosity.VectorLine::m_vectorObject
	GameObject_t1756533147 * ___m_vectorObject_3;
	// UnityEngine.CanvasRenderer Vectrosity.VectorLine::m_canvasRenderer
	CanvasRenderer_t261436805 * ___m_canvasRenderer_4;
	// UnityEngine.CanvasRenderer Vectrosity.VectorLine::m_capRenderer
	CanvasRenderer_t261436805 * ___m_capRenderer_5;
	// UnityEngine.CanvasRenderer Vectrosity.VectorLine::m_fillRenderer
	CanvasRenderer_t261436805 * ___m_fillRenderer_6;
	// UnityEngine.RectTransform Vectrosity.VectorLine::m_rectTransform
	RectTransform_t3349966182 * ___m_rectTransform_7;
	// System.Boolean Vectrosity.VectorLine::m_on2DCanvas
	bool ___m_on2DCanvas_8;
	// System.Int32 Vectrosity.VectorLine::adjustEnd
	int32_t ___adjustEnd_9;
	// UnityEngine.Color32 Vectrosity.VectorLine::m_color
	Color32_t874517518  ___m_color_10;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Vectrosity.VectorLine::m_points2
	List_1_t1612828711 * ___m_points2_11;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Vectrosity.VectorLine::m_points3
	List_1_t1612828712 * ___m_points3_12;
	// System.Int32 Vectrosity.VectorLine::m_pointsCount
	int32_t ___m_pointsCount_13;
	// System.Boolean Vectrosity.VectorLine::m_is2D
	bool ___m_is2D_14;
	// UnityEngine.Vector3[] Vectrosity.VectorLine::m_screenPoints
	Vector3U5BU5D_t1172311765* ___m_screenPoints_15;
	// System.Single[] Vectrosity.VectorLine::m_lineWidths
	SingleU5BU5D_t577127397* ___m_lineWidths_16;
	// System.Single Vectrosity.VectorLine::m_lineWidth
	float ___m_lineWidth_17;
	// System.Single Vectrosity.VectorLine::m_maxWeldDistance
	float ___m_maxWeldDistance_18;
	// System.Single[] Vectrosity.VectorLine::m_distances
	SingleU5BU5D_t577127397* ___m_distances_19;
	// System.String Vectrosity.VectorLine::m_name
	String_t* ___m_name_20;
	// UnityEngine.Material Vectrosity.VectorLine::m_material
	Material_t193706927 * ___m_material_21;
	// System.Int32 Vectrosity.VectorLine::m_fillVertexCount
	int32_t ___m_fillVertexCount_22;
	// System.Boolean Vectrosity.VectorLine::m_active
	bool ___m_active_23;
	// System.Single Vectrosity.VectorLine::m_capLength
	float ___m_capLength_24;
	// System.Boolean Vectrosity.VectorLine::m_smoothWidth
	bool ___m_smoothWidth_25;
	// System.Boolean Vectrosity.VectorLine::m_smoothColor
	bool ___m_smoothColor_26;
	// System.Boolean Vectrosity.VectorLine::m_continuous
	bool ___m_continuous_27;
	// System.Boolean Vectrosity.VectorLine::m_fillObjectSet
	bool ___m_fillObjectSet_28;
	// Vectrosity.Joins Vectrosity.VectorLine::m_joins
	int32_t ___m_joins_29;
	// System.Boolean Vectrosity.VectorLine::m_isPoints
	bool ___m_isPoints_30;
	// System.Boolean Vectrosity.VectorLine::m_isAutoDrawing
	bool ___m_isAutoDrawing_31;
	// System.Int32 Vectrosity.VectorLine::m_drawStart
	int32_t ___m_drawStart_32;
	// System.Int32 Vectrosity.VectorLine::m_drawEnd
	int32_t ___m_drawEnd_33;
	// System.Int32 Vectrosity.VectorLine::m_endPointsUpdate
	int32_t ___m_endPointsUpdate_34;
	// System.Boolean Vectrosity.VectorLine::m_useNormals
	bool ___m_useNormals_35;
	// System.Boolean Vectrosity.VectorLine::m_useTangents
	bool ___m_useTangents_36;
	// System.Boolean Vectrosity.VectorLine::m_normalsCalculated
	bool ___m_normalsCalculated_37;
	// System.Boolean Vectrosity.VectorLine::m_tangentsCalculated
	bool ___m_tangentsCalculated_38;
	// System.Int32 Vectrosity.VectorLine::m_vertexCount
	int32_t ___m_vertexCount_39;
	// Vectrosity.EndCap Vectrosity.VectorLine::m_capType
	int32_t ___m_capType_40;
	// System.String Vectrosity.VectorLine::m_endCap
	String_t* ___m_endCap_41;
	// System.Boolean Vectrosity.VectorLine::m_continuousTexture
	bool ___m_continuousTexture_42;
	// UnityEngine.Transform Vectrosity.VectorLine::m_drawTransform
	Transform_t3275118058 * ___m_drawTransform_43;
	// System.Boolean Vectrosity.VectorLine::m_viewportDraw
	bool ___m_viewportDraw_44;
	// System.Single Vectrosity.VectorLine::m_textureScale
	float ___m_textureScale_45;
	// System.Boolean Vectrosity.VectorLine::m_useTextureScale
	bool ___m_useTextureScale_46;
	// System.Single Vectrosity.VectorLine::m_textureOffset
	float ___m_textureOffset_47;
	// System.Boolean Vectrosity.VectorLine::m_useMatrix
	bool ___m_useMatrix_48;
	// UnityEngine.Matrix4x4 Vectrosity.VectorLine::m_matrix
	Matrix4x4_t2933234003  ___m_matrix_49;
	// System.Boolean Vectrosity.VectorLine::m_collider
	bool ___m_collider_50;
	// System.Boolean Vectrosity.VectorLine::m_trigger
	bool ___m_trigger_51;
	// UnityEngine.PhysicsMaterial2D Vectrosity.VectorLine::m_physicsMaterial
	PhysicsMaterial2D_t851691520 * ___m_physicsMaterial_52;
	// UnityEngine.Mesh Vectrosity.VectorLine::m_mesh
	Mesh_t1356156583 * ___m_mesh_53;
	// System.Int32 Vectrosity.VectorLine::m_canvasID
	int32_t ___m_canvasID_54;

public:
	inline static int32_t get_offset_of_m_UIVertices_0() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_UIVertices_0)); }
	inline UIVertexU5BU5D_t3048644023* get_m_UIVertices_0() const { return ___m_UIVertices_0; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_UIVertices_0() { return &___m_UIVertices_0; }
	inline void set_m_UIVertices_0(UIVertexU5BU5D_t3048644023* value)
	{
		___m_UIVertices_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_UIVertices_0, value);
	}

	inline static int32_t get_offset_of_m_capVertices_1() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_capVertices_1)); }
	inline UIVertexU5BU5D_t3048644023* get_m_capVertices_1() const { return ___m_capVertices_1; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_capVertices_1() { return &___m_capVertices_1; }
	inline void set_m_capVertices_1(UIVertexU5BU5D_t3048644023* value)
	{
		___m_capVertices_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_capVertices_1, value);
	}

	inline static int32_t get_offset_of_m_fillVertices_2() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_fillVertices_2)); }
	inline UIVertexU5BU5D_t3048644023* get_m_fillVertices_2() const { return ___m_fillVertices_2; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_fillVertices_2() { return &___m_fillVertices_2; }
	inline void set_m_fillVertices_2(UIVertexU5BU5D_t3048644023* value)
	{
		___m_fillVertices_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_fillVertices_2, value);
	}

	inline static int32_t get_offset_of_m_vectorObject_3() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_vectorObject_3)); }
	inline GameObject_t1756533147 * get_m_vectorObject_3() const { return ___m_vectorObject_3; }
	inline GameObject_t1756533147 ** get_address_of_m_vectorObject_3() { return &___m_vectorObject_3; }
	inline void set_m_vectorObject_3(GameObject_t1756533147 * value)
	{
		___m_vectorObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_vectorObject_3, value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_4() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_canvasRenderer_4)); }
	inline CanvasRenderer_t261436805 * get_m_canvasRenderer_4() const { return ___m_canvasRenderer_4; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_canvasRenderer_4() { return &___m_canvasRenderer_4; }
	inline void set_m_canvasRenderer_4(CanvasRenderer_t261436805 * value)
	{
		___m_canvasRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvasRenderer_4, value);
	}

	inline static int32_t get_offset_of_m_capRenderer_5() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_capRenderer_5)); }
	inline CanvasRenderer_t261436805 * get_m_capRenderer_5() const { return ___m_capRenderer_5; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_capRenderer_5() { return &___m_capRenderer_5; }
	inline void set_m_capRenderer_5(CanvasRenderer_t261436805 * value)
	{
		___m_capRenderer_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_capRenderer_5, value);
	}

	inline static int32_t get_offset_of_m_fillRenderer_6() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_fillRenderer_6)); }
	inline CanvasRenderer_t261436805 * get_m_fillRenderer_6() const { return ___m_fillRenderer_6; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_fillRenderer_6() { return &___m_fillRenderer_6; }
	inline void set_m_fillRenderer_6(CanvasRenderer_t261436805 * value)
	{
		___m_fillRenderer_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_fillRenderer_6, value);
	}

	inline static int32_t get_offset_of_m_rectTransform_7() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_rectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_rectTransform_7() const { return ___m_rectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_rectTransform_7() { return &___m_rectTransform_7; }
	inline void set_m_rectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_rectTransform_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_rectTransform_7, value);
	}

	inline static int32_t get_offset_of_m_on2DCanvas_8() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_on2DCanvas_8)); }
	inline bool get_m_on2DCanvas_8() const { return ___m_on2DCanvas_8; }
	inline bool* get_address_of_m_on2DCanvas_8() { return &___m_on2DCanvas_8; }
	inline void set_m_on2DCanvas_8(bool value)
	{
		___m_on2DCanvas_8 = value;
	}

	inline static int32_t get_offset_of_adjustEnd_9() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___adjustEnd_9)); }
	inline int32_t get_adjustEnd_9() const { return ___adjustEnd_9; }
	inline int32_t* get_address_of_adjustEnd_9() { return &___adjustEnd_9; }
	inline void set_adjustEnd_9(int32_t value)
	{
		___adjustEnd_9 = value;
	}

	inline static int32_t get_offset_of_m_color_10() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_color_10)); }
	inline Color32_t874517518  get_m_color_10() const { return ___m_color_10; }
	inline Color32_t874517518 * get_address_of_m_color_10() { return &___m_color_10; }
	inline void set_m_color_10(Color32_t874517518  value)
	{
		___m_color_10 = value;
	}

	inline static int32_t get_offset_of_m_points2_11() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_points2_11)); }
	inline List_1_t1612828711 * get_m_points2_11() const { return ___m_points2_11; }
	inline List_1_t1612828711 ** get_address_of_m_points2_11() { return &___m_points2_11; }
	inline void set_m_points2_11(List_1_t1612828711 * value)
	{
		___m_points2_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_points2_11, value);
	}

	inline static int32_t get_offset_of_m_points3_12() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_points3_12)); }
	inline List_1_t1612828712 * get_m_points3_12() const { return ___m_points3_12; }
	inline List_1_t1612828712 ** get_address_of_m_points3_12() { return &___m_points3_12; }
	inline void set_m_points3_12(List_1_t1612828712 * value)
	{
		___m_points3_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_points3_12, value);
	}

	inline static int32_t get_offset_of_m_pointsCount_13() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_pointsCount_13)); }
	inline int32_t get_m_pointsCount_13() const { return ___m_pointsCount_13; }
	inline int32_t* get_address_of_m_pointsCount_13() { return &___m_pointsCount_13; }
	inline void set_m_pointsCount_13(int32_t value)
	{
		___m_pointsCount_13 = value;
	}

	inline static int32_t get_offset_of_m_is2D_14() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_is2D_14)); }
	inline bool get_m_is2D_14() const { return ___m_is2D_14; }
	inline bool* get_address_of_m_is2D_14() { return &___m_is2D_14; }
	inline void set_m_is2D_14(bool value)
	{
		___m_is2D_14 = value;
	}

	inline static int32_t get_offset_of_m_screenPoints_15() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_screenPoints_15)); }
	inline Vector3U5BU5D_t1172311765* get_m_screenPoints_15() const { return ___m_screenPoints_15; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_screenPoints_15() { return &___m_screenPoints_15; }
	inline void set_m_screenPoints_15(Vector3U5BU5D_t1172311765* value)
	{
		___m_screenPoints_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_screenPoints_15, value);
	}

	inline static int32_t get_offset_of_m_lineWidths_16() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_lineWidths_16)); }
	inline SingleU5BU5D_t577127397* get_m_lineWidths_16() const { return ___m_lineWidths_16; }
	inline SingleU5BU5D_t577127397** get_address_of_m_lineWidths_16() { return &___m_lineWidths_16; }
	inline void set_m_lineWidths_16(SingleU5BU5D_t577127397* value)
	{
		___m_lineWidths_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_lineWidths_16, value);
	}

	inline static int32_t get_offset_of_m_lineWidth_17() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_lineWidth_17)); }
	inline float get_m_lineWidth_17() const { return ___m_lineWidth_17; }
	inline float* get_address_of_m_lineWidth_17() { return &___m_lineWidth_17; }
	inline void set_m_lineWidth_17(float value)
	{
		___m_lineWidth_17 = value;
	}

	inline static int32_t get_offset_of_m_maxWeldDistance_18() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_maxWeldDistance_18)); }
	inline float get_m_maxWeldDistance_18() const { return ___m_maxWeldDistance_18; }
	inline float* get_address_of_m_maxWeldDistance_18() { return &___m_maxWeldDistance_18; }
	inline void set_m_maxWeldDistance_18(float value)
	{
		___m_maxWeldDistance_18 = value;
	}

	inline static int32_t get_offset_of_m_distances_19() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_distances_19)); }
	inline SingleU5BU5D_t577127397* get_m_distances_19() const { return ___m_distances_19; }
	inline SingleU5BU5D_t577127397** get_address_of_m_distances_19() { return &___m_distances_19; }
	inline void set_m_distances_19(SingleU5BU5D_t577127397* value)
	{
		___m_distances_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_distances_19, value);
	}

	inline static int32_t get_offset_of_m_name_20() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_name_20)); }
	inline String_t* get_m_name_20() const { return ___m_name_20; }
	inline String_t** get_address_of_m_name_20() { return &___m_name_20; }
	inline void set_m_name_20(String_t* value)
	{
		___m_name_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_name_20, value);
	}

	inline static int32_t get_offset_of_m_material_21() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_material_21)); }
	inline Material_t193706927 * get_m_material_21() const { return ___m_material_21; }
	inline Material_t193706927 ** get_address_of_m_material_21() { return &___m_material_21; }
	inline void set_m_material_21(Material_t193706927 * value)
	{
		___m_material_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_material_21, value);
	}

	inline static int32_t get_offset_of_m_fillVertexCount_22() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_fillVertexCount_22)); }
	inline int32_t get_m_fillVertexCount_22() const { return ___m_fillVertexCount_22; }
	inline int32_t* get_address_of_m_fillVertexCount_22() { return &___m_fillVertexCount_22; }
	inline void set_m_fillVertexCount_22(int32_t value)
	{
		___m_fillVertexCount_22 = value;
	}

	inline static int32_t get_offset_of_m_active_23() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_active_23)); }
	inline bool get_m_active_23() const { return ___m_active_23; }
	inline bool* get_address_of_m_active_23() { return &___m_active_23; }
	inline void set_m_active_23(bool value)
	{
		___m_active_23 = value;
	}

	inline static int32_t get_offset_of_m_capLength_24() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_capLength_24)); }
	inline float get_m_capLength_24() const { return ___m_capLength_24; }
	inline float* get_address_of_m_capLength_24() { return &___m_capLength_24; }
	inline void set_m_capLength_24(float value)
	{
		___m_capLength_24 = value;
	}

	inline static int32_t get_offset_of_m_smoothWidth_25() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_smoothWidth_25)); }
	inline bool get_m_smoothWidth_25() const { return ___m_smoothWidth_25; }
	inline bool* get_address_of_m_smoothWidth_25() { return &___m_smoothWidth_25; }
	inline void set_m_smoothWidth_25(bool value)
	{
		___m_smoothWidth_25 = value;
	}

	inline static int32_t get_offset_of_m_smoothColor_26() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_smoothColor_26)); }
	inline bool get_m_smoothColor_26() const { return ___m_smoothColor_26; }
	inline bool* get_address_of_m_smoothColor_26() { return &___m_smoothColor_26; }
	inline void set_m_smoothColor_26(bool value)
	{
		___m_smoothColor_26 = value;
	}

	inline static int32_t get_offset_of_m_continuous_27() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_continuous_27)); }
	inline bool get_m_continuous_27() const { return ___m_continuous_27; }
	inline bool* get_address_of_m_continuous_27() { return &___m_continuous_27; }
	inline void set_m_continuous_27(bool value)
	{
		___m_continuous_27 = value;
	}

	inline static int32_t get_offset_of_m_fillObjectSet_28() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_fillObjectSet_28)); }
	inline bool get_m_fillObjectSet_28() const { return ___m_fillObjectSet_28; }
	inline bool* get_address_of_m_fillObjectSet_28() { return &___m_fillObjectSet_28; }
	inline void set_m_fillObjectSet_28(bool value)
	{
		___m_fillObjectSet_28 = value;
	}

	inline static int32_t get_offset_of_m_joins_29() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_joins_29)); }
	inline int32_t get_m_joins_29() const { return ___m_joins_29; }
	inline int32_t* get_address_of_m_joins_29() { return &___m_joins_29; }
	inline void set_m_joins_29(int32_t value)
	{
		___m_joins_29 = value;
	}

	inline static int32_t get_offset_of_m_isPoints_30() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_isPoints_30)); }
	inline bool get_m_isPoints_30() const { return ___m_isPoints_30; }
	inline bool* get_address_of_m_isPoints_30() { return &___m_isPoints_30; }
	inline void set_m_isPoints_30(bool value)
	{
		___m_isPoints_30 = value;
	}

	inline static int32_t get_offset_of_m_isAutoDrawing_31() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_isAutoDrawing_31)); }
	inline bool get_m_isAutoDrawing_31() const { return ___m_isAutoDrawing_31; }
	inline bool* get_address_of_m_isAutoDrawing_31() { return &___m_isAutoDrawing_31; }
	inline void set_m_isAutoDrawing_31(bool value)
	{
		___m_isAutoDrawing_31 = value;
	}

	inline static int32_t get_offset_of_m_drawStart_32() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_drawStart_32)); }
	inline int32_t get_m_drawStart_32() const { return ___m_drawStart_32; }
	inline int32_t* get_address_of_m_drawStart_32() { return &___m_drawStart_32; }
	inline void set_m_drawStart_32(int32_t value)
	{
		___m_drawStart_32 = value;
	}

	inline static int32_t get_offset_of_m_drawEnd_33() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_drawEnd_33)); }
	inline int32_t get_m_drawEnd_33() const { return ___m_drawEnd_33; }
	inline int32_t* get_address_of_m_drawEnd_33() { return &___m_drawEnd_33; }
	inline void set_m_drawEnd_33(int32_t value)
	{
		___m_drawEnd_33 = value;
	}

	inline static int32_t get_offset_of_m_endPointsUpdate_34() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_endPointsUpdate_34)); }
	inline int32_t get_m_endPointsUpdate_34() const { return ___m_endPointsUpdate_34; }
	inline int32_t* get_address_of_m_endPointsUpdate_34() { return &___m_endPointsUpdate_34; }
	inline void set_m_endPointsUpdate_34(int32_t value)
	{
		___m_endPointsUpdate_34 = value;
	}

	inline static int32_t get_offset_of_m_useNormals_35() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_useNormals_35)); }
	inline bool get_m_useNormals_35() const { return ___m_useNormals_35; }
	inline bool* get_address_of_m_useNormals_35() { return &___m_useNormals_35; }
	inline void set_m_useNormals_35(bool value)
	{
		___m_useNormals_35 = value;
	}

	inline static int32_t get_offset_of_m_useTangents_36() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_useTangents_36)); }
	inline bool get_m_useTangents_36() const { return ___m_useTangents_36; }
	inline bool* get_address_of_m_useTangents_36() { return &___m_useTangents_36; }
	inline void set_m_useTangents_36(bool value)
	{
		___m_useTangents_36 = value;
	}

	inline static int32_t get_offset_of_m_normalsCalculated_37() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_normalsCalculated_37)); }
	inline bool get_m_normalsCalculated_37() const { return ___m_normalsCalculated_37; }
	inline bool* get_address_of_m_normalsCalculated_37() { return &___m_normalsCalculated_37; }
	inline void set_m_normalsCalculated_37(bool value)
	{
		___m_normalsCalculated_37 = value;
	}

	inline static int32_t get_offset_of_m_tangentsCalculated_38() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_tangentsCalculated_38)); }
	inline bool get_m_tangentsCalculated_38() const { return ___m_tangentsCalculated_38; }
	inline bool* get_address_of_m_tangentsCalculated_38() { return &___m_tangentsCalculated_38; }
	inline void set_m_tangentsCalculated_38(bool value)
	{
		___m_tangentsCalculated_38 = value;
	}

	inline static int32_t get_offset_of_m_vertexCount_39() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_vertexCount_39)); }
	inline int32_t get_m_vertexCount_39() const { return ___m_vertexCount_39; }
	inline int32_t* get_address_of_m_vertexCount_39() { return &___m_vertexCount_39; }
	inline void set_m_vertexCount_39(int32_t value)
	{
		___m_vertexCount_39 = value;
	}

	inline static int32_t get_offset_of_m_capType_40() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_capType_40)); }
	inline int32_t get_m_capType_40() const { return ___m_capType_40; }
	inline int32_t* get_address_of_m_capType_40() { return &___m_capType_40; }
	inline void set_m_capType_40(int32_t value)
	{
		___m_capType_40 = value;
	}

	inline static int32_t get_offset_of_m_endCap_41() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_endCap_41)); }
	inline String_t* get_m_endCap_41() const { return ___m_endCap_41; }
	inline String_t** get_address_of_m_endCap_41() { return &___m_endCap_41; }
	inline void set_m_endCap_41(String_t* value)
	{
		___m_endCap_41 = value;
		Il2CppCodeGenWriteBarrier(&___m_endCap_41, value);
	}

	inline static int32_t get_offset_of_m_continuousTexture_42() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_continuousTexture_42)); }
	inline bool get_m_continuousTexture_42() const { return ___m_continuousTexture_42; }
	inline bool* get_address_of_m_continuousTexture_42() { return &___m_continuousTexture_42; }
	inline void set_m_continuousTexture_42(bool value)
	{
		___m_continuousTexture_42 = value;
	}

	inline static int32_t get_offset_of_m_drawTransform_43() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_drawTransform_43)); }
	inline Transform_t3275118058 * get_m_drawTransform_43() const { return ___m_drawTransform_43; }
	inline Transform_t3275118058 ** get_address_of_m_drawTransform_43() { return &___m_drawTransform_43; }
	inline void set_m_drawTransform_43(Transform_t3275118058 * value)
	{
		___m_drawTransform_43 = value;
		Il2CppCodeGenWriteBarrier(&___m_drawTransform_43, value);
	}

	inline static int32_t get_offset_of_m_viewportDraw_44() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_viewportDraw_44)); }
	inline bool get_m_viewportDraw_44() const { return ___m_viewportDraw_44; }
	inline bool* get_address_of_m_viewportDraw_44() { return &___m_viewportDraw_44; }
	inline void set_m_viewportDraw_44(bool value)
	{
		___m_viewportDraw_44 = value;
	}

	inline static int32_t get_offset_of_m_textureScale_45() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_textureScale_45)); }
	inline float get_m_textureScale_45() const { return ___m_textureScale_45; }
	inline float* get_address_of_m_textureScale_45() { return &___m_textureScale_45; }
	inline void set_m_textureScale_45(float value)
	{
		___m_textureScale_45 = value;
	}

	inline static int32_t get_offset_of_m_useTextureScale_46() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_useTextureScale_46)); }
	inline bool get_m_useTextureScale_46() const { return ___m_useTextureScale_46; }
	inline bool* get_address_of_m_useTextureScale_46() { return &___m_useTextureScale_46; }
	inline void set_m_useTextureScale_46(bool value)
	{
		___m_useTextureScale_46 = value;
	}

	inline static int32_t get_offset_of_m_textureOffset_47() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_textureOffset_47)); }
	inline float get_m_textureOffset_47() const { return ___m_textureOffset_47; }
	inline float* get_address_of_m_textureOffset_47() { return &___m_textureOffset_47; }
	inline void set_m_textureOffset_47(float value)
	{
		___m_textureOffset_47 = value;
	}

	inline static int32_t get_offset_of_m_useMatrix_48() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_useMatrix_48)); }
	inline bool get_m_useMatrix_48() const { return ___m_useMatrix_48; }
	inline bool* get_address_of_m_useMatrix_48() { return &___m_useMatrix_48; }
	inline void set_m_useMatrix_48(bool value)
	{
		___m_useMatrix_48 = value;
	}

	inline static int32_t get_offset_of_m_matrix_49() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_matrix_49)); }
	inline Matrix4x4_t2933234003  get_m_matrix_49() const { return ___m_matrix_49; }
	inline Matrix4x4_t2933234003 * get_address_of_m_matrix_49() { return &___m_matrix_49; }
	inline void set_m_matrix_49(Matrix4x4_t2933234003  value)
	{
		___m_matrix_49 = value;
	}

	inline static int32_t get_offset_of_m_collider_50() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_collider_50)); }
	inline bool get_m_collider_50() const { return ___m_collider_50; }
	inline bool* get_address_of_m_collider_50() { return &___m_collider_50; }
	inline void set_m_collider_50(bool value)
	{
		___m_collider_50 = value;
	}

	inline static int32_t get_offset_of_m_trigger_51() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_trigger_51)); }
	inline bool get_m_trigger_51() const { return ___m_trigger_51; }
	inline bool* get_address_of_m_trigger_51() { return &___m_trigger_51; }
	inline void set_m_trigger_51(bool value)
	{
		___m_trigger_51 = value;
	}

	inline static int32_t get_offset_of_m_physicsMaterial_52() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_physicsMaterial_52)); }
	inline PhysicsMaterial2D_t851691520 * get_m_physicsMaterial_52() const { return ___m_physicsMaterial_52; }
	inline PhysicsMaterial2D_t851691520 ** get_address_of_m_physicsMaterial_52() { return &___m_physicsMaterial_52; }
	inline void set_m_physicsMaterial_52(PhysicsMaterial2D_t851691520 * value)
	{
		___m_physicsMaterial_52 = value;
		Il2CppCodeGenWriteBarrier(&___m_physicsMaterial_52, value);
	}

	inline static int32_t get_offset_of_m_mesh_53() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_mesh_53)); }
	inline Mesh_t1356156583 * get_m_mesh_53() const { return ___m_mesh_53; }
	inline Mesh_t1356156583 ** get_address_of_m_mesh_53() { return &___m_mesh_53; }
	inline void set_m_mesh_53(Mesh_t1356156583 * value)
	{
		___m_mesh_53 = value;
		Il2CppCodeGenWriteBarrier(&___m_mesh_53, value);
	}

	inline static int32_t get_offset_of_m_canvasID_54() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087, ___m_canvasID_54)); }
	inline int32_t get_m_canvasID_54() const { return ___m_canvasID_54; }
	inline int32_t* get_address_of_m_canvasID_54() { return &___m_canvasID_54; }
	inline void set_m_canvasID_54(int32_t value)
	{
		___m_canvasID_54 = value;
	}
};

struct VectorLine_t3390220087_StaticFields
{
public:
	// UnityEngine.Vector3 Vectrosity.VectorLine::v3zero
	Vector3_t2243707580  ___v3zero_55;
	// System.Collections.Generic.List`1<UnityEngine.Canvas> Vectrosity.VectorLine::m_canvases
	List_1_t3873494194 * ___m_canvases_56;
	// System.Collections.Generic.List`1<UnityEngine.Canvas> Vectrosity.VectorLine::m_canvases3D
	List_1_t3873494194 * ___m_canvases3D_57;
	// UnityEngine.Material Vectrosity.VectorLine::defaultMaterial
	Material_t193706927 * ___defaultMaterial_58;
	// UnityEngine.Transform Vectrosity.VectorLine::camTransform
	Transform_t3275118058 * ___camTransform_59;
	// UnityEngine.Camera Vectrosity.VectorLine::cam3D
	Camera_t189460977 * ___cam3D_60;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldPosition
	Vector3_t2243707580  ___oldPosition_61;
	// UnityEngine.Vector3 Vectrosity.VectorLine::oldRotation
	Vector3_t2243707580  ___oldRotation_62;
	// System.Boolean Vectrosity.VectorLine::lineManagerCreated
	bool ___lineManagerCreated_63;
	// LineManager Vectrosity.VectorLine::_lineManager
	LineManager_t3088676951 * ____lineManager_64;
	// System.Collections.Generic.Dictionary`2<System.String,Vectrosity.CapInfo> Vectrosity.VectorLine::capDictionary
	Dictionary_2_t713084692 * ___capDictionary_65;
	// System.String[] Vectrosity.VectorLine::functionNames
	StringU5BU5D_t1642385972* ___functionNames_66;
	// System.Int32 Vectrosity.VectorLine::endianDiff1
	int32_t ___endianDiff1_67;
	// System.Int32 Vectrosity.VectorLine::endianDiff2
	int32_t ___endianDiff2_68;
	// System.Byte[] Vectrosity.VectorLine::byteBlock
	ByteU5BU5D_t3397334013* ___byteBlock_69;

public:
	inline static int32_t get_offset_of_v3zero_55() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___v3zero_55)); }
	inline Vector3_t2243707580  get_v3zero_55() const { return ___v3zero_55; }
	inline Vector3_t2243707580 * get_address_of_v3zero_55() { return &___v3zero_55; }
	inline void set_v3zero_55(Vector3_t2243707580  value)
	{
		___v3zero_55 = value;
	}

	inline static int32_t get_offset_of_m_canvases_56() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___m_canvases_56)); }
	inline List_1_t3873494194 * get_m_canvases_56() const { return ___m_canvases_56; }
	inline List_1_t3873494194 ** get_address_of_m_canvases_56() { return &___m_canvases_56; }
	inline void set_m_canvases_56(List_1_t3873494194 * value)
	{
		___m_canvases_56 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvases_56, value);
	}

	inline static int32_t get_offset_of_m_canvases3D_57() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___m_canvases3D_57)); }
	inline List_1_t3873494194 * get_m_canvases3D_57() const { return ___m_canvases3D_57; }
	inline List_1_t3873494194 ** get_address_of_m_canvases3D_57() { return &___m_canvases3D_57; }
	inline void set_m_canvases3D_57(List_1_t3873494194 * value)
	{
		___m_canvases3D_57 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvases3D_57, value);
	}

	inline static int32_t get_offset_of_defaultMaterial_58() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___defaultMaterial_58)); }
	inline Material_t193706927 * get_defaultMaterial_58() const { return ___defaultMaterial_58; }
	inline Material_t193706927 ** get_address_of_defaultMaterial_58() { return &___defaultMaterial_58; }
	inline void set_defaultMaterial_58(Material_t193706927 * value)
	{
		___defaultMaterial_58 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMaterial_58, value);
	}

	inline static int32_t get_offset_of_camTransform_59() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___camTransform_59)); }
	inline Transform_t3275118058 * get_camTransform_59() const { return ___camTransform_59; }
	inline Transform_t3275118058 ** get_address_of_camTransform_59() { return &___camTransform_59; }
	inline void set_camTransform_59(Transform_t3275118058 * value)
	{
		___camTransform_59 = value;
		Il2CppCodeGenWriteBarrier(&___camTransform_59, value);
	}

	inline static int32_t get_offset_of_cam3D_60() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___cam3D_60)); }
	inline Camera_t189460977 * get_cam3D_60() const { return ___cam3D_60; }
	inline Camera_t189460977 ** get_address_of_cam3D_60() { return &___cam3D_60; }
	inline void set_cam3D_60(Camera_t189460977 * value)
	{
		___cam3D_60 = value;
		Il2CppCodeGenWriteBarrier(&___cam3D_60, value);
	}

	inline static int32_t get_offset_of_oldPosition_61() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___oldPosition_61)); }
	inline Vector3_t2243707580  get_oldPosition_61() const { return ___oldPosition_61; }
	inline Vector3_t2243707580 * get_address_of_oldPosition_61() { return &___oldPosition_61; }
	inline void set_oldPosition_61(Vector3_t2243707580  value)
	{
		___oldPosition_61 = value;
	}

	inline static int32_t get_offset_of_oldRotation_62() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___oldRotation_62)); }
	inline Vector3_t2243707580  get_oldRotation_62() const { return ___oldRotation_62; }
	inline Vector3_t2243707580 * get_address_of_oldRotation_62() { return &___oldRotation_62; }
	inline void set_oldRotation_62(Vector3_t2243707580  value)
	{
		___oldRotation_62 = value;
	}

	inline static int32_t get_offset_of_lineManagerCreated_63() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___lineManagerCreated_63)); }
	inline bool get_lineManagerCreated_63() const { return ___lineManagerCreated_63; }
	inline bool* get_address_of_lineManagerCreated_63() { return &___lineManagerCreated_63; }
	inline void set_lineManagerCreated_63(bool value)
	{
		___lineManagerCreated_63 = value;
	}

	inline static int32_t get_offset_of__lineManager_64() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ____lineManager_64)); }
	inline LineManager_t3088676951 * get__lineManager_64() const { return ____lineManager_64; }
	inline LineManager_t3088676951 ** get_address_of__lineManager_64() { return &____lineManager_64; }
	inline void set__lineManager_64(LineManager_t3088676951 * value)
	{
		____lineManager_64 = value;
		Il2CppCodeGenWriteBarrier(&____lineManager_64, value);
	}

	inline static int32_t get_offset_of_capDictionary_65() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___capDictionary_65)); }
	inline Dictionary_2_t713084692 * get_capDictionary_65() const { return ___capDictionary_65; }
	inline Dictionary_2_t713084692 ** get_address_of_capDictionary_65() { return &___capDictionary_65; }
	inline void set_capDictionary_65(Dictionary_2_t713084692 * value)
	{
		___capDictionary_65 = value;
		Il2CppCodeGenWriteBarrier(&___capDictionary_65, value);
	}

	inline static int32_t get_offset_of_functionNames_66() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___functionNames_66)); }
	inline StringU5BU5D_t1642385972* get_functionNames_66() const { return ___functionNames_66; }
	inline StringU5BU5D_t1642385972** get_address_of_functionNames_66() { return &___functionNames_66; }
	inline void set_functionNames_66(StringU5BU5D_t1642385972* value)
	{
		___functionNames_66 = value;
		Il2CppCodeGenWriteBarrier(&___functionNames_66, value);
	}

	inline static int32_t get_offset_of_endianDiff1_67() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___endianDiff1_67)); }
	inline int32_t get_endianDiff1_67() const { return ___endianDiff1_67; }
	inline int32_t* get_address_of_endianDiff1_67() { return &___endianDiff1_67; }
	inline void set_endianDiff1_67(int32_t value)
	{
		___endianDiff1_67 = value;
	}

	inline static int32_t get_offset_of_endianDiff2_68() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___endianDiff2_68)); }
	inline int32_t get_endianDiff2_68() const { return ___endianDiff2_68; }
	inline int32_t* get_address_of_endianDiff2_68() { return &___endianDiff2_68; }
	inline void set_endianDiff2_68(int32_t value)
	{
		___endianDiff2_68 = value;
	}

	inline static int32_t get_offset_of_byteBlock_69() { return static_cast<int32_t>(offsetof(VectorLine_t3390220087_StaticFields, ___byteBlock_69)); }
	inline ByteU5BU5D_t3397334013* get_byteBlock_69() const { return ___byteBlock_69; }
	inline ByteU5BU5D_t3397334013** get_address_of_byteBlock_69() { return &___byteBlock_69; }
	inline void set_byteBlock_69(ByteU5BU5D_t3397334013* value)
	{
		___byteBlock_69 = value;
		Il2CppCodeGenWriteBarrier(&___byteBlock_69, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
