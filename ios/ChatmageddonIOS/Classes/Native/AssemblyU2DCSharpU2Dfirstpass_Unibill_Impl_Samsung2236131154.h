﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.IRawSamsungAppsBillingService
struct IRawSamsungAppsBillingService_t1919646871;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.SamsungAppsBillingService
struct  SamsungAppsBillingService_t2236131154  : public Il2CppObject
{
public:
	// Unibill.Impl.IBillingServiceCallback Unibill.Impl.SamsungAppsBillingService::callback
	Il2CppObject * ___callback_0;
	// Unibill.Impl.ProductIdRemapper Unibill.Impl.SamsungAppsBillingService::remapper
	ProductIdRemapper_t3313438456 * ___remapper_1;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.SamsungAppsBillingService::config
	UnibillConfiguration_t2915611853 * ___config_2;
	// Unibill.Impl.IRawSamsungAppsBillingService Unibill.Impl.SamsungAppsBillingService::rawSamsung
	Il2CppObject * ___rawSamsung_3;
	// Uniject.ILogger Unibill.Impl.SamsungAppsBillingService::logger
	Il2CppObject * ___logger_4;
	// System.Collections.Generic.HashSet`1<System.String> Unibill.Impl.SamsungAppsBillingService::unknownSamsungProducts
	HashSet_1_t362681087 * ___unknownSamsungProducts_5;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(SamsungAppsBillingService_t2236131154, ___callback_0)); }
	inline Il2CppObject * get_callback_0() const { return ___callback_0; }
	inline Il2CppObject ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Il2CppObject * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___callback_0, value);
	}

	inline static int32_t get_offset_of_remapper_1() { return static_cast<int32_t>(offsetof(SamsungAppsBillingService_t2236131154, ___remapper_1)); }
	inline ProductIdRemapper_t3313438456 * get_remapper_1() const { return ___remapper_1; }
	inline ProductIdRemapper_t3313438456 ** get_address_of_remapper_1() { return &___remapper_1; }
	inline void set_remapper_1(ProductIdRemapper_t3313438456 * value)
	{
		___remapper_1 = value;
		Il2CppCodeGenWriteBarrier(&___remapper_1, value);
	}

	inline static int32_t get_offset_of_config_2() { return static_cast<int32_t>(offsetof(SamsungAppsBillingService_t2236131154, ___config_2)); }
	inline UnibillConfiguration_t2915611853 * get_config_2() const { return ___config_2; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_config_2() { return &___config_2; }
	inline void set_config_2(UnibillConfiguration_t2915611853 * value)
	{
		___config_2 = value;
		Il2CppCodeGenWriteBarrier(&___config_2, value);
	}

	inline static int32_t get_offset_of_rawSamsung_3() { return static_cast<int32_t>(offsetof(SamsungAppsBillingService_t2236131154, ___rawSamsung_3)); }
	inline Il2CppObject * get_rawSamsung_3() const { return ___rawSamsung_3; }
	inline Il2CppObject ** get_address_of_rawSamsung_3() { return &___rawSamsung_3; }
	inline void set_rawSamsung_3(Il2CppObject * value)
	{
		___rawSamsung_3 = value;
		Il2CppCodeGenWriteBarrier(&___rawSamsung_3, value);
	}

	inline static int32_t get_offset_of_logger_4() { return static_cast<int32_t>(offsetof(SamsungAppsBillingService_t2236131154, ___logger_4)); }
	inline Il2CppObject * get_logger_4() const { return ___logger_4; }
	inline Il2CppObject ** get_address_of_logger_4() { return &___logger_4; }
	inline void set_logger_4(Il2CppObject * value)
	{
		___logger_4 = value;
		Il2CppCodeGenWriteBarrier(&___logger_4, value);
	}

	inline static int32_t get_offset_of_unknownSamsungProducts_5() { return static_cast<int32_t>(offsetof(SamsungAppsBillingService_t2236131154, ___unknownSamsungProducts_5)); }
	inline HashSet_1_t362681087 * get_unknownSamsungProducts_5() const { return ___unknownSamsungProducts_5; }
	inline HashSet_1_t362681087 ** get_address_of_unknownSamsungProducts_5() { return &___unknownSamsungProducts_5; }
	inline void set_unknownSamsungProducts_5(HashSet_1_t362681087 * value)
	{
		___unknownSamsungProducts_5 = value;
		Il2CppCodeGenWriteBarrier(&___unknownSamsungProducts_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
