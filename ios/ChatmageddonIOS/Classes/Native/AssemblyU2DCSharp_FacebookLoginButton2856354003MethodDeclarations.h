﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookLoginButton
struct FacebookLoginButton_t2856354003;

#include "codegen/il2cpp-codegen.h"

// System.Void FacebookLoginButton::.ctor()
extern "C"  void FacebookLoginButton__ctor_m2779500768 (FacebookLoginButton_t2856354003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookLoginButton::OnButtonClick()
extern "C"  void FacebookLoginButton_OnButtonClick_m877935063 (FacebookLoginButton_t2856354003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
