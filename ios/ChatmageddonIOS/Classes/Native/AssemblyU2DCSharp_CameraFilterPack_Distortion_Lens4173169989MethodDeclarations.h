﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Lens
struct CameraFilterPack_Distortion_Lens_t4173169989;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Lens::.ctor()
extern "C"  void CameraFilterPack_Distortion_Lens__ctor_m3401534114 (CameraFilterPack_Distortion_Lens_t4173169989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Lens::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Lens_get_material_m2968028373 (CameraFilterPack_Distortion_Lens_t4173169989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::Start()
extern "C"  void CameraFilterPack_Distortion_Lens_Start_m3613634386 (CameraFilterPack_Distortion_Lens_t4173169989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Lens_OnRenderImage_m1413875586 (CameraFilterPack_Distortion_Lens_t4173169989 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Lens_OnValidate_m2156512379 (CameraFilterPack_Distortion_Lens_t4173169989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::Update()
extern "C"  void CameraFilterPack_Distortion_Lens_Update_m1983695465 (CameraFilterPack_Distortion_Lens_t4173169989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Lens_OnDisable_m2729326221 (CameraFilterPack_Distortion_Lens_t4173169989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
