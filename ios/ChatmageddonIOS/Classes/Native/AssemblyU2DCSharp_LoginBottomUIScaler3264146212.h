﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginBottomUIScaler
struct  LoginBottomUIScaler_t3264146212  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.BoxCollider LoginBottomUIScaler::loginButtonCollider
	BoxCollider_t22920061 * ___loginButtonCollider_14;
	// UISprite LoginBottomUIScaler::loginButtonBackground
	UISprite_t603616735 * ___loginButtonBackground_15;
	// UnityEngine.BoxCollider LoginBottomUIScaler::closeUICollider
	BoxCollider_t22920061 * ___closeUICollider_16;
	// UnityEngine.BoxCollider LoginBottomUIScaler::usernameButtonCollider
	BoxCollider_t22920061 * ___usernameButtonCollider_17;
	// UISprite LoginBottomUIScaler::usernameButtonBackground
	UISprite_t603616735 * ___usernameButtonBackground_18;
	// UISprite LoginBottomUIScaler::usernameButtonDivider
	UISprite_t603616735 * ___usernameButtonDivider_19;
	// UISprite LoginBottomUIScaler::usernameButtonTopDivider
	UISprite_t603616735 * ___usernameButtonTopDivider_20;
	// UILabel LoginBottomUIScaler::usernameButtonLabel
	UILabel_t1795115428 * ___usernameButtonLabel_21;
	// UnityEngine.BoxCollider LoginBottomUIScaler::passwordButtonCollider
	BoxCollider_t22920061 * ___passwordButtonCollider_22;
	// UISprite LoginBottomUIScaler::passwordButtonBackground
	UISprite_t603616735 * ___passwordButtonBackground_23;
	// UISprite LoginBottomUIScaler::passwordButtonDivider
	UISprite_t603616735 * ___passwordButtonDivider_24;
	// UILabel LoginBottomUIScaler::passwordButtonLabel
	UILabel_t1795115428 * ___passwordButtonLabel_25;
	// UISprite LoginBottomUIScaler::regBackground
	UISprite_t603616735 * ___regBackground_26;
	// UISprite LoginBottomUIScaler::regTitleDivider
	UISprite_t603616735 * ___regTitleDivider_27;
	// UnityEngine.Transform LoginBottomUIScaler::regNavBackButton
	Transform_t3275118058 * ___regNavBackButton_28;
	// UnityEngine.Transform LoginBottomUIScaler::regNavForwardButton
	Transform_t3275118058 * ___regNavForwardButton_29;
	// UISprite LoginBottomUIScaler::regYNTopDivider
	UISprite_t603616735 * ___regYNTopDivider_30;
	// UISprite LoginBottomUIScaler::regYNMiddleDivider
	UISprite_t603616735 * ___regYNMiddleDivider_31;
	// UISprite LoginBottomUIScaler::regYNBottomDivider
	UISprite_t603616735 * ___regYNBottomDivider_32;
	// UnityEngine.BoxCollider LoginBottomUIScaler::regYNContinueButtonCollider
	BoxCollider_t22920061 * ___regYNContinueButtonCollider_33;
	// UISprite LoginBottomUIScaler::regYNContinueButton
	UISprite_t603616735 * ___regYNContinueButton_34;
	// UnityEngine.BoxCollider LoginBottomUIScaler::regCountryPickerCollider
	BoxCollider_t22920061 * ___regCountryPickerCollider_35;
	// UISprite LoginBottomUIScaler::regCountryPickerButton
	UISprite_t603616735 * ___regCountryPickerButton_36;
	// UILabel LoginBottomUIScaler::regCountryPickerLabel
	UILabel_t1795115428 * ___regCountryPickerLabel_37;
	// UILabel LoginBottomUIScaler::regCountryCodeLabel
	UILabel_t1795115428 * ___regCountryCodeLabel_38;
	// UISprite LoginBottomUIScaler::regCountryCodeSeperator
	UISprite_t603616735 * ___regCountryCodeSeperator_39;
	// UnityEngine.BoxCollider LoginBottomUIScaler::regYNPhoneNumberCollider
	BoxCollider_t22920061 * ___regYNPhoneNumberCollider_40;
	// UILabel LoginBottomUIScaler::regYNPhoneNumberLabel
	UILabel_t1795115428 * ___regYNPhoneNumberLabel_41;

public:
	inline static int32_t get_offset_of_loginButtonCollider_14() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___loginButtonCollider_14)); }
	inline BoxCollider_t22920061 * get_loginButtonCollider_14() const { return ___loginButtonCollider_14; }
	inline BoxCollider_t22920061 ** get_address_of_loginButtonCollider_14() { return &___loginButtonCollider_14; }
	inline void set_loginButtonCollider_14(BoxCollider_t22920061 * value)
	{
		___loginButtonCollider_14 = value;
		Il2CppCodeGenWriteBarrier(&___loginButtonCollider_14, value);
	}

	inline static int32_t get_offset_of_loginButtonBackground_15() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___loginButtonBackground_15)); }
	inline UISprite_t603616735 * get_loginButtonBackground_15() const { return ___loginButtonBackground_15; }
	inline UISprite_t603616735 ** get_address_of_loginButtonBackground_15() { return &___loginButtonBackground_15; }
	inline void set_loginButtonBackground_15(UISprite_t603616735 * value)
	{
		___loginButtonBackground_15 = value;
		Il2CppCodeGenWriteBarrier(&___loginButtonBackground_15, value);
	}

	inline static int32_t get_offset_of_closeUICollider_16() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___closeUICollider_16)); }
	inline BoxCollider_t22920061 * get_closeUICollider_16() const { return ___closeUICollider_16; }
	inline BoxCollider_t22920061 ** get_address_of_closeUICollider_16() { return &___closeUICollider_16; }
	inline void set_closeUICollider_16(BoxCollider_t22920061 * value)
	{
		___closeUICollider_16 = value;
		Il2CppCodeGenWriteBarrier(&___closeUICollider_16, value);
	}

	inline static int32_t get_offset_of_usernameButtonCollider_17() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___usernameButtonCollider_17)); }
	inline BoxCollider_t22920061 * get_usernameButtonCollider_17() const { return ___usernameButtonCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_usernameButtonCollider_17() { return &___usernameButtonCollider_17; }
	inline void set_usernameButtonCollider_17(BoxCollider_t22920061 * value)
	{
		___usernameButtonCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___usernameButtonCollider_17, value);
	}

	inline static int32_t get_offset_of_usernameButtonBackground_18() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___usernameButtonBackground_18)); }
	inline UISprite_t603616735 * get_usernameButtonBackground_18() const { return ___usernameButtonBackground_18; }
	inline UISprite_t603616735 ** get_address_of_usernameButtonBackground_18() { return &___usernameButtonBackground_18; }
	inline void set_usernameButtonBackground_18(UISprite_t603616735 * value)
	{
		___usernameButtonBackground_18 = value;
		Il2CppCodeGenWriteBarrier(&___usernameButtonBackground_18, value);
	}

	inline static int32_t get_offset_of_usernameButtonDivider_19() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___usernameButtonDivider_19)); }
	inline UISprite_t603616735 * get_usernameButtonDivider_19() const { return ___usernameButtonDivider_19; }
	inline UISprite_t603616735 ** get_address_of_usernameButtonDivider_19() { return &___usernameButtonDivider_19; }
	inline void set_usernameButtonDivider_19(UISprite_t603616735 * value)
	{
		___usernameButtonDivider_19 = value;
		Il2CppCodeGenWriteBarrier(&___usernameButtonDivider_19, value);
	}

	inline static int32_t get_offset_of_usernameButtonTopDivider_20() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___usernameButtonTopDivider_20)); }
	inline UISprite_t603616735 * get_usernameButtonTopDivider_20() const { return ___usernameButtonTopDivider_20; }
	inline UISprite_t603616735 ** get_address_of_usernameButtonTopDivider_20() { return &___usernameButtonTopDivider_20; }
	inline void set_usernameButtonTopDivider_20(UISprite_t603616735 * value)
	{
		___usernameButtonTopDivider_20 = value;
		Il2CppCodeGenWriteBarrier(&___usernameButtonTopDivider_20, value);
	}

	inline static int32_t get_offset_of_usernameButtonLabel_21() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___usernameButtonLabel_21)); }
	inline UILabel_t1795115428 * get_usernameButtonLabel_21() const { return ___usernameButtonLabel_21; }
	inline UILabel_t1795115428 ** get_address_of_usernameButtonLabel_21() { return &___usernameButtonLabel_21; }
	inline void set_usernameButtonLabel_21(UILabel_t1795115428 * value)
	{
		___usernameButtonLabel_21 = value;
		Il2CppCodeGenWriteBarrier(&___usernameButtonLabel_21, value);
	}

	inline static int32_t get_offset_of_passwordButtonCollider_22() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___passwordButtonCollider_22)); }
	inline BoxCollider_t22920061 * get_passwordButtonCollider_22() const { return ___passwordButtonCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_passwordButtonCollider_22() { return &___passwordButtonCollider_22; }
	inline void set_passwordButtonCollider_22(BoxCollider_t22920061 * value)
	{
		___passwordButtonCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___passwordButtonCollider_22, value);
	}

	inline static int32_t get_offset_of_passwordButtonBackground_23() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___passwordButtonBackground_23)); }
	inline UISprite_t603616735 * get_passwordButtonBackground_23() const { return ___passwordButtonBackground_23; }
	inline UISprite_t603616735 ** get_address_of_passwordButtonBackground_23() { return &___passwordButtonBackground_23; }
	inline void set_passwordButtonBackground_23(UISprite_t603616735 * value)
	{
		___passwordButtonBackground_23 = value;
		Il2CppCodeGenWriteBarrier(&___passwordButtonBackground_23, value);
	}

	inline static int32_t get_offset_of_passwordButtonDivider_24() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___passwordButtonDivider_24)); }
	inline UISprite_t603616735 * get_passwordButtonDivider_24() const { return ___passwordButtonDivider_24; }
	inline UISprite_t603616735 ** get_address_of_passwordButtonDivider_24() { return &___passwordButtonDivider_24; }
	inline void set_passwordButtonDivider_24(UISprite_t603616735 * value)
	{
		___passwordButtonDivider_24 = value;
		Il2CppCodeGenWriteBarrier(&___passwordButtonDivider_24, value);
	}

	inline static int32_t get_offset_of_passwordButtonLabel_25() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___passwordButtonLabel_25)); }
	inline UILabel_t1795115428 * get_passwordButtonLabel_25() const { return ___passwordButtonLabel_25; }
	inline UILabel_t1795115428 ** get_address_of_passwordButtonLabel_25() { return &___passwordButtonLabel_25; }
	inline void set_passwordButtonLabel_25(UILabel_t1795115428 * value)
	{
		___passwordButtonLabel_25 = value;
		Il2CppCodeGenWriteBarrier(&___passwordButtonLabel_25, value);
	}

	inline static int32_t get_offset_of_regBackground_26() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regBackground_26)); }
	inline UISprite_t603616735 * get_regBackground_26() const { return ___regBackground_26; }
	inline UISprite_t603616735 ** get_address_of_regBackground_26() { return &___regBackground_26; }
	inline void set_regBackground_26(UISprite_t603616735 * value)
	{
		___regBackground_26 = value;
		Il2CppCodeGenWriteBarrier(&___regBackground_26, value);
	}

	inline static int32_t get_offset_of_regTitleDivider_27() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regTitleDivider_27)); }
	inline UISprite_t603616735 * get_regTitleDivider_27() const { return ___regTitleDivider_27; }
	inline UISprite_t603616735 ** get_address_of_regTitleDivider_27() { return &___regTitleDivider_27; }
	inline void set_regTitleDivider_27(UISprite_t603616735 * value)
	{
		___regTitleDivider_27 = value;
		Il2CppCodeGenWriteBarrier(&___regTitleDivider_27, value);
	}

	inline static int32_t get_offset_of_regNavBackButton_28() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regNavBackButton_28)); }
	inline Transform_t3275118058 * get_regNavBackButton_28() const { return ___regNavBackButton_28; }
	inline Transform_t3275118058 ** get_address_of_regNavBackButton_28() { return &___regNavBackButton_28; }
	inline void set_regNavBackButton_28(Transform_t3275118058 * value)
	{
		___regNavBackButton_28 = value;
		Il2CppCodeGenWriteBarrier(&___regNavBackButton_28, value);
	}

	inline static int32_t get_offset_of_regNavForwardButton_29() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regNavForwardButton_29)); }
	inline Transform_t3275118058 * get_regNavForwardButton_29() const { return ___regNavForwardButton_29; }
	inline Transform_t3275118058 ** get_address_of_regNavForwardButton_29() { return &___regNavForwardButton_29; }
	inline void set_regNavForwardButton_29(Transform_t3275118058 * value)
	{
		___regNavForwardButton_29 = value;
		Il2CppCodeGenWriteBarrier(&___regNavForwardButton_29, value);
	}

	inline static int32_t get_offset_of_regYNTopDivider_30() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regYNTopDivider_30)); }
	inline UISprite_t603616735 * get_regYNTopDivider_30() const { return ___regYNTopDivider_30; }
	inline UISprite_t603616735 ** get_address_of_regYNTopDivider_30() { return &___regYNTopDivider_30; }
	inline void set_regYNTopDivider_30(UISprite_t603616735 * value)
	{
		___regYNTopDivider_30 = value;
		Il2CppCodeGenWriteBarrier(&___regYNTopDivider_30, value);
	}

	inline static int32_t get_offset_of_regYNMiddleDivider_31() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regYNMiddleDivider_31)); }
	inline UISprite_t603616735 * get_regYNMiddleDivider_31() const { return ___regYNMiddleDivider_31; }
	inline UISprite_t603616735 ** get_address_of_regYNMiddleDivider_31() { return &___regYNMiddleDivider_31; }
	inline void set_regYNMiddleDivider_31(UISprite_t603616735 * value)
	{
		___regYNMiddleDivider_31 = value;
		Il2CppCodeGenWriteBarrier(&___regYNMiddleDivider_31, value);
	}

	inline static int32_t get_offset_of_regYNBottomDivider_32() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regYNBottomDivider_32)); }
	inline UISprite_t603616735 * get_regYNBottomDivider_32() const { return ___regYNBottomDivider_32; }
	inline UISprite_t603616735 ** get_address_of_regYNBottomDivider_32() { return &___regYNBottomDivider_32; }
	inline void set_regYNBottomDivider_32(UISprite_t603616735 * value)
	{
		___regYNBottomDivider_32 = value;
		Il2CppCodeGenWriteBarrier(&___regYNBottomDivider_32, value);
	}

	inline static int32_t get_offset_of_regYNContinueButtonCollider_33() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regYNContinueButtonCollider_33)); }
	inline BoxCollider_t22920061 * get_regYNContinueButtonCollider_33() const { return ___regYNContinueButtonCollider_33; }
	inline BoxCollider_t22920061 ** get_address_of_regYNContinueButtonCollider_33() { return &___regYNContinueButtonCollider_33; }
	inline void set_regYNContinueButtonCollider_33(BoxCollider_t22920061 * value)
	{
		___regYNContinueButtonCollider_33 = value;
		Il2CppCodeGenWriteBarrier(&___regYNContinueButtonCollider_33, value);
	}

	inline static int32_t get_offset_of_regYNContinueButton_34() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regYNContinueButton_34)); }
	inline UISprite_t603616735 * get_regYNContinueButton_34() const { return ___regYNContinueButton_34; }
	inline UISprite_t603616735 ** get_address_of_regYNContinueButton_34() { return &___regYNContinueButton_34; }
	inline void set_regYNContinueButton_34(UISprite_t603616735 * value)
	{
		___regYNContinueButton_34 = value;
		Il2CppCodeGenWriteBarrier(&___regYNContinueButton_34, value);
	}

	inline static int32_t get_offset_of_regCountryPickerCollider_35() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regCountryPickerCollider_35)); }
	inline BoxCollider_t22920061 * get_regCountryPickerCollider_35() const { return ___regCountryPickerCollider_35; }
	inline BoxCollider_t22920061 ** get_address_of_regCountryPickerCollider_35() { return &___regCountryPickerCollider_35; }
	inline void set_regCountryPickerCollider_35(BoxCollider_t22920061 * value)
	{
		___regCountryPickerCollider_35 = value;
		Il2CppCodeGenWriteBarrier(&___regCountryPickerCollider_35, value);
	}

	inline static int32_t get_offset_of_regCountryPickerButton_36() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regCountryPickerButton_36)); }
	inline UISprite_t603616735 * get_regCountryPickerButton_36() const { return ___regCountryPickerButton_36; }
	inline UISprite_t603616735 ** get_address_of_regCountryPickerButton_36() { return &___regCountryPickerButton_36; }
	inline void set_regCountryPickerButton_36(UISprite_t603616735 * value)
	{
		___regCountryPickerButton_36 = value;
		Il2CppCodeGenWriteBarrier(&___regCountryPickerButton_36, value);
	}

	inline static int32_t get_offset_of_regCountryPickerLabel_37() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regCountryPickerLabel_37)); }
	inline UILabel_t1795115428 * get_regCountryPickerLabel_37() const { return ___regCountryPickerLabel_37; }
	inline UILabel_t1795115428 ** get_address_of_regCountryPickerLabel_37() { return &___regCountryPickerLabel_37; }
	inline void set_regCountryPickerLabel_37(UILabel_t1795115428 * value)
	{
		___regCountryPickerLabel_37 = value;
		Il2CppCodeGenWriteBarrier(&___regCountryPickerLabel_37, value);
	}

	inline static int32_t get_offset_of_regCountryCodeLabel_38() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regCountryCodeLabel_38)); }
	inline UILabel_t1795115428 * get_regCountryCodeLabel_38() const { return ___regCountryCodeLabel_38; }
	inline UILabel_t1795115428 ** get_address_of_regCountryCodeLabel_38() { return &___regCountryCodeLabel_38; }
	inline void set_regCountryCodeLabel_38(UILabel_t1795115428 * value)
	{
		___regCountryCodeLabel_38 = value;
		Il2CppCodeGenWriteBarrier(&___regCountryCodeLabel_38, value);
	}

	inline static int32_t get_offset_of_regCountryCodeSeperator_39() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regCountryCodeSeperator_39)); }
	inline UISprite_t603616735 * get_regCountryCodeSeperator_39() const { return ___regCountryCodeSeperator_39; }
	inline UISprite_t603616735 ** get_address_of_regCountryCodeSeperator_39() { return &___regCountryCodeSeperator_39; }
	inline void set_regCountryCodeSeperator_39(UISprite_t603616735 * value)
	{
		___regCountryCodeSeperator_39 = value;
		Il2CppCodeGenWriteBarrier(&___regCountryCodeSeperator_39, value);
	}

	inline static int32_t get_offset_of_regYNPhoneNumberCollider_40() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regYNPhoneNumberCollider_40)); }
	inline BoxCollider_t22920061 * get_regYNPhoneNumberCollider_40() const { return ___regYNPhoneNumberCollider_40; }
	inline BoxCollider_t22920061 ** get_address_of_regYNPhoneNumberCollider_40() { return &___regYNPhoneNumberCollider_40; }
	inline void set_regYNPhoneNumberCollider_40(BoxCollider_t22920061 * value)
	{
		___regYNPhoneNumberCollider_40 = value;
		Il2CppCodeGenWriteBarrier(&___regYNPhoneNumberCollider_40, value);
	}

	inline static int32_t get_offset_of_regYNPhoneNumberLabel_41() { return static_cast<int32_t>(offsetof(LoginBottomUIScaler_t3264146212, ___regYNPhoneNumberLabel_41)); }
	inline UILabel_t1795115428 * get_regYNPhoneNumberLabel_41() const { return ___regYNPhoneNumberLabel_41; }
	inline UILabel_t1795115428 ** get_address_of_regYNPhoneNumberLabel_41() { return &___regYNPhoneNumberLabel_41; }
	inline void set_regYNPhoneNumberLabel_41(UILabel_t1795115428 * value)
	{
		___regYNPhoneNumberLabel_41 = value;
		Il2CppCodeGenWriteBarrier(&___regYNPhoneNumberLabel_41, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
