﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraBinding/<takeScreenShot>c__Iterator0
struct U3CtakeScreenShotU3Ec__Iterator0_t72106313;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraBinding/<takeScreenShot>c__Iterator0::.ctor()
extern "C"  void U3CtakeScreenShotU3Ec__Iterator0__ctor_m3557216462 (U3CtakeScreenShotU3Ec__Iterator0_t72106313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding/<takeScreenShot>c__Iterator0::MoveNext()
extern "C"  bool U3CtakeScreenShotU3Ec__Iterator0_MoveNext_m1133896878 (U3CtakeScreenShotU3Ec__Iterator0_t72106313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<takeScreenShot>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CtakeScreenShotU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3304219682 (U3CtakeScreenShotU3Ec__Iterator0_t72106313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<takeScreenShot>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CtakeScreenShotU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3758525866 (U3CtakeScreenShotU3Ec__Iterator0_t72106313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<takeScreenShot>c__Iterator0::Dispose()
extern "C"  void U3CtakeScreenShotU3Ec__Iterator0_Dispose_m932170873 (U3CtakeScreenShotU3Ec__Iterator0_t72106313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<takeScreenShot>c__Iterator0::Reset()
extern "C"  void U3CtakeScreenShotU3Ec__Iterator0_Reset_m2481372879 (U3CtakeScreenShotU3Ec__Iterator0_t72106313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
