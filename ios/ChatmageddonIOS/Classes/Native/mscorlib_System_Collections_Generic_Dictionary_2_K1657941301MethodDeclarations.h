﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Vectrosity.Vector3Pair,System.Boolean>
struct Dictionary_2_t3263405159;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1657941301.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Vector3Pa2859078138.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3221947276_gshared (Enumerator_t1657941301 * __this, Dictionary_2_t3263405159 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3221947276(__this, ___host0, method) ((  void (*) (Enumerator_t1657941301 *, Dictionary_2_t3263405159 *, const MethodInfo*))Enumerator__ctor_m3221947276_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4078648603_gshared (Enumerator_t1657941301 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4078648603(__this, method) ((  Il2CppObject * (*) (Enumerator_t1657941301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4078648603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2782778159_gshared (Enumerator_t1657941301 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2782778159(__this, method) ((  void (*) (Enumerator_t1657941301 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2782778159_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m1666201920_gshared (Enumerator_t1657941301 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1666201920(__this, method) ((  void (*) (Enumerator_t1657941301 *, const MethodInfo*))Enumerator_Dispose_m1666201920_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2181798235_gshared (Enumerator_t1657941301 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2181798235(__this, method) ((  bool (*) (Enumerator_t1657941301 *, const MethodInfo*))Enumerator_MoveNext_m2181798235_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Vectrosity.Vector3Pair,System.Boolean>::get_Current()
extern "C"  Vector3Pair_t2859078138  Enumerator_get_Current_m343145593_gshared (Enumerator_t1657941301 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m343145593(__this, method) ((  Vector3Pair_t2859078138  (*) (Enumerator_t1657941301 *, const MethodInfo*))Enumerator_get_Current_m343145593_gshared)(__this, method)
