﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<ChatNavScreen>
struct List_1_t3706465689;
// System.Collections.Generic.IEnumerable`1<ChatNavScreen>
struct IEnumerable_1_t334504306;
// ChatNavScreen[]
struct ChatNavScreenU5BU5D_t2994307200;
// System.Collections.Generic.IEnumerator`1<ChatNavScreen>
struct IEnumerator_1_t1812868384;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<ChatNavScreen>
struct ICollection_1_t994452566;
// System.Collections.ObjectModel.ReadOnlyCollection`1<ChatNavScreen>
struct ReadOnlyCollection_1_t228162953;
// System.Predicate`1<ChatNavScreen>
struct Predicate_1_t2780314672;
// System.Action`1<ChatNavScreen>
struct Action_1_t4139143939;
// System.Collections.Generic.IComparer`1<ChatNavScreen>
struct IComparer_1_t2291807679;
// System.Comparison`1<ChatNavScreen>
struct Comparison_1_t1304116112;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3241195363.h"

// System.Void System.Collections.Generic.List`1<ChatNavScreen>::.ctor()
extern "C"  void List_1__ctor_m937900109_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1__ctor_m937900109(__this, method) ((  void (*) (List_1_t3706465689 *, const MethodInfo*))List_1__ctor_m937900109_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4114381160_gshared (List_1_t3706465689 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4114381160(__this, ___collection0, method) ((  void (*) (List_1_t3706465689 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4114381160_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3257723050_gshared (List_1_t3706465689 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3257723050(__this, ___capacity0, method) ((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1__ctor_m3257723050_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m1360716394_gshared (List_1_t3706465689 * __this, ChatNavScreenU5BU5D_t2994307200* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m1360716394(__this, ___data0, ___size1, method) ((  void (*) (List_1_t3706465689 *, ChatNavScreenU5BU5D_t2994307200*, int32_t, const MethodInfo*))List_1__ctor_m1360716394_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::.cctor()
extern "C"  void List_1__cctor_m3714868926_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3714868926(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3714868926_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m679665873_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m679665873(__this, method) ((  Il2CppObject* (*) (List_1_t3706465689 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m679665873_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1216866597_gshared (List_1_t3706465689 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1216866597(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3706465689 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1216866597_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1445296716_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1445296716(__this, method) ((  Il2CppObject * (*) (List_1_t3706465689 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1445296716_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m4142235985_gshared (List_1_t3706465689 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m4142235985(__this, ___item0, method) ((  int32_t (*) (List_1_t3706465689 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m4142235985_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m875380037_gshared (List_1_t3706465689 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m875380037(__this, ___item0, method) ((  bool (*) (List_1_t3706465689 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m875380037_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3767856771_gshared (List_1_t3706465689 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3767856771(__this, ___item0, method) ((  int32_t (*) (List_1_t3706465689 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3767856771_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1344329274_gshared (List_1_t3706465689 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1344329274(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3706465689 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1344329274_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2232255088_gshared (List_1_t3706465689 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2232255088(__this, ___item0, method) ((  void (*) (List_1_t3706465689 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2232255088_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2606669328_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2606669328(__this, method) ((  bool (*) (List_1_t3706465689 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2606669328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2585027089_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2585027089(__this, method) ((  bool (*) (List_1_t3706465689 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2585027089_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1858586501_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1858586501(__this, method) ((  Il2CppObject * (*) (List_1_t3706465689 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1858586501_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1599782850_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1599782850(__this, method) ((  bool (*) (List_1_t3706465689 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1599782850_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2848507725_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2848507725(__this, method) ((  bool (*) (List_1_t3706465689 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2848507725_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4252140622_gshared (List_1_t3706465689 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m4252140622(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m4252140622_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3798331323_gshared (List_1_t3706465689 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3798331323(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3706465689 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3798331323_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Add(T)
extern "C"  void List_1_Add_m2921695604_gshared (List_1_t3706465689 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m2921695604(__this, ___item0, method) ((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_Add_m2921695604_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2222424541_gshared (List_1_t3706465689 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2222424541(__this, ___newCount0, method) ((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2222424541_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2328104082_gshared (List_1_t3706465689 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2328104082(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3706465689 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2328104082_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m425283685_gshared (List_1_t3706465689 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m425283685(__this, ___collection0, method) ((  void (*) (List_1_t3706465689 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m425283685_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m479217061_gshared (List_1_t3706465689 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m479217061(__this, ___enumerable0, method) ((  void (*) (List_1_t3706465689 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m479217061_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3130171712_gshared (List_1_t3706465689 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3130171712(__this, ___collection0, method) ((  void (*) (List_1_t3706465689 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3130171712_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<ChatNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t228162953 * List_1_AsReadOnly_m2400538565_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2400538565(__this, method) ((  ReadOnlyCollection_1_t228162953 * (*) (List_1_t3706465689 *, const MethodInfo*))List_1_AsReadOnly_m2400538565_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Clear()
extern "C"  void List_1_Clear_m1139540162_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_Clear_m1139540162(__this, method) ((  void (*) (List_1_t3706465689 *, const MethodInfo*))List_1_Clear_m1139540162_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m793120748_gshared (List_1_t3706465689 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m793120748(__this, ___item0, method) ((  bool (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_Contains_m793120748_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m2899836501_gshared (List_1_t3706465689 * __this, ChatNavScreenU5BU5D_t2994307200* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m2899836501(__this, ___array0, method) ((  void (*) (List_1_t3706465689 *, ChatNavScreenU5BU5D_t2994307200*, const MethodInfo*))List_1_CopyTo_m2899836501_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m650836070_gshared (List_1_t3706465689 * __this, ChatNavScreenU5BU5D_t2994307200* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m650836070(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3706465689 *, ChatNavScreenU5BU5D_t2994307200*, int32_t, const MethodInfo*))List_1_CopyTo_m650836070_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m165067068_gshared (List_1_t3706465689 * __this, int32_t ___index0, ChatNavScreenU5BU5D_t2994307200* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m165067068(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t3706465689 *, int32_t, ChatNavScreenU5BU5D_t2994307200*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m165067068_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m3386120312_gshared (List_1_t3706465689 * __this, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_Exists_m3386120312(__this, ___match0, method) ((  bool (*) (List_1_t3706465689 *, Predicate_1_t2780314672 *, const MethodInfo*))List_1_Exists_m3386120312_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<ChatNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1442480770_gshared (List_1_t3706465689 * __this, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_Find_m1442480770(__this, ___match0, method) ((  int32_t (*) (List_1_t3706465689 *, Predicate_1_t2780314672 *, const MethodInfo*))List_1_Find_m1442480770_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2437101221_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2437101221(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2780314672 *, const MethodInfo*))List_1_CheckMatch_m2437101221_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ChatNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t3706465689 * List_1_FindAll_m3308951757_gshared (List_1_t3706465689 * __this, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m3308951757(__this, ___match0, method) ((  List_1_t3706465689 * (*) (List_1_t3706465689 *, Predicate_1_t2780314672 *, const MethodInfo*))List_1_FindAll_m3308951757_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ChatNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t3706465689 * List_1_FindAllStackBits_m1515628143_gshared (List_1_t3706465689 * __this, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1515628143(__this, ___match0, method) ((  List_1_t3706465689 * (*) (List_1_t3706465689 *, Predicate_1_t2780314672 *, const MethodInfo*))List_1_FindAllStackBits_m1515628143_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<ChatNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t3706465689 * List_1_FindAllList_m1104849455_gshared (List_1_t3706465689 * __this, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m1104849455(__this, ___match0, method) ((  List_1_t3706465689 * (*) (List_1_t3706465689 *, Predicate_1_t2780314672 *, const MethodInfo*))List_1_FindAllList_m1104849455_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m1854745065_gshared (List_1_t3706465689 * __this, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m1854745065(__this, ___match0, method) ((  int32_t (*) (List_1_t3706465689 *, Predicate_1_t2780314672 *, const MethodInfo*))List_1_FindIndex_m1854745065_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3752024754_gshared (List_1_t3706465689 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2780314672 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3752024754(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3706465689 *, int32_t, int32_t, Predicate_1_t2780314672 *, const MethodInfo*))List_1_GetIndex_m3752024754_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m3262751535_gshared (List_1_t3706465689 * __this, Action_1_t4139143939 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m3262751535(__this, ___action0, method) ((  void (*) (List_1_t3706465689 *, Action_1_t4139143939 *, const MethodInfo*))List_1_ForEach_m3262751535_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<ChatNavScreen>::GetEnumerator()
extern "C"  Enumerator_t3241195363  List_1_GetEnumerator_m1379090181_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1379090181(__this, method) ((  Enumerator_t3241195363  (*) (List_1_t3706465689 *, const MethodInfo*))List_1_GetEnumerator_m1379090181_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m93671724_gshared (List_1_t3706465689 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m93671724(__this, ___item0, method) ((  int32_t (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_IndexOf_m93671724_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4091822105_gshared (List_1_t3706465689 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4091822105(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3706465689 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4091822105_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3604116050_gshared (List_1_t3706465689 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3604116050(__this, ___index0, method) ((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3604116050_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3541817715_gshared (List_1_t3706465689 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m3541817715(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3706465689 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m3541817715_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3511863364_gshared (List_1_t3706465689 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3511863364(__this, ___collection0, method) ((  void (*) (List_1_t3706465689 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3511863364_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<ChatNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m3136876015_gshared (List_1_t3706465689 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3136876015(__this, ___item0, method) ((  bool (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_Remove_m3136876015_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m250157481_gshared (List_1_t3706465689 * __this, Predicate_1_t2780314672 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m250157481(__this, ___match0, method) ((  int32_t (*) (List_1_t3706465689 *, Predicate_1_t2780314672 *, const MethodInfo*))List_1_RemoveAll_m250157481_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3990429374_gshared (List_1_t3706465689 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3990429374(__this, ___index0, method) ((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3990429374_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m39387726_gshared (List_1_t3706465689 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m39387726(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3706465689 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m39387726_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m3711220033_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_Reverse_m3711220033(__this, method) ((  void (*) (List_1_t3706465689 *, const MethodInfo*))List_1_Reverse_m3711220033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Sort()
extern "C"  void List_1_Sort_m3895516343_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_Sort_m3895516343(__this, method) ((  void (*) (List_1_t3706465689 *, const MethodInfo*))List_1_Sort_m3895516343_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m842955763_gshared (List_1_t3706465689 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m842955763(__this, ___comparer0, method) ((  void (*) (List_1_t3706465689 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m842955763_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3218157734_gshared (List_1_t3706465689 * __this, Comparison_1_t1304116112 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3218157734(__this, ___comparison0, method) ((  void (*) (List_1_t3706465689 *, Comparison_1_t1304116112 *, const MethodInfo*))List_1_Sort_m3218157734_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<ChatNavScreen>::ToArray()
extern "C"  ChatNavScreenU5BU5D_t2994307200* List_1_ToArray_m800649062_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_ToArray_m800649062(__this, method) ((  ChatNavScreenU5BU5D_t2994307200* (*) (List_1_t3706465689 *, const MethodInfo*))List_1_ToArray_m800649062_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3965139844_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3965139844(__this, method) ((  void (*) (List_1_t3706465689 *, const MethodInfo*))List_1_TrimExcess_m3965139844_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m718882882_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m718882882(__this, method) ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))List_1_get_Capacity_m718882882_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1610003173_gshared (List_1_t3706465689 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1610003173(__this, ___value0, method) ((  void (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1610003173_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<ChatNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m567132114_gshared (List_1_t3706465689 * __this, const MethodInfo* method);
#define List_1_get_Count_m567132114(__this, method) ((  int32_t (*) (List_1_t3706465689 *, const MethodInfo*))List_1_get_Count_m567132114_gshared)(__this, method)
// T System.Collections.Generic.List`1<ChatNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m3941722657_gshared (List_1_t3706465689 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3941722657(__this, ___index0, method) ((  int32_t (*) (List_1_t3706465689 *, int32_t, const MethodInfo*))List_1_get_Item_m3941722657_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<ChatNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3812476740_gshared (List_1_t3706465689 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m3812476740(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3706465689 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m3812476740_gshared)(__this, ___index0, ___value1, method)
