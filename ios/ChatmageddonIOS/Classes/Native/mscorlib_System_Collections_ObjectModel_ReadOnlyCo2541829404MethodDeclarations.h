﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>
struct ReadOnlyCollection_1_t2541829404;
// System.Collections.Generic.IList`1<LeaderboardNavScreen>
struct IList_1_t2896984313;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// LeaderboardNavScreen[]
struct LeaderboardNavScreenU5BU5D_t2088986945;
// System.Collections.Generic.IEnumerator`1<LeaderboardNavScreen>
struct IEnumerator_1_t4126534835;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2412641028_gshared (ReadOnlyCollection_1_t2541829404 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2412641028(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2412641028_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m433731000_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m433731000(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m433731000_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m576031532_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m576031532(__this, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m576031532_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2219173237_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2219173237(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2219173237_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1381864245_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1381864245(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1381864245_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3324773473_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3324773473(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3324773473_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3856430477_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3856430477(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3856430477_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3574638570_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3574638570(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3574638570_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1812850602_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1812850602(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1812850602_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1879806671_gshared (ReadOnlyCollection_1_t2541829404 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1879806671(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1879806671_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2685001894_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2685001894(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2685001894_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1427071831_gshared (ReadOnlyCollection_1_t2541829404 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1427071831(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2541829404 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1427071831_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2939553095_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2939553095(__this, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2939553095_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1824960407_gshared (ReadOnlyCollection_1_t2541829404 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1824960407(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2541829404 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1824960407_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m834849865_gshared (ReadOnlyCollection_1_t2541829404 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m834849865(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2541829404 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m834849865_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3226276876_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3226276876(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3226276876_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1381171802_gshared (ReadOnlyCollection_1_t2541829404 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1381171802(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1381171802_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2749869742_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2749869742(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2749869742_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1304788943_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1304788943(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1304788943_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2106168211_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2106168211(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2106168211_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2890766400_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2890766400(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2890766400_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m328211227_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m328211227(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m328211227_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3297702902_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3297702902(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3297702902_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m263135693_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m263135693(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m263135693_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3859069862_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3859069862(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3859069862_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m786004500_gshared (ReadOnlyCollection_1_t2541829404 * __this, LeaderboardNavScreenU5BU5D_t2088986945* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m786004500(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2541829404 *, LeaderboardNavScreenU5BU5D_t2088986945*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m786004500_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m122463167_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m122463167(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m122463167_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1016479690_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1016479690(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1016479690_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2700395895_gshared (ReadOnlyCollection_1_t2541829404 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2700395895(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2541829404 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2700395895_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<LeaderboardNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m3435866665_gshared (ReadOnlyCollection_1_t2541829404 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3435866665(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2541829404 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3435866665_gshared)(__this, ___index0, method)
