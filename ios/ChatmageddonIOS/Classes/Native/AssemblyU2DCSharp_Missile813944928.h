﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Item2440468191.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Missile
struct  Missile_t813944928  : public Item_t2440468191
{
public:
	// System.Int32 Missile::flightTime
	int32_t ___flightTime_4;
	// System.Int32 Missile::points
	int32_t ___points_5;
	// System.Int32 Missile::criticalHitChance
	int32_t ___criticalHitChance_6;
	// System.Int32 Missile::backfireChance
	int32_t ___backfireChance_7;
	// System.Int32 Missile::launchNotificationDelay
	int32_t ___launchNotificationDelay_8;
	// System.Int32 Missile::fuelUse
	int32_t ___fuelUse_9;

public:
	inline static int32_t get_offset_of_flightTime_4() { return static_cast<int32_t>(offsetof(Missile_t813944928, ___flightTime_4)); }
	inline int32_t get_flightTime_4() const { return ___flightTime_4; }
	inline int32_t* get_address_of_flightTime_4() { return &___flightTime_4; }
	inline void set_flightTime_4(int32_t value)
	{
		___flightTime_4 = value;
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(Missile_t813944928, ___points_5)); }
	inline int32_t get_points_5() const { return ___points_5; }
	inline int32_t* get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(int32_t value)
	{
		___points_5 = value;
	}

	inline static int32_t get_offset_of_criticalHitChance_6() { return static_cast<int32_t>(offsetof(Missile_t813944928, ___criticalHitChance_6)); }
	inline int32_t get_criticalHitChance_6() const { return ___criticalHitChance_6; }
	inline int32_t* get_address_of_criticalHitChance_6() { return &___criticalHitChance_6; }
	inline void set_criticalHitChance_6(int32_t value)
	{
		___criticalHitChance_6 = value;
	}

	inline static int32_t get_offset_of_backfireChance_7() { return static_cast<int32_t>(offsetof(Missile_t813944928, ___backfireChance_7)); }
	inline int32_t get_backfireChance_7() const { return ___backfireChance_7; }
	inline int32_t* get_address_of_backfireChance_7() { return &___backfireChance_7; }
	inline void set_backfireChance_7(int32_t value)
	{
		___backfireChance_7 = value;
	}

	inline static int32_t get_offset_of_launchNotificationDelay_8() { return static_cast<int32_t>(offsetof(Missile_t813944928, ___launchNotificationDelay_8)); }
	inline int32_t get_launchNotificationDelay_8() const { return ___launchNotificationDelay_8; }
	inline int32_t* get_address_of_launchNotificationDelay_8() { return &___launchNotificationDelay_8; }
	inline void set_launchNotificationDelay_8(int32_t value)
	{
		___launchNotificationDelay_8 = value;
	}

	inline static int32_t get_offset_of_fuelUse_9() { return static_cast<int32_t>(offsetof(Missile_t813944928, ___fuelUse_9)); }
	inline int32_t get_fuelUse_9() const { return ___fuelUse_9; }
	inline int32_t* get_address_of_fuelUse_9() { return &___fuelUse_9; }
	inline void set_fuelUse_9(int32_t value)
	{
		___fuelUse_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
