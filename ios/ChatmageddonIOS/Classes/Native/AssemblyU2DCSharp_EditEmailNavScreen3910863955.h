﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditEmailNavScreen
struct  EditEmailNavScreen_t3910863955  : public NavigationScreen_t2333230110
{
public:
	// UIInput EditEmailNavScreen::emailInput
	UIInput_t860674234 * ___emailInput_3;
	// System.Boolean EditEmailNavScreen::UIclosing
	bool ___UIclosing_4;

public:
	inline static int32_t get_offset_of_emailInput_3() { return static_cast<int32_t>(offsetof(EditEmailNavScreen_t3910863955, ___emailInput_3)); }
	inline UIInput_t860674234 * get_emailInput_3() const { return ___emailInput_3; }
	inline UIInput_t860674234 ** get_address_of_emailInput_3() { return &___emailInput_3; }
	inline void set_emailInput_3(UIInput_t860674234 * value)
	{
		___emailInput_3 = value;
		Il2CppCodeGenWriteBarrier(&___emailInput_3, value);
	}

	inline static int32_t get_offset_of_UIclosing_4() { return static_cast<int32_t>(offsetof(EditEmailNavScreen_t3910863955, ___UIclosing_4)); }
	inline bool get_UIclosing_4() const { return ___UIclosing_4; }
	inline bool* get_address_of_UIclosing_4() { return &___UIclosing_4; }
	inline void set_UIclosing_4(bool value)
	{
		___UIclosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
