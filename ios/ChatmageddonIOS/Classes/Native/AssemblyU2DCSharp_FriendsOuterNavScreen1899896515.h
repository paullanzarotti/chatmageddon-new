﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// FriendsILP
struct FriendsILP_t207575760;
// OuterPhoneNumberButton
struct OuterPhoneNumberButton_t2854054552;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsOuterNavScreen
struct  FriendsOuterNavScreen_t1899896515  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject FriendsOuterNavScreen::hasFriendsUI
	GameObject_t1756533147 * ___hasFriendsUI_3;
	// UnityEngine.GameObject FriendsOuterNavScreen::hasNoFriendsUI
	GameObject_t1756533147 * ___hasNoFriendsUI_4;
	// FriendsILP FriendsOuterNavScreen::outerFriendsList
	FriendsILP_t207575760 * ___outerFriendsList_5;
	// OuterPhoneNumberButton FriendsOuterNavScreen::phoneNumButton
	OuterPhoneNumberButton_t2854054552 * ___phoneNumButton_6;
	// System.Boolean FriendsOuterNavScreen::UIclosing
	bool ___UIclosing_7;

public:
	inline static int32_t get_offset_of_hasFriendsUI_3() { return static_cast<int32_t>(offsetof(FriendsOuterNavScreen_t1899896515, ___hasFriendsUI_3)); }
	inline GameObject_t1756533147 * get_hasFriendsUI_3() const { return ___hasFriendsUI_3; }
	inline GameObject_t1756533147 ** get_address_of_hasFriendsUI_3() { return &___hasFriendsUI_3; }
	inline void set_hasFriendsUI_3(GameObject_t1756533147 * value)
	{
		___hasFriendsUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasFriendsUI_3, value);
	}

	inline static int32_t get_offset_of_hasNoFriendsUI_4() { return static_cast<int32_t>(offsetof(FriendsOuterNavScreen_t1899896515, ___hasNoFriendsUI_4)); }
	inline GameObject_t1756533147 * get_hasNoFriendsUI_4() const { return ___hasNoFriendsUI_4; }
	inline GameObject_t1756533147 ** get_address_of_hasNoFriendsUI_4() { return &___hasNoFriendsUI_4; }
	inline void set_hasNoFriendsUI_4(GameObject_t1756533147 * value)
	{
		___hasNoFriendsUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___hasNoFriendsUI_4, value);
	}

	inline static int32_t get_offset_of_outerFriendsList_5() { return static_cast<int32_t>(offsetof(FriendsOuterNavScreen_t1899896515, ___outerFriendsList_5)); }
	inline FriendsILP_t207575760 * get_outerFriendsList_5() const { return ___outerFriendsList_5; }
	inline FriendsILP_t207575760 ** get_address_of_outerFriendsList_5() { return &___outerFriendsList_5; }
	inline void set_outerFriendsList_5(FriendsILP_t207575760 * value)
	{
		___outerFriendsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___outerFriendsList_5, value);
	}

	inline static int32_t get_offset_of_phoneNumButton_6() { return static_cast<int32_t>(offsetof(FriendsOuterNavScreen_t1899896515, ___phoneNumButton_6)); }
	inline OuterPhoneNumberButton_t2854054552 * get_phoneNumButton_6() const { return ___phoneNumButton_6; }
	inline OuterPhoneNumberButton_t2854054552 ** get_address_of_phoneNumButton_6() { return &___phoneNumButton_6; }
	inline void set_phoneNumButton_6(OuterPhoneNumberButton_t2854054552 * value)
	{
		___phoneNumButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumButton_6, value);
	}

	inline static int32_t get_offset_of_UIclosing_7() { return static_cast<int32_t>(offsetof(FriendsOuterNavScreen_t1899896515, ___UIclosing_7)); }
	inline bool get_UIclosing_7() const { return ___UIclosing_7; }
	inline bool* get_address_of_UIclosing_7() { return &___UIclosing_7; }
	inline void set_UIclosing_7(bool value)
	{
		___UIclosing_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
