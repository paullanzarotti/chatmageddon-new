﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameters16149445.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameters30556752.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter433842492.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter852146563.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Parameter139417372.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2682613251.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete2592312329.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3425534311.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Paramete3532059937.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Prng_Cryp405007133.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Prng_Dig1445396782.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_4145682572.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1646699940.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1823299568.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_EC27564669.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_2702239436.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_3543322195.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1378799819.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_G601621931.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_4170685704.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1034645512.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_1670054114.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_R600049064.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_3187127386.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Signers_X244497657.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst2715111877.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3831412532.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst4187022390.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst1662285836.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3594632315.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3246029418.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3351798479.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst1629639237.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3501337442.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abstr463031607.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Abst3577453307.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Aler1943690406.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Aler2418630078.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Alway671795399.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Byte1600245655.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert3711240521.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert2775016569.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert4188827490.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert1829945713.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert2118695048.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cert1123924209.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Chach588232263.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Chan1387977466.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Ciph2647370547.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Ciph3625443269.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Clie4001196324.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Comb1177273743.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Comp3132420437.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Conn3056634225.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Cont1560631153.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Defau744769081.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Defa1499842933.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Defe1571847761.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Dige3911248312.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Dige2878944447.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Digi2312486481.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_ECBa3945911148.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_ECCu2024367045.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_ECPo3531342969.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Encr1304858704.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Expor839268261.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Exte2022447001.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_FiniteFie85465.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hand4067128167.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hash2960667477.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Heart950151469.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hear4074124803.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Hear3199822323.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_KeyE1390198871.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Lega3599698926.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Lega1589283767.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_MacA2921466062.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_MaxFr108152128.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_NameT221825315.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Named630294042.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_NewS3489773180.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Ocsp1991562814.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_PrfA4218585047.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Prot3273908466.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Recor911577901.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Secu3985528004.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Serv3525792961.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Serv2635557658.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Serve405134486.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sessi833892266.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sess1698926946.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign2898832071.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign3350051566.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign3777817038.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Tls_Sign2455255744.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4100 = { sizeof (ParametersWithRandom_t16149445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4100[2] = 
{
	ParametersWithRandom_t16149445::get_offset_of_parameters_0(),
	ParametersWithRandom_t16149445::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4101 = { sizeof (ParametersWithSBox_t30556752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4101[2] = 
{
	ParametersWithSBox_t30556752::get_offset_of_parameters_0(),
	ParametersWithSBox_t30556752::get_offset_of_sBox_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4102 = { sizeof (ParametersWithSalt_t433842492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4102[2] = 
{
	ParametersWithSalt_t433842492::get_offset_of_salt_0(),
	ParametersWithSalt_t433842492::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4103 = { sizeof (RC2Parameters_t852146563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4103[1] = 
{
	RC2Parameters_t852146563::get_offset_of_bits_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4104 = { sizeof (RC5Parameters_t139417372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4104[1] = 
{
	RC5Parameters_t139417372::get_offset_of_rounds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4105 = { sizeof (RsaBlindingParameters_t2682613251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4105[2] = 
{
	RsaBlindingParameters_t2682613251::get_offset_of_publicKey_0(),
	RsaBlindingParameters_t2682613251::get_offset_of_blindingFactor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4106 = { sizeof (RsaKeyGenerationParameters_t2592312329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4106[2] = 
{
	RsaKeyGenerationParameters_t2592312329::get_offset_of_publicExponent_2(),
	RsaKeyGenerationParameters_t2592312329::get_offset_of_certainty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4107 = { sizeof (RsaKeyParameters_t3425534311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4107[2] = 
{
	RsaKeyParameters_t3425534311::get_offset_of_modulus_1(),
	RsaKeyParameters_t3425534311::get_offset_of_exponent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4108 = { sizeof (RsaPrivateCrtKeyParameters_t3532059937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4108[6] = 
{
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_e_3(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_p_4(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_q_5(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_dP_6(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_dQ_7(),
	RsaPrivateCrtKeyParameters_t3532059937::get_offset_of_qInv_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4109 = { sizeof (CryptoApiRandomGenerator_t405007133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4109[1] = 
{
	CryptoApiRandomGenerator_t405007133::get_offset_of_rndProv_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4110 = { sizeof (DigestRandomGenerator_t1445396782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4110[6] = 
{
	0,
	DigestRandomGenerator_t1445396782::get_offset_of_stateCounter_1(),
	DigestRandomGenerator_t1445396782::get_offset_of_seedCounter_2(),
	DigestRandomGenerator_t1445396782::get_offset_of_digest_3(),
	DigestRandomGenerator_t1445396782::get_offset_of_state_4(),
	DigestRandomGenerator_t1445396782::get_offset_of_seed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4111 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4112 = { sizeof (DsaDigestSigner_t4145682572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4112[3] = 
{
	DsaDigestSigner_t4145682572::get_offset_of_digest_0(),
	DsaDigestSigner_t4145682572::get_offset_of_dsaSigner_1(),
	DsaDigestSigner_t4145682572::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4113 = { sizeof (DsaSigner_t1646699940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4113[3] = 
{
	DsaSigner_t1646699940::get_offset_of_kCalculator_0(),
	DsaSigner_t1646699940::get_offset_of_key_1(),
	DsaSigner_t1646699940::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4114 = { sizeof (ECDsaSigner_t1823299568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4114[3] = 
{
	ECDsaSigner_t1823299568::get_offset_of_kCalculator_0(),
	ECDsaSigner_t1823299568::get_offset_of_key_1(),
	ECDsaSigner_t1823299568::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4115 = { sizeof (ECGost3410Signer_t27564669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4115[2] = 
{
	ECGost3410Signer_t27564669::get_offset_of_key_0(),
	ECGost3410Signer_t27564669::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4116 = { sizeof (ECNRSigner_t2702239436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4116[3] = 
{
	ECNRSigner_t2702239436::get_offset_of_forSigning_0(),
	ECNRSigner_t2702239436::get_offset_of_key_1(),
	ECNRSigner_t2702239436::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4117 = { sizeof (Gost3410DigestSigner_t3543322195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4117[3] = 
{
	Gost3410DigestSigner_t3543322195::get_offset_of_digest_0(),
	Gost3410DigestSigner_t3543322195::get_offset_of_dsaSigner_1(),
	Gost3410DigestSigner_t3543322195::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4118 = { sizeof (Gost3410Signer_t1378799819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4118[2] = 
{
	Gost3410Signer_t1378799819::get_offset_of_key_0(),
	Gost3410Signer_t1378799819::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4119 = { sizeof (GenericSigner_t601621931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4119[3] = 
{
	GenericSigner_t601621931::get_offset_of_engine_0(),
	GenericSigner_t601621931::get_offset_of_digest_1(),
	GenericSigner_t601621931::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4120 = { sizeof (HMacDsaKCalculator_t4170685704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4120[4] = 
{
	HMacDsaKCalculator_t4170685704::get_offset_of_hMac_0(),
	HMacDsaKCalculator_t4170685704::get_offset_of_K_1(),
	HMacDsaKCalculator_t4170685704::get_offset_of_V_2(),
	HMacDsaKCalculator_t4170685704::get_offset_of_n_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4121 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4122 = { sizeof (Iso9796d2Signer_t1034645512), -1, sizeof(Iso9796d2Signer_t1034645512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4122[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Iso9796d2Signer_t1034645512_StaticFields::get_offset_of_trailerMap_8(),
	Iso9796d2Signer_t1034645512::get_offset_of_digest_9(),
	Iso9796d2Signer_t1034645512::get_offset_of_cipher_10(),
	Iso9796d2Signer_t1034645512::get_offset_of_trailer_11(),
	Iso9796d2Signer_t1034645512::get_offset_of_keyBits_12(),
	Iso9796d2Signer_t1034645512::get_offset_of_block_13(),
	Iso9796d2Signer_t1034645512::get_offset_of_mBuf_14(),
	Iso9796d2Signer_t1034645512::get_offset_of_messageLength_15(),
	Iso9796d2Signer_t1034645512::get_offset_of_fullMessage_16(),
	Iso9796d2Signer_t1034645512::get_offset_of_recoveredMessage_17(),
	Iso9796d2Signer_t1034645512::get_offset_of_preSig_18(),
	Iso9796d2Signer_t1034645512::get_offset_of_preBlock_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4123 = { sizeof (PssSigner_t1670054114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4123[14] = 
{
	0,
	PssSigner_t1670054114::get_offset_of_contentDigest1_1(),
	PssSigner_t1670054114::get_offset_of_contentDigest2_2(),
	PssSigner_t1670054114::get_offset_of_mgfDigest_3(),
	PssSigner_t1670054114::get_offset_of_cipher_4(),
	PssSigner_t1670054114::get_offset_of_random_5(),
	PssSigner_t1670054114::get_offset_of_hLen_6(),
	PssSigner_t1670054114::get_offset_of_mgfhLen_7(),
	PssSigner_t1670054114::get_offset_of_sLen_8(),
	PssSigner_t1670054114::get_offset_of_emBits_9(),
	PssSigner_t1670054114::get_offset_of_salt_10(),
	PssSigner_t1670054114::get_offset_of_mDash_11(),
	PssSigner_t1670054114::get_offset_of_block_12(),
	PssSigner_t1670054114::get_offset_of_trailer_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4124 = { sizeof (RandomDsaKCalculator_t600049064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4124[2] = 
{
	RandomDsaKCalculator_t600049064::get_offset_of_q_0(),
	RandomDsaKCalculator_t600049064::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4125 = { sizeof (RsaDigestSigner_t3187127386), -1, sizeof(RsaDigestSigner_t3187127386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4125[5] = 
{
	RsaDigestSigner_t3187127386::get_offset_of_rsaEngine_0(),
	RsaDigestSigner_t3187127386::get_offset_of_algId_1(),
	RsaDigestSigner_t3187127386::get_offset_of_digest_2(),
	RsaDigestSigner_t3187127386::get_offset_of_forSigning_3(),
	RsaDigestSigner_t3187127386_StaticFields::get_offset_of_oidMap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4126 = { sizeof (X931Signer_t244497657), -1, sizeof(X931Signer_t244497657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4126[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	X931Signer_t244497657_StaticFields::get_offset_of_trailerMap_9(),
	X931Signer_t244497657::get_offset_of_digest_10(),
	X931Signer_t244497657::get_offset_of_cipher_11(),
	X931Signer_t244497657::get_offset_of_kParam_12(),
	X931Signer_t244497657::get_offset_of_trailer_13(),
	X931Signer_t244497657::get_offset_of_keyBits_14(),
	X931Signer_t244497657::get_offset_of_block_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4127 = { sizeof (AbstractTlsAgreementCredentials_t2715111877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4128 = { sizeof (AbstractTlsCipherFactory_t3831412532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4129 = { sizeof (AbstractTlsClient_t4187022390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4129[9] = 
{
	AbstractTlsClient_t4187022390::get_offset_of_mCipherFactory_0(),
	AbstractTlsClient_t4187022390::get_offset_of_mContext_1(),
	AbstractTlsClient_t4187022390::get_offset_of_mSupportedSignatureAlgorithms_2(),
	AbstractTlsClient_t4187022390::get_offset_of_mNamedCurves_3(),
	AbstractTlsClient_t4187022390::get_offset_of_mClientECPointFormats_4(),
	AbstractTlsClient_t4187022390::get_offset_of_mServerECPointFormats_5(),
	AbstractTlsClient_t4187022390::get_offset_of_mSelectedCipherSuite_6(),
	AbstractTlsClient_t4187022390::get_offset_of_mSelectedCompressionMethod_7(),
	AbstractTlsClient_t4187022390::get_offset_of_U3CHostNamesU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4130 = { sizeof (AbstractTlsContext_t1662285836), -1, sizeof(AbstractTlsContext_t1662285836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4130[8] = 
{
	AbstractTlsContext_t1662285836_StaticFields::get_offset_of_counter_0(),
	AbstractTlsContext_t1662285836::get_offset_of_mNonceRandom_1(),
	AbstractTlsContext_t1662285836::get_offset_of_mSecureRandom_2(),
	AbstractTlsContext_t1662285836::get_offset_of_mSecurityParameters_3(),
	AbstractTlsContext_t1662285836::get_offset_of_mClientVersion_4(),
	AbstractTlsContext_t1662285836::get_offset_of_mServerVersion_5(),
	AbstractTlsContext_t1662285836::get_offset_of_mSession_6(),
	AbstractTlsContext_t1662285836::get_offset_of_mUserObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4131 = { sizeof (AbstractTlsCredentials_t3594632315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4132 = { sizeof (AbstractTlsEncryptionCredentials_t3246029418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4133 = { sizeof (AbstractTlsKeyExchange_t3351798479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4133[3] = 
{
	AbstractTlsKeyExchange_t3351798479::get_offset_of_mKeyExchange_0(),
	AbstractTlsKeyExchange_t3351798479::get_offset_of_mSupportedSignatureAlgorithms_1(),
	AbstractTlsKeyExchange_t3351798479::get_offset_of_mContext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4134 = { sizeof (AbstractTlsPeer_t1629639237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4135 = { sizeof (AbstractTlsServer_t3501337442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4135[18] = 
{
	AbstractTlsServer_t3501337442::get_offset_of_mCipherFactory_0(),
	AbstractTlsServer_t3501337442::get_offset_of_mContext_1(),
	AbstractTlsServer_t3501337442::get_offset_of_mClientVersion_2(),
	AbstractTlsServer_t3501337442::get_offset_of_mOfferedCipherSuites_3(),
	AbstractTlsServer_t3501337442::get_offset_of_mOfferedCompressionMethods_4(),
	AbstractTlsServer_t3501337442::get_offset_of_mClientExtensions_5(),
	AbstractTlsServer_t3501337442::get_offset_of_mEncryptThenMacOffered_6(),
	AbstractTlsServer_t3501337442::get_offset_of_mMaxFragmentLengthOffered_7(),
	AbstractTlsServer_t3501337442::get_offset_of_mTruncatedHMacOffered_8(),
	AbstractTlsServer_t3501337442::get_offset_of_mSupportedSignatureAlgorithms_9(),
	AbstractTlsServer_t3501337442::get_offset_of_mEccCipherSuitesOffered_10(),
	AbstractTlsServer_t3501337442::get_offset_of_mNamedCurves_11(),
	AbstractTlsServer_t3501337442::get_offset_of_mClientECPointFormats_12(),
	AbstractTlsServer_t3501337442::get_offset_of_mServerECPointFormats_13(),
	AbstractTlsServer_t3501337442::get_offset_of_mServerVersion_14(),
	AbstractTlsServer_t3501337442::get_offset_of_mSelectedCipherSuite_15(),
	AbstractTlsServer_t3501337442::get_offset_of_mSelectedCompressionMethod_16(),
	AbstractTlsServer_t3501337442::get_offset_of_mServerExtensions_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4136 = { sizeof (AbstractTlsSigner_t463031607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4136[1] = 
{
	AbstractTlsSigner_t463031607::get_offset_of_mContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4137 = { sizeof (AbstractTlsSignerCredentials_t3577453307), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4138 = { sizeof (AlertDescription_t1943690406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4138[31] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4139 = { sizeof (AlertLevel_t2418630078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4139[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4140 = { sizeof (AlwaysValidVerifyer_t671795399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4141 = { sizeof (ByteQueue_t1600245655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4141[4] = 
{
	0,
	ByteQueue_t1600245655::get_offset_of_databuf_1(),
	ByteQueue_t1600245655::get_offset_of_skipped_2(),
	ByteQueue_t1600245655::get_offset_of_available_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4142 = { sizeof (CertChainType_t3711240521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4142[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4143 = { sizeof (Certificate_t2775016569), -1, sizeof(Certificate_t2775016569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4143[2] = 
{
	Certificate_t2775016569_StaticFields::get_offset_of_EmptyChain_0(),
	Certificate_t2775016569::get_offset_of_mCertificateList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4144 = { sizeof (CertificateRequest_t4188827490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4144[3] = 
{
	CertificateRequest_t4188827490::get_offset_of_mCertificateTypes_0(),
	CertificateRequest_t4188827490::get_offset_of_mSupportedSignatureAlgorithms_1(),
	CertificateRequest_t4188827490::get_offset_of_mCertificateAuthorities_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4145 = { sizeof (CertificateStatus_t1829945713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4145[2] = 
{
	CertificateStatus_t1829945713::get_offset_of_mStatusType_0(),
	CertificateStatus_t1829945713::get_offset_of_mResponse_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4146 = { sizeof (CertificateStatusRequest_t2118695048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4146[2] = 
{
	CertificateStatusRequest_t2118695048::get_offset_of_mStatusType_0(),
	CertificateStatusRequest_t2118695048::get_offset_of_mRequest_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4147 = { sizeof (CertificateStatusType_t1123924209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4147[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4148 = { sizeof (Chacha20Poly1305_t588232263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4148[3] = 
{
	Chacha20Poly1305_t588232263::get_offset_of_context_0(),
	Chacha20Poly1305_t588232263::get_offset_of_encryptCipher_1(),
	Chacha20Poly1305_t588232263::get_offset_of_decryptCipher_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4149 = { sizeof (ChangeCipherSpec_t1387977466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4149[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4150 = { sizeof (CipherSuite_t2647370547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4150[270] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4151 = { sizeof (CipherType_t3625443269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4151[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4152 = { sizeof (ClientCertificateType_t4001196324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4152[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4153 = { sizeof (CombinedHash_t1177273743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4153[3] = 
{
	CombinedHash_t1177273743::get_offset_of_mContext_0(),
	CombinedHash_t1177273743::get_offset_of_mMd5_1(),
	CombinedHash_t1177273743::get_offset_of_mSha1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4154 = { sizeof (CompressionMethod_t3132420437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4154[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4155 = { sizeof (ConnectionEnd_t3056634225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4155[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4156 = { sizeof (ContentType_t1560631153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4156[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4157 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4158 = { sizeof (DefaultTlsCipherFactory_t744769081), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4159 = { sizeof (DefaultTlsClient_t1499842933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4160 = { sizeof (DeferredHash_t1571847761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4160[5] = 
{
	0,
	DeferredHash_t1571847761::get_offset_of_mContext_1(),
	DeferredHash_t1571847761::get_offset_of_mBuf_2(),
	DeferredHash_t1571847761::get_offset_of_mHashes_3(),
	DeferredHash_t1571847761::get_offset_of_mPrfHashAlgorithm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4161 = { sizeof (DigestInputBuffer_t3911248312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4162 = { sizeof (DigStream_t2878944447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4162[1] = 
{
	DigStream_t2878944447::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4163 = { sizeof (DigitallySigned_t2312486481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4163[2] = 
{
	DigitallySigned_t2312486481::get_offset_of_mAlgorithm_0(),
	DigitallySigned_t2312486481::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4164 = { sizeof (ECBasisType_t3945911148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4164[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4165 = { sizeof (ECCurveType_t2024367045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4165[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4166 = { sizeof (ECPointFormat_t3531342969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4166[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4167 = { sizeof (EncryptionAlgorithm_t1304858704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4167[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4168 = { sizeof (ExporterLabel_t839268261), -1, sizeof(ExporterLabel_t839268261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4168[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ExporterLabel_t839268261_StaticFields::get_offset_of_extended_master_secret_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4169 = { sizeof (ExtensionType_t2022447001), -1, sizeof(ExtensionType_t2022447001_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4169[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ExtensionType_t2022447001_StaticFields::get_offset_of_negotiated_ff_dhe_groups_16(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4170 = { sizeof (FiniteFieldDheGroup_t85465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4170[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4171 = { sizeof (HandshakeType_t4067128167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4171[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4172 = { sizeof (HashAlgorithm_t2960667477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4172[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4173 = { sizeof (HeartbeatExtension_t950151469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4173[1] = 
{
	HeartbeatExtension_t950151469::get_offset_of_mMode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4174 = { sizeof (HeartbeatMessageType_t4074124803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4174[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4175 = { sizeof (HeartbeatMode_t3199822323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4175[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4176 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4177 = { sizeof (KeyExchangeAlgorithm_t1390198871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4177[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4178 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4179 = { sizeof (LegacyTlsAuthentication_t3599698926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4179[3] = 
{
	LegacyTlsAuthentication_t3599698926::get_offset_of_verifyer_0(),
	LegacyTlsAuthentication_t3599698926::get_offset_of_credProvider_1(),
	LegacyTlsAuthentication_t3599698926::get_offset_of_TargetUri_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4180 = { sizeof (LegacyTlsClient_t1589283767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4180[3] = 
{
	LegacyTlsClient_t1589283767::get_offset_of_TargetUri_9(),
	LegacyTlsClient_t1589283767::get_offset_of_verifyer_10(),
	LegacyTlsClient_t1589283767::get_offset_of_credProvider_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4181 = { sizeof (MacAlgorithm_t2921466062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4181[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4182 = { sizeof (MaxFragmentLength_t108152128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4182[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4183 = { sizeof (NameType_t221825315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4183[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4184 = { sizeof (NamedCurve_t630294042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4184[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4185 = { sizeof (NewSessionTicket_t3489773180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4185[2] = 
{
	NewSessionTicket_t3489773180::get_offset_of_mTicketLifetimeHint_0(),
	NewSessionTicket_t3489773180::get_offset_of_mTicket_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4186 = { sizeof (OcspStatusRequest_t1991562814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4186[2] = 
{
	OcspStatusRequest_t1991562814::get_offset_of_mResponderIDList_0(),
	OcspStatusRequest_t1991562814::get_offset_of_mRequestExtensions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4187 = { sizeof (PrfAlgorithm_t4218585047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4187[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4188 = { sizeof (ProtocolVersion_t3273908466), -1, sizeof(ProtocolVersion_t3273908466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4188[8] = 
{
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_SSLv3_0(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_TLSv10_1(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_TLSv11_2(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_TLSv12_3(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_DTLSv10_4(),
	ProtocolVersion_t3273908466_StaticFields::get_offset_of_DTLSv12_5(),
	ProtocolVersion_t3273908466::get_offset_of_version_6(),
	ProtocolVersion_t3273908466::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4189 = { sizeof (RecordStream_t911577901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4189[20] = 
{
	0,
	RecordStream_t911577901::get_offset_of_mHandler_1(),
	RecordStream_t911577901::get_offset_of_mInput_2(),
	RecordStream_t911577901::get_offset_of_mOutput_3(),
	RecordStream_t911577901::get_offset_of_mPendingCompression_4(),
	RecordStream_t911577901::get_offset_of_mReadCompression_5(),
	RecordStream_t911577901::get_offset_of_mWriteCompression_6(),
	RecordStream_t911577901::get_offset_of_mPendingCipher_7(),
	RecordStream_t911577901::get_offset_of_mReadCipher_8(),
	RecordStream_t911577901::get_offset_of_mWriteCipher_9(),
	RecordStream_t911577901::get_offset_of_mReadSeqNo_10(),
	RecordStream_t911577901::get_offset_of_mWriteSeqNo_11(),
	RecordStream_t911577901::get_offset_of_mBuffer_12(),
	RecordStream_t911577901::get_offset_of_mHandshakeHash_13(),
	RecordStream_t911577901::get_offset_of_mReadVersion_14(),
	RecordStream_t911577901::get_offset_of_mWriteVersion_15(),
	RecordStream_t911577901::get_offset_of_mRestrictReadVersion_16(),
	RecordStream_t911577901::get_offset_of_mPlaintextLimit_17(),
	RecordStream_t911577901::get_offset_of_mCompressedLimit_18(),
	RecordStream_t911577901::get_offset_of_mCiphertextLimit_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4190 = { sizeof (SecurityParameters_t3985528004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4190[15] = 
{
	SecurityParameters_t3985528004::get_offset_of_entity_0(),
	SecurityParameters_t3985528004::get_offset_of_cipherSuite_1(),
	SecurityParameters_t3985528004::get_offset_of_compressionAlgorithm_2(),
	SecurityParameters_t3985528004::get_offset_of_prfAlgorithm_3(),
	SecurityParameters_t3985528004::get_offset_of_verifyDataLength_4(),
	SecurityParameters_t3985528004::get_offset_of_masterSecret_5(),
	SecurityParameters_t3985528004::get_offset_of_clientRandom_6(),
	SecurityParameters_t3985528004::get_offset_of_serverRandom_7(),
	SecurityParameters_t3985528004::get_offset_of_sessionHash_8(),
	SecurityParameters_t3985528004::get_offset_of_pskIdentity_9(),
	SecurityParameters_t3985528004::get_offset_of_srpIdentity_10(),
	SecurityParameters_t3985528004::get_offset_of_maxFragmentLength_11(),
	SecurityParameters_t3985528004::get_offset_of_truncatedHMac_12(),
	SecurityParameters_t3985528004::get_offset_of_encryptThenMac_13(),
	SecurityParameters_t3985528004::get_offset_of_extendedMasterSecret_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4191 = { sizeof (ServerDHParams_t3525792961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4191[1] = 
{
	ServerDHParams_t3525792961::get_offset_of_mPublicKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4192 = { sizeof (ServerName_t2635557658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4192[2] = 
{
	ServerName_t2635557658::get_offset_of_mNameType_0(),
	ServerName_t2635557658::get_offset_of_mName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4193 = { sizeof (ServerNameList_t405134486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4193[1] = 
{
	ServerNameList_t405134486::get_offset_of_mServerNameList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4194 = { sizeof (SessionParameters_t833892266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4194[7] = 
{
	SessionParameters_t833892266::get_offset_of_mCipherSuite_0(),
	SessionParameters_t833892266::get_offset_of_mCompressionAlgorithm_1(),
	SessionParameters_t833892266::get_offset_of_mMasterSecret_2(),
	SessionParameters_t833892266::get_offset_of_mPeerCertificate_3(),
	SessionParameters_t833892266::get_offset_of_mPskIdentity_4(),
	SessionParameters_t833892266::get_offset_of_mSrpIdentity_5(),
	SessionParameters_t833892266::get_offset_of_mEncodedServerExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4195 = { sizeof (Builder_t1698926946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4195[7] = 
{
	Builder_t1698926946::get_offset_of_mCipherSuite_0(),
	Builder_t1698926946::get_offset_of_mCompressionAlgorithm_1(),
	Builder_t1698926946::get_offset_of_mMasterSecret_2(),
	Builder_t1698926946::get_offset_of_mPeerCertificate_3(),
	Builder_t1698926946::get_offset_of_mPskIdentity_4(),
	Builder_t1698926946::get_offset_of_mSrpIdentity_5(),
	Builder_t1698926946::get_offset_of_mEncodedServerExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4196 = { sizeof (SignatureAlgorithm_t2898832071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4196[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4197 = { sizeof (SignatureAndHashAlgorithm_t3350051566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4197[2] = 
{
	SignatureAndHashAlgorithm_t3350051566::get_offset_of_mHash_0(),
	SignatureAndHashAlgorithm_t3350051566::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4198 = { sizeof (SignerInputBuffer_t3777817038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4199 = { sizeof (SigStream_t2455255744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4199[1] = 
{
	SigStream_t2455255744::get_offset_of_s_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
