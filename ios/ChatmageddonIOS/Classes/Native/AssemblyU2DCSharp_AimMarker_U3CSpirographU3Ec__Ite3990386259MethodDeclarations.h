﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AimMarker/<Spirograph>c__Iterator1
struct U3CSpirographU3Ec__Iterator1_t3990386259;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AimMarker/<Spirograph>c__Iterator1::.ctor()
extern "C"  void U3CSpirographU3Ec__Iterator1__ctor_m1709100534 (U3CSpirographU3Ec__Iterator1_t3990386259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AimMarker/<Spirograph>c__Iterator1::MoveNext()
extern "C"  bool U3CSpirographU3Ec__Iterator1_MoveNext_m2950742442 (U3CSpirographU3Ec__Iterator1_t3990386259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AimMarker/<Spirograph>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSpirographU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3897815194 (U3CSpirographU3Ec__Iterator1_t3990386259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AimMarker/<Spirograph>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSpirographU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2833752402 (U3CSpirographU3Ec__Iterator1_t3990386259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker/<Spirograph>c__Iterator1::Dispose()
extern "C"  void U3CSpirographU3Ec__Iterator1_Dispose_m144455169 (U3CSpirographU3Ec__Iterator1_t3990386259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AimMarker/<Spirograph>c__Iterator1::Reset()
extern "C"  void U3CSpirographU3Ec__Iterator1_Reset_m634034855 (U3CSpirographU3Ec__Iterator1_t3990386259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
