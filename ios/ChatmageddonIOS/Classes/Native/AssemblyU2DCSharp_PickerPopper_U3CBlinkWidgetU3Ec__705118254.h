﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PickerPopper
struct PickerPopper_t344149016;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickerPopper/<BlinkWidget>c__Iterator2
struct  U3CBlinkWidgetU3Ec__Iterator2_t705118254  : public Il2CppObject
{
public:
	// System.Int32 PickerPopper/<BlinkWidget>c__Iterator2::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 PickerPopper/<BlinkWidget>c__Iterator2::nbOfBlinks
	int32_t ___nbOfBlinks_1;
	// PickerPopper PickerPopper/<BlinkWidget>c__Iterator2::$this
	PickerPopper_t344149016 * ___U24this_2;
	// System.Object PickerPopper/<BlinkWidget>c__Iterator2::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean PickerPopper/<BlinkWidget>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 PickerPopper/<BlinkWidget>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CBlinkWidgetU3Ec__Iterator2_t705118254, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_nbOfBlinks_1() { return static_cast<int32_t>(offsetof(U3CBlinkWidgetU3Ec__Iterator2_t705118254, ___nbOfBlinks_1)); }
	inline int32_t get_nbOfBlinks_1() const { return ___nbOfBlinks_1; }
	inline int32_t* get_address_of_nbOfBlinks_1() { return &___nbOfBlinks_1; }
	inline void set_nbOfBlinks_1(int32_t value)
	{
		___nbOfBlinks_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBlinkWidgetU3Ec__Iterator2_t705118254, ___U24this_2)); }
	inline PickerPopper_t344149016 * get_U24this_2() const { return ___U24this_2; }
	inline PickerPopper_t344149016 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PickerPopper_t344149016 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CBlinkWidgetU3Ec__Iterator2_t705118254, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CBlinkWidgetU3Ec__Iterator2_t705118254, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CBlinkWidgetU3Ec__Iterator2_t705118254, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
