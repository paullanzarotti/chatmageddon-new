﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ProvidePropertyAttribute
struct ProvidePropertyAttribute_t3184620082;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.ProvidePropertyAttribute::.ctor(System.String,System.String)
extern "C"  void ProvidePropertyAttribute__ctor_m1392154115 (ProvidePropertyAttribute_t3184620082 * __this, String_t* ___propertyName0, String_t* ___receiverTypeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ProvidePropertyAttribute::.ctor(System.String,System.Type)
extern "C"  void ProvidePropertyAttribute__ctor_m2353973422 (ProvidePropertyAttribute_t3184620082 * __this, String_t* ___propertyName0, Type_t * ___receiverType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.ProvidePropertyAttribute::get_PropertyName()
extern "C"  String_t* ProvidePropertyAttribute_get_PropertyName_m1391431255 (ProvidePropertyAttribute_t3184620082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.ProvidePropertyAttribute::get_ReceiverTypeName()
extern "C"  String_t* ProvidePropertyAttribute_get_ReceiverTypeName_m2075273149 (ProvidePropertyAttribute_t3184620082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.ProvidePropertyAttribute::get_TypeId()
extern "C"  Il2CppObject * ProvidePropertyAttribute_get_TypeId_m2190174470 (ProvidePropertyAttribute_t3184620082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ProvidePropertyAttribute::Equals(System.Object)
extern "C"  bool ProvidePropertyAttribute_Equals_m1841261056 (ProvidePropertyAttribute_t3184620082 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.ProvidePropertyAttribute::GetHashCode()
extern "C"  int32_t ProvidePropertyAttribute_GetHashCode_m1995405800 (ProvidePropertyAttribute_t3184620082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
