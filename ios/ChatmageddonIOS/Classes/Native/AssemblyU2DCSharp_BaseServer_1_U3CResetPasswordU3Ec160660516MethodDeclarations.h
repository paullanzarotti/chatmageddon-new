﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<ResetPassword>c__AnonStoreyC<System.Object>
struct U3CResetPasswordU3Ec__AnonStoreyC_t160660516;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<ResetPassword>c__AnonStoreyC<System.Object>::.ctor()
extern "C"  void U3CResetPasswordU3Ec__AnonStoreyC__ctor_m3373005429_gshared (U3CResetPasswordU3Ec__AnonStoreyC_t160660516 * __this, const MethodInfo* method);
#define U3CResetPasswordU3Ec__AnonStoreyC__ctor_m3373005429(__this, method) ((  void (*) (U3CResetPasswordU3Ec__AnonStoreyC_t160660516 *, const MethodInfo*))U3CResetPasswordU3Ec__AnonStoreyC__ctor_m3373005429_gshared)(__this, method)
// System.Void BaseServer`1/<ResetPassword>c__AnonStoreyC<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CResetPasswordU3Ec__AnonStoreyC_U3CU3Em__0_m3794074788_gshared (U3CResetPasswordU3Ec__AnonStoreyC_t160660516 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CResetPasswordU3Ec__AnonStoreyC_U3CU3Em__0_m3794074788(__this, ___request0, ___response1, method) ((  void (*) (U3CResetPasswordU3Ec__AnonStoreyC_t160660516 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CResetPasswordU3Ec__AnonStoreyC_U3CU3Em__0_m3794074788_gshared)(__this, ___request0, ___response1, method)
