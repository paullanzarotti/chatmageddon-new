﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.CallbackValidatorAttribute
struct CallbackValidatorAttribute_t975572991;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Configuration.CallbackValidatorAttribute::.ctor()
extern "C"  void CallbackValidatorAttribute__ctor_m3897728344 (CallbackValidatorAttribute_t975572991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.CallbackValidatorAttribute::get_CallbackMethodName()
extern "C"  String_t* CallbackValidatorAttribute_get_CallbackMethodName_m2912763955 (CallbackValidatorAttribute_t975572991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CallbackValidatorAttribute::set_CallbackMethodName(System.String)
extern "C"  void CallbackValidatorAttribute_set_CallbackMethodName_m1126958840 (CallbackValidatorAttribute_t975572991 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Configuration.CallbackValidatorAttribute::get_Type()
extern "C"  Type_t * CallbackValidatorAttribute_get_Type_m720026647 (CallbackValidatorAttribute_t975572991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CallbackValidatorAttribute::set_Type(System.Type)
extern "C"  void CallbackValidatorAttribute_set_Type_m2596758652 (CallbackValidatorAttribute_t975572991 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.CallbackValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * CallbackValidatorAttribute_get_ValidatorInstance_m1823678621 (CallbackValidatorAttribute_t975572991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
