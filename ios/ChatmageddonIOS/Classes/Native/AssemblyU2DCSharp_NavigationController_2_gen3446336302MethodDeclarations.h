﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,PhoneNumberNavScreen>
struct NavigationController_2_t3446336302;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m1308199617_gshared (NavigationController_2_t3446336302 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m1308199617(__this, method) ((  void (*) (NavigationController_2_t3446336302 *, const MethodInfo*))NavigationController_2__ctor_m1308199617_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m45824959_gshared (NavigationController_2_t3446336302 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m45824959(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t3446336302 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m45824959_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m1132032114_gshared (NavigationController_2_t3446336302 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m1132032114(__this, method) ((  void (*) (NavigationController_2_t3446336302 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m1132032114_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m3705288999_gshared (NavigationController_2_t3446336302 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m3705288999(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t3446336302 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m3705288999_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,PhoneNumberNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m2067710955_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m2067710955(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t3446336302 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m2067710955_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,PhoneNumberNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m3105950792_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m3105950792(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t3446336302 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m3105950792_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3049343775_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m3049343775(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t3446336302 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3049343775_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2052677691_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m2052677691(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t3446336302 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2052677691_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,PhoneNumberNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m290611767_gshared (NavigationController_2_t3446336302 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m290611767(__this, ___screen0, method) ((  void (*) (NavigationController_2_t3446336302 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m290611767_gshared)(__this, ___screen0, method)
