﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3248597474MethodDeclarations.h"

// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::.ctor()
#define BaseInfiniteListPopulator_1__ctor_m3799998405(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1__ctor_m2641845277_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::Awake()
#define BaseInfiniteListPopulator_1_Awake_m3170246560(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_Awake_m758246746_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::ResetPanel()
#define BaseInfiniteListPopulator_1_ResetPanel_m1103455824(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_ResetPanel_m2816930074_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m789308612(__this, ___item0, ___dataIndex1, ___carouselIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, Transform_t3275118058 *, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422_gshared)(__this, ___item0, ___dataIndex1, ___carouselIndex2, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::SetStartIndex(System.Int32)
#define BaseInfiniteListPopulator_1_SetStartIndex_m3100254182(__this, ___inStartIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_SetStartIndex_m814882572_gshared)(__this, ___inStartIndex0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::SetOriginalData(System.Collections.ArrayList)
#define BaseInfiniteListPopulator_1_SetOriginalData_m1334889477(__this, ___inDataList0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, ArrayList_t4252133567 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetOriginalData_m3487250981_gshared)(__this, ___inDataList0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::SetSectionIndices(System.Collections.Generic.List`1<System.Int32>)
#define BaseInfiniteListPopulator_1_SetSectionIndices_m577796472(__this, ___inSectionsIndices0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, List_1_t1440998580 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658_gshared)(__this, ___inSectionsIndices0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::RefreshTableView()
#define BaseInfiniteListPopulator_1_RefreshTableView_m3067133823(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshTableView_m3255066675_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::InitTableView(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableView_m3933734637(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableView_m3216652517_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::InitTableViewImp(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableViewImp_m3593551045(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::RepositionList()
#define BaseInfiniteListPopulator_1_RepositionList_m3227236381(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_RepositionList_m111137877_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::UponPostRepositionReq()
#define BaseInfiniteListPopulator_1_UponPostRepositionReq_m2877282309(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_InitListItemWithIndex_m3833385775(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, Transform_t3275118058 *, int32_t, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259_gshared)(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::PrepareListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m2495402106(__this, ___item0, ___newIndex1, ___oldIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, Transform_t3275118058 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::PrepareCarouselListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m1959424705(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, Transform_t3275118058 *, int32_t, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<LeaderboardListItem>::ItemIsInvisible(System.Int32)
#define BaseInfiniteListPopulator_1_ItemIsInvisible_m1100523380(__this, ___itemNumber0, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::SetItemInvisible(Item,System.Boolean)
#define BaseInfiniteListPopulator_1_SetItemInvisible_m3459118705(__this, ___item0, ___forward1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, LeaderboardListItem_t3112309392 *, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353_gshared)(__this, ___item0, ___forward1, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::UpdateList(System.Int32)
#define BaseInfiniteListPopulator_1_UpdateList_m3621382971(__this, ___itemNumber0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateList_m2236403487_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::UpdateListDirection(BaseInfiniteListPopulator`1/ListMovementDirection<Item>,System.Int32)
#define BaseInfiniteListPopulator_1_UpdateListDirection_m3638123496(__this, ___direction0, ___itemNumber1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970_gshared)(__this, ___direction0, ___itemNumber1, method)
// System.Int32 BaseInfiniteListPopulator`1<LeaderboardListItem>::GetJumpIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetJumpIndexForItem_m150573800(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498_gshared)(__this, ___itemDataIndex0, method)
// System.Int32 BaseInfiniteListPopulator`1<LeaderboardListItem>::GetRealIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetRealIndexForItem_m1745826584(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::itemIsPressed(System.Int32,System.Boolean)
#define BaseInfiniteListPopulator_1_itemIsPressed_m2809704824(__this, ___itemDataIndex0, ___isDown1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, bool, const MethodInfo*))BaseInfiniteListPopulator_1_itemIsPressed_m852036530_gshared)(__this, ___itemDataIndex0, ___isDown1, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::itemClicked(System.Int32)
#define BaseInfiniteListPopulator_1_itemClicked_m3190620010(__this, ___itemDataIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_itemClicked_m3174637120_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::StopScrollViewMomentum()
#define BaseInfiniteListPopulator_1_StopScrollViewMomentum_m4233681505(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289_gshared)(__this, method)
// UnityEngine.Transform BaseInfiniteListPopulator`1<LeaderboardListItem>::GetItemFromPool(System.Int32)
#define BaseInfiniteListPopulator_1_GetItemFromPool_m2043085645(__this, ___i0, method) ((  Transform_t3275118058 * (*) (BaseInfiniteListPopulator_1_t3671457571 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773_gshared)(__this, ___i0, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::RefreshPool()
#define BaseInfiniteListPopulator_1_RefreshPool_m1293742754(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshPool_m4033485912_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::SetScrollToBottom()
#define BaseInfiniteListPopulator_1_SetScrollToBottom_m1962298696(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::SetScrollPosition(System.Single,System.Boolean)
#define BaseInfiniteListPopulator_1_SetScrollPosition_m2652386757(__this, ___exactPosition0, ___up1, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, float, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381_gshared)(__this, ___exactPosition0, ___up1, method)
// System.Void BaseInfiniteListPopulator`1<LeaderboardListItem>::SetScrollPositionOverTime(System.Single)
#define BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m1768752477(__this, ___exactPosition0, method) ((  void (*) (BaseInfiniteListPopulator_1_t3671457571 *, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221_gshared)(__this, ___exactPosition0, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<LeaderboardListItem>::SetPanelPosOverTime(System.Single,System.Single)
#define BaseInfiniteListPopulator_1_SetPanelPosOverTime_m261114106(__this, ___seconds0, ___position1, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t3671457571 *, float, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792_gshared)(__this, ___seconds0, ___position1, method)
