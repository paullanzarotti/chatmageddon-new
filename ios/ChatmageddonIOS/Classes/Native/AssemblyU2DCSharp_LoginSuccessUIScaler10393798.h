﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginSuccessUIScaler
struct  LoginSuccessUIScaler_t10393798  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UITexture LoginSuccessUIScaler::backgroundTexture
	UITexture_t2537039969 * ___backgroundTexture_14;

public:
	inline static int32_t get_offset_of_backgroundTexture_14() { return static_cast<int32_t>(offsetof(LoginSuccessUIScaler_t10393798, ___backgroundTexture_14)); }
	inline UITexture_t2537039969 * get_backgroundTexture_14() const { return ___backgroundTexture_14; }
	inline UITexture_t2537039969 ** get_address_of_backgroundTexture_14() { return &___backgroundTexture_14; }
	inline void set_backgroundTexture_14(UITexture_t2537039969 * value)
	{
		___backgroundTexture_14 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTexture_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
