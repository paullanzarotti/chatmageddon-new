﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnibillError>
struct DefaultComparer_t3375789948;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnibillError>::.ctor()
extern "C"  void DefaultComparer__ctor_m4044744129_gshared (DefaultComparer_t3375789948 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4044744129(__this, method) ((  void (*) (DefaultComparer_t3375789948 *, const MethodInfo*))DefaultComparer__ctor_m4044744129_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnibillError>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m746187540_gshared (DefaultComparer_t3375789948 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m746187540(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3375789948 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m746187540_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnibillError>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3170369460_gshared (DefaultComparer_t3375789948 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3170369460(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3375789948 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3170369460_gshared)(__this, ___x0, ___y1, method)
