﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreamBarController
struct ScreamBarController_t3220293990;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreamBarController::.ctor()
extern "C"  void ScreamBarController__ctor_m1559808889 (ScreamBarController_t3220293990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamBarController::StartScreamChecking()
extern "C"  void ScreamBarController_StartScreamChecking_m1480023936 (ScreamBarController_t3220293990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScreamBarController::CheckScream()
extern "C"  Il2CppObject * ScreamBarController_CheckScream_m2905581280 (ScreamBarController_t3220293990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamBarController::UpdateProgressBar()
extern "C"  void ScreamBarController_UpdateProgressBar_m43114978 (ScreamBarController_t3220293990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamBarController::DefendMissile()
extern "C"  void ScreamBarController_DefendMissile_m932009461 (ScreamBarController_t3220293990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
