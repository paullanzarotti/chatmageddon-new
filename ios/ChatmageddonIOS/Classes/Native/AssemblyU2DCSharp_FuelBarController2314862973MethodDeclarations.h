﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FuelBarController
struct FuelBarController_t2314862973;

#include "codegen/il2cpp-codegen.h"

// System.Void FuelBarController::.ctor()
extern "C"  void FuelBarController__ctor_m44379732 (FuelBarController_t2314862973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuelBarController::TransitionToPercent(System.Single,System.Single)
extern "C"  void FuelBarController_TransitionToPercent_m2232272059 (FuelBarController_t2314862973 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuelBarController::ProgressUpdated()
extern "C"  void FuelBarController_ProgressUpdated_m19489250 (FuelBarController_t2314862973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuelBarController::ProgressFinished()
extern "C"  void FuelBarController_ProgressFinished_m3040500543 (FuelBarController_t2314862973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FuelBarController::UpdateFuelColour()
extern "C"  void FuelBarController_UpdateFuelColour_m890761603 (FuelBarController_t2314862973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
