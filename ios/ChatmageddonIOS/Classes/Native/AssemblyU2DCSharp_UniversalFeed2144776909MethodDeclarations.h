﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniversalFeed
struct UniversalFeed_t2144776909;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharp_UniversalUpdateType1629501440.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AutoFriendType103565391.h"

// System.Void UniversalFeed::.ctor()
extern "C"  void UniversalFeed__ctor_m739608520 (UniversalFeed_t2144776909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::InitFeed()
extern "C"  void UniversalFeed_InitFeed_m2219532144 (UniversalFeed_t2144776909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::GetFeedInfo()
extern "C"  void UniversalFeed_GetFeedInfo_m3167845748 (UniversalFeed_t2144776909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::ProcessFeedInfo(System.Collections.Hashtable)
extern "C"  void UniversalFeed_ProcessFeedInfo_m3533661859 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___infoTable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniversalUpdateType UniversalFeed::GetTypeFromString(System.String)
extern "C"  int32_t UniversalFeed_GetTypeFromString_m4051903190 (UniversalFeed_t2144776909 * __this, String_t* ___typeString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::ProcessUpdate(UniversalUpdateType,System.Collections.Hashtable)
extern "C"  void UniversalFeed_ProcessUpdate_m1841288512 (UniversalFeed_t2144776909 * __this, int32_t ___type0, Hashtable_t909839986 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingFriendRequest(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingFriendRequest_m3177195349 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::FriendRequestReplied(System.Collections.Hashtable)
extern "C"  void UniversalFeed_FriendRequestReplied_m143828200 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::AutoFriendAdded(System.Collections.Hashtable,AutoFriendType)
extern "C"  void UniversalFeed_AutoFriendAdded_m1171627830 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::InnerFriendsUpdate(System.Collections.Hashtable)
extern "C"  void UniversalFeed_InnerFriendsUpdate_m854973838 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::PlayerBlocked(System.Collections.Hashtable)
extern "C"  void UniversalFeed_PlayerBlocked_m256374011 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::PlayerUnblocked(System.Collections.Hashtable)
extern "C"  void UniversalFeed_PlayerUnblocked_m2608247246 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::ActivateUserStealthMode(System.Collections.Hashtable)
extern "C"  void UniversalFeed_ActivateUserStealthMode_m807757662 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::DeactivateUserStealthMode(System.Collections.Hashtable)
extern "C"  void UniversalFeed_DeactivateUserStealthMode_m2475276757 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::CheckLockoutUIActivation()
extern "C"  void UniversalFeed_CheckLockoutUIActivation_m3357259255 (UniversalFeed_t2144776909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::KnockoutUser(System.Collections.Hashtable)
extern "C"  void UniversalFeed_KnockoutUser_m2430757729 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::RemoveUserKnockout(System.Collections.Hashtable)
extern "C"  void UniversalFeed_RemoveUserKnockout_m1715649883 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::KnockoutFriend(System.Collections.Hashtable)
extern "C"  void UniversalFeed_KnockoutFriend_m4181166270 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::RemoveFriendKnockout(System.Collections.Hashtable)
extern "C"  void UniversalFeed_RemoveFriendKnockout_m1930223550 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::FuelRegeneration(System.Collections.Hashtable)
extern "C"  void UniversalFeed_FuelRegeneration_m4051909779 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::LevelAwarded(System.Collections.Hashtable)
extern "C"  void UniversalFeed_LevelAwarded_m1898210124 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMissileLaunched(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMissileLaunched_m3413217878 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMissileBackfired(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMissileBackfired_m276608961 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMissileHit(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMissileHit_m3377027419 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMissileBlocked(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMissileBlocked_m1124794290 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::OutgoingMissileHit(System.Collections.Hashtable)
extern "C"  void UniversalFeed_OutgoingMissileHit_m2487313759 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::OutgoingMissileDefended(System.Collections.Hashtable)
extern "C"  void UniversalFeed_OutgoingMissileDefended_m3398775051 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::OutgoingMissileBlocked(System.Collections.Hashtable)
extern "C"  void UniversalFeed_OutgoingMissileBlocked_m1989842192 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::PointsReset(System.Collections.Hashtable)
extern "C"  void UniversalFeed_PointsReset_m1552661162 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::PointsUpdated(System.Collections.Hashtable)
extern "C"  void UniversalFeed_PointsUpdated_m4061199730 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::LongNotificationSent(System.Collections.Hashtable)
extern "C"  void UniversalFeed_LongNotificationSent_m4056106827 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMineLaid(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMineLaid_m677346689 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMineExpired(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMineExpired_m1386121628 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMineComplete(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMineComplete_m816087098 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::IncomingMineTimedOut(System.Collections.Hashtable)
extern "C"  void UniversalFeed_IncomingMineTimedOut_m528756124 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::NemesisUpdated(System.Collections.Hashtable)
extern "C"  void UniversalFeed_NemesisUpdated_m2406805583 (UniversalFeed_t2144776909 * __this, Hashtable_t909839986 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<GetFeedInfo>__BaseCallProxy0()
extern "C"  void UniversalFeed_U3CGetFeedInfoU3E__BaseCallProxy0_m3082176731 (UniversalFeed_t2144776909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<GetFeedInfo>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CGetFeedInfoU3Em__0_m1170692438 (UniversalFeed_t2144776909 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<IncomingMissileLaunched>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CIncomingMissileLaunchedU3Em__1_m958946509 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<IncomingMissileBackfired>m__2(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CIncomingMissileBackfiredU3Em__2_m1578107957 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<IncomingMissileHit>m__3(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CIncomingMissileHitU3Em__3_m3571877754 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<IncomingMissileBlocked>m__4(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CIncomingMissileBlockedU3Em__4_m2082762212 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<OutgoingMissileHit>m__5(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3COutgoingMissileHitU3Em__5_m2875933902 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<OutgoingMissileDefended>m__6(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3COutgoingMissileDefendedU3Em__6_m1535567463 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<OutgoingMissileBlocked>m__7(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3COutgoingMissileBlockedU3Em__7_m2383077407 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<PointsReset>m__8(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CPointsResetU3Em__8_m191711564 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<IncomingMineComplete>m__9(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CIncomingMineCompleteU3Em__9_m2365915627 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniversalFeed::<IncomingMineTimedOut>m__A(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void UniversalFeed_U3CIncomingMineTimedOutU3Em__A_m4053126829 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
