﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VibrationController/<ContinuousVibrate>c__Iterator0
struct U3CContinuousVibrateU3Ec__Iterator0_t1905345410;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VibrationController/<ContinuousVibrate>c__Iterator0::.ctor()
extern "C"  void U3CContinuousVibrateU3Ec__Iterator0__ctor_m2225703931 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VibrationController/<ContinuousVibrate>c__Iterator0::MoveNext()
extern "C"  bool U3CContinuousVibrateU3Ec__Iterator0_MoveNext_m1803085589 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<ContinuousVibrate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CContinuousVibrateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3649436573 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<ContinuousVibrate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CContinuousVibrateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2385699845 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<ContinuousVibrate>c__Iterator0::Dispose()
extern "C"  void U3CContinuousVibrateU3Ec__Iterator0_Dispose_m1425763732 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<ContinuousVibrate>c__Iterator0::Reset()
extern "C"  void U3CContinuousVibrateU3Ec__Iterator0_Reset_m977914630 (U3CContinuousVibrateU3Ec__Iterator0_t1905345410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
