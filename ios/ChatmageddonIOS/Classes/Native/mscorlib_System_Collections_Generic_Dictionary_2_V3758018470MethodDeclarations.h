﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>
struct ValueCollection_t3758018470;
// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t930005165;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Char[]
struct CharU5BU5D_t1328083999;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2446524095.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m240820302_gshared (ValueCollection_t3758018470 * __this, Dictionary_2_t759991331 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m240820302(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3758018470 *, Dictionary_2_t759991331 *, const MethodInfo*))ValueCollection__ctor_m240820302_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m949723572_gshared (ValueCollection_t3758018470 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m949723572(__this, ___item0, method) ((  void (*) (ValueCollection_t3758018470 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m949723572_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m83467103_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m83467103(__this, method) ((  void (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m83467103_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4029140096_gshared (ValueCollection_t3758018470 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4029140096(__this, ___item0, method) ((  bool (*) (ValueCollection_t3758018470 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4029140096_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3698394383_gshared (ValueCollection_t3758018470 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3698394383(__this, ___item0, method) ((  bool (*) (ValueCollection_t3758018470 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3698394383_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1183319049_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1183319049(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1183319049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2086903757_gshared (ValueCollection_t3758018470 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2086903757(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3758018470 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2086903757_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1876818092_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1876818092(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1876818092_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3488394051_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3488394051(__this, method) ((  bool (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3488394051_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3008250489_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3008250489(__this, method) ((  bool (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3008250489_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3055679549_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3055679549(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3055679549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1502672479_gshared (ValueCollection_t3758018470 * __this, CharU5BU5D_t1328083999* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1502672479(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3758018470 *, CharU5BU5D_t1328083999*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1502672479_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::GetEnumerator()
extern "C"  Enumerator_t2446524095  ValueCollection_GetEnumerator_m1280550180_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1280550180(__this, method) ((  Enumerator_t2446524095  (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_GetEnumerator_m1280550180_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Char,System.Char>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1065539489_gshared (ValueCollection_t3758018470 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1065539489(__this, method) ((  int32_t (*) (ValueCollection_t3758018470 *, const MethodInfo*))ValueCollection_get_Count_m1065539489_gshared)(__this, method)
