﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_EarthQuake
struct CameraFilterPack_FX_EarthQuake_t694922649;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_EarthQuake::.ctor()
extern "C"  void CameraFilterPack_FX_EarthQuake__ctor_m1472082064 (CameraFilterPack_FX_EarthQuake_t694922649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_EarthQuake::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_EarthQuake_get_material_m1220572805 (CameraFilterPack_FX_EarthQuake_t694922649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::Start()
extern "C"  void CameraFilterPack_FX_EarthQuake_Start_m766937688 (CameraFilterPack_FX_EarthQuake_t694922649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_EarthQuake_OnRenderImage_m3612892648 (CameraFilterPack_FX_EarthQuake_t694922649 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::OnValidate()
extern "C"  void CameraFilterPack_FX_EarthQuake_OnValidate_m2145402983 (CameraFilterPack_FX_EarthQuake_t694922649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::Update()
extern "C"  void CameraFilterPack_FX_EarthQuake_Update_m2390346657 (CameraFilterPack_FX_EarthQuake_t694922649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::OnDisable()
extern "C"  void CameraFilterPack_FX_EarthQuake_OnDisable_m2825502005 (CameraFilterPack_FX_EarthQuake_t694922649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
