﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.NumberFormat
struct  NumberFormat_t441739224  : public Il2CppObject
{
public:
	// System.Boolean PhoneNumbers.NumberFormat::hasPattern
	bool ___hasPattern_2;
	// System.String PhoneNumbers.NumberFormat::pattern_
	String_t* ___pattern__3;
	// System.Boolean PhoneNumbers.NumberFormat::hasFormat
	bool ___hasFormat_5;
	// System.String PhoneNumbers.NumberFormat::format_
	String_t* ___format__6;
	// System.Collections.Generic.List`1<System.String> PhoneNumbers.NumberFormat::leadingDigitsPattern_
	List_1_t1398341365 * ___leadingDigitsPattern__8;
	// System.Boolean PhoneNumbers.NumberFormat::hasNationalPrefixFormattingRule
	bool ___hasNationalPrefixFormattingRule_10;
	// System.String PhoneNumbers.NumberFormat::nationalPrefixFormattingRule_
	String_t* ___nationalPrefixFormattingRule__11;
	// System.Boolean PhoneNumbers.NumberFormat::hasNationalPrefixOptionalWhenFormatting
	bool ___hasNationalPrefixOptionalWhenFormatting_13;
	// System.Boolean PhoneNumbers.NumberFormat::nationalPrefixOptionalWhenFormatting_
	bool ___nationalPrefixOptionalWhenFormatting__14;
	// System.Boolean PhoneNumbers.NumberFormat::hasDomesticCarrierCodeFormattingRule
	bool ___hasDomesticCarrierCodeFormattingRule_16;
	// System.String PhoneNumbers.NumberFormat::domesticCarrierCodeFormattingRule_
	String_t* ___domesticCarrierCodeFormattingRule__17;

public:
	inline static int32_t get_offset_of_hasPattern_2() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___hasPattern_2)); }
	inline bool get_hasPattern_2() const { return ___hasPattern_2; }
	inline bool* get_address_of_hasPattern_2() { return &___hasPattern_2; }
	inline void set_hasPattern_2(bool value)
	{
		___hasPattern_2 = value;
	}

	inline static int32_t get_offset_of_pattern__3() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___pattern__3)); }
	inline String_t* get_pattern__3() const { return ___pattern__3; }
	inline String_t** get_address_of_pattern__3() { return &___pattern__3; }
	inline void set_pattern__3(String_t* value)
	{
		___pattern__3 = value;
		Il2CppCodeGenWriteBarrier(&___pattern__3, value);
	}

	inline static int32_t get_offset_of_hasFormat_5() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___hasFormat_5)); }
	inline bool get_hasFormat_5() const { return ___hasFormat_5; }
	inline bool* get_address_of_hasFormat_5() { return &___hasFormat_5; }
	inline void set_hasFormat_5(bool value)
	{
		___hasFormat_5 = value;
	}

	inline static int32_t get_offset_of_format__6() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___format__6)); }
	inline String_t* get_format__6() const { return ___format__6; }
	inline String_t** get_address_of_format__6() { return &___format__6; }
	inline void set_format__6(String_t* value)
	{
		___format__6 = value;
		Il2CppCodeGenWriteBarrier(&___format__6, value);
	}

	inline static int32_t get_offset_of_leadingDigitsPattern__8() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___leadingDigitsPattern__8)); }
	inline List_1_t1398341365 * get_leadingDigitsPattern__8() const { return ___leadingDigitsPattern__8; }
	inline List_1_t1398341365 ** get_address_of_leadingDigitsPattern__8() { return &___leadingDigitsPattern__8; }
	inline void set_leadingDigitsPattern__8(List_1_t1398341365 * value)
	{
		___leadingDigitsPattern__8 = value;
		Il2CppCodeGenWriteBarrier(&___leadingDigitsPattern__8, value);
	}

	inline static int32_t get_offset_of_hasNationalPrefixFormattingRule_10() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___hasNationalPrefixFormattingRule_10)); }
	inline bool get_hasNationalPrefixFormattingRule_10() const { return ___hasNationalPrefixFormattingRule_10; }
	inline bool* get_address_of_hasNationalPrefixFormattingRule_10() { return &___hasNationalPrefixFormattingRule_10; }
	inline void set_hasNationalPrefixFormattingRule_10(bool value)
	{
		___hasNationalPrefixFormattingRule_10 = value;
	}

	inline static int32_t get_offset_of_nationalPrefixFormattingRule__11() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___nationalPrefixFormattingRule__11)); }
	inline String_t* get_nationalPrefixFormattingRule__11() const { return ___nationalPrefixFormattingRule__11; }
	inline String_t** get_address_of_nationalPrefixFormattingRule__11() { return &___nationalPrefixFormattingRule__11; }
	inline void set_nationalPrefixFormattingRule__11(String_t* value)
	{
		___nationalPrefixFormattingRule__11 = value;
		Il2CppCodeGenWriteBarrier(&___nationalPrefixFormattingRule__11, value);
	}

	inline static int32_t get_offset_of_hasNationalPrefixOptionalWhenFormatting_13() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___hasNationalPrefixOptionalWhenFormatting_13)); }
	inline bool get_hasNationalPrefixOptionalWhenFormatting_13() const { return ___hasNationalPrefixOptionalWhenFormatting_13; }
	inline bool* get_address_of_hasNationalPrefixOptionalWhenFormatting_13() { return &___hasNationalPrefixOptionalWhenFormatting_13; }
	inline void set_hasNationalPrefixOptionalWhenFormatting_13(bool value)
	{
		___hasNationalPrefixOptionalWhenFormatting_13 = value;
	}

	inline static int32_t get_offset_of_nationalPrefixOptionalWhenFormatting__14() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___nationalPrefixOptionalWhenFormatting__14)); }
	inline bool get_nationalPrefixOptionalWhenFormatting__14() const { return ___nationalPrefixOptionalWhenFormatting__14; }
	inline bool* get_address_of_nationalPrefixOptionalWhenFormatting__14() { return &___nationalPrefixOptionalWhenFormatting__14; }
	inline void set_nationalPrefixOptionalWhenFormatting__14(bool value)
	{
		___nationalPrefixOptionalWhenFormatting__14 = value;
	}

	inline static int32_t get_offset_of_hasDomesticCarrierCodeFormattingRule_16() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___hasDomesticCarrierCodeFormattingRule_16)); }
	inline bool get_hasDomesticCarrierCodeFormattingRule_16() const { return ___hasDomesticCarrierCodeFormattingRule_16; }
	inline bool* get_address_of_hasDomesticCarrierCodeFormattingRule_16() { return &___hasDomesticCarrierCodeFormattingRule_16; }
	inline void set_hasDomesticCarrierCodeFormattingRule_16(bool value)
	{
		___hasDomesticCarrierCodeFormattingRule_16 = value;
	}

	inline static int32_t get_offset_of_domesticCarrierCodeFormattingRule__17() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224, ___domesticCarrierCodeFormattingRule__17)); }
	inline String_t* get_domesticCarrierCodeFormattingRule__17() const { return ___domesticCarrierCodeFormattingRule__17; }
	inline String_t** get_address_of_domesticCarrierCodeFormattingRule__17() { return &___domesticCarrierCodeFormattingRule__17; }
	inline void set_domesticCarrierCodeFormattingRule__17(String_t* value)
	{
		___domesticCarrierCodeFormattingRule__17 = value;
		Il2CppCodeGenWriteBarrier(&___domesticCarrierCodeFormattingRule__17, value);
	}
};

struct NumberFormat_t441739224_StaticFields
{
public:
	// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat::defaultInstance
	NumberFormat_t441739224 * ___defaultInstance_0;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(NumberFormat_t441739224_StaticFields, ___defaultInstance_0)); }
	inline NumberFormat_t441739224 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline NumberFormat_t441739224 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(NumberFormat_t441739224 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
