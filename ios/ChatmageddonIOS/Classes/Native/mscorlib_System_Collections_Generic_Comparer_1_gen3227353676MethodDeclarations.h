﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<ChatNavScreen>
struct Comparer_1_t3227353676;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<ChatNavScreen>::.ctor()
extern "C"  void Comparer_1__ctor_m2872382360_gshared (Comparer_1_t3227353676 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m2872382360(__this, method) ((  void (*) (Comparer_1_t3227353676 *, const MethodInfo*))Comparer_1__ctor_m2872382360_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<ChatNavScreen>::.cctor()
extern "C"  void Comparer_1__cctor_m3277252157_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m3277252157(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m3277252157_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<ChatNavScreen>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m84688869_gshared (Comparer_1_t3227353676 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m84688869(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t3227353676 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m84688869_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<ChatNavScreen>::get_Default()
extern "C"  Comparer_1_t3227353676 * Comparer_1_get_Default_m468948284_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m468948284(__this /* static, unused */, method) ((  Comparer_1_t3227353676 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m468948284_gshared)(__this /* static, unused */, method)
