﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileDeclineButton
struct PlayerProfileDeclineButton_t3344688636;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerProfileDeclineButton::.ctor()
extern "C"  void PlayerProfileDeclineButton__ctor_m3746521025 (PlayerProfileDeclineButton_t3344688636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileDeclineButton::OnButtonClick()
extern "C"  void PlayerProfileDeclineButton_OnButtonClick_m190604176 (PlayerProfileDeclineButton_t3344688636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
