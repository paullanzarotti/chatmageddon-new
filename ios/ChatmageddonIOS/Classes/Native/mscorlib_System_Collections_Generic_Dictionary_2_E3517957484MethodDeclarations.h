﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1603178222MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m295420689(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3517957484 *, Dictionary_2_t2197932782 *, const MethodInfo*))Enumerator__ctor_m1777707756_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3212423140(__this, method) ((  Il2CppObject * (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3540632177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1798072996(__this, method) ((  void (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m919527565_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1981390299(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m382383752_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m994834638(__this, method) ((  Il2CppObject * (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3716184131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3653602360(__this, method) ((  Il2CppObject * (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1529692723_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::MoveNext()
#define Enumerator_MoveNext_m3405822984(__this, method) ((  bool (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_MoveNext_m2401982917_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Current()
#define Enumerator_get_Current_m3856974092(__this, method) ((  KeyValuePair_2_t4250245300  (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_get_Current_m2065485869_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3879214597(__this, method) ((  int32_t (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_get_CurrentKey_m1491320644_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2124456677(__this, method) ((  Dictionary_2_t309261261 * (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_get_CurrentValue_m4242886148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Reset()
#define Enumerator_Reset_m2747546927(__this, method) ((  void (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_Reset_m2622064810_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::VerifyState()
#define Enumerator_VerifyState_m3754322968(__this, method) ((  void (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_VerifyState_m3422172663_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m963738996(__this, method) ((  void (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_VerifyCurrent_m4229930837_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Dispose()
#define Enumerator_Dispose_m3089213529(__this, method) ((  void (*) (Enumerator_t3517957484 *, const MethodInfo*))Enumerator_Dispose_m2279604588_gshared)(__this, method)
