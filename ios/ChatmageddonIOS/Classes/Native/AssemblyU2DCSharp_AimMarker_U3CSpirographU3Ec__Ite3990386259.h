﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AimMarker
struct AimMarker_t327093161;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AimMarker/<Spirograph>c__Iterator1
struct  U3CSpirographU3Ec__Iterator1_t3990386259  : public Il2CppObject
{
public:
	// System.Double AimMarker/<Spirograph>c__Iterator1::<t>__0
	double ___U3CtU3E__0_0;
	// System.Double AimMarker/<Spirograph>c__Iterator1::<dt>__1
	double ___U3CdtU3E__1_1;
	// System.Double AimMarker/<Spirograph>c__Iterator1::<max_t>__2
	double ___U3Cmax_tU3E__2_2;
	// System.Double AimMarker/<Spirograph>c__Iterator1::<x1>__3
	double ___U3Cx1U3E__3_3;
	// System.Double AimMarker/<Spirograph>c__Iterator1::<y1>__4
	double ___U3Cy1U3E__4_4;
	// UnityEngine.Vector3 AimMarker/<Spirograph>c__Iterator1::<point>__5
	Vector3_t2243707580  ___U3CpointU3E__5_5;
	// AimMarker AimMarker/<Spirograph>c__Iterator1::$this
	AimMarker_t327093161 * ___U24this_6;
	// System.Object AimMarker/<Spirograph>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean AimMarker/<Spirograph>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 AimMarker/<Spirograph>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U3CtU3E__0_0)); }
	inline double get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline double* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(double value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdtU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U3CdtU3E__1_1)); }
	inline double get_U3CdtU3E__1_1() const { return ___U3CdtU3E__1_1; }
	inline double* get_address_of_U3CdtU3E__1_1() { return &___U3CdtU3E__1_1; }
	inline void set_U3CdtU3E__1_1(double value)
	{
		___U3CdtU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3Cmax_tU3E__2_2() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U3Cmax_tU3E__2_2)); }
	inline double get_U3Cmax_tU3E__2_2() const { return ___U3Cmax_tU3E__2_2; }
	inline double* get_address_of_U3Cmax_tU3E__2_2() { return &___U3Cmax_tU3E__2_2; }
	inline void set_U3Cmax_tU3E__2_2(double value)
	{
		___U3Cmax_tU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3Cx1U3E__3_3() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U3Cx1U3E__3_3)); }
	inline double get_U3Cx1U3E__3_3() const { return ___U3Cx1U3E__3_3; }
	inline double* get_address_of_U3Cx1U3E__3_3() { return &___U3Cx1U3E__3_3; }
	inline void set_U3Cx1U3E__3_3(double value)
	{
		___U3Cx1U3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3Cy1U3E__4_4() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U3Cy1U3E__4_4)); }
	inline double get_U3Cy1U3E__4_4() const { return ___U3Cy1U3E__4_4; }
	inline double* get_address_of_U3Cy1U3E__4_4() { return &___U3Cy1U3E__4_4; }
	inline void set_U3Cy1U3E__4_4(double value)
	{
		___U3Cy1U3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CpointU3E__5_5() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U3CpointU3E__5_5)); }
	inline Vector3_t2243707580  get_U3CpointU3E__5_5() const { return ___U3CpointU3E__5_5; }
	inline Vector3_t2243707580 * get_address_of_U3CpointU3E__5_5() { return &___U3CpointU3E__5_5; }
	inline void set_U3CpointU3E__5_5(Vector3_t2243707580  value)
	{
		___U3CpointU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U24this_6)); }
	inline AimMarker_t327093161 * get_U24this_6() const { return ___U24this_6; }
	inline AimMarker_t327093161 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(AimMarker_t327093161 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CSpirographU3Ec__Iterator1_t3990386259, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
