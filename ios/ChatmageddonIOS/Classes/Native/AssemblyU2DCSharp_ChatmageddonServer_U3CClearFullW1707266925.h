﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;
// ChatmageddonServer
struct ChatmageddonServer_t594474938;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonServer/<ClearFullWarefareStatus>c__AnonStorey28
struct  U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925  : public Il2CppObject
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ChatmageddonServer/<ClearFullWarefareStatus>c__AnonStorey28::callBack
	Action_3_t3681841185 * ___callBack_0;
	// ChatmageddonServer ChatmageddonServer/<ClearFullWarefareStatus>c__AnonStorey28::$this
	ChatmageddonServer_t594474938 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callBack_0() { return static_cast<int32_t>(offsetof(U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925, ___callBack_0)); }
	inline Action_3_t3681841185 * get_callBack_0() const { return ___callBack_0; }
	inline Action_3_t3681841185 ** get_address_of_callBack_0() { return &___callBack_0; }
	inline void set_callBack_0(Action_3_t3681841185 * value)
	{
		___callBack_0 = value;
		Il2CppCodeGenWriteBarrier(&___callBack_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925, ___U24this_1)); }
	inline ChatmageddonServer_t594474938 * get_U24this_1() const { return ___U24this_1; }
	inline ChatmageddonServer_t594474938 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ChatmageddonServer_t594474938 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
