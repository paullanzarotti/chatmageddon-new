﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBManager`1/<LoadPictureAPI>c__AnonStorey4<System.Object>
struct U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354;
// Facebook.Unity.IGraphResult
struct IGraphResult_t3984946686;

#include "codegen/il2cpp-codegen.h"

// System.Void FBManager`1/<LoadPictureAPI>c__AnonStorey4<System.Object>::.ctor()
extern "C"  void U3CLoadPictureAPIU3Ec__AnonStorey4__ctor_m2661967309_gshared (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * __this, const MethodInfo* method);
#define U3CLoadPictureAPIU3Ec__AnonStorey4__ctor_m2661967309(__this, method) ((  void (*) (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 *, const MethodInfo*))U3CLoadPictureAPIU3Ec__AnonStorey4__ctor_m2661967309_gshared)(__this, method)
// System.Void FBManager`1/<LoadPictureAPI>c__AnonStorey4<System.Object>::<>m__0(Facebook.Unity.IGraphResult)
extern "C"  void U3CLoadPictureAPIU3Ec__AnonStorey4_U3CU3Em__0_m1328248257_gshared (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define U3CLoadPictureAPIU3Ec__AnonStorey4_U3CU3Em__0_m1328248257(__this, ___result0, method) ((  void (*) (U3CLoadPictureAPIU3Ec__AnonStorey4_t17207354 *, Il2CppObject *, const MethodInfo*))U3CLoadPictureAPIU3Ec__AnonStorey4_U3CU3Em__0_m1328248257_gshared)(__this, ___result0, method)
