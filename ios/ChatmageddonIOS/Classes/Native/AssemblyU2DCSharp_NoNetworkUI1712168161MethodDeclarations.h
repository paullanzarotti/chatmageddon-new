﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NoNetworkUI
struct NoNetworkUI_t1712168161;

#include "codegen/il2cpp-codegen.h"

// System.Void NoNetworkUI::.ctor()
extern "C"  void NoNetworkUI__ctor_m2909903632 (NoNetworkUI_t1712168161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUI::Awake()
extern "C"  void NoNetworkUI_Awake_m1709004161 (NoNetworkUI_t1712168161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUI::LoadUI(System.Boolean)
extern "C"  void NoNetworkUI_LoadUI_m4186780521 (NoNetworkUI_t1712168161 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUI::UnloadUI(System.Boolean)
extern "C"  void NoNetworkUI_UnloadUI_m2933782094 (NoNetworkUI_t1712168161 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUI::BackgroundColourTweenFinished()
extern "C"  void NoNetworkUI_BackgroundColourTweenFinished_m1888712145 (NoNetworkUI_t1712168161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUI::BackgroundTweenFinished()
extern "C"  void NoNetworkUI_BackgroundTweenFinished_m4025682661 (NoNetworkUI_t1712168161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUI::ContentTweenFinished()
extern "C"  void NoNetworkUI_ContentTweenFinished_m4169702644 (NoNetworkUI_t1712168161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NoNetworkUI::SetUISceneSprite()
extern "C"  void NoNetworkUI_SetUISceneSprite_m3774899045 (NoNetworkUI_t1712168161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
