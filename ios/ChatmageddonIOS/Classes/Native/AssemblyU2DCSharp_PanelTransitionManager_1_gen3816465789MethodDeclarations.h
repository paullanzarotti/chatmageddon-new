﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelTransitionManager`1<System.Object>
struct PanelTransitionManager_1_t3816465789;
// Panel
struct Panel_t1787746694;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// TweenPosition
struct TweenPosition_t1144714832;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "AssemblyU2DCSharp_Panel1787746694.h"
#include "AssemblyU2DCSharp_TweenPosition1144714832.h"

// System.Void PanelTransitionManager`1<System.Object>::.ctor()
extern "C"  void PanelTransitionManager_1__ctor_m3470287956_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method);
#define PanelTransitionManager_1__ctor_m3470287956(__this, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, const MethodInfo*))PanelTransitionManager_1__ctor_m3470287956_gshared)(__this, method)
// System.Void PanelTransitionManager`1<System.Object>::Initialise()
extern "C"  void PanelTransitionManager_1_Initialise_m3802373897_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method);
#define PanelTransitionManager_1_Initialise_m3802373897(__this, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, const MethodInfo*))PanelTransitionManager_1_Initialise_m3802373897_gshared)(__this, method)
// System.Void PanelTransitionManager`1<System.Object>::AddPanel(PanelType,Panel)
extern "C"  void PanelTransitionManager_1_AddPanel_m2443272611_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panelType0, Panel_t1787746694 * ___panelToAdd1, const MethodInfo* method);
#define PanelTransitionManager_1_AddPanel_m2443272611(__this, ___panelType0, ___panelToAdd1, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, int32_t, Panel_t1787746694 *, const MethodInfo*))PanelTransitionManager_1_AddPanel_m2443272611_gshared)(__this, ___panelType0, ___panelToAdd1, method)
// System.Void PanelTransitionManager`1<System.Object>::RemovePanel(PanelType)
extern "C"  void PanelTransitionManager_1_RemovePanel_m2528126402_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panelType0, const MethodInfo* method);
#define PanelTransitionManager_1_RemovePanel_m2528126402(__this, ___panelType0, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, int32_t, const MethodInfo*))PanelTransitionManager_1_RemovePanel_m2528126402_gshared)(__this, ___panelType0, method)
// System.Boolean PanelTransitionManager`1<System.Object>::get_PanelActive()
extern "C"  bool PanelTransitionManager_1_get_PanelActive_m3779807383_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method);
#define PanelTransitionManager_1_get_PanelActive_m3779807383(__this, method) ((  bool (*) (PanelTransitionManager_1_t3816465789 *, const MethodInfo*))PanelTransitionManager_1_get_PanelActive_m3779807383_gshared)(__this, method)
// System.Void PanelTransitionManager`1<System.Object>::TogglePanel(PanelType,System.Boolean)
extern "C"  void PanelTransitionManager_1_TogglePanel_m438349725_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panel0, bool ___switching1, const MethodInfo* method);
#define PanelTransitionManager_1_TogglePanel_m438349725(__this, ___panel0, ___switching1, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, int32_t, bool, const MethodInfo*))PanelTransitionManager_1_TogglePanel_m438349725_gshared)(__this, ___panel0, ___switching1, method)
// System.Void PanelTransitionManager`1<System.Object>::ActivatePanel(Panel)
extern "C"  void PanelTransitionManager_1_ActivatePanel_m1159509373_gshared (PanelTransitionManager_1_t3816465789 * __this, Panel_t1787746694 * ___passed_Panel0, const MethodInfo* method);
#define PanelTransitionManager_1_ActivatePanel_m1159509373(__this, ___passed_Panel0, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, Panel_t1787746694 *, const MethodInfo*))PanelTransitionManager_1_ActivatePanel_m1159509373_gshared)(__this, ___passed_Panel0, method)
// System.Void PanelTransitionManager`1<System.Object>::DeActivatePanel(Panel)
extern "C"  void PanelTransitionManager_1_DeActivatePanel_m413338124_gshared (PanelTransitionManager_1_t3816465789 * __this, Panel_t1787746694 * ___passed_Panel0, const MethodInfo* method);
#define PanelTransitionManager_1_DeActivatePanel_m413338124(__this, ___passed_Panel0, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, Panel_t1787746694 *, const MethodInfo*))PanelTransitionManager_1_DeActivatePanel_m413338124_gshared)(__this, ___passed_Panel0, method)
// System.Void PanelTransitionManager`1<System.Object>::DeActivateAllPanels()
extern "C"  void PanelTransitionManager_1_DeActivateAllPanels_m3808037922_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method);
#define PanelTransitionManager_1_DeActivateAllPanels_m3808037922(__this, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, const MethodInfo*))PanelTransitionManager_1_DeActivateAllPanels_m3808037922_gshared)(__this, method)
// System.Void PanelTransitionManager`1<System.Object>::ToggleUI()
extern "C"  void PanelTransitionManager_1_ToggleUI_m2580260392_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method);
#define PanelTransitionManager_1_ToggleUI_m2580260392(__this, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, const MethodInfo*))PanelTransitionManager_1_ToggleUI_m2580260392_gshared)(__this, method)
// System.Collections.IEnumerator PanelTransitionManager`1<System.Object>::SwitchPanels()
extern "C"  Il2CppObject * PanelTransitionManager_1_SwitchPanels_m1400862637_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method);
#define PanelTransitionManager_1_SwitchPanels_m1400862637(__this, method) ((  Il2CppObject * (*) (PanelTransitionManager_1_t3816465789 *, const MethodInfo*))PanelTransitionManager_1_SwitchPanels_m1400862637_gshared)(__this, method)
// System.Void PanelTransitionManager`1<System.Object>::TransitionObject(TweenPosition,System.Boolean)
extern "C"  void PanelTransitionManager_1_TransitionObject_m1292715937_gshared (PanelTransitionManager_1_t3816465789 * __this, TweenPosition_t1144714832 * ___tweenPos0, bool ___isHiding1, const MethodInfo* method);
#define PanelTransitionManager_1_TransitionObject_m1292715937(__this, ___tweenPos0, ___isHiding1, method) ((  void (*) (PanelTransitionManager_1_t3816465789 *, TweenPosition_t1144714832 *, bool, const MethodInfo*))PanelTransitionManager_1_TransitionObject_m1292715937_gshared)(__this, ___tweenPos0, ___isHiding1, method)
// System.Collections.IEnumerator PanelTransitionManager`1<System.Object>::TweenObjectForwards(TweenPosition)
extern "C"  Il2CppObject * PanelTransitionManager_1_TweenObjectForwards_m3624951864_gshared (PanelTransitionManager_1_t3816465789 * __this, TweenPosition_t1144714832 * ___tweenPos0, const MethodInfo* method);
#define PanelTransitionManager_1_TweenObjectForwards_m3624951864(__this, ___tweenPos0, method) ((  Il2CppObject * (*) (PanelTransitionManager_1_t3816465789 *, TweenPosition_t1144714832 *, const MethodInfo*))PanelTransitionManager_1_TweenObjectForwards_m3624951864_gshared)(__this, ___tweenPos0, method)
// Panel PanelTransitionManager`1<System.Object>::GetActivePanel()
extern "C"  Panel_t1787746694 * PanelTransitionManager_1_GetActivePanel_m1623511417_gshared (PanelTransitionManager_1_t3816465789 * __this, const MethodInfo* method);
#define PanelTransitionManager_1_GetActivePanel_m1623511417(__this, method) ((  Panel_t1787746694 * (*) (PanelTransitionManager_1_t3816465789 *, const MethodInfo*))PanelTransitionManager_1_GetActivePanel_m1623511417_gshared)(__this, method)
// Panel PanelTransitionManager`1<System.Object>::GetPanel(PanelType)
extern "C"  Panel_t1787746694 * PanelTransitionManager_1_GetPanel_m2500208523_gshared (PanelTransitionManager_1_t3816465789 * __this, int32_t ___panelType0, const MethodInfo* method);
#define PanelTransitionManager_1_GetPanel_m2500208523(__this, ___panelType0, method) ((  Panel_t1787746694 * (*) (PanelTransitionManager_1_t3816465789 *, int32_t, const MethodInfo*))PanelTransitionManager_1_GetPanel_m2500208523_gshared)(__this, ___panelType0, method)
