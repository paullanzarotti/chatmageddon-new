﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ShieldManager>::.ctor()
#define MonoSingleton_1__ctor_m1729278870(__this, method) ((  void (*) (MonoSingleton_1_t3302319956 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ShieldManager>::Awake()
#define MonoSingleton_1_Awake_m4140573649(__this, method) ((  void (*) (MonoSingleton_1_t3302319956 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ShieldManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m972225467(__this /* static, unused */, method) ((  ShieldManager_t3551654236 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ShieldManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3326952107(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ShieldManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3371916028(__this, method) ((  void (*) (MonoSingleton_1_t3302319956 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ShieldManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1184126984(__this, method) ((  void (*) (MonoSingleton_1_t3302319956 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ShieldManager>::.cctor()
#define MonoSingleton_1__cctor_m424229219(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
