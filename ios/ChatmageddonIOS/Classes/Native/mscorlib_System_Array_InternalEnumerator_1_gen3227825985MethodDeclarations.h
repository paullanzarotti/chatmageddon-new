﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3227825985.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22369073723.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2931000688_gshared (InternalEnumerator_1_t3227825985 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2931000688(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3227825985 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2931000688_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1625428076_gshared (InternalEnumerator_1_t3227825985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1625428076(__this, method) ((  void (*) (InternalEnumerator_1_t3227825985 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1625428076_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3784775506_gshared (InternalEnumerator_1_t3227825985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3784775506(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3227825985 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3784775506_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3265248917_gshared (InternalEnumerator_1_t3227825985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3265248917(__this, method) ((  void (*) (InternalEnumerator_1_t3227825985 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3265248917_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m68391488_gshared (InternalEnumerator_1_t3227825985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m68391488(__this, method) ((  bool (*) (InternalEnumerator_1_t3227825985 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m68391488_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Decimal>>::get_Current()
extern "C"  KeyValuePair_2_t2369073723  InternalEnumerator_1_get_Current_m867531301_gshared (InternalEnumerator_1_t3227825985 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m867531301(__this, method) ((  KeyValuePair_2_t2369073723  (*) (InternalEnumerator_1_t3227825985 *, const MethodInfo*))InternalEnumerator_1_get_Current_m867531301_gshared)(__this, method)
