﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.OSXStoreKitPluginImpl
struct OSXStoreKitPluginImpl_t3020431434;
// Unibill.Impl.AppleAppStoreBillingService
struct AppleAppStoreBillingService_t956296338;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_AppleApp956296338.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.OSXStoreKitPluginImpl::.ctor()
extern "C"  void OSXStoreKitPluginImpl__ctor_m1689502412 (OSXStoreKitPluginImpl_t3020431434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::initialise(Unibill.Impl.AppleAppStoreBillingService)
extern "C"  void OSXStoreKitPluginImpl_initialise_m3351347082 (OSXStoreKitPluginImpl_t3020431434 * __this, AppleAppStoreBillingService_t956296338 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.OSXStoreKitPluginImpl::storeKitPaymentsAvailable()
extern "C"  bool OSXStoreKitPluginImpl_storeKitPaymentsAvailable_m3176735189 (OSXStoreKitPluginImpl_t3020431434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::storeKitRequestProductData(System.String,System.String[])
extern "C"  void OSXStoreKitPluginImpl_storeKitRequestProductData_m831010773 (OSXStoreKitPluginImpl_t3020431434 * __this, String_t* ___productIdentifiers0, StringU5BU5D_t1642385972* ___productIds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::storeKitPurchaseProduct(System.String)
extern "C"  void OSXStoreKitPluginImpl_storeKitPurchaseProduct_m1485725775 (OSXStoreKitPluginImpl_t3020431434 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::storeKitRestoreTransactions()
extern "C"  void OSXStoreKitPluginImpl_storeKitRestoreTransactions_m130270766 (OSXStoreKitPluginImpl_t3020431434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::addTransactionObserver()
extern "C"  void OSXStoreKitPluginImpl_addTransactionObserver_m1042686885 (OSXStoreKitPluginImpl_t3020431434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::UnibillSendMessage(System.String,System.String)
extern "C"  void OSXStoreKitPluginImpl_UnibillSendMessage_m709899456 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___argument1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::onProductListReceived(System.String)
extern "C"  void OSXStoreKitPluginImpl_onProductListReceived_m4249850169 (Il2CppObject * __this /* static, unused */, String_t* ___productList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::onProductPurchaseSuccess(System.String)
extern "C"  void OSXStoreKitPluginImpl_onProductPurchaseSuccess_m2482625322 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::onProductPurchaseCancelled(System.String)
extern "C"  void OSXStoreKitPluginImpl_onProductPurchaseCancelled_m3915684254 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::onProductPurchaseFailed(System.String)
extern "C"  void OSXStoreKitPluginImpl_onProductPurchaseFailed_m1882837040 (Il2CppObject * __this /* static, unused */, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::onTransactionsRestoredSuccess(System.String)
extern "C"  void OSXStoreKitPluginImpl_onTransactionsRestoredSuccess_m1843499163 (Il2CppObject * __this /* static, unused */, String_t* ___empty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::onTransactionsRestoredFail(System.String)
extern "C"  void OSXStoreKitPluginImpl_onTransactionsRestoredFail_m2979002302 (Il2CppObject * __this /* static, unused */, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.OSXStoreKitPluginImpl::onFailedToRetrieveProductList(System.String)
extern "C"  void OSXStoreKitPluginImpl_onFailedToRetrieveProductList_m1681915306 (Il2CppObject * __this /* static, unused */, String_t* ___nop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
