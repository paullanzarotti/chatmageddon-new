﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// RecursiveCharacterPopulator/OnPopulationUpdate
struct OnPopulationUpdate_t30122867;
// RecursiveCharacterPopulator/OnPopulationComplete
struct OnPopulationComplete_t1952569739;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecursiveCharacterPopulator
struct  RecursiveCharacterPopulator_t271140971  : public MonoBehaviour_t1158329972
{
public:
	// System.String RecursiveCharacterPopulator::caretChoice
	String_t* ___caretChoice_2;
	// System.Single RecursiveCharacterPopulator::timeBetweenCharacters
	float ___timeBetweenCharacters_3;
	// RecursiveCharacterPopulator/OnPopulationUpdate RecursiveCharacterPopulator::onPopulationUpdate
	OnPopulationUpdate_t30122867 * ___onPopulationUpdate_4;
	// RecursiveCharacterPopulator/OnPopulationComplete RecursiveCharacterPopulator::onPopulationComplete
	OnPopulationComplete_t1952569739 * ___onPopulationComplete_5;
	// UnityEngine.Coroutine RecursiveCharacterPopulator::populator
	Coroutine_t2299508840 * ___populator_6;

public:
	inline static int32_t get_offset_of_caretChoice_2() { return static_cast<int32_t>(offsetof(RecursiveCharacterPopulator_t271140971, ___caretChoice_2)); }
	inline String_t* get_caretChoice_2() const { return ___caretChoice_2; }
	inline String_t** get_address_of_caretChoice_2() { return &___caretChoice_2; }
	inline void set_caretChoice_2(String_t* value)
	{
		___caretChoice_2 = value;
		Il2CppCodeGenWriteBarrier(&___caretChoice_2, value);
	}

	inline static int32_t get_offset_of_timeBetweenCharacters_3() { return static_cast<int32_t>(offsetof(RecursiveCharacterPopulator_t271140971, ___timeBetweenCharacters_3)); }
	inline float get_timeBetweenCharacters_3() const { return ___timeBetweenCharacters_3; }
	inline float* get_address_of_timeBetweenCharacters_3() { return &___timeBetweenCharacters_3; }
	inline void set_timeBetweenCharacters_3(float value)
	{
		___timeBetweenCharacters_3 = value;
	}

	inline static int32_t get_offset_of_onPopulationUpdate_4() { return static_cast<int32_t>(offsetof(RecursiveCharacterPopulator_t271140971, ___onPopulationUpdate_4)); }
	inline OnPopulationUpdate_t30122867 * get_onPopulationUpdate_4() const { return ___onPopulationUpdate_4; }
	inline OnPopulationUpdate_t30122867 ** get_address_of_onPopulationUpdate_4() { return &___onPopulationUpdate_4; }
	inline void set_onPopulationUpdate_4(OnPopulationUpdate_t30122867 * value)
	{
		___onPopulationUpdate_4 = value;
		Il2CppCodeGenWriteBarrier(&___onPopulationUpdate_4, value);
	}

	inline static int32_t get_offset_of_onPopulationComplete_5() { return static_cast<int32_t>(offsetof(RecursiveCharacterPopulator_t271140971, ___onPopulationComplete_5)); }
	inline OnPopulationComplete_t1952569739 * get_onPopulationComplete_5() const { return ___onPopulationComplete_5; }
	inline OnPopulationComplete_t1952569739 ** get_address_of_onPopulationComplete_5() { return &___onPopulationComplete_5; }
	inline void set_onPopulationComplete_5(OnPopulationComplete_t1952569739 * value)
	{
		___onPopulationComplete_5 = value;
		Il2CppCodeGenWriteBarrier(&___onPopulationComplete_5, value);
	}

	inline static int32_t get_offset_of_populator_6() { return static_cast<int32_t>(offsetof(RecursiveCharacterPopulator_t271140971, ___populator_6)); }
	inline Coroutine_t2299508840 * get_populator_6() const { return ___populator_6; }
	inline Coroutine_t2299508840 ** get_address_of_populator_6() { return &___populator_6; }
	inline void set_populator_6(Coroutine_t2299508840 * value)
	{
		___populator_6 = value;
		Il2CppCodeGenWriteBarrier(&___populator_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
