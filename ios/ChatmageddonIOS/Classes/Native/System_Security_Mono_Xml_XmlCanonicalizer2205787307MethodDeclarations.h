﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.XmlCanonicalizer
struct XmlCanonicalizer_t2205787307;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void Mono.Xml.XmlCanonicalizer::.ctor(System.Boolean,System.Boolean,System.Collections.Hashtable)
extern "C"  void XmlCanonicalizer__ctor_m643734415 (XmlCanonicalizer_t2205787307 * __this, bool ___withComments0, bool ___excC14N1, Hashtable_t909839986 * ___propagatedNamespaces2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
