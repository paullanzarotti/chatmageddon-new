﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.GenericEnumConverter
struct GenericEnumConverter_t1696720900;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.ComponentModel.ITypeDescriptorContext
struct ITypeDescriptorContext_t3633625151;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Configuration.GenericEnumConverter::.ctor(System.Type)
extern "C"  void GenericEnumConverter__ctor_m1329524312 (GenericEnumConverter_t1696720900 * __this, Type_t * ___typeEnum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.GenericEnumConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern "C"  Il2CppObject * GenericEnumConverter_ConvertFrom_m2747302547 (GenericEnumConverter_t1696720900 * __this, Il2CppObject * ___ctx0, CultureInfo_t3500843524 * ___ci1, Il2CppObject * ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.GenericEnumConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern "C"  Il2CppObject * GenericEnumConverter_ConvertTo_m3781330013 (GenericEnumConverter_t1696720900 * __this, Il2CppObject * ___ctx0, CultureInfo_t3500843524 * ___ci1, Il2CppObject * ___value2, Type_t * ___type3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
