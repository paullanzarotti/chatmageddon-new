﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenPanelClipRange
struct TweenPanelClipRange_t596408274;
// TweenScale
struct TweenScale_t2697902175;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void TweenPanelClipRange::.ctor()
extern "C"  void TweenPanelClipRange__ctor_m4013397249 (TweenPanelClipRange_t596408274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPanelClipRange::Awake()
extern "C"  void TweenPanelClipRange_Awake_m3906675666 (TweenPanelClipRange_t596408274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPanelClipRange::OnUpdate(System.Single,System.Boolean)
extern "C"  void TweenPanelClipRange_OnUpdate_m2171010219 (TweenPanelClipRange_t596408274 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenScale TweenPanelClipRange::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Vector3)
extern "C"  TweenScale_t2697902175 * TweenPanelClipRange_Begin_m3072220952 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, float ___duration1, Vector3_t2243707580  ___scale2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
