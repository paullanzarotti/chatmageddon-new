﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusListItemUIScaler
struct StatusListItemUIScaler_t1403897979;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusListItemUIScaler::.ctor()
extern "C"  void StatusListItemUIScaler__ctor_m1681166082 (StatusListItemUIScaler_t1403897979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItemUIScaler::GlobalUIScale()
extern "C"  void StatusListItemUIScaler_GlobalUIScale_m912910749 (StatusListItemUIScaler_t1403897979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
