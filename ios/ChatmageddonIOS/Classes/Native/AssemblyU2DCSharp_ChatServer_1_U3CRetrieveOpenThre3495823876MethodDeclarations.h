﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1/<RetrieveOpenThreadInfo>c__AnonStorey2<System.Object>
struct U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t3495823876;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatServer`1/<RetrieveOpenThreadInfo>c__AnonStorey2<System.Object>::.ctor()
extern "C"  void U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2__ctor_m19448395_gshared (U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t3495823876 * __this, const MethodInfo* method);
#define U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2__ctor_m19448395(__this, method) ((  void (*) (U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t3495823876 *, const MethodInfo*))U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2__ctor_m19448395_gshared)(__this, method)
// System.Void ChatServer`1/<RetrieveOpenThreadInfo>c__AnonStorey2<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_U3CU3Em__0_m74664360_gshared (U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t3495823876 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_U3CU3Em__0_m74664360(__this, ___request0, ___response1, method) ((  void (*) (U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_t3495823876 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRetrieveOpenThreadInfoU3Ec__AnonStorey2_U3CU3Em__0_m74664360_gshared)(__this, ___request0, ___response1, method)
