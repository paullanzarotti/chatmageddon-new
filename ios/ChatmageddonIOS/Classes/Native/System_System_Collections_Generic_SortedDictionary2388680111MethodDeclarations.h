﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_420481023MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3633969902(__this, ___dic0, method) ((  void (*) (Enumerator_t2388680111 *, SortedDictionary_2_t1020118611 *, const MethodInfo*))Enumerator__ctor_m3766456554_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4284637711(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3694502715_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1739347816(__this, method) ((  Il2CppObject * (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m917593492_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m557488110(__this, method) ((  Il2CppObject * (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2383712954_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4105264818(__this, method) ((  Il2CppObject * (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2855845382_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1222238700(__this, method) ((  void (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2384313008_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::get_Current()
#define Enumerator_get_Current_m1307830468(__this, method) ((  KeyValuePair_2_t1422819240  (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_get_Current_m1200532472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::MoveNext()
#define Enumerator_MoveNext_m4258815468(__this, method) ((  bool (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_MoveNext_m4279185496_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::Dispose()
#define Enumerator_Dispose_m3622319309(__this, method) ((  void (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_Dispose_m3555749825_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::get_CurrentNode()
#define Enumerator_get_CurrentNode_m402984096(__this, method) ((  Node_t3897414839 * (*) (Enumerator_t2388680111 *, const MethodInfo*))Enumerator_get_CurrentNode_m1248168108_gshared)(__this, method)
