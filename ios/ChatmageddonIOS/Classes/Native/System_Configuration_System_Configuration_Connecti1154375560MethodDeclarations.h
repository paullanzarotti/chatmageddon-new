﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConnectionStringSettings
struct ConnectionStringSettings_t1154375560;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.ConnectionStringSettings::.ctor()
extern "C"  void ConnectionStringSettings__ctor_m1652760251 (ConnectionStringSettings_t1154375560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettings::.ctor(System.String,System.String)
extern "C"  void ConnectionStringSettings__ctor_m3226609931 (ConnectionStringSettings_t1154375560 * __this, String_t* ___name0, String_t* ___connectionString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettings::.ctor(System.String,System.String,System.String)
extern "C"  void ConnectionStringSettings__ctor_m1304839113 (ConnectionStringSettings_t1154375560 * __this, String_t* ___name0, String_t* ___connectionString1, String_t* ___providerName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettings::.cctor()
extern "C"  void ConnectionStringSettings__cctor_m1552675530 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConnectionStringSettings::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * ConnectionStringSettings_get_Properties_m1374512976 (ConnectionStringSettings_t1154375560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConnectionStringSettings::get_Name()
extern "C"  String_t* ConnectionStringSettings_get_Name_m2898889358 (ConnectionStringSettings_t1154375560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettings::set_Name(System.String)
extern "C"  void ConnectionStringSettings_set_Name_m1097965551 (ConnectionStringSettings_t1154375560 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConnectionStringSettings::get_ProviderName()
extern "C"  String_t* ConnectionStringSettings_get_ProviderName_m3190135425 (ConnectionStringSettings_t1154375560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettings::set_ProviderName(System.String)
extern "C"  void ConnectionStringSettings_set_ProviderName_m2101863878 (ConnectionStringSettings_t1154375560 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConnectionStringSettings::get_ConnectionString()
extern "C"  String_t* ConnectionStringSettings_get_ConnectionString_m2159232480 (ConnectionStringSettings_t1154375560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConnectionStringSettings::set_ConnectionString(System.String)
extern "C"  void ConnectionStringSettings_set_ConnectionString_m2497789983 (ConnectionStringSettings_t1154375560 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConnectionStringSettings::ToString()
extern "C"  String_t* ConnectionStringSettings_ToString_m2337696028 (ConnectionStringSettings_t1154375560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
