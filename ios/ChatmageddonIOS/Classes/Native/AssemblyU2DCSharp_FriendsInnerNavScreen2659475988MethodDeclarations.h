﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsInnerNavScreen
struct FriendsInnerNavScreen_t2659475988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendsInnerNavScreen::.ctor()
extern "C"  void FriendsInnerNavScreen__ctor_m714816893 (FriendsInnerNavScreen_t2659475988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsInnerNavScreen::Start()
extern "C"  void FriendsInnerNavScreen_Start_m2822550533 (FriendsInnerNavScreen_t2659475988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsInnerNavScreen::UIClosing()
extern "C"  void FriendsInnerNavScreen_UIClosing_m3996874422 (FriendsInnerNavScreen_t2659475988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsInnerNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void FriendsInnerNavScreen_ScreenLoad_m3998880143 (FriendsInnerNavScreen_t2659475988 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsInnerNavScreen::UpdateUI()
extern "C"  void FriendsInnerNavScreen_UpdateUI_m1800309328 (FriendsInnerNavScreen_t2659475988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsInnerNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void FriendsInnerNavScreen_ScreenUnload_m2919269503 (FriendsInnerNavScreen_t2659475988 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsInnerNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool FriendsInnerNavScreen_ValidateScreenNavigation_m2867501676 (FriendsInnerNavScreen_t2659475988 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
