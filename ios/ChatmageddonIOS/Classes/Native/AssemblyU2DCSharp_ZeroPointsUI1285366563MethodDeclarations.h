﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZeroPointsUI
struct ZeroPointsUI_t1285366563;

#include "codegen/il2cpp-codegen.h"

// System.Void ZeroPointsUI::.ctor()
extern "C"  void ZeroPointsUI__ctor_m2510471974 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::OnEnable()
extern "C"  void ZeroPointsUI_OnEnable_m1289514338 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::OnDisable()
extern "C"  void ZeroPointsUI_OnDisable_m1481135407 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::Awake()
extern "C"  void ZeroPointsUI_Awake_m1881537779 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::CalculatorHitZero()
extern "C"  void ZeroPointsUI_CalculatorHitZero_m1398515437 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::SetKnockedOutByLabel()
extern "C"  void ZeroPointsUI_SetKnockedOutByLabel_m4226317228 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::LoadUI(System.Boolean)
extern "C"  void ZeroPointsUI_LoadUI_m1572827343 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::UnloadUI(System.Boolean)
extern "C"  void ZeroPointsUI_UnloadUI_m2092964220 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::ShowUI(System.Boolean,System.Boolean)
extern "C"  void ZeroPointsUI_ShowUI_m4104334781 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, bool ___reset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::HideUI(System.Boolean)
extern "C"  void ZeroPointsUI_HideUI_m2304286909 (ZeroPointsUI_t1285366563 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::BackgroundTweenFinished()
extern "C"  void ZeroPointsUI_BackgroundTweenFinished_m2129898799 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZeroPointsUI::ContentTweenFinished()
extern "C"  void ZeroPointsUI_ContentTweenFinished_m997736054 (ZeroPointsUI_t1285366563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
