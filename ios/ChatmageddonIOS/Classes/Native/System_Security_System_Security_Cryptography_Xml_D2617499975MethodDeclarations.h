﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.DataReference
struct DataReference_t2617499975;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.Xml.DataReference::.ctor()
extern "C"  void DataReference__ctor_m3399158853 (DataReference_t2617499975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
