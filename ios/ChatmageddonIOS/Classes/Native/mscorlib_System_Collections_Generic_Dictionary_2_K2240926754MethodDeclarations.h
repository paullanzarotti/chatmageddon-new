﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PanelType,System.Object>
struct Dictionary_2_t3846390612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2240926754.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PanelType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3551768021_gshared (Enumerator_t2240926754 * __this, Dictionary_2_t3846390612 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3551768021(__this, ___host0, method) ((  void (*) (Enumerator_t2240926754 *, Dictionary_2_t3846390612 *, const MethodInfo*))Enumerator__ctor_m3551768021_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PanelType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m5570560_gshared (Enumerator_t2240926754 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m5570560(__this, method) ((  Il2CppObject * (*) (Enumerator_t2240926754 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m5570560_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PanelType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m949904832_gshared (Enumerator_t2240926754 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m949904832(__this, method) ((  void (*) (Enumerator_t2240926754 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m949904832_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PanelType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3879787085_gshared (Enumerator_t2240926754 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3879787085(__this, method) ((  void (*) (Enumerator_t2240926754 *, const MethodInfo*))Enumerator_Dispose_m3879787085_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PanelType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3570806140_gshared (Enumerator_t2240926754 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3570806140(__this, method) ((  bool (*) (Enumerator_t2240926754 *, const MethodInfo*))Enumerator_MoveNext_m3570806140_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PanelType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1722924324_gshared (Enumerator_t2240926754 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1722924324(__this, method) ((  int32_t (*) (Enumerator_t2240926754 *, const MethodInfo*))Enumerator_get_Current_m1722924324_gshared)(__this, method)
