﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenderSwitch
struct GenderSwitch_t696294539;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"

// System.Void GenderSwitch::.ctor()
extern "C"  void GenderSwitch__ctor_m1554218664 (GenderSwitch_t696294539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderSwitch::OnSwitch(Gender)
extern "C"  void GenderSwitch_OnSwitch_m1161421000 (GenderSwitch_t696294539 * __this, int32_t ___newState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
