﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenderNavScreen
struct GenderNavScreen_t2608406338;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void GenderNavScreen::.ctor()
extern "C"  void GenderNavScreen__ctor_m1532970921 (GenderNavScreen_t2608406338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderNavScreen::Start()
extern "C"  void GenderNavScreen_Start_m3800795721 (GenderNavScreen_t2608406338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderNavScreen::UIClosing()
extern "C"  void GenderNavScreen_UIClosing_m1070346572 (GenderNavScreen_t2608406338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void GenderNavScreen_ScreenLoad_m2786369187 (GenderNavScreen_t2608406338 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void GenderNavScreen_ScreenUnload_m3055666867 (GenderNavScreen_t2608406338 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GenderNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool GenderNavScreen_ValidateScreenNavigation_m1490942614 (GenderNavScreen_t2608406338 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GenderNavScreen::SaveScreenContent(NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * GenderNavScreen_SaveScreenContent_m1152799981 (GenderNavScreen_t2608406338 * __this, int32_t ___direction0, Action_1_t3627374100 * ___savedAction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
