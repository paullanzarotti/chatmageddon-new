﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Func`2<UnityEngine.Vector2,System.Boolean>
struct Func_2_t1033119496;

#include "AssemblyU2DCSharp_OnlineMapsDrawingElement539447654.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsDrawingRect
struct  OnlineMapsDrawingRect_t2548315780  : public OnlineMapsDrawingElement_t539447654
{
public:
	// UnityEngine.Color OnlineMapsDrawingRect::backgroundColor
	Color_t2020392075  ___backgroundColor_21;
	// UnityEngine.Color OnlineMapsDrawingRect::borderColor
	Color_t2020392075  ___borderColor_22;
	// System.Single OnlineMapsDrawingRect::borderWeight
	float ___borderWeight_23;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> OnlineMapsDrawingRect::points
	List_1_t1612828711 * ___points_24;
	// System.Single OnlineMapsDrawingRect::_height
	float ____height_25;
	// System.Single OnlineMapsDrawingRect::_width
	float ____width_26;
	// System.Single OnlineMapsDrawingRect::_x
	float ____x_27;
	// System.Single OnlineMapsDrawingRect::_y
	float ____y_28;

public:
	inline static int32_t get_offset_of_backgroundColor_21() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ___backgroundColor_21)); }
	inline Color_t2020392075  get_backgroundColor_21() const { return ___backgroundColor_21; }
	inline Color_t2020392075 * get_address_of_backgroundColor_21() { return &___backgroundColor_21; }
	inline void set_backgroundColor_21(Color_t2020392075  value)
	{
		___backgroundColor_21 = value;
	}

	inline static int32_t get_offset_of_borderColor_22() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ___borderColor_22)); }
	inline Color_t2020392075  get_borderColor_22() const { return ___borderColor_22; }
	inline Color_t2020392075 * get_address_of_borderColor_22() { return &___borderColor_22; }
	inline void set_borderColor_22(Color_t2020392075  value)
	{
		___borderColor_22 = value;
	}

	inline static int32_t get_offset_of_borderWeight_23() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ___borderWeight_23)); }
	inline float get_borderWeight_23() const { return ___borderWeight_23; }
	inline float* get_address_of_borderWeight_23() { return &___borderWeight_23; }
	inline void set_borderWeight_23(float value)
	{
		___borderWeight_23 = value;
	}

	inline static int32_t get_offset_of_points_24() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ___points_24)); }
	inline List_1_t1612828711 * get_points_24() const { return ___points_24; }
	inline List_1_t1612828711 ** get_address_of_points_24() { return &___points_24; }
	inline void set_points_24(List_1_t1612828711 * value)
	{
		___points_24 = value;
		Il2CppCodeGenWriteBarrier(&___points_24, value);
	}

	inline static int32_t get_offset_of__height_25() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ____height_25)); }
	inline float get__height_25() const { return ____height_25; }
	inline float* get_address_of__height_25() { return &____height_25; }
	inline void set__height_25(float value)
	{
		____height_25 = value;
	}

	inline static int32_t get_offset_of__width_26() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ____width_26)); }
	inline float get__width_26() const { return ____width_26; }
	inline float* get_address_of__width_26() { return &____width_26; }
	inline void set__width_26(float value)
	{
		____width_26 = value;
	}

	inline static int32_t get_offset_of__x_27() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ____x_27)); }
	inline float get__x_27() const { return ____x_27; }
	inline float* get_address_of__x_27() { return &____x_27; }
	inline void set__x_27(float value)
	{
		____x_27 = value;
	}

	inline static int32_t get_offset_of__y_28() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780, ____y_28)); }
	inline float get__y_28() const { return ____y_28; }
	inline float* get_address_of__y_28() { return &____y_28; }
	inline void set__y_28(float value)
	{
		____y_28 = value;
	}
};

struct OnlineMapsDrawingRect_t2548315780_StaticFields
{
public:
	// System.Func`2<UnityEngine.Vector2,System.Boolean> OnlineMapsDrawingRect::<>f__am$cache0
	Func_2_t1033119496 * ___U3CU3Ef__amU24cache0_29;
	// System.Func`2<UnityEngine.Vector2,System.Boolean> OnlineMapsDrawingRect::<>f__am$cache1
	Func_2_t1033119496 * ___U3CU3Ef__amU24cache1_30;
	// System.Func`2<UnityEngine.Vector2,System.Boolean> OnlineMapsDrawingRect::<>f__am$cache2
	Func_2_t1033119496 * ___U3CU3Ef__amU24cache2_31;
	// System.Func`2<UnityEngine.Vector2,System.Boolean> OnlineMapsDrawingRect::<>f__am$cache3
	Func_2_t1033119496 * ___U3CU3Ef__amU24cache3_32;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_29() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780_StaticFields, ___U3CU3Ef__amU24cache0_29)); }
	inline Func_2_t1033119496 * get_U3CU3Ef__amU24cache0_29() const { return ___U3CU3Ef__amU24cache0_29; }
	inline Func_2_t1033119496 ** get_address_of_U3CU3Ef__amU24cache0_29() { return &___U3CU3Ef__amU24cache0_29; }
	inline void set_U3CU3Ef__amU24cache0_29(Func_2_t1033119496 * value)
	{
		___U3CU3Ef__amU24cache0_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_30() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780_StaticFields, ___U3CU3Ef__amU24cache1_30)); }
	inline Func_2_t1033119496 * get_U3CU3Ef__amU24cache1_30() const { return ___U3CU3Ef__amU24cache1_30; }
	inline Func_2_t1033119496 ** get_address_of_U3CU3Ef__amU24cache1_30() { return &___U3CU3Ef__amU24cache1_30; }
	inline void set_U3CU3Ef__amU24cache1_30(Func_2_t1033119496 * value)
	{
		___U3CU3Ef__amU24cache1_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_31() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780_StaticFields, ___U3CU3Ef__amU24cache2_31)); }
	inline Func_2_t1033119496 * get_U3CU3Ef__amU24cache2_31() const { return ___U3CU3Ef__amU24cache2_31; }
	inline Func_2_t1033119496 ** get_address_of_U3CU3Ef__amU24cache2_31() { return &___U3CU3Ef__amU24cache2_31; }
	inline void set_U3CU3Ef__amU24cache2_31(Func_2_t1033119496 * value)
	{
		___U3CU3Ef__amU24cache2_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_32() { return static_cast<int32_t>(offsetof(OnlineMapsDrawingRect_t2548315780_StaticFields, ___U3CU3Ef__amU24cache3_32)); }
	inline Func_2_t1033119496 * get_U3CU3Ef__amU24cache3_32() const { return ___U3CU3Ef__amU24cache3_32; }
	inline Func_2_t1033119496 ** get_address_of_U3CU3Ef__amU24cache3_32() { return &___U3CU3Ef__amU24cache3_32; }
	inline void set_U3CU3Ef__amU24cache3_32(Func_2_t1033119496 * value)
	{
		___U3CU3Ef__amU24cache3_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_32, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
