﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.AreaCodeMapStorageStrategy
struct  AreaCodeMapStorageStrategy_t3709015996  : public Il2CppObject
{
public:
	// System.Int32 PhoneNumbers.AreaCodeMapStorageStrategy::numOfEntries
	int32_t ___numOfEntries_0;
	// System.Collections.Generic.List`1<System.Int32> PhoneNumbers.AreaCodeMapStorageStrategy::possibleLengths
	List_1_t1440998580 * ___possibleLengths_1;

public:
	inline static int32_t get_offset_of_numOfEntries_0() { return static_cast<int32_t>(offsetof(AreaCodeMapStorageStrategy_t3709015996, ___numOfEntries_0)); }
	inline int32_t get_numOfEntries_0() const { return ___numOfEntries_0; }
	inline int32_t* get_address_of_numOfEntries_0() { return &___numOfEntries_0; }
	inline void set_numOfEntries_0(int32_t value)
	{
		___numOfEntries_0 = value;
	}

	inline static int32_t get_offset_of_possibleLengths_1() { return static_cast<int32_t>(offsetof(AreaCodeMapStorageStrategy_t3709015996, ___possibleLengths_1)); }
	inline List_1_t1440998580 * get_possibleLengths_1() const { return ___possibleLengths_1; }
	inline List_1_t1440998580 ** get_address_of_possibleLengths_1() { return &___possibleLengths_1; }
	inline void set_possibleLengths_1(List_1_t1440998580 * value)
	{
		___possibleLengths_1 = value;
		Il2CppCodeGenWriteBarrier(&___possibleLengths_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
