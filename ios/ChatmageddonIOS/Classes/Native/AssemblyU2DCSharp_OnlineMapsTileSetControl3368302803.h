﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<OnlineMapsTile,UnityEngine.Material>
struct Action_2_t2903233652;
// OnlineMapsTileSetControl/CheckMarker2DVisibilityDelegate
struct CheckMarker2DVisibilityDelegate_t3802502927;
// System.Action`2<UnityEngine.Vector2,UnityEngine.Vector2>
struct Action_2_t3492767325;
// OnlineMapsTileSetControl/GetFlatMarkerOffsetYDelegate
struct GetFlatMarkerOffsetYDelegate_t860568800;
// System.Action
struct Action_t3226471752;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Shader
struct Shader_t2430389951;
// OnlineMapsRange
struct OnlineMapsRange_t3791609909;
// System.Collections.Generic.IComparer`1<OnlineMapsMarker>
struct IComparer_1_t1446629804;
// UnityEngine.Material
struct Material_t193706927;
// OnlineMapsVector2i
struct OnlineMapsVector2i_t2180897250;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Int16[0...,0...]
struct Int16U5BU2CU5D_t3104283264;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Collections.Generic.List`1<OnlineMapsTileSetControl/TilesetFlatMarker>
struct List_1_t2920006219;
// System.Func`2<System.String,System.Int16>
struct Func_2_t2205052638;
// System.Func`2<OnlineMapsMarker,OnlineMapsMarker>
struct Func_2_t3438359081;
// System.Func`2<OnlineMapsMarker,System.Single>
struct Func_2_t2022702331;
// System.Func`2<OnlineMapsTileSetControl/TilesetSortedMarker,System.Single>
struct Func_2_t1961126510;

#include "AssemblyU2DCSharp_OnlineMapsControlBase3D2801313279.h"
#include "AssemblyU2DCSharp_OnlineMapsTilesetCheckMarker2DVi1564781124.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_OnlineM3256055626.h"
#include "AssemblyU2DCSharp_OnlineMapsTileSetControl_OnlineM3481217700.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsTileSetControl
struct  OnlineMapsTileSetControl_t3368302803  : public OnlineMapsControlBase3D_t2801313279
{
public:
	// System.Action`2<OnlineMapsTile,UnityEngine.Material> OnlineMapsTileSetControl::OnChangeMaterialTexture
	Action_2_t2903233652 * ___OnChangeMaterialTexture_53;
	// OnlineMapsTileSetControl/CheckMarker2DVisibilityDelegate OnlineMapsTileSetControl::OnCheckMarker2DVisibility
	CheckMarker2DVisibilityDelegate_t3802502927 * ___OnCheckMarker2DVisibility_54;
	// System.Action`2<UnityEngine.Vector2,UnityEngine.Vector2> OnlineMapsTileSetControl::OnGetElevation
	Action_2_t3492767325 * ___OnGetElevation_55;
	// OnlineMapsTileSetControl/GetFlatMarkerOffsetYDelegate OnlineMapsTileSetControl::OnGetFlatMarkerOffsetY
	GetFlatMarkerOffsetYDelegate_t860568800 * ___OnGetFlatMarkerOffsetY_56;
	// System.Action OnlineMapsTileSetControl::OnSmoothZoomBegin
	Action_t3226471752 * ___OnSmoothZoomBegin_57;
	// System.Action OnlineMapsTileSetControl::OnSmoothZoomFinish
	Action_t3226471752 * ___OnSmoothZoomFinish_58;
	// System.Action OnlineMapsTileSetControl::OnSmoothZoomProcess
	Action_t3226471752 * ___OnSmoothZoomProcess_59;
	// System.String OnlineMapsTileSetControl::bingAPI
	String_t* ___bingAPI_60;
	// OnlineMapsTilesetCheckMarker2DVisibility OnlineMapsTileSetControl::checkMarker2DVisibility
	int32_t ___checkMarker2DVisibility_61;
	// OnlineMapsTileSetControl/OnlineMapsColliderType OnlineMapsTileSetControl::colliderType
	int32_t ___colliderType_62;
	// UnityEngine.GameObject OnlineMapsTileSetControl::drawingsGameObject
	GameObject_t1756533147 * ___drawingsGameObject_63;
	// UnityEngine.Shader OnlineMapsTileSetControl::drawingShader
	Shader_t2430389951 * ___drawingShader_64;
	// OnlineMapsRange OnlineMapsTileSetControl::elevationZoomRange
	OnlineMapsRange_t3791609909 * ___elevationZoomRange_65;
	// System.Single OnlineMapsTileSetControl::elevationScale
	float ___elevationScale_66;
	// System.Collections.Generic.IComparer`1<OnlineMapsMarker> OnlineMapsTileSetControl::markerComparer
	Il2CppObject* ___markerComparer_67;
	// UnityEngine.Material OnlineMapsTileSetControl::markerMaterial
	Material_t193706927 * ___markerMaterial_68;
	// UnityEngine.Shader OnlineMapsTileSetControl::markerShader
	Shader_t2430389951 * ___markerShader_69;
	// System.Boolean OnlineMapsTileSetControl::smoothZoom
	bool ___smoothZoom_70;
	// OnlineMapsTileSetControl/OnlineMapsSmoothZoomMode OnlineMapsTileSetControl::smoothZoomMode
	int32_t ___smoothZoomMode_71;
	// UnityEngine.Material OnlineMapsTileSetControl::tileMaterial
	Material_t193706927 * ___tileMaterial_72;
	// UnityEngine.Shader OnlineMapsTileSetControl::tilesetShader
	Shader_t2430389951 * ___tilesetShader_73;
	// System.Boolean OnlineMapsTileSetControl::useElevation
	bool ___useElevation_74;
	// System.Boolean OnlineMapsTileSetControl::_useElevation
	bool ____useElevation_75;
	// OnlineMapsVector2i OnlineMapsTileSetControl::_bufferPosition
	OnlineMapsVector2i_t2180897250 * ____bufferPosition_76;
	// UnityEngine.WWW OnlineMapsTileSetControl::elevationRequest
	WWW_t2919945039 * ___elevationRequest_77;
	// UnityEngine.Rect OnlineMapsTileSetControl::elevationRequestRect
	Rect_t3681755626  ___elevationRequestRect_78;
	// System.Int16[0...,0...] OnlineMapsTileSetControl::elevationData
	Int16U5BU2CU5D_t3104283264* ___elevationData_79;
	// UnityEngine.Rect OnlineMapsTileSetControl::elevationRect
	Rect_t3681755626  ___elevationRect_80;
	// UnityEngine.MeshCollider OnlineMapsTileSetControl::meshCollider
	MeshCollider_t2718867283 * ___meshCollider_81;
	// System.Boolean OnlineMapsTileSetControl::ignoreGetElevation
	bool ___ignoreGetElevation_82;
	// UnityEngine.Mesh OnlineMapsTileSetControl::tilesetMesh
	Mesh_t1356156583 * ___tilesetMesh_83;
	// System.Int32[] OnlineMapsTileSetControl::triangles
	Int32U5BU5D_t3030399641* ___triangles_84;
	// UnityEngine.Vector2[] OnlineMapsTileSetControl::uv
	Vector2U5BU5D_t686124026* ___uv_85;
	// UnityEngine.Vector3[] OnlineMapsTileSetControl::vertices
	Vector3U5BU5D_t1172311765* ___vertices_86;
	// OnlineMapsVector2i OnlineMapsTileSetControl::elevationBufferPosition
	OnlineMapsVector2i_t2180897250 * ___elevationBufferPosition_87;
	// System.Boolean OnlineMapsTileSetControl::smoothZoomStarted
	bool ___smoothZoomStarted_88;
	// UnityEngine.Vector2 OnlineMapsTileSetControl::smoothZoomPoint
	Vector2_t2243707579  ___smoothZoomPoint_89;
	// UnityEngine.Vector3 OnlineMapsTileSetControl::originalPosition
	Vector3_t2243707580  ___originalPosition_90;
	// UnityEngine.Vector3 OnlineMapsTileSetControl::smoothZoomOffset
	Vector3_t2243707580  ___smoothZoomOffset_91;
	// UnityEngine.Vector3 OnlineMapsTileSetControl::smoothZoomHitPoint
	Vector3_t2243707580  ___smoothZoomHitPoint_92;
	// System.Boolean OnlineMapsTileSetControl::firstUpdate
	bool ___firstUpdate_93;
	// System.Collections.Generic.List`1<OnlineMapsTileSetControl/TilesetFlatMarker> OnlineMapsTileSetControl::usedMarkers
	List_1_t2920006219 * ___usedMarkers_94;

public:
	inline static int32_t get_offset_of_OnChangeMaterialTexture_53() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___OnChangeMaterialTexture_53)); }
	inline Action_2_t2903233652 * get_OnChangeMaterialTexture_53() const { return ___OnChangeMaterialTexture_53; }
	inline Action_2_t2903233652 ** get_address_of_OnChangeMaterialTexture_53() { return &___OnChangeMaterialTexture_53; }
	inline void set_OnChangeMaterialTexture_53(Action_2_t2903233652 * value)
	{
		___OnChangeMaterialTexture_53 = value;
		Il2CppCodeGenWriteBarrier(&___OnChangeMaterialTexture_53, value);
	}

	inline static int32_t get_offset_of_OnCheckMarker2DVisibility_54() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___OnCheckMarker2DVisibility_54)); }
	inline CheckMarker2DVisibilityDelegate_t3802502927 * get_OnCheckMarker2DVisibility_54() const { return ___OnCheckMarker2DVisibility_54; }
	inline CheckMarker2DVisibilityDelegate_t3802502927 ** get_address_of_OnCheckMarker2DVisibility_54() { return &___OnCheckMarker2DVisibility_54; }
	inline void set_OnCheckMarker2DVisibility_54(CheckMarker2DVisibilityDelegate_t3802502927 * value)
	{
		___OnCheckMarker2DVisibility_54 = value;
		Il2CppCodeGenWriteBarrier(&___OnCheckMarker2DVisibility_54, value);
	}

	inline static int32_t get_offset_of_OnGetElevation_55() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___OnGetElevation_55)); }
	inline Action_2_t3492767325 * get_OnGetElevation_55() const { return ___OnGetElevation_55; }
	inline Action_2_t3492767325 ** get_address_of_OnGetElevation_55() { return &___OnGetElevation_55; }
	inline void set_OnGetElevation_55(Action_2_t3492767325 * value)
	{
		___OnGetElevation_55 = value;
		Il2CppCodeGenWriteBarrier(&___OnGetElevation_55, value);
	}

	inline static int32_t get_offset_of_OnGetFlatMarkerOffsetY_56() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___OnGetFlatMarkerOffsetY_56)); }
	inline GetFlatMarkerOffsetYDelegate_t860568800 * get_OnGetFlatMarkerOffsetY_56() const { return ___OnGetFlatMarkerOffsetY_56; }
	inline GetFlatMarkerOffsetYDelegate_t860568800 ** get_address_of_OnGetFlatMarkerOffsetY_56() { return &___OnGetFlatMarkerOffsetY_56; }
	inline void set_OnGetFlatMarkerOffsetY_56(GetFlatMarkerOffsetYDelegate_t860568800 * value)
	{
		___OnGetFlatMarkerOffsetY_56 = value;
		Il2CppCodeGenWriteBarrier(&___OnGetFlatMarkerOffsetY_56, value);
	}

	inline static int32_t get_offset_of_OnSmoothZoomBegin_57() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___OnSmoothZoomBegin_57)); }
	inline Action_t3226471752 * get_OnSmoothZoomBegin_57() const { return ___OnSmoothZoomBegin_57; }
	inline Action_t3226471752 ** get_address_of_OnSmoothZoomBegin_57() { return &___OnSmoothZoomBegin_57; }
	inline void set_OnSmoothZoomBegin_57(Action_t3226471752 * value)
	{
		___OnSmoothZoomBegin_57 = value;
		Il2CppCodeGenWriteBarrier(&___OnSmoothZoomBegin_57, value);
	}

	inline static int32_t get_offset_of_OnSmoothZoomFinish_58() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___OnSmoothZoomFinish_58)); }
	inline Action_t3226471752 * get_OnSmoothZoomFinish_58() const { return ___OnSmoothZoomFinish_58; }
	inline Action_t3226471752 ** get_address_of_OnSmoothZoomFinish_58() { return &___OnSmoothZoomFinish_58; }
	inline void set_OnSmoothZoomFinish_58(Action_t3226471752 * value)
	{
		___OnSmoothZoomFinish_58 = value;
		Il2CppCodeGenWriteBarrier(&___OnSmoothZoomFinish_58, value);
	}

	inline static int32_t get_offset_of_OnSmoothZoomProcess_59() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___OnSmoothZoomProcess_59)); }
	inline Action_t3226471752 * get_OnSmoothZoomProcess_59() const { return ___OnSmoothZoomProcess_59; }
	inline Action_t3226471752 ** get_address_of_OnSmoothZoomProcess_59() { return &___OnSmoothZoomProcess_59; }
	inline void set_OnSmoothZoomProcess_59(Action_t3226471752 * value)
	{
		___OnSmoothZoomProcess_59 = value;
		Il2CppCodeGenWriteBarrier(&___OnSmoothZoomProcess_59, value);
	}

	inline static int32_t get_offset_of_bingAPI_60() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___bingAPI_60)); }
	inline String_t* get_bingAPI_60() const { return ___bingAPI_60; }
	inline String_t** get_address_of_bingAPI_60() { return &___bingAPI_60; }
	inline void set_bingAPI_60(String_t* value)
	{
		___bingAPI_60 = value;
		Il2CppCodeGenWriteBarrier(&___bingAPI_60, value);
	}

	inline static int32_t get_offset_of_checkMarker2DVisibility_61() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___checkMarker2DVisibility_61)); }
	inline int32_t get_checkMarker2DVisibility_61() const { return ___checkMarker2DVisibility_61; }
	inline int32_t* get_address_of_checkMarker2DVisibility_61() { return &___checkMarker2DVisibility_61; }
	inline void set_checkMarker2DVisibility_61(int32_t value)
	{
		___checkMarker2DVisibility_61 = value;
	}

	inline static int32_t get_offset_of_colliderType_62() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___colliderType_62)); }
	inline int32_t get_colliderType_62() const { return ___colliderType_62; }
	inline int32_t* get_address_of_colliderType_62() { return &___colliderType_62; }
	inline void set_colliderType_62(int32_t value)
	{
		___colliderType_62 = value;
	}

	inline static int32_t get_offset_of_drawingsGameObject_63() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___drawingsGameObject_63)); }
	inline GameObject_t1756533147 * get_drawingsGameObject_63() const { return ___drawingsGameObject_63; }
	inline GameObject_t1756533147 ** get_address_of_drawingsGameObject_63() { return &___drawingsGameObject_63; }
	inline void set_drawingsGameObject_63(GameObject_t1756533147 * value)
	{
		___drawingsGameObject_63 = value;
		Il2CppCodeGenWriteBarrier(&___drawingsGameObject_63, value);
	}

	inline static int32_t get_offset_of_drawingShader_64() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___drawingShader_64)); }
	inline Shader_t2430389951 * get_drawingShader_64() const { return ___drawingShader_64; }
	inline Shader_t2430389951 ** get_address_of_drawingShader_64() { return &___drawingShader_64; }
	inline void set_drawingShader_64(Shader_t2430389951 * value)
	{
		___drawingShader_64 = value;
		Il2CppCodeGenWriteBarrier(&___drawingShader_64, value);
	}

	inline static int32_t get_offset_of_elevationZoomRange_65() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___elevationZoomRange_65)); }
	inline OnlineMapsRange_t3791609909 * get_elevationZoomRange_65() const { return ___elevationZoomRange_65; }
	inline OnlineMapsRange_t3791609909 ** get_address_of_elevationZoomRange_65() { return &___elevationZoomRange_65; }
	inline void set_elevationZoomRange_65(OnlineMapsRange_t3791609909 * value)
	{
		___elevationZoomRange_65 = value;
		Il2CppCodeGenWriteBarrier(&___elevationZoomRange_65, value);
	}

	inline static int32_t get_offset_of_elevationScale_66() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___elevationScale_66)); }
	inline float get_elevationScale_66() const { return ___elevationScale_66; }
	inline float* get_address_of_elevationScale_66() { return &___elevationScale_66; }
	inline void set_elevationScale_66(float value)
	{
		___elevationScale_66 = value;
	}

	inline static int32_t get_offset_of_markerComparer_67() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___markerComparer_67)); }
	inline Il2CppObject* get_markerComparer_67() const { return ___markerComparer_67; }
	inline Il2CppObject** get_address_of_markerComparer_67() { return &___markerComparer_67; }
	inline void set_markerComparer_67(Il2CppObject* value)
	{
		___markerComparer_67 = value;
		Il2CppCodeGenWriteBarrier(&___markerComparer_67, value);
	}

	inline static int32_t get_offset_of_markerMaterial_68() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___markerMaterial_68)); }
	inline Material_t193706927 * get_markerMaterial_68() const { return ___markerMaterial_68; }
	inline Material_t193706927 ** get_address_of_markerMaterial_68() { return &___markerMaterial_68; }
	inline void set_markerMaterial_68(Material_t193706927 * value)
	{
		___markerMaterial_68 = value;
		Il2CppCodeGenWriteBarrier(&___markerMaterial_68, value);
	}

	inline static int32_t get_offset_of_markerShader_69() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___markerShader_69)); }
	inline Shader_t2430389951 * get_markerShader_69() const { return ___markerShader_69; }
	inline Shader_t2430389951 ** get_address_of_markerShader_69() { return &___markerShader_69; }
	inline void set_markerShader_69(Shader_t2430389951 * value)
	{
		___markerShader_69 = value;
		Il2CppCodeGenWriteBarrier(&___markerShader_69, value);
	}

	inline static int32_t get_offset_of_smoothZoom_70() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___smoothZoom_70)); }
	inline bool get_smoothZoom_70() const { return ___smoothZoom_70; }
	inline bool* get_address_of_smoothZoom_70() { return &___smoothZoom_70; }
	inline void set_smoothZoom_70(bool value)
	{
		___smoothZoom_70 = value;
	}

	inline static int32_t get_offset_of_smoothZoomMode_71() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___smoothZoomMode_71)); }
	inline int32_t get_smoothZoomMode_71() const { return ___smoothZoomMode_71; }
	inline int32_t* get_address_of_smoothZoomMode_71() { return &___smoothZoomMode_71; }
	inline void set_smoothZoomMode_71(int32_t value)
	{
		___smoothZoomMode_71 = value;
	}

	inline static int32_t get_offset_of_tileMaterial_72() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___tileMaterial_72)); }
	inline Material_t193706927 * get_tileMaterial_72() const { return ___tileMaterial_72; }
	inline Material_t193706927 ** get_address_of_tileMaterial_72() { return &___tileMaterial_72; }
	inline void set_tileMaterial_72(Material_t193706927 * value)
	{
		___tileMaterial_72 = value;
		Il2CppCodeGenWriteBarrier(&___tileMaterial_72, value);
	}

	inline static int32_t get_offset_of_tilesetShader_73() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___tilesetShader_73)); }
	inline Shader_t2430389951 * get_tilesetShader_73() const { return ___tilesetShader_73; }
	inline Shader_t2430389951 ** get_address_of_tilesetShader_73() { return &___tilesetShader_73; }
	inline void set_tilesetShader_73(Shader_t2430389951 * value)
	{
		___tilesetShader_73 = value;
		Il2CppCodeGenWriteBarrier(&___tilesetShader_73, value);
	}

	inline static int32_t get_offset_of_useElevation_74() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___useElevation_74)); }
	inline bool get_useElevation_74() const { return ___useElevation_74; }
	inline bool* get_address_of_useElevation_74() { return &___useElevation_74; }
	inline void set_useElevation_74(bool value)
	{
		___useElevation_74 = value;
	}

	inline static int32_t get_offset_of__useElevation_75() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ____useElevation_75)); }
	inline bool get__useElevation_75() const { return ____useElevation_75; }
	inline bool* get_address_of__useElevation_75() { return &____useElevation_75; }
	inline void set__useElevation_75(bool value)
	{
		____useElevation_75 = value;
	}

	inline static int32_t get_offset_of__bufferPosition_76() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ____bufferPosition_76)); }
	inline OnlineMapsVector2i_t2180897250 * get__bufferPosition_76() const { return ____bufferPosition_76; }
	inline OnlineMapsVector2i_t2180897250 ** get_address_of__bufferPosition_76() { return &____bufferPosition_76; }
	inline void set__bufferPosition_76(OnlineMapsVector2i_t2180897250 * value)
	{
		____bufferPosition_76 = value;
		Il2CppCodeGenWriteBarrier(&____bufferPosition_76, value);
	}

	inline static int32_t get_offset_of_elevationRequest_77() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___elevationRequest_77)); }
	inline WWW_t2919945039 * get_elevationRequest_77() const { return ___elevationRequest_77; }
	inline WWW_t2919945039 ** get_address_of_elevationRequest_77() { return &___elevationRequest_77; }
	inline void set_elevationRequest_77(WWW_t2919945039 * value)
	{
		___elevationRequest_77 = value;
		Il2CppCodeGenWriteBarrier(&___elevationRequest_77, value);
	}

	inline static int32_t get_offset_of_elevationRequestRect_78() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___elevationRequestRect_78)); }
	inline Rect_t3681755626  get_elevationRequestRect_78() const { return ___elevationRequestRect_78; }
	inline Rect_t3681755626 * get_address_of_elevationRequestRect_78() { return &___elevationRequestRect_78; }
	inline void set_elevationRequestRect_78(Rect_t3681755626  value)
	{
		___elevationRequestRect_78 = value;
	}

	inline static int32_t get_offset_of_elevationData_79() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___elevationData_79)); }
	inline Int16U5BU2CU5D_t3104283264* get_elevationData_79() const { return ___elevationData_79; }
	inline Int16U5BU2CU5D_t3104283264** get_address_of_elevationData_79() { return &___elevationData_79; }
	inline void set_elevationData_79(Int16U5BU2CU5D_t3104283264* value)
	{
		___elevationData_79 = value;
		Il2CppCodeGenWriteBarrier(&___elevationData_79, value);
	}

	inline static int32_t get_offset_of_elevationRect_80() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___elevationRect_80)); }
	inline Rect_t3681755626  get_elevationRect_80() const { return ___elevationRect_80; }
	inline Rect_t3681755626 * get_address_of_elevationRect_80() { return &___elevationRect_80; }
	inline void set_elevationRect_80(Rect_t3681755626  value)
	{
		___elevationRect_80 = value;
	}

	inline static int32_t get_offset_of_meshCollider_81() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___meshCollider_81)); }
	inline MeshCollider_t2718867283 * get_meshCollider_81() const { return ___meshCollider_81; }
	inline MeshCollider_t2718867283 ** get_address_of_meshCollider_81() { return &___meshCollider_81; }
	inline void set_meshCollider_81(MeshCollider_t2718867283 * value)
	{
		___meshCollider_81 = value;
		Il2CppCodeGenWriteBarrier(&___meshCollider_81, value);
	}

	inline static int32_t get_offset_of_ignoreGetElevation_82() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___ignoreGetElevation_82)); }
	inline bool get_ignoreGetElevation_82() const { return ___ignoreGetElevation_82; }
	inline bool* get_address_of_ignoreGetElevation_82() { return &___ignoreGetElevation_82; }
	inline void set_ignoreGetElevation_82(bool value)
	{
		___ignoreGetElevation_82 = value;
	}

	inline static int32_t get_offset_of_tilesetMesh_83() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___tilesetMesh_83)); }
	inline Mesh_t1356156583 * get_tilesetMesh_83() const { return ___tilesetMesh_83; }
	inline Mesh_t1356156583 ** get_address_of_tilesetMesh_83() { return &___tilesetMesh_83; }
	inline void set_tilesetMesh_83(Mesh_t1356156583 * value)
	{
		___tilesetMesh_83 = value;
		Il2CppCodeGenWriteBarrier(&___tilesetMesh_83, value);
	}

	inline static int32_t get_offset_of_triangles_84() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___triangles_84)); }
	inline Int32U5BU5D_t3030399641* get_triangles_84() const { return ___triangles_84; }
	inline Int32U5BU5D_t3030399641** get_address_of_triangles_84() { return &___triangles_84; }
	inline void set_triangles_84(Int32U5BU5D_t3030399641* value)
	{
		___triangles_84 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_84, value);
	}

	inline static int32_t get_offset_of_uv_85() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___uv_85)); }
	inline Vector2U5BU5D_t686124026* get_uv_85() const { return ___uv_85; }
	inline Vector2U5BU5D_t686124026** get_address_of_uv_85() { return &___uv_85; }
	inline void set_uv_85(Vector2U5BU5D_t686124026* value)
	{
		___uv_85 = value;
		Il2CppCodeGenWriteBarrier(&___uv_85, value);
	}

	inline static int32_t get_offset_of_vertices_86() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___vertices_86)); }
	inline Vector3U5BU5D_t1172311765* get_vertices_86() const { return ___vertices_86; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vertices_86() { return &___vertices_86; }
	inline void set_vertices_86(Vector3U5BU5D_t1172311765* value)
	{
		___vertices_86 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_86, value);
	}

	inline static int32_t get_offset_of_elevationBufferPosition_87() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___elevationBufferPosition_87)); }
	inline OnlineMapsVector2i_t2180897250 * get_elevationBufferPosition_87() const { return ___elevationBufferPosition_87; }
	inline OnlineMapsVector2i_t2180897250 ** get_address_of_elevationBufferPosition_87() { return &___elevationBufferPosition_87; }
	inline void set_elevationBufferPosition_87(OnlineMapsVector2i_t2180897250 * value)
	{
		___elevationBufferPosition_87 = value;
		Il2CppCodeGenWriteBarrier(&___elevationBufferPosition_87, value);
	}

	inline static int32_t get_offset_of_smoothZoomStarted_88() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___smoothZoomStarted_88)); }
	inline bool get_smoothZoomStarted_88() const { return ___smoothZoomStarted_88; }
	inline bool* get_address_of_smoothZoomStarted_88() { return &___smoothZoomStarted_88; }
	inline void set_smoothZoomStarted_88(bool value)
	{
		___smoothZoomStarted_88 = value;
	}

	inline static int32_t get_offset_of_smoothZoomPoint_89() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___smoothZoomPoint_89)); }
	inline Vector2_t2243707579  get_smoothZoomPoint_89() const { return ___smoothZoomPoint_89; }
	inline Vector2_t2243707579 * get_address_of_smoothZoomPoint_89() { return &___smoothZoomPoint_89; }
	inline void set_smoothZoomPoint_89(Vector2_t2243707579  value)
	{
		___smoothZoomPoint_89 = value;
	}

	inline static int32_t get_offset_of_originalPosition_90() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___originalPosition_90)); }
	inline Vector3_t2243707580  get_originalPosition_90() const { return ___originalPosition_90; }
	inline Vector3_t2243707580 * get_address_of_originalPosition_90() { return &___originalPosition_90; }
	inline void set_originalPosition_90(Vector3_t2243707580  value)
	{
		___originalPosition_90 = value;
	}

	inline static int32_t get_offset_of_smoothZoomOffset_91() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___smoothZoomOffset_91)); }
	inline Vector3_t2243707580  get_smoothZoomOffset_91() const { return ___smoothZoomOffset_91; }
	inline Vector3_t2243707580 * get_address_of_smoothZoomOffset_91() { return &___smoothZoomOffset_91; }
	inline void set_smoothZoomOffset_91(Vector3_t2243707580  value)
	{
		___smoothZoomOffset_91 = value;
	}

	inline static int32_t get_offset_of_smoothZoomHitPoint_92() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___smoothZoomHitPoint_92)); }
	inline Vector3_t2243707580  get_smoothZoomHitPoint_92() const { return ___smoothZoomHitPoint_92; }
	inline Vector3_t2243707580 * get_address_of_smoothZoomHitPoint_92() { return &___smoothZoomHitPoint_92; }
	inline void set_smoothZoomHitPoint_92(Vector3_t2243707580  value)
	{
		___smoothZoomHitPoint_92 = value;
	}

	inline static int32_t get_offset_of_firstUpdate_93() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___firstUpdate_93)); }
	inline bool get_firstUpdate_93() const { return ___firstUpdate_93; }
	inline bool* get_address_of_firstUpdate_93() { return &___firstUpdate_93; }
	inline void set_firstUpdate_93(bool value)
	{
		___firstUpdate_93 = value;
	}

	inline static int32_t get_offset_of_usedMarkers_94() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803, ___usedMarkers_94)); }
	inline List_1_t2920006219 * get_usedMarkers_94() const { return ___usedMarkers_94; }
	inline List_1_t2920006219 ** get_address_of_usedMarkers_94() { return &___usedMarkers_94; }
	inline void set_usedMarkers_94(List_1_t2920006219 * value)
	{
		___usedMarkers_94 = value;
		Il2CppCodeGenWriteBarrier(&___usedMarkers_94, value);
	}
};

struct OnlineMapsTileSetControl_t3368302803_StaticFields
{
public:
	// System.Func`2<System.String,System.Int16> OnlineMapsTileSetControl::<>f__mg$cache0
	Func_2_t2205052638 * ___U3CU3Ef__mgU24cache0_95;
	// System.Func`2<OnlineMapsMarker,OnlineMapsMarker> OnlineMapsTileSetControl::<>f__am$cache0
	Func_2_t3438359081 * ___U3CU3Ef__amU24cache0_96;
	// System.Func`2<OnlineMapsMarker,System.Single> OnlineMapsTileSetControl::<>f__am$cache1
	Func_2_t2022702331 * ___U3CU3Ef__amU24cache1_97;
	// System.Func`2<OnlineMapsTileSetControl/TilesetSortedMarker,System.Single> OnlineMapsTileSetControl::<>f__am$cache2
	Func_2_t1961126510 * ___U3CU3Ef__amU24cache2_98;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_95() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803_StaticFields, ___U3CU3Ef__mgU24cache0_95)); }
	inline Func_2_t2205052638 * get_U3CU3Ef__mgU24cache0_95() const { return ___U3CU3Ef__mgU24cache0_95; }
	inline Func_2_t2205052638 ** get_address_of_U3CU3Ef__mgU24cache0_95() { return &___U3CU3Ef__mgU24cache0_95; }
	inline void set_U3CU3Ef__mgU24cache0_95(Func_2_t2205052638 * value)
	{
		___U3CU3Ef__mgU24cache0_95 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_95, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_96() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803_StaticFields, ___U3CU3Ef__amU24cache0_96)); }
	inline Func_2_t3438359081 * get_U3CU3Ef__amU24cache0_96() const { return ___U3CU3Ef__amU24cache0_96; }
	inline Func_2_t3438359081 ** get_address_of_U3CU3Ef__amU24cache0_96() { return &___U3CU3Ef__amU24cache0_96; }
	inline void set_U3CU3Ef__amU24cache0_96(Func_2_t3438359081 * value)
	{
		___U3CU3Ef__amU24cache0_96 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_96, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_97() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803_StaticFields, ___U3CU3Ef__amU24cache1_97)); }
	inline Func_2_t2022702331 * get_U3CU3Ef__amU24cache1_97() const { return ___U3CU3Ef__amU24cache1_97; }
	inline Func_2_t2022702331 ** get_address_of_U3CU3Ef__amU24cache1_97() { return &___U3CU3Ef__amU24cache1_97; }
	inline void set_U3CU3Ef__amU24cache1_97(Func_2_t2022702331 * value)
	{
		___U3CU3Ef__amU24cache1_97 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_97, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_98() { return static_cast<int32_t>(offsetof(OnlineMapsTileSetControl_t3368302803_StaticFields, ___U3CU3Ef__amU24cache2_98)); }
	inline Func_2_t1961126510 * get_U3CU3Ef__amU24cache2_98() const { return ___U3CU3Ef__amU24cache2_98; }
	inline Func_2_t1961126510 ** get_address_of_U3CU3Ef__amU24cache2_98() { return &___U3CU3Ef__amU24cache2_98; }
	inline void set_U3CU3Ef__amU24cache2_98(Func_2_t1961126510 * value)
	{
		___U3CU3Ef__amU24cache2_98 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_98, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
