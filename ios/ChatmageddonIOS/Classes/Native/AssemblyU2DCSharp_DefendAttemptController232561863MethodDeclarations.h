﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendAttemptController
struct DefendAttemptController_t232561863;

#include "codegen/il2cpp-codegen.h"

// System.Void DefendAttemptController::.ctor()
extern "C"  void DefendAttemptController__ctor_m1377621356 (DefendAttemptController_t232561863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendAttemptController::Awake()
extern "C"  void DefendAttemptController_Awake_m1542087651 (DefendAttemptController_t232561863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendAttemptController::CreateTabs()
extern "C"  void DefendAttemptController_CreateTabs_m1202125114 (DefendAttemptController_t232561863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendAttemptController::PositionTabs()
extern "C"  void DefendAttemptController_PositionTabs_m1467666087 (DefendAttemptController_t232561863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendAttemptController::ActivateCurrentTab(System.Boolean)
extern "C"  void DefendAttemptController_ActivateCurrentTab_m2325909978 (DefendAttemptController_t232561863 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendAttemptController::UpdateAttempt(System.Boolean)
extern "C"  void DefendAttemptController_UpdateAttempt_m1956481465 (DefendAttemptController_t232561863 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DefendAttemptController::MaxAttemptsReached()
extern "C"  bool DefendAttemptController_MaxAttemptsReached_m3082171778 (DefendAttemptController_t232561863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
