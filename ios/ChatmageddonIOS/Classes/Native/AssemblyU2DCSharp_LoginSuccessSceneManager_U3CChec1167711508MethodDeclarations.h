﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginSuccessSceneManager/<CheckInternetConnection>c__AnonStorey2
struct U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoginSuccessSceneManager/<CheckInternetConnection>c__AnonStorey2::.ctor()
extern "C"  void U3CCheckInternetConnectionU3Ec__AnonStorey2__ctor_m2195099765 (U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager/<CheckInternetConnection>c__AnonStorey2::<>m__0(System.Boolean,System.String)
extern "C"  void U3CCheckInternetConnectionU3Ec__AnonStorey2_U3CU3Em__0_m692057707 (U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
