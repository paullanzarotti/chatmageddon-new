﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.LongValidator
struct LongValidator_t1020816802;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Configuration.LongValidator::.ctor(System.Int64,System.Int64,System.Boolean,System.Int64)
extern "C"  void LongValidator__ctor_m2287811560 (LongValidator_t1020816802 * __this, int64_t ___minValue0, int64_t ___maxValue1, bool ___rangeIsExclusive2, int64_t ___resolution3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.LongValidator::.ctor(System.Int64,System.Int64,System.Boolean)
extern "C"  void LongValidator__ctor_m760940322 (LongValidator_t1020816802 * __this, int64_t ___minValue0, int64_t ___maxValue1, bool ___rangeIsExclusive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.LongValidator::.ctor(System.Int64,System.Int64)
extern "C"  void LongValidator__ctor_m1829199973 (LongValidator_t1020816802 * __this, int64_t ___minValue0, int64_t ___maxValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.LongValidator::CanValidate(System.Type)
extern "C"  bool LongValidator_CanValidate_m1350671140 (LongValidator_t1020816802 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.LongValidator::Validate(System.Object)
extern "C"  void LongValidator_Validate_m2147347437 (LongValidator_t1020816802 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
