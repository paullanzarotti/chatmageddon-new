﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FBManager_1_LoadPictureCallback_2854975038MethodDeclarations.h"

// System.Void FBManager`1/LoadPictureCallback<ChatmageddonFBManager>::.ctor(System.Object,System.IntPtr)
#define LoadPictureCallback__ctor_m156938881(__this, ___object0, ___method1, method) ((  void (*) (LoadPictureCallback_t399926527 *, Il2CppObject *, IntPtr_t, const MethodInfo*))LoadPictureCallback__ctor_m3122573841_gshared)(__this, ___object0, ___method1, method)
// System.Void FBManager`1/LoadPictureCallback<ChatmageddonFBManager>::Invoke(UnityEngine.Texture)
#define LoadPictureCallback_Invoke_m3180433069(__this, ___texture0, method) ((  void (*) (LoadPictureCallback_t399926527 *, Texture_t2243626319 *, const MethodInfo*))LoadPictureCallback_Invoke_m2615178341_gshared)(__this, ___texture0, method)
// System.IAsyncResult FBManager`1/LoadPictureCallback<ChatmageddonFBManager>::BeginInvoke(UnityEngine.Texture,System.AsyncCallback,System.Object)
#define LoadPictureCallback_BeginInvoke_m3702969192(__this, ___texture0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (LoadPictureCallback_t399926527 *, Texture_t2243626319 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))LoadPictureCallback_BeginInvoke_m3362398610_gshared)(__this, ___texture0, ___callback1, ___object2, method)
// System.Void FBManager`1/LoadPictureCallback<ChatmageddonFBManager>::EndInvoke(System.IAsyncResult)
#define LoadPictureCallback_EndInvoke_m3150033111(__this, ___result0, method) ((  void (*) (LoadPictureCallback_t399926527 *, Il2CppObject *, const MethodInfo*))LoadPictureCallback_EndInvoke_m2740201835_gshared)(__this, ___result0, method)
