﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileResultsModal
struct MissileResultsModal_t1615487503;
// ResultModalUI
struct ResultModalUI_t969511824;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ResultModalUI969511824.h"
#include "AssemblyU2DCSharp_GameResult1017068805.h"

// System.Void MissileResultsModal::.ctor()
extern "C"  void MissileResultsModal__ctor_m608081922 (MissileResultsModal_t1615487503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsModal::LoadResultData(ResultModalUI)
extern "C"  void MissileResultsModal_LoadResultData_m1095033339 (MissileResultsModal_t1615487503 * __this, ResultModalUI_t969511824 * ___resultModal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsModal::SetDefendTimeLabel(GameResult)
extern "C"  void MissileResultsModal_SetDefendTimeLabel_m1396935776 (MissileResultsModal_t1615487503 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsModal::SetTrajectoryColour(GameResult)
extern "C"  void MissileResultsModal_SetTrajectoryColour_m2734312150 (MissileResultsModal_t1615487503 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsModal::SetMissileHit()
extern "C"  void MissileResultsModal_SetMissileHit_m805615525 (MissileResultsModal_t1615487503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsModal::SetMissileDefended()
extern "C"  void MissileResultsModal_SetMissileDefended_m3925030497 (MissileResultsModal_t1615487503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsModal::SetMissileBlocked()
extern "C"  void MissileResultsModal_SetMissileBlocked_m1394802934 (MissileResultsModal_t1615487503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsModal::SetMissileBackfired()
extern "C"  void MissileResultsModal_SetMissileBackfired_m1124448351 (MissileResultsModal_t1615487503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
