﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// User
struct User_t719925459;
// LeaderboardILP
struct LeaderboardILP_t694823370;
// UILabel
struct UILabel_t1795115428;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3887848756.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardListItem
struct  LeaderboardListItem_t3112309392  : public BaseInfiniteListItem_1_t3887848756
{
public:
	// User LeaderboardListItem::user
	User_t719925459 * ___user_10;
	// LeaderboardILP LeaderboardListItem::listPopulator
	LeaderboardILP_t694823370 * ___listPopulator_11;
	// UILabel LeaderboardListItem::rankLabel
	UILabel_t1795115428 * ___rankLabel_12;
	// UILabel LeaderboardListItem::nameLabel
	UILabel_t1795115428 * ___nameLabel_13;
	// UILabel LeaderboardListItem::userRankLabel
	UILabel_t1795115428 * ___userRankLabel_14;
	// UILabel LeaderboardListItem::scoreLabel
	UILabel_t1795115428 * ___scoreLabel_15;
	// UIScrollView LeaderboardListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_16;
	// UIScrollView LeaderboardListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_17;
	// UnityEngine.Transform LeaderboardListItem::mTrans
	Transform_t3275118058 * ___mTrans_18;
	// UIScrollView LeaderboardListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_19;
	// System.Boolean LeaderboardListItem::mAutoFind
	bool ___mAutoFind_20;
	// System.Boolean LeaderboardListItem::mStarted
	bool ___mStarted_21;

public:
	inline static int32_t get_offset_of_user_10() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___user_10)); }
	inline User_t719925459 * get_user_10() const { return ___user_10; }
	inline User_t719925459 ** get_address_of_user_10() { return &___user_10; }
	inline void set_user_10(User_t719925459 * value)
	{
		___user_10 = value;
		Il2CppCodeGenWriteBarrier(&___user_10, value);
	}

	inline static int32_t get_offset_of_listPopulator_11() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___listPopulator_11)); }
	inline LeaderboardILP_t694823370 * get_listPopulator_11() const { return ___listPopulator_11; }
	inline LeaderboardILP_t694823370 ** get_address_of_listPopulator_11() { return &___listPopulator_11; }
	inline void set_listPopulator_11(LeaderboardILP_t694823370 * value)
	{
		___listPopulator_11 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_11, value);
	}

	inline static int32_t get_offset_of_rankLabel_12() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___rankLabel_12)); }
	inline UILabel_t1795115428 * get_rankLabel_12() const { return ___rankLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_rankLabel_12() { return &___rankLabel_12; }
	inline void set_rankLabel_12(UILabel_t1795115428 * value)
	{
		___rankLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___rankLabel_12, value);
	}

	inline static int32_t get_offset_of_nameLabel_13() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___nameLabel_13)); }
	inline UILabel_t1795115428 * get_nameLabel_13() const { return ___nameLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_13() { return &___nameLabel_13; }
	inline void set_nameLabel_13(UILabel_t1795115428 * value)
	{
		___nameLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_13, value);
	}

	inline static int32_t get_offset_of_userRankLabel_14() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___userRankLabel_14)); }
	inline UILabel_t1795115428 * get_userRankLabel_14() const { return ___userRankLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_userRankLabel_14() { return &___userRankLabel_14; }
	inline void set_userRankLabel_14(UILabel_t1795115428 * value)
	{
		___userRankLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___userRankLabel_14, value);
	}

	inline static int32_t get_offset_of_scoreLabel_15() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___scoreLabel_15)); }
	inline UILabel_t1795115428 * get_scoreLabel_15() const { return ___scoreLabel_15; }
	inline UILabel_t1795115428 ** get_address_of_scoreLabel_15() { return &___scoreLabel_15; }
	inline void set_scoreLabel_15(UILabel_t1795115428 * value)
	{
		___scoreLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___scoreLabel_15, value);
	}

	inline static int32_t get_offset_of_scrollView_16() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___scrollView_16)); }
	inline UIScrollView_t3033954930 * get_scrollView_16() const { return ___scrollView_16; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_16() { return &___scrollView_16; }
	inline void set_scrollView_16(UIScrollView_t3033954930 * value)
	{
		___scrollView_16 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_16, value);
	}

	inline static int32_t get_offset_of_draggablePanel_17() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___draggablePanel_17)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_17() const { return ___draggablePanel_17; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_17() { return &___draggablePanel_17; }
	inline void set_draggablePanel_17(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_17, value);
	}

	inline static int32_t get_offset_of_mTrans_18() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___mTrans_18)); }
	inline Transform_t3275118058 * get_mTrans_18() const { return ___mTrans_18; }
	inline Transform_t3275118058 ** get_address_of_mTrans_18() { return &___mTrans_18; }
	inline void set_mTrans_18(Transform_t3275118058 * value)
	{
		___mTrans_18 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_18, value);
	}

	inline static int32_t get_offset_of_mScroll_19() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___mScroll_19)); }
	inline UIScrollView_t3033954930 * get_mScroll_19() const { return ___mScroll_19; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_19() { return &___mScroll_19; }
	inline void set_mScroll_19(UIScrollView_t3033954930 * value)
	{
		___mScroll_19 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_19, value);
	}

	inline static int32_t get_offset_of_mAutoFind_20() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___mAutoFind_20)); }
	inline bool get_mAutoFind_20() const { return ___mAutoFind_20; }
	inline bool* get_address_of_mAutoFind_20() { return &___mAutoFind_20; }
	inline void set_mAutoFind_20(bool value)
	{
		___mAutoFind_20 = value;
	}

	inline static int32_t get_offset_of_mStarted_21() { return static_cast<int32_t>(offsetof(LeaderboardListItem_t3112309392, ___mStarted_21)); }
	inline bool get_mStarted_21() const { return ___mStarted_21; }
	inline bool* get_address_of_mStarted_21() { return &___mStarted_21; }
	inline void set_mStarted_21(bool value)
	{
		___mStarted_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
