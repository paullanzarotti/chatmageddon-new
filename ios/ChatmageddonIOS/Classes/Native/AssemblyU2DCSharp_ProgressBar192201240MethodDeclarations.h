﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar
struct ProgressBar_t192201240;
// ProgressBar/OnProgressUpdated
struct OnProgressUpdated_t2843909178;
// ProgressBar/OnProgressFinished
struct OnProgressFinished_t739551803;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProgressBar_OnProgressUpdated2843909178.h"
#include "AssemblyU2DCSharp_ProgressBar_OnProgressFinished739551803.h"

// System.Void ProgressBar::.ctor()
extern "C"  void ProgressBar__ctor_m4144027091 (ProgressBar_t192201240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::add_onProgressUpdated(ProgressBar/OnProgressUpdated)
extern "C"  void ProgressBar_add_onProgressUpdated_m3925116466 (ProgressBar_t192201240 * __this, OnProgressUpdated_t2843909178 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::remove_onProgressUpdated(ProgressBar/OnProgressUpdated)
extern "C"  void ProgressBar_remove_onProgressUpdated_m1535495961 (ProgressBar_t192201240 * __this, OnProgressUpdated_t2843909178 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::add_onProgressFinished(ProgressBar/OnProgressFinished)
extern "C"  void ProgressBar_add_onProgressFinished_m2508180016 (ProgressBar_t192201240 * __this, OnProgressFinished_t739551803 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::remove_onProgressFinished(ProgressBar/OnProgressFinished)
extern "C"  void ProgressBar_remove_onProgressFinished_m1908472293 (ProgressBar_t192201240 * __this, OnProgressFinished_t739551803 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::JumpToPercent(System.Single)
extern "C"  void ProgressBar_JumpToPercent_m2426160206 (ProgressBar_t192201240 * __this, float ___percent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::TransitionToPercent(System.Single,System.Single)
extern "C"  void ProgressBar_TransitionToPercent_m3183583218 (ProgressBar_t192201240 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::StopTransition()
extern "C"  void ProgressBar_StopTransition_m1441491428 (ProgressBar_t192201240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ProgressBar::Transition(System.Single,System.Single)
extern "C"  Il2CppObject * ProgressBar_Transition_m1044242990 (ProgressBar_t192201240 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::ProgressUpdated()
extern "C"  void ProgressBar_ProgressUpdated_m327423371 (ProgressBar_t192201240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar::ProgressFinished()
extern "C"  void ProgressBar_ProgressFinished_m1755042866 (ProgressBar_t192201240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
