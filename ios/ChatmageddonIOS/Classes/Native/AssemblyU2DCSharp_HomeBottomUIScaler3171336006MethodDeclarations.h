﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeBottomUIScaler
struct HomeBottomUIScaler_t3171336006;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeBottomUIScaler::.ctor()
extern "C"  void HomeBottomUIScaler__ctor_m1552726701 (HomeBottomUIScaler_t3171336006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeBottomUIScaler::GlobalUIScale()
extern "C"  void HomeBottomUIScaler_GlobalUIScale_m3349638616 (HomeBottomUIScaler_t3171336006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
