﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UnityEngine.Transform
struct Transform_t3275118058;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LauncherLoaderUIScaler
struct  LauncherLoaderUIScaler_t1442786027  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UITexture LauncherLoaderUIScaler::animationOutline
	UITexture_t2537039969 * ___animationOutline_14;
	// UITexture LauncherLoaderUIScaler::animationMask
	UITexture_t2537039969 * ___animationMask_15;
	// UnityEngine.Transform LauncherLoaderUIScaler::statusButton
	Transform_t3275118058 * ___statusButton_16;
	// UISprite LauncherLoaderUIScaler::statusBackground
	UISprite_t603616735 * ___statusBackground_17;
	// UnityEngine.Transform LauncherLoaderUIScaler::initBulb
	Transform_t3275118058 * ___initBulb_18;
	// UnityEngine.Transform LauncherLoaderUIScaler::launchBulb
	Transform_t3275118058 * ___launchBulb_19;
	// UnityEngine.Transform LauncherLoaderUIScaler::skipButton
	Transform_t3275118058 * ___skipButton_20;
	// UISprite LauncherLoaderUIScaler::skipBackground
	UISprite_t603616735 * ___skipBackground_21;
	// UILabel LauncherLoaderUIScaler::skipLabel
	UILabel_t1795115428 * ___skipLabel_22;
	// UITexture LauncherLoaderUIScaler::reportBackground
	UITexture_t2537039969 * ___reportBackground_23;
	// UnityEngine.Transform LauncherLoaderUIScaler::reportTitle
	Transform_t3275118058 * ___reportTitle_24;
	// System.Collections.Generic.List`1<UnityEngine.Transform> LauncherLoaderUIScaler::reportLabels
	List_1_t2644239190 * ___reportLabels_25;

public:
	inline static int32_t get_offset_of_animationOutline_14() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___animationOutline_14)); }
	inline UITexture_t2537039969 * get_animationOutline_14() const { return ___animationOutline_14; }
	inline UITexture_t2537039969 ** get_address_of_animationOutline_14() { return &___animationOutline_14; }
	inline void set_animationOutline_14(UITexture_t2537039969 * value)
	{
		___animationOutline_14 = value;
		Il2CppCodeGenWriteBarrier(&___animationOutline_14, value);
	}

	inline static int32_t get_offset_of_animationMask_15() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___animationMask_15)); }
	inline UITexture_t2537039969 * get_animationMask_15() const { return ___animationMask_15; }
	inline UITexture_t2537039969 ** get_address_of_animationMask_15() { return &___animationMask_15; }
	inline void set_animationMask_15(UITexture_t2537039969 * value)
	{
		___animationMask_15 = value;
		Il2CppCodeGenWriteBarrier(&___animationMask_15, value);
	}

	inline static int32_t get_offset_of_statusButton_16() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___statusButton_16)); }
	inline Transform_t3275118058 * get_statusButton_16() const { return ___statusButton_16; }
	inline Transform_t3275118058 ** get_address_of_statusButton_16() { return &___statusButton_16; }
	inline void set_statusButton_16(Transform_t3275118058 * value)
	{
		___statusButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___statusButton_16, value);
	}

	inline static int32_t get_offset_of_statusBackground_17() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___statusBackground_17)); }
	inline UISprite_t603616735 * get_statusBackground_17() const { return ___statusBackground_17; }
	inline UISprite_t603616735 ** get_address_of_statusBackground_17() { return &___statusBackground_17; }
	inline void set_statusBackground_17(UISprite_t603616735 * value)
	{
		___statusBackground_17 = value;
		Il2CppCodeGenWriteBarrier(&___statusBackground_17, value);
	}

	inline static int32_t get_offset_of_initBulb_18() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___initBulb_18)); }
	inline Transform_t3275118058 * get_initBulb_18() const { return ___initBulb_18; }
	inline Transform_t3275118058 ** get_address_of_initBulb_18() { return &___initBulb_18; }
	inline void set_initBulb_18(Transform_t3275118058 * value)
	{
		___initBulb_18 = value;
		Il2CppCodeGenWriteBarrier(&___initBulb_18, value);
	}

	inline static int32_t get_offset_of_launchBulb_19() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___launchBulb_19)); }
	inline Transform_t3275118058 * get_launchBulb_19() const { return ___launchBulb_19; }
	inline Transform_t3275118058 ** get_address_of_launchBulb_19() { return &___launchBulb_19; }
	inline void set_launchBulb_19(Transform_t3275118058 * value)
	{
		___launchBulb_19 = value;
		Il2CppCodeGenWriteBarrier(&___launchBulb_19, value);
	}

	inline static int32_t get_offset_of_skipButton_20() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___skipButton_20)); }
	inline Transform_t3275118058 * get_skipButton_20() const { return ___skipButton_20; }
	inline Transform_t3275118058 ** get_address_of_skipButton_20() { return &___skipButton_20; }
	inline void set_skipButton_20(Transform_t3275118058 * value)
	{
		___skipButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___skipButton_20, value);
	}

	inline static int32_t get_offset_of_skipBackground_21() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___skipBackground_21)); }
	inline UISprite_t603616735 * get_skipBackground_21() const { return ___skipBackground_21; }
	inline UISprite_t603616735 ** get_address_of_skipBackground_21() { return &___skipBackground_21; }
	inline void set_skipBackground_21(UISprite_t603616735 * value)
	{
		___skipBackground_21 = value;
		Il2CppCodeGenWriteBarrier(&___skipBackground_21, value);
	}

	inline static int32_t get_offset_of_skipLabel_22() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___skipLabel_22)); }
	inline UILabel_t1795115428 * get_skipLabel_22() const { return ___skipLabel_22; }
	inline UILabel_t1795115428 ** get_address_of_skipLabel_22() { return &___skipLabel_22; }
	inline void set_skipLabel_22(UILabel_t1795115428 * value)
	{
		___skipLabel_22 = value;
		Il2CppCodeGenWriteBarrier(&___skipLabel_22, value);
	}

	inline static int32_t get_offset_of_reportBackground_23() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___reportBackground_23)); }
	inline UITexture_t2537039969 * get_reportBackground_23() const { return ___reportBackground_23; }
	inline UITexture_t2537039969 ** get_address_of_reportBackground_23() { return &___reportBackground_23; }
	inline void set_reportBackground_23(UITexture_t2537039969 * value)
	{
		___reportBackground_23 = value;
		Il2CppCodeGenWriteBarrier(&___reportBackground_23, value);
	}

	inline static int32_t get_offset_of_reportTitle_24() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___reportTitle_24)); }
	inline Transform_t3275118058 * get_reportTitle_24() const { return ___reportTitle_24; }
	inline Transform_t3275118058 ** get_address_of_reportTitle_24() { return &___reportTitle_24; }
	inline void set_reportTitle_24(Transform_t3275118058 * value)
	{
		___reportTitle_24 = value;
		Il2CppCodeGenWriteBarrier(&___reportTitle_24, value);
	}

	inline static int32_t get_offset_of_reportLabels_25() { return static_cast<int32_t>(offsetof(LauncherLoaderUIScaler_t1442786027, ___reportLabels_25)); }
	inline List_1_t2644239190 * get_reportLabels_25() const { return ___reportLabels_25; }
	inline List_1_t2644239190 ** get_address_of_reportLabels_25() { return &___reportLabels_25; }
	inline void set_reportLabels_25(List_1_t2644239190 * value)
	{
		___reportLabels_25 = value;
		Il2CppCodeGenWriteBarrier(&___reportLabels_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
