﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_502536975289.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_802536975284.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE2002277260.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE_21599056049.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Artefact2719757314.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_BrokenGlass3283168121.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical964237941.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical21938932783.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_CompressionF1500516684.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Distorted1709846242.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_LED2945198757.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Noise2320795570.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old3160266747.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie1608414762.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie_2256438953.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_PlanetMars2904232163.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Posterize2336945505.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Rgb1237953139.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Tiles183441211.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS3704714353.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS_Rewind3553104481.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Vcr3207321459.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Video3D4173711092.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Videoflip3917511384.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Vintage3232880030.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenCi1715479081.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHV2752996873.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHo4006230885.h"
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenVe3251907653.h"
#include "AssemblyU2DCSharp_CameraFilterPack_VHS_Tracking1539512780.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Blood227988326.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Crystal3702178000.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Plasma1366310808.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Psycho3419713482.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Tunnel582645848.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp3833859392.h"
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp23447343986.h"
#include "AssemblyU2DCSharp_Fabric_Answers_Answers1822067079.h"
#include "AssemblyU2DCSharp_Fabric_Answers_Internal_AnswersAp761141277.h"
#include "AssemblyU2DCSharp_Fabric_Answers_Internal_AnswersS4112235189.h"
#include "AssemblyU2DCSharp_Fabric_Crashlytics_Crashlytics3790431817.h"
#include "AssemblyU2DCSharp_Fabric_Internal_Crashlytics_Cras2902165287.h"
#include "AssemblyU2DCSharp_Fabric_Crashlytics_Internal_IOSI2874644713.h"
#include "AssemblyU2DCSharp_Fabric_Crashlytics_Internal_Impl2902205834.h"
#include "AssemblyU2DCSharp_Fabric_Crashlytics_Internal_Impl_236397402.h"
#include "AssemblyU2DCSharp_Fabric_Runtime_Fabric3026373293.h"
#include "AssemblyU2DCSharp_Fabric_Internal_FabricInit3443986959.h"
#include "AssemblyU2DCSharp_Fabric_Internal_Runtime_Utils925829391.h"
#include "AssemblyU2DCSharp_Fabric_Runtime_Internal_IOSImpl3175828673.h"
#include "AssemblyU2DCSharp_Fabric_Runtime_Internal_Impl1344545172.h"
#include "AssemblyU2DCSharp_Fabric_Internal_ThirdParty_MiniJ2375970760.h"
#include "AssemblyU2DCSharp_Fabric_Internal_ThirdParty_MiniJ4164574466.h"
#include "AssemblyU2DCSharp_Fabric_Internal_ThirdParty_MiniJSO67851492.h"
#include "AssemblyU2DCSharp_Fabric_Internal_ThirdParty_MiniJ3012148701.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_ConsoleBa4290192428.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_LogView3192394209.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MenuBase1506935956.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AccessToke400501458.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppEvents3685765292.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppInvite3031115107.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppLinks3144027900.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_AppReques1683098915.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_DialogShar329767461.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GameGroup3348180240.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ3721637485.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_GraphRequ3616411949.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_MainMenu3906043390.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Example_Pay1773509418.h"
#include "AssemblyU2DCSharp_GlitchEffect4241067900.h"
#include "AssemblyU2DCSharp_CacheTilesExample3121978397.h"
#include "AssemblyU2DCSharp_CalcAreaExample2814495786.h"
#include "AssemblyU2DCSharp_ChangeMapTextureExample833836899.h"
#include "AssemblyU2DCSharp_ChangeMarkerTextureExample402580549.h"
#include "AssemblyU2DCSharp_ChangePositionAndZoomEventsExamp1027651508.h"
#include "AssemblyU2DCSharp_CheckIntenetConnectionExample3945243703.h"
#include "AssemblyU2DCSharp_ControlDataTrafficExample3737142636.h"
#include "AssemblyU2DCSharp_CustomDownloadTileExample2726765381.h"
#include "AssemblyU2DCSharp_DetectWaterByTextureExample2775676976.h"
#include "AssemblyU2DCSharp_DragMarkerByLongPressExample724607740.h"
#include "AssemblyU2DCSharp_DrawingAPI_Example4110970837.h"
#include "AssemblyU2DCSharp_FindAutocompleteExample3995261805.h"
#include "AssemblyU2DCSharp_FindDirectionExample3722367702.h"
#include "AssemblyU2DCSharp_FindLocationExample2669706314.h"
#include "AssemblyU2DCSharp_FindPlacesExample604743347.h"
#include "AssemblyU2DCSharp_GetCenterPointOfMarkersExample3377133351.h"
#include "AssemblyU2DCSharp_GetElevationExample3555645451.h"
#include "AssemblyU2DCSharp_InterceptElevationRequestExample1706267016.h"
#include "AssemblyU2DCSharp_LockPositionAndZoomExample3560495400.h"
#include "AssemblyU2DCSharp_Marker3D_Example1432080764.h"
#include "AssemblyU2DCSharp_Marker3D_GPS_Example2514097371.h"
#include "AssemblyU2DCSharp_MarkerClickExample2196428416.h"
#include "AssemblyU2DCSharp_Marker_GPS_Example1835250040.h"
#include "AssemblyU2DCSharp_ModifyTooltipStyleExample824275140.h"
#include "AssemblyU2DCSharp_MoveMarkersExample2707127286.h"
#include "AssemblyU2DCSharp_OSMRequestExample188828912.h"
#include "AssemblyU2DCSharp_OnDrawTooltipExample3197201692.h"
#include "AssemblyU2DCSharp_SaveMarker3DExample3838390806.h"
#include "AssemblyU2DCSharp_SaveMarkersExample1902651330.h"
#include "AssemblyU2DCSharp_ScreenToGeoExample3755261980.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (CameraFilterPack_TV_50_t2536975289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4800[4] = 
{
	CameraFilterPack_TV_50_t2536975289::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_50_t2536975289::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_50_t2536975289::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_50_t2536975289::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (CameraFilterPack_TV_80_t2536975284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4801[4] = 
{
	CameraFilterPack_TV_80_t2536975284::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_80_t2536975284::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_80_t2536975284::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_80_t2536975284::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (CameraFilterPack_TV_ARCADE_t2002277260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4802[4] = 
{
	CameraFilterPack_TV_ARCADE_t2002277260::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_ARCADE_t2002277260::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_ARCADE_t2002277260::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_ARCADE_t2002277260::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (CameraFilterPack_TV_ARCADE_2_t1599056049), -1, sizeof(CameraFilterPack_TV_ARCADE_2_t1599056049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4803[12] = 
{
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_Interferance_Size_6(),
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_Interferance_Speed_7(),
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_Contrast_8(),
	CameraFilterPack_TV_ARCADE_2_t1599056049::get_offset_of_Value4_9(),
	CameraFilterPack_TV_ARCADE_2_t1599056049_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_ARCADE_2_t1599056049_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_ARCADE_2_t1599056049_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_ARCADE_2_t1599056049_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (CameraFilterPack_TV_Artefact_t2719757314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4804[7] = 
{
	CameraFilterPack_TV_Artefact_t2719757314::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Artefact_t2719757314::get_offset_of_ScreenResolution_3(),
	CameraFilterPack_TV_Artefact_t2719757314::get_offset_of_TimeX_4(),
	CameraFilterPack_TV_Artefact_t2719757314::get_offset_of_Colorisation_5(),
	CameraFilterPack_TV_Artefact_t2719757314::get_offset_of_Parasite_6(),
	CameraFilterPack_TV_Artefact_t2719757314::get_offset_of_Noise_7(),
	CameraFilterPack_TV_Artefact_t2719757314::get_offset_of_SCMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (CameraFilterPack_TV_BrokenGlass_t3283168121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4805[9] = 
{
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_Broken_Small_4(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_Broken_Medium_5(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_Broken_High_6(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_Broken_Big_7(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_LightReflect_8(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_SCMaterial_9(),
	CameraFilterPack_TV_BrokenGlass_t3283168121::get_offset_of_Texture2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (CameraFilterPack_TV_Chromatical_t964237941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4806[4] = 
{
	CameraFilterPack_TV_Chromatical_t964237941::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Chromatical_t964237941::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Chromatical_t964237941::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_Chromatical_t964237941::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (CameraFilterPack_TV_Chromatical2_t1938932783), -1, sizeof(CameraFilterPack_TV_Chromatical2_t1938932783_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4807[12] = 
{
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_Aberration_6(),
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_Value2_7(),
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_Value3_8(),
	CameraFilterPack_TV_Chromatical2_t1938932783::get_offset_of_Value4_9(),
	CameraFilterPack_TV_Chromatical2_t1938932783_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_Chromatical2_t1938932783_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_Chromatical2_t1938932783_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_Chromatical2_t1938932783_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (CameraFilterPack_TV_CompressionFX_t1500516684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4808[5] = 
{
	CameraFilterPack_TV_CompressionFX_t1500516684::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_CompressionFX_t1500516684::get_offset_of_ScreenResolution_3(),
	CameraFilterPack_TV_CompressionFX_t1500516684::get_offset_of_TimeX_4(),
	CameraFilterPack_TV_CompressionFX_t1500516684::get_offset_of_Parasite_5(),
	CameraFilterPack_TV_CompressionFX_t1500516684::get_offset_of_SCMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (CameraFilterPack_TV_Distorted_t1709846242), -1, sizeof(CameraFilterPack_TV_Distorted_t1709846242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4809[6] = 
{
	CameraFilterPack_TV_Distorted_t1709846242::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Distorted_t1709846242::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Distorted_t1709846242::get_offset_of_Distortion_4(),
	CameraFilterPack_TV_Distorted_t1709846242::get_offset_of_RGB_5(),
	CameraFilterPack_TV_Distorted_t1709846242::get_offset_of_SCMaterial_6(),
	CameraFilterPack_TV_Distorted_t1709846242_StaticFields::get_offset_of_ChangeDistortion_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (CameraFilterPack_TV_LED_t2945198757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4810[6] = 
{
	CameraFilterPack_TV_LED_t2945198757::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_LED_t2945198757::get_offset_of_ScreenResolution_3(),
	CameraFilterPack_TV_LED_t2945198757::get_offset_of_TimeX_4(),
	CameraFilterPack_TV_LED_t2945198757::get_offset_of_Distortion_5(),
	CameraFilterPack_TV_LED_t2945198757::get_offset_of_Size_6(),
	CameraFilterPack_TV_LED_t2945198757::get_offset_of_SCMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { sizeof (CameraFilterPack_TV_Noise_t2320795570), -1, sizeof(CameraFilterPack_TV_Noise_t2320795570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4811[12] = 
{
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_Size_6(),
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_LightBackGround_7(),
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_Speed_8(),
	CameraFilterPack_TV_Noise_t2320795570::get_offset_of_Size2_9(),
	CameraFilterPack_TV_Noise_t2320795570_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_Noise_t2320795570_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_Noise_t2320795570_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_Noise_t2320795570_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { sizeof (CameraFilterPack_TV_Old_t3160266747), -1, sizeof(CameraFilterPack_TV_Old_t3160266747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4812[5] = 
{
	CameraFilterPack_TV_Old_t3160266747::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Old_t3160266747::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Old_t3160266747::get_offset_of_Distortion_4(),
	CameraFilterPack_TV_Old_t3160266747::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Old_t3160266747_StaticFields::get_offset_of_ChangeDistortion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (CameraFilterPack_TV_Old_Movie_t1608414762), -1, sizeof(CameraFilterPack_TV_Old_Movie_t1608414762_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4813[5] = 
{
	CameraFilterPack_TV_Old_Movie_t1608414762::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Old_Movie_t1608414762::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Old_Movie_t1608414762::get_offset_of_Distortion_4(),
	CameraFilterPack_TV_Old_Movie_t1608414762::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Old_Movie_t1608414762_StaticFields::get_offset_of_ChangeDistortion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (CameraFilterPack_TV_Old_Movie_2_t256438953), -1, sizeof(CameraFilterPack_TV_Old_Movie_2_t256438953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4814[12] = 
{
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_FramePerSecond_6(),
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_Contrast_7(),
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_Burn_8(),
	CameraFilterPack_TV_Old_Movie_2_t256438953::get_offset_of_SceneCut_9(),
	CameraFilterPack_TV_Old_Movie_2_t256438953_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_Old_Movie_2_t256438953_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_Old_Movie_2_t256438953_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_Old_Movie_2_t256438953_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (CameraFilterPack_TV_PlanetMars_t2904232163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4815[5] = 
{
	CameraFilterPack_TV_PlanetMars_t2904232163::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_PlanetMars_t2904232163::get_offset_of_ScreenResolution_3(),
	CameraFilterPack_TV_PlanetMars_t2904232163::get_offset_of_TimeX_4(),
	CameraFilterPack_TV_PlanetMars_t2904232163::get_offset_of_Distortion_5(),
	CameraFilterPack_TV_PlanetMars_t2904232163::get_offset_of_SCMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { sizeof (CameraFilterPack_TV_Posterize_t2336945505), -1, sizeof(CameraFilterPack_TV_Posterize_t2336945505_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4816[5] = 
{
	CameraFilterPack_TV_Posterize_t2336945505::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Posterize_t2336945505::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Posterize_t2336945505::get_offset_of_Posterize_4(),
	CameraFilterPack_TV_Posterize_t2336945505::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Posterize_t2336945505_StaticFields::get_offset_of_ChangePosterize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { sizeof (CameraFilterPack_TV_Rgb_t1237953139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4817[5] = 
{
	CameraFilterPack_TV_Rgb_t1237953139::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Rgb_t1237953139::get_offset_of_ScreenResolution_3(),
	CameraFilterPack_TV_Rgb_t1237953139::get_offset_of_TimeX_4(),
	CameraFilterPack_TV_Rgb_t1237953139::get_offset_of_Distortion_5(),
	CameraFilterPack_TV_Rgb_t1237953139::get_offset_of_SCMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { sizeof (CameraFilterPack_TV_Tiles_t183441211), -1, sizeof(CameraFilterPack_TV_Tiles_t183441211_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4818[12] = 
{
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_Size_6(),
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_Intensity_7(),
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_StretchX_8(),
	CameraFilterPack_TV_Tiles_t183441211::get_offset_of_StretchY_9(),
	CameraFilterPack_TV_Tiles_t183441211_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_Tiles_t183441211_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_Tiles_t183441211_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_Tiles_t183441211_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (CameraFilterPack_TV_VHS_t3704714353), -1, sizeof(CameraFilterPack_TV_VHS_t3704714353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4819[12] = 
{
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_Cryptage_6(),
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_Parasite_7(),
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_Calibrage_8(),
	CameraFilterPack_TV_VHS_t3704714353::get_offset_of_WhiteParasite_9(),
	CameraFilterPack_TV_VHS_t3704714353_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_VHS_t3704714353_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_VHS_t3704714353_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_VHS_t3704714353_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { sizeof (CameraFilterPack_TV_VHS_Rewind_t3553104481), -1, sizeof(CameraFilterPack_TV_VHS_Rewind_t3553104481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4820[12] = 
{
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_Cryptage_6(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_Parasite_7(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_Parasite2_8(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481::get_offset_of_WhiteParasite_9(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_VHS_Rewind_t3553104481_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (CameraFilterPack_TV_Vcr_t3207321459), -1, sizeof(CameraFilterPack_TV_Vcr_t3207321459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4821[5] = 
{
	CameraFilterPack_TV_Vcr_t3207321459::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Vcr_t3207321459::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Vcr_t3207321459::get_offset_of_Distortion_4(),
	CameraFilterPack_TV_Vcr_t3207321459::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Vcr_t3207321459_StaticFields::get_offset_of_ChangeDistortion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { sizeof (CameraFilterPack_TV_Video3D_t4173711092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4822[4] = 
{
	CameraFilterPack_TV_Video3D_t4173711092::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Video3D_t4173711092::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Video3D_t4173711092::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_Video3D_t4173711092::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { sizeof (CameraFilterPack_TV_Videoflip_t3917511384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4823[4] = 
{
	CameraFilterPack_TV_Videoflip_t3917511384::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Videoflip_t3917511384::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Videoflip_t3917511384::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_Videoflip_t3917511384::get_offset_of_SCMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (CameraFilterPack_TV_Vintage_t3232880030), -1, sizeof(CameraFilterPack_TV_Vintage_t3232880030_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4824[5] = 
{
	CameraFilterPack_TV_Vintage_t3232880030::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_Vintage_t3232880030::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_Vintage_t3232880030::get_offset_of_Distortion_4(),
	CameraFilterPack_TV_Vintage_t3232880030::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_Vintage_t3232880030_StaticFields::get_offset_of_ChangeDistortion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (CameraFilterPack_TV_WideScreenCircle_t1715479081), -1, sizeof(CameraFilterPack_TV_WideScreenCircle_t1715479081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4825[12] = 
{
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_Size_6(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_Smooth_7(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_StretchX_8(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081::get_offset_of_StretchY_9(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_WideScreenCircle_t1715479081_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (CameraFilterPack_TV_WideScreenHV_t2752996873), -1, sizeof(CameraFilterPack_TV_WideScreenHV_t2752996873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4826[12] = 
{
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_Size_6(),
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_Smooth_7(),
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_StretchX_8(),
	CameraFilterPack_TV_WideScreenHV_t2752996873::get_offset_of_StretchY_9(),
	CameraFilterPack_TV_WideScreenHV_t2752996873_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_WideScreenHV_t2752996873_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_WideScreenHV_t2752996873_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_WideScreenHV_t2752996873_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { sizeof (CameraFilterPack_TV_WideScreenHorizontal_t4006230885), -1, sizeof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4827[12] = 
{
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_Size_6(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_Smooth_7(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_StretchX_8(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885::get_offset_of_StretchY_9(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (CameraFilterPack_TV_WideScreenVertical_t3251907653), -1, sizeof(CameraFilterPack_TV_WideScreenVertical_t3251907653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4828[12] = 
{
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_SCShader_2(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_TimeX_3(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_SCMaterial_5(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_Size_6(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_Smooth_7(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_StretchX_8(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653::get_offset_of_StretchY_9(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_TV_WideScreenVertical_t3251907653_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (CameraFilterPack_VHS_Tracking_t1539512780), -1, sizeof(CameraFilterPack_VHS_Tracking_t1539512780_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4829[6] = 
{
	CameraFilterPack_VHS_Tracking_t1539512780::get_offset_of_SCShader_2(),
	CameraFilterPack_VHS_Tracking_t1539512780::get_offset_of_TimeX_3(),
	CameraFilterPack_VHS_Tracking_t1539512780::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_VHS_Tracking_t1539512780::get_offset_of_SCMaterial_5(),
	CameraFilterPack_VHS_Tracking_t1539512780::get_offset_of_Tracking_6(),
	CameraFilterPack_VHS_Tracking_t1539512780_StaticFields::get_offset_of_ChangeTracking_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (CameraFilterPack_Vision_Blood_t227988326), -1, sizeof(CameraFilterPack_Vision_Blood_t227988326_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4830[12] = 
{
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_SCShader_2(),
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_TimeX_3(),
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_HoleSize_6(),
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_HoleSmooth_7(),
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_Color1_8(),
	CameraFilterPack_Vision_Blood_t227988326::get_offset_of_Color2_9(),
	CameraFilterPack_Vision_Blood_t227988326_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Vision_Blood_t227988326_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Vision_Blood_t227988326_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Vision_Blood_t227988326_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (CameraFilterPack_Vision_Crystal_t3702178000), -1, sizeof(CameraFilterPack_Vision_Crystal_t3702178000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4831[12] = 
{
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_SCShader_2(),
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_TimeX_3(),
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_Value_6(),
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_X_7(),
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_Y_8(),
	CameraFilterPack_Vision_Crystal_t3702178000::get_offset_of_Value4_9(),
	CameraFilterPack_Vision_Crystal_t3702178000_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Vision_Crystal_t3702178000_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Vision_Crystal_t3702178000_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Vision_Crystal_t3702178000_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (CameraFilterPack_Vision_Plasma_t1366310808), -1, sizeof(CameraFilterPack_Vision_Plasma_t1366310808_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4832[12] = 
{
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_SCShader_2(),
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_TimeX_3(),
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_Value_6(),
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_Value2_7(),
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_Intensity_8(),
	CameraFilterPack_Vision_Plasma_t1366310808::get_offset_of_Value4_9(),
	CameraFilterPack_Vision_Plasma_t1366310808_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Vision_Plasma_t1366310808_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Vision_Plasma_t1366310808_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Vision_Plasma_t1366310808_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (CameraFilterPack_Vision_Psycho_t3419713482), -1, sizeof(CameraFilterPack_Vision_Psycho_t3419713482_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4833[12] = 
{
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_SCShader_2(),
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_TimeX_3(),
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_HoleSize_6(),
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_HoleSmooth_7(),
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_Color1_8(),
	CameraFilterPack_Vision_Psycho_t3419713482::get_offset_of_Color2_9(),
	CameraFilterPack_Vision_Psycho_t3419713482_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Vision_Psycho_t3419713482_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Vision_Psycho_t3419713482_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Vision_Psycho_t3419713482_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (CameraFilterPack_Vision_Tunnel_t582645848), -1, sizeof(CameraFilterPack_Vision_Tunnel_t582645848_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4834[12] = 
{
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_SCShader_2(),
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_TimeX_3(),
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_Value_6(),
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_Value2_7(),
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_Intensity_8(),
	CameraFilterPack_Vision_Tunnel_t582645848::get_offset_of_Value4_9(),
	CameraFilterPack_Vision_Tunnel_t582645848_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Vision_Tunnel_t582645848_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Vision_Tunnel_t582645848_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Vision_Tunnel_t582645848_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (CameraFilterPack_Vision_Warp_t3833859392), -1, sizeof(CameraFilterPack_Vision_Warp_t3833859392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4835[12] = 
{
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_SCShader_2(),
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_TimeX_3(),
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_Value_6(),
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_Value2_7(),
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_Value3_8(),
	CameraFilterPack_Vision_Warp_t3833859392::get_offset_of_Value4_9(),
	CameraFilterPack_Vision_Warp_t3833859392_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Vision_Warp_t3833859392_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Vision_Warp_t3833859392_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Vision_Warp_t3833859392_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (CameraFilterPack_Vision_Warp2_t3447343986), -1, sizeof(CameraFilterPack_Vision_Warp2_t3447343986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4836[12] = 
{
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_SCShader_2(),
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_TimeX_3(),
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_ScreenResolution_4(),
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_SCMaterial_5(),
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_Value_6(),
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_Value2_7(),
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_Intensity_8(),
	CameraFilterPack_Vision_Warp2_t3447343986::get_offset_of_Value4_9(),
	CameraFilterPack_Vision_Warp2_t3447343986_StaticFields::get_offset_of_ChangeValue_10(),
	CameraFilterPack_Vision_Warp2_t3447343986_StaticFields::get_offset_of_ChangeValue2_11(),
	CameraFilterPack_Vision_Warp2_t3447343986_StaticFields::get_offset_of_ChangeValue3_12(),
	CameraFilterPack_Vision_Warp2_t3447343986_StaticFields::get_offset_of_ChangeValue4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (Answers_t1822067079), -1, sizeof(Answers_t1822067079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4837[1] = 
{
	Answers_t1822067079_StaticFields::get_offset_of_implementation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (AnswersAppleImplementation_t761141277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (AnswersStubImplementation_t4112235189), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (Crashlytics_t3790431817), -1, sizeof(Crashlytics_t3790431817_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4841[1] = 
{
	Crashlytics_t3790431817_StaticFields::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (CrashlyticsInit_t2902165287), -1, sizeof(CrashlyticsInit_t2902165287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4842[4] = 
{
	CrashlyticsInit_t2902165287_StaticFields::get_offset_of_kitName_2(),
	CrashlyticsInit_t2902165287_StaticFields::get_offset_of_instance_3(),
	CrashlyticsInit_t2902165287_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	CrashlyticsInit_t2902165287_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (IOSImpl_t2874644713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (Impl_t2902205834), -1, sizeof(Impl_t2902205834_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4844[6] = 
{
	0,
	Impl_t2902205834_StaticFields::get_offset_of_FrameArgsRegex_1(),
	Impl_t2902205834_StaticFields::get_offset_of_FrameRegexWithoutFileInfo_2(),
	Impl_t2902205834_StaticFields::get_offset_of_FrameRegexWithFileInfo_3(),
	Impl_t2902205834_StaticFields::get_offset_of_MonoFilenameUnknownString_4(),
	Impl_t2902205834_StaticFields::get_offset_of_StringDelimiters_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (FrameParser_t236397402), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { sizeof (Fabric_t3026373293), -1, sizeof(Fabric_t3026373293_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4846[1] = 
{
	Fabric_t3026373293_StaticFields::get_offset_of_impl_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { sizeof (FabricInit_t3443986959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (Utils_t925829391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { sizeof (IOSImpl_t3175828673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (Impl_t1344545172), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4850[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (Json_t2375970760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (Parser_t4164574466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4852[2] = 
{
	0,
	Parser_t4164574466::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (TOKEN_t67851492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4853[13] = 
{
	TOKEN_t67851492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (Serializer_t3012148701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4854[1] = 
{
	Serializer_t3012148701::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { sizeof (ConsoleBase_t4290192428), -1, sizeof(ConsoleBase_t4290192428_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4855[11] = 
{
	0,
	ConsoleBase_t4290192428_StaticFields::get_offset_of_menuStack_3(),
	ConsoleBase_t4290192428::get_offset_of_status_4(),
	ConsoleBase_t4290192428::get_offset_of_lastResponse_5(),
	ConsoleBase_t4290192428::get_offset_of_scrollPosition_6(),
	ConsoleBase_t4290192428::get_offset_of_scaleFactor_7(),
	ConsoleBase_t4290192428::get_offset_of_textStyle_8(),
	ConsoleBase_t4290192428::get_offset_of_buttonStyle_9(),
	ConsoleBase_t4290192428::get_offset_of_textInputStyle_10(),
	ConsoleBase_t4290192428::get_offset_of_labelStyle_11(),
	ConsoleBase_t4290192428::get_offset_of_U3CLastResponseTextureU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (LogView_t3192394209), -1, sizeof(LogView_t3192394209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4856[2] = 
{
	LogView_t3192394209_StaticFields::get_offset_of_datePatt_13(),
	LogView_t3192394209_StaticFields::get_offset_of_events_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (MenuBase_t1506935956), -1, sizeof(MenuBase_t1506935956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4857[1] = 
{
	MenuBase_t1506935956_StaticFields::get_offset_of_shareDialogMode_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (AccessTokenMenu_t400501458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { sizeof (AppEvents_t3685765292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (AppInvites_t3031115107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (AppLinks_t3144027900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4862 = { sizeof (AppRequests_t1683098915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4862[10] = 
{
	AppRequests_t1683098915::get_offset_of_requestMessage_14(),
	AppRequests_t1683098915::get_offset_of_requestTo_15(),
	AppRequests_t1683098915::get_offset_of_requestFilter_16(),
	AppRequests_t1683098915::get_offset_of_requestExcludes_17(),
	AppRequests_t1683098915::get_offset_of_requestMax_18(),
	AppRequests_t1683098915::get_offset_of_requestData_19(),
	AppRequests_t1683098915::get_offset_of_requestTitle_20(),
	AppRequests_t1683098915::get_offset_of_requestObjectID_21(),
	AppRequests_t1683098915::get_offset_of_selectedAction_22(),
	AppRequests_t1683098915::get_offset_of_actionTypeStrings_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4863 = { sizeof (DialogShare_t329767461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4863[11] = 
{
	DialogShare_t329767461::get_offset_of_shareLink_14(),
	DialogShare_t329767461::get_offset_of_shareTitle_15(),
	DialogShare_t329767461::get_offset_of_shareDescription_16(),
	DialogShare_t329767461::get_offset_of_shareImage_17(),
	DialogShare_t329767461::get_offset_of_feedTo_18(),
	DialogShare_t329767461::get_offset_of_feedLink_19(),
	DialogShare_t329767461::get_offset_of_feedTitle_20(),
	DialogShare_t329767461::get_offset_of_feedCaption_21(),
	DialogShare_t329767461::get_offset_of_feedDescription_22(),
	DialogShare_t329767461::get_offset_of_feedImage_23(),
	DialogShare_t329767461::get_offset_of_feedMediaSource_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4864 = { sizeof (GameGroups_t3348180240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4864[4] = 
{
	GameGroups_t3348180240::get_offset_of_gamerGroupName_14(),
	GameGroups_t3348180240::get_offset_of_gamerGroupDesc_15(),
	GameGroups_t3348180240::get_offset_of_gamerGroupPrivacy_16(),
	GameGroups_t3348180240::get_offset_of_gamerGroupCurrentGroup_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4865 = { sizeof (GraphRequest_t3721637485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4865[2] = 
{
	GraphRequest_t3721637485::get_offset_of_apiQuery_14(),
	GraphRequest_t3721637485::get_offset_of_profilePic_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4866 = { sizeof (U3CTakeScreenshotU3Ec__Iterator0_t3616411949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4866[9] = 
{
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U3CwidthU3E__0_0(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U3CheightU3E__1_1(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U3CtexU3E__2_2(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U3CscreenshotU3E__3_3(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U3CwwwFormU3E__4_4(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U24this_5(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U24current_6(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U24disposing_7(),
	U3CTakeScreenshotU3Ec__Iterator0_t3616411949::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4867 = { sizeof (MainMenu_t3906043390), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4868 = { sizeof (Pay_t1773509418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4868[1] = 
{
	Pay_t1773509418::get_offset_of_payProduct_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4869 = { sizeof (GlitchEffect_t4241067900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4869[8] = 
{
	GlitchEffect_t4241067900::get_offset_of_displacementMap_4(),
	GlitchEffect_t4241067900::get_offset_of_glitchup_5(),
	GlitchEffect_t4241067900::get_offset_of_glitchdown_6(),
	GlitchEffect_t4241067900::get_offset_of_flicker_7(),
	GlitchEffect_t4241067900::get_offset_of_glitchupTime_8(),
	GlitchEffect_t4241067900::get_offset_of_glitchdownTime_9(),
	GlitchEffect_t4241067900::get_offset_of_flickerTime_10(),
	GlitchEffect_t4241067900::get_offset_of_intensity_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4870 = { sizeof (CacheTilesExample_t3121978397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4871 = { sizeof (CalcAreaExample_t2814495786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4871[6] = 
{
	CalcAreaExample_t2814495786::get_offset_of_markerTexture_2(),
	CalcAreaExample_t2814495786::get_offset_of_api_3(),
	CalcAreaExample_t2814495786::get_offset_of_changed_4(),
	CalcAreaExample_t2814495786::get_offset_of_markers_5(),
	CalcAreaExample_t2814495786::get_offset_of_markerPositions_6(),
	CalcAreaExample_t2814495786::get_offset_of_polygon_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4872 = { sizeof (ChangeMapTextureExample_t833836899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4872[2] = 
{
	ChangeMapTextureExample_t833836899::get_offset_of_texture1_2(),
	ChangeMapTextureExample_t833836899::get_offset_of_texture2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4873 = { sizeof (ChangeMarkerTextureExample_t402580549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4873[1] = 
{
	ChangeMarkerTextureExample_t402580549::get_offset_of_newMarkerTexture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4874 = { sizeof (ChangePositionAndZoomEventsExample_t1027651508), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4875 = { sizeof (CheckIntenetConnectionExample_t3945243703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4876 = { sizeof (ControlDataTrafficExample_t3737142636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4876[1] = 
{
	ControlDataTrafficExample_t3737142636::get_offset_of_totalTileTraffic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4877 = { sizeof (CustomDownloadTileExample_t2726765381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4877[1] = 
{
	CustomDownloadTileExample_t2726765381::get_offset_of_api_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4878 = { sizeof (DetectWaterByTextureExample_t2775676976), -1, sizeof(DetectWaterByTextureExample_t2775676976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4878[2] = 
{
	DetectWaterByTextureExample_t2775676976_StaticFields::get_offset_of_waterColor_2(),
	DetectWaterByTextureExample_t2775676976::get_offset_of_mapForDetectWater_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4879 = { sizeof (DragMarkerByLongPressExample_t724607740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4880 = { sizeof (DrawingAPI_Example_t4110970837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4881 = { sizeof (FindAutocompleteExample_t3995261805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4882 = { sizeof (FindDirectionExample_t3722367702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4883 = { sizeof (FindLocationExample_t2669706314), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4884 = { sizeof (FindPlacesExample_t604743347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4885 = { sizeof (GetCenterPointOfMarkersExample_t3377133351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4886 = { sizeof (GetElevationExample_t3555645451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4887 = { sizeof (InterceptElevationRequestExample_t1706267016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4887[1] = 
{
	InterceptElevationRequestExample_t1706267016::get_offset_of_control_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4888 = { sizeof (LockPositionAndZoomExample_t3560495400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4889 = { sizeof (Marker3D_Example_t1432080764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4889[2] = 
{
	Marker3D_Example_t1432080764::get_offset_of_markerPrefab_2(),
	Marker3D_Example_t1432080764::get_offset_of_marker3D_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4890 = { sizeof (Marker3D_GPS_Example_t2514097371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4890[2] = 
{
	Marker3D_GPS_Example_t2514097371::get_offset_of_prefab_2(),
	Marker3D_GPS_Example_t2514097371::get_offset_of_locationMarker_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4891 = { sizeof (MarkerClickExample_t2196428416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4892 = { sizeof (Marker_GPS_Example_t1835250040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4892[1] = 
{
	Marker_GPS_Example_t1835250040::get_offset_of_playerMarker_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4893 = { sizeof (ModifyTooltipStyleExample_t824275140), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4894 = { sizeof (MoveMarkersExample_t2707127286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4894[6] = 
{
	MoveMarkersExample_t2707127286::get_offset_of_time_2(),
	MoveMarkersExample_t2707127286::get_offset_of_marker_3(),
	MoveMarkersExample_t2707127286::get_offset_of_fromPosition_4(),
	MoveMarkersExample_t2707127286::get_offset_of_toPosition_5(),
	MoveMarkersExample_t2707127286::get_offset_of_angle_6(),
	MoveMarkersExample_t2707127286::get_offset_of_direction_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4895 = { sizeof (OSMRequestExample_t188828912), -1, sizeof(OSMRequestExample_t188828912_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4895[1] = 
{
	OSMRequestExample_t188828912_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4896 = { sizeof (OnDrawTooltipExample_t3197201692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4897 = { sizeof (SaveMarker3DExample_t3838390806), -1, sizeof(SaveMarker3DExample_t3838390806_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4897[3] = 
{
	SaveMarker3DExample_t3838390806_StaticFields::get_offset_of_prefsKey_2(),
	SaveMarker3DExample_t3838390806::get_offset_of_markerPrefab_3(),
	SaveMarker3DExample_t3838390806::get_offset_of_markerScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4898 = { sizeof (SaveMarkersExample_t1902651330), -1, sizeof(SaveMarkersExample_t1902651330_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4898[1] = 
{
	SaveMarkersExample_t1902651330_StaticFields::get_offset_of_prefsKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4899 = { sizeof (ScreenToGeoExample_t3755261980), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
