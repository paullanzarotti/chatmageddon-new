﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// FriendAvatarUpdater
struct FriendAvatarUpdater_t4187090262;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReviewTargetLoader
struct  ReviewTargetLoader_t163570306  : public MonoBehaviour_t1158329972
{
public:
	// UILabel ReviewTargetLoader::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_2;
	// UILabel ReviewTargetLoader::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_3;
	// FriendAvatarUpdater ReviewTargetLoader::avatarUpdater
	FriendAvatarUpdater_t4187090262 * ___avatarUpdater_4;
	// UnityEngine.GameObject ReviewTargetLoader::firstStrikeObject
	GameObject_t1756533147 * ___firstStrikeObject_5;
	// UILabel ReviewTargetLoader::bonusAmountLabel
	UILabel_t1795115428 * ___bonusAmountLabel_6;

public:
	inline static int32_t get_offset_of_firstNameLabel_2() { return static_cast<int32_t>(offsetof(ReviewTargetLoader_t163570306, ___firstNameLabel_2)); }
	inline UILabel_t1795115428 * get_firstNameLabel_2() const { return ___firstNameLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_2() { return &___firstNameLabel_2; }
	inline void set_firstNameLabel_2(UILabel_t1795115428 * value)
	{
		___firstNameLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_2, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_3() { return static_cast<int32_t>(offsetof(ReviewTargetLoader_t163570306, ___lastNameLabel_3)); }
	inline UILabel_t1795115428 * get_lastNameLabel_3() const { return ___lastNameLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_3() { return &___lastNameLabel_3; }
	inline void set_lastNameLabel_3(UILabel_t1795115428 * value)
	{
		___lastNameLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_3, value);
	}

	inline static int32_t get_offset_of_avatarUpdater_4() { return static_cast<int32_t>(offsetof(ReviewTargetLoader_t163570306, ___avatarUpdater_4)); }
	inline FriendAvatarUpdater_t4187090262 * get_avatarUpdater_4() const { return ___avatarUpdater_4; }
	inline FriendAvatarUpdater_t4187090262 ** get_address_of_avatarUpdater_4() { return &___avatarUpdater_4; }
	inline void set_avatarUpdater_4(FriendAvatarUpdater_t4187090262 * value)
	{
		___avatarUpdater_4 = value;
		Il2CppCodeGenWriteBarrier(&___avatarUpdater_4, value);
	}

	inline static int32_t get_offset_of_firstStrikeObject_5() { return static_cast<int32_t>(offsetof(ReviewTargetLoader_t163570306, ___firstStrikeObject_5)); }
	inline GameObject_t1756533147 * get_firstStrikeObject_5() const { return ___firstStrikeObject_5; }
	inline GameObject_t1756533147 ** get_address_of_firstStrikeObject_5() { return &___firstStrikeObject_5; }
	inline void set_firstStrikeObject_5(GameObject_t1756533147 * value)
	{
		___firstStrikeObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___firstStrikeObject_5, value);
	}

	inline static int32_t get_offset_of_bonusAmountLabel_6() { return static_cast<int32_t>(offsetof(ReviewTargetLoader_t163570306, ___bonusAmountLabel_6)); }
	inline UILabel_t1795115428 * get_bonusAmountLabel_6() const { return ___bonusAmountLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_bonusAmountLabel_6() { return &___bonusAmountLabel_6; }
	inline void set_bonusAmountLabel_6(UILabel_t1795115428 * value)
	{
		___bonusAmountLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___bonusAmountLabel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
