﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Player
struct Player_t1147783557;

#include "AssemblyU2DCSharp_RadarMarkerBlip1130528275.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMarker
struct  PlayerMarker_t2200525527  : public RadarMarkerBlip_t1130528275
{
public:
	// Player PlayerMarker::player
	Player_t1147783557 * ___player_10;

public:
	inline static int32_t get_offset_of_player_10() { return static_cast<int32_t>(offsetof(PlayerMarker_t2200525527, ___player_10)); }
	inline Player_t1147783557 * get_player_10() const { return ___player_10; }
	inline Player_t1147783557 ** get_address_of_player_10() { return &___player_10; }
	inline void set_player_10(Player_t1147783557 * value)
	{
		___player_10 = value;
		Il2CppCodeGenWriteBarrier(&___player_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
