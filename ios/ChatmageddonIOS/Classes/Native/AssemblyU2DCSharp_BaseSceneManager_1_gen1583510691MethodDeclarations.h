﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m2286250849(__this, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m2913255246(__this, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::Start()
#define BaseSceneManager_1_Start_m1880772297(__this, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m2268096545(__this, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m3783533257(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m1912242759(__this, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<LoginSuccessSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m777668867(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m98668692(__this, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<LoginSuccessSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m1132368699(__this, method) ((  void (*) (BaseSceneManager_1_t1583510691 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
