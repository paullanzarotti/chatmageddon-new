﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BooleanSwitchButton
struct BooleanSwitchButton_t1408929844;

#include "codegen/il2cpp-codegen.h"

// System.Void BooleanSwitchButton::.ctor()
extern "C"  void BooleanSwitchButton__ctor_m1005884671 (BooleanSwitchButton_t1408929844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BooleanSwitchButton::OnClick()
extern "C"  void BooleanSwitchButton_OnClick_m1377929404 (BooleanSwitchButton_t1408929844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
