﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Decimal,System.Decimal>
struct Transform_1_t1240622329;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Decimal,System.Decimal>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4121209191_gshared (Transform_1_t1240622329 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m4121209191(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1240622329 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m4121209191_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Decimal,System.Decimal>::Invoke(TKey,TValue)
extern "C"  Decimal_t724701077  Transform_1_Invoke_m2776081363_gshared (Transform_1_t1240622329 * __this, Il2CppObject * ___key0, Decimal_t724701077  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2776081363(__this, ___key0, ___value1, method) ((  Decimal_t724701077  (*) (Transform_1_t1240622329 *, Il2CppObject *, Decimal_t724701077 , const MethodInfo*))Transform_1_Invoke_m2776081363_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Decimal,System.Decimal>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m568058762_gshared (Transform_1_t1240622329 * __this, Il2CppObject * ___key0, Decimal_t724701077  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m568058762(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1240622329 *, Il2CppObject *, Decimal_t724701077 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m568058762_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Decimal,System.Decimal>::EndInvoke(System.IAsyncResult)
extern "C"  Decimal_t724701077  Transform_1_EndInvoke_m451537649_gshared (Transform_1_t1240622329 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m451537649(__this, ___result0, method) ((  Decimal_t724701077  (*) (Transform_1_t1240622329 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m451537649_gshared)(__this, ___result0, method)
