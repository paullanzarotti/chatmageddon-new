﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AboutAndHelpType4001655409.h"
#include "AssemblyU2DCSharp_KnockoutRemovalType4219541724.h"
#include "AssemblyU2DCSharp_ChatmageddonServer594474938.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CPingServer2343871673.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CRegisterNe3368816153.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CSubmitMobi3026317709.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CVerifyMobil164903227.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CSubmitMobi2116236827.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CLoginViaUs3823169292.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CNotifyAppS1825316848.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CResetPoint3460021105.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CMarkAllNot1432190194.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CRetrieveBl3905741781.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CBlockUserU4045211069.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUnblockUse3544433809.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetLatestC3154160566.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateProf2030450453.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateURLP1909536015.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser3990550836.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUserU983683395.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser3037692135.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser1909199615.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser3815130747.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser3340540426.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser1500438975.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser1914041783.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser2660228245.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser3809964204.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUserA317880471.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser3937116268.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser1435546553.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser1601896396.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUser3859770858.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CUpdateUserS470248544.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CDeactivate1741638760.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CDeactivate2295932339.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CActivateSh3165385485.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CDeactivate2114188211.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CLaunchMiss2145793114.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CDefendMiss2333507770.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetFullWar3527477823.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetMissileW547944235.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetMineWar2059945141.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CClearFullW1707266925.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CClearMissile35438835.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CLayMineU3E2968755004.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CTriggerMin2536103459.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CDisarmMine2752246440.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetUnivers2160789767.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetChatFee3963748897.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CSaveContact211628084.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CDeleteCont1872176088.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CClearConta3782285915.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetPurchas3723821781.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CPurchaseIt3313395676.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CVerifyInAp4078131213.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetFullInv3467193277.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetMissileI519334381.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetShieldIn991045659.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetLeaderb3735323751.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetAboutAn2129019541.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CGetFAQInfo2671827046.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CActivateUse499954021.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CActivateUs2770701438.h"
#include "AssemblyU2DCSharp_ChatmageddonServer_U3CSetTotalPoi533271556.h"
#include "AssemblyU2DCSharp_UserPayload146188169.h"
#include "AssemblyU2DCSharp_ShieldManager3551654236.h"
#include "AssemblyU2DCSharp_ShieldManager_U3CTrackShieldU3Ec__85697528.h"
#include "AssemblyU2DCSharp_StealthModeCalculator3335204764.h"
#include "AssemblyU2DCSharp_StealthModeCalculator_OnZeroHit2015899103.h"
#include "AssemblyU2DCSharp_StealthModeCalculator_U3CCalcula4255380522.h"
#include "AssemblyU2DCSharp_BooleanSwitch869062888.h"
#include "AssemblyU2DCSharp_BooleanSwitchButton1408929844.h"
#include "AssemblyU2DCSharp_Bread457172030.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5400[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5401[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5402[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5403[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5404[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5405[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5406[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5407[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5408[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5409[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5410[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5411[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5412[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5413[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5414[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5416[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5417[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5418[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5419[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5420[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5421[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5422[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5423[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5424[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof (AboutAndHelpType_t4001655409)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5425[6] = 
{
	AboutAndHelpType_t4001655409::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof (KnockoutRemovalType_t4219541724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5426[5] = 
{
	KnockoutRemovalType_t4219541724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof (ChatmageddonServer_t594474938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5427[1] = 
{
	ChatmageddonServer_t594474938::get_offset_of_baseWebsiteURL_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof (U3CPingServerU3Ec__AnonStorey0_t2343871673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5428[2] = 
{
	U3CPingServerU3Ec__AnonStorey0_t2343871673::get_offset_of_callBack_0(),
	U3CPingServerU3Ec__AnonStorey0_t2343871673::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof (U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5429[2] = 
{
	U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153::get_offset_of_callBack_0(),
	U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof (U3CSubmitMobileNumberU3Ec__AnonStorey2_t3026317709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5430[2] = 
{
	U3CSubmitMobileNumberU3Ec__AnonStorey2_t3026317709::get_offset_of_callBack_0(),
	U3CSubmitMobileNumberU3Ec__AnonStorey2_t3026317709::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof (U3CVerifyMobileNumberU3Ec__AnonStorey3_t164903227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5431[1] = 
{
	U3CVerifyMobileNumberU3Ec__AnonStorey3_t164903227::get_offset_of_callBack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof (U3CSubmitMobilePinU3Ec__AnonStorey4_t2116236827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5432[1] = 
{
	U3CSubmitMobilePinU3Ec__AnonStorey4_t2116236827::get_offset_of_callBack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof (U3CLoginViaUsernameU3Ec__AnonStorey5_t3823169292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5433[2] = 
{
	U3CLoginViaUsernameU3Ec__AnonStorey5_t3823169292::get_offset_of_callBack_0(),
	U3CLoginViaUsernameU3Ec__AnonStorey5_t3823169292::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof (U3CNotifyAppStartU3Ec__AnonStorey6_t1825316848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5434[2] = 
{
	U3CNotifyAppStartU3Ec__AnonStorey6_t1825316848::get_offset_of_callBack_0(),
	U3CNotifyAppStartU3Ec__AnonStorey6_t1825316848::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof (U3CResetPointsU3Ec__AnonStorey7_t3460021105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5435[2] = 
{
	U3CResetPointsU3Ec__AnonStorey7_t3460021105::get_offset_of_callBack_0(),
	U3CResetPointsU3Ec__AnonStorey7_t3460021105::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof (U3CMarkAllNotificationsReadU3Ec__AnonStorey8_t1432190194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5436[2] = 
{
	U3CMarkAllNotificationsReadU3Ec__AnonStorey8_t1432190194::get_offset_of_callBack_0(),
	U3CMarkAllNotificationsReadU3Ec__AnonStorey8_t1432190194::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof (U3CRetrieveBlockedUsersU3Ec__AnonStorey9_t3905741781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5437[2] = 
{
	U3CRetrieveBlockedUsersU3Ec__AnonStorey9_t3905741781::get_offset_of_callBack_0(),
	U3CRetrieveBlockedUsersU3Ec__AnonStorey9_t3905741781::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof (U3CBlockUserU3Ec__AnonStoreyA_t4045211069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5438[2] = 
{
	U3CBlockUserU3Ec__AnonStoreyA_t4045211069::get_offset_of_callBack_0(),
	U3CBlockUserU3Ec__AnonStoreyA_t4045211069::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof (U3CUnblockUserU3Ec__AnonStoreyB_t3544433809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5439[2] = 
{
	U3CUnblockUserU3Ec__AnonStoreyB_t3544433809::get_offset_of_callBack_0(),
	U3CUnblockUserU3Ec__AnonStoreyB_t3544433809::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof (U3CGetLatestConfigU3Ec__AnonStoreyC_t3154160566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5440[2] = 
{
	U3CGetLatestConfigU3Ec__AnonStoreyC_t3154160566::get_offset_of_callBack_0(),
	U3CGetLatestConfigU3Ec__AnonStoreyC_t3154160566::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof (U3CUpdateProfileImageU3Ec__AnonStoreyD_t2030450453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5441[2] = 
{
	U3CUpdateProfileImageU3Ec__AnonStoreyD_t2030450453::get_offset_of_callBack_0(),
	U3CUpdateProfileImageU3Ec__AnonStoreyD_t2030450453::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof (U3CUpdateURLProfileImageU3Ec__AnonStoreyE_t1909536015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5442[2] = 
{
	U3CUpdateURLProfileImageU3Ec__AnonStoreyE_t1909536015::get_offset_of_callBack_0(),
	U3CUpdateURLProfileImageU3Ec__AnonStoreyE_t1909536015::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof (U3CUpdateUserEmailU3Ec__AnonStoreyF_t3990550836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5443[2] = 
{
	U3CUpdateUserEmailU3Ec__AnonStoreyF_t3990550836::get_offset_of_callBack_0(),
	U3CUpdateUserEmailU3Ec__AnonStoreyF_t3990550836::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof (U3CUpdateUserUsernameU3Ec__AnonStorey10_t983683395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5444[2] = 
{
	U3CUpdateUserUsernameU3Ec__AnonStorey10_t983683395::get_offset_of_callBack_0(),
	U3CUpdateUserUsernameU3Ec__AnonStorey10_t983683395::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof (U3CUpdateUserPasswordU3Ec__AnonStorey11_t3037692135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5445[2] = 
{
	U3CUpdateUserPasswordU3Ec__AnonStorey11_t3037692135::get_offset_of_callBack_0(),
	U3CUpdateUserPasswordU3Ec__AnonStorey11_t3037692135::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof (U3CUpdateUserFullNameU3Ec__AnonStorey12_t1909199615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5446[2] = 
{
	U3CUpdateUserFullNameU3Ec__AnonStorey12_t1909199615::get_offset_of_callBack_0(),
	U3CUpdateUserFullNameU3Ec__AnonStorey12_t1909199615::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof (U3CUpdateUserFirstNameU3Ec__AnonStorey13_t3815130747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5447[2] = 
{
	U3CUpdateUserFirstNameU3Ec__AnonStorey13_t3815130747::get_offset_of_callBack_0(),
	U3CUpdateUserFirstNameU3Ec__AnonStorey13_t3815130747::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof (U3CUpdateUserLastNameU3Ec__AnonStorey14_t3340540426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5448[2] = 
{
	U3CUpdateUserLastNameU3Ec__AnonStorey14_t3340540426::get_offset_of_callBack_0(),
	U3CUpdateUserLastNameU3Ec__AnonStorey14_t3340540426::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof (U3CUpdateUserGenderU3Ec__AnonStorey15_t1500438975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5449[2] = 
{
	U3CUpdateUserGenderU3Ec__AnonStorey15_t1500438975::get_offset_of_callBack_0(),
	U3CUpdateUserGenderU3Ec__AnonStorey15_t1500438975::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof (U3CUpdateUserAudienceU3Ec__AnonStorey16_t1914041783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5450[2] = 
{
	U3CUpdateUserAudienceU3Ec__AnonStorey16_t1914041783::get_offset_of_callBack_0(),
	U3CUpdateUserAudienceU3Ec__AnonStorey16_t1914041783::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof (U3CUpdateUserDisplayLocationU3Ec__AnonStorey17_t2660228245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5451[2] = 
{
	U3CUpdateUserDisplayLocationU3Ec__AnonStorey17_t2660228245::get_offset_of_callBack_0(),
	U3CUpdateUserDisplayLocationU3Ec__AnonStorey17_t2660228245::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof (U3CUpdateUserAudioMusicU3Ec__AnonStorey18_t3809964204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5452[2] = 
{
	U3CUpdateUserAudioMusicU3Ec__AnonStorey18_t3809964204::get_offset_of_callBack_0(),
	U3CUpdateUserAudioMusicU3Ec__AnonStorey18_t3809964204::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof (U3CUpdateUserAudioSFXU3Ec__AnonStorey19_t317880471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5453[2] = 
{
	U3CUpdateUserAudioSFXU3Ec__AnonStorey19_t317880471::get_offset_of_callBack_0(),
	U3CUpdateUserAudioSFXU3Ec__AnonStorey19_t317880471::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof (U3CUpdateUserNotificationsU3Ec__AnonStorey1A_t3937116268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5454[2] = 
{
	U3CUpdateUserNotificationsU3Ec__AnonStorey1A_t3937116268::get_offset_of_callBack_0(),
	U3CUpdateUserNotificationsU3Ec__AnonStorey1A_t3937116268::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof (U3CUpdateUserNotificationSoundU3Ec__AnonStorey1B_t1435546553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5455[2] = 
{
	U3CUpdateUserNotificationSoundU3Ec__AnonStorey1B_t1435546553::get_offset_of_callBack_0(),
	U3CUpdateUserNotificationSoundU3Ec__AnonStorey1B_t1435546553::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof (U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C_t1601896396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5456[2] = 
{
	U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C_t1601896396::get_offset_of_callBack_0(),
	U3CUpdateUserNotificationVibrateU3Ec__AnonStorey1C_t1601896396::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof (U3CUpdateUserLocationU3Ec__AnonStorey1D_t3859770858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5457[2] = 
{
	U3CUpdateUserLocationU3Ec__AnonStorey1D_t3859770858::get_offset_of_callBack_0(),
	U3CUpdateUserLocationU3Ec__AnonStorey1D_t3859770858::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof (U3CUpdateUserStealthModeU3Ec__AnonStorey1E_t470248544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5458[2] = 
{
	U3CUpdateUserStealthModeU3Ec__AnonStorey1E_t470248544::get_offset_of_callBack_0(),
	U3CUpdateUserStealthModeU3Ec__AnonStorey1E_t470248544::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof (U3CDeactivateUserStealthModeU3Ec__AnonStorey1F_t1741638760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5459[2] = 
{
	U3CDeactivateUserStealthModeU3Ec__AnonStorey1F_t1741638760::get_offset_of_callBack_0(),
	U3CDeactivateUserStealthModeU3Ec__AnonStorey1F_t1741638760::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof (U3CDeactivateUserKnockoutU3Ec__AnonStorey20_t2295932339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5460[2] = 
{
	U3CDeactivateUserKnockoutU3Ec__AnonStorey20_t2295932339::get_offset_of_callBack_0(),
	U3CDeactivateUserKnockoutU3Ec__AnonStorey20_t2295932339::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof (U3CActivateShieldU3Ec__AnonStorey21_t3165385485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5461[2] = 
{
	U3CActivateShieldU3Ec__AnonStorey21_t3165385485::get_offset_of_callBack_0(),
	U3CActivateShieldU3Ec__AnonStorey21_t3165385485::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof (U3CDeactivateShieldU3Ec__AnonStorey22_t2114188211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5462[2] = 
{
	U3CDeactivateShieldU3Ec__AnonStorey22_t2114188211::get_offset_of_callBack_0(),
	U3CDeactivateShieldU3Ec__AnonStorey22_t2114188211::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof (U3CLaunchMissileU3Ec__AnonStorey23_t2145793114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5463[2] = 
{
	U3CLaunchMissileU3Ec__AnonStorey23_t2145793114::get_offset_of_callBack_0(),
	U3CLaunchMissileU3Ec__AnonStorey23_t2145793114::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof (U3CDefendMissileU3Ec__AnonStorey24_t2333507770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5464[2] = 
{
	U3CDefendMissileU3Ec__AnonStorey24_t2333507770::get_offset_of_callBack_0(),
	U3CDefendMissileU3Ec__AnonStorey24_t2333507770::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof (U3CGetFullWarefareStatusU3Ec__AnonStorey25_t3527477823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5465[2] = 
{
	U3CGetFullWarefareStatusU3Ec__AnonStorey25_t3527477823::get_offset_of_callBack_0(),
	U3CGetFullWarefareStatusU3Ec__AnonStorey25_t3527477823::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof (U3CGetMissileWarefareStatusU3Ec__AnonStorey26_t547944235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5466[2] = 
{
	U3CGetMissileWarefareStatusU3Ec__AnonStorey26_t547944235::get_offset_of_callBack_0(),
	U3CGetMissileWarefareStatusU3Ec__AnonStorey26_t547944235::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof (U3CGetMineWarefareStatusU3Ec__AnonStorey27_t2059945141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5467[2] = 
{
	U3CGetMineWarefareStatusU3Ec__AnonStorey27_t2059945141::get_offset_of_callBack_0(),
	U3CGetMineWarefareStatusU3Ec__AnonStorey27_t2059945141::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof (U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5468[2] = 
{
	U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925::get_offset_of_callBack_0(),
	U3CClearFullWarefareStatusU3Ec__AnonStorey28_t1707266925::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof (U3CClearMissileWarefareStatusU3Ec__AnonStorey29_t35438835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5469[2] = 
{
	U3CClearMissileWarefareStatusU3Ec__AnonStorey29_t35438835::get_offset_of_callBack_0(),
	U3CClearMissileWarefareStatusU3Ec__AnonStorey29_t35438835::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof (U3CLayMineU3Ec__AnonStorey2A_t2968755004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5470[2] = 
{
	U3CLayMineU3Ec__AnonStorey2A_t2968755004::get_offset_of_callBack_0(),
	U3CLayMineU3Ec__AnonStorey2A_t2968755004::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof (U3CTriggerMineU3Ec__AnonStorey2B_t2536103459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5471[2] = 
{
	U3CTriggerMineU3Ec__AnonStorey2B_t2536103459::get_offset_of_callBack_0(),
	U3CTriggerMineU3Ec__AnonStorey2B_t2536103459::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof (U3CDisarmMineU3Ec__AnonStorey2C_t2752246440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5472[2] = 
{
	U3CDisarmMineU3Ec__AnonStorey2C_t2752246440::get_offset_of_callBack_0(),
	U3CDisarmMineU3Ec__AnonStorey2C_t2752246440::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof (U3CGetUniversalFeedU3Ec__AnonStorey2D_t2160789767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5473[2] = 
{
	U3CGetUniversalFeedU3Ec__AnonStorey2D_t2160789767::get_offset_of_callBack_0(),
	U3CGetUniversalFeedU3Ec__AnonStorey2D_t2160789767::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof (U3CGetChatFeedU3Ec__AnonStorey2E_t3963748897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5474[2] = 
{
	U3CGetChatFeedU3Ec__AnonStorey2E_t3963748897::get_offset_of_callBack_0(),
	U3CGetChatFeedU3Ec__AnonStorey2E_t3963748897::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof (U3CSaveContactsToServerU3Ec__AnonStorey2F_t211628084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5475[2] = 
{
	U3CSaveContactsToServerU3Ec__AnonStorey2F_t211628084::get_offset_of_callBack_0(),
	U3CSaveContactsToServerU3Ec__AnonStorey2F_t211628084::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof (U3CDeleteContactsFromServerU3Ec__AnonStorey30_t1872176088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5476[2] = 
{
	U3CDeleteContactsFromServerU3Ec__AnonStorey30_t1872176088::get_offset_of_callBack_0(),
	U3CDeleteContactsFromServerU3Ec__AnonStorey30_t1872176088::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof (U3CClearContactsFromServerU3Ec__AnonStorey31_t3782285915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5477[2] = 
{
	U3CClearContactsFromServerU3Ec__AnonStorey31_t3782285915::get_offset_of_callBack_0(),
	U3CClearContactsFromServerU3Ec__AnonStorey31_t3782285915::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof (U3CGetPurchasableItemsU3Ec__AnonStorey32_t3723821781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5478[2] = 
{
	U3CGetPurchasableItemsU3Ec__AnonStorey32_t3723821781::get_offset_of_callBack_0(),
	U3CGetPurchasableItemsU3Ec__AnonStorey32_t3723821781::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { sizeof (U3CPurchaseItemU3Ec__AnonStorey33_t3313395676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5479[2] = 
{
	U3CPurchaseItemU3Ec__AnonStorey33_t3313395676::get_offset_of_callBack_0(),
	U3CPurchaseItemU3Ec__AnonStorey33_t3313395676::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof (U3CVerifyInAppPurchaseU3Ec__AnonStorey34_t4078131213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5480[2] = 
{
	U3CVerifyInAppPurchaseU3Ec__AnonStorey34_t4078131213::get_offset_of_callBack_0(),
	U3CVerifyInAppPurchaseU3Ec__AnonStorey34_t4078131213::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof (U3CGetFullInventoryU3Ec__AnonStorey35_t3467193277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5481[2] = 
{
	U3CGetFullInventoryU3Ec__AnonStorey35_t3467193277::get_offset_of_callBack_0(),
	U3CGetFullInventoryU3Ec__AnonStorey35_t3467193277::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof (U3CGetMissileInventoryU3Ec__AnonStorey36_t519334381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5482[2] = 
{
	U3CGetMissileInventoryU3Ec__AnonStorey36_t519334381::get_offset_of_callBack_0(),
	U3CGetMissileInventoryU3Ec__AnonStorey36_t519334381::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof (U3CGetShieldInventoryU3Ec__AnonStorey37_t991045659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5483[2] = 
{
	U3CGetShieldInventoryU3Ec__AnonStorey37_t991045659::get_offset_of_callBack_0(),
	U3CGetShieldInventoryU3Ec__AnonStorey37_t991045659::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof (U3CGetLeaderboardPageU3Ec__AnonStorey38_t3735323751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5484[3] = 
{
	U3CGetLeaderboardPageU3Ec__AnonStorey38_t3735323751::get_offset_of_dailyPoints_0(),
	U3CGetLeaderboardPageU3Ec__AnonStorey38_t3735323751::get_offset_of_callBack_1(),
	U3CGetLeaderboardPageU3Ec__AnonStorey38_t3735323751::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof (U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5485[3] = 
{
	U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541::get_offset_of_callBack_0(),
	U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541::get_offset_of_type_1(),
	U3CGetAboutAndHelpInfoU3Ec__AnonStorey39_t2129019541::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof (U3CGetFAQInfoU3Ec__AnonStorey3A_t2671827046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5486[2] = 
{
	U3CGetFAQInfoU3Ec__AnonStorey3A_t2671827046::get_offset_of_callBack_0(),
	U3CGetFAQInfoU3Ec__AnonStorey3A_t2671827046::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof (U3CActivateUserStealthModeU3Ec__AnonStorey3B_t499954021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5487[2] = 
{
	U3CActivateUserStealthModeU3Ec__AnonStorey3B_t499954021::get_offset_of_callBack_0(),
	U3CActivateUserStealthModeU3Ec__AnonStorey3B_t499954021::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof (U3CActivateUserKnockoutU3Ec__AnonStorey3C_t2770701438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5488[2] = 
{
	U3CActivateUserKnockoutU3Ec__AnonStorey3C_t2770701438::get_offset_of_callBack_0(),
	U3CActivateUserKnockoutU3Ec__AnonStorey3C_t2770701438::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof (U3CSetTotalPointsU3Ec__AnonStorey3D_t533271556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5489[2] = 
{
	U3CSetTotalPointsU3Ec__AnonStorey3D_t533271556::get_offset_of_callBack_0(),
	U3CSetTotalPointsU3Ec__AnonStorey3D_t533271556::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof (UserPayload_t146188169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5490[34] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	UserPayload_t146188169::get_offset_of_tID_17(),
	UserPayload_t146188169::get_offset_of_tFirstName_18(),
	UserPayload_t146188169::get_offset_of_tSecondName_19(),
	UserPayload_t146188169::get_offset_of_tEmail_20(),
	UserPayload_t146188169::get_offset_of_tFullName_21(),
	UserPayload_t146188169::get_offset_of_tUserName_22(),
	UserPayload_t146188169::get_offset_of_tProfileImage_23(),
	UserPayload_t146188169::get_offset_of_tProfileImageThumbnail_24(),
	UserPayload_t146188169::get_offset_of_tGender_25(),
	UserPayload_t146188169::get_offset_of_tDisplayLocation_26(),
	UserPayload_t146188169::get_offset_of_tStealthMode_27(),
	UserPayload_t146188169::get_offset_of_tStealthModeActive_28(),
	UserPayload_t146188169::get_offset_of_tStealthModeHour_29(),
	UserPayload_t146188169::get_offset_of_tStealthModeMin_30(),
	UserPayload_t146188169::get_offset_of_tStealthModeDuration_31(),
	UserPayload_t146188169::get_offset_of_tLevel_32(),
	UserPayload_t146188169::get_offset_of_tRank_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof (ShieldManager_t3551654236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5491[2] = 
{
	ShieldManager_t3551654236::get_offset_of_missileShieldRoutine_3(),
	ShieldManager_t3551654236::get_offset_of_mineShieldRoutine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof (U3CTrackShieldU3Ec__Iterator0_t85697528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5492[8] = 
{
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_shield_0(),
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_U3CtotalShieldTimeU3E__0_1(),
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_U3CdifferenceU3E__1_2(),
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_U3CpercentageU3E__2_3(),
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_U24this_4(),
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_U24current_5(),
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_U24disposing_6(),
	U3CTrackShieldU3Ec__Iterator0_t85697528::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof (StealthModeCalculator_t3335204764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5493[5] = 
{
	StealthModeCalculator_t3335204764::get_offset_of_positiveColour_2(),
	StealthModeCalculator_t3335204764::get_offset_of_negativeColour_3(),
	StealthModeCalculator_t3335204764::get_offset_of_timeLabel_4(),
	StealthModeCalculator_t3335204764::get_offset_of_calcRoutine_5(),
	StealthModeCalculator_t3335204764::get_offset_of_onZeroHit_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof (OnZeroHit_t2015899103), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof (U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5495[5] = 
{
	U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522::get_offset_of_user_0(),
	U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522::get_offset_of_U24this_1(),
	U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522::get_offset_of_U24current_2(),
	U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522::get_offset_of_U24disposing_3(),
	U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof (BooleanSwitch_t869062888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5496[1] = 
{
	BooleanSwitch_t869062888::get_offset_of_currentSwitchState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof (BooleanSwitchButton_t1408929844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5497[1] = 
{
	BooleanSwitchButton_t1408929844::get_offset_of_sfxToPlay_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5498[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof (Bread_t457172030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5499[4] = 
{
	Bread_t457172030::get_offset_of_toastType_0(),
	Bread_t457172030::get_offset_of_showFade_1(),
	Bread_t457172030::get_offset_of_toastMessage_2(),
	Bread_t457172030::get_offset_of_toastSFX_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
