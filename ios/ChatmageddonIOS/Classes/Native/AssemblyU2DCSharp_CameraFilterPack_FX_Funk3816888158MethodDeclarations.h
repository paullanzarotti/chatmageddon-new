﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Funk
struct CameraFilterPack_FX_Funk_t3816888158;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Funk::.ctor()
extern "C"  void CameraFilterPack_FX_Funk__ctor_m4068838647 (CameraFilterPack_FX_Funk_t3816888158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Funk::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Funk_get_material_m2930805800 (CameraFilterPack_FX_Funk_t3816888158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::Start()
extern "C"  void CameraFilterPack_FX_Funk_Start_m2332037035 (CameraFilterPack_FX_Funk_t3816888158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Funk_OnRenderImage_m1288481315 (CameraFilterPack_FX_Funk_t3816888158 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::Update()
extern "C"  void CameraFilterPack_FX_Funk_Update_m838265192 (CameraFilterPack_FX_Funk_t3816888158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::OnDisable()
extern "C"  void CameraFilterPack_FX_Funk_OnDisable_m178035502 (CameraFilterPack_FX_Funk_t3816888158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
