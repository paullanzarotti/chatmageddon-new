﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<RetrieveUsersOutgoingPendindFriendRequests>c__AnonStorey11<System.Object>
struct U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t972729943;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<RetrieveUsersOutgoingPendindFriendRequests>c__AnonStorey11<System.Object>::.ctor()
extern "C"  void U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11__ctor_m1458330394_gshared (U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t972729943 * __this, const MethodInfo* method);
#define U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11__ctor_m1458330394(__this, method) ((  void (*) (U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t972729943 *, const MethodInfo*))U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11__ctor_m1458330394_gshared)(__this, method)
// System.Void BaseServer`1/<RetrieveUsersOutgoingPendindFriendRequests>c__AnonStorey11<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_U3CU3Em__0_m3759631627_gshared (U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t972729943 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_U3CU3Em__0_m3759631627(__this, ___request0, ___response1, method) ((  void (*) (U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_t972729943 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRetrieveUsersOutgoingPendindFriendRequestsU3Ec__AnonStorey11_U3CU3Em__0_m3759631627_gshared)(__this, ___request0, ___response1, method)
