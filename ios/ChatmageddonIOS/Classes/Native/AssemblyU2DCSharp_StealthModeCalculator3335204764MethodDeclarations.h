﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeCalculator
struct StealthModeCalculator_t3335204764;
// StealthModeCalculator/OnZeroHit
struct OnZeroHit_t2015899103;
// User
struct User_t719925459;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StealthModeCalculator_OnZeroHit2015899103.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void StealthModeCalculator::.ctor()
extern "C"  void StealthModeCalculator__ctor_m1371256963 (StealthModeCalculator_t3335204764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator::add_onZeroHit(StealthModeCalculator/OnZeroHit)
extern "C"  void StealthModeCalculator_add_onZeroHit_m794697170 (StealthModeCalculator_t3335204764 * __this, OnZeroHit_t2015899103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator::remove_onZeroHit(StealthModeCalculator/OnZeroHit)
extern "C"  void StealthModeCalculator_remove_onZeroHit_m2878645721 (StealthModeCalculator_t3335204764 * __this, OnZeroHit_t2015899103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator::StartCalculator(User)
extern "C"  void StealthModeCalculator_StartCalculator_m1691842218 (StealthModeCalculator_t3335204764 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator::StopCalculator()
extern "C"  void StealthModeCalculator_StopCalculator_m1131208295 (StealthModeCalculator_t3335204764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StealthModeCalculator::CalculateStealthTime(User)
extern "C"  Il2CppObject * StealthModeCalculator_CalculateStealthTime_m2615860228 (StealthModeCalculator_t3335204764 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator::SetTimeLabel(System.TimeSpan)
extern "C"  void StealthModeCalculator_SetTimeLabel_m317812012 (StealthModeCalculator_t3335204764 * __this, TimeSpan_t3430258949  ___span0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator::SetTimeToZero()
extern "C"  void StealthModeCalculator_SetTimeToZero_m2957315597 (StealthModeCalculator_t3335204764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator::SendZeroHitEvent(System.Boolean)
extern "C"  void StealthModeCalculator_SendZeroHitEvent_m311402655 (StealthModeCalculator_t3335204764 * __this, bool ___inStealth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
