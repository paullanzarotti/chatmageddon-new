﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.String
struct String_t;
// System.Configuration.InternalConfigurationSystem
struct InternalConfigurationSystem_t2108740756;
// System.Configuration.ConfigurationSaveEventHandler
struct ConfigurationSaveEventHandler_t3118213159;
// System.Configuration.Internal.IConfigSystem
struct IConfigSystem_t2950865222;
// System.Configuration.Internal.IInternalConfigHost
struct IInternalConfigHost_t3115158152;
// System.Configuration.AppSettingsSection
struct AppSettingsSection_t2255732047;
// System.Configuration.ConnectionStringsSection
struct ConnectionStringsSection_t306961967;
// System.Configuration.ContextInformation
struct ContextInformation_t250854957;
// System.Configuration.ConfigurationLocationCollection
struct ConfigurationLocationCollection_t1903842989;
// System.Configuration.ConfigurationSectionGroup
struct ConfigurationSectionGroup_t2230982736;
// System.Configuration.ConfigurationSectionGroupCollection
struct ConfigurationSectionGroupCollection_t575145286;
// System.Configuration.ConfigurationSectionCollection
struct ConfigurationSectionCollection_t4261113299;
// System.Configuration.ConfigurationSection
struct ConfigurationSection_t2600766927;
// System.Configuration.SectionInfo
struct SectionInfo_t1739019515;
// System.Configuration.SectionGroupInfo
struct SectionGroupInfo_t2346323570;
// System.Configuration.ConfigInfo
struct ConfigInfo_t546730838;
// System.IO.Stream
struct Stream_t3255436806;
// System.Xml.XmlTextWriter
struct XmlTextWriter_t2527250655;
// System.Xml.XmlReader
struct XmlReader_t3675626668;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Internal2108740756.h"
#include "System_Configuration_System_Configuration_Configur3118213159.h"
#include "System_Configuration_System_Configuration_SectionI1739019515.h"
#include "System_Configuration_System_Configuration_SectionG2346323570.h"
#include "System_Configuration_System_Configuration_Configur2600766927.h"
#include "System_Configuration_System_Configuration_Configur2230982736.h"
#include "System_Configuration_System_Configuration_ConfigInf546730838.h"
#include "System_Configuration_System_Configuration_Configura700320212.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"

// System.Void System.Configuration.Configuration::.ctor(System.Configuration.Configuration,System.String)
extern "C"  void Configuration__ctor_m3545852494 (Configuration_t3335372970 * __this, Configuration_t3335372970 * ___parent0, String_t* ___locationSubPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::.ctor(System.Configuration.InternalConfigurationSystem,System.String)
extern "C"  void Configuration__ctor_m88421328 (Configuration_t3335372970 * __this, InternalConfigurationSystem_t2108740756 * ___system0, String_t* ___locationSubPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::add_SaveStart(System.Configuration.ConfigurationSaveEventHandler)
extern "C"  void Configuration_add_SaveStart_m3760799418 (Il2CppObject * __this /* static, unused */, ConfigurationSaveEventHandler_t3118213159 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::remove_SaveStart(System.Configuration.ConfigurationSaveEventHandler)
extern "C"  void Configuration_remove_SaveStart_m324364753 (Il2CppObject * __this /* static, unused */, ConfigurationSaveEventHandler_t3118213159 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::add_SaveEnd(System.Configuration.ConfigurationSaveEventHandler)
extern "C"  void Configuration_add_SaveEnd_m2135527913 (Il2CppObject * __this /* static, unused */, ConfigurationSaveEventHandler_t3118213159 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::remove_SaveEnd(System.Configuration.ConfigurationSaveEventHandler)
extern "C"  void Configuration_remove_SaveEnd_m3985834544 (Il2CppObject * __this /* static, unused */, ConfigurationSaveEventHandler_t3118213159 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.Configuration::FindLocationConfiguration(System.String,System.Configuration.Configuration)
extern "C"  Configuration_t3335372970 * Configuration_FindLocationConfiguration_m4081437406 (Configuration_t3335372970 * __this, String_t* ___relativePath0, Configuration_t3335372970 * ___defaultConfiguration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::Init(System.Configuration.Internal.IConfigSystem,System.String,System.Configuration.Configuration)
extern "C"  void Configuration_Init_m2991025886 (Configuration_t3335372970 * __this, Il2CppObject * ___system0, String_t* ___configPath1, Configuration_t3335372970 * ___parent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.Configuration::get_Parent()
extern "C"  Configuration_t3335372970 * Configuration_get_Parent_m335637978 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::set_Parent(System.Configuration.Configuration)
extern "C"  void Configuration_set_Parent_m3106974805 (Configuration_t3335372970 * __this, Configuration_t3335372970 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.Configuration::GetParentWithFile()
extern "C"  Configuration_t3335372970 * Configuration_GetParentWithFile_m1651703053 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.Configuration::get_FileName()
extern "C"  String_t* Configuration_get_FileName_m445861912 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Internal.IInternalConfigHost System.Configuration.Configuration::get_ConfigHost()
extern "C"  Il2CppObject * Configuration_get_ConfigHost_m854336265 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.Configuration::get_LocationConfigPath()
extern "C"  String_t* Configuration_get_LocationConfigPath_m609030371 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.Configuration::GetLocationSubPath()
extern "C"  String_t* Configuration_GetLocationSubPath_m1558547220 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.Configuration::get_ConfigPath()
extern "C"  String_t* Configuration_get_ConfigPath_m937107392 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.AppSettingsSection System.Configuration.Configuration::get_AppSettings()
extern "C"  AppSettingsSection_t2255732047 * Configuration_get_AppSettings_m3357583159 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConnectionStringsSection System.Configuration.Configuration::get_ConnectionStrings()
extern "C"  ConnectionStringsSection_t306961967 * Configuration_get_ConnectionStrings_m3875189703 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.Configuration::get_FilePath()
extern "C"  String_t* Configuration_get_FilePath_m3184045596 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.Configuration::get_HasFile()
extern "C"  bool Configuration_get_HasFile_m1167170352 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ContextInformation System.Configuration.Configuration::get_EvaluationContext()
extern "C"  ContextInformation_t250854957 * Configuration_get_EvaluationContext_m1945874686 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationLocationCollection System.Configuration.Configuration::get_Locations()
extern "C"  ConfigurationLocationCollection_t1903842989 * Configuration_get_Locations_m2394414303 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.Configuration::get_NamespaceDeclared()
extern "C"  bool Configuration_get_NamespaceDeclared_m3739951207 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::set_NamespaceDeclared(System.Boolean)
extern "C"  void Configuration_set_NamespaceDeclared_m911650718 (Configuration_t3335372970 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSectionGroup System.Configuration.Configuration::get_RootSectionGroup()
extern "C"  ConfigurationSectionGroup_t2230982736 * Configuration_get_RootSectionGroup_m1643677634 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSectionGroupCollection System.Configuration.Configuration::get_SectionGroups()
extern "C"  ConfigurationSectionGroupCollection_t575145286 * Configuration_get_SectionGroups_m2782934403 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSectionCollection System.Configuration.Configuration::get_Sections()
extern "C"  ConfigurationSectionCollection_t4261113299 * Configuration_get_Sections_m3724328875 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSection System.Configuration.Configuration::GetSection(System.String)
extern "C"  ConfigurationSection_t2600766927 * Configuration_GetSection_m3056810807 (Configuration_t3335372970 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSectionGroup System.Configuration.Configuration::GetSectionGroup(System.String)
extern "C"  ConfigurationSectionGroup_t2230982736 * Configuration_GetSectionGroup_m3413139599 (Configuration_t3335372970 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSection System.Configuration.Configuration::GetSectionInstance(System.Configuration.SectionInfo,System.Boolean)
extern "C"  ConfigurationSection_t2600766927 * Configuration_GetSectionInstance_m3769455073 (Configuration_t3335372970 * __this, SectionInfo_t1739019515 * ___config0, bool ___createDefaultInstance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationSectionGroup System.Configuration.Configuration::GetSectionGroupInstance(System.Configuration.SectionGroupInfo)
extern "C"  ConfigurationSectionGroup_t2230982736 * Configuration_GetSectionGroupInstance_m2028928705 (Configuration_t3335372970 * __this, SectionGroupInfo_t2346323570 * ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::SetConfigurationSection(System.Configuration.SectionInfo,System.Configuration.ConfigurationSection)
extern "C"  void Configuration_SetConfigurationSection_m894439304 (Configuration_t3335372970 * __this, SectionInfo_t1739019515 * ___config0, ConfigurationSection_t2600766927 * ___sec1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::SetSectionXml(System.Configuration.SectionInfo,System.String)
extern "C"  void Configuration_SetSectionXml_m2783526347 (Configuration_t3335372970 * __this, SectionInfo_t1739019515 * ___config0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.Configuration::GetSectionXml(System.Configuration.SectionInfo)
extern "C"  String_t* Configuration_GetSectionXml_m1792995434 (Configuration_t3335372970 * __this, SectionInfo_t1739019515 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::CreateSection(System.Configuration.SectionGroupInfo,System.String,System.Configuration.ConfigurationSection)
extern "C"  void Configuration_CreateSection_m399327647 (Configuration_t3335372970 * __this, SectionGroupInfo_t2346323570 * ___group0, String_t* ___name1, ConfigurationSection_t2600766927 * ___sec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::CreateSectionGroup(System.Configuration.SectionGroupInfo,System.String,System.Configuration.ConfigurationSectionGroup)
extern "C"  void Configuration_CreateSectionGroup_m2702672015 (Configuration_t3335372970 * __this, SectionGroupInfo_t2346323570 * ___parentGroup0, String_t* ___name1, ConfigurationSectionGroup_t2230982736 * ___sec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::RemoveConfigInfo(System.Configuration.ConfigInfo)
extern "C"  void Configuration_RemoveConfigInfo_m2687739614 (Configuration_t3335372970 * __this, ConfigInfo_t546730838 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::Save()
extern "C"  void Configuration_Save_m2166161860 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::Save(System.Configuration.ConfigurationSaveMode)
extern "C"  void Configuration_Save_m2324547891 (Configuration_t3335372970 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::Save(System.Configuration.ConfigurationSaveMode,System.Boolean)
extern "C"  void Configuration_Save_m1131810356 (Configuration_t3335372970 * __this, int32_t ___mode0, bool ___forceUpdateAll1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::SaveAs(System.String)
extern "C"  void Configuration_SaveAs_m340891490 (Configuration_t3335372970 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::SaveAs(System.String,System.Configuration.ConfigurationSaveMode)
extern "C"  void Configuration_SaveAs_m3729371083 (Configuration_t3335372970 * __this, String_t* ___filename0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::SaveAs(System.String,System.Configuration.ConfigurationSaveMode,System.Boolean)
extern "C"  void Configuration_SaveAs_m2882047816 (Configuration_t3335372970 * __this, String_t* ___filename0, int32_t ___mode1, bool ___forceUpdateAll2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::Save(System.IO.Stream,System.Configuration.ConfigurationSaveMode,System.Boolean)
extern "C"  void Configuration_Save_m1490486211 (Configuration_t3335372970 * __this, Stream_t3255436806 * ___stream0, int32_t ___mode1, bool ___forceUpdateAll2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::SaveData(System.Xml.XmlTextWriter,System.Configuration.ConfigurationSaveMode,System.Boolean)
extern "C"  void Configuration_SaveData_m4249599867 (Configuration_t3335372970 * __this, XmlTextWriter_t2527250655 * ___tw0, int32_t ___mode1, bool ___forceUpdateAll2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.Configuration::Load()
extern "C"  bool Configuration_Load_m851220319 (Configuration_t3335372970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::ReadConfigFile(System.Xml.XmlReader,System.String)
extern "C"  void Configuration_ReadConfigFile_m1878607525 (Configuration_t3335372970 * __this, XmlReader_t3675626668 * ___reader0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::ReadData(System.Xml.XmlReader,System.Boolean)
extern "C"  void Configuration_ReadData_m342895002 (Configuration_t3335372970 * __this, XmlReader_t3675626668 * ___reader0, bool ___allowOverride1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.Configuration::ThrowException(System.String,System.Xml.XmlReader)
extern "C"  void Configuration_ThrowException_m3079645534 (Configuration_t3335372970 * __this, String_t* ___text0, XmlReader_t3675626668 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
