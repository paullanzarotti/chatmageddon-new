﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlockListItemUIScaler
struct BlockListItemUIScaler_t2422342852;

#include "codegen/il2cpp-codegen.h"

// System.Void BlockListItemUIScaler::.ctor()
extern "C"  void BlockListItemUIScaler__ctor_m2854299239 (BlockListItemUIScaler_t2422342852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockListItemUIScaler::GlobalUIScale()
extern "C"  void BlockListItemUIScaler_GlobalUIScale_m2647477242 (BlockListItemUIScaler_t2422342852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
