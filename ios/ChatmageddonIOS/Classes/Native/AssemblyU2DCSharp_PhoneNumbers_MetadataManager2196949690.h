﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Int32,PhoneNumbers.PhoneMetadata>
struct Dictionary_2_t3669654334;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t406167000;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.MetadataManager
struct  MetadataManager_t2196949690  : public Il2CppObject
{
public:

public:
};

struct MetadataManager_t2196949690_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,PhoneNumbers.PhoneMetadata> PhoneNumbers.MetadataManager::callingCodeToAlternateFormatsMap
	Dictionary_2_t3669654334 * ___callingCodeToAlternateFormatsMap_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>> PhoneNumbers.MetadataManager::countryCodeSet
	Dictionary_2_t406167000 * ___countryCodeSet_2;

public:
	inline static int32_t get_offset_of_callingCodeToAlternateFormatsMap_1() { return static_cast<int32_t>(offsetof(MetadataManager_t2196949690_StaticFields, ___callingCodeToAlternateFormatsMap_1)); }
	inline Dictionary_2_t3669654334 * get_callingCodeToAlternateFormatsMap_1() const { return ___callingCodeToAlternateFormatsMap_1; }
	inline Dictionary_2_t3669654334 ** get_address_of_callingCodeToAlternateFormatsMap_1() { return &___callingCodeToAlternateFormatsMap_1; }
	inline void set_callingCodeToAlternateFormatsMap_1(Dictionary_2_t3669654334 * value)
	{
		___callingCodeToAlternateFormatsMap_1 = value;
		Il2CppCodeGenWriteBarrier(&___callingCodeToAlternateFormatsMap_1, value);
	}

	inline static int32_t get_offset_of_countryCodeSet_2() { return static_cast<int32_t>(offsetof(MetadataManager_t2196949690_StaticFields, ___countryCodeSet_2)); }
	inline Dictionary_2_t406167000 * get_countryCodeSet_2() const { return ___countryCodeSet_2; }
	inline Dictionary_2_t406167000 ** get_address_of_countryCodeSet_2() { return &___countryCodeSet_2; }
	inline void set_countryCodeSet_2(Dictionary_2_t406167000 * value)
	{
		___countryCodeSet_2 = value;
		Il2CppCodeGenWriteBarrier(&___countryCodeSet_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
