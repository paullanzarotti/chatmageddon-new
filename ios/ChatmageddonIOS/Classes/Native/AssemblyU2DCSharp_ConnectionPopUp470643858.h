﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TweenAlpha
struct TweenAlpha_t2421518635;
// ChatmageddonLoadingIndicator
struct ChatmageddonLoadingIndicator_t472672888;
// UILabel
struct UILabel_t1795115428;
// RetryConnectionButton
struct RetryConnectionButton_t1348204850;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen221309578.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConnectionPopUp
struct  ConnectionPopUp_t470643858  : public MonoSingleton_1_t221309578
{
public:
	// UnityEngine.GameObject ConnectionPopUp::UI
	GameObject_t1756533147 * ___UI_3;
	// TweenAlpha ConnectionPopUp::backgroundAlphaTween
	TweenAlpha_t2421518635 * ___backgroundAlphaTween_4;
	// ChatmageddonLoadingIndicator ConnectionPopUp::loadingIndicator
	ChatmageddonLoadingIndicator_t472672888 * ___loadingIndicator_5;
	// TweenAlpha ConnectionPopUp::indicatorAlphaTween
	TweenAlpha_t2421518635 * ___indicatorAlphaTween_6;
	// UILabel ConnectionPopUp::reconnectLabel
	UILabel_t1795115428 * ___reconnectLabel_7;
	// TweenAlpha ConnectionPopUp::reconnectLabelAlphaTween
	TweenAlpha_t2421518635 * ___reconnectLabelAlphaTween_8;
	// UILabel ConnectionPopUp::messageLabel
	UILabel_t1795115428 * ___messageLabel_9;
	// TweenAlpha ConnectionPopUp::messageLabelAlphaTween
	TweenAlpha_t2421518635 * ___messageLabelAlphaTween_10;
	// TweenAlpha ConnectionPopUp::buttonSpriteAlphaTween
	TweenAlpha_t2421518635 * ___buttonSpriteAlphaTween_11;
	// TweenAlpha ConnectionPopUp::buttonLabelAlphaTween
	TweenAlpha_t2421518635 * ___buttonLabelAlphaTween_12;
	// RetryConnectionButton ConnectionPopUp::retryButton
	RetryConnectionButton_t1348204850 * ___retryButton_13;
	// System.Boolean ConnectionPopUp::backgroundUIClosing
	bool ___backgroundUIClosing_14;
	// System.Boolean ConnectionPopUp::attempt
	bool ___attempt_15;
	// System.Boolean ConnectionPopUp::closeAttempt
	bool ___closeAttempt_16;
	// UnityEngine.Coroutine ConnectionPopUp::checkRoutine
	Coroutine_t2299508840 * ___checkRoutine_17;
	// UnityEngine.Coroutine ConnectionPopUp::waitRoutine
	Coroutine_t2299508840 * ___waitRoutine_18;
	// System.Single ConnectionPopUp::currentTime
	float ___currentTime_19;
	// System.Single ConnectionPopUp::showTime
	float ___showTime_20;

public:
	inline static int32_t get_offset_of_UI_3() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___UI_3)); }
	inline GameObject_t1756533147 * get_UI_3() const { return ___UI_3; }
	inline GameObject_t1756533147 ** get_address_of_UI_3() { return &___UI_3; }
	inline void set_UI_3(GameObject_t1756533147 * value)
	{
		___UI_3 = value;
		Il2CppCodeGenWriteBarrier(&___UI_3, value);
	}

	inline static int32_t get_offset_of_backgroundAlphaTween_4() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___backgroundAlphaTween_4)); }
	inline TweenAlpha_t2421518635 * get_backgroundAlphaTween_4() const { return ___backgroundAlphaTween_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundAlphaTween_4() { return &___backgroundAlphaTween_4; }
	inline void set_backgroundAlphaTween_4(TweenAlpha_t2421518635 * value)
	{
		___backgroundAlphaTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundAlphaTween_4, value);
	}

	inline static int32_t get_offset_of_loadingIndicator_5() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___loadingIndicator_5)); }
	inline ChatmageddonLoadingIndicator_t472672888 * get_loadingIndicator_5() const { return ___loadingIndicator_5; }
	inline ChatmageddonLoadingIndicator_t472672888 ** get_address_of_loadingIndicator_5() { return &___loadingIndicator_5; }
	inline void set_loadingIndicator_5(ChatmageddonLoadingIndicator_t472672888 * value)
	{
		___loadingIndicator_5 = value;
		Il2CppCodeGenWriteBarrier(&___loadingIndicator_5, value);
	}

	inline static int32_t get_offset_of_indicatorAlphaTween_6() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___indicatorAlphaTween_6)); }
	inline TweenAlpha_t2421518635 * get_indicatorAlphaTween_6() const { return ___indicatorAlphaTween_6; }
	inline TweenAlpha_t2421518635 ** get_address_of_indicatorAlphaTween_6() { return &___indicatorAlphaTween_6; }
	inline void set_indicatorAlphaTween_6(TweenAlpha_t2421518635 * value)
	{
		___indicatorAlphaTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___indicatorAlphaTween_6, value);
	}

	inline static int32_t get_offset_of_reconnectLabel_7() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___reconnectLabel_7)); }
	inline UILabel_t1795115428 * get_reconnectLabel_7() const { return ___reconnectLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_reconnectLabel_7() { return &___reconnectLabel_7; }
	inline void set_reconnectLabel_7(UILabel_t1795115428 * value)
	{
		___reconnectLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___reconnectLabel_7, value);
	}

	inline static int32_t get_offset_of_reconnectLabelAlphaTween_8() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___reconnectLabelAlphaTween_8)); }
	inline TweenAlpha_t2421518635 * get_reconnectLabelAlphaTween_8() const { return ___reconnectLabelAlphaTween_8; }
	inline TweenAlpha_t2421518635 ** get_address_of_reconnectLabelAlphaTween_8() { return &___reconnectLabelAlphaTween_8; }
	inline void set_reconnectLabelAlphaTween_8(TweenAlpha_t2421518635 * value)
	{
		___reconnectLabelAlphaTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___reconnectLabelAlphaTween_8, value);
	}

	inline static int32_t get_offset_of_messageLabel_9() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___messageLabel_9)); }
	inline UILabel_t1795115428 * get_messageLabel_9() const { return ___messageLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_messageLabel_9() { return &___messageLabel_9; }
	inline void set_messageLabel_9(UILabel_t1795115428 * value)
	{
		___messageLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___messageLabel_9, value);
	}

	inline static int32_t get_offset_of_messageLabelAlphaTween_10() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___messageLabelAlphaTween_10)); }
	inline TweenAlpha_t2421518635 * get_messageLabelAlphaTween_10() const { return ___messageLabelAlphaTween_10; }
	inline TweenAlpha_t2421518635 ** get_address_of_messageLabelAlphaTween_10() { return &___messageLabelAlphaTween_10; }
	inline void set_messageLabelAlphaTween_10(TweenAlpha_t2421518635 * value)
	{
		___messageLabelAlphaTween_10 = value;
		Il2CppCodeGenWriteBarrier(&___messageLabelAlphaTween_10, value);
	}

	inline static int32_t get_offset_of_buttonSpriteAlphaTween_11() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___buttonSpriteAlphaTween_11)); }
	inline TweenAlpha_t2421518635 * get_buttonSpriteAlphaTween_11() const { return ___buttonSpriteAlphaTween_11; }
	inline TweenAlpha_t2421518635 ** get_address_of_buttonSpriteAlphaTween_11() { return &___buttonSpriteAlphaTween_11; }
	inline void set_buttonSpriteAlphaTween_11(TweenAlpha_t2421518635 * value)
	{
		___buttonSpriteAlphaTween_11 = value;
		Il2CppCodeGenWriteBarrier(&___buttonSpriteAlphaTween_11, value);
	}

	inline static int32_t get_offset_of_buttonLabelAlphaTween_12() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___buttonLabelAlphaTween_12)); }
	inline TweenAlpha_t2421518635 * get_buttonLabelAlphaTween_12() const { return ___buttonLabelAlphaTween_12; }
	inline TweenAlpha_t2421518635 ** get_address_of_buttonLabelAlphaTween_12() { return &___buttonLabelAlphaTween_12; }
	inline void set_buttonLabelAlphaTween_12(TweenAlpha_t2421518635 * value)
	{
		___buttonLabelAlphaTween_12 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabelAlphaTween_12, value);
	}

	inline static int32_t get_offset_of_retryButton_13() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___retryButton_13)); }
	inline RetryConnectionButton_t1348204850 * get_retryButton_13() const { return ___retryButton_13; }
	inline RetryConnectionButton_t1348204850 ** get_address_of_retryButton_13() { return &___retryButton_13; }
	inline void set_retryButton_13(RetryConnectionButton_t1348204850 * value)
	{
		___retryButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___retryButton_13, value);
	}

	inline static int32_t get_offset_of_backgroundUIClosing_14() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___backgroundUIClosing_14)); }
	inline bool get_backgroundUIClosing_14() const { return ___backgroundUIClosing_14; }
	inline bool* get_address_of_backgroundUIClosing_14() { return &___backgroundUIClosing_14; }
	inline void set_backgroundUIClosing_14(bool value)
	{
		___backgroundUIClosing_14 = value;
	}

	inline static int32_t get_offset_of_attempt_15() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___attempt_15)); }
	inline bool get_attempt_15() const { return ___attempt_15; }
	inline bool* get_address_of_attempt_15() { return &___attempt_15; }
	inline void set_attempt_15(bool value)
	{
		___attempt_15 = value;
	}

	inline static int32_t get_offset_of_closeAttempt_16() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___closeAttempt_16)); }
	inline bool get_closeAttempt_16() const { return ___closeAttempt_16; }
	inline bool* get_address_of_closeAttempt_16() { return &___closeAttempt_16; }
	inline void set_closeAttempt_16(bool value)
	{
		___closeAttempt_16 = value;
	}

	inline static int32_t get_offset_of_checkRoutine_17() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___checkRoutine_17)); }
	inline Coroutine_t2299508840 * get_checkRoutine_17() const { return ___checkRoutine_17; }
	inline Coroutine_t2299508840 ** get_address_of_checkRoutine_17() { return &___checkRoutine_17; }
	inline void set_checkRoutine_17(Coroutine_t2299508840 * value)
	{
		___checkRoutine_17 = value;
		Il2CppCodeGenWriteBarrier(&___checkRoutine_17, value);
	}

	inline static int32_t get_offset_of_waitRoutine_18() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___waitRoutine_18)); }
	inline Coroutine_t2299508840 * get_waitRoutine_18() const { return ___waitRoutine_18; }
	inline Coroutine_t2299508840 ** get_address_of_waitRoutine_18() { return &___waitRoutine_18; }
	inline void set_waitRoutine_18(Coroutine_t2299508840 * value)
	{
		___waitRoutine_18 = value;
		Il2CppCodeGenWriteBarrier(&___waitRoutine_18, value);
	}

	inline static int32_t get_offset_of_currentTime_19() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___currentTime_19)); }
	inline float get_currentTime_19() const { return ___currentTime_19; }
	inline float* get_address_of_currentTime_19() { return &___currentTime_19; }
	inline void set_currentTime_19(float value)
	{
		___currentTime_19 = value;
	}

	inline static int32_t get_offset_of_showTime_20() { return static_cast<int32_t>(offsetof(ConnectionPopUp_t470643858, ___showTime_20)); }
	inline float get_showTime_20() const { return ___showTime_20; }
	inline float* get_address_of_showTime_20() { return &___showTime_20; }
	inline void set_showTime_20(float value)
	{
		___showTime_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
