﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackGameNavScreen
struct AttackGameNavScreen_t4158424117;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackGameNavScreen::.ctor()
extern "C"  void AttackGameNavScreen__ctor_m3162264502 (AttackGameNavScreen_t4158424117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackGameNavScreen::Start()
extern "C"  void AttackGameNavScreen_Start_m1274555366 (AttackGameNavScreen_t4158424117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackGameNavScreen::UIClosing()
extern "C"  void AttackGameNavScreen_UIClosing_m1602666623 (AttackGameNavScreen_t4158424117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackGameNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackGameNavScreen_ScreenLoad_m961365350 (AttackGameNavScreen_t4158424117 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackGameNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackGameNavScreen_ScreenUnload_m324832438 (AttackGameNavScreen_t4158424117 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackGameNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackGameNavScreen_ValidateScreenNavigation_m3118201423 (AttackGameNavScreen_t4158424117 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
