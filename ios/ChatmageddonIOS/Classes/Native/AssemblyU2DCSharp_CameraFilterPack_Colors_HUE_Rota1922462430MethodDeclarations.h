﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_HUE_Rotate
struct CameraFilterPack_Colors_HUE_Rotate_t1922462430;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_HUE_Rotate::.ctor()
extern "C"  void CameraFilterPack_Colors_HUE_Rotate__ctor_m2733459327 (CameraFilterPack_Colors_HUE_Rotate_t1922462430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_HUE_Rotate::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_HUE_Rotate_get_material_m3927615064 (CameraFilterPack_Colors_HUE_Rotate_t1922462430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::Start()
extern "C"  void CameraFilterPack_Colors_HUE_Rotate_Start_m2771257323 (CameraFilterPack_Colors_HUE_Rotate_t1922462430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_HUE_Rotate_OnRenderImage_m1712146899 (CameraFilterPack_Colors_HUE_Rotate_t1922462430 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::OnValidate()
extern "C"  void CameraFilterPack_Colors_HUE_Rotate_OnValidate_m2470439226 (CameraFilterPack_Colors_HUE_Rotate_t1922462430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::Update()
extern "C"  void CameraFilterPack_Colors_HUE_Rotate_Update_m1540824104 (CameraFilterPack_Colors_HUE_Rotate_t1922462430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::OnDisable()
extern "C"  void CameraFilterPack_Colors_HUE_Rotate_OnDisable_m1625952630 (CameraFilterPack_Colors_HUE_Rotate_t1922462430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
