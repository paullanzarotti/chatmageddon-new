﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XText
struct XText_t1860529549;
// System.String
struct String_t;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XText1860529549.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"

// System.Void System.Xml.Linq.XText::.ctor(System.String)
extern "C"  void XText__ctor_m2852593626 (XText_t1860529549 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XText::.ctor(System.Xml.Linq.XText)
extern "C"  void XText__ctor_m2404229261 (XText_t1860529549 * __this, XText_t1860529549 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XText::get_NodeType()
extern "C"  int32_t XText_get_NodeType_m18198915 (XText_t1860529549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XText::get_Value()
extern "C"  String_t* XText_get_Value_m334359707 (XText_t1860529549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XText::set_Value(System.String)
extern "C"  void XText_set_Value_m1761166796 (XText_t1860529549 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XText::WriteTo(System.Xml.XmlWriter)
extern "C"  void XText_WriteTo_m2354977436 (XText_t1860529549 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
