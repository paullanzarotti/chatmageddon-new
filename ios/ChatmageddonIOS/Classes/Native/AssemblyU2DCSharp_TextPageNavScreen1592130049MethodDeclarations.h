﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextPageNavScreen
struct TextPageNavScreen_t1592130049;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "AssemblyU2DCSharp_AboutAndHelpUI3809615169.h"

// System.Void TextPageNavScreen::.ctor()
extern "C"  void TextPageNavScreen__ctor_m1401915644 (TextPageNavScreen_t1592130049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextPageNavScreen::Start()
extern "C"  void TextPageNavScreen_Start_m1068754972 (TextPageNavScreen_t1592130049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextPageNavScreen::UIClosing()
extern "C"  void TextPageNavScreen_UIClosing_m2021479347 (TextPageNavScreen_t1592130049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextPageNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void TextPageNavScreen_ScreenLoad_m3257945964 (TextPageNavScreen_t1592130049 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextPageNavScreen::LoadUI(AboutAndHelpUI)
extern "C"  void TextPageNavScreen_LoadUI_m3320659249 (TextPageNavScreen_t1592130049 * __this, int32_t ___uiToLoad0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextPageNavScreen::UnLoadAllUI()
extern "C"  void TextPageNavScreen_UnLoadAllUI_m3173397584 (TextPageNavScreen_t1592130049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextPageNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void TextPageNavScreen_ScreenUnload_m3566947244 (TextPageNavScreen_t1592130049 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TextPageNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool TextPageNavScreen_ValidateScreenNavigation_m4049572195 (TextPageNavScreen_t1592130049 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
