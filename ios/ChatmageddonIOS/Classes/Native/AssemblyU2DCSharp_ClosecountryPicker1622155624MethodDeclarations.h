﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ClosecountryPicker
struct ClosecountryPicker_t1622155624;

#include "codegen/il2cpp-codegen.h"

// System.Void ClosecountryPicker::.ctor()
extern "C"  void ClosecountryPicker__ctor_m3570927971 (ClosecountryPicker_t1622155624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ClosecountryPicker::OnButtonClick()
extern "C"  void ClosecountryPicker_OnButtonClick_m2140276960 (ClosecountryPicker_t1622155624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
