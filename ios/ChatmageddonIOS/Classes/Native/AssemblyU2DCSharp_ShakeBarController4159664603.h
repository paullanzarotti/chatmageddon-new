﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShakeMiniGame
struct ShakeMiniGame_t406515851;
// ShakeCheck
struct ShakeCheck_t3101070106;
// UIPanelProgressBar
struct UIPanelProgressBar_t2014302056;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShakeBarController
struct  ShakeBarController_t4159664603  : public MonoBehaviour_t1158329972
{
public:
	// ShakeMiniGame ShakeBarController::miniGame
	ShakeMiniGame_t406515851 * ___miniGame_2;
	// System.Single ShakeBarController::maxShakeSeconds
	float ___maxShakeSeconds_3;
	// ShakeCheck ShakeBarController::shakeChecker
	ShakeCheck_t3101070106 * ___shakeChecker_4;
	// UIPanelProgressBar ShakeBarController::progressBar
	UIPanelProgressBar_t2014302056 * ___progressBar_5;
	// UnityEngine.Coroutine ShakeBarController::shakeCheckRoutine
	Coroutine_t2299508840 * ___shakeCheckRoutine_6;
	// UILabel ShakeBarController::timeLabel
	UILabel_t1795115428 * ___timeLabel_7;
	// System.Single ShakeBarController::totalTime
	float ___totalTime_8;

public:
	inline static int32_t get_offset_of_miniGame_2() { return static_cast<int32_t>(offsetof(ShakeBarController_t4159664603, ___miniGame_2)); }
	inline ShakeMiniGame_t406515851 * get_miniGame_2() const { return ___miniGame_2; }
	inline ShakeMiniGame_t406515851 ** get_address_of_miniGame_2() { return &___miniGame_2; }
	inline void set_miniGame_2(ShakeMiniGame_t406515851 * value)
	{
		___miniGame_2 = value;
		Il2CppCodeGenWriteBarrier(&___miniGame_2, value);
	}

	inline static int32_t get_offset_of_maxShakeSeconds_3() { return static_cast<int32_t>(offsetof(ShakeBarController_t4159664603, ___maxShakeSeconds_3)); }
	inline float get_maxShakeSeconds_3() const { return ___maxShakeSeconds_3; }
	inline float* get_address_of_maxShakeSeconds_3() { return &___maxShakeSeconds_3; }
	inline void set_maxShakeSeconds_3(float value)
	{
		___maxShakeSeconds_3 = value;
	}

	inline static int32_t get_offset_of_shakeChecker_4() { return static_cast<int32_t>(offsetof(ShakeBarController_t4159664603, ___shakeChecker_4)); }
	inline ShakeCheck_t3101070106 * get_shakeChecker_4() const { return ___shakeChecker_4; }
	inline ShakeCheck_t3101070106 ** get_address_of_shakeChecker_4() { return &___shakeChecker_4; }
	inline void set_shakeChecker_4(ShakeCheck_t3101070106 * value)
	{
		___shakeChecker_4 = value;
		Il2CppCodeGenWriteBarrier(&___shakeChecker_4, value);
	}

	inline static int32_t get_offset_of_progressBar_5() { return static_cast<int32_t>(offsetof(ShakeBarController_t4159664603, ___progressBar_5)); }
	inline UIPanelProgressBar_t2014302056 * get_progressBar_5() const { return ___progressBar_5; }
	inline UIPanelProgressBar_t2014302056 ** get_address_of_progressBar_5() { return &___progressBar_5; }
	inline void set_progressBar_5(UIPanelProgressBar_t2014302056 * value)
	{
		___progressBar_5 = value;
		Il2CppCodeGenWriteBarrier(&___progressBar_5, value);
	}

	inline static int32_t get_offset_of_shakeCheckRoutine_6() { return static_cast<int32_t>(offsetof(ShakeBarController_t4159664603, ___shakeCheckRoutine_6)); }
	inline Coroutine_t2299508840 * get_shakeCheckRoutine_6() const { return ___shakeCheckRoutine_6; }
	inline Coroutine_t2299508840 ** get_address_of_shakeCheckRoutine_6() { return &___shakeCheckRoutine_6; }
	inline void set_shakeCheckRoutine_6(Coroutine_t2299508840 * value)
	{
		___shakeCheckRoutine_6 = value;
		Il2CppCodeGenWriteBarrier(&___shakeCheckRoutine_6, value);
	}

	inline static int32_t get_offset_of_timeLabel_7() { return static_cast<int32_t>(offsetof(ShakeBarController_t4159664603, ___timeLabel_7)); }
	inline UILabel_t1795115428 * get_timeLabel_7() const { return ___timeLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_7() { return &___timeLabel_7; }
	inline void set_timeLabel_7(UILabel_t1795115428 * value)
	{
		___timeLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_7, value);
	}

	inline static int32_t get_offset_of_totalTime_8() { return static_cast<int32_t>(offsetof(ShakeBarController_t4159664603, ___totalTime_8)); }
	inline float get_totalTime_8() const { return ___totalTime_8; }
	inline float* get_address_of_totalTime_8() { return &___totalTime_8; }
	inline void set_totalTime_8(float value)
	{
		___totalTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
