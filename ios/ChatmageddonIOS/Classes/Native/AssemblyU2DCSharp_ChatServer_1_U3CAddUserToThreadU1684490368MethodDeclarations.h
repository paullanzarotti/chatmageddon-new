﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1/<AddUserToThread>c__AnonStorey5<System.Object>
struct U3CAddUserToThreadU3Ec__AnonStorey5_t1684490368;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatServer`1/<AddUserToThread>c__AnonStorey5<System.Object>::.ctor()
extern "C"  void U3CAddUserToThreadU3Ec__AnonStorey5__ctor_m917030583_gshared (U3CAddUserToThreadU3Ec__AnonStorey5_t1684490368 * __this, const MethodInfo* method);
#define U3CAddUserToThreadU3Ec__AnonStorey5__ctor_m917030583(__this, method) ((  void (*) (U3CAddUserToThreadU3Ec__AnonStorey5_t1684490368 *, const MethodInfo*))U3CAddUserToThreadU3Ec__AnonStorey5__ctor_m917030583_gshared)(__this, method)
// System.Void ChatServer`1/<AddUserToThread>c__AnonStorey5<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CAddUserToThreadU3Ec__AnonStorey5_U3CU3Em__0_m740628648_gshared (U3CAddUserToThreadU3Ec__AnonStorey5_t1684490368 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CAddUserToThreadU3Ec__AnonStorey5_U3CU3Em__0_m740628648(__this, ___request0, ___response1, method) ((  void (*) (U3CAddUserToThreadU3Ec__AnonStorey5_t1684490368 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CAddUserToThreadU3Ec__AnonStorey5_U3CU3Em__0_m740628648_gshared)(__this, ___request0, ___response1, method)
