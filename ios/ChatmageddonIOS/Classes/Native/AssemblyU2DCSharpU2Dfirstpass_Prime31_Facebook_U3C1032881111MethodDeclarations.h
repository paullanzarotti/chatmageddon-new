﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.Facebook/<getMe>c__AnonStorey2
struct U3CgetMeU3Ec__AnonStorey2_t1032881111;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Prime31.Facebook/<getMe>c__AnonStorey2::.ctor()
extern "C"  void U3CgetMeU3Ec__AnonStorey2__ctor_m3818283966 (U3CgetMeU3Ec__AnonStorey2_t1032881111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook/<getMe>c__AnonStorey2::<>m__0(System.String,System.Object)
extern "C"  void U3CgetMeU3Ec__AnonStorey2_U3CU3Em__0_m4143350591 (U3CgetMeU3Ec__AnonStorey2_t1032881111 * __this, String_t* ___error0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
