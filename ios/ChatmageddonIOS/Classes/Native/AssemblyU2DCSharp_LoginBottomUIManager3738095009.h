﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// TweenPosition
struct TweenPosition_t1144714832;
// UsernameLoginButton
struct UsernameLoginButton_t2274508449;
// UserLoginButton
struct UserLoginButton_t3500827568;
// UserRegistrationButton
struct UserRegistrationButton_t1399913110;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3488760729.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginBottomUIManager
struct  LoginBottomUIManager_t3738095009  : public MonoSingleton_1_t3488760729
{
public:
	// UnityEngine.GameObject LoginBottomUIManager::startPage
	GameObject_t1756533147 * ___startPage_3;
	// TweenPosition LoginBottomUIManager::startTween
	TweenPosition_t1144714832 * ___startTween_4;
	// UsernameLoginButton LoginBottomUIManager::loginButton
	UsernameLoginButton_t2274508449 * ___loginButton_5;
	// UserLoginButton LoginBottomUIManager::menuLoginButton
	UserLoginButton_t3500827568 * ___menuLoginButton_6;
	// UserRegistrationButton LoginBottomUIManager::registerationButton
	UserRegistrationButton_t1399913110 * ___registerationButton_7;

public:
	inline static int32_t get_offset_of_startPage_3() { return static_cast<int32_t>(offsetof(LoginBottomUIManager_t3738095009, ___startPage_3)); }
	inline GameObject_t1756533147 * get_startPage_3() const { return ___startPage_3; }
	inline GameObject_t1756533147 ** get_address_of_startPage_3() { return &___startPage_3; }
	inline void set_startPage_3(GameObject_t1756533147 * value)
	{
		___startPage_3 = value;
		Il2CppCodeGenWriteBarrier(&___startPage_3, value);
	}

	inline static int32_t get_offset_of_startTween_4() { return static_cast<int32_t>(offsetof(LoginBottomUIManager_t3738095009, ___startTween_4)); }
	inline TweenPosition_t1144714832 * get_startTween_4() const { return ___startTween_4; }
	inline TweenPosition_t1144714832 ** get_address_of_startTween_4() { return &___startTween_4; }
	inline void set_startTween_4(TweenPosition_t1144714832 * value)
	{
		___startTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___startTween_4, value);
	}

	inline static int32_t get_offset_of_loginButton_5() { return static_cast<int32_t>(offsetof(LoginBottomUIManager_t3738095009, ___loginButton_5)); }
	inline UsernameLoginButton_t2274508449 * get_loginButton_5() const { return ___loginButton_5; }
	inline UsernameLoginButton_t2274508449 ** get_address_of_loginButton_5() { return &___loginButton_5; }
	inline void set_loginButton_5(UsernameLoginButton_t2274508449 * value)
	{
		___loginButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___loginButton_5, value);
	}

	inline static int32_t get_offset_of_menuLoginButton_6() { return static_cast<int32_t>(offsetof(LoginBottomUIManager_t3738095009, ___menuLoginButton_6)); }
	inline UserLoginButton_t3500827568 * get_menuLoginButton_6() const { return ___menuLoginButton_6; }
	inline UserLoginButton_t3500827568 ** get_address_of_menuLoginButton_6() { return &___menuLoginButton_6; }
	inline void set_menuLoginButton_6(UserLoginButton_t3500827568 * value)
	{
		___menuLoginButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___menuLoginButton_6, value);
	}

	inline static int32_t get_offset_of_registerationButton_7() { return static_cast<int32_t>(offsetof(LoginBottomUIManager_t3738095009, ___registerationButton_7)); }
	inline UserRegistrationButton_t1399913110 * get_registerationButton_7() const { return ___registerationButton_7; }
	inline UserRegistrationButton_t1399913110 ** get_address_of_registerationButton_7() { return &___registerationButton_7; }
	inline void set_registerationButton_7(UserRegistrationButton_t1399913110 * value)
	{
		___registerationButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___registerationButton_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
