﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackSearchGlobalNavScreen
struct  AttackSearchGlobalNavScreen_t581792468  : public NavigationScreen_t2333230110
{
public:
	// System.Boolean AttackSearchGlobalNavScreen::UIclosing
	bool ___UIclosing_3;

public:
	inline static int32_t get_offset_of_UIclosing_3() { return static_cast<int32_t>(offsetof(AttackSearchGlobalNavScreen_t581792468, ___UIclosing_3)); }
	inline bool get_UIclosing_3() const { return ___UIclosing_3; }
	inline bool* get_address_of_UIclosing_3() { return &___UIclosing_3; }
	inline void set_UIclosing_3(bool value)
	{
		___UIclosing_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
