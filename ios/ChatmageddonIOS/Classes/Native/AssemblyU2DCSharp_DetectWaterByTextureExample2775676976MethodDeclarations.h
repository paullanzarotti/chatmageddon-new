﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DetectWaterByTextureExample
struct DetectWaterByTextureExample_t2775676976;

#include "codegen/il2cpp-codegen.h"

// System.Void DetectWaterByTextureExample::.ctor()
extern "C"  void DetectWaterByTextureExample__ctor_m2563420353 (DetectWaterByTextureExample_t2775676976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DetectWaterByTextureExample::Update()
extern "C"  void DetectWaterByTextureExample_Update_m3429252990 (DetectWaterByTextureExample_t2775676976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DetectWaterByTextureExample::HasWater(System.Single,System.Single)
extern "C"  bool DetectWaterByTextureExample_HasWater_m1765291188 (DetectWaterByTextureExample_t2775676976 * __this, float ___lat0, float ___lng1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DetectWaterByTextureExample::.cctor()
extern "C"  void DetectWaterByTextureExample__cctor_m3518501298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
