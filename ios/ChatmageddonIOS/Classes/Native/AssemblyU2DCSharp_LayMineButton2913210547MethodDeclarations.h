﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LayMineButton
struct LayMineButton_t2913210547;

#include "codegen/il2cpp-codegen.h"

// System.Void LayMineButton::.ctor()
extern "C"  void LayMineButton__ctor_m3842429080 (LayMineButton_t2913210547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayMineButton::OnButtonClick()
extern "C"  void LayMineButton_OnButtonClick_m4249506807 (LayMineButton_t2913210547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
