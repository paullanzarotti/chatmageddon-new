﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookBinding/<showDialog>c__AnonStorey0
struct  U3CshowDialogU3Ec__AnonStorey0_t827800849  : public Il2CppObject
{
public:
	// System.String Prime31.FacebookBinding/<showDialog>c__AnonStorey0::dialogType
	String_t* ___dialogType_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Prime31.FacebookBinding/<showDialog>c__AnonStorey0::options
	Dictionary_2_t3943999495 * ___options_1;

public:
	inline static int32_t get_offset_of_dialogType_0() { return static_cast<int32_t>(offsetof(U3CshowDialogU3Ec__AnonStorey0_t827800849, ___dialogType_0)); }
	inline String_t* get_dialogType_0() const { return ___dialogType_0; }
	inline String_t** get_address_of_dialogType_0() { return &___dialogType_0; }
	inline void set_dialogType_0(String_t* value)
	{
		___dialogType_0 = value;
		Il2CppCodeGenWriteBarrier(&___dialogType_0, value);
	}

	inline static int32_t get_offset_of_options_1() { return static_cast<int32_t>(offsetof(U3CshowDialogU3Ec__AnonStorey0_t827800849, ___options_1)); }
	inline Dictionary_2_t3943999495 * get_options_1() const { return ___options_1; }
	inline Dictionary_2_t3943999495 ** get_address_of_options_1() { return &___options_1; }
	inline void set_options_1(Dictionary_2_t3943999495 * value)
	{
		___options_1 = value;
		Il2CppCodeGenWriteBarrier(&___options_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
