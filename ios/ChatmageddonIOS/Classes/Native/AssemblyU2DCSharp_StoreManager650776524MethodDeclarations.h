﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoreManager
struct StoreManager_t650776524;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// QuantitativeItem
struct QuantitativeItem_t3036513780;
// PurchaseableItem
struct PurchaseableItem_t3351122996;
// Bucks
struct Bucks_t3932015720;
// Shield
struct Shield_t3327121081;
// Missile
struct Missile_t813944928;
// Mine
struct Mine_t2729441277;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"
#include "AssemblyU2DCSharp_QuantitativeItem3036513780.h"
#include "AssemblyU2DCSharp_PurchaseableItem3351122996.h"
#include "AssemblyU2DCSharp_Bucks3932015720.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"

// System.Void StoreManager::.ctor()
extern "C"  void StoreManager__ctor_m2653670215 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::OnEnable()
extern "C"  void StoreManager_OnEnable_m300314443 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::CheckStarts()
extern "C"  void StoreManager_CheckStarts_m2569833362 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StoreManager::WaitToCheckStarts()
extern "C"  Il2CppObject * StoreManager_WaitToCheckStarts_m3522884126 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::LoadCategory(PurchaseableCategory)
extern "C"  void StoreManager_LoadCategory_m3578767858 (StoreManager_t650776524 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::SetCatergoryLighting(PurchaseableCategory)
extern "C"  void StoreManager_SetCatergoryLighting_m1115895770 (StoreManager_t650776524 * __this, int32_t ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::SetLightActive(System.Boolean)
extern "C"  void StoreManager_SetLightActive_m3505829720 (StoreManager_t650776524 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::HideCategory(PurchaseableCategory)
extern "C"  void StoreManager_HideCategory_m1287746798 (StoreManager_t650776524 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::SetSwipeCollider(PurchaseableCategory)
extern "C"  void StoreManager_SetSwipeCollider_m1862098856 (StoreManager_t650776524 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StoreManager::ScaleAndHide(UnityEngine.GameObject[])
extern "C"  Il2CppObject * StoreManager_ScaleAndHide_m3118508378 (StoreManager_t650776524 * __this, GameObjectU5BU5D_t3057952154* ___gameobjects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StoreManager::ScaleAndShow(UnityEngine.GameObject[])
extern "C"  Il2CppObject * StoreManager_ScaleAndShow_m1853020123 (StoreManager_t650776524 * __this, GameObjectU5BU5D_t3057952154* ___gameobjects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::HideActiveSlider()
extern "C"  void StoreManager_HideActiveSlider_m4209072540 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::ShowSlider(QuantitativeItem)
extern "C"  void StoreManager_ShowSlider_m2470553489 (StoreManager_t650776524 * __this, QuantitativeItem_t3036513780 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StoreManager::LoadDefaultModel()
extern "C"  Il2CppObject * StoreManager_LoadDefaultModel_m3775623279 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::LoadDefaultItem(PurchaseableCategory)
extern "C"  void StoreManager_LoadDefaultItem_m274483644 (StoreManager_t650776524 * __this, int32_t ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::OnDefaultItemShow(System.Boolean)
extern "C"  void StoreManager_OnDefaultItemShow_m1869320258 (StoreManager_t650776524 * __this, bool ___closed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::LoadNextItem()
extern "C"  void StoreManager_LoadNextItem_m1078775143 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::LoadPreviousItem()
extern "C"  void StoreManager_LoadPreviousItem_m4151793945 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::TweenItem(System.Boolean)
extern "C"  void StoreManager_TweenItem_m3834419966 (StoreManager_t650776524 * __this, bool ___right0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::OnTweenItemComplete()
extern "C"  void StoreManager_OnTweenItemComplete_m2100440283 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::AlphaTweenLabels(System.Boolean)
extern "C"  void StoreManager_AlphaTweenLabels_m2464281922 (StoreManager_t650776524 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::LoadItem(PurchaseableItem)
extern "C"  void StoreManager_LoadItem_m2804893140 (StoreManager_t650776524 * __this, PurchaseableItem_t3351122996 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::SetAmountLabel(System.Boolean)
extern "C"  void StoreManager_SetAmountLabel_m2519134060 (StoreManager_t650776524 * __this, bool ___inifinity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::LoadCurrencyItem(Bucks)
extern "C"  void StoreManager_LoadCurrencyItem_m1042962091 (StoreManager_t650776524 * __this, Bucks_t3932015720 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::UpdatePriceLabel(PurchaseableItem)
extern "C"  void StoreManager_UpdatePriceLabel_m971950041 (StoreManager_t650776524 * __this, PurchaseableItem_t3351122996 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::SetItemLockedData()
extern "C"  void StoreManager_SetItemLockedData_m2119124682 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::ShowItemDescription(System.Boolean)
extern "C"  void StoreManager_ShowItemDescription_m2103446126 (StoreManager_t650776524 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::SetItemDescriptionNull()
extern "C"  void StoreManager_SetItemDescriptionNull_m887303665 (StoreManager_t650776524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::OpenCategoryUI(PurchaseableCategory)
extern "C"  void StoreManager_OpenCategoryUI_m727970836 (StoreManager_t650776524 * __this, int32_t ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::OpenShieldUI(Shield)
extern "C"  void StoreManager_OpenShieldUI_m1937768599 (StoreManager_t650776524 * __this, Shield_t3327121081 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::OpenMissileUI(Missile)
extern "C"  void StoreManager_OpenMissileUI_m3820991763 (StoreManager_t650776524 * __this, Missile_t813944928 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoreManager::OpenMineUI(Mine)
extern "C"  void StoreManager_OpenMineUI_m407394679 (StoreManager_t650776524 * __this, Mine_t2729441277 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
