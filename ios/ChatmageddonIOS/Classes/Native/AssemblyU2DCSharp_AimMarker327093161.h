﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.Transform
struct Transform_t3275118058;
// TweenRotation
struct TweenRotation_t1747194511;
// CountdownTimer
struct CountdownTimer_t4123051008;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AimMarker
struct  AimMarker_t327093161  : public MonoBehaviour_t1158329972
{
public:
	// UITexture AimMarker::backfireRing
	UITexture_t2537039969 * ___backfireRing_2;
	// UISprite AimMarker::perfectLaunchRing
	UISprite_t603616735 * ___perfectLaunchRing_3;
	// UILabel AimMarker::pointsAvailableLabel
	UILabel_t1795115428 * ___pointsAvailableLabel_4;
	// UILabel AimMarker::latLabel
	UILabel_t1795115428 * ___latLabel_5;
	// UILabel AimMarker::longLabel
	UILabel_t1795115428 * ___longLabel_6;
	// UnityEngine.Transform AimMarker::targetAimer
	Transform_t3275118058 * ___targetAimer_7;
	// TweenRotation AimMarker::parentRotator
	TweenRotation_t1747194511 * ___parentRotator_8;
	// System.Single AimMarker::targetRadius
	float ___targetRadius_9;
	// System.Boolean AimMarker::playAnim
	bool ___playAnim_10;
	// System.Single AimMarker::m_Speed
	float ___m_Speed_11;
	// System.Single AimMarker::m_XScale
	float ___m_XScale_12;
	// System.Single AimMarker::m_YScale
	float ___m_YScale_13;
	// UnityEngine.Vector3 AimMarker::m_Pivot
	Vector3_t2243707580  ___m_Pivot_14;
	// UnityEngine.Vector3 AimMarker::m_PivotOffset
	Vector3_t2243707580  ___m_PivotOffset_15;
	// System.Single AimMarker::m_Phase
	float ___m_Phase_16;
	// System.Boolean AimMarker::m_Invert
	bool ___m_Invert_17;
	// System.Boolean AimMarker::previousInvert
	bool ___previousInvert_18;
	// System.Single AimMarker::m_2PI
	float ___m_2PI_19;
	// System.Double AimMarker::outsideRadius
	double ___outsideRadius_20;
	// System.Double AimMarker::insideRadius
	double ___insideRadius_21;
	// System.Double AimMarker::insideOffset
	double ___insideOffset_22;
	// System.Double AimMarker::pointsPerDraw
	double ___pointsPerDraw_23;
	// System.Boolean AimMarker::loopSpirograph
	bool ___loopSpirograph_24;
	// CountdownTimer AimMarker::smoothTimer
	CountdownTimer_t4123051008 * ___smoothTimer_25;
	// UnityEngine.Coroutine AimMarker::spirographRoutine
	Coroutine_t2299508840 * ___spirographRoutine_26;
	// System.Double AimMarker::difference
	double ___difference_27;
	// System.Single AimMarker::previousPercent
	float ___previousPercent_28;

public:
	inline static int32_t get_offset_of_backfireRing_2() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___backfireRing_2)); }
	inline UITexture_t2537039969 * get_backfireRing_2() const { return ___backfireRing_2; }
	inline UITexture_t2537039969 ** get_address_of_backfireRing_2() { return &___backfireRing_2; }
	inline void set_backfireRing_2(UITexture_t2537039969 * value)
	{
		___backfireRing_2 = value;
		Il2CppCodeGenWriteBarrier(&___backfireRing_2, value);
	}

	inline static int32_t get_offset_of_perfectLaunchRing_3() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___perfectLaunchRing_3)); }
	inline UISprite_t603616735 * get_perfectLaunchRing_3() const { return ___perfectLaunchRing_3; }
	inline UISprite_t603616735 ** get_address_of_perfectLaunchRing_3() { return &___perfectLaunchRing_3; }
	inline void set_perfectLaunchRing_3(UISprite_t603616735 * value)
	{
		___perfectLaunchRing_3 = value;
		Il2CppCodeGenWriteBarrier(&___perfectLaunchRing_3, value);
	}

	inline static int32_t get_offset_of_pointsAvailableLabel_4() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___pointsAvailableLabel_4)); }
	inline UILabel_t1795115428 * get_pointsAvailableLabel_4() const { return ___pointsAvailableLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_pointsAvailableLabel_4() { return &___pointsAvailableLabel_4; }
	inline void set_pointsAvailableLabel_4(UILabel_t1795115428 * value)
	{
		___pointsAvailableLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___pointsAvailableLabel_4, value);
	}

	inline static int32_t get_offset_of_latLabel_5() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___latLabel_5)); }
	inline UILabel_t1795115428 * get_latLabel_5() const { return ___latLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_latLabel_5() { return &___latLabel_5; }
	inline void set_latLabel_5(UILabel_t1795115428 * value)
	{
		___latLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___latLabel_5, value);
	}

	inline static int32_t get_offset_of_longLabel_6() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___longLabel_6)); }
	inline UILabel_t1795115428 * get_longLabel_6() const { return ___longLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_longLabel_6() { return &___longLabel_6; }
	inline void set_longLabel_6(UILabel_t1795115428 * value)
	{
		___longLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___longLabel_6, value);
	}

	inline static int32_t get_offset_of_targetAimer_7() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___targetAimer_7)); }
	inline Transform_t3275118058 * get_targetAimer_7() const { return ___targetAimer_7; }
	inline Transform_t3275118058 ** get_address_of_targetAimer_7() { return &___targetAimer_7; }
	inline void set_targetAimer_7(Transform_t3275118058 * value)
	{
		___targetAimer_7 = value;
		Il2CppCodeGenWriteBarrier(&___targetAimer_7, value);
	}

	inline static int32_t get_offset_of_parentRotator_8() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___parentRotator_8)); }
	inline TweenRotation_t1747194511 * get_parentRotator_8() const { return ___parentRotator_8; }
	inline TweenRotation_t1747194511 ** get_address_of_parentRotator_8() { return &___parentRotator_8; }
	inline void set_parentRotator_8(TweenRotation_t1747194511 * value)
	{
		___parentRotator_8 = value;
		Il2CppCodeGenWriteBarrier(&___parentRotator_8, value);
	}

	inline static int32_t get_offset_of_targetRadius_9() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___targetRadius_9)); }
	inline float get_targetRadius_9() const { return ___targetRadius_9; }
	inline float* get_address_of_targetRadius_9() { return &___targetRadius_9; }
	inline void set_targetRadius_9(float value)
	{
		___targetRadius_9 = value;
	}

	inline static int32_t get_offset_of_playAnim_10() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___playAnim_10)); }
	inline bool get_playAnim_10() const { return ___playAnim_10; }
	inline bool* get_address_of_playAnim_10() { return &___playAnim_10; }
	inline void set_playAnim_10(bool value)
	{
		___playAnim_10 = value;
	}

	inline static int32_t get_offset_of_m_Speed_11() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_Speed_11)); }
	inline float get_m_Speed_11() const { return ___m_Speed_11; }
	inline float* get_address_of_m_Speed_11() { return &___m_Speed_11; }
	inline void set_m_Speed_11(float value)
	{
		___m_Speed_11 = value;
	}

	inline static int32_t get_offset_of_m_XScale_12() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_XScale_12)); }
	inline float get_m_XScale_12() const { return ___m_XScale_12; }
	inline float* get_address_of_m_XScale_12() { return &___m_XScale_12; }
	inline void set_m_XScale_12(float value)
	{
		___m_XScale_12 = value;
	}

	inline static int32_t get_offset_of_m_YScale_13() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_YScale_13)); }
	inline float get_m_YScale_13() const { return ___m_YScale_13; }
	inline float* get_address_of_m_YScale_13() { return &___m_YScale_13; }
	inline void set_m_YScale_13(float value)
	{
		___m_YScale_13 = value;
	}

	inline static int32_t get_offset_of_m_Pivot_14() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_Pivot_14)); }
	inline Vector3_t2243707580  get_m_Pivot_14() const { return ___m_Pivot_14; }
	inline Vector3_t2243707580 * get_address_of_m_Pivot_14() { return &___m_Pivot_14; }
	inline void set_m_Pivot_14(Vector3_t2243707580  value)
	{
		___m_Pivot_14 = value;
	}

	inline static int32_t get_offset_of_m_PivotOffset_15() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_PivotOffset_15)); }
	inline Vector3_t2243707580  get_m_PivotOffset_15() const { return ___m_PivotOffset_15; }
	inline Vector3_t2243707580 * get_address_of_m_PivotOffset_15() { return &___m_PivotOffset_15; }
	inline void set_m_PivotOffset_15(Vector3_t2243707580  value)
	{
		___m_PivotOffset_15 = value;
	}

	inline static int32_t get_offset_of_m_Phase_16() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_Phase_16)); }
	inline float get_m_Phase_16() const { return ___m_Phase_16; }
	inline float* get_address_of_m_Phase_16() { return &___m_Phase_16; }
	inline void set_m_Phase_16(float value)
	{
		___m_Phase_16 = value;
	}

	inline static int32_t get_offset_of_m_Invert_17() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_Invert_17)); }
	inline bool get_m_Invert_17() const { return ___m_Invert_17; }
	inline bool* get_address_of_m_Invert_17() { return &___m_Invert_17; }
	inline void set_m_Invert_17(bool value)
	{
		___m_Invert_17 = value;
	}

	inline static int32_t get_offset_of_previousInvert_18() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___previousInvert_18)); }
	inline bool get_previousInvert_18() const { return ___previousInvert_18; }
	inline bool* get_address_of_previousInvert_18() { return &___previousInvert_18; }
	inline void set_previousInvert_18(bool value)
	{
		___previousInvert_18 = value;
	}

	inline static int32_t get_offset_of_m_2PI_19() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___m_2PI_19)); }
	inline float get_m_2PI_19() const { return ___m_2PI_19; }
	inline float* get_address_of_m_2PI_19() { return &___m_2PI_19; }
	inline void set_m_2PI_19(float value)
	{
		___m_2PI_19 = value;
	}

	inline static int32_t get_offset_of_outsideRadius_20() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___outsideRadius_20)); }
	inline double get_outsideRadius_20() const { return ___outsideRadius_20; }
	inline double* get_address_of_outsideRadius_20() { return &___outsideRadius_20; }
	inline void set_outsideRadius_20(double value)
	{
		___outsideRadius_20 = value;
	}

	inline static int32_t get_offset_of_insideRadius_21() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___insideRadius_21)); }
	inline double get_insideRadius_21() const { return ___insideRadius_21; }
	inline double* get_address_of_insideRadius_21() { return &___insideRadius_21; }
	inline void set_insideRadius_21(double value)
	{
		___insideRadius_21 = value;
	}

	inline static int32_t get_offset_of_insideOffset_22() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___insideOffset_22)); }
	inline double get_insideOffset_22() const { return ___insideOffset_22; }
	inline double* get_address_of_insideOffset_22() { return &___insideOffset_22; }
	inline void set_insideOffset_22(double value)
	{
		___insideOffset_22 = value;
	}

	inline static int32_t get_offset_of_pointsPerDraw_23() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___pointsPerDraw_23)); }
	inline double get_pointsPerDraw_23() const { return ___pointsPerDraw_23; }
	inline double* get_address_of_pointsPerDraw_23() { return &___pointsPerDraw_23; }
	inline void set_pointsPerDraw_23(double value)
	{
		___pointsPerDraw_23 = value;
	}

	inline static int32_t get_offset_of_loopSpirograph_24() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___loopSpirograph_24)); }
	inline bool get_loopSpirograph_24() const { return ___loopSpirograph_24; }
	inline bool* get_address_of_loopSpirograph_24() { return &___loopSpirograph_24; }
	inline void set_loopSpirograph_24(bool value)
	{
		___loopSpirograph_24 = value;
	}

	inline static int32_t get_offset_of_smoothTimer_25() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___smoothTimer_25)); }
	inline CountdownTimer_t4123051008 * get_smoothTimer_25() const { return ___smoothTimer_25; }
	inline CountdownTimer_t4123051008 ** get_address_of_smoothTimer_25() { return &___smoothTimer_25; }
	inline void set_smoothTimer_25(CountdownTimer_t4123051008 * value)
	{
		___smoothTimer_25 = value;
		Il2CppCodeGenWriteBarrier(&___smoothTimer_25, value);
	}

	inline static int32_t get_offset_of_spirographRoutine_26() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___spirographRoutine_26)); }
	inline Coroutine_t2299508840 * get_spirographRoutine_26() const { return ___spirographRoutine_26; }
	inline Coroutine_t2299508840 ** get_address_of_spirographRoutine_26() { return &___spirographRoutine_26; }
	inline void set_spirographRoutine_26(Coroutine_t2299508840 * value)
	{
		___spirographRoutine_26 = value;
		Il2CppCodeGenWriteBarrier(&___spirographRoutine_26, value);
	}

	inline static int32_t get_offset_of_difference_27() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___difference_27)); }
	inline double get_difference_27() const { return ___difference_27; }
	inline double* get_address_of_difference_27() { return &___difference_27; }
	inline void set_difference_27(double value)
	{
		___difference_27 = value;
	}

	inline static int32_t get_offset_of_previousPercent_28() { return static_cast<int32_t>(offsetof(AimMarker_t327093161, ___previousPercent_28)); }
	inline float get_previousPercent_28() const { return ___previousPercent_28; }
	inline float* get_address_of_previousPercent_28() { return &___previousPercent_28; }
	inline void set_previousPercent_28(float value)
	{
		___previousPercent_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
