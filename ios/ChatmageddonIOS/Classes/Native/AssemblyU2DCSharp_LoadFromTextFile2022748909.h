﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadFromTextFile
struct  LoadFromTextFile_t2022748909  : public Il2CppObject
{
public:
	// System.String[] LoadFromTextFile::entries
	StringU5BU5D_t1642385972* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(LoadFromTextFile_t2022748909, ___entries_0)); }
	inline StringU5BU5D_t1642385972* get_entries_0() const { return ___entries_0; }
	inline StringU5BU5D_t1642385972** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(StringU5BU5D_t1642385972* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier(&___entries_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
