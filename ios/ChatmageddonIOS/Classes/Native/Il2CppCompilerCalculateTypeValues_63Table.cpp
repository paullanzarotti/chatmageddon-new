﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberOfflineG2973898703.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberFormat2833677748.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberType324241265.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil4155573397.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Mat2223543287.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Val3210374836.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Len3057047663.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_U3C3641809394.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneRegex3216508019.h"
#include "AssemblyU2DCSharp_PhoneNumbers_Phonemetadata365779067.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat441739224.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat_Builde3378404562.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc922391174.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberDesc_Buil474280692.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata366861403.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata_Build1569388123.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadataCollec4114095021.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadataCollec1251570781.h"
#include "AssemblyU2DCSharp_PhoneNumbers_Phonenumber814039129.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Types2983327239.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Types_C2831594294.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Builder2361401461.h"
#include "AssemblyU2DCSharp_PhoneNumbers_RegexCache1271678307.h"
#include "AssemblyU2DCSharp_PhoneNumbers_RegexCache_Entry1519365992.h"
#include "AssemblyU2DCSharp_PhoneNumbers_RegionCode982551659.h"
#include "AssemblyU2DCSharp_PhoneNumbers_ShortNumberUtil1108790299.h"
#include "AssemblyU2DCSharp_libphonenumber_csharp_portable_X3619622016.h"
#include "AssemblyU2DCSharp_libphonenumber_csharp_portable_X3380385869.h"
#include "AssemblyU2DCSharp_libphonenumber_csharp_portable_X3380385870.h"
#include "AssemblyU2DCSharp_DemoCtrl4011351802.h"
#include "AssemblyU2DCSharp_DemoCtrl_U3CScreenshotU3Ec__Iter3558784402.h"
#include "AssemblyU2DCSharp_DemoCtrl_U3CDownloadJsonFileU3Ec2410277463.h"
#include "AssemblyU2DCSharp_DemoCtrl_U3CDownloadFileU3Ec__It1146962742.h"
#include "AssemblyU2DCSharp_SimpleJsonExtensions_SimpleJsonEx210116988.h"
#include "AssemblyU2DCSharp_SimpleJsonImporter1689921094.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2075964317.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3113433889.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864769.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864773.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236543.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206768.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3740780830.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3740780702.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3066379751.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2882533397.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1459944466.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637719.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1500295684.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3698097168.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379926265.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637715.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206766.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236537.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236541.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864765.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1459944470.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068660.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437128.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637717.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2217126741.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437126.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206772.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236547.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068658.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3740780834.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3113433695.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AmbientObscuran4277448813.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AAMode3182714109.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_AntialiasingAsP2974095527.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom1048415213.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_LensFlare2485171085.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_TweakMode2162868461.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_HDRBloomM1124970370.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_BloomScre4287047507.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Bloom_BloomQual3407301380.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_LensflareStyle33344173254.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_TweakMode341181002552.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_HDRBloomMode356052982.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BloomScreenBlen3677155143.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_BloomAndLensFla4077233549.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Blur3683821091.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Blur_BlurType2760015345.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_CameraMotionBlur32230768.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_CameraMotionBlu3030354800.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection1571628420.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection1552509009.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ColorCorrection2033555384.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_ContrastEnhance3766296508.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Crease1747016313.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_Dof34QualitySet2579674687.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6300 = { sizeof (U3CLoadMappingFileProviderU3Ec__AnonStorey0_t2973898703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6300[1] = 
{
	U3CLoadMappingFileProviderU3Ec__AnonStorey0_t2973898703::get_offset_of_prefix_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6301 = { sizeof (PhoneNumberFormat_t2833677748)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6301[5] = 
{
	PhoneNumberFormat_t2833677748::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6302 = { sizeof (PhoneNumberType_t324241265)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6302[13] = 
{
	PhoneNumberType_t324241265::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6303 = { sizeof (PhoneNumberUtil_t4155573397), -1, sizeof(PhoneNumberUtil_t4155573397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6303[56] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PhoneNumberUtil_t4155573397::get_offset_of_currentFilePrefix__7(),
	PhoneNumberUtil_t4155573397::get_offset_of_countryCallingCodeToRegionCodeMap__8(),
	PhoneNumberUtil_t4155573397::get_offset_of_supportedRegions__9(),
	PhoneNumberUtil_t4155573397::get_offset_of_nanpaRegions__10(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_DIALLABLE_CHAR_MAPPINGS_19(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_ALPHA_MAPPINGS_20(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_ALPHA_PHONE_MAPPINGS_21(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_ALL_PLUS_NUMBER_GROUPING_SYMBOLS_22(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_thisLock_23(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_UNIQUE_INTERNATIONAL_PREFIX_24(),
	0,
	0,
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_VALID_ALPHA_27(),
	0,
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_PLUS_CHARS_PATTERN_29(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_SEPARATOR_PATTERN_30(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_CAPTURING_DIGIT_PATTERN_31(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_VALID_START_CHAR_32(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_VALID_START_CHAR_PATTERN_33(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_SECOND_NUMBER_START_34(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_SECOND_NUMBER_START_PATTERN_35(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_VALID_ALPHA_PHONE_PATTERN_36(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_VALID_PHONE_NUMBER_37(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_DEFAULT_EXTN_PREFIX_38(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_CAPTURING_EXTN_DIGITS_39(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_EXTN_PATTERNS_FOR_PARSING_40(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_EXTN_PATTERNS_FOR_MATCHING_41(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_EXTN_PATTERN_42(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_VALID_PHONE_NUMBER_PATTERN_43(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_NON_DIGITS_PATTERN_44(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_FIRST_GROUP_PATTERN_45(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_NP_PATTERN_46(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_FG_PATTERN_47(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_CC_PATTERN_48(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_instance__49(),
	PhoneNumberUtil_t4155573397::get_offset_of_regionToMetadataMap_50(),
	PhoneNumberUtil_t4155573397::get_offset_of_countryCodeToNonGeographicalMetadataMap_51(),
	PhoneNumberUtil_t4155573397::get_offset_of_regexCache_52(),
	0,
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_54(),
	PhoneNumberUtil_t4155573397_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6304 = { sizeof (MatchType_t2223543287)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6304[6] = 
{
	MatchType_t2223543287::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6305 = { sizeof (ValidationResult_t3210374836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6305[5] = 
{
	ValidationResult_t3210374836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6306 = { sizeof (Leniency_t3057047663)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6306[5] = 
{
	Leniency_t3057047663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6307 = { sizeof (U3CFindNumbersU3Ec__AnonStorey0_t3641809394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6307[5] = 
{
	U3CFindNumbersU3Ec__AnonStorey0_t3641809394::get_offset_of_text_0(),
	U3CFindNumbersU3Ec__AnonStorey0_t3641809394::get_offset_of_defaultRegion_1(),
	U3CFindNumbersU3Ec__AnonStorey0_t3641809394::get_offset_of_leniency_2(),
	U3CFindNumbersU3Ec__AnonStorey0_t3641809394::get_offset_of_maxTries_3(),
	U3CFindNumbersU3Ec__AnonStorey0_t3641809394::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6308 = { sizeof (PhoneRegex_t3216508019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6308[2] = 
{
	PhoneRegex_t3216508019::get_offset_of_allRegex__16(),
	PhoneRegex_t3216508019::get_offset_of_beginRegex__17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6309 = { sizeof (Phonemetadata_t365779067), -1, sizeof(Phonemetadata_t365779067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6309[1] = 
{
	Phonemetadata_t365779067_StaticFields::get_offset_of_Descriptor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6310 = { sizeof (NumberFormat_t441739224), -1, sizeof(NumberFormat_t441739224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6310[18] = 
{
	NumberFormat_t441739224_StaticFields::get_offset_of_defaultInstance_0(),
	0,
	NumberFormat_t441739224::get_offset_of_hasPattern_2(),
	NumberFormat_t441739224::get_offset_of_pattern__3(),
	0,
	NumberFormat_t441739224::get_offset_of_hasFormat_5(),
	NumberFormat_t441739224::get_offset_of_format__6(),
	0,
	NumberFormat_t441739224::get_offset_of_leadingDigitsPattern__8(),
	0,
	NumberFormat_t441739224::get_offset_of_hasNationalPrefixFormattingRule_10(),
	NumberFormat_t441739224::get_offset_of_nationalPrefixFormattingRule__11(),
	0,
	NumberFormat_t441739224::get_offset_of_hasNationalPrefixOptionalWhenFormatting_13(),
	NumberFormat_t441739224::get_offset_of_nationalPrefixOptionalWhenFormatting__14(),
	0,
	NumberFormat_t441739224::get_offset_of_hasDomesticCarrierCodeFormattingRule_16(),
	NumberFormat_t441739224::get_offset_of_domesticCarrierCodeFormattingRule__17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6311 = { sizeof (Builder_t3378404562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6311[1] = 
{
	Builder_t3378404562::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6312 = { sizeof (PhoneNumberDesc_t922391174), -1, sizeof(PhoneNumberDesc_t922391174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6312[10] = 
{
	PhoneNumberDesc_t922391174_StaticFields::get_offset_of_defaultInstance_0(),
	0,
	PhoneNumberDesc_t922391174::get_offset_of_hasNationalNumberPattern_2(),
	PhoneNumberDesc_t922391174::get_offset_of_nationalNumberPattern__3(),
	0,
	PhoneNumberDesc_t922391174::get_offset_of_hasPossibleNumberPattern_5(),
	PhoneNumberDesc_t922391174::get_offset_of_possibleNumberPattern__6(),
	0,
	PhoneNumberDesc_t922391174::get_offset_of_hasExampleNumber_8(),
	PhoneNumberDesc_t922391174::get_offset_of_exampleNumber__9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6313 = { sizeof (Builder_t474280692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6313[1] = 
{
	Builder_t474280692::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6314 = { sizeof (PhoneMetadata_t366861403), -1, sizeof(PhoneMetadata_t366861403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6314[80] = 
{
	PhoneMetadata_t366861403_StaticFields::get_offset_of_defaultInstance_0(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasGeneralDesc_2(),
	PhoneMetadata_t366861403::get_offset_of_generalDesc__3(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasFixedLine_5(),
	PhoneMetadata_t366861403::get_offset_of_fixedLine__6(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasMobile_8(),
	PhoneMetadata_t366861403::get_offset_of_mobile__9(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasTollFree_11(),
	PhoneMetadata_t366861403::get_offset_of_tollFree__12(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasPremiumRate_14(),
	PhoneMetadata_t366861403::get_offset_of_premiumRate__15(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasSharedCost_17(),
	PhoneMetadata_t366861403::get_offset_of_sharedCost__18(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasPersonalNumber_20(),
	PhoneMetadata_t366861403::get_offset_of_personalNumber__21(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasVoip_23(),
	PhoneMetadata_t366861403::get_offset_of_voip__24(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasPager_26(),
	PhoneMetadata_t366861403::get_offset_of_pager__27(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasUan_29(),
	PhoneMetadata_t366861403::get_offset_of_uan__30(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasEmergency_32(),
	PhoneMetadata_t366861403::get_offset_of_emergency__33(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasVoicemail_35(),
	PhoneMetadata_t366861403::get_offset_of_voicemail__36(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasNoInternationalDialling_38(),
	PhoneMetadata_t366861403::get_offset_of_noInternationalDialling__39(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasId_41(),
	PhoneMetadata_t366861403::get_offset_of_id__42(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasCountryCode_44(),
	PhoneMetadata_t366861403::get_offset_of_countryCode__45(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasInternationalPrefix_47(),
	PhoneMetadata_t366861403::get_offset_of_internationalPrefix__48(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasPreferredInternationalPrefix_50(),
	PhoneMetadata_t366861403::get_offset_of_preferredInternationalPrefix__51(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasNationalPrefix_53(),
	PhoneMetadata_t366861403::get_offset_of_nationalPrefix__54(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasPreferredExtnPrefix_56(),
	PhoneMetadata_t366861403::get_offset_of_preferredExtnPrefix__57(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasNationalPrefixForParsing_59(),
	PhoneMetadata_t366861403::get_offset_of_nationalPrefixForParsing__60(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasNationalPrefixTransformRule_62(),
	PhoneMetadata_t366861403::get_offset_of_nationalPrefixTransformRule__63(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasSameMobileAndFixedLinePattern_65(),
	PhoneMetadata_t366861403::get_offset_of_sameMobileAndFixedLinePattern__66(),
	0,
	PhoneMetadata_t366861403::get_offset_of_numberFormat__68(),
	0,
	PhoneMetadata_t366861403::get_offset_of_intlNumberFormat__70(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasMainCountryForCode_72(),
	PhoneMetadata_t366861403::get_offset_of_mainCountryForCode__73(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasLeadingDigits_75(),
	PhoneMetadata_t366861403::get_offset_of_leadingDigits__76(),
	0,
	PhoneMetadata_t366861403::get_offset_of_hasLeadingZeroPossible_78(),
	PhoneMetadata_t366861403::get_offset_of_leadingZeroPossible__79(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6315 = { sizeof (Builder_t1569388123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6315[1] = 
{
	Builder_t1569388123::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6316 = { sizeof (PhoneMetadataCollection_t4114095021), -1, sizeof(PhoneMetadataCollection_t4114095021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6316[3] = 
{
	PhoneMetadataCollection_t4114095021_StaticFields::get_offset_of_defaultInstance_0(),
	0,
	PhoneMetadataCollection_t4114095021::get_offset_of_metadata__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6317 = { sizeof (Builder_t1251570781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6317[1] = 
{
	Builder_t1251570781::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6318 = { sizeof (Phonenumber_t814039129), -1, sizeof(Phonenumber_t814039129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6318[1] = 
{
	Phonenumber_t814039129_StaticFields::get_offset_of_Descriptor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6319 = { sizeof (PhoneNumber_t814071929), -1, sizeof(PhoneNumber_t814071929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6319[22] = 
{
	PhoneNumber_t814071929_StaticFields::get_offset_of_defaultInstance_0(),
	0,
	PhoneNumber_t814071929::get_offset_of_hasCountryCode_2(),
	PhoneNumber_t814071929::get_offset_of_countryCode__3(),
	0,
	PhoneNumber_t814071929::get_offset_of_hasNationalNumber_5(),
	PhoneNumber_t814071929::get_offset_of_nationalNumber__6(),
	0,
	PhoneNumber_t814071929::get_offset_of_hasExtension_8(),
	PhoneNumber_t814071929::get_offset_of_extension__9(),
	0,
	PhoneNumber_t814071929::get_offset_of_hasItalianLeadingZero_11(),
	PhoneNumber_t814071929::get_offset_of_italianLeadingZero__12(),
	0,
	PhoneNumber_t814071929::get_offset_of_hasRawInput_14(),
	PhoneNumber_t814071929::get_offset_of_rawInput__15(),
	0,
	PhoneNumber_t814071929::get_offset_of_hasCountryCodeSource_17(),
	PhoneNumber_t814071929::get_offset_of_countryCodeSource__18(),
	0,
	PhoneNumber_t814071929::get_offset_of_hasPreferredDomesticCarrierCode_20(),
	PhoneNumber_t814071929::get_offset_of_preferredDomesticCarrierCode__21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6320 = { sizeof (Types_t2983327239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6321 = { sizeof (CountryCodeSource_t2831594294)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6321[5] = 
{
	CountryCodeSource_t2831594294::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6322 = { sizeof (Builder_t2361401461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6322[1] = 
{
	Builder_t2361401461::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6323 = { sizeof (RegexCache_t1271678307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6323[3] = 
{
	RegexCache_t1271678307::get_offset_of_size__0(),
	RegexCache_t1271678307::get_offset_of_lru__1(),
	RegexCache_t1271678307::get_offset_of_cache__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6324 = { sizeof (Entry_t1519365992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6324[2] = 
{
	Entry_t1519365992::get_offset_of_Regex_0(),
	Entry_t1519365992::get_offset_of_Node_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6325 = { sizeof (RegionCode_t982551659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6325[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6326 = { sizeof (ShortNumberUtil_t1108790299), -1, sizeof(ShortNumberUtil_t1108790299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6326[1] = 
{
	ShortNumberUtil_t1108790299_StaticFields::get_offset_of_phoneUtil_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6327 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6327[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6328 = { sizeof (XDocumentLegacy_t3619622016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6329 = { sizeof (U3CGetElementsByTagNameU3Ec__AnonStorey0_t3380385869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6329[1] = 
{
	U3CGetElementsByTagNameU3Ec__AnonStorey0_t3380385869::get_offset_of_tagName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6330 = { sizeof (U3CGetElementsByTagNameU3Ec__AnonStorey1_t3380385870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6330[1] = 
{
	U3CGetElementsByTagNameU3Ec__AnonStorey1_t3380385870::get_offset_of_tagName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6331 = { sizeof (DemoCtrl_t4011351802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6331[18] = 
{
	DemoCtrl_t4011351802::get_offset_of_pathSample1_2(),
	DemoCtrl_t4011351802::get_offset_of_pathSample2_3(),
	DemoCtrl_t4011351802::get_offset_of_pathSample3_4(),
	DemoCtrl_t4011351802::get_offset_of_downloadedPath_5(),
	DemoCtrl_t4011351802::get_offset_of_url_6(),
	DemoCtrl_t4011351802::get_offset_of_path_7(),
	DemoCtrl_t4011351802::get_offset_of_findKey_8(),
	DemoCtrl_t4011351802::get_offset_of_findValue_9(),
	DemoCtrl_t4011351802::get_offset_of_scrollPosition_10(),
	DemoCtrl_t4011351802::get_offset_of_scrollPositionLog_11(),
	DemoCtrl_t4011351802::get_offset_of_logMsgs_12(),
	DemoCtrl_t4011351802::get_offset_of_jsonString_13(),
	DemoCtrl_t4011351802::get_offset_of_jsonLogString_14(),
	DemoCtrl_t4011351802::get_offset_of_jsonNode_15(),
	DemoCtrl_t4011351802::get_offset_of_selectedNode_16(),
	DemoCtrl_t4011351802::get_offset_of_bg_17(),
	DemoCtrl_t4011351802::get_offset_of_caseInsensitive_18(),
	DemoCtrl_t4011351802::get_offset_of_screenshotCounter_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6332 = { sizeof (U3CScreenshotU3Ec__Iterator0_t3558784402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6332[5] = 
{
	U3CScreenshotU3Ec__Iterator0_t3558784402::get_offset_of_U3CscreenshotU3E__0_0(),
	U3CScreenshotU3Ec__Iterator0_t3558784402::get_offset_of_U24this_1(),
	U3CScreenshotU3Ec__Iterator0_t3558784402::get_offset_of_U24current_2(),
	U3CScreenshotU3Ec__Iterator0_t3558784402::get_offset_of_U24disposing_3(),
	U3CScreenshotU3Ec__Iterator0_t3558784402::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6333 = { sizeof (U3CDownloadJsonFileU3Ec__Iterator1_t2410277463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6333[5] = 
{
	U3CDownloadJsonFileU3Ec__Iterator1_t2410277463::get_offset_of_url_0(),
	U3CDownloadJsonFileU3Ec__Iterator1_t2410277463::get_offset_of_U24this_1(),
	U3CDownloadJsonFileU3Ec__Iterator1_t2410277463::get_offset_of_U24current_2(),
	U3CDownloadJsonFileU3Ec__Iterator1_t2410277463::get_offset_of_U24disposing_3(),
	U3CDownloadJsonFileU3Ec__Iterator1_t2410277463::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6334 = { sizeof (U3CDownloadFileU3Ec__Iterator2_t1146962742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6334[7] = 
{
	U3CDownloadFileU3Ec__Iterator2_t1146962742::get_offset_of_url_0(),
	U3CDownloadFileU3Ec__Iterator2_t1146962742::get_offset_of_U3CwwwU3E__0_1(),
	U3CDownloadFileU3Ec__Iterator2_t1146962742::get_offset_of_result_2(),
	U3CDownloadFileU3Ec__Iterator2_t1146962742::get_offset_of_U24this_3(),
	U3CDownloadFileU3Ec__Iterator2_t1146962742::get_offset_of_U24current_4(),
	U3CDownloadFileU3Ec__Iterator2_t1146962742::get_offset_of_U24disposing_5(),
	U3CDownloadFileU3Ec__Iterator2_t1146962742::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6335 = { sizeof (SimpleJsonExtensions_t210116988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6336 = { sizeof (SimpleJsonImporter_t1689921094), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6337 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305145), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6337[275] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_0(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_1(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_2(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_3(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_4(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_5(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_6(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_7(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_8(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_9(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_10(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_11(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_12(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_13(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_14(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_15(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_16(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5EF06A1239E95C60F8D2C1DD8AE8028FC3742401_17(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_18(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_19(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_20(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_21(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_22(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_23(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_24(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_25(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_26(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_27(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D27C3AB82CCA6CE2F199F4F670BF19513A3825B87_28(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D30F65A149AF7DE938A9287048498B966AEBE54D4_29(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_30(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_31(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_32(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_33(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_34(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D102C522344FCAC1545BDA50C0FC675C502FFEC53_35(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D29EC5E8FCA12DB9EC0B5E6D2DE67B624D2E2372C_36(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D95CA85749ADCFBF9A2B82C0381DBCF95D175524C_37(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD9C221237B647EC215A7BCDED447349810E6BF9C_38(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE43F6BA634642FB90FEEE1A8F9905E957741960C_39(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D04D2A79C8A779AFAA779125335E9334C245EBB46_40(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D643A9D76937E94519B73BE072D65E79BAFF3C213_41(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE117A40BF9A3AE32474AD7B22EB4C60E95D3BE2A_42(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6F576E8737EAAC9B6B12BDFC370048CD205E2CDD_43(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE2F8F75B7ABE5A6E4ECF5E32B935909BE2CC9F4B_44(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1C1237F52E2ED7B4D229AE3978DA144B9E653F5E_45(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_46(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_47(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_48(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_49(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_50(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D37454D933508E238CFB980F1077B24ADA4A480F4_51(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D555BD8216452A1A1E3CB7AD8F4DAE8F41FB30380_52(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DEF813A47B13574822D335279EF445343654A4F04_53(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA47DBBBB6655DAA5E024374BB9C8AA44FF40D444_54(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D84F6B4137736E2F6671FC8787A500AC5C6E1D6AC_55(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D17651A9FA4DEA6C24D1287324CF4A640D080FE8E_56(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D7FF0A15672FF2807983AB77C0DA74928986427C0_57(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB539B4DC5A69E3FE6AB10F668DDCA96E9D51235B_58(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6DFD60679846B0E2064404E9EA3E1DDCE2C5709D_59(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_60(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_61(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_62(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_63(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_64(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_65(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_66(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_67(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_68(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_69(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_70(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_71(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_72(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_73(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_74(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_75(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_76(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_77(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_78(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_79(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAF3E960D2F0572119C78CAF04B900985A299000E_80(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0392525BCB01691D1F319D89F2C12BF93A478467_81(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5DBC1A420B61F594A834C83E3DDC229C8AB77FDC_82(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8AC10B57091AFE182D9E4375C05E3FA037FFB3FA_83(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DED1175D10394A9CC0BC1E46183E287124AE4EB44_84(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8D3FE5CBAB9E7C2A376892E4DD9EF43B5F8A712A_85(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D18F46E3C2ECF4BAAE65361632BA7C1B4B9028827_86(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD5B4B2417720C9E5EDBF83CB4440D9C8B76B5557_87(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D47A4F979AF156CDA62313B97411B4C6CE62B8B5A_88(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3E8E493888B1BFB763D7553A6CC1978C17C198A3_89(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3045D5B29BA900304981918A153806D371B02549_90(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF3E701C38098B41D23FA88977423371B3C00C0D1_91(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6D19ABD7FD0986E382242A9B88C8D8B5F52598DF_92(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_93(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_94(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_95(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_96(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_97(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D21A3E0CD2847E2F12EEE37749A9E206494A55100_98(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9E815C1DA1D29B2EE0FABE1D38FAADEB1CF6D367_99(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1D3E73DD251585C4908CBA58A179E1911834C891_100(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D31CDD717843C5C2B207F235634E6726898D4858A_101(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_102(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_103(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_104(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_105(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_106(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_107(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_108(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_109(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_110(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_111(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_112(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_113(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_114(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_115(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_116(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_117(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_118(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_119(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_120(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_121(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_122(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_123(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_124(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_125(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_126(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_127(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_128(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_129(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_130(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_131(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_132(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_133(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_134(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_135(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_136(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_137(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_138(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_139(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_140(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_141(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_142(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_143(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_144(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_145(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_146(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_147(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_148(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_149(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_150(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_151(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_152(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_153(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_154(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_155(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_156(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_157(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_158(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_159(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_160(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_161(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_162(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_163(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_164(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_165(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_166(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_167(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_168(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_169(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_170(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_171(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_172(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D643F7C1D25ADAFA2F5ED135D8331618A14714ED2_173(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_174(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_175(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_176(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_177(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_178(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_179(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_180(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_181(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_182(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_183(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_184(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_185(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_186(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_187(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_188(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_189(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_190(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_191(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_192(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_193(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_194(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_195(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_196(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_197(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_198(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_199(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_200(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_201(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_202(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_203(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_204(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_205(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_206(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_207(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_208(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_209(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_210(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_211(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_212(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB80D18C8E4FAC047E388EFD09B6B8B0167634839_213(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D42F0CC24F3873FEDD12BD572B278E0A71346BE65_214(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DCA6E6439A65B6AC13BA3F0B24489DE9EFD3432B0_215(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5E8F331B785E5FEA27072A602CBBC5210046E35B_216(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_217(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0953DF544832295E4A5B19928F95C351F25DA86A_218(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_219(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D148E9E3E864CD628C70D3DC1D8309483BD8C0E89_220(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DAAF72C1002FDBCAE040DAE16A10D82184CE83679_221(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5CF7299F6558A8AC3F821B4F2F65F23798D319D3_222(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB67A7FB4648C62F6A1337CA473436D0E787E8633_223(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC69BF4F300AD2C2E49A8072C2FE6B712FA73EA8F_224(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DFAD52931F5B79811D31566BB18B6E0B5D2E2A164_225(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D50B1635D1FB2907A171B71751E1A3FA79423CA17_226(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D96FD1582D90BAAE271E24B2C90FAD4656F6AB2D8_227(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0125CC3CB136D2FA245C4B85C2B0EDA357F976C3_228(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5F0F8562ECAB46015A377BBE282490A19F0164D7_229(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D63C38CD92581EEB7189BC4C9738BBF9079AEC77A_230(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD6BCEA1F23E790402D02C5AAC11CD0E018D5EAA3_231(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D41BD997428945D4720C58D70D44CC33BE33830E1_232(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D3990806C4EC965959471F976F01BD62DD6EC7AF0_233(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0A5C490B2B4D7E051515AB3DBCC5C56EEA300332_234(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DFFC8121487AF9451D4B73A6DAD025EA3246C005E_235(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D655D75DB7343EE81A1995BDA0CB58AA7D072DA6F_236(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D4FC78C8A04547A82EE4015A7FEEB16E9498EF5CE_237(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D0593A81B07DBEE4DA7A5B9C792B9D1B2EC819292_238(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DDA21F7BB7D0AE92A3EF493034E070DE6B642D59B_239(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D6712D4F9045448650DFF096DD5C2DABFFD91881F_240(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D5919ACCD1AED3463E3F7B2E10E4105E2A7276519_241(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB10D4E6D0E1086A222C77A3DDA04CE89FF28B832_242(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D89147A6AF97A1EAAA8BCBE0F328AF247AEFFDC19_243(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D72AB50CCD6854B3F3914535BDE6F9A8200D314F3_244(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE94267328F3AA246028047D981FBC157DAA8AA6A_245(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DE412DD2A599682217839F819655C2EDD63ACCE0A_246(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DDCF08BCDD530CEE363DB7AD82BEFB88E18891583_247(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC01C545B434FB07175CF325EBFFCD67ED3DE871A_248(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D78F2104B4C44AAE8B55A2FB998CEA2BCF138DFE1_249(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D88F414404C40F505B5068F19FA60FB67845A2337_250(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1F433BCF156500C744956DC4F6CF72D90171C699_251(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D4AE342AF681D8556F9F844B2DEAB460CE85F1E00_252(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D519053662B9633E300410800BCC75679EC16D7AE_253(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D2161BFBA04F65A8DD03880F03451CEAF40D3013C_254(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9978C64907E23B200B0EDA4D501A89B0F075363C_255(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DC96F4447805C79FD865B193E0E5231CACB3F7661_256(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D664308DA52018D3D32B7FB134AF69011874114AC_257(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DF67410C8F39EF676DF776183A98BDDD17C8B98F4_258(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DBB39BAF6CE0501A417BF105F435F00B6A46E1B09_259(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DB9B8E8DB27999DD7D297759E2C0DB5F14CDC32B9_260(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DA49B234BAED1E521F4CC502150FB5CD960556BB7_261(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D1BC81E84591291A507FFBB2389A425CDBC738E46_262(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D231A2B8BB20EA0E6A00B61ED86869E4AF38E568B_263(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D344F35933DADC294755E40C6C3723DE2C4E5FD90_264(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D36D456C626B20C21AFD26F2ECDF45C1FB877A1CD_265(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D175291D1FA3DEB078C2B51F363761FA287FE0D22_266(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D94D18A21349F54EE2CB44216F8FDA81F310F7D60_267(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D2E0E90126C561F5AC67617705F5604DB9E5284A1_268(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D49201D55AB670A481AC262DF9FA3BD17F93DC9FD_269(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9365FDAD4BD75AEF9446E0B22483D217BE36E309_270(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD236F746E09D6EABAC4B8DA6DF883599E7941F13_271(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D68F00C41318114691E02CD7532ACF69A8DBE23C2_272(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D7FB9790B49277F6151D3EB5D555CCF105904DB43_273(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2DD26A27B5531D6252D57917C90488F9C3F7AF8F98_274(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6338 = { sizeof (U24ArrayTypeU3D6144_t2075964317)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D6144_t2075964317 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6339 = { sizeof (U24ArrayTypeU3D384_t3113433889)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D384_t3113433889 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6340 = { sizeof (U24ArrayTypeU3D124_t2306864769)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D124_t2306864769 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6341 = { sizeof (U24ArrayTypeU3D120_t2306864773)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D120_t2306864773 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6342 = { sizeof (U24ArrayTypeU3D76_t3894236543)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D76_t3894236543 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6343 = { sizeof (U24ArrayTypeU3D68_t2375206768)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D68_t2375206768 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6344 = { sizeof (U24ArrayTypeU3D116_t740780830)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D116_t740780830 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6345 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6346 = { sizeof (U24ArrayTypeU3D512_t740780702)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D512_t740780702 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6347 = { sizeof (U24ArrayTypeU3D256_t3066379751)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D256_t3066379751 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6348 = { sizeof (U24ArrayTypeU3D1152_t2882533397)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1152_t2882533397 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6349 = { sizeof (U24ArrayTypeU3D8_t1459944466)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D8_t1459944466 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6350 = { sizeof (U24ArrayTypeU3D32_t1568637719)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637719 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6351 = { sizeof (U24ArrayTypeU3D640_t1500295684)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D640_t1500295684 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6352 = { sizeof (U24ArrayTypeU3D2048_t698097168)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D2048_t698097168 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6353 = { sizeof (U24ArrayTypeU3D1024_t3379926265)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D1024_t3379926265 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6354 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6355 = { sizeof (U24ArrayTypeU3D72_t1568637715)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D72_t1568637715 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6356 = { sizeof (U24ArrayTypeU3D48_t2375206766)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t2375206766 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6357 = { sizeof (U24ArrayTypeU3D96_t3894236537)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D96_t3894236537 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6358 = { sizeof (U24ArrayTypeU3D56_t3894236541)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D56_t3894236541 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6359 = { sizeof (U24ArrayTypeU3D128_t2306864765)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D128_t2306864765 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6360 = { sizeof (U24ArrayTypeU3D4_t1459944470)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4_t1459944470 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6361 = { sizeof (U24ArrayTypeU3D64_t762068660)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t762068660 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6362 = { sizeof (U24ArrayTypeU3D60_t2731437128)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D60_t2731437128 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6363 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6364 = { sizeof (U24ArrayTypeU3D4096_t2217126741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4096_t2217126741 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6365 = { sizeof (U24ArrayTypeU3D40_t2731437126)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D40_t2731437126 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6366 = { sizeof (U24ArrayTypeU3D28_t2375206772)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D28_t2375206772 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6367 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6368 = { sizeof (U24ArrayTypeU3D36_t3894236547)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D36_t3894236547 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6369 = { sizeof (U24ArrayTypeU3D44_t762068658)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D44_t762068658 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6370 = { sizeof (U24ArrayTypeU3D112_t740780834)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D112_t740780834 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6371 = { sizeof (U24ArrayTypeU3D580_t3113433695)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D580_t3113433695 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6372 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6372[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6373 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6373[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6374 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6374[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6375 = { sizeof (U3CModuleU3E_t3783534236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6376 = { sizeof (AmbientObscurance_t4277448813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6376[8] = 
{
	AmbientObscurance_t4277448813::get_offset_of_intensity_5(),
	AmbientObscurance_t4277448813::get_offset_of_radius_6(),
	AmbientObscurance_t4277448813::get_offset_of_blurIterations_7(),
	AmbientObscurance_t4277448813::get_offset_of_blurFilterDistance_8(),
	AmbientObscurance_t4277448813::get_offset_of_downsample_9(),
	AmbientObscurance_t4277448813::get_offset_of_rand_10(),
	AmbientObscurance_t4277448813::get_offset_of_aoShader_11(),
	AmbientObscurance_t4277448813::get_offset_of_aoMaterial_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6377 = { sizeof (AAMode_t3182714109)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6377[8] = 
{
	AAMode_t3182714109::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6378 = { sizeof (AntialiasingAsPostEffect_t2974095527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6378[22] = 
{
	AntialiasingAsPostEffect_t2974095527::get_offset_of_mode_5(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_showGeneratedNormals_6(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_offsetScale_7(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_blurRadius_8(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_edgeThresholdMin_9(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_edgeThreshold_10(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_edgeSharpness_11(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_dlaaSharp_12(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_ssaaShader_13(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_ssaa_14(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_dlaaShader_15(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_dlaa_16(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_nfaaShader_17(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_nfaa_18(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_shaderFXAAPreset2_19(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_materialFXAAPreset2_20(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_shaderFXAAPreset3_21(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_materialFXAAPreset3_22(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_shaderFXAAII_23(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_materialFXAAII_24(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_shaderFXAAIII_25(),
	AntialiasingAsPostEffect_t2974095527::get_offset_of_materialFXAAIII_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6379 = { sizeof (Bloom_t1048415213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6379[31] = 
{
	Bloom_t1048415213::get_offset_of_tweakMode_5(),
	Bloom_t1048415213::get_offset_of_screenBlendMode_6(),
	Bloom_t1048415213::get_offset_of_hdr_7(),
	Bloom_t1048415213::get_offset_of_doHdr_8(),
	Bloom_t1048415213::get_offset_of_sepBlurSpread_9(),
	Bloom_t1048415213::get_offset_of_quality_10(),
	Bloom_t1048415213::get_offset_of_bloomIntensity_11(),
	Bloom_t1048415213::get_offset_of_bloomThreshhold_12(),
	Bloom_t1048415213::get_offset_of_bloomThreshholdColor_13(),
	Bloom_t1048415213::get_offset_of_bloomBlurIterations_14(),
	Bloom_t1048415213::get_offset_of_hollywoodFlareBlurIterations_15(),
	Bloom_t1048415213::get_offset_of_flareRotation_16(),
	Bloom_t1048415213::get_offset_of_lensflareMode_17(),
	Bloom_t1048415213::get_offset_of_hollyStretchWidth_18(),
	Bloom_t1048415213::get_offset_of_lensflareIntensity_19(),
	Bloom_t1048415213::get_offset_of_lensflareThreshhold_20(),
	Bloom_t1048415213::get_offset_of_lensFlareSaturation_21(),
	Bloom_t1048415213::get_offset_of_flareColorA_22(),
	Bloom_t1048415213::get_offset_of_flareColorB_23(),
	Bloom_t1048415213::get_offset_of_flareColorC_24(),
	Bloom_t1048415213::get_offset_of_flareColorD_25(),
	Bloom_t1048415213::get_offset_of_blurWidth_26(),
	Bloom_t1048415213::get_offset_of_lensFlareVignetteMask_27(),
	Bloom_t1048415213::get_offset_of_lensFlareShader_28(),
	Bloom_t1048415213::get_offset_of_lensFlareMaterial_29(),
	Bloom_t1048415213::get_offset_of_screenBlendShader_30(),
	Bloom_t1048415213::get_offset_of_screenBlend_31(),
	Bloom_t1048415213::get_offset_of_blurAndFlaresShader_32(),
	Bloom_t1048415213::get_offset_of_blurAndFlaresMaterial_33(),
	Bloom_t1048415213::get_offset_of_brightPassFilterShader_34(),
	Bloom_t1048415213::get_offset_of_brightPassFilterMaterial_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6380 = { sizeof (LensFlareStyle_t2485171085)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6380[4] = 
{
	LensFlareStyle_t2485171085::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6381 = { sizeof (TweakMode_t2162868461)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6381[3] = 
{
	TweakMode_t2162868461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6382 = { sizeof (HDRBloomMode_t1124970370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6382[4] = 
{
	HDRBloomMode_t1124970370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6383 = { sizeof (BloomScreenBlendMode_t4287047507)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6383[3] = 
{
	BloomScreenBlendMode_t4287047507::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6384 = { sizeof (BloomQuality_t3407301380)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6384[3] = 
{
	BloomQuality_t3407301380::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6385 = { sizeof (LensflareStyle34_t3344173254)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6385[4] = 
{
	LensflareStyle34_t3344173254::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6386 = { sizeof (TweakMode34_t1181002552)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6386[3] = 
{
	TweakMode34_t1181002552::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6387 = { sizeof (HDRBloomMode_t356052982)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6387[4] = 
{
	HDRBloomMode_t356052982::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6388 = { sizeof (BloomScreenBlendMode_t3677155143)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6388[3] = 
{
	BloomScreenBlendMode_t3677155143::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6389 = { sizeof (BloomAndLensFlares_t4077233549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6389[35] = 
{
	BloomAndLensFlares_t4077233549::get_offset_of_tweakMode_5(),
	BloomAndLensFlares_t4077233549::get_offset_of_screenBlendMode_6(),
	BloomAndLensFlares_t4077233549::get_offset_of_hdr_7(),
	BloomAndLensFlares_t4077233549::get_offset_of_doHdr_8(),
	BloomAndLensFlares_t4077233549::get_offset_of_sepBlurSpread_9(),
	BloomAndLensFlares_t4077233549::get_offset_of_useSrcAlphaAsMask_10(),
	BloomAndLensFlares_t4077233549::get_offset_of_bloomIntensity_11(),
	BloomAndLensFlares_t4077233549::get_offset_of_bloomThreshhold_12(),
	BloomAndLensFlares_t4077233549::get_offset_of_bloomBlurIterations_13(),
	BloomAndLensFlares_t4077233549::get_offset_of_lensflares_14(),
	BloomAndLensFlares_t4077233549::get_offset_of_hollywoodFlareBlurIterations_15(),
	BloomAndLensFlares_t4077233549::get_offset_of_lensflareMode_16(),
	BloomAndLensFlares_t4077233549::get_offset_of_hollyStretchWidth_17(),
	BloomAndLensFlares_t4077233549::get_offset_of_lensflareIntensity_18(),
	BloomAndLensFlares_t4077233549::get_offset_of_lensflareThreshhold_19(),
	BloomAndLensFlares_t4077233549::get_offset_of_flareColorA_20(),
	BloomAndLensFlares_t4077233549::get_offset_of_flareColorB_21(),
	BloomAndLensFlares_t4077233549::get_offset_of_flareColorC_22(),
	BloomAndLensFlares_t4077233549::get_offset_of_flareColorD_23(),
	BloomAndLensFlares_t4077233549::get_offset_of_blurWidth_24(),
	BloomAndLensFlares_t4077233549::get_offset_of_lensFlareVignetteMask_25(),
	BloomAndLensFlares_t4077233549::get_offset_of_lensFlareShader_26(),
	BloomAndLensFlares_t4077233549::get_offset_of_lensFlareMaterial_27(),
	BloomAndLensFlares_t4077233549::get_offset_of_vignetteShader_28(),
	BloomAndLensFlares_t4077233549::get_offset_of_vignetteMaterial_29(),
	BloomAndLensFlares_t4077233549::get_offset_of_separableBlurShader_30(),
	BloomAndLensFlares_t4077233549::get_offset_of_separableBlurMaterial_31(),
	BloomAndLensFlares_t4077233549::get_offset_of_addBrightStuffOneOneShader_32(),
	BloomAndLensFlares_t4077233549::get_offset_of_addBrightStuffBlendOneOneMaterial_33(),
	BloomAndLensFlares_t4077233549::get_offset_of_screenBlendShader_34(),
	BloomAndLensFlares_t4077233549::get_offset_of_screenBlend_35(),
	BloomAndLensFlares_t4077233549::get_offset_of_hollywoodFlaresShader_36(),
	BloomAndLensFlares_t4077233549::get_offset_of_hollywoodFlaresMaterial_37(),
	BloomAndLensFlares_t4077233549::get_offset_of_brightPassFilterShader_38(),
	BloomAndLensFlares_t4077233549::get_offset_of_brightPassFilterMaterial_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6390 = { sizeof (Blur_t3683821091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6390[6] = 
{
	Blur_t3683821091::get_offset_of_downsample_5(),
	Blur_t3683821091::get_offset_of_blurSize_6(),
	Blur_t3683821091::get_offset_of_blurIterations_7(),
	Blur_t3683821091::get_offset_of_blurType_8(),
	Blur_t3683821091::get_offset_of_blurShader_9(),
	Blur_t3683821091::get_offset_of_blurMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6391 = { sizeof (BlurType_t2760015345)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6391[3] = 
{
	BlurType_t2760015345::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6392 = { sizeof (CameraMotionBlur_t32230768), -1, sizeof(CameraMotionBlur_t32230768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6392[30] = 
{
	CameraMotionBlur_t32230768_StaticFields::get_offset_of_MAX_RADIUS_5(),
	CameraMotionBlur_t32230768::get_offset_of_filterType_6(),
	CameraMotionBlur_t32230768::get_offset_of_preview_7(),
	CameraMotionBlur_t32230768::get_offset_of_previewScale_8(),
	CameraMotionBlur_t32230768::get_offset_of_movementScale_9(),
	CameraMotionBlur_t32230768::get_offset_of_rotationScale_10(),
	CameraMotionBlur_t32230768::get_offset_of_maxVelocity_11(),
	CameraMotionBlur_t32230768::get_offset_of_minVelocity_12(),
	CameraMotionBlur_t32230768::get_offset_of_velocityScale_13(),
	CameraMotionBlur_t32230768::get_offset_of_softZDistance_14(),
	CameraMotionBlur_t32230768::get_offset_of_velocityDownsample_15(),
	CameraMotionBlur_t32230768::get_offset_of_excludeLayers_16(),
	CameraMotionBlur_t32230768::get_offset_of_tmpCam_17(),
	CameraMotionBlur_t32230768::get_offset_of_shader_18(),
	CameraMotionBlur_t32230768::get_offset_of_dx11MotionBlurShader_19(),
	CameraMotionBlur_t32230768::get_offset_of_replacementClear_20(),
	CameraMotionBlur_t32230768::get_offset_of_motionBlurMaterial_21(),
	CameraMotionBlur_t32230768::get_offset_of_dx11MotionBlurMaterial_22(),
	CameraMotionBlur_t32230768::get_offset_of_noiseTexture_23(),
	CameraMotionBlur_t32230768::get_offset_of_jitter_24(),
	CameraMotionBlur_t32230768::get_offset_of_showVelocity_25(),
	CameraMotionBlur_t32230768::get_offset_of_showVelocityScale_26(),
	CameraMotionBlur_t32230768::get_offset_of_currentViewProjMat_27(),
	CameraMotionBlur_t32230768::get_offset_of_prevViewProjMat_28(),
	CameraMotionBlur_t32230768::get_offset_of_prevFrameCount_29(),
	CameraMotionBlur_t32230768::get_offset_of_wasActive_30(),
	CameraMotionBlur_t32230768::get_offset_of_prevFrameForward_31(),
	CameraMotionBlur_t32230768::get_offset_of_prevFrameRight_32(),
	CameraMotionBlur_t32230768::get_offset_of_prevFrameUp_33(),
	CameraMotionBlur_t32230768::get_offset_of_prevFramePos_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6393 = { sizeof (MotionBlurFilter_t3030354800)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6393[6] = 
{
	MotionBlurFilter_t3030354800::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6394 = { sizeof (ColorCorrectionMode_t1571628420)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6394[3] = 
{
	ColorCorrectionMode_t1571628420::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6395 = { sizeof (ColorCorrectionCurves_t1552509009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6395[24] = 
{
	ColorCorrectionCurves_t1552509009::get_offset_of_redChannel_5(),
	ColorCorrectionCurves_t1552509009::get_offset_of_greenChannel_6(),
	ColorCorrectionCurves_t1552509009::get_offset_of_blueChannel_7(),
	ColorCorrectionCurves_t1552509009::get_offset_of_useDepthCorrection_8(),
	ColorCorrectionCurves_t1552509009::get_offset_of_zCurve_9(),
	ColorCorrectionCurves_t1552509009::get_offset_of_depthRedChannel_10(),
	ColorCorrectionCurves_t1552509009::get_offset_of_depthGreenChannel_11(),
	ColorCorrectionCurves_t1552509009::get_offset_of_depthBlueChannel_12(),
	ColorCorrectionCurves_t1552509009::get_offset_of_ccMaterial_13(),
	ColorCorrectionCurves_t1552509009::get_offset_of_ccDepthMaterial_14(),
	ColorCorrectionCurves_t1552509009::get_offset_of_selectiveCcMaterial_15(),
	ColorCorrectionCurves_t1552509009::get_offset_of_rgbChannelTex_16(),
	ColorCorrectionCurves_t1552509009::get_offset_of_rgbDepthChannelTex_17(),
	ColorCorrectionCurves_t1552509009::get_offset_of_zCurveTex_18(),
	ColorCorrectionCurves_t1552509009::get_offset_of_saturation_19(),
	ColorCorrectionCurves_t1552509009::get_offset_of_selectiveCc_20(),
	ColorCorrectionCurves_t1552509009::get_offset_of_selectiveFromColor_21(),
	ColorCorrectionCurves_t1552509009::get_offset_of_selectiveToColor_22(),
	ColorCorrectionCurves_t1552509009::get_offset_of_mode_23(),
	ColorCorrectionCurves_t1552509009::get_offset_of_updateTextures_24(),
	ColorCorrectionCurves_t1552509009::get_offset_of_colorCorrectionCurvesShader_25(),
	ColorCorrectionCurves_t1552509009::get_offset_of_simpleColorCorrectionCurvesShader_26(),
	ColorCorrectionCurves_t1552509009::get_offset_of_colorCorrectionSelectiveShader_27(),
	ColorCorrectionCurves_t1552509009::get_offset_of_updateTexturesOnStartup_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6396 = { sizeof (ColorCorrectionLut_t2033555384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6396[4] = 
{
	ColorCorrectionLut_t2033555384::get_offset_of_shader_5(),
	ColorCorrectionLut_t2033555384::get_offset_of_material_6(),
	ColorCorrectionLut_t2033555384::get_offset_of_converted3DLut_7(),
	ColorCorrectionLut_t2033555384::get_offset_of_basedOnTempTex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6397 = { sizeof (ContrastEnhance_t3766296508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6397[7] = 
{
	ContrastEnhance_t3766296508::get_offset_of_intensity_5(),
	ContrastEnhance_t3766296508::get_offset_of_threshhold_6(),
	ContrastEnhance_t3766296508::get_offset_of_separableBlurMaterial_7(),
	ContrastEnhance_t3766296508::get_offset_of_contrastCompositeMaterial_8(),
	ContrastEnhance_t3766296508::get_offset_of_blurSpread_9(),
	ContrastEnhance_t3766296508::get_offset_of_separableBlurShader_10(),
	ContrastEnhance_t3766296508::get_offset_of_contrastCompositeShader_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6398 = { sizeof (Crease_t1747016313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6398[9] = 
{
	Crease_t1747016313::get_offset_of_intensity_5(),
	Crease_t1747016313::get_offset_of_softness_6(),
	Crease_t1747016313::get_offset_of_spread_7(),
	Crease_t1747016313::get_offset_of_blurShader_8(),
	Crease_t1747016313::get_offset_of_blurMaterial_9(),
	Crease_t1747016313::get_offset_of_depthFetchShader_10(),
	Crease_t1747016313::get_offset_of_depthFetchMaterial_11(),
	Crease_t1747016313::get_offset_of_creaseApplyShader_12(),
	Crease_t1747016313::get_offset_of_creaseApplyMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6399 = { sizeof (Dof34QualitySetting_t2579674687)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6399[3] = 
{
	Dof34QualitySetting_t2579674687::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
