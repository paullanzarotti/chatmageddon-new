﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_Stripe
struct CameraFilterPack_Gradients_Stripe_t3929419426;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_Stripe::.ctor()
extern "C"  void CameraFilterPack_Gradients_Stripe__ctor_m3962293575 (CameraFilterPack_Gradients_Stripe_t3929419426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Stripe::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_Stripe_get_material_m3656464606 (CameraFilterPack_Gradients_Stripe_t3929419426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::Start()
extern "C"  void CameraFilterPack_Gradients_Stripe_Start_m1464429843 (CameraFilterPack_Gradients_Stripe_t3929419426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_Stripe_OnRenderImage_m1791265539 (CameraFilterPack_Gradients_Stripe_t3929419426 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::Update()
extern "C"  void CameraFilterPack_Gradients_Stripe_Update_m2601019148 (CameraFilterPack_Gradients_Stripe_t3929419426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::OnDisable()
extern "C"  void CameraFilterPack_Gradients_Stripe_OnDisable_m3693099362 (CameraFilterPack_Gradients_Stripe_t3929419426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
