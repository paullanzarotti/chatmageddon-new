﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileResultsChatButton
struct MissileResultsChatButton_t36425224;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileResultsChatButton::.ctor()
extern "C"  void MissileResultsChatButton__ctor_m1218703317 (MissileResultsChatButton_t36425224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsChatButton::OnButtonClick()
extern "C"  void MissileResultsChatButton_OnButtonClick_m3591213948 (MissileResultsChatButton_t36425224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
