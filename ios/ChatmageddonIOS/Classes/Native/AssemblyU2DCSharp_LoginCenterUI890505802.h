﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginCenterUI
struct  LoginCenterUI_t890505802  : public MonoBehaviour_t1158329972
{
public:
	// UITexture LoginCenterUI::background
	UITexture_t2537039969 * ___background_2;

public:
	inline static int32_t get_offset_of_background_2() { return static_cast<int32_t>(offsetof(LoginCenterUI_t890505802, ___background_2)); }
	inline UITexture_t2537039969 * get_background_2() const { return ___background_2; }
	inline UITexture_t2537039969 ** get_address_of_background_2() { return &___background_2; }
	inline void set_background_2(UITexture_t2537039969 * value)
	{
		___background_2 = value;
		Il2CppCodeGenWriteBarrier(&___background_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
