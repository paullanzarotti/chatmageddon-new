﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendModal
struct DefendModal_t2645228609;
// LaunchedItem
struct LaunchedItem_t3670634427;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427.h"

// System.Void DefendModal::.ctor()
extern "C"  void DefendModal__ctor_m2614759280 (DefendModal_t2645228609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModal::LoadDefendData(LaunchedItem)
extern "C"  void DefendModal_LoadDefendData_m2376371639 (DefendModal_t2645228609 * __this, LaunchedItem_t3670634427 * ___itemData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModal::LoadDefendUI(System.Boolean)
extern "C"  void DefendModal_LoadDefendUI_m596846497 (DefendModal_t2645228609 * __this, bool ___withInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModal::OnUIDeactivate()
extern "C"  void DefendModal_OnUIDeactivate_m1282305649 (DefendModal_t2645228609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject DefendModal::GetItemModel()
extern "C"  GameObject_t1756533147 * DefendModal_GetItemModel_m1112445643 (DefendModal_t2645228609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
