﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileBlockButton
struct PlayerProfileBlockButton_t3859158973;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayerProfileBlockButton::.ctor()
extern "C"  void PlayerProfileBlockButton__ctor_m3590750882 (PlayerProfileBlockButton_t3859158973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileBlockButton::OnButtonClick()
extern "C"  void PlayerProfileBlockButton_OnButtonClick_m1358733665 (PlayerProfileBlockButton_t3859158973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileBlockButton::SetButtonActive(System.Boolean)
extern "C"  void PlayerProfileBlockButton_SetButtonActive_m2858830371 (PlayerProfileBlockButton_t3859158973 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileBlockButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void PlayerProfileBlockButton_U3COnButtonClickU3Em__0_m617134687 (PlayerProfileBlockButton_t3859158973 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileBlockButton::<OnButtonClick>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void PlayerProfileBlockButton_U3COnButtonClickU3Em__1_m287311294 (PlayerProfileBlockButton_t3859158973 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
