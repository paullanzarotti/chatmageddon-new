﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditNameNavScreen
struct  EditNameNavScreen_t2089604456  : public NavigationScreen_t2333230110
{
public:
	// UIInput EditNameNavScreen::firstNameInput
	UIInput_t860674234 * ___firstNameInput_3;
	// UIInput EditNameNavScreen::lastNameInput
	UIInput_t860674234 * ___lastNameInput_4;
	// System.Boolean EditNameNavScreen::UIclosing
	bool ___UIclosing_5;

public:
	inline static int32_t get_offset_of_firstNameInput_3() { return static_cast<int32_t>(offsetof(EditNameNavScreen_t2089604456, ___firstNameInput_3)); }
	inline UIInput_t860674234 * get_firstNameInput_3() const { return ___firstNameInput_3; }
	inline UIInput_t860674234 ** get_address_of_firstNameInput_3() { return &___firstNameInput_3; }
	inline void set_firstNameInput_3(UIInput_t860674234 * value)
	{
		___firstNameInput_3 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameInput_3, value);
	}

	inline static int32_t get_offset_of_lastNameInput_4() { return static_cast<int32_t>(offsetof(EditNameNavScreen_t2089604456, ___lastNameInput_4)); }
	inline UIInput_t860674234 * get_lastNameInput_4() const { return ___lastNameInput_4; }
	inline UIInput_t860674234 ** get_address_of_lastNameInput_4() { return &___lastNameInput_4; }
	inline void set_lastNameInput_4(UIInput_t860674234 * value)
	{
		___lastNameInput_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameInput_4, value);
	}

	inline static int32_t get_offset_of_UIclosing_5() { return static_cast<int32_t>(offsetof(EditNameNavScreen_t2089604456, ___UIclosing_5)); }
	inline bool get_UIclosing_5() const { return ___UIclosing_5; }
	inline bool* get_address_of_UIclosing_5() { return &___UIclosing_5; }
	inline void set_UIclosing_5(bool value)
	{
		___UIclosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
