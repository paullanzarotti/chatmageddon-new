﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TooltipWithoutBackgroundExample
struct TooltipWithoutBackgroundExample_t2651718405;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"

// System.Void TooltipWithoutBackgroundExample::.ctor()
extern "C"  void TooltipWithoutBackgroundExample__ctor_m1621044036 (TooltipWithoutBackgroundExample_t2651718405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TooltipWithoutBackgroundExample::Start()
extern "C"  void TooltipWithoutBackgroundExample_Start_m327287436 (TooltipWithoutBackgroundExample_t2651718405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TooltipWithoutBackgroundExample::OnPrepareTooltipStyle(UnityEngine.GUIStyle&)
extern "C"  void TooltipWithoutBackgroundExample_OnPrepareTooltipStyle_m379588271 (TooltipWithoutBackgroundExample_t2651718405 * __this, GUIStyle_t1799908754 ** ___style0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
