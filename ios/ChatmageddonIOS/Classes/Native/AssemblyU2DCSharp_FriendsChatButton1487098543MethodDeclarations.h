﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsChatButton
struct FriendsChatButton_t1487098543;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsChatButton::.ctor()
extern "C"  void FriendsChatButton__ctor_m3900396974 (FriendsChatButton_t1487098543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsChatButton::OnButtonClick()
extern "C"  void FriendsChatButton_OnButtonClick_m885085767 (FriendsChatButton_t1487098543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
