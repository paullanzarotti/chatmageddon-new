﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MapGroupModal
struct MapGroupModal_t1530151518;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseMapGroupButton
struct  CloseMapGroupButton_t794394401  : public SFXButton_t792651341
{
public:
	// MapGroupModal CloseMapGroupButton::modal
	MapGroupModal_t1530151518 * ___modal_5;

public:
	inline static int32_t get_offset_of_modal_5() { return static_cast<int32_t>(offsetof(CloseMapGroupButton_t794394401, ___modal_5)); }
	inline MapGroupModal_t1530151518 * get_modal_5() const { return ___modal_5; }
	inline MapGroupModal_t1530151518 ** get_address_of_modal_5() { return &___modal_5; }
	inline void set_modal_5(MapGroupModal_t1530151518 * value)
	{
		___modal_5 = value;
		Il2CppCodeGenWriteBarrier(&___modal_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
