﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Crashlytics.Internal.Impl
struct Impl_t2902205834;
// System.String
struct String_t;
// System.Diagnostics.StackTrace
struct StackTrace_t2500644597;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.String,System.String>[]
struct Dictionary_2U5BU5D_t131476478;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Diagnostics_StackTrace2500644597.h"

// System.Void Fabric.Crashlytics.Internal.Impl::.ctor()
extern "C"  void Impl__ctor_m646977750 (Impl_t2902205834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Fabric.Crashlytics.Internal.Impl Fabric.Crashlytics.Internal.Impl::Make()
extern "C"  Impl_t2902205834 * Impl_Make_m3532523228 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::SetDebugMode(System.Boolean)
extern "C"  void Impl_SetDebugMode_m1545373335 (Impl_t2902205834 * __this, bool ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::Crash()
extern "C"  void Impl_Crash_m2908233671 (Impl_t2902205834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::ThrowNonFatal()
extern "C"  void Impl_ThrowNonFatal_m1798953979 (Impl_t2902205834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::Log(System.String)
extern "C"  void Impl_Log_m1230632838 (Impl_t2902205834 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::SetKeyValue(System.String,System.String)
extern "C"  void Impl_SetKeyValue_m2481010860 (Impl_t2902205834 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::SetUserIdentifier(System.String)
extern "C"  void Impl_SetUserIdentifier_m1681506712 (Impl_t2902205834 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::SetUserEmail(System.String)
extern "C"  void Impl_SetUserEmail_m2587311217 (Impl_t2902205834 * __this, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::SetUserName(System.String)
extern "C"  void Impl_SetUserName_m146521388 (Impl_t2902205834 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::RecordCustomException(System.String,System.String,System.Diagnostics.StackTrace)
extern "C"  void Impl_RecordCustomException_m632837387 (Impl_t2902205834 * __this, String_t* ___name0, String_t* ___reason1, StackTrace_t2500644597 * ___stackTrace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::RecordCustomException(System.String,System.String,System.String)
extern "C"  void Impl_RecordCustomException_m986335927 (Impl_t2902205834 * __this, String_t* ___name0, String_t* ___reason1, String_t* ___stackTraceString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Fabric.Crashlytics.Internal.Impl::ParseFrameString(System.String,System.String)
extern "C"  Dictionary_2_t3943999495 * Impl_ParseFrameString_m2521505605 (Il2CppObject * __this /* static, unused */, String_t* ___regex0, String_t* ___frameString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String>[] Fabric.Crashlytics.Internal.Impl::ParseStackTraceString(System.String)
extern "C"  Dictionary_2U5BU5D_t131476478* Impl_ParseStackTraceString_m434837683 (Il2CppObject * __this /* static, unused */, String_t* ___stackTraceString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.Impl::.cctor()
extern "C"  void Impl__cctor_m3494236695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
