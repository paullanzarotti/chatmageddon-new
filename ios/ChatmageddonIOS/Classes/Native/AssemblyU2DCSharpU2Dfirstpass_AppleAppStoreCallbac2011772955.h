﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.AppleAppStoreBillingService
struct AppleAppStoreBillingService_t956296338;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppleAppStoreCallbackMonoBehaviour
struct  AppleAppStoreCallbackMonoBehaviour_t2011772955  : public MonoBehaviour_t1158329972
{
public:
	// Unibill.Impl.AppleAppStoreBillingService AppleAppStoreCallbackMonoBehaviour::callback
	AppleAppStoreBillingService_t956296338 * ___callback_2;

public:
	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(AppleAppStoreCallbackMonoBehaviour_t2011772955, ___callback_2)); }
	inline AppleAppStoreBillingService_t956296338 * get_callback_2() const { return ___callback_2; }
	inline AppleAppStoreBillingService_t956296338 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(AppleAppStoreBillingService_t956296338 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
