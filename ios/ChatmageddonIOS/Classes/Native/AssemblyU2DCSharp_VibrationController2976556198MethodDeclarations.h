﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VibrationController
struct VibrationController_t2976556198;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VibrationType1951301914.h"

// System.Void VibrationController::.ctor()
extern "C"  void VibrationController__ctor_m3021961997 (VibrationController_t2976556198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController::VibrateDevice(VibrationType,System.Single)
extern "C"  void VibrationController_VibrateDevice_m432977373 (VibrationController_t2976556198 * __this, int32_t ___vibration0, float ___durationInSecs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VibrationController::ContinuousVibrate(System.Single)
extern "C"  Il2CppObject * VibrationController_ContinuousVibrate_m2533401476 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VibrationController::PulseVibrate(System.Single)
extern "C"  Il2CppObject * VibrationController_PulseVibrate_m2606443574 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VibrationController::SingleVibrate(System.Single)
extern "C"  Il2CppObject * VibrationController_SingleVibrate_m597131523 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VibrationController::SwitchVibrate(System.Single)
extern "C"  Il2CppObject * VibrationController_SwitchVibrate_m1470384715 (VibrationController_t2976556198 * __this, float ___duration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
