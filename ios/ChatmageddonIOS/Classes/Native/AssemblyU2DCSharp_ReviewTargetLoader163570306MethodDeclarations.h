﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReviewTargetLoader
struct ReviewTargetLoader_t163570306;

#include "codegen/il2cpp-codegen.h"

// System.Void ReviewTargetLoader::.ctor()
extern "C"  void ReviewTargetLoader__ctor_m1632281009 (ReviewTargetLoader_t163570306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReviewTargetLoader::PopulateTarget()
extern "C"  void ReviewTargetLoader_PopulateTarget_m1946973054 (ReviewTargetLoader_t163570306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReviewTargetLoader::PopulateTargetName()
extern "C"  void ReviewTargetLoader_PopulateTargetName_m122465609 (ReviewTargetLoader_t163570306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReviewTargetLoader::PopulateTargetAvatar()
extern "C"  void ReviewTargetLoader_PopulateTargetAvatar_m1898631663 (ReviewTargetLoader_t163570306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReviewTargetLoader::PopulateTargetFirstStrikeBonus()
extern "C"  void ReviewTargetLoader_PopulateTargetFirstStrikeBonus_m94545231 (ReviewTargetLoader_t163570306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
