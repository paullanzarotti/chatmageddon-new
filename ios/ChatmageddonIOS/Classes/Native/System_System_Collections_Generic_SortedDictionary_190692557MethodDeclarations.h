﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>
struct SortedDictionary_2_t3346886819;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_190692557.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1909252904_gshared (Enumerator_t190692557 * __this, SortedDictionary_2_t3346886819 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m1909252904(__this, ___dic0, method) ((  void (*) (Enumerator_t190692557 *, SortedDictionary_2_t3346886819 *, const MethodInfo*))Enumerator__ctor_m1909252904_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1776647976_gshared (Enumerator_t190692557 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1776647976(__this, method) ((  Il2CppObject * (*) (Enumerator_t190692557 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1776647976_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m789808238_gshared (Enumerator_t190692557 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m789808238(__this, method) ((  void (*) (Enumerator_t190692557 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m789808238_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3403012472_gshared (Enumerator_t190692557 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3403012472(__this, method) ((  Il2CppObject * (*) (Enumerator_t190692557 *, const MethodInfo*))Enumerator_get_Current_m3403012472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1855354142_gshared (Enumerator_t190692557 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1855354142(__this, method) ((  bool (*) (Enumerator_t190692557 *, const MethodInfo*))Enumerator_MoveNext_m1855354142_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4083767379_gshared (Enumerator_t190692557 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4083767379(__this, method) ((  void (*) (Enumerator_t190692557 *, const MethodInfo*))Enumerator_Dispose_m4083767379_gshared)(__this, method)
