﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RotatingTabController
struct RotatingTabController_t2617193705;

#include "codegen/il2cpp-codegen.h"

// System.Void RotatingTabController::.ctor()
extern "C"  void RotatingTabController__ctor_m3224518410 (RotatingTabController_t2617193705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTabController::StartRotation()
extern "C"  void RotatingTabController_StartRotation_m2717067664 (RotatingTabController_t2617193705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTabController::ReverseRotation()
extern "C"  void RotatingTabController_ReverseRotation_m2436904078 (RotatingTabController_t2617193705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTabController::StopRotation()
extern "C"  void RotatingTabController_StopRotation_m229247938 (RotatingTabController_t2617193705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RotatingTabController::SetTabsColliding(System.Boolean)
extern "C"  void RotatingTabController_SetTabsColliding_m1728770380 (RotatingTabController_t2617193705 * __this, bool ___colliding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RotatingTabController::TabsColliding()
extern "C"  bool RotatingTabController_TabsColliding_m1515441745 (RotatingTabController_t2617193705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
