﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TOSNavScreen
struct TOSNavScreen_t2379335315;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void TOSNavScreen::.ctor()
extern "C"  void TOSNavScreen__ctor_m1211709122 (TOSNavScreen_t2379335315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TOSNavScreen::Start()
extern "C"  void TOSNavScreen_Start_m1569810686 (TOSNavScreen_t2379335315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TOSNavScreen::UIClosing()
extern "C"  void TOSNavScreen_UIClosing_m1392554165 (TOSNavScreen_t2379335315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TOSNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void TOSNavScreen_ScreenLoad_m2340043562 (TOSNavScreen_t2379335315 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TOSNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void TOSNavScreen_ScreenUnload_m3357885294 (TOSNavScreen_t2379335315 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TOSNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool TOSNavScreen_ValidateScreenNavigation_m2923582305 (TOSNavScreen_t2379335315 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
