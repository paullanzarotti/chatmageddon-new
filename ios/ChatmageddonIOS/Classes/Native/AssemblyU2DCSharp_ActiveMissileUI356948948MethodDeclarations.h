﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActiveMissileUI
struct ActiveMissileUI_t356948948;

#include "codegen/il2cpp-codegen.h"

// System.Void ActiveMissileUI::.ctor()
extern "C"  void ActiveMissileUI__ctor_m293108971 (ActiveMissileUI_t356948948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::OnEnable()
extern "C"  void ActiveMissileUI_OnEnable_m1432075439 (ActiveMissileUI_t356948948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::OnDisable()
extern "C"  void ActiveMissileUI_OnDisable_m675261360 (ActiveMissileUI_t356948948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::CalculatorHitZero()
extern "C"  void ActiveMissileUI_CalculatorHitZero_m98157080 (ActiveMissileUI_t356948948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::LoadUI(System.Boolean)
extern "C"  void ActiveMissileUI_LoadUI_m587826960 (ActiveMissileUI_t356948948 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::UpdateShieldUI()
extern "C"  void ActiveMissileUI_UpdateShieldUI_m2186932421 (ActiveMissileUI_t356948948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::SetShieldActive(System.Boolean)
extern "C"  void ActiveMissileUI_SetShieldActive_m2228316321 (ActiveMissileUI_t356948948 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::UnloadUI(System.Boolean)
extern "C"  void ActiveMissileUI_UnloadUI_m534025693 (ActiveMissileUI_t356948948 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveMissileUI::OnScaleTweenFinished()
extern "C"  void ActiveMissileUI_OnScaleTweenFinished_m2287190853 (ActiveMissileUI_t356948948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
