﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsNavigationController
struct SettingsNavigationController_t2581944457;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void SettingsNavigationController::.ctor()
extern "C"  void SettingsNavigationController__ctor_m2430851402 (SettingsNavigationController_t2581944457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::Start()
extern "C"  void SettingsNavigationController_Start_m2683917290 (SettingsNavigationController_t2581944457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::NavigateBackwards()
extern "C"  void SettingsNavigationController_NavigateBackwards_m18127703 (SettingsNavigationController_t2581944457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::NavigateForwards(SettingsNavScreen)
extern "C"  void SettingsNavigationController_NavigateForwards_m2147299011 (SettingsNavigationController_t2581944457 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SettingsNavigationController::ValidateNavigation(SettingsNavScreen,NavigationDirection)
extern "C"  bool SettingsNavigationController_ValidateNavigation_m3227439615 (SettingsNavigationController_t2581944457 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SettingsNavigationController::SaveScreenContent(SettingsNavScreen,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * SettingsNavigationController_SaveScreenContent_m3854934564 (SettingsNavigationController_t2581944457 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::LoadNavScreen(SettingsNavScreen,NavigationDirection,System.Boolean)
extern "C"  void SettingsNavigationController_LoadNavScreen_m2533128151 (SettingsNavigationController_t2581944457 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::UnloadNavScreen(SettingsNavScreen,NavigationDirection)
extern "C"  void SettingsNavigationController_UnloadNavScreen_m4243059523 (SettingsNavigationController_t2581944457 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::PopulateTitleBar(SettingsNavScreen)
extern "C"  void SettingsNavigationController_PopulateTitleBar_m1060070307 (SettingsNavigationController_t2581944457 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::UnloadSettingsFromBlock(System.Boolean)
extern "C"  void SettingsNavigationController_UnloadSettingsFromBlock_m585432284 (SettingsNavigationController_t2581944457 * __this, bool ___chat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController::ActivateLogoutUI()
extern "C"  void SettingsNavigationController_ActivateLogoutUI_m4040881707 (SettingsNavigationController_t2581944457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
