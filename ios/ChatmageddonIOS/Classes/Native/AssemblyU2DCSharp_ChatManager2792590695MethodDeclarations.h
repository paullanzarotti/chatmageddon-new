﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatManager
struct ChatManager_t2792590695;
// ChatContact
struct ChatContact_t3760700014;
// ChatThread
struct ChatThread_t2394323482;
// System.String
struct String_t;
// ChatMessage
struct ChatMessage_t2384228687;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatContact3760700014.h"
#include "AssemblyU2DCSharp_ChatThread2394323482.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ChatMessage2384228687.h"
#include "AssemblyU2DCSharp_ChatListType844669534.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void ChatManager::.ctor()
extern "C"  void ChatManager__ctor_m3800112208 (ChatManager_t2792590695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::StartChatUI()
extern "C"  void ChatManager_StartChatUI_m3152244280 (ChatManager_t2792590695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::LoadStartFriendMessage()
extern "C"  void ChatManager_LoadStartFriendMessage_m578875189 (ChatManager_t2792590695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::AddNewContact(ChatContact)
extern "C"  void ChatManager_AddNewContact_m392169593 (ChatManager_t2792590695 * __this, ChatContact_t3760700014 * ___contact0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChatThread ChatManager::SearchForExistingthread(ChatContact,System.Boolean)
extern "C"  ChatThread_t2394323482 * ChatManager_SearchForExistingthread_m2478491316 (ChatManager_t2792590695 * __this, ChatContact_t3760700014 * ___contact0, bool ___multiUser1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::CreateNewMessage(ChatContact)
extern "C"  void ChatManager_CreateNewMessage_m1785402231 (ChatManager_t2792590695 * __this, ChatContact_t3760700014 * ___contact0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::AddAdditionalContact(ChatContact)
extern "C"  void ChatManager_AddAdditionalContact_m1471334476 (ChatManager_t2792590695 * __this, ChatContact_t3760700014 * ___contact0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatManager::GetActiveContactString()
extern "C"  String_t* ChatManager_GetActiveContactString_m488404402 (ChatManager_t2792590695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::OpenExistingThread(ChatThread,System.Boolean)
extern "C"  void ChatManager_OpenExistingThread_m3241328272 (ChatManager_t2792590695 * __this, ChatThread_t2394323482 * ___thread0, bool ___navBackward1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::ClearActiveChatThred()
extern "C"  void ChatManager_ClearActiveChatThred_m3094033586 (ChatManager_t2792590695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::SendNewMessage(System.String)
extern "C"  void ChatManager_SendNewMessage_m536232167 (ChatManager_t2792590695 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::NewChatMessageUpdate(ChatMessage)
extern "C"  void ChatManager_NewChatMessageUpdate_m2220371599 (ChatManager_t2792590695 * __this, ChatMessage_t2384228687 * ___newMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::NewChatThreadUpdate(ChatThread)
extern "C"  void ChatManager_NewChatThreadUpdate_m3941797947 (ChatManager_t2792590695 * __this, ChatThread_t2394323482 * ___newThread0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::SearchUserContacts(System.String)
extern "C"  void ChatManager_SearchUserContacts_m1289086558 (ChatManager_t2792590695 * __this, String_t* ___searchTerm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::StartList(ChatListType)
extern "C"  void ChatManager_StartList_m1650398138 (ChatManager_t2792590695 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::SetListDrawn(ChatListType)
extern "C"  void ChatManager_SetListDrawn_m947280576 (ChatManager_t2792590695 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::refNativeTextBoxHeight(System.String)
extern "C"  void ChatManager_refNativeTextBoxHeight_m3518258935 (ChatManager_t2792590695 * __this, String_t* ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::refNativeKeyboardHeight(System.String)
extern "C"  void ChatManager_refNativeKeyboardHeight_m3420428634 (ChatManager_t2792590695 * __this, String_t* ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::<OpenExistingThread>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatManager_U3COpenExistingThreadU3Em__0_m3739013585 (ChatManager_t2792590695 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatManager::<OpenExistingThread>m__1(ChatThread)
extern "C"  bool ChatManager_U3COpenExistingThreadU3Em__1_m3314965449 (ChatManager_t2792590695 * __this, ChatThread_t2394323482 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::<OpenExistingThread>m__2(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatManager_U3COpenExistingThreadU3Em__2_m1642186127 (Il2CppObject * __this /* static, unused */, bool ___readSuccess0, Hashtable_t909839986 * ___readInfo1, String_t* ___readErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::<SendNewMessage>m__3(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatManager_U3CSendNewMessageU3Em__3_m3410350288 (ChatManager_t2792590695 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::<SendNewMessage>m__4(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatManager_U3CSendNewMessageU3Em__4_m3444062123 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatManager::<NewChatMessageUpdate>m__5(ChatThread)
extern "C"  bool ChatManager_U3CNewChatMessageUpdateU3Em__5_m2277664034 (ChatManager_t2792590695 * __this, ChatThread_t2394323482 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::<NewChatMessageUpdate>m__6(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatManager_U3CNewChatMessageUpdateU3Em__6_m656052048 (Il2CppObject * __this /* static, unused */, bool ___readSuccess0, Hashtable_t909839986 * ___readInfo1, String_t* ___readErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatManager::<OpenExistingThread>m__7(ChatThread)
extern "C"  bool ChatManager_U3COpenExistingThreadU3Em__7_m2041411603 (ChatManager_t2792590695 * __this, ChatThread_t2394323482 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatManager::<OpenExistingThread>m__8(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatManager_U3COpenExistingThreadU3Em__8_m725486793 (Il2CppObject * __this /* static, unused */, bool ___readSuccess0, Hashtable_t909839986 * ___readInfo1, String_t* ___readErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
