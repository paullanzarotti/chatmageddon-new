﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenScale
struct TweenScale_t2697902175;
// UILabel
struct UILabel_t1795115428;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotEnoughBucksPU
struct  NotEnoughBucksPU_t2637555884  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean NotEnoughBucksPU::puActive
	bool ___puActive_2;
	// TweenScale NotEnoughBucksPU::puTween
	TweenScale_t2697902175 * ___puTween_3;
	// UILabel NotEnoughBucksPU::bucksLabel
	UILabel_t1795115428 * ___bucksLabel_4;
	// System.Boolean NotEnoughBucksPU::puClosing
	bool ___puClosing_5;

public:
	inline static int32_t get_offset_of_puActive_2() { return static_cast<int32_t>(offsetof(NotEnoughBucksPU_t2637555884, ___puActive_2)); }
	inline bool get_puActive_2() const { return ___puActive_2; }
	inline bool* get_address_of_puActive_2() { return &___puActive_2; }
	inline void set_puActive_2(bool value)
	{
		___puActive_2 = value;
	}

	inline static int32_t get_offset_of_puTween_3() { return static_cast<int32_t>(offsetof(NotEnoughBucksPU_t2637555884, ___puTween_3)); }
	inline TweenScale_t2697902175 * get_puTween_3() const { return ___puTween_3; }
	inline TweenScale_t2697902175 ** get_address_of_puTween_3() { return &___puTween_3; }
	inline void set_puTween_3(TweenScale_t2697902175 * value)
	{
		___puTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___puTween_3, value);
	}

	inline static int32_t get_offset_of_bucksLabel_4() { return static_cast<int32_t>(offsetof(NotEnoughBucksPU_t2637555884, ___bucksLabel_4)); }
	inline UILabel_t1795115428 * get_bucksLabel_4() const { return ___bucksLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_bucksLabel_4() { return &___bucksLabel_4; }
	inline void set_bucksLabel_4(UILabel_t1795115428 * value)
	{
		___bucksLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___bucksLabel_4, value);
	}

	inline static int32_t get_offset_of_puClosing_5() { return static_cast<int32_t>(offsetof(NotEnoughBucksPU_t2637555884, ___puClosing_5)); }
	inline bool get_puClosing_5() const { return ___puClosing_5; }
	inline bool* get_address_of_puClosing_5() { return &___puClosing_5; }
	inline void set_puClosing_5(bool value)
	{
		___puClosing_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
