﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// unibill.Dummy.IWindowsIAP
struct IWindowsIAP_t2654797964;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.WP8BillingService
struct  WP8BillingService_t394118855  : public Il2CppObject
{
public:
	// unibill.Dummy.IWindowsIAP Unibill.Impl.WP8BillingService::wp8
	Il2CppObject * ___wp8_0;
	// Unibill.Impl.IBillingServiceCallback Unibill.Impl.WP8BillingService::callback
	Il2CppObject * ___callback_1;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.WP8BillingService::db
	UnibillConfiguration_t2915611853 * ___db_2;
	// TransactionDatabase Unibill.Impl.WP8BillingService::tDb
	TransactionDatabase_t201476183 * ___tDb_3;
	// Unibill.Impl.ProductIdRemapper Unibill.Impl.WP8BillingService::remapper
	ProductIdRemapper_t3313438456 * ___remapper_4;
	// Uniject.ILogger Unibill.Impl.WP8BillingService::logger
	Il2CppObject * ___logger_5;
	// System.Collections.Generic.HashSet`1<System.String> Unibill.Impl.WP8BillingService::unknownProducts
	HashSet_1_t362681087 * ___unknownProducts_6;

public:
	inline static int32_t get_offset_of_wp8_0() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855, ___wp8_0)); }
	inline Il2CppObject * get_wp8_0() const { return ___wp8_0; }
	inline Il2CppObject ** get_address_of_wp8_0() { return &___wp8_0; }
	inline void set_wp8_0(Il2CppObject * value)
	{
		___wp8_0 = value;
		Il2CppCodeGenWriteBarrier(&___wp8_0, value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855, ___callback_1)); }
	inline Il2CppObject * get_callback_1() const { return ___callback_1; }
	inline Il2CppObject ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Il2CppObject * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier(&___callback_1, value);
	}

	inline static int32_t get_offset_of_db_2() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855, ___db_2)); }
	inline UnibillConfiguration_t2915611853 * get_db_2() const { return ___db_2; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_db_2() { return &___db_2; }
	inline void set_db_2(UnibillConfiguration_t2915611853 * value)
	{
		___db_2 = value;
		Il2CppCodeGenWriteBarrier(&___db_2, value);
	}

	inline static int32_t get_offset_of_tDb_3() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855, ___tDb_3)); }
	inline TransactionDatabase_t201476183 * get_tDb_3() const { return ___tDb_3; }
	inline TransactionDatabase_t201476183 ** get_address_of_tDb_3() { return &___tDb_3; }
	inline void set_tDb_3(TransactionDatabase_t201476183 * value)
	{
		___tDb_3 = value;
		Il2CppCodeGenWriteBarrier(&___tDb_3, value);
	}

	inline static int32_t get_offset_of_remapper_4() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855, ___remapper_4)); }
	inline ProductIdRemapper_t3313438456 * get_remapper_4() const { return ___remapper_4; }
	inline ProductIdRemapper_t3313438456 ** get_address_of_remapper_4() { return &___remapper_4; }
	inline void set_remapper_4(ProductIdRemapper_t3313438456 * value)
	{
		___remapper_4 = value;
		Il2CppCodeGenWriteBarrier(&___remapper_4, value);
	}

	inline static int32_t get_offset_of_logger_5() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855, ___logger_5)); }
	inline Il2CppObject * get_logger_5() const { return ___logger_5; }
	inline Il2CppObject ** get_address_of_logger_5() { return &___logger_5; }
	inline void set_logger_5(Il2CppObject * value)
	{
		___logger_5 = value;
		Il2CppCodeGenWriteBarrier(&___logger_5, value);
	}

	inline static int32_t get_offset_of_unknownProducts_6() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855, ___unknownProducts_6)); }
	inline HashSet_1_t362681087 * get_unknownProducts_6() const { return ___unknownProducts_6; }
	inline HashSet_1_t362681087 ** get_address_of_unknownProducts_6() { return &___unknownProducts_6; }
	inline void set_unknownProducts_6(HashSet_1_t362681087 * value)
	{
		___unknownProducts_6 = value;
		Il2CppCodeGenWriteBarrier(&___unknownProducts_6, value);
	}
};

struct WP8BillingService_t394118855_StaticFields
{
public:
	// System.Int32 Unibill.Impl.WP8BillingService::count
	int32_t ___count_7;

public:
	inline static int32_t get_offset_of_count_7() { return static_cast<int32_t>(offsetof(WP8BillingService_t394118855_StaticFields, ___count_7)); }
	inline int32_t get_count_7() const { return ___count_7; }
	inline int32_t* get_address_of_count_7() { return &___count_7; }
	inline void set_count_7(int32_t value)
	{
		___count_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
