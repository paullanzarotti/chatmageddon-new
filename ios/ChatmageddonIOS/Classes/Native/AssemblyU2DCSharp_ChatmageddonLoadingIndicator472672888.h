﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// TweenRotation
struct TweenRotation_t1747194511;
// TweenScale
struct TweenScale_t2697902175;

#include "AssemblyU2DCSharp_LoadingIndicator3396405409.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonLoadingIndicator
struct  ChatmageddonLoadingIndicator_t472672888  : public LoadingIndicator_t3396405409
{
public:
	// UITexture ChatmageddonLoadingIndicator::indicatorImage
	UITexture_t2537039969 * ___indicatorImage_5;
	// TweenRotation ChatmageddonLoadingIndicator::rotationTween
	TweenRotation_t1747194511 * ___rotationTween_6;
	// TweenScale ChatmageddonLoadingIndicator::scaleTween
	TweenScale_t2697902175 * ___scaleTween_7;
	// System.Boolean ChatmageddonLoadingIndicator::closing
	bool ___closing_8;

public:
	inline static int32_t get_offset_of_indicatorImage_5() { return static_cast<int32_t>(offsetof(ChatmageddonLoadingIndicator_t472672888, ___indicatorImage_5)); }
	inline UITexture_t2537039969 * get_indicatorImage_5() const { return ___indicatorImage_5; }
	inline UITexture_t2537039969 ** get_address_of_indicatorImage_5() { return &___indicatorImage_5; }
	inline void set_indicatorImage_5(UITexture_t2537039969 * value)
	{
		___indicatorImage_5 = value;
		Il2CppCodeGenWriteBarrier(&___indicatorImage_5, value);
	}

	inline static int32_t get_offset_of_rotationTween_6() { return static_cast<int32_t>(offsetof(ChatmageddonLoadingIndicator_t472672888, ___rotationTween_6)); }
	inline TweenRotation_t1747194511 * get_rotationTween_6() const { return ___rotationTween_6; }
	inline TweenRotation_t1747194511 ** get_address_of_rotationTween_6() { return &___rotationTween_6; }
	inline void set_rotationTween_6(TweenRotation_t1747194511 * value)
	{
		___rotationTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___rotationTween_6, value);
	}

	inline static int32_t get_offset_of_scaleTween_7() { return static_cast<int32_t>(offsetof(ChatmageddonLoadingIndicator_t472672888, ___scaleTween_7)); }
	inline TweenScale_t2697902175 * get_scaleTween_7() const { return ___scaleTween_7; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_7() { return &___scaleTween_7; }
	inline void set_scaleTween_7(TweenScale_t2697902175 * value)
	{
		___scaleTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_7, value);
	}

	inline static int32_t get_offset_of_closing_8() { return static_cast<int32_t>(offsetof(ChatmageddonLoadingIndicator_t472672888, ___closing_8)); }
	inline bool get_closing_8() const { return ___closing_8; }
	inline bool* get_address_of_closing_8() { return &___closing_8; }
	inline void set_closing_8(bool value)
	{
		___closing_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
