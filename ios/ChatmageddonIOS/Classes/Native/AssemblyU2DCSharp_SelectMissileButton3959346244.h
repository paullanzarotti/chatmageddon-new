﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AttackInventoryNavScreen
struct AttackInventoryNavScreen_t33214553;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectMissileButton
struct  SelectMissileButton_t3959346244  : public SFXButton_t792651341
{
public:
	// AttackInventoryNavScreen SelectMissileButton::screen
	AttackInventoryNavScreen_t33214553 * ___screen_5;
	// UnityEngine.Color SelectMissileButton::activeColour
	Color_t2020392075  ___activeColour_6;
	// UnityEngine.Color SelectMissileButton::lockedColour
	Color_t2020392075  ___lockedColour_7;
	// UnityEngine.Color SelectMissileButton::inactiveColour
	Color_t2020392075  ___inactiveColour_8;
	// UILabel SelectMissileButton::selectLabel
	UILabel_t1795115428 * ___selectLabel_9;
	// System.Boolean SelectMissileButton::buyFuel
	bool ___buyFuel_10;

public:
	inline static int32_t get_offset_of_screen_5() { return static_cast<int32_t>(offsetof(SelectMissileButton_t3959346244, ___screen_5)); }
	inline AttackInventoryNavScreen_t33214553 * get_screen_5() const { return ___screen_5; }
	inline AttackInventoryNavScreen_t33214553 ** get_address_of_screen_5() { return &___screen_5; }
	inline void set_screen_5(AttackInventoryNavScreen_t33214553 * value)
	{
		___screen_5 = value;
		Il2CppCodeGenWriteBarrier(&___screen_5, value);
	}

	inline static int32_t get_offset_of_activeColour_6() { return static_cast<int32_t>(offsetof(SelectMissileButton_t3959346244, ___activeColour_6)); }
	inline Color_t2020392075  get_activeColour_6() const { return ___activeColour_6; }
	inline Color_t2020392075 * get_address_of_activeColour_6() { return &___activeColour_6; }
	inline void set_activeColour_6(Color_t2020392075  value)
	{
		___activeColour_6 = value;
	}

	inline static int32_t get_offset_of_lockedColour_7() { return static_cast<int32_t>(offsetof(SelectMissileButton_t3959346244, ___lockedColour_7)); }
	inline Color_t2020392075  get_lockedColour_7() const { return ___lockedColour_7; }
	inline Color_t2020392075 * get_address_of_lockedColour_7() { return &___lockedColour_7; }
	inline void set_lockedColour_7(Color_t2020392075  value)
	{
		___lockedColour_7 = value;
	}

	inline static int32_t get_offset_of_inactiveColour_8() { return static_cast<int32_t>(offsetof(SelectMissileButton_t3959346244, ___inactiveColour_8)); }
	inline Color_t2020392075  get_inactiveColour_8() const { return ___inactiveColour_8; }
	inline Color_t2020392075 * get_address_of_inactiveColour_8() { return &___inactiveColour_8; }
	inline void set_inactiveColour_8(Color_t2020392075  value)
	{
		___inactiveColour_8 = value;
	}

	inline static int32_t get_offset_of_selectLabel_9() { return static_cast<int32_t>(offsetof(SelectMissileButton_t3959346244, ___selectLabel_9)); }
	inline UILabel_t1795115428 * get_selectLabel_9() const { return ___selectLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_selectLabel_9() { return &___selectLabel_9; }
	inline void set_selectLabel_9(UILabel_t1795115428 * value)
	{
		___selectLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___selectLabel_9, value);
	}

	inline static int32_t get_offset_of_buyFuel_10() { return static_cast<int32_t>(offsetof(SelectMissileButton_t3959346244, ___buyFuel_10)); }
	inline bool get_buyFuel_10() const { return ___buyFuel_10; }
	inline bool* get_address_of_buyFuel_10() { return &___buyFuel_10; }
	inline void set_buyFuel_10(bool value)
	{
		___buyFuel_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
