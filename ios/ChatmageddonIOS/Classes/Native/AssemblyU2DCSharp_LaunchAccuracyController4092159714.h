﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// TweenPosition
struct TweenPosition_t1144714832;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchAccuracyController
struct  LaunchAccuracyController_t4092159714  : public MonoBehaviour_t1158329972
{
public:
	// UILabel LaunchAccuracyController::accuracyLabel
	UILabel_t1795115428 * ___accuracyLabel_2;
	// TweenPosition LaunchAccuracyController::posTween
	TweenPosition_t1144714832 * ___posTween_3;
	// UnityEngine.Coroutine LaunchAccuracyController::flash
	Coroutine_t2299508840 * ___flash_4;

public:
	inline static int32_t get_offset_of_accuracyLabel_2() { return static_cast<int32_t>(offsetof(LaunchAccuracyController_t4092159714, ___accuracyLabel_2)); }
	inline UILabel_t1795115428 * get_accuracyLabel_2() const { return ___accuracyLabel_2; }
	inline UILabel_t1795115428 ** get_address_of_accuracyLabel_2() { return &___accuracyLabel_2; }
	inline void set_accuracyLabel_2(UILabel_t1795115428 * value)
	{
		___accuracyLabel_2 = value;
		Il2CppCodeGenWriteBarrier(&___accuracyLabel_2, value);
	}

	inline static int32_t get_offset_of_posTween_3() { return static_cast<int32_t>(offsetof(LaunchAccuracyController_t4092159714, ___posTween_3)); }
	inline TweenPosition_t1144714832 * get_posTween_3() const { return ___posTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_3() { return &___posTween_3; }
	inline void set_posTween_3(TweenPosition_t1144714832 * value)
	{
		___posTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_3, value);
	}

	inline static int32_t get_offset_of_flash_4() { return static_cast<int32_t>(offsetof(LaunchAccuracyController_t4092159714, ___flash_4)); }
	inline Coroutine_t2299508840 * get_flash_4() const { return ___flash_4; }
	inline Coroutine_t2299508840 ** get_address_of_flash_4() { return &___flash_4; }
	inline void set_flash_4(Coroutine_t2299508840 * value)
	{
		___flash_4 = value;
		Il2CppCodeGenWriteBarrier(&___flash_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
