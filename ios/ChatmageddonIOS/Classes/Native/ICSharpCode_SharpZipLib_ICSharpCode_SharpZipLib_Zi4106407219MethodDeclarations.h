﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.DescriptorData
struct DescriptorData_t4106407219;

#include "codegen/il2cpp-codegen.h"

// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_CompressedSize()
extern "C"  int64_t DescriptorData_get_CompressedSize_m3383523441 (DescriptorData_t4106407219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_CompressedSize(System.Int64)
extern "C"  void DescriptorData_set_CompressedSize_m3229241118 (DescriptorData_t4106407219 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_Size()
extern "C"  int64_t DescriptorData_get_Size_m1456873818 (DescriptorData_t4106407219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_Size(System.Int64)
extern "C"  void DescriptorData_set_Size_m2586613261 (DescriptorData_t4106407219 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_Crc()
extern "C"  int64_t DescriptorData_get_Crc_m1588606453 (DescriptorData_t4106407219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_Crc(System.Int64)
extern "C"  void DescriptorData_set_Crc_m438507950 (DescriptorData_t4106407219 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::.ctor()
extern "C"  void DescriptorData__ctor_m854587251 (DescriptorData_t4106407219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
