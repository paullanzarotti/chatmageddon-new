﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackHomeWorldNavScreen
struct AttackHomeWorldNavScreen_t3820258470;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void AttackHomeWorldNavScreen::.ctor()
extern "C"  void AttackHomeWorldNavScreen__ctor_m236155087 (AttackHomeWorldNavScreen_t3820258470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeWorldNavScreen::Start()
extern "C"  void AttackHomeWorldNavScreen_Start_m276675187 (AttackHomeWorldNavScreen_t3820258470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeWorldNavScreen::UIClosing()
extern "C"  void AttackHomeWorldNavScreen_UIClosing_m3766820920 (AttackHomeWorldNavScreen_t3820258470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeWorldNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void AttackHomeWorldNavScreen_ScreenLoad_m4024205973 (AttackHomeWorldNavScreen_t3820258470 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackHomeWorldNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void AttackHomeWorldNavScreen_ScreenUnload_m1631006945 (AttackHomeWorldNavScreen_t3820258470 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackHomeWorldNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool AttackHomeWorldNavScreen_ValidateScreenNavigation_m2667970882 (AttackHomeWorldNavScreen_t3820258470 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
