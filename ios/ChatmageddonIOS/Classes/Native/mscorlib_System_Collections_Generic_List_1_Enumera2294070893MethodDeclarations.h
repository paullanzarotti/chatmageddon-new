﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorLine>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3048017448(__this, ___l0, method) ((  void (*) (Enumerator_t2294070893 *, List_1_t2759341219 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorLine>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3269484762(__this, method) ((  void (*) (Enumerator_t2294070893 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorLine>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m429948434(__this, method) ((  Il2CppObject * (*) (Enumerator_t2294070893 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorLine>::Dispose()
#define Enumerator_Dispose_m2298494405(__this, method) ((  void (*) (Enumerator_t2294070893 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorLine>::VerifyState()
#define Enumerator_VerifyState_m2056776606(__this, method) ((  void (*) (Enumerator_t2294070893 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorLine>::MoveNext()
#define Enumerator_MoveNext_m733857038(__this, method) ((  bool (*) (Enumerator_t2294070893 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vectrosity.VectorLine>::get_Current()
#define Enumerator_get_Current_m2924237517(__this, method) ((  VectorLine_t3390220087 * (*) (Enumerator_t2294070893 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
