﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsManager
struct SettingsManager_t2519859232;
// System.String
struct String_t;
// FAQ
struct FAQ_t2837997648;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AboutAndHelpType4001655409.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_FAQ2837997648.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void SettingsManager::.ctor()
extern "C"  void SettingsManager__ctor_m1130914037 (SettingsManager_t2519859232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::Init()
extern "C"  void SettingsManager_Init_m1382678639 (SettingsManager_t2519859232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::GetAboutAndHelpInformation(AboutAndHelpType)
extern "C"  void SettingsManager_GetAboutAndHelpInformation_m2214210231 (SettingsManager_t2519859232 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::SetInfoContent(AboutAndHelpType,System.String)
extern "C"  void SettingsManager_SetInfoContent_m1734502129 (SettingsManager_t2519859232 * __this, int32_t ___type0, String_t* ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::SetFAQText(FAQ)
extern "C"  void SettingsManager_SetFAQText_m4203535442 (SettingsManager_t2519859232 * __this, FAQ_t2837997648 * ___faq0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::PopulateProfileTexture(UnityEngine.Texture)
extern "C"  void SettingsManager_PopulateProfileTexture_m2888123311 (SettingsManager_t2519859232 * __this, Texture_t2243626319 * ___profileImage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::PopulateBlockedList()
extern "C"  void SettingsManager_PopulateBlockedList_m750901757 (SettingsManager_t2519859232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsManager::<PopulateProfileTexture>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void SettingsManager_U3CPopulateProfileTextureU3Em__0_m686817665 (Il2CppObject * __this /* static, unused */, bool ___imageSuccess0, Hashtable_t909839986 * ___info1, String_t* ___imagErrorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
