﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3464988659MethodDeclarations.h"

// System.Void BaseInfiniteListItem`1<ChatMessageListItem>::.ctor()
#define BaseInfiniteListItem_1__ctor_m1056541674(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3200590574 *, const MethodInfo*))BaseInfiniteListItem_1__ctor_m451819470_gshared)(__this, method)
// System.Boolean BaseInfiniteListItem`1<ChatMessageListItem>::verifyVisibility()
#define BaseInfiniteListItem_1_verifyVisibility_m2834163687(__this, method) ((  bool (*) (BaseInfiniteListItem_1_t3200590574 *, const MethodInfo*))BaseInfiniteListItem_1_verifyVisibility_m1990849747_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatMessageListItem>::InitItem()
#define BaseInfiniteListItem_1_InitItem_m3139331329(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3200590574 *, const MethodInfo*))BaseInfiniteListItem_1_InitItem_m2730396605_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatMessageListItem>::SetUpItem()
#define BaseInfiniteListItem_1_SetUpItem_m2923365084(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3200590574 *, const MethodInfo*))BaseInfiniteListItem_1_SetUpItem_m1802181512_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatMessageListItem>::SetItemSelected(System.Boolean)
#define BaseInfiniteListItem_1_SetItemSelected_m3162422883(__this, ___selected0, method) ((  void (*) (BaseInfiniteListItem_1_t3200590574 *, bool, const MethodInfo*))BaseInfiniteListItem_1_SetItemSelected_m718662847_gshared)(__this, ___selected0, method)
// System.Void BaseInfiniteListItem`1<ChatMessageListItem>::SetVisible()
#define BaseInfiniteListItem_1_SetVisible_m3826883914(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3200590574 *, const MethodInfo*))BaseInfiniteListItem_1_SetVisible_m2310471774_gshared)(__this, method)
// System.Void BaseInfiniteListItem`1<ChatMessageListItem>::SetInvisible()
#define BaseInfiniteListItem_1_SetInvisible_m3058020561(__this, method) ((  void (*) (BaseInfiniteListItem_1_t3200590574 *, const MethodInfo*))BaseInfiniteListItem_1_SetInvisible_m1500163933_gshared)(__this, method)
