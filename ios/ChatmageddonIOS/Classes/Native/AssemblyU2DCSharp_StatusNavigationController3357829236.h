﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenColor
struct TweenColor_t3390486518;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_NavigationController_2_gen1214554782.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusNavigationController
struct  StatusNavigationController_t3357829236  : public NavigationController_2_t1214554782
{
public:
	// TweenColor StatusNavigationController::incomingTween
	TweenColor_t3390486518 * ___incomingTween_8;
	// TweenColor StatusNavigationController::outgoingTween
	TweenColor_t3390486518 * ___outgoingTween_9;
	// UnityEngine.GameObject StatusNavigationController::incomingObject
	GameObject_t1756533147 * ___incomingObject_10;
	// UnityEngine.GameObject StatusNavigationController::outgoingObject
	GameObject_t1756533147 * ___outgoingObject_11;
	// UnityEngine.Vector3 StatusNavigationController::activePos
	Vector3_t2243707580  ___activePos_12;
	// UnityEngine.Vector3 StatusNavigationController::leftInactivePos
	Vector3_t2243707580  ___leftInactivePos_13;
	// UnityEngine.Vector3 StatusNavigationController::rightInactivePos
	Vector3_t2243707580  ___rightInactivePos_14;

public:
	inline static int32_t get_offset_of_incomingTween_8() { return static_cast<int32_t>(offsetof(StatusNavigationController_t3357829236, ___incomingTween_8)); }
	inline TweenColor_t3390486518 * get_incomingTween_8() const { return ___incomingTween_8; }
	inline TweenColor_t3390486518 ** get_address_of_incomingTween_8() { return &___incomingTween_8; }
	inline void set_incomingTween_8(TweenColor_t3390486518 * value)
	{
		___incomingTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___incomingTween_8, value);
	}

	inline static int32_t get_offset_of_outgoingTween_9() { return static_cast<int32_t>(offsetof(StatusNavigationController_t3357829236, ___outgoingTween_9)); }
	inline TweenColor_t3390486518 * get_outgoingTween_9() const { return ___outgoingTween_9; }
	inline TweenColor_t3390486518 ** get_address_of_outgoingTween_9() { return &___outgoingTween_9; }
	inline void set_outgoingTween_9(TweenColor_t3390486518 * value)
	{
		___outgoingTween_9 = value;
		Il2CppCodeGenWriteBarrier(&___outgoingTween_9, value);
	}

	inline static int32_t get_offset_of_incomingObject_10() { return static_cast<int32_t>(offsetof(StatusNavigationController_t3357829236, ___incomingObject_10)); }
	inline GameObject_t1756533147 * get_incomingObject_10() const { return ___incomingObject_10; }
	inline GameObject_t1756533147 ** get_address_of_incomingObject_10() { return &___incomingObject_10; }
	inline void set_incomingObject_10(GameObject_t1756533147 * value)
	{
		___incomingObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___incomingObject_10, value);
	}

	inline static int32_t get_offset_of_outgoingObject_11() { return static_cast<int32_t>(offsetof(StatusNavigationController_t3357829236, ___outgoingObject_11)); }
	inline GameObject_t1756533147 * get_outgoingObject_11() const { return ___outgoingObject_11; }
	inline GameObject_t1756533147 ** get_address_of_outgoingObject_11() { return &___outgoingObject_11; }
	inline void set_outgoingObject_11(GameObject_t1756533147 * value)
	{
		___outgoingObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___outgoingObject_11, value);
	}

	inline static int32_t get_offset_of_activePos_12() { return static_cast<int32_t>(offsetof(StatusNavigationController_t3357829236, ___activePos_12)); }
	inline Vector3_t2243707580  get_activePos_12() const { return ___activePos_12; }
	inline Vector3_t2243707580 * get_address_of_activePos_12() { return &___activePos_12; }
	inline void set_activePos_12(Vector3_t2243707580  value)
	{
		___activePos_12 = value;
	}

	inline static int32_t get_offset_of_leftInactivePos_13() { return static_cast<int32_t>(offsetof(StatusNavigationController_t3357829236, ___leftInactivePos_13)); }
	inline Vector3_t2243707580  get_leftInactivePos_13() const { return ___leftInactivePos_13; }
	inline Vector3_t2243707580 * get_address_of_leftInactivePos_13() { return &___leftInactivePos_13; }
	inline void set_leftInactivePos_13(Vector3_t2243707580  value)
	{
		___leftInactivePos_13 = value;
	}

	inline static int32_t get_offset_of_rightInactivePos_14() { return static_cast<int32_t>(offsetof(StatusNavigationController_t3357829236, ___rightInactivePos_14)); }
	inline Vector3_t2243707580  get_rightInactivePos_14() const { return ___rightInactivePos_14; }
	inline Vector3_t2243707580 * get_address_of_rightInactivePos_14() { return &___rightInactivePos_14; }
	inline void set_rightInactivePos_14(Vector3_t2243707580  value)
	{
		___rightInactivePos_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
