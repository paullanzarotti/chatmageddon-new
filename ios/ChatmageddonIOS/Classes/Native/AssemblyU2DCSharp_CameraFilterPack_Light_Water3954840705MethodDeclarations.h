﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Light_Water
struct CameraFilterPack_Light_Water_t3954840705;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Light_Water::.ctor()
extern "C"  void CameraFilterPack_Light_Water__ctor_m2660540894 (CameraFilterPack_Light_Water_t3954840705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Water::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Light_Water_get_material_m4053409153 (CameraFilterPack_Light_Water_t3954840705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::Start()
extern "C"  void CameraFilterPack_Light_Water_Start_m1469037838 (CameraFilterPack_Light_Water_t3954840705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Light_Water_OnRenderImage_m2432605574 (CameraFilterPack_Light_Water_t3954840705 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::OnValidate()
extern "C"  void CameraFilterPack_Light_Water_OnValidate_m3130011039 (CameraFilterPack_Light_Water_t3954840705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::Update()
extern "C"  void CameraFilterPack_Light_Water_Update_m1209704421 (CameraFilterPack_Light_Water_t3954840705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::OnDisable()
extern "C"  void CameraFilterPack_Light_Water_OnDisable_m3334552849 (CameraFilterPack_Light_Water_t3954840705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
