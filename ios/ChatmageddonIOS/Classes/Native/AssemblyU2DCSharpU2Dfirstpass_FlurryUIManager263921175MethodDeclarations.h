﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryUIManager
struct FlurryUIManager_t263921175;

#include "codegen/il2cpp-codegen.h"

// System.Void FlurryUIManager::.ctor()
extern "C"  void FlurryUIManager__ctor_m4271930354 (FlurryUIManager_t263921175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
