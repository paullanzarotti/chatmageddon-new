﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.MiniJsonExtensions::getHash(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  Dictionary_2_t309261261 * MiniJsonExtensions_getHash_m1106852940 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.MiniJsonExtensions::getString(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,System.String)
extern "C"  String_t* MiniJsonExtensions_getString_m1427211852 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, String_t* ___defaultValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Unibill.Impl.MiniJsonExtensions::getLong(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  int64_t MiniJsonExtensions_getLong_m2078791973 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Unibill.Impl.MiniJsonExtensions::getStringList(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  List_1_t1398341365 * MiniJsonExtensions_getStringList_m2968756258 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.MiniJsonExtensions::getBool(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  bool MiniJsonExtensions_getBool_m676421558 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dic0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  String_t* MiniJsonExtensions_toJson_m2010611320 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  String_t* MiniJsonExtensions_toJson_m1069209150 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3943999495 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Unibill.Impl.MiniJsonExtensions::arrayListFromJson(System.String)
extern "C"  List_1_t2058570427 * MiniJsonExtensions_arrayListFromJson_m744248409 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Unibill.Impl.MiniJsonExtensions::hashtableFromJson(System.String)
extern "C"  Dictionary_2_t309261261 * MiniJsonExtensions_hashtableFromJson_m3069796185 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
