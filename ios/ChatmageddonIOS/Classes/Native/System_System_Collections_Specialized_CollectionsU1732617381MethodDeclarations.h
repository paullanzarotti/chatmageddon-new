﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable System.Collections.Specialized.CollectionsUtil::CreateCaseInsensitiveHashtable()
extern "C"  Hashtable_t909839986 * CollectionsUtil_CreateCaseInsensitiveHashtable_m506153308 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
