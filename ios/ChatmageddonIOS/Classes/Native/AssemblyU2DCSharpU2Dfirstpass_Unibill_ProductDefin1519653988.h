﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseType1639241305.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.ProductDefinition
struct  ProductDefinition_t1519653988  : public Il2CppObject
{
public:
	// System.String Unibill.ProductDefinition::<PlatformSpecificId>k__BackingField
	String_t* ___U3CPlatformSpecificIdU3Ek__BackingField_0;
	// PurchaseType Unibill.ProductDefinition::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPlatformSpecificIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductDefinition_t1519653988, ___U3CPlatformSpecificIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CPlatformSpecificIdU3Ek__BackingField_0() const { return ___U3CPlatformSpecificIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPlatformSpecificIdU3Ek__BackingField_0() { return &___U3CPlatformSpecificIdU3Ek__BackingField_0; }
	inline void set_U3CPlatformSpecificIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CPlatformSpecificIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlatformSpecificIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductDefinition_t1519653988, ___U3CTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
