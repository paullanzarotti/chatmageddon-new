﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIBasicSprite
struct UIBasicSprite_t754925213;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t2464096222;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t2464096221;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t1094906160;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type545852334.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Flip3496710405.h"
#include "AssemblyU2DCSharp_UIBasicSprite_FillDirection3739624076.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void UIBasicSprite::.ctor()
extern "C"  void UIBasicSprite__ctor_m988936896 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/Type UIBasicSprite::get_type()
extern "C"  int32_t UIBasicSprite_get_type_m3832434424 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_type(UIBasicSprite/Type)
extern "C"  void UIBasicSprite_set_type_m1560639861 (UIBasicSprite_t754925213 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/Flip UIBasicSprite::get_flip()
extern "C"  int32_t UIBasicSprite_get_flip_m4061705656 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_flip(UIBasicSprite/Flip)
extern "C"  void UIBasicSprite_set_flip_m3797907693 (UIBasicSprite_t754925213 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIBasicSprite/FillDirection UIBasicSprite::get_fillDirection()
extern "C"  int32_t UIBasicSprite_get_fillDirection_m1223482816 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_fillDirection(UIBasicSprite/FillDirection)
extern "C"  void UIBasicSprite_set_fillDirection_m1396037461 (UIBasicSprite_t754925213 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSprite::get_fillAmount()
extern "C"  float UIBasicSprite_get_fillAmount_m1689721812 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_fillAmount(System.Single)
extern "C"  void UIBasicSprite_set_fillAmount_m1708931779 (UIBasicSprite_t754925213 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSprite::get_minWidth()
extern "C"  int32_t UIBasicSprite_get_minWidth_m1816545723 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIBasicSprite::get_minHeight()
extern "C"  int32_t UIBasicSprite_get_minHeight_m1964615190 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::get_invert()
extern "C"  bool UIBasicSprite_get_invert_m171634051 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::set_invert(System.Boolean)
extern "C"  void UIBasicSprite_set_invert_m1520386098 (UIBasicSprite_t754925213 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::get_hasBorder()
extern "C"  bool UIBasicSprite_get_hasBorder_m2090956719 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::get_premultipliedAlpha()
extern "C"  bool UIBasicSprite_get_premultipliedAlpha_m4141291143 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIBasicSprite::get_pixelSize()
extern "C"  float UIBasicSprite_get_pixelSize_m1849199916 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIBasicSprite::get_drawingUVs()
extern "C"  Vector4_t2243707581  UIBasicSprite_get_drawingUVs_m3268103394 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UIBasicSprite::get_drawingColor()
extern "C"  Color32_t874517518  UIBasicSprite_get_drawingColor_m1646878906 (UIBasicSprite_t754925213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::Fill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,UnityEngine.Rect,UnityEngine.Rect)
extern "C"  void UIBasicSprite_Fill_m4201065426 (UIBasicSprite_t754925213 * __this, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, Rect_t3681755626  ___outer3, Rect_t3681755626  ___inner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::SimpleFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_SimpleFill_m2236244758 (UIBasicSprite_t754925213 * __this, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::SlicedFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_SlicedFill_m72805126 (UIBasicSprite_t754925213 * __this, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::TiledFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_TiledFill_m1194575478 (UIBasicSprite_t754925213 * __this, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::FilledFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_FilledFill_m1866859340 (UIBasicSprite_t754925213 * __this, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::AdvancedFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIBasicSprite_AdvancedFill_m3899268750 (UIBasicSprite_t754925213 * __this, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIBasicSprite::RadialCut(UnityEngine.Vector2[],UnityEngine.Vector2[],System.Single,System.Boolean,System.Int32)
extern "C"  bool UIBasicSprite_RadialCut_m1863652426 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___xy0, Vector2U5BU5D_t686124026* ___uv1, float ___fill2, bool ___invert3, int32_t ___corner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::RadialCut(UnityEngine.Vector2[],System.Single,System.Single,System.Boolean,System.Int32)
extern "C"  void UIBasicSprite_RadialCut_m3493387829 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___xy0, float ___cos1, float ___sin2, bool ___invert3, int32_t ___corner4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::Fill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Color)
extern "C"  void UIBasicSprite_Fill_m2551012670 (Il2CppObject * __this /* static, unused */, BetterList_1_t2464096222 * ___verts0, BetterList_1_t2464096221 * ___uvs1, BetterList_1_t1094906160 * ___cols2, float ___v0x3, float ___v1x4, float ___v0y5, float ___v1y6, float ___u0x7, float ___u1x8, float ___u0y9, float ___u1y10, Color_t2020392075  ___col11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIBasicSprite::.cctor()
extern "C"  void UIBasicSprite__cctor_m244319631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
