﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PurchaseableItem
struct PurchaseableItem_t3351122996;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DCSharp_CostType3548590213.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"

// System.Void PurchaseableItem::.ctor(System.Collections.Hashtable)
extern "C"  void PurchaseableItem__ctor_m1625327979 (PurchaseableItem_t3351122996 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CostType PurchaseableItem::GetCostTypeFromString(System.String)
extern "C"  int32_t PurchaseableItem_GetCostTypeFromString_m3086523671 (PurchaseableItem_t3351122996 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchaseableCategory PurchaseableItem::GetCategoryFromString(System.String)
extern "C"  int32_t PurchaseableItem_GetCategoryFromString_m1666562602 (PurchaseableItem_t3351122996 * __this, String_t* ___String0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
