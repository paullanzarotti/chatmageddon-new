﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchRoutine
struct LaunchRoutine_t3791963153;
// LaunchAnimationController
struct LaunchAnimationController_t825305345;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchAnimationController825305345.h"

// System.Void LaunchRoutine::.ctor()
extern "C"  void LaunchRoutine__ctor_m3296671648 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::Start()
extern "C"  void LaunchRoutine_Start_m3548356056 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::OnEnable()
extern "C"  void LaunchRoutine_OnEnable_m2026315352 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::OnDisable()
extern "C"  void LaunchRoutine_OnDisable_m4179013465 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::SetAnimController(LaunchAnimationController)
extern "C"  void LaunchRoutine_SetAnimController_m3727138144 (LaunchRoutine_t3791963153 * __this, LaunchAnimationController_t825305345 * ___controller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::StartBackfireRoutine()
extern "C"  void LaunchRoutine_StartBackfireRoutine_m4203426057 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::CloseBackfireRoutine()
extern "C"  void LaunchRoutine_CloseBackfireRoutine_m547467359 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::StartLaunchRoutine()
extern "C"  void LaunchRoutine_StartLaunchRoutine_m3975509639 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::CloseLaunchSequence()
extern "C"  void LaunchRoutine_CloseLaunchSequence_m39044438 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::StartScreenInfoPopulation()
extern "C"  void LaunchRoutine_StartScreenInfoPopulation_m2306241741 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::ScreenInfoComplete()
extern "C"  void LaunchRoutine_ScreenInfoComplete_m343306155 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::PlayOverlayTween(System.Boolean)
extern "C"  void LaunchRoutine_PlayOverlayTween_m1511917474 (LaunchRoutine_t3791963153 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::OverlayTweenFinished()
extern "C"  void LaunchRoutine_OverlayTweenFinished_m4130780197 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::StartSuccessInfoPopulation()
extern "C"  void LaunchRoutine_StartSuccessInfoPopulation_m235393590 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::PlayTVEffect(System.Boolean)
extern "C"  void LaunchRoutine_PlayTVEffect_m1412006494 (LaunchRoutine_t3791963153 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::AnimRenderTweenFinished()
extern "C"  void LaunchRoutine_AnimRenderTweenFinished_m4152507006 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::PlayStep1()
extern "C"  void LaunchRoutine_PlayStep1_m1791726487 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchRoutine::reverseAnimWait()
extern "C"  Il2CppObject * LaunchRoutine_reverseAnimWait_m1446903514 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchRoutine::Step2AnimRenderTween()
extern "C"  Il2CppObject * LaunchRoutine_Step2AnimRenderTween_m1855485734 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchRoutine::FinishAnimRenderTween()
extern "C"  Il2CppObject * LaunchRoutine_FinishAnimRenderTween_m1333618201 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::CountdownTimerComplete()
extern "C"  void LaunchRoutine_CountdownTimerComplete_m309501575 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchRoutine::WaitForAnimEnd()
extern "C"  Il2CppObject * LaunchRoutine_WaitForAnimEnd_m1074683316 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchRoutine::TimedGlitchEffect()
extern "C"  Il2CppObject * LaunchRoutine_TimedGlitchEffect_m1724179213 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::SuccessTweenFinished()
extern "C"  void LaunchRoutine_SuccessTweenFinished_m3466168738 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaunchRoutine::WaitToTransitionScene(System.Single)
extern "C"  Il2CppObject * LaunchRoutine_WaitToTransitionScene_m159350698 (LaunchRoutine_t3791963153 * __this, float ___waitTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine::TransitionScene()
extern "C"  void LaunchRoutine_TransitionScene_m282891289 (LaunchRoutine_t3791963153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
