﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<PhoneNumberNavScreen>
struct DefaultComparer_t4047704779;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<PhoneNumberNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m2642078610_gshared (DefaultComparer_t4047704779 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2642078610(__this, method) ((  void (*) (DefaultComparer_t4047704779 *, const MethodInfo*))DefaultComparer__ctor_m2642078610_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<PhoneNumberNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2525076531_gshared (DefaultComparer_t4047704779 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2525076531(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t4047704779 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m2525076531_gshared)(__this, ___x0, ___y1, method)
