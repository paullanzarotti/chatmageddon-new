﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisplayPickerSprite
struct DisplayPickerSprite_t2709899627;

#include "codegen/il2cpp-codegen.h"

// System.Void DisplayPickerSprite::.ctor()
extern "C"  void DisplayPickerSprite__ctor_m3517089408 (DisplayPickerSprite_t2709899627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisplayPickerSprite::Awake()
extern "C"  void DisplayPickerSprite_Awake_m3720036999 (DisplayPickerSprite_t2709899627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisplayPickerSprite::DisplaySprite()
extern "C"  void DisplayPickerSprite_DisplaySprite_m3046936409 (DisplayPickerSprite_t2709899627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
