﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Object
struct Il2CppObject;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoCtrl
struct  DemoCtrl_t4011351802  : public MonoBehaviour_t1158329972
{
public:
	// System.String DemoCtrl::pathSample1
	String_t* ___pathSample1_2;
	// System.String DemoCtrl::pathSample2
	String_t* ___pathSample2_3;
	// System.String DemoCtrl::pathSample3
	String_t* ___pathSample3_4;
	// System.String DemoCtrl::downloadedPath
	String_t* ___downloadedPath_5;
	// System.String DemoCtrl::url
	String_t* ___url_6;
	// System.String[] DemoCtrl::path
	StringU5BU5D_t1642385972* ___path_7;
	// System.String DemoCtrl::findKey
	String_t* ___findKey_8;
	// System.String DemoCtrl::findValue
	String_t* ___findValue_9;
	// UnityEngine.Vector2 DemoCtrl::scrollPosition
	Vector2_t2243707579  ___scrollPosition_10;
	// UnityEngine.Vector2 DemoCtrl::scrollPositionLog
	Vector2_t2243707579  ___scrollPositionLog_11;
	// System.String DemoCtrl::logMsgs
	String_t* ___logMsgs_12;
	// System.String DemoCtrl::jsonString
	String_t* ___jsonString_13;
	// System.String DemoCtrl::jsonLogString
	String_t* ___jsonLogString_14;
	// System.Collections.Hashtable DemoCtrl::jsonNode
	Hashtable_t909839986 * ___jsonNode_15;
	// System.Object DemoCtrl::selectedNode
	Il2CppObject * ___selectedNode_16;
	// UnityEngine.Texture2D DemoCtrl::bg
	Texture2D_t3542995729 * ___bg_17;
	// System.Boolean DemoCtrl::caseInsensitive
	bool ___caseInsensitive_18;
	// System.Int32 DemoCtrl::screenshotCounter
	int32_t ___screenshotCounter_19;

public:
	inline static int32_t get_offset_of_pathSample1_2() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___pathSample1_2)); }
	inline String_t* get_pathSample1_2() const { return ___pathSample1_2; }
	inline String_t** get_address_of_pathSample1_2() { return &___pathSample1_2; }
	inline void set_pathSample1_2(String_t* value)
	{
		___pathSample1_2 = value;
		Il2CppCodeGenWriteBarrier(&___pathSample1_2, value);
	}

	inline static int32_t get_offset_of_pathSample2_3() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___pathSample2_3)); }
	inline String_t* get_pathSample2_3() const { return ___pathSample2_3; }
	inline String_t** get_address_of_pathSample2_3() { return &___pathSample2_3; }
	inline void set_pathSample2_3(String_t* value)
	{
		___pathSample2_3 = value;
		Il2CppCodeGenWriteBarrier(&___pathSample2_3, value);
	}

	inline static int32_t get_offset_of_pathSample3_4() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___pathSample3_4)); }
	inline String_t* get_pathSample3_4() const { return ___pathSample3_4; }
	inline String_t** get_address_of_pathSample3_4() { return &___pathSample3_4; }
	inline void set_pathSample3_4(String_t* value)
	{
		___pathSample3_4 = value;
		Il2CppCodeGenWriteBarrier(&___pathSample3_4, value);
	}

	inline static int32_t get_offset_of_downloadedPath_5() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___downloadedPath_5)); }
	inline String_t* get_downloadedPath_5() const { return ___downloadedPath_5; }
	inline String_t** get_address_of_downloadedPath_5() { return &___downloadedPath_5; }
	inline void set_downloadedPath_5(String_t* value)
	{
		___downloadedPath_5 = value;
		Il2CppCodeGenWriteBarrier(&___downloadedPath_5, value);
	}

	inline static int32_t get_offset_of_url_6() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___url_6)); }
	inline String_t* get_url_6() const { return ___url_6; }
	inline String_t** get_address_of_url_6() { return &___url_6; }
	inline void set_url_6(String_t* value)
	{
		___url_6 = value;
		Il2CppCodeGenWriteBarrier(&___url_6, value);
	}

	inline static int32_t get_offset_of_path_7() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___path_7)); }
	inline StringU5BU5D_t1642385972* get_path_7() const { return ___path_7; }
	inline StringU5BU5D_t1642385972** get_address_of_path_7() { return &___path_7; }
	inline void set_path_7(StringU5BU5D_t1642385972* value)
	{
		___path_7 = value;
		Il2CppCodeGenWriteBarrier(&___path_7, value);
	}

	inline static int32_t get_offset_of_findKey_8() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___findKey_8)); }
	inline String_t* get_findKey_8() const { return ___findKey_8; }
	inline String_t** get_address_of_findKey_8() { return &___findKey_8; }
	inline void set_findKey_8(String_t* value)
	{
		___findKey_8 = value;
		Il2CppCodeGenWriteBarrier(&___findKey_8, value);
	}

	inline static int32_t get_offset_of_findValue_9() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___findValue_9)); }
	inline String_t* get_findValue_9() const { return ___findValue_9; }
	inline String_t** get_address_of_findValue_9() { return &___findValue_9; }
	inline void set_findValue_9(String_t* value)
	{
		___findValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___findValue_9, value);
	}

	inline static int32_t get_offset_of_scrollPosition_10() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___scrollPosition_10)); }
	inline Vector2_t2243707579  get_scrollPosition_10() const { return ___scrollPosition_10; }
	inline Vector2_t2243707579 * get_address_of_scrollPosition_10() { return &___scrollPosition_10; }
	inline void set_scrollPosition_10(Vector2_t2243707579  value)
	{
		___scrollPosition_10 = value;
	}

	inline static int32_t get_offset_of_scrollPositionLog_11() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___scrollPositionLog_11)); }
	inline Vector2_t2243707579  get_scrollPositionLog_11() const { return ___scrollPositionLog_11; }
	inline Vector2_t2243707579 * get_address_of_scrollPositionLog_11() { return &___scrollPositionLog_11; }
	inline void set_scrollPositionLog_11(Vector2_t2243707579  value)
	{
		___scrollPositionLog_11 = value;
	}

	inline static int32_t get_offset_of_logMsgs_12() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___logMsgs_12)); }
	inline String_t* get_logMsgs_12() const { return ___logMsgs_12; }
	inline String_t** get_address_of_logMsgs_12() { return &___logMsgs_12; }
	inline void set_logMsgs_12(String_t* value)
	{
		___logMsgs_12 = value;
		Il2CppCodeGenWriteBarrier(&___logMsgs_12, value);
	}

	inline static int32_t get_offset_of_jsonString_13() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___jsonString_13)); }
	inline String_t* get_jsonString_13() const { return ___jsonString_13; }
	inline String_t** get_address_of_jsonString_13() { return &___jsonString_13; }
	inline void set_jsonString_13(String_t* value)
	{
		___jsonString_13 = value;
		Il2CppCodeGenWriteBarrier(&___jsonString_13, value);
	}

	inline static int32_t get_offset_of_jsonLogString_14() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___jsonLogString_14)); }
	inline String_t* get_jsonLogString_14() const { return ___jsonLogString_14; }
	inline String_t** get_address_of_jsonLogString_14() { return &___jsonLogString_14; }
	inline void set_jsonLogString_14(String_t* value)
	{
		___jsonLogString_14 = value;
		Il2CppCodeGenWriteBarrier(&___jsonLogString_14, value);
	}

	inline static int32_t get_offset_of_jsonNode_15() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___jsonNode_15)); }
	inline Hashtable_t909839986 * get_jsonNode_15() const { return ___jsonNode_15; }
	inline Hashtable_t909839986 ** get_address_of_jsonNode_15() { return &___jsonNode_15; }
	inline void set_jsonNode_15(Hashtable_t909839986 * value)
	{
		___jsonNode_15 = value;
		Il2CppCodeGenWriteBarrier(&___jsonNode_15, value);
	}

	inline static int32_t get_offset_of_selectedNode_16() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___selectedNode_16)); }
	inline Il2CppObject * get_selectedNode_16() const { return ___selectedNode_16; }
	inline Il2CppObject ** get_address_of_selectedNode_16() { return &___selectedNode_16; }
	inline void set_selectedNode_16(Il2CppObject * value)
	{
		___selectedNode_16 = value;
		Il2CppCodeGenWriteBarrier(&___selectedNode_16, value);
	}

	inline static int32_t get_offset_of_bg_17() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___bg_17)); }
	inline Texture2D_t3542995729 * get_bg_17() const { return ___bg_17; }
	inline Texture2D_t3542995729 ** get_address_of_bg_17() { return &___bg_17; }
	inline void set_bg_17(Texture2D_t3542995729 * value)
	{
		___bg_17 = value;
		Il2CppCodeGenWriteBarrier(&___bg_17, value);
	}

	inline static int32_t get_offset_of_caseInsensitive_18() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___caseInsensitive_18)); }
	inline bool get_caseInsensitive_18() const { return ___caseInsensitive_18; }
	inline bool* get_address_of_caseInsensitive_18() { return &___caseInsensitive_18; }
	inline void set_caseInsensitive_18(bool value)
	{
		___caseInsensitive_18 = value;
	}

	inline static int32_t get_offset_of_screenshotCounter_19() { return static_cast<int32_t>(offsetof(DemoCtrl_t4011351802, ___screenshotCounter_19)); }
	inline int32_t get_screenshotCounter_19() const { return ___screenshotCounter_19; }
	inline int32_t* get_address_of_screenshotCounter_19() { return &___screenshotCounter_19; }
	inline void set_screenshotCounter_19(int32_t value)
	{
		___screenshotCounter_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
