﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageModel
struct  LanguageModel_t2428340347  : public Il2CppObject
{
public:
	// System.Collections.Hashtable LanguageModel::attributes
	Hashtable_t909839986 * ___attributes_0;

public:
	inline static int32_t get_offset_of_attributes_0() { return static_cast<int32_t>(offsetof(LanguageModel_t2428340347, ___attributes_0)); }
	inline Hashtable_t909839986 * get_attributes_0() const { return ___attributes_0; }
	inline Hashtable_t909839986 ** get_address_of_attributes_0() { return &___attributes_0; }
	inline void set_attributes_0(Hashtable_t909839986 * value)
	{
		___attributes_0 = value;
		Il2CppCodeGenWriteBarrier(&___attributes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
