﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement
struct SmtpSpecifiedPickupDirectoryElement_t3442976541;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement::.ctor()
extern "C"  void SmtpSpecifiedPickupDirectoryElement__ctor_m3593839781 (SmtpSpecifiedPickupDirectoryElement_t3442976541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement::.cctor()
extern "C"  void SmtpSpecifiedPickupDirectoryElement__cctor_m604302916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement::get_PickupDirectoryLocation()
extern "C"  String_t* SmtpSpecifiedPickupDirectoryElement_get_PickupDirectoryLocation_m3559804381 (SmtpSpecifiedPickupDirectoryElement_t3442976541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement::set_PickupDirectoryLocation(System.String)
extern "C"  void SmtpSpecifiedPickupDirectoryElement_set_PickupDirectoryLocation_m953341440 (SmtpSpecifiedPickupDirectoryElement_t3442976541 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * SmtpSpecifiedPickupDirectoryElement_get_Properties_m1538904016 (SmtpSpecifiedPickupDirectoryElement_t3442976541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
