﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsHomeNaviagetForwardButton
struct FriendsHomeNaviagetForwardButton_t3956477812;

#include "codegen/il2cpp-codegen.h"

// System.Void FriendsHomeNaviagetForwardButton::.ctor()
extern "C"  void FriendsHomeNaviagetForwardButton__ctor_m2558036087 (FriendsHomeNaviagetForwardButton_t3956477812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsHomeNaviagetForwardButton::OnButtonClick()
extern "C"  void FriendsHomeNaviagetForwardButton_OnButtonClick_m4241194764 (FriendsHomeNaviagetForwardButton_t3956477812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
