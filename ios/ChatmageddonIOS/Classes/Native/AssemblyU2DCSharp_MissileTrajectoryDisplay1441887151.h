﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchedMissile
struct LaunchedMissile_t1542515810;
// UIPanel
struct UIPanel_t1795085332;
// UnityEngine.Transform
struct Transform_t3275118058;
// UISprite
struct UISprite_t603616735;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissileTrajectoryDisplay
struct  MissileTrajectoryDisplay_t1441887151  : public MonoBehaviour_t1158329972
{
public:
	// LaunchedMissile MissileTrajectoryDisplay::missileItem
	LaunchedMissile_t1542515810 * ___missileItem_2;
	// UIPanel MissileTrajectoryDisplay::travelLinePanel
	UIPanel_t1795085332 * ___travelLinePanel_3;
	// UnityEngine.Transform MissileTrajectoryDisplay::missileContainer
	Transform_t3275118058 * ___missileContainer_4;
	// UISprite MissileTrajectoryDisplay::missileTexture
	UISprite_t603616735 * ___missileTexture_5;
	// UISprite MissileTrajectoryDisplay::resultTexture
	UISprite_t603616735 * ___resultTexture_6;
	// UISprite MissileTrajectoryDisplay::missileTravelLine
	UISprite_t603616735 * ___missileTravelLine_7;
	// TweenAlpha MissileTrajectoryDisplay::travelLineTween
	TweenAlpha_t2421518635 * ___travelLineTween_8;
	// UnityEngine.Transform MissileTrajectoryDisplay::stealthIncResultAnchor
	Transform_t3275118058 * ___stealthIncResultAnchor_9;
	// UnityEngine.Transform MissileTrajectoryDisplay::stealthOutResultAnchor
	Transform_t3275118058 * ___stealthOutResultAnchor_10;
	// System.Double MissileTrajectoryDisplay::totalTimeAsSeconds
	double ___totalTimeAsSeconds_11;
	// System.Single MissileTrajectoryDisplay::startRotation
	float ___startRotation_12;
	// System.Single MissileTrajectoryDisplay::endRotation
	float ___endRotation_13;

public:
	inline static int32_t get_offset_of_missileItem_2() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___missileItem_2)); }
	inline LaunchedMissile_t1542515810 * get_missileItem_2() const { return ___missileItem_2; }
	inline LaunchedMissile_t1542515810 ** get_address_of_missileItem_2() { return &___missileItem_2; }
	inline void set_missileItem_2(LaunchedMissile_t1542515810 * value)
	{
		___missileItem_2 = value;
		Il2CppCodeGenWriteBarrier(&___missileItem_2, value);
	}

	inline static int32_t get_offset_of_travelLinePanel_3() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___travelLinePanel_3)); }
	inline UIPanel_t1795085332 * get_travelLinePanel_3() const { return ___travelLinePanel_3; }
	inline UIPanel_t1795085332 ** get_address_of_travelLinePanel_3() { return &___travelLinePanel_3; }
	inline void set_travelLinePanel_3(UIPanel_t1795085332 * value)
	{
		___travelLinePanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___travelLinePanel_3, value);
	}

	inline static int32_t get_offset_of_missileContainer_4() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___missileContainer_4)); }
	inline Transform_t3275118058 * get_missileContainer_4() const { return ___missileContainer_4; }
	inline Transform_t3275118058 ** get_address_of_missileContainer_4() { return &___missileContainer_4; }
	inline void set_missileContainer_4(Transform_t3275118058 * value)
	{
		___missileContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___missileContainer_4, value);
	}

	inline static int32_t get_offset_of_missileTexture_5() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___missileTexture_5)); }
	inline UISprite_t603616735 * get_missileTexture_5() const { return ___missileTexture_5; }
	inline UISprite_t603616735 ** get_address_of_missileTexture_5() { return &___missileTexture_5; }
	inline void set_missileTexture_5(UISprite_t603616735 * value)
	{
		___missileTexture_5 = value;
		Il2CppCodeGenWriteBarrier(&___missileTexture_5, value);
	}

	inline static int32_t get_offset_of_resultTexture_6() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___resultTexture_6)); }
	inline UISprite_t603616735 * get_resultTexture_6() const { return ___resultTexture_6; }
	inline UISprite_t603616735 ** get_address_of_resultTexture_6() { return &___resultTexture_6; }
	inline void set_resultTexture_6(UISprite_t603616735 * value)
	{
		___resultTexture_6 = value;
		Il2CppCodeGenWriteBarrier(&___resultTexture_6, value);
	}

	inline static int32_t get_offset_of_missileTravelLine_7() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___missileTravelLine_7)); }
	inline UISprite_t603616735 * get_missileTravelLine_7() const { return ___missileTravelLine_7; }
	inline UISprite_t603616735 ** get_address_of_missileTravelLine_7() { return &___missileTravelLine_7; }
	inline void set_missileTravelLine_7(UISprite_t603616735 * value)
	{
		___missileTravelLine_7 = value;
		Il2CppCodeGenWriteBarrier(&___missileTravelLine_7, value);
	}

	inline static int32_t get_offset_of_travelLineTween_8() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___travelLineTween_8)); }
	inline TweenAlpha_t2421518635 * get_travelLineTween_8() const { return ___travelLineTween_8; }
	inline TweenAlpha_t2421518635 ** get_address_of_travelLineTween_8() { return &___travelLineTween_8; }
	inline void set_travelLineTween_8(TweenAlpha_t2421518635 * value)
	{
		___travelLineTween_8 = value;
		Il2CppCodeGenWriteBarrier(&___travelLineTween_8, value);
	}

	inline static int32_t get_offset_of_stealthIncResultAnchor_9() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___stealthIncResultAnchor_9)); }
	inline Transform_t3275118058 * get_stealthIncResultAnchor_9() const { return ___stealthIncResultAnchor_9; }
	inline Transform_t3275118058 ** get_address_of_stealthIncResultAnchor_9() { return &___stealthIncResultAnchor_9; }
	inline void set_stealthIncResultAnchor_9(Transform_t3275118058 * value)
	{
		___stealthIncResultAnchor_9 = value;
		Il2CppCodeGenWriteBarrier(&___stealthIncResultAnchor_9, value);
	}

	inline static int32_t get_offset_of_stealthOutResultAnchor_10() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___stealthOutResultAnchor_10)); }
	inline Transform_t3275118058 * get_stealthOutResultAnchor_10() const { return ___stealthOutResultAnchor_10; }
	inline Transform_t3275118058 ** get_address_of_stealthOutResultAnchor_10() { return &___stealthOutResultAnchor_10; }
	inline void set_stealthOutResultAnchor_10(Transform_t3275118058 * value)
	{
		___stealthOutResultAnchor_10 = value;
		Il2CppCodeGenWriteBarrier(&___stealthOutResultAnchor_10, value);
	}

	inline static int32_t get_offset_of_totalTimeAsSeconds_11() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___totalTimeAsSeconds_11)); }
	inline double get_totalTimeAsSeconds_11() const { return ___totalTimeAsSeconds_11; }
	inline double* get_address_of_totalTimeAsSeconds_11() { return &___totalTimeAsSeconds_11; }
	inline void set_totalTimeAsSeconds_11(double value)
	{
		___totalTimeAsSeconds_11 = value;
	}

	inline static int32_t get_offset_of_startRotation_12() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___startRotation_12)); }
	inline float get_startRotation_12() const { return ___startRotation_12; }
	inline float* get_address_of_startRotation_12() { return &___startRotation_12; }
	inline void set_startRotation_12(float value)
	{
		___startRotation_12 = value;
	}

	inline static int32_t get_offset_of_endRotation_13() { return static_cast<int32_t>(offsetof(MissileTrajectoryDisplay_t1441887151, ___endRotation_13)); }
	inline float get_endRotation_13() const { return ___endRotation_13; }
	inline float* get_address_of_endRotation_13() { return &___endRotation_13; }
	inline void set_endRotation_13(float value)
	{
		___endRotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
