﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPanelScaleFixer
struct  UIPanelScaleFixer_t3587123562  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 UIPanelScaleFixer::scale
	Vector3_t2243707580  ___scale_2;
	// UIPanel UIPanelScaleFixer::panel
	UIPanel_t1795085332 * ___panel_3;
	// UnityEngine.Transform UIPanelScaleFixer::scaleParent
	Transform_t3275118058 * ___scaleParent_4;

public:
	inline static int32_t get_offset_of_scale_2() { return static_cast<int32_t>(offsetof(UIPanelScaleFixer_t3587123562, ___scale_2)); }
	inline Vector3_t2243707580  get_scale_2() const { return ___scale_2; }
	inline Vector3_t2243707580 * get_address_of_scale_2() { return &___scale_2; }
	inline void set_scale_2(Vector3_t2243707580  value)
	{
		___scale_2 = value;
	}

	inline static int32_t get_offset_of_panel_3() { return static_cast<int32_t>(offsetof(UIPanelScaleFixer_t3587123562, ___panel_3)); }
	inline UIPanel_t1795085332 * get_panel_3() const { return ___panel_3; }
	inline UIPanel_t1795085332 ** get_address_of_panel_3() { return &___panel_3; }
	inline void set_panel_3(UIPanel_t1795085332 * value)
	{
		___panel_3 = value;
		Il2CppCodeGenWriteBarrier(&___panel_3, value);
	}

	inline static int32_t get_offset_of_scaleParent_4() { return static_cast<int32_t>(offsetof(UIPanelScaleFixer_t3587123562, ___scaleParent_4)); }
	inline Transform_t3275118058 * get_scaleParent_4() const { return ___scaleParent_4; }
	inline Transform_t3275118058 ** get_address_of_scaleParent_4() { return &___scaleParent_4; }
	inline void set_scaleParent_4(Transform_t3275118058 * value)
	{
		___scaleParent_4 = value;
		Il2CppCodeGenWriteBarrier(&___scaleParent_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
