﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardListItem
struct LeaderboardListItem_t3112309392;
// User
struct User_t719925459;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void LeaderboardListItem::.ctor()
extern "C"  void LeaderboardListItem__ctor_m3956493325 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::OnEnable()
extern "C"  void LeaderboardListItem_OnEnable_m2019838197 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::OnDisable()
extern "C"  void LeaderboardListItem_OnDisable_m4023208428 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::CalculatorHitZero(System.Boolean)
extern "C"  void LeaderboardListItem_CalculatorHitZero_m2772059765 (LeaderboardListItem_t3112309392 * __this, bool ___inStealth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::SetUpItem()
extern "C"  void LeaderboardListItem_SetUpItem_m1387753747 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::PopulateItem(User,System.Boolean)
extern "C"  void LeaderboardListItem_PopulateItem_m1513934634 (LeaderboardListItem_t3112309392 * __this, User_t719925459 * ___newUser0, bool ___globalFriend1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::SetVisible()
extern "C"  void LeaderboardListItem_SetVisible_m2479528705 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::SetInvisible()
extern "C"  void LeaderboardListItem_SetInvisible_m433626532 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LeaderboardListItem::verifyVisibility()
extern "C"  bool LeaderboardListItem_verifyVisibility_m4022927456 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::CheckVisibilty()
extern "C"  void LeaderboardListItem_CheckVisibilty_m658695116 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::Update()
extern "C"  void LeaderboardListItem_Update_m892894554 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::FindScrollView()
extern "C"  void LeaderboardListItem_FindScrollView_m536170124 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::OnPress(System.Boolean)
extern "C"  void LeaderboardListItem_OnPress_m2574882418 (LeaderboardListItem_t3112309392 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void LeaderboardListItem_OnDrag_m1147147816 (LeaderboardListItem_t3112309392 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::OnDragEnd()
extern "C"  void LeaderboardListItem_OnDragEnd_m1423628363 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::OnScroll(System.Single)
extern "C"  void LeaderboardListItem_OnScroll_m1857959020 (LeaderboardListItem_t3112309392 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardListItem::OnClick()
extern "C"  void LeaderboardListItem_OnClick_m3680962952 (LeaderboardListItem_t3112309392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
