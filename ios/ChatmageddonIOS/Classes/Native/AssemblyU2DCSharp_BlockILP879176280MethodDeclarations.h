﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlockILP
struct BlockILP_t879176280;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void BlockILP::.ctor()
extern "C"  void BlockILP__ctor_m4056460999 (BlockILP_t879176280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockILP::StartIL()
extern "C"  void BlockILP_StartIL_m201516442 (BlockILP_t879176280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockILP::SetDataArray()
extern "C"  void BlockILP_SetDataArray_m3131826378 (BlockILP_t879176280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void BlockILP_InitListItemWithIndex_m745442745 (BlockILP_t879176280 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlockILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void BlockILP_PopulateListItemWithIndex_m4089694056 (BlockILP_t879176280 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
