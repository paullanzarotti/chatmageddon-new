﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenderNavScreen/<SaveScreenContent>c__Iterator0
struct U3CSaveScreenContentU3Ec__Iterator0_t751163754;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GenderNavScreen/<SaveScreenContent>c__Iterator0::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0__ctor_m3495442453 (U3CSaveScreenContentU3Ec__Iterator0_t751163754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GenderNavScreen/<SaveScreenContent>c__Iterator0::MoveNext()
extern "C"  bool U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m3728676199 (U3CSaveScreenContentU3Ec__Iterator0_t751163754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GenderNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2722731803 (U3CSaveScreenContentU3Ec__Iterator0_t751163754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GenderNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m151318195 (U3CSaveScreenContentU3Ec__Iterator0_t751163754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderNavScreen/<SaveScreenContent>c__Iterator0::Dispose()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Dispose_m445517948 (U3CSaveScreenContentU3Ec__Iterator0_t751163754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GenderNavScreen/<SaveScreenContent>c__Iterator0::Reset()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Reset_m1427692810 (U3CSaveScreenContentU3Ec__Iterator0_t751163754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
