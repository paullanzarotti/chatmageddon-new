﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ErrorMessagePopUp>::.ctor()
#define MonoSingleton_1__ctor_m394140677(__this, method) ((  void (*) (MonoSingleton_1_t962131611 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ErrorMessagePopUp>::Awake()
#define MonoSingleton_1_Awake_m4059812056(__this, method) ((  void (*) (MonoSingleton_1_t962131611 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ErrorMessagePopUp>::get_Instance()
#define MonoSingleton_1_get_Instance_m8922380(__this /* static, unused */, method) ((  ErrorMessagePopUp_t1211465891 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ErrorMessagePopUp>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1255152238(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ErrorMessagePopUp>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1552629451(__this, method) ((  void (*) (MonoSingleton_1_t962131611 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ErrorMessagePopUp>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m4039746839(__this, method) ((  void (*) (MonoSingleton_1_t962131611 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ErrorMessagePopUp>::.cctor()
#define MonoSingleton_1__cctor_m2580020786(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
