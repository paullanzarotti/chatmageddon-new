﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_Brightness
struct CameraFilterPack_Colors_Brightness_t697012433;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_Brightness::.ctor()
extern "C"  void CameraFilterPack_Colors_Brightness__ctor_m3495228368 (CameraFilterPack_Colors_Brightness_t697012433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Brightness::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_Brightness_get_material_m3301747165 (CameraFilterPack_Colors_Brightness_t697012433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::Start()
extern "C"  void CameraFilterPack_Colors_Brightness_Start_m2914044432 (CameraFilterPack_Colors_Brightness_t697012433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_Brightness_OnRenderImage_m677125344 (CameraFilterPack_Colors_Brightness_t697012433 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::OnValidate()
extern "C"  void CameraFilterPack_Colors_Brightness_OnValidate_m1764178439 (CameraFilterPack_Colors_Brightness_t697012433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::Update()
extern "C"  void CameraFilterPack_Colors_Brightness_Update_m2768256601 (CameraFilterPack_Colors_Brightness_t697012433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::OnDisable()
extern "C"  void CameraFilterPack_Colors_Brightness_OnDisable_m2207122693 (CameraFilterPack_Colors_Brightness_t697012433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
