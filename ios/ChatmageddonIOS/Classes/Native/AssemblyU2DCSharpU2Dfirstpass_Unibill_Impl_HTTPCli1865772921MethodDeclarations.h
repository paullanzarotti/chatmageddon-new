﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.HTTPClient
struct HTTPClient_t1865772921;
// Uniject.IUtil
struct IUtil_t2188430191;
// System.String
struct String_t;
// Unibill.Impl.PostParameter[]
struct PostParameterU5BU5D_t2629828016;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.HTTPClient::.ctor(Uniject.IUtil)
extern "C"  void HTTPClient__ctor_m3603925420 (HTTPClient_t1865772921 * __this, Il2CppObject * ___util0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.HTTPClient::doPost(System.String,Unibill.Impl.PostParameter[])
extern "C"  void HTTPClient_doPost_m1404482 (HTTPClient_t1865772921 * __this, String_t* ___url0, PostParameterU5BU5D_t2629828016* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Unibill.Impl.HTTPClient::pump()
extern "C"  Il2CppObject * HTTPClient_pump_m2000172507 (HTTPClient_t1865772921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
