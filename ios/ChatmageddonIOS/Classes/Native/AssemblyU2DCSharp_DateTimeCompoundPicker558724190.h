﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPDatePicker
struct IPDatePicker_t1471736889;
// IPNumberPicker
struct IPNumberPicker_t1469221158;
// IPDatePicker/Date
struct Date_t2675184608;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DateTimeCompoundPicker
struct  DateTimeCompoundPicker_t558724190  : public MonoBehaviour_t1158329972
{
public:
	// IPDatePicker DateTimeCompoundPicker::datePicker
	IPDatePicker_t1471736889 * ___datePicker_2;
	// IPNumberPicker DateTimeCompoundPicker::hourPicker
	IPNumberPicker_t1469221158 * ___hourPicker_3;
	// IPNumberPicker DateTimeCompoundPicker::minutesPicker
	IPNumberPicker_t1469221158 * ___minutesPicker_4;
	// System.Boolean DateTimeCompoundPicker::initAtNow
	bool ___initAtNow_5;
	// IPDatePicker/Date DateTimeCompoundPicker::initDate
	Date_t2675184608 * ___initDate_6;
	// System.Int32 DateTimeCompoundPicker::initHour
	int32_t ___initHour_7;
	// System.Int32 DateTimeCompoundPicker::initMinute
	int32_t ___initMinute_8;
	// System.Boolean DateTimeCompoundPicker::_isInitialized
	bool ____isInitialized_9;

public:
	inline static int32_t get_offset_of_datePicker_2() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ___datePicker_2)); }
	inline IPDatePicker_t1471736889 * get_datePicker_2() const { return ___datePicker_2; }
	inline IPDatePicker_t1471736889 ** get_address_of_datePicker_2() { return &___datePicker_2; }
	inline void set_datePicker_2(IPDatePicker_t1471736889 * value)
	{
		___datePicker_2 = value;
		Il2CppCodeGenWriteBarrier(&___datePicker_2, value);
	}

	inline static int32_t get_offset_of_hourPicker_3() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ___hourPicker_3)); }
	inline IPNumberPicker_t1469221158 * get_hourPicker_3() const { return ___hourPicker_3; }
	inline IPNumberPicker_t1469221158 ** get_address_of_hourPicker_3() { return &___hourPicker_3; }
	inline void set_hourPicker_3(IPNumberPicker_t1469221158 * value)
	{
		___hourPicker_3 = value;
		Il2CppCodeGenWriteBarrier(&___hourPicker_3, value);
	}

	inline static int32_t get_offset_of_minutesPicker_4() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ___minutesPicker_4)); }
	inline IPNumberPicker_t1469221158 * get_minutesPicker_4() const { return ___minutesPicker_4; }
	inline IPNumberPicker_t1469221158 ** get_address_of_minutesPicker_4() { return &___minutesPicker_4; }
	inline void set_minutesPicker_4(IPNumberPicker_t1469221158 * value)
	{
		___minutesPicker_4 = value;
		Il2CppCodeGenWriteBarrier(&___minutesPicker_4, value);
	}

	inline static int32_t get_offset_of_initAtNow_5() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ___initAtNow_5)); }
	inline bool get_initAtNow_5() const { return ___initAtNow_5; }
	inline bool* get_address_of_initAtNow_5() { return &___initAtNow_5; }
	inline void set_initAtNow_5(bool value)
	{
		___initAtNow_5 = value;
	}

	inline static int32_t get_offset_of_initDate_6() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ___initDate_6)); }
	inline Date_t2675184608 * get_initDate_6() const { return ___initDate_6; }
	inline Date_t2675184608 ** get_address_of_initDate_6() { return &___initDate_6; }
	inline void set_initDate_6(Date_t2675184608 * value)
	{
		___initDate_6 = value;
		Il2CppCodeGenWriteBarrier(&___initDate_6, value);
	}

	inline static int32_t get_offset_of_initHour_7() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ___initHour_7)); }
	inline int32_t get_initHour_7() const { return ___initHour_7; }
	inline int32_t* get_address_of_initHour_7() { return &___initHour_7; }
	inline void set_initHour_7(int32_t value)
	{
		___initHour_7 = value;
	}

	inline static int32_t get_offset_of_initMinute_8() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ___initMinute_8)); }
	inline int32_t get_initMinute_8() const { return ___initMinute_8; }
	inline int32_t* get_address_of_initMinute_8() { return &___initMinute_8; }
	inline void set_initMinute_8(int32_t value)
	{
		___initMinute_8 = value;
	}

	inline static int32_t get_offset_of__isInitialized_9() { return static_cast<int32_t>(offsetof(DateTimeCompoundPicker_t558724190, ____isInitialized_9)); }
	inline bool get__isInitialized_9() const { return ____isInitialized_9; }
	inline bool* get_address_of__isInitialized_9() { return &____isInitialized_9; }
	inline void set__isInitialized_9(bool value)
	{
		____isInitialized_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
