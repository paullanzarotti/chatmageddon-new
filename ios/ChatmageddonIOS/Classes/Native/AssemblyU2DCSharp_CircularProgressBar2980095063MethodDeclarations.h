﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CircularProgressBar
struct CircularProgressBar_t2980095063;

#include "codegen/il2cpp-codegen.h"

// System.Void CircularProgressBar::.ctor()
extern "C"  void CircularProgressBar__ctor_m2789106362 (CircularProgressBar_t2980095063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::Awake()
extern "C"  void CircularProgressBar_Awake_m117063187 (CircularProgressBar_t2980095063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::TransitionToPercent(System.Single,System.Single)
extern "C"  void CircularProgressBar_TransitionToPercent_m2663738129 (CircularProgressBar_t2980095063 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::ProgressUpdated()
extern "C"  void CircularProgressBar_ProgressUpdated_m2152456460 (CircularProgressBar_t2980095063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::ProgressFinished()
extern "C"  void CircularProgressBar_ProgressFinished_m2682252505 (CircularProgressBar_t2980095063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::SetSectionPercentage()
extern "C"  void CircularProgressBar_SetSectionPercentage_m1968602745 (CircularProgressBar_t2980095063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::ActivateProgressPanel(System.Boolean,System.Boolean)
extern "C"  void CircularProgressBar_ActivateProgressPanel_m978092298 (CircularProgressBar_t2980095063 * __this, bool ___active0, bool ___start1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::SetFixedSections()
extern "C"  void CircularProgressBar_SetFixedSections_m3033413938 (CircularProgressBar_t2980095063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CircularProgressBar::UpdateProgressPanel()
extern "C"  void CircularProgressBar_UpdateProgressPanel_m2481338838 (CircularProgressBar_t2980095063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
