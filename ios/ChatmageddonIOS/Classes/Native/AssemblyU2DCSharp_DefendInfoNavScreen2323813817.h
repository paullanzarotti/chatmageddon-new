﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefendModal
struct DefendModal_t2645228609;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// UILabel
struct UILabel_t1795115428;
// CountdownTimerProgressBar
struct CountdownTimerProgressBar_t1742287524;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Light
struct Light_t494725636;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendInfoNavScreen
struct  DefendInfoNavScreen_t2323813817  : public NavigationScreen_t2333230110
{
public:
	// DefendModal DefendInfoNavScreen::modal
	DefendModal_t2645228609 * ___modal_3;
	// AvatarUpdater DefendInfoNavScreen::avatar
	AvatarUpdater_t2404165026 * ___avatar_4;
	// UILabel DefendInfoNavScreen::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_5;
	// UILabel DefendInfoNavScreen::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_6;
	// UILabel DefendInfoNavScreen::itemNameLabel
	UILabel_t1795115428 * ___itemNameLabel_7;
	// UILabel DefendInfoNavScreen::actionLabel
	UILabel_t1795115428 * ___actionLabel_8;
	// CountdownTimerProgressBar DefendInfoNavScreen::timer
	CountdownTimerProgressBar_t1742287524 * ___timer_9;
	// UnityEngine.GameObject DefendInfoNavScreen::modelParent
	GameObject_t1756533147 * ___modelParent_10;
	// UnityEngine.GameObject DefendInfoNavScreen::currentModel
	GameObject_t1756533147 * ___currentModel_11;
	// UnityEngine.Light DefendInfoNavScreen::itemLight
	Light_t494725636 * ___itemLight_12;
	// System.Boolean DefendInfoNavScreen::uiClosing
	bool ___uiClosing_13;

public:
	inline static int32_t get_offset_of_modal_3() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___modal_3)); }
	inline DefendModal_t2645228609 * get_modal_3() const { return ___modal_3; }
	inline DefendModal_t2645228609 ** get_address_of_modal_3() { return &___modal_3; }
	inline void set_modal_3(DefendModal_t2645228609 * value)
	{
		___modal_3 = value;
		Il2CppCodeGenWriteBarrier(&___modal_3, value);
	}

	inline static int32_t get_offset_of_avatar_4() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___avatar_4)); }
	inline AvatarUpdater_t2404165026 * get_avatar_4() const { return ___avatar_4; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_4() { return &___avatar_4; }
	inline void set_avatar_4(AvatarUpdater_t2404165026 * value)
	{
		___avatar_4 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_4, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_5() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___firstNameLabel_5)); }
	inline UILabel_t1795115428 * get_firstNameLabel_5() const { return ___firstNameLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_5() { return &___firstNameLabel_5; }
	inline void set_firstNameLabel_5(UILabel_t1795115428 * value)
	{
		___firstNameLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_5, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_6() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___lastNameLabel_6)); }
	inline UILabel_t1795115428 * get_lastNameLabel_6() const { return ___lastNameLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_6() { return &___lastNameLabel_6; }
	inline void set_lastNameLabel_6(UILabel_t1795115428 * value)
	{
		___lastNameLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_6, value);
	}

	inline static int32_t get_offset_of_itemNameLabel_7() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___itemNameLabel_7)); }
	inline UILabel_t1795115428 * get_itemNameLabel_7() const { return ___itemNameLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_itemNameLabel_7() { return &___itemNameLabel_7; }
	inline void set_itemNameLabel_7(UILabel_t1795115428 * value)
	{
		___itemNameLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameLabel_7, value);
	}

	inline static int32_t get_offset_of_actionLabel_8() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___actionLabel_8)); }
	inline UILabel_t1795115428 * get_actionLabel_8() const { return ___actionLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_actionLabel_8() { return &___actionLabel_8; }
	inline void set_actionLabel_8(UILabel_t1795115428 * value)
	{
		___actionLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_8, value);
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___timer_9)); }
	inline CountdownTimerProgressBar_t1742287524 * get_timer_9() const { return ___timer_9; }
	inline CountdownTimerProgressBar_t1742287524 ** get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(CountdownTimerProgressBar_t1742287524 * value)
	{
		___timer_9 = value;
		Il2CppCodeGenWriteBarrier(&___timer_9, value);
	}

	inline static int32_t get_offset_of_modelParent_10() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___modelParent_10)); }
	inline GameObject_t1756533147 * get_modelParent_10() const { return ___modelParent_10; }
	inline GameObject_t1756533147 ** get_address_of_modelParent_10() { return &___modelParent_10; }
	inline void set_modelParent_10(GameObject_t1756533147 * value)
	{
		___modelParent_10 = value;
		Il2CppCodeGenWriteBarrier(&___modelParent_10, value);
	}

	inline static int32_t get_offset_of_currentModel_11() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___currentModel_11)); }
	inline GameObject_t1756533147 * get_currentModel_11() const { return ___currentModel_11; }
	inline GameObject_t1756533147 ** get_address_of_currentModel_11() { return &___currentModel_11; }
	inline void set_currentModel_11(GameObject_t1756533147 * value)
	{
		___currentModel_11 = value;
		Il2CppCodeGenWriteBarrier(&___currentModel_11, value);
	}

	inline static int32_t get_offset_of_itemLight_12() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___itemLight_12)); }
	inline Light_t494725636 * get_itemLight_12() const { return ___itemLight_12; }
	inline Light_t494725636 ** get_address_of_itemLight_12() { return &___itemLight_12; }
	inline void set_itemLight_12(Light_t494725636 * value)
	{
		___itemLight_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_12, value);
	}

	inline static int32_t get_offset_of_uiClosing_13() { return static_cast<int32_t>(offsetof(DefendInfoNavScreen_t2323813817, ___uiClosing_13)); }
	inline bool get_uiClosing_13() const { return ___uiClosing_13; }
	inline bool* get_address_of_uiClosing_13() { return &___uiClosing_13; }
	inline void set_uiClosing_13(bool value)
	{
		___uiClosing_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
