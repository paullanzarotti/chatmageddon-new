﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StatusListItem
struct StatusListItem_t459202613;
// TweenPosition
struct TweenPosition_t1144714832;

#include "AssemblyU2DCSharp_NGUISwipe4135300327.h"
#include "AssemblyU2DCSharp_StatusSwipeState381899333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusItemSwipe
struct  StatusItemSwipe_t1669089155  : public NGUISwipe_t4135300327
{
public:
	// StatusSwipeState StatusItemSwipe::currentState
	int32_t ___currentState_10;
	// StatusListItem StatusItemSwipe::listItem
	StatusListItem_t459202613 * ___listItem_11;
	// TweenPosition StatusItemSwipe::posTween
	TweenPosition_t1144714832 * ___posTween_12;

public:
	inline static int32_t get_offset_of_currentState_10() { return static_cast<int32_t>(offsetof(StatusItemSwipe_t1669089155, ___currentState_10)); }
	inline int32_t get_currentState_10() const { return ___currentState_10; }
	inline int32_t* get_address_of_currentState_10() { return &___currentState_10; }
	inline void set_currentState_10(int32_t value)
	{
		___currentState_10 = value;
	}

	inline static int32_t get_offset_of_listItem_11() { return static_cast<int32_t>(offsetof(StatusItemSwipe_t1669089155, ___listItem_11)); }
	inline StatusListItem_t459202613 * get_listItem_11() const { return ___listItem_11; }
	inline StatusListItem_t459202613 ** get_address_of_listItem_11() { return &___listItem_11; }
	inline void set_listItem_11(StatusListItem_t459202613 * value)
	{
		___listItem_11 = value;
		Il2CppCodeGenWriteBarrier(&___listItem_11, value);
	}

	inline static int32_t get_offset_of_posTween_12() { return static_cast<int32_t>(offsetof(StatusItemSwipe_t1669089155, ___posTween_12)); }
	inline TweenPosition_t1144714832 * get_posTween_12() const { return ___posTween_12; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_12() { return &___posTween_12; }
	inline void set_posTween_12(TweenPosition_t1144714832 * value)
	{
		___posTween_12 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
