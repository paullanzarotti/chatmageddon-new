﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatContactsListItemUIScaler
struct ChatContactsListItemUIScaler_t2894863298;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatContactsListItemUIScaler::.ctor()
extern "C"  void ChatContactsListItemUIScaler__ctor_m2249070547 (ChatContactsListItemUIScaler_t2894863298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsListItemUIScaler::GlobalUIScale()
extern "C"  void ChatContactsListItemUIScaler_GlobalUIScale_m3292747116 (ChatContactsListItemUIScaler_t2894863298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
