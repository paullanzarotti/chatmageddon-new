﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatThreadsILP
struct ChatThreadsILP_t2341903446;
// ChatMessageILP
struct ChatMessageILP_t4258019024;
// ChatContactsILP
struct ChatContactsILP_t1780035494;
// ChatThread
struct ChatThread_t2394323482;
// System.Collections.Generic.List`1<ChatContact>
struct List_1_t3129821146;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// System.String
struct String_t;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2543256415.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatManager
struct  ChatManager_t2792590695  : public MonoSingleton_1_t2543256415
{
public:
	// ChatThreadsILP ChatManager::threadsList
	ChatThreadsILP_t2341903446 * ___threadsList_3;
	// ChatMessageILP ChatManager::messagesList
	ChatMessageILP_t4258019024 * ___messagesList_4;
	// ChatContactsILP ChatManager::contatcsList
	ChatContactsILP_t1780035494 * ___contatcsList_5;
	// System.Boolean ChatManager::newMessage
	bool ___newMessage_6;
	// System.Boolean ChatManager::additionalContact
	bool ___additionalContact_7;
	// ChatThread ChatManager::activeThread
	ChatThread_t2394323482 * ___activeThread_8;
	// System.Collections.Generic.List`1<ChatContact> ChatManager::activeContacts
	List_1_t3129821146 * ___activeContacts_9;
	// System.Collections.Generic.List`1<Friend> ChatManager::localUserSearchList
	List_1_t2924135240 * ___localUserSearchList_10;
	// System.Boolean ChatManager::contactsListUpdated
	bool ___contactsListUpdated_11;
	// System.Boolean ChatManager::threadsListUpdated
	bool ___threadsListUpdated_12;
	// System.String ChatManager::currentSearchTerm
	String_t* ___currentSearchTerm_13;

public:
	inline static int32_t get_offset_of_threadsList_3() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___threadsList_3)); }
	inline ChatThreadsILP_t2341903446 * get_threadsList_3() const { return ___threadsList_3; }
	inline ChatThreadsILP_t2341903446 ** get_address_of_threadsList_3() { return &___threadsList_3; }
	inline void set_threadsList_3(ChatThreadsILP_t2341903446 * value)
	{
		___threadsList_3 = value;
		Il2CppCodeGenWriteBarrier(&___threadsList_3, value);
	}

	inline static int32_t get_offset_of_messagesList_4() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___messagesList_4)); }
	inline ChatMessageILP_t4258019024 * get_messagesList_4() const { return ___messagesList_4; }
	inline ChatMessageILP_t4258019024 ** get_address_of_messagesList_4() { return &___messagesList_4; }
	inline void set_messagesList_4(ChatMessageILP_t4258019024 * value)
	{
		___messagesList_4 = value;
		Il2CppCodeGenWriteBarrier(&___messagesList_4, value);
	}

	inline static int32_t get_offset_of_contatcsList_5() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___contatcsList_5)); }
	inline ChatContactsILP_t1780035494 * get_contatcsList_5() const { return ___contatcsList_5; }
	inline ChatContactsILP_t1780035494 ** get_address_of_contatcsList_5() { return &___contatcsList_5; }
	inline void set_contatcsList_5(ChatContactsILP_t1780035494 * value)
	{
		___contatcsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___contatcsList_5, value);
	}

	inline static int32_t get_offset_of_newMessage_6() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___newMessage_6)); }
	inline bool get_newMessage_6() const { return ___newMessage_6; }
	inline bool* get_address_of_newMessage_6() { return &___newMessage_6; }
	inline void set_newMessage_6(bool value)
	{
		___newMessage_6 = value;
	}

	inline static int32_t get_offset_of_additionalContact_7() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___additionalContact_7)); }
	inline bool get_additionalContact_7() const { return ___additionalContact_7; }
	inline bool* get_address_of_additionalContact_7() { return &___additionalContact_7; }
	inline void set_additionalContact_7(bool value)
	{
		___additionalContact_7 = value;
	}

	inline static int32_t get_offset_of_activeThread_8() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___activeThread_8)); }
	inline ChatThread_t2394323482 * get_activeThread_8() const { return ___activeThread_8; }
	inline ChatThread_t2394323482 ** get_address_of_activeThread_8() { return &___activeThread_8; }
	inline void set_activeThread_8(ChatThread_t2394323482 * value)
	{
		___activeThread_8 = value;
		Il2CppCodeGenWriteBarrier(&___activeThread_8, value);
	}

	inline static int32_t get_offset_of_activeContacts_9() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___activeContacts_9)); }
	inline List_1_t3129821146 * get_activeContacts_9() const { return ___activeContacts_9; }
	inline List_1_t3129821146 ** get_address_of_activeContacts_9() { return &___activeContacts_9; }
	inline void set_activeContacts_9(List_1_t3129821146 * value)
	{
		___activeContacts_9 = value;
		Il2CppCodeGenWriteBarrier(&___activeContacts_9, value);
	}

	inline static int32_t get_offset_of_localUserSearchList_10() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___localUserSearchList_10)); }
	inline List_1_t2924135240 * get_localUserSearchList_10() const { return ___localUserSearchList_10; }
	inline List_1_t2924135240 ** get_address_of_localUserSearchList_10() { return &___localUserSearchList_10; }
	inline void set_localUserSearchList_10(List_1_t2924135240 * value)
	{
		___localUserSearchList_10 = value;
		Il2CppCodeGenWriteBarrier(&___localUserSearchList_10, value);
	}

	inline static int32_t get_offset_of_contactsListUpdated_11() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___contactsListUpdated_11)); }
	inline bool get_contactsListUpdated_11() const { return ___contactsListUpdated_11; }
	inline bool* get_address_of_contactsListUpdated_11() { return &___contactsListUpdated_11; }
	inline void set_contactsListUpdated_11(bool value)
	{
		___contactsListUpdated_11 = value;
	}

	inline static int32_t get_offset_of_threadsListUpdated_12() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___threadsListUpdated_12)); }
	inline bool get_threadsListUpdated_12() const { return ___threadsListUpdated_12; }
	inline bool* get_address_of_threadsListUpdated_12() { return &___threadsListUpdated_12; }
	inline void set_threadsListUpdated_12(bool value)
	{
		___threadsListUpdated_12 = value;
	}

	inline static int32_t get_offset_of_currentSearchTerm_13() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695, ___currentSearchTerm_13)); }
	inline String_t* get_currentSearchTerm_13() const { return ___currentSearchTerm_13; }
	inline String_t** get_address_of_currentSearchTerm_13() { return &___currentSearchTerm_13; }
	inline void set_currentSearchTerm_13(String_t* value)
	{
		___currentSearchTerm_13 = value;
		Il2CppCodeGenWriteBarrier(&___currentSearchTerm_13, value);
	}
};

struct ChatManager_t2792590695_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ChatManager::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_14;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ChatManager::<>f__am$cache1
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache1_15;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ChatManager::<>f__am$cache2
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache2_16;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> ChatManager::<>f__am$cache3
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache3_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_15() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695_StaticFields, ___U3CU3Ef__amU24cache1_15)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache1_15() const { return ___U3CU3Ef__amU24cache1_15; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache1_15() { return &___U3CU3Ef__amU24cache1_15; }
	inline void set_U3CU3Ef__amU24cache1_15(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache1_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_16() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695_StaticFields, ___U3CU3Ef__amU24cache2_16)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache2_16() const { return ___U3CU3Ef__amU24cache2_16; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache2_16() { return &___U3CU3Ef__amU24cache2_16; }
	inline void set_U3CU3Ef__amU24cache2_16(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache2_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_17() { return static_cast<int32_t>(offsetof(ChatManager_t2792590695_StaticFields, ___U3CU3Ef__amU24cache3_17)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache3_17() const { return ___U3CU3Ef__amU24cache3_17; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache3_17() { return &___U3CU3Ef__amU24cache3_17; }
	inline void set_U3CU3Ef__amU24cache3_17(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache3_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
