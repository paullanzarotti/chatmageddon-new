﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineMiniGameController
struct MineMiniGameController_t2791970086;
// TweenPosition
struct TweenPosition_t1144714832;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// UILabel
struct UILabel_t1795115428;
// MineCountdownProgressBar
struct MineCountdownProgressBar_t1258896602;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Light
struct Light_t494725636;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineInfoController
struct  MineInfoController_t2990208339  : public MonoBehaviour_t1158329972
{
public:
	// MineMiniGameController MineInfoController::gameController
	MineMiniGameController_t2791970086 * ___gameController_2;
	// TweenPosition MineInfoController::posTween
	TweenPosition_t1144714832 * ___posTween_3;
	// AvatarUpdater MineInfoController::avatar
	AvatarUpdater_t2404165026 * ___avatar_4;
	// UILabel MineInfoController::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_5;
	// UILabel MineInfoController::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_6;
	// UILabel MineInfoController::itemNameLabel
	UILabel_t1795115428 * ___itemNameLabel_7;
	// UILabel MineInfoController::actionLabel
	UILabel_t1795115428 * ___actionLabel_8;
	// MineCountdownProgressBar MineInfoController::timer
	MineCountdownProgressBar_t1258896602 * ___timer_9;
	// UnityEngine.GameObject MineInfoController::modelParent
	GameObject_t1756533147 * ___modelParent_10;
	// UnityEngine.GameObject MineInfoController::currentModel
	GameObject_t1756533147 * ___currentModel_11;
	// UnityEngine.Light MineInfoController::itemLight
	Light_t494725636 * ___itemLight_12;
	// System.Boolean MineInfoController::uiClosing
	bool ___uiClosing_13;

public:
	inline static int32_t get_offset_of_gameController_2() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___gameController_2)); }
	inline MineMiniGameController_t2791970086 * get_gameController_2() const { return ___gameController_2; }
	inline MineMiniGameController_t2791970086 ** get_address_of_gameController_2() { return &___gameController_2; }
	inline void set_gameController_2(MineMiniGameController_t2791970086 * value)
	{
		___gameController_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameController_2, value);
	}

	inline static int32_t get_offset_of_posTween_3() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___posTween_3)); }
	inline TweenPosition_t1144714832 * get_posTween_3() const { return ___posTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_3() { return &___posTween_3; }
	inline void set_posTween_3(TweenPosition_t1144714832 * value)
	{
		___posTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_3, value);
	}

	inline static int32_t get_offset_of_avatar_4() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___avatar_4)); }
	inline AvatarUpdater_t2404165026 * get_avatar_4() const { return ___avatar_4; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_4() { return &___avatar_4; }
	inline void set_avatar_4(AvatarUpdater_t2404165026 * value)
	{
		___avatar_4 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_4, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_5() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___firstNameLabel_5)); }
	inline UILabel_t1795115428 * get_firstNameLabel_5() const { return ___firstNameLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_5() { return &___firstNameLabel_5; }
	inline void set_firstNameLabel_5(UILabel_t1795115428 * value)
	{
		___firstNameLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_5, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_6() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___lastNameLabel_6)); }
	inline UILabel_t1795115428 * get_lastNameLabel_6() const { return ___lastNameLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_6() { return &___lastNameLabel_6; }
	inline void set_lastNameLabel_6(UILabel_t1795115428 * value)
	{
		___lastNameLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_6, value);
	}

	inline static int32_t get_offset_of_itemNameLabel_7() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___itemNameLabel_7)); }
	inline UILabel_t1795115428 * get_itemNameLabel_7() const { return ___itemNameLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_itemNameLabel_7() { return &___itemNameLabel_7; }
	inline void set_itemNameLabel_7(UILabel_t1795115428 * value)
	{
		___itemNameLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameLabel_7, value);
	}

	inline static int32_t get_offset_of_actionLabel_8() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___actionLabel_8)); }
	inline UILabel_t1795115428 * get_actionLabel_8() const { return ___actionLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_actionLabel_8() { return &___actionLabel_8; }
	inline void set_actionLabel_8(UILabel_t1795115428 * value)
	{
		___actionLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_8, value);
	}

	inline static int32_t get_offset_of_timer_9() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___timer_9)); }
	inline MineCountdownProgressBar_t1258896602 * get_timer_9() const { return ___timer_9; }
	inline MineCountdownProgressBar_t1258896602 ** get_address_of_timer_9() { return &___timer_9; }
	inline void set_timer_9(MineCountdownProgressBar_t1258896602 * value)
	{
		___timer_9 = value;
		Il2CppCodeGenWriteBarrier(&___timer_9, value);
	}

	inline static int32_t get_offset_of_modelParent_10() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___modelParent_10)); }
	inline GameObject_t1756533147 * get_modelParent_10() const { return ___modelParent_10; }
	inline GameObject_t1756533147 ** get_address_of_modelParent_10() { return &___modelParent_10; }
	inline void set_modelParent_10(GameObject_t1756533147 * value)
	{
		___modelParent_10 = value;
		Il2CppCodeGenWriteBarrier(&___modelParent_10, value);
	}

	inline static int32_t get_offset_of_currentModel_11() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___currentModel_11)); }
	inline GameObject_t1756533147 * get_currentModel_11() const { return ___currentModel_11; }
	inline GameObject_t1756533147 ** get_address_of_currentModel_11() { return &___currentModel_11; }
	inline void set_currentModel_11(GameObject_t1756533147 * value)
	{
		___currentModel_11 = value;
		Il2CppCodeGenWriteBarrier(&___currentModel_11, value);
	}

	inline static int32_t get_offset_of_itemLight_12() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___itemLight_12)); }
	inline Light_t494725636 * get_itemLight_12() const { return ___itemLight_12; }
	inline Light_t494725636 ** get_address_of_itemLight_12() { return &___itemLight_12; }
	inline void set_itemLight_12(Light_t494725636 * value)
	{
		___itemLight_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_12, value);
	}

	inline static int32_t get_offset_of_uiClosing_13() { return static_cast<int32_t>(offsetof(MineInfoController_t2990208339, ___uiClosing_13)); }
	inline bool get_uiClosing_13() const { return ___uiClosing_13; }
	inline bool* get_address_of_uiClosing_13() { return &___uiClosing_13; }
	inline void set_uiClosing_13(bool value)
	{
		___uiClosing_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
