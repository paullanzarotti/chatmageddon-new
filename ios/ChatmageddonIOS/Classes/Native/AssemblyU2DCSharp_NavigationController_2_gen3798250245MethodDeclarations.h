﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen2850042411MethodDeclarations.h"

// System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::.ctor()
#define NavigationController_2__ctor_m1493452343(__this, method) ((  void (*) (NavigationController_2_t3798250245 *, const MethodInfo*))NavigationController_2__ctor_m356458940_gshared)(__this, method)
// System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m1635219081(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t3798250245 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m550598550_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m661214432(__this, method) ((  void (*) (NavigationController_2_t3798250245 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m1751947769_gshared)(__this, method)
// System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m1323490253(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t3798250245 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m639547216_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m3199778853(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t3798250245 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m2164337852_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m4017903134(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t3798250245 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m4143251419_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m2507046005(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t3798250245 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m2387324626_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m3129080337(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t3798250245 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2311448660_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<RegistrationNavigationcontroller,RegNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m540892537(__this, ___screen0, method) ((  void (*) (NavigationController_2_t3798250245 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m366324300_gshared)(__this, ___screen0, method)
