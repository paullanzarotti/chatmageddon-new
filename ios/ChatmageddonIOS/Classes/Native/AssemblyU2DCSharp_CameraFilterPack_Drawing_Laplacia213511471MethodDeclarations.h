﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Laplacian
struct CameraFilterPack_Drawing_Laplacian_t213511471;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Laplacian::.ctor()
extern "C"  void CameraFilterPack_Drawing_Laplacian__ctor_m2919603642 (CameraFilterPack_Drawing_Laplacian_t213511471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Laplacian::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Laplacian_get_material_m2218560519 (CameraFilterPack_Drawing_Laplacian_t213511471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::Start()
extern "C"  void CameraFilterPack_Drawing_Laplacian_Start_m1598404910 (CameraFilterPack_Drawing_Laplacian_t213511471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Laplacian_OnRenderImage_m2106871806 (CameraFilterPack_Drawing_Laplacian_t213511471 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::Update()
extern "C"  void CameraFilterPack_Drawing_Laplacian_Update_m1388380411 (CameraFilterPack_Drawing_Laplacian_t213511471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Laplacian_OnDisable_m2480955611 (CameraFilterPack_Drawing_Laplacian_t213511471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
