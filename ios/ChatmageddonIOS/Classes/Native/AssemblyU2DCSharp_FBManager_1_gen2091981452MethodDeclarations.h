﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBManager`1<System.Object>
struct FBManager_1_t2091981452;
// Facebook.Unity.ILoginResult
struct ILoginResult_t403585443;
// Facebook.Unity.IGraphResult
struct IGraphResult_t3984946686;
// FacebookPlayer
struct FacebookPlayer_t2645999341;
// System.Collections.Generic.List`1<System.Collections.Hashtable>
struct List_1_t278961118;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// Facebook.Unity.IShareResult
struct IShareResult_t830127229;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// FBManager`1/LoadPictureCallback<System.Object>
struct LoadPictureCallback_t2854975038;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LoginEntrance2243089009.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void FBManager`1<System.Object>::.ctor()
extern "C"  void FBManager_1__ctor_m1614973529_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1__ctor_m1614973529(__this, method) ((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1__ctor_m1614973529_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::InitFacebook()
extern "C"  void FBManager_1_InitFacebook_m3847425487_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_InitFacebook_m3847425487(__this, method) ((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_InitFacebook_m3847425487_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::CallInit()
extern "C"  void FBManager_1_CallInit_m1371816793_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_CallInit_m1371816793(__this, method) ((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_CallInit_m1371816793_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::SetInit()
extern "C"  void FBManager_1_SetInit_m3245493993_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_SetInit_m3245493993(__this, method) ((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_SetInit_m3245493993_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::OnHideUnity(System.Boolean)
extern "C"  void FBManager_1_OnHideUnity_m3813074720_gshared (FBManager_1_t2091981452 * __this, bool ___isGameShown0, const MethodInfo* method);
#define FBManager_1_OnHideUnity_m3813074720(__this, ___isGameShown0, method) ((  void (*) (FBManager_1_t2091981452 *, bool, const MethodInfo*))FBManager_1_OnHideUnity_m3813074720_gshared)(__this, ___isGameShown0, method)
// System.Void FBManager`1<System.Object>::LoginToFacebook(LoginEntrance)
extern "C"  void FBManager_1_LoginToFacebook_m3247006910_gshared (FBManager_1_t2091981452 * __this, int32_t ___entrance0, const MethodInfo* method);
#define FBManager_1_LoginToFacebook_m3247006910(__this, ___entrance0, method) ((  void (*) (FBManager_1_t2091981452 *, int32_t, const MethodInfo*))FBManager_1_LoginToFacebook_m3247006910_gshared)(__this, ___entrance0, method)
// System.Void FBManager`1<System.Object>::LoginCallback(Facebook.Unity.ILoginResult)
extern "C"  void FBManager_1_LoginCallback_m3384117207_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define FBManager_1_LoginCallback_m3384117207(__this, ___result0, method) ((  void (*) (FBManager_1_t2091981452 *, Il2CppObject *, const MethodInfo*))FBManager_1_LoginCallback_m3384117207_gshared)(__this, ___result0, method)
// System.Boolean FBManager`1<System.Object>::CurrentUserHasPublishPermission()
extern "C"  bool FBManager_1_CurrentUserHasPublishPermission_m3669725089_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_CurrentUserHasPublishPermission_m3669725089(__this, method) ((  bool (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_CurrentUserHasPublishPermission_m3669725089_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::OnLoggedIn()
extern "C"  void FBManager_1_OnLoggedIn_m1876336541_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_OnLoggedIn_m1876336541(__this, method) ((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_OnLoggedIn_m1876336541_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::APICallback(Facebook.Unity.IGraphResult)
extern "C"  void FBManager_1_APICallback_m2721173331_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define FBManager_1_APICallback_m2721173331(__this, ___result0, method) ((  void (*) (FBManager_1_t2091981452 *, Il2CppObject *, const MethodInfo*))FBManager_1_APICallback_m2721173331_gshared)(__this, ___result0, method)
// System.Void FBManager`1<System.Object>::OnLoginCancelled()
extern "C"  void FBManager_1_OnLoginCancelled_m3895251954_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_OnLoginCancelled_m3895251954(__this, method) ((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_OnLoginCancelled_m3895251954_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::LogOutOfFacebook(System.Boolean)
extern "C"  void FBManager_1_LogOutOfFacebook_m751438301_gshared (FBManager_1_t2091981452 * __this, bool ___online0, const MethodInfo* method);
#define FBManager_1_LogOutOfFacebook_m751438301(__this, ___online0, method) ((  void (*) (FBManager_1_t2091981452 *, bool, const MethodInfo*))FBManager_1_LogOutOfFacebook_m751438301_gshared)(__this, ___online0, method)
// System.Void FBManager`1<System.Object>::OnLoggedOut(System.Boolean)
extern "C"  void FBManager_1_OnLoggedOut_m1113730087_gshared (FBManager_1_t2091981452 * __this, bool ___online0, const MethodInfo* method);
#define FBManager_1_OnLoggedOut_m1113730087(__this, ___online0, method) ((  void (*) (FBManager_1_t2091981452 *, bool, const MethodInfo*))FBManager_1_OnLoggedOut_m1113730087_gshared)(__this, ___online0, method)
// System.Boolean FBManager`1<System.Object>::GetUserLoginStatus()
extern "C"  bool FBManager_1_GetUserLoginStatus_m3132348805_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_GetUserLoginStatus_m3132348805(__this, method) ((  bool (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_GetUserLoginStatus_m3132348805_gshared)(__this, method)
// System.Boolean FBManager`1<System.Object>::GetFacebookLoginStatus()
extern "C"  bool FBManager_1_GetFacebookLoginStatus_m1222898994_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_GetFacebookLoginStatus_m1222898994(__this, method) ((  bool (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_GetFacebookLoginStatus_m1222898994_gshared)(__this, method)
// FacebookPlayer FBManager`1<System.Object>::GetLoggedInUser()
extern "C"  FacebookPlayer_t2645999341 * FBManager_1_GetLoggedInUser_m2334710691_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_GetLoggedInUser_m2334710691(__this, method) ((  FacebookPlayer_t2645999341 * (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_GetLoggedInUser_m2334710691_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::PostHighScore(System.Int32)
extern "C"  void FBManager_1_PostHighScore_m3979228406_gshared (FBManager_1_t2091981452 * __this, int32_t ___score0, const MethodInfo* method);
#define FBManager_1_PostHighScore_m3979228406(__this, ___score0, method) ((  void (*) (FBManager_1_t2091981452 *, int32_t, const MethodInfo*))FBManager_1_PostHighScore_m3979228406_gshared)(__this, ___score0, method)
// System.Void FBManager`1<System.Object>::_PostHighScore(System.Int32)
extern "C"  void FBManager_1__PostHighScore_m401835617_gshared (FBManager_1_t2091981452 * __this, int32_t ___score0, const MethodInfo* method);
#define FBManager_1__PostHighScore_m401835617(__this, ___score0, method) ((  void (*) (FBManager_1_t2091981452 *, int32_t, const MethodInfo*))FBManager_1__PostHighScore_m401835617_gshared)(__this, ___score0, method)
// System.Collections.Generic.List`1<System.Collections.Hashtable> FBManager`1<System.Object>::GetAppFriendsScoreList()
extern "C"  List_1_t278961118 * FBManager_1_GetAppFriendsScoreList_m238380784_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_GetAppFriendsScoreList_m238380784(__this, method) ((  List_1_t278961118 * (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_GetAppFriendsScoreList_m238380784_gshared)(__this, method)
// System.Collections.IEnumerator FBManager`1<System.Object>::CheckScoreUpdate()
extern "C"  Il2CppObject * FBManager_1_CheckScoreUpdate_m308373672_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_CheckScoreUpdate_m308373672(__this, method) ((  Il2CppObject * (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_CheckScoreUpdate_m308373672_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::PostStory(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void FBManager_1_PostStory_m3776293142_gshared (FBManager_1_t2091981452 * __this, String_t* ___storyName0, String_t* ___storyCaption1, String_t* ___storyDescription2, String_t* ___imageURL3, String_t* ___linkURL4, String_t* ___userID5, String_t* ___mediaURL6, String_t* ___storyReference7, String_t* ___actionLink8, String_t* ___actionLinkName9, const MethodInfo* method);
#define FBManager_1_PostStory_m3776293142(__this, ___storyName0, ___storyCaption1, ___storyDescription2, ___imageURL3, ___linkURL4, ___userID5, ___mediaURL6, ___storyReference7, ___actionLink8, ___actionLinkName9, method) ((  void (*) (FBManager_1_t2091981452 *, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, const MethodInfo*))FBManager_1_PostStory_m3776293142_gshared)(__this, ___storyName0, ___storyCaption1, ___storyDescription2, ___imageURL3, ___linkURL4, ___userID5, ___mediaURL6, ___storyReference7, ___actionLink8, ___actionLinkName9, method)
// System.Void FBManager`1<System.Object>::PostStoryToFeed()
extern "C"  void FBManager_1_PostStoryToFeed_m2787795195_gshared (FBManager_1_t2091981452 * __this, const MethodInfo* method);
#define FBManager_1_PostStoryToFeed_m2787795195(__this, method) ((  void (*) (FBManager_1_t2091981452 *, const MethodInfo*))FBManager_1_PostStoryToFeed_m2787795195_gshared)(__this, method)
// System.Void FBManager`1<System.Object>::StoryFeedCallback(Facebook.Unity.IShareResult)
extern "C"  void FBManager_1_StoryFeedCallback_m3748462935_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___response0, const MethodInfo* method);
#define FBManager_1_StoryFeedCallback_m3748462935(__this, ___response0, method) ((  void (*) (FBManager_1_t2091981452 *, Il2CppObject *, const MethodInfo*))FBManager_1_StoryFeedCallback_m3748462935_gshared)(__this, ___response0, method)
// System.Void FBManager`1<System.Object>::StoryPosted(System.Boolean,System.Collections.Hashtable)
extern "C"  void FBManager_1_StoryPosted_m1151905406_gshared (FBManager_1_t2091981452 * __this, bool ___success0, Hashtable_t909839986 * ___data1, const MethodInfo* method);
#define FBManager_1_StoryPosted_m1151905406(__this, ___success0, ___data1, method) ((  void (*) (FBManager_1_t2091981452 *, bool, Hashtable_t909839986 *, const MethodInfo*))FBManager_1_StoryPosted_m1151905406_gshared)(__this, ___success0, ___data1, method)
// System.Void FBManager`1<System.Object>::CreateStory(System.Collections.Hashtable)
extern "C"  void FBManager_1_CreateStory_m3878977026_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHashTable0, const MethodInfo* method);
#define FBManager_1_CreateStory_m3878977026(__this, ___StoryHashTable0, method) ((  void (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, const MethodInfo*))FBManager_1_CreateStory_m3878977026_gshared)(__this, ___StoryHashTable0, method)
// System.Collections.Hashtable FBManager`1<System.Object>::BaseStoryHash(System.String,System.String,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_BaseStoryHash_m3957017830_gshared (FBManager_1_t2091981452 * __this, String_t* ___LinkName0, String_t* ___LinkCaption1, String_t* ___LinkDescription2, const MethodInfo* method);
#define FBManager_1_BaseStoryHash_m3957017830(__this, ___LinkName0, ___LinkCaption1, ___LinkDescription2, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, String_t*, String_t*, String_t*, const MethodInfo*))FBManager_1_BaseStoryHash_m3957017830_gshared)(__this, ___LinkName0, ___LinkCaption1, ___LinkDescription2, method)
// System.Collections.Hashtable FBManager`1<System.Object>::AddImageToStory(System.Collections.Hashtable,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_AddImageToStory_m964751696_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___ImageURL1, const MethodInfo* method);
#define FBManager_1_AddImageToStory_m964751696(__this, ___StoryHash0, ___ImageURL1, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))FBManager_1_AddImageToStory_m964751696_gshared)(__this, ___StoryHash0, ___ImageURL1, method)
// System.Collections.Hashtable FBManager`1<System.Object>::AddLinkToStory(System.Collections.Hashtable,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_AddLinkToStory_m1070610637_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___LinkURL1, const MethodInfo* method);
#define FBManager_1_AddLinkToStory_m1070610637(__this, ___StoryHash0, ___LinkURL1, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))FBManager_1_AddLinkToStory_m1070610637_gshared)(__this, ___StoryHash0, ___LinkURL1, method)
// System.Collections.Hashtable FBManager`1<System.Object>::AddUserIDToStory(System.Collections.Hashtable,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_AddUserIDToStory_m1119652137_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___UserID1, const MethodInfo* method);
#define FBManager_1_AddUserIDToStory_m1119652137(__this, ___StoryHash0, ___UserID1, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))FBManager_1_AddUserIDToStory_m1119652137_gshared)(__this, ___StoryHash0, ___UserID1, method)
// System.Collections.Hashtable FBManager`1<System.Object>::AddMediaToStory(System.Collections.Hashtable,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_AddMediaToStory_m622260141_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___MediaURL1, const MethodInfo* method);
#define FBManager_1_AddMediaToStory_m622260141(__this, ___StoryHash0, ___MediaURL1, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))FBManager_1_AddMediaToStory_m622260141_gshared)(__this, ___StoryHash0, ___MediaURL1, method)
// System.Collections.Hashtable FBManager`1<System.Object>::AddReferenceToStory(System.Collections.Hashtable,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_AddReferenceToStory_m4103099812_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___StoryReference1, const MethodInfo* method);
#define FBManager_1_AddReferenceToStory_m4103099812(__this, ___StoryHash0, ___StoryReference1, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))FBManager_1_AddReferenceToStory_m4103099812_gshared)(__this, ___StoryHash0, ___StoryReference1, method)
// System.Collections.Hashtable FBManager`1<System.Object>::AddActionLinkToStory(System.Collections.Hashtable,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_AddActionLinkToStory_m508191515_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___ActionLink1, const MethodInfo* method);
#define FBManager_1_AddActionLinkToStory_m508191515(__this, ___StoryHash0, ___ActionLink1, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))FBManager_1_AddActionLinkToStory_m508191515_gshared)(__this, ___StoryHash0, ___ActionLink1, method)
// System.Collections.Hashtable FBManager`1<System.Object>::AddActionNameToStory(System.Collections.Hashtable,System.String)
extern "C"  Hashtable_t909839986 * FBManager_1_AddActionNameToStory_m453440656_gshared (FBManager_1_t2091981452 * __this, Hashtable_t909839986 * ___StoryHash0, String_t* ___ActionLinkName1, const MethodInfo* method);
#define FBManager_1_AddActionNameToStory_m453440656(__this, ___StoryHash0, ___ActionLinkName1, method) ((  Hashtable_t909839986 * (*) (FBManager_1_t2091981452 *, Hashtable_t909839986 *, String_t*, const MethodInfo*))FBManager_1_AddActionNameToStory_m453440656_gshared)(__this, ___StoryHash0, ___ActionLinkName1, method)
// System.Void FBManager`1<System.Object>::LoadPictureAPI(System.String,FBManager`1/LoadPictureCallback<ManagerType>)
extern "C"  void FBManager_1_LoadPictureAPI_m1618865842_gshared (FBManager_1_t2091981452 * __this, String_t* ___url0, LoadPictureCallback_t2854975038 * ___callback1, const MethodInfo* method);
#define FBManager_1_LoadPictureAPI_m1618865842(__this, ___url0, ___callback1, method) ((  void (*) (FBManager_1_t2091981452 *, String_t*, LoadPictureCallback_t2854975038 *, const MethodInfo*))FBManager_1_LoadPictureAPI_m1618865842_gshared)(__this, ___url0, ___callback1, method)
// System.Collections.IEnumerator FBManager`1<System.Object>::LoadPictureEnumerator(System.String,FBManager`1/LoadPictureCallback<ManagerType>)
extern "C"  Il2CppObject * FBManager_1_LoadPictureEnumerator_m2946817620_gshared (FBManager_1_t2091981452 * __this, String_t* ___url0, LoadPictureCallback_t2854975038 * ___callback1, const MethodInfo* method);
#define FBManager_1_LoadPictureEnumerator_m2946817620(__this, ___url0, ___callback1, method) ((  Il2CppObject * (*) (FBManager_1_t2091981452 *, String_t*, LoadPictureCallback_t2854975038 *, const MethodInfo*))FBManager_1_LoadPictureEnumerator_m2946817620_gshared)(__this, ___url0, ___callback1, method)
// System.Void FBManager`1<System.Object>::MyPictureCallback(UnityEngine.Texture)
extern "C"  void FBManager_1_MyPictureCallback_m4066877448_gshared (FBManager_1_t2091981452 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method);
#define FBManager_1_MyPictureCallback_m4066877448(__this, ___texture0, method) ((  void (*) (FBManager_1_t2091981452 *, Texture_t2243626319 *, const MethodInfo*))FBManager_1_MyPictureCallback_m4066877448_gshared)(__this, ___texture0, method)
// System.String FBManager`1<System.Object>::DeserializePictureURLString(System.String)
extern "C"  String_t* FBManager_1_DeserializePictureURLString_m1044315199_gshared (FBManager_1_t2091981452 * __this, String_t* ___response0, const MethodInfo* method);
#define FBManager_1_DeserializePictureURLString_m1044315199(__this, ___response0, method) ((  String_t* (*) (FBManager_1_t2091981452 *, String_t*, const MethodInfo*))FBManager_1_DeserializePictureURLString_m1044315199_gshared)(__this, ___response0, method)
// System.String FBManager`1<System.Object>::DeserializePictureURLObject(System.Object)
extern "C"  String_t* FBManager_1_DeserializePictureURLObject_m1113785023_gshared (FBManager_1_t2091981452 * __this, Il2CppObject * ___pictureObj0, const MethodInfo* method);
#define FBManager_1_DeserializePictureURLObject_m1113785023(__this, ___pictureObj0, method) ((  String_t* (*) (FBManager_1_t2091981452 *, Il2CppObject *, const MethodInfo*))FBManager_1_DeserializePictureURLObject_m1113785023_gshared)(__this, ___pictureObj0, method)
// System.String FBManager`1<System.Object>::GetPictureURL(System.String,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String)
extern "C"  String_t* FBManager_1_GetPictureURL_m2272155829_gshared (Il2CppObject * __this /* static, unused */, String_t* ___facebookID0, Nullable_1_t334943763  ___width1, Nullable_1_t334943763  ___height2, String_t* ___type3, const MethodInfo* method);
#define FBManager_1_GetPictureURL_m2272155829(__this /* static, unused */, ___facebookID0, ___width1, ___height2, ___type3, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, Nullable_1_t334943763 , Nullable_1_t334943763 , String_t*, const MethodInfo*))FBManager_1_GetPictureURL_m2272155829_gshared)(__this /* static, unused */, ___facebookID0, ___width1, ___height2, ___type3, method)
// System.Void FBManager`1<System.Object>::.cctor()
extern "C"  void FBManager_1__cctor_m129197430_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define FBManager_1__cctor_m129197430(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))FBManager_1__cctor_m129197430_gshared)(__this /* static, unused */, method)
// System.Void FBManager`1<System.Object>::<_PostHighScore>m__0(Facebook.Unity.IGraphResult)
extern "C"  void FBManager_1_U3C_PostHighScoreU3Em__0_m235639230_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, const MethodInfo* method);
#define FBManager_1_U3C_PostHighScoreU3Em__0_m235639230(__this /* static, unused */, ___result0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))FBManager_1_U3C_PostHighScoreU3Em__0_m235639230_gshared)(__this /* static, unused */, ___result0, method)
