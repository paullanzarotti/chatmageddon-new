﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MessageKeyboardInput
struct MessageKeyboardInput_t4224908926;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyboardSendButton
struct  KeyboardSendButton_t1707295269  : public SFXButton_t792651341
{
public:
	// MessageKeyboardInput KeyboardSendButton::keyboardInput
	MessageKeyboardInput_t4224908926 * ___keyboardInput_5;

public:
	inline static int32_t get_offset_of_keyboardInput_5() { return static_cast<int32_t>(offsetof(KeyboardSendButton_t1707295269, ___keyboardInput_5)); }
	inline MessageKeyboardInput_t4224908926 * get_keyboardInput_5() const { return ___keyboardInput_5; }
	inline MessageKeyboardInput_t4224908926 ** get_address_of_keyboardInput_5() { return &___keyboardInput_5; }
	inline void set_keyboardInput_5(MessageKeyboardInput_t4224908926 * value)
	{
		___keyboardInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___keyboardInput_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
