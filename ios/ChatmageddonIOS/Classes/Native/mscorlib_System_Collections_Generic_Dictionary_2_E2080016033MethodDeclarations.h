﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2080016033.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22812303849.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2699542651_gshared (Enumerator_t2080016033 * __this, Dictionary_2_t759991331 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2699542651(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2080016033 *, Dictionary_2_t759991331 *, const MethodInfo*))Enumerator__ctor_m2699542651_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1705693580_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1705693580(__this, method) ((  Il2CppObject * (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1705693580_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3796232168_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3796232168(__this, method) ((  void (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3796232168_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2672129781_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2672129781(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2672129781_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m354147850_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m354147850(__this, method) ((  Il2CppObject * (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m354147850_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2864623104_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2864623104(__this, method) ((  Il2CppObject * (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2864623104_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2383441737_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2383441737(__this, method) ((  bool (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_MoveNext_m2383441737_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::get_Current()
extern "C"  KeyValuePair_2_t2812303849  Enumerator_get_Current_m605365090_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m605365090(__this, method) ((  KeyValuePair_2_t2812303849  (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_get_Current_m605365090_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::get_CurrentKey()
extern "C"  Il2CppChar Enumerator_get_CurrentKey_m2450514375_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2450514375(__this, method) ((  Il2CppChar (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_get_CurrentKey_m2450514375_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::get_CurrentValue()
extern "C"  Il2CppChar Enumerator_get_CurrentValue_m2657618151_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2657618151(__this, method) ((  Il2CppChar (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_get_CurrentValue_m2657618151_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::Reset()
extern "C"  void Enumerator_Reset_m4025182717_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_Reset_m4025182717(__this, method) ((  void (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_Reset_m4025182717_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3911403892_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3911403892(__this, method) ((  void (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_VerifyState_m3911403892_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1862247088_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1862247088(__this, method) ((  void (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_VerifyCurrent_m1862247088_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Char,System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m1378568567_gshared (Enumerator_t2080016033 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1378568567(__this, method) ((  void (*) (Enumerator_t2080016033 *, const MethodInfo*))Enumerator_Dispose_m1378568567_gshared)(__this, method)
