﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeCenterUIScaler
struct  HomeCenterUIScaler_t3067528588  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UITexture HomeCenterUIScaler::topGradient
	UITexture_t2537039969 * ___topGradient_14;
	// UITexture HomeCenterUIScaler::bottomGradient
	UITexture_t2537039969 * ___bottomGradient_15;
	// UISprite HomeCenterUIScaler::mapFade
	UISprite_t603616735 * ___mapFade_16;

public:
	inline static int32_t get_offset_of_topGradient_14() { return static_cast<int32_t>(offsetof(HomeCenterUIScaler_t3067528588, ___topGradient_14)); }
	inline UITexture_t2537039969 * get_topGradient_14() const { return ___topGradient_14; }
	inline UITexture_t2537039969 ** get_address_of_topGradient_14() { return &___topGradient_14; }
	inline void set_topGradient_14(UITexture_t2537039969 * value)
	{
		___topGradient_14 = value;
		Il2CppCodeGenWriteBarrier(&___topGradient_14, value);
	}

	inline static int32_t get_offset_of_bottomGradient_15() { return static_cast<int32_t>(offsetof(HomeCenterUIScaler_t3067528588, ___bottomGradient_15)); }
	inline UITexture_t2537039969 * get_bottomGradient_15() const { return ___bottomGradient_15; }
	inline UITexture_t2537039969 ** get_address_of_bottomGradient_15() { return &___bottomGradient_15; }
	inline void set_bottomGradient_15(UITexture_t2537039969 * value)
	{
		___bottomGradient_15 = value;
		Il2CppCodeGenWriteBarrier(&___bottomGradient_15, value);
	}

	inline static int32_t get_offset_of_mapFade_16() { return static_cast<int32_t>(offsetof(HomeCenterUIScaler_t3067528588, ___mapFade_16)); }
	inline UISprite_t603616735 * get_mapFade_16() const { return ___mapFade_16; }
	inline UISprite_t603616735 ** get_address_of_mapFade_16() { return &___mapFade_16; }
	inline void set_mapFade_16(UISprite_t603616735 * value)
	{
		___mapFade_16 = value;
		Il2CppCodeGenWriteBarrier(&___mapFade_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
