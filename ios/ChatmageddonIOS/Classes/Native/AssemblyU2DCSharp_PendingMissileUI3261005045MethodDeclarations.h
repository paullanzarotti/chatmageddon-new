﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PendingMissileUI
struct PendingMissileUI_t3261005045;

#include "codegen/il2cpp-codegen.h"

// System.Void PendingMissileUI::.ctor()
extern "C"  void PendingMissileUI__ctor_m1381375574 (PendingMissileUI_t3261005045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PendingMissileUI::OnEnable()
extern "C"  void PendingMissileUI_OnEnable_m1992748406 (PendingMissileUI_t3261005045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PendingMissileUI::OnDisable()
extern "C"  void PendingMissileUI_OnDisable_m1488707081 (PendingMissileUI_t3261005045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PendingMissileUI::LoadUI(System.Boolean)
extern "C"  void PendingMissileUI_LoadUI_m1107238665 (PendingMissileUI_t3261005045 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PendingMissileUI::UnloadUI(System.Boolean)
extern "C"  void PendingMissileUI_UnloadUI_m2575978512 (PendingMissileUI_t3261005045 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PendingMissileUI::OnScaleTweenFinished()
extern "C"  void PendingMissileUI_OnScaleTweenFinished_m3270866114 (PendingMissileUI_t3261005045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
