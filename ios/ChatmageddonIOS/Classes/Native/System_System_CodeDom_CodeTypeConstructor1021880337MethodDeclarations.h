﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeTypeConstructor
struct CodeTypeConstructor_t1021880337;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeTypeConstructor::.ctor()
extern "C"  void CodeTypeConstructor__ctor_m1632919493 (CodeTypeConstructor_t1021880337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
