﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.P31Error
struct  P31Error_t2856600608  : public Il2CppObject
{
public:
	// System.String Prime31.P31Error::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_0;
	// System.String Prime31.P31Error::<domain>k__BackingField
	String_t* ___U3CdomainU3Ek__BackingField_1;
	// System.Int32 Prime31.P31Error::<code>k__BackingField
	int32_t ___U3CcodeU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.P31Error::<userInfo>k__BackingField
	Dictionary_2_t309261261 * ___U3CuserInfoU3Ek__BackingField_3;
	// System.Boolean Prime31.P31Error::_containsOnlyMessage
	bool ____containsOnlyMessage_4;

public:
	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(P31Error_t2856600608, ___U3CmessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_0() const { return ___U3CmessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_0() { return &___U3CmessageU3Ek__BackingField_0; }
	inline void set_U3CmessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CdomainU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(P31Error_t2856600608, ___U3CdomainU3Ek__BackingField_1)); }
	inline String_t* get_U3CdomainU3Ek__BackingField_1() const { return ___U3CdomainU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdomainU3Ek__BackingField_1() { return &___U3CdomainU3Ek__BackingField_1; }
	inline void set_U3CdomainU3Ek__BackingField_1(String_t* value)
	{
		___U3CdomainU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdomainU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CcodeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(P31Error_t2856600608, ___U3CcodeU3Ek__BackingField_2)); }
	inline int32_t get_U3CcodeU3Ek__BackingField_2() const { return ___U3CcodeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CcodeU3Ek__BackingField_2() { return &___U3CcodeU3Ek__BackingField_2; }
	inline void set_U3CcodeU3Ek__BackingField_2(int32_t value)
	{
		___U3CcodeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CuserInfoU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(P31Error_t2856600608, ___U3CuserInfoU3Ek__BackingField_3)); }
	inline Dictionary_2_t309261261 * get_U3CuserInfoU3Ek__BackingField_3() const { return ___U3CuserInfoU3Ek__BackingField_3; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CuserInfoU3Ek__BackingField_3() { return &___U3CuserInfoU3Ek__BackingField_3; }
	inline void set_U3CuserInfoU3Ek__BackingField_3(Dictionary_2_t309261261 * value)
	{
		___U3CuserInfoU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuserInfoU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of__containsOnlyMessage_4() { return static_cast<int32_t>(offsetof(P31Error_t2856600608, ____containsOnlyMessage_4)); }
	inline bool get__containsOnlyMessage_4() const { return ____containsOnlyMessage_4; }
	inline bool* get_address_of__containsOnlyMessage_4() { return &____containsOnlyMessage_4; }
	inline void set__containsOnlyMessage_4(bool value)
	{
		____containsOnlyMessage_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
