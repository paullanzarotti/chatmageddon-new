﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BooleanSwitch
struct BooleanSwitch_t869062888;

#include "codegen/il2cpp-codegen.h"

// System.Void BooleanSwitch::.ctor()
extern "C"  void BooleanSwitch__ctor_m3448689217 (BooleanSwitch_t869062888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BooleanSwitch::SetSwitchStart(System.Boolean)
extern "C"  void BooleanSwitch_SetSwitchStart_m2067646506 (BooleanSwitch_t869062888 * __this, bool ___startState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BooleanSwitch::SwitchState()
extern "C"  void BooleanSwitch_SwitchState_m3884655790 (BooleanSwitch_t869062888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BooleanSwitch::OnSwitch(System.Boolean)
extern "C"  void BooleanSwitch_OnSwitch_m2759227925 (BooleanSwitch_t869062888 * __this, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
