﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Uniject.IStorage
struct IStorage_t1347868490;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransactionDatabase
struct  TransactionDatabase_t201476183  : public Il2CppObject
{
public:
	// Uniject.IStorage TransactionDatabase::storage
	Il2CppObject * ___storage_0;
	// Uniject.ILogger TransactionDatabase::logger
	Il2CppObject * ___logger_1;
	// System.String TransactionDatabase::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_storage_0() { return static_cast<int32_t>(offsetof(TransactionDatabase_t201476183, ___storage_0)); }
	inline Il2CppObject * get_storage_0() const { return ___storage_0; }
	inline Il2CppObject ** get_address_of_storage_0() { return &___storage_0; }
	inline void set_storage_0(Il2CppObject * value)
	{
		___storage_0 = value;
		Il2CppCodeGenWriteBarrier(&___storage_0, value);
	}

	inline static int32_t get_offset_of_logger_1() { return static_cast<int32_t>(offsetof(TransactionDatabase_t201476183, ___logger_1)); }
	inline Il2CppObject * get_logger_1() const { return ___logger_1; }
	inline Il2CppObject ** get_address_of_logger_1() { return &___logger_1; }
	inline void set_logger_1(Il2CppObject * value)
	{
		___logger_1 = value;
		Il2CppCodeGenWriteBarrier(&___logger_1, value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TransactionDatabase_t201476183, ___U3CUserIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_2() const { return ___U3CUserIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_2() { return &___U3CUserIdU3Ek__BackingField_2; }
	inline void set_U3CUserIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserIdU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
