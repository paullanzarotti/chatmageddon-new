﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditEmailNavScreen/<SaveScreenContent>c__Iterator0
struct U3CSaveScreenContentU3Ec__Iterator0_t2708455943;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EditEmailNavScreen/<SaveScreenContent>c__Iterator0::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0__ctor_m2711839136 (U3CSaveScreenContentU3Ec__Iterator0_t2708455943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EditEmailNavScreen/<SaveScreenContent>c__Iterator0::MoveNext()
extern "C"  bool U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m3395895300 (U3CSaveScreenContentU3Ec__Iterator0_t2708455943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EditEmailNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3185297292 (U3CSaveScreenContentU3Ec__Iterator0_t2708455943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EditEmailNavScreen/<SaveScreenContent>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2942542148 (U3CSaveScreenContentU3Ec__Iterator0_t2708455943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditEmailNavScreen/<SaveScreenContent>c__Iterator0::Dispose()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Dispose_m3382489901 (U3CSaveScreenContentU3Ec__Iterator0_t2708455943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditEmailNavScreen/<SaveScreenContent>c__Iterator0::Reset()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Reset_m137480855 (U3CSaveScreenContentU3Ec__Iterator0_t2708455943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
