﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoadingIndicator
struct LoadingIndicator_t3396405409;
// TweenScale
struct TweenScale_t2697902175;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// LeaderboardPlayerItem
struct LeaderboardPlayerItem_t2673913181;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Func`2<User,System.Int32>
struct Func_2_t3029842330;
// System.Func`2<User,System.String>
struct Func_2_t2987185115;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3671457571.h"
#include "AssemblyU2DCSharp_LeaderboardType1980945291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardILP
struct  LeaderboardILP_t694823370  : public BaseInfiniteListPopulator_1_t3671457571
{
public:
	// LoadingIndicator LeaderboardILP::listLoadingIndicator
	LoadingIndicator_t3396405409 * ___listLoadingIndicator_33;
	// System.Boolean LeaderboardILP::allTimeLeaderboard
	bool ___allTimeLeaderboard_34;
	// LeaderboardType LeaderboardILP::currentLoadedType
	int32_t ___currentLoadedType_35;
	// TweenScale LeaderboardILP::leaderboardScaleTween
	TweenScale_t2697902175 * ___leaderboardScaleTween_36;
	// System.Int32 LeaderboardILP::maxNameLength
	int32_t ___maxNameLength_37;
	// System.Boolean LeaderboardILP::listLoaded
	bool ___listLoaded_38;
	// System.Collections.ArrayList LeaderboardILP::leadersArray
	ArrayList_t4252133567 * ___leadersArray_39;
	// LeaderboardPlayerItem LeaderboardILP::playerUIItem
	LeaderboardPlayerItem_t2673913181 * ___playerUIItem_40;
	// System.Boolean LeaderboardILP::playerIncluded
	bool ___playerIncluded_41;
	// System.Int32 LeaderboardILP::playerRank
	int32_t ___playerRank_42;
	// LeaderboardType LeaderboardILP::activatingType
	int32_t ___activatingType_43;
	// UnityEngine.Coroutine LeaderboardILP::waitRoutine
	Coroutine_t2299508840 * ___waitRoutine_44;
	// UnityEngine.Coroutine LeaderboardILP::worldWaitRoutine
	Coroutine_t2299508840 * ___worldWaitRoutine_45;

public:
	inline static int32_t get_offset_of_listLoadingIndicator_33() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___listLoadingIndicator_33)); }
	inline LoadingIndicator_t3396405409 * get_listLoadingIndicator_33() const { return ___listLoadingIndicator_33; }
	inline LoadingIndicator_t3396405409 ** get_address_of_listLoadingIndicator_33() { return &___listLoadingIndicator_33; }
	inline void set_listLoadingIndicator_33(LoadingIndicator_t3396405409 * value)
	{
		___listLoadingIndicator_33 = value;
		Il2CppCodeGenWriteBarrier(&___listLoadingIndicator_33, value);
	}

	inline static int32_t get_offset_of_allTimeLeaderboard_34() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___allTimeLeaderboard_34)); }
	inline bool get_allTimeLeaderboard_34() const { return ___allTimeLeaderboard_34; }
	inline bool* get_address_of_allTimeLeaderboard_34() { return &___allTimeLeaderboard_34; }
	inline void set_allTimeLeaderboard_34(bool value)
	{
		___allTimeLeaderboard_34 = value;
	}

	inline static int32_t get_offset_of_currentLoadedType_35() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___currentLoadedType_35)); }
	inline int32_t get_currentLoadedType_35() const { return ___currentLoadedType_35; }
	inline int32_t* get_address_of_currentLoadedType_35() { return &___currentLoadedType_35; }
	inline void set_currentLoadedType_35(int32_t value)
	{
		___currentLoadedType_35 = value;
	}

	inline static int32_t get_offset_of_leaderboardScaleTween_36() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___leaderboardScaleTween_36)); }
	inline TweenScale_t2697902175 * get_leaderboardScaleTween_36() const { return ___leaderboardScaleTween_36; }
	inline TweenScale_t2697902175 ** get_address_of_leaderboardScaleTween_36() { return &___leaderboardScaleTween_36; }
	inline void set_leaderboardScaleTween_36(TweenScale_t2697902175 * value)
	{
		___leaderboardScaleTween_36 = value;
		Il2CppCodeGenWriteBarrier(&___leaderboardScaleTween_36, value);
	}

	inline static int32_t get_offset_of_maxNameLength_37() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___maxNameLength_37)); }
	inline int32_t get_maxNameLength_37() const { return ___maxNameLength_37; }
	inline int32_t* get_address_of_maxNameLength_37() { return &___maxNameLength_37; }
	inline void set_maxNameLength_37(int32_t value)
	{
		___maxNameLength_37 = value;
	}

	inline static int32_t get_offset_of_listLoaded_38() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___listLoaded_38)); }
	inline bool get_listLoaded_38() const { return ___listLoaded_38; }
	inline bool* get_address_of_listLoaded_38() { return &___listLoaded_38; }
	inline void set_listLoaded_38(bool value)
	{
		___listLoaded_38 = value;
	}

	inline static int32_t get_offset_of_leadersArray_39() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___leadersArray_39)); }
	inline ArrayList_t4252133567 * get_leadersArray_39() const { return ___leadersArray_39; }
	inline ArrayList_t4252133567 ** get_address_of_leadersArray_39() { return &___leadersArray_39; }
	inline void set_leadersArray_39(ArrayList_t4252133567 * value)
	{
		___leadersArray_39 = value;
		Il2CppCodeGenWriteBarrier(&___leadersArray_39, value);
	}

	inline static int32_t get_offset_of_playerUIItem_40() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___playerUIItem_40)); }
	inline LeaderboardPlayerItem_t2673913181 * get_playerUIItem_40() const { return ___playerUIItem_40; }
	inline LeaderboardPlayerItem_t2673913181 ** get_address_of_playerUIItem_40() { return &___playerUIItem_40; }
	inline void set_playerUIItem_40(LeaderboardPlayerItem_t2673913181 * value)
	{
		___playerUIItem_40 = value;
		Il2CppCodeGenWriteBarrier(&___playerUIItem_40, value);
	}

	inline static int32_t get_offset_of_playerIncluded_41() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___playerIncluded_41)); }
	inline bool get_playerIncluded_41() const { return ___playerIncluded_41; }
	inline bool* get_address_of_playerIncluded_41() { return &___playerIncluded_41; }
	inline void set_playerIncluded_41(bool value)
	{
		___playerIncluded_41 = value;
	}

	inline static int32_t get_offset_of_playerRank_42() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___playerRank_42)); }
	inline int32_t get_playerRank_42() const { return ___playerRank_42; }
	inline int32_t* get_address_of_playerRank_42() { return &___playerRank_42; }
	inline void set_playerRank_42(int32_t value)
	{
		___playerRank_42 = value;
	}

	inline static int32_t get_offset_of_activatingType_43() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___activatingType_43)); }
	inline int32_t get_activatingType_43() const { return ___activatingType_43; }
	inline int32_t* get_address_of_activatingType_43() { return &___activatingType_43; }
	inline void set_activatingType_43(int32_t value)
	{
		___activatingType_43 = value;
	}

	inline static int32_t get_offset_of_waitRoutine_44() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___waitRoutine_44)); }
	inline Coroutine_t2299508840 * get_waitRoutine_44() const { return ___waitRoutine_44; }
	inline Coroutine_t2299508840 ** get_address_of_waitRoutine_44() { return &___waitRoutine_44; }
	inline void set_waitRoutine_44(Coroutine_t2299508840 * value)
	{
		___waitRoutine_44 = value;
		Il2CppCodeGenWriteBarrier(&___waitRoutine_44, value);
	}

	inline static int32_t get_offset_of_worldWaitRoutine_45() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370, ___worldWaitRoutine_45)); }
	inline Coroutine_t2299508840 * get_worldWaitRoutine_45() const { return ___worldWaitRoutine_45; }
	inline Coroutine_t2299508840 ** get_address_of_worldWaitRoutine_45() { return &___worldWaitRoutine_45; }
	inline void set_worldWaitRoutine_45(Coroutine_t2299508840 * value)
	{
		___worldWaitRoutine_45 = value;
		Il2CppCodeGenWriteBarrier(&___worldWaitRoutine_45, value);
	}
};

struct LeaderboardILP_t694823370_StaticFields
{
public:
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cache0
	Func_2_t3029842330 * ___U3CU3Ef__amU24cache0_46;
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cache1
	Func_2_t3029842330 * ___U3CU3Ef__amU24cache1_47;
	// System.Func`2<User,System.String> LeaderboardILP::<>f__am$cache2
	Func_2_t2987185115 * ___U3CU3Ef__amU24cache2_48;
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cache3
	Func_2_t3029842330 * ___U3CU3Ef__amU24cache3_49;
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cache4
	Func_2_t3029842330 * ___U3CU3Ef__amU24cache4_50;
	// System.Func`2<User,System.String> LeaderboardILP::<>f__am$cache5
	Func_2_t2987185115 * ___U3CU3Ef__amU24cache5_51;
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cache6
	Func_2_t3029842330 * ___U3CU3Ef__amU24cache6_52;
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cache7
	Func_2_t3029842330 * ___U3CU3Ef__amU24cache7_53;
	// System.Func`2<User,System.String> LeaderboardILP::<>f__am$cache8
	Func_2_t2987185115 * ___U3CU3Ef__amU24cache8_54;
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cache9
	Func_2_t3029842330 * ___U3CU3Ef__amU24cache9_55;
	// System.Func`2<User,System.Int32> LeaderboardILP::<>f__am$cacheA
	Func_2_t3029842330 * ___U3CU3Ef__amU24cacheA_56;
	// System.Func`2<User,System.String> LeaderboardILP::<>f__am$cacheB
	Func_2_t2987185115 * ___U3CU3Ef__amU24cacheB_57;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_46() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache0_46)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cache0_46() const { return ___U3CU3Ef__amU24cache0_46; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cache0_46() { return &___U3CU3Ef__amU24cache0_46; }
	inline void set_U3CU3Ef__amU24cache0_46(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cache0_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_46, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_47() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache1_47)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cache1_47() const { return ___U3CU3Ef__amU24cache1_47; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cache1_47() { return &___U3CU3Ef__amU24cache1_47; }
	inline void set_U3CU3Ef__amU24cache1_47(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cache1_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_47, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_48() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache2_48)); }
	inline Func_2_t2987185115 * get_U3CU3Ef__amU24cache2_48() const { return ___U3CU3Ef__amU24cache2_48; }
	inline Func_2_t2987185115 ** get_address_of_U3CU3Ef__amU24cache2_48() { return &___U3CU3Ef__amU24cache2_48; }
	inline void set_U3CU3Ef__amU24cache2_48(Func_2_t2987185115 * value)
	{
		___U3CU3Ef__amU24cache2_48 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_48, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_49() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache3_49)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cache3_49() const { return ___U3CU3Ef__amU24cache3_49; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cache3_49() { return &___U3CU3Ef__amU24cache3_49; }
	inline void set_U3CU3Ef__amU24cache3_49(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cache3_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_49, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_50() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache4_50)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cache4_50() const { return ___U3CU3Ef__amU24cache4_50; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cache4_50() { return &___U3CU3Ef__amU24cache4_50; }
	inline void set_U3CU3Ef__amU24cache4_50(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cache4_50 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_50, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_51() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache5_51)); }
	inline Func_2_t2987185115 * get_U3CU3Ef__amU24cache5_51() const { return ___U3CU3Ef__amU24cache5_51; }
	inline Func_2_t2987185115 ** get_address_of_U3CU3Ef__amU24cache5_51() { return &___U3CU3Ef__amU24cache5_51; }
	inline void set_U3CU3Ef__amU24cache5_51(Func_2_t2987185115 * value)
	{
		___U3CU3Ef__amU24cache5_51 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_51, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_52() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache6_52)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cache6_52() const { return ___U3CU3Ef__amU24cache6_52; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cache6_52() { return &___U3CU3Ef__amU24cache6_52; }
	inline void set_U3CU3Ef__amU24cache6_52(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cache6_52 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_52, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_53() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache7_53)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cache7_53() const { return ___U3CU3Ef__amU24cache7_53; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cache7_53() { return &___U3CU3Ef__amU24cache7_53; }
	inline void set_U3CU3Ef__amU24cache7_53(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cache7_53 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_53, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_54() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache8_54)); }
	inline Func_2_t2987185115 * get_U3CU3Ef__amU24cache8_54() const { return ___U3CU3Ef__amU24cache8_54; }
	inline Func_2_t2987185115 ** get_address_of_U3CU3Ef__amU24cache8_54() { return &___U3CU3Ef__amU24cache8_54; }
	inline void set_U3CU3Ef__amU24cache8_54(Func_2_t2987185115 * value)
	{
		___U3CU3Ef__amU24cache8_54 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_54, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_55() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cache9_55)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cache9_55() const { return ___U3CU3Ef__amU24cache9_55; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cache9_55() { return &___U3CU3Ef__amU24cache9_55; }
	inline void set_U3CU3Ef__amU24cache9_55(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cache9_55 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_55, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_56() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cacheA_56)); }
	inline Func_2_t3029842330 * get_U3CU3Ef__amU24cacheA_56() const { return ___U3CU3Ef__amU24cacheA_56; }
	inline Func_2_t3029842330 ** get_address_of_U3CU3Ef__amU24cacheA_56() { return &___U3CU3Ef__amU24cacheA_56; }
	inline void set_U3CU3Ef__amU24cacheA_56(Func_2_t3029842330 * value)
	{
		___U3CU3Ef__amU24cacheA_56 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_56, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_57() { return static_cast<int32_t>(offsetof(LeaderboardILP_t694823370_StaticFields, ___U3CU3Ef__amU24cacheB_57)); }
	inline Func_2_t2987185115 * get_U3CU3Ef__amU24cacheB_57() const { return ___U3CU3Ef__amU24cacheB_57; }
	inline Func_2_t2987185115 ** get_address_of_U3CU3Ef__amU24cacheB_57() { return &___U3CU3Ef__amU24cacheB_57; }
	inline void set_U3CU3Ef__amU24cacheB_57(Func_2_t2987185115 * value)
	{
		___U3CU3Ef__amU24cacheB_57 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_57, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
