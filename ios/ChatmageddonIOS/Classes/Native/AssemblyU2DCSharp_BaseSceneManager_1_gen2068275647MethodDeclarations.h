﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseSceneManager`1<System.Object>
struct BaseSceneManager_1_t2068275647;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AnchorPoint970610655.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void BaseSceneManager`1<System.Object>::.ctor()
extern "C"  void BaseSceneManager_1__ctor_m53509762_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1__ctor_m53509762(__this, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<System.Object>::Awake()
extern "C"  void BaseSceneManager_1_Awake_m1676433883_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1_Awake_m1676433883(__this, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<System.Object>::Start()
extern "C"  void BaseSceneManager_1_Start_m2010791446_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1_Start_m2010791446(__this, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<System.Object>::InitScene()
extern "C"  void BaseSceneManager_1_InitScene_m147665072_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1_InitScene_m147665072(__this, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<System.Object>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
extern "C"  void BaseSceneManager_1_AddObjectToScene_m1741167544_gshared (BaseSceneManager_1_t2068275647 * __this, int32_t ___anchor0, GameObject_t1756533147 * ___objectToAdd1, const MethodInfo* method);
#define BaseSceneManager_1_AddObjectToScene_m1741167544(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<System.Object>::StartScene()
extern "C"  void BaseSceneManager_1_StartScene_m1996485408_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1_StartScene_m1996485408(__this, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<System.Object>::SceneLoad()
extern "C"  Il2CppObject * BaseSceneManager_1_SceneLoad_m614703994_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1_SceneLoad_m614703994(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<System.Object>::UnloadScene()
extern "C"  void BaseSceneManager_1_UnloadScene_m2008479001_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1_UnloadScene_m2008479001(__this, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<System.Object>::ExitScene()
extern "C"  void BaseSceneManager_1_ExitScene_m2148921902_gshared (BaseSceneManager_1_t2068275647 * __this, const MethodInfo* method);
#define BaseSceneManager_1_ExitScene_m2148921902(__this, method) ((  void (*) (BaseSceneManager_1_t2068275647 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
