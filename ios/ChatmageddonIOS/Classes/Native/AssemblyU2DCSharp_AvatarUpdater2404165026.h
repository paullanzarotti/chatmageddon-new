﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;
// System.String
struct String_t;
// FirstStrikeUI
struct FirstStrikeUI_t2168768222;
// AvatarChatUI
struct AvatarChatUI_t2524884989;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AvatarUpdater
struct  AvatarUpdater_t2404165026  : public MonoBehaviour_t1158329972
{
public:
	// UITexture AvatarUpdater::avatarTexture
	UITexture_t2537039969 * ___avatarTexture_2;
	// UISprite AvatarUpdater::avatarOverlay
	UISprite_t603616735 * ___avatarOverlay_3;
	// UISprite AvatarUpdater::avatarOutline
	UISprite_t603616735 * ___avatarOutline_4;
	// UILabel AvatarUpdater::initialsLabel
	UILabel_t1795115428 * ___initialsLabel_5;
	// System.Boolean AvatarUpdater::player
	bool ___player_6;
	// System.Boolean AvatarUpdater::initialsOnly
	bool ___initialsOnly_7;
	// UnityEngine.Color AvatarUpdater::outlineColour
	Color_t2020392075  ___outlineColour_8;
	// UnityEngine.Vector2 AvatarUpdater::outlineSize
	Vector2_t2243707579  ___outlineSize_9;
	// UnityEngine.Vector3 AvatarUpdater::initialsOffset
	Vector3_t2243707580  ___initialsOffset_10;
	// System.Boolean AvatarUpdater::activeNemesis
	bool ___activeNemesis_11;
	// UILabel AvatarUpdater::nemesisLabel
	UILabel_t1795115428 * ___nemesisLabel_12;
	// UnityEngine.Color AvatarUpdater::nemesisColour
	Color_t2020392075  ___nemesisColour_13;
	// UnityEngine.Vector2 AvatarUpdater::nemesisSize
	Vector2_t2243707579  ___nemesisSize_14;
	// UnityEngine.Vector3 AvatarUpdater::nemesisOffset
	Vector3_t2243707580  ___nemesisOffset_15;
	// System.String AvatarUpdater::outlineSpriteName
	String_t* ___outlineSpriteName_16;
	// System.String AvatarUpdater::nemesisSpriteName
	String_t* ___nemesisSpriteName_17;
	// System.Boolean AvatarUpdater::showFirstStrike
	bool ___showFirstStrike_18;
	// FirstStrikeUI AvatarUpdater::firstStrikeUI
	FirstStrikeUI_t2168768222 * ___firstStrikeUI_19;
	// System.Boolean AvatarUpdater::showChat
	bool ___showChat_20;
	// AvatarChatUI AvatarUpdater::chatUI
	AvatarChatUI_t2524884989 * ___chatUI_21;

public:
	inline static int32_t get_offset_of_avatarTexture_2() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___avatarTexture_2)); }
	inline UITexture_t2537039969 * get_avatarTexture_2() const { return ___avatarTexture_2; }
	inline UITexture_t2537039969 ** get_address_of_avatarTexture_2() { return &___avatarTexture_2; }
	inline void set_avatarTexture_2(UITexture_t2537039969 * value)
	{
		___avatarTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___avatarTexture_2, value);
	}

	inline static int32_t get_offset_of_avatarOverlay_3() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___avatarOverlay_3)); }
	inline UISprite_t603616735 * get_avatarOverlay_3() const { return ___avatarOverlay_3; }
	inline UISprite_t603616735 ** get_address_of_avatarOverlay_3() { return &___avatarOverlay_3; }
	inline void set_avatarOverlay_3(UISprite_t603616735 * value)
	{
		___avatarOverlay_3 = value;
		Il2CppCodeGenWriteBarrier(&___avatarOverlay_3, value);
	}

	inline static int32_t get_offset_of_avatarOutline_4() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___avatarOutline_4)); }
	inline UISprite_t603616735 * get_avatarOutline_4() const { return ___avatarOutline_4; }
	inline UISprite_t603616735 ** get_address_of_avatarOutline_4() { return &___avatarOutline_4; }
	inline void set_avatarOutline_4(UISprite_t603616735 * value)
	{
		___avatarOutline_4 = value;
		Il2CppCodeGenWriteBarrier(&___avatarOutline_4, value);
	}

	inline static int32_t get_offset_of_initialsLabel_5() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___initialsLabel_5)); }
	inline UILabel_t1795115428 * get_initialsLabel_5() const { return ___initialsLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_initialsLabel_5() { return &___initialsLabel_5; }
	inline void set_initialsLabel_5(UILabel_t1795115428 * value)
	{
		___initialsLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___initialsLabel_5, value);
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___player_6)); }
	inline bool get_player_6() const { return ___player_6; }
	inline bool* get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(bool value)
	{
		___player_6 = value;
	}

	inline static int32_t get_offset_of_initialsOnly_7() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___initialsOnly_7)); }
	inline bool get_initialsOnly_7() const { return ___initialsOnly_7; }
	inline bool* get_address_of_initialsOnly_7() { return &___initialsOnly_7; }
	inline void set_initialsOnly_7(bool value)
	{
		___initialsOnly_7 = value;
	}

	inline static int32_t get_offset_of_outlineColour_8() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___outlineColour_8)); }
	inline Color_t2020392075  get_outlineColour_8() const { return ___outlineColour_8; }
	inline Color_t2020392075 * get_address_of_outlineColour_8() { return &___outlineColour_8; }
	inline void set_outlineColour_8(Color_t2020392075  value)
	{
		___outlineColour_8 = value;
	}

	inline static int32_t get_offset_of_outlineSize_9() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___outlineSize_9)); }
	inline Vector2_t2243707579  get_outlineSize_9() const { return ___outlineSize_9; }
	inline Vector2_t2243707579 * get_address_of_outlineSize_9() { return &___outlineSize_9; }
	inline void set_outlineSize_9(Vector2_t2243707579  value)
	{
		___outlineSize_9 = value;
	}

	inline static int32_t get_offset_of_initialsOffset_10() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___initialsOffset_10)); }
	inline Vector3_t2243707580  get_initialsOffset_10() const { return ___initialsOffset_10; }
	inline Vector3_t2243707580 * get_address_of_initialsOffset_10() { return &___initialsOffset_10; }
	inline void set_initialsOffset_10(Vector3_t2243707580  value)
	{
		___initialsOffset_10 = value;
	}

	inline static int32_t get_offset_of_activeNemesis_11() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___activeNemesis_11)); }
	inline bool get_activeNemesis_11() const { return ___activeNemesis_11; }
	inline bool* get_address_of_activeNemesis_11() { return &___activeNemesis_11; }
	inline void set_activeNemesis_11(bool value)
	{
		___activeNemesis_11 = value;
	}

	inline static int32_t get_offset_of_nemesisLabel_12() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___nemesisLabel_12)); }
	inline UILabel_t1795115428 * get_nemesisLabel_12() const { return ___nemesisLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_nemesisLabel_12() { return &___nemesisLabel_12; }
	inline void set_nemesisLabel_12(UILabel_t1795115428 * value)
	{
		___nemesisLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___nemesisLabel_12, value);
	}

	inline static int32_t get_offset_of_nemesisColour_13() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___nemesisColour_13)); }
	inline Color_t2020392075  get_nemesisColour_13() const { return ___nemesisColour_13; }
	inline Color_t2020392075 * get_address_of_nemesisColour_13() { return &___nemesisColour_13; }
	inline void set_nemesisColour_13(Color_t2020392075  value)
	{
		___nemesisColour_13 = value;
	}

	inline static int32_t get_offset_of_nemesisSize_14() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___nemesisSize_14)); }
	inline Vector2_t2243707579  get_nemesisSize_14() const { return ___nemesisSize_14; }
	inline Vector2_t2243707579 * get_address_of_nemesisSize_14() { return &___nemesisSize_14; }
	inline void set_nemesisSize_14(Vector2_t2243707579  value)
	{
		___nemesisSize_14 = value;
	}

	inline static int32_t get_offset_of_nemesisOffset_15() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___nemesisOffset_15)); }
	inline Vector3_t2243707580  get_nemesisOffset_15() const { return ___nemesisOffset_15; }
	inline Vector3_t2243707580 * get_address_of_nemesisOffset_15() { return &___nemesisOffset_15; }
	inline void set_nemesisOffset_15(Vector3_t2243707580  value)
	{
		___nemesisOffset_15 = value;
	}

	inline static int32_t get_offset_of_outlineSpriteName_16() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___outlineSpriteName_16)); }
	inline String_t* get_outlineSpriteName_16() const { return ___outlineSpriteName_16; }
	inline String_t** get_address_of_outlineSpriteName_16() { return &___outlineSpriteName_16; }
	inline void set_outlineSpriteName_16(String_t* value)
	{
		___outlineSpriteName_16 = value;
		Il2CppCodeGenWriteBarrier(&___outlineSpriteName_16, value);
	}

	inline static int32_t get_offset_of_nemesisSpriteName_17() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___nemesisSpriteName_17)); }
	inline String_t* get_nemesisSpriteName_17() const { return ___nemesisSpriteName_17; }
	inline String_t** get_address_of_nemesisSpriteName_17() { return &___nemesisSpriteName_17; }
	inline void set_nemesisSpriteName_17(String_t* value)
	{
		___nemesisSpriteName_17 = value;
		Il2CppCodeGenWriteBarrier(&___nemesisSpriteName_17, value);
	}

	inline static int32_t get_offset_of_showFirstStrike_18() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___showFirstStrike_18)); }
	inline bool get_showFirstStrike_18() const { return ___showFirstStrike_18; }
	inline bool* get_address_of_showFirstStrike_18() { return &___showFirstStrike_18; }
	inline void set_showFirstStrike_18(bool value)
	{
		___showFirstStrike_18 = value;
	}

	inline static int32_t get_offset_of_firstStrikeUI_19() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___firstStrikeUI_19)); }
	inline FirstStrikeUI_t2168768222 * get_firstStrikeUI_19() const { return ___firstStrikeUI_19; }
	inline FirstStrikeUI_t2168768222 ** get_address_of_firstStrikeUI_19() { return &___firstStrikeUI_19; }
	inline void set_firstStrikeUI_19(FirstStrikeUI_t2168768222 * value)
	{
		___firstStrikeUI_19 = value;
		Il2CppCodeGenWriteBarrier(&___firstStrikeUI_19, value);
	}

	inline static int32_t get_offset_of_showChat_20() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___showChat_20)); }
	inline bool get_showChat_20() const { return ___showChat_20; }
	inline bool* get_address_of_showChat_20() { return &___showChat_20; }
	inline void set_showChat_20(bool value)
	{
		___showChat_20 = value;
	}

	inline static int32_t get_offset_of_chatUI_21() { return static_cast<int32_t>(offsetof(AvatarUpdater_t2404165026, ___chatUI_21)); }
	inline AvatarChatUI_t2524884989 * get_chatUI_21() const { return ___chatUI_21; }
	inline AvatarChatUI_t2524884989 ** get_address_of_chatUI_21() { return &___chatUI_21; }
	inline void set_chatUI_21(AvatarChatUI_t2524884989 * value)
	{
		___chatUI_21 = value;
		Il2CppCodeGenWriteBarrier(&___chatUI_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
