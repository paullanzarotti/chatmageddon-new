﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Contact>
struct List_1_t3383317540;
// System.Collections.Generic.List`1<ValidContact>
struct List_1_t849036066;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Func`2<ValidContact,System.String>
struct Func_2_t2017994764;
// System.Func`2<ValidContact,System.Boolean>
struct Func_2_t3814349249;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddressBook
struct  AddressBook_t411411037  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Contact> AddressBook::abContacts
	List_1_t3383317540 * ___abContacts_2;
	// System.Collections.Generic.List`1<ValidContact> AddressBook::validContacts
	List_1_t849036066 * ___validContacts_3;
	// PhoneNumbers.PhoneNumberUtil AddressBook::util
	PhoneNumberUtil_t4155573397 * ___util_4;
	// System.String AddressBook::CurrentAddressBookChecksum
	String_t* ___CurrentAddressBookChecksum_8;
	// System.Collections.Generic.List`1<ValidContact> AddressBook::existingContacts
	List_1_t849036066 * ___existingContacts_9;
	// System.Collections.Generic.List`1<ValidContact> AddressBook::newContacts
	List_1_t849036066 * ___newContacts_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AddressBook::existingContactsKeyedByRawChecksum
	Dictionary_2_t3986656710 * ___existingContactsKeyedByRawChecksum_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AddressBook::existingContactsKeyedByValidChecksum
	Dictionary_2_t3986656710 * ___existingContactsKeyedByValidChecksum_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AddressBook::newContactsKeyedByRawChecksum
	Dictionary_2_t3986656710 * ___newContactsKeyedByRawChecksum_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AddressBook::newContactsKeyedByValidChecksum
	Dictionary_2_t3986656710 * ___newContactsKeyedByValidChecksum_14;
	// System.Int32 AddressBook::numImportBatches
	int32_t ___numImportBatches_15;
	// System.Int32 AddressBook::returnedImportBatchesCounter
	int32_t ___returnedImportBatchesCounter_16;
	// System.Int32 AddressBook::numDeleteBatches
	int32_t ___numDeleteBatches_17;
	// System.Int32 AddressBook::returnedDeleteBatchesCounter
	int32_t ___returnedDeleteBatchesCounter_18;

public:
	inline static int32_t get_offset_of_abContacts_2() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___abContacts_2)); }
	inline List_1_t3383317540 * get_abContacts_2() const { return ___abContacts_2; }
	inline List_1_t3383317540 ** get_address_of_abContacts_2() { return &___abContacts_2; }
	inline void set_abContacts_2(List_1_t3383317540 * value)
	{
		___abContacts_2 = value;
		Il2CppCodeGenWriteBarrier(&___abContacts_2, value);
	}

	inline static int32_t get_offset_of_validContacts_3() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___validContacts_3)); }
	inline List_1_t849036066 * get_validContacts_3() const { return ___validContacts_3; }
	inline List_1_t849036066 ** get_address_of_validContacts_3() { return &___validContacts_3; }
	inline void set_validContacts_3(List_1_t849036066 * value)
	{
		___validContacts_3 = value;
		Il2CppCodeGenWriteBarrier(&___validContacts_3, value);
	}

	inline static int32_t get_offset_of_util_4() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___util_4)); }
	inline PhoneNumberUtil_t4155573397 * get_util_4() const { return ___util_4; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_util_4() { return &___util_4; }
	inline void set_util_4(PhoneNumberUtil_t4155573397 * value)
	{
		___util_4 = value;
		Il2CppCodeGenWriteBarrier(&___util_4, value);
	}

	inline static int32_t get_offset_of_CurrentAddressBookChecksum_8() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___CurrentAddressBookChecksum_8)); }
	inline String_t* get_CurrentAddressBookChecksum_8() const { return ___CurrentAddressBookChecksum_8; }
	inline String_t** get_address_of_CurrentAddressBookChecksum_8() { return &___CurrentAddressBookChecksum_8; }
	inline void set_CurrentAddressBookChecksum_8(String_t* value)
	{
		___CurrentAddressBookChecksum_8 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentAddressBookChecksum_8, value);
	}

	inline static int32_t get_offset_of_existingContacts_9() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___existingContacts_9)); }
	inline List_1_t849036066 * get_existingContacts_9() const { return ___existingContacts_9; }
	inline List_1_t849036066 ** get_address_of_existingContacts_9() { return &___existingContacts_9; }
	inline void set_existingContacts_9(List_1_t849036066 * value)
	{
		___existingContacts_9 = value;
		Il2CppCodeGenWriteBarrier(&___existingContacts_9, value);
	}

	inline static int32_t get_offset_of_newContacts_10() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___newContacts_10)); }
	inline List_1_t849036066 * get_newContacts_10() const { return ___newContacts_10; }
	inline List_1_t849036066 ** get_address_of_newContacts_10() { return &___newContacts_10; }
	inline void set_newContacts_10(List_1_t849036066 * value)
	{
		___newContacts_10 = value;
		Il2CppCodeGenWriteBarrier(&___newContacts_10, value);
	}

	inline static int32_t get_offset_of_existingContactsKeyedByRawChecksum_11() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___existingContactsKeyedByRawChecksum_11)); }
	inline Dictionary_2_t3986656710 * get_existingContactsKeyedByRawChecksum_11() const { return ___existingContactsKeyedByRawChecksum_11; }
	inline Dictionary_2_t3986656710 ** get_address_of_existingContactsKeyedByRawChecksum_11() { return &___existingContactsKeyedByRawChecksum_11; }
	inline void set_existingContactsKeyedByRawChecksum_11(Dictionary_2_t3986656710 * value)
	{
		___existingContactsKeyedByRawChecksum_11 = value;
		Il2CppCodeGenWriteBarrier(&___existingContactsKeyedByRawChecksum_11, value);
	}

	inline static int32_t get_offset_of_existingContactsKeyedByValidChecksum_12() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___existingContactsKeyedByValidChecksum_12)); }
	inline Dictionary_2_t3986656710 * get_existingContactsKeyedByValidChecksum_12() const { return ___existingContactsKeyedByValidChecksum_12; }
	inline Dictionary_2_t3986656710 ** get_address_of_existingContactsKeyedByValidChecksum_12() { return &___existingContactsKeyedByValidChecksum_12; }
	inline void set_existingContactsKeyedByValidChecksum_12(Dictionary_2_t3986656710 * value)
	{
		___existingContactsKeyedByValidChecksum_12 = value;
		Il2CppCodeGenWriteBarrier(&___existingContactsKeyedByValidChecksum_12, value);
	}

	inline static int32_t get_offset_of_newContactsKeyedByRawChecksum_13() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___newContactsKeyedByRawChecksum_13)); }
	inline Dictionary_2_t3986656710 * get_newContactsKeyedByRawChecksum_13() const { return ___newContactsKeyedByRawChecksum_13; }
	inline Dictionary_2_t3986656710 ** get_address_of_newContactsKeyedByRawChecksum_13() { return &___newContactsKeyedByRawChecksum_13; }
	inline void set_newContactsKeyedByRawChecksum_13(Dictionary_2_t3986656710 * value)
	{
		___newContactsKeyedByRawChecksum_13 = value;
		Il2CppCodeGenWriteBarrier(&___newContactsKeyedByRawChecksum_13, value);
	}

	inline static int32_t get_offset_of_newContactsKeyedByValidChecksum_14() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___newContactsKeyedByValidChecksum_14)); }
	inline Dictionary_2_t3986656710 * get_newContactsKeyedByValidChecksum_14() const { return ___newContactsKeyedByValidChecksum_14; }
	inline Dictionary_2_t3986656710 ** get_address_of_newContactsKeyedByValidChecksum_14() { return &___newContactsKeyedByValidChecksum_14; }
	inline void set_newContactsKeyedByValidChecksum_14(Dictionary_2_t3986656710 * value)
	{
		___newContactsKeyedByValidChecksum_14 = value;
		Il2CppCodeGenWriteBarrier(&___newContactsKeyedByValidChecksum_14, value);
	}

	inline static int32_t get_offset_of_numImportBatches_15() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___numImportBatches_15)); }
	inline int32_t get_numImportBatches_15() const { return ___numImportBatches_15; }
	inline int32_t* get_address_of_numImportBatches_15() { return &___numImportBatches_15; }
	inline void set_numImportBatches_15(int32_t value)
	{
		___numImportBatches_15 = value;
	}

	inline static int32_t get_offset_of_returnedImportBatchesCounter_16() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___returnedImportBatchesCounter_16)); }
	inline int32_t get_returnedImportBatchesCounter_16() const { return ___returnedImportBatchesCounter_16; }
	inline int32_t* get_address_of_returnedImportBatchesCounter_16() { return &___returnedImportBatchesCounter_16; }
	inline void set_returnedImportBatchesCounter_16(int32_t value)
	{
		___returnedImportBatchesCounter_16 = value;
	}

	inline static int32_t get_offset_of_numDeleteBatches_17() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___numDeleteBatches_17)); }
	inline int32_t get_numDeleteBatches_17() const { return ___numDeleteBatches_17; }
	inline int32_t* get_address_of_numDeleteBatches_17() { return &___numDeleteBatches_17; }
	inline void set_numDeleteBatches_17(int32_t value)
	{
		___numDeleteBatches_17 = value;
	}

	inline static int32_t get_offset_of_returnedDeleteBatchesCounter_18() { return static_cast<int32_t>(offsetof(AddressBook_t411411037, ___returnedDeleteBatchesCounter_18)); }
	inline int32_t get_returnedDeleteBatchesCounter_18() const { return ___returnedDeleteBatchesCounter_18; }
	inline int32_t* get_address_of_returnedDeleteBatchesCounter_18() { return &___returnedDeleteBatchesCounter_18; }
	inline void set_returnedDeleteBatchesCounter_18(int32_t value)
	{
		___returnedDeleteBatchesCounter_18 = value;
	}
};

struct AddressBook_t411411037_StaticFields
{
public:
	// System.String AddressBook::TEST_META_DATA_FILE_PREFIX
	String_t* ___TEST_META_DATA_FILE_PREFIX_5;
	// System.Boolean AddressBook::loadedContacts
	bool ___loadedContacts_6;
	// System.Boolean AddressBook::isBulkImport
	bool ___isBulkImport_7;
	// System.Func`2<ValidContact,System.String> AddressBook::<>f__am$cache0
	Func_2_t2017994764 * ___U3CU3Ef__amU24cache0_19;
	// System.Func`2<ValidContact,System.Boolean> AddressBook::<>f__am$cache1
	Func_2_t3814349249 * ___U3CU3Ef__amU24cache1_20;
	// System.Func`2<ValidContact,System.String> AddressBook::<>f__am$cache2
	Func_2_t2017994764 * ___U3CU3Ef__amU24cache2_21;

public:
	inline static int32_t get_offset_of_TEST_META_DATA_FILE_PREFIX_5() { return static_cast<int32_t>(offsetof(AddressBook_t411411037_StaticFields, ___TEST_META_DATA_FILE_PREFIX_5)); }
	inline String_t* get_TEST_META_DATA_FILE_PREFIX_5() const { return ___TEST_META_DATA_FILE_PREFIX_5; }
	inline String_t** get_address_of_TEST_META_DATA_FILE_PREFIX_5() { return &___TEST_META_DATA_FILE_PREFIX_5; }
	inline void set_TEST_META_DATA_FILE_PREFIX_5(String_t* value)
	{
		___TEST_META_DATA_FILE_PREFIX_5 = value;
		Il2CppCodeGenWriteBarrier(&___TEST_META_DATA_FILE_PREFIX_5, value);
	}

	inline static int32_t get_offset_of_loadedContacts_6() { return static_cast<int32_t>(offsetof(AddressBook_t411411037_StaticFields, ___loadedContacts_6)); }
	inline bool get_loadedContacts_6() const { return ___loadedContacts_6; }
	inline bool* get_address_of_loadedContacts_6() { return &___loadedContacts_6; }
	inline void set_loadedContacts_6(bool value)
	{
		___loadedContacts_6 = value;
	}

	inline static int32_t get_offset_of_isBulkImport_7() { return static_cast<int32_t>(offsetof(AddressBook_t411411037_StaticFields, ___isBulkImport_7)); }
	inline bool get_isBulkImport_7() const { return ___isBulkImport_7; }
	inline bool* get_address_of_isBulkImport_7() { return &___isBulkImport_7; }
	inline void set_isBulkImport_7(bool value)
	{
		___isBulkImport_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_19() { return static_cast<int32_t>(offsetof(AddressBook_t411411037_StaticFields, ___U3CU3Ef__amU24cache0_19)); }
	inline Func_2_t2017994764 * get_U3CU3Ef__amU24cache0_19() const { return ___U3CU3Ef__amU24cache0_19; }
	inline Func_2_t2017994764 ** get_address_of_U3CU3Ef__amU24cache0_19() { return &___U3CU3Ef__amU24cache0_19; }
	inline void set_U3CU3Ef__amU24cache0_19(Func_2_t2017994764 * value)
	{
		___U3CU3Ef__amU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_20() { return static_cast<int32_t>(offsetof(AddressBook_t411411037_StaticFields, ___U3CU3Ef__amU24cache1_20)); }
	inline Func_2_t3814349249 * get_U3CU3Ef__amU24cache1_20() const { return ___U3CU3Ef__amU24cache1_20; }
	inline Func_2_t3814349249 ** get_address_of_U3CU3Ef__amU24cache1_20() { return &___U3CU3Ef__amU24cache1_20; }
	inline void set_U3CU3Ef__amU24cache1_20(Func_2_t3814349249 * value)
	{
		___U3CU3Ef__amU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_21() { return static_cast<int32_t>(offsetof(AddressBook_t411411037_StaticFields, ___U3CU3Ef__amU24cache2_21)); }
	inline Func_2_t2017994764 * get_U3CU3Ef__amU24cache2_21() const { return ___U3CU3Ef__amU24cache2_21; }
	inline Func_2_t2017994764 ** get_address_of_U3CU3Ef__amU24cache2_21() { return &___U3CU3Ef__amU24cache2_21; }
	inline void set_U3CU3Ef__amU24cache2_21(Func_2_t2017994764 * value)
	{
		___U3CU3Ef__amU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
