﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GenderSwitch
struct GenderSwitch_t696294539;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GenderNavScreen
struct  GenderNavScreen_t2608406338  : public NavigationScreen_t2333230110
{
public:
	// GenderSwitch GenderNavScreen::genderSwitch
	GenderSwitch_t696294539 * ___genderSwitch_3;
	// System.Boolean GenderNavScreen::UIclosing
	bool ___UIclosing_4;

public:
	inline static int32_t get_offset_of_genderSwitch_3() { return static_cast<int32_t>(offsetof(GenderNavScreen_t2608406338, ___genderSwitch_3)); }
	inline GenderSwitch_t696294539 * get_genderSwitch_3() const { return ___genderSwitch_3; }
	inline GenderSwitch_t696294539 ** get_address_of_genderSwitch_3() { return &___genderSwitch_3; }
	inline void set_genderSwitch_3(GenderSwitch_t696294539 * value)
	{
		___genderSwitch_3 = value;
		Il2CppCodeGenWriteBarrier(&___genderSwitch_3, value);
	}

	inline static int32_t get_offset_of_UIclosing_4() { return static_cast<int32_t>(offsetof(GenderNavScreen_t2608406338, ___UIclosing_4)); }
	inline bool get_UIclosing_4() const { return ___UIclosing_4; }
	inline bool* get_address_of_UIclosing_4() { return &___UIclosing_4; }
	inline void set_UIclosing_4(bool value)
	{
		___UIclosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
