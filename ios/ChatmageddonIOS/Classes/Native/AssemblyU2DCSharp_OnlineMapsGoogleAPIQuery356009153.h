﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Action`1<OnlineMapsGoogleAPIQuery>
struct Action_1_t157808535;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryStatus3400398814.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsGoogleAPIQuery
struct  OnlineMapsGoogleAPIQuery_t356009153  : public Il2CppObject
{
public:
	// System.Action`1<System.String> OnlineMapsGoogleAPIQuery::OnComplete
	Action_1_t1831019615 * ___OnComplete_0;
	// System.Action`1<OnlineMapsGoogleAPIQuery> OnlineMapsGoogleAPIQuery::OnDispose
	Action_1_t157808535 * ___OnDispose_1;
	// System.Action`1<OnlineMapsGoogleAPIQuery> OnlineMapsGoogleAPIQuery::OnFinish
	Action_1_t157808535 * ___OnFinish_2;
	// OnlineMapsQueryStatus OnlineMapsGoogleAPIQuery::_status
	int32_t ____status_3;
	// UnityEngine.WWW OnlineMapsGoogleAPIQuery::www
	WWW_t2919945039 * ___www_4;
	// System.String OnlineMapsGoogleAPIQuery::_response
	String_t* ____response_5;

public:
	inline static int32_t get_offset_of_OnComplete_0() { return static_cast<int32_t>(offsetof(OnlineMapsGoogleAPIQuery_t356009153, ___OnComplete_0)); }
	inline Action_1_t1831019615 * get_OnComplete_0() const { return ___OnComplete_0; }
	inline Action_1_t1831019615 ** get_address_of_OnComplete_0() { return &___OnComplete_0; }
	inline void set_OnComplete_0(Action_1_t1831019615 * value)
	{
		___OnComplete_0 = value;
		Il2CppCodeGenWriteBarrier(&___OnComplete_0, value);
	}

	inline static int32_t get_offset_of_OnDispose_1() { return static_cast<int32_t>(offsetof(OnlineMapsGoogleAPIQuery_t356009153, ___OnDispose_1)); }
	inline Action_1_t157808535 * get_OnDispose_1() const { return ___OnDispose_1; }
	inline Action_1_t157808535 ** get_address_of_OnDispose_1() { return &___OnDispose_1; }
	inline void set_OnDispose_1(Action_1_t157808535 * value)
	{
		___OnDispose_1 = value;
		Il2CppCodeGenWriteBarrier(&___OnDispose_1, value);
	}

	inline static int32_t get_offset_of_OnFinish_2() { return static_cast<int32_t>(offsetof(OnlineMapsGoogleAPIQuery_t356009153, ___OnFinish_2)); }
	inline Action_1_t157808535 * get_OnFinish_2() const { return ___OnFinish_2; }
	inline Action_1_t157808535 ** get_address_of_OnFinish_2() { return &___OnFinish_2; }
	inline void set_OnFinish_2(Action_1_t157808535 * value)
	{
		___OnFinish_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnFinish_2, value);
	}

	inline static int32_t get_offset_of__status_3() { return static_cast<int32_t>(offsetof(OnlineMapsGoogleAPIQuery_t356009153, ____status_3)); }
	inline int32_t get__status_3() const { return ____status_3; }
	inline int32_t* get_address_of__status_3() { return &____status_3; }
	inline void set__status_3(int32_t value)
	{
		____status_3 = value;
	}

	inline static int32_t get_offset_of_www_4() { return static_cast<int32_t>(offsetof(OnlineMapsGoogleAPIQuery_t356009153, ___www_4)); }
	inline WWW_t2919945039 * get_www_4() const { return ___www_4; }
	inline WWW_t2919945039 ** get_address_of_www_4() { return &___www_4; }
	inline void set_www_4(WWW_t2919945039 * value)
	{
		___www_4 = value;
		Il2CppCodeGenWriteBarrier(&___www_4, value);
	}

	inline static int32_t get_offset_of__response_5() { return static_cast<int32_t>(offsetof(OnlineMapsGoogleAPIQuery_t356009153, ____response_5)); }
	inline String_t* get__response_5() const { return ____response_5; }
	inline String_t** get_address_of__response_5() { return &____response_5; }
	inline void set__response_5(String_t* value)
	{
		____response_5 = value;
		Il2CppCodeGenWriteBarrier(&____response_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
