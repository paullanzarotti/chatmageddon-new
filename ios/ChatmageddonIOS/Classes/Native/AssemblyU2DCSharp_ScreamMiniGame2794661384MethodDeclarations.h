﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreamMiniGame
struct ScreamMiniGame_t2794661384;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void ScreamMiniGame::.ctor()
extern "C"  void ScreamMiniGame__ctor_m432180237 (ScreamMiniGame_t2794661384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamMiniGame::StartMiniGame()
extern "C"  void ScreamMiniGame_StartMiniGame_m2335160174 (ScreamMiniGame_t2794661384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamMiniGame::StopMiniGame()
extern "C"  void ScreamMiniGame_StopMiniGame_m2496796266 (ScreamMiniGame_t2794661384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamMiniGame::OnPaused()
extern "C"  void ScreamMiniGame_OnPaused_m1502445656 (ScreamMiniGame_t2794661384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamMiniGame::ResetMiniGame()
extern "C"  void ScreamMiniGame_ResetMiniGame_m4004574045 (ScreamMiniGame_t2794661384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreamMiniGame::TimerUpdated(System.TimeSpan)
extern "C"  void ScreamMiniGame_TimerUpdated_m3475900241 (ScreamMiniGame_t2794661384 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
