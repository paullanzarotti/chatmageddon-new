﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_FacebookUser244724091.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookFriend
struct  FacebookFriend_t758270532  : public FacebookUser_t244724091
{
public:
	// System.Int32 FacebookFriend::score
	int32_t ___score_5;

public:
	inline static int32_t get_offset_of_score_5() { return static_cast<int32_t>(offsetof(FacebookFriend_t758270532, ___score_5)); }
	inline int32_t get_score_5() const { return ___score_5; }
	inline int32_t* get_address_of_score_5() { return &___score_5; }
	inline void set_score_5(int32_t value)
	{
		___score_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
