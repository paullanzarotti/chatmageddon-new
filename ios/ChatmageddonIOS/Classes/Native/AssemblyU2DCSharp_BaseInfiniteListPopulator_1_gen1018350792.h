﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UITable
struct UITable_t3717403602;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseInfiniteListPopulator`1<StatusListItem>
struct  BaseInfiniteListPopulator_1_t1018350792  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BaseInfiniteListPopulator`1::enableLog
	bool ___enableLog_2;
	// UnityEngine.Vector3 BaseInfiniteListPopulator`1::customListScale
	Vector3_t2243707580  ___customListScale_3;
	// System.Boolean BaseInfiniteListPopulator`1::carousel
	bool ___carousel_4;
	// System.Boolean BaseInfiniteListPopulator`1::horizontal
	bool ___horizontal_5;
	// System.Boolean BaseInfiniteListPopulator`1::upOrLeft
	bool ___upOrLeft_6;
	// System.Boolean BaseInfiniteListPopulator`1::downOrRight
	bool ___downOrRight_7;
	// System.Single BaseInfiniteListPopulator`1::startXpos
	float ___startXpos_8;
	// System.Single BaseInfiniteListPopulator`1::startYpos
	float ___startYpos_9;
	// System.String BaseInfiniteListPopulator`1::Tag
	String_t* ___Tag_10;
	// UITable BaseInfiniteListPopulator`1::table
	UITable_t3717403602 * ___table_11;
	// UIScrollView BaseInfiniteListPopulator`1::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_12;
	// System.Single BaseInfiniteListPopulator`1::cellHeight
	float ___cellHeight_13;
	// System.Int32 BaseInfiniteListPopulator`1::poolSize
	int32_t ___poolSize_14;
	// System.Int32 BaseInfiniteListPopulator`1::originalPoolSize
	int32_t ___originalPoolSize_15;
	// System.String BaseInfiniteListPopulator`1::itemName
	String_t* ___itemName_16;
	// UnityEngine.Transform BaseInfiniteListPopulator`1::itemPrefab
	Transform_t3275118058 * ___itemPrefab_17;
	// System.Int32 BaseInfiniteListPopulator`1::startIndex
	int32_t ___startIndex_18;
	// System.Int32 BaseInfiniteListPopulator`1::itemDataIndex
	int32_t ___itemDataIndex_19;
	// System.Boolean BaseInfiniteListPopulator`1::isUpdatingList
	bool ___isUpdatingList_20;
	// System.Collections.Generic.List`1<UnityEngine.Transform> BaseInfiniteListPopulator`1::itemsPool
	List_1_t2644239190 * ___itemsPool_21;
	// System.Collections.Hashtable BaseInfiniteListPopulator`1::dataTracker
	Hashtable_t909839986 * ___dataTracker_22;
	// System.Collections.ArrayList BaseInfiniteListPopulator`1::originalData
	ArrayList_t4252133567 * ___originalData_23;
	// System.Collections.ArrayList BaseInfiniteListPopulator`1::dataList
	ArrayList_t4252133567 * ___dataList_24;
	// System.Int32 BaseInfiniteListPopulator`1::numberOfSections
	int32_t ___numberOfSections_25;
	// System.Collections.Generic.List`1<System.Int32> BaseInfiniteListPopulator`1::sectionsIndices
	List_1_t1440998580 * ___sectionsIndices_26;
	// System.Int32 BaseInfiniteListPopulator`1::carouselBackwardsActualIndex
	int32_t ___carouselBackwardsActualIndex_27;
	// System.Int32 BaseInfiniteListPopulator`1::carouselForwardsActualIndex
	int32_t ___carouselForwardsActualIndex_28;
	// System.Int32 BaseInfiniteListPopulator`1::carouselBackwardsNewIndex
	int32_t ___carouselBackwardsNewIndex_29;
	// System.Int32 BaseInfiniteListPopulator`1::carouselForwardsNewIndex
	int32_t ___carouselForwardsNewIndex_30;
	// System.Boolean BaseInfiniteListPopulator`1::carouselling
	bool ___carouselling_31;
	// UnityEngine.Vector3 BaseInfiniteListPopulator`1::previousListPos
	Vector3_t2243707580  ___previousListPos_32;

public:
	inline static int32_t get_offset_of_enableLog_2() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___enableLog_2)); }
	inline bool get_enableLog_2() const { return ___enableLog_2; }
	inline bool* get_address_of_enableLog_2() { return &___enableLog_2; }
	inline void set_enableLog_2(bool value)
	{
		___enableLog_2 = value;
	}

	inline static int32_t get_offset_of_customListScale_3() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___customListScale_3)); }
	inline Vector3_t2243707580  get_customListScale_3() const { return ___customListScale_3; }
	inline Vector3_t2243707580 * get_address_of_customListScale_3() { return &___customListScale_3; }
	inline void set_customListScale_3(Vector3_t2243707580  value)
	{
		___customListScale_3 = value;
	}

	inline static int32_t get_offset_of_carousel_4() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___carousel_4)); }
	inline bool get_carousel_4() const { return ___carousel_4; }
	inline bool* get_address_of_carousel_4() { return &___carousel_4; }
	inline void set_carousel_4(bool value)
	{
		___carousel_4 = value;
	}

	inline static int32_t get_offset_of_horizontal_5() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___horizontal_5)); }
	inline bool get_horizontal_5() const { return ___horizontal_5; }
	inline bool* get_address_of_horizontal_5() { return &___horizontal_5; }
	inline void set_horizontal_5(bool value)
	{
		___horizontal_5 = value;
	}

	inline static int32_t get_offset_of_upOrLeft_6() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___upOrLeft_6)); }
	inline bool get_upOrLeft_6() const { return ___upOrLeft_6; }
	inline bool* get_address_of_upOrLeft_6() { return &___upOrLeft_6; }
	inline void set_upOrLeft_6(bool value)
	{
		___upOrLeft_6 = value;
	}

	inline static int32_t get_offset_of_downOrRight_7() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___downOrRight_7)); }
	inline bool get_downOrRight_7() const { return ___downOrRight_7; }
	inline bool* get_address_of_downOrRight_7() { return &___downOrRight_7; }
	inline void set_downOrRight_7(bool value)
	{
		___downOrRight_7 = value;
	}

	inline static int32_t get_offset_of_startXpos_8() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___startXpos_8)); }
	inline float get_startXpos_8() const { return ___startXpos_8; }
	inline float* get_address_of_startXpos_8() { return &___startXpos_8; }
	inline void set_startXpos_8(float value)
	{
		___startXpos_8 = value;
	}

	inline static int32_t get_offset_of_startYpos_9() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___startYpos_9)); }
	inline float get_startYpos_9() const { return ___startYpos_9; }
	inline float* get_address_of_startYpos_9() { return &___startYpos_9; }
	inline void set_startYpos_9(float value)
	{
		___startYpos_9 = value;
	}

	inline static int32_t get_offset_of_Tag_10() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___Tag_10)); }
	inline String_t* get_Tag_10() const { return ___Tag_10; }
	inline String_t** get_address_of_Tag_10() { return &___Tag_10; }
	inline void set_Tag_10(String_t* value)
	{
		___Tag_10 = value;
		Il2CppCodeGenWriteBarrier(&___Tag_10, value);
	}

	inline static int32_t get_offset_of_table_11() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___table_11)); }
	inline UITable_t3717403602 * get_table_11() const { return ___table_11; }
	inline UITable_t3717403602 ** get_address_of_table_11() { return &___table_11; }
	inline void set_table_11(UITable_t3717403602 * value)
	{
		___table_11 = value;
		Il2CppCodeGenWriteBarrier(&___table_11, value);
	}

	inline static int32_t get_offset_of_draggablePanel_12() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___draggablePanel_12)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_12() const { return ___draggablePanel_12; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_12() { return &___draggablePanel_12; }
	inline void set_draggablePanel_12(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_12 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_12, value);
	}

	inline static int32_t get_offset_of_cellHeight_13() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___cellHeight_13)); }
	inline float get_cellHeight_13() const { return ___cellHeight_13; }
	inline float* get_address_of_cellHeight_13() { return &___cellHeight_13; }
	inline void set_cellHeight_13(float value)
	{
		___cellHeight_13 = value;
	}

	inline static int32_t get_offset_of_poolSize_14() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___poolSize_14)); }
	inline int32_t get_poolSize_14() const { return ___poolSize_14; }
	inline int32_t* get_address_of_poolSize_14() { return &___poolSize_14; }
	inline void set_poolSize_14(int32_t value)
	{
		___poolSize_14 = value;
	}

	inline static int32_t get_offset_of_originalPoolSize_15() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___originalPoolSize_15)); }
	inline int32_t get_originalPoolSize_15() const { return ___originalPoolSize_15; }
	inline int32_t* get_address_of_originalPoolSize_15() { return &___originalPoolSize_15; }
	inline void set_originalPoolSize_15(int32_t value)
	{
		___originalPoolSize_15 = value;
	}

	inline static int32_t get_offset_of_itemName_16() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___itemName_16)); }
	inline String_t* get_itemName_16() const { return ___itemName_16; }
	inline String_t** get_address_of_itemName_16() { return &___itemName_16; }
	inline void set_itemName_16(String_t* value)
	{
		___itemName_16 = value;
		Il2CppCodeGenWriteBarrier(&___itemName_16, value);
	}

	inline static int32_t get_offset_of_itemPrefab_17() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___itemPrefab_17)); }
	inline Transform_t3275118058 * get_itemPrefab_17() const { return ___itemPrefab_17; }
	inline Transform_t3275118058 ** get_address_of_itemPrefab_17() { return &___itemPrefab_17; }
	inline void set_itemPrefab_17(Transform_t3275118058 * value)
	{
		___itemPrefab_17 = value;
		Il2CppCodeGenWriteBarrier(&___itemPrefab_17, value);
	}

	inline static int32_t get_offset_of_startIndex_18() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___startIndex_18)); }
	inline int32_t get_startIndex_18() const { return ___startIndex_18; }
	inline int32_t* get_address_of_startIndex_18() { return &___startIndex_18; }
	inline void set_startIndex_18(int32_t value)
	{
		___startIndex_18 = value;
	}

	inline static int32_t get_offset_of_itemDataIndex_19() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___itemDataIndex_19)); }
	inline int32_t get_itemDataIndex_19() const { return ___itemDataIndex_19; }
	inline int32_t* get_address_of_itemDataIndex_19() { return &___itemDataIndex_19; }
	inline void set_itemDataIndex_19(int32_t value)
	{
		___itemDataIndex_19 = value;
	}

	inline static int32_t get_offset_of_isUpdatingList_20() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___isUpdatingList_20)); }
	inline bool get_isUpdatingList_20() const { return ___isUpdatingList_20; }
	inline bool* get_address_of_isUpdatingList_20() { return &___isUpdatingList_20; }
	inline void set_isUpdatingList_20(bool value)
	{
		___isUpdatingList_20 = value;
	}

	inline static int32_t get_offset_of_itemsPool_21() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___itemsPool_21)); }
	inline List_1_t2644239190 * get_itemsPool_21() const { return ___itemsPool_21; }
	inline List_1_t2644239190 ** get_address_of_itemsPool_21() { return &___itemsPool_21; }
	inline void set_itemsPool_21(List_1_t2644239190 * value)
	{
		___itemsPool_21 = value;
		Il2CppCodeGenWriteBarrier(&___itemsPool_21, value);
	}

	inline static int32_t get_offset_of_dataTracker_22() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___dataTracker_22)); }
	inline Hashtable_t909839986 * get_dataTracker_22() const { return ___dataTracker_22; }
	inline Hashtable_t909839986 ** get_address_of_dataTracker_22() { return &___dataTracker_22; }
	inline void set_dataTracker_22(Hashtable_t909839986 * value)
	{
		___dataTracker_22 = value;
		Il2CppCodeGenWriteBarrier(&___dataTracker_22, value);
	}

	inline static int32_t get_offset_of_originalData_23() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___originalData_23)); }
	inline ArrayList_t4252133567 * get_originalData_23() const { return ___originalData_23; }
	inline ArrayList_t4252133567 ** get_address_of_originalData_23() { return &___originalData_23; }
	inline void set_originalData_23(ArrayList_t4252133567 * value)
	{
		___originalData_23 = value;
		Il2CppCodeGenWriteBarrier(&___originalData_23, value);
	}

	inline static int32_t get_offset_of_dataList_24() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___dataList_24)); }
	inline ArrayList_t4252133567 * get_dataList_24() const { return ___dataList_24; }
	inline ArrayList_t4252133567 ** get_address_of_dataList_24() { return &___dataList_24; }
	inline void set_dataList_24(ArrayList_t4252133567 * value)
	{
		___dataList_24 = value;
		Il2CppCodeGenWriteBarrier(&___dataList_24, value);
	}

	inline static int32_t get_offset_of_numberOfSections_25() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___numberOfSections_25)); }
	inline int32_t get_numberOfSections_25() const { return ___numberOfSections_25; }
	inline int32_t* get_address_of_numberOfSections_25() { return &___numberOfSections_25; }
	inline void set_numberOfSections_25(int32_t value)
	{
		___numberOfSections_25 = value;
	}

	inline static int32_t get_offset_of_sectionsIndices_26() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___sectionsIndices_26)); }
	inline List_1_t1440998580 * get_sectionsIndices_26() const { return ___sectionsIndices_26; }
	inline List_1_t1440998580 ** get_address_of_sectionsIndices_26() { return &___sectionsIndices_26; }
	inline void set_sectionsIndices_26(List_1_t1440998580 * value)
	{
		___sectionsIndices_26 = value;
		Il2CppCodeGenWriteBarrier(&___sectionsIndices_26, value);
	}

	inline static int32_t get_offset_of_carouselBackwardsActualIndex_27() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___carouselBackwardsActualIndex_27)); }
	inline int32_t get_carouselBackwardsActualIndex_27() const { return ___carouselBackwardsActualIndex_27; }
	inline int32_t* get_address_of_carouselBackwardsActualIndex_27() { return &___carouselBackwardsActualIndex_27; }
	inline void set_carouselBackwardsActualIndex_27(int32_t value)
	{
		___carouselBackwardsActualIndex_27 = value;
	}

	inline static int32_t get_offset_of_carouselForwardsActualIndex_28() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___carouselForwardsActualIndex_28)); }
	inline int32_t get_carouselForwardsActualIndex_28() const { return ___carouselForwardsActualIndex_28; }
	inline int32_t* get_address_of_carouselForwardsActualIndex_28() { return &___carouselForwardsActualIndex_28; }
	inline void set_carouselForwardsActualIndex_28(int32_t value)
	{
		___carouselForwardsActualIndex_28 = value;
	}

	inline static int32_t get_offset_of_carouselBackwardsNewIndex_29() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___carouselBackwardsNewIndex_29)); }
	inline int32_t get_carouselBackwardsNewIndex_29() const { return ___carouselBackwardsNewIndex_29; }
	inline int32_t* get_address_of_carouselBackwardsNewIndex_29() { return &___carouselBackwardsNewIndex_29; }
	inline void set_carouselBackwardsNewIndex_29(int32_t value)
	{
		___carouselBackwardsNewIndex_29 = value;
	}

	inline static int32_t get_offset_of_carouselForwardsNewIndex_30() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___carouselForwardsNewIndex_30)); }
	inline int32_t get_carouselForwardsNewIndex_30() const { return ___carouselForwardsNewIndex_30; }
	inline int32_t* get_address_of_carouselForwardsNewIndex_30() { return &___carouselForwardsNewIndex_30; }
	inline void set_carouselForwardsNewIndex_30(int32_t value)
	{
		___carouselForwardsNewIndex_30 = value;
	}

	inline static int32_t get_offset_of_carouselling_31() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___carouselling_31)); }
	inline bool get_carouselling_31() const { return ___carouselling_31; }
	inline bool* get_address_of_carouselling_31() { return &___carouselling_31; }
	inline void set_carouselling_31(bool value)
	{
		___carouselling_31 = value;
	}

	inline static int32_t get_offset_of_previousListPos_32() { return static_cast<int32_t>(offsetof(BaseInfiniteListPopulator_1_t1018350792, ___previousListPos_32)); }
	inline Vector3_t2243707580  get_previousListPos_32() const { return ___previousListPos_32; }
	inline Vector3_t2243707580 * get_address_of_previousListPos_32() { return &___previousListPos_32; }
	inline void set_previousListPos_32(Vector3_t2243707580  value)
	{
		___previousListPos_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
