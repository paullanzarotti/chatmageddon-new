﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotificationClearAllButton
struct NotificationClearAllButton_t3531371129;

#include "codegen/il2cpp-codegen.h"

// System.Void NotificationClearAllButton::.ctor()
extern "C"  void NotificationClearAllButton__ctor_m1304920806 (NotificationClearAllButton_t3531371129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotificationClearAllButton::OnButtonClick()
extern "C"  void NotificationClearAllButton_OnButtonClick_m3491836413 (NotificationClearAllButton_t3531371129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
