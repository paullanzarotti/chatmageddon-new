﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseUIScaler
struct BaseUIScaler_t568866473;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void BaseUIScaler::.ctor()
extern "C"  void BaseUIScaler__ctor_m3408112586 (BaseUIScaler_t568866473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseUIScaler::Awake()
extern "C"  void BaseUIScaler_Awake_m2774361109 (BaseUIScaler_t568866473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseUIScaler::Start()
extern "C"  void BaseUIScaler_Start_m233827338 (BaseUIScaler_t568866473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseUIScaler::SetUICalculation()
extern "C"  void BaseUIScaler_SetUICalculation_m1323830999 (BaseUIScaler_t568866473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseUIScaler::CalulateUIScale()
extern "C"  void BaseUIScaler_CalulateUIScale_m2783074407 (BaseUIScaler_t568866473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseUIScaler::ScaleUI()
extern "C"  void BaseUIScaler_ScaleUI_m2361041734 (BaseUIScaler_t568866473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BaseUIScaler::AltAspectScale(System.Int32,System.Int32)
extern "C"  Vector3_t2243707580  BaseUIScaler_AltAspectScale_m2075180763 (BaseUIScaler_t568866473 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseUIScaler::GlobalUIScale()
extern "C"  void BaseUIScaler_GlobalUIScale_m2469894115 (BaseUIScaler_t568866473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BaseUIScaler::ExactScaleVector3(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  BaseUIScaler_ExactScaleVector3_m2041284374 (BaseUIScaler_t568866473 * __this, Vector3_t2243707580  ___scaleVector0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
