﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineCountdownProgressBar
struct MineCountdownProgressBar_t1258896602;

#include "codegen/il2cpp-codegen.h"

// System.Void MineCountdownProgressBar::.ctor()
extern "C"  void MineCountdownProgressBar__ctor_m412357785 (MineCountdownProgressBar_t1258896602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineCountdownProgressBar::SetCountdownTime(System.Single)
extern "C"  void MineCountdownProgressBar_SetCountdownTime_m1898359316 (MineCountdownProgressBar_t1258896602 * __this, float ___newTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineCountdownProgressBar::TransitionToPercent(System.Single,System.Single)
extern "C"  void MineCountdownProgressBar_TransitionToPercent_m538638040 (MineCountdownProgressBar_t1258896602 * __this, float ___percent0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineCountdownProgressBar::ProgressUpdated()
extern "C"  void MineCountdownProgressBar_ProgressUpdated_m3452230013 (MineCountdownProgressBar_t1258896602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineCountdownProgressBar::ProgressFinished()
extern "C"  void MineCountdownProgressBar_ProgressFinished_m2235254460 (MineCountdownProgressBar_t1258896602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineCountdownProgressBar::StartCountDownTimer()
extern "C"  void MineCountdownProgressBar_StartCountDownTimer_m1048427351 (MineCountdownProgressBar_t1258896602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
