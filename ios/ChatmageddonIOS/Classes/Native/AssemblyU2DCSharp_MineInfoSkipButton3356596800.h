﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineInfoController
struct MineInfoController_t2990208339;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineInfoSkipButton
struct  MineInfoSkipButton_t3356596800  : public SFXButton_t792651341
{
public:
	// MineInfoController MineInfoSkipButton::infoController
	MineInfoController_t2990208339 * ___infoController_5;

public:
	inline static int32_t get_offset_of_infoController_5() { return static_cast<int32_t>(offsetof(MineInfoSkipButton_t3356596800, ___infoController_5)); }
	inline MineInfoController_t2990208339 * get_infoController_5() const { return ___infoController_5; }
	inline MineInfoController_t2990208339 ** get_address_of_infoController_5() { return &___infoController_5; }
	inline void set_infoController_5(MineInfoController_t2990208339 * value)
	{
		___infoController_5 = value;
		Il2CppCodeGenWriteBarrier(&___infoController_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
