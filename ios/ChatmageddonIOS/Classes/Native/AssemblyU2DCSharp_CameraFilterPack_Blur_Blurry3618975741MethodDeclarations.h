﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Blurry
struct CameraFilterPack_Blur_Blurry_t3618975741;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Blurry::.ctor()
extern "C"  void CameraFilterPack_Blur_Blurry__ctor_m2440480376 (CameraFilterPack_Blur_Blurry_t3618975741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Blurry::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Blurry_get_material_m3384218429 (CameraFilterPack_Blur_Blurry_t3618975741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::Start()
extern "C"  void CameraFilterPack_Blur_Blurry_Start_m1315764904 (CameraFilterPack_Blur_Blurry_t3618975741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Blurry_OnRenderImage_m3557643968 (CameraFilterPack_Blur_Blurry_t3618975741 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::OnValidate()
extern "C"  void CameraFilterPack_Blur_Blurry_OnValidate_m4092632739 (CameraFilterPack_Blur_Blurry_t3618975741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::Update()
extern "C"  void CameraFilterPack_Blur_Blurry_Update_m1664317673 (CameraFilterPack_Blur_Blurry_t3618975741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::OnDisable()
extern "C"  void CameraFilterPack_Blur_Blurry_OnDisable_m581238445 (CameraFilterPack_Blur_Blurry_t3618975741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
