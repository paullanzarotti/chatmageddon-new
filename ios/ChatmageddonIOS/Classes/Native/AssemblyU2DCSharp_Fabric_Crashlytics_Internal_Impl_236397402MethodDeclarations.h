﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Crashlytics.Internal.Impl/FrameParser
struct FrameParser_t236397402;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Fabric.Crashlytics.Internal.Impl/FrameParser::.ctor(System.Object,System.IntPtr)
extern "C"  void FrameParser__ctor_m4142851837 (FrameParser_t236397402 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Fabric.Crashlytics.Internal.Impl/FrameParser::Invoke(System.String)
extern "C"  Dictionary_2_t3943999495 * FrameParser_Invoke_m846777147 (FrameParser_t236397402 * __this, String_t* ___frameString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Fabric.Crashlytics.Internal.Impl/FrameParser::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FrameParser_BeginInvoke_m2111162484 (FrameParser_t236397402 * __this, String_t* ___frameString0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Fabric.Crashlytics.Internal.Impl/FrameParser::EndInvoke(System.IAsyncResult)
extern "C"  Dictionary_2_t3943999495 * FrameParser_EndInvoke_m1109124355 (FrameParser_t236397402 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
