﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XProcessingInstruction
struct XProcessingInstruction_t2854903359;
// System.String
struct String_t;
// System.Xml.XmlWriter
struct XmlWriter_t1048088568;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_Linq_System_Xml_Linq_XProcessingInstruc2854903359.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"

// System.Void System.Xml.Linq.XProcessingInstruction::.ctor(System.String,System.String)
extern "C"  void XProcessingInstruction__ctor_m3547038108 (XProcessingInstruction_t2854903359 * __this, String_t* ___name0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XProcessingInstruction::.ctor(System.Xml.Linq.XProcessingInstruction)
extern "C"  void XProcessingInstruction__ctor_m2560507313 (XProcessingInstruction_t2854903359 * __this, XProcessingInstruction_t2854903359 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XProcessingInstruction::get_Data()
extern "C"  String_t* XProcessingInstruction_get_Data_m705442580 (XProcessingInstruction_t2854903359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.Linq.XProcessingInstruction::get_NodeType()
extern "C"  int32_t XProcessingInstruction_get_NodeType_m1066128541 (XProcessingInstruction_t2854903359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.Linq.XProcessingInstruction::get_Target()
extern "C"  String_t* XProcessingInstruction_get_Target_m627615819 (XProcessingInstruction_t2854903359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XProcessingInstruction::WriteTo(System.Xml.XmlWriter)
extern "C"  void XProcessingInstruction_WriteTo_m4161808002 (XProcessingInstruction_t2854903359 * __this, XmlWriter_t1048088568 * ___w0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
