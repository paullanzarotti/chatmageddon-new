﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<UpdateUserStealthMode>c__AnonStorey1E
struct U3CUpdateUserStealthModeU3Ec__AnonStorey1E_t470248544;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<UpdateUserStealthMode>c__AnonStorey1E::.ctor()
extern "C"  void U3CUpdateUserStealthModeU3Ec__AnonStorey1E__ctor_m3250207613 (U3CUpdateUserStealthModeU3Ec__AnonStorey1E_t470248544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<UpdateUserStealthMode>c__AnonStorey1E::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CUpdateUserStealthModeU3Ec__AnonStorey1E_U3CU3Em__0_m1483146180 (U3CUpdateUserStealthModeU3Ec__AnonStorey1E_t470248544 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
