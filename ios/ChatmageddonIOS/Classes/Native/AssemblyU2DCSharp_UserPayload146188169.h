﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserPayload
struct  UserPayload_t146188169  : public Il2CppObject
{
public:
	// System.String UserPayload::tID
	String_t* ___tID_17;
	// System.String UserPayload::tFirstName
	String_t* ___tFirstName_18;
	// System.String UserPayload::tSecondName
	String_t* ___tSecondName_19;
	// System.String UserPayload::tEmail
	String_t* ___tEmail_20;
	// System.String UserPayload::tFullName
	String_t* ___tFullName_21;
	// System.String UserPayload::tUserName
	String_t* ___tUserName_22;
	// System.String UserPayload::tProfileImage
	String_t* ___tProfileImage_23;
	// System.String UserPayload::tProfileImageThumbnail
	String_t* ___tProfileImageThumbnail_24;
	// System.String UserPayload::tGender
	String_t* ___tGender_25;
	// System.String UserPayload::tDisplayLocation
	String_t* ___tDisplayLocation_26;
	// System.String UserPayload::tStealthMode
	String_t* ___tStealthMode_27;
	// System.String UserPayload::tStealthModeActive
	String_t* ___tStealthModeActive_28;
	// System.String UserPayload::tStealthModeHour
	String_t* ___tStealthModeHour_29;
	// System.String UserPayload::tStealthModeMin
	String_t* ___tStealthModeMin_30;
	// System.String UserPayload::tStealthModeDuration
	String_t* ___tStealthModeDuration_31;
	// System.String UserPayload::tLevel
	String_t* ___tLevel_32;
	// System.String UserPayload::tRank
	String_t* ___tRank_33;

public:
	inline static int32_t get_offset_of_tID_17() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tID_17)); }
	inline String_t* get_tID_17() const { return ___tID_17; }
	inline String_t** get_address_of_tID_17() { return &___tID_17; }
	inline void set_tID_17(String_t* value)
	{
		___tID_17 = value;
		Il2CppCodeGenWriteBarrier(&___tID_17, value);
	}

	inline static int32_t get_offset_of_tFirstName_18() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tFirstName_18)); }
	inline String_t* get_tFirstName_18() const { return ___tFirstName_18; }
	inline String_t** get_address_of_tFirstName_18() { return &___tFirstName_18; }
	inline void set_tFirstName_18(String_t* value)
	{
		___tFirstName_18 = value;
		Il2CppCodeGenWriteBarrier(&___tFirstName_18, value);
	}

	inline static int32_t get_offset_of_tSecondName_19() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tSecondName_19)); }
	inline String_t* get_tSecondName_19() const { return ___tSecondName_19; }
	inline String_t** get_address_of_tSecondName_19() { return &___tSecondName_19; }
	inline void set_tSecondName_19(String_t* value)
	{
		___tSecondName_19 = value;
		Il2CppCodeGenWriteBarrier(&___tSecondName_19, value);
	}

	inline static int32_t get_offset_of_tEmail_20() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tEmail_20)); }
	inline String_t* get_tEmail_20() const { return ___tEmail_20; }
	inline String_t** get_address_of_tEmail_20() { return &___tEmail_20; }
	inline void set_tEmail_20(String_t* value)
	{
		___tEmail_20 = value;
		Il2CppCodeGenWriteBarrier(&___tEmail_20, value);
	}

	inline static int32_t get_offset_of_tFullName_21() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tFullName_21)); }
	inline String_t* get_tFullName_21() const { return ___tFullName_21; }
	inline String_t** get_address_of_tFullName_21() { return &___tFullName_21; }
	inline void set_tFullName_21(String_t* value)
	{
		___tFullName_21 = value;
		Il2CppCodeGenWriteBarrier(&___tFullName_21, value);
	}

	inline static int32_t get_offset_of_tUserName_22() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tUserName_22)); }
	inline String_t* get_tUserName_22() const { return ___tUserName_22; }
	inline String_t** get_address_of_tUserName_22() { return &___tUserName_22; }
	inline void set_tUserName_22(String_t* value)
	{
		___tUserName_22 = value;
		Il2CppCodeGenWriteBarrier(&___tUserName_22, value);
	}

	inline static int32_t get_offset_of_tProfileImage_23() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tProfileImage_23)); }
	inline String_t* get_tProfileImage_23() const { return ___tProfileImage_23; }
	inline String_t** get_address_of_tProfileImage_23() { return &___tProfileImage_23; }
	inline void set_tProfileImage_23(String_t* value)
	{
		___tProfileImage_23 = value;
		Il2CppCodeGenWriteBarrier(&___tProfileImage_23, value);
	}

	inline static int32_t get_offset_of_tProfileImageThumbnail_24() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tProfileImageThumbnail_24)); }
	inline String_t* get_tProfileImageThumbnail_24() const { return ___tProfileImageThumbnail_24; }
	inline String_t** get_address_of_tProfileImageThumbnail_24() { return &___tProfileImageThumbnail_24; }
	inline void set_tProfileImageThumbnail_24(String_t* value)
	{
		___tProfileImageThumbnail_24 = value;
		Il2CppCodeGenWriteBarrier(&___tProfileImageThumbnail_24, value);
	}

	inline static int32_t get_offset_of_tGender_25() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tGender_25)); }
	inline String_t* get_tGender_25() const { return ___tGender_25; }
	inline String_t** get_address_of_tGender_25() { return &___tGender_25; }
	inline void set_tGender_25(String_t* value)
	{
		___tGender_25 = value;
		Il2CppCodeGenWriteBarrier(&___tGender_25, value);
	}

	inline static int32_t get_offset_of_tDisplayLocation_26() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tDisplayLocation_26)); }
	inline String_t* get_tDisplayLocation_26() const { return ___tDisplayLocation_26; }
	inline String_t** get_address_of_tDisplayLocation_26() { return &___tDisplayLocation_26; }
	inline void set_tDisplayLocation_26(String_t* value)
	{
		___tDisplayLocation_26 = value;
		Il2CppCodeGenWriteBarrier(&___tDisplayLocation_26, value);
	}

	inline static int32_t get_offset_of_tStealthMode_27() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tStealthMode_27)); }
	inline String_t* get_tStealthMode_27() const { return ___tStealthMode_27; }
	inline String_t** get_address_of_tStealthMode_27() { return &___tStealthMode_27; }
	inline void set_tStealthMode_27(String_t* value)
	{
		___tStealthMode_27 = value;
		Il2CppCodeGenWriteBarrier(&___tStealthMode_27, value);
	}

	inline static int32_t get_offset_of_tStealthModeActive_28() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tStealthModeActive_28)); }
	inline String_t* get_tStealthModeActive_28() const { return ___tStealthModeActive_28; }
	inline String_t** get_address_of_tStealthModeActive_28() { return &___tStealthModeActive_28; }
	inline void set_tStealthModeActive_28(String_t* value)
	{
		___tStealthModeActive_28 = value;
		Il2CppCodeGenWriteBarrier(&___tStealthModeActive_28, value);
	}

	inline static int32_t get_offset_of_tStealthModeHour_29() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tStealthModeHour_29)); }
	inline String_t* get_tStealthModeHour_29() const { return ___tStealthModeHour_29; }
	inline String_t** get_address_of_tStealthModeHour_29() { return &___tStealthModeHour_29; }
	inline void set_tStealthModeHour_29(String_t* value)
	{
		___tStealthModeHour_29 = value;
		Il2CppCodeGenWriteBarrier(&___tStealthModeHour_29, value);
	}

	inline static int32_t get_offset_of_tStealthModeMin_30() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tStealthModeMin_30)); }
	inline String_t* get_tStealthModeMin_30() const { return ___tStealthModeMin_30; }
	inline String_t** get_address_of_tStealthModeMin_30() { return &___tStealthModeMin_30; }
	inline void set_tStealthModeMin_30(String_t* value)
	{
		___tStealthModeMin_30 = value;
		Il2CppCodeGenWriteBarrier(&___tStealthModeMin_30, value);
	}

	inline static int32_t get_offset_of_tStealthModeDuration_31() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tStealthModeDuration_31)); }
	inline String_t* get_tStealthModeDuration_31() const { return ___tStealthModeDuration_31; }
	inline String_t** get_address_of_tStealthModeDuration_31() { return &___tStealthModeDuration_31; }
	inline void set_tStealthModeDuration_31(String_t* value)
	{
		___tStealthModeDuration_31 = value;
		Il2CppCodeGenWriteBarrier(&___tStealthModeDuration_31, value);
	}

	inline static int32_t get_offset_of_tLevel_32() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tLevel_32)); }
	inline String_t* get_tLevel_32() const { return ___tLevel_32; }
	inline String_t** get_address_of_tLevel_32() { return &___tLevel_32; }
	inline void set_tLevel_32(String_t* value)
	{
		___tLevel_32 = value;
		Il2CppCodeGenWriteBarrier(&___tLevel_32, value);
	}

	inline static int32_t get_offset_of_tRank_33() { return static_cast<int32_t>(offsetof(UserPayload_t146188169, ___tRank_33)); }
	inline String_t* get_tRank_33() const { return ___tRank_33; }
	inline String_t** get_address_of_tRank_33() { return &___tRank_33; }
	inline void set_tRank_33(String_t* value)
	{
		___tRank_33 = value;
		Il2CppCodeGenWriteBarrier(&___tRank_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
