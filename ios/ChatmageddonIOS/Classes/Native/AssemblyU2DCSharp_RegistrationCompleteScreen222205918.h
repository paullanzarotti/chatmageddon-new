﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RegistrationCompleteScreen
struct  RegistrationCompleteScreen_t222205918  : public NavigationScreen_t2333230110
{
public:
	// UIInput RegistrationCompleteScreen::firstNameInput
	UIInput_t860674234 * ___firstNameInput_3;
	// UIInput RegistrationCompleteScreen::lastNameInput
	UIInput_t860674234 * ___lastNameInput_4;
	// UIInput RegistrationCompleteScreen::usernameInput
	UIInput_t860674234 * ___usernameInput_5;
	// UIInput RegistrationCompleteScreen::emailInput
	UIInput_t860674234 * ___emailInput_6;
	// UIInput RegistrationCompleteScreen::passwordInput
	UIInput_t860674234 * ___passwordInput_7;
	// UIInput RegistrationCompleteScreen::confirmPasswordInput
	UIInput_t860674234 * ___confirmPasswordInput_8;

public:
	inline static int32_t get_offset_of_firstNameInput_3() { return static_cast<int32_t>(offsetof(RegistrationCompleteScreen_t222205918, ___firstNameInput_3)); }
	inline UIInput_t860674234 * get_firstNameInput_3() const { return ___firstNameInput_3; }
	inline UIInput_t860674234 ** get_address_of_firstNameInput_3() { return &___firstNameInput_3; }
	inline void set_firstNameInput_3(UIInput_t860674234 * value)
	{
		___firstNameInput_3 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameInput_3, value);
	}

	inline static int32_t get_offset_of_lastNameInput_4() { return static_cast<int32_t>(offsetof(RegistrationCompleteScreen_t222205918, ___lastNameInput_4)); }
	inline UIInput_t860674234 * get_lastNameInput_4() const { return ___lastNameInput_4; }
	inline UIInput_t860674234 ** get_address_of_lastNameInput_4() { return &___lastNameInput_4; }
	inline void set_lastNameInput_4(UIInput_t860674234 * value)
	{
		___lastNameInput_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameInput_4, value);
	}

	inline static int32_t get_offset_of_usernameInput_5() { return static_cast<int32_t>(offsetof(RegistrationCompleteScreen_t222205918, ___usernameInput_5)); }
	inline UIInput_t860674234 * get_usernameInput_5() const { return ___usernameInput_5; }
	inline UIInput_t860674234 ** get_address_of_usernameInput_5() { return &___usernameInput_5; }
	inline void set_usernameInput_5(UIInput_t860674234 * value)
	{
		___usernameInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___usernameInput_5, value);
	}

	inline static int32_t get_offset_of_emailInput_6() { return static_cast<int32_t>(offsetof(RegistrationCompleteScreen_t222205918, ___emailInput_6)); }
	inline UIInput_t860674234 * get_emailInput_6() const { return ___emailInput_6; }
	inline UIInput_t860674234 ** get_address_of_emailInput_6() { return &___emailInput_6; }
	inline void set_emailInput_6(UIInput_t860674234 * value)
	{
		___emailInput_6 = value;
		Il2CppCodeGenWriteBarrier(&___emailInput_6, value);
	}

	inline static int32_t get_offset_of_passwordInput_7() { return static_cast<int32_t>(offsetof(RegistrationCompleteScreen_t222205918, ___passwordInput_7)); }
	inline UIInput_t860674234 * get_passwordInput_7() const { return ___passwordInput_7; }
	inline UIInput_t860674234 ** get_address_of_passwordInput_7() { return &___passwordInput_7; }
	inline void set_passwordInput_7(UIInput_t860674234 * value)
	{
		___passwordInput_7 = value;
		Il2CppCodeGenWriteBarrier(&___passwordInput_7, value);
	}

	inline static int32_t get_offset_of_confirmPasswordInput_8() { return static_cast<int32_t>(offsetof(RegistrationCompleteScreen_t222205918, ___confirmPasswordInput_8)); }
	inline UIInput_t860674234 * get_confirmPasswordInput_8() const { return ___confirmPasswordInput_8; }
	inline UIInput_t860674234 ** get_address_of_confirmPasswordInput_8() { return &___confirmPasswordInput_8; }
	inline void set_confirmPasswordInput_8(UIInput_t860674234 * value)
	{
		___confirmPasswordInput_8 = value;
		Il2CppCodeGenWriteBarrier(&___confirmPasswordInput_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
