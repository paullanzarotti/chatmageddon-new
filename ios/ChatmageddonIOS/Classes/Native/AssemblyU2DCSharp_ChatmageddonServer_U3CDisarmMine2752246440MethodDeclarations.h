﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<DisarmMine>c__AnonStorey2C
struct U3CDisarmMineU3Ec__AnonStorey2C_t2752246440;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<DisarmMine>c__AnonStorey2C::.ctor()
extern "C"  void U3CDisarmMineU3Ec__AnonStorey2C__ctor_m1964076567 (U3CDisarmMineU3Ec__AnonStorey2C_t2752246440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<DisarmMine>c__AnonStorey2C::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDisarmMineU3Ec__AnonStorey2C_U3CU3Em__0_m2791961596 (U3CDisarmMineU3Ec__AnonStorey2C_t2752246440 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
