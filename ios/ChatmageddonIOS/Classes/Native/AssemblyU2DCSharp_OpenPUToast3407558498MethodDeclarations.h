﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenPUToast
struct OpenPUToast_t3407558498;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenPUToast::.ctor()
extern "C"  void OpenPUToast__ctor_m3763205999 (OpenPUToast_t3407558498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenPUToast::OnButtonClick()
extern "C"  void OpenPUToast_OnButtonClick_m2317442390 (OpenPUToast_t3407558498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
