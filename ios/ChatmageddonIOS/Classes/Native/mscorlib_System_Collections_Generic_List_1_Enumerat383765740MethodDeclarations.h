﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ValidContact>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1917190769(__this, ___l0, method) ((  void (*) (Enumerator_t383765740 *, List_1_t849036066 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ValidContact>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1947499489(__this, method) ((  void (*) (Enumerator_t383765740 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ValidContact>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2191091613(__this, method) ((  Il2CppObject * (*) (Enumerator_t383765740 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ValidContact>::Dispose()
#define Enumerator_Dispose_m2730201466(__this, method) ((  void (*) (Enumerator_t383765740 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ValidContact>::VerifyState()
#define Enumerator_VerifyState_m206516595(__this, method) ((  void (*) (Enumerator_t383765740 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ValidContact>::MoveNext()
#define Enumerator_MoveNext_m980295948(__this, method) ((  bool (*) (Enumerator_t383765740 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ValidContact>::get_Current()
#define Enumerator_get_Current_m2219715166(__this, method) ((  ValidContact_t1479914934 * (*) (Enumerator_t383765740 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
