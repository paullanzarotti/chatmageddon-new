﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPNumberPicker
struct IPNumberPicker_t1469221158;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeNumberPicker
struct  TimeNumberPicker_t1112809442  : public MonoBehaviour_t1158329972
{
public:
	// IPNumberPicker TimeNumberPicker::timePicker
	IPNumberPicker_t1469221158 * ___timePicker_2;
	// UnityEngine.Color TimeNumberPicker::activeColour
	Color_t2020392075  ___activeColour_3;
	// UnityEngine.Color TimeNumberPicker::inactiveColour
	Color_t2020392075  ___inactiveColour_4;
	// System.Boolean TimeNumberPicker::setup
	bool ___setup_5;

public:
	inline static int32_t get_offset_of_timePicker_2() { return static_cast<int32_t>(offsetof(TimeNumberPicker_t1112809442, ___timePicker_2)); }
	inline IPNumberPicker_t1469221158 * get_timePicker_2() const { return ___timePicker_2; }
	inline IPNumberPicker_t1469221158 ** get_address_of_timePicker_2() { return &___timePicker_2; }
	inline void set_timePicker_2(IPNumberPicker_t1469221158 * value)
	{
		___timePicker_2 = value;
		Il2CppCodeGenWriteBarrier(&___timePicker_2, value);
	}

	inline static int32_t get_offset_of_activeColour_3() { return static_cast<int32_t>(offsetof(TimeNumberPicker_t1112809442, ___activeColour_3)); }
	inline Color_t2020392075  get_activeColour_3() const { return ___activeColour_3; }
	inline Color_t2020392075 * get_address_of_activeColour_3() { return &___activeColour_3; }
	inline void set_activeColour_3(Color_t2020392075  value)
	{
		___activeColour_3 = value;
	}

	inline static int32_t get_offset_of_inactiveColour_4() { return static_cast<int32_t>(offsetof(TimeNumberPicker_t1112809442, ___inactiveColour_4)); }
	inline Color_t2020392075  get_inactiveColour_4() const { return ___inactiveColour_4; }
	inline Color_t2020392075 * get_address_of_inactiveColour_4() { return &___inactiveColour_4; }
	inline void set_inactiveColour_4(Color_t2020392075  value)
	{
		___inactiveColour_4 = value;
	}

	inline static int32_t get_offset_of_setup_5() { return static_cast<int32_t>(offsetof(TimeNumberPicker_t1112809442, ___setup_5)); }
	inline bool get_setup_5() const { return ___setup_5; }
	inline bool* get_address_of_setup_5() { return &___setup_5; }
	inline void set_setup_5(bool value)
	{
		___setup_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
