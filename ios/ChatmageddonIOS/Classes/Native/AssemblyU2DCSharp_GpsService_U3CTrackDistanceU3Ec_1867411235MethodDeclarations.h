﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GpsService/<TrackDistance>c__Iterator0
struct U3CTrackDistanceU3Ec__Iterator0_t1867411235;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GpsService/<TrackDistance>c__Iterator0::.ctor()
extern "C"  void U3CTrackDistanceU3Ec__Iterator0__ctor_m1423903036 (U3CTrackDistanceU3Ec__Iterator0_t1867411235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GpsService/<TrackDistance>c__Iterator0::MoveNext()
extern "C"  bool U3CTrackDistanceU3Ec__Iterator0_MoveNext_m1250484952 (U3CTrackDistanceU3Ec__Iterator0_t1867411235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GpsService/<TrackDistance>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTrackDistanceU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2655661640 (U3CTrackDistanceU3Ec__Iterator0_t1867411235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GpsService/<TrackDistance>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTrackDistanceU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2957310400 (U3CTrackDistanceU3Ec__Iterator0_t1867411235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GpsService/<TrackDistance>c__Iterator0::Dispose()
extern "C"  void U3CTrackDistanceU3Ec__Iterator0_Dispose_m260178065 (U3CTrackDistanceU3Ec__Iterator0_t1867411235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GpsService/<TrackDistance>c__Iterator0::Reset()
extern "C"  void U3CTrackDistanceU3Ec__Iterator0_Reset_m2499746491 (U3CTrackDistanceU3Ec__Iterator0_t1867411235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
