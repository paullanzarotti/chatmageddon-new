﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsTileSetControl/CheckMarker2DVisibilityDelegate
struct CheckMarker2DVisibilityDelegate_t3802502927;
// System.Object
struct Il2CppObject;
// OnlineMapsMarker
struct OnlineMapsMarker_t3492166682;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3492166682.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void OnlineMapsTileSetControl/CheckMarker2DVisibilityDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void CheckMarker2DVisibilityDelegate__ctor_m3619096692 (CheckMarker2DVisibilityDelegate_t3802502927 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsTileSetControl/CheckMarker2DVisibilityDelegate::Invoke(OnlineMapsMarker)
extern "C"  bool CheckMarker2DVisibilityDelegate_Invoke_m3917179734 (CheckMarker2DVisibilityDelegate_t3802502927 * __this, OnlineMapsMarker_t3492166682 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnlineMapsTileSetControl/CheckMarker2DVisibilityDelegate::BeginInvoke(OnlineMapsMarker,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CheckMarker2DVisibilityDelegate_BeginInvoke_m2928328051 (CheckMarker2DVisibilityDelegate_t3802502927 * __this, OnlineMapsMarker_t3492166682 * ___marker0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsTileSetControl/CheckMarker2DVisibilityDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool CheckMarker2DVisibilityDelegate_EndInvoke_m2617920896 (CheckMarker2DVisibilityDelegate_t3802502927 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
