﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpriteSheetNGUIAnimation
struct SpriteSheetNGUIAnimation_t2524734547;

#include "codegen/il2cpp-codegen.h"

// System.Void SpriteSheetNGUIAnimation::.ctor()
extern "C"  void SpriteSheetNGUIAnimation__ctor_m710986168 (SpriteSheetNGUIAnimation_t2524734547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteSheetNGUIAnimation::OnAnimStart()
extern "C"  void SpriteSheetNGUIAnimation_OnAnimStart_m4285094248 (SpriteSheetNGUIAnimation_t2524734547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteSheetNGUIAnimation::OnAnimUpdate()
extern "C"  void SpriteSheetNGUIAnimation_OnAnimUpdate_m1346807825 (SpriteSheetNGUIAnimation_t2524734547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteSheetNGUIAnimation::OnAnimLooped()
extern "C"  void SpriteSheetNGUIAnimation_OnAnimLooped_m2214098429 (SpriteSheetNGUIAnimation_t2524734547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteSheetNGUIAnimation::OnAnimFinished()
extern "C"  void SpriteSheetNGUIAnimation_OnAnimFinished_m3619776660 (SpriteSheetNGUIAnimation_t2524734547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpriteSheetNGUIAnimation::OnResetCurrents()
extern "C"  void SpriteSheetNGUIAnimation_OnResetCurrents_m4093370074 (SpriteSheetNGUIAnimation_t2524734547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
