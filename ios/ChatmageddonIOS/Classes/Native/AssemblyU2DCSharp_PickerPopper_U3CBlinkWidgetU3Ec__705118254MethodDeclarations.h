﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PickerPopper/<BlinkWidget>c__Iterator2
struct U3CBlinkWidgetU3Ec__Iterator2_t705118254;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PickerPopper/<BlinkWidget>c__Iterator2::.ctor()
extern "C"  void U3CBlinkWidgetU3Ec__Iterator2__ctor_m3747504989 (U3CBlinkWidgetU3Ec__Iterator2_t705118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PickerPopper/<BlinkWidget>c__Iterator2::MoveNext()
extern "C"  bool U3CBlinkWidgetU3Ec__Iterator2_MoveNext_m3349005631 (U3CBlinkWidgetU3Ec__Iterator2_t705118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<BlinkWidget>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBlinkWidgetU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1283386343 (U3CBlinkWidgetU3Ec__Iterator2_t705118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PickerPopper/<BlinkWidget>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBlinkWidgetU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3936511967 (U3CBlinkWidgetU3Ec__Iterator2_t705118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<BlinkWidget>c__Iterator2::Dispose()
extern "C"  void U3CBlinkWidgetU3Ec__Iterator2_Dispose_m2200417712 (U3CBlinkWidgetU3Ec__Iterator2_t705118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PickerPopper/<BlinkWidget>c__Iterator2::Reset()
extern "C"  void U3CBlinkWidgetU3Ec__Iterator2_Reset_m1414592038 (U3CBlinkWidgetU3Ec__Iterator2_t705118254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
