﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserPayload
struct UserPayload_t146188169;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UserPayload::.ctor()
extern "C"  void UserPayload__ctor_m1877607510 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetLevelTag()
extern "C"  String_t* UserPayload_GetLevelTag_m1590126275 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetRankTag()
extern "C"  String_t* UserPayload_GetRankTag_m2881288159 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetStealthModeActiveTag()
extern "C"  String_t* UserPayload_GetStealthModeActiveTag_m3885858145 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetStealthModeDurationTag()
extern "C"  String_t* UserPayload_GetStealthModeDurationTag_m4026287879 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetStealthModeMinTag()
extern "C"  String_t* UserPayload_GetStealthModeMinTag_m682840799 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetStealthModeHourTag()
extern "C"  String_t* UserPayload_GetStealthModeHourTag_m3913217993 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetStealthModeTag()
extern "C"  String_t* UserPayload_GetStealthModeTag_m3747900581 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetDisplayLocationTag()
extern "C"  String_t* UserPayload_GetDisplayLocationTag_m559639940 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetGenderTag()
extern "C"  String_t* UserPayload_GetGenderTag_m3290465964 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetUserNameTag()
extern "C"  String_t* UserPayload_GetUserNameTag_m153107099 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetFirstNameTag()
extern "C"  String_t* UserPayload_GetFirstNameTag_m4133148532 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetSecondNameTag()
extern "C"  String_t* UserPayload_GetSecondNameTag_m3730413342 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetEmailTag()
extern "C"  String_t* UserPayload_GetEmailTag_m1715712457 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetFullNameTag()
extern "C"  String_t* UserPayload_GetFullNameTag_m679247165 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetIDTag()
extern "C"  String_t* UserPayload_GetIDTag_m2447284976 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetProfileImageTag()
extern "C"  String_t* UserPayload_GetProfileImageTag_m4172151285 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UserPayload::GetProfileThumbnailTag()
extern "C"  String_t* UserPayload_GetProfileThumbnailTag_m4004667390 (UserPayload_t146188169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
