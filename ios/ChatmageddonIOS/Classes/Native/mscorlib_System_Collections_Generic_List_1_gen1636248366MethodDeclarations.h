﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SettingsNavScreen>
struct List_1_t1636248366;
// System.Collections.Generic.IEnumerable`1<SettingsNavScreen>
struct IEnumerable_1_t2559254279;
// SettingsNavScreen[]
struct SettingsNavScreenU5BU5D_t655014135;
// System.Collections.Generic.IEnumerator`1<SettingsNavScreen>
struct IEnumerator_1_t4037618357;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<SettingsNavScreen>
struct ICollection_1_t3219202539;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>
struct ReadOnlyCollection_1_t2452912926;
// System.Predicate`1<SettingsNavScreen>
struct Predicate_1_t710097349;
// System.Action`1<SettingsNavScreen>
struct Action_1_t2068926616;
// System.Collections.Generic.IComparer`1<SettingsNavScreen>
struct IComparer_1_t221590356;
// System.Comparison`1<SettingsNavScreen>
struct Comparison_1_t3528866085;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1170978040.h"

// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::.ctor()
extern "C"  void List_1__ctor_m3910250662_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1__ctor_m3910250662(__this, method) ((  void (*) (List_1_t1636248366 *, const MethodInfo*))List_1__ctor_m3910250662_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1537200309_gshared (List_1_t1636248366 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1537200309(__this, ___collection0, method) ((  void (*) (List_1_t1636248366 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1537200309_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1258354967_gshared (List_1_t1636248366 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1258354967(__this, ___capacity0, method) ((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1__ctor_m1258354967_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m4026147793_gshared (List_1_t1636248366 * __this, SettingsNavScreenU5BU5D_t655014135* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m4026147793(__this, ___data0, ___size1, method) ((  void (*) (List_1_t1636248366 *, SettingsNavScreenU5BU5D_t655014135*, int32_t, const MethodInfo*))List_1__ctor_m4026147793_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::.cctor()
extern "C"  void List_1__cctor_m648064453_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m648064453(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m648064453_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4123329176_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4123329176(__this, method) ((  Il2CppObject* (*) (List_1_t1636248366 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4123329176_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2409279234_gshared (List_1_t1636248366 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2409279234(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1636248366 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2409279234_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3609992803_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3609992803(__this, method) ((  Il2CppObject * (*) (List_1_t1636248366 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3609992803_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2845041894_gshared (List_1_t1636248366 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2845041894(__this, ___item0, method) ((  int32_t (*) (List_1_t1636248366 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2845041894_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m4178966560_gshared (List_1_t1636248366 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m4178966560(__this, ___item0, method) ((  bool (*) (List_1_t1636248366 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m4178966560_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m773350776_gshared (List_1_t1636248366 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m773350776(__this, ___item0, method) ((  int32_t (*) (List_1_t1636248366 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m773350776_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3975184995_gshared (List_1_t1636248366 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3975184995(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1636248366 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3975184995_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m327958867_gshared (List_1_t1636248366 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m327958867(__this, ___item0, method) ((  void (*) (List_1_t1636248366 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m327958867_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m220924151_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m220924151(__this, method) ((  bool (*) (List_1_t1636248366 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m220924151_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3390447158_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3390447158(__this, method) ((  bool (*) (List_1_t1636248366 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3390447158_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m4154589714_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m4154589714(__this, method) ((  Il2CppObject * (*) (List_1_t1636248366 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4154589714_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3378138027_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3378138027(__this, method) ((  bool (*) (List_1_t1636248366 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3378138027_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3579664946_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3579664946(__this, method) ((  bool (*) (List_1_t1636248366 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3579664946_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m657963995_gshared (List_1_t1636248366 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m657963995(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m657963995_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m227857012_gshared (List_1_t1636248366 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m227857012(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1636248366 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m227857012_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Add(T)
extern "C"  void List_1_Add_m1584603639_gshared (List_1_t1636248366 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m1584603639(__this, ___item0, method) ((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_Add_m1584603639_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1414408656_gshared (List_1_t1636248366 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1414408656(__this, ___newCount0, method) ((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1414408656_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1870276063_gshared (List_1_t1636248366 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1870276063(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1636248366 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1870276063_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4265682088_gshared (List_1_t1636248366 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m4265682088(__this, ___collection0, method) ((  void (*) (List_1_t1636248366 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m4265682088_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3264928792_gshared (List_1_t1636248366 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3264928792(__this, ___enumerable0, method) ((  void (*) (List_1_t1636248366 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3264928792_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2424488727_gshared (List_1_t1636248366 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2424488727(__this, ___collection0, method) ((  void (*) (List_1_t1636248366 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2424488727_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SettingsNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2452912926 * List_1_AsReadOnly_m1079366266_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1079366266(__this, method) ((  ReadOnlyCollection_1_t2452912926 * (*) (List_1_t1636248366 *, const MethodInfo*))List_1_AsReadOnly_m1079366266_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Clear()
extern "C"  void List_1_Clear_m468316331_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_Clear_m468316331(__this, method) ((  void (*) (List_1_t1636248366 *, const MethodInfo*))List_1_Clear_m468316331_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m138786357_gshared (List_1_t1636248366 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m138786357(__this, ___item0, method) ((  bool (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_Contains_m138786357_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m167410078_gshared (List_1_t1636248366 * __this, SettingsNavScreenU5BU5D_t655014135* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m167410078(__this, ___array0, method) ((  void (*) (List_1_t1636248366 *, SettingsNavScreenU5BU5D_t655014135*, const MethodInfo*))List_1_CopyTo_m167410078_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3980658707_gshared (List_1_t1636248366 * __this, SettingsNavScreenU5BU5D_t655014135* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3980658707(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1636248366 *, SettingsNavScreenU5BU5D_t655014135*, int32_t, const MethodInfo*))List_1_CopyTo_m3980658707_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m3862137871_gshared (List_1_t1636248366 * __this, int32_t ___index0, SettingsNavScreenU5BU5D_t655014135* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m3862137871(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t1636248366 *, int32_t, SettingsNavScreenU5BU5D_t655014135*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3862137871_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m1663078047_gshared (List_1_t1636248366 * __this, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_Exists_m1663078047(__this, ___match0, method) ((  bool (*) (List_1_t1636248366 *, Predicate_1_t710097349 *, const MethodInfo*))List_1_Exists_m1663078047_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<SettingsNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m41796421_gshared (List_1_t1636248366 * __this, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_Find_m41796421(__this, ___match0, method) ((  int32_t (*) (List_1_t1636248366 *, Predicate_1_t710097349 *, const MethodInfo*))List_1_Find_m41796421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3119575762_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3119575762(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t710097349 *, const MethodInfo*))List_1_CheckMatch_m3119575762_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<SettingsNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t1636248366 * List_1_FindAll_m2357962212_gshared (List_1_t1636248366 * __this, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m2357962212(__this, ___match0, method) ((  List_1_t1636248366 * (*) (List_1_t1636248366 *, Predicate_1_t710097349 *, const MethodInfo*))List_1_FindAll_m2357962212_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<SettingsNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t1636248366 * List_1_FindAllStackBits_m3026706180_gshared (List_1_t1636248366 * __this, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m3026706180(__this, ___match0, method) ((  List_1_t1636248366 * (*) (List_1_t1636248366 *, Predicate_1_t710097349 *, const MethodInfo*))List_1_FindAllStackBits_m3026706180_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<SettingsNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t1636248366 * List_1_FindAllList_m1849464980_gshared (List_1_t1636248366 * __this, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m1849464980(__this, ___match0, method) ((  List_1_t1636248366 * (*) (List_1_t1636248366 *, Predicate_1_t710097349 *, const MethodInfo*))List_1_FindAllList_m1849464980_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m1166690528_gshared (List_1_t1636248366 * __this, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m1166690528(__this, ___match0, method) ((  int32_t (*) (List_1_t1636248366 *, Predicate_1_t710097349 *, const MethodInfo*))List_1_FindIndex_m1166690528_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3401585961_gshared (List_1_t1636248366 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t710097349 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3401585961(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1636248366 *, int32_t, int32_t, Predicate_1_t710097349 *, const MethodInfo*))List_1_GetIndex_m3401585961_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m2250258902_gshared (List_1_t1636248366 * __this, Action_1_t2068926616 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m2250258902(__this, ___action0, method) ((  void (*) (List_1_t1636248366 *, Action_1_t2068926616 *, const MethodInfo*))List_1_ForEach_m2250258902_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SettingsNavScreen>::GetEnumerator()
extern "C"  Enumerator_t1170978040  List_1_GetEnumerator_m1813322030_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1813322030(__this, method) ((  Enumerator_t1170978040  (*) (List_1_t1636248366 *, const MethodInfo*))List_1_GetEnumerator_m1813322030_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2541225895_gshared (List_1_t1636248366 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2541225895(__this, ___item0, method) ((  int32_t (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_IndexOf_m2541225895_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2224239734_gshared (List_1_t1636248366 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2224239734(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1636248366 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2224239734_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3111756011_gshared (List_1_t1636248366 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3111756011(__this, ___index0, method) ((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3111756011_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3523279888_gshared (List_1_t1636248366 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m3523279888(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1636248366 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m3523279888_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2570870269_gshared (List_1_t1636248366 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2570870269(__this, ___collection0, method) ((  void (*) (List_1_t1636248366 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2570870269_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<SettingsNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m3184738854_gshared (List_1_t1636248366 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3184738854(__this, ___item0, method) ((  bool (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_Remove_m3184738854_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2433748622_gshared (List_1_t1636248366 * __this, Predicate_1_t710097349 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2433748622(__this, ___match0, method) ((  int32_t (*) (List_1_t1636248366 *, Predicate_1_t710097349 *, const MethodInfo*))List_1_RemoveAll_m2433748622_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2064849348_gshared (List_1_t1636248366 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2064849348(__this, ___index0, method) ((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2064849348_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1135407335_gshared (List_1_t1636248366 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1135407335(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1636248366 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1135407335_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m4081455688_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_Reverse_m4081455688(__this, method) ((  void (*) (List_1_t1636248366 *, const MethodInfo*))List_1_Reverse_m4081455688_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Sort()
extern "C"  void List_1_Sort_m3721154656_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_Sort_m3721154656(__this, method) ((  void (*) (List_1_t1636248366 *, const MethodInfo*))List_1_Sort_m3721154656_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3105311804_gshared (List_1_t1636248366 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3105311804(__this, ___comparer0, method) ((  void (*) (List_1_t1636248366 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3105311804_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m854862749_gshared (List_1_t1636248366 * __this, Comparison_1_t3528866085 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m854862749(__this, ___comparison0, method) ((  void (*) (List_1_t1636248366 *, Comparison_1_t3528866085 *, const MethodInfo*))List_1_Sort_m854862749_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<SettingsNavScreen>::ToArray()
extern "C"  SettingsNavScreenU5BU5D_t655014135* List_1_ToArray_m3770425709_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_ToArray_m3770425709(__this, method) ((  SettingsNavScreenU5BU5D_t655014135* (*) (List_1_t1636248366 *, const MethodInfo*))List_1_ToArray_m3770425709_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3683772551_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3683772551(__this, method) ((  void (*) (List_1_t1636248366 *, const MethodInfo*))List_1_TrimExcess_m3683772551_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m598450861_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m598450861(__this, method) ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))List_1_get_Capacity_m598450861_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3166986120_gshared (List_1_t1636248366 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3166986120(__this, ___value0, method) ((  void (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3166986120_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<SettingsNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m845047222_gshared (List_1_t1636248366 * __this, const MethodInfo* method);
#define List_1_get_Count_m845047222(__this, method) ((  int32_t (*) (List_1_t1636248366 *, const MethodInfo*))List_1_get_Count_m845047222_gshared)(__this, method)
// T System.Collections.Generic.List`1<SettingsNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m3516750018_gshared (List_1_t1636248366 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3516750018(__this, ___index0, method) ((  int32_t (*) (List_1_t1636248366 *, int32_t, const MethodInfo*))List_1_get_Item_m3516750018_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SettingsNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m631849495_gshared (List_1_t1636248366 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m631849495(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1636248366 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m631849495_gshared)(__this, ___index0, ___value1, method)
