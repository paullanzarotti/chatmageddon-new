﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectGenderSwitchButton
struct SelectGenderSwitchButton_t1386910845;

#include "codegen/il2cpp-codegen.h"

// System.Void SelectGenderSwitchButton::.ctor()
extern "C"  void SelectGenderSwitchButton__ctor_m2147032512 (SelectGenderSwitchButton_t1386910845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectGenderSwitchButton::OnButtonClick()
extern "C"  void SelectGenderSwitchButton_OnButtonClick_m4173073409 (SelectGenderSwitchButton_t1386910845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
