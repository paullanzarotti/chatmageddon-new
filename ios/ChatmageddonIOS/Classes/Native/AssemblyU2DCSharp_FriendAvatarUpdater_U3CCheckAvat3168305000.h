﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Friend
struct Friend_t3555014108;
// FriendAvatarUpdater
struct FriendAvatarUpdater_t4187090262;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0
struct  U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000  : public Il2CppObject
{
public:
	// Friend FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::friend
	Friend_t3555014108 * ___friend_0;
	// FriendAvatarUpdater FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::$this
	FriendAvatarUpdater_t4187090262 * ___U24this_1;
	// System.Object FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 FriendAvatarUpdater/<CheckAvatarLoaded>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_friend_0() { return static_cast<int32_t>(offsetof(U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000, ___friend_0)); }
	inline Friend_t3555014108 * get_friend_0() const { return ___friend_0; }
	inline Friend_t3555014108 ** get_address_of_friend_0() { return &___friend_0; }
	inline void set_friend_0(Friend_t3555014108 * value)
	{
		___friend_0 = value;
		Il2CppCodeGenWriteBarrier(&___friend_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000, ___U24this_1)); }
	inline FriendAvatarUpdater_t4187090262 * get_U24this_1() const { return ___U24this_1; }
	inline FriendAvatarUpdater_t4187090262 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(FriendAvatarUpdater_t4187090262 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCheckAvatarLoadedU3Ec__Iterator0_t3168305000, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
