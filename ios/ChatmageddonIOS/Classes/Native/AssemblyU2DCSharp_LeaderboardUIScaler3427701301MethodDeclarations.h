﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardUIScaler
struct LeaderboardUIScaler_t3427701301;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardUIScaler::.ctor()
extern "C"  void LeaderboardUIScaler__ctor_m3860316552 (LeaderboardUIScaler_t3427701301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardUIScaler::GlobalUIScale()
extern "C"  void LeaderboardUIScaler_GlobalUIScale_m2678025983 (LeaderboardUIScaler_t3427701301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
