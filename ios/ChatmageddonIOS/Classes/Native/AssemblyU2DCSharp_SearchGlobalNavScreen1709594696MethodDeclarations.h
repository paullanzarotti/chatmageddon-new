﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SearchGlobalNavScreen
struct SearchGlobalNavScreen_t1709594696;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void SearchGlobalNavScreen::.ctor()
extern "C"  void SearchGlobalNavScreen__ctor_m1332976503 (SearchGlobalNavScreen_t1709594696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchGlobalNavScreen::Start()
extern "C"  void SearchGlobalNavScreen_Start_m2125272419 (SearchGlobalNavScreen_t1709594696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchGlobalNavScreen::UIClosing()
extern "C"  void SearchGlobalNavScreen_UIClosing_m1776866238 (SearchGlobalNavScreen_t1709594696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchGlobalNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void SearchGlobalNavScreen_ScreenLoad_m2960594789 (SearchGlobalNavScreen_t1709594696 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchGlobalNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void SearchGlobalNavScreen_ScreenUnload_m278064577 (SearchGlobalNavScreen_t1709594696 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SearchGlobalNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool SearchGlobalNavScreen_ValidateScreenNavigation_m3923429180 (SearchGlobalNavScreen_t1709594696 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
