﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PanelType,System.Object>
struct Dictionary_2_t3846390612;
// System.Collections.Generic.IEqualityComparer`1<PanelType>
struct IEqualityComparer_1_t3990369304;
// System.Collections.Generic.IDictionary`2<PanelType,System.Object>
struct IDictionary_2_t1845474033;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<PanelType>
struct ICollection_1_t1434844535;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3933345087;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>
struct IEnumerator_1_t3374226957;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<PanelType,System.Object>
struct KeyCollection_t2034921087;
// System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,System.Object>
struct ValueCollection_t2549450455;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21603735834.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En871448018.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m518702647_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m518702647(__this, method) ((  void (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2__ctor_m518702647_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2055316988_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2055316988(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2055316988_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2868937009_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2868937009(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2868937009_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m172282272_gshared (Dictionary_2_t3846390612 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m172282272(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3846390612 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m172282272_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1857388224_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1857388224(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1857388224_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m619419273_gshared (Dictionary_2_t3846390612 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m619419273(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3846390612 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m619419273_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2992150130_gshared (Dictionary_2_t3846390612 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2992150130(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3846390612 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2992150130_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m829824803_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m829824803(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m829824803_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1482093219_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1482093219(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1482093219_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2464709761_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2464709761(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2464709761_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3785625177_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3785625177(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3785625177_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3633850792_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3633850792(__this, method) ((  bool (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3633850792_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3166630141_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3166630141(__this, method) ((  bool (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3166630141_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2968121863_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2968121863(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2968121863_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m4058603438_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m4058603438(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m4058603438_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3862960969_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3862960969(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3862960969_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m448838501_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m448838501(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3846390612 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m448838501_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1136250794_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1136250794(__this, ___key0, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1136250794_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3292935283_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3292935283(__this, method) ((  bool (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3292935283_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1757026051_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1757026051(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1757026051_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3717557329_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3717557329(__this, method) ((  bool (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3717557329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m185654672_gshared (Dictionary_2_t3846390612 * __this, KeyValuePair_2_t1603735834  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m185654672(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3846390612 *, KeyValuePair_2_t1603735834 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m185654672_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3479089936_gshared (Dictionary_2_t3846390612 * __this, KeyValuePair_2_t1603735834  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3479089936(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3846390612 *, KeyValuePair_2_t1603735834 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3479089936_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m260891924_gshared (Dictionary_2_t3846390612 * __this, KeyValuePair_2U5BU5D_t3933345087* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m260891924(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3846390612 *, KeyValuePair_2U5BU5D_t3933345087*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m260891924_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3706498783_gshared (Dictionary_2_t3846390612 * __this, KeyValuePair_2_t1603735834  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3706498783(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3846390612 *, KeyValuePair_2_t1603735834 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3706498783_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4144983059_gshared (Dictionary_2_t3846390612 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4144983059(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4144983059_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1946789214_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1946789214(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1946789214_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3335706201_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3335706201(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3335706201_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<PanelType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2845650508_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2845650508(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2845650508_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<PanelType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m8788459_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m8788459(__this, method) ((  int32_t (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_get_Count_m8788459_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<PanelType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2717784348_gshared (Dictionary_2_t3846390612 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2717784348(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2717784348_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1192062103_gshared (Dictionary_2_t3846390612 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1192062103(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3846390612 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m1192062103_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2885162279_gshared (Dictionary_2_t3846390612 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2885162279(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3846390612 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2885162279_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2859193660_gshared (Dictionary_2_t3846390612 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2859193660(__this, ___size0, method) ((  void (*) (Dictionary_2_t3846390612 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2859193660_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4034449154_gshared (Dictionary_2_t3846390612 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4034449154(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4034449154_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<PanelType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1603735834  Dictionary_2_make_pair_m3900704692_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3900704692(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1603735834  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3900704692_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<PanelType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1949244466_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1949244466(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1949244466_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<PanelType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m1150851946_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1150851946(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m1150851946_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1845351075_gshared (Dictionary_2_t3846390612 * __this, KeyValuePair_2U5BU5D_t3933345087* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1845351075(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3846390612 *, KeyValuePair_2U5BU5D_t3933345087*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1845351075_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m806152857_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m806152857(__this, method) ((  void (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_Resize_m806152857_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4267061612_gshared (Dictionary_2_t3846390612 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4267061612(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3846390612 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m4267061612_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2421049804_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2421049804(__this, method) ((  void (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_Clear_m2421049804_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3070324140_gshared (Dictionary_2_t3846390612 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3070324140(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3846390612 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3070324140_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3273063508_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3273063508(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3846390612 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3273063508_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3772229119_gshared (Dictionary_2_t3846390612 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3772229119(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3846390612 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3772229119_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<PanelType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m550662847_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m550662847(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3846390612 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m550662847_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m767555888_gshared (Dictionary_2_t3846390612 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m767555888(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3846390612 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m767555888_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m925853631_gshared (Dictionary_2_t3846390612 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m925853631(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3846390612 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m925853631_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<PanelType,System.Object>::get_Keys()
extern "C"  KeyCollection_t2034921087 * Dictionary_2_get_Keys_m411477256_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m411477256(__this, method) ((  KeyCollection_t2034921087 * (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_get_Keys_m411477256_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<PanelType,System.Object>::get_Values()
extern "C"  ValueCollection_t2549450455 * Dictionary_2_get_Values_m3882202040_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3882202040(__this, method) ((  ValueCollection_t2549450455 * (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_get_Values_m3882202040_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<PanelType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1502215169_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1502215169(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3846390612 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1502215169_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<PanelType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m781252785_gshared (Dictionary_2_t3846390612 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m781252785(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3846390612 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m781252785_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PanelType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1518832495_gshared (Dictionary_2_t3846390612 * __this, KeyValuePair_2_t1603735834  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1518832495(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3846390612 *, KeyValuePair_2_t1603735834 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1518832495_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<PanelType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t871448018  Dictionary_2_GetEnumerator_m1735109436_gshared (Dictionary_2_t3846390612 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1735109436(__this, method) ((  Enumerator_t871448018  (*) (Dictionary_2_t3846390612 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1735109436_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<PanelType,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__2_m3617162691_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m3617162691(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m3617162691_gshared)(__this /* static, unused */, ___key0, ___value1, method)
