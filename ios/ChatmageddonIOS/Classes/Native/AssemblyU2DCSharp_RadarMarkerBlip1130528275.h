﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// TweenColor
struct TweenColor_t3390486518;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadarMarkerBlip
struct  RadarMarkerBlip_t1130528275  : public MonoBehaviour_t1158329972
{
public:
	// OnlineMapsMarker3D RadarMarkerBlip::instance
	OnlineMapsMarker3D_t576815539 * ___instance_2;
	// TweenColor RadarMarkerBlip::colorTween
	TweenColor_t3390486518 * ___colorTween_3;
	// UnityEngine.Color RadarMarkerBlip::blipColor
	Color_t2020392075  ___blipColor_4;
	// UnityEngine.SpriteRenderer RadarMarkerBlip::sp
	SpriteRenderer_t1209076198 * ___sp_5;
	// System.Single RadarMarkerBlip::markerScale
	float ___markerScale_6;
	// UnityEngine.Vector3 RadarMarkerBlip::colliderCenter
	Vector3_t2243707580  ___colliderCenter_7;
	// UnityEngine.Vector3 RadarMarkerBlip::colliderSize
	Vector3_t2243707580  ___colliderSize_8;
	// UnityEngine.BoxCollider RadarMarkerBlip::markerCollider
	BoxCollider_t22920061 * ___markerCollider_9;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___instance_2)); }
	inline OnlineMapsMarker3D_t576815539 * get_instance_2() const { return ___instance_2; }
	inline OnlineMapsMarker3D_t576815539 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(OnlineMapsMarker3D_t576815539 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_colorTween_3() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___colorTween_3)); }
	inline TweenColor_t3390486518 * get_colorTween_3() const { return ___colorTween_3; }
	inline TweenColor_t3390486518 ** get_address_of_colorTween_3() { return &___colorTween_3; }
	inline void set_colorTween_3(TweenColor_t3390486518 * value)
	{
		___colorTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___colorTween_3, value);
	}

	inline static int32_t get_offset_of_blipColor_4() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___blipColor_4)); }
	inline Color_t2020392075  get_blipColor_4() const { return ___blipColor_4; }
	inline Color_t2020392075 * get_address_of_blipColor_4() { return &___blipColor_4; }
	inline void set_blipColor_4(Color_t2020392075  value)
	{
		___blipColor_4 = value;
	}

	inline static int32_t get_offset_of_sp_5() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___sp_5)); }
	inline SpriteRenderer_t1209076198 * get_sp_5() const { return ___sp_5; }
	inline SpriteRenderer_t1209076198 ** get_address_of_sp_5() { return &___sp_5; }
	inline void set_sp_5(SpriteRenderer_t1209076198 * value)
	{
		___sp_5 = value;
		Il2CppCodeGenWriteBarrier(&___sp_5, value);
	}

	inline static int32_t get_offset_of_markerScale_6() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___markerScale_6)); }
	inline float get_markerScale_6() const { return ___markerScale_6; }
	inline float* get_address_of_markerScale_6() { return &___markerScale_6; }
	inline void set_markerScale_6(float value)
	{
		___markerScale_6 = value;
	}

	inline static int32_t get_offset_of_colliderCenter_7() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___colliderCenter_7)); }
	inline Vector3_t2243707580  get_colliderCenter_7() const { return ___colliderCenter_7; }
	inline Vector3_t2243707580 * get_address_of_colliderCenter_7() { return &___colliderCenter_7; }
	inline void set_colliderCenter_7(Vector3_t2243707580  value)
	{
		___colliderCenter_7 = value;
	}

	inline static int32_t get_offset_of_colliderSize_8() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___colliderSize_8)); }
	inline Vector3_t2243707580  get_colliderSize_8() const { return ___colliderSize_8; }
	inline Vector3_t2243707580 * get_address_of_colliderSize_8() { return &___colliderSize_8; }
	inline void set_colliderSize_8(Vector3_t2243707580  value)
	{
		___colliderSize_8 = value;
	}

	inline static int32_t get_offset_of_markerCollider_9() { return static_cast<int32_t>(offsetof(RadarMarkerBlip_t1130528275, ___markerCollider_9)); }
	inline BoxCollider_t22920061 * get_markerCollider_9() const { return ___markerCollider_9; }
	inline BoxCollider_t22920061 ** get_address_of_markerCollider_9() { return &___markerCollider_9; }
	inline void set_markerCollider_9(BoxCollider_t22920061 * value)
	{
		___markerCollider_9 = value;
		Il2CppCodeGenWriteBarrier(&___markerCollider_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
