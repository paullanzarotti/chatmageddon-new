﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackInventoryUIScaler
struct AttackInventoryUIScaler_t3118091758;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackInventoryUIScaler::.ctor()
extern "C"  void AttackInventoryUIScaler__ctor_m891180775 (AttackInventoryUIScaler_t3118091758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackInventoryUIScaler::GlobalUIScale()
extern "C"  void AttackInventoryUIScaler_GlobalUIScale_m1933608004 (AttackInventoryUIScaler_t3118091758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
