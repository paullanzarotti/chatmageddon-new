﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModalPanel
struct ModalPanel_t3020707755;

#include "codegen/il2cpp-codegen.h"

// System.Void ModalPanel::.ctor()
extern "C"  void ModalPanel__ctor_m630973376 (ModalPanel_t3020707755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalPanel::SetPanelScene()
extern "C"  void ModalPanel_SetPanelScene_m986116566 (ModalPanel_t3020707755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalPanel::SetCloseButtonActive(System.Boolean)
extern "C"  void ModalPanel_SetCloseButtonActive_m2159583971 (ModalPanel_t3020707755 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalPanel::ShowCloseButton(System.Boolean)
extern "C"  void ModalPanel_ShowCloseButton_m3862618202 (ModalPanel_t3020707755 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
