﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// UITexture
struct UITexture_t2537039969;
// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseInfiniteListItem`1<StatusListItem>
struct  BaseInfiniteListItem_1_t1234741977  : public MonoBehaviour_t1158329972
{
public:
	// UIPanel BaseInfiniteListItem`1::panel
	UIPanel_t1795085332 * ___panel_2;
	// System.Int32 BaseInfiniteListItem`1::itemDataIndex
	int32_t ___itemDataIndex_3;
	// System.Int32 BaseInfiniteListItem`1::itemNumber
	int32_t ___itemNumber_4;
	// System.Int32 BaseInfiniteListItem`1::carouselNumber
	int32_t ___carouselNumber_5;
	// UITexture BaseInfiniteListItem`1::backgroundTexture
	UITexture_t2537039969 * ___backgroundTexture_6;
	// UISprite BaseInfiniteListItem`1::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_7;
	// UnityEngine.BoxCollider BaseInfiniteListItem`1::thisCollider
	BoxCollider_t22920061 * ___thisCollider_8;
	// System.Boolean BaseInfiniteListItem`1::isVisible
	bool ___isVisible_9;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___panel_2)); }
	inline UIPanel_t1795085332 * get_panel_2() const { return ___panel_2; }
	inline UIPanel_t1795085332 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(UIPanel_t1795085332 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_itemDataIndex_3() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___itemDataIndex_3)); }
	inline int32_t get_itemDataIndex_3() const { return ___itemDataIndex_3; }
	inline int32_t* get_address_of_itemDataIndex_3() { return &___itemDataIndex_3; }
	inline void set_itemDataIndex_3(int32_t value)
	{
		___itemDataIndex_3 = value;
	}

	inline static int32_t get_offset_of_itemNumber_4() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___itemNumber_4)); }
	inline int32_t get_itemNumber_4() const { return ___itemNumber_4; }
	inline int32_t* get_address_of_itemNumber_4() { return &___itemNumber_4; }
	inline void set_itemNumber_4(int32_t value)
	{
		___itemNumber_4 = value;
	}

	inline static int32_t get_offset_of_carouselNumber_5() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___carouselNumber_5)); }
	inline int32_t get_carouselNumber_5() const { return ___carouselNumber_5; }
	inline int32_t* get_address_of_carouselNumber_5() { return &___carouselNumber_5; }
	inline void set_carouselNumber_5(int32_t value)
	{
		___carouselNumber_5 = value;
	}

	inline static int32_t get_offset_of_backgroundTexture_6() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___backgroundTexture_6)); }
	inline UITexture_t2537039969 * get_backgroundTexture_6() const { return ___backgroundTexture_6; }
	inline UITexture_t2537039969 ** get_address_of_backgroundTexture_6() { return &___backgroundTexture_6; }
	inline void set_backgroundTexture_6(UITexture_t2537039969 * value)
	{
		___backgroundTexture_6 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTexture_6, value);
	}

	inline static int32_t get_offset_of_backgroundSprite_7() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___backgroundSprite_7)); }
	inline UISprite_t603616735 * get_backgroundSprite_7() const { return ___backgroundSprite_7; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_7() { return &___backgroundSprite_7; }
	inline void set_backgroundSprite_7(UISprite_t603616735 * value)
	{
		___backgroundSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_7, value);
	}

	inline static int32_t get_offset_of_thisCollider_8() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___thisCollider_8)); }
	inline BoxCollider_t22920061 * get_thisCollider_8() const { return ___thisCollider_8; }
	inline BoxCollider_t22920061 ** get_address_of_thisCollider_8() { return &___thisCollider_8; }
	inline void set_thisCollider_8(BoxCollider_t22920061 * value)
	{
		___thisCollider_8 = value;
		Il2CppCodeGenWriteBarrier(&___thisCollider_8, value);
	}

	inline static int32_t get_offset_of_isVisible_9() { return static_cast<int32_t>(offsetof(BaseInfiniteListItem_1_t1234741977, ___isVisible_9)); }
	inline bool get_isVisible_9() const { return ___isVisible_9; }
	inline bool* get_address_of_isVisible_9() { return &___isVisible_9; }
	inline void set_isVisible_9(bool value)
	{
		___isVisible_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
