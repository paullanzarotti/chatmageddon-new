﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttemptTab
struct AttemptTab_t3964708364;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void AttemptTab::.ctor()
extern "C"  void AttemptTab__ctor_m1849003881 (AttemptTab_t3964708364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttemptTab::SetTabColour(UnityEngine.Color)
extern "C"  void AttemptTab_SetTabColour_m1411074140 (AttemptTab_t3964708364 * __this, Color_t2020392075  ___tabColour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttemptTab::PlayTabTween(System.Boolean)
extern "C"  void AttemptTab_PlayTabTween_m2176869606 (AttemptTab_t3964708364 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
