﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SetTotalPoints
struct SetTotalPoints_t1575563713;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SetTotalPoints::.ctor()
extern "C"  void SetTotalPoints__ctor_m3322981116 (SetTotalPoints_t1575563713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetTotalPoints::OnButtonClick()
extern "C"  void SetTotalPoints_OnButtonClick_m210539477 (SetTotalPoints_t1575563713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetTotalPoints::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void SetTotalPoints_U3COnButtonClickU3Em__0_m653735867 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
