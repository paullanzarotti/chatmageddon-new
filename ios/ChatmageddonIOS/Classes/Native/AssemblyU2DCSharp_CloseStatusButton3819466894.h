﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseStatusButton
struct  CloseStatusButton_t3819466894  : public SFXButton_t792651341
{
public:
	// PanelType CloseStatusButton::panel
	int32_t ___panel_5;

public:
	inline static int32_t get_offset_of_panel_5() { return static_cast<int32_t>(offsetof(CloseStatusButton_t3819466894, ___panel_5)); }
	inline int32_t get_panel_5() const { return ___panel_5; }
	inline int32_t* get_address_of_panel_5() { return &___panel_5; }
	inline void set_panel_5(int32_t value)
	{
		___panel_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
