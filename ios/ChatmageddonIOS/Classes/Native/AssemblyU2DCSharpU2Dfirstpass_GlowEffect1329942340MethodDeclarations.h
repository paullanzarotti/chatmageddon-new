﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlowEffect
struct GlowEffect_t1329942340;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void GlowEffect::.ctor()
extern "C"  void GlowEffect__ctor_m3608836313 (GlowEffect_t1329942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material GlowEffect::get_compositeMaterial()
extern "C"  Material_t193706927 * GlowEffect_get_compositeMaterial_m3811866257 (GlowEffect_t1329942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material GlowEffect::get_blurMaterial()
extern "C"  Material_t193706927 * GlowEffect_get_blurMaterial_m1418737277 (GlowEffect_t1329942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material GlowEffect::get_downsampleMaterial()
extern "C"  Material_t193706927 * GlowEffect_get_downsampleMaterial_m1216303758 (GlowEffect_t1329942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowEffect::OnDisable()
extern "C"  void GlowEffect_OnDisable_m3351407078 (GlowEffect_t1329942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowEffect::Start()
extern "C"  void GlowEffect_Start_m1052539385 (GlowEffect_t1329942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowEffect::FourTapCone(UnityEngine.RenderTexture,UnityEngine.RenderTexture,System.Int32)
extern "C"  void GlowEffect_FourTapCone_m2919783318 (GlowEffect_t1329942340 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___dest1, int32_t ___iteration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowEffect::DownSample4x(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void GlowEffect_DownSample4x_m1121946133 (GlowEffect_t1329942340 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void GlowEffect_OnRenderImage_m288891809 (GlowEffect_t1329942340 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowEffect::BlitGlow(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void GlowEffect_BlitGlow_m4086147353 (GlowEffect_t1329942340 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
