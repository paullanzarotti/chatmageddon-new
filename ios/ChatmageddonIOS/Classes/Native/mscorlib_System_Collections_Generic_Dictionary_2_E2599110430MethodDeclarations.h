﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3155792410MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3881859462(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2599110430 *, Dictionary_2_t1279085728 *, const MethodInfo*))Enumerator__ctor_m2035396300_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1592359895(__this, method) ((  Il2CppObject * (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m180864481_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1676071999(__this, method) ((  void (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2872824993_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3139504514(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m253262698_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m142426785(__this, method) ((  Il2CppObject * (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m356115379_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m753847945(__this, method) ((  Il2CppObject * (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2505263771_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::MoveNext()
#define Enumerator_MoveNext_m2304583915(__this, method) ((  bool (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_MoveNext_m2125092321_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::get_Current()
#define Enumerator_get_Current_m1372774195(__this, method) ((  KeyValuePair_2_t3331398246  (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_get_Current_m4240631493_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2500988646(__this, method) ((  GameObject_t1756533147 * (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_get_CurrentKey_m3877868020_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m562447670(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_get_CurrentValue_m3593693876_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::Reset()
#define Enumerator_Reset_m1385031708(__this, method) ((  void (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_Reset_m3146199818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::VerifyState()
#define Enumerator_VerifyState_m3420979409(__this, method) ((  void (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_VerifyState_m1769814515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2049993431(__this, method) ((  void (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_VerifyCurrent_m4157560473_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.GameObject,UnityEngine.Vector3>::Dispose()
#define Enumerator_Dispose_m4265576490(__this, method) ((  void (*) (Enumerator_t2599110430 *, const MethodInfo*))Enumerator_Dispose_m498224468_gshared)(__this, method)
