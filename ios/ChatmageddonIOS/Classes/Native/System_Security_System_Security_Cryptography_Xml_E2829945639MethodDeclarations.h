﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptedXml
struct EncryptedXml_t2829945639;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Text.Encoding
struct Encoding_t663144255;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Security.Cryptography.Xml.EncryptedData
struct EncryptedData_t1564008194;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1108166522;
// System.Security.Cryptography.Xml.EncryptedKey
struct EncryptedKey_t3186175257;
// System.Security.Cryptography.RSA
struct RSA_t3719518354;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t281704372;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode3032142640.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Security_System_Security_Cryptography_Xml_E1564008194.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg1108166522.h"
#include "System_Security_System_Security_Cryptography_Xml_E3186175257.h"
#include "mscorlib_System_Security_Cryptography_RSA3719518354.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.EncryptedXml::.ctor()
extern "C"  void EncryptedXml__ctor_m3697292253 (EncryptedXml_t2829945639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedXml::.ctor(System.Xml.XmlDocument)
extern "C"  void EncryptedXml__ctor_m2731385319 (EncryptedXml_t2829945639 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Security.Cryptography.Xml.EncryptedXml::get_Encoding()
extern "C"  Encoding_t663144255 * EncryptedXml_get_Encoding_m3747053307 (EncryptedXml_t2829945639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.PaddingMode System.Security.Cryptography.Xml.EncryptedXml::get_Padding()
extern "C"  int32_t EncryptedXml_get_Padding_m1841629675 (EncryptedXml_t2829945639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedXml::AddKeyNameMapping(System.String,System.Object)
extern "C"  void EncryptedXml_AddKeyNameMapping_m1766692704 (EncryptedXml_t2829945639 * __this, String_t* ___keyName0, Il2CppObject * ___keyObject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::DecryptData(System.Security.Cryptography.Xml.EncryptedData,System.Security.Cryptography.SymmetricAlgorithm)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_DecryptData_m760812409 (EncryptedXml_t2829945639 * __this, EncryptedData_t1564008194 * ___encryptedData0, SymmetricAlgorithm_t1108166522 * ___symAlg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedXml::DecryptDocument()
extern "C"  void EncryptedXml_DecryptDocument_m2137891155 (EncryptedXml_t2829945639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::DecryptEncryptedKey(System.Security.Cryptography.Xml.EncryptedKey)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_DecryptEncryptedKey_m4033138450 (EncryptedXml_t2829945639 * __this, EncryptedKey_t3186175257 * ___encryptedKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::DecryptKey(System.Byte[],System.Security.Cryptography.SymmetricAlgorithm)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_DecryptKey_m3164434491 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___keyData0, SymmetricAlgorithm_t1108166522 * ___symAlg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::DecryptKey(System.Byte[],System.Security.Cryptography.RSA,System.Boolean)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_DecryptKey_m2305924976 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___keyData0, RSA_t3719518354 * ___rsa1, bool ___fOAEP2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.EncryptedData System.Security.Cryptography.Xml.EncryptedXml::Encrypt(System.Xml.XmlElement,System.String)
extern "C"  EncryptedData_t1564008194 * EncryptedXml_Encrypt_m749504988 (EncryptedXml_t2829945639 * __this, XmlElement_t2877111883 * ___inputElement0, String_t* ___keyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::EncryptData(System.Byte[],System.Security.Cryptography.SymmetricAlgorithm)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_EncryptData_m2495388316 (EncryptedXml_t2829945639 * __this, ByteU5BU5D_t3397334013* ___plainText0, SymmetricAlgorithm_t1108166522 * ___symAlg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::EncryptDataCore(System.Byte[],System.Security.Cryptography.SymmetricAlgorithm)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_EncryptDataCore_m2395323613 (EncryptedXml_t2829945639 * __this, ByteU5BU5D_t3397334013* ___plainText0, SymmetricAlgorithm_t1108166522 * ___symAlg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::EncryptData(System.Xml.XmlElement,System.Security.Cryptography.SymmetricAlgorithm,System.Boolean)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_EncryptData_m1059280113 (EncryptedXml_t2829945639 * __this, XmlElement_t2877111883 * ___inputElement0, SymmetricAlgorithm_t1108166522 * ___symAlg1, bool ___content2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::EncryptKey(System.Byte[],System.Security.Cryptography.SymmetricAlgorithm)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_EncryptKey_m3785243013 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___keyData0, SymmetricAlgorithm_t1108166522 * ___symAlg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::EncryptKey(System.Byte[],System.Security.Cryptography.RSA,System.Boolean)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_EncryptKey_m3312478572 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___keyData0, RSA_t3719518354 * ___rsa1, bool ___fOAEP2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.SymmetricAlgorithm System.Security.Cryptography.Xml.EncryptedXml::GetAlgorithm(System.String)
extern "C"  SymmetricAlgorithm_t1108166522 * EncryptedXml_GetAlgorithm_m4265727976 (Il2CppObject * __this /* static, unused */, String_t* ___symAlgUri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedXml::GetAlgorithmUri(System.Security.Cryptography.SymmetricAlgorithm)
extern "C"  String_t* EncryptedXml_GetAlgorithmUri_m3191865334 (Il2CppObject * __this /* static, unused */, SymmetricAlgorithm_t1108166522 * ___symAlg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedXml::GetKeyWrapAlgorithmUri(System.Object)
extern "C"  String_t* EncryptedXml_GetKeyWrapAlgorithmUri_m3999552300 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___keyAlg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::GetDecryptionIV(System.Security.Cryptography.Xml.EncryptedData,System.String)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_GetDecryptionIV_m2696908221 (EncryptedXml_t2829945639 * __this, EncryptedData_t1564008194 * ___encryptedData0, String_t* ___symAlgUri1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.SymmetricAlgorithm System.Security.Cryptography.Xml.EncryptedXml::GetDecryptionKey(System.Security.Cryptography.Xml.EncryptedData,System.String)
extern "C"  SymmetricAlgorithm_t1108166522 * EncryptedXml_GetDecryptionKey_m158641105 (EncryptedXml_t2829945639 * __this, EncryptedData_t1564008194 * ___encryptedData0, String_t* ___symAlgUri1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedXml::ReplaceData(System.Xml.XmlElement,System.Byte[])
extern "C"  void EncryptedXml_ReplaceData_m2601477353 (EncryptedXml_t2829945639 * __this, XmlElement_t2877111883 * ___inputElement0, ByteU5BU5D_t3397334013* ___decryptedData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::Transform(System.Byte[],System.Security.Cryptography.ICryptoTransform)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_Transform_m895876359 (EncryptedXml_t2829945639 * __this, ByteU5BU5D_t3397334013* ___data0, Il2CppObject * ___transform1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.Xml.EncryptedXml::Transform(System.Byte[],System.Security.Cryptography.ICryptoTransform,System.Int32,System.Boolean)
extern "C"  ByteU5BU5D_t3397334013* EncryptedXml_Transform_m3497681209 (EncryptedXml_t2829945639 * __this, ByteU5BU5D_t3397334013* ___data0, Il2CppObject * ___transform1, int32_t ___blockOctetCount2, bool ___trimPadding3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
