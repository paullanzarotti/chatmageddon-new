﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<LoadImageFromURL>c__AnonStorey1F<System.Object>
struct U3CLoadImageFromURLU3Ec__AnonStorey1F_t2169722064;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<LoadImageFromURL>c__AnonStorey1F<System.Object>::.ctor()
extern "C"  void U3CLoadImageFromURLU3Ec__AnonStorey1F__ctor_m60528919_gshared (U3CLoadImageFromURLU3Ec__AnonStorey1F_t2169722064 * __this, const MethodInfo* method);
#define U3CLoadImageFromURLU3Ec__AnonStorey1F__ctor_m60528919(__this, method) ((  void (*) (U3CLoadImageFromURLU3Ec__AnonStorey1F_t2169722064 *, const MethodInfo*))U3CLoadImageFromURLU3Ec__AnonStorey1F__ctor_m60528919_gshared)(__this, method)
// System.Void BaseServer`1/<LoadImageFromURL>c__AnonStorey1F<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CLoadImageFromURLU3Ec__AnonStorey1F_U3CU3Em__0_m4053787144_gshared (U3CLoadImageFromURLU3Ec__AnonStorey1F_t2169722064 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CLoadImageFromURLU3Ec__AnonStorey1F_U3CU3Em__0_m4053787144(__this, ___request0, ___response1, method) ((  void (*) (U3CLoadImageFromURLU3Ec__AnonStorey1F_t2169722064 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CLoadImageFromURLU3Ec__AnonStorey1F_U3CU3Em__0_m4053787144_gshared)(__this, ___request0, ___response1, method)
