﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginSuccessUIScaler
struct LoginSuccessUIScaler_t10393798;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginSuccessUIScaler::.ctor()
extern "C"  void LoginSuccessUIScaler__ctor_m3748414725 (LoginSuccessUIScaler_t10393798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessUIScaler::GlobalUIScale()
extern "C"  void LoginSuccessUIScaler_GlobalUIScale_m1628014304 (LoginSuccessUIScaler_t10393798 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
