﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TilesetOverlayExample
struct TilesetOverlayExample_t4212851882;

#include "codegen/il2cpp-codegen.h"

// System.Void TilesetOverlayExample::.ctor()
extern "C"  void TilesetOverlayExample__ctor_m2749803303 (TilesetOverlayExample_t4212851882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetOverlayExample::Start()
extern "C"  void TilesetOverlayExample_Start_m3611627419 (TilesetOverlayExample_t4212851882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetOverlayExample::UpdateMesh()
extern "C"  void TilesetOverlayExample_UpdateMesh_m1375264153 (TilesetOverlayExample_t4212851882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TilesetOverlayExample::Update()
extern "C"  void TilesetOverlayExample_Update_m3458834868 (TilesetOverlayExample_t4212851882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
