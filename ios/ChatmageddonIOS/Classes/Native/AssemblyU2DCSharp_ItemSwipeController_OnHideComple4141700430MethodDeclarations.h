﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemSwipeController/OnHideComplete
struct OnHideComplete_t4141700430;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void ItemSwipeController/OnHideComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void OnHideComplete__ctor_m3299415233 (OnHideComplete_t4141700430 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController/OnHideComplete::Invoke(System.Boolean)
extern "C"  void OnHideComplete_Invoke_m3529495824 (OnHideComplete_t4141700430 * __this, bool ___closed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ItemSwipeController/OnHideComplete::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnHideComplete_BeginInvoke_m1335480885 (OnHideComplete_t4141700430 * __this, bool ___closed0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemSwipeController/OnHideComplete::EndInvoke(System.IAsyncResult)
extern "C"  void OnHideComplete_EndInvoke_m1995526891 (OnHideComplete_t4141700430 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
