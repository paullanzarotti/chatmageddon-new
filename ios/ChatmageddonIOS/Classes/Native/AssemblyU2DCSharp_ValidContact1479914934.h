﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ValidContact
struct  ValidContact_t1479914934  : public Il2CppObject
{
public:
	// System.String ValidContact::firstName
	String_t* ___firstName_0;
	// System.String ValidContact::lastName
	String_t* ___lastName_1;
	// System.Collections.Generic.List`1<System.String> ValidContact::phoneNumbers
	List_1_t1398341365 * ___phoneNumbers_2;
	// System.Collections.Generic.List`1<System.String> ValidContact::phoneNumberHashes
	List_1_t1398341365 * ___phoneNumberHashes_3;
	// System.String ValidContact::rawChecksum
	String_t* ___rawChecksum_4;
	// System.String ValidContact::validatedChecksum
	String_t* ___validatedChecksum_5;
	// System.Boolean ValidContact::didFind
	bool ___didFind_6;
	// System.Boolean ValidContact::hasChanged
	bool ___hasChanged_7;

public:
	inline static int32_t get_offset_of_firstName_0() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___firstName_0)); }
	inline String_t* get_firstName_0() const { return ___firstName_0; }
	inline String_t** get_address_of_firstName_0() { return &___firstName_0; }
	inline void set_firstName_0(String_t* value)
	{
		___firstName_0 = value;
		Il2CppCodeGenWriteBarrier(&___firstName_0, value);
	}

	inline static int32_t get_offset_of_lastName_1() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___lastName_1)); }
	inline String_t* get_lastName_1() const { return ___lastName_1; }
	inline String_t** get_address_of_lastName_1() { return &___lastName_1; }
	inline void set_lastName_1(String_t* value)
	{
		___lastName_1 = value;
		Il2CppCodeGenWriteBarrier(&___lastName_1, value);
	}

	inline static int32_t get_offset_of_phoneNumbers_2() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___phoneNumbers_2)); }
	inline List_1_t1398341365 * get_phoneNumbers_2() const { return ___phoneNumbers_2; }
	inline List_1_t1398341365 ** get_address_of_phoneNumbers_2() { return &___phoneNumbers_2; }
	inline void set_phoneNumbers_2(List_1_t1398341365 * value)
	{
		___phoneNumbers_2 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumbers_2, value);
	}

	inline static int32_t get_offset_of_phoneNumberHashes_3() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___phoneNumberHashes_3)); }
	inline List_1_t1398341365 * get_phoneNumberHashes_3() const { return ___phoneNumberHashes_3; }
	inline List_1_t1398341365 ** get_address_of_phoneNumberHashes_3() { return &___phoneNumberHashes_3; }
	inline void set_phoneNumberHashes_3(List_1_t1398341365 * value)
	{
		___phoneNumberHashes_3 = value;
		Il2CppCodeGenWriteBarrier(&___phoneNumberHashes_3, value);
	}

	inline static int32_t get_offset_of_rawChecksum_4() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___rawChecksum_4)); }
	inline String_t* get_rawChecksum_4() const { return ___rawChecksum_4; }
	inline String_t** get_address_of_rawChecksum_4() { return &___rawChecksum_4; }
	inline void set_rawChecksum_4(String_t* value)
	{
		___rawChecksum_4 = value;
		Il2CppCodeGenWriteBarrier(&___rawChecksum_4, value);
	}

	inline static int32_t get_offset_of_validatedChecksum_5() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___validatedChecksum_5)); }
	inline String_t* get_validatedChecksum_5() const { return ___validatedChecksum_5; }
	inline String_t** get_address_of_validatedChecksum_5() { return &___validatedChecksum_5; }
	inline void set_validatedChecksum_5(String_t* value)
	{
		___validatedChecksum_5 = value;
		Il2CppCodeGenWriteBarrier(&___validatedChecksum_5, value);
	}

	inline static int32_t get_offset_of_didFind_6() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___didFind_6)); }
	inline bool get_didFind_6() const { return ___didFind_6; }
	inline bool* get_address_of_didFind_6() { return &___didFind_6; }
	inline void set_didFind_6(bool value)
	{
		___didFind_6 = value;
	}

	inline static int32_t get_offset_of_hasChanged_7() { return static_cast<int32_t>(offsetof(ValidContact_t1479914934, ___hasChanged_7)); }
	inline bool get_hasChanged_7() const { return ___hasChanged_7; }
	inline bool* get_address_of_hasChanged_7() { return &___hasChanged_7; }
	inline void set_hasChanged_7(bool value)
	{
		___hasChanged_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
