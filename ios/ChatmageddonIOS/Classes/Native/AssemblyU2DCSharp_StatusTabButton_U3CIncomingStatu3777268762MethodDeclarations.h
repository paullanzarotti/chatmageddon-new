﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusTabButton/<IncomingStatusUpdate>c__Iterator0
struct U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StatusTabButton/<IncomingStatusUpdate>c__Iterator0::.ctor()
extern "C"  void U3CIncomingStatusUpdateU3Ec__Iterator0__ctor_m4261310097 (U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusTabButton/<IncomingStatusUpdate>c__Iterator0::MoveNext()
extern "C"  bool U3CIncomingStatusUpdateU3Ec__Iterator0_MoveNext_m2469094715 (U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StatusTabButton/<IncomingStatusUpdate>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIncomingStatusUpdateU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1187958899 (U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StatusTabButton/<IncomingStatusUpdate>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIncomingStatusUpdateU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3991497467 (U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusTabButton/<IncomingStatusUpdate>c__Iterator0::Dispose()
extern "C"  void U3CIncomingStatusUpdateU3Ec__Iterator0_Dispose_m1435612636 (U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusTabButton/<IncomingStatusUpdate>c__Iterator0::Reset()
extern "C"  void U3CIncomingStatusUpdateU3Ec__Iterator0_Reset_m758234642 (U3CIncomingStatusUpdateU3Ec__Iterator0_t3777268762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
