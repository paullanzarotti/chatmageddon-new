﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Object
struct Il2CppObject;
// PhoneNumbers.NumberFormat/Builder
struct Builder_t3378404562;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_NumberFormat441739224.h"

// System.Void PhoneNumbers.NumberFormat::.cctor()
extern "C"  void NumberFormat__cctor_m51373684 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.NumberFormat::.ctor()
extern "C"  void NumberFormat__ctor_m250926101 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat::get_DefaultInstance()
extern "C"  NumberFormat_t441739224 * NumberFormat_get_DefaultInstance_m711337727 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat::get_DefaultInstanceForType()
extern "C"  NumberFormat_t441739224 * NumberFormat_get_DefaultInstanceForType_m3966260740 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat::get_ThisMessage()
extern "C"  NumberFormat_t441739224 * NumberFormat_get_ThisMessage_m4262045226 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::get_HasPattern()
extern "C"  bool NumberFormat_get_HasPattern_m3709206256 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat::get_Pattern()
extern "C"  String_t* NumberFormat_get_Pattern_m3164380681 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::get_HasFormat()
extern "C"  bool NumberFormat_get_HasFormat_m1330054873 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat::get_Format()
extern "C"  String_t* NumberFormat_get_Format_m2718224688 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> PhoneNumbers.NumberFormat::get_LeadingDigitsPatternList()
extern "C"  Il2CppObject* NumberFormat_get_LeadingDigitsPatternList_m666093310 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.NumberFormat::get_LeadingDigitsPatternCount()
extern "C"  int32_t NumberFormat_get_LeadingDigitsPatternCount_m3358565549 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat::GetLeadingDigitsPattern(System.Int32)
extern "C"  String_t* NumberFormat_GetLeadingDigitsPattern_m4025699319 (NumberFormat_t441739224 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::get_HasNationalPrefixFormattingRule()
extern "C"  bool NumberFormat_get_HasNationalPrefixFormattingRule_m3807226117 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat::get_NationalPrefixFormattingRule()
extern "C"  String_t* NumberFormat_get_NationalPrefixFormattingRule_m1136435692 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::get_HasNationalPrefixOptionalWhenFormatting()
extern "C"  bool NumberFormat_get_HasNationalPrefixOptionalWhenFormatting_m1860528581 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::get_NationalPrefixOptionalWhenFormatting()
extern "C"  bool NumberFormat_get_NationalPrefixOptionalWhenFormatting_m2108403331 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::get_HasDomesticCarrierCodeFormattingRule()
extern "C"  bool NumberFormat_get_HasDomesticCarrierCodeFormattingRule_m4001927398 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.NumberFormat::get_DomesticCarrierCodeFormattingRule()
extern "C"  String_t* NumberFormat_get_DomesticCarrierCodeFormattingRule_m1799833103 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::get_IsInitialized()
extern "C"  bool NumberFormat_get_IsInitialized_m3273075262 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.NumberFormat::GetHashCode()
extern "C"  int32_t NumberFormat_GetHashCode_m148728054 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.NumberFormat::Equals(System.Object)
extern "C"  bool NumberFormat_Equals_m1475204518 (NumberFormat_t441739224 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat::CreateBuilder()
extern "C"  Builder_t3378404562 * NumberFormat_CreateBuilder_m1429585987 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat::ToBuilder()
extern "C"  Builder_t3378404562 * NumberFormat_ToBuilder_m3475026950 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat::CreateBuilderForType()
extern "C"  Builder_t3378404562 * NumberFormat_CreateBuilderForType_m3243382218 (NumberFormat_t441739224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat/Builder PhoneNumbers.NumberFormat::CreateBuilder(PhoneNumbers.NumberFormat)
extern "C"  Builder_t3378404562 * NumberFormat_CreateBuilder_m3399152293 (Il2CppObject * __this /* static, unused */, NumberFormat_t441739224 * ___prototype0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
