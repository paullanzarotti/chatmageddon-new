﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocationDistanceController
struct LocationDistanceController_t4191421708;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void LocationDistanceController::.ctor()
extern "C"  void LocationDistanceController__ctor_m3451212341 (LocationDistanceController_t4191421708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocationDistanceController::StartDistanceTracking()
extern "C"  void LocationDistanceController_StartDistanceTracking_m3806568493 (LocationDistanceController_t4191421708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LocationDistanceController::CheckDistance()
extern "C"  Il2CppObject * LocationDistanceController_CheckDistance_m2273318140 (LocationDistanceController_t4191421708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocationDistanceController::UpdateDistanceDisplay()
extern "C"  void LocationDistanceController_UpdateDistanceDisplay_m1889189133 (LocationDistanceController_t4191421708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocationDistanceController::DefendMissile(System.Boolean)
extern "C"  void LocationDistanceController_DefendMissile_m1577278882 (LocationDistanceController_t4191421708 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
