﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen1069771297.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitSceneManager
struct  InitSceneManager_t1690944945  : public BaseSceneManager_1_t1069771297
{
public:
	// System.Boolean InitSceneManager::clearData
	bool ___clearData_17;
	// UnityEngine.GameObject InitSceneManager::InitCenterUI
	GameObject_t1756533147 * ___InitCenterUI_18;
	// UnityEngine.GameObject InitSceneManager::ConnectionPopUpUI
	GameObject_t1756533147 * ___ConnectionPopUpUI_19;
	// UnityEngine.GameObject InitSceneManager::launchAnimation
	GameObject_t1756533147 * ___launchAnimation_20;
	// System.Boolean InitSceneManager::internetManagersLoaded
	bool ___internetManagersLoaded_21;

public:
	inline static int32_t get_offset_of_clearData_17() { return static_cast<int32_t>(offsetof(InitSceneManager_t1690944945, ___clearData_17)); }
	inline bool get_clearData_17() const { return ___clearData_17; }
	inline bool* get_address_of_clearData_17() { return &___clearData_17; }
	inline void set_clearData_17(bool value)
	{
		___clearData_17 = value;
	}

	inline static int32_t get_offset_of_InitCenterUI_18() { return static_cast<int32_t>(offsetof(InitSceneManager_t1690944945, ___InitCenterUI_18)); }
	inline GameObject_t1756533147 * get_InitCenterUI_18() const { return ___InitCenterUI_18; }
	inline GameObject_t1756533147 ** get_address_of_InitCenterUI_18() { return &___InitCenterUI_18; }
	inline void set_InitCenterUI_18(GameObject_t1756533147 * value)
	{
		___InitCenterUI_18 = value;
		Il2CppCodeGenWriteBarrier(&___InitCenterUI_18, value);
	}

	inline static int32_t get_offset_of_ConnectionPopUpUI_19() { return static_cast<int32_t>(offsetof(InitSceneManager_t1690944945, ___ConnectionPopUpUI_19)); }
	inline GameObject_t1756533147 * get_ConnectionPopUpUI_19() const { return ___ConnectionPopUpUI_19; }
	inline GameObject_t1756533147 ** get_address_of_ConnectionPopUpUI_19() { return &___ConnectionPopUpUI_19; }
	inline void set_ConnectionPopUpUI_19(GameObject_t1756533147 * value)
	{
		___ConnectionPopUpUI_19 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectionPopUpUI_19, value);
	}

	inline static int32_t get_offset_of_launchAnimation_20() { return static_cast<int32_t>(offsetof(InitSceneManager_t1690944945, ___launchAnimation_20)); }
	inline GameObject_t1756533147 * get_launchAnimation_20() const { return ___launchAnimation_20; }
	inline GameObject_t1756533147 ** get_address_of_launchAnimation_20() { return &___launchAnimation_20; }
	inline void set_launchAnimation_20(GameObject_t1756533147 * value)
	{
		___launchAnimation_20 = value;
		Il2CppCodeGenWriteBarrier(&___launchAnimation_20, value);
	}

	inline static int32_t get_offset_of_internetManagersLoaded_21() { return static_cast<int32_t>(offsetof(InitSceneManager_t1690944945, ___internetManagersLoaded_21)); }
	inline bool get_internetManagersLoaded_21() const { return ___internetManagersLoaded_21; }
	inline bool* get_address_of_internetManagersLoaded_21() { return &___internetManagersLoaded_21; }
	inline void set_internetManagersLoaded_21(bool value)
	{
		___internetManagersLoaded_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
