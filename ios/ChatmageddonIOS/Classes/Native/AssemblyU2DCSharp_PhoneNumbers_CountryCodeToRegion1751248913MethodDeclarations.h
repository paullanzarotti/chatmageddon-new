﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.CountryCodeToRegionCodeMap
struct CountryCodeToRegionCodeMap_t1751248913;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t406167000;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumbers.CountryCodeToRegionCodeMap::.ctor()
extern "C"  void CountryCodeToRegionCodeMap__ctor_m3831125754 (CountryCodeToRegionCodeMap_t1751248913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.String>> PhoneNumbers.CountryCodeToRegionCodeMap::GetCountryCodeToRegionCodeMap()
extern "C"  Dictionary_2_t406167000 * CountryCodeToRegionCodeMap_GetCountryCodeToRegionCodeMap_m4157392346 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
