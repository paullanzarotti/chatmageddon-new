﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_BigFace
struct CameraFilterPack_Distortion_BigFace_t2835595270;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_BigFace::.ctor()
extern "C"  void CameraFilterPack_Distortion_BigFace__ctor_m329928209 (CameraFilterPack_Distortion_BigFace_t2835595270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_BigFace::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_BigFace_get_material_m335578686 (CameraFilterPack_Distortion_BigFace_t2835595270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::Start()
extern "C"  void CameraFilterPack_Distortion_BigFace_Start_m4291699425 (CameraFilterPack_Distortion_BigFace_t2835595270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_BigFace_OnRenderImage_m3099956753 (CameraFilterPack_Distortion_BigFace_t2835595270 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::OnValidate()
extern "C"  void CameraFilterPack_Distortion_BigFace_OnValidate_m3936959098 (CameraFilterPack_Distortion_BigFace_t2835595270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::Update()
extern "C"  void CameraFilterPack_Distortion_BigFace_Update_m665912292 (CameraFilterPack_Distortion_BigFace_t2835595270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::OnDisable()
extern "C"  void CameraFilterPack_Distortion_BigFace_OnDisable_m1760364522 (CameraFilterPack_Distortion_BigFace_t2835595270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
