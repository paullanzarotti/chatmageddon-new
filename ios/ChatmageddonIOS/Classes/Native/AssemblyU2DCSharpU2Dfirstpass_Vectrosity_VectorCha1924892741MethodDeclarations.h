﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vectrosity.VectorChar
struct VectorChar_t1924892741;
// UnityEngine.Vector2[][]
struct Vector2U5BU5DU5BU5D_t4105933535;

#include "codegen/il2cpp-codegen.h"

// System.Void Vectrosity.VectorChar::.ctor()
extern "C"  void VectorChar__ctor_m3988087580 (VectorChar_t1924892741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[][] Vectrosity.VectorChar::get_data()
extern "C"  Vector2U5BU5DU5BU5D_t4105933535* VectorChar_get_data_m4268068486 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
