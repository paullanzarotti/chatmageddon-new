﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenAlpha
struct TweenAlpha_t2421518635;
// TweenPosition
struct TweenPosition_t1144714832;
// TweenScale
struct TweenScale_t2697902175;
// StealthModeCalculator
struct StealthModeCalculator_t3335204764;

#include "AssemblyU2DCSharp_LockoutUI1337294023.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StealthModeUI
struct  StealthModeUI_t2601479126  : public LockoutUI_t1337294023
{
public:
	// TweenAlpha StealthModeUI::backgroundTween
	TweenAlpha_t2421518635 * ___backgroundTween_3;
	// TweenPosition StealthModeUI::exitButtonTween
	TweenPosition_t1144714832 * ___exitButtonTween_4;
	// TweenScale StealthModeUI::contentTween
	TweenScale_t2697902175 * ___contentTween_5;
	// StealthModeCalculator StealthModeUI::calculator
	StealthModeCalculator_t3335204764 * ___calculator_6;

public:
	inline static int32_t get_offset_of_backgroundTween_3() { return static_cast<int32_t>(offsetof(StealthModeUI_t2601479126, ___backgroundTween_3)); }
	inline TweenAlpha_t2421518635 * get_backgroundTween_3() const { return ___backgroundTween_3; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundTween_3() { return &___backgroundTween_3; }
	inline void set_backgroundTween_3(TweenAlpha_t2421518635 * value)
	{
		___backgroundTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundTween_3, value);
	}

	inline static int32_t get_offset_of_exitButtonTween_4() { return static_cast<int32_t>(offsetof(StealthModeUI_t2601479126, ___exitButtonTween_4)); }
	inline TweenPosition_t1144714832 * get_exitButtonTween_4() const { return ___exitButtonTween_4; }
	inline TweenPosition_t1144714832 ** get_address_of_exitButtonTween_4() { return &___exitButtonTween_4; }
	inline void set_exitButtonTween_4(TweenPosition_t1144714832 * value)
	{
		___exitButtonTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___exitButtonTween_4, value);
	}

	inline static int32_t get_offset_of_contentTween_5() { return static_cast<int32_t>(offsetof(StealthModeUI_t2601479126, ___contentTween_5)); }
	inline TweenScale_t2697902175 * get_contentTween_5() const { return ___contentTween_5; }
	inline TweenScale_t2697902175 ** get_address_of_contentTween_5() { return &___contentTween_5; }
	inline void set_contentTween_5(TweenScale_t2697902175 * value)
	{
		___contentTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___contentTween_5, value);
	}

	inline static int32_t get_offset_of_calculator_6() { return static_cast<int32_t>(offsetof(StealthModeUI_t2601479126, ___calculator_6)); }
	inline StealthModeCalculator_t3335204764 * get_calculator_6() const { return ___calculator_6; }
	inline StealthModeCalculator_t3335204764 ** get_address_of_calculator_6() { return &___calculator_6; }
	inline void set_calculator_6(StealthModeCalculator_t3335204764 * value)
	{
		___calculator_6 = value;
		Il2CppCodeGenWriteBarrier(&___calculator_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
