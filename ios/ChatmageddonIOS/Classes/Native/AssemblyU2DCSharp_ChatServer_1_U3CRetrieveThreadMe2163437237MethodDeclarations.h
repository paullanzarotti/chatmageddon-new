﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatServer`1/<RetrieveThreadMessages>c__AnonStorey3<System.Object>
struct U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t2163437237;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatServer`1/<RetrieveThreadMessages>c__AnonStorey3<System.Object>::.ctor()
extern "C"  void U3CRetrieveThreadMessagesU3Ec__AnonStorey3__ctor_m3860701528_gshared (U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t2163437237 * __this, const MethodInfo* method);
#define U3CRetrieveThreadMessagesU3Ec__AnonStorey3__ctor_m3860701528(__this, method) ((  void (*) (U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t2163437237 *, const MethodInfo*))U3CRetrieveThreadMessagesU3Ec__AnonStorey3__ctor_m3860701528_gshared)(__this, method)
// System.Void ChatServer`1/<RetrieveThreadMessages>c__AnonStorey3<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveThreadMessagesU3Ec__AnonStorey3_U3CU3Em__0_m1984697981_gshared (U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t2163437237 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRetrieveThreadMessagesU3Ec__AnonStorey3_U3CU3Em__0_m1984697981(__this, ___request0, ___response1, method) ((  void (*) (U3CRetrieveThreadMessagesU3Ec__AnonStorey3_t2163437237 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRetrieveThreadMessagesU3Ec__AnonStorey3_U3CU3Em__0_m1984697981_gshared)(__this, ___request0, ___response1, method)
