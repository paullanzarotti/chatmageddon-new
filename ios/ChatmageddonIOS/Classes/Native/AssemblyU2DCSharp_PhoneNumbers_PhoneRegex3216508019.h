﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;

#include "System_System_Text_RegularExpressions_Regex1803876613.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneRegex
struct  PhoneRegex_t3216508019  : public Regex_t1803876613
{
public:
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneRegex::allRegex_
	Regex_t1803876613 * ___allRegex__16;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneRegex::beginRegex_
	Regex_t1803876613 * ___beginRegex__17;

public:
	inline static int32_t get_offset_of_allRegex__16() { return static_cast<int32_t>(offsetof(PhoneRegex_t3216508019, ___allRegex__16)); }
	inline Regex_t1803876613 * get_allRegex__16() const { return ___allRegex__16; }
	inline Regex_t1803876613 ** get_address_of_allRegex__16() { return &___allRegex__16; }
	inline void set_allRegex__16(Regex_t1803876613 * value)
	{
		___allRegex__16 = value;
		Il2CppCodeGenWriteBarrier(&___allRegex__16, value);
	}

	inline static int32_t get_offset_of_beginRegex__17() { return static_cast<int32_t>(offsetof(PhoneRegex_t3216508019, ___beginRegex__17)); }
	inline Regex_t1803876613 * get_beginRegex__17() const { return ___beginRegex__17; }
	inline Regex_t1803876613 ** get_address_of_beginRegex__17() { return &___beginRegex__17; }
	inline void set_beginRegex__17(Regex_t1803876613 * value)
	{
		___beginRegex__17 = value;
		Il2CppCodeGenWriteBarrier(&___beginRegex__17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
