﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ProtectedConfigurationProvider
struct ProtectedConfigurationProvider_t3971982415;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Configuration.ProtectedConfigurationProvider::.ctor()
extern "C"  void ProtectedConfigurationProvider__ctor_m559302856 (ProtectedConfigurationProvider_t3971982415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
