﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CloseMapGroupButton
struct CloseMapGroupButton_t794394401;

#include "codegen/il2cpp-codegen.h"

// System.Void CloseMapGroupButton::.ctor()
extern "C"  void CloseMapGroupButton__ctor_m1548898484 (CloseMapGroupButton_t794394401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CloseMapGroupButton::OnButtonClick()
extern "C"  void CloseMapGroupButton_OnButtonClick_m3548479049 (CloseMapGroupButton_t794394401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
