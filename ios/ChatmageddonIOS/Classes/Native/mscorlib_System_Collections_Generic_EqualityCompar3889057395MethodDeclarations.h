﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SettingsNavScreen>
struct DefaultComparer_t3889057395;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SettingsNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m4269473380_gshared (DefaultComparer_t3889057395 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4269473380(__this, method) ((  void (*) (DefaultComparer_t3889057395 *, const MethodInfo*))DefaultComparer__ctor_m4269473380_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SettingsNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2192893787_gshared (DefaultComparer_t3889057395 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2192893787(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3889057395 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2192893787_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SettingsNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1042644163_gshared (DefaultComparer_t3889057395 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1042644163(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3889057395 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1042644163_gshared)(__this, ___x0, ___y1, method)
