﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Distortion_Lens
struct  CameraFilterPack_Distortion_Lens_t4173169989  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Distortion_Lens::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Lens::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Lens::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Lens::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Lens::CenterX
	float ___CenterX_6;
	// System.Single CameraFilterPack_Distortion_Lens::CenterY
	float ___CenterY_7;
	// System.Single CameraFilterPack_Distortion_Lens::Distortion
	float ___Distortion_8;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_CenterX_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989, ___CenterX_6)); }
	inline float get_CenterX_6() const { return ___CenterX_6; }
	inline float* get_address_of_CenterX_6() { return &___CenterX_6; }
	inline void set_CenterX_6(float value)
	{
		___CenterX_6 = value;
	}

	inline static int32_t get_offset_of_CenterY_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989, ___CenterY_7)); }
	inline float get_CenterY_7() const { return ___CenterY_7; }
	inline float* get_address_of_CenterY_7() { return &___CenterY_7; }
	inline void set_CenterY_7(float value)
	{
		___CenterY_7 = value;
	}

	inline static int32_t get_offset_of_Distortion_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989, ___Distortion_8)); }
	inline float get_Distortion_8() const { return ___Distortion_8; }
	inline float* get_address_of_Distortion_8() { return &___Distortion_8; }
	inline void set_Distortion_8(float value)
	{
		___Distortion_8 = value;
	}
};

struct CameraFilterPack_Distortion_Lens_t4173169989_StaticFields
{
public:
	// System.Single CameraFilterPack_Distortion_Lens::ChangeCenterX
	float ___ChangeCenterX_9;
	// System.Single CameraFilterPack_Distortion_Lens::ChangeCenterY
	float ___ChangeCenterY_10;
	// System.Single CameraFilterPack_Distortion_Lens::ChangeDistortion
	float ___ChangeDistortion_11;

public:
	inline static int32_t get_offset_of_ChangeCenterX_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989_StaticFields, ___ChangeCenterX_9)); }
	inline float get_ChangeCenterX_9() const { return ___ChangeCenterX_9; }
	inline float* get_address_of_ChangeCenterX_9() { return &___ChangeCenterX_9; }
	inline void set_ChangeCenterX_9(float value)
	{
		___ChangeCenterX_9 = value;
	}

	inline static int32_t get_offset_of_ChangeCenterY_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989_StaticFields, ___ChangeCenterY_10)); }
	inline float get_ChangeCenterY_10() const { return ___ChangeCenterY_10; }
	inline float* get_address_of_ChangeCenterY_10() { return &___ChangeCenterY_10; }
	inline void set_ChangeCenterY_10(float value)
	{
		___ChangeCenterY_10 = value;
	}

	inline static int32_t get_offset_of_ChangeDistortion_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Lens_t4173169989_StaticFields, ___ChangeDistortion_11)); }
	inline float get_ChangeDistortion_11() const { return ___ChangeDistortion_11; }
	inline float* get_address_of_ChangeDistortion_11() { return &___ChangeDistortion_11; }
	inline void set_ChangeDistortion_11(float value)
	{
		___ChangeDistortion_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
