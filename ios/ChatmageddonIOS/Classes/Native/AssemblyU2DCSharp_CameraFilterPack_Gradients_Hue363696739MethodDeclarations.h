﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_Hue
struct CameraFilterPack_Gradients_Hue_t363696739;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_Hue::.ctor()
extern "C"  void CameraFilterPack_Gradients_Hue__ctor_m2012181842 (CameraFilterPack_Gradients_Hue_t363696739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Hue::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_Hue_get_material_m297204871 (CameraFilterPack_Gradients_Hue_t363696739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::Start()
extern "C"  void CameraFilterPack_Gradients_Hue_Start_m2369168270 (CameraFilterPack_Gradients_Hue_t363696739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_Hue_OnRenderImage_m544631582 (CameraFilterPack_Gradients_Hue_t363696739 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::Update()
extern "C"  void CameraFilterPack_Gradients_Hue_Update_m2960148139 (CameraFilterPack_Gradients_Hue_t363696739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::OnDisable()
extern "C"  void CameraFilterPack_Gradients_Hue_OnDisable_m3074782963 (CameraFilterPack_Gradients_Hue_t363696739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
