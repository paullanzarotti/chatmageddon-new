﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuyBucksPUButton
struct BuyBucksPUButton_t1827949611;

#include "codegen/il2cpp-codegen.h"

// System.Void BuyBucksPUButton::.ctor()
extern "C"  void BuyBucksPUButton__ctor_m3679251862 (BuyBucksPUButton_t1827949611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuyBucksPUButton::OnButtonClick()
extern "C"  void BuyBucksPUButton_OnButtonClick_m1481450899 (BuyBucksPUButton_t1827949611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
