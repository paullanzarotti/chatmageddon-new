﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,ProfileNavScreen>
struct NavigationController_2_t3309526906;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProfileNavScreen1126657854.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,ProfileNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m2166287613_gshared (NavigationController_2_t3309526906 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m2166287613(__this, method) ((  void (*) (NavigationController_2_t3309526906 *, const MethodInfo*))NavigationController_2__ctor_m2166287613_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m2761873091_gshared (NavigationController_2_t3309526906 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m2761873091(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t3309526906 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m2761873091_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m538863782_gshared (NavigationController_2_t3309526906 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m538863782(__this, method) ((  void (*) (NavigationController_2_t3309526906 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m538863782_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m2294274539_gshared (NavigationController_2_t3309526906 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m2294274539(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t3309526906 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m2294274539_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,ProfileNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m4024368191_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m4024368191(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t3309526906 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m4024368191_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,ProfileNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m1522047260_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m1522047260(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t3309526906 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m1522047260_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m286403891_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m286403891(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t3309526906 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m286403891_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2954494359_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m2954494359(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t3309526906 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2954494359_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,ProfileNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2444270883_gshared (NavigationController_2_t3309526906 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m2444270883(__this, ___screen0, method) ((  void (*) (NavigationController_2_t3309526906 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2444270883_gshared)(__this, ___screen0, method)
