﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OSMRequestExample
struct OSMRequestExample_t188828912;
// System.String
struct String_t;
// OnlineMapsOSMTag
struct OnlineMapsOSMTag_t3629071465;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMTag3629071465.h"

// System.Void OSMRequestExample::.ctor()
extern "C"  void OSMRequestExample__ctor_m2847067311 (OSMRequestExample_t188828912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSMRequestExample::Start()
extern "C"  void OSMRequestExample_Start_m3639321355 (OSMRequestExample_t188828912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OSMRequestExample::OnComplete(System.String)
extern "C"  void OSMRequestExample_OnComplete_m3455627193 (OSMRequestExample_t188828912 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OSMRequestExample::<OnComplete>m__0(OnlineMapsOSMTag)
extern "C"  bool OSMRequestExample_U3COnCompleteU3Em__0_m1002766519 (Il2CppObject * __this /* static, unused */, OnlineMapsOSMTag_t3629071465 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
