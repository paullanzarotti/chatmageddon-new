﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RotateButton
struct RotateButton_t2565449149;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateButton/<Rotate>c__Iterator0
struct  U3CRotateU3Ec__Iterator0_t2725296188  : public Il2CppObject
{
public:
	// UnityEngine.Quaternion RotateButton/<Rotate>c__Iterator0::<rotation>__0
	Quaternion_t4030073918  ___U3CrotationU3E__0_0;
	// UnityEngine.Quaternion RotateButton/<Rotate>c__Iterator0::<rotation>__1
	Quaternion_t4030073918  ___U3CrotationU3E__1_1;
	// RotateButton RotateButton/<Rotate>c__Iterator0::$this
	RotateButton_t2565449149 * ___U24this_2;
	// System.Object RotateButton/<Rotate>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean RotateButton/<Rotate>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 RotateButton/<Rotate>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrotationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRotateU3Ec__Iterator0_t2725296188, ___U3CrotationU3E__0_0)); }
	inline Quaternion_t4030073918  get_U3CrotationU3E__0_0() const { return ___U3CrotationU3E__0_0; }
	inline Quaternion_t4030073918 * get_address_of_U3CrotationU3E__0_0() { return &___U3CrotationU3E__0_0; }
	inline void set_U3CrotationU3E__0_0(Quaternion_t4030073918  value)
	{
		___U3CrotationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CrotationU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRotateU3Ec__Iterator0_t2725296188, ___U3CrotationU3E__1_1)); }
	inline Quaternion_t4030073918  get_U3CrotationU3E__1_1() const { return ___U3CrotationU3E__1_1; }
	inline Quaternion_t4030073918 * get_address_of_U3CrotationU3E__1_1() { return &___U3CrotationU3E__1_1; }
	inline void set_U3CrotationU3E__1_1(Quaternion_t4030073918  value)
	{
		___U3CrotationU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CRotateU3Ec__Iterator0_t2725296188, ___U24this_2)); }
	inline RotateButton_t2565449149 * get_U24this_2() const { return ___U24this_2; }
	inline RotateButton_t2565449149 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(RotateButton_t2565449149 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CRotateU3Ec__Iterator0_t2725296188, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CRotateU3Ec__Iterator0_t2725296188, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CRotateU3Ec__Iterator0_t2725296188, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
