﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GroupCircleAnimation
struct GroupCircleAnimation_t1271941897;
// GroupFriendUI
struct GroupFriendUI_t1634706847;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GroupFriendUI1634706847.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void GroupCircleAnimation::.ctor()
extern "C"  void GroupCircleAnimation__ctor_m3574406818 (GroupCircleAnimation_t1271941897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupCircleAnimation::AddFriendToGroup(GroupFriendUI)
extern "C"  void GroupCircleAnimation_AddFriendToGroup_m2442723798 (GroupCircleAnimation_t1271941897 * __this, GroupFriendUI_t1634706847 * ___friendUI0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupCircleAnimation::PlayAnimation(System.Boolean)
extern "C"  void GroupCircleAnimation_PlayAnimation_m3822404337 (GroupCircleAnimation_t1271941897 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 GroupCircleAnimation::GetPositionForState(System.Int32)
extern "C"  Vector3_t2243707580  GroupCircleAnimation_GetPositionForState_m2750730642 (GroupCircleAnimation_t1271941897 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GroupCircleAnimation::SetUICollisionActive(System.Boolean)
extern "C"  void GroupCircleAnimation_SetUICollisionActive_m2594750343 (GroupCircleAnimation_t1271941897 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
