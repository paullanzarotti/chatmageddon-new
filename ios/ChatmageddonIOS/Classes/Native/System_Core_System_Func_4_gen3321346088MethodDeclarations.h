﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_4_gen4066896662MethodDeclarations.h"

// System.Void System.Func`4<BestHTTP.HTTPRequest,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_4__ctor_m3257512246(__this, ___object0, ___method1, method) ((  void (*) (Func_4_t3321346088 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_4__ctor_m2172527533_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`4<BestHTTP.HTTPRequest,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Boolean>::Invoke(T1,T2,T3)
#define Func_4_Invoke_m4080880075(__this, ___arg10, ___arg21, ___arg32, method) ((  bool (*) (Func_4_t3321346088 *, HTTPRequest_t138485887 *, X509Certificate_t283079845 *, X509Chain_t777637347 *, const MethodInfo*))Func_4_Invoke_m4093412051_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Func`4<BestHTTP.HTTPRequest,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Boolean>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Func_4_BeginInvoke_m3398761919(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Func_4_t3321346088 *, HTTPRequest_t138485887 *, X509Certificate_t283079845 *, X509Chain_t777637347 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_4_BeginInvoke_m3963604364_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TResult System.Func`4<BestHTTP.HTTPRequest,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_4_EndInvoke_m1702206210(__this, ___result0, method) ((  bool (*) (Func_4_t3321346088 *, Il2CppObject *, const MethodInfo*))Func_4_EndInvoke_m568092533_gshared)(__this, ___result0, method)
