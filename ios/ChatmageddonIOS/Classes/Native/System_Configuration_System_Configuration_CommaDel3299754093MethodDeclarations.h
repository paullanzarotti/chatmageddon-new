﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.CommaDelimitedStringCollection
struct CommaDelimitedStringCollection_t3299754093;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.CommaDelimitedStringCollection::.ctor()
extern "C"  void CommaDelimitedStringCollection__ctor_m39555474 (CommaDelimitedStringCollection_t3299754093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.CommaDelimitedStringCollection::get_IsModified()
extern "C"  bool CommaDelimitedStringCollection_get_IsModified_m720029694 (CommaDelimitedStringCollection_t3299754093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.CommaDelimitedStringCollection::get_IsReadOnly()
extern "C"  bool CommaDelimitedStringCollection_get_IsReadOnly_m786864375 (CommaDelimitedStringCollection_t3299754093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.CommaDelimitedStringCollection::get_Item(System.Int32)
extern "C"  String_t* CommaDelimitedStringCollection_get_Item_m1695696440 (CommaDelimitedStringCollection_t3299754093 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CommaDelimitedStringCollection::set_Item(System.Int32,System.String)
extern "C"  void CommaDelimitedStringCollection_set_Item_m3790950973 (CommaDelimitedStringCollection_t3299754093 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CommaDelimitedStringCollection::Add(System.String)
extern "C"  void CommaDelimitedStringCollection_Add_m1550289197 (CommaDelimitedStringCollection_t3299754093 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CommaDelimitedStringCollection::AddRange(System.String[])
extern "C"  void CommaDelimitedStringCollection_AddRange_m2085267198 (CommaDelimitedStringCollection_t3299754093 * __this, StringU5BU5D_t1642385972* ___range0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CommaDelimitedStringCollection::Clear()
extern "C"  void CommaDelimitedStringCollection_Clear_m4254431595 (CommaDelimitedStringCollection_t3299754093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.CommaDelimitedStringCollection System.Configuration.CommaDelimitedStringCollection::Clone()
extern "C"  CommaDelimitedStringCollection_t3299754093 * CommaDelimitedStringCollection_Clone_m2486393408 (CommaDelimitedStringCollection_t3299754093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CommaDelimitedStringCollection::Insert(System.Int32,System.String)
extern "C"  void CommaDelimitedStringCollection_Insert_m2180922208 (CommaDelimitedStringCollection_t3299754093 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CommaDelimitedStringCollection::Remove(System.String)
extern "C"  void CommaDelimitedStringCollection_Remove_m2944648926 (CommaDelimitedStringCollection_t3299754093 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.CommaDelimitedStringCollection::SetReadOnly()
extern "C"  void CommaDelimitedStringCollection_SetReadOnly_m1515132376 (CommaDelimitedStringCollection_t3299754093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.CommaDelimitedStringCollection::ToString()
extern "C"  String_t* CommaDelimitedStringCollection_ToString_m1584623373 (CommaDelimitedStringCollection_t3299754093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
