﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VerificationUIScaler
struct VerificationUIScaler_t217609153;

#include "codegen/il2cpp-codegen.h"

// System.Void VerificationUIScaler::.ctor()
extern "C"  void VerificationUIScaler__ctor_m283385268 (VerificationUIScaler_t217609153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerificationUIScaler::GlobalUIScale()
extern "C"  void VerificationUIScaler_GlobalUIScale_m1431232971 (VerificationUIScaler_t217609153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
