﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatThreadsListItem
struct ChatThreadsListItem_t303868848;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// TweenPosition
struct TweenPosition_t1144714832;

#include "AssemblyU2DCSharp_NGUISwipe4135300327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatThreadSwipe
struct  ChatThreadSwipe_t397952508  : public NGUISwipe_t4135300327
{
public:
	// ChatThreadsListItem ChatThreadSwipe::listItem
	ChatThreadsListItem_t303868848 * ___listItem_10;
	// UnityEngine.GameObject ChatThreadSwipe::deleteContent
	GameObject_t1756533147 * ___deleteContent_11;
	// TweenPosition ChatThreadSwipe::posTween
	TweenPosition_t1144714832 * ___posTween_12;
	// System.Boolean ChatThreadSwipe::uiclosing
	bool ___uiclosing_13;
	// System.Boolean ChatThreadSwipe::deleteOpen
	bool ___deleteOpen_14;

public:
	inline static int32_t get_offset_of_listItem_10() { return static_cast<int32_t>(offsetof(ChatThreadSwipe_t397952508, ___listItem_10)); }
	inline ChatThreadsListItem_t303868848 * get_listItem_10() const { return ___listItem_10; }
	inline ChatThreadsListItem_t303868848 ** get_address_of_listItem_10() { return &___listItem_10; }
	inline void set_listItem_10(ChatThreadsListItem_t303868848 * value)
	{
		___listItem_10 = value;
		Il2CppCodeGenWriteBarrier(&___listItem_10, value);
	}

	inline static int32_t get_offset_of_deleteContent_11() { return static_cast<int32_t>(offsetof(ChatThreadSwipe_t397952508, ___deleteContent_11)); }
	inline GameObject_t1756533147 * get_deleteContent_11() const { return ___deleteContent_11; }
	inline GameObject_t1756533147 ** get_address_of_deleteContent_11() { return &___deleteContent_11; }
	inline void set_deleteContent_11(GameObject_t1756533147 * value)
	{
		___deleteContent_11 = value;
		Il2CppCodeGenWriteBarrier(&___deleteContent_11, value);
	}

	inline static int32_t get_offset_of_posTween_12() { return static_cast<int32_t>(offsetof(ChatThreadSwipe_t397952508, ___posTween_12)); }
	inline TweenPosition_t1144714832 * get_posTween_12() const { return ___posTween_12; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_12() { return &___posTween_12; }
	inline void set_posTween_12(TweenPosition_t1144714832 * value)
	{
		___posTween_12 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_12, value);
	}

	inline static int32_t get_offset_of_uiclosing_13() { return static_cast<int32_t>(offsetof(ChatThreadSwipe_t397952508, ___uiclosing_13)); }
	inline bool get_uiclosing_13() const { return ___uiclosing_13; }
	inline bool* get_address_of_uiclosing_13() { return &___uiclosing_13; }
	inline void set_uiclosing_13(bool value)
	{
		___uiclosing_13 = value;
	}

	inline static int32_t get_offset_of_deleteOpen_14() { return static_cast<int32_t>(offsetof(ChatThreadSwipe_t397952508, ___deleteOpen_14)); }
	inline bool get_deleteOpen_14() const { return ___deleteOpen_14; }
	inline bool* get_address_of_deleteOpen_14() { return &___deleteOpen_14; }
	inline void set_deleteOpen_14(bool value)
	{
		___deleteOpen_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
