﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Pixel_Pixelisation
struct CameraFilterPack_Pixel_Pixelisation_t2392528693;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Pixel_Pixelisation::.ctor()
extern "C"  void CameraFilterPack_Pixel_Pixelisation__ctor_m4146572128 (CameraFilterPack_Pixel_Pixelisation_t2392528693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixel_Pixelisation::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Pixel_Pixelisation_get_material_m1353025273 (CameraFilterPack_Pixel_Pixelisation_t2392528693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::Start()
extern "C"  void CameraFilterPack_Pixel_Pixelisation_Start_m1715260688 (CameraFilterPack_Pixel_Pixelisation_t2392528693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Pixel_Pixelisation_OnRenderImage_m3823296640 (CameraFilterPack_Pixel_Pixelisation_t2392528693 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::OnValidate()
extern "C"  void CameraFilterPack_Pixel_Pixelisation_OnValidate_m1543777931 (CameraFilterPack_Pixel_Pixelisation_t2392528693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::Update()
extern "C"  void CameraFilterPack_Pixel_Pixelisation_Update_m3652057877 (CameraFilterPack_Pixel_Pixelisation_t2392528693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::OnDisable()
extern "C"  void CameraFilterPack_Pixel_Pixelisation_OnDisable_m2026861401 (CameraFilterPack_Pixel_Pixelisation_t2392528693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
