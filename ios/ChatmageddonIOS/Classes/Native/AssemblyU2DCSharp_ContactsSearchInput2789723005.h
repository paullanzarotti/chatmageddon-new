﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIInput
struct UIInput_t860674234;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContactsSearchInput
struct  ContactsSearchInput_t2789723005  : public MonoBehaviour_t1158329972
{
public:
	// UIInput ContactsSearchInput::searchInput
	UIInput_t860674234 * ___searchInput_2;

public:
	inline static int32_t get_offset_of_searchInput_2() { return static_cast<int32_t>(offsetof(ContactsSearchInput_t2789723005, ___searchInput_2)); }
	inline UIInput_t860674234 * get_searchInput_2() const { return ___searchInput_2; }
	inline UIInput_t860674234 ** get_address_of_searchInput_2() { return &___searchInput_2; }
	inline void set_searchInput_2(UIInput_t860674234 * value)
	{
		___searchInput_2 = value;
		Il2CppCodeGenWriteBarrier(&___searchInput_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
