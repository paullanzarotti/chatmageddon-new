﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineInfoController
struct MineInfoController_t2990208339;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_CircularProgressBar2980095063.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineCountdownProgressBar
struct  MineCountdownProgressBar_t1258896602  : public CircularProgressBar_t2980095063
{
public:
	// MineInfoController MineCountdownProgressBar::infoController
	MineInfoController_t2990208339 * ___infoController_13;
	// UILabel MineCountdownProgressBar::countdownLabel
	UILabel_t1795115428 * ___countdownLabel_14;
	// System.Single MineCountdownProgressBar::countdownTimeAmount
	float ___countdownTimeAmount_15;
	// System.Int32 MineCountdownProgressBar::numberOn
	int32_t ___numberOn_16;

public:
	inline static int32_t get_offset_of_infoController_13() { return static_cast<int32_t>(offsetof(MineCountdownProgressBar_t1258896602, ___infoController_13)); }
	inline MineInfoController_t2990208339 * get_infoController_13() const { return ___infoController_13; }
	inline MineInfoController_t2990208339 ** get_address_of_infoController_13() { return &___infoController_13; }
	inline void set_infoController_13(MineInfoController_t2990208339 * value)
	{
		___infoController_13 = value;
		Il2CppCodeGenWriteBarrier(&___infoController_13, value);
	}

	inline static int32_t get_offset_of_countdownLabel_14() { return static_cast<int32_t>(offsetof(MineCountdownProgressBar_t1258896602, ___countdownLabel_14)); }
	inline UILabel_t1795115428 * get_countdownLabel_14() const { return ___countdownLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_countdownLabel_14() { return &___countdownLabel_14; }
	inline void set_countdownLabel_14(UILabel_t1795115428 * value)
	{
		___countdownLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___countdownLabel_14, value);
	}

	inline static int32_t get_offset_of_countdownTimeAmount_15() { return static_cast<int32_t>(offsetof(MineCountdownProgressBar_t1258896602, ___countdownTimeAmount_15)); }
	inline float get_countdownTimeAmount_15() const { return ___countdownTimeAmount_15; }
	inline float* get_address_of_countdownTimeAmount_15() { return &___countdownTimeAmount_15; }
	inline void set_countdownTimeAmount_15(float value)
	{
		___countdownTimeAmount_15 = value;
	}

	inline static int32_t get_offset_of_numberOn_16() { return static_cast<int32_t>(offsetof(MineCountdownProgressBar_t1258896602, ___numberOn_16)); }
	inline int32_t get_numberOn_16() const { return ___numberOn_16; }
	inline int32_t* get_address_of_numberOn_16() { return &___numberOn_16; }
	inline void set_numberOn_16(int32_t value)
	{
		___numberOn_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
