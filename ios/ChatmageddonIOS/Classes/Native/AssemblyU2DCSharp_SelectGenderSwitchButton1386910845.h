﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GenderSwitch
struct GenderSwitch_t696294539;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectGenderSwitchButton
struct  SelectGenderSwitchButton_t1386910845  : public SFXButton_t792651341
{
public:
	// GenderSwitch SelectGenderSwitchButton::enumSwitch
	GenderSwitch_t696294539 * ___enumSwitch_5;
	// Gender SelectGenderSwitchButton::switchSelection
	int32_t ___switchSelection_6;

public:
	inline static int32_t get_offset_of_enumSwitch_5() { return static_cast<int32_t>(offsetof(SelectGenderSwitchButton_t1386910845, ___enumSwitch_5)); }
	inline GenderSwitch_t696294539 * get_enumSwitch_5() const { return ___enumSwitch_5; }
	inline GenderSwitch_t696294539 ** get_address_of_enumSwitch_5() { return &___enumSwitch_5; }
	inline void set_enumSwitch_5(GenderSwitch_t696294539 * value)
	{
		___enumSwitch_5 = value;
		Il2CppCodeGenWriteBarrier(&___enumSwitch_5, value);
	}

	inline static int32_t get_offset_of_switchSelection_6() { return static_cast<int32_t>(offsetof(SelectGenderSwitchButton_t1386910845, ___switchSelection_6)); }
	inline int32_t get_switchSelection_6() const { return ___switchSelection_6; }
	inline int32_t* get_address_of_switchSelection_6() { return &___switchSelection_6; }
	inline void set_switchSelection_6(int32_t value)
	{
		___switchSelection_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
