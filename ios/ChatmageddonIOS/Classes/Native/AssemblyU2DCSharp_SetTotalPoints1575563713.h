﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetTotalPoints
struct  SetTotalPoints_t1575563713  : public SFXButton_t792651341
{
public:
	// System.String SetTotalPoints::pointsToSet
	String_t* ___pointsToSet_5;

public:
	inline static int32_t get_offset_of_pointsToSet_5() { return static_cast<int32_t>(offsetof(SetTotalPoints_t1575563713, ___pointsToSet_5)); }
	inline String_t* get_pointsToSet_5() const { return ___pointsToSet_5; }
	inline String_t** get_address_of_pointsToSet_5() { return &___pointsToSet_5; }
	inline void set_pointsToSet_5(String_t* value)
	{
		___pointsToSet_5 = value;
		Il2CppCodeGenWriteBarrier(&___pointsToSet_5, value);
	}
};

struct SetTotalPoints_t1575563713_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> SetTotalPoints::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(SetTotalPoints_t1575563713_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
