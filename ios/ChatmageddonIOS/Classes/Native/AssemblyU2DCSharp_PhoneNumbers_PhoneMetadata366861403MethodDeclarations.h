﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat>
struct IList_1_t982679825;
// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;
// System.Object
struct Il2CppObject;
// PhoneNumbers.PhoneMetadata/Builder
struct Builder_t1569388123;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneMetadata366861403.h"

// System.Void PhoneNumbers.PhoneMetadata::.cctor()
extern "C"  void PhoneMetadata__cctor_m2329834215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneMetadata::.ctor()
extern "C"  void PhoneMetadata__ctor_m1048627508 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata::get_DefaultInstance()
extern "C"  PhoneMetadata_t366861403 * PhoneMetadata_get_DefaultInstance_m1428574395 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata::get_DefaultInstanceForType()
extern "C"  PhoneMetadata_t366861403 * PhoneMetadata_get_DefaultInstanceForType_m300058038 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata::get_ThisMessage()
extern "C"  PhoneMetadata_t366861403 * PhoneMetadata_get_ThisMessage_m3158175768 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasGeneralDesc()
extern "C"  bool PhoneMetadata_get_HasGeneralDesc_m2973393348 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_GeneralDesc()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_GeneralDesc_m3611476369 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasFixedLine()
extern "C"  bool PhoneMetadata_get_HasFixedLine_m3519267659 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_FixedLine()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_FixedLine_m3082830978 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasMobile()
extern "C"  bool PhoneMetadata_get_HasMobile_m1027631099 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_Mobile()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_Mobile_m2838290 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasTollFree()
extern "C"  bool PhoneMetadata_get_HasTollFree_m4127032270 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_TollFree()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_TollFree_m801808241 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasPremiumRate()
extern "C"  bool PhoneMetadata_get_HasPremiumRate_m3702398162 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_PremiumRate()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_PremiumRate_m2343102207 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasSharedCost()
extern "C"  bool PhoneMetadata_get_HasSharedCost_m1328560397 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_SharedCost()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_SharedCost_m1046039692 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasPersonalNumber()
extern "C"  bool PhoneMetadata_get_HasPersonalNumber_m1552275916 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_PersonalNumber()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_PersonalNumber_m572096513 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasVoip()
extern "C"  bool PhoneMetadata_get_HasVoip_m912350707 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_Voip()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_Voip_m141919690 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasPager()
extern "C"  bool PhoneMetadata_get_HasPager_m3275989322 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_Pager()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_Pager_m4148598631 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasUan()
extern "C"  bool PhoneMetadata_get_HasUan_m2581883697 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_Uan()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_Uan_m303413826 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasEmergency()
extern "C"  bool PhoneMetadata_get_HasEmergency_m479832832 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_Emergency()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_Emergency_m2037138163 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasVoicemail()
extern "C"  bool PhoneMetadata_get_HasVoicemail_m3029684264 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_Voicemail()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_Voicemail_m4071167781 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasNoInternationalDialling()
extern "C"  bool PhoneMetadata_get_HasNoInternationalDialling_m1725370170 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::get_NoInternationalDialling()
extern "C"  PhoneNumberDesc_t922391174 * PhoneMetadata_get_NoInternationalDialling_m2448081149 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasId()
extern "C"  bool PhoneMetadata_get_HasId_m1432118028 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_Id()
extern "C"  String_t* PhoneMetadata_get_Id_m2785027845 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasCountryCode()
extern "C"  bool PhoneMetadata_get_HasCountryCode_m3248332430 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadata::get_CountryCode()
extern "C"  int32_t PhoneMetadata_get_CountryCode_m2175397362 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasInternationalPrefix()
extern "C"  bool PhoneMetadata_get_HasInternationalPrefix_m3671320661 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_InternationalPrefix()
extern "C"  String_t* PhoneMetadata_get_InternationalPrefix_m297348016 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasPreferredInternationalPrefix()
extern "C"  bool PhoneMetadata_get_HasPreferredInternationalPrefix_m3408361842 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_PreferredInternationalPrefix()
extern "C"  String_t* PhoneMetadata_get_PreferredInternationalPrefix_m2823525083 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasNationalPrefix()
extern "C"  bool PhoneMetadata_get_HasNationalPrefix_m1253014049 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_NationalPrefix()
extern "C"  String_t* PhoneMetadata_get_NationalPrefix_m3389614748 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasPreferredExtnPrefix()
extern "C"  bool PhoneMetadata_get_HasPreferredExtnPrefix_m1309726273 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_PreferredExtnPrefix()
extern "C"  String_t* PhoneMetadata_get_PreferredExtnPrefix_m2981050538 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasNationalPrefixForParsing()
extern "C"  bool PhoneMetadata_get_HasNationalPrefixForParsing_m1563922350 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_NationalPrefixForParsing()
extern "C"  String_t* PhoneMetadata_get_NationalPrefixForParsing_m1629673865 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasNationalPrefixTransformRule()
extern "C"  bool PhoneMetadata_get_HasNationalPrefixTransformRule_m2711351703 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_NationalPrefixTransformRule()
extern "C"  String_t* PhoneMetadata_get_NationalPrefixTransformRule_m1909075760 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasSameMobileAndFixedLinePattern()
extern "C"  bool PhoneMetadata_get_HasSameMobileAndFixedLinePattern_m2634568274 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_SameMobileAndFixedLinePattern()
extern "C"  bool PhoneMetadata_get_SameMobileAndFixedLinePattern_m4080923206 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat> PhoneNumbers.PhoneMetadata::get_NumberFormatList()
extern "C"  Il2CppObject* PhoneMetadata_get_NumberFormatList_m585753921 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadata::get_NumberFormatCount()
extern "C"  int32_t PhoneMetadata_get_NumberFormatCount_m2800961324 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.PhoneMetadata::GetNumberFormat(System.Int32)
extern "C"  NumberFormat_t441739224 * PhoneMetadata_GetNumberFormat_m3949792410 (PhoneMetadata_t366861403 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<PhoneNumbers.NumberFormat> PhoneNumbers.PhoneMetadata::get_IntlNumberFormatList()
extern "C"  Il2CppObject* PhoneMetadata_get_IntlNumberFormatList_m3836549572 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadata::get_IntlNumberFormatCount()
extern "C"  int32_t PhoneMetadata_get_IntlNumberFormatCount_m186089319 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.NumberFormat PhoneNumbers.PhoneMetadata::GetIntlNumberFormat(System.Int32)
extern "C"  NumberFormat_t441739224 * PhoneMetadata_GetIntlNumberFormat_m2957849501 (PhoneMetadata_t366861403 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasMainCountryForCode()
extern "C"  bool PhoneMetadata_get_HasMainCountryForCode_m852997580 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_MainCountryForCode()
extern "C"  bool PhoneMetadata_get_MainCountryForCode_m93129384 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasLeadingDigits()
extern "C"  bool PhoneMetadata_get_HasLeadingDigits_m2898296923 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneMetadata::get_LeadingDigits()
extern "C"  String_t* PhoneMetadata_get_LeadingDigits_m3181676964 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_HasLeadingZeroPossible()
extern "C"  bool PhoneMetadata_get_HasLeadingZeroPossible_m2176519738 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_LeadingZeroPossible()
extern "C"  bool PhoneMetadata_get_LeadingZeroPossible_m2768990970 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::get_IsInitialized()
extern "C"  bool PhoneMetadata_get_IsInitialized_m2171596535 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PhoneNumbers.PhoneMetadata::GetHashCode()
extern "C"  int32_t PhoneMetadata_GetHashCode_m3028054391 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneMetadata::Equals(System.Object)
extern "C"  bool PhoneMetadata_Equals_m4117141277 (PhoneMetadata_t366861403 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata::CreateBuilder()
extern "C"  Builder_t1569388123 * PhoneMetadata_CreateBuilder_m2882961055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata::ToBuilder()
extern "C"  Builder_t1569388123 * PhoneMetadata_ToBuilder_m3284400328 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata::CreateBuilderForType()
extern "C"  Builder_t1569388123 * PhoneMetadata_CreateBuilderForType_m3843074468 (PhoneMetadata_t366861403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneMetadata/Builder PhoneNumbers.PhoneMetadata::CreateBuilder(PhoneNumbers.PhoneMetadata)
extern "C"  Builder_t1569388123 * PhoneMetadata_CreateBuilder_m2899629444 (Il2CppObject * __this /* static, unused */, PhoneMetadata_t366861403 * ___prototype0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
