﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsFindPlaces
struct OnlineMapsFindPlaces_t49118535;
// System.String
struct String_t;
// OnlineMapsGoogleAPIQuery
struct OnlineMapsGoogleAPIQuery_t356009153;
// OnlineMapsFindPlacesResult[]
struct OnlineMapsFindPlacesResultU5BU5D_t492800091;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlaces_OnlineMapsFi761642977.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsFindPlaces_OnlineMapsFi153906316.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"

// System.Void OnlineMapsFindPlaces::.ctor(OnlineMapsFindPlaces/OnlineMapsFindPlacesType,System.String,UnityEngine.Vector2,System.Int32,System.String,System.String,System.String,System.String,System.String,System.Int32,System.Int32,System.Boolean,OnlineMapsFindPlaces/OnlineMapsFindPlacesRankBy)
extern "C"  void OnlineMapsFindPlaces__ctor_m945272481 (OnlineMapsFindPlaces_t49118535 * __this, int32_t ___reqType0, String_t* ___key1, Vector2_t2243707579  ___latlng2, int32_t ___radius3, String_t* ___keyword4, String_t* ___name5, String_t* ___types6, String_t* ___query7, String_t* ___language8, int32_t ___minprice9, int32_t ___maxprice10, bool ___opennow11, int32_t ___rankBy12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsQueryType OnlineMapsFindPlaces::get_type()
extern "C"  int32_t OnlineMapsFindPlaces_get_type_m652125776 (OnlineMapsFindPlaces_t49118535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindPlaces::FindNearby(UnityEngine.Vector2,System.Int32,System.String,System.String,System.String,System.String,System.Int32,System.Int32,System.Boolean,OnlineMapsFindPlaces/OnlineMapsFindPlacesRankBy)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindPlaces_FindNearby_m3739507230 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___latlng0, int32_t ___radius1, String_t* ___key2, String_t* ___keyword3, String_t* ___name4, String_t* ___types5, int32_t ___minprice6, int32_t ___maxprice7, bool ___opennow8, int32_t ___rankBy9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindPlaces::FindText(System.String,System.String,UnityEngine.Vector2,System.Int32,System.String,System.String,System.Int32,System.Int32,System.Boolean)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindPlaces_FindText_m3907002962 (Il2CppObject * __this /* static, unused */, String_t* ___query0, String_t* ___key1, Vector2_t2243707579  ___latlng2, int32_t ___radius3, String_t* ___language4, String_t* ___types5, int32_t ___minprice6, int32_t ___maxprice7, bool ___opennow8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindPlaces::FindRadar(UnityEngine.Vector2,System.Int32,System.String,System.String,System.String,System.String,System.Int32,System.Int32,System.Boolean)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindPlaces_FindRadar_m284939529 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___latlng0, int32_t ___radius1, String_t* ___key2, String_t* ___keyword3, String_t* ___name4, String_t* ___types5, int32_t ___minprice6, int32_t ___maxprice7, bool ___opennow8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindPlacesResult[] OnlineMapsFindPlaces::GetResults(System.String)
extern "C"  OnlineMapsFindPlacesResultU5BU5D_t492800091* OnlineMapsFindPlaces_GetResults_m3211217217 (Il2CppObject * __this /* static, unused */, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
