﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,StatusNavScreen>
struct NavigationController_2_t760270839;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,StatusNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m887236312_gshared (NavigationController_2_t760270839 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m887236312(__this, method) ((  void (*) (NavigationController_2_t760270839 *, const MethodInfo*))NavigationController_2__ctor_m887236312_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,StatusNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m3463568014_gshared (NavigationController_2_t760270839 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m3463568014(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t760270839 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m3463568014_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,StatusNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m622743621_gshared (NavigationController_2_t760270839 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m622743621(__this, method) ((  void (*) (NavigationController_2_t760270839 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m622743621_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,StatusNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m1563548144_gshared (NavigationController_2_t760270839 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m1563548144(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t760270839 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1563548144_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,StatusNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m806311540_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m806311540(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t760270839 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m806311540_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,StatusNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m2919636131_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m2919636131(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t760270839 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m2919636131_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,StatusNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m3436155602_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m3436155602(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t760270839 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3436155602_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,StatusNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m680577980_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m680577980(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t760270839 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m680577980_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,StatusNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m3786128028_gshared (NavigationController_2_t760270839 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m3786128028(__this, ___screen0, method) ((  void (*) (NavigationController_2_t760270839 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m3786128028_gshared)(__this, ___screen0, method)
