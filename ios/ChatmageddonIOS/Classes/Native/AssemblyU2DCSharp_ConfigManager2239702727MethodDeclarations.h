﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfigManager
struct ConfigManager_t2239702727;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void ConfigManager::.ctor()
extern "C"  void ConfigManager__ctor_m3590754604 (ConfigManager_t2239702727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigManager::SetupConfig(System.Collections.Hashtable)
extern "C"  void ConfigManager_SetupConfig_m540594259 (ConfigManager_t2239702727 * __this, Hashtable_t909839986 * ___configInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
