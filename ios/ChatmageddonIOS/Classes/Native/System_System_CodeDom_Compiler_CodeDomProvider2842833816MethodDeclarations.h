﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.CodeDomProvider
struct CodeDomProvider_t2842833816;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.Compiler.CodeDomProvider::.ctor()
extern "C"  void CodeDomProvider__ctor_m3439252787 (CodeDomProvider_t2842833816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
