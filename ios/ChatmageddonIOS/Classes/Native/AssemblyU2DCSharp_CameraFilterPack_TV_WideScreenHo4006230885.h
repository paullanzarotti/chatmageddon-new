﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_TV_WideScreenHorizontal
struct  CameraFilterPack_TV_WideScreenHorizontal_t4006230885  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_TV_WideScreenHorizontal::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_TV_WideScreenHorizontal::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_TV_WideScreenHorizontal::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::Size
	float ___Size_6;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::Smooth
	float ___Smooth_7;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::StretchX
	float ___StretchX_8;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::StretchY
	float ___StretchY_9;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Size_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___Size_6)); }
	inline float get_Size_6() const { return ___Size_6; }
	inline float* get_address_of_Size_6() { return &___Size_6; }
	inline void set_Size_6(float value)
	{
		___Size_6 = value;
	}

	inline static int32_t get_offset_of_Smooth_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___Smooth_7)); }
	inline float get_Smooth_7() const { return ___Smooth_7; }
	inline float* get_address_of_Smooth_7() { return &___Smooth_7; }
	inline void set_Smooth_7(float value)
	{
		___Smooth_7 = value;
	}

	inline static int32_t get_offset_of_StretchX_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___StretchX_8)); }
	inline float get_StretchX_8() const { return ___StretchX_8; }
	inline float* get_address_of_StretchX_8() { return &___StretchX_8; }
	inline void set_StretchX_8(float value)
	{
		___StretchX_8 = value;
	}

	inline static int32_t get_offset_of_StretchY_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885, ___StretchY_9)); }
	inline float get_StretchY_9() const { return ___StretchY_9; }
	inline float* get_address_of_StretchY_9() { return &___StretchY_9; }
	inline void set_StretchY_9(float value)
	{
		___StretchY_9 = value;
	}
};

struct CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields
{
public:
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue4
	float ___ChangeValue4_13;

public:
	inline static int32_t get_offset_of_ChangeValue_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields, ___ChangeValue_10)); }
	inline float get_ChangeValue_10() const { return ___ChangeValue_10; }
	inline float* get_address_of_ChangeValue_10() { return &___ChangeValue_10; }
	inline void set_ChangeValue_10(float value)
	{
		___ChangeValue_10 = value;
	}

	inline static int32_t get_offset_of_ChangeValue2_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields, ___ChangeValue2_11)); }
	inline float get_ChangeValue2_11() const { return ___ChangeValue2_11; }
	inline float* get_address_of_ChangeValue2_11() { return &___ChangeValue2_11; }
	inline void set_ChangeValue2_11(float value)
	{
		___ChangeValue2_11 = value;
	}

	inline static int32_t get_offset_of_ChangeValue3_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields, ___ChangeValue3_12)); }
	inline float get_ChangeValue3_12() const { return ___ChangeValue3_12; }
	inline float* get_address_of_ChangeValue3_12() { return &___ChangeValue3_12; }
	inline void set_ChangeValue3_12(float value)
	{
		___ChangeValue3_12 = value;
	}

	inline static int32_t get_offset_of_ChangeValue4_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_TV_WideScreenHorizontal_t4006230885_StaticFields, ___ChangeValue4_13)); }
	inline float get_ChangeValue4_13() const { return ___ChangeValue4_13; }
	inline float* get_address_of_ChangeValue4_13() { return &___ChangeValue4_13; }
	inline void set_ChangeValue4_13(float value)
	{
		___ChangeValue4_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
