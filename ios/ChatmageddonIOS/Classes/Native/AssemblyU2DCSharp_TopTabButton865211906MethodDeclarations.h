﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TopTabButton
struct TopTabButton_t865211906;

#include "codegen/il2cpp-codegen.h"

// System.Void TopTabButton::.ctor()
extern "C"  void TopTabButton__ctor_m1744369981 (TopTabButton_t865211906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TopTabButton::OnButtonClick()
extern "C"  void TopTabButton_OnButtonClick_m2641144710 (TopTabButton_t865211906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
