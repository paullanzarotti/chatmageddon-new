﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TilesetFadeExample/<OnChangeMaterialTexture>c__AnonStorey0
struct U3COnChangeMaterialTextureU3Ec__AnonStorey0_t2046870675;
// TilesetFadeExampleItem
struct TilesetFadeExampleItem_t1982028563;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TilesetFadeExampleItem1982028563.h"

// System.Void TilesetFadeExample/<OnChangeMaterialTexture>c__AnonStorey0::.ctor()
extern "C"  void U3COnChangeMaterialTextureU3Ec__AnonStorey0__ctor_m2283017346 (U3COnChangeMaterialTextureU3Ec__AnonStorey0_t2046870675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TilesetFadeExample/<OnChangeMaterialTexture>c__AnonStorey0::<>m__0(TilesetFadeExampleItem)
extern "C"  bool U3COnChangeMaterialTextureU3Ec__AnonStorey0_U3CU3Em__0_m2775039742 (U3COnChangeMaterialTextureU3Ec__AnonStorey0_t2046870675 * __this, TilesetFadeExampleItem_t1982028563 * ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
