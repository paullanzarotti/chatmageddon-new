﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VibrationController/<PulseVibrate>c__Iterator1
struct U3CPulseVibrateU3Ec__Iterator1_t3172871213;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VibrationController/<PulseVibrate>c__Iterator1::.ctor()
extern "C"  void U3CPulseVibrateU3Ec__Iterator1__ctor_m3213733588 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VibrationController/<PulseVibrate>c__Iterator1::MoveNext()
extern "C"  bool U3CPulseVibrateU3Ec__Iterator1_MoveNext_m1976877108 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<PulseVibrate>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPulseVibrateU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1585096720 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<PulseVibrate>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPulseVibrateU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4195196936 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<PulseVibrate>c__Iterator1::Dispose()
extern "C"  void U3CPulseVibrateU3Ec__Iterator1_Dispose_m3095994143 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<PulseVibrate>c__Iterator1::Reset()
extern "C"  void U3CPulseVibrateU3Ec__Iterator1_Reset_m4006132869 (U3CPulseVibrateU3Ec__Iterator1_t3172871213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
