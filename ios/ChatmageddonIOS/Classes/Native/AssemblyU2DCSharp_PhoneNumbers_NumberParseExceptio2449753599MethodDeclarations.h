﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.NumberParseException
struct NumberParseException_t2449753599;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PhoneNumbers_ErrorType3615858992.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.NumberParseException::.ctor(PhoneNumbers.ErrorType,System.String)
extern "C"  void NumberParseException__ctor_m3648107158 (NumberParseException_t2449753599 * __this, int32_t ___errorType0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
