﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoCtrl/<Screenshot>c__Iterator0
struct U3CScreenshotU3Ec__Iterator0_t3558784402;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoCtrl/<Screenshot>c__Iterator0::.ctor()
extern "C"  void U3CScreenshotU3Ec__Iterator0__ctor_m3028712377 (U3CScreenshotU3Ec__Iterator0_t3558784402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DemoCtrl/<Screenshot>c__Iterator0::MoveNext()
extern "C"  bool U3CScreenshotU3Ec__Iterator0_MoveNext_m3335014807 (U3CScreenshotU3Ec__Iterator0_t3558784402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DemoCtrl/<Screenshot>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CScreenshotU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m853856803 (U3CScreenshotU3Ec__Iterator0_t3558784402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DemoCtrl/<Screenshot>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CScreenshotU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2038316747 (U3CScreenshotU3Ec__Iterator0_t3558784402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl/<Screenshot>c__Iterator0::Dispose()
extern "C"  void U3CScreenshotU3Ec__Iterator0_Dispose_m1881679964 (U3CScreenshotU3Ec__Iterator0_t3558784402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoCtrl/<Screenshot>c__Iterator0::Reset()
extern "C"  void U3CScreenshotU3Ec__Iterator0_Reset_m2895865694 (U3CScreenshotU3Ec__Iterator0_t3558784402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
