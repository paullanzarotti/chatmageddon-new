﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_2_5085141MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::.ctor(System.Collections.Generic.IComparer`1<TKey>)
#define NodeHelper__ctor_m2005806767(__this, ___cmp0, method) ((  void (*) (NodeHelper_t1973284229 *, Il2CppObject*, const MethodInfo*))NodeHelper__ctor_m3442470219_gshared)(__this, ___cmp0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::.cctor()
#define NodeHelper__cctor_m1395365543(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NodeHelper__cctor_m1695207379_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::Compare(TKey,System.Collections.Generic.RBTree/Node)
#define NodeHelper_Compare_m240331818(__this, ___key0, ___node1, method) ((  int32_t (*) (NodeHelper_t1973284229 *, int32_t, Node_t2499136326 *, const MethodInfo*))NodeHelper_Compare_m597226158_gshared)(__this, ___key0, ___node1, method)
// System.Collections.Generic.RBTree/Node System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::CreateNode(TKey)
#define NodeHelper_CreateNode_m2363959040(__this, ___key0, method) ((  Node_t2499136326 * (*) (NodeHelper_t1973284229 *, int32_t, const MethodInfo*))NodeHelper_CreateNode_m738375508_gshared)(__this, ___key0, method)
// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2/NodeHelper<System.Int32,System.Collections.Generic.HashSet`1<System.String>>::GetHelper(System.Collections.Generic.IComparer`1<TKey>)
#define NodeHelper_GetHelper_m2512343221(__this /* static, unused */, ___cmp0, method) ((  NodeHelper_t1973284229 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))NodeHelper_GetHelper_m1332008937_gshared)(__this /* static, unused */, ___cmp0, method)
