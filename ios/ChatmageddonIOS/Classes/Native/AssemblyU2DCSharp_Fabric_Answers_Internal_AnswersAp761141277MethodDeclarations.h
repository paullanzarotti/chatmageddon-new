﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Answers.Internal.AnswersAppleImplementation
struct AnswersAppleImplementation_t761141277;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"
#include "mscorlib_System_Nullable_1_gen3282734688.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::.ctor()
extern "C"  void AnswersAppleImplementation__ctor_m1656104853 (AnswersAppleImplementation_t761141277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogSignUp(System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogSignUp_m3210150417 (Il2CppObject * __this /* static, unused */, String_t* ___signupMethod0, String_t* ___success1, String_t* ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogLogin(System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogLogin_m1627728986 (Il2CppObject * __this /* static, unused */, String_t* ___loginMethod0, String_t* ___success1, String_t* ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogShare(System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogShare_m4035018756 (Il2CppObject * __this /* static, unused */, String_t* ___shareMethod0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, String_t* ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogInvite(System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogInvite_m2319423606 (Il2CppObject * __this /* static, unused */, String_t* ___inviteMethod0, String_t* ___customAttribute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogPurchase(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogPurchase_m214087968 (Il2CppObject * __this /* static, unused */, String_t* ___purchasePrice0, String_t* ___currency1, String_t* ___success2, String_t* ___itemName3, String_t* ___itemType4, String_t* ___itemId5, String_t* ___customAttributes6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogLevelStart(System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogLevelStart_m931089187 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, String_t* ___customAttribute1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogLevelEnd(System.String,System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogLevelEnd_m1503337772 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, String_t* ___levelScore1, String_t* ___success2, String_t* ___customAttribute3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogAddToCart(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogAddToCart_m1706546899 (Il2CppObject * __this /* static, unused */, String_t* ___itemPrice0, String_t* ___currency1, String_t* ___itemName2, String_t* ___itemType3, String_t* ___itemId4, String_t* ___customAttributes5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogStartCheckout(System.String,System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogStartCheckout_m2155035505 (Il2CppObject * __this /* static, unused */, String_t* ___itemPrice0, String_t* ___currency1, String_t* ___itemCount2, String_t* ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogRating(System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogRating_m724995938 (Il2CppObject * __this /* static, unused */, String_t* ___rating0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, String_t* ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogContentView(System.String,System.String,System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogContentView_m3752973181 (Il2CppObject * __this /* static, unused */, String_t* ___contentName0, String_t* ___contentType1, String_t* ___contentId2, String_t* ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogSearch(System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogSearch_m164962775 (Il2CppObject * __this /* static, unused */, String_t* ___query0, String_t* ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::ANSLogCustom(System.String,System.String)
extern "C"  void AnswersAppleImplementation_ANSLogCustom_m1904231280 (Il2CppObject * __this /* static, unused */, String_t* ___customEventName0, String_t* ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogSignUp(System.String,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogSignUp_m2369323058 (AnswersAppleImplementation_t761141277 * __this, String_t* ___method0, Nullable_1_t2088641033  ___success1, Dictionary_2_t309261261 * ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogLogin(System.String,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogLogin_m1377757721 (AnswersAppleImplementation_t761141277 * __this, String_t* ___method0, Nullable_1_t2088641033  ___success1, Dictionary_2_t309261261 * ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogShare(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogShare_m1865744843 (AnswersAppleImplementation_t761141277 * __this, String_t* ___method0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, Dictionary_2_t309261261 * ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogInvite(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogInvite_m1652185869 (AnswersAppleImplementation_t761141277 * __this, String_t* ___method0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogLevelStart(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogLevelStart_m3684396700 (AnswersAppleImplementation_t761141277 * __this, String_t* ___level0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogLevelEnd(System.String,System.Nullable`1<System.Double>,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogLevelEnd_m1663921088 (AnswersAppleImplementation_t761141277 * __this, String_t* ___level0, Nullable_1_t2341081996  ___score1, Nullable_1_t2088641033  ___success2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogAddToCart(System.Nullable`1<System.Decimal>,System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogAddToCart_m1341680497 (AnswersAppleImplementation_t761141277 * __this, Nullable_1_t3282734688  ___itemPrice0, String_t* ___currency1, String_t* ___itemName2, String_t* ___itemType3, String_t* ___itemId4, Dictionary_2_t309261261 * ___customAttributes5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogPurchase(System.Nullable`1<System.Decimal>,System.String,System.Nullable`1<System.Boolean>,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogPurchase_m2308603700 (AnswersAppleImplementation_t761141277 * __this, Nullable_1_t3282734688  ___price0, String_t* ___currency1, Nullable_1_t2088641033  ___success2, String_t* ___itemName3, String_t* ___itemType4, String_t* ___itemId5, Dictionary_2_t309261261 * ___customAttributes6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogStartCheckout(System.Nullable`1<System.Decimal>,System.String,System.Nullable`1<System.Int32>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogStartCheckout_m2297995233 (AnswersAppleImplementation_t761141277 * __this, Nullable_1_t3282734688  ___totalPrice0, String_t* ___currency1, Nullable_1_t334943763  ___itemCount2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogRating(System.Nullable`1<System.Int32>,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogRating_m2869573015 (AnswersAppleImplementation_t761141277 * __this, Nullable_1_t334943763  ___rating0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, Dictionary_2_t309261261 * ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogContentView(System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogContentView_m4283407174 (AnswersAppleImplementation_t761141277 * __this, String_t* ___contentName0, String_t* ___contentType1, String_t* ___contentId2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogSearch(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogSearch_m2736039358 (AnswersAppleImplementation_t761141277 * __this, String_t* ___query0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersAppleImplementation::LogCustom(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersAppleImplementation_LogCustom_m2870369147 (AnswersAppleImplementation_t761141277 * __this, String_t* ___eventName0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Answers.Internal.AnswersAppleImplementation::AsStringOrNull(System.Object)
extern "C"  String_t* AnswersAppleImplementation_AsStringOrNull_m2034993851 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Answers.Internal.AnswersAppleImplementation::boolToString(System.Nullable`1<System.Boolean>)
extern "C"  String_t* AnswersAppleImplementation_boolToString_m4045505176 (Il2CppObject * __this /* static, unused */, Nullable_1_t2088641033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fabric.Answers.Internal.AnswersAppleImplementation::dictionaryToString(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  String_t* AnswersAppleImplementation_dictionaryToString_m1033360217 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
