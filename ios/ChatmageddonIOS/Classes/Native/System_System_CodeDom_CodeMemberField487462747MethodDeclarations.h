﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.CodeMemberField
struct CodeMemberField_t487462747;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.CodeMemberField::.ctor()
extern "C"  void CodeMemberField__ctor_m2988537779 (CodeMemberField_t487462747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
