﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchedMissile
struct LaunchedMissile_t1542515810;
// LaunchedMine
struct LaunchedMine_t3677282773;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LaunchedItemType186177771.h"
#include "AssemblyU2DCSharp_LaunchDirection3410220992.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchedItem
struct  LaunchedItem_t3670634427  : public Il2CppObject
{
public:
	// LaunchedItemType LaunchedItem::itemType
	int32_t ___itemType_0;
	// LaunchedMissile LaunchedItem::missileItem
	LaunchedMissile_t1542515810 * ___missileItem_1;
	// LaunchedMine LaunchedItem::mineItem
	LaunchedMine_t3677282773 * ___mineItem_2;
	// LaunchDirection LaunchedItem::direction
	int32_t ___direction_3;
	// System.DateTime LaunchedItem::launchedTime
	DateTime_t693205669  ___launchedTime_4;
	// System.String LaunchedItem::id
	String_t* ___id_5;

public:
	inline static int32_t get_offset_of_itemType_0() { return static_cast<int32_t>(offsetof(LaunchedItem_t3670634427, ___itemType_0)); }
	inline int32_t get_itemType_0() const { return ___itemType_0; }
	inline int32_t* get_address_of_itemType_0() { return &___itemType_0; }
	inline void set_itemType_0(int32_t value)
	{
		___itemType_0 = value;
	}

	inline static int32_t get_offset_of_missileItem_1() { return static_cast<int32_t>(offsetof(LaunchedItem_t3670634427, ___missileItem_1)); }
	inline LaunchedMissile_t1542515810 * get_missileItem_1() const { return ___missileItem_1; }
	inline LaunchedMissile_t1542515810 ** get_address_of_missileItem_1() { return &___missileItem_1; }
	inline void set_missileItem_1(LaunchedMissile_t1542515810 * value)
	{
		___missileItem_1 = value;
		Il2CppCodeGenWriteBarrier(&___missileItem_1, value);
	}

	inline static int32_t get_offset_of_mineItem_2() { return static_cast<int32_t>(offsetof(LaunchedItem_t3670634427, ___mineItem_2)); }
	inline LaunchedMine_t3677282773 * get_mineItem_2() const { return ___mineItem_2; }
	inline LaunchedMine_t3677282773 ** get_address_of_mineItem_2() { return &___mineItem_2; }
	inline void set_mineItem_2(LaunchedMine_t3677282773 * value)
	{
		___mineItem_2 = value;
		Il2CppCodeGenWriteBarrier(&___mineItem_2, value);
	}

	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(LaunchedItem_t3670634427, ___direction_3)); }
	inline int32_t get_direction_3() const { return ___direction_3; }
	inline int32_t* get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(int32_t value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_launchedTime_4() { return static_cast<int32_t>(offsetof(LaunchedItem_t3670634427, ___launchedTime_4)); }
	inline DateTime_t693205669  get_launchedTime_4() const { return ___launchedTime_4; }
	inline DateTime_t693205669 * get_address_of_launchedTime_4() { return &___launchedTime_4; }
	inline void set_launchedTime_4(DateTime_t693205669  value)
	{
		___launchedTime_4 = value;
	}

	inline static int32_t get_offset_of_id_5() { return static_cast<int32_t>(offsetof(LaunchedItem_t3670634427, ___id_5)); }
	inline String_t* get_id_5() const { return ___id_5; }
	inline String_t** get_address_of_id_5() { return &___id_5; }
	inline void set_id_5(String_t* value)
	{
		___id_5 = value;
		Il2CppCodeGenWriteBarrier(&___id_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
