﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<RegNavScreen>
struct Comparer_1_t3852149774;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<RegNavScreen>::.ctor()
extern "C"  void Comparer_1__ctor_m559106788_gshared (Comparer_1_t3852149774 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m559106788(__this, method) ((  void (*) (Comparer_1_t3852149774 *, const MethodInfo*))Comparer_1__ctor_m559106788_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<RegNavScreen>::.cctor()
extern "C"  void Comparer_1__cctor_m1682090135_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1682090135(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1682090135_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<RegNavScreen>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3590579007_gshared (Comparer_1_t3852149774 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m3590579007(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t3852149774 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m3590579007_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<RegNavScreen>::get_Default()
extern "C"  Comparer_1_t3852149774 * Comparer_1_get_Default_m3222778652_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m3222778652(__this /* static, unused */, method) ((  Comparer_1_t3852149774 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m3222778652_gshared)(__this /* static, unused */, method)
