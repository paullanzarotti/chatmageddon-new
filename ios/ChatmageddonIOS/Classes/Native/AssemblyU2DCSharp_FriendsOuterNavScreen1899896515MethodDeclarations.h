﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FriendsOuterNavScreen
struct FriendsOuterNavScreen_t1899896515;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void FriendsOuterNavScreen::.ctor()
extern "C"  void FriendsOuterNavScreen__ctor_m997720528 (FriendsOuterNavScreen_t1899896515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOuterNavScreen::Start()
extern "C"  void FriendsOuterNavScreen_Start_m1416209332 (FriendsOuterNavScreen_t1899896515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOuterNavScreen::UIClosing()
extern "C"  void FriendsOuterNavScreen_UIClosing_m1468103601 (FriendsOuterNavScreen_t1899896515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOuterNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void FriendsOuterNavScreen_ScreenLoad_m1878768800 (FriendsOuterNavScreen_t1899896515 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOuterNavScreen::UpdateUI()
extern "C"  void FriendsOuterNavScreen_UpdateUI_m3040312115 (FriendsOuterNavScreen_t1899896515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FriendsOuterNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void FriendsOuterNavScreen_ScreenUnload_m68467676 (FriendsOuterNavScreen_t1899896515 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FriendsOuterNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool FriendsOuterNavScreen_ValidateScreenNavigation_m1976331173 (FriendsOuterNavScreen_t1899896515 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
