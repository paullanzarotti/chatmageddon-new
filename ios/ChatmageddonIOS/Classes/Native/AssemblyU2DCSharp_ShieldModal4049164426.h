﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// ShieldInfoButton
struct ShieldInfoButton_t822471353;
// ItemInfoController
struct ItemInfoController_t4231956141;
// ItemSwipeController
struct ItemSwipeController_t3011021799;
// UILabel
struct UILabel_t1795115428;
// TweenScale
struct TweenScale_t2697902175;
// System.Collections.Generic.List`1<Shield>
struct List_1_t2696242213;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldModal
struct  ShieldModal_t4049164426  : public ModalUI_t2568752073
{
public:
	// System.Int32 ShieldModal::currentItemIndex
	int32_t ___currentItemIndex_3;
	// UnityEngine.GameObject ShieldModal::infoObject
	GameObject_t1756533147 * ___infoObject_4;
	// UnityEngine.GameObject ShieldModal::itemObject
	GameObject_t1756533147 * ___itemObject_5;
	// ShieldInfoButton ShieldModal::infoButton
	ShieldInfoButton_t822471353 * ___infoButton_6;
	// ItemInfoController ShieldModal::infoController
	ItemInfoController_t4231956141 * ___infoController_7;
	// ItemSwipeController ShieldModal::swipeController
	ItemSwipeController_t3011021799 * ___swipeController_8;
	// UILabel ShieldModal::itemNameLabel
	UILabel_t1795115428 * ___itemNameLabel_9;
	// UILabel ShieldModal::itemAmountLabel
	UILabel_t1795115428 * ___itemAmountLabel_10;
	// UILabel ShieldModal::itemDurationLabel
	UILabel_t1795115428 * ___itemDurationLabel_11;
	// UnityEngine.GameObject ShieldModal::warningPopUp
	GameObject_t1756533147 * ___warningPopUp_12;
	// TweenScale ShieldModal::popupScaleTween
	TweenScale_t2697902175 * ___popupScaleTween_13;
	// UILabel ShieldModal::warningLabel
	UILabel_t1795115428 * ___warningLabel_14;
	// UnityEngine.GameObject ShieldModal::itemLight
	GameObject_t1756533147 * ___itemLight_15;
	// System.Collections.Generic.List`1<Shield> ShieldModal::shieldItems
	List_1_t2696242213 * ___shieldItems_16;
	// System.Boolean ShieldModal::showingInfo
	bool ___showingInfo_17;
	// System.Boolean ShieldModal::firstTimeLoading
	bool ___firstTimeLoading_18;
	// System.Boolean ShieldModal::popUpClosing
	bool ___popUpClosing_19;

public:
	inline static int32_t get_offset_of_currentItemIndex_3() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___currentItemIndex_3)); }
	inline int32_t get_currentItemIndex_3() const { return ___currentItemIndex_3; }
	inline int32_t* get_address_of_currentItemIndex_3() { return &___currentItemIndex_3; }
	inline void set_currentItemIndex_3(int32_t value)
	{
		___currentItemIndex_3 = value;
	}

	inline static int32_t get_offset_of_infoObject_4() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___infoObject_4)); }
	inline GameObject_t1756533147 * get_infoObject_4() const { return ___infoObject_4; }
	inline GameObject_t1756533147 ** get_address_of_infoObject_4() { return &___infoObject_4; }
	inline void set_infoObject_4(GameObject_t1756533147 * value)
	{
		___infoObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___infoObject_4, value);
	}

	inline static int32_t get_offset_of_itemObject_5() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___itemObject_5)); }
	inline GameObject_t1756533147 * get_itemObject_5() const { return ___itemObject_5; }
	inline GameObject_t1756533147 ** get_address_of_itemObject_5() { return &___itemObject_5; }
	inline void set_itemObject_5(GameObject_t1756533147 * value)
	{
		___itemObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemObject_5, value);
	}

	inline static int32_t get_offset_of_infoButton_6() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___infoButton_6)); }
	inline ShieldInfoButton_t822471353 * get_infoButton_6() const { return ___infoButton_6; }
	inline ShieldInfoButton_t822471353 ** get_address_of_infoButton_6() { return &___infoButton_6; }
	inline void set_infoButton_6(ShieldInfoButton_t822471353 * value)
	{
		___infoButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___infoButton_6, value);
	}

	inline static int32_t get_offset_of_infoController_7() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___infoController_7)); }
	inline ItemInfoController_t4231956141 * get_infoController_7() const { return ___infoController_7; }
	inline ItemInfoController_t4231956141 ** get_address_of_infoController_7() { return &___infoController_7; }
	inline void set_infoController_7(ItemInfoController_t4231956141 * value)
	{
		___infoController_7 = value;
		Il2CppCodeGenWriteBarrier(&___infoController_7, value);
	}

	inline static int32_t get_offset_of_swipeController_8() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___swipeController_8)); }
	inline ItemSwipeController_t3011021799 * get_swipeController_8() const { return ___swipeController_8; }
	inline ItemSwipeController_t3011021799 ** get_address_of_swipeController_8() { return &___swipeController_8; }
	inline void set_swipeController_8(ItemSwipeController_t3011021799 * value)
	{
		___swipeController_8 = value;
		Il2CppCodeGenWriteBarrier(&___swipeController_8, value);
	}

	inline static int32_t get_offset_of_itemNameLabel_9() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___itemNameLabel_9)); }
	inline UILabel_t1795115428 * get_itemNameLabel_9() const { return ___itemNameLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_itemNameLabel_9() { return &___itemNameLabel_9; }
	inline void set_itemNameLabel_9(UILabel_t1795115428 * value)
	{
		___itemNameLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameLabel_9, value);
	}

	inline static int32_t get_offset_of_itemAmountLabel_10() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___itemAmountLabel_10)); }
	inline UILabel_t1795115428 * get_itemAmountLabel_10() const { return ___itemAmountLabel_10; }
	inline UILabel_t1795115428 ** get_address_of_itemAmountLabel_10() { return &___itemAmountLabel_10; }
	inline void set_itemAmountLabel_10(UILabel_t1795115428 * value)
	{
		___itemAmountLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___itemAmountLabel_10, value);
	}

	inline static int32_t get_offset_of_itemDurationLabel_11() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___itemDurationLabel_11)); }
	inline UILabel_t1795115428 * get_itemDurationLabel_11() const { return ___itemDurationLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_itemDurationLabel_11() { return &___itemDurationLabel_11; }
	inline void set_itemDurationLabel_11(UILabel_t1795115428 * value)
	{
		___itemDurationLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___itemDurationLabel_11, value);
	}

	inline static int32_t get_offset_of_warningPopUp_12() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___warningPopUp_12)); }
	inline GameObject_t1756533147 * get_warningPopUp_12() const { return ___warningPopUp_12; }
	inline GameObject_t1756533147 ** get_address_of_warningPopUp_12() { return &___warningPopUp_12; }
	inline void set_warningPopUp_12(GameObject_t1756533147 * value)
	{
		___warningPopUp_12 = value;
		Il2CppCodeGenWriteBarrier(&___warningPopUp_12, value);
	}

	inline static int32_t get_offset_of_popupScaleTween_13() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___popupScaleTween_13)); }
	inline TweenScale_t2697902175 * get_popupScaleTween_13() const { return ___popupScaleTween_13; }
	inline TweenScale_t2697902175 ** get_address_of_popupScaleTween_13() { return &___popupScaleTween_13; }
	inline void set_popupScaleTween_13(TweenScale_t2697902175 * value)
	{
		___popupScaleTween_13 = value;
		Il2CppCodeGenWriteBarrier(&___popupScaleTween_13, value);
	}

	inline static int32_t get_offset_of_warningLabel_14() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___warningLabel_14)); }
	inline UILabel_t1795115428 * get_warningLabel_14() const { return ___warningLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_warningLabel_14() { return &___warningLabel_14; }
	inline void set_warningLabel_14(UILabel_t1795115428 * value)
	{
		___warningLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___warningLabel_14, value);
	}

	inline static int32_t get_offset_of_itemLight_15() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___itemLight_15)); }
	inline GameObject_t1756533147 * get_itemLight_15() const { return ___itemLight_15; }
	inline GameObject_t1756533147 ** get_address_of_itemLight_15() { return &___itemLight_15; }
	inline void set_itemLight_15(GameObject_t1756533147 * value)
	{
		___itemLight_15 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_15, value);
	}

	inline static int32_t get_offset_of_shieldItems_16() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___shieldItems_16)); }
	inline List_1_t2696242213 * get_shieldItems_16() const { return ___shieldItems_16; }
	inline List_1_t2696242213 ** get_address_of_shieldItems_16() { return &___shieldItems_16; }
	inline void set_shieldItems_16(List_1_t2696242213 * value)
	{
		___shieldItems_16 = value;
		Il2CppCodeGenWriteBarrier(&___shieldItems_16, value);
	}

	inline static int32_t get_offset_of_showingInfo_17() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___showingInfo_17)); }
	inline bool get_showingInfo_17() const { return ___showingInfo_17; }
	inline bool* get_address_of_showingInfo_17() { return &___showingInfo_17; }
	inline void set_showingInfo_17(bool value)
	{
		___showingInfo_17 = value;
	}

	inline static int32_t get_offset_of_firstTimeLoading_18() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___firstTimeLoading_18)); }
	inline bool get_firstTimeLoading_18() const { return ___firstTimeLoading_18; }
	inline bool* get_address_of_firstTimeLoading_18() { return &___firstTimeLoading_18; }
	inline void set_firstTimeLoading_18(bool value)
	{
		___firstTimeLoading_18 = value;
	}

	inline static int32_t get_offset_of_popUpClosing_19() { return static_cast<int32_t>(offsetof(ShieldModal_t4049164426, ___popUpClosing_19)); }
	inline bool get_popUpClosing_19() const { return ___popUpClosing_19; }
	inline bool* get_address_of_popUpClosing_19() { return &___popUpClosing_19; }
	inline void set_popUpClosing_19(bool value)
	{
		___popUpClosing_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
