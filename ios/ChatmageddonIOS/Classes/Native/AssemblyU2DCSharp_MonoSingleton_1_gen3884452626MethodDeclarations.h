﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<AttackSearchNavigationController>::.ctor()
#define MonoSingleton_1__ctor_m314658630(__this, method) ((  void (*) (MonoSingleton_1_t3884452626 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<AttackSearchNavigationController>::Awake()
#define MonoSingleton_1_Awake_m323541829(__this, method) ((  void (*) (MonoSingleton_1_t3884452626 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<AttackSearchNavigationController>::get_Instance()
#define MonoSingleton_1_get_Instance_m2852552215(__this /* static, unused */, method) ((  AttackSearchNavigationController_t4133786906 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<AttackSearchNavigationController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4013593483(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<AttackSearchNavigationController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m4193867304(__this, method) ((  void (*) (MonoSingleton_1_t3884452626 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<AttackSearchNavigationController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m461978020(__this, method) ((  void (*) (MonoSingleton_1_t3884452626 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<AttackSearchNavigationController>::.cctor()
#define MonoSingleton_1__cctor_m3352797035(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
