﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenPanelClipSoftness
struct TweenPanelClipSoftness_t2480894570;
// TweenScale
struct TweenScale_t2697902175;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void TweenPanelClipSoftness::.ctor()
extern "C"  void TweenPanelClipSoftness__ctor_m289513151 (TweenPanelClipSoftness_t2480894570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPanelClipSoftness::Awake()
extern "C"  void TweenPanelClipSoftness_Awake_m3950297626 (TweenPanelClipSoftness_t2480894570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPanelClipSoftness::OnUpdate(System.Single,System.Boolean)
extern "C"  void TweenPanelClipSoftness_OnUpdate_m2215903957 (TweenPanelClipSoftness_t2480894570 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenScale TweenPanelClipSoftness::Begin(UnityEngine.GameObject,System.Single,UnityEngine.Vector3)
extern "C"  TweenScale_t2697902175 * TweenPanelClipSoftness_Begin_m314777372 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, float ___duration1, Vector3_t2243707580  ___scale2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
