﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneLoaderUI
struct SceneLoaderUI_t1806459353;

#include "codegen/il2cpp-codegen.h"

// System.Void SceneLoaderUI::.ctor()
extern "C"  void SceneLoaderUI__ctor_m1370350180 (SceneLoaderUI_t1806459353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoaderUI::LoadSceneUI()
extern "C"  void SceneLoaderUI_LoadSceneUI_m2997407222 (SceneLoaderUI_t1806459353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoaderUI::UnloadSceneUI()
extern "C"  void SceneLoaderUI_UnloadSceneUI_m3423030103 (SceneLoaderUI_t1806459353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
