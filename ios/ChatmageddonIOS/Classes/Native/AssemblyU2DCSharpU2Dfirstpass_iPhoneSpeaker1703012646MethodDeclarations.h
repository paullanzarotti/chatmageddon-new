﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iPhoneSpeaker
struct iPhoneSpeaker_t1703012646;

#include "codegen/il2cpp-codegen.h"

// System.Void iPhoneSpeaker::.ctor()
extern "C"  void iPhoneSpeaker__ctor_m486314007 (iPhoneSpeaker_t1703012646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iPhoneSpeaker::_forceToSpeaker()
extern "C"  void iPhoneSpeaker__forceToSpeaker_m2973046869 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iPhoneSpeaker::ForceToSpeaker()
extern "C"  void iPhoneSpeaker_ForceToSpeaker_m3276938204 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
