﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<RegisterNewUser>c__AnonStorey1
struct U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<RegisterNewUser>c__AnonStorey1::.ctor()
extern "C"  void U3CRegisterNewUserU3Ec__AnonStorey1__ctor_m2153077340 (U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<RegisterNewUser>c__AnonStorey1::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRegisterNewUserU3Ec__AnonStorey1_U3CU3Em__0_m964823937 (U3CRegisterNewUserU3Ec__AnonStorey1_t3368816153 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
