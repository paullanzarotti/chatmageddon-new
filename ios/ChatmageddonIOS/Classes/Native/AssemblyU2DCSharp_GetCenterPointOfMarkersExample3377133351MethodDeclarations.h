﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetCenterPointOfMarkersExample
struct GetCenterPointOfMarkersExample_t3377133351;

#include "codegen/il2cpp-codegen.h"

// System.Void GetCenterPointOfMarkersExample::.ctor()
extern "C"  void GetCenterPointOfMarkersExample__ctor_m3942723662 (GetCenterPointOfMarkersExample_t3377133351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetCenterPointOfMarkersExample::OnGUI()
extern "C"  void GetCenterPointOfMarkersExample_OnGUI_m1770110358 (GetCenterPointOfMarkersExample_t3377133351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
