﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_8bits_gb
struct CameraFilterPack_FX_8bits_gb_t2819315972;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_8bits_gb::.ctor()
extern "C"  void CameraFilterPack_FX_8bits_gb__ctor_m958872643 (CameraFilterPack_FX_8bits_gb_t2819315972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_8bits_gb::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_8bits_gb_get_material_m2730670738 (CameraFilterPack_FX_8bits_gb_t2819315972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::Start()
extern "C"  void CameraFilterPack_FX_8bits_gb_Start_m2407181039 (CameraFilterPack_FX_8bits_gb_t2819315972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_8bits_gb_OnRenderImage_m1447822647 (CameraFilterPack_FX_8bits_gb_t2819315972 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::OnValidate()
extern "C"  void CameraFilterPack_FX_8bits_gb_OnValidate_m924676884 (CameraFilterPack_FX_8bits_gb_t2819315972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::Update()
extern "C"  void CameraFilterPack_FX_8bits_gb_Update_m259962914 (CameraFilterPack_FX_8bits_gb_t2819315972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::OnDisable()
extern "C"  void CameraFilterPack_FX_8bits_gb_OnDisable_m2983846108 (CameraFilterPack_FX_8bits_gb_t2819315972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
