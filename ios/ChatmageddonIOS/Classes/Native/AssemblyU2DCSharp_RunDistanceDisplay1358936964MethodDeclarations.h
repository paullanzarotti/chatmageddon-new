﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RunDistanceDisplay
struct RunDistanceDisplay_t1358936964;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void RunDistanceDisplay::.ctor()
extern "C"  void RunDistanceDisplay__ctor_m2041996163 (RunDistanceDisplay_t1358936964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunDistanceDisplay::Awake()
extern "C"  void RunDistanceDisplay_Awake_m154220592 (RunDistanceDisplay_t1358936964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunDistanceDisplay::CalculatingPixelsToMeter()
extern "C"  void RunDistanceDisplay_CalculatingPixelsToMeter_m2614087925 (RunDistanceDisplay_t1358936964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RunDistanceDisplay::UpdateDistance(UnityEngine.LocationInfo,UnityEngine.LocationInfo)
extern "C"  void RunDistanceDisplay_UpdateDistance_m288879825 (RunDistanceDisplay_t1358936964 * __this, LocationInfo_t1364725149  ___startLocation0, LocationInfo_t1364725149  ___currentLocation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single RunDistanceDisplay::deg_2_rad(System.Single)
extern "C"  float RunDistanceDisplay_deg_2_rad_m2632516051 (Il2CppObject * __this /* static, unused */, float ___deg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double RunDistanceDisplay::lon2meters(System.Single)
extern "C"  double RunDistanceDisplay_lon2meters_m2471088262 (Il2CppObject * __this /* static, unused */, float ___lon0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double RunDistanceDisplay::lat2meters(System.Single)
extern "C"  double RunDistanceDisplay_lat2meters_m3893488742 (Il2CppObject * __this /* static, unused */, float ___lat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 RunDistanceDisplay::CalculateLatLongDistanceToXY(System.Single,System.Single,System.Single,System.Single)
extern "C"  Vector3_t2243707580  RunDistanceDisplay_CalculateLatLongDistanceToXY_m1131092471 (Il2CppObject * __this /* static, unused */, float ___originalLat0, float ___originalLong1, float ___destinationLat2, float ___destinationLong3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
