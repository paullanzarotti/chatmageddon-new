﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EdgeDetectEffectNormals
struct EdgeDetectEffectNormals_t1361102129;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void EdgeDetectEffectNormals::.ctor()
extern "C"  void EdgeDetectEffectNormals__ctor_m4081233939 (EdgeDetectEffectNormals_t1361102129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EdgeDetectEffectNormals::CheckResources()
extern "C"  bool EdgeDetectEffectNormals_CheckResources_m2955116892 (EdgeDetectEffectNormals_t1361102129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::Start()
extern "C"  void EdgeDetectEffectNormals_Start_m1227649775 (EdgeDetectEffectNormals_t1361102129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::SetCameraFlag()
extern "C"  void EdgeDetectEffectNormals_SetCameraFlag_m1305851884 (EdgeDetectEffectNormals_t1361102129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::OnEnable()
extern "C"  void EdgeDetectEffectNormals_OnEnable_m2618187279 (EdgeDetectEffectNormals_t1361102129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void EdgeDetectEffectNormals_OnRenderImage_m956560703 (EdgeDetectEffectNormals_t1361102129 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::Main()
extern "C"  void EdgeDetectEffectNormals_Main_m2397647272 (EdgeDetectEffectNormals_t1361102129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
