﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PhoneNumbers.PhoneMetadata>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2779770783(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1426999556 *, int32_t, PhoneMetadata_t366861403 *, const MethodInfo*))KeyValuePair_2__ctor_m3201181706_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,PhoneNumbers.PhoneMetadata>::get_Key()
#define KeyValuePair_2_get_Key_m2352185645(__this, method) ((  int32_t (*) (KeyValuePair_2_t1426999556 *, const MethodInfo*))KeyValuePair_2_get_Key_m1435832840_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PhoneNumbers.PhoneMetadata>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2955494240(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1426999556 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1350990071_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,PhoneNumbers.PhoneMetadata>::get_Value()
#define KeyValuePair_2_get_Value_m1087314765(__this, method) ((  PhoneMetadata_t366861403 * (*) (KeyValuePair_2_t1426999556 *, const MethodInfo*))KeyValuePair_2_get_Value_m3690000728_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PhoneNumbers.PhoneMetadata>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1875837336(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1426999556 *, PhoneMetadata_t366861403 *, const MethodInfo*))KeyValuePair_2_set_Value_m2726037047_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,PhoneNumbers.PhoneMetadata>::ToString()
#define KeyValuePair_2_ToString_m862666360(__this, method) ((  String_t* (*) (KeyValuePair_2_t1426999556 *, const MethodInfo*))KeyValuePair_2_ToString_m1391611625_gshared)(__this, method)
