﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageNavScreen
struct MessageNavScreen_t601131416;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void MessageNavScreen::.ctor()
extern "C"  void MessageNavScreen__ctor_m5942001 (MessageNavScreen_t601131416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageNavScreen::Start()
extern "C"  void MessageNavScreen_Start_m4014129913 (MessageNavScreen_t601131416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageNavScreen::UIClosing()
extern "C"  void MessageNavScreen_UIClosing_m2535688770 (MessageNavScreen_t601131416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void MessageNavScreen_ScreenLoad_m757667639 (MessageNavScreen_t601131416 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MessageNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void MessageNavScreen_ScreenUnload_m1940408175 (MessageNavScreen_t601131416 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MessageNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool MessageNavScreen_ValidateScreenNavigation_m1737598288 (MessageNavScreen_t601131416 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
