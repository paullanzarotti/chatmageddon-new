﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetMissileWarefareStatus>c__AnonStorey26
struct U3CGetMissileWarefareStatusU3Ec__AnonStorey26_t547944235;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetMissileWarefareStatus>c__AnonStorey26::.ctor()
extern "C"  void U3CGetMissileWarefareStatusU3Ec__AnonStorey26__ctor_m1888584244 (U3CGetMissileWarefareStatusU3Ec__AnonStorey26_t547944235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetMissileWarefareStatus>c__AnonStorey26::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetMissileWarefareStatusU3Ec__AnonStorey26_U3CU3Em__0_m94312047 (U3CGetMissileWarefareStatusU3Ec__AnonStorey26_t547944235 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
