﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingPopUp
struct LoadingPopUp_t1016279926;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void LoadingPopUp::.ctor()
extern "C"  void LoadingPopUp__ctor_m3640668875 (LoadingPopUp_t1016279926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingPopUp::ShowLoadingPopUp(System.String)
extern "C"  void LoadingPopUp_ShowLoadingPopUp_m2347559388 (LoadingPopUp_t1016279926 * __this, String_t* ___loadingContent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingPopUp::HideLoadingPopUp()
extern "C"  void LoadingPopUp_HideLoadingPopUp_m2514869899 (LoadingPopUp_t1016279926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
