﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPDatePicker/Date
struct Date_t2675184608;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_IPPickerLabelBase2669006230.h"
#include "AssemblyU2DCSharp_DateTimeLocalization_Language3679215353.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPDatePicker
struct  IPDatePicker_t1471736889  : public IPPickerLabelBase_t2669006230
{
public:
	// DateTimeLocalization/Language IPDatePicker::language
	int32_t ___language_15;
	// IPDatePicker/Date IPDatePicker::pickerMinDate
	Date_t2675184608 * ___pickerMinDate_16;
	// IPDatePicker/Date IPDatePicker::pickerMaxDate
	Date_t2675184608 * ___pickerMaxDate_17;
	// IPDatePicker/Date IPDatePicker::initDate
	Date_t2675184608 * ___initDate_18;
	// System.String IPDatePicker::dateFormat
	String_t* ___dateFormat_19;
	// System.DateTime IPDatePicker::<CurrentDate>k__BackingField
	DateTime_t693205669  ___U3CCurrentDateU3Ek__BackingField_20;
	// System.DateTime IPDatePicker::_minDate
	DateTime_t693205669  ____minDate_21;

public:
	inline static int32_t get_offset_of_language_15() { return static_cast<int32_t>(offsetof(IPDatePicker_t1471736889, ___language_15)); }
	inline int32_t get_language_15() const { return ___language_15; }
	inline int32_t* get_address_of_language_15() { return &___language_15; }
	inline void set_language_15(int32_t value)
	{
		___language_15 = value;
	}

	inline static int32_t get_offset_of_pickerMinDate_16() { return static_cast<int32_t>(offsetof(IPDatePicker_t1471736889, ___pickerMinDate_16)); }
	inline Date_t2675184608 * get_pickerMinDate_16() const { return ___pickerMinDate_16; }
	inline Date_t2675184608 ** get_address_of_pickerMinDate_16() { return &___pickerMinDate_16; }
	inline void set_pickerMinDate_16(Date_t2675184608 * value)
	{
		___pickerMinDate_16 = value;
		Il2CppCodeGenWriteBarrier(&___pickerMinDate_16, value);
	}

	inline static int32_t get_offset_of_pickerMaxDate_17() { return static_cast<int32_t>(offsetof(IPDatePicker_t1471736889, ___pickerMaxDate_17)); }
	inline Date_t2675184608 * get_pickerMaxDate_17() const { return ___pickerMaxDate_17; }
	inline Date_t2675184608 ** get_address_of_pickerMaxDate_17() { return &___pickerMaxDate_17; }
	inline void set_pickerMaxDate_17(Date_t2675184608 * value)
	{
		___pickerMaxDate_17 = value;
		Il2CppCodeGenWriteBarrier(&___pickerMaxDate_17, value);
	}

	inline static int32_t get_offset_of_initDate_18() { return static_cast<int32_t>(offsetof(IPDatePicker_t1471736889, ___initDate_18)); }
	inline Date_t2675184608 * get_initDate_18() const { return ___initDate_18; }
	inline Date_t2675184608 ** get_address_of_initDate_18() { return &___initDate_18; }
	inline void set_initDate_18(Date_t2675184608 * value)
	{
		___initDate_18 = value;
		Il2CppCodeGenWriteBarrier(&___initDate_18, value);
	}

	inline static int32_t get_offset_of_dateFormat_19() { return static_cast<int32_t>(offsetof(IPDatePicker_t1471736889, ___dateFormat_19)); }
	inline String_t* get_dateFormat_19() const { return ___dateFormat_19; }
	inline String_t** get_address_of_dateFormat_19() { return &___dateFormat_19; }
	inline void set_dateFormat_19(String_t* value)
	{
		___dateFormat_19 = value;
		Il2CppCodeGenWriteBarrier(&___dateFormat_19, value);
	}

	inline static int32_t get_offset_of_U3CCurrentDateU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(IPDatePicker_t1471736889, ___U3CCurrentDateU3Ek__BackingField_20)); }
	inline DateTime_t693205669  get_U3CCurrentDateU3Ek__BackingField_20() const { return ___U3CCurrentDateU3Ek__BackingField_20; }
	inline DateTime_t693205669 * get_address_of_U3CCurrentDateU3Ek__BackingField_20() { return &___U3CCurrentDateU3Ek__BackingField_20; }
	inline void set_U3CCurrentDateU3Ek__BackingField_20(DateTime_t693205669  value)
	{
		___U3CCurrentDateU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of__minDate_21() { return static_cast<int32_t>(offsetof(IPDatePicker_t1471736889, ____minDate_21)); }
	inline DateTime_t693205669  get__minDate_21() const { return ____minDate_21; }
	inline DateTime_t693205669 * get_address_of__minDate_21() { return &____minDate_21; }
	inline void set__minDate_21(DateTime_t693205669  value)
	{
		____minDate_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
