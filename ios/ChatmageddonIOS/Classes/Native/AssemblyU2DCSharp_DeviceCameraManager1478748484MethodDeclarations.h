﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceCameraManager
struct DeviceCameraManager_t1478748484;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_CameraRequestLocation942914719.h"

// System.Void DeviceCameraManager::.ctor()
extern "C"  void DeviceCameraManager__ctor_m2871395865 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::Start()
extern "C"  void DeviceCameraManager_Start_m2592268745 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::OnEnable()
extern "C"  void DeviceCameraManager_OnEnable_m1920164777 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::OnDisable()
extern "C"  void DeviceCameraManager_OnDisable_m3565608000 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::SetUIAtlas()
extern "C"  void DeviceCameraManager_SetUIAtlas_m4124295158 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::PromtForPhoto(System.Boolean)
extern "C"  void DeviceCameraManager_PromtForPhoto_m3670758477 (DeviceCameraManager_t1478748484 * __this, bool ___albumn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::imagePickerCancelled()
extern "C"  void DeviceCameraManager_imagePickerCancelled_m3860314683 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::imagePickerChoseImage(System.String)
extern "C"  void DeviceCameraManager_imagePickerChoseImage_m457924803 (DeviceCameraManager_t1478748484 * __this, String_t* ___imagePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::textureLoaded(UnityEngine.Texture2D)
extern "C"  void DeviceCameraManager_textureLoaded_m4109737359 (DeviceCameraManager_t1478748484 * __this, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::textureLoadFailed(System.String)
extern "C"  void DeviceCameraManager_textureLoadFailed_m1695671685 (DeviceCameraManager_t1478748484 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::OpenUI(CameraRequestLocation)
extern "C"  void DeviceCameraManager_OpenUI_m2804778972 (DeviceCameraManager_t1478748484 * __this, int32_t ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::UIClosing()
extern "C"  void DeviceCameraManager_UIClosing_m80354646 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::CloseUI()
extern "C"  void DeviceCameraManager_CloseUI_m258536031 (DeviceCameraManager_t1478748484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceCameraManager::SetLocationAtlas(CameraRequestLocation)
extern "C"  void DeviceCameraManager_SetLocationAtlas_m3767377536 (DeviceCameraManager_t1478748484 * __this, int32_t ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
