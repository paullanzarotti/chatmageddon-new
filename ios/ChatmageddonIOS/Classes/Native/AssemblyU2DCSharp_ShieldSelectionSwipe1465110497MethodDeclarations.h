﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldSelectionSwipe
struct ShieldSelectionSwipe_t1465110497;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldSelectionSwipe::.ctor()
extern "C"  void ShieldSelectionSwipe__ctor_m1152283840 (ShieldSelectionSwipe_t1465110497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldSelectionSwipe::OnSwipeLeft()
extern "C"  void ShieldSelectionSwipe_OnSwipeLeft_m1377230804 (ShieldSelectionSwipe_t1465110497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldSelectionSwipe::OnSwipeRight()
extern "C"  void ShieldSelectionSwipe_OnSwipeRight_m2013036721 (ShieldSelectionSwipe_t1465110497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldSelectionSwipe::OnStaticClick()
extern "C"  void ShieldSelectionSwipe_OnStaticClick_m2486971427 (ShieldSelectionSwipe_t1465110497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
