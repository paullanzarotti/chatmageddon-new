﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.InheritanceAttribute
struct InheritanceAttribute_t2851181518;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_InheritanceLevel4275660400.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.InheritanceAttribute::.ctor()
extern "C"  void InheritanceAttribute__ctor_m2557768107 (InheritanceAttribute_t2851181518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.InheritanceAttribute::.ctor(System.ComponentModel.InheritanceLevel)
extern "C"  void InheritanceAttribute__ctor_m2596872036 (InheritanceAttribute_t2851181518 * __this, int32_t ___inheritanceLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.InheritanceAttribute::.cctor()
extern "C"  void InheritanceAttribute__cctor_m3262381852 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.InheritanceLevel System.ComponentModel.InheritanceAttribute::get_InheritanceLevel()
extern "C"  int32_t InheritanceAttribute_get_InheritanceLevel_m3891235698 (InheritanceAttribute_t2851181518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.InheritanceAttribute::Equals(System.Object)
extern "C"  bool InheritanceAttribute_Equals_m3532540104 (InheritanceAttribute_t2851181518 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.InheritanceAttribute::GetHashCode()
extern "C"  int32_t InheritanceAttribute_GetHashCode_m4190190980 (InheritanceAttribute_t2851181518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.InheritanceAttribute::IsDefaultAttribute()
extern "C"  bool InheritanceAttribute_IsDefaultAttribute_m610469718 (InheritanceAttribute_t2851181518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.InheritanceAttribute::ToString()
extern "C"  String_t* InheritanceAttribute_ToString_m855168452 (InheritanceAttribute_t2851181518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
