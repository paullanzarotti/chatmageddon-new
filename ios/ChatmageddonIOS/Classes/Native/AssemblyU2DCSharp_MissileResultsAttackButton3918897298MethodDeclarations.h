﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileResultsAttackButton
struct MissileResultsAttackButton_t3918897298;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileResultsAttackButton::.ctor()
extern "C"  void MissileResultsAttackButton__ctor_m3656970711 (MissileResultsAttackButton_t3918897298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileResultsAttackButton::OnButtonClick()
extern "C"  void MissileResultsAttackButton_OnButtonClick_m268034874 (MissileResultsAttackButton_t3918897298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
