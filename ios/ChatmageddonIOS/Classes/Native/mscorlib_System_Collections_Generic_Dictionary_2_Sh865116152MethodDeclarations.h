﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>
struct ShimEnumerator_t865116152;
// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1580419290_gshared (ShimEnumerator_t865116152 * __this, Dictionary_2_t759991331 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1580419290(__this, ___host0, method) ((  void (*) (ShimEnumerator_t865116152 *, Dictionary_2_t759991331 *, const MethodInfo*))ShimEnumerator__ctor_m1580419290_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2643130397_gshared (ShimEnumerator_t865116152 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2643130397(__this, method) ((  bool (*) (ShimEnumerator_t865116152 *, const MethodInfo*))ShimEnumerator_MoveNext_m2643130397_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3680683489_gshared (ShimEnumerator_t865116152 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3680683489(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t865116152 *, const MethodInfo*))ShimEnumerator_get_Entry_m3680683489_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3380114500_gshared (ShimEnumerator_t865116152 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3380114500(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t865116152 *, const MethodInfo*))ShimEnumerator_get_Key_m3380114500_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m474096718_gshared (ShimEnumerator_t865116152 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m474096718(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t865116152 *, const MethodInfo*))ShimEnumerator_get_Value_m474096718_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3235998306_gshared (ShimEnumerator_t865116152 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3235998306(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t865116152 *, const MethodInfo*))ShimEnumerator_get_Current_m3235998306_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Char,System.Char>::Reset()
extern "C"  void ShimEnumerator_Reset_m402789628_gshared (ShimEnumerator_t865116152 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m402789628(__this, method) ((  void (*) (ShimEnumerator_t865116152 *, const MethodInfo*))ShimEnumerator_Reset_m402789628_gshared)(__this, method)
