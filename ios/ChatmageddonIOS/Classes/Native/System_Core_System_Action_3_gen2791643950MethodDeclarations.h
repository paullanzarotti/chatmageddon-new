﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen28289771MethodDeclarations.h"

// System.Void System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m1043582556(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t2791643950 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m2795754957_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m3628332330(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t2791643950 *, WebSocketResponse_t3376763264 *, uint16_t, String_t*, const MethodInfo*))Action_3_Invoke_m2754766605_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m2229249519(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t2791643950 *, WebSocketResponse_t3376763264 *, uint16_t, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m1850453620_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<BestHTTP.WebSocket.WebSocketResponse,System.UInt16,System.String>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m3060954728(__this, ___result0, method) ((  void (*) (Action_3_t2791643950 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m2830366695_gshared)(__this, ___result0, method)
