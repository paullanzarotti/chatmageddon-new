﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsNavigationController/<SaveScreenContent>c__Iterator0
struct U3CSaveScreenContentU3Ec__Iterator0_t861775105;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsNavigationController/<SaveScreenContent>c__Iterator0::.ctor()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0__ctor_m2346414110 (U3CSaveScreenContentU3Ec__Iterator0_t861775105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SettingsNavigationController/<SaveScreenContent>c__Iterator0::MoveNext()
extern "C"  bool U3CSaveScreenContentU3Ec__Iterator0_MoveNext_m774693742 (U3CSaveScreenContentU3Ec__Iterator0_t861775105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SettingsNavigationController/<SaveScreenContent>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3844211786 (U3CSaveScreenContentU3Ec__Iterator0_t861775105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SettingsNavigationController/<SaveScreenContent>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSaveScreenContentU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2278590034 (U3CSaveScreenContentU3Ec__Iterator0_t861775105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController/<SaveScreenContent>c__Iterator0::Dispose()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Dispose_m2823505011 (U3CSaveScreenContentU3Ec__Iterator0_t861775105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsNavigationController/<SaveScreenContent>c__Iterator0::Reset()
extern "C"  void U3CSaveScreenContentU3Ec__Iterator0_Reset_m4027517981 (U3CSaveScreenContentU3Ec__Iterator0_t861775105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
