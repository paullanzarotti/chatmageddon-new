﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpringPosition/OnFinished
struct OnFinished_t3890054880;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void SpringPosition/OnFinished::.ctor(System.Object,System.IntPtr)
extern "C"  void OnFinished__ctor_m932607119 (OnFinished_t3890054880 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPosition/OnFinished::Invoke()
extern "C"  void OnFinished_Invoke_m2704218019 (OnFinished_t3890054880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult SpringPosition/OnFinished::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnFinished_BeginInvoke_m1580979898 (OnFinished_t3890054880 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpringPosition/OnFinished::EndInvoke(System.IAsyncResult)
extern "C"  void OnFinished_EndInvoke_m1929061697 (OnFinished_t3890054880 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
