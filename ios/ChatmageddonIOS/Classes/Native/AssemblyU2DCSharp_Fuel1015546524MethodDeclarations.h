﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fuel
struct Fuel_t1015546524;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void Fuel::.ctor(System.Collections.Hashtable)
extern "C"  void Fuel__ctor_m1741833413 (Fuel_t1015546524 * __this, Hashtable_t909839986 * ___itemHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
