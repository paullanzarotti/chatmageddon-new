﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3561032432MethodDeclarations.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2918734891(__this, ___dic0, method) ((  void (*) (KeyCollection_t2900803370 *, SortedDictionary_2_t2686657757 *, const MethodInfo*))KeyCollection__ctor_m2360962727_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1626015080(__this, ___item0, method) ((  void (*) (KeyCollection_t2900803370 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1452078018_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1367909277(__this, method) ((  void (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1877705257_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m861015356(__this, ___item0, method) ((  bool (*) (KeyCollection_t2900803370 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3023445058_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m340647939(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2985680479_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1605272369(__this, method) ((  bool (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m945847853_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1476699325(__this, ___item0, method) ((  bool (*) (KeyCollection_t2900803370 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4130601921_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1963800067(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2900803370 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1628337375_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1564796155(__this, method) ((  bool (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m633672767_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m317130287(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4189355763_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m725815060(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2399335214_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m4188016025(__this, ___array0, ___arrayIndex1, method) ((  void (*) (KeyCollection_t2900803370 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2361880853_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::get_Count()
#define KeyCollection_get_Count_m543322595(__this, method) ((  int32_t (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_get_Count_m1497005991_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1189205231(__this, method) ((  Enumerator_t2551780023  (*) (KeyCollection_t2900803370 *, const MethodInfo*))KeyCollection_GetEnumerator_m253781891_gshared)(__this, method)
