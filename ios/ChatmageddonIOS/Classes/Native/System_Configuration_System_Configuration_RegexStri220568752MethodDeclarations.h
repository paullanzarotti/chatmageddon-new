﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.RegexStringValidator
struct RegexStringValidator_t220568752;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Configuration.RegexStringValidator::.ctor(System.String)
extern "C"  void RegexStringValidator__ctor_m559606177 (RegexStringValidator_t220568752 * __this, String_t* ___regex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.RegexStringValidator::CanValidate(System.Type)
extern "C"  bool RegexStringValidator_CanValidate_m2965493264 (RegexStringValidator_t220568752 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.RegexStringValidator::Validate(System.Object)
extern "C"  void RegexStringValidator_Validate_m3542777055 (RegexStringValidator_t220568752 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
