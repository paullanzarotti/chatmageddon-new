﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserLoginButton
struct  UserLoginButton_t3500827568  : public SFXButton_t792651341
{
public:
	// UnityEngine.GameObject UserLoginButton::startPage
	GameObject_t1756533147 * ___startPage_5;
	// System.Boolean UserLoginButton::loaded
	bool ___loaded_6;
	// System.Boolean UserLoginButton::facebookLogin
	bool ___facebookLogin_7;

public:
	inline static int32_t get_offset_of_startPage_5() { return static_cast<int32_t>(offsetof(UserLoginButton_t3500827568, ___startPage_5)); }
	inline GameObject_t1756533147 * get_startPage_5() const { return ___startPage_5; }
	inline GameObject_t1756533147 ** get_address_of_startPage_5() { return &___startPage_5; }
	inline void set_startPage_5(GameObject_t1756533147 * value)
	{
		___startPage_5 = value;
		Il2CppCodeGenWriteBarrier(&___startPage_5, value);
	}

	inline static int32_t get_offset_of_loaded_6() { return static_cast<int32_t>(offsetof(UserLoginButton_t3500827568, ___loaded_6)); }
	inline bool get_loaded_6() const { return ___loaded_6; }
	inline bool* get_address_of_loaded_6() { return &___loaded_6; }
	inline void set_loaded_6(bool value)
	{
		___loaded_6 = value;
	}

	inline static int32_t get_offset_of_facebookLogin_7() { return static_cast<int32_t>(offsetof(UserLoginButton_t3500827568, ___facebookLogin_7)); }
	inline bool get_facebookLogin_7() const { return ___facebookLogin_7; }
	inline bool* get_address_of_facebookLogin_7() { return &___facebookLogin_7; }
	inline void set_facebookLogin_7(bool value)
	{
		___facebookLogin_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
