﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_MonoSingleton_1_gen490878433.h"
#include "AssemblyU2DCSharp_ShareRequestLocation2965642179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceShareManager`2<ChatmageddonDeviceShareManager,ShareRequestLocation>
struct  DeviceShareManager_2_t4204038203  : public MonoSingleton_1_t490878433
{
public:
	// RequestLocation DeviceShareManager`2::currentLocation
	int32_t ___currentLocation_3;

public:
	inline static int32_t get_offset_of_currentLocation_3() { return static_cast<int32_t>(offsetof(DeviceShareManager_2_t4204038203, ___currentLocation_3)); }
	inline int32_t get_currentLocation_3() const { return ___currentLocation_3; }
	inline int32_t* get_address_of_currentLocation_3() { return &___currentLocation_3; }
	inline void set_currentLocation_3(int32_t value)
	{
		___currentLocation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
