﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenURLButton
struct  OpenURLButton_t170290035  : public SFXButton_t792651341
{
public:
	// System.String OpenURLButton::URL
	String_t* ___URL_5;

public:
	inline static int32_t get_offset_of_URL_5() { return static_cast<int32_t>(offsetof(OpenURLButton_t170290035, ___URL_5)); }
	inline String_t* get_URL_5() const { return ___URL_5; }
	inline String_t** get_address_of_URL_5() { return &___URL_5; }
	inline void set_URL_5(String_t* value)
	{
		___URL_5 = value;
		Il2CppCodeGenWriteBarrier(&___URL_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
