﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeNumberPicker
struct TimeNumberPicker_t1112809442;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void TimeNumberPicker::.ctor()
extern "C"  void TimeNumberPicker__ctor_m3232362279 (TimeNumberPicker_t1112809442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeNumberPicker::SetupPicker(System.Int32)
extern "C"  void TimeNumberPicker_SetupPicker_m1090955471 (TimeNumberPicker_t1112809442 * __this, int32_t ___initTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TimeNumberPicker::Wait(System.Int32)
extern "C"  Il2CppObject * TimeNumberPicker_Wait_m150880867 (TimeNumberPicker_t1112809442 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TimeNumberPicker::GetCurrentValue()
extern "C"  int32_t TimeNumberPicker_GetCurrentValue_m235713161 (TimeNumberPicker_t1112809442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeNumberPicker::PickerSelectionStart()
extern "C"  void TimeNumberPicker_PickerSelectionStart_m1844057029 (TimeNumberPicker_t1112809442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeNumberPicker::PickerRecenterStart()
extern "C"  void TimeNumberPicker_PickerRecenterStart_m3017933033 (TimeNumberPicker_t1112809442 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
