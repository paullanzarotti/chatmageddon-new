﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Panel/<OpenPanelCoroutine>c__Iterator0
struct U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Panel/<OpenPanelCoroutine>c__Iterator0::.ctor()
extern "C"  void U3COpenPanelCoroutineU3Ec__Iterator0__ctor_m3265241725 (U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Panel/<OpenPanelCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3COpenPanelCoroutineU3Ec__Iterator0_MoveNext_m992806067 (U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Panel/<OpenPanelCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COpenPanelCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3312334643 (U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Panel/<OpenPanelCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COpenPanelCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m730782651 (U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Panel/<OpenPanelCoroutine>c__Iterator0::Dispose()
extern "C"  void U3COpenPanelCoroutineU3Ec__Iterator0_Dispose_m1786250548 (U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Panel/<OpenPanelCoroutine>c__Iterator0::Reset()
extern "C"  void U3COpenPanelCoroutineU3Ec__Iterator0_Reset_m3085372734 (U3COpenPanelCoroutineU3Ec__Iterator0_t4147259658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
