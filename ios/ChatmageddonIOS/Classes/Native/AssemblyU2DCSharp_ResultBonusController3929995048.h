﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultModalUI
struct ResultModalUI_t969511824;
// FirstStrikeUI
struct FirstStrikeUI_t2168768222;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultBonusController
struct  ResultBonusController_t3929995048  : public MonoBehaviour_t1158329972
{
public:
	// ResultModalUI ResultBonusController::modal
	ResultModalUI_t969511824 * ___modal_2;
	// FirstStrikeUI ResultBonusController::firstStrikeBonus
	FirstStrikeUI_t2168768222 * ___firstStrikeBonus_3;
	// UnityEngine.GameObject ResultBonusController::perfectLaunchBonus
	GameObject_t1756533147 * ___perfectLaunchBonus_4;
	// UnityEngine.Transform ResultBonusController::scoreZone
	Transform_t3275118058 * ___scoreZone_5;
	// System.Single ResultBonusController::leftXPos
	float ___leftXPos_6;
	// System.Single ResultBonusController::middileXPos
	float ___middileXPos_7;
	// System.Single ResultBonusController::rightXPos
	float ___rightXPos_8;

public:
	inline static int32_t get_offset_of_modal_2() { return static_cast<int32_t>(offsetof(ResultBonusController_t3929995048, ___modal_2)); }
	inline ResultModalUI_t969511824 * get_modal_2() const { return ___modal_2; }
	inline ResultModalUI_t969511824 ** get_address_of_modal_2() { return &___modal_2; }
	inline void set_modal_2(ResultModalUI_t969511824 * value)
	{
		___modal_2 = value;
		Il2CppCodeGenWriteBarrier(&___modal_2, value);
	}

	inline static int32_t get_offset_of_firstStrikeBonus_3() { return static_cast<int32_t>(offsetof(ResultBonusController_t3929995048, ___firstStrikeBonus_3)); }
	inline FirstStrikeUI_t2168768222 * get_firstStrikeBonus_3() const { return ___firstStrikeBonus_3; }
	inline FirstStrikeUI_t2168768222 ** get_address_of_firstStrikeBonus_3() { return &___firstStrikeBonus_3; }
	inline void set_firstStrikeBonus_3(FirstStrikeUI_t2168768222 * value)
	{
		___firstStrikeBonus_3 = value;
		Il2CppCodeGenWriteBarrier(&___firstStrikeBonus_3, value);
	}

	inline static int32_t get_offset_of_perfectLaunchBonus_4() { return static_cast<int32_t>(offsetof(ResultBonusController_t3929995048, ___perfectLaunchBonus_4)); }
	inline GameObject_t1756533147 * get_perfectLaunchBonus_4() const { return ___perfectLaunchBonus_4; }
	inline GameObject_t1756533147 ** get_address_of_perfectLaunchBonus_4() { return &___perfectLaunchBonus_4; }
	inline void set_perfectLaunchBonus_4(GameObject_t1756533147 * value)
	{
		___perfectLaunchBonus_4 = value;
		Il2CppCodeGenWriteBarrier(&___perfectLaunchBonus_4, value);
	}

	inline static int32_t get_offset_of_scoreZone_5() { return static_cast<int32_t>(offsetof(ResultBonusController_t3929995048, ___scoreZone_5)); }
	inline Transform_t3275118058 * get_scoreZone_5() const { return ___scoreZone_5; }
	inline Transform_t3275118058 ** get_address_of_scoreZone_5() { return &___scoreZone_5; }
	inline void set_scoreZone_5(Transform_t3275118058 * value)
	{
		___scoreZone_5 = value;
		Il2CppCodeGenWriteBarrier(&___scoreZone_5, value);
	}

	inline static int32_t get_offset_of_leftXPos_6() { return static_cast<int32_t>(offsetof(ResultBonusController_t3929995048, ___leftXPos_6)); }
	inline float get_leftXPos_6() const { return ___leftXPos_6; }
	inline float* get_address_of_leftXPos_6() { return &___leftXPos_6; }
	inline void set_leftXPos_6(float value)
	{
		___leftXPos_6 = value;
	}

	inline static int32_t get_offset_of_middileXPos_7() { return static_cast<int32_t>(offsetof(ResultBonusController_t3929995048, ___middileXPos_7)); }
	inline float get_middileXPos_7() const { return ___middileXPos_7; }
	inline float* get_address_of_middileXPos_7() { return &___middileXPos_7; }
	inline void set_middileXPos_7(float value)
	{
		___middileXPos_7 = value;
	}

	inline static int32_t get_offset_of_rightXPos_8() { return static_cast<int32_t>(offsetof(ResultBonusController_t3929995048, ___rightXPos_8)); }
	inline float get_rightXPos_8() const { return ___rightXPos_8; }
	inline float* get_address_of_rightXPos_8() { return &___rightXPos_8; }
	inline void set_rightXPos_8(float value)
	{
		___rightXPos_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
