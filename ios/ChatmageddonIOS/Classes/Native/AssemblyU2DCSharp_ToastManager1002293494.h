﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Toaster
struct Toaster_t2428368812;
// System.Collections.Generic.List`1<Bread>
struct List_1_t4121260458;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen752959214.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToastManager
struct  ToastManager_t1002293494  : public MonoSingleton_1_t752959214
{
public:
	// Toaster ToastManager::toaster
	Toaster_t2428368812 * ___toaster_3;
	// System.Collections.Generic.List`1<Bread> ToastManager::loaf
	List_1_t4121260458 * ___loaf_4;
	// System.String ToastManager::uiResourcePath
	String_t* ___uiResourcePath_5;

public:
	inline static int32_t get_offset_of_toaster_3() { return static_cast<int32_t>(offsetof(ToastManager_t1002293494, ___toaster_3)); }
	inline Toaster_t2428368812 * get_toaster_3() const { return ___toaster_3; }
	inline Toaster_t2428368812 ** get_address_of_toaster_3() { return &___toaster_3; }
	inline void set_toaster_3(Toaster_t2428368812 * value)
	{
		___toaster_3 = value;
		Il2CppCodeGenWriteBarrier(&___toaster_3, value);
	}

	inline static int32_t get_offset_of_loaf_4() { return static_cast<int32_t>(offsetof(ToastManager_t1002293494, ___loaf_4)); }
	inline List_1_t4121260458 * get_loaf_4() const { return ___loaf_4; }
	inline List_1_t4121260458 ** get_address_of_loaf_4() { return &___loaf_4; }
	inline void set_loaf_4(List_1_t4121260458 * value)
	{
		___loaf_4 = value;
		Il2CppCodeGenWriteBarrier(&___loaf_4, value);
	}

	inline static int32_t get_offset_of_uiResourcePath_5() { return static_cast<int32_t>(offsetof(ToastManager_t1002293494, ___uiResourcePath_5)); }
	inline String_t* get_uiResourcePath_5() const { return ___uiResourcePath_5; }
	inline String_t** get_address_of_uiResourcePath_5() { return &___uiResourcePath_5; }
	inline void set_uiResourcePath_5(String_t* value)
	{
		___uiResourcePath_5 = value;
		Il2CppCodeGenWriteBarrier(&___uiResourcePath_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
