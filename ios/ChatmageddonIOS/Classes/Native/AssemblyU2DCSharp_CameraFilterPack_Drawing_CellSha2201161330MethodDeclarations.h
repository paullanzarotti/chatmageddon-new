﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_CellShading2
struct CameraFilterPack_Drawing_CellShading2_t2201161330;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_CellShading2::.ctor()
extern "C"  void CameraFilterPack_Drawing_CellShading2__ctor_m873300845 (CameraFilterPack_Drawing_CellShading2_t2201161330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_CellShading2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_CellShading2_get_material_m1997540282 (CameraFilterPack_Drawing_CellShading2_t2201161330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::Start()
extern "C"  void CameraFilterPack_Drawing_CellShading2_Start_m516034957 (CameraFilterPack_Drawing_CellShading2_t2201161330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_CellShading2_OnRenderImage_m2215771061 (CameraFilterPack_Drawing_CellShading2_t2201161330 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::OnValidate()
extern "C"  void CameraFilterPack_Drawing_CellShading2_OnValidate_m3744539550 (CameraFilterPack_Drawing_CellShading2_t2201161330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::Update()
extern "C"  void CameraFilterPack_Drawing_CellShading2_Update_m2851901712 (CameraFilterPack_Drawing_CellShading2_t2201161330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::OnDisable()
extern "C"  void CameraFilterPack_Drawing_CellShading2_OnDisable_m3028695102 (CameraFilterPack_Drawing_CellShading2_t2201161330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
