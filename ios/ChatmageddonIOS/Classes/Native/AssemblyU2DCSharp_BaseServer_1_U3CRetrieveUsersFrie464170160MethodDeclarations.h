﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<RetrieveUsersFriendsListPlusPending>c__AnonStoreyE<System.Object>
struct U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t464170160;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<RetrieveUsersFriendsListPlusPending>c__AnonStoreyE<System.Object>::.ctor()
extern "C"  void U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE__ctor_m2360934971_gshared (U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t464170160 * __this, const MethodInfo* method);
#define U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE__ctor_m2360934971(__this, method) ((  void (*) (U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t464170160 *, const MethodInfo*))U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE__ctor_m2360934971_gshared)(__this, method)
// System.Void BaseServer`1/<RetrieveUsersFriendsListPlusPending>c__AnonStoreyE<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_U3CU3Em__0_m3111754196_gshared (U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t464170160 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_U3CU3Em__0_m3111754196(__this, ___request0, ___response1, method) ((  void (*) (U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_t464170160 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRetrieveUsersFriendsListPlusPendingU3Ec__AnonStoreyE_U3CU3Em__0_m3111754196_gshared)(__this, ___request0, ___response1, method)
