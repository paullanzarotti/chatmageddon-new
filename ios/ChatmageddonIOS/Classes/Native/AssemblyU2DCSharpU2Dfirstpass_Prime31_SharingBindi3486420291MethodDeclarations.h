﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.SharingBinding
struct SharingBinding_t3486420291;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Prime31.SharingBinding::.ctor()
extern "C"  void SharingBinding__ctor_m1101800491 (SharingBinding_t3486420291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingBinding::_sharingShareItems(System.String,System.String)
extern "C"  void SharingBinding__sharingShareItems_m982313587 (Il2CppObject * __this /* static, unused */, String_t* ___items0, String_t* ___excludedActivityTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingBinding::shareItems(System.String[])
extern "C"  void SharingBinding_shareItems_m2558534608 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___items0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingBinding::shareItems(System.String[],System.String[])
extern "C"  void SharingBinding_shareItems_m3794442052 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___items0, StringU5BU5D_t1642385972* ___excludedActivityTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingBinding::_sharingSetPopoverPosition(System.Single,System.Single)
extern "C"  void SharingBinding__sharingSetPopoverPosition_m2119073504 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.SharingBinding::setPopoverPosition(System.Single,System.Single)
extern "C"  void SharingBinding_setPopoverPosition_m1051176585 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
