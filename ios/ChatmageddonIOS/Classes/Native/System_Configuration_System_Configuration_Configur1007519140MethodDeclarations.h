﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ConfigurationValidatorAttribute
struct ConfigurationValidatorAttribute_t1007519140;
// System.Type
struct Type_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Configuration.ConfigurationValidatorAttribute::.ctor()
extern "C"  void ConfigurationValidatorAttribute__ctor_m354562843 (ConfigurationValidatorAttribute_t1007519140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationValidatorAttribute::.ctor(System.Type)
extern "C"  void ConfigurationValidatorAttribute__ctor_m1320911436 (ConfigurationValidatorAttribute_t1007519140 * __this, Type_t * ___validator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.ConfigurationValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * ConfigurationValidatorAttribute_get_ValidatorInstance_m35782466 (ConfigurationValidatorAttribute_t1007519140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Configuration.ConfigurationValidatorAttribute::get_ValidatorType()
extern "C"  Type_t * ConfigurationValidatorAttribute_get_ValidatorType_m431163140 (ConfigurationValidatorAttribute_t1007519140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
