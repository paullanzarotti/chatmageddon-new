﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Drunk
struct CameraFilterPack_FX_Drunk_t4195520962;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Drunk::.ctor()
extern "C"  void CameraFilterPack_FX_Drunk__ctor_m119446547 (CameraFilterPack_FX_Drunk_t4195520962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Drunk::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Drunk_get_material_m3570574994 (CameraFilterPack_FX_Drunk_t4195520962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::Start()
extern "C"  void CameraFilterPack_FX_Drunk_Start_m3037255559 (CameraFilterPack_FX_Drunk_t4195520962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Drunk_OnRenderImage_m1118470431 (CameraFilterPack_FX_Drunk_t4195520962 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::OnValidate()
extern "C"  void CameraFilterPack_FX_Drunk_OnValidate_m3299395138 (CameraFilterPack_FX_Drunk_t4195520962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::Update()
extern "C"  void CameraFilterPack_FX_Drunk_Update_m1017567088 (CameraFilterPack_FX_Drunk_t4195520962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::OnDisable()
extern "C"  void CameraFilterPack_FX_Drunk_OnDisable_m1340223382 (CameraFilterPack_FX_Drunk_t4195520962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
