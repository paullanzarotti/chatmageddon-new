﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Answers.Internal.AnswersStubImplementation
struct AnswersStubImplementation_t4112235189;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen2341081996.h"
#include "mscorlib_System_Nullable_1_gen3282734688.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void Fabric.Answers.Internal.AnswersStubImplementation::.ctor()
extern "C"  void AnswersStubImplementation__ctor_m4051706137 (AnswersStubImplementation_t4112235189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogSignUp(System.String,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogSignUp_m639032090 (AnswersStubImplementation_t4112235189 * __this, String_t* ___method0, Nullable_1_t2088641033  ___success1, Dictionary_2_t309261261 * ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogLogin(System.String,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogLogin_m516203509 (AnswersStubImplementation_t4112235189 * __this, String_t* ___method0, Nullable_1_t2088641033  ___success1, Dictionary_2_t309261261 * ___customAttributes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogShare(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogShare_m4084607143 (AnswersStubImplementation_t4112235189 * __this, String_t* ___method0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, Dictionary_2_t309261261 * ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogInvite(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogInvite_m625541985 (AnswersStubImplementation_t4112235189 * __this, String_t* ___method0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogLevelStart(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogLevelStart_m2486574764 (AnswersStubImplementation_t4112235189 * __this, String_t* ___level0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogLevelEnd(System.String,System.Nullable`1<System.Double>,System.Nullable`1<System.Boolean>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogLevelEnd_m2478603824 (AnswersStubImplementation_t4112235189 * __this, String_t* ___level0, Nullable_1_t2341081996  ___score1, Nullable_1_t2088641033  ___success2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogAddToCart(System.Nullable`1<System.Decimal>,System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogAddToCart_m3735720477 (AnswersStubImplementation_t4112235189 * __this, Nullable_1_t3282734688  ___itemPrice0, String_t* ___currency1, String_t* ___itemName2, String_t* ___itemType3, String_t* ___itemId4, Dictionary_2_t309261261 * ___customAttributes5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogPurchase(System.Nullable`1<System.Decimal>,System.String,System.Nullable`1<System.Boolean>,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogPurchase_m117116996 (AnswersStubImplementation_t4112235189 * __this, Nullable_1_t3282734688  ___price0, String_t* ___currency1, Nullable_1_t2088641033  ___success2, String_t* ___itemName3, String_t* ___itemType4, String_t* ___itemId5, Dictionary_2_t309261261 * ___customAttributes6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogStartCheckout(System.Nullable`1<System.Decimal>,System.String,System.Nullable`1<System.Int32>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogStartCheckout_m1587319053 (AnswersStubImplementation_t4112235189 * __this, Nullable_1_t3282734688  ___totalPrice0, String_t* ___currency1, Nullable_1_t334943763  ___itemCount2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogRating(System.Nullable`1<System.Int32>,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogRating_m2507348491 (AnswersStubImplementation_t4112235189 * __this, Nullable_1_t334943763  ___rating0, String_t* ___contentName1, String_t* ___contentType2, String_t* ___contentId3, Dictionary_2_t309261261 * ___customAttributes4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogContentView(System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogContentView_m1337549694 (AnswersStubImplementation_t4112235189 * __this, String_t* ___contentName0, String_t* ___contentType1, String_t* ___contentId2, Dictionary_2_t309261261 * ___customAttributes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogSearch(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogSearch_m133311486 (AnswersStubImplementation_t4112235189 * __this, String_t* ___query0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Answers.Internal.AnswersStubImplementation::LogCustom(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void AnswersStubImplementation_LogCustom_m2592253575 (AnswersStubImplementation_t4112235189 * __this, String_t* ___eventName0, Dictionary_2_t309261261 * ___customAttributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
