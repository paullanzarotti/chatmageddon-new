﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryAnalytics
struct FlurryAnalytics_t1653170852;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FlurryAnalytics::.ctor()
extern "C"  void FlurryAnalytics__ctor_m1078859397 (FlurryAnalytics_t1653170852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurryStartSession(System.String)
extern "C"  void FlurryAnalytics__flurryStartSession_m1149300910 (Il2CppObject * __this /* static, unused */, String_t* ___apiKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::startSession(System.String)
extern "C"  void FlurryAnalytics_startSession_m89629825 (Il2CppObject * __this /* static, unused */, String_t* ___apiKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurryLogEvent(System.String,System.Boolean)
extern "C"  void FlurryAnalytics__flurryLogEvent_m3078342741 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, bool ___isTimed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::logEvent(System.String,System.Boolean)
extern "C"  void FlurryAnalytics_logEvent_m3955306246 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, bool ___isTimed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurryLogEventWithParameters(System.String,System.String,System.Boolean)
extern "C"  void FlurryAnalytics__flurryLogEventWithParameters_m1661032899 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___parameters1, bool ___isTimed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::logEventWithParameters(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Boolean)
extern "C"  void FlurryAnalytics_logEventWithParameters_m567251195 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t3943999495 * ___parameters1, bool ___isTimed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurryEndTimedEvent(System.String,System.String)
extern "C"  void FlurryAnalytics__flurryEndTimedEvent_m738129752 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::endTimedEvent(System.String)
extern "C"  void FlurryAnalytics_endTimedEvent_m4022007809 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::endTimedEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FlurryAnalytics_endTimedEvent_m1756789212 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t3943999495 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurrySetAge(System.Int32)
extern "C"  void FlurryAnalytics__flurrySetAge_m2077095498 (Il2CppObject * __this /* static, unused */, int32_t ___age0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::setAge(System.Int32)
extern "C"  void FlurryAnalytics_setAge_m986984589 (Il2CppObject * __this /* static, unused */, int32_t ___age0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurrySetGender(System.String)
extern "C"  void FlurryAnalytics__flurrySetGender_m1388461071 (Il2CppObject * __this /* static, unused */, String_t* ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::setGender(System.String)
extern "C"  void FlurryAnalytics_setGender_m3455225198 (Il2CppObject * __this /* static, unused */, String_t* ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurrySetUserId(System.String)
extern "C"  void FlurryAnalytics__flurrySetUserId_m1206302882 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::setUserId(System.String)
extern "C"  void FlurryAnalytics_setUserId_m798208373 (Il2CppObject * __this /* static, unused */, String_t* ___userId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurrySetSessionReportsOnCloseEnabled(System.Boolean)
extern "C"  void FlurryAnalytics__flurrySetSessionReportsOnCloseEnabled_m3103306572 (Il2CppObject * __this /* static, unused */, bool ___sendSessionReportsOnClose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::setSessionReportsOnCloseEnabled(System.Boolean)
extern "C"  void FlurryAnalytics_setSessionReportsOnCloseEnabled_m4190287025 (Il2CppObject * __this /* static, unused */, bool ___sendSessionReportsOnClose0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::_flurrySetSessionReportsOnPauseEnabled(System.Boolean)
extern "C"  void FlurryAnalytics__flurrySetSessionReportsOnPauseEnabled_m2969946438 (Il2CppObject * __this /* static, unused */, bool ___setSessionReportsOnPauseEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryAnalytics::setSessionReportsOnPauseEnabled(System.Boolean)
extern "C"  void FlurryAnalytics_setSessionReportsOnPauseEnabled_m2846348441 (Il2CppObject * __this /* static, unused */, bool ___setSessionReportsOnPauseEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
