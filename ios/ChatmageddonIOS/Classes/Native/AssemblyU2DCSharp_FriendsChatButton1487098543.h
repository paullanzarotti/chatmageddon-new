﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FriendsListItem
struct FriendsListItem_t521220246;

#include "AssemblyU2DCSharp_DoubleStateButton1032633262.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FriendsChatButton
struct  FriendsChatButton_t1487098543  : public DoubleStateButton_t1032633262
{
public:
	// FriendsListItem FriendsChatButton::item
	FriendsListItem_t521220246 * ___item_13;

public:
	inline static int32_t get_offset_of_item_13() { return static_cast<int32_t>(offsetof(FriendsChatButton_t1487098543, ___item_13)); }
	inline FriendsListItem_t521220246 * get_item_13() const { return ___item_13; }
	inline FriendsListItem_t521220246 ** get_address_of_item_13() { return &___item_13; }
	inline void set_item_13(FriendsListItem_t521220246 * value)
	{
		___item_13 = value;
		Il2CppCodeGenWriteBarrier(&___item_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
