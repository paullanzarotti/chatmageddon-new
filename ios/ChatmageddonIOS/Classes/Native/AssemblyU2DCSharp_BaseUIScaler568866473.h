﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseUIScaler
struct  BaseUIScaler_t568866473  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BaseUIScaler::scaleOnAwake
	bool ___scaleOnAwake_2;
	// System.Boolean BaseUIScaler::scaleOnStart
	bool ___scaleOnStart_3;
	// System.Single BaseUIScaler::devResWidth
	float ___devResWidth_4;
	// System.Single BaseUIScaler::devResHeight
	float ___devResHeight_5;
	// System.Single BaseUIScaler::calcScaleAspect
	float ___calcScaleAspect_6;
	// System.Single BaseUIScaler::startScaleWidth
	float ___startScaleWidth_7;
	// System.Single BaseUIScaler::startScaleHeight
	float ___startScaleHeight_8;
	// System.Single BaseUIScaler::calcScaleWidth
	float ___calcScaleWidth_9;
	// System.Single BaseUIScaler::calcScaleHeight
	float ___calcScaleHeight_10;
	// UnityEngine.Vector3 BaseUIScaler::calcScaleVector
	Vector3_t2243707580  ___calcScaleVector_11;
	// UnityEngine.Vector3 BaseUIScaler::evenCalcScaleVectorWidth
	Vector3_t2243707580  ___evenCalcScaleVectorWidth_12;
	// UnityEngine.Vector3 BaseUIScaler::evenCalcScaleVectorHeight
	Vector3_t2243707580  ___evenCalcScaleVectorHeight_13;

public:
	inline static int32_t get_offset_of_scaleOnAwake_2() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___scaleOnAwake_2)); }
	inline bool get_scaleOnAwake_2() const { return ___scaleOnAwake_2; }
	inline bool* get_address_of_scaleOnAwake_2() { return &___scaleOnAwake_2; }
	inline void set_scaleOnAwake_2(bool value)
	{
		___scaleOnAwake_2 = value;
	}

	inline static int32_t get_offset_of_scaleOnStart_3() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___scaleOnStart_3)); }
	inline bool get_scaleOnStart_3() const { return ___scaleOnStart_3; }
	inline bool* get_address_of_scaleOnStart_3() { return &___scaleOnStart_3; }
	inline void set_scaleOnStart_3(bool value)
	{
		___scaleOnStart_3 = value;
	}

	inline static int32_t get_offset_of_devResWidth_4() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___devResWidth_4)); }
	inline float get_devResWidth_4() const { return ___devResWidth_4; }
	inline float* get_address_of_devResWidth_4() { return &___devResWidth_4; }
	inline void set_devResWidth_4(float value)
	{
		___devResWidth_4 = value;
	}

	inline static int32_t get_offset_of_devResHeight_5() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___devResHeight_5)); }
	inline float get_devResHeight_5() const { return ___devResHeight_5; }
	inline float* get_address_of_devResHeight_5() { return &___devResHeight_5; }
	inline void set_devResHeight_5(float value)
	{
		___devResHeight_5 = value;
	}

	inline static int32_t get_offset_of_calcScaleAspect_6() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___calcScaleAspect_6)); }
	inline float get_calcScaleAspect_6() const { return ___calcScaleAspect_6; }
	inline float* get_address_of_calcScaleAspect_6() { return &___calcScaleAspect_6; }
	inline void set_calcScaleAspect_6(float value)
	{
		___calcScaleAspect_6 = value;
	}

	inline static int32_t get_offset_of_startScaleWidth_7() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___startScaleWidth_7)); }
	inline float get_startScaleWidth_7() const { return ___startScaleWidth_7; }
	inline float* get_address_of_startScaleWidth_7() { return &___startScaleWidth_7; }
	inline void set_startScaleWidth_7(float value)
	{
		___startScaleWidth_7 = value;
	}

	inline static int32_t get_offset_of_startScaleHeight_8() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___startScaleHeight_8)); }
	inline float get_startScaleHeight_8() const { return ___startScaleHeight_8; }
	inline float* get_address_of_startScaleHeight_8() { return &___startScaleHeight_8; }
	inline void set_startScaleHeight_8(float value)
	{
		___startScaleHeight_8 = value;
	}

	inline static int32_t get_offset_of_calcScaleWidth_9() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___calcScaleWidth_9)); }
	inline float get_calcScaleWidth_9() const { return ___calcScaleWidth_9; }
	inline float* get_address_of_calcScaleWidth_9() { return &___calcScaleWidth_9; }
	inline void set_calcScaleWidth_9(float value)
	{
		___calcScaleWidth_9 = value;
	}

	inline static int32_t get_offset_of_calcScaleHeight_10() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___calcScaleHeight_10)); }
	inline float get_calcScaleHeight_10() const { return ___calcScaleHeight_10; }
	inline float* get_address_of_calcScaleHeight_10() { return &___calcScaleHeight_10; }
	inline void set_calcScaleHeight_10(float value)
	{
		___calcScaleHeight_10 = value;
	}

	inline static int32_t get_offset_of_calcScaleVector_11() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___calcScaleVector_11)); }
	inline Vector3_t2243707580  get_calcScaleVector_11() const { return ___calcScaleVector_11; }
	inline Vector3_t2243707580 * get_address_of_calcScaleVector_11() { return &___calcScaleVector_11; }
	inline void set_calcScaleVector_11(Vector3_t2243707580  value)
	{
		___calcScaleVector_11 = value;
	}

	inline static int32_t get_offset_of_evenCalcScaleVectorWidth_12() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___evenCalcScaleVectorWidth_12)); }
	inline Vector3_t2243707580  get_evenCalcScaleVectorWidth_12() const { return ___evenCalcScaleVectorWidth_12; }
	inline Vector3_t2243707580 * get_address_of_evenCalcScaleVectorWidth_12() { return &___evenCalcScaleVectorWidth_12; }
	inline void set_evenCalcScaleVectorWidth_12(Vector3_t2243707580  value)
	{
		___evenCalcScaleVectorWidth_12 = value;
	}

	inline static int32_t get_offset_of_evenCalcScaleVectorHeight_13() { return static_cast<int32_t>(offsetof(BaseUIScaler_t568866473, ___evenCalcScaleVectorHeight_13)); }
	inline Vector3_t2243707580  get_evenCalcScaleVectorHeight_13() const { return ___evenCalcScaleVectorHeight_13; }
	inline Vector3_t2243707580 * get_address_of_evenCalcScaleVectorHeight_13() { return &___evenCalcScaleVectorHeight_13; }
	inline void set_evenCalcScaleVectorHeight_13(Vector3_t2243707580  value)
	{
		___evenCalcScaleVectorHeight_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
