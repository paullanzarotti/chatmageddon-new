﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.Process/ProcessAsyncReader
struct ProcessAsyncReader_t2663114743;
// System.Diagnostics.Process
struct Process_t1448008787;
// System.Threading.WaitHandle
struct WaitHandle_t677569169;
struct ProcessAsyncReader_t2663114743_marshaled_pinvoke;
struct ProcessAsyncReader_t2663114743_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Diagnostics_Process1448008787.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void System.Diagnostics.Process/ProcessAsyncReader::.ctor(System.Diagnostics.Process,System.IntPtr,System.Boolean)
extern "C"  void ProcessAsyncReader__ctor_m3919818622 (ProcessAsyncReader_t2663114743 * __this, Process_t1448008787 * ___process0, IntPtr_t ___handle1, bool ___err_out2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Process/ProcessAsyncReader::AddInput()
extern "C"  void ProcessAsyncReader_AddInput_m3687887830 (ProcessAsyncReader_t2663114743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Process/ProcessAsyncReader::FlushLast()
extern "C"  void ProcessAsyncReader_FlushLast_m1751309261 (ProcessAsyncReader_t2663114743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Process/ProcessAsyncReader::Flush(System.Boolean)
extern "C"  void ProcessAsyncReader_Flush_m1502876772 (ProcessAsyncReader_t2663114743 * __this, bool ___last0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.Process/ProcessAsyncReader::get_IsCompleted()
extern "C"  bool ProcessAsyncReader_get_IsCompleted_m4194138905 (ProcessAsyncReader_t2663114743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle System.Diagnostics.Process/ProcessAsyncReader::get_WaitHandle()
extern "C"  WaitHandle_t677569169 * ProcessAsyncReader_get_WaitHandle_m2764147894 (ProcessAsyncReader_t2663114743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Process/ProcessAsyncReader::Close()
extern "C"  void ProcessAsyncReader_Close_m921155157 (ProcessAsyncReader_t2663114743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ProcessAsyncReader_t2663114743;
struct ProcessAsyncReader_t2663114743_marshaled_pinvoke;

extern "C" void ProcessAsyncReader_t2663114743_marshal_pinvoke(const ProcessAsyncReader_t2663114743& unmarshaled, ProcessAsyncReader_t2663114743_marshaled_pinvoke& marshaled);
extern "C" void ProcessAsyncReader_t2663114743_marshal_pinvoke_back(const ProcessAsyncReader_t2663114743_marshaled_pinvoke& marshaled, ProcessAsyncReader_t2663114743& unmarshaled);
extern "C" void ProcessAsyncReader_t2663114743_marshal_pinvoke_cleanup(ProcessAsyncReader_t2663114743_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ProcessAsyncReader_t2663114743;
struct ProcessAsyncReader_t2663114743_marshaled_com;

extern "C" void ProcessAsyncReader_t2663114743_marshal_com(const ProcessAsyncReader_t2663114743& unmarshaled, ProcessAsyncReader_t2663114743_marshaled_com& marshaled);
extern "C" void ProcessAsyncReader_t2663114743_marshal_com_back(const ProcessAsyncReader_t2663114743_marshaled_com& marshaled, ProcessAsyncReader_t2663114743& unmarshaled);
extern "C" void ProcessAsyncReader_t2663114743_marshal_com_cleanup(ProcessAsyncReader_t2663114743_marshaled_com& marshaled);
