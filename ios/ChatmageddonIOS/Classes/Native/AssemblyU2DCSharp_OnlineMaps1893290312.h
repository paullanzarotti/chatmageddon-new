﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3226471752;
// OnlineMaps/OnPrepareTooltipStyleHandler
struct OnPrepareTooltipStyleHandler_t626313099;
// System.Action`1<OnlineMapsTile>
struct Action_1_t4118096618;
// OnlineMaps
struct OnlineMaps_t1893290312;
// OnlineMapsControlBase
struct OnlineMapsControlBase_t473237564;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Collections.Generic.List`1<OnlineMapsDrawingElement>
struct List_1_t4203536082;
// OnlineMapsMarker[]
struct OnlineMapsMarkerU5BU5D_t3424771647;
// OnlineMapsPositionRange
struct OnlineMapsPositionRange_t2844425400;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// OnlineMapsDrawingElement
struct OnlineMapsDrawingElement_t539447654;
// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;
// OnlineMapsRange
struct OnlineMapsRange_t3791609909;
// OnlineMapsBuffer
struct OnlineMapsBuffer_t2643049474;
// System.Collections.Generic.List`1<OnlineMapsGoogleAPIQuery>
struct List_1_t4020097581;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;
// System.Threading.Thread
struct Thread_t241561612;
// OnlineMapsMarker
struct OnlineMapsMarker_t3492166682;
// System.Predicate`1<OnlineMapsGoogleAPIQuery>
struct Predicate_1_t3093946564;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Func`2<OnlineMapsTile,System.Boolean>
struct Func_2_t2493586475;
// System.Func`2<OnlineMapsTile,System.Int32>
struct Func_2_t739889205;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_OnlineMapsAlign3858887827.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_OnlineMapsProviderEnum1784547284.h"
#include "AssemblyU2DCSharp_OnlineMapsShowMarkerTooltip3336221582.h"
#include "AssemblyU2DCSharp_OnlineMapsSource4087519963.h"
#include "AssemblyU2DCSharp_OnlineMapsTarget2604759619.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsRedrawType1698729245.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMaps
struct  OnlineMaps_t1893290312  : public MonoBehaviour_t1158329972
{
public:
	// System.Action OnlineMaps::OnChangePosition
	Action_t3226471752 * ___OnChangePosition_4;
	// System.Action OnlineMaps::OnChangeZoom
	Action_t3226471752 * ___OnChangeZoom_5;
	// System.Action OnlineMaps::OnFindLocationAfter
	Action_t3226471752 * ___OnFindLocationAfter_6;
	// System.Action OnlineMaps::OnMapUpdated
	Action_t3226471752 * ___OnMapUpdated_7;
	// OnlineMaps/OnPrepareTooltipStyleHandler OnlineMaps::OnPrepareTooltipStyle
	OnPrepareTooltipStyleHandler_t626313099 * ___OnPrepareTooltipStyle_8;
	// System.Action`1<OnlineMapsTile> OnlineMaps::OnStartDownloadTile
	Action_1_t4118096618 * ___OnStartDownloadTile_9;
	// System.Action OnlineMaps::OnUpdateBefore
	Action_t3226471752 * ___OnUpdateBefore_10;
	// System.Action OnlineMaps::OnUpdateLate
	Action_t3226471752 * ___OnUpdateLate_11;
	// System.Boolean OnlineMaps::allowRedraw
	bool ___allowRedraw_14;
	// OnlineMapsControlBase OnlineMaps::control
	OnlineMapsControlBase_t473237564 * ___control_15;
	// System.String OnlineMaps::customProviderURL
	String_t* ___customProviderURL_16;
	// OnlineMapsAlign OnlineMaps::defaultMarkerAlign
	int32_t ___defaultMarkerAlign_17;
	// UnityEngine.Texture2D OnlineMaps::defaultMarkerTexture
	Texture2D_t3542995729 * ___defaultMarkerTexture_18;
	// UnityEngine.Texture2D OnlineMaps::defaultTileTexture
	Texture2D_t3542995729 * ___defaultTileTexture_19;
	// System.Boolean OnlineMaps::dispatchEvents
	bool ___dispatchEvents_20;
	// System.Collections.Generic.List`1<OnlineMapsDrawingElement> OnlineMaps::drawingElements
	List_1_t4203536082 * ___drawingElements_21;
	// UnityEngine.Color OnlineMaps::emptyColor
	Color_t2020392075  ___emptyColor_22;
	// System.Int32 OnlineMaps::height
	int32_t ___height_23;
	// System.Boolean OnlineMaps::labels
	bool ___labels_24;
	// System.String OnlineMaps::language
	String_t* ___language_25;
	// System.Boolean OnlineMaps::lockRedraw
	bool ___lockRedraw_26;
	// OnlineMapsMarker[] OnlineMaps::markers
	OnlineMapsMarkerU5BU5D_t3424771647* ___markers_27;
	// System.Boolean OnlineMaps::needGC
	bool ___needGC_28;
	// System.Boolean OnlineMaps::needRedraw
	bool ___needRedraw_29;
	// OnlineMapsPositionRange OnlineMaps::positionRange
	OnlineMapsPositionRange_t2844425400 * ___positionRange_30;
	// OnlineMapsProviderEnum OnlineMaps::provider
	int32_t ___provider_31;
	// System.Boolean OnlineMaps::redrawOnPlay
	bool ___redrawOnPlay_32;
	// System.Boolean OnlineMaps::renderInThread
	bool ___renderInThread_33;
	// System.String OnlineMaps::resourcesPath
	String_t* ___resourcesPath_34;
	// OnlineMapsShowMarkerTooltip OnlineMaps::showMarkerTooltip
	int32_t ___showMarkerTooltip_35;
	// UnityEngine.GUISkin OnlineMaps::skin
	GUISkin_t1436893342 * ___skin_36;
	// UnityEngine.Texture2D OnlineMaps::smartTexture
	Texture2D_t3542995729 * ___smartTexture_37;
	// OnlineMapsSource OnlineMaps::source
	int32_t ___source_38;
	// OnlineMapsTarget OnlineMaps::target
	int32_t ___target_39;
	// UnityEngine.Texture2D OnlineMaps::texture
	Texture2D_t3542995729 * ___texture_40;
	// System.Int32 OnlineMaps::tilesetWidth
	int32_t ___tilesetWidth_41;
	// System.Int32 OnlineMaps::tilesetHeight
	int32_t ___tilesetHeight_42;
	// UnityEngine.Vector2 OnlineMaps::tilesetSize
	Vector2_t2243707579  ___tilesetSize_43;
	// System.String OnlineMaps::tooltip
	String_t* ___tooltip_44;
	// OnlineMapsDrawingElement OnlineMaps::tooltipDrawingElement
	OnlineMapsDrawingElement_t539447654 * ___tooltipDrawingElement_45;
	// OnlineMapsMarkerBase OnlineMaps::tooltipMarker
	OnlineMapsMarkerBase_t3900955221 * ___tooltipMarker_46;
	// System.Boolean OnlineMaps::traffic
	bool ___traffic_47;
	// System.Int32 OnlineMaps::type
	int32_t ___type_48;
	// System.Boolean OnlineMaps::useSoftwareJPEGDecoder
	bool ___useSoftwareJPEGDecoder_49;
	// System.Boolean OnlineMaps::useSmartTexture
	bool ___useSmartTexture_50;
	// System.String OnlineMaps::webplayerProxyURL
	String_t* ___webplayerProxyURL_51;
	// System.Int32 OnlineMaps::width
	int32_t ___width_52;
	// OnlineMapsRange OnlineMaps::zoomRange
	OnlineMapsRange_t3791609909 * ___zoomRange_53;
	// System.Double OnlineMaps::latitude
	double ___latitude_54;
	// System.Double OnlineMaps::longitude
	double ___longitude_55;
	// UnityEngine.Vector2 OnlineMaps::_position
	Vector2_t2243707579  ____position_56;
	// System.Int32 OnlineMaps::_zoom
	int32_t ____zoom_57;
	// OnlineMapsBuffer OnlineMaps::_buffer
	OnlineMapsBuffer_t2643049474 * ____buffer_58;
	// System.Collections.Generic.List`1<OnlineMapsGoogleAPIQuery> OnlineMaps::_googleQueries
	List_1_t4020097581 * ____googleQueries_59;
	// System.Boolean OnlineMaps::_labels
	bool ____labels_60;
	// System.String OnlineMaps::_language
	String_t* ____language_61;
	// OnlineMapsProviderEnum OnlineMaps::_provider
	int32_t ____provider_62;
	// System.Boolean OnlineMaps::_traffic
	bool ____traffic_63;
	// System.Int32 OnlineMaps::_type
	int32_t ____type_64;
	// UnityEngine.Texture2D OnlineMaps::activeTexture
	Texture2D_t3542995729 * ___activeTexture_65;
	// System.Action`1<System.Boolean> OnlineMaps::checkConnectioCallback
	Action_1_t3627374100 * ___checkConnectioCallback_66;
	// UnityEngine.WWW OnlineMaps::checkConnectionWWW
	WWW_t2919945039 * ___checkConnectionWWW_67;
	// UnityEngine.Color[] OnlineMaps::defaultColors
	ColorU5BU5D_t672350442* ___defaultColors_68;
	// OnlineMapsTile OnlineMaps::downloads
	OnlineMapsTile_t21329940 * ___downloads_69;
	// System.Int64 OnlineMaps::lastGC
	int64_t ___lastGC_70;
	// OnlineMapsRedrawType OnlineMaps::redrawType
	int32_t ___redrawType_71;
	// System.Threading.Thread OnlineMaps::renderThread
	Thread_t241561612 * ___renderThread_72;
	// OnlineMapsMarker OnlineMaps::rolledMarker
	OnlineMapsMarker_t3492166682 * ___rolledMarker_73;
	// System.Double OnlineMaps::bottomRightLatitude
	double ___bottomRightLatitude_74;
	// System.Double OnlineMaps::bottomRightLongitude
	double ___bottomRightLongitude_75;
	// System.Double OnlineMaps::topLeftLatitude
	double ___topLeftLatitude_76;
	// System.Double OnlineMaps::topLeftLongitude
	double ___topLeftLongitude_77;

public:
	inline static int32_t get_offset_of_OnChangePosition_4() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnChangePosition_4)); }
	inline Action_t3226471752 * get_OnChangePosition_4() const { return ___OnChangePosition_4; }
	inline Action_t3226471752 ** get_address_of_OnChangePosition_4() { return &___OnChangePosition_4; }
	inline void set_OnChangePosition_4(Action_t3226471752 * value)
	{
		___OnChangePosition_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnChangePosition_4, value);
	}

	inline static int32_t get_offset_of_OnChangeZoom_5() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnChangeZoom_5)); }
	inline Action_t3226471752 * get_OnChangeZoom_5() const { return ___OnChangeZoom_5; }
	inline Action_t3226471752 ** get_address_of_OnChangeZoom_5() { return &___OnChangeZoom_5; }
	inline void set_OnChangeZoom_5(Action_t3226471752 * value)
	{
		___OnChangeZoom_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnChangeZoom_5, value);
	}

	inline static int32_t get_offset_of_OnFindLocationAfter_6() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnFindLocationAfter_6)); }
	inline Action_t3226471752 * get_OnFindLocationAfter_6() const { return ___OnFindLocationAfter_6; }
	inline Action_t3226471752 ** get_address_of_OnFindLocationAfter_6() { return &___OnFindLocationAfter_6; }
	inline void set_OnFindLocationAfter_6(Action_t3226471752 * value)
	{
		___OnFindLocationAfter_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnFindLocationAfter_6, value);
	}

	inline static int32_t get_offset_of_OnMapUpdated_7() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnMapUpdated_7)); }
	inline Action_t3226471752 * get_OnMapUpdated_7() const { return ___OnMapUpdated_7; }
	inline Action_t3226471752 ** get_address_of_OnMapUpdated_7() { return &___OnMapUpdated_7; }
	inline void set_OnMapUpdated_7(Action_t3226471752 * value)
	{
		___OnMapUpdated_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnMapUpdated_7, value);
	}

	inline static int32_t get_offset_of_OnPrepareTooltipStyle_8() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnPrepareTooltipStyle_8)); }
	inline OnPrepareTooltipStyleHandler_t626313099 * get_OnPrepareTooltipStyle_8() const { return ___OnPrepareTooltipStyle_8; }
	inline OnPrepareTooltipStyleHandler_t626313099 ** get_address_of_OnPrepareTooltipStyle_8() { return &___OnPrepareTooltipStyle_8; }
	inline void set_OnPrepareTooltipStyle_8(OnPrepareTooltipStyleHandler_t626313099 * value)
	{
		___OnPrepareTooltipStyle_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnPrepareTooltipStyle_8, value);
	}

	inline static int32_t get_offset_of_OnStartDownloadTile_9() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnStartDownloadTile_9)); }
	inline Action_1_t4118096618 * get_OnStartDownloadTile_9() const { return ___OnStartDownloadTile_9; }
	inline Action_1_t4118096618 ** get_address_of_OnStartDownloadTile_9() { return &___OnStartDownloadTile_9; }
	inline void set_OnStartDownloadTile_9(Action_1_t4118096618 * value)
	{
		___OnStartDownloadTile_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnStartDownloadTile_9, value);
	}

	inline static int32_t get_offset_of_OnUpdateBefore_10() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnUpdateBefore_10)); }
	inline Action_t3226471752 * get_OnUpdateBefore_10() const { return ___OnUpdateBefore_10; }
	inline Action_t3226471752 ** get_address_of_OnUpdateBefore_10() { return &___OnUpdateBefore_10; }
	inline void set_OnUpdateBefore_10(Action_t3226471752 * value)
	{
		___OnUpdateBefore_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnUpdateBefore_10, value);
	}

	inline static int32_t get_offset_of_OnUpdateLate_11() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___OnUpdateLate_11)); }
	inline Action_t3226471752 * get_OnUpdateLate_11() const { return ___OnUpdateLate_11; }
	inline Action_t3226471752 ** get_address_of_OnUpdateLate_11() { return &___OnUpdateLate_11; }
	inline void set_OnUpdateLate_11(Action_t3226471752 * value)
	{
		___OnUpdateLate_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnUpdateLate_11, value);
	}

	inline static int32_t get_offset_of_allowRedraw_14() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___allowRedraw_14)); }
	inline bool get_allowRedraw_14() const { return ___allowRedraw_14; }
	inline bool* get_address_of_allowRedraw_14() { return &___allowRedraw_14; }
	inline void set_allowRedraw_14(bool value)
	{
		___allowRedraw_14 = value;
	}

	inline static int32_t get_offset_of_control_15() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___control_15)); }
	inline OnlineMapsControlBase_t473237564 * get_control_15() const { return ___control_15; }
	inline OnlineMapsControlBase_t473237564 ** get_address_of_control_15() { return &___control_15; }
	inline void set_control_15(OnlineMapsControlBase_t473237564 * value)
	{
		___control_15 = value;
		Il2CppCodeGenWriteBarrier(&___control_15, value);
	}

	inline static int32_t get_offset_of_customProviderURL_16() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___customProviderURL_16)); }
	inline String_t* get_customProviderURL_16() const { return ___customProviderURL_16; }
	inline String_t** get_address_of_customProviderURL_16() { return &___customProviderURL_16; }
	inline void set_customProviderURL_16(String_t* value)
	{
		___customProviderURL_16 = value;
		Il2CppCodeGenWriteBarrier(&___customProviderURL_16, value);
	}

	inline static int32_t get_offset_of_defaultMarkerAlign_17() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___defaultMarkerAlign_17)); }
	inline int32_t get_defaultMarkerAlign_17() const { return ___defaultMarkerAlign_17; }
	inline int32_t* get_address_of_defaultMarkerAlign_17() { return &___defaultMarkerAlign_17; }
	inline void set_defaultMarkerAlign_17(int32_t value)
	{
		___defaultMarkerAlign_17 = value;
	}

	inline static int32_t get_offset_of_defaultMarkerTexture_18() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___defaultMarkerTexture_18)); }
	inline Texture2D_t3542995729 * get_defaultMarkerTexture_18() const { return ___defaultMarkerTexture_18; }
	inline Texture2D_t3542995729 ** get_address_of_defaultMarkerTexture_18() { return &___defaultMarkerTexture_18; }
	inline void set_defaultMarkerTexture_18(Texture2D_t3542995729 * value)
	{
		___defaultMarkerTexture_18 = value;
		Il2CppCodeGenWriteBarrier(&___defaultMarkerTexture_18, value);
	}

	inline static int32_t get_offset_of_defaultTileTexture_19() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___defaultTileTexture_19)); }
	inline Texture2D_t3542995729 * get_defaultTileTexture_19() const { return ___defaultTileTexture_19; }
	inline Texture2D_t3542995729 ** get_address_of_defaultTileTexture_19() { return &___defaultTileTexture_19; }
	inline void set_defaultTileTexture_19(Texture2D_t3542995729 * value)
	{
		___defaultTileTexture_19 = value;
		Il2CppCodeGenWriteBarrier(&___defaultTileTexture_19, value);
	}

	inline static int32_t get_offset_of_dispatchEvents_20() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___dispatchEvents_20)); }
	inline bool get_dispatchEvents_20() const { return ___dispatchEvents_20; }
	inline bool* get_address_of_dispatchEvents_20() { return &___dispatchEvents_20; }
	inline void set_dispatchEvents_20(bool value)
	{
		___dispatchEvents_20 = value;
	}

	inline static int32_t get_offset_of_drawingElements_21() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___drawingElements_21)); }
	inline List_1_t4203536082 * get_drawingElements_21() const { return ___drawingElements_21; }
	inline List_1_t4203536082 ** get_address_of_drawingElements_21() { return &___drawingElements_21; }
	inline void set_drawingElements_21(List_1_t4203536082 * value)
	{
		___drawingElements_21 = value;
		Il2CppCodeGenWriteBarrier(&___drawingElements_21, value);
	}

	inline static int32_t get_offset_of_emptyColor_22() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___emptyColor_22)); }
	inline Color_t2020392075  get_emptyColor_22() const { return ___emptyColor_22; }
	inline Color_t2020392075 * get_address_of_emptyColor_22() { return &___emptyColor_22; }
	inline void set_emptyColor_22(Color_t2020392075  value)
	{
		___emptyColor_22 = value;
	}

	inline static int32_t get_offset_of_height_23() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___height_23)); }
	inline int32_t get_height_23() const { return ___height_23; }
	inline int32_t* get_address_of_height_23() { return &___height_23; }
	inline void set_height_23(int32_t value)
	{
		___height_23 = value;
	}

	inline static int32_t get_offset_of_labels_24() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___labels_24)); }
	inline bool get_labels_24() const { return ___labels_24; }
	inline bool* get_address_of_labels_24() { return &___labels_24; }
	inline void set_labels_24(bool value)
	{
		___labels_24 = value;
	}

	inline static int32_t get_offset_of_language_25() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___language_25)); }
	inline String_t* get_language_25() const { return ___language_25; }
	inline String_t** get_address_of_language_25() { return &___language_25; }
	inline void set_language_25(String_t* value)
	{
		___language_25 = value;
		Il2CppCodeGenWriteBarrier(&___language_25, value);
	}

	inline static int32_t get_offset_of_lockRedraw_26() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___lockRedraw_26)); }
	inline bool get_lockRedraw_26() const { return ___lockRedraw_26; }
	inline bool* get_address_of_lockRedraw_26() { return &___lockRedraw_26; }
	inline void set_lockRedraw_26(bool value)
	{
		___lockRedraw_26 = value;
	}

	inline static int32_t get_offset_of_markers_27() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___markers_27)); }
	inline OnlineMapsMarkerU5BU5D_t3424771647* get_markers_27() const { return ___markers_27; }
	inline OnlineMapsMarkerU5BU5D_t3424771647** get_address_of_markers_27() { return &___markers_27; }
	inline void set_markers_27(OnlineMapsMarkerU5BU5D_t3424771647* value)
	{
		___markers_27 = value;
		Il2CppCodeGenWriteBarrier(&___markers_27, value);
	}

	inline static int32_t get_offset_of_needGC_28() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___needGC_28)); }
	inline bool get_needGC_28() const { return ___needGC_28; }
	inline bool* get_address_of_needGC_28() { return &___needGC_28; }
	inline void set_needGC_28(bool value)
	{
		___needGC_28 = value;
	}

	inline static int32_t get_offset_of_needRedraw_29() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___needRedraw_29)); }
	inline bool get_needRedraw_29() const { return ___needRedraw_29; }
	inline bool* get_address_of_needRedraw_29() { return &___needRedraw_29; }
	inline void set_needRedraw_29(bool value)
	{
		___needRedraw_29 = value;
	}

	inline static int32_t get_offset_of_positionRange_30() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___positionRange_30)); }
	inline OnlineMapsPositionRange_t2844425400 * get_positionRange_30() const { return ___positionRange_30; }
	inline OnlineMapsPositionRange_t2844425400 ** get_address_of_positionRange_30() { return &___positionRange_30; }
	inline void set_positionRange_30(OnlineMapsPositionRange_t2844425400 * value)
	{
		___positionRange_30 = value;
		Il2CppCodeGenWriteBarrier(&___positionRange_30, value);
	}

	inline static int32_t get_offset_of_provider_31() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___provider_31)); }
	inline int32_t get_provider_31() const { return ___provider_31; }
	inline int32_t* get_address_of_provider_31() { return &___provider_31; }
	inline void set_provider_31(int32_t value)
	{
		___provider_31 = value;
	}

	inline static int32_t get_offset_of_redrawOnPlay_32() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___redrawOnPlay_32)); }
	inline bool get_redrawOnPlay_32() const { return ___redrawOnPlay_32; }
	inline bool* get_address_of_redrawOnPlay_32() { return &___redrawOnPlay_32; }
	inline void set_redrawOnPlay_32(bool value)
	{
		___redrawOnPlay_32 = value;
	}

	inline static int32_t get_offset_of_renderInThread_33() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___renderInThread_33)); }
	inline bool get_renderInThread_33() const { return ___renderInThread_33; }
	inline bool* get_address_of_renderInThread_33() { return &___renderInThread_33; }
	inline void set_renderInThread_33(bool value)
	{
		___renderInThread_33 = value;
	}

	inline static int32_t get_offset_of_resourcesPath_34() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___resourcesPath_34)); }
	inline String_t* get_resourcesPath_34() const { return ___resourcesPath_34; }
	inline String_t** get_address_of_resourcesPath_34() { return &___resourcesPath_34; }
	inline void set_resourcesPath_34(String_t* value)
	{
		___resourcesPath_34 = value;
		Il2CppCodeGenWriteBarrier(&___resourcesPath_34, value);
	}

	inline static int32_t get_offset_of_showMarkerTooltip_35() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___showMarkerTooltip_35)); }
	inline int32_t get_showMarkerTooltip_35() const { return ___showMarkerTooltip_35; }
	inline int32_t* get_address_of_showMarkerTooltip_35() { return &___showMarkerTooltip_35; }
	inline void set_showMarkerTooltip_35(int32_t value)
	{
		___showMarkerTooltip_35 = value;
	}

	inline static int32_t get_offset_of_skin_36() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___skin_36)); }
	inline GUISkin_t1436893342 * get_skin_36() const { return ___skin_36; }
	inline GUISkin_t1436893342 ** get_address_of_skin_36() { return &___skin_36; }
	inline void set_skin_36(GUISkin_t1436893342 * value)
	{
		___skin_36 = value;
		Il2CppCodeGenWriteBarrier(&___skin_36, value);
	}

	inline static int32_t get_offset_of_smartTexture_37() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___smartTexture_37)); }
	inline Texture2D_t3542995729 * get_smartTexture_37() const { return ___smartTexture_37; }
	inline Texture2D_t3542995729 ** get_address_of_smartTexture_37() { return &___smartTexture_37; }
	inline void set_smartTexture_37(Texture2D_t3542995729 * value)
	{
		___smartTexture_37 = value;
		Il2CppCodeGenWriteBarrier(&___smartTexture_37, value);
	}

	inline static int32_t get_offset_of_source_38() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___source_38)); }
	inline int32_t get_source_38() const { return ___source_38; }
	inline int32_t* get_address_of_source_38() { return &___source_38; }
	inline void set_source_38(int32_t value)
	{
		___source_38 = value;
	}

	inline static int32_t get_offset_of_target_39() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___target_39)); }
	inline int32_t get_target_39() const { return ___target_39; }
	inline int32_t* get_address_of_target_39() { return &___target_39; }
	inline void set_target_39(int32_t value)
	{
		___target_39 = value;
	}

	inline static int32_t get_offset_of_texture_40() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___texture_40)); }
	inline Texture2D_t3542995729 * get_texture_40() const { return ___texture_40; }
	inline Texture2D_t3542995729 ** get_address_of_texture_40() { return &___texture_40; }
	inline void set_texture_40(Texture2D_t3542995729 * value)
	{
		___texture_40 = value;
		Il2CppCodeGenWriteBarrier(&___texture_40, value);
	}

	inline static int32_t get_offset_of_tilesetWidth_41() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___tilesetWidth_41)); }
	inline int32_t get_tilesetWidth_41() const { return ___tilesetWidth_41; }
	inline int32_t* get_address_of_tilesetWidth_41() { return &___tilesetWidth_41; }
	inline void set_tilesetWidth_41(int32_t value)
	{
		___tilesetWidth_41 = value;
	}

	inline static int32_t get_offset_of_tilesetHeight_42() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___tilesetHeight_42)); }
	inline int32_t get_tilesetHeight_42() const { return ___tilesetHeight_42; }
	inline int32_t* get_address_of_tilesetHeight_42() { return &___tilesetHeight_42; }
	inline void set_tilesetHeight_42(int32_t value)
	{
		___tilesetHeight_42 = value;
	}

	inline static int32_t get_offset_of_tilesetSize_43() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___tilesetSize_43)); }
	inline Vector2_t2243707579  get_tilesetSize_43() const { return ___tilesetSize_43; }
	inline Vector2_t2243707579 * get_address_of_tilesetSize_43() { return &___tilesetSize_43; }
	inline void set_tilesetSize_43(Vector2_t2243707579  value)
	{
		___tilesetSize_43 = value;
	}

	inline static int32_t get_offset_of_tooltip_44() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___tooltip_44)); }
	inline String_t* get_tooltip_44() const { return ___tooltip_44; }
	inline String_t** get_address_of_tooltip_44() { return &___tooltip_44; }
	inline void set_tooltip_44(String_t* value)
	{
		___tooltip_44 = value;
		Il2CppCodeGenWriteBarrier(&___tooltip_44, value);
	}

	inline static int32_t get_offset_of_tooltipDrawingElement_45() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___tooltipDrawingElement_45)); }
	inline OnlineMapsDrawingElement_t539447654 * get_tooltipDrawingElement_45() const { return ___tooltipDrawingElement_45; }
	inline OnlineMapsDrawingElement_t539447654 ** get_address_of_tooltipDrawingElement_45() { return &___tooltipDrawingElement_45; }
	inline void set_tooltipDrawingElement_45(OnlineMapsDrawingElement_t539447654 * value)
	{
		___tooltipDrawingElement_45 = value;
		Il2CppCodeGenWriteBarrier(&___tooltipDrawingElement_45, value);
	}

	inline static int32_t get_offset_of_tooltipMarker_46() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___tooltipMarker_46)); }
	inline OnlineMapsMarkerBase_t3900955221 * get_tooltipMarker_46() const { return ___tooltipMarker_46; }
	inline OnlineMapsMarkerBase_t3900955221 ** get_address_of_tooltipMarker_46() { return &___tooltipMarker_46; }
	inline void set_tooltipMarker_46(OnlineMapsMarkerBase_t3900955221 * value)
	{
		___tooltipMarker_46 = value;
		Il2CppCodeGenWriteBarrier(&___tooltipMarker_46, value);
	}

	inline static int32_t get_offset_of_traffic_47() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___traffic_47)); }
	inline bool get_traffic_47() const { return ___traffic_47; }
	inline bool* get_address_of_traffic_47() { return &___traffic_47; }
	inline void set_traffic_47(bool value)
	{
		___traffic_47 = value;
	}

	inline static int32_t get_offset_of_type_48() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___type_48)); }
	inline int32_t get_type_48() const { return ___type_48; }
	inline int32_t* get_address_of_type_48() { return &___type_48; }
	inline void set_type_48(int32_t value)
	{
		___type_48 = value;
	}

	inline static int32_t get_offset_of_useSoftwareJPEGDecoder_49() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___useSoftwareJPEGDecoder_49)); }
	inline bool get_useSoftwareJPEGDecoder_49() const { return ___useSoftwareJPEGDecoder_49; }
	inline bool* get_address_of_useSoftwareJPEGDecoder_49() { return &___useSoftwareJPEGDecoder_49; }
	inline void set_useSoftwareJPEGDecoder_49(bool value)
	{
		___useSoftwareJPEGDecoder_49 = value;
	}

	inline static int32_t get_offset_of_useSmartTexture_50() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___useSmartTexture_50)); }
	inline bool get_useSmartTexture_50() const { return ___useSmartTexture_50; }
	inline bool* get_address_of_useSmartTexture_50() { return &___useSmartTexture_50; }
	inline void set_useSmartTexture_50(bool value)
	{
		___useSmartTexture_50 = value;
	}

	inline static int32_t get_offset_of_webplayerProxyURL_51() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___webplayerProxyURL_51)); }
	inline String_t* get_webplayerProxyURL_51() const { return ___webplayerProxyURL_51; }
	inline String_t** get_address_of_webplayerProxyURL_51() { return &___webplayerProxyURL_51; }
	inline void set_webplayerProxyURL_51(String_t* value)
	{
		___webplayerProxyURL_51 = value;
		Il2CppCodeGenWriteBarrier(&___webplayerProxyURL_51, value);
	}

	inline static int32_t get_offset_of_width_52() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___width_52)); }
	inline int32_t get_width_52() const { return ___width_52; }
	inline int32_t* get_address_of_width_52() { return &___width_52; }
	inline void set_width_52(int32_t value)
	{
		___width_52 = value;
	}

	inline static int32_t get_offset_of_zoomRange_53() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___zoomRange_53)); }
	inline OnlineMapsRange_t3791609909 * get_zoomRange_53() const { return ___zoomRange_53; }
	inline OnlineMapsRange_t3791609909 ** get_address_of_zoomRange_53() { return &___zoomRange_53; }
	inline void set_zoomRange_53(OnlineMapsRange_t3791609909 * value)
	{
		___zoomRange_53 = value;
		Il2CppCodeGenWriteBarrier(&___zoomRange_53, value);
	}

	inline static int32_t get_offset_of_latitude_54() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___latitude_54)); }
	inline double get_latitude_54() const { return ___latitude_54; }
	inline double* get_address_of_latitude_54() { return &___latitude_54; }
	inline void set_latitude_54(double value)
	{
		___latitude_54 = value;
	}

	inline static int32_t get_offset_of_longitude_55() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___longitude_55)); }
	inline double get_longitude_55() const { return ___longitude_55; }
	inline double* get_address_of_longitude_55() { return &___longitude_55; }
	inline void set_longitude_55(double value)
	{
		___longitude_55 = value;
	}

	inline static int32_t get_offset_of__position_56() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____position_56)); }
	inline Vector2_t2243707579  get__position_56() const { return ____position_56; }
	inline Vector2_t2243707579 * get_address_of__position_56() { return &____position_56; }
	inline void set__position_56(Vector2_t2243707579  value)
	{
		____position_56 = value;
	}

	inline static int32_t get_offset_of__zoom_57() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____zoom_57)); }
	inline int32_t get__zoom_57() const { return ____zoom_57; }
	inline int32_t* get_address_of__zoom_57() { return &____zoom_57; }
	inline void set__zoom_57(int32_t value)
	{
		____zoom_57 = value;
	}

	inline static int32_t get_offset_of__buffer_58() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____buffer_58)); }
	inline OnlineMapsBuffer_t2643049474 * get__buffer_58() const { return ____buffer_58; }
	inline OnlineMapsBuffer_t2643049474 ** get_address_of__buffer_58() { return &____buffer_58; }
	inline void set__buffer_58(OnlineMapsBuffer_t2643049474 * value)
	{
		____buffer_58 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_58, value);
	}

	inline static int32_t get_offset_of__googleQueries_59() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____googleQueries_59)); }
	inline List_1_t4020097581 * get__googleQueries_59() const { return ____googleQueries_59; }
	inline List_1_t4020097581 ** get_address_of__googleQueries_59() { return &____googleQueries_59; }
	inline void set__googleQueries_59(List_1_t4020097581 * value)
	{
		____googleQueries_59 = value;
		Il2CppCodeGenWriteBarrier(&____googleQueries_59, value);
	}

	inline static int32_t get_offset_of__labels_60() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____labels_60)); }
	inline bool get__labels_60() const { return ____labels_60; }
	inline bool* get_address_of__labels_60() { return &____labels_60; }
	inline void set__labels_60(bool value)
	{
		____labels_60 = value;
	}

	inline static int32_t get_offset_of__language_61() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____language_61)); }
	inline String_t* get__language_61() const { return ____language_61; }
	inline String_t** get_address_of__language_61() { return &____language_61; }
	inline void set__language_61(String_t* value)
	{
		____language_61 = value;
		Il2CppCodeGenWriteBarrier(&____language_61, value);
	}

	inline static int32_t get_offset_of__provider_62() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____provider_62)); }
	inline int32_t get__provider_62() const { return ____provider_62; }
	inline int32_t* get_address_of__provider_62() { return &____provider_62; }
	inline void set__provider_62(int32_t value)
	{
		____provider_62 = value;
	}

	inline static int32_t get_offset_of__traffic_63() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____traffic_63)); }
	inline bool get__traffic_63() const { return ____traffic_63; }
	inline bool* get_address_of__traffic_63() { return &____traffic_63; }
	inline void set__traffic_63(bool value)
	{
		____traffic_63 = value;
	}

	inline static int32_t get_offset_of__type_64() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ____type_64)); }
	inline int32_t get__type_64() const { return ____type_64; }
	inline int32_t* get_address_of__type_64() { return &____type_64; }
	inline void set__type_64(int32_t value)
	{
		____type_64 = value;
	}

	inline static int32_t get_offset_of_activeTexture_65() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___activeTexture_65)); }
	inline Texture2D_t3542995729 * get_activeTexture_65() const { return ___activeTexture_65; }
	inline Texture2D_t3542995729 ** get_address_of_activeTexture_65() { return &___activeTexture_65; }
	inline void set_activeTexture_65(Texture2D_t3542995729 * value)
	{
		___activeTexture_65 = value;
		Il2CppCodeGenWriteBarrier(&___activeTexture_65, value);
	}

	inline static int32_t get_offset_of_checkConnectioCallback_66() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___checkConnectioCallback_66)); }
	inline Action_1_t3627374100 * get_checkConnectioCallback_66() const { return ___checkConnectioCallback_66; }
	inline Action_1_t3627374100 ** get_address_of_checkConnectioCallback_66() { return &___checkConnectioCallback_66; }
	inline void set_checkConnectioCallback_66(Action_1_t3627374100 * value)
	{
		___checkConnectioCallback_66 = value;
		Il2CppCodeGenWriteBarrier(&___checkConnectioCallback_66, value);
	}

	inline static int32_t get_offset_of_checkConnectionWWW_67() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___checkConnectionWWW_67)); }
	inline WWW_t2919945039 * get_checkConnectionWWW_67() const { return ___checkConnectionWWW_67; }
	inline WWW_t2919945039 ** get_address_of_checkConnectionWWW_67() { return &___checkConnectionWWW_67; }
	inline void set_checkConnectionWWW_67(WWW_t2919945039 * value)
	{
		___checkConnectionWWW_67 = value;
		Il2CppCodeGenWriteBarrier(&___checkConnectionWWW_67, value);
	}

	inline static int32_t get_offset_of_defaultColors_68() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___defaultColors_68)); }
	inline ColorU5BU5D_t672350442* get_defaultColors_68() const { return ___defaultColors_68; }
	inline ColorU5BU5D_t672350442** get_address_of_defaultColors_68() { return &___defaultColors_68; }
	inline void set_defaultColors_68(ColorU5BU5D_t672350442* value)
	{
		___defaultColors_68 = value;
		Il2CppCodeGenWriteBarrier(&___defaultColors_68, value);
	}

	inline static int32_t get_offset_of_downloads_69() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___downloads_69)); }
	inline OnlineMapsTile_t21329940 * get_downloads_69() const { return ___downloads_69; }
	inline OnlineMapsTile_t21329940 ** get_address_of_downloads_69() { return &___downloads_69; }
	inline void set_downloads_69(OnlineMapsTile_t21329940 * value)
	{
		___downloads_69 = value;
		Il2CppCodeGenWriteBarrier(&___downloads_69, value);
	}

	inline static int32_t get_offset_of_lastGC_70() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___lastGC_70)); }
	inline int64_t get_lastGC_70() const { return ___lastGC_70; }
	inline int64_t* get_address_of_lastGC_70() { return &___lastGC_70; }
	inline void set_lastGC_70(int64_t value)
	{
		___lastGC_70 = value;
	}

	inline static int32_t get_offset_of_redrawType_71() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___redrawType_71)); }
	inline int32_t get_redrawType_71() const { return ___redrawType_71; }
	inline int32_t* get_address_of_redrawType_71() { return &___redrawType_71; }
	inline void set_redrawType_71(int32_t value)
	{
		___redrawType_71 = value;
	}

	inline static int32_t get_offset_of_renderThread_72() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___renderThread_72)); }
	inline Thread_t241561612 * get_renderThread_72() const { return ___renderThread_72; }
	inline Thread_t241561612 ** get_address_of_renderThread_72() { return &___renderThread_72; }
	inline void set_renderThread_72(Thread_t241561612 * value)
	{
		___renderThread_72 = value;
		Il2CppCodeGenWriteBarrier(&___renderThread_72, value);
	}

	inline static int32_t get_offset_of_rolledMarker_73() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___rolledMarker_73)); }
	inline OnlineMapsMarker_t3492166682 * get_rolledMarker_73() const { return ___rolledMarker_73; }
	inline OnlineMapsMarker_t3492166682 ** get_address_of_rolledMarker_73() { return &___rolledMarker_73; }
	inline void set_rolledMarker_73(OnlineMapsMarker_t3492166682 * value)
	{
		___rolledMarker_73 = value;
		Il2CppCodeGenWriteBarrier(&___rolledMarker_73, value);
	}

	inline static int32_t get_offset_of_bottomRightLatitude_74() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___bottomRightLatitude_74)); }
	inline double get_bottomRightLatitude_74() const { return ___bottomRightLatitude_74; }
	inline double* get_address_of_bottomRightLatitude_74() { return &___bottomRightLatitude_74; }
	inline void set_bottomRightLatitude_74(double value)
	{
		___bottomRightLatitude_74 = value;
	}

	inline static int32_t get_offset_of_bottomRightLongitude_75() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___bottomRightLongitude_75)); }
	inline double get_bottomRightLongitude_75() const { return ___bottomRightLongitude_75; }
	inline double* get_address_of_bottomRightLongitude_75() { return &___bottomRightLongitude_75; }
	inline void set_bottomRightLongitude_75(double value)
	{
		___bottomRightLongitude_75 = value;
	}

	inline static int32_t get_offset_of_topLeftLatitude_76() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___topLeftLatitude_76)); }
	inline double get_topLeftLatitude_76() const { return ___topLeftLatitude_76; }
	inline double* get_address_of_topLeftLatitude_76() { return &___topLeftLatitude_76; }
	inline void set_topLeftLatitude_76(double value)
	{
		___topLeftLatitude_76 = value;
	}

	inline static int32_t get_offset_of_topLeftLongitude_77() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312, ___topLeftLongitude_77)); }
	inline double get_topLeftLongitude_77() const { return ___topLeftLongitude_77; }
	inline double* get_address_of_topLeftLongitude_77() { return &___topLeftLongitude_77; }
	inline void set_topLeftLongitude_77(double value)
	{
		___topLeftLongitude_77 = value;
	}
};

struct OnlineMaps_t1893290312_StaticFields
{
public:
	// System.Int32 OnlineMaps::maxTileDownloads
	int32_t ___maxTileDownloads_3;
	// System.Boolean OnlineMaps::isUserControl
	bool ___isUserControl_12;
	// OnlineMaps OnlineMaps::_instance
	OnlineMaps_t1893290312 * ____instance_13;
	// System.Predicate`1<OnlineMapsGoogleAPIQuery> OnlineMaps::<>f__am$cache0
	Predicate_1_t3093946564 * ___U3CU3Ef__amU24cache0_78;
	// System.Action`1<System.String> OnlineMaps::<>f__mg$cache0
	Action_1_t1831019615 * ___U3CU3Ef__mgU24cache0_79;
	// System.Func`2<OnlineMapsTile,System.Boolean> OnlineMaps::<>f__am$cache1
	Func_2_t2493586475 * ___U3CU3Ef__amU24cache1_80;
	// System.Func`2<OnlineMapsTile,System.Boolean> OnlineMaps::<>f__am$cache2
	Func_2_t2493586475 * ___U3CU3Ef__amU24cache2_81;
	// System.Func`2<OnlineMapsTile,System.Int32> OnlineMaps::<>f__am$cache3
	Func_2_t739889205 * ___U3CU3Ef__amU24cache3_82;

public:
	inline static int32_t get_offset_of_maxTileDownloads_3() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ___maxTileDownloads_3)); }
	inline int32_t get_maxTileDownloads_3() const { return ___maxTileDownloads_3; }
	inline int32_t* get_address_of_maxTileDownloads_3() { return &___maxTileDownloads_3; }
	inline void set_maxTileDownloads_3(int32_t value)
	{
		___maxTileDownloads_3 = value;
	}

	inline static int32_t get_offset_of_isUserControl_12() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ___isUserControl_12)); }
	inline bool get_isUserControl_12() const { return ___isUserControl_12; }
	inline bool* get_address_of_isUserControl_12() { return &___isUserControl_12; }
	inline void set_isUserControl_12(bool value)
	{
		___isUserControl_12 = value;
	}

	inline static int32_t get_offset_of__instance_13() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ____instance_13)); }
	inline OnlineMaps_t1893290312 * get__instance_13() const { return ____instance_13; }
	inline OnlineMaps_t1893290312 ** get_address_of__instance_13() { return &____instance_13; }
	inline void set__instance_13(OnlineMaps_t1893290312 * value)
	{
		____instance_13 = value;
		Il2CppCodeGenWriteBarrier(&____instance_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_78() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ___U3CU3Ef__amU24cache0_78)); }
	inline Predicate_1_t3093946564 * get_U3CU3Ef__amU24cache0_78() const { return ___U3CU3Ef__amU24cache0_78; }
	inline Predicate_1_t3093946564 ** get_address_of_U3CU3Ef__amU24cache0_78() { return &___U3CU3Ef__amU24cache0_78; }
	inline void set_U3CU3Ef__amU24cache0_78(Predicate_1_t3093946564 * value)
	{
		___U3CU3Ef__amU24cache0_78 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_78, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_79() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ___U3CU3Ef__mgU24cache0_79)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__mgU24cache0_79() const { return ___U3CU3Ef__mgU24cache0_79; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__mgU24cache0_79() { return &___U3CU3Ef__mgU24cache0_79; }
	inline void set_U3CU3Ef__mgU24cache0_79(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__mgU24cache0_79 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_79, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_80() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ___U3CU3Ef__amU24cache1_80)); }
	inline Func_2_t2493586475 * get_U3CU3Ef__amU24cache1_80() const { return ___U3CU3Ef__amU24cache1_80; }
	inline Func_2_t2493586475 ** get_address_of_U3CU3Ef__amU24cache1_80() { return &___U3CU3Ef__amU24cache1_80; }
	inline void set_U3CU3Ef__amU24cache1_80(Func_2_t2493586475 * value)
	{
		___U3CU3Ef__amU24cache1_80 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_80, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_81() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ___U3CU3Ef__amU24cache2_81)); }
	inline Func_2_t2493586475 * get_U3CU3Ef__amU24cache2_81() const { return ___U3CU3Ef__amU24cache2_81; }
	inline Func_2_t2493586475 ** get_address_of_U3CU3Ef__amU24cache2_81() { return &___U3CU3Ef__amU24cache2_81; }
	inline void set_U3CU3Ef__amU24cache2_81(Func_2_t2493586475 * value)
	{
		___U3CU3Ef__amU24cache2_81 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_81, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_82() { return static_cast<int32_t>(offsetof(OnlineMaps_t1893290312_StaticFields, ___U3CU3Ef__amU24cache3_82)); }
	inline Func_2_t739889205 * get_U3CU3Ef__amU24cache3_82() const { return ___U3CU3Ef__amU24cache3_82; }
	inline Func_2_t739889205 ** get_address_of_U3CU3Ef__amU24cache3_82() { return &___U3CU3Ef__amU24cache3_82; }
	inline void set_U3CU3Ef__amU24cache3_82(Func_2_t739889205 * value)
	{
		___U3CU3Ef__amU24cache3_82 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_82, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
