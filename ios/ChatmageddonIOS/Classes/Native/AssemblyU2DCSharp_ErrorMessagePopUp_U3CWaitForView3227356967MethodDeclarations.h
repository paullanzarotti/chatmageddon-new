﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessagePopUp/<WaitForViewTime>c__Iterator0
struct U3CWaitForViewTimeU3Ec__Iterator0_t3227356967;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ErrorMessagePopUp/<WaitForViewTime>c__Iterator0::.ctor()
extern "C"  void U3CWaitForViewTimeU3Ec__Iterator0__ctor_m2108187146 (U3CWaitForViewTimeU3Ec__Iterator0_t3227356967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ErrorMessagePopUp/<WaitForViewTime>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitForViewTimeU3Ec__Iterator0_MoveNext_m1366708974 (U3CWaitForViewTimeU3Ec__Iterator0_t3227356967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ErrorMessagePopUp/<WaitForViewTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForViewTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3177037272 (U3CWaitForViewTimeU3Ec__Iterator0_t3227356967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ErrorMessagePopUp/<WaitForViewTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForViewTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2214322752 (U3CWaitForViewTimeU3Ec__Iterator0_t3227356967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessagePopUp/<WaitForViewTime>c__Iterator0::Dispose()
extern "C"  void U3CWaitForViewTimeU3Ec__Iterator0_Dispose_m4016302569 (U3CWaitForViewTimeU3Ec__Iterator0_t3227356967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessagePopUp/<WaitForViewTime>c__Iterator0::Reset()
extern "C"  void U3CWaitForViewTimeU3Ec__Iterator0_Reset_m79340743 (U3CWaitForViewTimeU3Ec__Iterator0_t3227356967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
