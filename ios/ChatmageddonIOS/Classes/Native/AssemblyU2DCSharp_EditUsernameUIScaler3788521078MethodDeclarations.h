﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditUsernameUIScaler
struct EditUsernameUIScaler_t3788521078;

#include "codegen/il2cpp-codegen.h"

// System.Void EditUsernameUIScaler::.ctor()
extern "C"  void EditUsernameUIScaler__ctor_m1088576979 (EditUsernameUIScaler_t3788521078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditUsernameUIScaler::GlobalUIScale()
extern "C"  void EditUsernameUIScaler_GlobalUIScale_m2529355048 (EditUsernameUIScaler_t3788521078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
