﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineSelectionSwipe
struct MineSelectionSwipe_t3861158053;

#include "codegen/il2cpp-codegen.h"

// System.Void MineSelectionSwipe::.ctor()
extern "C"  void MineSelectionSwipe__ctor_m563259374 (MineSelectionSwipe_t3861158053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineSelectionSwipe::OnSwipeLeft()
extern "C"  void MineSelectionSwipe_OnSwipeLeft_m2029959374 (MineSelectionSwipe_t3861158053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineSelectionSwipe::OnSwipeRight()
extern "C"  void MineSelectionSwipe_OnSwipeRight_m3993438381 (MineSelectionSwipe_t3861158053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineSelectionSwipe::OnStaticClick()
extern "C"  void MineSelectionSwipe_OnStaticClick_m1342641471 (MineSelectionSwipe_t3861158053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
