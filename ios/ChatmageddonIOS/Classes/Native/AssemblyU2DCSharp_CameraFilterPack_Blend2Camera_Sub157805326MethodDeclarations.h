﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Subtract
struct CameraFilterPack_Blend2Camera_Subtract_t157805326;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Subtract::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Subtract__ctor_m4096175997 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Subtract::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Subtract_get_material_m377248100 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Subtract_Start_m551517773 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Subtract_OnRenderImage_m422868541 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Subtract_OnValidate_m4056326682 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Subtract_Update_m3921758572 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Subtract_OnEnable_m1618007629 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Subtract_OnDisable_m2038010962 (CameraFilterPack_Blend2Camera_Subtract_t157805326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
