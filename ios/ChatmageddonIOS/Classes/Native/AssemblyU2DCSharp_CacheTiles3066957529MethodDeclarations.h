﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CacheTiles
struct CacheTiles_t3066957529;
// System.String
struct String_t;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"

// System.Void CacheTiles::.ctor()
extern "C"  void CacheTiles__ctor_m3094216174 (CacheTiles_t3066957529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CacheTiles::GetTilePath(OnlineMapsTile)
extern "C"  String_t* CacheTiles_GetTilePath_m2231270130 (Il2CppObject * __this /* static, unused */, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CacheTiles::OnStartDownloadTile(OnlineMapsTile)
extern "C"  void CacheTiles_OnStartDownloadTile_m1561670625 (CacheTiles_t3066957529 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CacheTiles::OnTileDownloaded(OnlineMapsTile)
extern "C"  void CacheTiles_OnTileDownloaded_m3233648600 (CacheTiles_t3066957529 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CacheTiles::Start()
extern "C"  void CacheTiles_Start_m3065840550 (CacheTiles_t3066957529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
