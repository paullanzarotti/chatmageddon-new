﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.SamsungAppsBillingService
struct SamsungAppsBillingService_t2236131154;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.IRawSamsungAppsBillingService
struct IRawSamsungAppsBillingService_t1919646871;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.SamsungAppsBillingService::.ctor(Unibill.Impl.UnibillConfiguration,Unibill.Impl.ProductIdRemapper,Unibill.Impl.IRawSamsungAppsBillingService,Uniject.ILogger)
extern "C"  void SamsungAppsBillingService__ctor_m1475887672 (SamsungAppsBillingService_t2236131154 * __this, UnibillConfiguration_t2915611853 * ___config0, ProductIdRemapper_t3313438456 * ___remapper1, Il2CppObject * ___rawSamsung2, Il2CppObject * ___logger3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::initialise(Unibill.Impl.IBillingServiceCallback)
extern "C"  void SamsungAppsBillingService_initialise_m3583745916 (SamsungAppsBillingService_t2236131154 * __this, Il2CppObject * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::purchase(System.String,System.String)
extern "C"  void SamsungAppsBillingService_purchase_m1877193995 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::restoreTransactions()
extern "C"  void SamsungAppsBillingService_restoreTransactions_m3509680023 (SamsungAppsBillingService_t2236131154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::onProductListReceived(System.String)
extern "C"  void SamsungAppsBillingService_onProductListReceived_m2354923297 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___productListString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::onPurchaseFailed(System.String)
extern "C"  void SamsungAppsBillingService_onPurchaseFailed_m1146626387 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::onPurchaseCancelled(System.String)
extern "C"  void SamsungAppsBillingService_onPurchaseCancelled_m2045751639 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::onPurchaseSucceeded(System.String)
extern "C"  void SamsungAppsBillingService_onPurchaseSucceeded_m4143906291 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::onTransactionsRestored(System.String)
extern "C"  void SamsungAppsBillingService_onTransactionsRestored_m2885657680 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.SamsungAppsBillingService::onInitFail()
extern "C"  void SamsungAppsBillingService_onInitFail_m1343605081 (SamsungAppsBillingService_t2236131154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.SamsungAppsBillingService::hasReceipt(System.String)
extern "C"  bool SamsungAppsBillingService_hasReceipt_m2984640900 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.SamsungAppsBillingService::getReceipt(System.String)
extern "C"  String_t* SamsungAppsBillingService_getReceipt_m1315735509 (SamsungAppsBillingService_t2236131154 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
