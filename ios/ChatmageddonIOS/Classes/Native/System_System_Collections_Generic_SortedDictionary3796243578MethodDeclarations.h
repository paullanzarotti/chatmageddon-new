﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t3931121312;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3796243578.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m706482839_gshared (Enumerator_t3796243578 * __this, SortedDictionary_2_t3931121312 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m706482839(__this, ___dic0, method) ((  void (*) (Enumerator_t3796243578 *, SortedDictionary_2_t3931121312 *, const MethodInfo*))Enumerator__ctor_m706482839_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3764739033_gshared (Enumerator_t3796243578 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3764739033(__this, method) ((  Il2CppObject * (*) (Enumerator_t3796243578 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3764739033_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3322935985_gshared (Enumerator_t3796243578 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3322935985(__this, method) ((  void (*) (Enumerator_t3796243578 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3322935985_gshared)(__this, method)
// TKey System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2739389707_gshared (Enumerator_t3796243578 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2739389707(__this, method) ((  Il2CppObject * (*) (Enumerator_t3796243578 *, const MethodInfo*))Enumerator_get_Current_m2739389707_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3593332149_gshared (Enumerator_t3796243578 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3593332149(__this, method) ((  bool (*) (Enumerator_t3796243578 *, const MethodInfo*))Enumerator_MoveNext_m3593332149_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3562924790_gshared (Enumerator_t3796243578 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3562924790(__this, method) ((  void (*) (Enumerator_t3796243578 *, const MethodInfo*))Enumerator_Dispose_m3562924790_gshared)(__this, method)
