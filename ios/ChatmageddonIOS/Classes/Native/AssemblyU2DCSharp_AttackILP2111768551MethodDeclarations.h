﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackILP
struct AttackILP_t2111768551;
// AttackListItem
struct AttackListItem_t3405986821;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void AttackILP::.ctor()
extern "C"  void AttackILP__ctor_m4229293986 (AttackILP_t2111768551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackILP::StartIL()
extern "C"  void AttackILP_StartIL_m3805431611 (AttackILP_t2111768551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackILP::DrawIL()
extern "C"  void AttackILP_DrawIL_m1161241695 (AttackILP_t2111768551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackILP::SetDataArray()
extern "C"  void AttackILP_SetDataArray_m3587843253 (AttackILP_t2111768551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackILP::UpdateListData()
extern "C"  void AttackILP_UpdateListData_m4277587681 (AttackILP_t2111768551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AttackListItem AttackILP::GetItemByUserID(System.String)
extern "C"  AttackListItem_t3405986821 * AttackILP_GetItemByUserID_m1879860886 (AttackILP_t2111768551 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void AttackILP_InitListItemWithIndex_m2810271992 (AttackILP_t2111768551 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void AttackILP_PopulateListItemWithIndex_m559489653 (AttackILP_t2111768551 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
