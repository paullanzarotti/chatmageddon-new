﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Color_Sepia
struct CameraFilterPack_Color_Sepia_t2116766517;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Color_Sepia::.ctor()
extern "C"  void CameraFilterPack_Color_Sepia__ctor_m1315198688 (CameraFilterPack_Color_Sepia_t2116766517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Sepia::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Color_Sepia_get_material_m3447217189 (CameraFilterPack_Color_Sepia_t2116766517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::Start()
extern "C"  void CameraFilterPack_Color_Sepia_Start_m3443822048 (CameraFilterPack_Color_Sepia_t2116766517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Color_Sepia_OnRenderImage_m793194264 (CameraFilterPack_Color_Sepia_t2116766517 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::Update()
extern "C"  void CameraFilterPack_Color_Sepia_Update_m195413505 (CameraFilterPack_Color_Sepia_t2116766517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::OnDisable()
extern "C"  void CameraFilterPack_Color_Sepia_OnDisable_m2396904597 (CameraFilterPack_Color_Sepia_t2116766517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
