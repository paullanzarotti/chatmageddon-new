﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsUnblockButton
struct SettingsUnblockButton_t3175045615;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SettingsUnblockButton::.ctor()
extern "C"  void SettingsUnblockButton__ctor_m3668520806 (SettingsUnblockButton_t3175045615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsUnblockButton::OnButtonClick()
extern "C"  void SettingsUnblockButton_OnButtonClick_m3878168423 (SettingsUnblockButton_t3175045615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsUnblockButton::<OnButtonClick>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void SettingsUnblockButton_U3COnButtonClickU3Em__0_m1724653945 (SettingsUnblockButton_t3175045615 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
