﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Shield
struct Shield_t3327121081;
// ShieldManager
struct ShieldManager_t3551654236;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldManager/<TrackShield>c__Iterator0
struct  U3CTrackShieldU3Ec__Iterator0_t85697528  : public Il2CppObject
{
public:
	// Shield ShieldManager/<TrackShield>c__Iterator0::shield
	Shield_t3327121081 * ___shield_0;
	// System.Double ShieldManager/<TrackShield>c__Iterator0::<totalShieldTime>__0
	double ___U3CtotalShieldTimeU3E__0_1;
	// System.TimeSpan ShieldManager/<TrackShield>c__Iterator0::<difference>__1
	TimeSpan_t3430258949  ___U3CdifferenceU3E__1_2;
	// System.Double ShieldManager/<TrackShield>c__Iterator0::<percentage>__2
	double ___U3CpercentageU3E__2_3;
	// ShieldManager ShieldManager/<TrackShield>c__Iterator0::$this
	ShieldManager_t3551654236 * ___U24this_4;
	// System.Object ShieldManager/<TrackShield>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean ShieldManager/<TrackShield>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 ShieldManager/<TrackShield>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_shield_0() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___shield_0)); }
	inline Shield_t3327121081 * get_shield_0() const { return ___shield_0; }
	inline Shield_t3327121081 ** get_address_of_shield_0() { return &___shield_0; }
	inline void set_shield_0(Shield_t3327121081 * value)
	{
		___shield_0 = value;
		Il2CppCodeGenWriteBarrier(&___shield_0, value);
	}

	inline static int32_t get_offset_of_U3CtotalShieldTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___U3CtotalShieldTimeU3E__0_1)); }
	inline double get_U3CtotalShieldTimeU3E__0_1() const { return ___U3CtotalShieldTimeU3E__0_1; }
	inline double* get_address_of_U3CtotalShieldTimeU3E__0_1() { return &___U3CtotalShieldTimeU3E__0_1; }
	inline void set_U3CtotalShieldTimeU3E__0_1(double value)
	{
		___U3CtotalShieldTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CdifferenceU3E__1_2() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___U3CdifferenceU3E__1_2)); }
	inline TimeSpan_t3430258949  get_U3CdifferenceU3E__1_2() const { return ___U3CdifferenceU3E__1_2; }
	inline TimeSpan_t3430258949 * get_address_of_U3CdifferenceU3E__1_2() { return &___U3CdifferenceU3E__1_2; }
	inline void set_U3CdifferenceU3E__1_2(TimeSpan_t3430258949  value)
	{
		___U3CdifferenceU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageU3E__2_3() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___U3CpercentageU3E__2_3)); }
	inline double get_U3CpercentageU3E__2_3() const { return ___U3CpercentageU3E__2_3; }
	inline double* get_address_of_U3CpercentageU3E__2_3() { return &___U3CpercentageU3E__2_3; }
	inline void set_U3CpercentageU3E__2_3(double value)
	{
		___U3CpercentageU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___U24this_4)); }
	inline ShieldManager_t3551654236 * get_U24this_4() const { return ___U24this_4; }
	inline ShieldManager_t3551654236 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ShieldManager_t3551654236 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CTrackShieldU3Ec__Iterator0_t85697528, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
