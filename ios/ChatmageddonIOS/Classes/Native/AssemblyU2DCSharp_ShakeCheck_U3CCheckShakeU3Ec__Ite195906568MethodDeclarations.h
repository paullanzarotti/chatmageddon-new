﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShakeCheck/<CheckShake>c__Iterator0
struct U3CCheckShakeU3Ec__Iterator0_t195906568;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ShakeCheck/<CheckShake>c__Iterator0::.ctor()
extern "C"  void U3CCheckShakeU3Ec__Iterator0__ctor_m1310186179 (U3CCheckShakeU3Ec__Iterator0_t195906568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShakeCheck/<CheckShake>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckShakeU3Ec__Iterator0_MoveNext_m1189537905 (U3CCheckShakeU3Ec__Iterator0_t195906568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShakeCheck/<CheckShake>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckShakeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3683344185 (U3CCheckShakeU3Ec__Iterator0_t195906568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShakeCheck/<CheckShake>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckShakeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1834032161 (U3CCheckShakeU3Ec__Iterator0_t195906568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCheck/<CheckShake>c__Iterator0::Dispose()
extern "C"  void U3CCheckShakeU3Ec__Iterator0_Dispose_m1930247794 (U3CCheckShakeU3Ec__Iterator0_t195906568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShakeCheck/<CheckShake>c__Iterator0::Reset()
extern "C"  void U3CCheckShakeU3Ec__Iterator0_Reset_m3611447408 (U3CCheckShakeU3Ec__Iterator0_t195906568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
