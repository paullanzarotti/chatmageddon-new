﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile_OnGetResourcesPat2869117133.h"
#include "AssemblyU2DCSharp_OnlineMapsUtils2843619045.h"
#include "AssemblyU2DCSharp_OnlineMapsVector2i2180897250.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingBase650727021.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingBase_OnlineMap1421320360.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingMetaInfo1480818351.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingBuiltIn544190665.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingBuiltIn_U3CCre3978292547.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingBuiltIn_U3CCre2176372452.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingBuiltIn_U3CStr2863735018.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingMaterial302108695.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildings3411229051.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildings_U3COnBuildin1455629980.h"
#include "AssemblyU2DCSharp_OnlineMapsBuildingsNodeData1471864431.h"
#include "AssemblyU2DCSharp_OnlineMapsDirectionStep2252483185.h"
#include "AssemblyU2DCSharp_OnlineMapsJPEGDecoder872605750.h"
#include "AssemblyU2DCSharp_OnlineMapsJPEGDecoder_Code3472511518.h"
#include "AssemblyU2DCSharp_OnlineMapsJPEGDecoder_Component3519836246.h"
#include "AssemblyU2DCSharp_OnlineMapsJPEGDecoder_Context2423182662.h"
#include "AssemblyU2DCSharp_OnlineMapsJPEGDecoder_JPEGExcept3449532446.h"
#include "AssemblyU2DCSharp_OnlineMapsJPEGDecoder_JPEGResult4270095944.h"
#include "AssemblyU2DCSharp_OnlineMapsLocationService21331090.h"
#include "AssemblyU2DCSharp_OnlineMapsRWTConnector1023776356.h"
#include "AssemblyU2DCSharp_OnlineMapsRWTConnectorMode2301498079.h"
#include "AssemblyU2DCSharp_OnlineMapsRWTConnectorPositionMo1024163612.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"
#include "AssemblyU2DCSharp_OnlineMapsXMLEnum1817127220.h"
#include "AssemblyU2DCSharp_OnlineMapsXMLList1635558505.h"
#include "AssemblyU2DCSharp_OnlineMapsXMLListEnum1723955324.h"
#include "AssemblyU2DCSharp_LanguageSelection3655407812.h"
#include "AssemblyU2DCSharp_TypewriterEffect1547188070.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry3041229383.h"
#include "AssemblyU2DCSharp_UIButton3377238306.h"
#include "AssemblyU2DCSharp_UIButtonActivate791039529.h"
#include "AssemblyU2DCSharp_UIButtonColor3793385709.h"
#include "AssemblyU2DCSharp_UIButtonColor_State1377781065.h"
#include "AssemblyU2DCSharp_UIButtonKeys2099118702.h"
#include "AssemblyU2DCSharp_UIButtonMessage3791537815.h"
#include "AssemblyU2DCSharp_UIButtonMessage_Trigger610760934.h"
#include "AssemblyU2DCSharp_UIButtonOffset1501810361.h"
#include "AssemblyU2DCSharp_UIButtonRotation491694416.h"
#include "AssemblyU2DCSharp_UIButtonScale627477860.h"
#include "AssemblyU2DCSharp_UICenterOnChild1687745660.h"
#include "AssemblyU2DCSharp_UICenterOnChild_OnCenterCallback378748890.h"
#include "AssemblyU2DCSharp_UICenterOnClick1492880774.h"
#include "AssemblyU2DCSharp_UIDragCamera1363661919.h"
#include "AssemblyU2DCSharp_UIDragDropContainer4036588756.h"
#include "AssemblyU2DCSharp_UIDragDropItem4109477862.h"
#include "AssemblyU2DCSharp_UIDragDropItem_Restriction3942216849.h"
#include "AssemblyU2DCSharp_UIDragDropItem_U3CEnableDragScro3584381351.h"
#include "AssemblyU2DCSharp_UIDragDropRoot997512525.h"
#include "AssemblyU2DCSharp_UIDragObject1520449903.h"
#include "AssemblyU2DCSharp_UIDragObject_DragEffect533639763.h"
#include "AssemblyU2DCSharp_UIDragResize3448881960.h"
#include "AssemblyU2DCSharp_UIDragScrollView2942595320.h"
#include "AssemblyU2DCSharp_UIDraggableCamera2562792962.h"
#include "AssemblyU2DCSharp_UIEventTrigger1351778058.h"
#include "AssemblyU2DCSharp_UIForwardEvents1807908878.h"
#include "AssemblyU2DCSharp_UIGrid2420180906.h"
#include "AssemblyU2DCSharp_UIGrid_OnReposition581118304.h"
#include "AssemblyU2DCSharp_UIGrid_Arrangement177822543.h"
#include "AssemblyU2DCSharp_UIGrid_Sorting2413224087.h"
#include "AssemblyU2DCSharp_UIImageButton3909632983.h"
#include "AssemblyU2DCSharp_UIKeyBinding790450850.h"
#include "AssemblyU2DCSharp_UIKeyBinding_Action2562301673.h"
#include "AssemblyU2DCSharp_UIKeyBinding_Modifier1318739074.h"
#include "AssemblyU2DCSharp_UIKeyNavigation1158973079.h"
#include "AssemblyU2DCSharp_UIKeyNavigation_Constraint1628080441.h"
#include "AssemblyU2DCSharp_UIPlayAnimation3152825278.h"
#include "AssemblyU2DCSharp_UIPlaySound2984775557.h"
#include "AssemblyU2DCSharp_UIPlaySound_Trigger1926488856.h"
#include "AssemblyU2DCSharp_UIPlayTween2614996159.h"
#include "AssemblyU2DCSharp_UIPopupList109953940.h"
#include "AssemblyU2DCSharp_UIPopupList_Position1780870098.h"
#include "AssemblyU2DCSharp_UIPopupList_OpenOn3711353968.h"
#include "AssemblyU2DCSharp_UIPopupList_LegacyEvent3991167770.h"
#include "AssemblyU2DCSharp_UIPopupList_U3CUpdateTweenPositi3769473947.h"
#include "AssemblyU2DCSharp_UIPopupList_U3CCloseIfUnselected1592075018.h"
#include "AssemblyU2DCSharp_UIProgressBar3824507790.h"
#include "AssemblyU2DCSharp_UIProgressBar_FillDirection1824020367.h"
#include "AssemblyU2DCSharp_UIProgressBar_OnDragFinished2651612482.h"
#include "AssemblyU2DCSharp_UISavedOption3426088284.h"
#include "AssemblyU2DCSharp_UIScrollBar1736046648.h"
#include "AssemblyU2DCSharp_UIScrollBar_Direction3728253650.h"
#include "AssemblyU2DCSharp_UIScrollView3033954930.h"
#include "AssemblyU2DCSharp_UIScrollView_Movement1682624982.h"
#include "AssemblyU2DCSharp_UIScrollView_DragEffect614031488.h"
#include "AssemblyU2DCSharp_UIScrollView_ShowCondition3179581401.h"
#include "AssemblyU2DCSharp_UIScrollView_OnDragNotification685967913.h"
#include "AssemblyU2DCSharp_UIShowControlScheme4140061467.h"
#include "AssemblyU2DCSharp_UISlider2191058247.h"
#include "AssemblyU2DCSharp_UISlider_Direction3287923381.h"
#include "AssemblyU2DCSharp_UISoundVolume1670191865.h"
#include "AssemblyU2DCSharp_UITable3717403602.h"
#include "AssemblyU2DCSharp_UITable_OnReposition1194954916.h"
#include "AssemblyU2DCSharp_UITable_Direction1281150584.h"
#include "AssemblyU2DCSharp_UITable_Sorting3318157469.h"
#include "AssemblyU2DCSharp_UIToggle3036740318.h"
#include "AssemblyU2DCSharp_UIToggle_Validate558489517.h"
#include "AssemblyU2DCSharp_UIToggledComponents1876468998.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5000 = { sizeof (OnGetResourcesPathDelegate_t2869117133), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5001 = { sizeof (OnlineMapsUtils_t2843619045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5001[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5002 = { sizeof (OnlineMapsVector2i_t2180897250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5002[2] = 
{
	OnlineMapsVector2i_t2180897250::get_offset_of_x_0(),
	OnlineMapsVector2i_t2180897250::get_offset_of_y_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5003 = { sizeof (OnlineMapsBuildingBase_t650727021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5003[15] = 
{
	OnlineMapsBuildingBase_t650727021::get_offset_of_OnClick_2(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_OnDispose_3(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_OnPress_4(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_OnRelease_5(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_boundsCoords_6(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_centerCoordinates_7(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_id_8(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_initialZoom_9(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_metaInfo_10(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_perimeter_11(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_lastTouchCount_12(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_buildingCollider_13(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_hasErrors_14(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_isPressed_15(),
	OnlineMapsBuildingBase_t650727021::get_offset_of_pressPoint_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5004 = { sizeof (OnlineMapsBuildingRoofType_t1421320360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5004[3] = 
{
	OnlineMapsBuildingRoofType_t1421320360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5005 = { sizeof (OnlineMapsBuildingMetaInfo_t1480818351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5005[2] = 
{
	OnlineMapsBuildingMetaInfo_t1480818351::get_offset_of_info_0(),
	OnlineMapsBuildingMetaInfo_t1480818351::get_offset_of_title_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5006 = { sizeof (OnlineMapsBuildingBuiltIn_t544190665), -1, sizeof(OnlineMapsBuildingBuiltIn_t544190665_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5006[5] = 
{
	OnlineMapsBuildingBuiltIn_t544190665::get_offset_of_way_17(),
	OnlineMapsBuildingBuiltIn_t544190665_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_18(),
	OnlineMapsBuildingBuiltIn_t544190665_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_19(),
	OnlineMapsBuildingBuiltIn_t544190665_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_20(),
	OnlineMapsBuildingBuiltIn_t544190665_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5007 = { sizeof (U3CCreateU3Ec__AnonStorey0_t3978292547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5007[1] = 
{
	U3CCreateU3Ec__AnonStorey0_t3978292547::get_offset_of_centerPoint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5008 = { sizeof (U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5008[4] = 
{
	U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452::get_offset_of_minX_0(),
	U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452::get_offset_of_offX_1(),
	U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452::get_offset_of_minZ_2(),
	U3CCreateHouseRoofU3Ec__AnonStorey1_t2176372452::get_offset_of_offZ_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5009 = { sizeof (U3CStringToColorU3Ec__AnonStorey2_t2863735018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5009[1] = 
{
	U3CStringToColorU3Ec__AnonStorey2_t2863735018::get_offset_of_hex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5010 = { sizeof (OnlineMapsBuildingMaterial_t302108695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5010[3] = 
{
	OnlineMapsBuildingMaterial_t302108695::get_offset_of_wall_0(),
	OnlineMapsBuildingMaterial_t302108695::get_offset_of_roof_1(),
	OnlineMapsBuildingMaterial_t302108695::get_offset_of_scale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5011 = { sizeof (OnlineMapsBuildings_t3411229051), -1, sizeof(OnlineMapsBuildings_t3411229051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5011[19] = 
{
	OnlineMapsBuildings_t3411229051::get_offset_of_OnBuildingCreated_2(),
	OnlineMapsBuildings_t3411229051::get_offset_of_OnBuildingDispose_3(),
	OnlineMapsBuildings_t3411229051::get_offset_of_OnNewBuildingsReceived_4(),
	OnlineMapsBuildings_t3411229051::get_offset_of_OnRequestSent_5(),
	OnlineMapsBuildings_t3411229051::get_offset_of_OnRequestComplete_6(),
	OnlineMapsBuildings_t3411229051_StaticFields::get_offset_of_buildingContainer_7(),
	OnlineMapsBuildings_t3411229051_StaticFields::get_offset_of__instance_8(),
	OnlineMapsBuildings_t3411229051::get_offset_of_zoomRange_9(),
	OnlineMapsBuildings_t3411229051::get_offset_of_levelsRange_10(),
	OnlineMapsBuildings_t3411229051::get_offset_of_levelHeight_11(),
	OnlineMapsBuildings_t3411229051::get_offset_of_minHeight_12(),
	OnlineMapsBuildings_t3411229051::get_offset_of_heightScale_13(),
	OnlineMapsBuildings_t3411229051::get_offset_of_materials_14(),
	OnlineMapsBuildings_t3411229051::get_offset_of_topLeft_15(),
	OnlineMapsBuildings_t3411229051::get_offset_of_bottomRight_16(),
	OnlineMapsBuildings_t3411229051::get_offset_of_buildings_17(),
	OnlineMapsBuildings_t3411229051::get_offset_of_unusedBuildings_18(),
	OnlineMapsBuildings_t3411229051::get_offset_of_newBuildingsData_19(),
	OnlineMapsBuildings_t3411229051::get_offset_of_sendBuildingsReceived_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5012 = { sizeof (U3COnBuildingRequestCompleteU3Ec__AnonStorey0_t1455629980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5012[2] = 
{
	U3COnBuildingRequestCompleteU3Ec__AnonStorey0_t1455629980::get_offset_of_response_0(),
	U3COnBuildingRequestCompleteU3Ec__AnonStorey0_t1455629980::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5013 = { sizeof (OnlineMapsBuildingsNodeData_t1471864431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5013[2] = 
{
	OnlineMapsBuildingsNodeData_t1471864431::get_offset_of_way_0(),
	OnlineMapsBuildingsNodeData_t1471864431::get_offset_of_nodes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5014 = { sizeof (OnlineMapsDirectionStep_t2252483185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5014[8] = 
{
	OnlineMapsDirectionStep_t2252483185::get_offset_of_distance_0(),
	OnlineMapsDirectionStep_t2252483185::get_offset_of_duration_1(),
	OnlineMapsDirectionStep_t2252483185::get_offset_of_end_2(),
	OnlineMapsDirectionStep_t2252483185::get_offset_of_instructions_3(),
	OnlineMapsDirectionStep_t2252483185::get_offset_of_maneuver_4(),
	OnlineMapsDirectionStep_t2252483185::get_offset_of_points_5(),
	OnlineMapsDirectionStep_t2252483185::get_offset_of_start_6(),
	OnlineMapsDirectionStep_t2252483185::get_offset_of_stringInstructions_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5015 = { sizeof (OnlineMapsJPEGDecoder_t872605750), -1, sizeof(OnlineMapsJPEGDecoder_t872605750_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5015[20] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	OnlineMapsJPEGDecoder_t872605750_StaticFields::get_offset_of_ZZ_18(),
	OnlineMapsJPEGDecoder_t872605750::get_offset_of_context_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5016 = { sizeof (Code_t3472511518)+ sizeof (Il2CppObject), sizeof(Code_t3472511518 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5016[2] = 
{
	Code_t3472511518::get_offset_of_bits_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Code_t3472511518::get_offset_of_code_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5017 = { sizeof (Component_t3519836246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5017[11] = 
{
	Component_t3519836246::get_offset_of_cid_0(),
	Component_t3519836246::get_offset_of_ssx_1(),
	Component_t3519836246::get_offset_of_ssy_2(),
	Component_t3519836246::get_offset_of_width_3(),
	Component_t3519836246::get_offset_of_height_4(),
	Component_t3519836246::get_offset_of_stride_5(),
	Component_t3519836246::get_offset_of_qtsel_6(),
	Component_t3519836246::get_offset_of_actabsel_7(),
	Component_t3519836246::get_offset_of_dctabsel_8(),
	Component_t3519836246::get_offset_of_dcpred_9(),
	Component_t3519836246::get_offset_of_pixels_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5018 = { sizeof (Context_t2423182662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5018[19] = 
{
	Context_t2423182662::get_offset_of_posb_0(),
	Context_t2423182662::get_offset_of_error_1(),
	Context_t2423182662::get_offset_of_pos_2(),
	Context_t2423182662::get_offset_of_size_3(),
	Context_t2423182662::get_offset_of_length_4(),
	Context_t2423182662::get_offset_of_width_5(),
	Context_t2423182662::get_offset_of_height_6(),
	Context_t2423182662::get_offset_of_mbwidth_7(),
	Context_t2423182662::get_offset_of_mbheight_8(),
	Context_t2423182662::get_offset_of_mbsizex_9(),
	Context_t2423182662::get_offset_of_mbsizey_10(),
	Context_t2423182662::get_offset_of_ncomp_11(),
	Context_t2423182662::get_offset_of_comp_12(),
	Context_t2423182662::get_offset_of_qtab_13(),
	Context_t2423182662::get_offset_of_vlctab_14(),
	Context_t2423182662::get_offset_of_buf_15(),
	Context_t2423182662::get_offset_of_bufbits_16(),
	Context_t2423182662::get_offset_of_rstinterval_17(),
	Context_t2423182662::get_offset_of_rgb_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5019 = { sizeof (JPEGException_t3449532446), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5020 = { sizeof (JPEGResult_t4270095944)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5020[8] = 
{
	JPEGResult_t4270095944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5021 = { sizeof (OnlineMapsLocationService_t21331090), -1, sizeof(OnlineMapsLocationService_t21331090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5021[27] = 
{
	OnlineMapsLocationService_t21331090_StaticFields::get_offset_of__instance_2(),
	OnlineMapsLocationService_t21331090::get_offset_of_OnCompassChanged_3(),
	OnlineMapsLocationService_t21331090::get_offset_of_OnLocationChanged_4(),
	OnlineMapsLocationService_t21331090::get_offset_of_autoStopUpdateOnInput_5(),
	OnlineMapsLocationService_t21331090::get_offset_of_createMarkerInUserPosition_6(),
	OnlineMapsLocationService_t21331090::get_offset_of_desiredAccuracy_7(),
	OnlineMapsLocationService_t21331090::get_offset_of_emulatorCompass_8(),
	OnlineMapsLocationService_t21331090::get_offset_of_emulatorPosition_9(),
	OnlineMapsLocationService_t21331090::get_offset_of_markerTooltip_10(),
	OnlineMapsLocationService_t21331090::get_offset_of_markerType_11(),
	OnlineMapsLocationService_t21331090::get_offset_of_marker2DAlign_12(),
	OnlineMapsLocationService_t21331090::get_offset_of_marker2DTexture_13(),
	OnlineMapsLocationService_t21331090::get_offset_of_marker3DPrefab_14(),
	OnlineMapsLocationService_t21331090::get_offset_of_position_15(),
	OnlineMapsLocationService_t21331090::get_offset_of_restoreAfter_16(),
	OnlineMapsLocationService_t21331090::get_offset_of_trueHeading_17(),
	OnlineMapsLocationService_t21331090::get_offset_of_updateDistance_18(),
	OnlineMapsLocationService_t21331090::get_offset_of_updatePosition_19(),
	OnlineMapsLocationService_t21331090::get_offset_of_useCompassForMarker_20(),
	OnlineMapsLocationService_t21331090::get_offset_of_useGPSEmulator_21(),
	OnlineMapsLocationService_t21331090::get_offset_of_api_22(),
	OnlineMapsLocationService_t21331090::get_offset_of_allowUpdatePosition_23(),
	OnlineMapsLocationService_t21331090::get_offset_of_lastPositionChangedTime_24(),
	OnlineMapsLocationService_t21331090::get_offset_of_lockDisable_25(),
	OnlineMapsLocationService_t21331090::get_offset_of_marker_26(),
	OnlineMapsLocationService_t21331090::get_offset_of_started_27(),
	OnlineMapsLocationService_t21331090::get_offset_of_errorMessage_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5022 = { sizeof (OnlineMapsRWTConnector_t1023776356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5022[7] = 
{
	OnlineMapsRWTConnector_t1023776356::get_offset_of_mode_2(),
	OnlineMapsRWTConnector_t1023776356::get_offset_of_positionMode_3(),
	OnlineMapsRWTConnector_t1023776356::get_offset_of_markerTexture_4(),
	OnlineMapsRWTConnector_t1023776356::get_offset_of_markerLabel_5(),
	OnlineMapsRWTConnector_t1023776356::get_offset_of_scenePosition_6(),
	OnlineMapsRWTConnector_t1023776356::get_offset_of_coordinates_7(),
	OnlineMapsRWTConnector_t1023776356::get_offset_of_targetTransform_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5023 = { sizeof (OnlineMapsRWTConnectorMode_t2301498079)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5023[3] = 
{
	OnlineMapsRWTConnectorMode_t2301498079::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5024 = { sizeof (OnlineMapsRWTConnectorPositionMode_t1024163612)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5024[4] = 
{
	OnlineMapsRWTConnectorPositionMode_t1024163612::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5025 = { sizeof (OnlineMapsXML_t3341520387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5025[2] = 
{
	OnlineMapsXML_t3341520387::get_offset_of__document_0(),
	OnlineMapsXML_t3341520387::get_offset_of__element_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5026 = { sizeof (OnlineMapsXMLEnum_t1817127220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5026[2] = 
{
	OnlineMapsXMLEnum_t1817127220::get_offset_of_el_0(),
	OnlineMapsXMLEnum_t1817127220::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5027 = { sizeof (OnlineMapsXMLList_t1635558505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5027[1] = 
{
	OnlineMapsXMLList_t1635558505::get_offset_of__list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5028 = { sizeof (OnlineMapsXMLListEnum_t1723955324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5028[2] = 
{
	OnlineMapsXMLListEnum_t1723955324::get_offset_of_list_0(),
	OnlineMapsXMLListEnum_t1723955324::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5029 = { sizeof (LanguageSelection_t3655407812), -1, sizeof(LanguageSelection_t3655407812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5029[2] = 
{
	LanguageSelection_t3655407812::get_offset_of_mList_2(),
	LanguageSelection_t3655407812_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5030 = { sizeof (TypewriterEffect_t1547188070), -1, sizeof(TypewriterEffect_t1547188070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5030[15] = 
{
	TypewriterEffect_t1547188070_StaticFields::get_offset_of_current_2(),
	TypewriterEffect_t1547188070::get_offset_of_charsPerSecond_3(),
	TypewriterEffect_t1547188070::get_offset_of_fadeInTime_4(),
	TypewriterEffect_t1547188070::get_offset_of_delayOnPeriod_5(),
	TypewriterEffect_t1547188070::get_offset_of_delayOnNewLine_6(),
	TypewriterEffect_t1547188070::get_offset_of_scrollView_7(),
	TypewriterEffect_t1547188070::get_offset_of_keepFullDimensions_8(),
	TypewriterEffect_t1547188070::get_offset_of_onFinished_9(),
	TypewriterEffect_t1547188070::get_offset_of_mLabel_10(),
	TypewriterEffect_t1547188070::get_offset_of_mFullText_11(),
	TypewriterEffect_t1547188070::get_offset_of_mCurrentOffset_12(),
	TypewriterEffect_t1547188070::get_offset_of_mNextChar_13(),
	TypewriterEffect_t1547188070::get_offset_of_mReset_14(),
	TypewriterEffect_t1547188070::get_offset_of_mActive_15(),
	TypewriterEffect_t1547188070::get_offset_of_mFade_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5031 = { sizeof (FadeEntry_t3041229383)+ sizeof (Il2CppObject), sizeof(FadeEntry_t3041229383_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5031[3] = 
{
	FadeEntry_t3041229383::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FadeEntry_t3041229383::get_offset_of_text_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FadeEntry_t3041229383::get_offset_of_alpha_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5032 = { sizeof (UIButton_t3377238306), -1, sizeof(UIButton_t3377238306_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5032[14] = 
{
	UIButton_t3377238306_StaticFields::get_offset_of_current_12(),
	UIButton_t3377238306::get_offset_of_dragHighlight_13(),
	UIButton_t3377238306::get_offset_of_hoverSprite_14(),
	UIButton_t3377238306::get_offset_of_pressedSprite_15(),
	UIButton_t3377238306::get_offset_of_disabledSprite_16(),
	UIButton_t3377238306::get_offset_of_hoverSprite2D_17(),
	UIButton_t3377238306::get_offset_of_pressedSprite2D_18(),
	UIButton_t3377238306::get_offset_of_disabledSprite2D_19(),
	UIButton_t3377238306::get_offset_of_pixelSnap_20(),
	UIButton_t3377238306::get_offset_of_onClick_21(),
	UIButton_t3377238306::get_offset_of_mSprite_22(),
	UIButton_t3377238306::get_offset_of_mSprite2D_23(),
	UIButton_t3377238306::get_offset_of_mNormalSprite_24(),
	UIButton_t3377238306::get_offset_of_mNormalSprite2D_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5033 = { sizeof (UIButtonActivate_t791039529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5033[2] = 
{
	UIButtonActivate_t791039529::get_offset_of_target_2(),
	UIButtonActivate_t791039529::get_offset_of_state_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5034 = { sizeof (UIButtonColor_t3793385709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5034[10] = 
{
	UIButtonColor_t3793385709::get_offset_of_tweenTarget_2(),
	UIButtonColor_t3793385709::get_offset_of_hover_3(),
	UIButtonColor_t3793385709::get_offset_of_pressed_4(),
	UIButtonColor_t3793385709::get_offset_of_disabledColor_5(),
	UIButtonColor_t3793385709::get_offset_of_duration_6(),
	UIButtonColor_t3793385709::get_offset_of_mStartingColor_7(),
	UIButtonColor_t3793385709::get_offset_of_mDefaultColor_8(),
	UIButtonColor_t3793385709::get_offset_of_mInitDone_9(),
	UIButtonColor_t3793385709::get_offset_of_mWidget_10(),
	UIButtonColor_t3793385709::get_offset_of_mState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5035 = { sizeof (State_t1377781065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5035[5] = 
{
	State_t1377781065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5036 = { sizeof (UIButtonKeys_t2099118702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5036[5] = 
{
	UIButtonKeys_t2099118702::get_offset_of_selectOnClick_12(),
	UIButtonKeys_t2099118702::get_offset_of_selectOnUp_13(),
	UIButtonKeys_t2099118702::get_offset_of_selectOnDown_14(),
	UIButtonKeys_t2099118702::get_offset_of_selectOnLeft_15(),
	UIButtonKeys_t2099118702::get_offset_of_selectOnRight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5037 = { sizeof (UIButtonMessage_t3791537815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5037[5] = 
{
	UIButtonMessage_t3791537815::get_offset_of_target_2(),
	UIButtonMessage_t3791537815::get_offset_of_functionName_3(),
	UIButtonMessage_t3791537815::get_offset_of_trigger_4(),
	UIButtonMessage_t3791537815::get_offset_of_includeChildren_5(),
	UIButtonMessage_t3791537815::get_offset_of_mStarted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5038 = { sizeof (Trigger_t610760934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5038[7] = 
{
	Trigger_t610760934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5039 = { sizeof (UIButtonOffset_t1501810361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5039[7] = 
{
	UIButtonOffset_t1501810361::get_offset_of_tweenTarget_2(),
	UIButtonOffset_t1501810361::get_offset_of_hover_3(),
	UIButtonOffset_t1501810361::get_offset_of_pressed_4(),
	UIButtonOffset_t1501810361::get_offset_of_duration_5(),
	UIButtonOffset_t1501810361::get_offset_of_mPos_6(),
	UIButtonOffset_t1501810361::get_offset_of_mStarted_7(),
	UIButtonOffset_t1501810361::get_offset_of_mPressed_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5040 = { sizeof (UIButtonRotation_t491694416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5040[6] = 
{
	UIButtonRotation_t491694416::get_offset_of_tweenTarget_2(),
	UIButtonRotation_t491694416::get_offset_of_hover_3(),
	UIButtonRotation_t491694416::get_offset_of_pressed_4(),
	UIButtonRotation_t491694416::get_offset_of_duration_5(),
	UIButtonRotation_t491694416::get_offset_of_mRot_6(),
	UIButtonRotation_t491694416::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5041 = { sizeof (UIButtonScale_t627477860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5041[6] = 
{
	UIButtonScale_t627477860::get_offset_of_tweenTarget_2(),
	UIButtonScale_t627477860::get_offset_of_hover_3(),
	UIButtonScale_t627477860::get_offset_of_pressed_4(),
	UIButtonScale_t627477860::get_offset_of_duration_5(),
	UIButtonScale_t627477860::get_offset_of_mScale_6(),
	UIButtonScale_t627477860::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5042 = { sizeof (UICenterOnChild_t1687745660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5042[6] = 
{
	UICenterOnChild_t1687745660::get_offset_of_springStrength_2(),
	UICenterOnChild_t1687745660::get_offset_of_nextPageThreshold_3(),
	UICenterOnChild_t1687745660::get_offset_of_onFinished_4(),
	UICenterOnChild_t1687745660::get_offset_of_onCenter_5(),
	UICenterOnChild_t1687745660::get_offset_of_mScrollView_6(),
	UICenterOnChild_t1687745660::get_offset_of_mCenteredObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5043 = { sizeof (OnCenterCallback_t378748890), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5044 = { sizeof (UICenterOnClick_t1492880774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5045 = { sizeof (UIDragCamera_t1363661919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5045[1] = 
{
	UIDragCamera_t1363661919::get_offset_of_draggableCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5046 = { sizeof (UIDragDropContainer_t4036588756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5046[1] = 
{
	UIDragDropContainer_t4036588756::get_offset_of_reparentTarget_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5047 = { sizeof (UIDragDropItem_t4109477862), -1, sizeof(UIDragDropItem_t4109477862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5047[18] = 
{
	UIDragDropItem_t4109477862::get_offset_of_restriction_2(),
	UIDragDropItem_t4109477862::get_offset_of_cloneOnDrag_3(),
	UIDragDropItem_t4109477862::get_offset_of_pressAndHoldDelay_4(),
	UIDragDropItem_t4109477862::get_offset_of_interactable_5(),
	UIDragDropItem_t4109477862::get_offset_of_mTrans_6(),
	UIDragDropItem_t4109477862::get_offset_of_mParent_7(),
	UIDragDropItem_t4109477862::get_offset_of_mCollider_8(),
	UIDragDropItem_t4109477862::get_offset_of_mCollider2D_9(),
	UIDragDropItem_t4109477862::get_offset_of_mButton_10(),
	UIDragDropItem_t4109477862::get_offset_of_mRoot_11(),
	UIDragDropItem_t4109477862::get_offset_of_mGrid_12(),
	UIDragDropItem_t4109477862::get_offset_of_mTable_13(),
	UIDragDropItem_t4109477862::get_offset_of_mDragStartTime_14(),
	UIDragDropItem_t4109477862::get_offset_of_mDragScrollView_15(),
	UIDragDropItem_t4109477862::get_offset_of_mPressed_16(),
	UIDragDropItem_t4109477862::get_offset_of_mDragging_17(),
	UIDragDropItem_t4109477862::get_offset_of_mTouch_18(),
	UIDragDropItem_t4109477862_StaticFields::get_offset_of_draggedItems_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5048 = { sizeof (Restriction_t3942216849)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5048[5] = 
{
	Restriction_t3942216849::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5049 = { sizeof (U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5049[4] = 
{
	U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351::get_offset_of_U24this_0(),
	U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351::get_offset_of_U24current_1(),
	U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351::get_offset_of_U24disposing_2(),
	U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5050 = { sizeof (UIDragDropRoot_t997512525), -1, sizeof(UIDragDropRoot_t997512525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5050[1] = 
{
	UIDragDropRoot_t997512525_StaticFields::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5051 = { sizeof (UIDragObject_t1520449903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5051[18] = 
{
	UIDragObject_t1520449903::get_offset_of_target_2(),
	UIDragObject_t1520449903::get_offset_of_panelRegion_3(),
	UIDragObject_t1520449903::get_offset_of_scrollMomentum_4(),
	UIDragObject_t1520449903::get_offset_of_restrictWithinPanel_5(),
	UIDragObject_t1520449903::get_offset_of_contentRect_6(),
	UIDragObject_t1520449903::get_offset_of_dragEffect_7(),
	UIDragObject_t1520449903::get_offset_of_momentumAmount_8(),
	UIDragObject_t1520449903::get_offset_of_scale_9(),
	UIDragObject_t1520449903::get_offset_of_scrollWheelFactor_10(),
	UIDragObject_t1520449903::get_offset_of_mPlane_11(),
	UIDragObject_t1520449903::get_offset_of_mTargetPos_12(),
	UIDragObject_t1520449903::get_offset_of_mLastPos_13(),
	UIDragObject_t1520449903::get_offset_of_mMomentum_14(),
	UIDragObject_t1520449903::get_offset_of_mScroll_15(),
	UIDragObject_t1520449903::get_offset_of_mBounds_16(),
	UIDragObject_t1520449903::get_offset_of_mTouchID_17(),
	UIDragObject_t1520449903::get_offset_of_mStarted_18(),
	UIDragObject_t1520449903::get_offset_of_mPressed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5052 = { sizeof (DragEffect_t533639763)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5052[4] = 
{
	DragEffect_t533639763::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5053 = { sizeof (UIDragResize_t3448881960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5053[13] = 
{
	UIDragResize_t3448881960::get_offset_of_target_2(),
	UIDragResize_t3448881960::get_offset_of_pivot_3(),
	UIDragResize_t3448881960::get_offset_of_minWidth_4(),
	UIDragResize_t3448881960::get_offset_of_minHeight_5(),
	UIDragResize_t3448881960::get_offset_of_maxWidth_6(),
	UIDragResize_t3448881960::get_offset_of_maxHeight_7(),
	UIDragResize_t3448881960::get_offset_of_updateAnchors_8(),
	UIDragResize_t3448881960::get_offset_of_mPlane_9(),
	UIDragResize_t3448881960::get_offset_of_mRayPos_10(),
	UIDragResize_t3448881960::get_offset_of_mLocalPos_11(),
	UIDragResize_t3448881960::get_offset_of_mWidth_12(),
	UIDragResize_t3448881960::get_offset_of_mHeight_13(),
	UIDragResize_t3448881960::get_offset_of_mDragging_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5054 = { sizeof (UIDragScrollView_t2942595320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5054[6] = 
{
	UIDragScrollView_t2942595320::get_offset_of_scrollView_2(),
	UIDragScrollView_t2942595320::get_offset_of_draggablePanel_3(),
	UIDragScrollView_t2942595320::get_offset_of_mTrans_4(),
	UIDragScrollView_t2942595320::get_offset_of_mScroll_5(),
	UIDragScrollView_t2942595320::get_offset_of_mAutoFind_6(),
	UIDragScrollView_t2942595320::get_offset_of_mStarted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5055 = { sizeof (UIDraggableCamera_t2562792962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5055[14] = 
{
	UIDraggableCamera_t2562792962::get_offset_of_rootForBounds_2(),
	UIDraggableCamera_t2562792962::get_offset_of_scale_3(),
	UIDraggableCamera_t2562792962::get_offset_of_scrollWheelFactor_4(),
	UIDraggableCamera_t2562792962::get_offset_of_dragEffect_5(),
	UIDraggableCamera_t2562792962::get_offset_of_smoothDragStart_6(),
	UIDraggableCamera_t2562792962::get_offset_of_momentumAmount_7(),
	UIDraggableCamera_t2562792962::get_offset_of_mCam_8(),
	UIDraggableCamera_t2562792962::get_offset_of_mTrans_9(),
	UIDraggableCamera_t2562792962::get_offset_of_mPressed_10(),
	UIDraggableCamera_t2562792962::get_offset_of_mMomentum_11(),
	UIDraggableCamera_t2562792962::get_offset_of_mBounds_12(),
	UIDraggableCamera_t2562792962::get_offset_of_mScroll_13(),
	UIDraggableCamera_t2562792962::get_offset_of_mRoot_14(),
	UIDraggableCamera_t2562792962::get_offset_of_mDragStarted_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5056 = { sizeof (UIEventTrigger_t1351778058), -1, sizeof(UIEventTrigger_t1351778058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5056[14] = 
{
	UIEventTrigger_t1351778058_StaticFields::get_offset_of_current_2(),
	UIEventTrigger_t1351778058::get_offset_of_onHoverOver_3(),
	UIEventTrigger_t1351778058::get_offset_of_onHoverOut_4(),
	UIEventTrigger_t1351778058::get_offset_of_onPress_5(),
	UIEventTrigger_t1351778058::get_offset_of_onRelease_6(),
	UIEventTrigger_t1351778058::get_offset_of_onSelect_7(),
	UIEventTrigger_t1351778058::get_offset_of_onDeselect_8(),
	UIEventTrigger_t1351778058::get_offset_of_onClick_9(),
	UIEventTrigger_t1351778058::get_offset_of_onDoubleClick_10(),
	UIEventTrigger_t1351778058::get_offset_of_onDragStart_11(),
	UIEventTrigger_t1351778058::get_offset_of_onDragEnd_12(),
	UIEventTrigger_t1351778058::get_offset_of_onDragOver_13(),
	UIEventTrigger_t1351778058::get_offset_of_onDragOut_14(),
	UIEventTrigger_t1351778058::get_offset_of_onDrag_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5057 = { sizeof (UIForwardEvents_t1807908878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5057[10] = 
{
	UIForwardEvents_t1807908878::get_offset_of_target_2(),
	UIForwardEvents_t1807908878::get_offset_of_onHover_3(),
	UIForwardEvents_t1807908878::get_offset_of_onPress_4(),
	UIForwardEvents_t1807908878::get_offset_of_onClick_5(),
	UIForwardEvents_t1807908878::get_offset_of_onDoubleClick_6(),
	UIForwardEvents_t1807908878::get_offset_of_onSelect_7(),
	UIForwardEvents_t1807908878::get_offset_of_onDrag_8(),
	UIForwardEvents_t1807908878::get_offset_of_onDrop_9(),
	UIForwardEvents_t1807908878::get_offset_of_onSubmit_10(),
	UIForwardEvents_t1807908878::get_offset_of_onScroll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5058 = { sizeof (UIGrid_t2420180906), -1, sizeof(UIGrid_t2420180906_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5058[18] = 
{
	UIGrid_t2420180906::get_offset_of_arrangement_2(),
	UIGrid_t2420180906::get_offset_of_sorting_3(),
	UIGrid_t2420180906::get_offset_of_pivot_4(),
	UIGrid_t2420180906::get_offset_of_maxPerLine_5(),
	UIGrid_t2420180906::get_offset_of_cellWidth_6(),
	UIGrid_t2420180906::get_offset_of_cellHeight_7(),
	UIGrid_t2420180906::get_offset_of_animateSmoothly_8(),
	UIGrid_t2420180906::get_offset_of_hideInactive_9(),
	UIGrid_t2420180906::get_offset_of_keepWithinPanel_10(),
	UIGrid_t2420180906::get_offset_of_onReposition_11(),
	UIGrid_t2420180906::get_offset_of_onCustomSort_12(),
	UIGrid_t2420180906::get_offset_of_sorted_13(),
	UIGrid_t2420180906::get_offset_of_mReposition_14(),
	UIGrid_t2420180906::get_offset_of_mPanel_15(),
	UIGrid_t2420180906::get_offset_of_mInitDone_16(),
	UIGrid_t2420180906_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_17(),
	UIGrid_t2420180906_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_18(),
	UIGrid_t2420180906_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5059 = { sizeof (OnReposition_t581118304), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5060 = { sizeof (Arrangement_t177822543)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5060[4] = 
{
	Arrangement_t177822543::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5061 = { sizeof (Sorting_t2413224087)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5061[6] = 
{
	Sorting_t2413224087::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5062 = { sizeof (UIImageButton_t3909632983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5062[6] = 
{
	UIImageButton_t3909632983::get_offset_of_target_2(),
	UIImageButton_t3909632983::get_offset_of_normalSprite_3(),
	UIImageButton_t3909632983::get_offset_of_hoverSprite_4(),
	UIImageButton_t3909632983::get_offset_of_pressedSprite_5(),
	UIImageButton_t3909632983::get_offset_of_disabledSprite_6(),
	UIImageButton_t3909632983::get_offset_of_pixelSnap_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5063 = { sizeof (UIKeyBinding_t790450850), -1, sizeof(UIKeyBinding_t790450850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5063[7] = 
{
	UIKeyBinding_t790450850_StaticFields::get_offset_of_mList_2(),
	UIKeyBinding_t790450850::get_offset_of_keyCode_3(),
	UIKeyBinding_t790450850::get_offset_of_modifier_4(),
	UIKeyBinding_t790450850::get_offset_of_action_5(),
	UIKeyBinding_t790450850::get_offset_of_mIgnoreUp_6(),
	UIKeyBinding_t790450850::get_offset_of_mIsInput_7(),
	UIKeyBinding_t790450850::get_offset_of_mPress_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5064 = { sizeof (Action_t2562301673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5064[4] = 
{
	Action_t2562301673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5065 = { sizeof (Modifier_t1318739074)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5065[6] = 
{
	Modifier_t1318739074::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5066 = { sizeof (UIKeyNavigation_t1158973079), -1, sizeof(UIKeyNavigation_t1158973079_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5066[10] = 
{
	UIKeyNavigation_t1158973079_StaticFields::get_offset_of_list_2(),
	UIKeyNavigation_t1158973079::get_offset_of_constraint_3(),
	UIKeyNavigation_t1158973079::get_offset_of_onUp_4(),
	UIKeyNavigation_t1158973079::get_offset_of_onDown_5(),
	UIKeyNavigation_t1158973079::get_offset_of_onLeft_6(),
	UIKeyNavigation_t1158973079::get_offset_of_onRight_7(),
	UIKeyNavigation_t1158973079::get_offset_of_onClick_8(),
	UIKeyNavigation_t1158973079::get_offset_of_onTab_9(),
	UIKeyNavigation_t1158973079::get_offset_of_startsSelected_10(),
	UIKeyNavigation_t1158973079::get_offset_of_mStarted_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5067 = { sizeof (Constraint_t1628080441)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5067[5] = 
{
	Constraint_t1628080441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5068 = { sizeof (UIPlayAnimation_t3152825278), -1, sizeof(UIPlayAnimation_t3152825278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5068[16] = 
{
	UIPlayAnimation_t3152825278_StaticFields::get_offset_of_current_2(),
	UIPlayAnimation_t3152825278::get_offset_of_target_3(),
	UIPlayAnimation_t3152825278::get_offset_of_animator_4(),
	UIPlayAnimation_t3152825278::get_offset_of_clipName_5(),
	UIPlayAnimation_t3152825278::get_offset_of_trigger_6(),
	UIPlayAnimation_t3152825278::get_offset_of_playDirection_7(),
	UIPlayAnimation_t3152825278::get_offset_of_resetOnPlay_8(),
	UIPlayAnimation_t3152825278::get_offset_of_clearSelection_9(),
	UIPlayAnimation_t3152825278::get_offset_of_ifDisabledOnPlay_10(),
	UIPlayAnimation_t3152825278::get_offset_of_disableWhenFinished_11(),
	UIPlayAnimation_t3152825278::get_offset_of_onFinished_12(),
	UIPlayAnimation_t3152825278::get_offset_of_eventReceiver_13(),
	UIPlayAnimation_t3152825278::get_offset_of_callWhenFinished_14(),
	UIPlayAnimation_t3152825278::get_offset_of_mStarted_15(),
	UIPlayAnimation_t3152825278::get_offset_of_mActivated_16(),
	UIPlayAnimation_t3152825278::get_offset_of_dragHighlight_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5069 = { sizeof (UIPlaySound_t2984775557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5069[5] = 
{
	UIPlaySound_t2984775557::get_offset_of_audioClip_2(),
	UIPlaySound_t2984775557::get_offset_of_trigger_3(),
	UIPlaySound_t2984775557::get_offset_of_volume_4(),
	UIPlaySound_t2984775557::get_offset_of_pitch_5(),
	UIPlaySound_t2984775557::get_offset_of_mIsOver_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5070 = { sizeof (Trigger_t1926488856)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5070[9] = 
{
	Trigger_t1926488856::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5071 = { sizeof (UIPlayTween_t2614996159), -1, sizeof(UIPlayTween_t2614996159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5071[17] = 
{
	UIPlayTween_t2614996159_StaticFields::get_offset_of_current_2(),
	UIPlayTween_t2614996159::get_offset_of_tweenTarget_3(),
	UIPlayTween_t2614996159::get_offset_of_tweenGroup_4(),
	UIPlayTween_t2614996159::get_offset_of_trigger_5(),
	UIPlayTween_t2614996159::get_offset_of_playDirection_6(),
	UIPlayTween_t2614996159::get_offset_of_resetOnPlay_7(),
	UIPlayTween_t2614996159::get_offset_of_resetIfDisabled_8(),
	UIPlayTween_t2614996159::get_offset_of_ifDisabledOnPlay_9(),
	UIPlayTween_t2614996159::get_offset_of_disableWhenFinished_10(),
	UIPlayTween_t2614996159::get_offset_of_includeChildren_11(),
	UIPlayTween_t2614996159::get_offset_of_onFinished_12(),
	UIPlayTween_t2614996159::get_offset_of_eventReceiver_13(),
	UIPlayTween_t2614996159::get_offset_of_callWhenFinished_14(),
	UIPlayTween_t2614996159::get_offset_of_mTweens_15(),
	UIPlayTween_t2614996159::get_offset_of_mStarted_16(),
	UIPlayTween_t2614996159::get_offset_of_mActive_17(),
	UIPlayTween_t2614996159::get_offset_of_mActivated_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5072 = { sizeof (UIPopupList_t109953940), -1, sizeof(UIPopupList_t109953940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5072[42] = 
{
	UIPopupList_t109953940_StaticFields::get_offset_of_current_2(),
	UIPopupList_t109953940_StaticFields::get_offset_of_mChild_3(),
	UIPopupList_t109953940_StaticFields::get_offset_of_mFadeOutComplete_4(),
	0,
	UIPopupList_t109953940::get_offset_of_atlas_6(),
	UIPopupList_t109953940::get_offset_of_bitmapFont_7(),
	UIPopupList_t109953940::get_offset_of_trueTypeFont_8(),
	UIPopupList_t109953940::get_offset_of_fontSize_9(),
	UIPopupList_t109953940::get_offset_of_fontStyle_10(),
	UIPopupList_t109953940::get_offset_of_backgroundSprite_11(),
	UIPopupList_t109953940::get_offset_of_highlightSprite_12(),
	UIPopupList_t109953940::get_offset_of_position_13(),
	UIPopupList_t109953940::get_offset_of_alignment_14(),
	UIPopupList_t109953940::get_offset_of_items_15(),
	UIPopupList_t109953940::get_offset_of_itemData_16(),
	UIPopupList_t109953940::get_offset_of_padding_17(),
	UIPopupList_t109953940::get_offset_of_textColor_18(),
	UIPopupList_t109953940::get_offset_of_backgroundColor_19(),
	UIPopupList_t109953940::get_offset_of_highlightColor_20(),
	UIPopupList_t109953940::get_offset_of_isAnimated_21(),
	UIPopupList_t109953940::get_offset_of_isLocalized_22(),
	UIPopupList_t109953940::get_offset_of_openOn_23(),
	UIPopupList_t109953940::get_offset_of_onChange_24(),
	UIPopupList_t109953940::get_offset_of_mSelectedItem_25(),
	UIPopupList_t109953940::get_offset_of_mPanel_26(),
	UIPopupList_t109953940::get_offset_of_mBackground_27(),
	UIPopupList_t109953940::get_offset_of_mHighlight_28(),
	UIPopupList_t109953940::get_offset_of_mHighlightedLabel_29(),
	UIPopupList_t109953940::get_offset_of_mLabelList_30(),
	UIPopupList_t109953940::get_offset_of_mBgBorder_31(),
	UIPopupList_t109953940::get_offset_of_mSelection_32(),
	UIPopupList_t109953940::get_offset_of_mOpenFrame_33(),
	UIPopupList_t109953940::get_offset_of_eventReceiver_34(),
	UIPopupList_t109953940::get_offset_of_functionName_35(),
	UIPopupList_t109953940::get_offset_of_textScale_36(),
	UIPopupList_t109953940::get_offset_of_font_37(),
	UIPopupList_t109953940::get_offset_of_textLabel_38(),
	UIPopupList_t109953940::get_offset_of_mLegacyEvent_39(),
	UIPopupList_t109953940::get_offset_of_mExecuting_40(),
	UIPopupList_t109953940::get_offset_of_mUseDynamicFont_41(),
	UIPopupList_t109953940::get_offset_of_mTweening_42(),
	UIPopupList_t109953940::get_offset_of_source_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5073 = { sizeof (Position_t1780870098)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5073[4] = 
{
	Position_t1780870098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5074 = { sizeof (OpenOn_t3711353968)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5074[5] = 
{
	OpenOn_t3711353968::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5075 = { sizeof (LegacyEvent_t3991167770), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5076 = { sizeof (U3CUpdateTweenPositionU3Ec__Iterator0_t3769473947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5076[5] = 
{
	U3CUpdateTweenPositionU3Ec__Iterator0_t3769473947::get_offset_of_U3CtpU3E__0_0(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t3769473947::get_offset_of_U24this_1(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t3769473947::get_offset_of_U24current_2(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t3769473947::get_offset_of_U24disposing_3(),
	U3CUpdateTweenPositionU3Ec__Iterator0_t3769473947::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5077 = { sizeof (U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5077[4] = 
{
	U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018::get_offset_of_U24this_0(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018::get_offset_of_U24current_1(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018::get_offset_of_U24disposing_2(),
	U3CCloseIfUnselectedU3Ec__Iterator1_t1592075018::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5078 = { sizeof (UIProgressBar_t3824507790), -1, sizeof(UIProgressBar_t3824507790_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5078[13] = 
{
	UIProgressBar_t3824507790_StaticFields::get_offset_of_current_2(),
	UIProgressBar_t3824507790::get_offset_of_onDragFinished_3(),
	UIProgressBar_t3824507790::get_offset_of_thumb_4(),
	UIProgressBar_t3824507790::get_offset_of_mBG_5(),
	UIProgressBar_t3824507790::get_offset_of_mFG_6(),
	UIProgressBar_t3824507790::get_offset_of_mValue_7(),
	UIProgressBar_t3824507790::get_offset_of_mFill_8(),
	UIProgressBar_t3824507790::get_offset_of_mTrans_9(),
	UIProgressBar_t3824507790::get_offset_of_mIsDirty_10(),
	UIProgressBar_t3824507790::get_offset_of_mCam_11(),
	UIProgressBar_t3824507790::get_offset_of_mOffset_12(),
	UIProgressBar_t3824507790::get_offset_of_numberOfSteps_13(),
	UIProgressBar_t3824507790::get_offset_of_onChange_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5079 = { sizeof (FillDirection_t1824020367)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5079[5] = 
{
	FillDirection_t1824020367::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5080 = { sizeof (OnDragFinished_t2651612482), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5081 = { sizeof (UISavedOption_t3426088284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5081[4] = 
{
	UISavedOption_t3426088284::get_offset_of_keyName_2(),
	UISavedOption_t3426088284::get_offset_of_mList_3(),
	UISavedOption_t3426088284::get_offset_of_mCheck_4(),
	UISavedOption_t3426088284::get_offset_of_mSlider_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5082 = { sizeof (UIScrollBar_t1736046648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5082[3] = 
{
	UIScrollBar_t1736046648::get_offset_of_mSize_19(),
	UIScrollBar_t1736046648::get_offset_of_mScroll_20(),
	UIScrollBar_t1736046648::get_offset_of_mDir_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5083 = { sizeof (Direction_t3728253650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5083[4] = 
{
	Direction_t3728253650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5084 = { sizeof (UIScrollView_t3033954930), -1, sizeof(UIScrollView_t3033954930_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5084[37] = 
{
	UIScrollView_t3033954930_StaticFields::get_offset_of_list_2(),
	UIScrollView_t3033954930::get_offset_of_movement_3(),
	UIScrollView_t3033954930::get_offset_of_dragEffect_4(),
	UIScrollView_t3033954930::get_offset_of_restrictWithinPanel_5(),
	UIScrollView_t3033954930::get_offset_of_disableDragIfFits_6(),
	UIScrollView_t3033954930::get_offset_of_smoothDragStart_7(),
	UIScrollView_t3033954930::get_offset_of_iOSDragEmulation_8(),
	UIScrollView_t3033954930::get_offset_of_scrollWheelFactor_9(),
	UIScrollView_t3033954930::get_offset_of_momentumAmount_10(),
	UIScrollView_t3033954930::get_offset_of_dampenStrength_11(),
	UIScrollView_t3033954930::get_offset_of_horizontalScrollBar_12(),
	UIScrollView_t3033954930::get_offset_of_verticalScrollBar_13(),
	UIScrollView_t3033954930::get_offset_of_showScrollBars_14(),
	UIScrollView_t3033954930::get_offset_of_customMovement_15(),
	UIScrollView_t3033954930::get_offset_of_contentPivot_16(),
	UIScrollView_t3033954930::get_offset_of_onDragStarted_17(),
	UIScrollView_t3033954930::get_offset_of_onDragFinished_18(),
	UIScrollView_t3033954930::get_offset_of_onMomentumMove_19(),
	UIScrollView_t3033954930::get_offset_of_onStoppedMoving_20(),
	UIScrollView_t3033954930::get_offset_of_scale_21(),
	UIScrollView_t3033954930::get_offset_of_relativePositionOnReset_22(),
	UIScrollView_t3033954930::get_offset_of_mTrans_23(),
	UIScrollView_t3033954930::get_offset_of_mPanel_24(),
	UIScrollView_t3033954930::get_offset_of_mPlane_25(),
	UIScrollView_t3033954930::get_offset_of_mLastPos_26(),
	UIScrollView_t3033954930::get_offset_of_mPressed_27(),
	UIScrollView_t3033954930::get_offset_of_mMomentum_28(),
	UIScrollView_t3033954930::get_offset_of_mScroll_29(),
	UIScrollView_t3033954930::get_offset_of_mBounds_30(),
	UIScrollView_t3033954930::get_offset_of_mCalculatedBounds_31(),
	UIScrollView_t3033954930::get_offset_of_mShouldMove_32(),
	UIScrollView_t3033954930::get_offset_of_mIgnoreCallbacks_33(),
	UIScrollView_t3033954930::get_offset_of_mDragID_34(),
	UIScrollView_t3033954930::get_offset_of_mDragStartOffset_35(),
	UIScrollView_t3033954930::get_offset_of_mDragStarted_36(),
	UIScrollView_t3033954930::get_offset_of_mStarted_37(),
	UIScrollView_t3033954930::get_offset_of_centerOnChild_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5085 = { sizeof (Movement_t1682624982)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5085[5] = 
{
	Movement_t1682624982::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5086 = { sizeof (DragEffect_t614031488)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5086[4] = 
{
	DragEffect_t614031488::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5087 = { sizeof (ShowCondition_t3179581401)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5087[4] = 
{
	ShowCondition_t3179581401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5088 = { sizeof (OnDragNotification_t685967913), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5089 = { sizeof (UIShowControlScheme_t4140061467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5089[4] = 
{
	UIShowControlScheme_t4140061467::get_offset_of_target_2(),
	UIShowControlScheme_t4140061467::get_offset_of_mouse_3(),
	UIShowControlScheme_t4140061467::get_offset_of_touch_4(),
	UIShowControlScheme_t4140061467::get_offset_of_controller_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5090 = { sizeof (UISlider_t2191058247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5090[4] = 
{
	UISlider_t2191058247::get_offset_of_foreground_15(),
	UISlider_t2191058247::get_offset_of_rawValue_16(),
	UISlider_t2191058247::get_offset_of_direction_17(),
	UISlider_t2191058247::get_offset_of_mInverted_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5091 = { sizeof (Direction_t3287923381)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5091[4] = 
{
	Direction_t3287923381::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5092 = { sizeof (UISoundVolume_t1670191865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5093 = { sizeof (UITable_t3717403602), -1, sizeof(UITable_t3717403602_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5093[17] = 
{
	UITable_t3717403602::get_offset_of_columns_2(),
	UITable_t3717403602::get_offset_of_direction_3(),
	UITable_t3717403602::get_offset_of_sorting_4(),
	UITable_t3717403602::get_offset_of_pivot_5(),
	UITable_t3717403602::get_offset_of_cellAlignment_6(),
	UITable_t3717403602::get_offset_of_hideInactive_7(),
	UITable_t3717403602::get_offset_of_keepWithinPanel_8(),
	UITable_t3717403602::get_offset_of_padding_9(),
	UITable_t3717403602::get_offset_of_onReposition_10(),
	UITable_t3717403602::get_offset_of_onCustomSort_11(),
	UITable_t3717403602::get_offset_of_mPanel_12(),
	UITable_t3717403602::get_offset_of_mInitDone_13(),
	UITable_t3717403602::get_offset_of_mReposition_14(),
	UITable_t3717403602_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
	UITable_t3717403602_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_16(),
	UITable_t3717403602_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_17(),
	UITable_t3717403602_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5094 = { sizeof (OnReposition_t1194954916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5095 = { sizeof (Direction_t1281150584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5095[3] = 
{
	Direction_t1281150584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5096 = { sizeof (Sorting_t3318157469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5096[6] = 
{
	Sorting_t3318157469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5097 = { sizeof (UIToggle_t3036740318), -1, sizeof(UIToggle_t3036740318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5097[18] = 
{
	UIToggle_t3036740318_StaticFields::get_offset_of_list_2(),
	UIToggle_t3036740318_StaticFields::get_offset_of_current_3(),
	UIToggle_t3036740318::get_offset_of_group_4(),
	UIToggle_t3036740318::get_offset_of_activeSprite_5(),
	UIToggle_t3036740318::get_offset_of_activeAnimation_6(),
	UIToggle_t3036740318::get_offset_of_animator_7(),
	UIToggle_t3036740318::get_offset_of_startsActive_8(),
	UIToggle_t3036740318::get_offset_of_instantTween_9(),
	UIToggle_t3036740318::get_offset_of_optionCanBeNone_10(),
	UIToggle_t3036740318::get_offset_of_onChange_11(),
	UIToggle_t3036740318::get_offset_of_validator_12(),
	UIToggle_t3036740318::get_offset_of_checkSprite_13(),
	UIToggle_t3036740318::get_offset_of_checkAnimation_14(),
	UIToggle_t3036740318::get_offset_of_eventReceiver_15(),
	UIToggle_t3036740318::get_offset_of_functionName_16(),
	UIToggle_t3036740318::get_offset_of_startsChecked_17(),
	UIToggle_t3036740318::get_offset_of_mIsActive_18(),
	UIToggle_t3036740318::get_offset_of_mStarted_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5098 = { sizeof (Validate_t558489517), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5099 = { sizeof (UIToggledComponents_t1876468998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5099[4] = 
{
	UIToggledComponents_t1876468998::get_offset_of_activate_2(),
	UIToggledComponents_t1876468998::get_offset_of_deactivate_3(),
	UIToggledComponents_t1876468998::get_offset_of_target_4(),
	UIToggledComponents_t1876468998::get_offset_of_inverse_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
