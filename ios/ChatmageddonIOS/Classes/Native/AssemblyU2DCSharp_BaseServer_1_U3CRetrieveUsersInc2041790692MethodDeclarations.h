﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<RetrieveUsersIncomingPendindFriendRequests>c__AnonStorey10<System.Object>
struct U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2041790692;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<RetrieveUsersIncomingPendindFriendRequests>c__AnonStorey10<System.Object>::.ctor()
extern "C"  void U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10__ctor_m2009137107_gshared (U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2041790692 * __this, const MethodInfo* method);
#define U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10__ctor_m2009137107(__this, method) ((  void (*) (U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2041790692 *, const MethodInfo*))U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10__ctor_m2009137107_gshared)(__this, method)
// System.Void BaseServer`1/<RetrieveUsersIncomingPendindFriendRequests>c__AnonStorey10<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_U3CU3Em__0_m2721589380_gshared (U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2041790692 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_U3CU3Em__0_m2721589380(__this, ___request0, ___response1, method) ((  void (*) (U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_t2041790692 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRetrieveUsersIncomingPendindFriendRequestsU3Ec__AnonStorey10_U3CU3Em__0_m2721589380_gshared)(__this, ___request0, ___response1, method)
