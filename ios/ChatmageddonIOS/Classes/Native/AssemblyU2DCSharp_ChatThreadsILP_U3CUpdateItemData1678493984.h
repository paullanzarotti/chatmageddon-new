﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatMessage
struct ChatMessage_t2384228687;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatThreadsILP/<UpdateItemData>c__AnonStorey0
struct  U3CUpdateItemDataU3Ec__AnonStorey0_t1678493984  : public Il2CppObject
{
public:
	// ChatMessage ChatThreadsILP/<UpdateItemData>c__AnonStorey0::newMessage
	ChatMessage_t2384228687 * ___newMessage_0;

public:
	inline static int32_t get_offset_of_newMessage_0() { return static_cast<int32_t>(offsetof(U3CUpdateItemDataU3Ec__AnonStorey0_t1678493984, ___newMessage_0)); }
	inline ChatMessage_t2384228687 * get_newMessage_0() const { return ___newMessage_0; }
	inline ChatMessage_t2384228687 ** get_address_of_newMessage_0() { return &___newMessage_0; }
	inline void set_newMessage_0(ChatMessage_t2384228687 * value)
	{
		___newMessage_0 = value;
		Il2CppCodeGenWriteBarrier(&___newMessage_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
