﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.RawGooglePlayInterface
struct RawGooglePlayInterface_t2949402238;
// Unibill.Impl.GooglePlayBillingService
struct GooglePlayBillingService_t2494001613;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_GoogleP2494001613.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.RawGooglePlayInterface::.ctor()
extern "C"  void RawGooglePlayInterface__ctor_m555585580 (RawGooglePlayInterface_t2949402238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawGooglePlayInterface::initialise(Unibill.Impl.GooglePlayBillingService,System.String,System.String[])
extern "C"  void RawGooglePlayInterface_initialise_m2467831925 (RawGooglePlayInterface_t2949402238 * __this, GooglePlayBillingService_t2494001613 * ___callback0, String_t* ___publicKey1, StringU5BU5D_t1642385972* ___productIds2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawGooglePlayInterface::restoreTransactions()
extern "C"  void RawGooglePlayInterface_restoreTransactions_m1105465391 (RawGooglePlayInterface_t2949402238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawGooglePlayInterface::purchase(System.String)
extern "C"  void RawGooglePlayInterface_purchase_m1277411929 (RawGooglePlayInterface_t2949402238 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RawGooglePlayInterface::pollForConsumables()
extern "C"  void RawGooglePlayInterface_pollForConsumables_m1081658718 (RawGooglePlayInterface_t2949402238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
