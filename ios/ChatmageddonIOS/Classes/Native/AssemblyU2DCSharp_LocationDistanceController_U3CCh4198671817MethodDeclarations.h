﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocationDistanceController/<CheckDistance>c__Iterator0
struct U3CCheckDistanceU3Ec__Iterator0_t4198671817;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LocationDistanceController/<CheckDistance>c__Iterator0::.ctor()
extern "C"  void U3CCheckDistanceU3Ec__Iterator0__ctor_m2396697634 (U3CCheckDistanceU3Ec__Iterator0_t4198671817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocationDistanceController/<CheckDistance>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckDistanceU3Ec__Iterator0_MoveNext_m1308467238 (U3CCheckDistanceU3Ec__Iterator0_t4198671817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LocationDistanceController/<CheckDistance>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckDistanceU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2193251106 (U3CCheckDistanceU3Ec__Iterator0_t4198671817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LocationDistanceController/<CheckDistance>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckDistanceU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1794537562 (U3CCheckDistanceU3Ec__Iterator0_t4198671817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocationDistanceController/<CheckDistance>c__Iterator0::Dispose()
extern "C"  void U3CCheckDistanceU3Ec__Iterator0_Dispose_m818624267 (U3CCheckDistanceU3Ec__Iterator0_t4198671817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocationDistanceController/<CheckDistance>c__Iterator0::Reset()
extern "C"  void U3CCheckDistanceU3Ec__Iterator0_Reset_m2576566753 (U3CCheckDistanceU3Ec__Iterator0_t4198671817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
