﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,ChatNavScreen>
struct NavigationController_2_t2225246313;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,ChatNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m1644053680_gshared (NavigationController_2_t2225246313 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m1644053680(__this, method) ((  void (*) (NavigationController_2_t2225246313 *, const MethodInfo*))NavigationController_2__ctor_m1644053680_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,ChatNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m1767840762_gshared (NavigationController_2_t2225246313 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m1767840762(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t2225246313 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m1767840762_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,ChatNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3268801571_gshared (NavigationController_2_t2225246313 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m3268801571(__this, method) ((  void (*) (NavigationController_2_t2225246313 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3268801571_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,ChatNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m929386684_gshared (NavigationController_2_t2225246313 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m929386684(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t2225246313 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m929386684_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,ChatNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m599142940_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m599142940(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t2225246313 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m599142940_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,ChatNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m33970125_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m33970125(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t2225246313 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m33970125_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,ChatNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1163805226_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m1163805226(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t2225246313 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1163805226_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,ChatNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m605338048_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m605338048(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t2225246313 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m605338048_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,ChatNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m2027155932_gshared (NavigationController_2_t2225246313 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m2027155932(__this, ___screen0, method) ((  void (*) (NavigationController_2_t2225246313 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m2027155932_gshared)(__this, ___screen0, method)
