﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileChatButton
struct PlayerProfileChatButton_t1433962016;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerProfileChatButton::.ctor()
extern "C"  void PlayerProfileChatButton__ctor_m2722889047 (PlayerProfileChatButton_t1433962016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileChatButton::OnButtonClick()
extern "C"  void PlayerProfileChatButton_OnButtonClick_m2488446840 (PlayerProfileChatButton_t1433962016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
