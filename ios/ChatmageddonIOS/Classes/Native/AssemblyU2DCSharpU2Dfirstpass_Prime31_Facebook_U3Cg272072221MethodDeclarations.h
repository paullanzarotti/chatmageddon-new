﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.Facebook/<getSessionPermissionsOnServer>c__AnonStorey6
struct U3CgetSessionPermissionsOnServerU3Ec__AnonStorey6_t272072221;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Prime31.Facebook/<getSessionPermissionsOnServer>c__AnonStorey6::.ctor()
extern "C"  void U3CgetSessionPermissionsOnServerU3Ec__AnonStorey6__ctor_m35072458 (U3CgetSessionPermissionsOnServerU3Ec__AnonStorey6_t272072221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.Facebook/<getSessionPermissionsOnServer>c__AnonStorey6::<>m__0(System.String,System.Object)
extern "C"  void U3CgetSessionPermissionsOnServerU3Ec__AnonStorey6_U3CU3Em__0_m3765869569 (U3CgetSessionPermissionsOnServerU3Ec__AnonStorey6_t272072221 * __this, String_t* ___error0, Il2CppObject * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
