﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// TweenAlpha
struct TweenAlpha_t2421518635;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// ToastUI
struct ToastUI_t270783635;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Toaster
struct  Toaster_t2428368812  : public MonoBehaviour_t1158329972
{
public:
	// System.String Toaster::uiResourcePath
	String_t* ___uiResourcePath_2;
	// TweenAlpha Toaster::backgroundFade
	TweenAlpha_t2421518635 * ___backgroundFade_3;
	// UnityEngine.GameObject Toaster::toastContentObject
	GameObject_t1756533147 * ___toastContentObject_4;
	// ToastUI Toaster::activeUI
	ToastUI_t270783635 * ___activeUI_5;
	// System.Boolean Toaster::closingFade
	bool ___closingFade_6;

public:
	inline static int32_t get_offset_of_uiResourcePath_2() { return static_cast<int32_t>(offsetof(Toaster_t2428368812, ___uiResourcePath_2)); }
	inline String_t* get_uiResourcePath_2() const { return ___uiResourcePath_2; }
	inline String_t** get_address_of_uiResourcePath_2() { return &___uiResourcePath_2; }
	inline void set_uiResourcePath_2(String_t* value)
	{
		___uiResourcePath_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiResourcePath_2, value);
	}

	inline static int32_t get_offset_of_backgroundFade_3() { return static_cast<int32_t>(offsetof(Toaster_t2428368812, ___backgroundFade_3)); }
	inline TweenAlpha_t2421518635 * get_backgroundFade_3() const { return ___backgroundFade_3; }
	inline TweenAlpha_t2421518635 ** get_address_of_backgroundFade_3() { return &___backgroundFade_3; }
	inline void set_backgroundFade_3(TweenAlpha_t2421518635 * value)
	{
		___backgroundFade_3 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundFade_3, value);
	}

	inline static int32_t get_offset_of_toastContentObject_4() { return static_cast<int32_t>(offsetof(Toaster_t2428368812, ___toastContentObject_4)); }
	inline GameObject_t1756533147 * get_toastContentObject_4() const { return ___toastContentObject_4; }
	inline GameObject_t1756533147 ** get_address_of_toastContentObject_4() { return &___toastContentObject_4; }
	inline void set_toastContentObject_4(GameObject_t1756533147 * value)
	{
		___toastContentObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___toastContentObject_4, value);
	}

	inline static int32_t get_offset_of_activeUI_5() { return static_cast<int32_t>(offsetof(Toaster_t2428368812, ___activeUI_5)); }
	inline ToastUI_t270783635 * get_activeUI_5() const { return ___activeUI_5; }
	inline ToastUI_t270783635 ** get_address_of_activeUI_5() { return &___activeUI_5; }
	inline void set_activeUI_5(ToastUI_t270783635 * value)
	{
		___activeUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___activeUI_5, value);
	}

	inline static int32_t get_offset_of_closingFade_6() { return static_cast<int32_t>(offsetof(Toaster_t2428368812, ___closingFade_6)); }
	inline bool get_closingFade_6() const { return ___closingFade_6; }
	inline bool* get_address_of_closingFade_6() { return &___closingFade_6; }
	inline void set_closingFade_6(bool value)
	{
		___closingFade_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
