﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.Win8_1BillingService
struct Win8_1BillingService_t3938749894;
// unibill.Dummy.IWindowsIAP
struct IWindowsIAP_t2654797964;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// System.String
struct String_t;
// unibill.Dummy.Product[]
struct ProductU5BU5D_t4190069746;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TransactionDatabase201476183.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.Win8_1BillingService::.ctor(unibill.Dummy.IWindowsIAP,Unibill.Impl.UnibillConfiguration,Unibill.Impl.ProductIdRemapper,TransactionDatabase,Uniject.ILogger)
extern "C"  void Win8_1BillingService__ctor_m1498627478 (Win8_1BillingService_t3938749894 * __this, Il2CppObject * ___wp80, UnibillConfiguration_t2915611853 * ___config1, ProductIdRemapper_t3313438456 * ___remapper2, TransactionDatabase_t201476183 * ___tDb3, Il2CppObject * ___logger4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::initialise(Unibill.Impl.IBillingServiceCallback)
extern "C"  void Win8_1BillingService_initialise_m1991067792 (Win8_1BillingService_t3938749894 * __this, Il2CppObject * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::init(System.Int32)
extern "C"  void Win8_1BillingService_init_m398459587 (Win8_1BillingService_t3938749894 * __this, int32_t ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::purchase(System.String,System.String)
extern "C"  void Win8_1BillingService_purchase_m494994771 (Win8_1BillingService_t3938749894 * __this, String_t* ___item0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::restoreTransactions()
extern "C"  void Win8_1BillingService_restoreTransactions_m1991314775 (Win8_1BillingService_t3938749894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::enumerateLicenses()
extern "C"  void Win8_1BillingService_enumerateLicenses_m2187392712 (Win8_1BillingService_t3938749894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::logError(System.String)
extern "C"  void Win8_1BillingService_logError_m3869658934 (Win8_1BillingService_t3938749894 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::OnProductListReceived(unibill.Dummy.Product[])
extern "C"  void Win8_1BillingService_OnProductListReceived_m3099909551 (Win8_1BillingService_t3938749894 * __this, ProductU5BU5D_t4190069746* ___products0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::log(System.String)
extern "C"  void Win8_1BillingService_log_m1273954182 (Win8_1BillingService_t3938749894 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::OnPurchaseFailed(System.String,System.String)
extern "C"  void Win8_1BillingService_OnPurchaseFailed_m4249987885 (Win8_1BillingService_t3938749894 * __this, String_t* ___productId0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::OnPurchaseCancelled(System.String)
extern "C"  void Win8_1BillingService_OnPurchaseCancelled_m3194650871 (Win8_1BillingService_t3938749894 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::OnPurchaseSucceeded(System.String,System.String)
extern "C"  void Win8_1BillingService_OnPurchaseSucceeded_m3331912345 (Win8_1BillingService_t3938749894 * __this, String_t* ___productId0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::OnPurchaseSucceeded(System.String)
extern "C"  void Win8_1BillingService_OnPurchaseSucceeded_m280711715 (Win8_1BillingService_t3938749894 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.Win8_1BillingService::OnProductListError(System.String)
extern "C"  void Win8_1BillingService_OnProductListError_m2471221256 (Win8_1BillingService_t3938749894 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.Win8_1BillingService::hasReceipt(System.String)
extern "C"  bool Win8_1BillingService_hasReceipt_m3207970876 (Win8_1BillingService_t3938749894 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.Win8_1BillingService::getReceipt(System.String)
extern "C"  String_t* Win8_1BillingService_getReceipt_m1044954485 (Win8_1BillingService_t3938749894 * __this, String_t* ___forItem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
