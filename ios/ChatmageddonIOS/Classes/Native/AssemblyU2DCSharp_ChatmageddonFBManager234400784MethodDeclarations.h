﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonFBManager
struct ChatmageddonFBManager_t234400784;
// Facebook.Unity.IGraphResult
struct IGraphResult_t3984946686;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatmageddonFBManager::.ctor()
extern "C"  void ChatmageddonFBManager__ctor_m41049197 (ChatmageddonFBManager_t234400784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::InitFacebook()
extern "C"  void ChatmageddonFBManager_InitFacebook_m2606617495 (ChatmageddonFBManager_t234400784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::OnLoggedIn()
extern "C"  void ChatmageddonFBManager_OnLoggedIn_m716320489 (ChatmageddonFBManager_t234400784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::APICallback(Facebook.Unity.IGraphResult)
extern "C"  void ChatmageddonFBManager_APICallback_m2211708763 (ChatmageddonFBManager_t234400784 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::MyPictureCallback(UnityEngine.Texture)
extern "C"  void ChatmageddonFBManager_MyPictureCallback_m3547276192 (ChatmageddonFBManager_t234400784 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::StoryPosted(System.Boolean,System.Collections.Hashtable)
extern "C"  void ChatmageddonFBManager_StoryPosted_m2355712166 (ChatmageddonFBManager_t234400784 * __this, bool ___success0, Hashtable_t909839986 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::OnLoginCancelled()
extern "C"  void ChatmageddonFBManager_OnLoginCancelled_m2519601666 (ChatmageddonFBManager_t234400784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::OnLoggedOut(System.Boolean)
extern "C"  void ChatmageddonFBManager_OnLoggedOut_m1506544159 (ChatmageddonFBManager_t234400784 * __this, bool ___online0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::<InitFacebook>m__0(System.Boolean,System.String)
extern "C"  void ChatmageddonFBManager_U3CInitFacebookU3Em__0_m2884032597 (ChatmageddonFBManager_t234400784 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::<APICallback>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatmageddonFBManager_U3CAPICallbackU3Em__1_m2796321719 (Il2CppObject * __this /* static, unused */, bool ___loginSuccess0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonFBManager::<StoryPosted>m__2(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void ChatmageddonFBManager_U3CStoryPostedU3Em__2_m895576339 (Il2CppObject * __this /* static, unused */, bool ___deactivateSuccess0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
