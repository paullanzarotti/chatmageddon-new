﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.String,Prime31.FacebookFriendsResult>
struct Action_2_t2665012870;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.Facebook/<getFriends>c__AnonStorey3
struct  U3CgetFriendsU3Ec__AnonStorey3_t1196242495  : public Il2CppObject
{
public:
	// System.Action`2<System.String,Prime31.FacebookFriendsResult> Prime31.Facebook/<getFriends>c__AnonStorey3::completionHandler
	Action_2_t2665012870 * ___completionHandler_0;

public:
	inline static int32_t get_offset_of_completionHandler_0() { return static_cast<int32_t>(offsetof(U3CgetFriendsU3Ec__AnonStorey3_t1196242495, ___completionHandler_0)); }
	inline Action_2_t2665012870 * get_completionHandler_0() const { return ___completionHandler_0; }
	inline Action_2_t2665012870 ** get_address_of_completionHandler_0() { return &___completionHandler_0; }
	inline void set_completionHandler_0(Action_2_t2665012870 * value)
	{
		___completionHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___completionHandler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
