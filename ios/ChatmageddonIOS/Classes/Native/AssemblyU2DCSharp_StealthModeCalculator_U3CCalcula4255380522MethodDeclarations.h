﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StealthModeCalculator/<CalculateStealthTime>c__Iterator0
struct U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StealthModeCalculator/<CalculateStealthTime>c__Iterator0::.ctor()
extern "C"  void U3CCalculateStealthTimeU3Ec__Iterator0__ctor_m1536854325 (U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StealthModeCalculator/<CalculateStealthTime>c__Iterator0::MoveNext()
extern "C"  bool U3CCalculateStealthTimeU3Ec__Iterator0_MoveNext_m1940503931 (U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StealthModeCalculator/<CalculateStealthTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCalculateStealthTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1092741363 (U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StealthModeCalculator/<CalculateStealthTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCalculateStealthTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4267720059 (U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator/<CalculateStealthTime>c__Iterator0::Dispose()
extern "C"  void U3CCalculateStealthTimeU3Ec__Iterator0_Dispose_m571798060 (U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StealthModeCalculator/<CalculateStealthTime>c__Iterator0::Reset()
extern "C"  void U3CCalculateStealthTimeU3Ec__Iterator0_Reset_m2613279142 (U3CCalculateStealthTimeU3Ec__Iterator0_t4255380522 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
