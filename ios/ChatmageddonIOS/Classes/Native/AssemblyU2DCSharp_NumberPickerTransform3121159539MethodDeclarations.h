﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NumberPickerTransform
struct NumberPickerTransform_t3121159539;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void NumberPickerTransform::.ctor()
extern "C"  void NumberPickerTransform__ctor_m2626447364 (NumberPickerTransform_t3121159539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumberPickerTransform::SetColour(UnityEngine.Color)
extern "C"  void NumberPickerTransform_SetColour_m2619528576 (NumberPickerTransform_t3121159539 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
