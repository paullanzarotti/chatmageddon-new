﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t2537039969;
// UIPanel
struct UIPanel_t1795085332;
// UnityEngine.Transform
struct Transform_t3275118058;
// FireGuage
struct FireGuage_t4001636259;
// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UILabel
struct UILabel_t1795115428;
// TweenPosition
struct TweenPosition_t1144714832;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackGameUIScaler
struct  AttackGameUIScaler_t784344924  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UITexture AttackGameUIScaler::tvBackground
	UITexture_t2537039969 * ___tvBackground_14;
	// UIPanel AttackGameUIScaler::gamePanel
	UIPanel_t1795085332 * ___gamePanel_15;
	// UnityEngine.Transform AttackGameUIScaler::pointsLabel
	Transform_t3275118058 * ___pointsLabel_16;
	// UnityEngine.Transform AttackGameUIScaler::latLabel
	Transform_t3275118058 * ___latLabel_17;
	// UnityEngine.Transform AttackGameUIScaler::longLabel
	Transform_t3275118058 * ___longLabel_18;
	// UIPanel AttackGameUIScaler::fireGuagePanel
	UIPanel_t1795085332 * ___fireGuagePanel_19;
	// UITexture AttackGameUIScaler::fireBackground
	UITexture_t2537039969 * ___fireBackground_20;
	// FireGuage AttackGameUIScaler::fireGuage
	FireGuage_t4001636259 * ___fireGuage_21;
	// UISprite AttackGameUIScaler::aimButton
	UISprite_t603616735 * ___aimButton_22;
	// UnityEngine.BoxCollider AttackGameUIScaler::aimCollider
	BoxCollider_t22920061 * ___aimCollider_23;
	// UISprite AttackGameUIScaler::cancelButton
	UISprite_t603616735 * ___cancelButton_24;
	// UILabel AttackGameUIScaler::cancelLabel
	UILabel_t1795115428 * ___cancelLabel_25;
	// UnityEngine.BoxCollider AttackGameUIScaler::cancelCollider
	BoxCollider_t22920061 * ___cancelCollider_26;
	// UISprite AttackGameUIScaler::targetsButton
	UISprite_t603616735 * ___targetsButton_27;
	// UILabel AttackGameUIScaler::targetsLabel
	UILabel_t1795115428 * ___targetsLabel_28;
	// UnityEngine.BoxCollider AttackGameUIScaler::targetsCollider
	BoxCollider_t22920061 * ___targetsCollider_29;
	// UISprite AttackGameUIScaler::topHorizLine
	UISprite_t603616735 * ___topHorizLine_30;
	// UISprite AttackGameUIScaler::aimLine
	UISprite_t603616735 * ___aimLine_31;
	// UISprite AttackGameUIScaler::botHorizLine
	UISprite_t603616735 * ___botHorizLine_32;
	// TweenPosition AttackGameUIScaler::launchAccuracy
	TweenPosition_t1144714832 * ___launchAccuracy_33;

public:
	inline static int32_t get_offset_of_tvBackground_14() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___tvBackground_14)); }
	inline UITexture_t2537039969 * get_tvBackground_14() const { return ___tvBackground_14; }
	inline UITexture_t2537039969 ** get_address_of_tvBackground_14() { return &___tvBackground_14; }
	inline void set_tvBackground_14(UITexture_t2537039969 * value)
	{
		___tvBackground_14 = value;
		Il2CppCodeGenWriteBarrier(&___tvBackground_14, value);
	}

	inline static int32_t get_offset_of_gamePanel_15() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___gamePanel_15)); }
	inline UIPanel_t1795085332 * get_gamePanel_15() const { return ___gamePanel_15; }
	inline UIPanel_t1795085332 ** get_address_of_gamePanel_15() { return &___gamePanel_15; }
	inline void set_gamePanel_15(UIPanel_t1795085332 * value)
	{
		___gamePanel_15 = value;
		Il2CppCodeGenWriteBarrier(&___gamePanel_15, value);
	}

	inline static int32_t get_offset_of_pointsLabel_16() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___pointsLabel_16)); }
	inline Transform_t3275118058 * get_pointsLabel_16() const { return ___pointsLabel_16; }
	inline Transform_t3275118058 ** get_address_of_pointsLabel_16() { return &___pointsLabel_16; }
	inline void set_pointsLabel_16(Transform_t3275118058 * value)
	{
		___pointsLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___pointsLabel_16, value);
	}

	inline static int32_t get_offset_of_latLabel_17() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___latLabel_17)); }
	inline Transform_t3275118058 * get_latLabel_17() const { return ___latLabel_17; }
	inline Transform_t3275118058 ** get_address_of_latLabel_17() { return &___latLabel_17; }
	inline void set_latLabel_17(Transform_t3275118058 * value)
	{
		___latLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___latLabel_17, value);
	}

	inline static int32_t get_offset_of_longLabel_18() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___longLabel_18)); }
	inline Transform_t3275118058 * get_longLabel_18() const { return ___longLabel_18; }
	inline Transform_t3275118058 ** get_address_of_longLabel_18() { return &___longLabel_18; }
	inline void set_longLabel_18(Transform_t3275118058 * value)
	{
		___longLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___longLabel_18, value);
	}

	inline static int32_t get_offset_of_fireGuagePanel_19() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___fireGuagePanel_19)); }
	inline UIPanel_t1795085332 * get_fireGuagePanel_19() const { return ___fireGuagePanel_19; }
	inline UIPanel_t1795085332 ** get_address_of_fireGuagePanel_19() { return &___fireGuagePanel_19; }
	inline void set_fireGuagePanel_19(UIPanel_t1795085332 * value)
	{
		___fireGuagePanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___fireGuagePanel_19, value);
	}

	inline static int32_t get_offset_of_fireBackground_20() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___fireBackground_20)); }
	inline UITexture_t2537039969 * get_fireBackground_20() const { return ___fireBackground_20; }
	inline UITexture_t2537039969 ** get_address_of_fireBackground_20() { return &___fireBackground_20; }
	inline void set_fireBackground_20(UITexture_t2537039969 * value)
	{
		___fireBackground_20 = value;
		Il2CppCodeGenWriteBarrier(&___fireBackground_20, value);
	}

	inline static int32_t get_offset_of_fireGuage_21() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___fireGuage_21)); }
	inline FireGuage_t4001636259 * get_fireGuage_21() const { return ___fireGuage_21; }
	inline FireGuage_t4001636259 ** get_address_of_fireGuage_21() { return &___fireGuage_21; }
	inline void set_fireGuage_21(FireGuage_t4001636259 * value)
	{
		___fireGuage_21 = value;
		Il2CppCodeGenWriteBarrier(&___fireGuage_21, value);
	}

	inline static int32_t get_offset_of_aimButton_22() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___aimButton_22)); }
	inline UISprite_t603616735 * get_aimButton_22() const { return ___aimButton_22; }
	inline UISprite_t603616735 ** get_address_of_aimButton_22() { return &___aimButton_22; }
	inline void set_aimButton_22(UISprite_t603616735 * value)
	{
		___aimButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___aimButton_22, value);
	}

	inline static int32_t get_offset_of_aimCollider_23() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___aimCollider_23)); }
	inline BoxCollider_t22920061 * get_aimCollider_23() const { return ___aimCollider_23; }
	inline BoxCollider_t22920061 ** get_address_of_aimCollider_23() { return &___aimCollider_23; }
	inline void set_aimCollider_23(BoxCollider_t22920061 * value)
	{
		___aimCollider_23 = value;
		Il2CppCodeGenWriteBarrier(&___aimCollider_23, value);
	}

	inline static int32_t get_offset_of_cancelButton_24() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___cancelButton_24)); }
	inline UISprite_t603616735 * get_cancelButton_24() const { return ___cancelButton_24; }
	inline UISprite_t603616735 ** get_address_of_cancelButton_24() { return &___cancelButton_24; }
	inline void set_cancelButton_24(UISprite_t603616735 * value)
	{
		___cancelButton_24 = value;
		Il2CppCodeGenWriteBarrier(&___cancelButton_24, value);
	}

	inline static int32_t get_offset_of_cancelLabel_25() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___cancelLabel_25)); }
	inline UILabel_t1795115428 * get_cancelLabel_25() const { return ___cancelLabel_25; }
	inline UILabel_t1795115428 ** get_address_of_cancelLabel_25() { return &___cancelLabel_25; }
	inline void set_cancelLabel_25(UILabel_t1795115428 * value)
	{
		___cancelLabel_25 = value;
		Il2CppCodeGenWriteBarrier(&___cancelLabel_25, value);
	}

	inline static int32_t get_offset_of_cancelCollider_26() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___cancelCollider_26)); }
	inline BoxCollider_t22920061 * get_cancelCollider_26() const { return ___cancelCollider_26; }
	inline BoxCollider_t22920061 ** get_address_of_cancelCollider_26() { return &___cancelCollider_26; }
	inline void set_cancelCollider_26(BoxCollider_t22920061 * value)
	{
		___cancelCollider_26 = value;
		Il2CppCodeGenWriteBarrier(&___cancelCollider_26, value);
	}

	inline static int32_t get_offset_of_targetsButton_27() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___targetsButton_27)); }
	inline UISprite_t603616735 * get_targetsButton_27() const { return ___targetsButton_27; }
	inline UISprite_t603616735 ** get_address_of_targetsButton_27() { return &___targetsButton_27; }
	inline void set_targetsButton_27(UISprite_t603616735 * value)
	{
		___targetsButton_27 = value;
		Il2CppCodeGenWriteBarrier(&___targetsButton_27, value);
	}

	inline static int32_t get_offset_of_targetsLabel_28() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___targetsLabel_28)); }
	inline UILabel_t1795115428 * get_targetsLabel_28() const { return ___targetsLabel_28; }
	inline UILabel_t1795115428 ** get_address_of_targetsLabel_28() { return &___targetsLabel_28; }
	inline void set_targetsLabel_28(UILabel_t1795115428 * value)
	{
		___targetsLabel_28 = value;
		Il2CppCodeGenWriteBarrier(&___targetsLabel_28, value);
	}

	inline static int32_t get_offset_of_targetsCollider_29() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___targetsCollider_29)); }
	inline BoxCollider_t22920061 * get_targetsCollider_29() const { return ___targetsCollider_29; }
	inline BoxCollider_t22920061 ** get_address_of_targetsCollider_29() { return &___targetsCollider_29; }
	inline void set_targetsCollider_29(BoxCollider_t22920061 * value)
	{
		___targetsCollider_29 = value;
		Il2CppCodeGenWriteBarrier(&___targetsCollider_29, value);
	}

	inline static int32_t get_offset_of_topHorizLine_30() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___topHorizLine_30)); }
	inline UISprite_t603616735 * get_topHorizLine_30() const { return ___topHorizLine_30; }
	inline UISprite_t603616735 ** get_address_of_topHorizLine_30() { return &___topHorizLine_30; }
	inline void set_topHorizLine_30(UISprite_t603616735 * value)
	{
		___topHorizLine_30 = value;
		Il2CppCodeGenWriteBarrier(&___topHorizLine_30, value);
	}

	inline static int32_t get_offset_of_aimLine_31() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___aimLine_31)); }
	inline UISprite_t603616735 * get_aimLine_31() const { return ___aimLine_31; }
	inline UISprite_t603616735 ** get_address_of_aimLine_31() { return &___aimLine_31; }
	inline void set_aimLine_31(UISprite_t603616735 * value)
	{
		___aimLine_31 = value;
		Il2CppCodeGenWriteBarrier(&___aimLine_31, value);
	}

	inline static int32_t get_offset_of_botHorizLine_32() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___botHorizLine_32)); }
	inline UISprite_t603616735 * get_botHorizLine_32() const { return ___botHorizLine_32; }
	inline UISprite_t603616735 ** get_address_of_botHorizLine_32() { return &___botHorizLine_32; }
	inline void set_botHorizLine_32(UISprite_t603616735 * value)
	{
		___botHorizLine_32 = value;
		Il2CppCodeGenWriteBarrier(&___botHorizLine_32, value);
	}

	inline static int32_t get_offset_of_launchAccuracy_33() { return static_cast<int32_t>(offsetof(AttackGameUIScaler_t784344924, ___launchAccuracy_33)); }
	inline TweenPosition_t1144714832 * get_launchAccuracy_33() const { return ___launchAccuracy_33; }
	inline TweenPosition_t1144714832 ** get_address_of_launchAccuracy_33() { return &___launchAccuracy_33; }
	inline void set_launchAccuracy_33(TweenPosition_t1144714832 * value)
	{
		___launchAccuracy_33 = value;
		Il2CppCodeGenWriteBarrier(&___launchAccuracy_33, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
