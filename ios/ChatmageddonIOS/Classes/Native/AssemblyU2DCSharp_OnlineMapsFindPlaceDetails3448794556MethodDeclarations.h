﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsFindPlaceDetails
struct OnlineMapsFindPlaceDetails_t3448794556;
// System.String
struct String_t;
// OnlineMapsGoogleAPIQuery
struct OnlineMapsGoogleAPIQuery_t356009153;
// OnlineMapsFindPlaceDetailsResult
struct OnlineMapsFindPlaceDetailsResult_t4015393875;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"

// System.Void OnlineMapsFindPlaceDetails::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void OnlineMapsFindPlaceDetails__ctor_m1863422711 (OnlineMapsFindPlaceDetails_t3448794556 * __this, String_t* ___key0, String_t* ___place_id1, String_t* ___reference2, String_t* ___language3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsQueryType OnlineMapsFindPlaceDetails::get_type()
extern "C"  int32_t OnlineMapsFindPlaceDetails_get_type_m2594987659 (OnlineMapsFindPlaceDetails_t3448794556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindPlaceDetails::FindByPlaceID(System.String,System.String,System.String)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindPlaceDetails_FindByPlaceID_m3581519473 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___place_id1, String_t* ___language2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMapsFindPlaceDetails::FindByReference(System.String,System.String,System.String)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMapsFindPlaceDetails_FindByReference_m1838632328 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___reference1, String_t* ___language2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindPlaceDetailsResult OnlineMapsFindPlaceDetails::GetResult(System.String)
extern "C"  OnlineMapsFindPlaceDetailsResult_t4015393875 * OnlineMapsFindPlaceDetails_GetResult_m2272129636 (Il2CppObject * __this /* static, unused */, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
