﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShieldModal
struct ShieldModal_t4049164426;

#include "AssemblyU2DCSharp_ItemSwipeController3011021799.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldSwipeController
struct  ShieldSwipeController_t2505851713  : public ItemSwipeController_t3011021799
{
public:
	// ShieldModal ShieldSwipeController::modal
	ShieldModal_t4049164426 * ___modal_23;

public:
	inline static int32_t get_offset_of_modal_23() { return static_cast<int32_t>(offsetof(ShieldSwipeController_t2505851713, ___modal_23)); }
	inline ShieldModal_t4049164426 * get_modal_23() const { return ___modal_23; }
	inline ShieldModal_t4049164426 ** get_address_of_modal_23() { return &___modal_23; }
	inline void set_modal_23(ShieldModal_t4049164426 * value)
	{
		___modal_23 = value;
		Il2CppCodeGenWriteBarrier(&___modal_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
