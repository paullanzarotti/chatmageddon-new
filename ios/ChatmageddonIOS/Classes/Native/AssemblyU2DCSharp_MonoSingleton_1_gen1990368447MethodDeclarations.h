﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ConfigManager>::.ctor()
#define MonoSingleton_1__ctor_m3187536129(__this, method) ((  void (*) (MonoSingleton_1_t1990368447 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ConfigManager>::Awake()
#define MonoSingleton_1_Awake_m3814027004(__this, method) ((  void (*) (MonoSingleton_1_t1990368447 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ConfigManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m4274868308(__this /* static, unused */, method) ((  ConfigManager_t2239702727 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ConfigManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3019686038(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ConfigManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3375681419(__this, method) ((  void (*) (MonoSingleton_1_t1990368447 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ConfigManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m41605775(__this, method) ((  void (*) (MonoSingleton_1_t1990368447 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ConfigManager>::.cctor()
#define MonoSingleton_1__cctor_m2191758706(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
