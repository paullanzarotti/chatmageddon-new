﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "AssemblyU2DCSharp_User719925459.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Friend
struct  Friend_t3555014108  : public User_t719925459
{
public:
	// System.Collections.Hashtable Friend::friendRequest
	Hashtable_t909839986 * ___friendRequest_41;
	// System.Boolean Friend::incomingFriendRequest
	bool ___incomingFriendRequest_42;
	// System.Boolean Friend::outgoingFriendRequest
	bool ___outgoingFriendRequest_43;
	// System.Boolean Friend::incomingBlocked
	bool ___incomingBlocked_44;
	// System.Boolean Friend::outgoingBlocked
	bool ___outgoingBlocked_45;
	// System.Boolean Friend::globalFriend
	bool ___globalFriend_46;
	// System.Boolean Friend::innerFriend
	bool ___innerFriend_47;
	// System.Int32 Friend::innerFriendID
	int32_t ___innerFriendID_48;
	// System.Int32 Friend::strikeFirstPoints
	int32_t ___strikeFirstPoints_49;

public:
	inline static int32_t get_offset_of_friendRequest_41() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___friendRequest_41)); }
	inline Hashtable_t909839986 * get_friendRequest_41() const { return ___friendRequest_41; }
	inline Hashtable_t909839986 ** get_address_of_friendRequest_41() { return &___friendRequest_41; }
	inline void set_friendRequest_41(Hashtable_t909839986 * value)
	{
		___friendRequest_41 = value;
		Il2CppCodeGenWriteBarrier(&___friendRequest_41, value);
	}

	inline static int32_t get_offset_of_incomingFriendRequest_42() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___incomingFriendRequest_42)); }
	inline bool get_incomingFriendRequest_42() const { return ___incomingFriendRequest_42; }
	inline bool* get_address_of_incomingFriendRequest_42() { return &___incomingFriendRequest_42; }
	inline void set_incomingFriendRequest_42(bool value)
	{
		___incomingFriendRequest_42 = value;
	}

	inline static int32_t get_offset_of_outgoingFriendRequest_43() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___outgoingFriendRequest_43)); }
	inline bool get_outgoingFriendRequest_43() const { return ___outgoingFriendRequest_43; }
	inline bool* get_address_of_outgoingFriendRequest_43() { return &___outgoingFriendRequest_43; }
	inline void set_outgoingFriendRequest_43(bool value)
	{
		___outgoingFriendRequest_43 = value;
	}

	inline static int32_t get_offset_of_incomingBlocked_44() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___incomingBlocked_44)); }
	inline bool get_incomingBlocked_44() const { return ___incomingBlocked_44; }
	inline bool* get_address_of_incomingBlocked_44() { return &___incomingBlocked_44; }
	inline void set_incomingBlocked_44(bool value)
	{
		___incomingBlocked_44 = value;
	}

	inline static int32_t get_offset_of_outgoingBlocked_45() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___outgoingBlocked_45)); }
	inline bool get_outgoingBlocked_45() const { return ___outgoingBlocked_45; }
	inline bool* get_address_of_outgoingBlocked_45() { return &___outgoingBlocked_45; }
	inline void set_outgoingBlocked_45(bool value)
	{
		___outgoingBlocked_45 = value;
	}

	inline static int32_t get_offset_of_globalFriend_46() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___globalFriend_46)); }
	inline bool get_globalFriend_46() const { return ___globalFriend_46; }
	inline bool* get_address_of_globalFriend_46() { return &___globalFriend_46; }
	inline void set_globalFriend_46(bool value)
	{
		___globalFriend_46 = value;
	}

	inline static int32_t get_offset_of_innerFriend_47() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___innerFriend_47)); }
	inline bool get_innerFriend_47() const { return ___innerFriend_47; }
	inline bool* get_address_of_innerFriend_47() { return &___innerFriend_47; }
	inline void set_innerFriend_47(bool value)
	{
		___innerFriend_47 = value;
	}

	inline static int32_t get_offset_of_innerFriendID_48() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___innerFriendID_48)); }
	inline int32_t get_innerFriendID_48() const { return ___innerFriendID_48; }
	inline int32_t* get_address_of_innerFriendID_48() { return &___innerFriendID_48; }
	inline void set_innerFriendID_48(int32_t value)
	{
		___innerFriendID_48 = value;
	}

	inline static int32_t get_offset_of_strikeFirstPoints_49() { return static_cast<int32_t>(offsetof(Friend_t3555014108, ___strikeFirstPoints_49)); }
	inline int32_t get_strikeFirstPoints_49() const { return ___strikeFirstPoints_49; }
	inline int32_t* get_address_of_strikeFirstPoints_49() { return &___strikeFirstPoints_49; }
	inline void set_strikeFirstPoints_49(int32_t value)
	{
		___strikeFirstPoints_49 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
