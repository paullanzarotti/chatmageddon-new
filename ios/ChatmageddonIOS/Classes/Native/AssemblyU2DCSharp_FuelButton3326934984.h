﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuelButton
struct  FuelButton_t3326934984  : public SFXButton_t792651341
{
public:
	// System.Boolean FuelButton::allowToast
	bool ___allowToast_5;
	// System.Single FuelButton::toastWaitTime
	float ___toastWaitTime_6;

public:
	inline static int32_t get_offset_of_allowToast_5() { return static_cast<int32_t>(offsetof(FuelButton_t3326934984, ___allowToast_5)); }
	inline bool get_allowToast_5() const { return ___allowToast_5; }
	inline bool* get_address_of_allowToast_5() { return &___allowToast_5; }
	inline void set_allowToast_5(bool value)
	{
		___allowToast_5 = value;
	}

	inline static int32_t get_offset_of_toastWaitTime_6() { return static_cast<int32_t>(offsetof(FuelButton_t3326934984, ___toastWaitTime_6)); }
	inline float get_toastWaitTime_6() const { return ___toastWaitTime_6; }
	inline float* get_address_of_toastWaitTime_6() { return &___toastWaitTime_6; }
	inline void set_toastWaitTime_6(float value)
	{
		___toastWaitTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
