﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AwardModal
struct AwardModal_t1856366994;
// LevelAward
struct LevelAward_t2605567013;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LevelAward2605567013.h"

// System.Void AwardModal::.ctor()
extern "C"  void AwardModal__ctor_m2107505985 (AwardModal_t1856366994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AwardModal::LoadAward(LevelAward)
extern "C"  void AwardModal_LoadAward_m3583281705 (AwardModal_t1856366994 * __this, LevelAward_t2605567013 * ___award0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AwardModal::OnUIActivate()
extern "C"  void AwardModal_OnUIActivate_m2187804369 (AwardModal_t1856366994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AwardModal::OnUIDeactivate()
extern "C"  void AwardModal_OnUIDeactivate_m1652950780 (AwardModal_t1856366994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
