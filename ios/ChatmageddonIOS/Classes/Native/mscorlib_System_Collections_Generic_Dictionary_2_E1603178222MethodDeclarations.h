﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Object>
struct Dictionary_2_t283153520;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1603178222.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22335466038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1777707756_gshared (Enumerator_t1603178222 * __this, Dictionary_2_t283153520 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1777707756(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1603178222 *, Dictionary_2_t283153520 *, const MethodInfo*))Enumerator__ctor_m1777707756_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3540632177_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3540632177(__this, method) ((  Il2CppObject * (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3540632177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m919527565_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m919527565(__this, method) ((  void (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m919527565_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m382383752_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m382383752(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m382383752_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3716184131_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3716184131(__this, method) ((  Il2CppObject * (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3716184131_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1529692723_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1529692723(__this, method) ((  Il2CppObject * (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1529692723_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2401982917_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2401982917(__this, method) ((  bool (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_MoveNext_m2401982917_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2335466038  Enumerator_get_Current_m2065485869_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2065485869(__this, method) ((  KeyValuePair_2_t2335466038  (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_get_Current_m2065485869_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1491320644_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1491320644(__this, method) ((  int32_t (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_get_CurrentKey_m1491320644_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m4242886148_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m4242886148(__this, method) ((  Il2CppObject * (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_get_CurrentValue_m4242886148_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m2622064810_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2622064810(__this, method) ((  void (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_Reset_m2622064810_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3422172663_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3422172663(__this, method) ((  void (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_VerifyState_m3422172663_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m4229930837_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m4229930837(__this, method) ((  void (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_VerifyCurrent_m4229930837_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Unibill.Impl.BillingPlatform,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2279604588_gshared (Enumerator_t1603178222 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2279604588(__this, method) ((  void (*) (Enumerator_t1603178222 *, const MethodInfo*))Enumerator_Dispose_m2279604588_gshared)(__this, method)
