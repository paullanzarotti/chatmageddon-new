﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragMarkerByLongPressExample
struct DragMarkerByLongPressExample_t724607740;
// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarkerBase3900955221.h"

// System.Void DragMarkerByLongPressExample::.ctor()
extern "C"  void DragMarkerByLongPressExample__ctor_m1404234223 (DragMarkerByLongPressExample_t724607740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMarkerByLongPressExample::Start()
extern "C"  void DragMarkerByLongPressExample_Start_m3803025979 (DragMarkerByLongPressExample_t724607740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DragMarkerByLongPressExample::OnMarkerLongPress(OnlineMapsMarkerBase)
extern "C"  void DragMarkerByLongPressExample_OnMarkerLongPress_m1050170846 (DragMarkerByLongPressExample_t724607740 * __this, OnlineMapsMarkerBase_t3900955221 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
