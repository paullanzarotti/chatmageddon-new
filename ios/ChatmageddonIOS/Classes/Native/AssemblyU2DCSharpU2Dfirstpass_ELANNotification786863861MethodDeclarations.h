﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ELANNotification
struct ELANNotification_t786863861;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void ELANNotification::.ctor()
extern "C"  void ELANNotification__ctor_m901820210 (ELANNotification_t786863861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANNotification::Start()
extern "C"  void ELANNotification_Start_m1256363054 (ELANNotification_t786863861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANNotification::send()
extern "C"  void ELANNotification_send_m2531647874 (ELANNotification_t786863861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANNotification::cancel()
extern "C"  void ELANNotification_cancel_m3933017326 (ELANNotification_t786863861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ELANNotification::getDelay()
extern "C"  int64_t ELANNotification_getDelay_m1886465376 (ELANNotification_t786863861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ELANNotification::getRepetition()
extern "C"  int64_t ELANNotification_getRepetition_m2099357020 (ELANNotification_t786863861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ELANNotification::setFireDate(System.DateTime)
extern "C"  void ELANNotification_setFireDate_m3989972432 (ELANNotification_t786863861 * __this, DateTime_t693205669  ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ELANNotification::toString()
extern "C"  String_t* ELANNotification_toString_m2514797405 (ELANNotification_t786863861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
