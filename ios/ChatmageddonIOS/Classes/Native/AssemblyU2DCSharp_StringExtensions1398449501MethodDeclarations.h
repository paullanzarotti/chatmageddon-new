﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Collections.Generic.List`1<System.String> StringExtensions::SplitInParts(System.String,System.Int32)
extern "C"  List_1_t1398341365 * StringExtensions_SplitInParts_m2117931493 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___partLength1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
