﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<QuantitativeItem>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2619592363(__this, ___l0, method) ((  void (*) (Enumerator_t1940364586 *, List_1_t2405634912 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<QuantitativeItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m305052975(__this, method) ((  void (*) (Enumerator_t1940364586 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<QuantitativeItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3459748103(__this, method) ((  Il2CppObject * (*) (Enumerator_t1940364586 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<QuantitativeItem>::Dispose()
#define Enumerator_Dispose_m1586317172(__this, method) ((  void (*) (Enumerator_t1940364586 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<QuantitativeItem>::VerifyState()
#define Enumerator_VerifyState_m2329970461(__this, method) ((  void (*) (Enumerator_t1940364586 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<QuantitativeItem>::MoveNext()
#define Enumerator_MoveNext_m71027742(__this, method) ((  bool (*) (Enumerator_t1940364586 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<QuantitativeItem>::get_Current()
#define Enumerator_get_Current_m1368273712(__this, method) ((  QuantitativeItem_t3036513780 * (*) (Enumerator_t1940364586 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
