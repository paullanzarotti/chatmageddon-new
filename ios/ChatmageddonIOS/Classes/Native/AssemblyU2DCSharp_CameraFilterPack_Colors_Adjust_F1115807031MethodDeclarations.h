﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_Adjust_FullColors
struct CameraFilterPack_Colors_Adjust_FullColors_t1115807031;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_Adjust_FullColors::.ctor()
extern "C"  void CameraFilterPack_Colors_Adjust_FullColors__ctor_m4041826886 (CameraFilterPack_Colors_Adjust_FullColors_t1115807031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Adjust_FullColors::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_Adjust_FullColors_get_material_m2077107731 (CameraFilterPack_Colors_Adjust_FullColors_t1115807031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::Start()
extern "C"  void CameraFilterPack_Colors_Adjust_FullColors_Start_m3708670802 (CameraFilterPack_Colors_Adjust_FullColors_t1115807031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_Adjust_FullColors_OnRenderImage_m4144277978 (CameraFilterPack_Colors_Adjust_FullColors_t1115807031 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::Update()
extern "C"  void CameraFilterPack_Colors_Adjust_FullColors_Update_m3467465115 (CameraFilterPack_Colors_Adjust_FullColors_t1115807031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::OnDisable()
extern "C"  void CameraFilterPack_Colors_Adjust_FullColors_OnDisable_m2455886611 (CameraFilterPack_Colors_Adjust_FullColors_t1115807031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
