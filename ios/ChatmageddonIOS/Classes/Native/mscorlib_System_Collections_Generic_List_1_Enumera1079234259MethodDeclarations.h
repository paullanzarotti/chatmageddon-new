﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FriendNavScreen>
struct List_1_t1544504585;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1079234259.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FriendNavScreen>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m4235136824_gshared (Enumerator_t1079234259 * __this, List_1_t1544504585 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m4235136824(__this, ___l0, method) ((  void (*) (Enumerator_t1079234259 *, List_1_t1544504585 *, const MethodInfo*))Enumerator__ctor_m4235136824_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4172046506_gshared (Enumerator_t1079234259 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4172046506(__this, method) ((  void (*) (Enumerator_t1079234259 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4172046506_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FriendNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1105222018_gshared (Enumerator_t1079234259 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1105222018(__this, method) ((  Il2CppObject * (*) (Enumerator_t1079234259 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1105222018_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendNavScreen>::Dispose()
extern "C"  void Enumerator_Dispose_m1873691349_gshared (Enumerator_t1079234259 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1873691349(__this, method) ((  void (*) (Enumerator_t1079234259 *, const MethodInfo*))Enumerator_Dispose_m1873691349_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FriendNavScreen>::VerifyState()
extern "C"  void Enumerator_VerifyState_m568205934_gshared (Enumerator_t1079234259 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m568205934(__this, method) ((  void (*) (Enumerator_t1079234259 *, const MethodInfo*))Enumerator_VerifyState_m568205934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FriendNavScreen>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3211816126_gshared (Enumerator_t1079234259 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3211816126(__this, method) ((  bool (*) (Enumerator_t1079234259 *, const MethodInfo*))Enumerator_MoveNext_m3211816126_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FriendNavScreen>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1532506333_gshared (Enumerator_t1079234259 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1532506333(__this, method) ((  int32_t (*) (Enumerator_t1079234259 *, const MethodInfo*))Enumerator_get_Current_m1532506333_gshared)(__this, method)
