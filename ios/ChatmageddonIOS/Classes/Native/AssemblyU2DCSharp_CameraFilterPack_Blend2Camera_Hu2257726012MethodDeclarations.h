﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Hue
struct CameraFilterPack_Blend2Camera_Hue_t2257726012;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Hue::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Hue__ctor_m2033042137 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Hue::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Hue_get_material_m2439358916 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Hue_Start_m3350862209 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Hue_OnRenderImage_m3686515545 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Hue_OnValidate_m3457935484 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Hue_Update_m2592490502 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Hue_OnEnable_m2162212961 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Hue_OnDisable_m3070191296 (CameraFilterPack_Blend2Camera_Hue_t2257726012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
