﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<RegisterNewUser>c__AnonStorey1C<System.Object>
struct U3CRegisterNewUserU3Ec__AnonStorey1C_t4120269715;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<RegisterNewUser>c__AnonStorey1C<System.Object>::.ctor()
extern "C"  void U3CRegisterNewUserU3Ec__AnonStorey1C__ctor_m965723280_gshared (U3CRegisterNewUserU3Ec__AnonStorey1C_t4120269715 * __this, const MethodInfo* method);
#define U3CRegisterNewUserU3Ec__AnonStorey1C__ctor_m965723280(__this, method) ((  void (*) (U3CRegisterNewUserU3Ec__AnonStorey1C_t4120269715 *, const MethodInfo*))U3CRegisterNewUserU3Ec__AnonStorey1C__ctor_m965723280_gshared)(__this, method)
// System.Void BaseServer`1/<RegisterNewUser>c__AnonStorey1C<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CRegisterNewUserU3Ec__AnonStorey1C_U3CU3Em__0_m1119342291_gshared (U3CRegisterNewUserU3Ec__AnonStorey1C_t4120269715 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CRegisterNewUserU3Ec__AnonStorey1C_U3CU3Em__0_m1119342291(__this, ___request0, ___response1, method) ((  void (*) (U3CRegisterNewUserU3Ec__AnonStorey1C_t4120269715 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CRegisterNewUserU3Ec__AnonStorey1C_U3CU3Em__0_m1119342291_gshared)(__this, ___request0, ___response1, method)
