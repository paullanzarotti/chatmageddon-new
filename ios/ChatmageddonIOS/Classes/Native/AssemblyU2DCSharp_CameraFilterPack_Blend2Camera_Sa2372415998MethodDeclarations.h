﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Saturation
struct CameraFilterPack_Blend2Camera_Saturation_t2372415998;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Saturation::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Saturation__ctor_m4019053897 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Saturation::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Saturation_get_material_m930328648 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Saturation_Start_m2575235497 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Saturation_OnRenderImage_m3584655993 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Saturation_OnValidate_m301964370 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Saturation_Update_m2204585888 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Saturation_OnEnable_m675748841 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Saturation_OnDisable_m2296035670 (CameraFilterPack_Blend2Camera_Saturation_t2372415998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
