﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatThread
struct ChatThread_t2394323482;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// ChatMessage
struct ChatMessage_t2384228687;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// System.Collections.Generic.List`1<ChatMessage>
struct List_1_t1753349819;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "AssemblyU2DCSharp_ChatMessage2384228687.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void ChatThread::.ctor(System.Collections.Hashtable,System.Collections.ArrayList)
extern "C"  void ChatThread__ctor_m3232360610 (ChatThread_t2394323482 * __this, Hashtable_t909839986 * ___thread0, ArrayList_t4252133567 * ___users1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatThread::AddMessageToThread(ChatMessage)
extern "C"  void ChatThread_AddMessageToThread_m525441617 (ChatThread_t2394323482 * __this, ChatMessage_t2384228687 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatThread::GetLastMessage()
extern "C"  String_t* ChatThread_GetLastMessage_m1516343065 (ChatThread_t2394323482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ChatThread::GetLastMessageDate()
extern "C"  DateTime_t693205669  ChatThread_GetLastMessageDate_m546497253 (ChatThread_t2394323482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Friend> ChatThread::GetThreadUsers()
extern "C"  List_1_t2924135240 * ChatThread_GetThreadUsers_m4223956656 (ChatThread_t2394323482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ChatMessage> ChatThread::GetMessages()
extern "C"  List_1_t1753349819 * ChatThread_GetMessages_m756634845 (ChatThread_t2394323482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatThread::GetThreadID()
extern "C"  String_t* ChatThread_GetThreadID_m4202061651 (ChatThread_t2394323482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime ChatThread::<AddMessageToThread>m__0(ChatMessage)
extern "C"  DateTime_t693205669  ChatThread_U3CAddMessageToThreadU3Em__0_m2349568011 (Il2CppObject * __this /* static, unused */, ChatMessage_t2384228687 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
