﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t1158628263;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3303917033_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3303917033(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m3303917033_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  uint8_t U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1083457286_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1083457286(__this, method) ((  uint8_t (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1083457286_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2819521347_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2819521347(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2819521347_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m1943430486_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m1943430486(__this, method) ((  Il2CppObject * (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m1943430486_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m35409509_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m35409509(__this, method) ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m35409509_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::MoveNext()
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1294791019_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1294791019(__this, method) ((  bool (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1294791019_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::Dispose()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2667867866_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2667867866(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m2667867866_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Int32,System.Byte>::Reset()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1581670096_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 * __this, const MethodInfo* method);
#define U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1581670096(__this, method) ((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t2075803647 *, const MethodInfo*))U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m1581670096_gshared)(__this, method)
