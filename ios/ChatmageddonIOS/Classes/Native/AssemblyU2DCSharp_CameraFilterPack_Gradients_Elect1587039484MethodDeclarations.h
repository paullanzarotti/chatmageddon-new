﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Gradients_ElectricGradient
struct CameraFilterPack_Gradients_ElectricGradient_t1587039484;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Gradients_ElectricGradient::.ctor()
extern "C"  void CameraFilterPack_Gradients_ElectricGradient__ctor_m1490719759 (CameraFilterPack_Gradients_ElectricGradient_t1587039484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_ElectricGradient::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Gradients_ElectricGradient_get_material_m2590635336 (CameraFilterPack_Gradients_ElectricGradient_t1587039484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::Start()
extern "C"  void CameraFilterPack_Gradients_ElectricGradient_Start_m1567870723 (CameraFilterPack_Gradients_ElectricGradient_t1587039484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Gradients_ElectricGradient_OnRenderImage_m1952208827 (CameraFilterPack_Gradients_ElectricGradient_t1587039484 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::Update()
extern "C"  void CameraFilterPack_Gradients_ElectricGradient_Update_m507876314 (CameraFilterPack_Gradients_ElectricGradient_t1587039484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::OnDisable()
extern "C"  void CameraFilterPack_Gradients_ElectricGradient_OnDisable_m3428835148 (CameraFilterPack_Gradients_ElectricGradient_t1587039484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
