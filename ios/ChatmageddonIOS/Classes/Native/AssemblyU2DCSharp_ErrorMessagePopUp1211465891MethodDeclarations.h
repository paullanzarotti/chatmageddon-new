﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ErrorMessagePopUp
struct ErrorMessagePopUp_t1211465891;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"

// System.Void ErrorMessagePopUp::.ctor()
extern "C"  void ErrorMessagePopUp__ctor_m4011034612 (ErrorMessagePopUp_t1211465891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessagePopUp::Awake()
extern "C"  void ErrorMessagePopUp_Awake_m2304650323 (ErrorMessagePopUp_t1211465891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessagePopUp::OnAlphaTweenFinished()
extern "C"  void ErrorMessagePopUp_OnAlphaTweenFinished_m2013500746 (ErrorMessagePopUp_t1211465891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessagePopUp::LoadErrorMessage(System.String,System.Nullable`1<System.Single>,System.Boolean)
extern "C"  void ErrorMessagePopUp_LoadErrorMessage_m3418110718 (ErrorMessagePopUp_t1211465891 * __this, String_t* ___message0, Nullable_1_t339576247  ___viewTime1, bool ___removeTitle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ErrorMessagePopUp::WaitForViewTime()
extern "C"  Il2CppObject * ErrorMessagePopUp_WaitForViewTime_m3148888300 (ErrorMessagePopUp_t1211465891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ErrorMessagePopUp::UnLoadErrorMessage()
extern "C"  void ErrorMessagePopUp_UnLoadErrorMessage_m975456116 (ErrorMessagePopUp_t1211465891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
