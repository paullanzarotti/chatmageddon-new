﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ContactsNavScreen
struct ContactsNavScreen_t1794286542;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void ContactsNavScreen::.ctor()
extern "C"  void ContactsNavScreen__ctor_m2710690213 (ContactsNavScreen_t1794286542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNavScreen::Start()
extern "C"  void ContactsNavScreen_Start_m3409046933 (ContactsNavScreen_t1794286542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNavScreen::UIClosing()
extern "C"  void ContactsNavScreen_UIClosing_m247945296 (ContactsNavScreen_t1794286542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void ContactsNavScreen_ScreenLoad_m980606559 (ContactsNavScreen_t1794286542 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ContactsNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void ContactsNavScreen_ScreenUnload_m1033599815 (ContactsNavScreen_t1794286542 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ContactsNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool ContactsNavScreen_ValidateScreenNavigation_m2755331186 (ContactsNavScreen_t1794286542 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
