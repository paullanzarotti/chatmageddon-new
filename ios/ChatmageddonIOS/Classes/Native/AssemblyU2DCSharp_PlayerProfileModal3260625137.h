﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// User
struct User_t719925459;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// UILabel
struct UILabel_t1795115428;
// StealthModeCalculator
struct StealthModeCalculator_t3335204764;
// DoubleStateButton
struct DoubleStateButton_t1032633262;
// TweenScale
struct TweenScale_t2697902175;
// TweenPosition
struct TweenPosition_t1144714832;
// SettingsButton
struct SettingsButton_t3510963965;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerProfileModal
struct  PlayerProfileModal_t3260625137  : public ModalUI_t2568752073
{
public:
	// User PlayerProfileModal::user
	User_t719925459 * ___user_3;
	// UnityEngine.Color PlayerProfileModal::positiveColour
	Color_t2020392075  ___positiveColour_4;
	// UnityEngine.Color PlayerProfileModal::negativeColour
	Color_t2020392075  ___negativeColour_5;
	// AvatarUpdater PlayerProfileModal::avatar
	AvatarUpdater_t2404165026 * ___avatar_6;
	// UILabel PlayerProfileModal::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_7;
	// UILabel PlayerProfileModal::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_8;
	// UILabel PlayerProfileModal::usernameLabel
	UILabel_t1795115428 * ___usernameLabel_9;
	// UILabel PlayerProfileModal::rankLabel
	UILabel_t1795115428 * ___rankLabel_10;
	// UILabel PlayerProfileModal::rankNameLabel
	UILabel_t1795115428 * ___rankNameLabel_11;
	// UILabel PlayerProfileModal::rankLevelLabel
	UILabel_t1795115428 * ___rankLevelLabel_12;
	// UILabel PlayerProfileModal::stealthLabel
	UILabel_t1795115428 * ___stealthLabel_13;
	// UILabel PlayerProfileModal::stealthStateLabel
	UILabel_t1795115428 * ___stealthStateLabel_14;
	// StealthModeCalculator PlayerProfileModal::smCalculator
	StealthModeCalculator_t3335204764 * ___smCalculator_15;
	// UILabel PlayerProfileModal::dailyPointsLabel
	UILabel_t1795115428 * ___dailyPointsLabel_16;
	// UILabel PlayerProfileModal::totalPointsLabel
	UILabel_t1795115428 * ___totalPointsLabel_17;
	// DoubleStateButton PlayerProfileModal::chatButton
	DoubleStateButton_t1032633262 * ___chatButton_18;
	// TweenScale PlayerProfileModal::chatTween
	TweenScale_t2697902175 * ___chatTween_19;
	// DoubleStateButton PlayerProfileModal::attackButton
	DoubleStateButton_t1032633262 * ___attackButton_20;
	// TweenPosition PlayerProfileModal::attackTween
	TweenPosition_t1144714832 * ___attackTween_21;
	// DoubleStateButton PlayerProfileModal::blockButton
	DoubleStateButton_t1032633262 * ___blockButton_22;
	// TweenPosition PlayerProfileModal::blockTween
	TweenPosition_t1144714832 * ___blockTween_23;
	// SettingsButton PlayerProfileModal::settingsButton
	SettingsButton_t3510963965 * ___settingsButton_24;
	// DoubleStateButton PlayerProfileModal::friendButton
	DoubleStateButton_t1032633262 * ___friendButton_25;
	// TweenScale PlayerProfileModal::friendTween
	TweenScale_t2697902175 * ___friendTween_26;
	// UILabel PlayerProfileModal::requestSentLabel
	UILabel_t1795115428 * ___requestSentLabel_27;
	// DoubleStateButton PlayerProfileModal::acceptButton
	DoubleStateButton_t1032633262 * ___acceptButton_28;
	// TweenScale PlayerProfileModal::acceptTween
	TweenScale_t2697902175 * ___acceptTween_29;
	// DoubleStateButton PlayerProfileModal::declineButton
	DoubleStateButton_t1032633262 * ___declineButton_30;
	// TweenScale PlayerProfileModal::declineTween
	TweenScale_t2697902175 * ___declineTween_31;
	// UILabel PlayerProfileModal::missilesFiredLabel
	UILabel_t1795115428 * ___missilesFiredLabel_32;
	// UILabel PlayerProfileModal::successRateLabel
	UILabel_t1795115428 * ___successRateLabel_33;
	// UILabel PlayerProfileModal::incomingAttacksLabel
	UILabel_t1795115428 * ___incomingAttacksLabel_34;
	// UILabel PlayerProfileModal::hitByLabel
	UILabel_t1795115428 * ___hitByLabel_35;
	// UILabel PlayerProfileModal::defenceRateLabel
	UILabel_t1795115428 * ___defenceRateLabel_36;
	// UILabel PlayerProfileModal::laidLabel
	UILabel_t1795115428 * ___laidLabel_37;
	// UILabel PlayerProfileModal::triggeredLabel
	UILabel_t1795115428 * ___triggeredLabel_38;
	// UILabel PlayerProfileModal::defusedLabel
	UILabel_t1795115428 * ___defusedLabel_39;
	// System.Boolean PlayerProfileModal::player
	bool ___player_40;
	// System.Boolean PlayerProfileModal::profileUpdated
	bool ___profileUpdated_41;
	// System.Boolean PlayerProfileModal::friendUpdate
	bool ___friendUpdate_42;
	// System.Boolean PlayerProfileModal::declined
	bool ___declined_43;

public:
	inline static int32_t get_offset_of_user_3() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___user_3)); }
	inline User_t719925459 * get_user_3() const { return ___user_3; }
	inline User_t719925459 ** get_address_of_user_3() { return &___user_3; }
	inline void set_user_3(User_t719925459 * value)
	{
		___user_3 = value;
		Il2CppCodeGenWriteBarrier(&___user_3, value);
	}

	inline static int32_t get_offset_of_positiveColour_4() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___positiveColour_4)); }
	inline Color_t2020392075  get_positiveColour_4() const { return ___positiveColour_4; }
	inline Color_t2020392075 * get_address_of_positiveColour_4() { return &___positiveColour_4; }
	inline void set_positiveColour_4(Color_t2020392075  value)
	{
		___positiveColour_4 = value;
	}

	inline static int32_t get_offset_of_negativeColour_5() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___negativeColour_5)); }
	inline Color_t2020392075  get_negativeColour_5() const { return ___negativeColour_5; }
	inline Color_t2020392075 * get_address_of_negativeColour_5() { return &___negativeColour_5; }
	inline void set_negativeColour_5(Color_t2020392075  value)
	{
		___negativeColour_5 = value;
	}

	inline static int32_t get_offset_of_avatar_6() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___avatar_6)); }
	inline AvatarUpdater_t2404165026 * get_avatar_6() const { return ___avatar_6; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_6() { return &___avatar_6; }
	inline void set_avatar_6(AvatarUpdater_t2404165026 * value)
	{
		___avatar_6 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_6, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_7() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___firstNameLabel_7)); }
	inline UILabel_t1795115428 * get_firstNameLabel_7() const { return ___firstNameLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_7() { return &___firstNameLabel_7; }
	inline void set_firstNameLabel_7(UILabel_t1795115428 * value)
	{
		___firstNameLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_7, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_8() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___lastNameLabel_8)); }
	inline UILabel_t1795115428 * get_lastNameLabel_8() const { return ___lastNameLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_8() { return &___lastNameLabel_8; }
	inline void set_lastNameLabel_8(UILabel_t1795115428 * value)
	{
		___lastNameLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_8, value);
	}

	inline static int32_t get_offset_of_usernameLabel_9() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___usernameLabel_9)); }
	inline UILabel_t1795115428 * get_usernameLabel_9() const { return ___usernameLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_usernameLabel_9() { return &___usernameLabel_9; }
	inline void set_usernameLabel_9(UILabel_t1795115428 * value)
	{
		___usernameLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___usernameLabel_9, value);
	}

	inline static int32_t get_offset_of_rankLabel_10() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___rankLabel_10)); }
	inline UILabel_t1795115428 * get_rankLabel_10() const { return ___rankLabel_10; }
	inline UILabel_t1795115428 ** get_address_of_rankLabel_10() { return &___rankLabel_10; }
	inline void set_rankLabel_10(UILabel_t1795115428 * value)
	{
		___rankLabel_10 = value;
		Il2CppCodeGenWriteBarrier(&___rankLabel_10, value);
	}

	inline static int32_t get_offset_of_rankNameLabel_11() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___rankNameLabel_11)); }
	inline UILabel_t1795115428 * get_rankNameLabel_11() const { return ___rankNameLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_rankNameLabel_11() { return &___rankNameLabel_11; }
	inline void set_rankNameLabel_11(UILabel_t1795115428 * value)
	{
		___rankNameLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___rankNameLabel_11, value);
	}

	inline static int32_t get_offset_of_rankLevelLabel_12() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___rankLevelLabel_12)); }
	inline UILabel_t1795115428 * get_rankLevelLabel_12() const { return ___rankLevelLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_rankLevelLabel_12() { return &___rankLevelLabel_12; }
	inline void set_rankLevelLabel_12(UILabel_t1795115428 * value)
	{
		___rankLevelLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___rankLevelLabel_12, value);
	}

	inline static int32_t get_offset_of_stealthLabel_13() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___stealthLabel_13)); }
	inline UILabel_t1795115428 * get_stealthLabel_13() const { return ___stealthLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_stealthLabel_13() { return &___stealthLabel_13; }
	inline void set_stealthLabel_13(UILabel_t1795115428 * value)
	{
		___stealthLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___stealthLabel_13, value);
	}

	inline static int32_t get_offset_of_stealthStateLabel_14() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___stealthStateLabel_14)); }
	inline UILabel_t1795115428 * get_stealthStateLabel_14() const { return ___stealthStateLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_stealthStateLabel_14() { return &___stealthStateLabel_14; }
	inline void set_stealthStateLabel_14(UILabel_t1795115428 * value)
	{
		___stealthStateLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___stealthStateLabel_14, value);
	}

	inline static int32_t get_offset_of_smCalculator_15() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___smCalculator_15)); }
	inline StealthModeCalculator_t3335204764 * get_smCalculator_15() const { return ___smCalculator_15; }
	inline StealthModeCalculator_t3335204764 ** get_address_of_smCalculator_15() { return &___smCalculator_15; }
	inline void set_smCalculator_15(StealthModeCalculator_t3335204764 * value)
	{
		___smCalculator_15 = value;
		Il2CppCodeGenWriteBarrier(&___smCalculator_15, value);
	}

	inline static int32_t get_offset_of_dailyPointsLabel_16() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___dailyPointsLabel_16)); }
	inline UILabel_t1795115428 * get_dailyPointsLabel_16() const { return ___dailyPointsLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_dailyPointsLabel_16() { return &___dailyPointsLabel_16; }
	inline void set_dailyPointsLabel_16(UILabel_t1795115428 * value)
	{
		___dailyPointsLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___dailyPointsLabel_16, value);
	}

	inline static int32_t get_offset_of_totalPointsLabel_17() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___totalPointsLabel_17)); }
	inline UILabel_t1795115428 * get_totalPointsLabel_17() const { return ___totalPointsLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_totalPointsLabel_17() { return &___totalPointsLabel_17; }
	inline void set_totalPointsLabel_17(UILabel_t1795115428 * value)
	{
		___totalPointsLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___totalPointsLabel_17, value);
	}

	inline static int32_t get_offset_of_chatButton_18() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___chatButton_18)); }
	inline DoubleStateButton_t1032633262 * get_chatButton_18() const { return ___chatButton_18; }
	inline DoubleStateButton_t1032633262 ** get_address_of_chatButton_18() { return &___chatButton_18; }
	inline void set_chatButton_18(DoubleStateButton_t1032633262 * value)
	{
		___chatButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___chatButton_18, value);
	}

	inline static int32_t get_offset_of_chatTween_19() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___chatTween_19)); }
	inline TweenScale_t2697902175 * get_chatTween_19() const { return ___chatTween_19; }
	inline TweenScale_t2697902175 ** get_address_of_chatTween_19() { return &___chatTween_19; }
	inline void set_chatTween_19(TweenScale_t2697902175 * value)
	{
		___chatTween_19 = value;
		Il2CppCodeGenWriteBarrier(&___chatTween_19, value);
	}

	inline static int32_t get_offset_of_attackButton_20() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___attackButton_20)); }
	inline DoubleStateButton_t1032633262 * get_attackButton_20() const { return ___attackButton_20; }
	inline DoubleStateButton_t1032633262 ** get_address_of_attackButton_20() { return &___attackButton_20; }
	inline void set_attackButton_20(DoubleStateButton_t1032633262 * value)
	{
		___attackButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___attackButton_20, value);
	}

	inline static int32_t get_offset_of_attackTween_21() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___attackTween_21)); }
	inline TweenPosition_t1144714832 * get_attackTween_21() const { return ___attackTween_21; }
	inline TweenPosition_t1144714832 ** get_address_of_attackTween_21() { return &___attackTween_21; }
	inline void set_attackTween_21(TweenPosition_t1144714832 * value)
	{
		___attackTween_21 = value;
		Il2CppCodeGenWriteBarrier(&___attackTween_21, value);
	}

	inline static int32_t get_offset_of_blockButton_22() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___blockButton_22)); }
	inline DoubleStateButton_t1032633262 * get_blockButton_22() const { return ___blockButton_22; }
	inline DoubleStateButton_t1032633262 ** get_address_of_blockButton_22() { return &___blockButton_22; }
	inline void set_blockButton_22(DoubleStateButton_t1032633262 * value)
	{
		___blockButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___blockButton_22, value);
	}

	inline static int32_t get_offset_of_blockTween_23() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___blockTween_23)); }
	inline TweenPosition_t1144714832 * get_blockTween_23() const { return ___blockTween_23; }
	inline TweenPosition_t1144714832 ** get_address_of_blockTween_23() { return &___blockTween_23; }
	inline void set_blockTween_23(TweenPosition_t1144714832 * value)
	{
		___blockTween_23 = value;
		Il2CppCodeGenWriteBarrier(&___blockTween_23, value);
	}

	inline static int32_t get_offset_of_settingsButton_24() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___settingsButton_24)); }
	inline SettingsButton_t3510963965 * get_settingsButton_24() const { return ___settingsButton_24; }
	inline SettingsButton_t3510963965 ** get_address_of_settingsButton_24() { return &___settingsButton_24; }
	inline void set_settingsButton_24(SettingsButton_t3510963965 * value)
	{
		___settingsButton_24 = value;
		Il2CppCodeGenWriteBarrier(&___settingsButton_24, value);
	}

	inline static int32_t get_offset_of_friendButton_25() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___friendButton_25)); }
	inline DoubleStateButton_t1032633262 * get_friendButton_25() const { return ___friendButton_25; }
	inline DoubleStateButton_t1032633262 ** get_address_of_friendButton_25() { return &___friendButton_25; }
	inline void set_friendButton_25(DoubleStateButton_t1032633262 * value)
	{
		___friendButton_25 = value;
		Il2CppCodeGenWriteBarrier(&___friendButton_25, value);
	}

	inline static int32_t get_offset_of_friendTween_26() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___friendTween_26)); }
	inline TweenScale_t2697902175 * get_friendTween_26() const { return ___friendTween_26; }
	inline TweenScale_t2697902175 ** get_address_of_friendTween_26() { return &___friendTween_26; }
	inline void set_friendTween_26(TweenScale_t2697902175 * value)
	{
		___friendTween_26 = value;
		Il2CppCodeGenWriteBarrier(&___friendTween_26, value);
	}

	inline static int32_t get_offset_of_requestSentLabel_27() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___requestSentLabel_27)); }
	inline UILabel_t1795115428 * get_requestSentLabel_27() const { return ___requestSentLabel_27; }
	inline UILabel_t1795115428 ** get_address_of_requestSentLabel_27() { return &___requestSentLabel_27; }
	inline void set_requestSentLabel_27(UILabel_t1795115428 * value)
	{
		___requestSentLabel_27 = value;
		Il2CppCodeGenWriteBarrier(&___requestSentLabel_27, value);
	}

	inline static int32_t get_offset_of_acceptButton_28() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___acceptButton_28)); }
	inline DoubleStateButton_t1032633262 * get_acceptButton_28() const { return ___acceptButton_28; }
	inline DoubleStateButton_t1032633262 ** get_address_of_acceptButton_28() { return &___acceptButton_28; }
	inline void set_acceptButton_28(DoubleStateButton_t1032633262 * value)
	{
		___acceptButton_28 = value;
		Il2CppCodeGenWriteBarrier(&___acceptButton_28, value);
	}

	inline static int32_t get_offset_of_acceptTween_29() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___acceptTween_29)); }
	inline TweenScale_t2697902175 * get_acceptTween_29() const { return ___acceptTween_29; }
	inline TweenScale_t2697902175 ** get_address_of_acceptTween_29() { return &___acceptTween_29; }
	inline void set_acceptTween_29(TweenScale_t2697902175 * value)
	{
		___acceptTween_29 = value;
		Il2CppCodeGenWriteBarrier(&___acceptTween_29, value);
	}

	inline static int32_t get_offset_of_declineButton_30() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___declineButton_30)); }
	inline DoubleStateButton_t1032633262 * get_declineButton_30() const { return ___declineButton_30; }
	inline DoubleStateButton_t1032633262 ** get_address_of_declineButton_30() { return &___declineButton_30; }
	inline void set_declineButton_30(DoubleStateButton_t1032633262 * value)
	{
		___declineButton_30 = value;
		Il2CppCodeGenWriteBarrier(&___declineButton_30, value);
	}

	inline static int32_t get_offset_of_declineTween_31() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___declineTween_31)); }
	inline TweenScale_t2697902175 * get_declineTween_31() const { return ___declineTween_31; }
	inline TweenScale_t2697902175 ** get_address_of_declineTween_31() { return &___declineTween_31; }
	inline void set_declineTween_31(TweenScale_t2697902175 * value)
	{
		___declineTween_31 = value;
		Il2CppCodeGenWriteBarrier(&___declineTween_31, value);
	}

	inline static int32_t get_offset_of_missilesFiredLabel_32() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___missilesFiredLabel_32)); }
	inline UILabel_t1795115428 * get_missilesFiredLabel_32() const { return ___missilesFiredLabel_32; }
	inline UILabel_t1795115428 ** get_address_of_missilesFiredLabel_32() { return &___missilesFiredLabel_32; }
	inline void set_missilesFiredLabel_32(UILabel_t1795115428 * value)
	{
		___missilesFiredLabel_32 = value;
		Il2CppCodeGenWriteBarrier(&___missilesFiredLabel_32, value);
	}

	inline static int32_t get_offset_of_successRateLabel_33() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___successRateLabel_33)); }
	inline UILabel_t1795115428 * get_successRateLabel_33() const { return ___successRateLabel_33; }
	inline UILabel_t1795115428 ** get_address_of_successRateLabel_33() { return &___successRateLabel_33; }
	inline void set_successRateLabel_33(UILabel_t1795115428 * value)
	{
		___successRateLabel_33 = value;
		Il2CppCodeGenWriteBarrier(&___successRateLabel_33, value);
	}

	inline static int32_t get_offset_of_incomingAttacksLabel_34() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___incomingAttacksLabel_34)); }
	inline UILabel_t1795115428 * get_incomingAttacksLabel_34() const { return ___incomingAttacksLabel_34; }
	inline UILabel_t1795115428 ** get_address_of_incomingAttacksLabel_34() { return &___incomingAttacksLabel_34; }
	inline void set_incomingAttacksLabel_34(UILabel_t1795115428 * value)
	{
		___incomingAttacksLabel_34 = value;
		Il2CppCodeGenWriteBarrier(&___incomingAttacksLabel_34, value);
	}

	inline static int32_t get_offset_of_hitByLabel_35() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___hitByLabel_35)); }
	inline UILabel_t1795115428 * get_hitByLabel_35() const { return ___hitByLabel_35; }
	inline UILabel_t1795115428 ** get_address_of_hitByLabel_35() { return &___hitByLabel_35; }
	inline void set_hitByLabel_35(UILabel_t1795115428 * value)
	{
		___hitByLabel_35 = value;
		Il2CppCodeGenWriteBarrier(&___hitByLabel_35, value);
	}

	inline static int32_t get_offset_of_defenceRateLabel_36() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___defenceRateLabel_36)); }
	inline UILabel_t1795115428 * get_defenceRateLabel_36() const { return ___defenceRateLabel_36; }
	inline UILabel_t1795115428 ** get_address_of_defenceRateLabel_36() { return &___defenceRateLabel_36; }
	inline void set_defenceRateLabel_36(UILabel_t1795115428 * value)
	{
		___defenceRateLabel_36 = value;
		Il2CppCodeGenWriteBarrier(&___defenceRateLabel_36, value);
	}

	inline static int32_t get_offset_of_laidLabel_37() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___laidLabel_37)); }
	inline UILabel_t1795115428 * get_laidLabel_37() const { return ___laidLabel_37; }
	inline UILabel_t1795115428 ** get_address_of_laidLabel_37() { return &___laidLabel_37; }
	inline void set_laidLabel_37(UILabel_t1795115428 * value)
	{
		___laidLabel_37 = value;
		Il2CppCodeGenWriteBarrier(&___laidLabel_37, value);
	}

	inline static int32_t get_offset_of_triggeredLabel_38() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___triggeredLabel_38)); }
	inline UILabel_t1795115428 * get_triggeredLabel_38() const { return ___triggeredLabel_38; }
	inline UILabel_t1795115428 ** get_address_of_triggeredLabel_38() { return &___triggeredLabel_38; }
	inline void set_triggeredLabel_38(UILabel_t1795115428 * value)
	{
		___triggeredLabel_38 = value;
		Il2CppCodeGenWriteBarrier(&___triggeredLabel_38, value);
	}

	inline static int32_t get_offset_of_defusedLabel_39() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___defusedLabel_39)); }
	inline UILabel_t1795115428 * get_defusedLabel_39() const { return ___defusedLabel_39; }
	inline UILabel_t1795115428 ** get_address_of_defusedLabel_39() { return &___defusedLabel_39; }
	inline void set_defusedLabel_39(UILabel_t1795115428 * value)
	{
		___defusedLabel_39 = value;
		Il2CppCodeGenWriteBarrier(&___defusedLabel_39, value);
	}

	inline static int32_t get_offset_of_player_40() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___player_40)); }
	inline bool get_player_40() const { return ___player_40; }
	inline bool* get_address_of_player_40() { return &___player_40; }
	inline void set_player_40(bool value)
	{
		___player_40 = value;
	}

	inline static int32_t get_offset_of_profileUpdated_41() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___profileUpdated_41)); }
	inline bool get_profileUpdated_41() const { return ___profileUpdated_41; }
	inline bool* get_address_of_profileUpdated_41() { return &___profileUpdated_41; }
	inline void set_profileUpdated_41(bool value)
	{
		___profileUpdated_41 = value;
	}

	inline static int32_t get_offset_of_friendUpdate_42() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___friendUpdate_42)); }
	inline bool get_friendUpdate_42() const { return ___friendUpdate_42; }
	inline bool* get_address_of_friendUpdate_42() { return &___friendUpdate_42; }
	inline void set_friendUpdate_42(bool value)
	{
		___friendUpdate_42 = value;
	}

	inline static int32_t get_offset_of_declined_43() { return static_cast<int32_t>(offsetof(PlayerProfileModal_t3260625137, ___declined_43)); }
	inline bool get_declined_43() const { return ___declined_43; }
	inline bool* get_address_of_declined_43() { return &___declined_43; }
	inline void set_declined_43(bool value)
	{
		___declined_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
