﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<UnityEngine.Vector2,System.Boolean>
struct Func_2_t1033119496;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Func`2<UnityEngine.Vector2,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3449383897_gshared (Func_2_t1033119496 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m3449383897(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1033119496 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3449383897_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UnityEngine.Vector2,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m2982869255_gshared (Func_2_t1033119496 * __this, Vector2_t2243707579  ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m2982869255(__this, ___arg10, method) ((  bool (*) (Func_2_t1033119496 *, Vector2_t2243707579 , const MethodInfo*))Func_2_Invoke_m2982869255_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UnityEngine.Vector2,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3115364536_gshared (Func_2_t1033119496 * __this, Vector2_t2243707579  ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m3115364536(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1033119496 *, Vector2_t2243707579 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3115364536_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UnityEngine.Vector2,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m1725131781_gshared (Func_2_t1033119496 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m1725131781(__this, ___result0, method) ((  bool (*) (Func_2_t1033119496 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1725131781_gshared)(__this, ___result0, method)
