﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchedItem
struct LaunchedItem_t3670634427;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_ModalUI2568752073.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendModal
struct  DefendModal_t2645228609  : public ModalUI_t2568752073
{
public:
	// LaunchedItem DefendModal::item
	LaunchedItem_t3670634427 * ___item_3;
	// UnityEngine.GameObject DefendModal::navigationUI
	GameObject_t1756533147 * ___navigationUI_4;
	// UnityEngine.GameObject DefendModal::infoObject
	GameObject_t1756533147 * ___infoObject_5;
	// UnityEngine.GameObject DefendModal::defendGameObject
	GameObject_t1756533147 * ___defendGameObject_6;

public:
	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(DefendModal_t2645228609, ___item_3)); }
	inline LaunchedItem_t3670634427 * get_item_3() const { return ___item_3; }
	inline LaunchedItem_t3670634427 ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(LaunchedItem_t3670634427 * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier(&___item_3, value);
	}

	inline static int32_t get_offset_of_navigationUI_4() { return static_cast<int32_t>(offsetof(DefendModal_t2645228609, ___navigationUI_4)); }
	inline GameObject_t1756533147 * get_navigationUI_4() const { return ___navigationUI_4; }
	inline GameObject_t1756533147 ** get_address_of_navigationUI_4() { return &___navigationUI_4; }
	inline void set_navigationUI_4(GameObject_t1756533147 * value)
	{
		___navigationUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___navigationUI_4, value);
	}

	inline static int32_t get_offset_of_infoObject_5() { return static_cast<int32_t>(offsetof(DefendModal_t2645228609, ___infoObject_5)); }
	inline GameObject_t1756533147 * get_infoObject_5() const { return ___infoObject_5; }
	inline GameObject_t1756533147 ** get_address_of_infoObject_5() { return &___infoObject_5; }
	inline void set_infoObject_5(GameObject_t1756533147 * value)
	{
		___infoObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___infoObject_5, value);
	}

	inline static int32_t get_offset_of_defendGameObject_6() { return static_cast<int32_t>(offsetof(DefendModal_t2645228609, ___defendGameObject_6)); }
	inline GameObject_t1756533147 * get_defendGameObject_6() const { return ___defendGameObject_6; }
	inline GameObject_t1756533147 ** get_address_of_defendGameObject_6() { return &___defendGameObject_6; }
	inline void set_defendGameObject_6(GameObject_t1756533147 * value)
	{
		___defendGameObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___defendGameObject_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
