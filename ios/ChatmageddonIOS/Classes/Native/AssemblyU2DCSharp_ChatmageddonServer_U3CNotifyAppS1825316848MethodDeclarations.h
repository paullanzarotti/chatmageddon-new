﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<NotifyAppStart>c__AnonStorey6
struct U3CNotifyAppStartU3Ec__AnonStorey6_t1825316848;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<NotifyAppStart>c__AnonStorey6::.ctor()
extern "C"  void U3CNotifyAppStartU3Ec__AnonStorey6__ctor_m2076471821 (U3CNotifyAppStartU3Ec__AnonStorey6_t1825316848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<NotifyAppStart>c__AnonStorey6::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CNotifyAppStartU3Ec__AnonStorey6_U3CU3Em__0_m3900847124 (U3CNotifyAppStartU3Ec__AnonStorey6_t1825316848 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
