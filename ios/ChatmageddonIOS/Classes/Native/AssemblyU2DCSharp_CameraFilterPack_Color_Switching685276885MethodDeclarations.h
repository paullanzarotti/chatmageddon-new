﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Color_Switching
struct CameraFilterPack_Color_Switching_t685276885;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Color_Switching::.ctor()
extern "C"  void CameraFilterPack_Color_Switching__ctor_m2394827250 (CameraFilterPack_Color_Switching_t685276885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Switching::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Color_Switching_get_material_m872358085 (CameraFilterPack_Color_Switching_t685276885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::Start()
extern "C"  void CameraFilterPack_Color_Switching_Start_m2605607522 (CameraFilterPack_Color_Switching_t685276885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Color_Switching_OnRenderImage_m833679346 (CameraFilterPack_Color_Switching_t685276885 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::OnValidate()
extern "C"  void CameraFilterPack_Color_Switching_OnValidate_m444317643 (CameraFilterPack_Color_Switching_t685276885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::Update()
extern "C"  void CameraFilterPack_Color_Switching_Update_m3900204409 (CameraFilterPack_Color_Switching_t685276885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::OnDisable()
extern "C"  void CameraFilterPack_Color_Switching_OnDisable_m3320967677 (CameraFilterPack_Color_Switching_t685276885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
