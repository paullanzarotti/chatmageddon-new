﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LockoutUI
struct LockoutUI_t1337294023;

#include "codegen/il2cpp-codegen.h"

// System.Void LockoutUI::.ctor()
extern "C"  void LockoutUI__ctor_m3681405870 (LockoutUI_t1337294023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutUI::LoadUI(System.Boolean)
extern "C"  void LockoutUI_LoadUI_m2697867739 (LockoutUI_t1337294023 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutUI::UnloadUI(System.Boolean)
extern "C"  void LockoutUI_UnloadUI_m4060370304 (LockoutUI_t1337294023 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutUI::ShowUI(System.Boolean,System.Boolean)
extern "C"  void LockoutUI_ShowUI_m815079381 (LockoutUI_t1337294023 * __this, bool ___instant0, bool ___reset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockoutUI::HideUI(System.Boolean)
extern "C"  void LockoutUI_HideUI_m2397945269 (LockoutUI_t1337294023 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
