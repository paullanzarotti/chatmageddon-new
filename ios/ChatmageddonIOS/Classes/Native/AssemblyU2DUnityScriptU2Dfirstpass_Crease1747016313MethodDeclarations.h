﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Crease
struct Crease_t1747016313;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void Crease::.ctor()
extern "C"  void Crease__ctor_m69994775 (Crease_t1747016313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Crease::CheckResources()
extern "C"  bool Crease_CheckResources_m3953567636 (Crease_t1747016313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crease::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Crease_OnRenderImage_m1410541787 (Crease_t1747016313 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Crease::Main()
extern "C"  void Crease_Main_m1657719992 (Crease_t1747016313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
