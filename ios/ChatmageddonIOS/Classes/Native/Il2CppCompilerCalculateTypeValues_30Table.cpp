﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "P31RestKit_Prime31_ActionExtensions2727039990.h"
#include "P31RestKit_Prime31_StringExtensions2370178785.h"
#include "P31RestKit_Prime31_MonoBehaviourGUI1135617291.h"
#include "P31RestKit_Prime31_AbstractManager1005944057.h"
#include "P31RestKit_Prime31_ThreadingCallbackHelper3782680035.h"
#include "P31RestKit_Prime31_Json1852637766.h"
#include "P31RestKit_Prime31_Json_ObjectDecoder1026857897.h"
#include "P31RestKit_Prime31_Json_ObjectDecoder_U3CgetMembers397190414.h"
#include "P31RestKit_Prime31_Json_ObjectDecoder_U3CgetMember1963274355.h"
#include "P31RestKit_Prime31_Json_Deserializer3930269075.h"
#include "P31RestKit_Prime31_Json_Deserializer_JsonToken3872411607.h"
#include "P31RestKit_Prime31_Json_Serializer2406420290.h"
#include "P31RestKit_Prime31_JsonExtensions3771853076.h"
#include "P31RestKit_Prime31_JsonFormatter2583709410.h"
#include "P31RestKit_Prime31_JsonFormatter_JsonContextType3787516849.h"
#include "P31RestKit_Prime31_JsonArray67100779.h"
#include "P31RestKit_Prime31_JsonObject3301091759.h"
#include "P31RestKit_Prime31_SimpleJson4140114772.h"
#include "P31RestKit_Prime31_PocoJsonSerializerStrategy1405041182.h"
#include "P31RestKit_Prime31_Reflection_ReflectionUtils1280603200.h"
#include "P31RestKit_Prime31_Reflection_GetHandler2553917450.h"
#include "P31RestKit_Prime31_Reflection_SetHandler3932263246.h"
#include "P31RestKit_Prime31_Reflection_MemberMapLoader2009616003.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver3899543758.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver_CtorDe3614558424.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver_MemberM574666045.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver_U3Cget3350983475.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver_U3Ccre1529894103.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver_U3Ccre1837578470.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver_U3Ccre1529894105.h"
#include "P31RestKit_Prime31_Reflection_CacheResolver_U3Ccre1837578472.h"
#include "P31RestKit_Prime31_P31Error2856600608.h"
#include "P31RestKit_Prime31_LifecycleHelper337468626.h"
#include "unibill_U3CModuleU3E3783534214.h"
#include "unibill_unibill_Dummy_Product1158373411.h"
#include "unibill_unibill_Dummy_Factory1706693194.h"
#include "Facebook_Unity_U3CModuleU3E3783534214.h"
#include "Facebook_Unity_Facebook_Unity_AccessToken2518141643.h"
#include "Facebook_Unity_Facebook_Unity_CallbackManager4242095184.h"
#include "Facebook_Unity_Facebook_Unity_ComponentFactory2270101009.h"
#include "Facebook_Unity_Facebook_Unity_ComponentFactory_IfNo643838650.h"
#include "Facebook_Unity_Facebook_Unity_Constants3246017429.h"
#include "Facebook_Unity_Facebook_Unity_FB1612658294.h"
#include "Facebook_Unity_Facebook_Unity_FB_OnDLLLoaded1727406294.h"
#include "Facebook_Unity_Facebook_Unity_FB_Canvas2415435652.h"
#include "Facebook_Unity_Facebook_Unity_FB_Mobile3800732530.h"
#include "Facebook_Unity_Facebook_Unity_FB_CompiledFacebookL3305868676.h"
#include "Facebook_Unity_Facebook_Unity_FB_U3CInitU3Ec__Anon4214738661.h"
#include "Facebook_Unity_Facebook_Unity_FacebookBase2463540723.h"
#include "Facebook_Unity_Facebook_Unity_InitDelegate3410465555.h"
#include "Facebook_Unity_Facebook_Unity_HideUnityDelegate712804158.h"
#include "Facebook_Unity_Facebook_Unity_FacebookGameObject2922570967.h"
#include "Facebook_Unity_Facebook_Unity_FacebookSdkVersion1532426400.h"
#include "Facebook_Unity_Facebook_Unity_FacebookUnityPlatfor1867507902.h"
#include "Facebook_Unity_Facebook_Unity_MethodArguments735664811.h"
#include "Facebook_Unity_Facebook_Unity_ShareDialogMode1445392044.h"
#include "Facebook_Unity_Facebook_Unity_OGActionType1978093408.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo1390391306.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo3137418185.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo1732848805.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasFaceboo1985905999.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_CanvasJSWrapp3429491632.h"
#include "Facebook_Unity_Facebook_Unity_Canvas_JsBridge2435492180.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFacebook622171242.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo3968443998.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo1372893397.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo1353419607.h"
#include "Facebook_Unity_Facebook_Unity_Arcade_ArcadeFaceboo3930015791.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_MobileFaceboo2099931062.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_MobileFacebook668179329.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro3038444049.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro2149209708.h"
#include "Facebook_Unity_Facebook_Unity_Mobile_Android_Andro3776634754.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (ActionExtensions_t2727039990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (StringExtensions_t2370178785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (MonoBehaviourGUI_t1135617291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[16] = 
{
	MonoBehaviourGUI_t1135617291::get_offset_of__width_2(),
	MonoBehaviourGUI_t1135617291::get_offset_of__buttonHeight_3(),
	MonoBehaviourGUI_t1135617291::get_offset_of__toggleButtons_4(),
	MonoBehaviourGUI_t1135617291::get_offset_of__logBuilder_5(),
	MonoBehaviourGUI_t1135617291::get_offset_of__logRegistered_6(),
	MonoBehaviourGUI_t1135617291::get_offset_of__logScrollPosition_7(),
	MonoBehaviourGUI_t1135617291::get_offset_of__isShowingLogConsole_8(),
	MonoBehaviourGUI_t1135617291::get_offset_of__doubleClickDelay_9(),
	MonoBehaviourGUI_t1135617291::get_offset_of__previousClickTime_10(),
	MonoBehaviourGUI_t1135617291::get_offset_of__isWindowsPhone_11(),
	MonoBehaviourGUI_t1135617291::get_offset_of__normalBackground_12(),
	MonoBehaviourGUI_t1135617291::get_offset_of__bottomButtonBackground_13(),
	MonoBehaviourGUI_t1135617291::get_offset_of__activeBackground_14(),
	MonoBehaviourGUI_t1135617291::get_offset_of__toggleButtonBackground_15(),
	MonoBehaviourGUI_t1135617291::get_offset_of__didRetinaIpadCheck_16(),
	MonoBehaviourGUI_t1135617291::get_offset_of__isRetinaIpad_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (AbstractManager_t1005944057), -1, sizeof(AbstractManager_t1005944057_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3003[3] = 
{
	AbstractManager_t1005944057_StaticFields::get_offset_of__prime31LifecycleHelperRef_2(),
	AbstractManager_t1005944057_StaticFields::get_offset_of__threadingCallbackHelper_3(),
	AbstractManager_t1005944057_StaticFields::get_offset_of__prime31GameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (ThreadingCallbackHelper_t3782680035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[2] = 
{
	ThreadingCallbackHelper_t3782680035::get_offset_of__actions_2(),
	ThreadingCallbackHelper_t3782680035::get_offset_of__currentActions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (Json_t1852637766), -1, sizeof(Json_t1852637766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3005[1] = 
{
	Json_t1852637766_StaticFields::get_offset_of_useSimpleJson_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (ObjectDecoder_t1026857897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[1] = 
{
	ObjectDecoder_t1026857897::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (U3CgetMembersWithSettersU3Ec__AnonStorey0_t397190414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[2] = 
{
	U3CgetMembersWithSettersU3Ec__AnonStorey0_t397190414::get_offset_of_theInfo_0(),
	U3CgetMembersWithSettersU3Ec__AnonStorey0_t397190414::get_offset_of_theFieldType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (U3CgetMembersWithSettersU3Ec__AnonStorey1_t1963274355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[2] = 
{
	U3CgetMembersWithSettersU3Ec__AnonStorey1_t1963274355::get_offset_of_theInfo_0(),
	U3CgetMembersWithSettersU3Ec__AnonStorey1_t1963274355::get_offset_of_thePropertyType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (Deserializer_t3930269075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[1] = 
{
	Deserializer_t3930269075::get_offset_of_charArray_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (JsonToken_t3872411607)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3010[13] = 
{
	JsonToken_t3872411607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (Serializer_t2406420290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[1] = 
{
	Serializer_t2406420290::get_offset_of__builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (JsonExtensions_t3771853076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (JsonFormatter_t2583709410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[5] = 
{
	JsonFormatter_t2583709410::get_offset_of_inDoubleString_0(),
	JsonFormatter_t2583709410::get_offset_of_inSingleString_1(),
	JsonFormatter_t2583709410::get_offset_of_inVariableAssignment_2(),
	JsonFormatter_t2583709410::get_offset_of_prevChar_3(),
	JsonFormatter_t2583709410::get_offset_of_context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (JsonContextType_t3787516849)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3014[3] = 
{
	JsonContextType_t3787516849::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (JsonArray_t67100779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (JsonObject_t3301091759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (SimpleJson_t4140114772), -1, sizeof(SimpleJson_t4140114772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3017[2] = 
{
	SimpleJson_t4140114772_StaticFields::get_offset_of__currentJsonSerializerStrategy_0(),
	SimpleJson_t4140114772_StaticFields::get_offset_of__pocoJsonSerializerStrategy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (PocoJsonSerializerStrategy_t1405041182), -1, sizeof(PocoJsonSerializerStrategy_t1405041182_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3019[2] = 
{
	PocoJsonSerializerStrategy_t1405041182::get_offset_of_cacheResolver_0(),
	PocoJsonSerializerStrategy_t1405041182_StaticFields::get_offset_of_Iso8601Format_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (ReflectionUtils_t1280603200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (GetHandler_t2553917450), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (SetHandler_t3932263246), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (MemberMapLoader_t2009616003), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (CacheResolver_t3899543758), -1, sizeof(CacheResolver_t3899543758_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3024[3] = 
{
	CacheResolver_t3899543758::get_offset_of__memberMapLoader_0(),
	CacheResolver_t3899543758::get_offset_of__memberMapsCache_1(),
	CacheResolver_t3899543758_StaticFields::get_offset_of_constructorCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (CtorDelegate_t3614558424), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (MemberMap_t574666045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[4] = 
{
	MemberMap_t574666045::get_offset_of_MemberInfo_0(),
	MemberMap_t574666045::get_offset_of_Type_1(),
	MemberMap_t574666045::get_offset_of_Getter_2(),
	MemberMap_t574666045::get_offset_of_Setter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (U3CgetNewInstanceU3Ec__AnonStorey0_t3350983475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[1] = 
{
	U3CgetNewInstanceU3Ec__AnonStorey0_t3350983475::get_offset_of_constructorInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (U3CcreateGetHandlerU3Ec__AnonStorey1_t1529894103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[1] = 
{
	U3CcreateGetHandlerU3Ec__AnonStorey1_t1529894103::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (U3CcreateSetHandlerU3Ec__AnonStorey2_t1837578470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[1] = 
{
	U3CcreateSetHandlerU3Ec__AnonStorey2_t1837578470::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (U3CcreateGetHandlerU3Ec__AnonStorey3_t1529894105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[1] = 
{
	U3CcreateGetHandlerU3Ec__AnonStorey3_t1529894105::get_offset_of_getMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (U3CcreateSetHandlerU3Ec__AnonStorey4_t1837578472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[1] = 
{
	U3CcreateSetHandlerU3Ec__AnonStorey4_t1837578472::get_offset_of_setMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (P31Error_t2856600608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[5] = 
{
	P31Error_t2856600608::get_offset_of_U3CmessageU3Ek__BackingField_0(),
	P31Error_t2856600608::get_offset_of_U3CdomainU3Ek__BackingField_1(),
	P31Error_t2856600608::get_offset_of_U3CcodeU3Ek__BackingField_2(),
	P31Error_t2856600608::get_offset_of_U3CuserInfoU3Ek__BackingField_3(),
	P31Error_t2856600608::get_offset_of__containsOnlyMessage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (LifecycleHelper_t337468626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[1] = 
{
	LifecycleHelper_t337468626::get_offset_of_onApplicationPausedEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (Product_t1158373411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[7] = 
{
	Product_t1158373411::get_offset_of_Id_0(),
	Product_t1158373411::get_offset_of_Title_1(),
	Product_t1158373411::get_offset_of_Description_2(),
	Product_t1158373411::get_offset_of_Price_3(),
	Product_t1158373411::get_offset_of_Consumable_4(),
	Product_t1158373411::get_offset_of_IsoCurrencyCode_5(),
	Product_t1158373411::get_offset_of_PriceDecimal_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (Factory_t1706693194), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (AccessToken_t2518141643), -1, sizeof(AccessToken_t2518141643_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3041[6] = 
{
	AccessToken_t2518141643_StaticFields::get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0(),
	AccessToken_t2518141643::get_offset_of_U3CTokenStringU3Ek__BackingField_1(),
	AccessToken_t2518141643::get_offset_of_U3CExpirationTimeU3Ek__BackingField_2(),
	AccessToken_t2518141643::get_offset_of_U3CPermissionsU3Ek__BackingField_3(),
	AccessToken_t2518141643::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
	AccessToken_t2518141643::get_offset_of_U3CLastRefreshU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (CallbackManager_t4242095184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[2] = 
{
	CallbackManager_t4242095184::get_offset_of_facebookDelegates_0(),
	CallbackManager_t4242095184::get_offset_of_nextAsyncId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (ComponentFactory_t2270101009), -1, sizeof(ComponentFactory_t2270101009_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3043[1] = 
{
	ComponentFactory_t2270101009_StaticFields::get_offset_of_facebookGameObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (IfNotExist_t643838650)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3044[3] = 
{
	IfNotExist_t643838650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (Constants_t3246017429), -1, sizeof(Constants_t3246017429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3045[1] = 
{
	Constants_t3246017429_StaticFields::get_offset_of_currentPlatform_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (FB_t1612658294), -1, sizeof(FB_t1612658294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3046[8] = 
{
	0,
	FB_t1612658294_StaticFields::get_offset_of_facebook_3(),
	FB_t1612658294_StaticFields::get_offset_of_isInitCalled_4(),
	FB_t1612658294_StaticFields::get_offset_of_facebookDomain_5(),
	FB_t1612658294_StaticFields::get_offset_of_graphApiVersion_6(),
	FB_t1612658294_StaticFields::get_offset_of_U3CAppIdU3Ek__BackingField_7(),
	FB_t1612658294_StaticFields::get_offset_of_U3CClientTokenU3Ek__BackingField_8(),
	FB_t1612658294_StaticFields::get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (OnDLLLoaded_t1727406294), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (Canvas_t2415435652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (Mobile_t3800732530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (CompiledFacebookLoader_t3305868676), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (U3CInitU3Ec__AnonStorey0_t4214738661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[10] = 
{
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_onInitComplete_0(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_appId_1(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_cookie_2(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_logging_3(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_status_4(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_xfbml_5(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_authResponse_6(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_frictionlessRequests_7(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_javascriptSDKLocale_8(),
	U3CInitU3Ec__AnonStorey0_t4214738661::get_offset_of_onHideUnity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (FacebookBase_t2463540723), -1, sizeof(FacebookBase_t2463540723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3052[4] = 
{
	FacebookBase_t2463540723::get_offset_of_onInitCompleteDelegate_0(),
	FacebookBase_t2463540723::get_offset_of_U3CInitializedU3Ek__BackingField_1(),
	FacebookBase_t2463540723::get_offset_of_U3CCallbackManagerU3Ek__BackingField_2(),
	FacebookBase_t2463540723_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (InitDelegate_t3410465555), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (HideUnityDelegate_t712804158), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (FacebookGameObject_t2922570967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[1] = 
{
	FacebookGameObject_t2922570967::get_offset_of_U3CFacebookU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (FacebookSdkVersion_t1532426400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (FacebookUnityPlatform_t1867507902)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3058[6] = 
{
	FacebookUnityPlatform_t1867507902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (MethodArguments_t735664811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[1] = 
{
	MethodArguments_t735664811::get_offset_of_arguments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (ShareDialogMode_t1445392044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3066[5] = 
{
	ShareDialogMode_t1445392044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (OGActionType_t1978093408)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3067[4] = 
{
	OGActionType_t1978093408::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (CanvasFacebook_t1390391306), -1, sizeof(CanvasFacebook_t1390391306_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3068[6] = 
{
	CanvasFacebook_t1390391306::get_offset_of_appId_4(),
	CanvasFacebook_t1390391306::get_offset_of_appLinkUrl_5(),
	CanvasFacebook_t1390391306::get_offset_of_canvasJSWrapper_6(),
	CanvasFacebook_t1390391306::get_offset_of_onHideUnityDelegate_7(),
	CanvasFacebook_t1390391306::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_8(),
	CanvasFacebook_t1390391306_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (U3CFormatAuthResponseU3Ec__AnonStorey0_t3137418185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[2] = 
{
	U3CFormatAuthResponseU3Ec__AnonStorey0_t3137418185::get_offset_of_result_0(),
	U3CFormatAuthResponseU3Ec__AnonStorey0_t3137418185::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (CanvasFacebookGameObject_t1732848805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (CanvasFacebookLoader_t1985905999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (CanvasJSWrapper_t3429491632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (JsBridge_t2435492180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[1] = 
{
	JsBridge_t2435492180::get_offset_of_facebook_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (ArcadeFacebook_t622171242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[3] = 
{
	ArcadeFacebook_t622171242::get_offset_of_appId_4(),
	ArcadeFacebook_t622171242::get_offset_of_arcadeWrapper_5(),
	ArcadeFacebook_t622171242::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (OnComplete_t3968443998), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (ArcadeFacebookGameObject_t1372893397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[6] = 
{
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_onCompleteDelegate_0(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_callbackId_1(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24this_2(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24current_3(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24disposing_4(),
	U3CWaitForPipeResponseU3Ec__Iterator0_t1353419607::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (ArcadeFacebookLoader_t3930015791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (MobileFacebook_t2099931062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3093[1] = 
{
	MobileFacebook_t2099931062::get_offset_of_shareDialogMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (MobileFacebookGameObject_t668179329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (AndroidFacebook_t3038444049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[3] = 
{
	AndroidFacebook_t3038444049::get_offset_of_limitEventUsage_5(),
	AndroidFacebook_t3038444049::get_offset_of_androidWrapper_6(),
	AndroidFacebook_t3038444049::get_offset_of_U3CKeyHashU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (AndroidFacebookGameObject_t2149209708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (AndroidFacebookLoader_t3776634754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
