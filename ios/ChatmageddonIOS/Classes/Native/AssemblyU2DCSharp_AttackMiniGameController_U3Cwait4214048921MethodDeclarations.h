﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackMiniGameController/<wait>c__Iterator1
struct U3CwaitU3Ec__Iterator1_t4214048921;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackMiniGameController/<wait>c__Iterator1::.ctor()
extern "C"  void U3CwaitU3Ec__Iterator1__ctor_m4156929574 (U3CwaitU3Ec__Iterator1_t4214048921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AttackMiniGameController/<wait>c__Iterator1::MoveNext()
extern "C"  bool U3CwaitU3Ec__Iterator1_MoveNext_m1871692138 (U3CwaitU3Ec__Iterator1_t4214048921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackMiniGameController/<wait>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1389819028 (U3CwaitU3Ec__Iterator1_t4214048921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AttackMiniGameController/<wait>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3454034236 (U3CwaitU3Ec__Iterator1_t4214048921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController/<wait>c__Iterator1::Dispose()
extern "C"  void U3CwaitU3Ec__Iterator1_Dispose_m2455458011 (U3CwaitU3Ec__Iterator1_t4214048921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController/<wait>c__Iterator1::Reset()
extern "C"  void U3CwaitU3Ec__Iterator1_Reset_m2408373881 (U3CwaitU3Ec__Iterator1_t4214048921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
