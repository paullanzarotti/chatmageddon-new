﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMBase/<GetTagValue>c__AnonStorey0
struct U3CGetTagValueU3Ec__AnonStorey0_t1279171491;
// OnlineMapsOSMTag
struct OnlineMapsOSMTag_t3629071465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMTag3629071465.h"

// System.Void OnlineMapsOSMBase/<GetTagValue>c__AnonStorey0::.ctor()
extern "C"  void U3CGetTagValueU3Ec__AnonStorey0__ctor_m4212522632 (U3CGetTagValueU3Ec__AnonStorey0_t1279171491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsOSMBase/<GetTagValue>c__AnonStorey0::<>m__0(OnlineMapsOSMTag)
extern "C"  bool U3CGetTagValueU3Ec__AnonStorey0_U3CU3Em__0_m3055673456 (U3CGetTagValueU3Ec__AnonStorey0_t1279171491 * __this, OnlineMapsOSMTag_t3629071465 * ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
