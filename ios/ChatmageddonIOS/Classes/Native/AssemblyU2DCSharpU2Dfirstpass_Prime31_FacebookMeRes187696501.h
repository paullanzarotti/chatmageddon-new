﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Prime31.FacebookMeResult/FacebookMeHometown
struct FacebookMeHometown_t3213935844;
// Prime31.FacebookMeResult/FacebookMeLocation
struct FacebookMeLocation_t1093501248;

#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookBaseDT79607460.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookMeResult
struct  FacebookMeResult_t187696501  : public FacebookBaseDTO_t79607460
{
public:
	// System.String Prime31.FacebookMeResult::id
	String_t* ___id_0;
	// System.String Prime31.FacebookMeResult::name
	String_t* ___name_1;
	// System.String Prime31.FacebookMeResult::first_name
	String_t* ___first_name_2;
	// System.String Prime31.FacebookMeResult::last_name
	String_t* ___last_name_3;
	// System.String Prime31.FacebookMeResult::link
	String_t* ___link_4;
	// System.String Prime31.FacebookMeResult::username
	String_t* ___username_5;
	// Prime31.FacebookMeResult/FacebookMeHometown Prime31.FacebookMeResult::hometown
	FacebookMeHometown_t3213935844 * ___hometown_6;
	// Prime31.FacebookMeResult/FacebookMeLocation Prime31.FacebookMeResult::location
	FacebookMeLocation_t1093501248 * ___location_7;
	// System.String Prime31.FacebookMeResult::gender
	String_t* ___gender_8;
	// System.String Prime31.FacebookMeResult::email
	String_t* ___email_9;
	// System.Int32 Prime31.FacebookMeResult::timezone
	int32_t ___timezone_10;
	// System.String Prime31.FacebookMeResult::locale
	String_t* ___locale_11;
	// System.Boolean Prime31.FacebookMeResult::verified
	bool ___verified_12;
	// System.DateTime Prime31.FacebookMeResult::updated_time
	DateTime_t693205669  ___updated_time_13;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_first_name_2() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___first_name_2)); }
	inline String_t* get_first_name_2() const { return ___first_name_2; }
	inline String_t** get_address_of_first_name_2() { return &___first_name_2; }
	inline void set_first_name_2(String_t* value)
	{
		___first_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___first_name_2, value);
	}

	inline static int32_t get_offset_of_last_name_3() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___last_name_3)); }
	inline String_t* get_last_name_3() const { return ___last_name_3; }
	inline String_t** get_address_of_last_name_3() { return &___last_name_3; }
	inline void set_last_name_3(String_t* value)
	{
		___last_name_3 = value;
		Il2CppCodeGenWriteBarrier(&___last_name_3, value);
	}

	inline static int32_t get_offset_of_link_4() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___link_4)); }
	inline String_t* get_link_4() const { return ___link_4; }
	inline String_t** get_address_of_link_4() { return &___link_4; }
	inline void set_link_4(String_t* value)
	{
		___link_4 = value;
		Il2CppCodeGenWriteBarrier(&___link_4, value);
	}

	inline static int32_t get_offset_of_username_5() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___username_5)); }
	inline String_t* get_username_5() const { return ___username_5; }
	inline String_t** get_address_of_username_5() { return &___username_5; }
	inline void set_username_5(String_t* value)
	{
		___username_5 = value;
		Il2CppCodeGenWriteBarrier(&___username_5, value);
	}

	inline static int32_t get_offset_of_hometown_6() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___hometown_6)); }
	inline FacebookMeHometown_t3213935844 * get_hometown_6() const { return ___hometown_6; }
	inline FacebookMeHometown_t3213935844 ** get_address_of_hometown_6() { return &___hometown_6; }
	inline void set_hometown_6(FacebookMeHometown_t3213935844 * value)
	{
		___hometown_6 = value;
		Il2CppCodeGenWriteBarrier(&___hometown_6, value);
	}

	inline static int32_t get_offset_of_location_7() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___location_7)); }
	inline FacebookMeLocation_t1093501248 * get_location_7() const { return ___location_7; }
	inline FacebookMeLocation_t1093501248 ** get_address_of_location_7() { return &___location_7; }
	inline void set_location_7(FacebookMeLocation_t1093501248 * value)
	{
		___location_7 = value;
		Il2CppCodeGenWriteBarrier(&___location_7, value);
	}

	inline static int32_t get_offset_of_gender_8() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___gender_8)); }
	inline String_t* get_gender_8() const { return ___gender_8; }
	inline String_t** get_address_of_gender_8() { return &___gender_8; }
	inline void set_gender_8(String_t* value)
	{
		___gender_8 = value;
		Il2CppCodeGenWriteBarrier(&___gender_8, value);
	}

	inline static int32_t get_offset_of_email_9() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___email_9)); }
	inline String_t* get_email_9() const { return ___email_9; }
	inline String_t** get_address_of_email_9() { return &___email_9; }
	inline void set_email_9(String_t* value)
	{
		___email_9 = value;
		Il2CppCodeGenWriteBarrier(&___email_9, value);
	}

	inline static int32_t get_offset_of_timezone_10() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___timezone_10)); }
	inline int32_t get_timezone_10() const { return ___timezone_10; }
	inline int32_t* get_address_of_timezone_10() { return &___timezone_10; }
	inline void set_timezone_10(int32_t value)
	{
		___timezone_10 = value;
	}

	inline static int32_t get_offset_of_locale_11() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___locale_11)); }
	inline String_t* get_locale_11() const { return ___locale_11; }
	inline String_t** get_address_of_locale_11() { return &___locale_11; }
	inline void set_locale_11(String_t* value)
	{
		___locale_11 = value;
		Il2CppCodeGenWriteBarrier(&___locale_11, value);
	}

	inline static int32_t get_offset_of_verified_12() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___verified_12)); }
	inline bool get_verified_12() const { return ___verified_12; }
	inline bool* get_address_of_verified_12() { return &___verified_12; }
	inline void set_verified_12(bool value)
	{
		___verified_12 = value;
	}

	inline static int32_t get_offset_of_updated_time_13() { return static_cast<int32_t>(offsetof(FacebookMeResult_t187696501, ___updated_time_13)); }
	inline DateTime_t693205669  get_updated_time_13() const { return ___updated_time_13; }
	inline DateTime_t693205669 * get_address_of_updated_time_13() { return &___updated_time_13; }
	inline void set_updated_time_13(DateTime_t693205669  value)
	{
		___updated_time_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
