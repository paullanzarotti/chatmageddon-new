﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`1<System.Collections.Generic.IEnumerator`1<PhoneNumbers.PhoneNumberMatch>>
struct Func_1_t1593775089;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.EnumerableFromConstructor`1<PhoneNumbers.PhoneNumberMatch>
struct  EnumerableFromConstructor_1_t1496639844  : public Il2CppObject
{
public:
	// System.Func`1<System.Collections.Generic.IEnumerator`1<T>> PhoneNumbers.EnumerableFromConstructor`1::fn_
	Func_1_t1593775089 * ___fn__0;

public:
	inline static int32_t get_offset_of_fn__0() { return static_cast<int32_t>(offsetof(EnumerableFromConstructor_1_t1496639844, ___fn__0)); }
	inline Func_1_t1593775089 * get_fn__0() const { return ___fn__0; }
	inline Func_1_t1593775089 ** get_address_of_fn__0() { return &___fn__0; }
	inline void set_fn__0(Func_1_t1593775089 * value)
	{
		___fn__0 = value;
		Il2CppCodeGenWriteBarrier(&___fn__0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
