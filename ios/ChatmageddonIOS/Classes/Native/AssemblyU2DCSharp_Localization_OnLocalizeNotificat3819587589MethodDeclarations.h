﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Localization/OnLocalizeNotification
struct OnLocalizeNotification_t3819587589;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Localization/OnLocalizeNotification::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLocalizeNotification__ctor_m2855163438 (OnLocalizeNotification_t3819587589 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Localization/OnLocalizeNotification::Invoke()
extern "C"  void OnLocalizeNotification_Invoke_m4074283148 (OnLocalizeNotification_t3819587589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Localization/OnLocalizeNotification::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLocalizeNotification_BeginInvoke_m3477260171 (OnLocalizeNotification_t3819587589 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Localization/OnLocalizeNotification::EndInvoke(System.IAsyncResult)
extern "C"  void OnLocalizeNotification_EndInvoke_m2103749816 (OnLocalizeNotification_t3819587589 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
