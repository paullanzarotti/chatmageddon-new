﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<FAQ>
struct List_1_t2207118780;
// BlockILP
struct BlockILP_t879176280;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2270524952.h"
#include "AssemblyU2DCSharp_AboutAndHelpUI3809615169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsManager
struct  SettingsManager_t2519859232  : public MonoSingleton_1_t2270524952
{
public:
	// AboutAndHelpUI SettingsManager::uiToLoad
	int32_t ___uiToLoad_3;
	// System.String SettingsManager::questionText
	String_t* ___questionText_4;
	// System.String SettingsManager::anwserText
	String_t* ___anwserText_5;
	// System.String SettingsManager::instructionsText
	String_t* ___instructionsText_6;
	// System.String SettingsManager::eulaText
	String_t* ___eulaText_7;
	// System.String SettingsManager::privacyPolicyText
	String_t* ___privacyPolicyText_8;
	// System.String SettingsManager::tosText
	String_t* ___tosText_9;
	// System.Collections.Generic.List`1<FAQ> SettingsManager::faqList
	List_1_t2207118780 * ___faqList_10;
	// BlockILP SettingsManager::blockList
	BlockILP_t879176280 * ___blockList_11;

public:
	inline static int32_t get_offset_of_uiToLoad_3() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___uiToLoad_3)); }
	inline int32_t get_uiToLoad_3() const { return ___uiToLoad_3; }
	inline int32_t* get_address_of_uiToLoad_3() { return &___uiToLoad_3; }
	inline void set_uiToLoad_3(int32_t value)
	{
		___uiToLoad_3 = value;
	}

	inline static int32_t get_offset_of_questionText_4() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___questionText_4)); }
	inline String_t* get_questionText_4() const { return ___questionText_4; }
	inline String_t** get_address_of_questionText_4() { return &___questionText_4; }
	inline void set_questionText_4(String_t* value)
	{
		___questionText_4 = value;
		Il2CppCodeGenWriteBarrier(&___questionText_4, value);
	}

	inline static int32_t get_offset_of_anwserText_5() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___anwserText_5)); }
	inline String_t* get_anwserText_5() const { return ___anwserText_5; }
	inline String_t** get_address_of_anwserText_5() { return &___anwserText_5; }
	inline void set_anwserText_5(String_t* value)
	{
		___anwserText_5 = value;
		Il2CppCodeGenWriteBarrier(&___anwserText_5, value);
	}

	inline static int32_t get_offset_of_instructionsText_6() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___instructionsText_6)); }
	inline String_t* get_instructionsText_6() const { return ___instructionsText_6; }
	inline String_t** get_address_of_instructionsText_6() { return &___instructionsText_6; }
	inline void set_instructionsText_6(String_t* value)
	{
		___instructionsText_6 = value;
		Il2CppCodeGenWriteBarrier(&___instructionsText_6, value);
	}

	inline static int32_t get_offset_of_eulaText_7() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___eulaText_7)); }
	inline String_t* get_eulaText_7() const { return ___eulaText_7; }
	inline String_t** get_address_of_eulaText_7() { return &___eulaText_7; }
	inline void set_eulaText_7(String_t* value)
	{
		___eulaText_7 = value;
		Il2CppCodeGenWriteBarrier(&___eulaText_7, value);
	}

	inline static int32_t get_offset_of_privacyPolicyText_8() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___privacyPolicyText_8)); }
	inline String_t* get_privacyPolicyText_8() const { return ___privacyPolicyText_8; }
	inline String_t** get_address_of_privacyPolicyText_8() { return &___privacyPolicyText_8; }
	inline void set_privacyPolicyText_8(String_t* value)
	{
		___privacyPolicyText_8 = value;
		Il2CppCodeGenWriteBarrier(&___privacyPolicyText_8, value);
	}

	inline static int32_t get_offset_of_tosText_9() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___tosText_9)); }
	inline String_t* get_tosText_9() const { return ___tosText_9; }
	inline String_t** get_address_of_tosText_9() { return &___tosText_9; }
	inline void set_tosText_9(String_t* value)
	{
		___tosText_9 = value;
		Il2CppCodeGenWriteBarrier(&___tosText_9, value);
	}

	inline static int32_t get_offset_of_faqList_10() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___faqList_10)); }
	inline List_1_t2207118780 * get_faqList_10() const { return ___faqList_10; }
	inline List_1_t2207118780 ** get_address_of_faqList_10() { return &___faqList_10; }
	inline void set_faqList_10(List_1_t2207118780 * value)
	{
		___faqList_10 = value;
		Il2CppCodeGenWriteBarrier(&___faqList_10, value);
	}

	inline static int32_t get_offset_of_blockList_11() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232, ___blockList_11)); }
	inline BlockILP_t879176280 * get_blockList_11() const { return ___blockList_11; }
	inline BlockILP_t879176280 ** get_address_of_blockList_11() { return &___blockList_11; }
	inline void set_blockList_11(BlockILP_t879176280 * value)
	{
		___blockList_11 = value;
		Il2CppCodeGenWriteBarrier(&___blockList_11, value);
	}
};

struct SettingsManager_t2519859232_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> SettingsManager::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(SettingsManager_t2519859232_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
