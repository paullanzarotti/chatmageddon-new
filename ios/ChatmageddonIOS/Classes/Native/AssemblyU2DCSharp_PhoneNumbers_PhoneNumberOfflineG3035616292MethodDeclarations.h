﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberOfflineGeocoder
struct PhoneNumberOfflineGeocoder_t3035616292;
// System.String
struct String_t;
// PhoneNumbers.AreaCodeMap
struct AreaCodeMap_t3230759372;
// PhoneNumbers.Locale
struct Locale_t2376277312;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhoneNumbers_Locale2376277312.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"

// System.Void PhoneNumbers.PhoneNumberOfflineGeocoder::.ctor(System.String)
extern "C"  void PhoneNumberOfflineGeocoder__ctor_m1900375535 (PhoneNumberOfflineGeocoder_t3035616292 * __this, String_t* ___phonePrefixDataDirectory0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberOfflineGeocoder::LoadMappingFileProvider()
extern "C"  void PhoneNumberOfflineGeocoder_LoadMappingFileProvider_m1113054972 (PhoneNumberOfflineGeocoder_t3035616292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.AreaCodeMap PhoneNumbers.PhoneNumberOfflineGeocoder::GetPhonePrefixDescriptions(System.Int32,System.String,System.String,System.String)
extern "C"  AreaCodeMap_t3230759372 * PhoneNumberOfflineGeocoder_GetPhonePrefixDescriptions_m1470933176 (PhoneNumberOfflineGeocoder_t3035616292 * __this, int32_t ___prefixMapKey0, String_t* ___language1, String_t* ___script2, String_t* ___region3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberOfflineGeocoder::LoadAreaCodeMapFromFile(System.String)
extern "C"  void PhoneNumberOfflineGeocoder_LoadAreaCodeMapFromFile_m3528832747 (PhoneNumberOfflineGeocoder_t3035616292 * __this, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneNumberOfflineGeocoder PhoneNumbers.PhoneNumberOfflineGeocoder::GetInstance()
extern "C"  PhoneNumberOfflineGeocoder_t3035616292 * PhoneNumberOfflineGeocoder_GetInstance_m799170351 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberOfflineGeocoder::LoadDataFile(PhoneNumbers.Locale,System.Int32)
extern "C"  void PhoneNumberOfflineGeocoder_LoadDataFile_m2344585816 (PhoneNumberOfflineGeocoder_t3035616292 * __this, Locale_t2376277312 * ___locale0, int32_t ___countryCallingCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::GetCountryNameForNumber(PhoneNumbers.PhoneNumber,PhoneNumbers.Locale)
extern "C"  String_t* PhoneNumberOfflineGeocoder_GetCountryNameForNumber_m1638484364 (PhoneNumberOfflineGeocoder_t3035616292 * __this, PhoneNumber_t814071929 * ___number0, Locale_t2376277312 * ___language1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::GetRegionDisplayName(System.String,PhoneNumbers.Locale)
extern "C"  String_t* PhoneNumberOfflineGeocoder_GetRegionDisplayName_m2243315107 (PhoneNumberOfflineGeocoder_t3035616292 * __this, String_t* ___regionCode0, Locale_t2376277312 * ___language1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::GetDescriptionForValidNumber(PhoneNumbers.PhoneNumber,PhoneNumbers.Locale)
extern "C"  String_t* PhoneNumberOfflineGeocoder_GetDescriptionForValidNumber_m2596438729 (PhoneNumberOfflineGeocoder_t3035616292 * __this, PhoneNumber_t814071929 * ___number0, Locale_t2376277312 * ___languageCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::GetDescriptionForValidNumber(PhoneNumbers.PhoneNumber,PhoneNumbers.Locale,System.String)
extern "C"  String_t* PhoneNumberOfflineGeocoder_GetDescriptionForValidNumber_m1810991615 (PhoneNumberOfflineGeocoder_t3035616292 * __this, PhoneNumber_t814071929 * ___number0, Locale_t2376277312 * ___languageCode1, String_t* ___userRegion2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::GetDescriptionForNumber(PhoneNumbers.PhoneNumber,PhoneNumbers.Locale)
extern "C"  String_t* PhoneNumberOfflineGeocoder_GetDescriptionForNumber_m3494147715 (PhoneNumberOfflineGeocoder_t3035616292 * __this, PhoneNumber_t814071929 * ___number0, Locale_t2376277312 * ___languageCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::GetDescriptionForNumber(PhoneNumbers.PhoneNumber,PhoneNumbers.Locale,System.String)
extern "C"  String_t* PhoneNumberOfflineGeocoder_GetDescriptionForNumber_m3991794105 (PhoneNumberOfflineGeocoder_t3035616292 * __this, PhoneNumber_t814071929 * ___number0, Locale_t2376277312 * ___languageCode1, String_t* ___userRegion2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PhoneNumbers.PhoneNumberOfflineGeocoder::GetAreaDescriptionForNumber(PhoneNumbers.PhoneNumber,System.String,System.String,System.String)
extern "C"  String_t* PhoneNumberOfflineGeocoder_GetAreaDescriptionForNumber_m1996652626 (PhoneNumberOfflineGeocoder_t3035616292 * __this, PhoneNumber_t814071929 * ___number0, String_t* ___lang1, String_t* ___script2, String_t* ___region3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberOfflineGeocoder::MayFallBackToEnglish(System.String)
extern "C"  bool PhoneNumberOfflineGeocoder_MayFallBackToEnglish_m626387331 (PhoneNumberOfflineGeocoder_t3035616292 * __this, String_t* ___lang0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumbers.PhoneNumberOfflineGeocoder::.cctor()
extern "C"  void PhoneNumberOfflineGeocoder__cctor_m254473648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
