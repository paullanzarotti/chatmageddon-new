﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationController_2_gen760270839MethodDeclarations.h"

// System.Void NavigationController`2<StatusNavigationController,StatusNavScreen>::.ctor()
#define NavigationController_2__ctor_m782242134(__this, method) ((  void (*) (NavigationController_2_t1214554782 *, const MethodInfo*))NavigationController_2__ctor_m887236312_gshared)(__this, method)
// System.Void NavigationController`2<StatusNavigationController,StatusNavScreen>::SetupStartScreen(NavScreenType)
#define NavigationController_2_SetupStartScreen_m977273500(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t1214554782 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m3463568014_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<StatusNavigationController,StatusNavScreen>::NavigateBackwards()
#define NavigationController_2_NavigateBackwards_m1762961225(__this, method) ((  void (*) (NavigationController_2_t1214554782 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m622743621_gshared)(__this, method)
// System.Void NavigationController`2<StatusNavigationController,StatusNavScreen>::NavigateForwards(NavScreenType)
#define NavigationController_2_NavigateForwards_m3454937554(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t1214554782 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m1563548144_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<StatusNavigationController,StatusNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
#define NavigationController_2_ValidateNavigation_m3320030294(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t1214554782 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m806311540_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<StatusNavigationController,StatusNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
#define NavigationController_2_SaveScreenContent_m221423559(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t1214554782 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m2919636131_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<StatusNavigationController,StatusNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
#define NavigationController_2_LoadNavScreen_m505011792(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t1214554782 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m3436155602_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<StatusNavigationController,StatusNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
#define NavigationController_2_UnloadNavScreen_m2641132286(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t1214554782 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m680577980_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<StatusNavigationController,StatusNavScreen>::PopulateTitleBar(NavScreenType)
#define NavigationController_2_PopulateTitleBar_m2083648134(__this, ___screen0, method) ((  void (*) (NavigationController_2_t1214554782 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m3786128028_gshared)(__this, ___screen0, method)
