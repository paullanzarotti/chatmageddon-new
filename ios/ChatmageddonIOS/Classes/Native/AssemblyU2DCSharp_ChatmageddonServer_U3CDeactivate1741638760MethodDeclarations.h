﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<DeactivateUserStealthMode>c__AnonStorey1F
struct U3CDeactivateUserStealthModeU3Ec__AnonStorey1F_t1741638760;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<DeactivateUserStealthMode>c__AnonStorey1F::.ctor()
extern "C"  void U3CDeactivateUserStealthModeU3Ec__AnonStorey1F__ctor_m2743575971 (U3CDeactivateUserStealthModeU3Ec__AnonStorey1F_t1741638760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<DeactivateUserStealthMode>c__AnonStorey1F::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeactivateUserStealthModeU3Ec__AnonStorey1F_U3CU3Em__0_m2047230524 (U3CDeactivateUserStealthModeU3Ec__AnonStorey1F_t1741638760 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
