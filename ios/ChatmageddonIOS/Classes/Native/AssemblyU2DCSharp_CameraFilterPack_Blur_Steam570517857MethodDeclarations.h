﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Steam
struct CameraFilterPack_Blur_Steam_t570517857;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Steam::.ctor()
extern "C"  void CameraFilterPack_Blur_Steam__ctor_m2228841886 (CameraFilterPack_Blur_Steam_t570517857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Steam::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Steam_get_material_m226224397 (CameraFilterPack_Blur_Steam_t570517857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::Start()
extern "C"  void CameraFilterPack_Blur_Steam_Start_m905744742 (CameraFilterPack_Blur_Steam_t570517857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Steam_OnRenderImage_m1891766102 (CameraFilterPack_Blur_Steam_t570517857 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::OnValidate()
extern "C"  void CameraFilterPack_Blur_Steam_OnValidate_m2045780599 (CameraFilterPack_Blur_Steam_t570517857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::Update()
extern "C"  void CameraFilterPack_Blur_Steam_Update_m1507717545 (CameraFilterPack_Blur_Steam_t570517857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::OnDisable()
extern "C"  void CameraFilterPack_Blur_Steam_OnDisable_m1819388797 (CameraFilterPack_Blur_Steam_t570517857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
