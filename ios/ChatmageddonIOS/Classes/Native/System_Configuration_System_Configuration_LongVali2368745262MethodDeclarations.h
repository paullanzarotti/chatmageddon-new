﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.LongValidatorAttribute
struct LongValidatorAttribute_t2368745262;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Configuration.LongValidatorAttribute::.ctor()
extern "C"  void LongValidatorAttribute__ctor_m2344197185 (LongValidatorAttribute_t2368745262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.LongValidatorAttribute::get_ExcludeRange()
extern "C"  bool LongValidatorAttribute_get_ExcludeRange_m1156194477 (LongValidatorAttribute_t2368745262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.LongValidatorAttribute::set_ExcludeRange(System.Boolean)
extern "C"  void LongValidatorAttribute_set_ExcludeRange_m1852901460 (LongValidatorAttribute_t2368745262 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Configuration.LongValidatorAttribute::get_MaxValue()
extern "C"  int64_t LongValidatorAttribute_get_MaxValue_m853270030 (LongValidatorAttribute_t2368745262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.LongValidatorAttribute::set_MaxValue(System.Int64)
extern "C"  void LongValidatorAttribute_set_MaxValue_m798245217 (LongValidatorAttribute_t2368745262 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Configuration.LongValidatorAttribute::get_MinValue()
extern "C"  int64_t LongValidatorAttribute_get_MinValue_m2054254252 (LongValidatorAttribute_t2368745262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.LongValidatorAttribute::set_MinValue(System.Int64)
extern "C"  void LongValidatorAttribute_set_MinValue_m3715226123 (LongValidatorAttribute_t2368745262 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.LongValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * LongValidatorAttribute_get_ValidatorInstance_m356262336 (LongValidatorAttribute_t2368745262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
