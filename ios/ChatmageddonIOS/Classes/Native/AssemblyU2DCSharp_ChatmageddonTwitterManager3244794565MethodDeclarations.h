﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonTwitterManager
struct ChatmageddonTwitterManager_t3244794565;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatmageddonTwitterManager::.ctor()
extern "C"  void ChatmageddonTwitterManager__ctor_m3536580194 (ChatmageddonTwitterManager_t3244794565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonTwitterManager::TwitterLoginSuccess()
extern "C"  void ChatmageddonTwitterManager_TwitterLoginSuccess_m4268456221 (ChatmageddonTwitterManager_t3244794565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonTwitterManager::TwitterRequestSuccess(System.String,System.String)
extern "C"  void ChatmageddonTwitterManager_TwitterRequestSuccess_m3457420347 (ChatmageddonTwitterManager_t3244794565 * __this, String_t* ___code0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonTwitterManager::TwitterRequestFailed(System.String)
extern "C"  void ChatmageddonTwitterManager_TwitterRequestFailed_m4248340461 (ChatmageddonTwitterManager_t3244794565 * __this, String_t* ___failString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
