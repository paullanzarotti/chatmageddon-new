﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Uniject.Impl.UnityResourceLoader
struct UnityResourceLoader_t3124360604;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Uniject.Impl.UnityResourceLoader::.ctor()
extern "C"  void UnityResourceLoader__ctor_m3597579063 (UnityResourceLoader_t3124360604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextReader Uniject.Impl.UnityResourceLoader::openTextFile(System.String)
extern "C"  TextReader_t1561828458 * UnityResourceLoader_openTextFile_m1456581342 (UnityResourceLoader_t3124360604 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
