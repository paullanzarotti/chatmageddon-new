﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookShareButton
struct FacebookShareButton_t509072349;

#include "codegen/il2cpp-codegen.h"

// System.Void FacebookShareButton::.ctor()
extern "C"  void FacebookShareButton__ctor_m3780095574 (FacebookShareButton_t509072349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookShareButton::OnButtonClick()
extern "C"  void FacebookShareButton_OnButtonClick_m1515714817 (FacebookShareButton_t509072349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
