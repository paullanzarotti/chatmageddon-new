﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StandardMiniGame
struct StandardMiniGame_t3672006024;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"

// System.Void StandardMiniGame::.ctor()
extern "C"  void StandardMiniGame__ctor_m2715195669 (StandardMiniGame_t3672006024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardMiniGame::StartMiniGame()
extern "C"  void StandardMiniGame_StartMiniGame_m3230692406 (StandardMiniGame_t3672006024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardMiniGame::OnCountdownFinished()
extern "C"  void StandardMiniGame_OnCountdownFinished_m286483623 (StandardMiniGame_t3672006024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardMiniGame::StopMiniGame()
extern "C"  void StandardMiniGame_StopMiniGame_m2858743746 (StandardMiniGame_t3672006024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardMiniGame::OnPaused()
extern "C"  void StandardMiniGame_OnPaused_m4148156624 (StandardMiniGame_t3672006024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardMiniGame::ResetMiniGame()
extern "C"  void StandardMiniGame_ResetMiniGame_m954836093 (StandardMiniGame_t3672006024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardMiniGame::TimerUpdated(System.TimeSpan)
extern "C"  void StandardMiniGame_TimerUpdated_m1863873841 (StandardMiniGame_t3672006024 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StandardMiniGame::UpdateModel()
extern "C"  void StandardMiniGame_UpdateModel_m2443053635 (StandardMiniGame_t3672006024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
