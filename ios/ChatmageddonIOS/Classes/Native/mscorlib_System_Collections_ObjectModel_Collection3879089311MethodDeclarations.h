﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<ChatNavScreen>
struct Collection_1_t3879089311;
// System.Collections.Generic.IList`1<ChatNavScreen>
struct IList_1_t583317862;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// ChatNavScreen[]
struct ChatNavScreenU5BU5D_t2994307200;
// System.Collections.Generic.IEnumerator`1<ChatNavScreen>
struct IEnumerator_1_t1812868384;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"

// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m3993501424_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3993501424(__this, method) ((  void (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1__ctor_m3993501424_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m1498938201_gshared (Collection_1_t3879089311 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m1498938201(__this, ___list0, method) ((  void (*) (Collection_1_t3879089311 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m1498938201_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3794140271_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3794140271(__this, method) ((  bool (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3794140271_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1557676844_gshared (Collection_1_t3879089311 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1557676844(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3879089311 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1557676844_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2563890843_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2563890843(__this, method) ((  Il2CppObject * (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2563890843_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2867328536_gshared (Collection_1_t3879089311 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2867328536(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3879089311 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2867328536_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m151363790_gshared (Collection_1_t3879089311 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m151363790(__this, ___value0, method) ((  bool (*) (Collection_1_t3879089311 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m151363790_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1620354962_gshared (Collection_1_t3879089311 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1620354962(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3879089311 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1620354962_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2253139299_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2253139299(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2253139299_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m825718315_gshared (Collection_1_t3879089311 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m825718315(__this, ___value0, method) ((  void (*) (Collection_1_t3879089311 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m825718315_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2855486480_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2855486480(__this, method) ((  bool (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2855486480_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1201770556_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1201770556(__this, method) ((  Il2CppObject * (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1201770556_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1796574555_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1796574555(__this, method) ((  bool (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1796574555_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3254793764_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3254793764(__this, method) ((  bool (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3254793764_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1222821251_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1222821251(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1222821251_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3173987382_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3173987382(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3173987382_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m279554527_gshared (Collection_1_t3879089311 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m279554527(__this, ___item0, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_Add_m279554527_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m4171077939_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_Clear_m4171077939(__this, method) ((  void (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_Clear_m4171077939_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m545337933_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m545337933(__this, method) ((  void (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_ClearItems_m545337933_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m1646805021_gshared (Collection_1_t3879089311 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1646805021(__this, ___item0, method) ((  bool (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_Contains_m1646805021_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m142523515_gshared (Collection_1_t3879089311 * __this, ChatNavScreenU5BU5D_t2994307200* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m142523515(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3879089311 *, ChatNavScreenU5BU5D_t2994307200*, int32_t, const MethodInfo*))Collection_1_CopyTo_m142523515_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<ChatNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3479591076_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3479591076(__this, method) ((  Il2CppObject* (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_GetEnumerator_m3479591076_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ChatNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3406204319_gshared (Collection_1_t3879089311 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3406204319(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m3406204319_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2351591482_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2351591482(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m2351591482_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1520409797_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1520409797(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1520409797_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m82809780_gshared (Collection_1_t3879089311 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m82809780(__this, ___item0, method) ((  bool (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_Remove_m82809780_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1401622318_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1401622318(__this, ___index0, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1401622318_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1096330296_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1096330296(__this, ___index0, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1096330296_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<ChatNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3694143016_gshared (Collection_1_t3879089311 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3694143016(__this, method) ((  int32_t (*) (Collection_1_t3879089311 *, const MethodInfo*))Collection_1_get_Count_m3694143016_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<ChatNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m2165458488_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2165458488(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3879089311 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2165458488_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3128178351_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3128178351(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m3128178351_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3505795702_gshared (Collection_1_t3879089311 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3505795702(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3879089311 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m3505795702_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2494149353_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2494149353(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2494149353_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<ChatNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m3656396781_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3656396781(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3656396781_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<ChatNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3677684561_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3677684561(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3677684561_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m556850841_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m556850841(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m556850841_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<ChatNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3721885518_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3721885518(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3721885518_gshared)(__this /* static, unused */, ___list0, method)
