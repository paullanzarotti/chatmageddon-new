﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.BillerFactory
struct BillerFactory_t1450757922;
// Uniject.IResourceLoader
struct IResourceLoader_t2860850570;
// Uniject.ILogger
struct ILogger_t2858843691;
// Uniject.IStorage
struct IStorage_t1347868490;
// Unibill.Impl.IRawBillingPlatformProvider
struct IRawBillingPlatformProvider_t643465426;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Uniject.IUtil
struct IUtil_t2188430191;
// Unibill.Biller
struct Biller_t1615588570;
// Unibill.Impl.DownloadManager
struct DownloadManager_t32803451;
// Unibill.Impl.IBillingService
struct IBillingService_t403880577;
// Unibill.Impl.CurrencyManager
struct CurrencyManager_t3009003778;
// unibill.Dummy.Product[]
struct ProductU5BU5D_t4190069746;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Unibill.Impl.HelpCentre
struct HelpCentre_t214342444;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// PurchasableItem
struct PurchasableItem_t3963353899;
// unibill.Dummy.Product
struct Product_t1158373411;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Biller1615588570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void Unibill.Impl.BillerFactory::.ctor(Uniject.IResourceLoader,Uniject.ILogger,Uniject.IStorage,Unibill.Impl.IRawBillingPlatformProvider,Unibill.Impl.UnibillConfiguration,Uniject.IUtil)
extern "C"  void BillerFactory__ctor_m4104703387 (BillerFactory_t1450757922 * __this, Il2CppObject * ___resourceLoader0, Il2CppObject * ___logger1, Il2CppObject * ___storage2, Il2CppObject * ___platformProvider3, UnibillConfiguration_t2915611853 * ___config4, Il2CppObject * ___util5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Biller Unibill.Impl.BillerFactory::instantiate()
extern "C"  Biller_t1615588570 * BillerFactory_instantiate_m2012421408 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.DownloadManager Unibill.Impl.BillerFactory::instantiateDownloadManager(Unibill.Biller)
extern "C"  DownloadManager_t32803451 * BillerFactory_instantiateDownloadManager_m139141435 (BillerFactory_t1450757922 * __this, Biller_t1615588570 * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.BillerFactory::instantiateAnalytics(Unibill.Biller)
extern "C"  void BillerFactory_instantiateAnalytics_m2867781455 (BillerFactory_t1450757922 * __this, Biller_t1615588570 * ___biller0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.IBillingService Unibill.Impl.BillerFactory::instantiateBillingSubsystem()
extern "C"  Il2CppObject * BillerFactory_instantiateBillingSubsystem_m1104163285 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.CurrencyManager Unibill.Impl.BillerFactory::getCurrencyManager()
extern "C"  CurrencyManager_t3009003778 * BillerFactory_getCurrencyManager_m2400660868 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// unibill.Dummy.Product[] Unibill.Impl.BillerFactory::GetDummyProducts()
extern "C"  ProductU5BU5D_t4190069746* BillerFactory_GetDummyProducts_m3200876301 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TransactionDatabase Unibill.Impl.BillerFactory::getTransactionDatabase()
extern "C"  TransactionDatabase_t201476183 * BillerFactory_getTransactionDatabase_m2191237909 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Uniject.IStorage Unibill.Impl.BillerFactory::getStorage()
extern "C"  Il2CppObject * BillerFactory_getStorage_m1870000562 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.HelpCentre Unibill.Impl.BillerFactory::getHelp()
extern "C"  HelpCentre_t214342444 * BillerFactory_getHelp_m2754141999 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.ProductIdRemapper Unibill.Impl.BillerFactory::getMapper()
extern "C"  ProductIdRemapper_t3313438456 * BillerFactory_getMapper_m2574493873 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Uniject.ILogger Unibill.Impl.BillerFactory::getLogger()
extern "C"  Il2CppObject * BillerFactory_getLogger_m3355681998 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Uniject.IResourceLoader Unibill.Impl.BillerFactory::getResourceLoader()
extern "C"  Il2CppObject * BillerFactory_getResourceLoader_m1103248558 (BillerFactory_t1450757922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.BillerFactory::<GetDummyProducts>m__0(PurchasableItem)
extern "C"  bool BillerFactory_U3CGetDummyProductsU3Em__0_m2961174292 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// unibill.Dummy.Product Unibill.Impl.BillerFactory::<GetDummyProducts>m__1(PurchasableItem)
extern "C"  Product_t1158373411 * BillerFactory_U3CGetDummyProductsU3Em__1_m3771276284 (Il2CppObject * __this /* static, unused */, PurchasableItem_t3963353899 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
