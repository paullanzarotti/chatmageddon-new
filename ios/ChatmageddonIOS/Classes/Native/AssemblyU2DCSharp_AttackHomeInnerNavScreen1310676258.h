﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// AttackILP
struct AttackILP_t2111768551;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackHomeInnerNavScreen
struct  AttackHomeInnerNavScreen_t1310676258  : public NavigationScreen_t2333230110
{
public:
	// UnityEngine.GameObject AttackHomeInnerNavScreen::hasFriendsUI
	GameObject_t1756533147 * ___hasFriendsUI_3;
	// UnityEngine.GameObject AttackHomeInnerNavScreen::hasNoFriendsUI
	GameObject_t1756533147 * ___hasNoFriendsUI_4;
	// AttackILP AttackHomeInnerNavScreen::innerFriendsList
	AttackILP_t2111768551 * ___innerFriendsList_5;
	// System.Boolean AttackHomeInnerNavScreen::UIclosing
	bool ___UIclosing_6;

public:
	inline static int32_t get_offset_of_hasFriendsUI_3() { return static_cast<int32_t>(offsetof(AttackHomeInnerNavScreen_t1310676258, ___hasFriendsUI_3)); }
	inline GameObject_t1756533147 * get_hasFriendsUI_3() const { return ___hasFriendsUI_3; }
	inline GameObject_t1756533147 ** get_address_of_hasFriendsUI_3() { return &___hasFriendsUI_3; }
	inline void set_hasFriendsUI_3(GameObject_t1756533147 * value)
	{
		___hasFriendsUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___hasFriendsUI_3, value);
	}

	inline static int32_t get_offset_of_hasNoFriendsUI_4() { return static_cast<int32_t>(offsetof(AttackHomeInnerNavScreen_t1310676258, ___hasNoFriendsUI_4)); }
	inline GameObject_t1756533147 * get_hasNoFriendsUI_4() const { return ___hasNoFriendsUI_4; }
	inline GameObject_t1756533147 ** get_address_of_hasNoFriendsUI_4() { return &___hasNoFriendsUI_4; }
	inline void set_hasNoFriendsUI_4(GameObject_t1756533147 * value)
	{
		___hasNoFriendsUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___hasNoFriendsUI_4, value);
	}

	inline static int32_t get_offset_of_innerFriendsList_5() { return static_cast<int32_t>(offsetof(AttackHomeInnerNavScreen_t1310676258, ___innerFriendsList_5)); }
	inline AttackILP_t2111768551 * get_innerFriendsList_5() const { return ___innerFriendsList_5; }
	inline AttackILP_t2111768551 ** get_address_of_innerFriendsList_5() { return &___innerFriendsList_5; }
	inline void set_innerFriendsList_5(AttackILP_t2111768551 * value)
	{
		___innerFriendsList_5 = value;
		Il2CppCodeGenWriteBarrier(&___innerFriendsList_5, value);
	}

	inline static int32_t get_offset_of_UIclosing_6() { return static_cast<int32_t>(offsetof(AttackHomeInnerNavScreen_t1310676258, ___UIclosing_6)); }
	inline bool get_UIclosing_6() const { return ___UIclosing_6; }
	inline bool* get_address_of_UIclosing_6() { return &___UIclosing_6; }
	inline void set_UIclosing_6(bool value)
	{
		___UIclosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
