﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StandardMiniGame
struct StandardMiniGame_t3672006024;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StandardDefendButton
struct  StandardDefendButton_t3215661011  : public SFXButton_t792651341
{
public:
	// StandardMiniGame StandardDefendButton::miniGame
	StandardMiniGame_t3672006024 * ___miniGame_5;
	// System.Single StandardDefendButton::waitTime
	float ___waitTime_6;
	// UnityEngine.Coroutine StandardDefendButton::waitRoutine
	Coroutine_t2299508840 * ___waitRoutine_7;

public:
	inline static int32_t get_offset_of_miniGame_5() { return static_cast<int32_t>(offsetof(StandardDefendButton_t3215661011, ___miniGame_5)); }
	inline StandardMiniGame_t3672006024 * get_miniGame_5() const { return ___miniGame_5; }
	inline StandardMiniGame_t3672006024 ** get_address_of_miniGame_5() { return &___miniGame_5; }
	inline void set_miniGame_5(StandardMiniGame_t3672006024 * value)
	{
		___miniGame_5 = value;
		Il2CppCodeGenWriteBarrier(&___miniGame_5, value);
	}

	inline static int32_t get_offset_of_waitTime_6() { return static_cast<int32_t>(offsetof(StandardDefendButton_t3215661011, ___waitTime_6)); }
	inline float get_waitTime_6() const { return ___waitTime_6; }
	inline float* get_address_of_waitTime_6() { return &___waitTime_6; }
	inline void set_waitTime_6(float value)
	{
		___waitTime_6 = value;
	}

	inline static int32_t get_offset_of_waitRoutine_7() { return static_cast<int32_t>(offsetof(StandardDefendButton_t3215661011, ___waitRoutine_7)); }
	inline Coroutine_t2299508840 * get_waitRoutine_7() const { return ___waitRoutine_7; }
	inline Coroutine_t2299508840 ** get_address_of_waitRoutine_7() { return &___waitRoutine_7; }
	inline void set_waitRoutine_7(Coroutine_t2299508840 * value)
	{
		___waitRoutine_7 = value;
		Il2CppCodeGenWriteBarrier(&___waitRoutine_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
