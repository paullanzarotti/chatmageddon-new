﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldInfoButton
struct ShieldInfoButton_t822471353;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldInfoButton::.ctor()
extern "C"  void ShieldInfoButton__ctor_m3403700604 (ShieldInfoButton_t822471353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldInfoButton::OnButtonReset()
extern "C"  void ShieldInfoButton_OnButtonReset_m2786749924 (ShieldInfoButton_t822471353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldInfoButton::OnButtonRotate()
extern "C"  void ShieldInfoButton_OnButtonRotate_m2435624424 (ShieldInfoButton_t822471353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
