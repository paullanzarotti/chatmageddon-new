﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EditUsernameUIScaler
struct  EditUsernameUIScaler_t3788521078  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UILabel EditUsernameUIScaler::usernameLabel
	UILabel_t1795115428 * ___usernameLabel_14;
	// UnityEngine.BoxCollider EditUsernameUIScaler::usernameCollider
	BoxCollider_t22920061 * ___usernameCollider_15;
	// UISprite EditUsernameUIScaler::usernameDivider
	UISprite_t603616735 * ___usernameDivider_16;

public:
	inline static int32_t get_offset_of_usernameLabel_14() { return static_cast<int32_t>(offsetof(EditUsernameUIScaler_t3788521078, ___usernameLabel_14)); }
	inline UILabel_t1795115428 * get_usernameLabel_14() const { return ___usernameLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_usernameLabel_14() { return &___usernameLabel_14; }
	inline void set_usernameLabel_14(UILabel_t1795115428 * value)
	{
		___usernameLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___usernameLabel_14, value);
	}

	inline static int32_t get_offset_of_usernameCollider_15() { return static_cast<int32_t>(offsetof(EditUsernameUIScaler_t3788521078, ___usernameCollider_15)); }
	inline BoxCollider_t22920061 * get_usernameCollider_15() const { return ___usernameCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_usernameCollider_15() { return &___usernameCollider_15; }
	inline void set_usernameCollider_15(BoxCollider_t22920061 * value)
	{
		___usernameCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___usernameCollider_15, value);
	}

	inline static int32_t get_offset_of_usernameDivider_16() { return static_cast<int32_t>(offsetof(EditUsernameUIScaler_t3788521078, ___usernameDivider_16)); }
	inline UISprite_t603616735 * get_usernameDivider_16() const { return ___usernameDivider_16; }
	inline UISprite_t603616735 ** get_address_of_usernameDivider_16() { return &___usernameDivider_16; }
	inline void set_usernameDivider_16(UISprite_t603616735 * value)
	{
		___usernameDivider_16 = value;
		Il2CppCodeGenWriteBarrier(&___usernameDivider_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
