﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ICSharpCode.SharpZipLib.Checksums.Adler32
struct Adler32_t1759601101;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3420759743;
// System.IO.Stream
struct Stream_t3255436806;
// ICSharpCode.SharpZipLib.Encryption.PkzipClassic
struct PkzipClassic_t1612326202;
// ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase
struct PkzipClassicCryptoBase_t4134149114;
// ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform
struct PkzipClassicDecryptCryptoTransform_t1369162510;
// ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform
struct PkzipClassicEncryptCryptoTransform_t13679930;
// ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged
struct PkzipClassicManaged_t4138271613;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1153004758;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t281704372;
// ICSharpCode.SharpZipLib.Encryption.ZipAESStream
struct ZipAESStream_t1390211376;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t2148616552;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.SharpZipBaseException
struct SharpZipBaseException_t4116900137;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t3958302971;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_t3298452259;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t1922289728;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t3246684106;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t3211561891;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct InflaterInputStream_t663799889;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t824811027;
// ICSharpCode.SharpZipLib.Zip.DescriptorData
struct DescriptorData_t4106407219;
// ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs
struct KeysRequiredEventArgs_t881954000;
// ICSharpCode.SharpZipLib.Zip.TestStatus
struct TestStatus_t3549853348;
// ICSharpCode.SharpZipLib.Zip.ZipFile
struct ZipFile_t1110175137;
// ICSharpCode.SharpZipLib.Zip.ZipEntry
struct ZipEntry_t1764014695;
// ICSharpCode.SharpZipLib.Zip.ZipExtraData
struct ZipExtraData_t3152287325;
// ICSharpCode.SharpZipLib.Zip.ZipEntryFactory
struct ZipEntryFactory_t3498628499;
// ICSharpCode.SharpZipLib.Zip.ZipException
struct ZipException_t65220526;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler
struct ZipTestResultHandler_t2034591904;
// System.Security.Cryptography.CryptoStream
struct CryptoStream_t3531341937;
// ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler
struct KeysRequiredEventHandler_t4231608811;
// ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream
struct PartialInputStream_t2358850295;
// ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator
struct ZipEntryEnumerator_t729644389;
// ICSharpCode.SharpZipLib.Zip.ZipEntry[]
struct ZipEntryU5BU5D_t2096538654;
// ICSharpCode.SharpZipLib.Zip.ZipHelperStream
struct ZipHelperStream_t3454744209;
// ICSharpCode.SharpZipLib.Zip.ZipInputStream
struct ZipInputStream_t1369792737;
// ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler
struct ReadDataHandler_t1814344764;
// ICSharpCode.SharpZipLib.Zip.ZipNameTransform
struct ZipNameTransform_t3311483194;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "ICSharpCode_SharpZipLib_U3CModuleU3E3783534214.h"
#include "ICSharpCode_SharpZipLib_U3CModuleU3E3783534214MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet777596142.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet777596142MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet880868641.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet880868641MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1072038090.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1072038090MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe4079420781.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe4079420781MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe2234837501.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe2234837501MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe4079420777.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe4079420777MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe2916621363.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe2916621363MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch1759601101.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch1759601101MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch3420759743.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch3420759743MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHel266230107MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Co1056498181.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Co1056498181MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_EndOfStreamException1711658693MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_IO_EndOfStreamException1711658693.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En1612326202.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En1612326202MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg1108166522MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En4134149114.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En4134149114MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En1369162510.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En1369162510MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Encr13679930.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Encr13679930MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En4138271613.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En4138271613MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi3349726436MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi3349726436.h"
#include "mscorlib_System_Security_Cryptography_KeySizes3144736271.h"
#include "mscorlib_System_Security_Cryptography_KeySizes3144736271MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg1108166522.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Random1044426839MethodDeclarations.h"
#include "mscorlib_System_Random1044426839.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En1390211376.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En1390211376MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En2148616552.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream1337713182.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3531341937MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_En2148616552MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_Rfc2898Deriv1773348698MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana1034060848MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA11958407246MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_Rfc2898Deriv1773348698.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana1034060848.h"
#include "mscorlib_System_Security_Cryptography_CipherMode162592484.h"
#include "mscorlib_System_Security_Cryptography_DeriveBytes1087525826MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_DeriveBytes1087525826.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA11958407246.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Sh4116900137.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Sh4116900137MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_ApplicationException474868623MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip343008798.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip343008798MethodDeclarations.h"
#include "mscorlib_System_Int164041245914.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3958302971.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3958302971MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1922289728MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip824811027MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1922289728.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip824811027.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3298452259.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3246684106.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3246684106MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3298452259MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3211561891.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3211561891MethodDeclarations.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip_65220526MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip_65220526.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip663799889.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip663799889MethodDeclarations.h"
#include "mscorlib_System_IO_SeekOrigin2475945306.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1510270153.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1510270153MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4106407219.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4106407219MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip881954000.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip881954000MethodDeclarations.h"
#include "mscorlib_System_EventArgs3289624707MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4223507703.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4223507703MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3549853348.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3549853348MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1110175137.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1764014695.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2322915013.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2322915013MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2471403746.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2471403746MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip_82438310.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip_82438310MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Threading_Thread241561612MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_Globalization_TextInfo3620182823.h"
#include "mscorlib_System_Globalization_TextInfo3620182823MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1764014695MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_UInt16986882611.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3046490811.h"
#include "mscorlib_System_UInt642909196914.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3152287325MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3152287325.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3046490811MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3498628499.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3498628499MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3311483194MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3311483194.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1110175137MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4231608811MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4231608811.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1695958676.h"
#include "mscorlib_System_IO_FileMode236403845.h"
#include "mscorlib_System_IO_FileAccess4282042064.h"
#include "mscorlib_System_IO_FileShare3362491215.h"
#include "mscorlib_System_GC2902933594MethodDeclarations.h"
#include "ICSharpCode.SharpZipLib_ArrayTypes.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip729644389MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip729644389.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2358850295MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2358850295.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2034591904.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2034591904MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3454744209MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4114042119.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3454744209.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3531341937.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi4114042119MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_IO_IOException2458421087MethodDeclarations.h"
#include "mscorlib_System_IO_IOException2458421087.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1369792737.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1369792737MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1814344764MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1814344764.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_IO_Path41728875MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int64 ICSharpCode.SharpZipLib.Checksums.Adler32::get_Value()
extern "C"  int64_t Adler32_get_Value_m24599817 (Adler32_t1759601101 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_checksum_0();
		return (((int64_t)((uint64_t)L_0)));
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::.ctor()
extern "C"  void Adler32__ctor_m3717921916 (Adler32_t1759601101 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Adler32_Reset_m2520109847(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Reset()
extern "C"  void Adler32_Reset_m2520109847 (Adler32_t1759601101 * __this, const MethodInfo* method)
{
	{
		__this->set_checksum_0(1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Update(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern Il2CppCodeGenString* _stringLiteral1303852717;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral4077131926;
extern Il2CppCodeGenString* _stringLiteral585424892;
extern const uint32_t Adler32_Update_m2876580056_MetadataUsageId;
extern "C"  void Adler32_Update_m2876580056 (Adler32_t1759601101 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Adler32_Update_m2876580056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___offset1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1673268925, _stringLiteral1303852717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___count2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_5, _stringLiteral1554746267, _stringLiteral1303852717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0036:
	{
		int32_t L_6 = ___offset1;
		ByteU5BU5D_t3397334013* L_7 = ___buffer0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, _stringLiteral1673268925, _stringLiteral4077131926, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004c:
	{
		int32_t L_9 = ___offset1;
		int32_t L_10 = ___count2;
		ByteU5BU5D_t3397334013* L_11 = ___buffer0;
		NullCheck(L_11);
		if ((((int32_t)((int32_t)((int32_t)L_9+(int32_t)L_10))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0064;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_12 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_12, _stringLiteral1554746267, _stringLiteral585424892, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0064:
	{
		uint32_t L_13 = __this->get_checksum_0();
		V_0 = ((int32_t)((int32_t)L_13&(int32_t)((int32_t)65535)));
		uint32_t L_14 = __this->get_checksum_0();
		V_1 = ((int32_t)((uint32_t)L_14>>((int32_t)16)));
		goto IL_00bd;
	}

IL_007d:
	{
		V_2 = ((int32_t)3800);
		int32_t L_15 = V_2;
		int32_t L_16 = ___count2;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_17 = ___count2;
		V_2 = L_17;
	}

IL_0089:
	{
		int32_t L_18 = ___count2;
		int32_t L_19 = V_2;
		___count2 = ((int32_t)((int32_t)L_18-(int32_t)L_19));
		goto IL_00a5;
	}

IL_0090:
	{
		uint32_t L_20 = V_0;
		ByteU5BU5D_t3397334013* L_21 = ___buffer0;
		int32_t L_22 = ___offset1;
		int32_t L_23 = L_22;
		___offset1 = ((int32_t)((int32_t)L_23+(int32_t)1));
		NullCheck(L_21);
		int32_t L_24 = L_23;
		uint8_t L_25 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_0 = ((int32_t)((int32_t)L_20+(int32_t)((int32_t)((int32_t)L_25&(int32_t)((int32_t)255)))));
		uint32_t L_26 = V_1;
		uint32_t L_27 = V_0;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)L_27));
	}

IL_00a5:
	{
		int32_t L_28 = V_2;
		int32_t L_29 = ((int32_t)((int32_t)L_28-(int32_t)1));
		V_2 = L_29;
		if ((((int32_t)L_29) >= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		uint32_t L_30 = V_0;
		V_0 = ((int32_t)((uint32_t)(int32_t)L_30%(uint32_t)(int32_t)((int32_t)65521)));
		uint32_t L_31 = V_1;
		V_1 = ((int32_t)((uint32_t)(int32_t)L_31%(uint32_t)(int32_t)((int32_t)65521)));
	}

IL_00bd:
	{
		int32_t L_32 = ___count2;
		if ((((int32_t)L_32) > ((int32_t)0)))
		{
			goto IL_007d;
		}
	}
	{
		uint32_t L_33 = V_1;
		uint32_t L_34 = V_0;
		__this->set_checksum_0(((int32_t)((int32_t)((int32_t)((int32_t)L_33<<(int32_t)((int32_t)16)))|(int32_t)L_34)));
		return;
	}
}
// System.UInt32 ICSharpCode.SharpZipLib.Checksums.Crc32::ComputeCrc32(System.UInt32,System.Byte)
extern Il2CppClass* Crc32_t3420759743_il2cpp_TypeInfo_var;
extern const uint32_t Crc32_ComputeCrc32_m1449573370_MetadataUsageId;
extern "C"  uint32_t Crc32_ComputeCrc32_m1449573370 (Il2CppObject * __this /* static, unused */, uint32_t ___oldCrc0, uint8_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Crc32_ComputeCrc32_m1449573370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t3420759743_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t59386216* L_0 = ((Crc32_t3420759743_StaticFields*)Crc32_t3420759743_il2cpp_TypeInfo_var->static_fields)->get_CrcTable_0();
		uint32_t L_1 = ___oldCrc0;
		uint8_t L_2 = ___value1;
		NullCheck(L_0);
		uintptr_t L_3 = (((uintptr_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_2))&(int32_t)((int32_t)255)))));
		uint32_t L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		uint32_t L_5 = ___oldCrc0;
		return ((int32_t)((int32_t)L_4^(int32_t)((int32_t)((uint32_t)L_5>>8))));
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Checksums.Crc32::get_Value()
extern "C"  int64_t Crc32_get_Value_m1420497589 (Crc32_t3420759743 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_crc_1();
		return (((int64_t)((uint64_t)L_0)));
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Reset()
extern "C"  void Crc32_Reset_m2645532975 (Crc32_t3420759743 * __this, const MethodInfo* method)
{
	{
		__this->set_crc_1(0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* Crc32_t3420759743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral3970917621;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern const uint32_t Crc32_Update_m3476396462_MetadataUsageId;
extern "C"  void Crc32_Update_m3476396462 (Crc32_t3420759743 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Crc32_Update_m3476396462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___count2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1554746267, _stringLiteral3970917621, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_5 = ___offset1;
		int32_t L_6 = ___count2;
		ByteU5BU5D_t3397334013* L_7 = ___buffer0;
		NullCheck(L_7);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0039;
		}
	}

IL_002e:
	{
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_8, _stringLiteral1673268925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0039:
	{
		uint32_t L_9 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_9^(int32_t)(-1))));
		goto IL_0074;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t3420759743_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t59386216* L_10 = ((Crc32_t3420759743_StaticFields*)Crc32_t3420759743_il2cpp_TypeInfo_var->static_fields)->get_CrcTable_0();
		uint32_t L_11 = __this->get_crc_1();
		ByteU5BU5D_t3397334013* L_12 = ___buffer0;
		int32_t L_13 = ___offset1;
		int32_t L_14 = L_13;
		___offset1 = ((int32_t)((int32_t)L_14+(int32_t)1));
		NullCheck(L_12);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_10);
		uintptr_t L_17 = (((uintptr_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11^(int32_t)L_16))&(int32_t)((int32_t)255)))));
		uint32_t L_18 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint32_t L_19 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_18^(int32_t)((int32_t)((uint32_t)L_19>>8)))));
	}

IL_0074:
	{
		int32_t L_20 = ___count2;
		int32_t L_21 = ((int32_t)((int32_t)L_20-(int32_t)1));
		___count2 = L_21;
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		uint32_t L_22 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_22^(int32_t)(-1))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.ctor()
extern "C"  void Crc32__ctor_m2818634674 (Crc32_t3420759743 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.cctor()
extern Il2CppClass* UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var;
extern Il2CppClass* Crc32_t3420759743_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004f9U2D1_0_FieldInfo_var;
extern const uint32_t Crc32__cctor_m578332849_MetadataUsageId;
extern "C"  void Crc32__cctor_m578332849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Crc32__cctor_m578332849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UInt32U5BU5D_t59386216* L_0 = ((UInt32U5BU5D_t59386216*)SZArrayNew(UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004f9U2D1_0_FieldInfo_var), /*hidden argument*/NULL);
		((Crc32_t3420759743_StaticFields*)Crc32_t3420759743_il2cpp_TypeInfo_var->static_fields)->set_CrcTable_0(L_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::ReadFully(System.IO.Stream,System.Byte[])
extern "C"  void StreamUtils_ReadFully_m1907144787 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, ByteU5BU5D_t3397334013* ___buffer1, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = ___stream0;
		ByteU5BU5D_t3397334013* L_1 = ___buffer1;
		ByteU5BU5D_t3397334013* L_2 = ___buffer1;
		NullCheck(L_2);
		StreamUtils_ReadFully_m84790847(NULL /*static, unused*/, L_0, L_1, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Core.StreamUtils::ReadFully(System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* EndOfStreamException_t1711658693_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3909163458;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern const uint32_t StreamUtils_ReadFully_m84790847_MetadataUsageId;
extern "C"  void StreamUtils_ReadFully_m84790847 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, ByteU5BU5D_t3397334013* ___buffer1, int32_t ___offset2, int32_t ___count3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StreamUtils_ReadFully_m84790847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Stream_t3255436806 * L_0 = ___stream0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral3909163458, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		ByteU5BU5D_t3397334013* L_2 = ___buffer1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = ___offset2;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_5 = ___offset2;
		ByteU5BU5D_t3397334013* L_6 = ___buffer1;
		NullCheck(L_6);
		if ((((int32_t)L_5) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))))
		{
			goto IL_0031;
		}
	}

IL_0026:
	{
		ArgumentOutOfRangeException_t279959794 * L_7 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_7, _stringLiteral1673268925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0031:
	{
		int32_t L_8 = ___count3;
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_9 = ___offset2;
		int32_t L_10 = ___count3;
		ByteU5BU5D_t3397334013* L_11 = ___buffer1;
		NullCheck(L_11);
		if ((((int32_t)((int32_t)((int32_t)L_9+(int32_t)L_10))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0066;
		}
	}

IL_003d:
	{
		ArgumentOutOfRangeException_t279959794 * L_12 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_12, _stringLiteral1554746267, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0048:
	{
		Stream_t3255436806 * L_13 = ___stream0;
		ByteU5BU5D_t3397334013* L_14 = ___buffer1;
		int32_t L_15 = ___offset2;
		int32_t L_16 = ___count3;
		NullCheck(L_13);
		int32_t L_17 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_13, L_14, L_15, L_16);
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) > ((int32_t)0)))
		{
			goto IL_005c;
		}
	}
	{
		EndOfStreamException_t1711658693 * L_19 = (EndOfStreamException_t1711658693 *)il2cpp_codegen_object_new(EndOfStreamException_t1711658693_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2877696588(L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_005c:
	{
		int32_t L_20 = ___offset2;
		int32_t L_21 = V_0;
		___offset2 = ((int32_t)((int32_t)L_20+(int32_t)L_21));
		int32_t L_22 = ___count3;
		int32_t L_23 = V_0;
		___count3 = ((int32_t)((int32_t)L_22-(int32_t)L_23));
	}

IL_0066:
	{
		int32_t L_24 = ___count3;
		if ((((int32_t)L_24) > ((int32_t)0)))
		{
			goto IL_0048;
		}
	}
	{
		return;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassic::GenerateKeys(System.Byte[])
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var;
extern Il2CppClass* Crc32_t3420759743_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60000ddU2D1_1_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral4136222375;
extern Il2CppCodeGenString* _stringLiteral3267569156;
extern const uint32_t PkzipClassic_GenerateKeys_m4071511090_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* PkzipClassic_GenerateKeys_m4071511090 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___seed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassic_GenerateKeys_m4071511090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UInt32U5BU5D_t59386216* V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___seed0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral4136222375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		ByteU5BU5D_t3397334013* L_2 = ___seed0;
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_3, _stringLiteral3267569156, _stringLiteral4136222375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		UInt32U5BU5D_t59386216* L_4 = ((UInt32U5BU5D_t59386216*)SZArrayNew(UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60000ddU2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = 0;
		goto IL_0076;
	}

IL_0039:
	{
		UInt32U5BU5D_t59386216* L_5 = V_0;
		UInt32U5BU5D_t59386216* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = 0;
		uint32_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		ByteU5BU5D_t3397334013* L_9 = ___seed0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		uint8_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t3420759743_il2cpp_TypeInfo_var);
		uint32_t L_13 = Crc32_ComputeCrc32_m1449573370(NULL /*static, unused*/, L_8, L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint32_t)L_13);
		UInt32U5BU5D_t59386216* L_14 = V_0;
		UInt32U5BU5D_t59386216* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = 1;
		uint32_t L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		UInt32U5BU5D_t59386216* L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = 0;
		uint32_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)((int32_t)((int32_t)L_17+(int32_t)(((int32_t)((uint8_t)L_20))))));
		UInt32U5BU5D_t59386216* L_21 = V_0;
		UInt32U5BU5D_t59386216* L_22 = V_0;
		NullCheck(L_22);
		int32_t L_23 = 1;
		uint32_t L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24*(int32_t)((int32_t)134775813)))+(int32_t)1)));
		UInt32U5BU5D_t59386216* L_25 = V_0;
		UInt32U5BU5D_t59386216* L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = 2;
		uint32_t L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		UInt32U5BU5D_t59386216* L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = 1;
		uint32_t L_31 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		uint32_t L_32 = Crc32_ComputeCrc32_m1449573370(NULL /*static, unused*/, L_28, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_31>>((int32_t)24)))))), /*hidden argument*/NULL);
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint32_t)L_32);
		int32_t L_33 = V_1;
		V_1 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_34 = V_1;
		ByteU5BU5D_t3397334013* L_35 = ___seed0;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0039;
		}
	}
	{
		V_2 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)12)));
		ByteU5BU5D_t3397334013* L_36 = V_2;
		UInt32U5BU5D_t59386216* L_37 = V_0;
		NullCheck(L_37);
		int32_t L_38 = 0;
		uint32_t L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_36);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_39&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_40 = V_2;
		UInt32U5BU5D_t59386216* L_41 = V_0;
		NullCheck(L_41);
		int32_t L_42 = 0;
		uint32_t L_43 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_40);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_43>>8))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_44 = V_2;
		UInt32U5BU5D_t59386216* L_45 = V_0;
		NullCheck(L_45);
		int32_t L_46 = 0;
		uint32_t L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_44);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_47>>((int32_t)16)))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_48 = V_2;
		UInt32U5BU5D_t59386216* L_49 = V_0;
		NullCheck(L_49);
		int32_t L_50 = 0;
		uint32_t L_51 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		NullCheck(L_48);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_51>>((int32_t)24)))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_52 = V_2;
		UInt32U5BU5D_t59386216* L_53 = V_0;
		NullCheck(L_53);
		int32_t L_54 = 1;
		uint32_t L_55 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_52);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_55&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_56 = V_2;
		UInt32U5BU5D_t59386216* L_57 = V_0;
		NullCheck(L_57);
		int32_t L_58 = 1;
		uint32_t L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		NullCheck(L_56);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_59>>8))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_60 = V_2;
		UInt32U5BU5D_t59386216* L_61 = V_0;
		NullCheck(L_61);
		int32_t L_62 = 1;
		uint32_t L_63 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		NullCheck(L_60);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_63>>((int32_t)16)))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_64 = V_2;
		UInt32U5BU5D_t59386216* L_65 = V_0;
		NullCheck(L_65);
		int32_t L_66 = 1;
		uint32_t L_67 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		NullCheck(L_64);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_67>>((int32_t)24)))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_68 = V_2;
		UInt32U5BU5D_t59386216* L_69 = V_0;
		NullCheck(L_69);
		int32_t L_70 = 2;
		uint32_t L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		NullCheck(L_68);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(8), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_71&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_72 = V_2;
		UInt32U5BU5D_t59386216* L_73 = V_0;
		NullCheck(L_73);
		int32_t L_74 = 2;
		uint32_t L_75 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		NullCheck(L_72);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_75>>8))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_76 = V_2;
		UInt32U5BU5D_t59386216* L_77 = V_0;
		NullCheck(L_77);
		int32_t L_78 = 2;
		uint32_t L_79 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		NullCheck(L_76);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_79>>((int32_t)16)))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_80 = V_2;
		UInt32U5BU5D_t59386216* L_81 = V_0;
		NullCheck(L_81);
		int32_t L_82 = 2;
		uint32_t L_83 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_80);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_83>>((int32_t)24)))&(int32_t)((int32_t)255)))))));
		ByteU5BU5D_t3397334013* L_84 = V_2;
		return L_84;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassic::.ctor()
extern "C"  void PkzipClassic__ctor_m1153871058 (PkzipClassic_t1612326202 * __this, const MethodInfo* method)
{
	{
		SymmetricAlgorithm__ctor_m851603055(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::TransformByte()
extern "C"  uint8_t PkzipClassicCryptoBase_TransformByte_m2972352868 (PkzipClassicCryptoBase_t4134149114 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		UInt32U5BU5D_t59386216* L_0 = __this->get_keys_0();
		NullCheck(L_0);
		int32_t L_1 = 2;
		uint32_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)65535)))|(int32_t)2));
		uint32_t L_3 = V_0;
		uint32_t L_4 = V_0;
		return (((int32_t)((uint8_t)((int32_t)((uint32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)((int32_t)L_4^(int32_t)1))))>>8)))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::SetKeys(System.Byte[])
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2362120007;
extern Il2CppCodeGenString* _stringLiteral208676106;
extern const uint32_t PkzipClassicCryptoBase_SetKeys_m1603091503_MetadataUsageId;
extern "C"  void PkzipClassicCryptoBase_SetKeys_m1603091503 (PkzipClassicCryptoBase_t4134149114 * __this, ByteU5BU5D_t3397334013* ___keyData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicCryptoBase_SetKeys_m1603091503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___keyData0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2362120007, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		ByteU5BU5D_t3397334013* L_2 = ___keyData0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) == ((int32_t)((int32_t)12))))
		{
			goto IL_0020;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, _stringLiteral208676106, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		__this->set_keys_0(((UInt32U5BU5D_t59386216*)SZArrayNew(UInt32U5BU5D_t59386216_il2cpp_TypeInfo_var, (uint32_t)3)));
		UInt32U5BU5D_t59386216* L_4 = __this->get_keys_0();
		ByteU5BU5D_t3397334013* L_5 = ___keyData0;
		NullCheck(L_5);
		int32_t L_6 = 3;
		uint8_t L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		ByteU5BU5D_t3397334013* L_8 = ___keyData0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		uint8_t L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ByteU5BU5D_t3397334013* L_11 = ___keyData0;
		NullCheck(L_11);
		int32_t L_12 = 1;
		uint8_t L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ByteU5BU5D_t3397334013* L_14 = ___keyData0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		uint8_t L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_10<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_13<<(int32_t)8))))|(int32_t)L_16)));
		UInt32U5BU5D_t59386216* L_17 = __this->get_keys_0();
		ByteU5BU5D_t3397334013* L_18 = ___keyData0;
		NullCheck(L_18);
		int32_t L_19 = 7;
		uint8_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		ByteU5BU5D_t3397334013* L_21 = ___keyData0;
		NullCheck(L_21);
		int32_t L_22 = 6;
		uint8_t L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		ByteU5BU5D_t3397334013* L_24 = ___keyData0;
		NullCheck(L_24);
		int32_t L_25 = 5;
		uint8_t L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		ByteU5BU5D_t3397334013* L_27 = ___keyData0;
		NullCheck(L_27);
		int32_t L_28 = 4;
		uint8_t L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_20<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_23<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_26<<(int32_t)8))))|(int32_t)L_29)));
		UInt32U5BU5D_t59386216* L_30 = __this->get_keys_0();
		ByteU5BU5D_t3397334013* L_31 = ___keyData0;
		NullCheck(L_31);
		int32_t L_32 = ((int32_t)11);
		uint8_t L_33 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		ByteU5BU5D_t3397334013* L_34 = ___keyData0;
		NullCheck(L_34);
		int32_t L_35 = ((int32_t)10);
		uint8_t L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		ByteU5BU5D_t3397334013* L_37 = ___keyData0;
		NullCheck(L_37);
		int32_t L_38 = ((int32_t)9);
		uint8_t L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		ByteU5BU5D_t3397334013* L_40 = ___keyData0;
		NullCheck(L_40);
		int32_t L_41 = 8;
		uint8_t L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33<<(int32_t)((int32_t)24)))|(int32_t)((int32_t)((int32_t)L_36<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_39<<(int32_t)8))))|(int32_t)L_42)));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::UpdateKeys(System.Byte)
extern Il2CppClass* Crc32_t3420759743_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicCryptoBase_UpdateKeys_m1158306728_MetadataUsageId;
extern "C"  void PkzipClassicCryptoBase_UpdateKeys_m1158306728 (PkzipClassicCryptoBase_t4134149114 * __this, uint8_t ___ch0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicCryptoBase_UpdateKeys_m1158306728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UInt32U5BU5D_t59386216* L_0 = __this->get_keys_0();
		UInt32U5BU5D_t59386216* L_1 = __this->get_keys_0();
		NullCheck(L_1);
		int32_t L_2 = 0;
		uint32_t L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		uint8_t L_4 = ___ch0;
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t3420759743_il2cpp_TypeInfo_var);
		uint32_t L_5 = Crc32_ComputeCrc32_m1449573370(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint32_t)L_5);
		UInt32U5BU5D_t59386216* L_6 = __this->get_keys_0();
		UInt32U5BU5D_t59386216* L_7 = __this->get_keys_0();
		NullCheck(L_7);
		int32_t L_8 = 1;
		uint32_t L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		UInt32U5BU5D_t59386216* L_10 = __this->get_keys_0();
		NullCheck(L_10);
		int32_t L_11 = 0;
		uint32_t L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)((int32_t)((int32_t)L_9+(int32_t)(((int32_t)((uint8_t)L_12))))));
		UInt32U5BU5D_t59386216* L_13 = __this->get_keys_0();
		UInt32U5BU5D_t59386216* L_14 = __this->get_keys_0();
		NullCheck(L_14);
		int32_t L_15 = 1;
		uint32_t L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16*(int32_t)((int32_t)134775813)))+(int32_t)1)));
		UInt32U5BU5D_t59386216* L_17 = __this->get_keys_0();
		UInt32U5BU5D_t59386216* L_18 = __this->get_keys_0();
		NullCheck(L_18);
		int32_t L_19 = 2;
		uint32_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		UInt32U5BU5D_t59386216* L_21 = __this->get_keys_0();
		NullCheck(L_21);
		int32_t L_22 = 1;
		uint32_t L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		uint32_t L_24 = Crc32_ComputeCrc32_m1449573370(NULL /*static, unused*/, L_20, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_23>>((int32_t)24)))))), /*hidden argument*/NULL);
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint32_t)L_24);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::Reset()
extern "C"  void PkzipClassicCryptoBase_Reset_m164714687 (PkzipClassicCryptoBase_t4134149114 * __this, const MethodInfo* method)
{
	{
		UInt32U5BU5D_t59386216* L_0 = __this->get_keys_0();
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint32_t)0);
		UInt32U5BU5D_t59386216* L_1 = __this->get_keys_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)0);
		UInt32U5BU5D_t59386216* L_2 = __this->get_keys_0();
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint32_t)0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicCryptoBase::.ctor()
extern "C"  void PkzipClassicCryptoBase__ctor_m1164175602 (PkzipClassicCryptoBase_t4134149114 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::.ctor(System.Byte[])
extern "C"  void PkzipClassicDecryptCryptoTransform__ctor_m3583175229 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, ByteU5BU5D_t3397334013* ___keyBlock0, const MethodInfo* method)
{
	{
		PkzipClassicCryptoBase__ctor_m1164175602(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___keyBlock0;
		PkzipClassicCryptoBase_SetKeys_m1603091503(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicDecryptCryptoTransform_TransformFinalBlock_m3041379004_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* PkzipClassicDecryptCryptoTransform_TransformFinalBlock_m3041379004 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicDecryptCryptoTransform_TransformFinalBlock_m3041379004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		int32_t L_0 = ___inputCount2;
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_0));
		ByteU5BU5D_t3397334013* L_1 = ___inputBuffer0;
		int32_t L_2 = ___inputOffset1;
		int32_t L_3 = ___inputCount2;
		ByteU5BU5D_t3397334013* L_4 = V_0;
		PkzipClassicDecryptCryptoTransform_TransformBlock_m2868400146(__this, L_1, L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = V_0;
		return L_5;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t PkzipClassicDecryptCryptoTransform_TransformBlock_m2868400146 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t3397334013* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	uint8_t V_1 = 0x0;
	{
		int32_t L_0 = ___inputOffset1;
		V_0 = L_0;
		goto IL_0026;
	}

IL_0004:
	{
		ByteU5BU5D_t3397334013* L_1 = ___inputBuffer0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		uint8_t L_5 = PkzipClassicCryptoBase_TransformByte_m2972352868(__this, /*hidden argument*/NULL);
		V_1 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_4^(int32_t)L_5)))));
		ByteU5BU5D_t3397334013* L_6 = ___outputBuffer3;
		int32_t L_7 = ___outputOffset4;
		int32_t L_8 = L_7;
		___outputOffset4 = ((int32_t)((int32_t)L_8+(int32_t)1));
		uint8_t L_9 = V_1;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (uint8_t)L_9);
		uint8_t L_10 = V_1;
		PkzipClassicCryptoBase_UpdateKeys_m1158306728(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = ___inputOffset1;
		int32_t L_14 = ___inputCount2;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)L_13+(int32_t)L_14)))))
		{
			goto IL_0004;
		}
	}
	{
		int32_t L_15 = ___inputCount2;
		return L_15;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_CanReuseTransform()
extern "C"  bool PkzipClassicDecryptCryptoTransform_get_CanReuseTransform_m2983456339 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_InputBlockSize()
extern "C"  int32_t PkzipClassicDecryptCryptoTransform_get_InputBlockSize_m2568311075 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_OutputBlockSize()
extern "C"  int32_t PkzipClassicDecryptCryptoTransform_get_OutputBlockSize_m3681876718 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::get_CanTransformMultipleBlocks()
extern "C"  bool PkzipClassicDecryptCryptoTransform_get_CanTransformMultipleBlocks_m2215393225 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicDecryptCryptoTransform::Dispose()
extern "C"  void PkzipClassicDecryptCryptoTransform_Dispose_m1326022445 (PkzipClassicDecryptCryptoTransform_t1369162510 * __this, const MethodInfo* method)
{
	{
		PkzipClassicCryptoBase_Reset_m164714687(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::.ctor(System.Byte[])
extern "C"  void PkzipClassicEncryptCryptoTransform__ctor_m2801825635 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, ByteU5BU5D_t3397334013* ___keyBlock0, const MethodInfo* method)
{
	{
		PkzipClassicCryptoBase__ctor_m1164175602(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___keyBlock0;
		PkzipClassicCryptoBase_SetKeys_m1603091503(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicEncryptCryptoTransform_TransformFinalBlock_m3795471660_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* PkzipClassicEncryptCryptoTransform_TransformFinalBlock_m3795471660 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicEncryptCryptoTransform_TransformFinalBlock_m3795471660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		int32_t L_0 = ___inputCount2;
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_0));
		ByteU5BU5D_t3397334013* L_1 = ___inputBuffer0;
		int32_t L_2 = ___inputOffset1;
		int32_t L_3 = ___inputCount2;
		ByteU5BU5D_t3397334013* L_4 = V_0;
		PkzipClassicEncryptCryptoTransform_TransformBlock_m2405506378(__this, L_1, L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = V_0;
		return L_5;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t PkzipClassicEncryptCryptoTransform_TransformBlock_m2405506378 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t3397334013* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	uint8_t V_1 = 0x0;
	{
		int32_t L_0 = ___inputOffset1;
		V_0 = L_0;
		goto IL_0028;
	}

IL_0004:
	{
		ByteU5BU5D_t3397334013* L_1 = ___inputBuffer0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		ByteU5BU5D_t3397334013* L_5 = ___outputBuffer3;
		int32_t L_6 = ___outputOffset4;
		int32_t L_7 = L_6;
		___outputOffset4 = ((int32_t)((int32_t)L_7+(int32_t)1));
		ByteU5BU5D_t3397334013* L_8 = ___inputBuffer0;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		uint8_t L_12 = PkzipClassicCryptoBase_TransformByte_m2972352868(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_11^(int32_t)L_12))))));
		uint8_t L_13 = V_1;
		PkzipClassicCryptoBase_UpdateKeys_m1158306728(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = ___inputOffset1;
		int32_t L_17 = ___inputCount2;
		if ((((int32_t)L_15) < ((int32_t)((int32_t)((int32_t)L_16+(int32_t)L_17)))))
		{
			goto IL_0004;
		}
	}
	{
		int32_t L_18 = ___inputCount2;
		return L_18;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_CanReuseTransform()
extern "C"  bool PkzipClassicEncryptCryptoTransform_get_CanReuseTransform_m3437999725 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_InputBlockSize()
extern "C"  int32_t PkzipClassicEncryptCryptoTransform_get_InputBlockSize_m1649601585 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_OutputBlockSize()
extern "C"  int32_t PkzipClassicEncryptCryptoTransform_get_OutputBlockSize_m2910404178 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::get_CanTransformMultipleBlocks()
extern "C"  bool PkzipClassicEncryptCryptoTransform_get_CanTransformMultipleBlocks_m1860241759 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicEncryptCryptoTransform::Dispose()
extern "C"  void PkzipClassicEncryptCryptoTransform_Dispose_m1229966255 (PkzipClassicEncryptCryptoTransform_t13679930 * __this, const MethodInfo* method)
{
	{
		PkzipClassicCryptoBase_Reset_m164714687(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_BlockSize()
extern "C"  int32_t PkzipClassicManaged_get_BlockSize_m3066179900 (PkzipClassicManaged_t4138271613 * __this, const MethodInfo* method)
{
	{
		return 8;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_BlockSize(System.Int32)
extern Il2CppClass* CryptographicException_t3349726436_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1884633051;
extern const uint32_t PkzipClassicManaged_set_BlockSize_m2936058981_MetadataUsageId;
extern "C"  void PkzipClassicManaged_set_BlockSize_m2936058981 (PkzipClassicManaged_t4138271613 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_set_BlockSize_m2936058981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_000f;
		}
	}
	{
		CryptographicException_t3349726436 * L_1 = (CryptographicException_t3349726436 *)il2cpp_codegen_object_new(CryptographicException_t3349726436_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2415891459(L_1, _stringLiteral1884633051, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000f:
	{
		return;
	}
}
// System.Security.Cryptography.KeySizes[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_LegalKeySizes()
extern Il2CppClass* KeySizesU5BU5D_t1153004758_il2cpp_TypeInfo_var;
extern Il2CppClass* KeySizes_t3144736271_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicManaged_get_LegalKeySizes_m2212659125_MetadataUsageId;
extern "C"  KeySizesU5BU5D_t1153004758* PkzipClassicManaged_get_LegalKeySizes_m2212659125 (PkzipClassicManaged_t4138271613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_get_LegalKeySizes_m2212659125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeySizesU5BU5D_t1153004758* V_0 = NULL;
	{
		V_0 = ((KeySizesU5BU5D_t1153004758*)SZArrayNew(KeySizesU5BU5D_t1153004758_il2cpp_TypeInfo_var, (uint32_t)1));
		KeySizesU5BU5D_t1153004758* L_0 = V_0;
		KeySizes_t3144736271 * L_1 = (KeySizes_t3144736271 *)il2cpp_codegen_object_new(KeySizes_t3144736271_il2cpp_TypeInfo_var);
		KeySizes__ctor_m3526899007(L_1, ((int32_t)96), ((int32_t)96), 0, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (KeySizes_t3144736271 *)L_1);
		KeySizesU5BU5D_t1153004758* L_2 = V_0;
		return L_2;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateIV()
extern "C"  void PkzipClassicManaged_GenerateIV_m3620088017 (PkzipClassicManaged_t4138271613 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Security.Cryptography.KeySizes[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_LegalBlockSizes()
extern Il2CppClass* KeySizesU5BU5D_t1153004758_il2cpp_TypeInfo_var;
extern Il2CppClass* KeySizes_t3144736271_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicManaged_get_LegalBlockSizes_m3883840115_MetadataUsageId;
extern "C"  KeySizesU5BU5D_t1153004758* PkzipClassicManaged_get_LegalBlockSizes_m3883840115 (PkzipClassicManaged_t4138271613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_get_LegalBlockSizes_m3883840115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeySizesU5BU5D_t1153004758* V_0 = NULL;
	{
		V_0 = ((KeySizesU5BU5D_t1153004758*)SZArrayNew(KeySizesU5BU5D_t1153004758_il2cpp_TypeInfo_var, (uint32_t)1));
		KeySizesU5BU5D_t1153004758* L_0 = V_0;
		KeySizes_t3144736271 * L_1 = (KeySizes_t3144736271 *)il2cpp_codegen_object_new(KeySizes_t3144736271_il2cpp_TypeInfo_var);
		KeySizes__ctor_m3526899007(L_1, 8, 8, 0, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (KeySizes_t3144736271 *)L_1);
		KeySizesU5BU5D_t1153004758* L_2 = V_0;
		return L_2;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::get_Key()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicManaged_get_Key_m3250387039_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* PkzipClassicManaged_get_Key_m3250387039 (PkzipClassicManaged_t4138271613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_get_Key_m3250387039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_key__10();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		VirtActionInvoker0::Invoke(27 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::GenerateKey() */, __this);
	}

IL_000e:
	{
		ByteU5BU5D_t3397334013* L_1 = __this->get_key__10();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
		Il2CppObject * L_2 = Array_Clone_m768574314((Il2CppArray *)(Il2CppArray *)L_1, /*hidden argument*/NULL);
		return ((ByteU5BU5D_t3397334013*)Castclass(L_2, ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var));
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::set_Key(System.Byte[])
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* CryptographicException_t3349726436_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern Il2CppCodeGenString* _stringLiteral3931749060;
extern const uint32_t PkzipClassicManaged_set_Key_m1328383866_MetadataUsageId;
extern "C"  void PkzipClassicManaged_set_Key_m1328383866 (PkzipClassicManaged_t4138271613 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_set_Key_m1328383866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		ByteU5BU5D_t3397334013* L_2 = ___value0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) == ((int32_t)((int32_t)12))))
		{
			goto IL_0020;
		}
	}
	{
		CryptographicException_t3349726436 * L_3 = (CryptographicException_t3349726436 *)il2cpp_codegen_object_new(CryptographicException_t3349726436_il2cpp_TypeInfo_var);
		CryptographicException__ctor_m2415891459(L_3, _stringLiteral3931749060, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		ByteU5BU5D_t3397334013* L_4 = ___value0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_4);
		Il2CppObject * L_5 = Array_Clone_m768574314((Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		__this->set_key__10(((ByteU5BU5D_t3397334013*)Castclass(L_5, ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::GenerateKey()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Random_t1044426839_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicManaged_GenerateKey_m1891111149_MetadataUsageId;
extern "C"  void PkzipClassicManaged_GenerateKey_m1891111149 (PkzipClassicManaged_t4138271613 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_GenerateKey_m1891111149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Random_t1044426839 * V_0 = NULL;
	{
		__this->set_key__10(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)12))));
		Random_t1044426839 * L_0 = (Random_t1044426839 *)il2cpp_codegen_object_new(Random_t1044426839_il2cpp_TypeInfo_var);
		Random__ctor_m1561335652(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Random_t1044426839 * L_1 = V_0;
		ByteU5BU5D_t3397334013* L_2 = __this->get_key__10();
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(8 /* System.Void System.Random::NextBytes(System.Byte[]) */, L_1, L_2);
		return;
	}
}
// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern Il2CppClass* PkzipClassicEncryptCryptoTransform_t13679930_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicManaged_CreateEncryptor_m3706001281_MetadataUsageId;
extern "C"  Il2CppObject * PkzipClassicManaged_CreateEncryptor_m3706001281 (PkzipClassicManaged_t4138271613 * __this, ByteU5BU5D_t3397334013* ___rgbKey0, ByteU5BU5D_t3397334013* ___rgbIV1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_CreateEncryptor_m3706001281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___rgbKey0;
		__this->set_key__10(L_0);
		ByteU5BU5D_t3397334013* L_1 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(12 /* System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::get_Key() */, __this);
		PkzipClassicEncryptCryptoTransform_t13679930 * L_2 = (PkzipClassicEncryptCryptoTransform_t13679930 *)il2cpp_codegen_object_new(PkzipClassicEncryptCryptoTransform_t13679930_il2cpp_TypeInfo_var);
		PkzipClassicEncryptCryptoTransform__ctor_m2801825635(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern Il2CppClass* PkzipClassicDecryptCryptoTransform_t1369162510_il2cpp_TypeInfo_var;
extern const uint32_t PkzipClassicManaged_CreateDecryptor_m2218038735_MetadataUsageId;
extern "C"  Il2CppObject * PkzipClassicManaged_CreateDecryptor_m2218038735 (PkzipClassicManaged_t4138271613 * __this, ByteU5BU5D_t3397334013* ___rgbKey0, ByteU5BU5D_t3397334013* ___rgbIV1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PkzipClassicManaged_CreateDecryptor_m2218038735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___rgbKey0;
		__this->set_key__10(L_0);
		ByteU5BU5D_t3397334013* L_1 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(12 /* System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::get_Key() */, __this);
		PkzipClassicDecryptCryptoTransform_t1369162510 * L_2 = (PkzipClassicDecryptCryptoTransform_t1369162510 *)il2cpp_codegen_object_new(PkzipClassicDecryptCryptoTransform_t1369162510_il2cpp_TypeInfo_var);
		PkzipClassicDecryptCryptoTransform__ctor_m3583175229(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.PkzipClassicManaged::.ctor()
extern "C"  void PkzipClassicManaged__ctor_m1234613529 (PkzipClassicManaged_t4138271613 * __this, const MethodInfo* method)
{
	{
		PkzipClassic__ctor_m1153871058(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Encryption.ZipAESTransform,System.Security.Cryptography.CryptoStreamMode)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1410050277;
extern const uint32_t ZipAESStream__ctor_m2578024602_MetadataUsageId;
extern "C"  void ZipAESStream__ctor_m2578024602 (ZipAESStream_t1390211376 * __this, Stream_t3255436806 * ___stream0, ZipAESTransform_t2148616552 * ___transform1, int32_t ___mode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESStream__ctor_m2578024602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t3255436806 * L_0 = ___stream0;
		ZipAESTransform_t2148616552 * L_1 = ___transform1;
		int32_t L_2 = ___mode2;
		CryptoStream__ctor_m2578098817(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		Stream_t3255436806 * L_3 = ___stream0;
		__this->set__stream_17(L_3);
		ZipAESTransform_t2148616552 * L_4 = ___transform1;
		__this->set__transform_18(L_4);
		__this->set__slideBuffer_19(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024))));
		__this->set__blockAndAuth_22(((int32_t)26));
		int32_t L_5 = ___mode2;
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		Exception_t1927440687 * L_6 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_6, _stringLiteral1410050277, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_003d:
	{
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESStream::Read(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral483904447;
extern Il2CppCodeGenString* _stringLiteral752864801;
extern const uint32_t ZipAESStream_Read_m3324505803_MetadataUsageId;
extern "C"  int32_t ZipAESStream_Read_m3324505803 (ZipAESStream_t1390211376 * __this, ByteU5BU5D_t3397334013* ___outBuffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESStream_Read_m3324505803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ByteU5BU5D_t3397334013* V_7 = NULL;
	int32_t V_8 = 0;
	{
		V_0 = 0;
		goto IL_0185;
	}

IL_0007:
	{
		int32_t L_0 = __this->get__slideBufFreePos_21();
		int32_t L_1 = __this->get__slideBufStartPos_20();
		V_1 = ((int32_t)((int32_t)L_0-(int32_t)L_1));
		int32_t L_2 = __this->get__blockAndAuth_22();
		int32_t L_3 = V_1;
		V_2 = ((int32_t)((int32_t)L_2-(int32_t)L_3));
		ByteU5BU5D_t3397334013* L_4 = __this->get__slideBuffer_19();
		NullCheck(L_4);
		int32_t L_5 = __this->get__slideBufFreePos_21();
		int32_t L_6 = V_2;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))-(int32_t)L_5))) >= ((int32_t)L_6)))
		{
			goto IL_007b;
		}
	}
	{
		V_3 = 0;
		int32_t L_7 = __this->get__slideBufStartPos_20();
		V_4 = L_7;
		goto IL_0057;
	}

IL_003c:
	{
		ByteU5BU5D_t3397334013* L_8 = __this->get__slideBuffer_19();
		int32_t L_9 = V_3;
		ByteU5BU5D_t3397334013* L_10 = __this->get__slideBuffer_19();
		int32_t L_11 = V_4;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		uint8_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (uint8_t)L_13);
		int32_t L_14 = V_4;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)1));
		int32_t L_15 = V_3;
		V_3 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_16 = V_4;
		int32_t L_17 = __this->get__slideBufFreePos_21();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_18 = __this->get__slideBufFreePos_21();
		int32_t L_19 = __this->get__slideBufStartPos_20();
		__this->set__slideBufFreePos_21(((int32_t)((int32_t)L_18-(int32_t)L_19)));
		__this->set__slideBufStartPos_20(0);
	}

IL_007b:
	{
		Stream_t3255436806 * L_20 = __this->get__stream_17();
		ByteU5BU5D_t3397334013* L_21 = __this->get__slideBuffer_19();
		int32_t L_22 = __this->get__slideBufFreePos_21();
		int32_t L_23 = V_2;
		NullCheck(L_20);
		int32_t L_24 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, L_22, L_23);
		V_5 = L_24;
		int32_t L_25 = __this->get__slideBufFreePos_21();
		int32_t L_26 = V_5;
		__this->set__slideBufFreePos_21(((int32_t)((int32_t)L_25+(int32_t)L_26)));
		int32_t L_27 = __this->get__slideBufFreePos_21();
		int32_t L_28 = __this->get__slideBufStartPos_20();
		V_1 = ((int32_t)((int32_t)L_27-(int32_t)L_28));
		int32_t L_29 = V_1;
		int32_t L_30 = __this->get__blockAndAuth_22();
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_00f6;
		}
	}
	{
		ZipAESTransform_t2148616552 * L_31 = __this->get__transform_18();
		ByteU5BU5D_t3397334013* L_32 = __this->get__slideBuffer_19();
		int32_t L_33 = __this->get__slideBufStartPos_20();
		ByteU5BU5D_t3397334013* L_34 = ___outBuffer0;
		int32_t L_35 = ___offset1;
		NullCheck(L_31);
		ZipAESTransform_TransformBlock_m1966603526(L_31, L_32, L_33, ((int32_t)16), L_34, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_0;
		V_0 = ((int32_t)((int32_t)L_36+(int32_t)((int32_t)16)));
		int32_t L_37 = ___offset1;
		___offset1 = ((int32_t)((int32_t)L_37+(int32_t)((int32_t)16)));
		int32_t L_38 = __this->get__slideBufStartPos_20();
		__this->set__slideBufStartPos_20(((int32_t)((int32_t)L_38+(int32_t)((int32_t)16))));
		goto IL_0185;
	}

IL_00f6:
	{
		int32_t L_39 = V_1;
		if ((((int32_t)L_39) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0133;
		}
	}
	{
		int32_t L_40 = V_1;
		V_6 = ((int32_t)((int32_t)L_40-(int32_t)((int32_t)10)));
		ZipAESTransform_t2148616552 * L_41 = __this->get__transform_18();
		ByteU5BU5D_t3397334013* L_42 = __this->get__slideBuffer_19();
		int32_t L_43 = __this->get__slideBufStartPos_20();
		int32_t L_44 = V_6;
		ByteU5BU5D_t3397334013* L_45 = ___outBuffer0;
		int32_t L_46 = ___offset1;
		NullCheck(L_41);
		ZipAESTransform_TransformBlock_m1966603526(L_41, L_42, L_43, L_44, L_45, L_46, /*hidden argument*/NULL);
		int32_t L_47 = V_0;
		int32_t L_48 = V_6;
		V_0 = ((int32_t)((int32_t)L_47+(int32_t)L_48));
		int32_t L_49 = __this->get__slideBufStartPos_20();
		int32_t L_50 = V_6;
		__this->set__slideBufStartPos_20(((int32_t)((int32_t)L_49+(int32_t)L_50)));
		goto IL_0143;
	}

IL_0133:
	{
		int32_t L_51 = V_1;
		if ((((int32_t)L_51) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0143;
		}
	}
	{
		Exception_t1927440687 * L_52 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_52, _stringLiteral483904447, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_52);
	}

IL_0143:
	{
		ZipAESTransform_t2148616552 * L_53 = __this->get__transform_18();
		NullCheck(L_53);
		ByteU5BU5D_t3397334013* L_54 = ZipAESTransform_GetAuthCode_m771081471(L_53, /*hidden argument*/NULL);
		V_7 = L_54;
		V_8 = 0;
		goto IL_017d;
	}

IL_0155:
	{
		ByteU5BU5D_t3397334013* L_55 = V_7;
		int32_t L_56 = V_8;
		NullCheck(L_55);
		int32_t L_57 = L_56;
		uint8_t L_58 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_57));
		ByteU5BU5D_t3397334013* L_59 = __this->get__slideBuffer_19();
		int32_t L_60 = __this->get__slideBufStartPos_20();
		int32_t L_61 = V_8;
		NullCheck(L_59);
		int32_t L_62 = ((int32_t)((int32_t)L_60+(int32_t)L_61));
		uint8_t L_63 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		if ((((int32_t)L_58) == ((int32_t)L_63)))
		{
			goto IL_0177;
		}
	}
	{
		Exception_t1927440687 * L_64 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_64, _stringLiteral752864801, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_64);
	}

IL_0177:
	{
		int32_t L_65 = V_8;
		V_8 = ((int32_t)((int32_t)L_65+(int32_t)1));
	}

IL_017d:
	{
		int32_t L_66 = V_8;
		if ((((int32_t)L_66) < ((int32_t)((int32_t)10))))
		{
			goto IL_0155;
		}
	}
	{
		goto IL_018c;
	}

IL_0185:
	{
		int32_t L_67 = V_0;
		int32_t L_68 = ___count2;
		if ((((int32_t)L_67) < ((int32_t)L_68)))
		{
			goto IL_0007;
		}
	}

IL_018c:
	{
		int32_t L_69 = V_0;
		return L_69;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESStream::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ZipAESStream_Write_m3423171332_MetadataUsageId;
extern "C"  void ZipAESStream_Write_m3423171332 (ZipAESStream_t1390211376 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESStream_Write_m3423171332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::.ctor(System.String,System.Byte[],System.Int32,System.Boolean)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Rfc2898DeriveBytes_t1773348698_il2cpp_TypeInfo_var;
extern Il2CppClass* RijndaelManaged_t1034060848_il2cpp_TypeInfo_var;
extern Il2CppClass* HMACSHA1_t1958407246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3641403569;
extern Il2CppCodeGenString* _stringLiteral4231021007;
extern Il2CppCodeGenString* _stringLiteral81385776;
extern Il2CppCodeGenString* _stringLiteral3206998355;
extern const uint32_t ZipAESTransform__ctor_m1528222691_MetadataUsageId;
extern "C"  void ZipAESTransform__ctor_m1528222691 (ZipAESTransform_t2148616552 * __this, String_t* ___key0, ByteU5BU5D_t3397334013* ___saltBytes1, int32_t ___blockSize2, bool ___writeMode3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform__ctor_m1528222691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rfc2898DeriveBytes_t1773348698 * V_0 = NULL;
	RijndaelManaged_t1034060848 * V_1 = NULL;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	ObjectU5BU5D_t3614634134* V_4 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___blockSize2;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)16))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = ___blockSize2;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)32))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_2 = ___blockSize2;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral3641403569, L_4, _stringLiteral4231021007, /*hidden argument*/NULL);
		Exception_t1927440687 * L_6 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002b:
	{
		ByteU5BU5D_t3397334013* L_7 = ___saltBytes1;
		NullCheck(L_7);
		int32_t L_8 = ___blockSize2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))) == ((int32_t)((int32_t)((int32_t)L_8/(int32_t)2)))))
		{
			goto IL_0070;
		}
	}
	{
		V_4 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		ObjectU5BU5D_t3614634134* L_9 = V_4;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral81385776);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral81385776);
		ObjectU5BU5D_t3614634134* L_10 = V_4;
		int32_t L_11 = ___blockSize2;
		int32_t L_12 = ((int32_t)((int32_t)L_11/(int32_t)2));
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_13);
		ObjectU5BU5D_t3614634134* L_14 = V_4;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral3206998355);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3206998355);
		ObjectU5BU5D_t3614634134* L_15 = V_4;
		int32_t L_16 = ___blockSize2;
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_18);
		ObjectU5BU5D_t3614634134* L_19 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m3881798623(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Exception_t1927440687 * L_21 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_21, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_0070:
	{
		int32_t L_22 = ___blockSize2;
		__this->set__blockSize_0(L_22);
		int32_t L_23 = __this->get__blockSize_0();
		__this->set__encryptBuffer_3(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_23)));
		__this->set__encrPos_4(((int32_t)16));
		String_t* L_24 = ___key0;
		ByteU5BU5D_t3397334013* L_25 = ___saltBytes1;
		Rfc2898DeriveBytes_t1773348698 * L_26 = (Rfc2898DeriveBytes_t1773348698 *)il2cpp_codegen_object_new(Rfc2898DeriveBytes_t1773348698_il2cpp_TypeInfo_var);
		Rfc2898DeriveBytes__ctor_m3326438931(L_26, L_24, L_25, ((int32_t)1000), /*hidden argument*/NULL);
		V_0 = L_26;
		RijndaelManaged_t1034060848 * L_27 = (RijndaelManaged_t1034060848 *)il2cpp_codegen_object_new(RijndaelManaged_t1034060848_il2cpp_TypeInfo_var);
		RijndaelManaged__ctor_m1723326849(L_27, /*hidden argument*/NULL);
		V_1 = L_27;
		RijndaelManaged_t1034060848 * L_28 = V_1;
		NullCheck(L_28);
		VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.Security.Cryptography.SymmetricAlgorithm::set_Mode(System.Security.Cryptography.CipherMode) */, L_28, 2);
		int32_t L_29 = __this->get__blockSize_0();
		__this->set__counterNonce_2(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_29)));
		Rfc2898DeriveBytes_t1773348698 * L_30 = V_0;
		int32_t L_31 = __this->get__blockSize_0();
		NullCheck(L_30);
		ByteU5BU5D_t3397334013* L_32 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.DeriveBytes::GetBytes(System.Int32) */, L_30, L_31);
		V_2 = L_32;
		Rfc2898DeriveBytes_t1773348698 * L_33 = V_0;
		int32_t L_34 = __this->get__blockSize_0();
		NullCheck(L_33);
		ByteU5BU5D_t3397334013* L_35 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.DeriveBytes::GetBytes(System.Int32) */, L_33, L_34);
		V_3 = L_35;
		RijndaelManaged_t1034060848 * L_36 = V_1;
		ByteU5BU5D_t3397334013* L_37 = V_2;
		ByteU5BU5D_t3397334013* L_38 = V_3;
		NullCheck(L_36);
		Il2CppObject * L_39 = VirtFuncInvoker2< Il2CppObject *, ByteU5BU5D_t3397334013*, ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateEncryptor(System.Byte[],System.Byte[]) */, L_36, L_37, L_38);
		__this->set__encryptor_1(L_39);
		Rfc2898DeriveBytes_t1773348698 * L_40 = V_0;
		NullCheck(L_40);
		ByteU5BU5D_t3397334013* L_41 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, int32_t >::Invoke(4 /* System.Byte[] System.Security.Cryptography.DeriveBytes::GetBytes(System.Int32) */, L_40, 2);
		__this->set__pwdVerifier_5(L_41);
		ByteU5BU5D_t3397334013* L_42 = V_3;
		HMACSHA1_t1958407246 * L_43 = (HMACSHA1_t1958407246 *)il2cpp_codegen_object_new(HMACSHA1_t1958407246_il2cpp_TypeInfo_var);
		HMACSHA1__ctor_m2027292306(L_43, L_42, /*hidden argument*/NULL);
		__this->set__hmacsha1_6(L_43);
		bool L_44 = ___writeMode3;
		__this->set__writeMode_8(L_44);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern Il2CppClass* ICryptoTransform_t281704372_il2cpp_TypeInfo_var;
extern const uint32_t ZipAESTransform_TransformBlock_m1966603526_MetadataUsageId;
extern "C"  int32_t ZipAESTransform_TransformBlock_m1966603526 (ZipAESTransform_t2148616552 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t3397334013* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_TransformBlock_m1966603526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = 0x0;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get__writeMode_8();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		HMACSHA1_t1958407246 * L_1 = __this->get__hmacsha1_6();
		ByteU5BU5D_t3397334013* L_2 = ___inputBuffer0;
		int32_t L_3 = ___inputOffset1;
		int32_t L_4 = ___inputCount2;
		ByteU5BU5D_t3397334013* L_5 = ___inputBuffer0;
		int32_t L_6 = ___inputOffset1;
		NullCheck(L_1);
		HashAlgorithm_TransformBlock_m4190258810(L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0019:
	{
		V_0 = 0;
		goto IL_00a2;
	}

IL_0020:
	{
		int32_t L_7 = __this->get__encrPos_4();
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0078;
		}
	}
	{
		V_1 = 0;
		goto IL_0032;
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0032:
	{
		ByteU5BU5D_t3397334013* L_9 = __this->get__counterNonce_2();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		uint8_t* L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)));
		int32_t L_12 = (((int32_t)((uint8_t)((int32_t)((int32_t)(*(uint8_t*)L_11)+(int32_t)1)))));
		V_2 = L_12;
		(*(uint8_t*)L_11) = L_12;
		uint8_t L_13 = V_2;
		if (!L_13)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_14 = __this->get__encryptor_1();
		ByteU5BU5D_t3397334013* L_15 = __this->get__counterNonce_2();
		int32_t L_16 = __this->get__blockSize_0();
		ByteU5BU5D_t3397334013* L_17 = __this->get__encryptBuffer_3();
		NullCheck(L_14);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t, ByteU5BU5D_t3397334013*, int32_t >::Invoke(4 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t281704372_il2cpp_TypeInfo_var, L_14, L_15, 0, L_16, L_17, 0);
		__this->set__encrPos_4(0);
	}

IL_0078:
	{
		ByteU5BU5D_t3397334013* L_18 = ___outputBuffer3;
		int32_t L_19 = V_0;
		int32_t L_20 = ___outputOffset4;
		ByteU5BU5D_t3397334013* L_21 = ___inputBuffer0;
		int32_t L_22 = V_0;
		int32_t L_23 = ___inputOffset1;
		NullCheck(L_21);
		int32_t L_24 = ((int32_t)((int32_t)L_22+(int32_t)L_23));
		uint8_t L_25 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ByteU5BU5D_t3397334013* L_26 = __this->get__encryptBuffer_3();
		int32_t L_27 = __this->get__encrPos_4();
		int32_t L_28 = L_27;
		V_3 = L_28;
		__this->set__encrPos_4(((int32_t)((int32_t)L_28+(int32_t)1)));
		int32_t L_29 = V_3;
		NullCheck(L_26);
		int32_t L_30 = L_29;
		uint8_t L_31 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)L_20))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_25^(int32_t)L_31))))));
		int32_t L_32 = V_0;
		V_0 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00a2:
	{
		int32_t L_33 = V_0;
		int32_t L_34 = ___inputCount2;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0020;
		}
	}
	{
		bool L_35 = __this->get__writeMode_8();
		if (!L_35)
		{
			goto IL_00c6;
		}
	}
	{
		HMACSHA1_t1958407246 * L_36 = __this->get__hmacsha1_6();
		ByteU5BU5D_t3397334013* L_37 = ___outputBuffer3;
		int32_t L_38 = ___outputOffset4;
		int32_t L_39 = ___inputCount2;
		ByteU5BU5D_t3397334013* L_40 = ___outputBuffer3;
		int32_t L_41 = ___outputOffset4;
		NullCheck(L_36);
		HashAlgorithm_TransformBlock_m4190258810(L_36, L_37, L_38, L_39, L_40, L_41, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		int32_t L_42 = ___inputCount2;
		return L_42;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_PwdVerifier()
extern "C"  ByteU5BU5D_t3397334013* ZipAESTransform_get_PwdVerifier_m1365083820 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get__pwdVerifier_5();
		return L_0;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::GetAuthCode()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t ZipAESTransform_GetAuthCode_m771081471_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* ZipAESTransform_GetAuthCode_m771081471 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_GetAuthCode_m771081471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		bool L_0 = __this->get__finalised_7();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)0));
		HMACSHA1_t1958407246 * L_1 = __this->get__hmacsha1_6();
		ByteU5BU5D_t3397334013* L_2 = V_0;
		NullCheck(L_1);
		HashAlgorithm_TransformFinalBlock_m3071802428(L_1, L_2, 0, 0, /*hidden argument*/NULL);
		__this->set__finalised_7((bool)1);
	}

IL_0025:
	{
		HMACSHA1_t1958407246 * L_3 = __this->get__hmacsha1_6();
		NullCheck(L_3);
		ByteU5BU5D_t3397334013* L_4 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(13 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash() */, L_3);
		return L_4;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2044384747;
extern const uint32_t ZipAESTransform_TransformFinalBlock_m704909432_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* ZipAESTransform_TransformFinalBlock_m704909432 (ZipAESTransform_t2148616552 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_TransformFinalBlock_m704909432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral2044384747, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_InputBlockSize()
extern "C"  int32_t ZipAESTransform_get_InputBlockSize_m3304664717 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__blockSize_0();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_OutputBlockSize()
extern "C"  int32_t ZipAESTransform_get_OutputBlockSize_m3614661226 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__blockSize_0();
		return L_0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanTransformMultipleBlocks()
extern "C"  bool ZipAESTransform_get_CanTransformMultipleBlocks_m338158751 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanReuseTransform()
extern "C"  bool ZipAESTransform_get_CanReuseTransform_m3504446733 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t ZipAESTransform_Dispose_m2147833679_MetadataUsageId;
extern "C"  void ZipAESTransform_Dispose_m2147833679 (ZipAESTransform_t2148616552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_Dispose_m2147833679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__encryptor_1();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SharpZipBaseException__ctor_m1246294255 (SharpZipBaseException_t4116900137 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		ApplicationException__ctor_m2143262149(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor()
extern "C"  void SharpZipBaseException__ctor_m3776338968 (SharpZipBaseException_t4116900137 * __this, const MethodInfo* method)
{
	{
		ApplicationException__ctor_m2097946732(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.String)
extern "C"  void SharpZipBaseException__ctor_m3232255894 (SharpZipBaseException_t4116900137 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ApplicationException__ctor_m1120437222(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterHuffman_t343008798_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t3104283263_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x600029dU2D1_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x600029dU2D2_3_FieldInfo_var;
extern const uint32_t DeflaterHuffman__cctor_m456922121_MetadataUsageId;
extern "C"  void DeflaterHuffman__cctor_m456922121 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman__cctor_m456922121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x600029dU2D1_2_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->set_BL_ORDER_0(L_0);
		ByteU5BU5D_t3397334013* L_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x600029dU2D2_3_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->set_bit4Reverse_1(L_1);
		((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->set_staticLCodes_2(((Int16U5BU5D_t3104283263*)SZArrayNew(Int16U5BU5D_t3104283263_il2cpp_TypeInfo_var, (uint32_t)((int32_t)286))));
		((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->set_staticLLength_3(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)286))));
		V_0 = 0;
		goto IL_006e;
	}

IL_0050:
	{
		Int16U5BU5D_t3104283263* L_2 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_3 = V_0;
		int32_t L_4 = V_0;
		int16_t L_5 = DeflaterHuffman_BitReverse_m4273222008(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)L_4))<<(int32_t)8)), /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (int16_t)L_5);
		ByteU5BU5D_t3397334013* L_6 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_7 = V_0;
		int32_t L_8 = L_7;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (uint8_t)8);
	}

IL_006e:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)144))))
		{
			goto IL_0050;
		}
	}
	{
		goto IL_009a;
	}

IL_0078:
	{
		Int16U5BU5D_t3104283263* L_10 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_11 = V_0;
		int32_t L_12 = V_0;
		int16_t L_13 = DeflaterHuffman_BitReverse_m4273222008(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)256)+(int32_t)L_12))<<(int32_t)7)), /*hidden argument*/NULL);
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int16_t)L_13);
		ByteU5BU5D_t3397334013* L_14 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_15 = V_0;
		int32_t L_16 = L_15;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (uint8_t)((int32_t)9));
	}

IL_009a:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)256))))
		{
			goto IL_0078;
		}
	}
	{
		goto IL_00c6;
	}

IL_00a4:
	{
		Int16U5BU5D_t3104283263* L_18 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_19 = V_0;
		int32_t L_20 = V_0;
		int16_t L_21 = DeflaterHuffman_BitReverse_m4273222008(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-256)+(int32_t)L_20))<<(int32_t)((int32_t)9))), /*hidden argument*/NULL);
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (int16_t)L_21);
		ByteU5BU5D_t3397334013* L_22 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_23 = V_0;
		int32_t L_24 = L_23;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (uint8_t)7);
	}

IL_00c6:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) < ((int32_t)((int32_t)280))))
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00ee;
	}

IL_00d0:
	{
		Int16U5BU5D_t3104283263* L_26 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_27 = V_0;
		int32_t L_28 = V_0;
		int16_t L_29 = DeflaterHuffman_BitReverse_m4273222008(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-88)+(int32_t)L_28))<<(int32_t)8)), /*hidden argument*/NULL);
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int16_t)L_29);
		ByteU5BU5D_t3397334013* L_30 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_31 = V_0;
		int32_t L_32 = L_31;
		V_0 = ((int32_t)((int32_t)L_32+(int32_t)1));
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (uint8_t)8);
	}

IL_00ee:
	{
		int32_t L_33 = V_0;
		if ((((int32_t)L_33) < ((int32_t)((int32_t)286))))
		{
			goto IL_00d0;
		}
	}
	{
		((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->set_staticDCodes_4(((Int16U5BU5D_t3104283263*)SZArrayNew(Int16U5BU5D_t3104283263_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30))));
		((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->set_staticDLength_5(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30))));
		V_0 = 0;
		goto IL_012e;
	}

IL_0112:
	{
		Int16U5BU5D_t3104283263* L_34 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticDCodes_4();
		int32_t L_35 = V_0;
		int32_t L_36 = V_0;
		int16_t L_37 = DeflaterHuffman_BitReverse_m4273222008(NULL /*static, unused*/, ((int32_t)((int32_t)L_36<<(int32_t)((int32_t)11))), /*hidden argument*/NULL);
		NullCheck(L_34);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(L_35), (int16_t)L_37);
		ByteU5BU5D_t3397334013* L_38 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_staticDLength_5();
		int32_t L_39 = V_0;
		NullCheck(L_38);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(L_39), (uint8_t)5);
		int32_t L_40 = V_0;
		V_0 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_012e:
	{
		int32_t L_41 = V_0;
		if ((((int32_t)L_41) < ((int32_t)((int32_t)30))))
		{
			goto IL_0112;
		}
	}
	{
		return;
	}
}
// System.Int16 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
extern Il2CppClass* DeflaterHuffman_t343008798_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterHuffman_BitReverse_m4273222008_MetadataUsageId;
extern "C"  int16_t DeflaterHuffman_BitReverse_m4273222008 (Il2CppObject * __this /* static, unused */, int32_t ___toReverse0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman_BitReverse_m4273222008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t343008798_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_1 = ___toReverse0;
		NullCheck(L_0);
		int32_t L_2 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)15)));
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t3397334013* L_4 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_5 = ___toReverse0;
		NullCheck(L_4);
		int32_t L_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5>>(int32_t)4))&(int32_t)((int32_t)15)));
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		ByteU5BU5D_t3397334013* L_8 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_9 = ___toReverse0;
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9>>(int32_t)8))&(int32_t)((int32_t)15)));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		ByteU5BU5D_t3397334013* L_12 = ((DeflaterHuffman_t343008798_StaticFields*)DeflaterHuffman_t343008798_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_13 = ___toReverse0;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)((int32_t)L_13>>(int32_t)((int32_t)12)));
		uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		return (((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)12)))|(int32_t)((int32_t)((int32_t)L_7<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)L_11<<(int32_t)4))))|(int32_t)L_15)))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern Il2CppClass* Adler32_t1759601101_il2cpp_TypeInfo_var;
extern Il2CppClass* StreamManipulator_t1922289728_il2cpp_TypeInfo_var;
extern Il2CppClass* OutputWindow_t824811027_il2cpp_TypeInfo_var;
extern const uint32_t Inflater__ctor_m1334719096_MetadataUsageId;
extern "C"  void Inflater__ctor_m1334719096 (Inflater_t3958302971 * __this, bool ___noHeader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inflater__ctor_m1334719096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Inflater_t3958302971 * G_B2_0 = NULL;
	Inflater_t3958302971 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Inflater_t3958302971 * G_B3_1 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		bool L_0 = ___noHeader0;
		__this->set_noHeader_13(L_0);
		Adler32_t1759601101 * L_1 = (Adler32_t1759601101 *)il2cpp_codegen_object_new(Adler32_t1759601101_il2cpp_TypeInfo_var);
		Adler32__ctor_m3717921916(L_1, /*hidden argument*/NULL);
		__this->set_adler_19(L_1);
		StreamManipulator_t1922289728 * L_2 = (StreamManipulator_t1922289728 *)il2cpp_codegen_object_new(StreamManipulator_t1922289728_il2cpp_TypeInfo_var);
		StreamManipulator__ctor_m1888846459(L_2, /*hidden argument*/NULL);
		__this->set_input_14(L_2);
		OutputWindow_t824811027 * L_3 = (OutputWindow_t824811027 *)il2cpp_codegen_object_new(OutputWindow_t824811027_il2cpp_TypeInfo_var);
		OutputWindow__ctor_m2270688772(L_3, /*hidden argument*/NULL);
		__this->set_outputWindow_15(L_3);
		bool L_4 = ___noHeader0;
		G_B1_0 = __this;
		if (L_4)
		{
			G_B2_0 = __this;
			goto IL_0035;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 2;
		G_B3_1 = G_B2_0;
	}

IL_0036:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mode_4(G_B3_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Reset()
extern "C"  void Inflater_Reset_m3735238578 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	Inflater_t3958302971 * G_B2_0 = NULL;
	Inflater_t3958302971 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Inflater_t3958302971 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_noHeader_13();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 2;
		G_B3_1 = G_B2_0;
	}

IL_000d:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mode_4(G_B3_0);
		__this->set_totalIn_12((((int64_t)((int64_t)0))));
		__this->set_totalOut_11((((int64_t)((int64_t)0))));
		StreamManipulator_t1922289728 * L_1 = __this->get_input_14();
		NullCheck(L_1);
		StreamManipulator_Reset_m13518536(L_1, /*hidden argument*/NULL);
		OutputWindow_t824811027 * L_2 = __this->get_outputWindow_15();
		NullCheck(L_2);
		OutputWindow_Reset_m545756291(L_2, /*hidden argument*/NULL);
		__this->set_dynHeader_16((InflaterDynHeader_t3298452259 *)NULL);
		__this->set_litlenTree_17((InflaterHuffmanTree_t3246684106 *)NULL);
		__this->set_distTree_18((InflaterHuffmanTree_t3246684106 *)NULL);
		__this->set_isLastBlock_10((bool)0);
		Adler32_t1759601101 * L_3 = __this->get_adler_19();
		NullCheck(L_3);
		Adler32_Reset_m2520109847(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3888897392;
extern Il2CppCodeGenString* _stringLiteral85063797;
extern const uint32_t Inflater_DecodeHeader_m308898150_MetadataUsageId;
extern "C"  bool Inflater_DecodeHeader_m308898150 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inflater_DecodeHeader_m308898150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		StreamManipulator_t1922289728 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m1052229955(L_0, ((int32_t)16), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		StreamManipulator_t1922289728 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m4077687679(L_3, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)((int32_t)((int32_t)L_5>>(int32_t)8))))&(int32_t)((int32_t)65535)));
		int32_t L_6 = V_0;
		if (!((int32_t)((int32_t)L_6%(int32_t)((int32_t)31))))
		{
			goto IL_0040;
		}
	}
	{
		SharpZipBaseException_t4116900137 * L_7 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_7, _stringLiteral3888897392, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0040:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)3840)))) == ((int32_t)((int32_t)2048))))
		{
			goto IL_0059;
		}
	}
	{
		SharpZipBaseException_t4116900137 * L_9 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_9, _stringLiteral85063797, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0059:
	{
		int32_t L_10 = V_0;
		if (((int32_t)((int32_t)L_10&(int32_t)((int32_t)32))))
		{
			goto IL_0068;
		}
	}
	{
		__this->set_mode_4(2);
		goto IL_0077;
	}

IL_0068:
	{
		__this->set_mode_4(1);
		__this->set_neededBits_6(((int32_t)32));
	}

IL_0077:
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern "C"  bool Inflater_DecodeDict_m4058750473 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_003f;
	}

IL_0002:
	{
		StreamManipulator_t1922289728 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m1052229955(L_0, 8, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		StreamManipulator_t1922289728 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m4077687679(L_3, 8, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_readAdler_5();
		int32_t L_5 = V_0;
		__this->set_readAdler_5(((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)L_5)));
		int32_t L_6 = __this->get_neededBits_6();
		__this->set_neededBits_6(((int32_t)((int32_t)L_6-(int32_t)8)));
	}

IL_003f:
	{
		int32_t L_7 = __this->get_neededBits_6();
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern Il2CppClass* Inflater_t3958302971_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral921496558;
extern Il2CppCodeGenString* _stringLiteral1606390926;
extern Il2CppCodeGenString* _stringLiteral335350342;
extern const uint32_t Inflater_DecodeHuffman_m779657766_MetadataUsageId;
extern "C"  bool Inflater_DecodeHuffman_m779657766 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inflater_DecodeHuffman_m779657766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OutputWindow_t824811027 * L_0 = __this->get_outputWindow_15();
		NullCheck(L_0);
		int32_t L_1 = OutputWindow_GetFreeSpace_m841544794(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_01d0;
	}

IL_0011:
	{
		int32_t L_2 = __this->get_mode_4();
		V_4 = L_2;
		int32_t L_3 = V_4;
		switch (((int32_t)((int32_t)L_3-(int32_t)7)))
		{
			case 0:
			{
				goto IL_0051;
			}
			case 1:
			{
				goto IL_00c5;
			}
			case 2:
			{
				goto IL_0114;
			}
			case 3:
			{
				goto IL_0154;
			}
		}
	}
	{
		goto IL_01c5;
	}

IL_0037:
	{
		OutputWindow_t824811027 * L_4 = __this->get_outputWindow_15();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		OutputWindow_Write_m1680840174(L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = ((int32_t)((int32_t)L_6-(int32_t)1));
		V_0 = L_7;
		if ((((int32_t)L_7) >= ((int32_t)((int32_t)258))))
		{
			goto IL_0051;
		}
	}
	{
		return (bool)1;
	}

IL_0051:
	{
		InflaterHuffmanTree_t3246684106 * L_8 = __this->get_litlenTree_17();
		StreamManipulator_t1922289728 * L_9 = __this->get_input_14();
		NullCheck(L_8);
		int32_t L_10 = InflaterHuffmanTree_GetSymbol_m189842273(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		V_1 = L_11;
		if (!((int32_t)((int32_t)L_11&(int32_t)((int32_t)-256))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) >= ((int32_t)((int32_t)257))))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_13 = V_1;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_007a;
		}
	}
	{
		return (bool)0;
	}

IL_007a:
	{
		__this->set_distTree_18((InflaterHuffmanTree_t3246684106 *)NULL);
		__this->set_litlenTree_17((InflaterHuffmanTree_t3246684106 *)NULL);
		__this->set_mode_4(2);
		return (bool)1;
	}

IL_0091:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Inflater_t3958302971_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3030399641* L_14 = ((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->get_CPLENS_0();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)((int32_t)L_15-(int32_t)((int32_t)257)));
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		__this->set_repLength_7(L_17);
		Int32U5BU5D_t3030399641* L_18 = ((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->get_CPLEXT_1();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)((int32_t)L_19-(int32_t)((int32_t)257)));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		__this->set_neededBits_6(L_21);
		goto IL_00c5;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00b9;
		throw e;
	}

CATCH_00b9:
	{ // begin catch(System.Exception)
		SharpZipBaseException_t4116900137 * L_22 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_22, _stringLiteral921496558, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	} // end catch (depth: 1)

IL_00c5:
	{
		int32_t L_23 = __this->get_neededBits_6();
		if ((((int32_t)L_23) <= ((int32_t)0)))
		{
			goto IL_010c;
		}
	}
	{
		__this->set_mode_4(8);
		StreamManipulator_t1922289728 * L_24 = __this->get_input_14();
		int32_t L_25 = __this->get_neededBits_6();
		NullCheck(L_24);
		int32_t L_26 = StreamManipulator_PeekBits_m1052229955(L_24, L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		int32_t L_27 = V_2;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_00ed;
		}
	}
	{
		return (bool)0;
	}

IL_00ed:
	{
		StreamManipulator_t1922289728 * L_28 = __this->get_input_14();
		int32_t L_29 = __this->get_neededBits_6();
		NullCheck(L_28);
		StreamManipulator_DropBits_m4077687679(L_28, L_29, /*hidden argument*/NULL);
		int32_t L_30 = __this->get_repLength_7();
		int32_t L_31 = V_2;
		__this->set_repLength_7(((int32_t)((int32_t)L_30+(int32_t)L_31)));
	}

IL_010c:
	{
		__this->set_mode_4(((int32_t)9));
	}

IL_0114:
	{
		InflaterHuffmanTree_t3246684106 * L_32 = __this->get_distTree_18();
		StreamManipulator_t1922289728 * L_33 = __this->get_input_14();
		NullCheck(L_32);
		int32_t L_34 = InflaterHuffmanTree_GetSymbol_m189842273(L_32, L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)0)))
		{
			goto IL_012c;
		}
	}
	{
		return (bool)0;
	}

IL_012c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Inflater_t3958302971_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3030399641* L_36 = ((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->get_CPDIST_2();
		int32_t L_37 = V_1;
		NullCheck(L_36);
		int32_t L_38 = L_37;
		int32_t L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		__this->set_repDist_8(L_39);
		Int32U5BU5D_t3030399641* L_40 = ((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->get_CPDEXT_3();
		int32_t L_41 = V_1;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		__this->set_neededBits_6(L_43);
		goto IL_0154;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0148;
		throw e;
	}

CATCH_0148:
	{ // begin catch(System.Exception)
		SharpZipBaseException_t4116900137 * L_44 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_44, _stringLiteral1606390926, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	} // end catch (depth: 1)

IL_0154:
	{
		int32_t L_45 = __this->get_neededBits_6();
		if ((((int32_t)L_45) <= ((int32_t)0)))
		{
			goto IL_019c;
		}
	}
	{
		__this->set_mode_4(((int32_t)10));
		StreamManipulator_t1922289728 * L_46 = __this->get_input_14();
		int32_t L_47 = __this->get_neededBits_6();
		NullCheck(L_46);
		int32_t L_48 = StreamManipulator_PeekBits_m1052229955(L_46, L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		int32_t L_49 = V_3;
		if ((((int32_t)L_49) >= ((int32_t)0)))
		{
			goto IL_017d;
		}
	}
	{
		return (bool)0;
	}

IL_017d:
	{
		StreamManipulator_t1922289728 * L_50 = __this->get_input_14();
		int32_t L_51 = __this->get_neededBits_6();
		NullCheck(L_50);
		StreamManipulator_DropBits_m4077687679(L_50, L_51, /*hidden argument*/NULL);
		int32_t L_52 = __this->get_repDist_8();
		int32_t L_53 = V_3;
		__this->set_repDist_8(((int32_t)((int32_t)L_52+(int32_t)L_53)));
	}

IL_019c:
	{
		OutputWindow_t824811027 * L_54 = __this->get_outputWindow_15();
		int32_t L_55 = __this->get_repLength_7();
		int32_t L_56 = __this->get_repDist_8();
		NullCheck(L_54);
		OutputWindow_Repeat_m4052354553(L_54, L_55, L_56, /*hidden argument*/NULL);
		int32_t L_57 = V_0;
		int32_t L_58 = __this->get_repLength_7();
		V_0 = ((int32_t)((int32_t)L_57-(int32_t)L_58));
		__this->set_mode_4(7);
		goto IL_01d0;
	}

IL_01c5:
	{
		SharpZipBaseException_t4116900137 * L_59 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_59, _stringLiteral335350342, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_59);
	}

IL_01d0:
	{
		int32_t L_60 = V_0;
		if ((((int32_t)L_60) >= ((int32_t)((int32_t)258))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2334256028;
extern Il2CppCodeGenString* _stringLiteral3517393831;
extern const uint32_t Inflater_DecodeChksum_m3189548788_MetadataUsageId;
extern "C"  bool Inflater_DecodeChksum_m3189548788 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inflater_DecodeChksum_m3189548788_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	{
		goto IL_003f;
	}

IL_0002:
	{
		StreamManipulator_t1922289728 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m1052229955(L_0, 8, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		StreamManipulator_t1922289728 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m4077687679(L_3, 8, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_readAdler_5();
		int32_t L_5 = V_0;
		__this->set_readAdler_5(((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)L_5)));
		int32_t L_6 = __this->get_neededBits_6();
		__this->set_neededBits_6(((int32_t)((int32_t)L_6-(int32_t)8)));
	}

IL_003f:
	{
		int32_t L_7 = __this->get_neededBits_6();
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		Adler32_t1759601101 * L_8 = __this->get_adler_19();
		NullCheck(L_8);
		int64_t L_9 = Adler32_get_Value_m24599817(L_8, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_readAdler_5();
		if ((((int32_t)(((int32_t)((int32_t)L_9)))) == ((int32_t)L_10)))
		{
			goto IL_00a1;
		}
	}
	{
		V_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		ObjectU5BU5D_t3614634134* L_11 = V_1;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral2334256028);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2334256028);
		ObjectU5BU5D_t3614634134* L_12 = V_1;
		Adler32_t1759601101 * L_13 = __this->get_adler_19();
		NullCheck(L_13);
		int64_t L_14 = Adler32_get_Value_m24599817(L_13, /*hidden argument*/NULL);
		int32_t L_15 = (((int32_t)((int32_t)L_14)));
		Il2CppObject * L_16 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_16);
		ObjectU5BU5D_t3614634134* L_17 = V_1;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral3517393831);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3517393831);
		ObjectU5BU5D_t3614634134* L_18 = V_1;
		int32_t L_19 = __this->get_readAdler_5();
		int32_t L_20 = L_19;
		Il2CppObject * L_21 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_21);
		ObjectU5BU5D_t3614634134* L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m3881798623(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		SharpZipBaseException_t4116900137 * L_24 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_24, L_23, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00a1:
	{
		__this->set_mode_4(((int32_t)12));
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Decode()
extern Il2CppClass* InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1582075645;
extern Il2CppCodeGenString* _stringLiteral867523666;
extern Il2CppCodeGenString* _stringLiteral1696980;
extern const uint32_t Inflater_Decode_m891627623_MetadataUsageId;
extern "C"  bool Inflater_Decode_m891627623 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inflater_Decode_m891627623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0 = __this->get_mode_4();
		V_3 = L_0;
		int32_t L_1 = V_3;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0046;
			}
			case 1:
			{
				goto IL_004d;
			}
			case 2:
			{
				goto IL_005b;
			}
			case 3:
			{
				goto IL_0136;
			}
			case 4:
			{
				goto IL_0167;
			}
			case 5:
			{
				goto IL_01a9;
			}
			case 6:
			{
				goto IL_01ef;
			}
			case 7:
			{
				goto IL_022d;
			}
			case 8:
			{
				goto IL_022d;
			}
			case 9:
			{
				goto IL_022d;
			}
			case 10:
			{
				goto IL_022d;
			}
			case 11:
			{
				goto IL_0054;
			}
			case 12:
			{
				goto IL_0234;
			}
		}
	}
	{
		goto IL_0236;
	}

IL_0046:
	{
		bool L_2 = Inflater_DecodeHeader_m308898150(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_004d:
	{
		bool L_3 = Inflater_DecodeDict_m4058750473(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0054:
	{
		bool L_4 = Inflater_DecodeChksum_m3189548788(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_005b:
	{
		bool L_5 = __this->get_isLastBlock_10();
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		bool L_6 = __this->get_noHeader_13();
		if (!L_6)
		{
			goto IL_0075;
		}
	}
	{
		__this->set_mode_4(((int32_t)12));
		return (bool)0;
	}

IL_0075:
	{
		StreamManipulator_t1922289728 * L_7 = __this->get_input_14();
		NullCheck(L_7);
		StreamManipulator_SkipToByteBoundary_m2487502935(L_7, /*hidden argument*/NULL);
		__this->set_neededBits_6(((int32_t)32));
		__this->set_mode_4(((int32_t)11));
		return (bool)1;
	}

IL_0092:
	{
		StreamManipulator_t1922289728 * L_8 = __this->get_input_14();
		NullCheck(L_8);
		int32_t L_9 = StreamManipulator_PeekBits_m1052229955(L_8, 3, /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_00a5;
		}
	}
	{
		return (bool)0;
	}

IL_00a5:
	{
		StreamManipulator_t1922289728 * L_11 = __this->get_input_14();
		NullCheck(L_11);
		StreamManipulator_DropBits_m4077687679(L_11, 3, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		if (!((int32_t)((int32_t)L_12&(int32_t)1)))
		{
			goto IL_00bd;
		}
	}
	{
		__this->set_isLastBlock_10((bool)1);
	}

IL_00bd:
	{
		int32_t L_13 = V_0;
		V_4 = ((int32_t)((int32_t)L_13>>(int32_t)1));
		int32_t L_14 = V_4;
		switch (L_14)
		{
			case 0:
			{
				goto IL_00d7;
			}
			case 1:
			{
				goto IL_00eb;
			}
			case 2:
			{
				goto IL_010a;
			}
		}
	}
	{
		goto IL_011e;
	}

IL_00d7:
	{
		StreamManipulator_t1922289728 * L_15 = __this->get_input_14();
		NullCheck(L_15);
		StreamManipulator_SkipToByteBoundary_m2487502935(L_15, /*hidden argument*/NULL);
		__this->set_mode_4(3);
		goto IL_0134;
	}

IL_00eb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var);
		InflaterHuffmanTree_t3246684106 * L_16 = ((InflaterHuffmanTree_t3246684106_StaticFields*)InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var->static_fields)->get_defLitLenTree_1();
		__this->set_litlenTree_17(L_16);
		InflaterHuffmanTree_t3246684106 * L_17 = ((InflaterHuffmanTree_t3246684106_StaticFields*)InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var->static_fields)->get_defDistTree_2();
		__this->set_distTree_18(L_17);
		__this->set_mode_4(7);
		goto IL_0134;
	}

IL_010a:
	{
		InflaterDynHeader_t3298452259 * L_18 = (InflaterDynHeader_t3298452259 *)il2cpp_codegen_object_new(InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var);
		InflaterDynHeader__ctor_m1417066751(L_18, /*hidden argument*/NULL);
		__this->set_dynHeader_16(L_18);
		__this->set_mode_4(6);
		goto IL_0134;
	}

IL_011e:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = L_19;
		Il2CppObject * L_21 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1582075645, L_21, /*hidden argument*/NULL);
		SharpZipBaseException_t4116900137 * L_23 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_23, L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
	}

IL_0134:
	{
		return (bool)1;
	}

IL_0136:
	{
		StreamManipulator_t1922289728 * L_24 = __this->get_input_14();
		NullCheck(L_24);
		int32_t L_25 = StreamManipulator_PeekBits_m1052229955(L_24, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_26 = L_25;
		V_5 = L_26;
		__this->set_uncomprLen_9(L_26);
		int32_t L_27 = V_5;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_0153;
		}
	}
	{
		return (bool)0;
	}

IL_0153:
	{
		StreamManipulator_t1922289728 * L_28 = __this->get_input_14();
		NullCheck(L_28);
		StreamManipulator_DropBits_m4077687679(L_28, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_mode_4(4);
	}

IL_0167:
	{
		StreamManipulator_t1922289728 * L_29 = __this->get_input_14();
		NullCheck(L_29);
		int32_t L_30 = StreamManipulator_PeekBits_m1052229955(L_29, ((int32_t)16), /*hidden argument*/NULL);
		V_1 = L_30;
		int32_t L_31 = V_1;
		if ((((int32_t)L_31) >= ((int32_t)0)))
		{
			goto IL_017b;
		}
	}
	{
		return (bool)0;
	}

IL_017b:
	{
		StreamManipulator_t1922289728 * L_32 = __this->get_input_14();
		NullCheck(L_32);
		StreamManipulator_DropBits_m4077687679(L_32, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_33 = V_1;
		int32_t L_34 = __this->get_uncomprLen_9();
		if ((((int32_t)L_33) == ((int32_t)((int32_t)((int32_t)L_34^(int32_t)((int32_t)65535))))))
		{
			goto IL_01a2;
		}
	}
	{
		SharpZipBaseException_t4116900137 * L_35 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_35, _stringLiteral867523666, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_01a2:
	{
		__this->set_mode_4(5);
	}

IL_01a9:
	{
		OutputWindow_t824811027 * L_36 = __this->get_outputWindow_15();
		StreamManipulator_t1922289728 * L_37 = __this->get_input_14();
		int32_t L_38 = __this->get_uncomprLen_9();
		NullCheck(L_36);
		int32_t L_39 = OutputWindow_CopyStored_m1154690660(L_36, L_37, L_38, /*hidden argument*/NULL);
		V_2 = L_39;
		int32_t L_40 = __this->get_uncomprLen_9();
		int32_t L_41 = V_2;
		__this->set_uncomprLen_9(((int32_t)((int32_t)L_40-(int32_t)L_41)));
		int32_t L_42 = __this->get_uncomprLen_9();
		if (L_42)
		{
			goto IL_01e0;
		}
	}
	{
		__this->set_mode_4(2);
		return (bool)1;
	}

IL_01e0:
	{
		StreamManipulator_t1922289728 * L_43 = __this->get_input_14();
		NullCheck(L_43);
		bool L_44 = StreamManipulator_get_IsNeedingInput_m2895772626(L_43, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_44) == ((int32_t)0))? 1 : 0);
	}

IL_01ef:
	{
		InflaterDynHeader_t3298452259 * L_45 = __this->get_dynHeader_16();
		StreamManipulator_t1922289728 * L_46 = __this->get_input_14();
		NullCheck(L_45);
		bool L_47 = InflaterDynHeader_Decode_m2190858166(L_45, L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_0204;
		}
	}
	{
		return (bool)0;
	}

IL_0204:
	{
		InflaterDynHeader_t3298452259 * L_48 = __this->get_dynHeader_16();
		NullCheck(L_48);
		InflaterHuffmanTree_t3246684106 * L_49 = InflaterDynHeader_BuildLitLenTree_m63482416(L_48, /*hidden argument*/NULL);
		__this->set_litlenTree_17(L_49);
		InflaterDynHeader_t3298452259 * L_50 = __this->get_dynHeader_16();
		NullCheck(L_50);
		InflaterHuffmanTree_t3246684106 * L_51 = InflaterDynHeader_BuildDistTree_m554109756(L_50, /*hidden argument*/NULL);
		__this->set_distTree_18(L_51);
		__this->set_mode_4(7);
	}

IL_022d:
	{
		bool L_52 = Inflater_DecodeHuffman_m779657766(__this, /*hidden argument*/NULL);
		return L_52;
	}

IL_0234:
	{
		return (bool)0;
	}

IL_0236:
	{
		SharpZipBaseException_t4116900137 * L_53 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_53, _stringLiteral1696980, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_53);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void Inflater_SetInput_m1225927750 (Inflater_t3958302971 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___index1, int32_t ___count2, const MethodInfo* method)
{
	{
		StreamManipulator_t1922289728 * L_0 = __this->get_input_14();
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t L_2 = ___index1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		StreamManipulator_SetInput_m3559023036(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		int64_t L_4 = __this->get_totalIn_12();
		int32_t L_5 = ___count2;
		__this->set_totalIn_12(((int64_t)((int64_t)L_4+(int64_t)(((int64_t)((int64_t)L_5))))));
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral1421405844;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern Il2CppCodeGenString* _stringLiteral2228609792;
extern Il2CppCodeGenString* _stringLiteral734409743;
extern const uint32_t Inflater_Inflate_m2382363959_MetadataUsageId;
extern "C"  int32_t Inflater_Inflate_m2382363959 (Inflater_t3958302971 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inflater_Inflate_m2382363959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___count2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1554746267, _stringLiteral1421405844, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_5, _stringLiteral1673268925, _stringLiteral2228609792, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0036:
	{
		int32_t L_6 = ___offset1;
		int32_t L_7 = ___count2;
		ByteU5BU5D_t3397334013* L_8 = ___buffer0;
		NullCheck(L_8);
		if ((((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_7))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0049;
		}
	}
	{
		ArgumentException_t3259014390 * L_9 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_9, _stringLiteral734409743, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0049:
	{
		int32_t L_10 = ___count2;
		if (L_10)
		{
			goto IL_005d;
		}
	}
	{
		bool L_11 = Inflater_get_IsFinished_m3508682262(__this, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_005b;
		}
	}
	{
		Inflater_Decode_m891627623(__this, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return 0;
	}

IL_005d:
	{
		V_0 = 0;
	}

IL_005f:
	{
		int32_t L_12 = __this->get_mode_4();
		if ((((int32_t)L_12) == ((int32_t)((int32_t)11))))
		{
			goto IL_00ac;
		}
	}
	{
		OutputWindow_t824811027 * L_13 = __this->get_outputWindow_15();
		ByteU5BU5D_t3397334013* L_14 = ___buffer0;
		int32_t L_15 = ___offset1;
		int32_t L_16 = ___count2;
		NullCheck(L_13);
		int32_t L_17 = OutputWindow_CopyOutput_m200326477(L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00ac;
		}
	}
	{
		Adler32_t1759601101 * L_19 = __this->get_adler_19();
		ByteU5BU5D_t3397334013* L_20 = ___buffer0;
		int32_t L_21 = ___offset1;
		int32_t L_22 = V_1;
		NullCheck(L_19);
		Adler32_Update_m2876580056(L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = ___offset1;
		int32_t L_24 = V_1;
		___offset1 = ((int32_t)((int32_t)L_23+(int32_t)L_24));
		int32_t L_25 = V_0;
		int32_t L_26 = V_1;
		V_0 = ((int32_t)((int32_t)L_25+(int32_t)L_26));
		int64_t L_27 = __this->get_totalOut_11();
		int32_t L_28 = V_1;
		__this->set_totalOut_11(((int64_t)((int64_t)L_27+(int64_t)(((int64_t)((int64_t)L_28))))));
		int32_t L_29 = ___count2;
		int32_t L_30 = V_1;
		___count2 = ((int32_t)((int32_t)L_29-(int32_t)L_30));
		int32_t L_31 = ___count2;
		if (L_31)
		{
			goto IL_00ac;
		}
	}
	{
		int32_t L_32 = V_0;
		return L_32;
	}

IL_00ac:
	{
		bool L_33 = Inflater_Decode_m891627623(__this, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_005f;
		}
	}
	{
		OutputWindow_t824811027 * L_34 = __this->get_outputWindow_15();
		NullCheck(L_34);
		int32_t L_35 = OutputWindow_GetAvailable_m2569373299(L_34, /*hidden argument*/NULL);
		if ((((int32_t)L_35) <= ((int32_t)0)))
		{
			goto IL_00cc;
		}
	}
	{
		int32_t L_36 = __this->get_mode_4();
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_005f;
		}
	}

IL_00cc:
	{
		int32_t L_37 = V_0;
		return L_37;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern "C"  bool Inflater_get_IsNeedingInput_m3454491888 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	{
		StreamManipulator_t1922289728 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		bool L_1 = StreamManipulator_get_IsNeedingInput_m2895772626(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern "C"  bool Inflater_get_IsNeedingDictionary_m130412086 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mode_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = __this->get_neededBits_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0013:
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern "C"  bool Inflater_get_IsFinished_m3508682262 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mode_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0019;
		}
	}
	{
		OutputWindow_t824811027 * L_1 = __this->get_outputWindow_15();
		NullCheck(L_1);
		int32_t L_2 = OutputWindow_GetAvailable_m2569373299(L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0019:
	{
		return (bool)0;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern "C"  int64_t Inflater_get_TotalOut_m3944891659 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_totalOut_11();
		return L_0;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalIn()
extern "C"  int64_t Inflater_get_TotalIn_m2049506924 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_totalIn_12();
		int32_t L_1 = Inflater_get_RemainingInput_m2370068744(__this, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)L_0-(int64_t)(((int64_t)((int64_t)L_1)))));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern "C"  int32_t Inflater_get_RemainingInput_m2370068744 (Inflater_t3958302971 * __this, const MethodInfo* method)
{
	{
		StreamManipulator_t1922289728 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_get_AvailableBytes_m4021342904(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* Inflater_t3958302971_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D1_4_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D2_5_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D3_6_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D4_7_FieldInfo_var;
extern const uint32_t Inflater__cctor_m3421098168_MetadataUsageId;
extern "C"  void Inflater__cctor_m3421098168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inflater__cctor_m3421098168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t3030399641* L_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)29)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D1_4_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->set_CPLENS_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)29)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D2_5_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->set_CPLEXT_1(L_1);
		Int32U5BU5D_t3030399641* L_2 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D3_6_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->set_CPDIST_2(L_2);
		Int32U5BU5D_t3030399641* L_3 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004fdU2D4_7_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t3958302971_StaticFields*)Inflater_t3958302971_il2cpp_TypeInfo_var->static_fields)->set_CPDEXT_3(L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor()
extern "C"  void InflaterDynHeader__ctor_m1417066751 (InflaterDynHeader_t3298452259 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::Decode(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern const uint32_t InflaterDynHeader_Decode_m2190858166_MetadataUsageId;
extern "C"  bool InflaterDynHeader_Decode_m2190858166 (InflaterDynHeader_t3298452259 * __this, StreamManipulator_t1922289728 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader_Decode_m2190858166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint8_t V_6 = 0x0;
	int32_t V_7 = 0;

IL_0000:
	{
		int32_t L_0 = __this->get_mode_6();
		V_4 = L_0;
		int32_t L_1 = V_4;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_0061;
			}
			case 2:
			{
				goto IL_00b9;
			}
			case 3:
			{
				goto IL_013b;
			}
			case 4:
			{
				goto IL_01a8;
			}
			case 5:
			{
				goto IL_01ee;
			}
		}
	}
	{
		goto IL_0000;
	}

IL_0029:
	{
		StreamManipulator_t1922289728 * L_2 = ___input0;
		NullCheck(L_2);
		int32_t L_3 = StreamManipulator_PeekBits_m1052229955(L_2, 5, /*hidden argument*/NULL);
		__this->set_lnum_7(L_3);
		int32_t L_4 = __this->get_lnum_7();
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0041;
		}
	}
	{
		return (bool)0;
	}

IL_0041:
	{
		int32_t L_5 = __this->get_lnum_7();
		__this->set_lnum_7(((int32_t)((int32_t)L_5+(int32_t)((int32_t)257))));
		StreamManipulator_t1922289728 * L_6 = ___input0;
		NullCheck(L_6);
		StreamManipulator_DropBits_m4077687679(L_6, 5, /*hidden argument*/NULL);
		__this->set_mode_6(1);
	}

IL_0061:
	{
		StreamManipulator_t1922289728 * L_7 = ___input0;
		NullCheck(L_7);
		int32_t L_8 = StreamManipulator_PeekBits_m1052229955(L_7, 5, /*hidden argument*/NULL);
		__this->set_dnum_8(L_8);
		int32_t L_9 = __this->get_dnum_8();
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		return (bool)0;
	}

IL_0079:
	{
		int32_t L_10 = __this->get_dnum_8();
		__this->set_dnum_8(((int32_t)((int32_t)L_10+(int32_t)1)));
		StreamManipulator_t1922289728 * L_11 = ___input0;
		NullCheck(L_11);
		StreamManipulator_DropBits_m4077687679(L_11, 5, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_lnum_7();
		int32_t L_13 = __this->get_dnum_8();
		__this->set_num_10(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = __this->get_num_10();
		__this->set_litdistLens_4(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_14)));
		__this->set_mode_6(2);
	}

IL_00b9:
	{
		StreamManipulator_t1922289728 * L_15 = ___input0;
		NullCheck(L_15);
		int32_t L_16 = StreamManipulator_PeekBits_m1052229955(L_15, 4, /*hidden argument*/NULL);
		__this->set_blnum_9(L_16);
		int32_t L_17 = __this->get_blnum_9();
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_00d1;
		}
	}
	{
		return (bool)0;
	}

IL_00d1:
	{
		int32_t L_18 = __this->get_blnum_9();
		__this->set_blnum_9(((int32_t)((int32_t)L_18+(int32_t)4)));
		StreamManipulator_t1922289728 * L_19 = ___input0;
		NullCheck(L_19);
		StreamManipulator_DropBits_m4077687679(L_19, 4, /*hidden argument*/NULL);
		__this->set_blLens_3(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19))));
		__this->set_ptr_13(0);
		__this->set_mode_6(3);
		goto IL_013b;
	}

IL_0103:
	{
		StreamManipulator_t1922289728 * L_20 = ___input0;
		NullCheck(L_20);
		int32_t L_21 = StreamManipulator_PeekBits_m1052229955(L_20, 3, /*hidden argument*/NULL);
		V_0 = L_21;
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) >= ((int32_t)0)))
		{
			goto IL_0111;
		}
	}
	{
		return (bool)0;
	}

IL_0111:
	{
		StreamManipulator_t1922289728 * L_23 = ___input0;
		NullCheck(L_23);
		StreamManipulator_DropBits_m4077687679(L_23, 3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_24 = __this->get_blLens_3();
		IL2CPP_RUNTIME_CLASS_INIT(InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3030399641* L_25 = ((InflaterDynHeader_t3298452259_StaticFields*)InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var->static_fields)->get_BL_ORDER_2();
		int32_t L_26 = __this->get_ptr_13();
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		int32_t L_29 = V_0;
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (uint8_t)(((int32_t)((uint8_t)L_29))));
		int32_t L_30 = __this->get_ptr_13();
		__this->set_ptr_13(((int32_t)((int32_t)L_30+(int32_t)1)));
	}

IL_013b:
	{
		int32_t L_31 = __this->get_ptr_13();
		int32_t L_32 = __this->get_blnum_9();
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_0103;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_33 = __this->get_blLens_3();
		InflaterHuffmanTree_t3246684106 * L_34 = (InflaterHuffmanTree_t3246684106 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m3710971905(L_34, L_33, /*hidden argument*/NULL);
		__this->set_blTree_5(L_34);
		__this->set_blLens_3((ByteU5BU5D_t3397334013*)NULL);
		__this->set_ptr_13(0);
		__this->set_mode_6(4);
		goto IL_01a8;
	}

IL_0171:
	{
		ByteU5BU5D_t3397334013* L_35 = __this->get_litdistLens_4();
		int32_t L_36 = __this->get_ptr_13();
		int32_t L_37 = L_36;
		V_5 = L_37;
		__this->set_ptr_13(((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = V_5;
		int32_t L_39 = V_1;
		int32_t L_40 = (((int32_t)((uint8_t)L_39)));
		V_6 = L_40;
		__this->set_lastLen_12(L_40);
		uint8_t L_41 = V_6;
		NullCheck(L_35);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (uint8_t)L_41);
		int32_t L_42 = __this->get_ptr_13();
		int32_t L_43 = __this->get_num_10();
		if ((!(((uint32_t)L_42) == ((uint32_t)L_43))))
		{
			goto IL_01a8;
		}
	}
	{
		return (bool)1;
	}

IL_01a8:
	{
		InflaterHuffmanTree_t3246684106 * L_44 = __this->get_blTree_5();
		StreamManipulator_t1922289728 * L_45 = ___input0;
		NullCheck(L_44);
		int32_t L_46 = InflaterHuffmanTree_GetSymbol_m189842273(L_44, L_45, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		V_1 = L_47;
		if (!((int32_t)((int32_t)L_47&(int32_t)((int32_t)-16))))
		{
			goto IL_0171;
		}
	}
	{
		int32_t L_48 = V_1;
		if ((((int32_t)L_48) >= ((int32_t)0)))
		{
			goto IL_01c1;
		}
	}
	{
		return (bool)0;
	}

IL_01c1:
	{
		int32_t L_49 = V_1;
		if ((((int32_t)L_49) < ((int32_t)((int32_t)17))))
		{
			goto IL_01cf;
		}
	}
	{
		__this->set_lastLen_12(0);
		goto IL_01dd;
	}

IL_01cf:
	{
		int32_t L_50 = __this->get_ptr_13();
		if (L_50)
		{
			goto IL_01dd;
		}
	}
	{
		SharpZipBaseException_t4116900137 * L_51 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3776338968(L_51, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_51);
	}

IL_01dd:
	{
		int32_t L_52 = V_1;
		__this->set_repSymbol_11(((int32_t)((int32_t)L_52-(int32_t)((int32_t)16))));
		__this->set_mode_6(5);
	}

IL_01ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3030399641* L_53 = ((InflaterDynHeader_t3298452259_StaticFields*)InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var->static_fields)->get_repBits_1();
		int32_t L_54 = __this->get_repSymbol_11();
		NullCheck(L_53);
		int32_t L_55 = L_54;
		int32_t L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		V_2 = L_56;
		StreamManipulator_t1922289728 * L_57 = ___input0;
		int32_t L_58 = V_2;
		NullCheck(L_57);
		int32_t L_59 = StreamManipulator_PeekBits_m1052229955(L_57, L_58, /*hidden argument*/NULL);
		V_3 = L_59;
		int32_t L_60 = V_3;
		if ((((int32_t)L_60) >= ((int32_t)0)))
		{
			goto IL_0209;
		}
	}
	{
		return (bool)0;
	}

IL_0209:
	{
		StreamManipulator_t1922289728 * L_61 = ___input0;
		int32_t L_62 = V_2;
		NullCheck(L_61);
		StreamManipulator_DropBits_m4077687679(L_61, L_62, /*hidden argument*/NULL);
		int32_t L_63 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3030399641* L_64 = ((InflaterDynHeader_t3298452259_StaticFields*)InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var->static_fields)->get_repMin_0();
		int32_t L_65 = __this->get_repSymbol_11();
		NullCheck(L_64);
		int32_t L_66 = L_65;
		int32_t L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		V_3 = ((int32_t)((int32_t)L_63+(int32_t)L_67));
		int32_t L_68 = __this->get_ptr_13();
		int32_t L_69 = V_3;
		int32_t L_70 = __this->get_num_10();
		if ((((int32_t)((int32_t)((int32_t)L_68+(int32_t)L_69))) <= ((int32_t)L_70)))
		{
			goto IL_0255;
		}
	}
	{
		SharpZipBaseException_t4116900137 * L_71 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3776338968(L_71, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_71);
	}

IL_0235:
	{
		ByteU5BU5D_t3397334013* L_72 = __this->get_litdistLens_4();
		int32_t L_73 = __this->get_ptr_13();
		int32_t L_74 = L_73;
		V_7 = L_74;
		__this->set_ptr_13(((int32_t)((int32_t)L_74+(int32_t)1)));
		int32_t L_75 = V_7;
		uint8_t L_76 = __this->get_lastLen_12();
		NullCheck(L_72);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(L_75), (uint8_t)L_76);
	}

IL_0255:
	{
		int32_t L_77 = V_3;
		int32_t L_78 = L_77;
		V_3 = ((int32_t)((int32_t)L_78-(int32_t)1));
		if ((((int32_t)L_78) > ((int32_t)0)))
		{
			goto IL_0235;
		}
	}
	{
		int32_t L_79 = __this->get_ptr_13();
		int32_t L_80 = __this->get_num_10();
		if ((!(((uint32_t)L_79) == ((uint32_t)L_80))))
		{
			goto IL_026d;
		}
	}
	{
		return (bool)1;
	}

IL_026d:
	{
		__this->set_mode_6(4);
		goto IL_0000;
	}
}
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildLitLenTree()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var;
extern const uint32_t InflaterDynHeader_BuildLitLenTree_m63482416_MetadataUsageId;
extern "C"  InflaterHuffmanTree_t3246684106 * InflaterDynHeader_BuildLitLenTree_m63482416 (InflaterDynHeader_t3298452259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader_BuildLitLenTree_m63482416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		int32_t L_0 = __this->get_lnum_7();
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_0));
		ByteU5BU5D_t3397334013* L_1 = __this->get_litdistLens_4();
		ByteU5BU5D_t3397334013* L_2 = V_0;
		int32_t L_3 = __this->get_lnum_7();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, 0, (Il2CppArray *)(Il2CppArray *)L_2, 0, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_4 = V_0;
		InflaterHuffmanTree_t3246684106 * L_5 = (InflaterHuffmanTree_t3246684106 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m3710971905(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildDistTree()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var;
extern const uint32_t InflaterDynHeader_BuildDistTree_m554109756_MetadataUsageId;
extern "C"  InflaterHuffmanTree_t3246684106 * InflaterDynHeader_BuildDistTree_m554109756 (InflaterDynHeader_t3298452259 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader_BuildDistTree_m554109756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		int32_t L_0 = __this->get_dnum_8();
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_0));
		ByteU5BU5D_t3397334013* L_1 = __this->get_litdistLens_4();
		int32_t L_2 = __this->get_lnum_7();
		ByteU5BU5D_t3397334013* L_3 = V_0;
		int32_t L_4 = __this->get_dnum_8();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, L_2, (Il2CppArray *)(Il2CppArray *)L_3, 0, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = V_0;
		InflaterHuffmanTree_t3246684106 * L_6 = (InflaterHuffmanTree_t3246684106 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m3710971905(L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004feU2D1_8_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004feU2D2_9_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004feU2D3_10_FieldInfo_var;
extern const uint32_t InflaterDynHeader__cctor_m1858886190_MetadataUsageId;
extern "C"  void InflaterDynHeader__cctor_m1858886190 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader__cctor_m1858886190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t3030399641* L_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004feU2D1_8_FieldInfo_var), /*hidden argument*/NULL);
		((InflaterDynHeader_t3298452259_StaticFields*)InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var->static_fields)->set_repMin_0(L_0);
		Int32U5BU5D_t3030399641* L_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004feU2D2_9_FieldInfo_var), /*hidden argument*/NULL);
		((InflaterDynHeader_t3298452259_StaticFields*)InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var->static_fields)->set_repBits_1(L_1);
		Int32U5BU5D_t3030399641* L_2 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19)));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t777596142____U24U24method0x60004feU2D3_10_FieldInfo_var), /*hidden argument*/NULL);
		((InflaterDynHeader_t3298452259_StaticFields*)InflaterDynHeader_t3298452259_il2cpp_TypeInfo_var->static_fields)->set_BL_ORDER_2(L_2);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral310969332;
extern const uint32_t InflaterHuffmanTree__cctor_m3202130013_MetadataUsageId;
extern "C"  void InflaterHuffmanTree__cctor_m3202130013 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterHuffmanTree__cctor_m3202130013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)288)));
			V_1 = 0;
			goto IL_0017;
		}

IL_000f:
		{
			ByteU5BU5D_t3397334013* L_0 = V_0;
			int32_t L_1 = V_1;
			int32_t L_2 = L_1;
			V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
			NullCheck(L_0);
			(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (uint8_t)8);
		}

IL_0017:
		{
			int32_t L_3 = V_1;
			if ((((int32_t)L_3) < ((int32_t)((int32_t)144))))
			{
				goto IL_000f;
			}
		}

IL_001f:
		{
			goto IL_002a;
		}

IL_0021:
		{
			ByteU5BU5D_t3397334013* L_4 = V_0;
			int32_t L_5 = V_1;
			int32_t L_6 = L_5;
			V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
			NullCheck(L_4);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)((int32_t)9));
		}

IL_002a:
		{
			int32_t L_7 = V_1;
			if ((((int32_t)L_7) < ((int32_t)((int32_t)256))))
			{
				goto IL_0021;
			}
		}

IL_0032:
		{
			goto IL_003c;
		}

IL_0034:
		{
			ByteU5BU5D_t3397334013* L_8 = V_0;
			int32_t L_9 = V_1;
			int32_t L_10 = L_9;
			V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
			NullCheck(L_8);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (uint8_t)7);
		}

IL_003c:
		{
			int32_t L_11 = V_1;
			if ((((int32_t)L_11) < ((int32_t)((int32_t)280))))
			{
				goto IL_0034;
			}
		}

IL_0044:
		{
			goto IL_004e;
		}

IL_0046:
		{
			ByteU5BU5D_t3397334013* L_12 = V_0;
			int32_t L_13 = V_1;
			int32_t L_14 = L_13;
			V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
			NullCheck(L_12);
			(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint8_t)8);
		}

IL_004e:
		{
			int32_t L_15 = V_1;
			if ((((int32_t)L_15) < ((int32_t)((int32_t)288))))
			{
				goto IL_0046;
			}
		}

IL_0056:
		{
			ByteU5BU5D_t3397334013* L_16 = V_0;
			InflaterHuffmanTree_t3246684106 * L_17 = (InflaterHuffmanTree_t3246684106 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var);
			InflaterHuffmanTree__ctor_m3710971905(L_17, L_16, /*hidden argument*/NULL);
			((InflaterHuffmanTree_t3246684106_StaticFields*)InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var->static_fields)->set_defLitLenTree_1(L_17);
			V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32)));
			V_1 = 0;
			goto IL_0075;
		}

IL_006d:
		{
			ByteU5BU5D_t3397334013* L_18 = V_0;
			int32_t L_19 = V_1;
			int32_t L_20 = L_19;
			V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
			NullCheck(L_18);
			(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)5);
		}

IL_0075:
		{
			int32_t L_21 = V_1;
			if ((((int32_t)L_21) < ((int32_t)((int32_t)32))))
			{
				goto IL_006d;
			}
		}

IL_007a:
		{
			ByteU5BU5D_t3397334013* L_22 = V_0;
			InflaterHuffmanTree_t3246684106 * L_23 = (InflaterHuffmanTree_t3246684106 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var);
			InflaterHuffmanTree__ctor_m3710971905(L_23, L_22, /*hidden argument*/NULL);
			((InflaterHuffmanTree_t3246684106_StaticFields*)InflaterHuffmanTree_t3246684106_il2cpp_TypeInfo_var->static_fields)->set_defDistTree_2(L_23);
			goto IL_0093;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0087;
		throw e;
	}

CATCH_0087:
	{ // begin catch(System.Exception)
		SharpZipBaseException_t4116900137 * L_24 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_24, _stringLiteral310969332, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	} // end catch (depth: 1)

IL_0093:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Byte[])
extern "C"  void InflaterHuffmanTree__ctor_m3710971905 (InflaterHuffmanTree_t3246684106 * __this, ByteU5BU5D_t3397334013* ___codeLengths0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___codeLengths0;
		InflaterHuffmanTree_BuildTree_m163291661(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Byte[])
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t3104283263_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterHuffman_t343008798_il2cpp_TypeInfo_var;
extern const uint32_t InflaterHuffmanTree_BuildTree_m163291661_MetadataUsageId;
extern "C"  void InflaterHuffmanTree_BuildTree_m163291661 (InflaterHuffmanTree_t3246684106 * __this, ByteU5BU5D_t3397334013* ___codeLengths0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterHuffmanTree_BuildTree_m163291661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	Int32U5BU5D_t3030399641* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	{
		V_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		V_1 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		V_2 = 0;
		goto IL_0034;
	}

IL_0014:
	{
		ByteU5BU5D_t3397334013* L_0 = ___codeLengths0;
		int32_t L_1 = V_2;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_3 = L_3;
		int32_t L_4 = V_3;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_5 = V_0;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		int32_t* L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)));
		(*(int32_t*)L_7) = ((int32_t)((int32_t)(*(int32_t*)L_7)+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_9 = V_2;
		ByteU5BU5D_t3397334013* L_10 = ___codeLengths0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		V_4 = 0;
		V_5 = ((int32_t)512);
		V_6 = 1;
		goto IL_0096;
	}

IL_0049:
	{
		Int32U5BU5D_t3030399641* L_11 = V_1;
		int32_t L_12 = V_6;
		int32_t L_13 = V_4;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (int32_t)L_13);
		int32_t L_14 = V_4;
		Int32U5BU5D_t3030399641* L_15 = V_0;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = V_6;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_18<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_19))&(int32_t)((int32_t)31)))))));
		int32_t L_20 = V_6;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)10))))
		{
			goto IL_0090;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_21 = V_1;
		int32_t L_22 = V_6;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		V_7 = ((int32_t)((int32_t)L_24&(int32_t)((int32_t)130944)));
		int32_t L_25 = V_4;
		V_8 = ((int32_t)((int32_t)L_25&(int32_t)((int32_t)130944)));
		int32_t L_26 = V_5;
		int32_t L_27 = V_8;
		int32_t L_28 = V_7;
		int32_t L_29 = V_6;
		V_5 = ((int32_t)((int32_t)L_26+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27-(int32_t)L_28))>>(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_29))&(int32_t)((int32_t)31)))))));
	}

IL_0090:
	{
		int32_t L_30 = V_6;
		V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_31 = V_6;
		if ((((int32_t)L_31) <= ((int32_t)((int32_t)15))))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_32 = V_5;
		__this->set_tree_0(((Int16U5BU5D_t3104283263*)SZArrayNew(Int16U5BU5D_t3104283263_il2cpp_TypeInfo_var, (uint32_t)L_32)));
		V_9 = ((int32_t)512);
		V_10 = ((int32_t)15);
		goto IL_011e;
	}

IL_00b6:
	{
		int32_t L_33 = V_4;
		V_11 = ((int32_t)((int32_t)L_33&(int32_t)((int32_t)130944)));
		int32_t L_34 = V_4;
		Int32U5BU5D_t3030399641* L_35 = V_0;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		int32_t L_39 = V_10;
		V_4 = ((int32_t)((int32_t)L_34-(int32_t)((int32_t)((int32_t)L_38<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_39))&(int32_t)((int32_t)31)))))));
		int32_t L_40 = V_4;
		V_12 = ((int32_t)((int32_t)L_40&(int32_t)((int32_t)130944)));
		int32_t L_41 = V_12;
		V_13 = L_41;
		goto IL_0112;
	}

IL_00e2:
	{
		Int16U5BU5D_t3104283263* L_42 = __this->get_tree_0();
		int32_t L_43 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t343008798_il2cpp_TypeInfo_var);
		int16_t L_44 = DeflaterHuffman_BitReverse_m4273222008(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		int32_t L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_42);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(L_44), (int16_t)(((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)((-L_45))<<(int32_t)4))|(int32_t)L_46))))));
		int32_t L_47 = V_9;
		int32_t L_48 = V_10;
		V_9 = ((int32_t)((int32_t)L_47+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_48-(int32_t)((int32_t)9)))&(int32_t)((int32_t)31)))))));
		int32_t L_49 = V_13;
		V_13 = ((int32_t)((int32_t)L_49+(int32_t)((int32_t)128)));
	}

IL_0112:
	{
		int32_t L_50 = V_13;
		int32_t L_51 = V_11;
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_52 = V_10;
		V_10 = ((int32_t)((int32_t)L_52-(int32_t)1));
	}

IL_011e:
	{
		int32_t L_53 = V_10;
		if ((((int32_t)L_53) >= ((int32_t)((int32_t)10))))
		{
			goto IL_00b6;
		}
	}
	{
		V_14 = 0;
		goto IL_01da;
	}

IL_012c:
	{
		ByteU5BU5D_t3397334013* L_54 = ___codeLengths0;
		int32_t L_55 = V_14;
		NullCheck(L_54);
		int32_t L_56 = L_55;
		uint8_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		V_15 = L_57;
		int32_t L_58 = V_15;
		if (!L_58)
		{
			goto IL_01d4;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_59 = V_1;
		int32_t L_60 = V_15;
		NullCheck(L_59);
		int32_t L_61 = L_60;
		int32_t L_62 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		V_4 = L_62;
		int32_t L_63 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t343008798_il2cpp_TypeInfo_var);
		int16_t L_64 = DeflaterHuffman_BitReverse_m4273222008(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		V_16 = L_64;
		int32_t L_65 = V_15;
		if ((((int32_t)L_65) > ((int32_t)((int32_t)9))))
		{
			goto IL_0176;
		}
	}

IL_014e:
	{
		Int16U5BU5D_t3104283263* L_66 = __this->get_tree_0();
		int32_t L_67 = V_16;
		int32_t L_68 = V_14;
		int32_t L_69 = V_15;
		NullCheck(L_66);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(L_67), (int16_t)(((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_68<<(int32_t)4))|(int32_t)L_69))))));
		int32_t L_70 = V_16;
		int32_t L_71 = V_15;
		V_16 = ((int32_t)((int32_t)L_70+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_71&(int32_t)((int32_t)31)))))));
		int32_t L_72 = V_16;
		if ((((int32_t)L_72) < ((int32_t)((int32_t)512))))
		{
			goto IL_014e;
		}
	}
	{
		goto IL_01c3;
	}

IL_0176:
	{
		Int16U5BU5D_t3104283263* L_73 = __this->get_tree_0();
		int32_t L_74 = V_16;
		NullCheck(L_73);
		int32_t L_75 = ((int32_t)((int32_t)L_74&(int32_t)((int32_t)511)));
		int16_t L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		V_17 = L_76;
		int32_t L_77 = V_17;
		V_18 = ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_77&(int32_t)((int32_t)15)))&(int32_t)((int32_t)31)))));
		int32_t L_78 = V_17;
		V_17 = ((-((int32_t)((int32_t)L_78>>(int32_t)4))));
	}

IL_019a:
	{
		Int16U5BU5D_t3104283263* L_79 = __this->get_tree_0();
		int32_t L_80 = V_17;
		int32_t L_81 = V_16;
		int32_t L_82 = V_14;
		int32_t L_83 = V_15;
		NullCheck(L_79);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_80|(int32_t)((int32_t)((int32_t)L_81>>(int32_t)((int32_t)9)))))), (int16_t)(((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_82<<(int32_t)4))|(int32_t)L_83))))));
		int32_t L_84 = V_16;
		int32_t L_85 = V_15;
		V_16 = ((int32_t)((int32_t)L_84+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_85&(int32_t)((int32_t)31)))))));
		int32_t L_86 = V_16;
		int32_t L_87 = V_18;
		if ((((int32_t)L_86) < ((int32_t)L_87)))
		{
			goto IL_019a;
		}
	}

IL_01c3:
	{
		Int32U5BU5D_t3030399641* L_88 = V_1;
		int32_t L_89 = V_15;
		int32_t L_90 = V_4;
		int32_t L_91 = V_15;
		NullCheck(L_88);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(L_89), (int32_t)((int32_t)((int32_t)L_90+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_91))&(int32_t)((int32_t)31))))))));
	}

IL_01d4:
	{
		int32_t L_92 = V_14;
		V_14 = ((int32_t)((int32_t)L_92+(int32_t)1));
	}

IL_01da:
	{
		int32_t L_93 = V_14;
		ByteU5BU5D_t3397334013* L_94 = ___codeLengths0;
		NullCheck(L_94);
		if ((((int32_t)L_93) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_94)->max_length)))))))
		{
			goto IL_012c;
		}
	}
	{
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern "C"  int32_t InflaterHuffmanTree_GetSymbol_m189842273 (InflaterHuffmanTree_t3246684106 * __this, StreamManipulator_t1922289728 * ___input0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		StreamManipulator_t1922289728 * L_0 = ___input0;
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m1052229955(L_0, ((int32_t)9), /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		V_0 = L_2;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		Int16U5BU5D_t3104283263* L_3 = __this->get_tree_0();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int16_t L_7 = L_6;
		V_1 = L_7;
		if ((((int32_t)L_7) < ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		StreamManipulator_t1922289728 * L_8 = ___input0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		StreamManipulator_DropBits_m4077687679(L_8, ((int32_t)((int32_t)L_9&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		return ((int32_t)((int32_t)L_10>>(int32_t)4));
	}

IL_002b:
	{
		int32_t L_11 = V_1;
		V_2 = ((-((int32_t)((int32_t)L_11>>(int32_t)4))));
		int32_t L_12 = V_1;
		V_3 = ((int32_t)((int32_t)L_12&(int32_t)((int32_t)15)));
		StreamManipulator_t1922289728 * L_13 = ___input0;
		int32_t L_14 = V_3;
		NullCheck(L_13);
		int32_t L_15 = StreamManipulator_PeekBits_m1052229955(L_13, L_14, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		V_0 = L_16;
		if ((((int32_t)L_16) < ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		Int16U5BU5D_t3104283263* L_17 = __this->get_tree_0();
		int32_t L_18 = V_2;
		int32_t L_19 = V_0;
		NullCheck(L_17);
		int32_t L_20 = ((int32_t)((int32_t)L_18|(int32_t)((int32_t)((int32_t)L_19>>(int32_t)((int32_t)9)))));
		int16_t L_21 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_1 = L_21;
		StreamManipulator_t1922289728 * L_22 = ___input0;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		StreamManipulator_DropBits_m4077687679(L_22, ((int32_t)((int32_t)L_23&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		return ((int32_t)((int32_t)L_24>>(int32_t)4));
	}

IL_005d:
	{
		StreamManipulator_t1922289728 * L_25 = ___input0;
		NullCheck(L_25);
		int32_t L_26 = StreamManipulator_get_AvailableBits_m4055873297(L_25, /*hidden argument*/NULL);
		V_4 = L_26;
		StreamManipulator_t1922289728 * L_27 = ___input0;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		int32_t L_29 = StreamManipulator_PeekBits_m1052229955(L_27, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		Int16U5BU5D_t3104283263* L_30 = __this->get_tree_0();
		int32_t L_31 = V_2;
		int32_t L_32 = V_0;
		NullCheck(L_30);
		int32_t L_33 = ((int32_t)((int32_t)L_31|(int32_t)((int32_t)((int32_t)L_32>>(int32_t)((int32_t)9)))));
		int16_t L_34 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		V_1 = L_34;
		int32_t L_35 = V_1;
		int32_t L_36 = V_4;
		if ((((int32_t)((int32_t)((int32_t)L_35&(int32_t)((int32_t)15)))) > ((int32_t)L_36)))
		{
			goto IL_0092;
		}
	}
	{
		StreamManipulator_t1922289728 * L_37 = ___input0;
		int32_t L_38 = V_1;
		NullCheck(L_37);
		StreamManipulator_DropBits_m4077687679(L_37, ((int32_t)((int32_t)L_38&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_39 = V_1;
		return ((int32_t)((int32_t)L_39>>(int32_t)4));
	}

IL_0092:
	{
		return (-1);
	}

IL_0094:
	{
		StreamManipulator_t1922289728 * L_40 = ___input0;
		NullCheck(L_40);
		int32_t L_41 = StreamManipulator_get_AvailableBits_m4055873297(L_40, /*hidden argument*/NULL);
		V_5 = L_41;
		StreamManipulator_t1922289728 * L_42 = ___input0;
		int32_t L_43 = V_5;
		NullCheck(L_42);
		int32_t L_44 = StreamManipulator_PeekBits_m1052229955(L_42, L_43, /*hidden argument*/NULL);
		V_0 = L_44;
		Int16U5BU5D_t3104283263* L_45 = __this->get_tree_0();
		int32_t L_46 = V_0;
		NullCheck(L_45);
		int32_t L_47 = L_46;
		int16_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		V_1 = L_48;
		int32_t L_49 = V_1;
		if ((((int32_t)L_49) < ((int32_t)0)))
		{
			goto IL_00c8;
		}
	}
	{
		int32_t L_50 = V_1;
		int32_t L_51 = V_5;
		if ((((int32_t)((int32_t)((int32_t)L_50&(int32_t)((int32_t)15)))) > ((int32_t)L_51)))
		{
			goto IL_00c8;
		}
	}
	{
		StreamManipulator_t1922289728 * L_52 = ___input0;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		StreamManipulator_DropBits_m4077687679(L_52, ((int32_t)((int32_t)L_53&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_54 = V_1;
		return ((int32_t)((int32_t)L_54>>(int32_t)4));
	}

IL_00c8:
	{
		return (-1);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t InflaterInputBuffer__ctor_m1983366716_MetadataUsageId;
extern "C"  void InflaterInputBuffer__ctor_m1983366716 (InflaterInputBuffer_t3211561891 * __this, Stream_t3255436806 * ___stream0, int32_t ___bufferSize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer__ctor_m1983366716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___stream0;
		__this->set_inputStream_7(L_0);
		int32_t L_1 = ___bufferSize1;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)1024))))
		{
			goto IL_001c;
		}
	}
	{
		___bufferSize1 = ((int32_t)1024);
	}

IL_001c:
	{
		int32_t L_2 = ___bufferSize1;
		__this->set_rawData_1(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		ByteU5BU5D_t3397334013* L_3 = __this->get_rawData_1();
		__this->set_clearText_3(L_3);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawLength()
extern "C"  int32_t InflaterInputBuffer_get_RawLength_m1091203859 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_rawLength_0();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern "C"  int32_t InflaterInputBuffer_get_Available_m2874517854 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_available_5();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern "C"  void InflaterInputBuffer_set_Available_m4062003677 (InflaterInputBuffer_t3211561891 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_available_5(L_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern "C"  void InflaterInputBuffer_SetInflaterInput_m1611710434 (InflaterInputBuffer_t3211561891 * __this, Inflater_t3958302971 * ___inflater0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_available_5();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		Inflater_t3958302971 * L_1 = ___inflater0;
		ByteU5BU5D_t3397334013* L_2 = __this->get_clearText_3();
		int32_t L_3 = __this->get_clearTextLength_2();
		int32_t L_4 = __this->get_available_5();
		int32_t L_5 = __this->get_available_5();
		NullCheck(L_1);
		Inflater_SetInput_m1225927750(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)L_4)), L_5, /*hidden argument*/NULL);
		__this->set_available_5(0);
	}

IL_002f:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern Il2CppClass* ICryptoTransform_t281704372_il2cpp_TypeInfo_var;
extern const uint32_t InflaterInputBuffer_Fill_m1947457127_MetadataUsageId;
extern "C"  void InflaterInputBuffer_Fill_m1947457127 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_Fill_m1947457127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		__this->set_rawLength_0(0);
		ByteU5BU5D_t3397334013* L_0 = __this->get_rawData_1();
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		goto IL_0041;
	}

IL_0012:
	{
		Stream_t3255436806 * L_1 = __this->get_inputStream_7();
		ByteU5BU5D_t3397334013* L_2 = __this->get_rawData_1();
		int32_t L_3 = __this->get_rawLength_0();
		int32_t L_4 = V_0;
		NullCheck(L_1);
		int32_t L_5 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, L_3, L_4);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_7 = __this->get_rawLength_0();
		int32_t L_8 = V_1;
		__this->set_rawLength_0(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		V_0 = ((int32_t)((int32_t)L_9-(int32_t)L_10));
	}

IL_0041:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) > ((int32_t)0)))
		{
			goto IL_0012;
		}
	}

IL_0045:
	{
		Il2CppObject * L_12 = __this->get_cryptoTransform_6();
		if (!L_12)
		{
			goto IL_0074;
		}
	}
	{
		Il2CppObject * L_13 = __this->get_cryptoTransform_6();
		ByteU5BU5D_t3397334013* L_14 = __this->get_rawData_1();
		int32_t L_15 = __this->get_rawLength_0();
		ByteU5BU5D_t3397334013* L_16 = __this->get_clearText_3();
		NullCheck(L_13);
		int32_t L_17 = InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t, ByteU5BU5D_t3397334013*, int32_t >::Invoke(4 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t281704372_il2cpp_TypeInfo_var, L_13, L_14, 0, L_15, L_16, 0);
		__this->set_clearTextLength_2(L_17);
		goto IL_0080;
	}

IL_0074:
	{
		int32_t L_18 = __this->get_rawLength_0();
		__this->set_clearTextLength_2(L_18);
	}

IL_0080:
	{
		int32_t L_19 = __this->get_clearTextLength_2();
		__this->set_available_5(L_19);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[])
extern "C"  int32_t InflaterInputBuffer_ReadRawBuffer_m688284419 (InflaterInputBuffer_t3211561891 * __this, ByteU5BU5D_t3397334013* ___buffer0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		NullCheck(L_1);
		int32_t L_2 = InflaterInputBuffer_ReadRawBuffer_m721002543(__this, L_0, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadRawBuffer(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3438791774;
extern const uint32_t InflaterInputBuffer_ReadRawBuffer_m721002543_MetadataUsageId;
extern "C"  int32_t InflaterInputBuffer_ReadRawBuffer_m721002543 (InflaterInputBuffer_t3211561891 * __this, ByteU5BU5D_t3397334013* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_ReadRawBuffer_m721002543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, _stringLiteral3438791774, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000f:
	{
		int32_t L_2 = ___offset1;
		V_0 = L_2;
		int32_t L_3 = ___length2;
		V_1 = L_3;
		goto IL_006d;
	}

IL_0015:
	{
		int32_t L_4 = __this->get_available_5();
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		InflaterInputBuffer_Fill_m1947457127(__this, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_available_5();
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		return 0;
	}

IL_002f:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = __this->get_available_5();
		int32_t L_8 = Math_Min_m4290821911(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		ByteU5BU5D_t3397334013* L_9 = __this->get_rawData_1();
		int32_t L_10 = __this->get_rawLength_0();
		int32_t L_11 = __this->get_available_5();
		ByteU5BU5D_t3397334013* L_12 = ___outBuffer0;
		int32_t L_13 = V_0;
		int32_t L_14 = V_2;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, ((int32_t)((int32_t)L_10-(int32_t)L_11)), (Il2CppArray *)(Il2CppArray *)L_12, L_13, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		int32_t L_16 = V_2;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)L_16));
		int32_t L_17 = V_1;
		int32_t L_18 = V_2;
		V_1 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		int32_t L_19 = __this->get_available_5();
		int32_t L_20 = V_2;
		__this->set_available_5(((int32_t)((int32_t)L_19-(int32_t)L_20)));
	}

IL_006d:
	{
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_22 = ___length2;
		return L_22;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3438791774;
extern const uint32_t InflaterInputBuffer_ReadClearTextBuffer_m370674671_MetadataUsageId;
extern "C"  int32_t InflaterInputBuffer_ReadClearTextBuffer_m370674671 (InflaterInputBuffer_t3211561891 * __this, ByteU5BU5D_t3397334013* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_ReadClearTextBuffer_m370674671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, _stringLiteral3438791774, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000f:
	{
		int32_t L_2 = ___offset1;
		V_0 = L_2;
		int32_t L_3 = ___length2;
		V_1 = L_3;
		goto IL_006d;
	}

IL_0015:
	{
		int32_t L_4 = __this->get_available_5();
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		InflaterInputBuffer_Fill_m1947457127(__this, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_available_5();
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		return 0;
	}

IL_002f:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = __this->get_available_5();
		int32_t L_8 = Math_Min_m4290821911(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		ByteU5BU5D_t3397334013* L_9 = __this->get_clearText_3();
		int32_t L_10 = __this->get_clearTextLength_2();
		int32_t L_11 = __this->get_available_5();
		ByteU5BU5D_t3397334013* L_12 = ___outBuffer0;
		int32_t L_13 = V_0;
		int32_t L_14 = V_2;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, ((int32_t)((int32_t)L_10-(int32_t)L_11)), (Il2CppArray *)(Il2CppArray *)L_12, L_13, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		int32_t L_16 = V_2;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)L_16));
		int32_t L_17 = V_1;
		int32_t L_18 = V_2;
		V_1 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		int32_t L_19 = __this->get_available_5();
		int32_t L_20 = V_2;
		__this->set_available_5(((int32_t)((int32_t)L_19-(int32_t)L_20)));
	}

IL_006d:
	{
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_22 = ___length2;
		return L_22;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral959493796;
extern const uint32_t InflaterInputBuffer_ReadLeByte_m1338146083_MetadataUsageId;
extern "C"  int32_t InflaterInputBuffer_ReadLeByte_m1338146083 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_ReadLeByte_m1338146083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		int32_t L_0 = __this->get_available_5();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		InflaterInputBuffer_Fill_m1947457127(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_available_5();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ZipException_t65220526 * L_2 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_2, _stringLiteral959493796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0023:
	{
		ByteU5BU5D_t3397334013* L_3 = __this->get_rawData_1();
		int32_t L_4 = __this->get_rawLength_0();
		int32_t L_5 = __this->get_available_5();
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)((int32_t)L_4-(int32_t)L_5));
		uint8_t L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		int32_t L_8 = __this->get_available_5();
		__this->set_available_5(((int32_t)((int32_t)L_8-(int32_t)1)));
		uint8_t L_9 = V_0;
		return L_9;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeShort()
extern "C"  int32_t InflaterInputBuffer_ReadLeShort_m3204983647 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InflaterInputBuffer_ReadLeByte_m1338146083(__this, /*hidden argument*/NULL);
		int32_t L_1 = InflaterInputBuffer_ReadLeByte_m1338146083(__this, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)8))));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeInt()
extern "C"  int32_t InflaterInputBuffer_ReadLeInt_m3478398094 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InflaterInputBuffer_ReadLeShort_m3204983647(__this, /*hidden argument*/NULL);
		int32_t L_1 = InflaterInputBuffer_ReadLeShort_m3204983647(__this, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)16)))));
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeLong()
extern "C"  int64_t InflaterInputBuffer_ReadLeLong_m1849502234 (InflaterInputBuffer_t3211561891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InflaterInputBuffer_ReadLeInt_m3478398094(__this, /*hidden argument*/NULL);
		int32_t L_1 = InflaterInputBuffer_ReadLeInt_m3478398094(__this, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_0))))))|(int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_CryptoTransform(System.Security.Cryptography.ICryptoTransform)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ICryptoTransform_t281704372_il2cpp_TypeInfo_var;
extern const uint32_t InflaterInputBuffer_set_CryptoTransform_m4126126743_MetadataUsageId;
extern "C"  void InflaterInputBuffer_set_CryptoTransform_m4126126743 (InflaterInputBuffer_t3211561891 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_set_CryptoTransform_m4126126743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_cryptoTransform_6(L_0);
		Il2CppObject * L_1 = __this->get_cryptoTransform_6();
		if (!L_1)
		{
			goto IL_0095;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_2 = __this->get_rawData_1();
		ByteU5BU5D_t3397334013* L_3 = __this->get_clearText_3();
		if ((!(((Il2CppObject*)(ByteU5BU5D_t3397334013*)L_2) == ((Il2CppObject*)(ByteU5BU5D_t3397334013*)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = __this->get_internalClearText_4();
		if (L_4)
		{
			goto IL_003b;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_5 = __this->get_rawData_1();
		NullCheck(L_5);
		__this->set_internalClearText_4(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))))));
	}

IL_003b:
	{
		ByteU5BU5D_t3397334013* L_6 = __this->get_internalClearText_4();
		__this->set_clearText_3(L_6);
	}

IL_0047:
	{
		int32_t L_7 = __this->get_rawLength_0();
		__this->set_clearTextLength_2(L_7);
		int32_t L_8 = __this->get_available_5();
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_00ad;
		}
	}
	{
		Il2CppObject * L_9 = __this->get_cryptoTransform_6();
		ByteU5BU5D_t3397334013* L_10 = __this->get_rawData_1();
		int32_t L_11 = __this->get_rawLength_0();
		int32_t L_12 = __this->get_available_5();
		int32_t L_13 = __this->get_available_5();
		ByteU5BU5D_t3397334013* L_14 = __this->get_clearText_3();
		int32_t L_15 = __this->get_rawLength_0();
		int32_t L_16 = __this->get_available_5();
		NullCheck(L_9);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t, ByteU5BU5D_t3397334013*, int32_t >::Invoke(4 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t281704372_il2cpp_TypeInfo_var, L_9, L_10, ((int32_t)((int32_t)L_11-(int32_t)L_12)), L_13, L_14, ((int32_t)((int32_t)L_15-(int32_t)L_16)));
		return;
	}

IL_0095:
	{
		ByteU5BU5D_t3397334013* L_17 = __this->get_rawData_1();
		__this->set_clearText_3(L_17);
		int32_t L_18 = __this->get_rawLength_0();
		__this->set_clearTextLength_2(L_18);
	}

IL_00ad:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern "C"  void InflaterInputStream__ctor_m151851384 (InflaterInputStream_t663799889 * __this, Stream_t3255436806 * ___baseInputStream0, Inflater_t3958302971 * ___inf1, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = ___baseInputStream0;
		Inflater_t3958302971 * L_1 = ___inf1;
		InflaterInputStream__ctor_m2026116311(__this, L_0, L_1, ((int32_t)4096), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater,System.Int32)
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterInputBuffer_t3211561891_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral298979143;
extern Il2CppCodeGenString* _stringLiteral3230242355;
extern Il2CppCodeGenString* _stringLiteral2350639573;
extern const uint32_t InflaterInputStream__ctor_m2026116311_MetadataUsageId;
extern "C"  void InflaterInputStream__ctor_m2026116311 (InflaterInputStream_t663799889 * __this, Stream_t3255436806 * ___baseInputStream0, Inflater_t3958302971 * ___inflater1, int32_t ___bufferSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream__ctor_m2026116311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_isStreamOwner_7((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream__ctor_m1531324023(__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___baseInputStream0;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral298979143, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001b:
	{
		Inflater_t3958302971 * L_2 = ___inflater1;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral3230242355, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		int32_t L_4 = ___bufferSize2;
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_5, _stringLiteral2350639573, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0038:
	{
		Stream_t3255436806 * L_6 = ___baseInputStream0;
		__this->set_baseInputStream_4(L_6);
		Inflater_t3958302971 * L_7 = ___inflater1;
		__this->set_inf_2(L_7);
		Stream_t3255436806 * L_8 = ___baseInputStream0;
		int32_t L_9 = ___bufferSize2;
		InflaterInputBuffer_t3211561891 * L_10 = (InflaterInputBuffer_t3211561891 *)il2cpp_codegen_object_new(InflaterInputBuffer_t3211561891_il2cpp_TypeInfo_var);
		InflaterInputBuffer__ctor_m1983366716(L_10, L_8, L_9, /*hidden argument*/NULL);
		__this->set_inputBuffer_3(L_10);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Skip(System.Int64)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern const uint32_t InflaterInputStream_Skip_m2816449498_MetadataUsageId;
extern "C"  int64_t InflaterInputStream_Skip_m2816449498 (InflaterInputStream_t663799889 * __this, int64_t ___count0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Skip_m2816449498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	{
		int64_t L_0 = ___count0;
		if ((((int64_t)L_0) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0010;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, _stringLiteral1554746267, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0010:
	{
		Stream_t3255436806 * L_2 = __this->get_baseInputStream_4();
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.IO.Stream::get_CanSeek() */, L_2);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Stream_t3255436806 * L_4 = __this->get_baseInputStream_4();
		int64_t L_5 = ___count0;
		NullCheck(L_4);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_4, L_5, 1);
		int64_t L_6 = ___count0;
		return L_6;
	}

IL_002d:
	{
		V_0 = ((int32_t)2048);
		int64_t L_7 = ___count0;
		int32_t L_8 = V_0;
		if ((((int64_t)L_7) >= ((int64_t)(((int64_t)((int64_t)L_8))))))
		{
			goto IL_003b;
		}
	}
	{
		int64_t L_9 = ___count0;
		V_0 = (((int32_t)((int32_t)L_9)));
	}

IL_003b:
	{
		int32_t L_10 = V_0;
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_10));
		V_2 = 1;
		int64_t L_11 = ___count0;
		V_3 = L_11;
		goto IL_0064;
	}

IL_0048:
	{
		int64_t L_12 = V_3;
		int32_t L_13 = V_0;
		if ((((int64_t)L_12) >= ((int64_t)(((int64_t)((int64_t)L_13))))))
		{
			goto IL_0050;
		}
	}
	{
		int64_t L_14 = V_3;
		V_0 = (((int32_t)((int32_t)L_14)));
	}

IL_0050:
	{
		Stream_t3255436806 * L_15 = __this->get_baseInputStream_4();
		ByteU5BU5D_t3397334013* L_16 = V_1;
		int32_t L_17 = V_0;
		NullCheck(L_15);
		int32_t L_18 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_15, L_16, 0, L_17);
		V_2 = L_18;
		int64_t L_19 = V_3;
		int32_t L_20 = V_2;
		V_3 = ((int64_t)((int64_t)L_19-(int64_t)(((int64_t)((int64_t)L_20)))));
	}

IL_0064:
	{
		int64_t L_21 = V_3;
		if ((((int64_t)L_21) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_006d;
		}
	}
	{
		int32_t L_22 = V_2;
		if ((((int32_t)L_22) > ((int32_t)0)))
		{
			goto IL_0048;
		}
	}

IL_006d:
	{
		int64_t L_23 = ___count0;
		int64_t L_24 = V_3;
		return ((int64_t)((int64_t)L_23-(int64_t)L_24));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::StopDecrypting()
extern "C"  void InflaterInputStream_StopDecrypting_m3004313053 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		InflaterInputBuffer_t3211561891 * L_0 = __this->get_inputBuffer_3();
		NullCheck(L_0);
		InflaterInputBuffer_set_CryptoTransform_m4126126743(L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3700388773;
extern const uint32_t InflaterInputStream_Fill_m3290598767_MetadataUsageId;
extern "C"  void InflaterInputStream_Fill_m3290598767 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Fill_m3290598767_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InflaterInputBuffer_t3211561891 * L_0 = __this->get_inputBuffer_3();
		NullCheck(L_0);
		int32_t L_1 = InflaterInputBuffer_get_Available_m2874517854(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		InflaterInputBuffer_t3211561891 * L_2 = __this->get_inputBuffer_3();
		NullCheck(L_2);
		InflaterInputBuffer_Fill_m1947457127(L_2, /*hidden argument*/NULL);
		InflaterInputBuffer_t3211561891 * L_3 = __this->get_inputBuffer_3();
		NullCheck(L_3);
		int32_t L_4 = InflaterInputBuffer_get_Available_m2874517854(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		SharpZipBaseException_t4116900137 * L_5 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_5, _stringLiteral3700388773, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0032:
	{
		InflaterInputBuffer_t3211561891 * L_6 = __this->get_inputBuffer_3();
		Inflater_t3958302971 * L_7 = __this->get_inf_2();
		NullCheck(L_6);
		InflaterInputBuffer_SetInflaterInput_m1611710434(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanRead()
extern "C"  bool InflaterInputStream_get_CanRead_m1913146067 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_baseInputStream_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanRead() */, L_0);
		return L_1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanSeek()
extern "C"  bool InflaterInputStream_get_CanSeek_m2841191721 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanWrite()
extern "C"  bool InflaterInputStream_get_CanWrite_m2864049414 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Length()
extern "C"  int64_t InflaterInputStream_get_Length_m3697460204 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		InflaterInputBuffer_t3211561891 * L_0 = __this->get_inputBuffer_3();
		NullCheck(L_0);
		int32_t L_1 = InflaterInputBuffer_get_RawLength_m1091203859(L_0, /*hidden argument*/NULL);
		return (((int64_t)((int64_t)L_1)));
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Position()
extern "C"  int64_t InflaterInputStream_get_Position_m1849419937 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_baseInputStream_4();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::set_Position(System.Int64)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral10325963;
extern const uint32_t InflaterInputStream_set_Position_m4107520104_MetadataUsageId;
extern "C"  void InflaterInputStream_set_Position_m4107520104 (InflaterInputStream_t663799889 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_set_Position_m4107520104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, _stringLiteral10325963, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Flush()
extern "C"  void InflaterInputStream_Flush_m3998855354 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_baseInputStream_4();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(17 /* System.Void System.IO.Stream::Flush() */, L_0);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1547127503;
extern const uint32_t InflaterInputStream_Seek_m2298159084_MetadataUsageId;
extern "C"  int64_t InflaterInputStream_Seek_m2298159084 (InflaterInputStream_t663799889 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Seek_m2298159084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, _stringLiteral1547127503, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::SetLength(System.Int64)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1496895928;
extern const uint32_t InflaterInputStream_SetLength_m3703035084_MetadataUsageId;
extern "C"  void InflaterInputStream_SetLength_m3703035084 (InflaterInputStream_t663799889 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_SetLength_m3703035084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, _stringLiteral1496895928, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4264386567;
extern const uint32_t InflaterInputStream_Write_m1446803052_MetadataUsageId;
extern "C"  void InflaterInputStream_Write_m1446803052 (InflaterInputStream_t663799889 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Write_m1446803052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, _stringLiteral4264386567, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::WriteByte(System.Byte)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1222067985;
extern const uint32_t InflaterInputStream_WriteByte_m2246300832_MetadataUsageId;
extern "C"  void InflaterInputStream_WriteByte_m2246300832 (InflaterInputStream_t663799889 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_WriteByte_m2246300832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, _stringLiteral1222067985, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2884968230;
extern const uint32_t InflaterInputStream_BeginWrite_m1256988965_MetadataUsageId;
extern "C"  Il2CppObject * InflaterInputStream_BeginWrite_m1256988965 (InflaterInputStream_t663799889 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_BeginWrite_m1256988965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, _stringLiteral2884968230, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Close()
extern "C"  void InflaterInputStream_Close_m2859275264 (InflaterInputStream_t663799889 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isClosed_6();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		__this->set_isClosed_6((bool)1);
		bool L_1 = __this->get_isStreamOwner_7();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Stream_t3255436806 * L_2 = __this->get_baseInputStream_4();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(14 /* System.Void System.IO.Stream::Close() */, L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3636869655;
extern Il2CppCodeGenString* _stringLiteral2578238588;
extern const uint32_t InflaterInputStream_Read_m2526892531_MetadataUsageId;
extern "C"  int32_t InflaterInputStream_Read_m2526892531 (InflaterInputStream_t663799889 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Read_m2526892531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Inflater_t3958302971 * L_0 = __this->get_inf_2();
		NullCheck(L_0);
		bool L_1 = Inflater_get_IsNeedingDictionary_m130412086(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		SharpZipBaseException_t4116900137 * L_2 = (SharpZipBaseException_t4116900137 *)il2cpp_codegen_object_new(SharpZipBaseException_t4116900137_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m3232255894(L_2, _stringLiteral3636869655, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		int32_t L_3 = ___count2;
		V_0 = L_3;
	}

IL_001a:
	{
		Inflater_t3958302971 * L_4 = __this->get_inf_2();
		ByteU5BU5D_t3397334013* L_5 = ___buffer0;
		int32_t L_6 = ___offset1;
		int32_t L_7 = V_0;
		NullCheck(L_4);
		int32_t L_8 = Inflater_Inflate_m2382363959(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = ___offset1;
		int32_t L_10 = V_1;
		___offset1 = ((int32_t)((int32_t)L_9+(int32_t)L_10));
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		V_0 = ((int32_t)((int32_t)L_11-(int32_t)L_12));
		int32_t L_13 = V_0;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		Inflater_t3958302971 * L_14 = __this->get_inf_2();
		NullCheck(L_14);
		bool L_15 = Inflater_get_IsFinished_m3508682262(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0065;
		}
	}
	{
		Inflater_t3958302971 * L_16 = __this->get_inf_2();
		NullCheck(L_16);
		bool L_17 = Inflater_get_IsNeedingInput_m3454491888(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0057;
		}
	}
	{
		InflaterInputStream_Fill_m3290598767(__this, /*hidden argument*/NULL);
		goto IL_001a;
	}

IL_0057:
	{
		int32_t L_18 = V_1;
		if (L_18)
		{
			goto IL_001a;
		}
	}
	{
		ZipException_t65220526 * L_19 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_19, _stringLiteral2578238588, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0065:
	{
		int32_t L_20 = ___count2;
		int32_t L_21 = V_0;
		return ((int32_t)((int32_t)L_20-(int32_t)L_21));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3554181139;
extern const uint32_t OutputWindow_Write_m1680840174_MetadataUsageId;
extern "C"  void OutputWindow_Write_m1680840174 (OutputWindow_t824811027 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow_Write_m1680840174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_windowFilled_2();
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_windowFilled_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)32768)))))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, _stringLiteral3554181139, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		ByteU5BU5D_t3397334013* L_4 = __this->get_window_0();
		int32_t L_5 = __this->get_windowEnd_1();
		int32_t L_6 = L_5;
		V_1 = L_6;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_6+(int32_t)1)));
		int32_t L_7 = V_1;
		int32_t L_8 = ___value0;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (uint8_t)(((int32_t)((uint8_t)L_8))));
		int32_t L_9 = __this->get_windowEnd_1();
		__this->set_windowEnd_1(((int32_t)((int32_t)L_9&(int32_t)((int32_t)32767))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern "C"  void OutputWindow_SlowRepeat_m1028604019 (OutputWindow_t824811027 * __this, int32_t ___repStart0, int32_t ___length1, int32_t ___distance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0042;
	}

IL_0002:
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_window_0();
		int32_t L_1 = __this->get_windowEnd_1();
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_0;
		ByteU5BU5D_t3397334013* L_4 = __this->get_window_0();
		int32_t L_5 = ___repStart0;
		int32_t L_6 = L_5;
		___repStart0 = ((int32_t)((int32_t)L_6+(int32_t)1));
		NullCheck(L_4);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)L_8);
		int32_t L_9 = __this->get_windowEnd_1();
		__this->set_windowEnd_1(((int32_t)((int32_t)L_9&(int32_t)((int32_t)32767))));
		int32_t L_10 = ___repStart0;
		___repStart0 = ((int32_t)((int32_t)L_10&(int32_t)((int32_t)32767)));
	}

IL_0042:
	{
		int32_t L_11 = ___length1;
		int32_t L_12 = L_11;
		___length1 = ((int32_t)((int32_t)L_12-(int32_t)1));
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3554181139;
extern const uint32_t OutputWindow_Repeat_m4052354553_MetadataUsageId;
extern "C"  void OutputWindow_Repeat_m4052354553 (OutputWindow_t824811027 * __this, int32_t ___length0, int32_t ___distance1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow_Repeat_m4052354553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_windowFilled_2();
		int32_t L_1 = ___length0;
		int32_t L_2 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		V_2 = L_2;
		__this->set_windowFilled_2(L_2);
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)32768))))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_4 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_4, _stringLiteral3554181139, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		int32_t L_5 = __this->get_windowEnd_1();
		int32_t L_6 = ___distance1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))&(int32_t)((int32_t)32767)));
		int32_t L_7 = ___length0;
		V_1 = ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_7));
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		if ((((int32_t)L_8) > ((int32_t)L_9)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_10 = __this->get_windowEnd_1();
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_12 = ___length0;
		int32_t L_13 = ___distance1;
		if ((((int32_t)L_12) > ((int32_t)L_13)))
		{
			goto IL_0097;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_14 = __this->get_window_0();
		int32_t L_15 = V_0;
		ByteU5BU5D_t3397334013* L_16 = __this->get_window_0();
		int32_t L_17 = __this->get_windowEnd_1();
		int32_t L_18 = ___length0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_14, L_15, (Il2CppArray *)(Il2CppArray *)L_16, L_17, L_18, /*hidden argument*/NULL);
		int32_t L_19 = __this->get_windowEnd_1();
		int32_t L_20 = ___length0;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_19+(int32_t)L_20)));
		return;
	}

IL_0073:
	{
		ByteU5BU5D_t3397334013* L_21 = __this->get_window_0();
		int32_t L_22 = __this->get_windowEnd_1();
		int32_t L_23 = L_22;
		V_3 = L_23;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_23+(int32_t)1)));
		int32_t L_24 = V_3;
		ByteU5BU5D_t3397334013* L_25 = __this->get_window_0();
		int32_t L_26 = V_0;
		int32_t L_27 = L_26;
		V_0 = ((int32_t)((int32_t)L_27+(int32_t)1));
		NullCheck(L_25);
		int32_t L_28 = L_27;
		uint8_t L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (uint8_t)L_29);
	}

IL_0097:
	{
		int32_t L_30 = ___length0;
		int32_t L_31 = L_30;
		___length0 = ((int32_t)((int32_t)L_31-(int32_t)1));
		if ((((int32_t)L_31) > ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		return;
	}

IL_00a1:
	{
		int32_t L_32 = V_0;
		int32_t L_33 = ___length0;
		int32_t L_34 = ___distance1;
		OutputWindow_SlowRepeat_m1028604019(__this, L_32, L_33, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern "C"  int32_t OutputWindow_CopyStored_m1154690660 (OutputWindow_t824811027 * __this, StreamManipulator_t1922289728 * ___input0, int32_t ___length1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___length1;
		int32_t L_1 = __this->get_windowFilled_2();
		int32_t L_2 = Math_Min_m4290821911(NULL /*static, unused*/, L_0, ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_1)), /*hidden argument*/NULL);
		StreamManipulator_t1922289728 * L_3 = ___input0;
		NullCheck(L_3);
		int32_t L_4 = StreamManipulator_get_AvailableBytes_m4021342904(L_3, /*hidden argument*/NULL);
		int32_t L_5 = Math_Min_m4290821911(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		___length1 = L_5;
		int32_t L_6 = __this->get_windowEnd_1();
		V_1 = ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_6));
		int32_t L_7 = ___length1;
		int32_t L_8 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_005d;
		}
	}
	{
		StreamManipulator_t1922289728 * L_9 = ___input0;
		ByteU5BU5D_t3397334013* L_10 = __this->get_window_0();
		int32_t L_11 = __this->get_windowEnd_1();
		int32_t L_12 = V_1;
		NullCheck(L_9);
		int32_t L_13 = StreamManipulator_CopyBytes_m860716032(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_16 = V_0;
		StreamManipulator_t1922289728 * L_17 = ___input0;
		ByteU5BU5D_t3397334013* L_18 = __this->get_window_0();
		int32_t L_19 = ___length1;
		int32_t L_20 = V_1;
		NullCheck(L_17);
		int32_t L_21 = StreamManipulator_CopyBytes_m860716032(L_17, L_18, 0, ((int32_t)((int32_t)L_19-(int32_t)L_20)), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)L_21));
		goto IL_0071;
	}

IL_005d:
	{
		StreamManipulator_t1922289728 * L_22 = ___input0;
		ByteU5BU5D_t3397334013* L_23 = __this->get_window_0();
		int32_t L_24 = __this->get_windowEnd_1();
		int32_t L_25 = ___length1;
		NullCheck(L_22);
		int32_t L_26 = StreamManipulator_CopyBytes_m860716032(L_22, L_23, L_24, L_25, /*hidden argument*/NULL);
		V_0 = L_26;
	}

IL_0071:
	{
		int32_t L_27 = __this->get_windowEnd_1();
		int32_t L_28 = V_0;
		__this->set_windowEnd_1(((int32_t)((int32_t)((int32_t)((int32_t)L_27+(int32_t)L_28))&(int32_t)((int32_t)32767))));
		int32_t L_29 = __this->get_windowFilled_2();
		int32_t L_30 = V_0;
		__this->set_windowFilled_2(((int32_t)((int32_t)L_29+(int32_t)L_30)));
		int32_t L_31 = V_0;
		return L_31;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern "C"  int32_t OutputWindow_GetFreeSpace_m841544794 (OutputWindow_t824811027 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowFilled_2();
		return ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_0));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern "C"  int32_t OutputWindow_GetAvailable_m2569373299 (OutputWindow_t824811027 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowFilled_2();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t OutputWindow_CopyOutput_m200326477_MetadataUsageId;
extern "C"  int32_t OutputWindow_CopyOutput_m200326477 (OutputWindow_t824811027 * __this, ByteU5BU5D_t3397334013* ___output0, int32_t ___offset1, int32_t ___len2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow_CopyOutput_m200326477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_windowEnd_1();
		V_0 = L_0;
		int32_t L_1 = ___len2;
		int32_t L_2 = __this->get_windowFilled_2();
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = __this->get_windowFilled_2();
		___len2 = L_3;
		goto IL_0030;
	}

IL_001a:
	{
		int32_t L_4 = __this->get_windowEnd_1();
		int32_t L_5 = __this->get_windowFilled_2();
		int32_t L_6 = ___len2;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)L_5))+(int32_t)L_6))&(int32_t)((int32_t)32767)));
	}

IL_0030:
	{
		int32_t L_7 = ___len2;
		V_1 = L_7;
		int32_t L_8 = ___len2;
		int32_t L_9 = V_0;
		V_2 = ((int32_t)((int32_t)L_8-(int32_t)L_9));
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0057;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_11 = __this->get_window_0();
		int32_t L_12 = V_2;
		ByteU5BU5D_t3397334013* L_13 = ___output0;
		int32_t L_14 = ___offset1;
		int32_t L_15 = V_2;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_11, ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_12)), (Il2CppArray *)(Il2CppArray *)L_13, L_14, L_15, /*hidden argument*/NULL);
		int32_t L_16 = ___offset1;
		int32_t L_17 = V_2;
		___offset1 = ((int32_t)((int32_t)L_16+(int32_t)L_17));
		int32_t L_18 = V_0;
		___len2 = L_18;
	}

IL_0057:
	{
		ByteU5BU5D_t3397334013* L_19 = __this->get_window_0();
		int32_t L_20 = V_0;
		int32_t L_21 = ___len2;
		ByteU5BU5D_t3397334013* L_22 = ___output0;
		int32_t L_23 = ___offset1;
		int32_t L_24 = ___len2;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_19, ((int32_t)((int32_t)L_20-(int32_t)L_21)), (Il2CppArray *)(Il2CppArray *)L_22, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = __this->get_windowFilled_2();
		int32_t L_26 = V_1;
		__this->set_windowFilled_2(((int32_t)((int32_t)L_25-(int32_t)L_26)));
		int32_t L_27 = __this->get_windowFilled_2();
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		InvalidOperationException_t721527559 * L_28 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_0085:
	{
		int32_t L_29 = V_1;
		return L_29;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern "C"  void OutputWindow_Reset_m545756291 (OutputWindow_t824811027 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_windowEnd_1(L_0);
		int32_t L_1 = V_0;
		__this->set_windowFilled_2(L_1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t OutputWindow__ctor_m2270688772_MetadataUsageId;
extern "C"  void OutputWindow__ctor_m2270688772 (OutputWindow_t824811027 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow__ctor_m2270688772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_window_0(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32768))));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
extern "C"  void StreamManipulator__ctor_m1888846459 (StreamManipulator_t1922289728 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
extern "C"  int32_t StreamManipulator_PeekBits_m1052229955 (StreamManipulator_t1922289728 * __this, int32_t ___bitCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_bitsInBuffer__4();
		int32_t L_1 = ___bitCount0;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_2 = __this->get_windowStart__1();
		int32_t L_3 = __this->get_windowEnd__2();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0019;
		}
	}
	{
		return (-1);
	}

IL_0019:
	{
		uint32_t L_4 = __this->get_buffer__3();
		ByteU5BU5D_t3397334013* L_5 = __this->get_window__0();
		int32_t L_6 = __this->get_windowStart__1();
		int32_t L_7 = L_6;
		V_0 = L_7;
		__this->set_windowStart__1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ByteU5BU5D_t3397334013* L_11 = __this->get_window__0();
		int32_t L_12 = __this->get_windowStart__1();
		int32_t L_13 = L_12;
		V_1 = L_13;
		__this->set_windowStart__1(((int32_t)((int32_t)L_13+(int32_t)1)));
		int32_t L_14 = V_1;
		NullCheck(L_11);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((int32_t)L_4|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)255)))<<(int32_t)8))))<<(int32_t)((int32_t)((int32_t)L_17&(int32_t)((int32_t)31))))))));
		int32_t L_18 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_18+(int32_t)((int32_t)16))));
	}

IL_007e:
	{
		uint32_t L_19 = __this->get_buffer__3();
		int32_t L_20 = ___bitCount0;
		return (((int32_t)((int32_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_19)))&(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_20&(int32_t)((int32_t)31)))))-(int32_t)1))))))))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
extern "C"  void StreamManipulator_DropBits_m4077687679 (StreamManipulator_t1922289728 * __this, int32_t ___bitCount0, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_buffer__3();
		int32_t L_1 = ___bitCount0;
		__this->set_buffer__3(((int32_t)((uint32_t)L_0>>((int32_t)((int32_t)L_1&(int32_t)((int32_t)31))))));
		int32_t L_2 = __this->get_bitsInBuffer__4();
		int32_t L_3 = ___bitCount0;
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_2-(int32_t)L_3)));
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
extern "C"  int32_t StreamManipulator_get_AvailableBits_m4055873297 (StreamManipulator_t1922289728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_bitsInBuffer__4();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
extern "C"  int32_t StreamManipulator_get_AvailableBytes_m4021342904 (StreamManipulator_t1922289728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowEnd__2();
		int32_t L_1 = __this->get_windowStart__1();
		int32_t L_2 = __this->get_bitsInBuffer__4();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0-(int32_t)L_1))+(int32_t)((int32_t)((int32_t)L_2>>(int32_t)3))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
extern "C"  void StreamManipulator_SkipToByteBoundary_m2487502935 (StreamManipulator_t1922289728 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_buffer__3();
		int32_t L_1 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((uint32_t)L_0>>((int32_t)((int32_t)((int32_t)((int32_t)L_1&(int32_t)7))&(int32_t)((int32_t)31))))));
		int32_t L_2 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_2&(int32_t)((int32_t)-8))));
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
extern "C"  bool StreamManipulator_get_IsNeedingInput_m2895772626 (StreamManipulator_t1922289728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowStart__1();
		int32_t L_1 = __this->get_windowEnd__2();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3438791774;
extern Il2CppCodeGenString* _stringLiteral557564043;
extern const uint32_t StreamManipulator_CopyBytes_m860716032_MetadataUsageId;
extern "C"  int32_t StreamManipulator_CopyBytes_m860716032 (StreamManipulator_t1922289728 * __this, ByteU5BU5D_t3397334013* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StreamManipulator_CopyBytes_m860716032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, _stringLiteral3438791774, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000f:
	{
		int32_t L_2 = __this->get_bitsInBuffer__4();
		if (!((int32_t)((int32_t)L_2&(int32_t)7)))
		{
			goto IL_0024;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, _stringLiteral557564043, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		V_0 = 0;
		goto IL_005c;
	}

IL_0028:
	{
		ByteU5BU5D_t3397334013* L_4 = ___output0;
		int32_t L_5 = ___offset1;
		int32_t L_6 = L_5;
		___offset1 = ((int32_t)((int32_t)L_6+(int32_t)1));
		uint32_t L_7 = __this->get_buffer__3();
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)(((int32_t)((uint8_t)L_7))));
		uint32_t L_8 = __this->get_buffer__3();
		__this->set_buffer__3(((int32_t)((uint32_t)L_8>>8)));
		int32_t L_9 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_9-(int32_t)8)));
		int32_t L_10 = ___length2;
		___length2 = ((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_12 = __this->get_bitsInBuffer__4();
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_13 = ___length2;
		if ((((int32_t)L_13) > ((int32_t)0)))
		{
			goto IL_0028;
		}
	}

IL_0069:
	{
		int32_t L_14 = ___length2;
		if (L_14)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_15 = V_0;
		return L_15;
	}

IL_006e:
	{
		int32_t L_16 = __this->get_windowEnd__2();
		int32_t L_17 = __this->get_windowStart__1();
		V_1 = ((int32_t)((int32_t)L_16-(int32_t)L_17));
		int32_t L_18 = ___length2;
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)L_19)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_20 = V_1;
		___length2 = L_20;
	}

IL_0083:
	{
		ByteU5BU5D_t3397334013* L_21 = __this->get_window__0();
		int32_t L_22 = __this->get_windowStart__1();
		ByteU5BU5D_t3397334013* L_23 = ___output0;
		int32_t L_24 = ___offset1;
		int32_t L_25 = ___length2;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_21, L_22, (Il2CppArray *)(Il2CppArray *)L_23, L_24, L_25, /*hidden argument*/NULL);
		int32_t L_26 = __this->get_windowStart__1();
		int32_t L_27 = ___length2;
		__this->set_windowStart__1(((int32_t)((int32_t)L_26+(int32_t)L_27)));
		int32_t L_28 = __this->get_windowStart__1();
		int32_t L_29 = __this->get_windowEnd__2();
		if (!((int32_t)((int32_t)((int32_t)((int32_t)L_28-(int32_t)L_29))&(int32_t)1)))
		{
			goto IL_00e1;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_30 = __this->get_window__0();
		int32_t L_31 = __this->get_windowStart__1();
		int32_t L_32 = L_31;
		V_2 = L_32;
		__this->set_windowStart__1(((int32_t)((int32_t)L_32+(int32_t)1)));
		int32_t L_33 = V_2;
		NullCheck(L_30);
		int32_t L_34 = L_33;
		uint8_t L_35 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		__this->set_buffer__3(((int32_t)((int32_t)L_35&(int32_t)((int32_t)255))));
		__this->set_bitsInBuffer__4(8);
	}

IL_00e1:
	{
		int32_t L_36 = V_0;
		int32_t L_37 = ___length2;
		return ((int32_t)((int32_t)L_36+(int32_t)L_37));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
extern "C"  void StreamManipulator_Reset_m13518536 (StreamManipulator_t1922289728 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		__this->set_buffer__3(0);
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_bitsInBuffer__4(L_0);
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->set_windowEnd__2(L_2);
		int32_t L_3 = V_1;
		__this->set_windowStart__1(L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern Il2CppCodeGenString* _stringLiteral2769135693;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral1362041555;
extern const uint32_t StreamManipulator_SetInput_m3559023036_MetadataUsageId;
extern "C"  void StreamManipulator_SetInput_m3559023036 (StreamManipulator_t1922289728 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StreamManipulator_SetInput_m3559023036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___offset1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1673268925, _stringLiteral2769135693, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___count2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_5, _stringLiteral1554746267, _stringLiteral2769135693, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0036:
	{
		int32_t L_6 = __this->get_windowStart__1();
		int32_t L_7 = __this->get_windowEnd__2();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_004f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_8 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_8, _stringLiteral1362041555, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004f:
	{
		int32_t L_9 = ___offset1;
		int32_t L_10 = ___count2;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)L_10));
		int32_t L_11 = ___offset1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) > ((int32_t)L_12)))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_13 = V_0;
		ByteU5BU5D_t3397334013* L_14 = ___buffer0;
		NullCheck(L_14);
		if ((((int32_t)L_13) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0068;
		}
	}

IL_005d:
	{
		ArgumentOutOfRangeException_t279959794 * L_15 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_15, _stringLiteral1554746267, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0068:
	{
		int32_t L_16 = ___count2;
		if (!((int32_t)((int32_t)L_16&(int32_t)1)))
		{
			goto IL_00a0;
		}
	}
	{
		uint32_t L_17 = __this->get_buffer__3();
		ByteU5BU5D_t3397334013* L_18 = ___buffer0;
		int32_t L_19 = ___offset1;
		int32_t L_20 = L_19;
		___offset1 = ((int32_t)((int32_t)L_20+(int32_t)1));
		NullCheck(L_18);
		int32_t L_21 = L_20;
		uint8_t L_22 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		int32_t L_23 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((int32_t)L_17|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_22&(int32_t)((int32_t)255)))<<(int32_t)((int32_t)((int32_t)L_23&(int32_t)((int32_t)31))))))));
		int32_t L_24 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_24+(int32_t)8)));
	}

IL_00a0:
	{
		ByteU5BU5D_t3397334013* L_25 = ___buffer0;
		__this->set_window__0(L_25);
		int32_t L_26 = ___offset1;
		__this->set_windowStart__1(L_26);
		int32_t L_27 = V_0;
		__this->set_windowEnd__2(L_27);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_CompressedSize()
extern "C"  int64_t DescriptorData_get_CompressedSize_m3383523441 (DescriptorData_t4106407219 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_compressedSize_1();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_CompressedSize(System.Int64)
extern "C"  void DescriptorData_set_CompressedSize_m3229241118 (DescriptorData_t4106407219 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_compressedSize_1(L_0);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_Size()
extern "C"  int64_t DescriptorData_get_Size_m1456873818 (DescriptorData_t4106407219 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_size_0();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_Size(System.Int64)
extern "C"  void DescriptorData_set_Size_m2586613261 (DescriptorData_t4106407219 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_size_0(L_0);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.DescriptorData::get_Crc()
extern "C"  int64_t DescriptorData_get_Crc_m1588606453 (DescriptorData_t4106407219 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_crc_2();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::set_Crc(System.Int64)
extern "C"  void DescriptorData_set_Crc_m438507950 (DescriptorData_t4106407219 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_crc_2(((int64_t)((int64_t)L_0&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.DescriptorData::.ctor()
extern "C"  void DescriptorData__ctor_m854587251 (DescriptorData_t4106407219 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::.ctor(System.String,System.Byte[])
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t KeysRequiredEventArgs__ctor_m554196931_MetadataUsageId;
extern "C"  void KeysRequiredEventArgs__ctor_m554196931 (KeysRequiredEventArgs_t881954000 * __this, String_t* ___name0, ByteU5BU5D_t3397334013* ___keyValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeysRequiredEventArgs__ctor_m554196931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_fileName_1(L_0);
		ByteU5BU5D_t3397334013* L_1 = ___keyValue1;
		__this->set_key_2(L_1);
		return;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs::get_Key()
extern "C"  ByteU5BU5D_t3397334013* KeysRequiredEventArgs_get_Key_m4244227656 (KeysRequiredEventArgs_t881954000 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_key_2();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile)
extern "C"  void TestStatus__ctor_m1152201749 (TestStatus_t3549853348 * __this, ZipFile_t1110175137 * ___file0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ZipFile_t1110175137 * L_0 = ___file0;
		__this->set_file__0(L_0);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.TestStatus::get_ErrorCount()
extern "C"  int32_t TestStatus_get_ErrorCount_m3841239444 (TestStatus_t3549853348 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_errorCount__3();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::AddError()
extern "C"  void TestStatus_AddError_m3731266275 (TestStatus_t3549853348 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_errorCount__3();
		__this->set_errorCount__3(((int32_t)((int32_t)L_0+(int32_t)1)));
		__this->set_entryValid__2((bool)0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetOperation(ICSharpCode.SharpZipLib.Zip.TestOperation)
extern "C"  void TestStatus_SetOperation_m1653784204 (TestStatus_t3549853348 * __this, int32_t ___operation0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___operation0;
		__this->set_operation__5(L_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  void TestStatus_SetEntry_m3439478977 (TestStatus_t3549853348 * __this, ZipEntry_t1764014695 * ___entry0, const MethodInfo* method)
{
	{
		ZipEntry_t1764014695 * L_0 = ___entry0;
		__this->set_entry__1(L_0);
		__this->set_entryValid__2((bool)1);
		__this->set_bytesTested__4((((int64_t)((int64_t)0))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.TestStatus::SetBytesTested(System.Int64)
extern "C"  void TestStatus_SetBytesTested_m1639387106 (TestStatus_t3549853348 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_bytesTested__4(L_0);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipConstants::get_DefaultCodePage()
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern const uint32_t ZipConstants_get_DefaultCodePage_m3773495384_MetadataUsageId;
extern "C"  int32_t ZipConstants_get_DefaultCodePage_m3773495384 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipConstants_get_DefaultCodePage_m3773495384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ZipConstants_t82438310_StaticFields*)ZipConstants_t82438310_il2cpp_TypeInfo_var->static_fields)->get_defaultCodePage_0();
		return L_0;
	}
}
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToString(System.Byte[],System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t ZipConstants_ConvertToString_m1175786030_MetadataUsageId;
extern "C"  String_t* ZipConstants_ConvertToString_m1175786030 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, int32_t ___count1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipConstants_ConvertToString_m1175786030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0009:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		int32_t L_2 = ZipConstants_get_DefaultCodePage_m3773495384(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_3 = Encoding_GetEncoding_m3069572543(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_4 = ___data0;
		int32_t L_5 = ___count1;
		NullCheck(L_3);
		String_t* L_6 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, L_5);
		return L_6;
	}
}
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToString(System.Byte[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern const uint32_t ZipConstants_ConvertToString_m314855573_MetadataUsageId;
extern "C"  String_t* ZipConstants_ConvertToString_m314855573 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipConstants_ConvertToString_m314855573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0009:
	{
		ByteU5BU5D_t3397334013* L_2 = ___data0;
		ByteU5BU5D_t3397334013* L_3 = ___data0;
		NullCheck(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		String_t* L_4 = ZipConstants_ConvertToString_m1175786030(NULL /*static, unused*/, L_2, (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToStringExt(System.Int32,System.Byte[],System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern const uint32_t ZipConstants_ConvertToStringExt_m2565864120_MetadataUsageId;
extern "C"  String_t* ZipConstants_ConvertToStringExt_m2565864120 (Il2CppObject * __this /* static, unused */, int32_t ___flags0, ByteU5BU5D_t3397334013* ___data1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipConstants_ConvertToStringExt_m2565864120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___data1;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0009:
	{
		int32_t L_2 = ___flags0;
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)2048))))
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_4 = ___data1;
		int32_t L_5 = ___count2;
		NullCheck(L_3);
		String_t* L_6 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, L_5);
		return L_6;
	}

IL_0020:
	{
		ByteU5BU5D_t3397334013* L_7 = ___data1;
		int32_t L_8 = ___count2;
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		String_t* L_9 = ZipConstants_ConvertToString_m1175786030(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.String ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToStringExt(System.Int32,System.Byte[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern const uint32_t ZipConstants_ConvertToStringExt_m2429635105_MetadataUsageId;
extern "C"  String_t* ZipConstants_ConvertToStringExt_m2429635105 (Il2CppObject * __this /* static, unused */, int32_t ___flags0, ByteU5BU5D_t3397334013* ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipConstants_ConvertToStringExt_m2429635105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___data1;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0009:
	{
		int32_t L_2 = ___flags0;
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)2048))))
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_4 = ___data1;
		ByteU5BU5D_t3397334013* L_5 = ___data1;
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))));
		return L_6;
	}

IL_0022:
	{
		ByteU5BU5D_t3397334013* L_7 = ___data1;
		ByteU5BU5D_t3397334013* L_8 = ___data1;
		NullCheck(L_8);
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		String_t* L_9 = ZipConstants_ConvertToString_m1175786030(NULL /*static, unused*/, L_7, (((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))), /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Zip.ZipConstants::ConvertToArray(System.String)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t ZipConstants_ConvertToArray_m4193379269_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* ZipConstants_ConvertToArray_m4193379269 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipConstants_ConvertToArray_m4193379269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_000a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		int32_t L_1 = ZipConstants_get_DefaultCodePage_m3773495384(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_2 = Encoding_GetEncoding_m3069572543(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___str0;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_4 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, L_3);
		return L_4;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipConstants::.cctor()
extern Il2CppClass* Thread_t241561612_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern const uint32_t ZipConstants__cctor_m2436260669_MetadataUsageId;
extern "C"  void ZipConstants__cctor_m2436260669 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipConstants__cctor_m2436260669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t241561612_il2cpp_TypeInfo_var);
		Thread_t241561612 * L_0 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CultureInfo_t3500843524 * L_1 = Thread_get_CurrentCulture_m1387904254(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		TextInfo_t3620182823 * L_2 = VirtFuncInvoker0< TextInfo_t3620182823 * >::Invoke(9 /* System.Globalization.TextInfo System.Globalization.CultureInfo::get_TextInfo() */, L_1);
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.Globalization.TextInfo::get_OEMCodePage() */, L_2);
		((ZipConstants_t82438310_StaticFields*)ZipConstants_t82438310_il2cpp_TypeInfo_var->static_fields)->set_defaultCodePage_0(L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32)
extern "C"  void ZipEntry__ctor_m2202549470 (ZipEntry_t1764014695 * __this, String_t* ___name0, int32_t ___versionRequiredToExtract1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = ___versionRequiredToExtract1;
		ZipEntry__ctor_m509450494(__this, L_0, L_1, ((int32_t)51), 8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::.ctor(System.String,System.Int32,System.Int32,ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern Il2CppCodeGenString* _stringLiteral474487319;
extern Il2CppCodeGenString* _stringLiteral3248152629;
extern const uint32_t ZipEntry__ctor_m509450494_MetadataUsageId;
extern "C"  void ZipEntry__ctor_m509450494 (ZipEntry_t1764014695 * __this, String_t* ___name0, int32_t ___versionRequiredToExtract1, int32_t ___madeByInfo2, int32_t ___method3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry__ctor_m509450494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_externalFileAttributes_1((-1));
		__this->set_method_9(8);
		__this->set_zipFileIndex_13((((int64_t)((int64_t)(-1)))));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_002a:
	{
		String_t* L_2 = ___name0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0047;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_4, _stringLiteral474487319, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0047:
	{
		int32_t L_5 = ___versionRequiredToExtract1;
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_6 = ___versionRequiredToExtract1;
		if ((((int32_t)L_6) >= ((int32_t)((int32_t)10))))
		{
			goto IL_005a;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_7 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_7, _stringLiteral3248152629, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_8 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		ZipEntry_set_DateTime_m97102529(__this, L_8, /*hidden argument*/NULL);
		String_t* L_9 = ___name0;
		__this->set_name_3(L_9);
		int32_t L_10 = ___madeByInfo2;
		__this->set_versionMadeBy_2((((int32_t)((uint16_t)L_10))));
		int32_t L_11 = ___versionRequiredToExtract1;
		__this->set_versionToExtract_6((((int32_t)((uint16_t)L_11))));
		int32_t L_12 = ___method3;
		__this->set_method_9(L_12);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsCrypted()
extern "C"  bool ZipEntry_get_IsCrypted_m2288425721 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_flags_12();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Byte ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CryptoCheckValue()
extern "C"  uint8_t ZipEntry_get_CryptoCheckValue_m2906695838 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_cryptoCheckValue__16();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CryptoCheckValue(System.Byte)
extern "C"  void ZipEntry_set_CryptoCheckValue_m3042546353 (ZipEntry_t1764014695 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value0;
		__this->set_cryptoCheckValue__16(L_0);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Flags()
extern "C"  int32_t ZipEntry_get_Flags_m1454638551 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_flags_12();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Flags(System.Int32)
extern "C"  void ZipEntry_set_Flags_m3605637110 (ZipEntry_t1764014695 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_flags_12(L_0);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ZipFileIndex()
extern "C"  int64_t ZipEntry_get_ZipFileIndex_m1800604384 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_zipFileIndex_13();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ZipFileIndex(System.Int64)
extern "C"  void ZipEntry_set_ZipFileIndex_m4266064271 (ZipEntry_t1764014695 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_zipFileIndex_13(L_0);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Offset()
extern "C"  int64_t ZipEntry_get_Offset_m1777266548 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_offset_14();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Offset(System.Int64)
extern "C"  void ZipEntry_set_Offset_m1977872389 (ZipEntry_t1764014695 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_offset_14(L_0);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_ExternalFileAttributes()
extern "C"  int32_t ZipEntry_get_ExternalFileAttributes_m527699766 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_known_0();
		if ((((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))))))
		{
			goto IL_000e;
		}
	}
	{
		return (-1);
	}

IL_000e:
	{
		int32_t L_1 = __this->get_externalFileAttributes_1();
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ExternalFileAttributes(System.Int32)
extern "C"  void ZipEntry_set_ExternalFileAttributes_m2744399311 (ZipEntry_t1764014695 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_externalFileAttributes_1(L_0);
		uint8_t L_1 = __this->get_known_0();
		__this->set_known_0((((int32_t)((uint8_t)((int32_t)((int32_t)L_1|(int32_t)((int32_t)16)))))));
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::HasDosAttributes(System.Int32)
extern "C"  bool ZipEntry_HasDosAttributes_m3034016483 (ZipEntry_t1764014695 * __this, int32_t ___attributes0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		uint8_t L_0 = __this->get_known_0();
		if (!(((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))))))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_1 = ZipEntry_get_HostSystem_m821441961(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ZipEntry_get_HostSystem_m821441961(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_002d;
		}
	}

IL_0020:
	{
		int32_t L_3 = ZipEntry_get_ExternalFileAttributes_m527699766(__this, /*hidden argument*/NULL);
		int32_t L_4 = ___attributes0;
		int32_t L_5 = ___attributes0;
		if ((!(((uint32_t)((int32_t)((int32_t)L_3&(int32_t)L_4))) == ((uint32_t)L_5))))
		{
			goto IL_002d;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_002d:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_HostSystem()
extern "C"  int32_t ZipEntry_get_HostSystem_m821441961 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = __this->get_versionMadeBy_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0>>(int32_t)8))&(int32_t)((int32_t)255)));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Version()
extern "C"  int32_t ZipEntry_get_Version_m3445061640 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		uint16_t L_0 = __this->get_versionToExtract_6();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		uint16_t L_1 = __this->get_versionToExtract_6();
		return L_1;
	}

IL_000f:
	{
		V_0 = ((int32_t)10);
		int32_t L_2 = ZipEntry_get_AESKeySize_m898847625(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		V_0 = ((int32_t)51);
		goto IL_0061;
	}

IL_0020:
	{
		bool L_3 = ZipEntry_get_CentralHeaderRequiresZip64_m819889797(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		V_0 = ((int32_t)45);
		goto IL_0061;
	}

IL_002d:
	{
		int32_t L_4 = __this->get_method_9();
		if ((!(((uint32_t)8) == ((uint32_t)L_4))))
		{
			goto IL_003b;
		}
	}
	{
		V_0 = ((int32_t)20);
		goto IL_0061;
	}

IL_003b:
	{
		bool L_5 = ZipEntry_get_IsDirectory_m4084667369(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		V_0 = ((int32_t)20);
		goto IL_0061;
	}

IL_0048:
	{
		bool L_6 = ZipEntry_get_IsCrypted_m2288425721(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		V_0 = ((int32_t)20);
		goto IL_0061;
	}

IL_0055:
	{
		bool L_7 = ZipEntry_HasDosAttributes_m3034016483(__this, 8, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		V_0 = ((int32_t)11);
	}

IL_0061:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CanDecompress()
extern "C"  bool ZipEntry_get_CanDecompress_m2861654889 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ZipEntry_get_Version_m3445061640(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)((int32_t)51))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_1 = ZipEntry_get_Version_m3445061640(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)10))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_2 = ZipEntry_get_Version_m3445061640(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)11))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = ZipEntry_get_Version_m3445061640(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)((int32_t)20))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_4 = ZipEntry_get_Version_m3445061640(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)45))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = ZipEntry_get_Version_m3445061640(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)51)))))
		{
			goto IL_0043;
		}
	}

IL_003c:
	{
		bool L_6 = ZipEntry_IsCompressionMethodSupported_m535555216(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_0043:
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_LocalHeaderRequiresZip64()
extern "C"  bool ZipEntry_get_LocalHeaderRequiresZip64_m3409813355 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	uint64_t V_1 = 0;
	int32_t G_B10_0 = 0;
	{
		bool L_0 = __this->get_forceZip64__15();
		V_0 = L_0;
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0052;
		}
	}
	{
		uint64_t L_2 = __this->get_compressedSize_5();
		V_1 = L_2;
		uint16_t L_3 = __this->get_versionToExtract_6();
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		bool L_4 = ZipEntry_get_IsCrypted_m2288425721(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		uint64_t L_5 = V_1;
		V_1 = ((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)((int32_t)12))))));
	}

IL_0027:
	{
		uint64_t L_6 = __this->get_size_4();
		if ((!(((uint64_t)L_6) < ((uint64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))))
		{
			goto IL_0036;
		}
	}
	{
		uint64_t L_7 = V_1;
		if ((!(((uint64_t)L_7) >= ((uint64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))))
		{
			goto IL_0050;
		}
	}

IL_0036:
	{
		uint16_t L_8 = __this->get_versionToExtract_6();
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		uint16_t L_9 = __this->get_versionToExtract_6();
		G_B10_0 = ((((int32_t)((((int32_t)L_9) < ((int32_t)((int32_t)45)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0051;
	}

IL_004d:
	{
		G_B10_0 = 1;
		goto IL_0051;
	}

IL_0050:
	{
		G_B10_0 = 0;
	}

IL_0051:
	{
		V_0 = (bool)G_B10_0;
	}

IL_0052:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CentralHeaderRequiresZip64()
extern "C"  bool ZipEntry_get_CentralHeaderRequiresZip64_m819889797 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ZipEntry_get_LocalHeaderRequiresZip64_m3409813355(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		int64_t L_1 = __this->get_offset_14();
		return (bool)((((int32_t)((((int64_t)L_1) < ((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0016:
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DosTime(System.Int64)
extern "C"  void ZipEntry_set_DosTime_m3930736101 (ZipEntry_t1764014695 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_dosTime_8((((int32_t)((uint32_t)L_0))));
		uint8_t L_1 = __this->get_known_0();
		__this->set_known_0((((int32_t)((uint8_t)((int32_t)((int32_t)L_1|(int32_t)8))))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_DateTime(System.DateTime)
extern "C"  void ZipEntry_set_DateTime_m97102529 (ZipEntry_t1764014695 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	uint32_t V_2 = 0;
	uint32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	{
		int32_t L_0 = DateTime_get_Year_m1985210972((&___value0), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = DateTime_get_Month_m1464831817((&___value0), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = DateTime_get_Day_m2066530041((&___value0), /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = DateTime_get_Hour_m2925715777((&___value0), /*hidden argument*/NULL);
		V_3 = L_3;
		int32_t L_4 = DateTime_get_Minute_m803043551((&___value0), /*hidden argument*/NULL);
		V_4 = L_4;
		int32_t L_5 = DateTime_get_Second_m853575361((&___value0), /*hidden argument*/NULL);
		V_5 = L_5;
		uint32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) < ((uint32_t)((int32_t)1980)))))
		{
			goto IL_004e;
		}
	}
	{
		V_0 = ((int32_t)1980);
		V_1 = 1;
		V_2 = 1;
		V_3 = 0;
		V_4 = 0;
		V_5 = 0;
		goto IL_006d;
	}

IL_004e:
	{
		uint32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) > ((uint32_t)((int32_t)2107)))))
		{
			goto IL_006d;
		}
	}
	{
		V_0 = ((int32_t)2107);
		V_1 = ((int32_t)12);
		V_2 = ((int32_t)31);
		V_3 = ((int32_t)23);
		V_4 = ((int32_t)59);
		V_5 = ((int32_t)59);
	}

IL_006d:
	{
		uint32_t L_8 = V_0;
		uint32_t L_9 = V_1;
		uint32_t L_10 = V_2;
		uint32_t L_11 = V_3;
		uint32_t L_12 = V_4;
		uint32_t L_13 = V_5;
		ZipEntry_set_DosTime_m3930736101(__this, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8-(int32_t)((int32_t)1980)))&(int32_t)((int32_t)127)))<<(int32_t)((int32_t)25)))|(int32_t)((int32_t)((int32_t)L_9<<(int32_t)((int32_t)21)))))|(int32_t)((int32_t)((int32_t)L_10<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_11<<(int32_t)((int32_t)11)))))|(int32_t)((int32_t)((int32_t)L_12<<(int32_t)5))))|(int32_t)((int32_t)((uint32_t)L_13>>1)))))))))), /*hidden argument*/NULL);
		return;
	}
}
// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Name()
extern "C"  String_t* ZipEntry_get_Name_m3476240732 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_3();
		return L_0;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Size()
extern "C"  int64_t ZipEntry_get_Size_m4101971814 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_known_0();
		if ((((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)1))))))
		{
			goto IL_000e;
		}
	}
	{
		return (((int64_t)((int64_t)(-1))));
	}

IL_000e:
	{
		uint64_t L_1 = __this->get_size_4();
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Size(System.Int64)
extern "C"  void ZipEntry_set_Size_m1592469057 (ZipEntry_t1764014695 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_size_4(L_0);
		uint8_t L_1 = __this->get_known_0();
		__this->set_known_0((((int32_t)((uint8_t)((int32_t)((int32_t)L_1|(int32_t)1))))));
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressedSize()
extern "C"  int64_t ZipEntry_get_CompressedSize_m1350691421 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_known_0();
		if ((((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)2))))))
		{
			goto IL_000e;
		}
	}
	{
		return (((int64_t)((int64_t)(-1))));
	}

IL_000e:
	{
		uint64_t L_1 = __this->get_compressedSize_5();
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressedSize(System.Int64)
extern "C"  void ZipEntry_set_CompressedSize_m3769989458 (ZipEntry_t1764014695 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_compressedSize_5(L_0);
		uint8_t L_1 = __this->get_known_0();
		__this->set_known_0((((int32_t)((uint8_t)((int32_t)((int32_t)L_1|(int32_t)2))))));
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_Crc()
extern "C"  int64_t ZipEntry_get_Crc_m3987268705 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_known_0();
		if ((((int32_t)((uint8_t)((int32_t)((int32_t)L_0&(int32_t)4))))))
		{
			goto IL_000e;
		}
	}
	{
		return (((int64_t)((int64_t)(-1))));
	}

IL_000e:
	{
		uint32_t L_1 = __this->get_crc_7();
		return ((int64_t)((int64_t)(((int64_t)((uint64_t)L_1)))&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Crc(System.Int64)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t ZipEntry_set_Crc_m2947942594_MetadataUsageId;
extern "C"  void ZipEntry_set_Crc_m2947942594 (ZipEntry_t1764014695 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_set_Crc_m2947942594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint32_t L_0 = __this->get_crc_7();
		if ((((int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_0)))&(int64_t)((int64_t)-4294967296LL)))) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0020;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0020:
	{
		int64_t L_2 = ___value0;
		__this->set_crc_7((((int32_t)((uint32_t)L_2))));
		uint8_t L_3 = __this->get_known_0();
		__this->set_known_0((((int32_t)((uint8_t)((int32_t)((int32_t)L_3|(int32_t)4))))));
		return;
	}
}
// ICSharpCode.SharpZipLib.Zip.CompressionMethod ICSharpCode.SharpZipLib.Zip.ZipEntry::get_CompressionMethod()
extern "C"  int32_t ZipEntry_get_CompressionMethod_m2735584663 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_method_9();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_CompressionMethod(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral776191010;
extern const uint32_t ZipEntry_set_CompressionMethod_m1964326120_MetadataUsageId;
extern "C"  void ZipEntry_set_CompressionMethod_m1964326120 (ZipEntry_t1764014695 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_set_CompressionMethod_m1964326120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		bool L_1 = ZipEntry_IsCompressionMethodSupported_m1749756129(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		NotSupportedException_t1793819818 * L_2 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_2, _stringLiteral776191010, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0013:
	{
		int32_t L_3 = ___value0;
		__this->set_method_9(L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_ExtraData(System.Byte[])
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t ZipEntry_set_ExtraData_m2783375251_MetadataUsageId;
extern "C"  void ZipEntry_set_ExtraData_m2783375251 (ZipEntry_t1764014695 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_set_ExtraData_m2783375251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___value0;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		__this->set_extra_10((ByteU5BU5D_t3397334013*)NULL);
		return;
	}

IL_000b:
	{
		ByteU5BU5D_t3397334013* L_1 = ___value0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0020;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0020:
	{
		ByteU5BU5D_t3397334013* L_3 = ___value0;
		NullCheck(L_3);
		__this->set_extra_10(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		ByteU5BU5D_t3397334013* L_4 = ___value0;
		ByteU5BU5D_t3397334013* L_5 = __this->get_extra_10();
		ByteU5BU5D_t3397334013* L_6 = ___value0;
		NullCheck(L_6);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, 0, (Il2CppArray *)(Il2CppArray *)L_5, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESKeySize()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2724872530;
extern const uint32_t ZipEntry_get_AESKeySize_m898847625_MetadataUsageId;
extern "C"  int32_t ZipEntry_get_AESKeySize_m898847625 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_get_AESKeySize_m898847625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get__aesEncryptionStrength_18();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_0021;
			}
			case 2:
			{
				goto IL_0027;
			}
			case 3:
			{
				goto IL_002d;
			}
		}
	}
	{
		goto IL_0033;
	}

IL_001f:
	{
		return 0;
	}

IL_0021:
	{
		return ((int32_t)128);
	}

IL_0027:
	{
		return ((int32_t)192);
	}

IL_002d:
	{
		return ((int32_t)256);
	}

IL_0033:
	{
		int32_t L_2 = __this->get__aesEncryptionStrength_18();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2724872530, L_4, /*hidden argument*/NULL);
		ZipException_t65220526 * L_6 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipEntry::get_AESSaltLen()
extern "C"  int32_t ZipEntry_get_AESSaltLen_m1790361416 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ZipEntry_get_AESKeySize_m898847625(__this, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0/(int32_t)((int32_t)16)));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessExtraData(System.Boolean)
extern Il2CppClass* ZipExtraData_t3152287325_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1470925255;
extern Il2CppCodeGenString* _stringLiteral287206254;
extern Il2CppCodeGenString* _stringLiteral1440400900;
extern const uint32_t ZipEntry_ProcessExtraData_m320672633_MetadataUsageId;
extern "C"  void ZipEntry_ProcessExtraData_m320672633 (ZipEntry_t1764014695 * __this, bool ___localHeader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_ProcessExtraData_m320672633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ZipExtraData_t3152287325 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	DateTime_t693205669  V_7;
	memset(&V_7, 0, sizeof(V_7));
	DateTime_t693205669  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_extra_10();
		ZipExtraData_t3152287325 * L_1 = (ZipExtraData_t3152287325 *)il2cpp_codegen_object_new(ZipExtraData_t3152287325_il2cpp_TypeInfo_var);
		ZipExtraData__ctor_m221466928(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ZipExtraData_t3152287325 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = ZipExtraData_Find_m2992530013(L_2, 1, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_007d;
		}
	}
	{
		__this->set_forceZip64__15((bool)1);
		ZipExtraData_t3152287325 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = ZipExtraData_get_ValueLength_m1914910453(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) >= ((int32_t)4)))
		{
			goto IL_0030;
		}
	}
	{
		ZipException_t65220526 * L_6 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_6, _stringLiteral1470925255, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0030:
	{
		bool L_7 = ___localHeader0;
		if (L_7)
		{
			goto IL_003d;
		}
	}
	{
		uint64_t L_8 = __this->get_size_4();
		if ((!(((uint64_t)L_8) == ((uint64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))))
		{
			goto IL_0049;
		}
	}

IL_003d:
	{
		ZipExtraData_t3152287325 * L_9 = V_0;
		NullCheck(L_9);
		int64_t L_10 = ZipExtraData_ReadLong_m583809138(L_9, /*hidden argument*/NULL);
		__this->set_size_4(L_10);
	}

IL_0049:
	{
		bool L_11 = ___localHeader0;
		if (L_11)
		{
			goto IL_0056;
		}
	}
	{
		uint64_t L_12 = __this->get_compressedSize_5();
		if ((!(((uint64_t)L_12) == ((uint64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))))
		{
			goto IL_0062;
		}
	}

IL_0056:
	{
		ZipExtraData_t3152287325 * L_13 = V_0;
		NullCheck(L_13);
		int64_t L_14 = ZipExtraData_ReadLong_m583809138(L_13, /*hidden argument*/NULL);
		__this->set_compressedSize_5(L_14);
	}

IL_0062:
	{
		bool L_15 = ___localHeader0;
		if (L_15)
		{
			goto IL_00ac;
		}
	}
	{
		int64_t L_16 = __this->get_offset_14();
		if ((!(((uint64_t)L_16) == ((uint64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))))
		{
			goto IL_00ac;
		}
	}
	{
		ZipExtraData_t3152287325 * L_17 = V_0;
		NullCheck(L_17);
		int64_t L_18 = ZipExtraData_ReadLong_m583809138(L_17, /*hidden argument*/NULL);
		__this->set_offset_14(L_18);
		goto IL_00ac;
	}

IL_007d:
	{
		uint16_t L_19 = __this->get_versionToExtract_6();
		if ((((int32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)255)))) < ((int32_t)((int32_t)45))))
		{
			goto IL_00ac;
		}
	}
	{
		uint64_t L_20 = __this->get_size_4();
		if ((((int64_t)L_20) == ((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))))
		{
			goto IL_00a1;
		}
	}
	{
		uint64_t L_21 = __this->get_compressedSize_5();
		if ((!(((uint64_t)L_21) == ((uint64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))))
		{
			goto IL_00ac;
		}
	}

IL_00a1:
	{
		ZipException_t65220526 * L_22 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_22, _stringLiteral287206254, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00ac:
	{
		ZipExtraData_t3152287325 * L_23 = V_0;
		NullCheck(L_23);
		bool L_24 = ZipExtraData_Find_m2992530013(L_23, ((int32_t)10), /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0122;
		}
	}
	{
		ZipExtraData_t3152287325 * L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26 = ZipExtraData_get_ValueLength_m1914910453(L_25, /*hidden argument*/NULL);
		if ((((int32_t)L_26) >= ((int32_t)4)))
		{
			goto IL_00ca;
		}
	}
	{
		ZipException_t65220526 * L_27 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_27, _stringLiteral1440400900, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27);
	}

IL_00ca:
	{
		ZipExtraData_t3152287325 * L_28 = V_0;
		NullCheck(L_28);
		ZipExtraData_ReadInt_m1089191532(L_28, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_00d3:
	{
		ZipExtraData_t3152287325 * L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = ZipExtraData_ReadShort_m4078015031(L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		ZipExtraData_t3152287325 * L_31 = V_0;
		NullCheck(L_31);
		int32_t L_32 = ZipExtraData_ReadShort_m4078015031(L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		int32_t L_33 = V_1;
		if ((!(((uint32_t)L_33) == ((uint32_t)1))))
		{
			goto IL_0110;
		}
	}
	{
		int32_t L_34 = V_2;
		if ((((int32_t)L_34) < ((int32_t)((int32_t)24))))
		{
			goto IL_0189;
		}
	}
	{
		ZipExtraData_t3152287325 * L_35 = V_0;
		NullCheck(L_35);
		int64_t L_36 = ZipExtraData_ReadLong_m583809138(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		ZipExtraData_t3152287325 * L_37 = V_0;
		NullCheck(L_37);
		ZipExtraData_ReadLong_m583809138(L_37, /*hidden argument*/NULL);
		ZipExtraData_t3152287325 * L_38 = V_0;
		NullCheck(L_38);
		ZipExtraData_ReadLong_m583809138(L_38, /*hidden argument*/NULL);
		int64_t L_39 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_40 = DateTime_FromFileTime_m725937452(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		ZipEntry_set_DateTime_m97102529(__this, L_40, /*hidden argument*/NULL);
		goto IL_0189;
	}

IL_0110:
	{
		ZipExtraData_t3152287325 * L_41 = V_0;
		int32_t L_42 = V_2;
		NullCheck(L_41);
		ZipExtraData_Skip_m1738302941(L_41, L_42, /*hidden argument*/NULL);
	}

IL_0117:
	{
		ZipExtraData_t3152287325 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = ZipExtraData_get_UnreadCount_m2092915220(L_43, /*hidden argument*/NULL);
		if ((((int32_t)L_44) >= ((int32_t)4)))
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_0189;
	}

IL_0122:
	{
		ZipExtraData_t3152287325 * L_45 = V_0;
		NullCheck(L_45);
		bool L_46 = ZipExtraData_Find_m2992530013(L_45, ((int32_t)21589), /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0189;
		}
	}
	{
		ZipExtraData_t3152287325 * L_47 = V_0;
		NullCheck(L_47);
		int32_t L_48 = ZipExtraData_get_ValueLength_m1914910453(L_47, /*hidden argument*/NULL);
		V_4 = L_48;
		ZipExtraData_t3152287325 * L_49 = V_0;
		NullCheck(L_49);
		int32_t L_50 = ZipExtraData_ReadByte_m1507753819(L_49, /*hidden argument*/NULL);
		V_5 = L_50;
		int32_t L_51 = V_5;
		if (!((int32_t)((int32_t)L_51&(int32_t)1)))
		{
			goto IL_0189;
		}
	}
	{
		int32_t L_52 = V_4;
		if ((((int32_t)L_52) < ((int32_t)5)))
		{
			goto IL_0189;
		}
	}
	{
		ZipExtraData_t3152287325 * L_53 = V_0;
		NullCheck(L_53);
		int32_t L_54 = ZipExtraData_ReadInt_m1089191532(L_53, /*hidden argument*/NULL);
		V_6 = L_54;
		DateTime_t693205669  L_55;
		memset(&L_55, 0, sizeof(L_55));
		DateTime__ctor_m3153923094(&L_55, ((int32_t)1970), 1, 1, 0, 0, 0, /*hidden argument*/NULL);
		V_7 = L_55;
		DateTime_t693205669  L_56 = DateTime_ToUniversalTime_m1815024752((&V_7), /*hidden argument*/NULL);
		int32_t L_57 = V_6;
		TimeSpan_t3430258949  L_58;
		memset(&L_58, 0, sizeof(L_58));
		TimeSpan__ctor_m1560702407(&L_58, 0, 0, 0, L_57, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_59 = DateTime_op_Addition_m1268923147(NULL /*static, unused*/, L_56, L_58, /*hidden argument*/NULL);
		V_8 = L_59;
		DateTime_t693205669  L_60 = DateTime_ToLocalTime_m1957689902((&V_8), /*hidden argument*/NULL);
		ZipEntry_set_DateTime_m97102529(__this, L_60, /*hidden argument*/NULL);
	}

IL_0189:
	{
		int32_t L_61 = __this->get_method_9();
		if ((!(((uint32_t)L_61) == ((uint32_t)((int32_t)99)))))
		{
			goto IL_019a;
		}
	}
	{
		ZipExtraData_t3152287325 * L_62 = V_0;
		ZipEntry_ProcessAESExtraData_m3415759846(__this, L_62, /*hidden argument*/NULL);
	}

IL_019a:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::ProcessAESExtraData(ICSharpCode.SharpZipLib.Zip.ZipExtraData)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2057104393;
extern Il2CppCodeGenString* _stringLiteral963492391;
extern Il2CppCodeGenString* _stringLiteral3079833349;
extern const uint32_t ZipEntry_ProcessAESExtraData_m3415759846_MetadataUsageId;
extern "C"  void ZipEntry_ProcessAESExtraData_m3415759846 (ZipEntry_t1764014695 * __this, ZipExtraData_t3152287325 * ___extraData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_ProcessAESExtraData_m3415759846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		ZipExtraData_t3152287325 * L_0 = ___extraData0;
		NullCheck(L_0);
		bool L_1 = ZipExtraData_Find_m2992530013(L_0, ((int32_t)39169), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		__this->set_versionToExtract_6(((int32_t)51));
		int32_t L_2 = ZipEntry_get_Flags_m1454638551(__this, /*hidden argument*/NULL);
		ZipEntry_set_Flags_m3605637110(__this, ((int32_t)((int32_t)L_2|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		ZipExtraData_t3152287325 * L_3 = ___extraData0;
		NullCheck(L_3);
		int32_t L_4 = ZipExtraData_get_ValueLength_m1914910453(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) >= ((int32_t)7)))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral2057104393, L_8, _stringLiteral963492391, /*hidden argument*/NULL);
		ZipException_t65220526 * L_10 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_004a:
	{
		ZipExtraData_t3152287325 * L_11 = ___extraData0;
		NullCheck(L_11);
		int32_t L_12 = ZipExtraData_ReadShort_m4078015031(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		ZipExtraData_t3152287325 * L_13 = ___extraData0;
		NullCheck(L_13);
		ZipExtraData_ReadShort_m4078015031(L_13, /*hidden argument*/NULL);
		ZipExtraData_t3152287325 * L_14 = ___extraData0;
		NullCheck(L_14);
		int32_t L_15 = ZipExtraData_ReadByte_m1507753819(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		ZipExtraData_t3152287325 * L_16 = ___extraData0;
		NullCheck(L_16);
		int32_t L_17 = ZipExtraData_ReadShort_m4078015031(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		int32_t L_18 = V_1;
		__this->set__aesVer_17(L_18);
		int32_t L_19 = V_2;
		__this->set__aesEncryptionStrength_18(L_19);
		int32_t L_20 = V_3;
		__this->set_method_9(L_20);
		return;
	}

IL_007c:
	{
		ZipException_t65220526 * L_21 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_21, _stringLiteral3079833349, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntry::set_Comment(System.String)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern Il2CppCodeGenString* _stringLiteral3842068143;
extern const uint32_t ZipEntry_set_Comment_m184649891_MetadataUsageId;
extern "C"  void ZipEntry_set_Comment_m184649891 (ZipEntry_t1764014695 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_set_Comment_m184649891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0020;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1803325615, _stringLiteral3842068143, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		String_t* L_4 = ___value0;
		__this->set_comment_11(L_4);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsDirectory()
extern "C"  bool ZipEntry_get_IsDirectory_m4084667369 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B5_0 = 0;
	{
		String_t* L_0 = __this->get_name_3();
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_3 = __this->get_name_3();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)47))))
		{
			goto IL_003e;
		}
	}
	{
		String_t* L_6 = __this->get_name_3();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m4230566705(L_6, ((int32_t)((int32_t)L_7-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)92))))
		{
			goto IL_003e;
		}
	}

IL_0034:
	{
		bool L_9 = ZipEntry_HasDosAttributes_m3034016483(__this, ((int32_t)16), /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_9));
		goto IL_003f;
	}

IL_003e:
	{
		G_B5_0 = 1;
	}

IL_003f:
	{
		V_1 = (bool)G_B5_0;
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::get_IsFile()
extern "C"  bool ZipEntry_get_IsFile_m222504024 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ZipEntry_get_IsDirectory_m4084667369(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		bool L_1 = ZipEntry_HasDosAttributes_m3034016483(__this, 8, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0013:
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported()
extern "C"  bool ZipEntry_IsCompressionMethodSupported_m535555216 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ZipEntry_get_CompressionMethod_m2735584663(__this, /*hidden argument*/NULL);
		bool L_1 = ZipEntry_IsCompressionMethodSupported_m1749756129(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object ICSharpCode.SharpZipLib.Zip.ZipEntry::Clone()
extern Il2CppClass* ZipEntry_t1764014695_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t ZipEntry_Clone_m3547274961_MetadataUsageId;
extern "C"  Il2CppObject * ZipEntry_Clone_m3547274961 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntry_Clone_m3547274961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ZipEntry_t1764014695 * V_0 = NULL;
	{
		Il2CppObject * L_0 = Object_MemberwiseClone_m2655589444(__this, /*hidden argument*/NULL);
		V_0 = ((ZipEntry_t1764014695 *)CastclassClass(L_0, ZipEntry_t1764014695_il2cpp_TypeInfo_var));
		ByteU5BU5D_t3397334013* L_1 = __this->get_extra_10();
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		ZipEntry_t1764014695 * L_2 = V_0;
		ByteU5BU5D_t3397334013* L_3 = __this->get_extra_10();
		NullCheck(L_3);
		NullCheck(L_2);
		L_2->set_extra_10(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))));
		ByteU5BU5D_t3397334013* L_4 = __this->get_extra_10();
		ZipEntry_t1764014695 * L_5 = V_0;
		NullCheck(L_5);
		ByteU5BU5D_t3397334013* L_6 = L_5->get_extra_10();
		ByteU5BU5D_t3397334013* L_7 = __this->get_extra_10();
		NullCheck(L_7);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, 0, (Il2CppArray *)(Il2CppArray *)L_6, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
	}

IL_0042:
	{
		ZipEntry_t1764014695 * L_8 = V_0;
		return L_8;
	}
}
// System.String ICSharpCode.SharpZipLib.Zip.ZipEntry::ToString()
extern "C"  String_t* ZipEntry_ToString_m331788614 (ZipEntry_t1764014695 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_3();
		return L_0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipEntry::IsCompressionMethodSupported(ICSharpCode.SharpZipLib.Zip.CompressionMethod)
extern "C"  bool ZipEntry_IsCompressionMethodSupported_m1749756129 (Il2CppObject * __this /* static, unused */, int32_t ___method0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___method0;
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___method0;
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0009:
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipEntryFactory::.ctor()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipNameTransform_t3311483194_il2cpp_TypeInfo_var;
extern const uint32_t ZipEntryFactory__ctor_m4255068507_MetadataUsageId;
extern "C"  void ZipEntryFactory__ctor_m4255068507 (ZipEntryFactory_t3498628499 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipEntryFactory__ctor_m4255068507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_fixedDateTime__1(L_0);
		__this->set_getAttributes__2((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ZipNameTransform_t3311483194 * L_1 = (ZipNameTransform_t3311483194 *)il2cpp_codegen_object_new(ZipNameTransform_t3311483194_il2cpp_TypeInfo_var);
		ZipNameTransform__ctor_m1825291376(L_1, /*hidden argument*/NULL);
		__this->set_nameTransform__0(L_1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ZipException__ctor_m565051227 (ZipException_t65220526 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		SharpZipBaseException__ctor_m1246294255(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor()
extern "C"  void ZipException__ctor_m3714735064 (ZipException_t65220526 * __this, const MethodInfo* method)
{
	{
		SharpZipBaseException__ctor_m3776338968(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.String)
extern "C"  void ZipException__ctor_m2626676718 (ZipException_t65220526 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SharpZipBaseException__ctor_m3232255894(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::.ctor(System.Byte[])
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t ZipExtraData__ctor_m221466928_MetadataUsageId;
extern "C"  void ZipExtraData__ctor_m221466928 (ZipExtraData_t3152287325 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipExtraData__ctor_m221466928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		__this->set__data_4(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}

IL_0016:
	{
		ByteU5BU5D_t3397334013* L_1 = ___data0;
		__this->set__data_4(L_1);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_ValueLength()
extern "C"  int32_t ZipExtraData_get_ValueLength_m1914910453 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__readValueLength_2();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::get_UnreadCount()
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3053020033;
extern const uint32_t ZipExtraData_get_UnreadCount_m2092915220_MetadataUsageId;
extern "C"  int32_t ZipExtraData_get_UnreadCount_m2092915220 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipExtraData_get_UnreadCount_m2092915220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__readValueStart_1();
		ByteU5BU5D_t3397334013* L_1 = __this->get__data_4();
		NullCheck(L_1);
		if ((((int32_t)L_0) > ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = __this->get__readValueStart_1();
		if ((((int32_t)L_2) >= ((int32_t)4)))
		{
			goto IL_0024;
		}
	}

IL_0019:
	{
		ZipException_t65220526 * L_3 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_3, _stringLiteral3053020033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		int32_t L_4 = __this->get__readValueStart_1();
		int32_t L_5 = __this->get__readValueLength_2();
		int32_t L_6 = __this->get__index_0();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))-(int32_t)L_6));
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipExtraData::Find(System.Int32)
extern "C"  bool ZipExtraData_Find_m2992530013 (ZipExtraData_t3152287325 * __this, int32_t ___headerID0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t G_B8_0 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get__data_4();
		NullCheck(L_0);
		__this->set__readValueStart_1((((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))));
		__this->set__readValueLength_2(0);
		__this->set__index_0(0);
		int32_t L_1 = __this->get__readValueStart_1();
		V_0 = L_1;
		int32_t L_2 = ___headerID0;
		V_1 = ((int32_t)((int32_t)L_2-(int32_t)1));
		goto IL_0049;
	}

IL_0029:
	{
		int32_t L_3 = ZipExtraData_ReadShortInternal_m2206901638(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = ZipExtraData_ReadShortInternal_m2206901638(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_1;
		int32_t L_6 = ___headerID0;
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_7 = __this->get__index_0();
		int32_t L_8 = V_0;
		__this->set__index_0(((int32_t)((int32_t)L_7+(int32_t)L_8)));
	}

IL_0049:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___headerID0;
		if ((((int32_t)L_9) == ((int32_t)L_10)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_11 = __this->get__index_0();
		ByteU5BU5D_t3397334013* L_12 = __this->get__data_4();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))-(int32_t)3)))))
		{
			goto IL_0029;
		}
	}

IL_005f:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = ___headerID0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_15 = __this->get__index_0();
		int32_t L_16 = V_0;
		ByteU5BU5D_t3397334013* L_17 = __this->get__data_4();
		NullCheck(L_17);
		G_B8_0 = ((((int32_t)((((int32_t)((int32_t)((int32_t)L_15+(int32_t)L_16))) > ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_007b;
	}

IL_007a:
	{
		G_B8_0 = 0;
	}

IL_007b:
	{
		V_2 = (bool)G_B8_0;
		bool L_18 = V_2;
		if (!L_18)
		{
			goto IL_0092;
		}
	}
	{
		int32_t L_19 = __this->get__index_0();
		__this->set__readValueStart_1(L_19);
		int32_t L_20 = V_0;
		__this->set__readValueLength_2(L_20);
	}

IL_0092:
	{
		bool L_21 = V_2;
		return L_21;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadLong()
extern "C"  int64_t ZipExtraData_ReadLong_m583809138 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	{
		ZipExtraData_ReadCheck_m97637932(__this, 8, /*hidden argument*/NULL);
		int32_t L_0 = ZipExtraData_ReadInt_m1089191532(__this, /*hidden argument*/NULL);
		int32_t L_1 = ZipExtraData_ReadInt_m1089191532(__this, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_0)))&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))|(int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadInt()
extern "C"  int32_t ZipExtraData_ReadInt_m1089191532 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ZipExtraData_ReadCheck_m97637932(__this, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = __this->get__data_4();
		int32_t L_1 = __this->get__index_0();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t3397334013* L_4 = __this->get__data_4();
		int32_t L_5 = __this->get__index_0();
		NullCheck(L_4);
		int32_t L_6 = ((int32_t)((int32_t)L_5+(int32_t)1));
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		ByteU5BU5D_t3397334013* L_8 = __this->get__data_4();
		int32_t L_9 = __this->get__index_0();
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)L_9+(int32_t)2));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		ByteU5BU5D_t3397334013* L_12 = __this->get__data_4();
		int32_t L_13 = __this->get__index_0();
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)((int32_t)L_13+(int32_t)3));
		uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)8))))+(int32_t)((int32_t)((int32_t)L_11<<(int32_t)((int32_t)16)))))+(int32_t)((int32_t)((int32_t)L_15<<(int32_t)((int32_t)24)))));
		int32_t L_16 = __this->get__index_0();
		__this->set__index_0(((int32_t)((int32_t)L_16+(int32_t)4)));
		int32_t L_17 = V_0;
		return L_17;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShort()
extern "C"  int32_t ZipExtraData_ReadShort_m4078015031 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ZipExtraData_ReadCheck_m97637932(__this, 2, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = __this->get__data_4();
		int32_t L_1 = __this->get__index_0();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t3397334013* L_4 = __this->get__data_4();
		int32_t L_5 = __this->get__index_0();
		NullCheck(L_4);
		int32_t L_6 = ((int32_t)((int32_t)L_5+(int32_t)1));
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)((int32_t)((int32_t)L_7<<(int32_t)8))));
		int32_t L_8 = __this->get__index_0();
		__this->set__index_0(((int32_t)((int32_t)L_8+(int32_t)2)));
		int32_t L_9 = V_0;
		return L_9;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadByte()
extern "C"  int32_t ZipExtraData_ReadByte_m1507753819 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = __this->get__index_0();
		ByteU5BU5D_t3397334013* L_1 = __this->get__data_4();
		NullCheck(L_1);
		if ((((int32_t)L_0) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_2 = __this->get__readValueStart_1();
		int32_t L_3 = __this->get__readValueLength_2();
		int32_t L_4 = __this->get__index_0();
		if ((((int32_t)((int32_t)((int32_t)L_2+(int32_t)L_3))) <= ((int32_t)L_4)))
		{
			goto IL_0043;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_5 = __this->get__data_4();
		int32_t L_6 = __this->get__index_0();
		NullCheck(L_5);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = L_8;
		int32_t L_9 = __this->get__index_0();
		__this->set__index_0(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0043:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Skip(System.Int32)
extern "C"  void ZipExtraData_Skip_m1738302941 (ZipExtraData_t3152287325 * __this, int32_t ___amount0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___amount0;
		ZipExtraData_ReadCheck_m97637932(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get__index_0();
		int32_t L_2 = ___amount0;
		__this->set__index_0(((int32_t)((int32_t)L_1+(int32_t)L_2)));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadCheck(System.Int32)
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3053020033;
extern Il2CppCodeGenString* _stringLiteral1846843426;
extern Il2CppCodeGenString* _stringLiteral3051461173;
extern const uint32_t ZipExtraData_ReadCheck_m97637932_MetadataUsageId;
extern "C"  void ZipExtraData_ReadCheck_m97637932 (ZipExtraData_t3152287325 * __this, int32_t ___length0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipExtraData_ReadCheck_m97637932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get__readValueStart_1();
		ByteU5BU5D_t3397334013* L_1 = __this->get__data_4();
		NullCheck(L_1);
		if ((((int32_t)L_0) > ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = __this->get__readValueStart_1();
		if ((((int32_t)L_2) >= ((int32_t)4)))
		{
			goto IL_0024;
		}
	}

IL_0019:
	{
		ZipException_t65220526 * L_3 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_3, _stringLiteral3053020033, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		int32_t L_4 = __this->get__index_0();
		int32_t L_5 = __this->get__readValueStart_1();
		int32_t L_6 = __this->get__readValueLength_2();
		int32_t L_7 = ___length0;
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))-(int32_t)L_7)))))
		{
			goto IL_0046;
		}
	}
	{
		ZipException_t65220526 * L_8 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_8, _stringLiteral1846843426, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0046:
	{
		int32_t L_9 = __this->get__index_0();
		int32_t L_10 = ___length0;
		if ((((int32_t)((int32_t)((int32_t)L_9+(int32_t)L_10))) >= ((int32_t)4)))
		{
			goto IL_005c;
		}
	}
	{
		ZipException_t65220526 * L_11 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_11, _stringLiteral3051461173, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_005c:
	{
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipExtraData::ReadShortInternal()
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1846843426;
extern const uint32_t ZipExtraData_ReadShortInternal_m2206901638_MetadataUsageId;
extern "C"  int32_t ZipExtraData_ReadShortInternal_m2206901638 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipExtraData_ReadShortInternal_m2206901638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get__index_0();
		ByteU5BU5D_t3397334013* L_1 = __this->get__data_4();
		NullCheck(L_1);
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))-(int32_t)2)))))
		{
			goto IL_001d;
		}
	}
	{
		ZipException_t65220526 * L_2 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_2, _stringLiteral1846843426, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001d:
	{
		ByteU5BU5D_t3397334013* L_3 = __this->get__data_4();
		int32_t L_4 = __this->get__index_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		ByteU5BU5D_t3397334013* L_7 = __this->get__data_4();
		int32_t L_8 = __this->get__index_0();
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)((int32_t)L_8+(int32_t)1));
		uint8_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_10<<(int32_t)8))));
		int32_t L_11 = __this->get__index_0();
		__this->set__index_0(((int32_t)((int32_t)L_11+(int32_t)2)));
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipExtraData::Dispose()
extern "C"  void ZipExtraData_Dispose_m3244432292 (ZipExtraData_t3152287325 * __this, const MethodInfo* method)
{
	{
		MemoryStream_t743994179 * L_0 = __this->get__newEntry_3();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		MemoryStream_t743994179 * L_1 = __this->get__newEntry_3();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(14 /* System.Void System.IO.Stream::Close() */, L_1);
	}

IL_0013:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::OnKeysRequired(System.String)
extern Il2CppClass* KeysRequiredEventArgs_t881954000_il2cpp_TypeInfo_var;
extern const uint32_t ZipFile_OnKeysRequired_m1772411083_MetadataUsageId;
extern "C"  void ZipFile_OnKeysRequired_m1772411083 (ZipFile_t1110175137 * __this, String_t* ___fileName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_OnKeysRequired_m1772411083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeysRequiredEventArgs_t881954000 * V_0 = NULL;
	{
		KeysRequiredEventHandler_t4231608811 * L_0 = __this->get_KeysRequired_0();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_1 = ___fileName0;
		ByteU5BU5D_t3397334013* L_2 = __this->get_key_9();
		KeysRequiredEventArgs_t881954000 * L_3 = (KeysRequiredEventArgs_t881954000 *)il2cpp_codegen_object_new(KeysRequiredEventArgs_t881954000_il2cpp_TypeInfo_var);
		KeysRequiredEventArgs__ctor_m554196931(L_3, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		KeysRequiredEventHandler_t4231608811 * L_4 = __this->get_KeysRequired_0();
		KeysRequiredEventArgs_t881954000 * L_5 = V_0;
		NullCheck(L_4);
		KeysRequiredEventHandler_Invoke_m4258674557(L_4, __this, L_5, /*hidden argument*/NULL);
		KeysRequiredEventArgs_t881954000 * L_6 = V_0;
		NullCheck(L_6);
		ByteU5BU5D_t3397334013* L_7 = KeysRequiredEventArgs_get_Key_m4244227656(L_6, /*hidden argument*/NULL);
		__this->set_key_9(L_7);
	}

IL_002e:
	{
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::get_HaveKeys()
extern "C"  bool ZipFile_get_HaveKeys_m109147906 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t3397334013* L_0 = __this->get_key_9();
		return (bool)((((int32_t)((((Il2CppObject*)(ByteU5BU5D_t3397334013*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::.ctor(System.String)
extern Il2CppClass* ZipEntryFactory_t3498628499_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2328218955;
extern const uint32_t ZipFile__ctor_m1988244607_MetadataUsageId;
extern "C"  void ZipFile__ctor_m1988244607 (ZipFile_t1110175137 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile__ctor_m1988244607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set_useZip64__10(2);
		__this->set_bufferSize__15(((int32_t)4096));
		ZipEntryFactory_t3498628499 * L_0 = (ZipEntryFactory_t3498628499 *)il2cpp_codegen_object_new(ZipEntryFactory_t3498628499_il2cpp_TypeInfo_var);
		ZipEntryFactory__ctor_m4255068507(L_0, /*hidden argument*/NULL);
		__this->set_updateEntryFactory__16(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___name0;
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral2328218955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0031:
	{
		String_t* L_3 = ___name0;
		__this->set_name__2(L_3);
		String_t* L_4 = ___name0;
		FileStream_t1695958676 * L_5 = File_Open_m290791476(NULL /*static, unused*/, L_4, 3, 1, 1, /*hidden argument*/NULL);
		__this->set_baseStream__5(L_5);
		__this->set_isStreamOwner_6((bool)1);
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		ZipFile_ReadEntries_m2392786121(__this, /*hidden argument*/NULL);
		goto IL_0060;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0056;
		throw e;
	}

CATCH_0056:
	{ // begin catch(System.Object)
		ZipFile_DisposeInternal_m2432789312(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
	} // end catch (depth: 1)

IL_0060:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Finalize()
extern "C"  void ZipFile_Finalize_m517470779 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(6 /* System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x10, FINALLY_0009);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0009;
	}

FINALLY_0009:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(9)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(9)
	{
		IL2CPP_JUMP_TBL(0x10, IL_0010)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0010:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Close()
extern "C"  void ZipFile_Close_m3129624767 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	{
		ZipFile_DisposeInternal_m2432789312(__this, (bool)1, /*hidden argument*/NULL);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::get_IsStreamOwner()
extern "C"  bool ZipFile_get_IsStreamOwner_m3802111051 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isStreamOwner_6();
		return L_0;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::get_Count()
extern "C"  int64_t ZipFile_get_Count_m4157201304 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	{
		ZipEntryU5BU5D_t2096538654* L_0 = __this->get_entries__8();
		NullCheck(L_0);
		return (((int64_t)((int64_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
	}
}
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipFile::get_EntryByIndex(System.Int32)
extern Il2CppClass* ZipEntry_t1764014695_il2cpp_TypeInfo_var;
extern const uint32_t ZipFile_get_EntryByIndex_m72693826_MetadataUsageId;
extern "C"  ZipEntry_t1764014695 * ZipFile_get_EntryByIndex_m72693826 (ZipFile_t1110175137 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_get_EntryByIndex_m72693826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ZipEntryU5BU5D_t2096538654* L_0 = __this->get_entries__8();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		ZipEntry_t1764014695 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Il2CppObject * L_4 = ZipEntry_Clone_m3547274961(L_3, /*hidden argument*/NULL);
		return ((ZipEntry_t1764014695 *)CastclassClass(L_4, ZipEntry_t1764014695_il2cpp_TypeInfo_var));
	}
}
// System.Collections.IEnumerator ICSharpCode.SharpZipLib.Zip.ZipFile::GetEnumerator()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipEntryEnumerator_t729644389_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3699698113;
extern const uint32_t ZipFile_GetEnumerator_m3615191729_MetadataUsageId;
extern "C"  Il2CppObject * ZipFile_GetEnumerator_m3615191729 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_GetEnumerator_m3615191729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isDisposed__1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, _stringLiteral3699698113, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		ZipEntryU5BU5D_t2096538654* L_2 = __this->get_entries__8();
		ZipEntryEnumerator_t729644389 * L_3 = (ZipEntryEnumerator_t729644389 *)il2cpp_codegen_object_new(ZipEntryEnumerator_t729644389_il2cpp_TypeInfo_var);
		ZipEntryEnumerator__ctor_m3611962882(L_3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile::FindEntry(System.String,System.Boolean)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3699698113;
extern const uint32_t ZipFile_FindEntry_m1754921127_MetadataUsageId;
extern "C"  int32_t ZipFile_FindEntry_m1754921127 (ZipFile_t1110175137 * __this, String_t* ___name0, bool ___ignoreCase1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_FindEntry_m1754921127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_isDisposed__1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, _stringLiteral3699698113, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		V_0 = 0;
		goto IL_0038;
	}

IL_0017:
	{
		String_t* L_2 = ___name0;
		ZipEntryU5BU5D_t2096538654* L_3 = __this->get_entries__8();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ZipEntry_t1764014695 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		String_t* L_7 = ZipEntry_get_Name_m3476240732(L_6, /*hidden argument*/NULL);
		bool L_8 = ___ignoreCase1;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_9 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_10 = String_Compare_m1847873744(NULL /*static, unused*/, L_2, L_7, L_8, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_11 = V_0;
		return L_11;
	}

IL_0034:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_13 = V_0;
		ZipEntryU5BU5D_t2096538654* L_14 = __this->get_entries__8();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		return (-1);
	}
}
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::GetInputStream(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1507681642;
extern Il2CppCodeGenString* _stringLiteral3699698113;
extern Il2CppCodeGenString* _stringLiteral942289666;
extern const uint32_t ZipFile_GetInputStream_m1781807622_MetadataUsageId;
extern "C"  Stream_t3255436806 * ZipFile_GetInputStream_m1781807622 (ZipFile_t1110175137 * __this, ZipEntry_t1764014695 * ___entry0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_GetInputStream_m1781807622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		ZipEntry_t1764014695 * L_0 = ___entry0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1507681642, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		bool L_2 = __this->get_isDisposed__1();
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_3 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_3, _stringLiteral3699698113, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0021:
	{
		ZipEntry_t1764014695 * L_4 = ___entry0;
		NullCheck(L_4);
		int64_t L_5 = ZipEntry_get_ZipFileIndex_m1800604384(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int64_t L_6 = V_0;
		if ((((int64_t)L_6) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0054;
		}
	}
	{
		int64_t L_7 = V_0;
		ZipEntryU5BU5D_t2096538654* L_8 = __this->get_entries__8();
		NullCheck(L_8);
		if ((((int64_t)L_7) >= ((int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))))))))
		{
			goto IL_0054;
		}
	}
	{
		ZipEntryU5BU5D_t2096538654* L_9 = __this->get_entries__8();
		int64_t L_10 = V_0;
		if ((int64_t)(L_10) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_9);
		intptr_t L_11 = (((intptr_t)L_10));
		ZipEntry_t1764014695 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		String_t* L_13 = ZipEntry_get_Name_m3476240732(L_12, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_14 = ___entry0;
		NullCheck(L_14);
		String_t* L_15 = ZipEntry_get_Name_m3476240732(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0073;
		}
	}

IL_0054:
	{
		ZipEntry_t1764014695 * L_17 = ___entry0;
		NullCheck(L_17);
		String_t* L_18 = ZipEntry_get_Name_m3476240732(L_17, /*hidden argument*/NULL);
		int32_t L_19 = ZipFile_FindEntry_m1754921127(__this, L_18, (bool)1, /*hidden argument*/NULL);
		V_0 = (((int64_t)((int64_t)L_19)));
		int64_t L_20 = V_0;
		if ((((int64_t)L_20) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0073;
		}
	}
	{
		ZipException_t65220526 * L_21 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_21, _stringLiteral942289666, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_0073:
	{
		int64_t L_22 = V_0;
		Stream_t3255436806 * L_23 = ZipFile_GetInputStream_m1202448107(__this, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::GetInputStream(System.Int64)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* PartialInputStream_t2358850295_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* Inflater_t3958302971_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterInputStream_t663799889_il2cpp_TypeInfo_var;
extern Il2CppClass* CompressionMethod_t1510270153_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3699698113;
extern Il2CppCodeGenString* _stringLiteral1852449909;
extern Il2CppCodeGenString* _stringLiteral3779234368;
extern const uint32_t ZipFile_GetInputStream_m1202448107_MetadataUsageId;
extern "C"  Stream_t3255436806 * ZipFile_GetInputStream_m1202448107 (ZipFile_t1110175137 * __this, int64_t ___entryIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_GetInputStream_m1202448107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	Stream_t3255436806 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_isDisposed__1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, _stringLiteral3699698113, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		ZipEntryU5BU5D_t2096538654* L_2 = __this->get_entries__8();
		int64_t L_3 = ___entryIndex0;
		if ((int64_t)(L_3) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_2);
		intptr_t L_4 = (((intptr_t)L_3));
		ZipEntry_t1764014695 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int64_t L_6 = ZipFile_LocateEntry_m246682085(__this, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		ZipEntryU5BU5D_t2096538654* L_7 = __this->get_entries__8();
		int64_t L_8 = ___entryIndex0;
		if ((int64_t)(L_8) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_7);
		intptr_t L_9 = (((intptr_t)L_8));
		ZipEntry_t1764014695 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		int32_t L_11 = ZipEntry_get_CompressionMethod_m2735584663(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		int64_t L_12 = V_0;
		ZipEntryU5BU5D_t2096538654* L_13 = __this->get_entries__8();
		int64_t L_14 = ___entryIndex0;
		if ((int64_t)(L_14) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_13);
		intptr_t L_15 = (((intptr_t)L_14));
		ZipEntry_t1764014695 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		int64_t L_17 = ZipEntry_get_CompressedSize_m1350691421(L_16, /*hidden argument*/NULL);
		PartialInputStream_t2358850295 * L_18 = (PartialInputStream_t2358850295 *)il2cpp_codegen_object_new(PartialInputStream_t2358850295_il2cpp_TypeInfo_var);
		PartialInputStream__ctor_m484094218(L_18, __this, L_12, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		ZipEntryU5BU5D_t2096538654* L_19 = __this->get_entries__8();
		int64_t L_20 = ___entryIndex0;
		if ((int64_t)(L_20) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_19);
		intptr_t L_21 = (((intptr_t)L_20));
		ZipEntry_t1764014695 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		bool L_23 = ZipEntry_get_IsCrypted_m2288425721(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0077;
		}
	}
	{
		Stream_t3255436806 * L_24 = V_2;
		ZipEntryU5BU5D_t2096538654* L_25 = __this->get_entries__8();
		int64_t L_26 = ___entryIndex0;
		if ((int64_t)(L_26) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_25);
		intptr_t L_27 = (((intptr_t)L_26));
		ZipEntry_t1764014695 * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		Stream_t3255436806 * L_29 = ZipFile_CreateAndInitDecryptionStream_m3860111983(__this, L_24, L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		Stream_t3255436806 * L_30 = V_2;
		if (L_30)
		{
			goto IL_0077;
		}
	}
	{
		ZipException_t65220526 * L_31 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_31, _stringLiteral1852449909, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_0077:
	{
		int32_t L_32 = V_1;
		V_3 = L_32;
		int32_t L_33 = V_3;
		if ((((int32_t)L_33) == ((int32_t)0)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_34 = V_3;
		if ((!(((uint32_t)L_34) == ((uint32_t)8))))
		{
			goto IL_0090;
		}
	}
	{
		Stream_t3255436806 * L_35 = V_2;
		Inflater_t3958302971 * L_36 = (Inflater_t3958302971 *)il2cpp_codegen_object_new(Inflater_t3958302971_il2cpp_TypeInfo_var);
		Inflater__ctor_m1334719096(L_36, (bool)1, /*hidden argument*/NULL);
		InflaterInputStream_t663799889 * L_37 = (InflaterInputStream_t663799889 *)il2cpp_codegen_object_new(InflaterInputStream_t663799889_il2cpp_TypeInfo_var);
		InflaterInputStream__ctor_m151851384(L_37, L_35, L_36, /*hidden argument*/NULL);
		V_2 = L_37;
		goto IL_00a6;
	}

IL_0090:
	{
		int32_t L_38 = V_1;
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(CompressionMethod_t1510270153_il2cpp_TypeInfo_var, &L_39);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3779234368, L_40, /*hidden argument*/NULL);
		ZipException_t65220526 * L_42 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_42, L_41, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_42);
	}

IL_00a6:
	{
		Stream_t3255436806 * L_43 = V_2;
		return L_43;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::TestArchive(System.Boolean)
extern "C"  bool ZipFile_TestArchive_m243402462 (ZipFile_t1110175137 * __this, bool ___testData0, const MethodInfo* method)
{
	{
		bool L_0 = ___testData0;
		bool L_1 = ZipFile_TestArchive_m727429463(__this, L_0, 0, (ZipTestResultHandler_t2034591904 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile::TestArchive(System.Boolean,ICSharpCode.SharpZipLib.Zip.TestStrategy,ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* TestStatus_t3549853348_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Crc32_t3420759743_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipHelperStream_t3454744209_il2cpp_TypeInfo_var;
extern Il2CppClass* DescriptorData_t4106407219_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3699698113;
extern Il2CppCodeGenString* _stringLiteral2837744509;
extern Il2CppCodeGenString* _stringLiteral1350619428;
extern const uint32_t ZipFile_TestArchive_m727429463_MetadataUsageId;
extern "C"  bool ZipFile_TestArchive_m727429463 (ZipFile_t1110175137 * __this, bool ___testData0, int32_t ___strategy1, ZipTestResultHandler_t2034591904 * ___resultHandler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_TestArchive_m727429463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TestStatus_t3549853348 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	ZipException_t65220526 * V_4 = NULL;
	Crc32_t3420759743 * V_5 = NULL;
	Stream_t3255436806 * V_6 = NULL;
	ByteU5BU5D_t3397334013* V_7 = NULL;
	int64_t V_8 = 0;
	int32_t V_9 = 0;
	ZipHelperStream_t3454744209 * V_10 = NULL;
	DescriptorData_t4106407219 * V_11 = NULL;
	Exception_t1927440687 * V_12 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B7_0 = 0;
	{
		bool L_0 = __this->get_isDisposed__1();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, _stringLiteral3699698113, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		TestStatus_t3549853348 * L_2 = (TestStatus_t3549853348 *)il2cpp_codegen_object_new(TestStatus_t3549853348_il2cpp_TypeInfo_var);
		TestStatus__ctor_m1152201749(L_2, __this, /*hidden argument*/NULL);
		V_0 = L_2;
		ZipTestResultHandler_t2034591904 * L_3 = ___resultHandler2;
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		ZipTestResultHandler_t2034591904 * L_4 = ___resultHandler2;
		TestStatus_t3549853348 * L_5 = V_0;
		NullCheck(L_4);
		ZipTestResultHandler_Invoke_m849185304(L_4, L_5, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_0025:
	{
		bool L_6 = ___testData0;
		if (L_6)
		{
			goto IL_002b;
		}
	}
	{
		G_B7_0 = 2;
		goto IL_002c;
	}

IL_002b:
	{
		G_B7_0 = 3;
	}

IL_002c:
	{
		V_1 = G_B7_0;
		V_2 = (bool)1;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			V_3 = 0;
			goto IL_01ff;
		}

IL_0036:
		{
			ZipTestResultHandler_t2034591904 * L_7 = ___resultHandler2;
			if (!L_7)
			{
				goto IL_0055;
			}
		}

IL_0039:
		{
			TestStatus_t3549853348 * L_8 = V_0;
			int32_t L_9 = V_3;
			ZipEntry_t1764014695 * L_10 = ZipFile_get_EntryByIndex_m72693826(__this, L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			TestStatus_SetEntry_m3439478977(L_8, L_10, /*hidden argument*/NULL);
			TestStatus_t3549853348 * L_11 = V_0;
			NullCheck(L_11);
			TestStatus_SetOperation_m1653784204(L_11, 1, /*hidden argument*/NULL);
			ZipTestResultHandler_t2034591904 * L_12 = ___resultHandler2;
			TestStatus_t3549853348 * L_13 = V_0;
			NullCheck(L_12);
			ZipTestResultHandler_Invoke_m849185304(L_12, L_13, (String_t*)NULL, /*hidden argument*/NULL);
		}

IL_0055:
		try
		{ // begin try (depth: 2)
			int32_t L_14 = V_3;
			ZipEntry_t1764014695 * L_15 = ZipFile_get_EntryByIndex_m72693826(__this, L_14, /*hidden argument*/NULL);
			int32_t L_16 = V_1;
			ZipFile_TestLocalHeader_m1468002544(__this, L_15, L_16, /*hidden argument*/NULL);
			goto IL_0090;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ZipException_t65220526_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0066;
			throw e;
		}

CATCH_0066:
		{ // begin catch(ICSharpCode.SharpZipLib.Zip.ZipException)
			{
				V_4 = ((ZipException_t65220526 *)__exception_local);
				TestStatus_t3549853348 * L_17 = V_0;
				NullCheck(L_17);
				TestStatus_AddError_m3731266275(L_17, /*hidden argument*/NULL);
				ZipTestResultHandler_t2034591904 * L_18 = ___resultHandler2;
				if (!L_18)
				{
					goto IL_0089;
				}
			}

IL_0071:
			{
				ZipTestResultHandler_t2034591904 * L_19 = ___resultHandler2;
				TestStatus_t3549853348 * L_20 = V_0;
				ZipException_t65220526 * L_21 = V_4;
				NullCheck(L_21);
				String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_21);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_23 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2837744509, L_22, /*hidden argument*/NULL);
				NullCheck(L_19);
				ZipTestResultHandler_Invoke_m849185304(L_19, L_20, L_23, /*hidden argument*/NULL);
			}

IL_0089:
			{
				int32_t L_24 = ___strategy1;
				if (L_24)
				{
					goto IL_008e;
				}
			}

IL_008c:
			{
				V_2 = (bool)0;
			}

IL_008e:
			{
				goto IL_0090;
			}
		} // end catch (depth: 2)

IL_0090:
		{
			bool L_25 = V_2;
			if (!L_25)
			{
				goto IL_01e9;
			}
		}

IL_0096:
		{
			bool L_26 = ___testData0;
			if (!L_26)
			{
				goto IL_01e9;
			}
		}

IL_009c:
		{
			int32_t L_27 = V_3;
			ZipEntry_t1764014695 * L_28 = ZipFile_get_EntryByIndex_m72693826(__this, L_27, /*hidden argument*/NULL);
			NullCheck(L_28);
			bool L_29 = ZipEntry_get_IsFile_m222504024(L_28, /*hidden argument*/NULL);
			if (!L_29)
			{
				goto IL_01e9;
			}
		}

IL_00ad:
		{
			ZipTestResultHandler_t2034591904 * L_30 = ___resultHandler2;
			if (!L_30)
			{
				goto IL_00bf;
			}
		}

IL_00b0:
		{
			TestStatus_t3549853348 * L_31 = V_0;
			NullCheck(L_31);
			TestStatus_SetOperation_m1653784204(L_31, 2, /*hidden argument*/NULL);
			ZipTestResultHandler_t2034591904 * L_32 = ___resultHandler2;
			TestStatus_t3549853348 * L_33 = V_0;
			NullCheck(L_32);
			ZipTestResultHandler_Invoke_m849185304(L_32, L_33, (String_t*)NULL, /*hidden argument*/NULL);
		}

IL_00bf:
		{
			Crc32_t3420759743 * L_34 = (Crc32_t3420759743 *)il2cpp_codegen_object_new(Crc32_t3420759743_il2cpp_TypeInfo_var);
			Crc32__ctor_m2818634674(L_34, /*hidden argument*/NULL);
			V_5 = L_34;
			int32_t L_35 = V_3;
			ZipEntry_t1764014695 * L_36 = ZipFile_get_EntryByIndex_m72693826(__this, L_35, /*hidden argument*/NULL);
			Stream_t3255436806 * L_37 = ZipFile_GetInputStream_m1781807622(__this, L_36, /*hidden argument*/NULL);
			V_6 = L_37;
		}

IL_00d5:
		try
		{ // begin try (depth: 2)
			{
				V_7 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096)));
				V_8 = (((int64_t)((int64_t)0)));
				goto IL_010e;
			}

IL_00e7:
			{
				Crc32_t3420759743 * L_38 = V_5;
				ByteU5BU5D_t3397334013* L_39 = V_7;
				int32_t L_40 = V_9;
				NullCheck(L_38);
				Crc32_Update_m3476396462(L_38, L_39, 0, L_40, /*hidden argument*/NULL);
				ZipTestResultHandler_t2034591904 * L_41 = ___resultHandler2;
				if (!L_41)
				{
					goto IL_010e;
				}
			}

IL_00f6:
			{
				int64_t L_42 = V_8;
				int32_t L_43 = V_9;
				V_8 = ((int64_t)((int64_t)L_42+(int64_t)(((int64_t)((int64_t)L_43)))));
				TestStatus_t3549853348 * L_44 = V_0;
				int64_t L_45 = V_8;
				NullCheck(L_44);
				TestStatus_SetBytesTested_m1639387106(L_44, L_45, /*hidden argument*/NULL);
				ZipTestResultHandler_t2034591904 * L_46 = ___resultHandler2;
				TestStatus_t3549853348 * L_47 = V_0;
				NullCheck(L_46);
				ZipTestResultHandler_Invoke_m849185304(L_46, L_47, (String_t*)NULL, /*hidden argument*/NULL);
			}

IL_010e:
			{
				Stream_t3255436806 * L_48 = V_6;
				ByteU5BU5D_t3397334013* L_49 = V_7;
				ByteU5BU5D_t3397334013* L_50 = V_7;
				NullCheck(L_50);
				NullCheck(L_48);
				int32_t L_51 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_48, L_49, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))));
				int32_t L_52 = L_51;
				V_9 = L_52;
				if ((((int32_t)L_52) > ((int32_t)0)))
				{
					goto IL_00e7;
				}
			}

IL_0122:
			{
				IL2CPP_LEAVE(0x130, FINALLY_0124);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0124;
		}

FINALLY_0124:
		{ // begin finally (depth: 2)
			{
				Stream_t3255436806 * L_53 = V_6;
				if (!L_53)
				{
					goto IL_012f;
				}
			}

IL_0128:
			{
				Stream_t3255436806 * L_54 = V_6;
				NullCheck(L_54);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_54);
			}

IL_012f:
			{
				IL2CPP_END_FINALLY(292)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(292)
		{
			IL2CPP_JUMP_TBL(0x130, IL_0130)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0130:
		{
			int32_t L_55 = V_3;
			ZipEntry_t1764014695 * L_56 = ZipFile_get_EntryByIndex_m72693826(__this, L_55, /*hidden argument*/NULL);
			NullCheck(L_56);
			int64_t L_57 = ZipEntry_get_Crc_m3987268705(L_56, /*hidden argument*/NULL);
			Crc32_t3420759743 * L_58 = V_5;
			NullCheck(L_58);
			int64_t L_59 = Crc32_get_Value_m1420497589(L_58, /*hidden argument*/NULL);
			if ((((int64_t)L_57) == ((int64_t)L_59)))
			{
				goto IL_015f;
			}
		}

IL_0145:
		{
			TestStatus_t3549853348 * L_60 = V_0;
			NullCheck(L_60);
			TestStatus_AddError_m3731266275(L_60, /*hidden argument*/NULL);
			ZipTestResultHandler_t2034591904 * L_61 = ___resultHandler2;
			if (!L_61)
			{
				goto IL_015a;
			}
		}

IL_014e:
		{
			ZipTestResultHandler_t2034591904 * L_62 = ___resultHandler2;
			TestStatus_t3549853348 * L_63 = V_0;
			NullCheck(L_62);
			ZipTestResultHandler_Invoke_m849185304(L_62, L_63, _stringLiteral1350619428, /*hidden argument*/NULL);
		}

IL_015a:
		{
			int32_t L_64 = ___strategy1;
			if (L_64)
			{
				goto IL_015f;
			}
		}

IL_015d:
		{
			V_2 = (bool)0;
		}

IL_015f:
		{
			int32_t L_65 = V_3;
			ZipEntry_t1764014695 * L_66 = ZipFile_get_EntryByIndex_m72693826(__this, L_65, /*hidden argument*/NULL);
			NullCheck(L_66);
			int32_t L_67 = ZipEntry_get_Flags_m1454638551(L_66, /*hidden argument*/NULL);
			if (!((int32_t)((int32_t)L_67&(int32_t)8)))
			{
				goto IL_01e9;
			}
		}

IL_016f:
		{
			Stream_t3255436806 * L_68 = __this->get_baseStream__5();
			ZipHelperStream_t3454744209 * L_69 = (ZipHelperStream_t3454744209 *)il2cpp_codegen_object_new(ZipHelperStream_t3454744209_il2cpp_TypeInfo_var);
			ZipHelperStream__ctor_m1126958192(L_69, L_68, /*hidden argument*/NULL);
			V_10 = L_69;
			DescriptorData_t4106407219 * L_70 = (DescriptorData_t4106407219 *)il2cpp_codegen_object_new(DescriptorData_t4106407219_il2cpp_TypeInfo_var);
			DescriptorData__ctor_m854587251(L_70, /*hidden argument*/NULL);
			V_11 = L_70;
			ZipHelperStream_t3454744209 * L_71 = V_10;
			int32_t L_72 = V_3;
			ZipEntry_t1764014695 * L_73 = ZipFile_get_EntryByIndex_m72693826(__this, L_72, /*hidden argument*/NULL);
			NullCheck(L_73);
			bool L_74 = ZipEntry_get_LocalHeaderRequiresZip64_m3409813355(L_73, /*hidden argument*/NULL);
			DescriptorData_t4106407219 * L_75 = V_11;
			NullCheck(L_71);
			ZipHelperStream_ReadDataDescriptor_m711352486(L_71, L_74, L_75, /*hidden argument*/NULL);
			int32_t L_76 = V_3;
			ZipEntry_t1764014695 * L_77 = ZipFile_get_EntryByIndex_m72693826(__this, L_76, /*hidden argument*/NULL);
			NullCheck(L_77);
			int64_t L_78 = ZipEntry_get_Crc_m3987268705(L_77, /*hidden argument*/NULL);
			DescriptorData_t4106407219 * L_79 = V_11;
			NullCheck(L_79);
			int64_t L_80 = DescriptorData_get_Crc_m1588606453(L_79, /*hidden argument*/NULL);
			if ((((int64_t)L_78) == ((int64_t)L_80)))
			{
				goto IL_01b3;
			}
		}

IL_01ad:
		{
			TestStatus_t3549853348 * L_81 = V_0;
			NullCheck(L_81);
			TestStatus_AddError_m3731266275(L_81, /*hidden argument*/NULL);
		}

IL_01b3:
		{
			int32_t L_82 = V_3;
			ZipEntry_t1764014695 * L_83 = ZipFile_get_EntryByIndex_m72693826(__this, L_82, /*hidden argument*/NULL);
			NullCheck(L_83);
			int64_t L_84 = ZipEntry_get_CompressedSize_m1350691421(L_83, /*hidden argument*/NULL);
			DescriptorData_t4106407219 * L_85 = V_11;
			NullCheck(L_85);
			int64_t L_86 = DescriptorData_get_CompressedSize_m3383523441(L_85, /*hidden argument*/NULL);
			if ((((int64_t)L_84) == ((int64_t)L_86)))
			{
				goto IL_01ce;
			}
		}

IL_01c8:
		{
			TestStatus_t3549853348 * L_87 = V_0;
			NullCheck(L_87);
			TestStatus_AddError_m3731266275(L_87, /*hidden argument*/NULL);
		}

IL_01ce:
		{
			int32_t L_88 = V_3;
			ZipEntry_t1764014695 * L_89 = ZipFile_get_EntryByIndex_m72693826(__this, L_88, /*hidden argument*/NULL);
			NullCheck(L_89);
			int64_t L_90 = ZipEntry_get_Size_m4101971814(L_89, /*hidden argument*/NULL);
			DescriptorData_t4106407219 * L_91 = V_11;
			NullCheck(L_91);
			int64_t L_92 = DescriptorData_get_Size_m1456873818(L_91, /*hidden argument*/NULL);
			if ((((int64_t)L_90) == ((int64_t)L_92)))
			{
				goto IL_01e9;
			}
		}

IL_01e3:
		{
			TestStatus_t3549853348 * L_93 = V_0;
			NullCheck(L_93);
			TestStatus_AddError_m3731266275(L_93, /*hidden argument*/NULL);
		}

IL_01e9:
		{
			ZipTestResultHandler_t2034591904 * L_94 = ___resultHandler2;
			if (!L_94)
			{
				goto IL_01fb;
			}
		}

IL_01ec:
		{
			TestStatus_t3549853348 * L_95 = V_0;
			NullCheck(L_95);
			TestStatus_SetOperation_m1653784204(L_95, 3, /*hidden argument*/NULL);
			ZipTestResultHandler_t2034591904 * L_96 = ___resultHandler2;
			TestStatus_t3549853348 * L_97 = V_0;
			NullCheck(L_96);
			ZipTestResultHandler_Invoke_m849185304(L_96, L_97, (String_t*)NULL, /*hidden argument*/NULL);
		}

IL_01fb:
		{
			int32_t L_98 = V_3;
			V_3 = ((int32_t)((int32_t)L_98+(int32_t)1));
		}

IL_01ff:
		{
			bool L_99 = V_2;
			if (!L_99)
			{
				goto IL_020f;
			}
		}

IL_0202:
		{
			int32_t L_100 = V_3;
			int64_t L_101 = ZipFile_get_Count_m4157201304(__this, /*hidden argument*/NULL);
			if ((((int64_t)(((int64_t)((int64_t)L_100)))) < ((int64_t)L_101)))
			{
				goto IL_0036;
			}
		}

IL_020f:
		{
			ZipTestResultHandler_t2034591904 * L_102 = ___resultHandler2;
			if (!L_102)
			{
				goto IL_0221;
			}
		}

IL_0212:
		{
			TestStatus_t3549853348 * L_103 = V_0;
			NullCheck(L_103);
			TestStatus_SetOperation_m1653784204(L_103, 4, /*hidden argument*/NULL);
			ZipTestResultHandler_t2034591904 * L_104 = ___resultHandler2;
			TestStatus_t3549853348 * L_105 = V_0;
			NullCheck(L_104);
			ZipTestResultHandler_Invoke_m849185304(L_104, L_105, (String_t*)NULL, /*hidden argument*/NULL);
		}

IL_0221:
		{
			goto IL_0248;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0223;
		throw e;
	}

CATCH_0223:
	{ // begin catch(System.Exception)
		{
			V_12 = ((Exception_t1927440687 *)__exception_local);
			TestStatus_t3549853348 * L_106 = V_0;
			NullCheck(L_106);
			TestStatus_AddError_m3731266275(L_106, /*hidden argument*/NULL);
			ZipTestResultHandler_t2034591904 * L_107 = ___resultHandler2;
			if (!L_107)
			{
				goto IL_0246;
			}
		}

IL_022e:
		{
			ZipTestResultHandler_t2034591904 * L_108 = ___resultHandler2;
			TestStatus_t3549853348 * L_109 = V_0;
			Exception_t1927440687 * L_110 = V_12;
			NullCheck(L_110);
			String_t* L_111 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_110);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_112 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2837744509, L_111, /*hidden argument*/NULL);
			NullCheck(L_108);
			ZipTestResultHandler_Invoke_m849185304(L_108, L_109, L_112, /*hidden argument*/NULL);
		}

IL_0246:
		{
			goto IL_0248;
		}
	} // end catch (depth: 1)

IL_0248:
	{
		ZipTestResultHandler_t2034591904 * L_113 = ___resultHandler2;
		if (!L_113)
		{
			goto IL_0261;
		}
	}
	{
		TestStatus_t3549853348 * L_114 = V_0;
		NullCheck(L_114);
		TestStatus_SetOperation_m1653784204(L_114, 5, /*hidden argument*/NULL);
		TestStatus_t3549853348 * L_115 = V_0;
		NullCheck(L_115);
		TestStatus_SetEntry_m3439478977(L_115, (ZipEntry_t1764014695 *)NULL, /*hidden argument*/NULL);
		ZipTestResultHandler_t2034591904 * L_116 = ___resultHandler2;
		TestStatus_t3549853348 * L_117 = V_0;
		NullCheck(L_116);
		ZipTestResultHandler_Invoke_m849185304(L_116, L_117, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_0261:
	{
		TestStatus_t3549853348 * L_118 = V_0;
		NullCheck(L_118);
		int32_t L_119 = TestStatus_get_ErrorCount_m3841239444(L_118, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_119) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::TestLocalHeader(ICSharpCode.SharpZipLib.Zip.ZipEntry,ICSharpCode.SharpZipLib.Zip.ZipFile/HeaderTest)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipExtraData_t3152287325_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipNameTransform_t3311483194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2152658261;
extern Il2CppCodeGenString* _stringLiteral1439864732;
extern Il2CppCodeGenString* _stringLiteral1568311647;
extern Il2CppCodeGenString* _stringLiteral866541751;
extern Il2CppCodeGenString* _stringLiteral776191010;
extern Il2CppCodeGenString* _stringLiteral1499311687;
extern Il2CppCodeGenString* _stringLiteral505251630;
extern Il2CppCodeGenString* _stringLiteral483312583;
extern Il2CppCodeGenString* _stringLiteral2651583308;
extern Il2CppCodeGenString* _stringLiteral683113518;
extern Il2CppCodeGenString* _stringLiteral2572668491;
extern Il2CppCodeGenString* _stringLiteral677175438;
extern Il2CppCodeGenString* _stringLiteral4288685474;
extern Il2CppCodeGenString* _stringLiteral1739775984;
extern Il2CppCodeGenString* _stringLiteral1715453759;
extern Il2CppCodeGenString* _stringLiteral2844446070;
extern Il2CppCodeGenString* _stringLiteral1590143261;
extern Il2CppCodeGenString* _stringLiteral29073443;
extern Il2CppCodeGenString* _stringLiteral1110220233;
extern Il2CppCodeGenString* _stringLiteral3712914545;
extern Il2CppCodeGenString* _stringLiteral1382843202;
extern Il2CppCodeGenString* _stringLiteral2506234759;
extern Il2CppCodeGenString* _stringLiteral1832358200;
extern Il2CppCodeGenString* _stringLiteral1542509384;
extern Il2CppCodeGenString* _stringLiteral2954404759;
extern Il2CppCodeGenString* _stringLiteral4008652648;
extern const uint32_t ZipFile_TestLocalHeader_m1468002544_MetadataUsageId;
extern "C"  int64_t ZipFile_TestLocalHeader_m1468002544 (ZipFile_t1110175137 * __this, ZipEntry_t1764014695 * ___entry0, int32_t ___tests1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_TestLocalHeader_m1468002544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int16_t V_2 = 0;
	int16_t V_3 = 0;
	int16_t V_4 = 0;
	int16_t V_5 = 0;
	int16_t V_6 = 0;
	uint32_t V_7 = 0;
	int64_t V_8 = 0;
	int64_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	ByteU5BU5D_t3397334013* V_12 = NULL;
	ByteU5BU5D_t3397334013* V_13 = NULL;
	ZipExtraData_t3152287325 * V_14 = NULL;
	String_t* V_15 = NULL;
	int32_t V_16 = 0;
	int64_t V_17 = 0;
	Stream_t3255436806 * V_18 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stream_t3255436806 * L_0 = __this->get_baseStream__5();
		Stream_t3255436806 * L_1 = L_0;
		V_18 = L_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___tests1;
			V_0 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_2&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			int32_t L_3 = ___tests1;
			V_1 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_3&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			Stream_t3255436806 * L_4 = __this->get_baseStream__5();
			int64_t L_5 = __this->get_offsetOfFirstEntry_7();
			ZipEntry_t1764014695 * L_6 = ___entry0;
			NullCheck(L_6);
			int64_t L_7 = ZipEntry_get_Offset_m1777266548(L_6, /*hidden argument*/NULL);
			NullCheck(L_4);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_4, ((int64_t)((int64_t)L_5+(int64_t)L_7)), 0);
			uint32_t L_8 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
			if ((((int32_t)L_8) == ((int32_t)((int32_t)67324752))))
			{
				goto IL_006b;
			}
		}

IL_0049:
		{
			int64_t L_9 = __this->get_offsetOfFirstEntry_7();
			ZipEntry_t1764014695 * L_10 = ___entry0;
			NullCheck(L_10);
			int64_t L_11 = ZipEntry_get_Offset_m1777266548(L_10, /*hidden argument*/NULL);
			int64_t L_12 = ((int64_t)((int64_t)L_9+(int64_t)L_11));
			Il2CppObject * L_13 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_12);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2152658261, L_13, /*hidden argument*/NULL);
			ZipException_t65220526 * L_15 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_15, L_14, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
		}

IL_006b:
		{
			uint16_t L_16 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
			V_2 = (((int16_t)((int16_t)L_16)));
			uint16_t L_17 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
			V_3 = (((int16_t)((int16_t)L_17)));
			uint16_t L_18 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
			V_4 = (((int16_t)((int16_t)L_18)));
			uint16_t L_19 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
			V_5 = (((int16_t)((int16_t)L_19)));
			uint16_t L_20 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
			V_6 = (((int16_t)((int16_t)L_20)));
			uint32_t L_21 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
			V_7 = L_21;
			uint32_t L_22 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
			V_8 = (((int64_t)((uint64_t)L_22)));
			uint32_t L_23 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
			V_9 = (((int64_t)((uint64_t)L_23)));
			uint16_t L_24 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
			V_10 = L_24;
			uint16_t L_25 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
			V_11 = L_25;
			int32_t L_26 = V_10;
			V_12 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_26));
			Stream_t3255436806 * L_27 = __this->get_baseStream__5();
			ByteU5BU5D_t3397334013* L_28 = V_12;
			StreamUtils_ReadFully_m1907144787(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
			int32_t L_29 = V_11;
			V_13 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_29));
			Stream_t3255436806 * L_30 = __this->get_baseStream__5();
			ByteU5BU5D_t3397334013* L_31 = V_13;
			StreamUtils_ReadFully_m1907144787(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
			ByteU5BU5D_t3397334013* L_32 = V_13;
			ZipExtraData_t3152287325 * L_33 = (ZipExtraData_t3152287325 *)il2cpp_codegen_object_new(ZipExtraData_t3152287325_il2cpp_TypeInfo_var);
			ZipExtraData__ctor_m221466928(L_33, L_32, /*hidden argument*/NULL);
			V_14 = L_33;
			ZipExtraData_t3152287325 * L_34 = V_14;
			NullCheck(L_34);
			bool L_35 = ZipExtraData_Find_m2992530013(L_34, 1, /*hidden argument*/NULL);
			if (!L_35)
			{
				goto IL_014c;
			}
		}

IL_00ff:
		{
			ZipExtraData_t3152287325 * L_36 = V_14;
			NullCheck(L_36);
			int64_t L_37 = ZipExtraData_ReadLong_m583809138(L_36, /*hidden argument*/NULL);
			V_9 = L_37;
			ZipExtraData_t3152287325 * L_38 = V_14;
			NullCheck(L_38);
			int64_t L_39 = ZipExtraData_ReadLong_m583809138(L_38, /*hidden argument*/NULL);
			V_8 = L_39;
			int16_t L_40 = V_3;
			if (!((int32_t)((int32_t)L_40&(int32_t)8)))
			{
				goto IL_0168;
			}
		}

IL_0116:
		{
			int64_t L_41 = V_9;
			if ((((int64_t)L_41) == ((int64_t)(((int64_t)((int64_t)(-1)))))))
			{
				goto IL_0131;
			}
		}

IL_011c:
		{
			int64_t L_42 = V_9;
			ZipEntry_t1764014695 * L_43 = ___entry0;
			NullCheck(L_43);
			int64_t L_44 = ZipEntry_get_Size_m4101971814(L_43, /*hidden argument*/NULL);
			if ((((int64_t)L_42) == ((int64_t)L_44)))
			{
				goto IL_0131;
			}
		}

IL_0126:
		{
			ZipException_t65220526 * L_45 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_45, _stringLiteral1439864732, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_45);
		}

IL_0131:
		{
			int64_t L_46 = V_8;
			if ((((int64_t)L_46) == ((int64_t)(((int64_t)((int64_t)(-1)))))))
			{
				goto IL_0168;
			}
		}

IL_0137:
		{
			int64_t L_47 = V_8;
			ZipEntry_t1764014695 * L_48 = ___entry0;
			NullCheck(L_48);
			int64_t L_49 = ZipEntry_get_CompressedSize_m1350691421(L_48, /*hidden argument*/NULL);
			if ((((int64_t)L_47) == ((int64_t)L_49)))
			{
				goto IL_0168;
			}
		}

IL_0141:
		{
			ZipException_t65220526 * L_50 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_50, _stringLiteral1568311647, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_50);
		}

IL_014c:
		{
			int16_t L_51 = V_2;
			if ((((int32_t)L_51) < ((int32_t)((int32_t)45))))
			{
				goto IL_0168;
			}
		}

IL_0151:
		{
			int64_t L_52 = V_9;
			if ((((int32_t)(((int32_t)((uint32_t)L_52)))) == ((int32_t)(-1))))
			{
				goto IL_015d;
			}
		}

IL_0157:
		{
			int64_t L_53 = V_8;
			if ((!(((uint32_t)(((int32_t)((uint32_t)L_53)))) == ((uint32_t)(-1)))))
			{
				goto IL_0168;
			}
		}

IL_015d:
		{
			ZipException_t65220526 * L_54 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_54, _stringLiteral866541751, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_54);
		}

IL_0168:
		{
			bool L_55 = V_1;
			if (!L_55)
			{
				goto IL_01bf;
			}
		}

IL_016b:
		{
			ZipEntry_t1764014695 * L_56 = ___entry0;
			NullCheck(L_56);
			bool L_57 = ZipEntry_get_IsFile_m222504024(L_56, /*hidden argument*/NULL);
			if (!L_57)
			{
				goto IL_01bf;
			}
		}

IL_0173:
		{
			ZipEntry_t1764014695 * L_58 = ___entry0;
			NullCheck(L_58);
			bool L_59 = ZipEntry_IsCompressionMethodSupported_m535555216(L_58, /*hidden argument*/NULL);
			if (L_59)
			{
				goto IL_0186;
			}
		}

IL_017b:
		{
			ZipException_t65220526 * L_60 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_60, _stringLiteral776191010, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_60);
		}

IL_0186:
		{
			int16_t L_61 = V_2;
			if ((((int32_t)L_61) > ((int32_t)((int32_t)51))))
			{
				goto IL_0195;
			}
		}

IL_018b:
		{
			int16_t L_62 = V_2;
			if ((((int32_t)L_62) <= ((int32_t)((int32_t)20))))
			{
				goto IL_01ab;
			}
		}

IL_0190:
		{
			int16_t L_63 = V_2;
			if ((((int32_t)L_63) >= ((int32_t)((int32_t)45))))
			{
				goto IL_01ab;
			}
		}

IL_0195:
		{
			int16_t L_64 = V_2;
			int16_t L_65 = L_64;
			Il2CppObject * L_66 = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &L_65);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_67 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1499311687, L_66, /*hidden argument*/NULL);
			ZipException_t65220526 * L_68 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_68, L_67, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_68);
		}

IL_01ab:
		{
			int16_t L_69 = V_3;
			if (!((int32_t)((int32_t)L_69&(int32_t)((int32_t)12384))))
			{
				goto IL_01bf;
			}
		}

IL_01b4:
		{
			ZipException_t65220526 * L_70 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_70, _stringLiteral505251630, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_70);
		}

IL_01bf:
		{
			bool L_71 = V_0;
			if (!L_71)
			{
				goto IL_03e8;
			}
		}

IL_01c5:
		{
			int16_t L_72 = V_2;
			if ((((int32_t)L_72) > ((int32_t)((int32_t)63))))
			{
				goto IL_0226;
			}
		}

IL_01ca:
		{
			int16_t L_73 = V_2;
			if ((((int32_t)L_73) == ((int32_t)((int32_t)10))))
			{
				goto IL_0226;
			}
		}

IL_01cf:
		{
			int16_t L_74 = V_2;
			if ((((int32_t)L_74) == ((int32_t)((int32_t)11))))
			{
				goto IL_0226;
			}
		}

IL_01d4:
		{
			int16_t L_75 = V_2;
			if ((((int32_t)L_75) == ((int32_t)((int32_t)20))))
			{
				goto IL_0226;
			}
		}

IL_01d9:
		{
			int16_t L_76 = V_2;
			if ((((int32_t)L_76) == ((int32_t)((int32_t)21))))
			{
				goto IL_0226;
			}
		}

IL_01de:
		{
			int16_t L_77 = V_2;
			if ((((int32_t)L_77) == ((int32_t)((int32_t)25))))
			{
				goto IL_0226;
			}
		}

IL_01e3:
		{
			int16_t L_78 = V_2;
			if ((((int32_t)L_78) == ((int32_t)((int32_t)27))))
			{
				goto IL_0226;
			}
		}

IL_01e8:
		{
			int16_t L_79 = V_2;
			if ((((int32_t)L_79) == ((int32_t)((int32_t)45))))
			{
				goto IL_0226;
			}
		}

IL_01ed:
		{
			int16_t L_80 = V_2;
			if ((((int32_t)L_80) == ((int32_t)((int32_t)46))))
			{
				goto IL_0226;
			}
		}

IL_01f2:
		{
			int16_t L_81 = V_2;
			if ((((int32_t)L_81) == ((int32_t)((int32_t)50))))
			{
				goto IL_0226;
			}
		}

IL_01f7:
		{
			int16_t L_82 = V_2;
			if ((((int32_t)L_82) == ((int32_t)((int32_t)51))))
			{
				goto IL_0226;
			}
		}

IL_01fc:
		{
			int16_t L_83 = V_2;
			if ((((int32_t)L_83) == ((int32_t)((int32_t)52))))
			{
				goto IL_0226;
			}
		}

IL_0201:
		{
			int16_t L_84 = V_2;
			if ((((int32_t)L_84) == ((int32_t)((int32_t)61))))
			{
				goto IL_0226;
			}
		}

IL_0206:
		{
			int16_t L_85 = V_2;
			if ((((int32_t)L_85) == ((int32_t)((int32_t)62))))
			{
				goto IL_0226;
			}
		}

IL_020b:
		{
			int16_t L_86 = V_2;
			if ((((int32_t)L_86) == ((int32_t)((int32_t)63))))
			{
				goto IL_0226;
			}
		}

IL_0210:
		{
			int16_t L_87 = V_2;
			int16_t L_88 = L_87;
			Il2CppObject * L_89 = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &L_88);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_90 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral483312583, L_89, /*hidden argument*/NULL);
			ZipException_t65220526 * L_91 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_91, L_90, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_91);
		}

IL_0226:
		{
			int16_t L_92 = V_3;
			if (!((int32_t)((int32_t)L_92&(int32_t)((int32_t)49168))))
			{
				goto IL_023a;
			}
		}

IL_022f:
		{
			ZipException_t65220526 * L_93 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_93, _stringLiteral2651583308, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_93);
		}

IL_023a:
		{
			int16_t L_94 = V_3;
			if (!((int32_t)((int32_t)L_94&(int32_t)1)))
			{
				goto IL_025a;
			}
		}

IL_023f:
		{
			int16_t L_95 = V_2;
			if ((((int32_t)L_95) >= ((int32_t)((int32_t)20))))
			{
				goto IL_025a;
			}
		}

IL_0244:
		{
			int16_t L_96 = V_2;
			int16_t L_97 = L_96;
			Il2CppObject * L_98 = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &L_97);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_99 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral683113518, L_98, /*hidden argument*/NULL);
			ZipException_t65220526 * L_100 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_100, L_99, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_100);
		}

IL_025a:
		{
			int16_t L_101 = V_3;
			if (!((int32_t)((int32_t)L_101&(int32_t)((int32_t)64))))
			{
				goto IL_028b;
			}
		}

IL_0260:
		{
			int16_t L_102 = V_3;
			if (((int32_t)((int32_t)L_102&(int32_t)1)))
			{
				goto IL_0270;
			}
		}

IL_0265:
		{
			ZipException_t65220526 * L_103 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_103, _stringLiteral2572668491, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_103);
		}

IL_0270:
		{
			int16_t L_104 = V_2;
			if ((((int32_t)L_104) >= ((int32_t)((int32_t)50))))
			{
				goto IL_028b;
			}
		}

IL_0275:
		{
			int16_t L_105 = V_2;
			int16_t L_106 = L_105;
			Il2CppObject * L_107 = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &L_106);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_108 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral683113518, L_107, /*hidden argument*/NULL);
			ZipException_t65220526 * L_109 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_109, L_108, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_109);
		}

IL_028b:
		{
			int16_t L_110 = V_3;
			if (!((int32_t)((int32_t)L_110&(int32_t)((int32_t)32))))
			{
				goto IL_02ac;
			}
		}

IL_0291:
		{
			int16_t L_111 = V_2;
			if ((((int32_t)L_111) >= ((int32_t)((int32_t)27))))
			{
				goto IL_02ac;
			}
		}

IL_0296:
		{
			int16_t L_112 = V_2;
			int16_t L_113 = L_112;
			Il2CppObject * L_114 = Box(Int16_t4041245914_il2cpp_TypeInfo_var, &L_113);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_115 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral677175438, L_114, /*hidden argument*/NULL);
			ZipException_t65220526 * L_116 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_116, L_115, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_116);
		}

IL_02ac:
		{
			int16_t L_117 = V_3;
			ZipEntry_t1764014695 * L_118 = ___entry0;
			NullCheck(L_118);
			int32_t L_119 = ZipEntry_get_Flags_m1454638551(L_118, /*hidden argument*/NULL);
			if ((((int32_t)L_117) == ((int32_t)L_119)))
			{
				goto IL_02c0;
			}
		}

IL_02b5:
		{
			ZipException_t65220526 * L_120 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_120, _stringLiteral4288685474, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_120);
		}

IL_02c0:
		{
			ZipEntry_t1764014695 * L_121 = ___entry0;
			NullCheck(L_121);
			int32_t L_122 = ZipEntry_get_CompressionMethod_m2735584663(L_121, /*hidden argument*/NULL);
			int16_t L_123 = V_4;
			if ((((int32_t)L_122) == ((int32_t)L_123)))
			{
				goto IL_02d5;
			}
		}

IL_02ca:
		{
			ZipException_t65220526 * L_124 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_124, _stringLiteral1739775984, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_124);
		}

IL_02d5:
		{
			ZipEntry_t1764014695 * L_125 = ___entry0;
			NullCheck(L_125);
			int32_t L_126 = ZipEntry_get_Version_m3445061640(L_125, /*hidden argument*/NULL);
			int16_t L_127 = V_2;
			if ((((int32_t)L_126) == ((int32_t)L_127)))
			{
				goto IL_02e9;
			}
		}

IL_02de:
		{
			ZipException_t65220526 * L_128 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_128, _stringLiteral1715453759, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_128);
		}

IL_02e9:
		{
			int16_t L_129 = V_3;
			if (!((int32_t)((int32_t)L_129&(int32_t)((int32_t)64))))
			{
				goto IL_02ff;
			}
		}

IL_02ef:
		{
			int16_t L_130 = V_2;
			if ((((int32_t)L_130) >= ((int32_t)((int32_t)62))))
			{
				goto IL_02ff;
			}
		}

IL_02f4:
		{
			ZipException_t65220526 * L_131 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_131, _stringLiteral2844446070, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_131);
		}

IL_02ff:
		{
			int16_t L_132 = V_3;
			if (!((int32_t)((int32_t)L_132&(int32_t)((int32_t)8192))))
			{
				goto IL_031b;
			}
		}

IL_0308:
		{
			int16_t L_133 = V_5;
			if (L_133)
			{
				goto IL_0310;
			}
		}

IL_030c:
		{
			int16_t L_134 = V_6;
			if (!L_134)
			{
				goto IL_031b;
			}
		}

IL_0310:
		{
			ZipException_t65220526 * L_135 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_135, _stringLiteral1590143261, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_135);
		}

IL_031b:
		{
			int16_t L_136 = V_3;
			if (((int32_t)((int32_t)L_136&(int32_t)8)))
			{
				goto IL_0336;
			}
		}

IL_0320:
		{
			uint32_t L_137 = V_7;
			ZipEntry_t1764014695 * L_138 = ___entry0;
			NullCheck(L_138);
			int64_t L_139 = ZipEntry_get_Crc_m3987268705(L_138, /*hidden argument*/NULL);
			if ((((int32_t)L_137) == ((int32_t)(((int32_t)((uint32_t)L_139))))))
			{
				goto IL_0336;
			}
		}

IL_032b:
		{
			ZipException_t65220526 * L_140 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_140, _stringLiteral29073443, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_140);
		}

IL_0336:
		{
			int64_t L_141 = V_9;
			if ((!(((uint64_t)L_141) == ((uint64_t)(((int64_t)((int64_t)0)))))))
			{
				goto IL_0351;
			}
		}

IL_033c:
		{
			int64_t L_142 = V_8;
			if ((!(((uint64_t)L_142) == ((uint64_t)(((int64_t)((int64_t)0)))))))
			{
				goto IL_0351;
			}
		}

IL_0342:
		{
			uint32_t L_143 = V_7;
			if (!L_143)
			{
				goto IL_0351;
			}
		}

IL_0346:
		{
			ZipException_t65220526 * L_144 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_144, _stringLiteral1110220233, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_144);
		}

IL_0351:
		{
			ZipEntry_t1764014695 * L_145 = ___entry0;
			NullCheck(L_145);
			String_t* L_146 = ZipEntry_get_Name_m3476240732(L_145, /*hidden argument*/NULL);
			NullCheck(L_146);
			int32_t L_147 = String_get_Length_m1606060069(L_146, /*hidden argument*/NULL);
			int32_t L_148 = V_10;
			if ((((int32_t)L_147) <= ((int32_t)L_148)))
			{
				goto IL_036b;
			}
		}

IL_0360:
		{
			ZipException_t65220526 * L_149 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_149, _stringLiteral3712914545, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_149);
		}

IL_036b:
		{
			int16_t L_150 = V_3;
			ByteU5BU5D_t3397334013* L_151 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
			String_t* L_152 = ZipConstants_ConvertToStringExt_m2429635105(NULL /*static, unused*/, L_150, L_151, /*hidden argument*/NULL);
			V_15 = L_152;
			String_t* L_153 = V_15;
			ZipEntry_t1764014695 * L_154 = ___entry0;
			NullCheck(L_154);
			String_t* L_155 = ZipEntry_get_Name_m3476240732(L_154, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_156 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_153, L_155, /*hidden argument*/NULL);
			if (!L_156)
			{
				goto IL_038f;
			}
		}

IL_0384:
		{
			ZipException_t65220526 * L_157 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_157, _stringLiteral1382843202, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_157);
		}

IL_038f:
		{
			ZipEntry_t1764014695 * L_158 = ___entry0;
			NullCheck(L_158);
			bool L_159 = ZipEntry_get_IsDirectory_m4084667369(L_158, /*hidden argument*/NULL);
			if (!L_159)
			{
				goto IL_03d3;
			}
		}

IL_0397:
		{
			int64_t L_160 = V_9;
			if ((((int64_t)L_160) <= ((int64_t)(((int64_t)((int64_t)0))))))
			{
				goto IL_03a8;
			}
		}

IL_039d:
		{
			ZipException_t65220526 * L_161 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_161, _stringLiteral2506234759, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_161);
		}

IL_03a8:
		{
			ZipEntry_t1764014695 * L_162 = ___entry0;
			NullCheck(L_162);
			bool L_163 = ZipEntry_get_IsCrypted_m2288425721(L_162, /*hidden argument*/NULL);
			if (!L_163)
			{
				goto IL_03c2;
			}
		}

IL_03b0:
		{
			int64_t L_164 = V_8;
			if ((((int64_t)L_164) <= ((int64_t)(((int64_t)((int64_t)((int32_t)14)))))))
			{
				goto IL_03d3;
			}
		}

IL_03b7:
		{
			ZipException_t65220526 * L_165 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_165, _stringLiteral1832358200, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_165);
		}

IL_03c2:
		{
			int64_t L_166 = V_8;
			if ((((int64_t)L_166) <= ((int64_t)(((int64_t)((int64_t)2))))))
			{
				goto IL_03d3;
			}
		}

IL_03c8:
		{
			ZipException_t65220526 * L_167 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_167, _stringLiteral1832358200, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_167);
		}

IL_03d3:
		{
			String_t* L_168 = V_15;
			IL2CPP_RUNTIME_CLASS_INIT(ZipNameTransform_t3311483194_il2cpp_TypeInfo_var);
			bool L_169 = ZipNameTransform_IsValidName_m4036463524(NULL /*static, unused*/, L_168, (bool)1, /*hidden argument*/NULL);
			if (L_169)
			{
				goto IL_03e8;
			}
		}

IL_03dd:
		{
			ZipException_t65220526 * L_170 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_170, _stringLiteral1542509384, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_170);
		}

IL_03e8:
		{
			int16_t L_171 = V_3;
			if (!((int32_t)((int32_t)L_171&(int32_t)8)))
			{
				goto IL_03f9;
			}
		}

IL_03ed:
		{
			int64_t L_172 = V_9;
			if ((((int64_t)L_172) > ((int64_t)(((int64_t)((int64_t)0))))))
			{
				goto IL_03f9;
			}
		}

IL_03f3:
		{
			int64_t L_173 = V_8;
			if ((((int64_t)L_173) <= ((int64_t)(((int64_t)((int64_t)0))))))
			{
				goto IL_045d;
			}
		}

IL_03f9:
		{
			int64_t L_174 = V_9;
			ZipEntry_t1764014695 * L_175 = ___entry0;
			NullCheck(L_175);
			int64_t L_176 = ZipEntry_get_Size_m4101971814(L_175, /*hidden argument*/NULL);
			if ((((int64_t)L_174) == ((int64_t)L_176)))
			{
				goto IL_0425;
			}
		}

IL_0403:
		{
			ZipEntry_t1764014695 * L_177 = ___entry0;
			NullCheck(L_177);
			int64_t L_178 = ZipEntry_get_Size_m4101971814(L_177, /*hidden argument*/NULL);
			int64_t L_179 = L_178;
			Il2CppObject * L_180 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_179);
			int64_t L_181 = V_9;
			int64_t L_182 = L_181;
			Il2CppObject * L_183 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_182);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_184 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2954404759, L_180, L_183, /*hidden argument*/NULL);
			ZipException_t65220526 * L_185 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_185, L_184, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_185);
		}

IL_0425:
		{
			int64_t L_186 = V_8;
			ZipEntry_t1764014695 * L_187 = ___entry0;
			NullCheck(L_187);
			int64_t L_188 = ZipEntry_get_CompressedSize_m1350691421(L_187, /*hidden argument*/NULL);
			if ((((int64_t)L_186) == ((int64_t)L_188)))
			{
				goto IL_045d;
			}
		}

IL_042f:
		{
			int64_t L_189 = V_8;
			if ((((int64_t)L_189) == ((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))))
			{
				goto IL_045d;
			}
		}

IL_0435:
		{
			int64_t L_190 = V_8;
			if ((((int64_t)L_190) == ((int64_t)(((int64_t)((int64_t)(-1)))))))
			{
				goto IL_045d;
			}
		}

IL_043b:
		{
			ZipEntry_t1764014695 * L_191 = ___entry0;
			NullCheck(L_191);
			int64_t L_192 = ZipEntry_get_CompressedSize_m1350691421(L_191, /*hidden argument*/NULL);
			int64_t L_193 = L_192;
			Il2CppObject * L_194 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_193);
			int64_t L_195 = V_8;
			int64_t L_196 = L_195;
			Il2CppObject * L_197 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_196);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_198 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4008652648, L_194, L_197, /*hidden argument*/NULL);
			ZipException_t65220526 * L_199 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
			ZipException__ctor_m2626676718(L_199, L_198, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_199);
		}

IL_045d:
		{
			int32_t L_200 = V_10;
			int32_t L_201 = V_11;
			V_16 = ((int32_t)((int32_t)L_200+(int32_t)L_201));
			int64_t L_202 = __this->get_offsetOfFirstEntry_7();
			ZipEntry_t1764014695 * L_203 = ___entry0;
			NullCheck(L_203);
			int64_t L_204 = ZipEntry_get_Offset_m1777266548(L_203, /*hidden argument*/NULL);
			int32_t L_205 = V_16;
			V_17 = ((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)L_202+(int64_t)L_204))+(int64_t)(((int64_t)((int64_t)((int32_t)30))))))+(int64_t)(((int64_t)((int64_t)L_205)))));
			IL2CPP_LEAVE(0x485, FINALLY_047d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_047d;
	}

FINALLY_047d:
	{ // begin finally (depth: 1)
		Stream_t3255436806 * L_206 = V_18;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_206, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(1149)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1149)
	{
		IL2CPP_JUMP_TBL(0x485, IL_0485)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0485:
	{
		int64_t L_207 = V_17;
		return L_207;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::PostUpdateCleanup()
extern Il2CppClass* IArchiveStorage_t26540254_il2cpp_TypeInfo_var;
extern const uint32_t ZipFile_PostUpdateCleanup_m2491866564_MetadataUsageId;
extern "C"  void ZipFile_PostUpdateCleanup_m2491866564 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_PostUpdateCleanup_m2491866564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_updateDataSource__14((Il2CppObject *)NULL);
		__this->set_updates__11((ArrayList_t4252133567 *)NULL);
		__this->set_updateIndex__12((Hashtable_t909839986 *)NULL);
		Il2CppObject * L_0 = __this->get_archiveStorage__13();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_archiveStorage__13();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void ICSharpCode.SharpZipLib.Zip.IArchiveStorage::Dispose() */, IArchiveStorage_t26540254_il2cpp_TypeInfo_var, L_1);
		__this->set_archiveStorage__13((Il2CppObject *)NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::System.IDisposable.Dispose()
extern "C"  void ZipFile_System_IDisposable_Dispose_m2544498532 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	{
		ZipFile_Close_m3129624767(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::DisposeInternal(System.Boolean)
extern Il2CppClass* ZipEntryU5BU5D_t2096538654_il2cpp_TypeInfo_var;
extern const uint32_t ZipFile_DisposeInternal_m2432789312_MetadataUsageId;
extern "C"  void ZipFile_DisposeInternal_m2432789312 (ZipFile_t1110175137 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_DisposeInternal_m2432789312_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stream_t3255436806 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_isDisposed__1();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed__1((bool)1);
		__this->set_entries__8(((ZipEntryU5BU5D_t2096538654*)SZArrayNew(ZipEntryU5BU5D_t2096538654_il2cpp_TypeInfo_var, (uint32_t)0)));
		bool L_1 = ZipFile_get_IsStreamOwner_m3802111051(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004c;
		}
	}
	{
		Stream_t3255436806 * L_2 = __this->get_baseStream__5();
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		Stream_t3255436806 * L_3 = __this->get_baseStream__5();
		Stream_t3255436806 * L_4 = L_3;
		V_0 = L_4;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		Stream_t3255436806 * L_5 = __this->get_baseStream__5();
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(14 /* System.Void System.IO.Stream::Close() */, L_5);
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Stream_t3255436806 * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004c:
	{
		ZipFile_PostUpdateCleanup_m2491866564(__this, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::Dispose(System.Boolean)
extern "C"  void ZipFile_Dispose_m3163465169 (ZipFile_t1110175137 * __this, bool ___disposing0, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing0;
		ZipFile_DisposeInternal_m2432789312(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt16 ICSharpCode.SharpZipLib.Zip.ZipFile::ReadLEUshort()
extern Il2CppClass* EndOfStreamException_t1711658693_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3399544824;
extern const uint32_t ZipFile_ReadLEUshort_m3166752384_MetadataUsageId;
extern "C"  uint16_t ZipFile_ReadLEUshort_m3166752384 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_ReadLEUshort_m3166752384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Stream_t3255436806 * L_0 = __this->get_baseStream__5();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.Stream::ReadByte() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		EndOfStreamException_t1711658693 * L_3 = (EndOfStreamException_t1711658693 *)il2cpp_codegen_object_new(EndOfStreamException_t1711658693_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m463559158(L_3, _stringLiteral3399544824, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001b:
	{
		Stream_t3255436806 * L_4 = __this->get_baseStream__5();
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.Stream::ReadByte() */, L_4);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		EndOfStreamException_t1711658693 * L_7 = (EndOfStreamException_t1711658693 *)il2cpp_codegen_object_new(EndOfStreamException_t1711658693_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m463559158(L_7, _stringLiteral3399544824, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0036:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		return (((int32_t)((uint16_t)((int32_t)((int32_t)(((int32_t)((uint16_t)L_8)))|(int32_t)(((int32_t)((uint16_t)((int32_t)((int32_t)L_9<<(int32_t)8))))))))));
	}
}
// System.UInt32 ICSharpCode.SharpZipLib.Zip.ZipFile::ReadLEUint()
extern "C"  uint32_t ZipFile_ReadLEUint_m2561149099 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		uint16_t L_1 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)16)))));
	}
}
// System.UInt64 ICSharpCode.SharpZipLib.Zip.ZipFile::ReadLEUlong()
extern "C"  uint64_t ZipFile_ReadLEUlong_m759240445 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		uint32_t L_1 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)(((int64_t)((uint64_t)L_0)))|(int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_1)))<<(int32_t)((int32_t)32)))));
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::LocateBlockWithSignature(System.Int32,System.Int64,System.Int32,System.Int32)
extern Il2CppClass* ZipHelperStream_t3454744209_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t ZipFile_LocateBlockWithSignature_m2724987100_MetadataUsageId;
extern "C"  int64_t ZipFile_LocateBlockWithSignature_m2724987100 (ZipFile_t1110175137 * __this, int32_t ___signature0, int64_t ___endLocation1, int32_t ___minimumBlockSize2, int32_t ___maximumVariableData3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_LocateBlockWithSignature_m2724987100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ZipHelperStream_t3454744209 * V_0 = NULL;
	int64_t V_1 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stream_t3255436806 * L_0 = __this->get_baseStream__5();
		ZipHelperStream_t3454744209 * L_1 = (ZipHelperStream_t3454744209 *)il2cpp_codegen_object_new(ZipHelperStream_t3454744209_il2cpp_TypeInfo_var);
		ZipHelperStream__ctor_m1126958192(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		ZipHelperStream_t3454744209 * L_2 = V_0;
		int32_t L_3 = ___signature0;
		int64_t L_4 = ___endLocation1;
		int32_t L_5 = ___minimumBlockSize2;
		int32_t L_6 = ___maximumVariableData3;
		NullCheck(L_2);
		int64_t L_7 = ZipHelperStream_LocateBlockWithSignature_m2981193978(L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_LEAVE(0x24, FINALLY_001a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001a;
	}

FINALLY_001a:
	{ // begin finally (depth: 1)
		{
			ZipHelperStream_t3454744209 * L_8 = V_0;
			if (!L_8)
			{
				goto IL_0023;
			}
		}

IL_001d:
		{
			ZipHelperStream_t3454744209 * L_9 = V_0;
			NullCheck(L_9);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_9);
		}

IL_0023:
		{
			IL2CPP_END_FINALLY(26)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(26)
	{
		IL2CPP_JUMP_TBL(0x24, IL_0024)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0024:
	{
		int64_t L_10 = V_1;
		return L_10;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::ReadEntries()
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipEntryU5BU5D_t2096538654_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipEntry_t1764014695_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2282852011;
extern Il2CppCodeGenString* _stringLiteral3947413142;
extern Il2CppCodeGenString* _stringLiteral2083583013;
extern Il2CppCodeGenString* _stringLiteral4144043195;
extern Il2CppCodeGenString* _stringLiteral2174382044;
extern Il2CppCodeGenString* _stringLiteral2037746553;
extern const uint32_t ZipFile_ReadEntries_m2392786121_MetadataUsageId;
extern "C"  void ZipFile_ReadEntries_m2392786121 (ZipFile_t1110175137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_ReadEntries_m2392786121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	uint16_t V_1 = 0;
	uint16_t V_2 = 0;
	uint64_t V_3 = 0;
	uint64_t V_4 = 0;
	uint64_t V_5 = 0;
	int64_t V_6 = 0;
	uint32_t V_7 = 0;
	ByteU5BU5D_t3397334013* V_8 = NULL;
	bool V_9 = false;
	int64_t V_10 = 0;
	uint64_t V_11 = 0;
	int64_t V_12 = 0;
	uint64_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	uint32_t V_18 = 0;
	uint32_t V_19 = 0;
	int64_t V_20 = 0;
	int64_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	int32_t V_24 = 0;
	uint32_t V_25 = 0;
	int64_t V_26 = 0;
	ByteU5BU5D_t3397334013* V_27 = NULL;
	String_t* V_28 = NULL;
	ZipEntry_t1764014695 * V_29 = NULL;
	ByteU5BU5D_t3397334013* V_30 = NULL;
	{
		Stream_t3255436806 * L_0 = __this->get_baseStream__5();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.IO.Stream::get_CanSeek() */, L_0);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ZipException_t65220526 * L_2 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_2, _stringLiteral2282852011, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		Stream_t3255436806 * L_3 = __this->get_baseStream__5();
		NullCheck(L_3);
		int64_t L_4 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_3);
		int64_t L_5 = ZipFile_LocateBlockWithSignature_m2724987100(__this, ((int32_t)101010256), L_4, ((int32_t)22), ((int32_t)65535), /*hidden argument*/NULL);
		V_0 = L_5;
		int64_t L_6 = V_0;
		if ((((int64_t)L_6) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0046;
		}
	}
	{
		ZipException_t65220526 * L_7 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_7, _stringLiteral3947413142, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0046:
	{
		uint16_t L_8 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		uint16_t L_9 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_2 = L_9;
		uint16_t L_10 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_3 = (((int64_t)((uint64_t)L_10)));
		uint16_t L_11 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_4 = (((int64_t)((uint64_t)L_11)));
		uint32_t L_12 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_5 = (((int64_t)((uint64_t)L_12)));
		uint32_t L_13 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_6 = (((int64_t)((uint64_t)L_13)));
		uint16_t L_14 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_7 = L_14;
		uint32_t L_15 = V_7;
		if ((!(((uint32_t)L_15) > ((uint32_t)0))))
		{
			goto IL_00aa;
		}
	}
	{
		uint32_t L_16 = V_7;
		V_8 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((uintptr_t)L_16))));
		Stream_t3255436806 * L_17 = __this->get_baseStream__5();
		ByteU5BU5D_t3397334013* L_18 = V_8;
		StreamUtils_ReadFully_m1907144787(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_19 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		String_t* L_20 = ZipConstants_ConvertToString_m314855573(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		__this->set_comment__3(L_20);
		goto IL_00b5;
	}

IL_00aa:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_comment__3(L_21);
	}

IL_00b5:
	{
		V_9 = (bool)0;
		uint16_t L_22 = V_1;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)65535))))
		{
			goto IL_00ea;
		}
	}
	{
		uint16_t L_23 = V_2;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)65535))))
		{
			goto IL_00ea;
		}
	}
	{
		uint64_t L_24 = V_3;
		if ((((int64_t)L_24) == ((int64_t)(((int64_t)((int64_t)((int32_t)65535)))))))
		{
			goto IL_00ea;
		}
	}
	{
		uint64_t L_25 = V_4;
		if ((((int64_t)L_25) == ((int64_t)(((int64_t)((int64_t)((int32_t)65535)))))))
		{
			goto IL_00ea;
		}
	}
	{
		uint64_t L_26 = V_5;
		if ((((int64_t)L_26) == ((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))))
		{
			goto IL_00ea;
		}
	}
	{
		int64_t L_27 = V_6;
		if ((!(((uint64_t)L_27) == ((uint64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))))
		{
			goto IL_01a1;
		}
	}

IL_00ea:
	{
		V_9 = (bool)1;
		int64_t L_28 = V_0;
		int64_t L_29 = ZipFile_LocateBlockWithSignature_m2724987100(__this, ((int32_t)117853008), L_28, 0, ((int32_t)4096), /*hidden argument*/NULL);
		V_10 = L_29;
		int64_t L_30 = V_10;
		if ((((int64_t)L_30) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0112;
		}
	}
	{
		ZipException_t65220526 * L_31 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_31, _stringLiteral2083583013, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_0112:
	{
		ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		uint64_t L_32 = ZipFile_ReadLEUlong_m759240445(__this, /*hidden argument*/NULL);
		V_11 = L_32;
		ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_33 = __this->get_baseStream__5();
		uint64_t L_34 = V_11;
		NullCheck(L_33);
		VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_33, L_34);
		uint32_t L_35 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_12 = (((int64_t)((uint64_t)L_35)));
		int64_t L_36 = V_12;
		if ((((int64_t)L_36) == ((int64_t)(((int64_t)((int64_t)((int32_t)101075792)))))))
		{
			goto IL_015f;
		}
	}
	{
		uint64_t L_37 = V_11;
		uint64_t L_38 = L_37;
		Il2CppObject * L_39 = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral4144043195, L_39, /*hidden argument*/NULL);
		ZipException_t65220526 * L_41 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_41, L_40, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_41);
	}

IL_015f:
	{
		ZipFile_ReadLEUlong_m759240445(__this, /*hidden argument*/NULL);
		ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		uint64_t L_42 = ZipFile_ReadLEUlong_m759240445(__this, /*hidden argument*/NULL);
		V_3 = L_42;
		uint64_t L_43 = ZipFile_ReadLEUlong_m759240445(__this, /*hidden argument*/NULL);
		V_4 = L_43;
		uint64_t L_44 = ZipFile_ReadLEUlong_m759240445(__this, /*hidden argument*/NULL);
		V_5 = L_44;
		uint64_t L_45 = ZipFile_ReadLEUlong_m759240445(__this, /*hidden argument*/NULL);
		V_6 = L_45;
	}

IL_01a1:
	{
		uint64_t L_46 = V_3;
		if ((uint64_t)(L_46) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		__this->set_entries__8(((ZipEntryU5BU5D_t2096538654*)SZArrayNew(ZipEntryU5BU5D_t2096538654_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_46)))));
		bool L_47 = V_9;
		if (L_47)
		{
			goto IL_01e2;
		}
	}
	{
		int64_t L_48 = V_6;
		int64_t L_49 = V_0;
		uint64_t L_50 = V_5;
		if ((((int64_t)L_48) >= ((int64_t)((int64_t)((int64_t)L_49-(int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)4)))+(int64_t)L_50)))))))
		{
			goto IL_01e2;
		}
	}
	{
		int64_t L_51 = V_0;
		uint64_t L_52 = V_5;
		int64_t L_53 = V_6;
		__this->set_offsetOfFirstEntry_7(((int64_t)((int64_t)L_51-(int64_t)((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)4)))+(int64_t)L_52))+(int64_t)L_53)))));
		int64_t L_54 = __this->get_offsetOfFirstEntry_7();
		if ((((int64_t)L_54) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_01e2;
		}
	}
	{
		ZipException_t65220526 * L_55 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_55, _stringLiteral2174382044, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_55);
	}

IL_01e2:
	{
		Stream_t3255436806 * L_56 = __this->get_baseStream__5();
		int64_t L_57 = __this->get_offsetOfFirstEntry_7();
		int64_t L_58 = V_6;
		NullCheck(L_56);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_56, ((int64_t)((int64_t)L_57+(int64_t)L_58)), 0);
		V_13 = (((int64_t)((int64_t)0)));
		goto IL_03ae;
	}

IL_0201:
	{
		uint32_t L_59 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_59) == ((int32_t)((int32_t)33639248))))
		{
			goto IL_0219;
		}
	}
	{
		ZipException_t65220526 * L_60 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_60, _stringLiteral2037746553, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_60);
	}

IL_0219:
	{
		uint16_t L_61 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_14 = L_61;
		uint16_t L_62 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_15 = L_62;
		uint16_t L_63 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_16 = L_63;
		uint16_t L_64 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_17 = L_64;
		uint32_t L_65 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_18 = L_65;
		uint32_t L_66 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_19 = L_66;
		uint32_t L_67 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_20 = (((int64_t)((uint64_t)L_67)));
		uint32_t L_68 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_21 = (((int64_t)((uint64_t)L_68)));
		uint16_t L_69 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_22 = L_69;
		uint16_t L_70 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_23 = L_70;
		uint16_t L_71 = ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		V_24 = L_71;
		ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		ZipFile_ReadLEUshort_m3166752384(__this, /*hidden argument*/NULL);
		uint32_t L_72 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_25 = L_72;
		uint32_t L_73 = ZipFile_ReadLEUint_m2561149099(__this, /*hidden argument*/NULL);
		V_26 = (((int64_t)((uint64_t)L_73)));
		int32_t L_74 = V_22;
		int32_t L_75 = V_24;
		int32_t L_76 = Math_Max_m2671311541(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		V_27 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_76));
		Stream_t3255436806 * L_77 = __this->get_baseStream__5();
		ByteU5BU5D_t3397334013* L_78 = V_27;
		int32_t L_79 = V_22;
		StreamUtils_ReadFully_m84790847(NULL /*static, unused*/, L_77, L_78, 0, L_79, /*hidden argument*/NULL);
		int32_t L_80 = V_16;
		ByteU5BU5D_t3397334013* L_81 = V_27;
		int32_t L_82 = V_22;
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		String_t* L_83 = ZipConstants_ConvertToStringExt_m2565864120(NULL /*static, unused*/, L_80, L_81, L_82, /*hidden argument*/NULL);
		V_28 = L_83;
		String_t* L_84 = V_28;
		int32_t L_85 = V_15;
		int32_t L_86 = V_14;
		int32_t L_87 = V_17;
		ZipEntry_t1764014695 * L_88 = (ZipEntry_t1764014695 *)il2cpp_codegen_object_new(ZipEntry_t1764014695_il2cpp_TypeInfo_var);
		ZipEntry__ctor_m509450494(L_88, L_84, L_85, L_86, L_87, /*hidden argument*/NULL);
		V_29 = L_88;
		ZipEntry_t1764014695 * L_89 = V_29;
		uint32_t L_90 = V_19;
		NullCheck(L_89);
		ZipEntry_set_Crc_m2947942594(L_89, ((int64_t)((int64_t)(((int64_t)((uint64_t)L_90)))&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_91 = V_29;
		int64_t L_92 = V_21;
		NullCheck(L_91);
		ZipEntry_set_Size_m1592469057(L_91, ((int64_t)((int64_t)L_92&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_93 = V_29;
		int64_t L_94 = V_20;
		NullCheck(L_93);
		ZipEntry_set_CompressedSize_m3769989458(L_93, ((int64_t)((int64_t)L_94&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_95 = V_29;
		int32_t L_96 = V_16;
		NullCheck(L_95);
		ZipEntry_set_Flags_m3605637110(L_95, L_96, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_97 = V_29;
		uint32_t L_98 = V_18;
		NullCheck(L_97);
		ZipEntry_set_DosTime_m3930736101(L_97, (((int64_t)((uint64_t)L_98))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_99 = V_29;
		uint64_t L_100 = V_13;
		NullCheck(L_99);
		ZipEntry_set_ZipFileIndex_m4266064271(L_99, L_100, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_101 = V_29;
		int64_t L_102 = V_26;
		NullCheck(L_101);
		ZipEntry_set_Offset_m1977872389(L_101, L_102, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_103 = V_29;
		uint32_t L_104 = V_25;
		NullCheck(L_103);
		ZipEntry_set_ExternalFileAttributes_m2744399311(L_103, L_104, /*hidden argument*/NULL);
		int32_t L_105 = V_16;
		if (((int32_t)((int32_t)L_105&(int32_t)8)))
		{
			goto IL_0336;
		}
	}
	{
		ZipEntry_t1764014695 * L_106 = V_29;
		uint32_t L_107 = V_19;
		NullCheck(L_106);
		ZipEntry_set_CryptoCheckValue_m3042546353(L_106, (((int32_t)((uint8_t)((int32_t)((uint32_t)L_107>>((int32_t)24)))))), /*hidden argument*/NULL);
		goto IL_0348;
	}

IL_0336:
	{
		ZipEntry_t1764014695 * L_108 = V_29;
		uint32_t L_109 = V_18;
		NullCheck(L_108);
		ZipEntry_set_CryptoCheckValue_m3042546353(L_108, (((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_109>>8))&(int32_t)((int32_t)255)))))), /*hidden argument*/NULL);
	}

IL_0348:
	{
		int32_t L_110 = V_23;
		if ((((int32_t)L_110) <= ((int32_t)0)))
		{
			goto IL_036c;
		}
	}
	{
		int32_t L_111 = V_23;
		V_30 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_111));
		Stream_t3255436806 * L_112 = __this->get_baseStream__5();
		ByteU5BU5D_t3397334013* L_113 = V_30;
		StreamUtils_ReadFully_m1907144787(NULL /*static, unused*/, L_112, L_113, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_114 = V_29;
		ByteU5BU5D_t3397334013* L_115 = V_30;
		NullCheck(L_114);
		ZipEntry_set_ExtraData_m2783375251(L_114, L_115, /*hidden argument*/NULL);
	}

IL_036c:
	{
		ZipEntry_t1764014695 * L_116 = V_29;
		NullCheck(L_116);
		ZipEntry_ProcessExtraData_m320672633(L_116, (bool)0, /*hidden argument*/NULL);
		int32_t L_117 = V_24;
		if ((((int32_t)L_117) <= ((int32_t)0)))
		{
			goto IL_039b;
		}
	}
	{
		Stream_t3255436806 * L_118 = __this->get_baseStream__5();
		ByteU5BU5D_t3397334013* L_119 = V_27;
		int32_t L_120 = V_24;
		StreamUtils_ReadFully_m84790847(NULL /*static, unused*/, L_118, L_119, 0, L_120, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_121 = V_29;
		int32_t L_122 = V_16;
		ByteU5BU5D_t3397334013* L_123 = V_27;
		int32_t L_124 = V_24;
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		String_t* L_125 = ZipConstants_ConvertToStringExt_m2565864120(NULL /*static, unused*/, L_122, L_123, L_124, /*hidden argument*/NULL);
		NullCheck(L_121);
		ZipEntry_set_Comment_m184649891(L_121, L_125, /*hidden argument*/NULL);
	}

IL_039b:
	{
		ZipEntryU5BU5D_t2096538654* L_126 = __this->get_entries__8();
		uint64_t L_127 = V_13;
		if ((uint64_t)(L_127) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		ZipEntry_t1764014695 * L_128 = V_29;
		NullCheck(L_126);
		ArrayElementTypeCheck (L_126, L_128);
		(L_126)->SetAt(static_cast<il2cpp_array_size_t>((((intptr_t)L_127))), (ZipEntry_t1764014695 *)L_128);
		uint64_t L_129 = V_13;
		V_13 = ((int64_t)((int64_t)L_129+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_03ae:
	{
		uint64_t L_130 = V_13;
		uint64_t L_131 = V_3;
		if ((!(((uint64_t)L_130) >= ((uint64_t)L_131))))
		{
			goto IL_0201;
		}
	}
	{
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile::LocateEntry(ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern "C"  int64_t ZipFile_LocateEntry_m246682085 (ZipFile_t1110175137 * __this, ZipEntry_t1764014695 * ___entry0, const MethodInfo* method)
{
	{
		ZipEntry_t1764014695 * L_0 = ___entry0;
		int64_t L_1 = ZipFile_TestLocalHeader_m1468002544(__this, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IO.Stream ICSharpCode.SharpZipLib.Zip.ZipFile::CreateAndInitDecryptionStream(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern Il2CppClass* PkzipClassicManaged_t4138271613_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* CryptoStream_t3531341937_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipAESTransform_t2148616552_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipAESStream_t1390211376_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral949045686;
extern Il2CppCodeGenString* _stringLiteral164334069;
extern Il2CppCodeGenString* _stringLiteral1149453795;
extern Il2CppCodeGenString* _stringLiteral2488849976;
extern Il2CppCodeGenString* _stringLiteral1796743210;
extern Il2CppCodeGenString* _stringLiteral894940021;
extern const uint32_t ZipFile_CreateAndInitDecryptionStream_m3860111983_MetadataUsageId;
extern "C"  Stream_t3255436806 * ZipFile_CreateAndInitDecryptionStream_m3860111983 (ZipFile_t1110175137 * __this, Stream_t3255436806 * ___baseStream0, ZipEntry_t1764014695 * ___entry1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_CreateAndInitDecryptionStream_m3860111983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CryptoStream_t3531341937 * V_0 = NULL;
	PkzipClassicManaged_t4138271613 * V_1 = NULL;
	int32_t V_2 = 0;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	int32_t V_4 = 0;
	ByteU5BU5D_t3397334013* V_5 = NULL;
	int32_t V_6 = 0;
	ZipAESTransform_t2148616552 * V_7 = NULL;
	ByteU5BU5D_t3397334013* V_8 = NULL;
	ObjectU5BU5D_t3614634134* V_9 = NULL;
	{
		V_0 = (CryptoStream_t3531341937 *)NULL;
		ZipEntry_t1764014695 * L_0 = ___entry1;
		NullCheck(L_0);
		int32_t L_1 = ZipEntry_get_Version_m3445061640(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)((int32_t)50))))
		{
			goto IL_0017;
		}
	}
	{
		ZipEntry_t1764014695 * L_2 = ___entry1;
		NullCheck(L_2);
		int32_t L_3 = ZipEntry_get_Flags_m1454638551(L_2, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_3&(int32_t)((int32_t)64))))
		{
			goto IL_005d;
		}
	}

IL_0017:
	{
		PkzipClassicManaged_t4138271613 * L_4 = (PkzipClassicManaged_t4138271613 *)il2cpp_codegen_object_new(PkzipClassicManaged_t4138271613_il2cpp_TypeInfo_var);
		PkzipClassicManaged__ctor_m1234613529(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		ZipEntry_t1764014695 * L_5 = ___entry1;
		NullCheck(L_5);
		String_t* L_6 = ZipEntry_get_Name_m3476240732(L_5, /*hidden argument*/NULL);
		ZipFile_OnKeysRequired_m1772411083(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = ZipFile_get_HaveKeys_m109147906(__this, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003c;
		}
	}
	{
		ZipException_t65220526 * L_8 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_8, _stringLiteral949045686, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003c:
	{
		Stream_t3255436806 * L_9 = ___baseStream0;
		PkzipClassicManaged_t4138271613 * L_10 = V_1;
		ByteU5BU5D_t3397334013* L_11 = __this->get_key_9();
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker2< Il2CppObject *, ByteU5BU5D_t3397334013*, ByteU5BU5D_t3397334013* >::Invoke(23 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateDecryptor(System.Byte[],System.Byte[]) */, L_10, L_11, (ByteU5BU5D_t3397334013*)(ByteU5BU5D_t3397334013*)NULL);
		CryptoStream_t3531341937 * L_13 = (CryptoStream_t3531341937 *)il2cpp_codegen_object_new(CryptoStream_t3531341937_il2cpp_TypeInfo_var);
		CryptoStream__ctor_m2578098817(L_13, L_9, L_12, 0, /*hidden argument*/NULL);
		V_0 = L_13;
		CryptoStream_t3531341937 * L_14 = V_0;
		ZipEntry_t1764014695 * L_15 = ___entry1;
		ZipFile_CheckClassicPassword_m3364579369(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_005d:
	{
		ZipEntry_t1764014695 * L_16 = ___entry1;
		NullCheck(L_16);
		int32_t L_17 = ZipEntry_get_Version_m3445061640(L_16, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)51)))))
		{
			goto IL_0145;
		}
	}
	{
		ZipEntry_t1764014695 * L_18 = ___entry1;
		NullCheck(L_18);
		String_t* L_19 = ZipEntry_get_Name_m3476240732(L_18, /*hidden argument*/NULL);
		ZipFile_OnKeysRequired_m1772411083(__this, L_19, /*hidden argument*/NULL);
		bool L_20 = ZipFile_get_HaveKeys_m109147906(__this, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0089;
		}
	}
	{
		ZipException_t65220526 * L_21 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_21, _stringLiteral164334069, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_0089:
	{
		ZipEntry_t1764014695 * L_22 = ___entry1;
		NullCheck(L_22);
		int32_t L_23 = ZipEntry_get_AESSaltLen_m1790361416(L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		int32_t L_24 = V_2;
		V_3 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_24));
		Stream_t3255436806 * L_25 = ___baseStream0;
		ByteU5BU5D_t3397334013* L_26 = V_3;
		int32_t L_27 = V_2;
		NullCheck(L_25);
		int32_t L_28 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_25, L_26, 0, L_27);
		V_4 = L_28;
		int32_t L_29 = V_4;
		int32_t L_30 = V_2;
		if ((((int32_t)L_29) == ((int32_t)L_30)))
		{
			goto IL_00e3;
		}
	}
	{
		V_9 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		ObjectU5BU5D_t3614634134* L_31 = V_9;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral1149453795);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1149453795);
		ObjectU5BU5D_t3614634134* L_32 = V_9;
		int32_t L_33 = V_2;
		int32_t L_34 = L_33;
		Il2CppObject * L_35 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_35);
		ObjectU5BU5D_t3614634134* L_36 = V_9;
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, _stringLiteral2488849976);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2488849976);
		ObjectU5BU5D_t3614634134* L_37 = V_9;
		int32_t L_38 = V_4;
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_40);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_40);
		ObjectU5BU5D_t3614634134* L_41 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m3881798623(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		ZipException_t65220526 * L_43 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_43, L_42, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_43);
	}

IL_00e3:
	{
		V_5 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)2));
		Stream_t3255436806 * L_44 = ___baseStream0;
		ByteU5BU5D_t3397334013* L_45 = V_5;
		NullCheck(L_44);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_44, L_45, 0, 2);
		ZipEntry_t1764014695 * L_46 = ___entry1;
		NullCheck(L_46);
		int32_t L_47 = ZipEntry_get_AESKeySize_m898847625(L_46, /*hidden argument*/NULL);
		V_6 = ((int32_t)((int32_t)L_47/(int32_t)8));
		String_t* L_48 = __this->get_rawPassword__4();
		ByteU5BU5D_t3397334013* L_49 = V_3;
		int32_t L_50 = V_6;
		ZipAESTransform_t2148616552 * L_51 = (ZipAESTransform_t2148616552 *)il2cpp_codegen_object_new(ZipAESTransform_t2148616552_il2cpp_TypeInfo_var);
		ZipAESTransform__ctor_m1528222691(L_51, L_48, L_49, L_50, (bool)0, /*hidden argument*/NULL);
		V_7 = L_51;
		ZipAESTransform_t2148616552 * L_52 = V_7;
		NullCheck(L_52);
		ByteU5BU5D_t3397334013* L_53 = ZipAESTransform_get_PwdVerifier_m1365083820(L_52, /*hidden argument*/NULL);
		V_8 = L_53;
		ByteU5BU5D_t3397334013* L_54 = V_8;
		NullCheck(L_54);
		int32_t L_55 = 0;
		uint8_t L_56 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		ByteU5BU5D_t3397334013* L_57 = V_5;
		NullCheck(L_57);
		int32_t L_58 = 0;
		uint8_t L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		if ((!(((uint32_t)L_56) == ((uint32_t)L_59))))
		{
			goto IL_012e;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_60 = V_8;
		NullCheck(L_60);
		int32_t L_61 = 1;
		uint8_t L_62 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		ByteU5BU5D_t3397334013* L_63 = V_5;
		NullCheck(L_63);
		int32_t L_64 = 1;
		uint8_t L_65 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		if ((((int32_t)L_62) == ((int32_t)L_65)))
		{
			goto IL_0139;
		}
	}

IL_012e:
	{
		Exception_t1927440687 * L_66 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_66, _stringLiteral1796743210, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_66);
	}

IL_0139:
	{
		Stream_t3255436806 * L_67 = ___baseStream0;
		ZipAESTransform_t2148616552 * L_68 = V_7;
		ZipAESStream_t1390211376 * L_69 = (ZipAESStream_t1390211376 *)il2cpp_codegen_object_new(ZipAESStream_t1390211376_il2cpp_TypeInfo_var);
		ZipAESStream__ctor_m2578024602(L_69, L_67, L_68, 0, /*hidden argument*/NULL);
		V_0 = L_69;
		goto IL_0150;
	}

IL_0145:
	{
		ZipException_t65220526 * L_70 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_70, _stringLiteral894940021, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_70);
	}

IL_0150:
	{
		CryptoStream_t3531341937 * L_71 = V_0;
		return L_71;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile::CheckClassicPassword(System.Security.Cryptography.CryptoStream,ICSharpCode.SharpZipLib.Zip.ZipEntry)
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4193664406;
extern const uint32_t ZipFile_CheckClassicPassword_m3364579369_MetadataUsageId;
extern "C"  void ZipFile_CheckClassicPassword_m3364579369 (Il2CppObject * __this /* static, unused */, CryptoStream_t3531341937 * ___classicCryptoStream0, ZipEntry_t1764014695 * ___entry1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipFile_CheckClassicPassword_m3364579369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)12)));
		CryptoStream_t3531341937 * L_0 = ___classicCryptoStream0;
		ByteU5BU5D_t3397334013* L_1 = V_0;
		StreamUtils_ReadFully_m1907144787(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = ((int32_t)11);
		uint8_t L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ZipEntry_t1764014695 * L_5 = ___entry1;
		NullCheck(L_5);
		uint8_t L_6 = ZipEntry_get_CryptoCheckValue_m2906695838(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_6)))
		{
			goto IL_0026;
		}
	}
	{
		ZipException_t65220526 * L_7 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_7, _stringLiteral4193664406, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0026:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void KeysRequiredEventHandler__ctor_m3611807203 (KeysRequiredEventHandler_t4231608811 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler::Invoke(System.Object,ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs)
extern "C"  void KeysRequiredEventHandler_Invoke_m4258674557 (KeysRequiredEventHandler_t4231608811 * __this, Il2CppObject * ___sender0, KeysRequiredEventArgs_t881954000 * ___e1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		KeysRequiredEventHandler_Invoke_m4258674557((KeysRequiredEventHandler_t4231608811 *)__this->get_prev_9(),___sender0, ___e1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___sender0, KeysRequiredEventArgs_t881954000 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___sender0, KeysRequiredEventArgs_t881954000 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, KeysRequiredEventArgs_t881954000 * ___e1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___sender0, ___e1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler::BeginInvoke(System.Object,ICSharpCode.SharpZipLib.Zip.KeysRequiredEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * KeysRequiredEventHandler_BeginInvoke_m2446664134 (KeysRequiredEventHandler_t4231608811 * __this, Il2CppObject * ___sender0, KeysRequiredEventArgs_t881954000 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = ___e1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/KeysRequiredEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void KeysRequiredEventHandler_EndInvoke_m3747947989 (KeysRequiredEventHandler_t4231608811 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::.ctor(ICSharpCode.SharpZipLib.Zip.ZipFile,System.Int64,System.Int64)
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern const uint32_t PartialInputStream__ctor_m484094218_MetadataUsageId;
extern "C"  void PartialInputStream__ctor_m484094218 (PartialInputStream_t2358850295 * __this, ZipFile_t1110175137 * ___zipFile0, int64_t ___start1, int64_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PartialInputStream__ctor_m484094218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream__ctor_m1531324023(__this, /*hidden argument*/NULL);
		int64_t L_0 = ___start1;
		__this->set_start__4(L_0);
		int64_t L_1 = ___length2;
		__this->set_length__5(L_1);
		ZipFile_t1110175137 * L_2 = ___zipFile0;
		__this->set_zipFile__2(L_2);
		ZipFile_t1110175137 * L_3 = __this->get_zipFile__2();
		NullCheck(L_3);
		Stream_t3255436806 * L_4 = L_3->get_baseStream__5();
		__this->set_baseStream__3(L_4);
		int64_t L_5 = ___start1;
		__this->set_readPos__6(L_5);
		int64_t L_6 = ___start1;
		int64_t L_7 = ___length2;
		__this->set_end__7(((int64_t)((int64_t)L_6+(int64_t)L_7)));
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::ReadByte()
extern "C"  int32_t PartialInputStream_ReadByte_m1845671945 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Stream_t3255436806 * V_1 = NULL;
	int64_t V_2 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int64_t L_0 = __this->get_readPos__6();
		int64_t L_1 = __this->get_end__7();
		if ((((int64_t)L_0) < ((int64_t)L_1)))
		{
			goto IL_0010;
		}
	}
	{
		return (-1);
	}

IL_0010:
	{
		Stream_t3255436806 * L_2 = __this->get_baseStream__3();
		Stream_t3255436806 * L_3 = L_2;
		V_1 = L_3;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		Stream_t3255436806 * L_4 = __this->get_baseStream__3();
		int64_t L_5 = __this->get_readPos__6();
		int64_t L_6 = L_5;
		V_2 = L_6;
		__this->set_readPos__6(((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)1))))));
		int64_t L_7 = V_2;
		NullCheck(L_4);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_4, L_7, 0);
		Stream_t3255436806 * L_8 = __this->get_baseStream__3();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.Stream::ReadByte() */, L_8);
		V_0 = L_9;
		IL2CPP_LEAVE(0x51, FINALLY_004a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		Stream_t3255436806 * L_10 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(74)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0051:
	{
		int32_t L_11 = V_0;
		return L_11;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Close()
extern "C"  void PartialInputStream_Close_m3033862713 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t PartialInputStream_Read_m1834487540 (PartialInputStream_t2358850295 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Stream_t3255436806 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stream_t3255436806 * L_0 = __this->get_baseStream__3();
		Stream_t3255436806 * L_1 = L_0;
		V_2 = L_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___count2;
			int64_t L_3 = __this->get_end__7();
			int64_t L_4 = __this->get_readPos__6();
			if ((((int64_t)(((int64_t)((int64_t)L_2)))) <= ((int64_t)((int64_t)((int64_t)L_3-(int64_t)L_4)))))
			{
				goto IL_0035;
			}
		}

IL_001e:
		{
			int64_t L_5 = __this->get_end__7();
			int64_t L_6 = __this->get_readPos__6();
			___count2 = (((int32_t)((int32_t)((int64_t)((int64_t)L_5-(int64_t)L_6)))));
			int32_t L_7 = ___count2;
			if (L_7)
			{
				goto IL_0035;
			}
		}

IL_0031:
		{
			V_1 = 0;
			IL2CPP_LEAVE(0x75, FINALLY_006e);
		}

IL_0035:
		{
			Stream_t3255436806 * L_8 = __this->get_baseStream__3();
			int64_t L_9 = __this->get_readPos__6();
			NullCheck(L_8);
			VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_8, L_9, 0);
			Stream_t3255436806 * L_10 = __this->get_baseStream__3();
			ByteU5BU5D_t3397334013* L_11 = ___buffer0;
			int32_t L_12 = ___offset1;
			int32_t L_13 = ___count2;
			NullCheck(L_10);
			int32_t L_14 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_10, L_11, L_12, L_13);
			V_0 = L_14;
			int32_t L_15 = V_0;
			if ((((int32_t)L_15) <= ((int32_t)0)))
			{
				goto IL_006a;
			}
		}

IL_005b:
		{
			int64_t L_16 = __this->get_readPos__6();
			int32_t L_17 = V_0;
			__this->set_readPos__6(((int64_t)((int64_t)L_16+(int64_t)(((int64_t)((int64_t)L_17))))));
		}

IL_006a:
		{
			int32_t L_18 = V_0;
			V_1 = L_18;
			IL2CPP_LEAVE(0x75, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		Stream_t3255436806 * L_19 = V_2;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(110)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0075:
	{
		int32_t L_20 = V_1;
		return L_20;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PartialInputStream_Write_m3544527789_MetadataUsageId;
extern "C"  void PartialInputStream_Write_m3544527789 (PartialInputStream_t2358850295 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PartialInputStream_Write_m3544527789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::SetLength(System.Int64)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t PartialInputStream_SetLength_m1264841115_MetadataUsageId;
extern "C"  void PartialInputStream_SetLength_m1264841115 (PartialInputStream_t2358850295 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PartialInputStream_SetLength_m1264841115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* IOException_t2458421087_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4086988149;
extern Il2CppCodeGenString* _stringLiteral2425127952;
extern const uint32_t PartialInputStream_Seek_m3915432243_MetadataUsageId;
extern "C"  int64_t PartialInputStream_Seek_m3915432243 (PartialInputStream_t2358850295 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PartialInputStream_Seek_m3915432243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int64_t L_0 = __this->get_readPos__6();
		V_0 = L_0;
		int32_t L_1 = ___origin1;
		V_1 = L_1;
		int32_t L_2 = V_1;
		switch (L_2)
		{
			case 0:
			{
				goto IL_001d;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_0033;
			}
		}
	}
	{
		goto IL_003c;
	}

IL_001d:
	{
		int64_t L_3 = __this->get_start__4();
		int64_t L_4 = ___offset0;
		V_0 = ((int64_t)((int64_t)L_3+(int64_t)L_4));
		goto IL_003c;
	}

IL_0028:
	{
		int64_t L_5 = __this->get_readPos__6();
		int64_t L_6 = ___offset0;
		V_0 = ((int64_t)((int64_t)L_5+(int64_t)L_6));
		goto IL_003c;
	}

IL_0033:
	{
		int64_t L_7 = __this->get_end__7();
		int64_t L_8 = ___offset0;
		V_0 = ((int64_t)((int64_t)L_7+(int64_t)L_8));
	}

IL_003c:
	{
		int64_t L_9 = V_0;
		int64_t L_10 = __this->get_start__4();
		if ((((int64_t)L_9) >= ((int64_t)L_10)))
		{
			goto IL_0050;
		}
	}
	{
		ArgumentException_t3259014390 * L_11 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_11, _stringLiteral4086988149, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0050:
	{
		int64_t L_12 = V_0;
		int64_t L_13 = __this->get_end__7();
		if ((((int64_t)L_12) < ((int64_t)L_13)))
		{
			goto IL_0064;
		}
	}
	{
		IOException_t2458421087 * L_14 = (IOException_t2458421087 *)il2cpp_codegen_object_new(IOException_t2458421087_il2cpp_TypeInfo_var);
		IOException__ctor_m3496190950(L_14, _stringLiteral2425127952, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0064:
	{
		int64_t L_15 = V_0;
		__this->set_readPos__6(L_15);
		int64_t L_16 = __this->get_readPos__6();
		return L_16;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::Flush()
extern "C"  void PartialInputStream_Flush_m3033698439 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_Position()
extern "C"  int64_t PartialInputStream_get_Position_m1446355428 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_readPos__6();
		int64_t L_1 = __this->get_start__4();
		return ((int64_t)((int64_t)L_0-(int64_t)L_1));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::set_Position(System.Int64)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4086988149;
extern Il2CppCodeGenString* _stringLiteral2425127952;
extern const uint32_t PartialInputStream_set_Position_m1280666373_MetadataUsageId;
extern "C"  void PartialInputStream_set_Position_m1280666373 (PartialInputStream_t2358850295 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PartialInputStream_set_Position_m1280666373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	{
		int64_t L_0 = __this->get_start__4();
		int64_t L_1 = ___value0;
		V_0 = ((int64_t)((int64_t)L_0+(int64_t)L_1));
		int64_t L_2 = V_0;
		int64_t L_3 = __this->get_start__4();
		if ((((int64_t)L_2) >= ((int64_t)L_3)))
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, _stringLiteral4086988149, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001d:
	{
		int64_t L_5 = V_0;
		int64_t L_6 = __this->get_end__7();
		if ((((int64_t)L_5) < ((int64_t)L_6)))
		{
			goto IL_0031;
		}
	}
	{
		InvalidOperationException_t721527559 * L_7 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_7, _stringLiteral2425127952, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0031:
	{
		int64_t L_8 = V_0;
		__this->set_readPos__6(L_8);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_Length()
extern "C"  int64_t PartialInputStream_get_Length_m2158634551 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_length__5();
		return L_0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_CanWrite()
extern "C"  bool PartialInputStream_get_CanWrite_m857292679 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_CanSeek()
extern "C"  bool PartialInputStream_get_CanSeek_m4178640278 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/PartialInputStream::get_CanRead()
extern "C"  bool PartialInputStream_get_CanRead_m3325009566 (PartialInputStream_t2358850295 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::.ctor(ICSharpCode.SharpZipLib.Zip.ZipEntry[])
extern "C"  void ZipEntryEnumerator__ctor_m3611962882 (ZipEntryEnumerator_t729644389 * __this, ZipEntryU5BU5D_t2096538654* ___entries0, const MethodInfo* method)
{
	{
		__this->set_index_1((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		ZipEntryU5BU5D_t2096538654* L_0 = ___entries0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Object ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::get_Current()
extern "C"  Il2CppObject * ZipEntryEnumerator_get_Current_m1850484400 (ZipEntryEnumerator_t729644389 * __this, const MethodInfo* method)
{
	{
		ZipEntryU5BU5D_t2096538654* L_0 = __this->get_array_0();
		int32_t L_1 = __this->get_index_1();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		ZipEntry_t1764014695 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		return L_3;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::Reset()
extern "C"  void ZipEntryEnumerator_Reset_m3477211656 (ZipEntryEnumerator_t729644389 * __this, const MethodInfo* method)
{
	{
		__this->set_index_1((-1));
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipFile/ZipEntryEnumerator::MoveNext()
extern "C"  bool ZipEntryEnumerator_MoveNext_m2366066547 (ZipEntryEnumerator_t729644389 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_index_1();
		int32_t L_1 = ((int32_t)((int32_t)L_0+(int32_t)1));
		V_0 = L_1;
		__this->set_index_1(L_1);
		int32_t L_2 = V_0;
		ZipEntryU5BU5D_t2096538654* L_3 = __this->get_array_0();
		NullCheck(L_3);
		return (bool)((((int32_t)L_2) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))))? 1 : 0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::.ctor(System.IO.Stream)
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern const uint32_t ZipHelperStream__ctor_m1126958192_MetadataUsageId;
extern "C"  void ZipHelperStream__ctor_m1126958192 (ZipHelperStream_t3454744209 * __this, Stream_t3255436806 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipHelperStream__ctor_m1126958192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream__ctor_m1531324023(__this, /*hidden argument*/NULL);
		Stream_t3255436806 * L_0 = ___stream0;
		__this->set_stream__3(L_0);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanRead()
extern "C"  bool ZipHelperStream_get_CanRead_m3775489630 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanRead() */, L_0);
		return L_1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanSeek()
extern "C"  bool ZipHelperStream_get_CanSeek_m2925672702 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.IO.Stream::get_CanSeek() */, L_0);
		return L_1;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Length()
extern "C"  int64_t ZipHelperStream_get_Length_m1497013625 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_0);
		return L_1;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_Position()
extern "C"  int64_t ZipHelperStream_get_Position_m2640572118 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::set_Position(System.Int64)
extern "C"  void ZipHelperStream_set_Position_m333091383 (ZipHelperStream_t3454744209 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		int64_t L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< int64_t >::Invoke(12 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_0, L_1);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipHelperStream::get_CanWrite()
extern "C"  bool ZipHelperStream_get_CanWrite_m1811081673 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.Stream::get_CanWrite() */, L_0);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Flush()
extern "C"  void ZipHelperStream_Flush_m2227625153 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(17 /* System.Void System.IO.Stream::Flush() */, L_0);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t ZipHelperStream_Seek_m3647472801 (ZipHelperStream_t3454744209 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		int64_t L_1 = ___offset0;
		int32_t L_2 = ___origin1;
		NullCheck(L_0);
		int64_t L_3 = VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::SetLength(System.Int64)
extern "C"  void ZipHelperStream_SetLength_m234583605 (ZipHelperStream_t3454744209 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		int64_t L_1 = ___value0;
		NullCheck(L_0);
		VirtActionInvoker1< int64_t >::Invoke(21 /* System.Void System.IO.Stream::SetLength(System.Int64) */, L_0, L_1);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZipHelperStream_Read_m2003101728 (ZipHelperStream_t3454744209 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		int32_t L_4 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, L_2, L_3);
		return L_4;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZipHelperStream_Write_m1129079083 (ZipHelperStream_t3454744209 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		ByteU5BU5D_t3397334013* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(22 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::Close()
extern "C"  void ZipHelperStream_Close_m3492237447 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	Stream_t3255436806 * V_0 = NULL;
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		V_0 = L_0;
		__this->set_stream__3((Stream_t3255436806 *)NULL);
		bool L_1 = __this->get_isOwner__2();
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		Stream_t3255436806 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		__this->set_isOwner__2((bool)0);
		Stream_t3255436806 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(14 /* System.Void System.IO.Stream::Close() */, L_3);
	}

IL_0026:
	{
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::LocateBlockWithSignature(System.Int32,System.Int64,System.Int32,System.Int32)
extern "C"  int64_t ZipHelperStream_LocateBlockWithSignature_m2981193978 (ZipHelperStream_t3454744209 * __this, int32_t ___signature0, int64_t ___endLocation1, int32_t ___minimumBlockSize2, int32_t ___maximumVariableData3, const MethodInfo* method)
{
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	{
		int64_t L_0 = ___endLocation1;
		int32_t L_1 = ___minimumBlockSize2;
		V_0 = ((int64_t)((int64_t)L_0-(int64_t)(((int64_t)((int64_t)L_1)))));
		int64_t L_2 = V_0;
		if ((((int64_t)L_2) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_000d;
		}
	}
	{
		return (((int64_t)((int64_t)(-1))));
	}

IL_000d:
	{
		int64_t L_3 = V_0;
		int32_t L_4 = ___maximumVariableData3;
		int64_t L_5 = Math_Max_m2783254082(NULL /*static, unused*/, ((int64_t)((int64_t)L_3-(int64_t)(((int64_t)((int64_t)L_4))))), (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		V_1 = L_5;
	}

IL_001a:
	{
		int64_t L_6 = V_0;
		int64_t L_7 = V_1;
		if ((((int64_t)L_6) >= ((int64_t)L_7)))
		{
			goto IL_0021;
		}
	}
	{
		return (((int64_t)((int64_t)(-1))));
	}

IL_0021:
	{
		int64_t L_8 = V_0;
		int64_t L_9 = L_8;
		V_0 = ((int64_t)((int64_t)L_9-(int64_t)(((int64_t)((int64_t)1)))));
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(20 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, __this, L_9, 0);
		int32_t L_10 = ZipHelperStream_ReadLEInt_m2263293059(__this, /*hidden argument*/NULL);
		int32_t L_11 = ___signature0;
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_001a;
		}
	}
	{
		int64_t L_12 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, __this);
		return L_12;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadLEShort()
extern Il2CppClass* EndOfStreamException_t1711658693_il2cpp_TypeInfo_var;
extern const uint32_t ZipHelperStream_ReadLEShort_m1696479828_MetadataUsageId;
extern "C"  int32_t ZipHelperStream_ReadLEShort_m1696479828 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipHelperStream_ReadLEShort_m1696479828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Stream_t3255436806 * L_0 = __this->get_stream__3();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.Stream::ReadByte() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		EndOfStreamException_t1711658693 * L_3 = (EndOfStreamException_t1711658693 *)il2cpp_codegen_object_new(EndOfStreamException_t1711658693_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2877696588(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0016:
	{
		Stream_t3255436806 * L_4 = __this->get_stream__3();
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.Stream::ReadByte() */, L_4);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		EndOfStreamException_t1711658693 * L_7 = (EndOfStreamException_t1711658693 *)il2cpp_codegen_object_new(EndOfStreamException_t1711658693_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2877696588(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		return ((int32_t)((int32_t)L_8|(int32_t)((int32_t)((int32_t)L_9<<(int32_t)8))));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadLEInt()
extern "C"  int32_t ZipHelperStream_ReadLEInt_m2263293059 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ZipHelperStream_ReadLEShort_m1696479828(__this, /*hidden argument*/NULL);
		int32_t L_1 = ZipHelperStream_ReadLEShort_m1696479828(__this, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)16)))));
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadLELong()
extern "C"  int64_t ZipHelperStream_ReadLELong_m4059713269 (ZipHelperStream_t3454744209 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ZipHelperStream_ReadLEInt_m2263293059(__this, /*hidden argument*/NULL);
		int32_t L_1 = ZipHelperStream_ReadLEInt_m2263293059(__this, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_0))))))|(int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipHelperStream::ReadDataDescriptor(System.Boolean,ICSharpCode.SharpZipLib.Zip.DescriptorData)
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3401622756;
extern const uint32_t ZipHelperStream_ReadDataDescriptor_m711352486_MetadataUsageId;
extern "C"  void ZipHelperStream_ReadDataDescriptor_m711352486 (ZipHelperStream_t3454744209 * __this, bool ___zip640, DescriptorData_t4106407219 * ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipHelperStream_ReadDataDescriptor_m711352486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ZipHelperStream_ReadLEInt_m2263293059(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)134695760))))
		{
			goto IL_001a;
		}
	}
	{
		ZipException_t65220526 * L_2 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_2, _stringLiteral3401622756, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001a:
	{
		DescriptorData_t4106407219 * L_3 = ___data1;
		int32_t L_4 = ZipHelperStream_ReadLEInt_m2263293059(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		DescriptorData_set_Crc_m438507950(L_3, (((int64_t)((int64_t)L_4))), /*hidden argument*/NULL);
		bool L_5 = ___zip640;
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		DescriptorData_t4106407219 * L_6 = ___data1;
		int64_t L_7 = ZipHelperStream_ReadLELong_m4059713269(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		DescriptorData_set_CompressedSize_m3229241118(L_6, L_7, /*hidden argument*/NULL);
		DescriptorData_t4106407219 * L_8 = ___data1;
		int64_t L_9 = ZipHelperStream_ReadLELong_m4059713269(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		DescriptorData_set_Size_m2586613261(L_8, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		DescriptorData_t4106407219 * L_10 = ___data1;
		int32_t L_11 = ZipHelperStream_ReadLEInt_m2263293059(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		DescriptorData_set_CompressedSize_m3229241118(L_10, (((int64_t)((int64_t)L_11))), /*hidden argument*/NULL);
		DescriptorData_t4106407219 * L_12 = ___data1;
		int32_t L_13 = ZipHelperStream_ReadLEInt_m2263293059(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		DescriptorData_set_Size_m2586613261(L_12, (((int64_t)((int64_t)L_13))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::.ctor(System.IO.Stream)
extern Il2CppClass* Crc32_t3420759743_il2cpp_TypeInfo_var;
extern Il2CppClass* Inflater_t3958302971_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadDataHandler_t1814344764_il2cpp_TypeInfo_var;
extern const MethodInfo* ZipInputStream_ReadingNotAvailable_m3606461204_MethodInfo_var;
extern const uint32_t ZipInputStream__ctor_m2152262024_MetadataUsageId;
extern "C"  void ZipInputStream__ctor_m2152262024 (ZipInputStream_t1369792737 * __this, Stream_t3255436806 * ___baseInputStream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream__ctor_m2152262024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Crc32_t3420759743 * L_0 = (Crc32_t3420759743 *)il2cpp_codegen_object_new(Crc32_t3420759743_il2cpp_TypeInfo_var);
		Crc32__ctor_m2818634674(L_0, /*hidden argument*/NULL);
		__this->set_crc_9(L_0);
		Stream_t3255436806 * L_1 = ___baseInputStream0;
		Inflater_t3958302971 * L_2 = (Inflater_t3958302971 *)il2cpp_codegen_object_new(Inflater_t3958302971_il2cpp_TypeInfo_var);
		Inflater__ctor_m1334719096(L_2, (bool)1, /*hidden argument*/NULL);
		InflaterInputStream__ctor_m151851384(__this, L_1, L_2, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)ZipInputStream_ReadingNotAvailable_m3606461204_MethodInfo_var);
		ReadDataHandler_t1814344764 * L_4 = (ReadDataHandler_t1814344764 *)il2cpp_codegen_object_new(ReadDataHandler_t1814344764_il2cpp_TypeInfo_var);
		ReadDataHandler__ctor_m1555072274(L_4, __this, L_3, /*hidden argument*/NULL);
		__this->set_internalReader_8(L_4);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_CanDecompressEntry()
extern "C"  bool ZipInputStream_get_CanDecompressEntry_m3676272695 (ZipInputStream_t1369792737 * __this, const MethodInfo* method)
{
	{
		ZipEntry_t1764014695 * L_0 = __this->get_entry_10();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		ZipEntry_t1764014695 * L_1 = __this->get_entry_10();
		NullCheck(L_1);
		bool L_2 = ZipEntry_get_CanDecompress_m2861654889(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0014:
	{
		return (bool)0;
	}
}
// ICSharpCode.SharpZipLib.Zip.ZipEntry ICSharpCode.SharpZipLib.Zip.ZipInputStream::GetNextEntry()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipEntry_t1764014695_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadDataHandler_t1814344764_il2cpp_TypeInfo_var;
extern const MethodInfo* ZipInputStream_InitialRead_m3367700748_MethodInfo_var;
extern const MethodInfo* ZipInputStream_ReadingNotSupported_m1168240437_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral417355544;
extern Il2CppCodeGenString* _stringLiteral1594760155;
extern Il2CppCodeGenString* _stringLiteral955844698;
extern Il2CppCodeGenString* _stringLiteral871227903;
extern const uint32_t ZipInputStream_GetNextEntry_m905824784_MetadataUsageId;
extern "C"  ZipEntry_t1764014695 * ZipInputStream_GetNextEntry_m905824784 (ZipInputStream_t1369792737 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_GetNextEntry_m905824784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int16_t V_1 = 0;
	uint32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	bool V_6 = false;
	ByteU5BU5D_t3397334013* V_7 = NULL;
	String_t* V_8 = NULL;
	ByteU5BU5D_t3397334013* V_9 = NULL;
	{
		Crc32_t3420759743 * L_0 = __this->get_crc_9();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, _stringLiteral417355544, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		ZipEntry_t1764014695 * L_2 = __this->get_entry_10();
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		ZipInputStream_CloseEntry_m3341626155(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		InflaterInputBuffer_t3211561891 * L_3 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_3);
		int32_t L_4 = InflaterInputBuffer_ReadLeInt_m3478398094(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)33639248))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)101010256))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)84233040))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)117853008))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)101075792)))))
		{
			goto IL_005d;
		}
	}

IL_0055:
	{
		VirtActionInvoker0::Invoke(14 /* System.Void System.IO.Stream::Close() */, __this);
		return (ZipEntry_t1764014695 *)NULL;
	}

IL_005d:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)808471376))))
		{
			goto IL_006d;
		}
	}
	{
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)134695760)))))
		{
			goto IL_0079;
		}
	}

IL_006d:
	{
		InflaterInputBuffer_t3211561891 * L_12 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_12);
		int32_t L_13 = InflaterInputBuffer_ReadLeInt_m3478398094(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_0079:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)67324752))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_15 = V_0;
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral955844698, L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1594760155, L_18, /*hidden argument*/NULL);
		ZipException_t65220526 * L_20 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_00a1:
	{
		InflaterInputBuffer_t3211561891 * L_21 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_21);
		int32_t L_22 = InflaterInputBuffer_ReadLeShort_m3204983647(L_21, /*hidden argument*/NULL);
		V_1 = (((int16_t)((int16_t)L_22)));
		InflaterInputBuffer_t3211561891 * L_23 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_23);
		int32_t L_24 = InflaterInputBuffer_ReadLeShort_m3204983647(L_23, /*hidden argument*/NULL);
		__this->set_flags_13(L_24);
		InflaterInputBuffer_t3211561891 * L_25 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_25);
		int32_t L_26 = InflaterInputBuffer_ReadLeShort_m3204983647(L_25, /*hidden argument*/NULL);
		__this->set_method_12(L_26);
		InflaterInputBuffer_t3211561891 * L_27 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_27);
		int32_t L_28 = InflaterInputBuffer_ReadLeInt_m3478398094(L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		InflaterInputBuffer_t3211561891 * L_29 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_29);
		int32_t L_30 = InflaterInputBuffer_ReadLeInt_m3478398094(L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		InflaterInputBuffer_t3211561891 * L_31 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_31);
		int32_t L_32 = InflaterInputBuffer_ReadLeInt_m3478398094(L_31, /*hidden argument*/NULL);
		((InflaterInputStream_t663799889 *)__this)->set_csize_5((((int64_t)((int64_t)L_32))));
		InflaterInputBuffer_t3211561891 * L_33 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_33);
		int32_t L_34 = InflaterInputBuffer_ReadLeInt_m3478398094(L_33, /*hidden argument*/NULL);
		__this->set_size_11((((int64_t)((int64_t)L_34))));
		InflaterInputBuffer_t3211561891 * L_35 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_35);
		int32_t L_36 = InflaterInputBuffer_ReadLeShort_m3204983647(L_35, /*hidden argument*/NULL);
		V_4 = L_36;
		InflaterInputBuffer_t3211561891 * L_37 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_37);
		int32_t L_38 = InflaterInputBuffer_ReadLeShort_m3204983647(L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		int32_t L_39 = __this->get_flags_13();
		V_6 = (bool)((((int32_t)((int32_t)((int32_t)L_39&(int32_t)1))) == ((int32_t)1))? 1 : 0);
		int32_t L_40 = V_4;
		V_7 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_40));
		InflaterInputBuffer_t3211561891 * L_41 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		ByteU5BU5D_t3397334013* L_42 = V_7;
		NullCheck(L_41);
		InflaterInputBuffer_ReadRawBuffer_m688284419(L_41, L_42, /*hidden argument*/NULL);
		int32_t L_43 = __this->get_flags_13();
		ByteU5BU5D_t3397334013* L_44 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		String_t* L_45 = ZipConstants_ConvertToStringExt_m2429635105(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		V_8 = L_45;
		String_t* L_46 = V_8;
		int16_t L_47 = V_1;
		ZipEntry_t1764014695 * L_48 = (ZipEntry_t1764014695 *)il2cpp_codegen_object_new(ZipEntry_t1764014695_il2cpp_TypeInfo_var);
		ZipEntry__ctor_m2202549470(L_48, L_46, L_47, /*hidden argument*/NULL);
		__this->set_entry_10(L_48);
		ZipEntry_t1764014695 * L_49 = __this->get_entry_10();
		int32_t L_50 = __this->get_flags_13();
		NullCheck(L_49);
		ZipEntry_set_Flags_m3605637110(L_49, L_50, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_51 = __this->get_entry_10();
		int32_t L_52 = __this->get_method_12();
		NullCheck(L_51);
		ZipEntry_set_CompressionMethod_m1964326120(L_51, L_52, /*hidden argument*/NULL);
		int32_t L_53 = __this->get_flags_13();
		if (((int32_t)((int32_t)L_53&(int32_t)8)))
		{
			goto IL_01e3;
		}
	}
	{
		ZipEntry_t1764014695 * L_54 = __this->get_entry_10();
		int32_t L_55 = V_3;
		NullCheck(L_54);
		ZipEntry_set_Crc_m2947942594(L_54, ((int64_t)((int64_t)(((int64_t)((int64_t)L_55)))&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_56 = __this->get_entry_10();
		int64_t L_57 = __this->get_size_11();
		NullCheck(L_56);
		ZipEntry_set_Size_m1592469057(L_56, ((int64_t)((int64_t)L_57&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_58 = __this->get_entry_10();
		int64_t L_59 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		NullCheck(L_58);
		ZipEntry_set_CompressedSize_m3769989458(L_58, ((int64_t)((int64_t)L_59&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_60 = __this->get_entry_10();
		int32_t L_61 = V_3;
		NullCheck(L_60);
		ZipEntry_set_CryptoCheckValue_m3042546353(L_60, (((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_61>>(int32_t)((int32_t)24)))&(int32_t)((int32_t)255)))))), /*hidden argument*/NULL);
		goto IL_0247;
	}

IL_01e3:
	{
		int32_t L_62 = V_3;
		if (!L_62)
		{
			goto IL_01f6;
		}
	}
	{
		ZipEntry_t1764014695 * L_63 = __this->get_entry_10();
		int32_t L_64 = V_3;
		NullCheck(L_63);
		ZipEntry_set_Crc_m2947942594(L_63, ((int64_t)((int64_t)(((int64_t)((int64_t)L_64)))&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
	}

IL_01f6:
	{
		int64_t L_65 = __this->get_size_11();
		if ((((int64_t)L_65) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0214;
		}
	}
	{
		ZipEntry_t1764014695 * L_66 = __this->get_entry_10();
		int64_t L_67 = __this->get_size_11();
		NullCheck(L_66);
		ZipEntry_set_Size_m1592469057(L_66, ((int64_t)((int64_t)L_67&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
	}

IL_0214:
	{
		int64_t L_68 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)L_68) == ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0232;
		}
	}
	{
		ZipEntry_t1764014695 * L_69 = __this->get_entry_10();
		int64_t L_70 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		NullCheck(L_69);
		ZipEntry_set_CompressedSize_m3769989458(L_69, ((int64_t)((int64_t)L_70&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
	}

IL_0232:
	{
		ZipEntry_t1764014695 * L_71 = __this->get_entry_10();
		uint32_t L_72 = V_2;
		NullCheck(L_71);
		ZipEntry_set_CryptoCheckValue_m3042546353(L_71, (((int32_t)((uint8_t)((int32_t)((int32_t)((int32_t)((uint32_t)L_72>>8))&(int32_t)((int32_t)255)))))), /*hidden argument*/NULL);
	}

IL_0247:
	{
		ZipEntry_t1764014695 * L_73 = __this->get_entry_10();
		uint32_t L_74 = V_2;
		NullCheck(L_73);
		ZipEntry_set_DosTime_m3930736101(L_73, (((int64_t)((uint64_t)L_74))), /*hidden argument*/NULL);
		int32_t L_75 = V_5;
		if ((((int32_t)L_75) <= ((int32_t)0)))
		{
			goto IL_027d;
		}
	}
	{
		int32_t L_76 = V_5;
		V_9 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_76));
		InflaterInputBuffer_t3211561891 * L_77 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		ByteU5BU5D_t3397334013* L_78 = V_9;
		NullCheck(L_77);
		InflaterInputBuffer_ReadRawBuffer_m688284419(L_77, L_78, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_79 = __this->get_entry_10();
		ByteU5BU5D_t3397334013* L_80 = V_9;
		NullCheck(L_79);
		ZipEntry_set_ExtraData_m2783375251(L_79, L_80, /*hidden argument*/NULL);
	}

IL_027d:
	{
		ZipEntry_t1764014695 * L_81 = __this->get_entry_10();
		NullCheck(L_81);
		ZipEntry_ProcessExtraData_m320672633(L_81, (bool)1, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_82 = __this->get_entry_10();
		NullCheck(L_82);
		int64_t L_83 = ZipEntry_get_CompressedSize_m1350691421(L_82, /*hidden argument*/NULL);
		if ((((int64_t)L_83) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_02a9;
		}
	}
	{
		ZipEntry_t1764014695 * L_84 = __this->get_entry_10();
		NullCheck(L_84);
		int64_t L_85 = ZipEntry_get_CompressedSize_m1350691421(L_84, /*hidden argument*/NULL);
		((InflaterInputStream_t663799889 *)__this)->set_csize_5(L_85);
	}

IL_02a9:
	{
		ZipEntry_t1764014695 * L_86 = __this->get_entry_10();
		NullCheck(L_86);
		int64_t L_87 = ZipEntry_get_Size_m4101971814(L_86, /*hidden argument*/NULL);
		if ((((int64_t)L_87) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_02c9;
		}
	}
	{
		ZipEntry_t1764014695 * L_88 = __this->get_entry_10();
		NullCheck(L_88);
		int64_t L_89 = ZipEntry_get_Size_m4101971814(L_88, /*hidden argument*/NULL);
		__this->set_size_11(L_89);
	}

IL_02c9:
	{
		int32_t L_90 = __this->get_method_12();
		if (L_90)
		{
			goto IL_0304;
		}
	}
	{
		bool L_91 = V_6;
		if (L_91)
		{
			goto IL_02e3;
		}
	}
	{
		int64_t L_92 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		int64_t L_93 = __this->get_size_11();
		if ((!(((uint64_t)L_92) == ((uint64_t)L_93))))
		{
			goto IL_02f9;
		}
	}

IL_02e3:
	{
		bool L_94 = V_6;
		if (!L_94)
		{
			goto IL_0304;
		}
	}
	{
		int64_t L_95 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		int64_t L_96 = __this->get_size_11();
		if ((((int64_t)((int64_t)((int64_t)L_95-(int64_t)(((int64_t)((int64_t)((int32_t)12))))))) == ((int64_t)L_96)))
		{
			goto IL_0304;
		}
	}

IL_02f9:
	{
		ZipException_t65220526 * L_97 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_97, _stringLiteral871227903, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_97);
	}

IL_0304:
	{
		ZipEntry_t1764014695 * L_98 = __this->get_entry_10();
		NullCheck(L_98);
		bool L_99 = ZipEntry_IsCompressionMethodSupported_m535555216(L_98, /*hidden argument*/NULL);
		if (!L_99)
		{
			goto IL_0325;
		}
	}
	{
		IntPtr_t L_100;
		L_100.set_m_value_0((void*)(void*)ZipInputStream_InitialRead_m3367700748_MethodInfo_var);
		ReadDataHandler_t1814344764 * L_101 = (ReadDataHandler_t1814344764 *)il2cpp_codegen_object_new(ReadDataHandler_t1814344764_il2cpp_TypeInfo_var);
		ReadDataHandler__ctor_m1555072274(L_101, __this, L_100, /*hidden argument*/NULL);
		__this->set_internalReader_8(L_101);
		goto IL_0337;
	}

IL_0325:
	{
		IntPtr_t L_102;
		L_102.set_m_value_0((void*)(void*)ZipInputStream_ReadingNotSupported_m1168240437_MethodInfo_var);
		ReadDataHandler_t1814344764 * L_103 = (ReadDataHandler_t1814344764 *)il2cpp_codegen_object_new(ReadDataHandler_t1814344764_il2cpp_TypeInfo_var);
		ReadDataHandler__ctor_m1555072274(L_103, __this, L_102, /*hidden argument*/NULL);
		__this->set_internalReader_8(L_103);
	}

IL_0337:
	{
		ZipEntry_t1764014695 * L_104 = __this->get_entry_10();
		return L_104;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadDataDescriptor()
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3401622756;
extern const uint32_t ZipInputStream_ReadDataDescriptor_m532537086_MetadataUsageId;
extern "C"  void ZipInputStream_ReadDataDescriptor_m532537086 (ZipInputStream_t1369792737 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_ReadDataDescriptor_m532537086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InflaterInputBuffer_t3211561891 * L_0 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_0);
		int32_t L_1 = InflaterInputBuffer_ReadLeInt_m3478398094(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)134695760))))
		{
			goto IL_001d;
		}
	}
	{
		ZipException_t65220526 * L_2 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_2, _stringLiteral3401622756, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001d:
	{
		ZipEntry_t1764014695 * L_3 = __this->get_entry_10();
		InflaterInputBuffer_t3211561891 * L_4 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_4);
		int32_t L_5 = InflaterInputBuffer_ReadLeInt_m3478398094(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		ZipEntry_set_Crc_m2947942594(L_3, ((int64_t)((int64_t)(((int64_t)((int64_t)L_5)))&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))), /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_6 = __this->get_entry_10();
		NullCheck(L_6);
		bool L_7 = ZipEntry_get_LocalHeaderRequiresZip64_m3409813355(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		InflaterInputBuffer_t3211561891 * L_8 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_8);
		int64_t L_9 = InflaterInputBuffer_ReadLeLong_m1849502234(L_8, /*hidden argument*/NULL);
		((InflaterInputStream_t663799889 *)__this)->set_csize_5(L_9);
		InflaterInputBuffer_t3211561891 * L_10 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_10);
		int64_t L_11 = InflaterInputBuffer_ReadLeLong_m1849502234(L_10, /*hidden argument*/NULL);
		__this->set_size_11(L_11);
		goto IL_008c;
	}

IL_0068:
	{
		InflaterInputBuffer_t3211561891 * L_12 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_12);
		int32_t L_13 = InflaterInputBuffer_ReadLeInt_m3478398094(L_12, /*hidden argument*/NULL);
		((InflaterInputStream_t663799889 *)__this)->set_csize_5((((int64_t)((int64_t)L_13))));
		InflaterInputBuffer_t3211561891 * L_14 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_14);
		int32_t L_15 = InflaterInputBuffer_ReadLeInt_m3478398094(L_14, /*hidden argument*/NULL);
		__this->set_size_11((((int64_t)((int64_t)L_15))));
	}

IL_008c:
	{
		ZipEntry_t1764014695 * L_16 = __this->get_entry_10();
		int64_t L_17 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		NullCheck(L_16);
		ZipEntry_set_CompressedSize_m3769989458(L_16, L_17, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_18 = __this->get_entry_10();
		int64_t L_19 = __this->get_size_11();
		NullCheck(L_18);
		ZipEntry_set_Size_m1592469057(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CompleteCloseEntry(System.Boolean)
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1350619428;
extern const uint32_t ZipInputStream_CompleteCloseEntry_m1755215957_MetadataUsageId;
extern "C"  void ZipInputStream_CompleteCloseEntry_m1755215957 (ZipInputStream_t1369792737 * __this, bool ___testCrc0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_CompleteCloseEntry_m1755215957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InflaterInputStream_StopDecrypting_m3004313053(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_flags_13();
		if (!((int32_t)((int32_t)L_0&(int32_t)8)))
		{
			goto IL_0016;
		}
	}
	{
		ZipInputStream_ReadDataDescriptor_m532537086(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		__this->set_size_11((((int64_t)((int64_t)0))));
		bool L_1 = ___testCrc0;
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		Crc32_t3420759743 * L_2 = __this->get_crc_9();
		NullCheck(L_2);
		int64_t L_3 = Crc32_get_Value_m1420497589(L_2, /*hidden argument*/NULL);
		ZipEntry_t1764014695 * L_4 = __this->get_entry_10();
		NullCheck(L_4);
		int64_t L_5 = ZipEntry_get_Crc_m3987268705(L_4, /*hidden argument*/NULL);
		if ((((int64_t)((int64_t)((int64_t)L_3&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))))) == ((int64_t)L_5)))
		{
			goto IL_0056;
		}
	}
	{
		ZipEntry_t1764014695 * L_6 = __this->get_entry_10();
		NullCheck(L_6);
		int64_t L_7 = ZipEntry_get_Crc_m3987268705(L_6, /*hidden argument*/NULL);
		if ((((int64_t)L_7) == ((int64_t)(((int64_t)((int64_t)(-1)))))))
		{
			goto IL_0056;
		}
	}
	{
		ZipException_t65220526 * L_8 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_8, _stringLiteral1350619428, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0056:
	{
		Crc32_t3420759743 * L_9 = __this->get_crc_9();
		NullCheck(L_9);
		Crc32_Reset_m2645532975(L_9, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_method_12();
		if ((!(((uint32_t)L_10) == ((uint32_t)8))))
		{
			goto IL_0075;
		}
	}
	{
		Inflater_t3958302971 * L_11 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_11);
		Inflater_Reset_m3735238578(L_11, /*hidden argument*/NULL);
	}

IL_0075:
	{
		__this->set_entry_10((ZipEntry_t1764014695 *)NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::CloseEntry()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2816927106;
extern Il2CppCodeGenString* _stringLiteral2084128162;
extern const uint32_t ZipInputStream_CloseEntry_m3341626155_MetadataUsageId;
extern "C"  void ZipInputStream_CloseEntry_m3341626155 (ZipInputStream_t1369792737 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_CloseEntry_m3341626155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int64_t V_1 = 0;
	{
		Crc32_t3420759743 * L_0 = __this->get_crc_9();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, _stringLiteral2816927106, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		ZipEntry_t1764014695 * L_2 = __this->get_entry_10();
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		int32_t L_3 = __this->get_method_12();
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_4 = __this->get_flags_13();
		if (!((int32_t)((int32_t)L_4&(int32_t)8)))
		{
			goto IL_0049;
		}
	}
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096)));
	}

IL_003a:
	{
		ByteU5BU5D_t3397334013* L_5 = V_0;
		ByteU5BU5D_t3397334013* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, __this, L_5, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))));
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		return;
	}

IL_0049:
	{
		int64_t L_8 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		Inflater_t3958302971 * L_9 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_9);
		int64_t L_10 = Inflater_get_TotalIn_m2049506924(L_9, /*hidden argument*/NULL);
		((InflaterInputStream_t663799889 *)__this)->set_csize_5(((int64_t)((int64_t)L_8-(int64_t)L_10)));
		InflaterInputBuffer_t3211561891 * L_11 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		InflaterInputBuffer_t3211561891 * L_12 = L_11;
		NullCheck(L_12);
		int32_t L_13 = InflaterInputBuffer_get_Available_m2874517854(L_12, /*hidden argument*/NULL);
		Inflater_t3958302971 * L_14 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_14);
		int32_t L_15 = Inflater_get_RemainingInput_m2370068744(L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		InflaterInputBuffer_set_Available_m4062003677(L_12, ((int32_t)((int32_t)L_13+(int32_t)L_15)), /*hidden argument*/NULL);
	}

IL_007e:
	{
		InflaterInputBuffer_t3211561891 * L_16 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_16);
		int32_t L_17 = InflaterInputBuffer_get_Available_m2874517854(L_16, /*hidden argument*/NULL);
		int64_t L_18 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)(((int64_t)((int64_t)L_17)))) <= ((int64_t)L_18)))
		{
			goto IL_00bd;
		}
	}
	{
		int64_t L_19 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)L_19) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_00bd;
		}
	}
	{
		InflaterInputBuffer_t3211561891 * L_20 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		InflaterInputBuffer_t3211561891 * L_21 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_21);
		int32_t L_22 = InflaterInputBuffer_get_Available_m2874517854(L_21, /*hidden argument*/NULL);
		int64_t L_23 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		NullCheck(L_20);
		InflaterInputBuffer_set_Available_m4062003677(L_20, (((int32_t)((int32_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_22)))-(int64_t)L_23))))), /*hidden argument*/NULL);
		goto IL_0119;
	}

IL_00bd:
	{
		int64_t L_24 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		InflaterInputBuffer_t3211561891 * L_25 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_25);
		int32_t L_26 = InflaterInputBuffer_get_Available_m2874517854(L_25, /*hidden argument*/NULL);
		((InflaterInputStream_t663799889 *)__this)->set_csize_5(((int64_t)((int64_t)L_24-(int64_t)(((int64_t)((int64_t)L_26))))));
		InflaterInputBuffer_t3211561891 * L_27 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_27);
		InflaterInputBuffer_set_Available_m4062003677(L_27, 0, /*hidden argument*/NULL);
		goto IL_010f;
	}

IL_00e4:
	{
		int64_t L_28 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		int64_t L_29 = InflaterInputStream_Skip_m2816449498(__this, L_28, /*hidden argument*/NULL);
		V_1 = L_29;
		int64_t L_30 = V_1;
		if ((((int64_t)L_30) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0101;
		}
	}
	{
		ZipException_t65220526 * L_31 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_31, _stringLiteral2084128162, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_0101:
	{
		int64_t L_32 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		int64_t L_33 = V_1;
		((InflaterInputStream_t663799889 *)__this)->set_csize_5(((int64_t)((int64_t)L_32-(int64_t)L_33)));
	}

IL_010f:
	{
		int64_t L_34 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((!(((uint64_t)L_34) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_00e4;
		}
	}

IL_0119:
	{
		ZipInputStream_CompleteCloseEntry_m1755215957(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.ZipInputStream::get_Length()
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3612051227;
extern Il2CppCodeGenString* _stringLiteral1294897480;
extern const uint32_t ZipInputStream_get_Length_m4273692625_MetadataUsageId;
extern "C"  int64_t ZipInputStream_get_Length_m4273692625 (ZipInputStream_t1369792737 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_get_Length_m4273692625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ZipEntry_t1764014695 * L_0 = __this->get_entry_10();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		ZipEntry_t1764014695 * L_1 = __this->get_entry_10();
		NullCheck(L_1);
		int64_t L_2 = ZipEntry_get_Size_m4101971814(L_1, /*hidden argument*/NULL);
		if ((((int64_t)L_2) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0023;
		}
	}
	{
		ZipEntry_t1764014695 * L_3 = __this->get_entry_10();
		NullCheck(L_3);
		int64_t L_4 = ZipEntry_get_Size_m4101971814(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0023:
	{
		ZipException_t65220526 * L_5 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_5, _stringLiteral3612051227, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002e:
	{
		InvalidOperationException_t721527559 * L_6 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_6, _stringLiteral1294897480, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadByte()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t ZipInputStream_ReadByte_m3836589815_MetadataUsageId;
extern "C"  int32_t ZipInputStream_ReadByte_m3836589815 (ZipInputStream_t1369792737 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_ReadByte_m3836589815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)1));
		ByteU5BU5D_t3397334013* L_0 = V_0;
		int32_t L_1 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, __this, L_0, 0, 1);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return (-1);
	}

IL_0015:
	{
		ByteU5BU5D_t3397334013* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		uint8_t L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		return ((int32_t)((int32_t)L_4&(int32_t)((int32_t)255)));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotAvailable(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1815991638;
extern const uint32_t ZipInputStream_ReadingNotAvailable_m3606461204_MetadataUsageId;
extern "C"  int32_t ZipInputStream_ReadingNotAvailable_m3606461204 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_ReadingNotAvailable_m3606461204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InvalidOperationException_t721527559 * L_0 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_0, _stringLiteral1815991638, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::ReadingNotSupported(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral938653580;
extern const uint32_t ZipInputStream_ReadingNotSupported_m1168240437_MetadataUsageId;
extern "C"  int32_t ZipInputStream_ReadingNotSupported_m1168240437 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_ReadingNotSupported_m1168240437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ZipException_t65220526 * L_0 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_0, _stringLiteral938653580, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::InitialRead(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* PkzipClassicManaged_t4138271613_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipConstants_t82438310_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadDataHandler_t1814344764_il2cpp_TypeInfo_var;
extern const MethodInfo* ZipInputStream_BodyRead_m1704405032_MethodInfo_var;
extern const MethodInfo* ZipInputStream_ReadingNotAvailable_m3606461204_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2165990644;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral527942648;
extern Il2CppCodeGenString* _stringLiteral4193664406;
extern Il2CppCodeGenString* _stringLiteral539196635;
extern const uint32_t ZipInputStream_InitialRead_m3367700748_MetadataUsageId;
extern "C"  int32_t ZipInputStream_InitialRead_m3367700748 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___destination0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_InitialRead_m3367700748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PkzipClassicManaged_t4138271613 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = ZipInputStream_get_CanDecompressEntry_m3676272695(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		ZipEntry_t1764014695 * L_1 = __this->get_entry_10();
		NullCheck(L_1);
		int32_t L_2 = ZipEntry_get_Version_m3445061640(L_1, /*hidden argument*/NULL);
		V_3 = L_2;
		String_t* L_3 = Int32_ToString_m2960866144((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2165990644, L_3, _stringLiteral372029317, /*hidden argument*/NULL);
		ZipException_t65220526 * L_5 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0030:
	{
		ZipEntry_t1764014695 * L_6 = __this->get_entry_10();
		NullCheck(L_6);
		bool L_7 = ZipEntry_get_IsCrypted_m2288425721(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00f8;
		}
	}
	{
		String_t* L_8 = __this->get_password_14();
		if (L_8)
		{
			goto IL_0053;
		}
	}
	{
		ZipException_t65220526 * L_9 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_9, _stringLiteral527942648, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0053:
	{
		PkzipClassicManaged_t4138271613 * L_10 = (PkzipClassicManaged_t4138271613 *)il2cpp_codegen_object_new(PkzipClassicManaged_t4138271613_il2cpp_TypeInfo_var);
		PkzipClassicManaged__ctor_m1234613529(L_10, /*hidden argument*/NULL);
		V_0 = L_10;
		String_t* L_11 = __this->get_password_14();
		IL2CPP_RUNTIME_CLASS_INIT(ZipConstants_t82438310_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_12 = ZipConstants_ConvertToArray_m4193379269(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_13 = PkzipClassic_GenerateKeys_m4071511090(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		InflaterInputBuffer_t3211561891 * L_14 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		PkzipClassicManaged_t4138271613 * L_15 = V_0;
		ByteU5BU5D_t3397334013* L_16 = V_1;
		NullCheck(L_15);
		Il2CppObject * L_17 = VirtFuncInvoker2< Il2CppObject *, ByteU5BU5D_t3397334013*, ByteU5BU5D_t3397334013* >::Invoke(23 /* System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.SymmetricAlgorithm::CreateDecryptor(System.Byte[],System.Byte[]) */, L_15, L_16, (ByteU5BU5D_t3397334013*)(ByteU5BU5D_t3397334013*)NULL);
		NullCheck(L_14);
		InflaterInputBuffer_set_CryptoTransform_m4126126743(L_14, L_17, /*hidden argument*/NULL);
		V_2 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)12)));
		InflaterInputBuffer_t3211561891 * L_18 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		ByteU5BU5D_t3397334013* L_19 = V_2;
		NullCheck(L_18);
		InflaterInputBuffer_ReadClearTextBuffer_m370674671(L_18, L_19, 0, ((int32_t)12), /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_20 = V_2;
		NullCheck(L_20);
		int32_t L_21 = ((int32_t)11);
		uint8_t L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ZipEntry_t1764014695 * L_23 = __this->get_entry_10();
		NullCheck(L_23);
		uint8_t L_24 = ZipEntry_get_CryptoCheckValue_m2906695838(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) == ((int32_t)L_24)))
		{
			goto IL_00b1;
		}
	}
	{
		ZipException_t65220526 * L_25 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_25, _stringLiteral4193664406, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25);
	}

IL_00b1:
	{
		int64_t L_26 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)L_26) < ((int64_t)(((int64_t)((int64_t)((int32_t)12)))))))
		{
			goto IL_00ce;
		}
	}
	{
		int64_t L_27 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		((InflaterInputStream_t663799889 *)__this)->set_csize_5(((int64_t)((int64_t)L_27-(int64_t)(((int64_t)((int64_t)((int32_t)12)))))));
		goto IL_0104;
	}

IL_00ce:
	{
		ZipEntry_t1764014695 * L_28 = __this->get_entry_10();
		NullCheck(L_28);
		int32_t L_29 = ZipEntry_get_Flags_m1454638551(L_28, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_29&(int32_t)8)))
		{
			goto IL_0104;
		}
	}
	{
		int64_t L_30 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		int64_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral539196635, L_32, /*hidden argument*/NULL);
		ZipException_t65220526 * L_34 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_34, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_00f8:
	{
		InflaterInputBuffer_t3211561891 * L_35 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_35);
		InflaterInputBuffer_set_CryptoTransform_m4126126743(L_35, (Il2CppObject *)NULL, /*hidden argument*/NULL);
	}

IL_0104:
	{
		int64_t L_36 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)L_36) > ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_37 = __this->get_flags_13();
		if (!((int32_t)((int32_t)L_37&(int32_t)8)))
		{
			goto IL_015c;
		}
	}

IL_0118:
	{
		int32_t L_38 = __this->get_method_12();
		if ((!(((uint32_t)L_38) == ((uint32_t)8))))
		{
			goto IL_0140;
		}
	}
	{
		InflaterInputBuffer_t3211561891 * L_39 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		NullCheck(L_39);
		int32_t L_40 = InflaterInputBuffer_get_Available_m2874517854(L_39, /*hidden argument*/NULL);
		if ((((int32_t)L_40) <= ((int32_t)0)))
		{
			goto IL_0140;
		}
	}
	{
		InflaterInputBuffer_t3211561891 * L_41 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		Inflater_t3958302971 * L_42 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_41);
		InflaterInputBuffer_SetInflaterInput_m1611710434(L_41, L_42, /*hidden argument*/NULL);
	}

IL_0140:
	{
		IntPtr_t L_43;
		L_43.set_m_value_0((void*)(void*)ZipInputStream_BodyRead_m1704405032_MethodInfo_var);
		ReadDataHandler_t1814344764 * L_44 = (ReadDataHandler_t1814344764 *)il2cpp_codegen_object_new(ReadDataHandler_t1814344764_il2cpp_TypeInfo_var);
		ReadDataHandler__ctor_m1555072274(L_44, __this, L_43, /*hidden argument*/NULL);
		__this->set_internalReader_8(L_44);
		ByteU5BU5D_t3397334013* L_45 = ___destination0;
		int32_t L_46 = ___offset1;
		int32_t L_47 = ___count2;
		int32_t L_48 = ZipInputStream_BodyRead_m1704405032(__this, L_45, L_46, L_47, /*hidden argument*/NULL);
		return L_48;
	}

IL_015c:
	{
		IntPtr_t L_49;
		L_49.set_m_value_0((void*)(void*)ZipInputStream_ReadingNotAvailable_m3606461204_MethodInfo_var);
		ReadDataHandler_t1814344764 * L_50 = (ReadDataHandler_t1814344764 *)il2cpp_codegen_object_new(ReadDataHandler_t1814344764_il2cpp_TypeInfo_var);
		ReadDataHandler__ctor_m1555072274(L_50, __this, L_49, /*hidden argument*/NULL);
		__this->set_internalReader_8(L_50);
		return 0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2713104920;
extern Il2CppCodeGenString* _stringLiteral1673268925;
extern Il2CppCodeGenString* _stringLiteral2769135693;
extern Il2CppCodeGenString* _stringLiteral1554746267;
extern Il2CppCodeGenString* _stringLiteral1064054509;
extern const uint32_t ZipInputStream_Read_m1441760760_MetadataUsageId;
extern "C"  int32_t ZipInputStream_Read_m1441760760 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_Read_m1441760760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral2713104920, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___offset1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_3, _stringLiteral1673268925, _stringLiteral2769135693, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___count2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_5, _stringLiteral1554746267, _stringLiteral2769135693, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0036:
	{
		ByteU5BU5D_t3397334013* L_6 = ___buffer0;
		NullCheck(L_6);
		int32_t L_7 = ___offset1;
		int32_t L_8 = ___count2;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))-(int32_t)L_7))) >= ((int32_t)L_8)))
		{
			goto IL_0049;
		}
	}
	{
		ArgumentException_t3259014390 * L_9 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_9, _stringLiteral1064054509, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0049:
	{
		ReadDataHandler_t1814344764 * L_10 = __this->get_internalReader_8();
		ByteU5BU5D_t3397334013* L_11 = ___buffer0;
		int32_t L_12 = ___offset1;
		int32_t L_13 = ___count2;
		NullCheck(L_10);
		int32_t L_14 = ReadDataHandler_Invoke_m3406984789(L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream::BodyRead(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t65220526_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2816927106;
extern Il2CppCodeGenString* _stringLiteral3301008073;
extern Il2CppCodeGenString* _stringLiteral4266345089;
extern Il2CppCodeGenString* _stringLiteral3085615743;
extern Il2CppCodeGenString* _stringLiteral372029335;
extern Il2CppCodeGenString* _stringLiteral3019223403;
extern Il2CppCodeGenString* _stringLiteral3830674213;
extern const uint32_t ZipInputStream_BodyRead_m1704405032_MetadataUsageId;
extern "C"  int32_t ZipInputStream_BodyRead_m1704405032 (ZipInputStream_t1369792737 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_BodyRead_m1704405032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	{
		Crc32_t3420759743 * L_0 = __this->get_crc_9();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, _stringLiteral2816927106, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		ZipEntry_t1764014695 * L_2 = __this->get_entry_10();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = ___count2;
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_001f:
	{
		return 0;
	}

IL_0021:
	{
		int32_t L_4 = ___offset1;
		int32_t L_5 = ___count2;
		ByteU5BU5D_t3397334013* L_6 = ___buffer0;
		NullCheck(L_6);
		if ((((int32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))))
		{
			goto IL_0034;
		}
	}
	{
		ArgumentException_t3259014390 * L_7 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_7, _stringLiteral3301008073, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0034:
	{
		V_0 = (bool)0;
		int32_t L_8 = __this->get_method_12();
		V_1 = L_8;
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)0)))
		{
			goto IL_0156;
		}
	}
	{
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)8))))
		{
			goto IL_01c6;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_11 = ___buffer0;
		int32_t L_12 = ___offset1;
		int32_t L_13 = ___count2;
		int32_t L_14 = InflaterInputStream_Read_m2526892531(__this, L_11, L_12, L_13, /*hidden argument*/NULL);
		___count2 = L_14;
		int32_t L_15 = ___count2;
		if ((((int32_t)L_15) > ((int32_t)0)))
		{
			goto IL_01c6;
		}
	}
	{
		Inflater_t3958302971 * L_16 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_16);
		bool L_17 = Inflater_get_IsFinished_m3508682262(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0075;
		}
	}
	{
		ZipException_t65220526 * L_18 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_18, _stringLiteral4266345089, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0075:
	{
		InflaterInputBuffer_t3211561891 * L_19 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		Inflater_t3958302971 * L_20 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_20);
		int32_t L_21 = Inflater_get_RemainingInput_m2370068744(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		InflaterInputBuffer_set_Available_m4062003677(L_19, L_21, /*hidden argument*/NULL);
		int32_t L_22 = __this->get_flags_13();
		if (((int32_t)((int32_t)L_22&(int32_t)8)))
		{
			goto IL_0147;
		}
	}
	{
		Inflater_t3958302971 * L_23 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_23);
		int64_t L_24 = Inflater_get_TotalIn_m2049506924(L_23, /*hidden argument*/NULL);
		int64_t L_25 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)L_24) == ((int64_t)L_25)))
		{
			goto IL_00bf;
		}
	}
	{
		int64_t L_26 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)L_26) == ((int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))))
		{
			goto IL_00bf;
		}
	}
	{
		int64_t L_27 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((!(((uint64_t)L_27) == ((uint64_t)(((int64_t)((int64_t)(-1))))))))
		{
			goto IL_00d2;
		}
	}

IL_00bf:
	{
		Inflater_t3958302971 * L_28 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_28);
		int64_t L_29 = Inflater_get_TotalOut_m3944891659(L_28, /*hidden argument*/NULL);
		int64_t L_30 = __this->get_size_11();
		if ((((int64_t)L_29) == ((int64_t)L_30)))
		{
			goto IL_0147;
		}
	}

IL_00d2:
	{
		V_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)8));
		ObjectU5BU5D_t3614634134* L_31 = V_2;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral3085615743);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3085615743);
		ObjectU5BU5D_t3614634134* L_32 = V_2;
		int64_t L_33 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		int64_t L_34 = L_33;
		Il2CppObject * L_35 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_35);
		ObjectU5BU5D_t3614634134* L_36 = V_2;
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, _stringLiteral372029335);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029335);
		ObjectU5BU5D_t3614634134* L_37 = V_2;
		int64_t L_38 = __this->get_size_11();
		int64_t L_39 = L_38;
		Il2CppObject * L_40 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_40);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_40);
		ObjectU5BU5D_t3614634134* L_41 = V_2;
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, _stringLiteral3019223403);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3019223403);
		ObjectU5BU5D_t3614634134* L_42 = V_2;
		Inflater_t3958302971 * L_43 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_43);
		int64_t L_44 = Inflater_get_TotalIn_m2049506924(L_43, /*hidden argument*/NULL);
		int64_t L_45 = L_44;
		Il2CppObject * L_46 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, L_46);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_46);
		ObjectU5BU5D_t3614634134* L_47 = V_2;
		NullCheck(L_47);
		ArrayElementTypeCheck (L_47, _stringLiteral372029335);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral372029335);
		ObjectU5BU5D_t3614634134* L_48 = V_2;
		Inflater_t3958302971 * L_49 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_49);
		int64_t L_50 = Inflater_get_TotalOut_m3944891659(L_49, /*hidden argument*/NULL);
		int64_t L_51 = L_50;
		Il2CppObject * L_52 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_51);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_52);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_52);
		ObjectU5BU5D_t3614634134* L_53 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = String_Concat_m3881798623(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		ZipException_t65220526 * L_55 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_55, L_54, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_55);
	}

IL_0147:
	{
		Inflater_t3958302971 * L_56 = ((InflaterInputStream_t663799889 *)__this)->get_inf_2();
		NullCheck(L_56);
		Inflater_Reset_m3735238578(L_56, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_01c6;
	}

IL_0156:
	{
		int32_t L_57 = ___count2;
		int64_t L_58 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)(((int64_t)((int64_t)L_57)))) <= ((int64_t)L_58)))
		{
			goto IL_0173;
		}
	}
	{
		int64_t L_59 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((((int64_t)L_59) < ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0173;
		}
	}
	{
		int64_t L_60 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		___count2 = (((int32_t)((int32_t)L_60)));
	}

IL_0173:
	{
		int32_t L_61 = ___count2;
		if ((((int32_t)L_61) <= ((int32_t)0)))
		{
			goto IL_01a9;
		}
	}
	{
		InflaterInputBuffer_t3211561891 * L_62 = ((InflaterInputStream_t663799889 *)__this)->get_inputBuffer_3();
		ByteU5BU5D_t3397334013* L_63 = ___buffer0;
		int32_t L_64 = ___offset1;
		int32_t L_65 = ___count2;
		NullCheck(L_62);
		int32_t L_66 = InflaterInputBuffer_ReadClearTextBuffer_m370674671(L_62, L_63, L_64, L_65, /*hidden argument*/NULL);
		___count2 = L_66;
		int32_t L_67 = ___count2;
		if ((((int32_t)L_67) <= ((int32_t)0)))
		{
			goto IL_01a9;
		}
	}
	{
		int64_t L_68 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		int32_t L_69 = ___count2;
		((InflaterInputStream_t663799889 *)__this)->set_csize_5(((int64_t)((int64_t)L_68-(int64_t)(((int64_t)((int64_t)L_69))))));
		int64_t L_70 = __this->get_size_11();
		int32_t L_71 = ___count2;
		__this->set_size_11(((int64_t)((int64_t)L_70-(int64_t)(((int64_t)((int64_t)L_71))))));
	}

IL_01a9:
	{
		int64_t L_72 = ((InflaterInputStream_t663799889 *)__this)->get_csize_5();
		if ((!(((uint64_t)L_72) == ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_01b7;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_01c6;
	}

IL_01b7:
	{
		int32_t L_73 = ___count2;
		if ((((int32_t)L_73) >= ((int32_t)0)))
		{
			goto IL_01c6;
		}
	}
	{
		ZipException_t65220526 * L_74 = (ZipException_t65220526 *)il2cpp_codegen_object_new(ZipException_t65220526_il2cpp_TypeInfo_var);
		ZipException__ctor_m2626676718(L_74, _stringLiteral3830674213, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74);
	}

IL_01c6:
	{
		int32_t L_75 = ___count2;
		if ((((int32_t)L_75) <= ((int32_t)0)))
		{
			goto IL_01d8;
		}
	}
	{
		Crc32_t3420759743 * L_76 = __this->get_crc_9();
		ByteU5BU5D_t3397334013* L_77 = ___buffer0;
		int32_t L_78 = ___offset1;
		int32_t L_79 = ___count2;
		NullCheck(L_76);
		Crc32_Update_m3476396462(L_76, L_77, L_78, L_79, /*hidden argument*/NULL);
	}

IL_01d8:
	{
		bool L_80 = V_0;
		if (!L_80)
		{
			goto IL_01e2;
		}
	}
	{
		ZipInputStream_CompleteCloseEntry_m1755215957(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_01e2:
	{
		int32_t L_81 = ___count2;
		return L_81;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream::Close()
extern Il2CppClass* ReadDataHandler_t1814344764_il2cpp_TypeInfo_var;
extern const MethodInfo* ZipInputStream_ReadingNotAvailable_m3606461204_MethodInfo_var;
extern const uint32_t ZipInputStream_Close_m1006504851_MetadataUsageId;
extern "C"  void ZipInputStream_Close_m1006504851 (ZipInputStream_t1369792737 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipInputStream_Close_m1006504851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)ZipInputStream_ReadingNotAvailable_m3606461204_MethodInfo_var);
		ReadDataHandler_t1814344764 * L_1 = (ReadDataHandler_t1814344764 *)il2cpp_codegen_object_new(ReadDataHandler_t1814344764_il2cpp_TypeInfo_var);
		ReadDataHandler__ctor_m1555072274(L_1, __this, L_0, /*hidden argument*/NULL);
		__this->set_internalReader_8(L_1);
		__this->set_crc_9((Crc32_t3420759743 *)NULL);
		__this->set_entry_10((ZipEntry_t1764014695 *)NULL);
		InflaterInputStream_Close_m2859275264(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ReadDataHandler__ctor_m1555072274 (ReadDataHandler_t1814344764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::Invoke(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ReadDataHandler_Invoke_m3406984789 (ReadDataHandler_t1814344764 * __this, ByteU5BU5D_t3397334013* ___b0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReadDataHandler_Invoke_m3406984789((ReadDataHandler_t1814344764 *)__this->get_prev_9(),___b0, ___offset1, ___length2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, ByteU5BU5D_t3397334013* ___b0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___b0, ___offset1, ___length2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, ByteU5BU5D_t3397334013* ___b0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___b0, ___offset1, ___length2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___offset1, int32_t ___length2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___b0, ___offset1, ___length2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  int32_t DelegatePInvokeWrapper_ReadDataHandler_t1814344764 (ReadDataHandler_t1814344764 * __this, ByteU5BU5D_t3397334013* ___b0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	typedef int32_t (STDCALL *PInvokeFunc)(uint8_t*, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___b0' to native representation
	uint8_t* ____b0_marshaled = NULL;
	if (___b0 != NULL)
	{
		____b0_marshaled = reinterpret_cast<uint8_t*>((___b0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____b0_marshaled, ___offset1, ___length2);

	return returnValue;
}
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::BeginInvoke(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t ReadDataHandler_BeginInvoke_m115987378_MetadataUsageId;
extern "C"  Il2CppObject * ReadDataHandler_BeginInvoke_m115987378 (ReadDataHandler_t1814344764 * __this, ByteU5BU5D_t3397334013* ___b0, int32_t ___offset1, int32_t ___length2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadDataHandler_BeginInvoke_m115987378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___b0;
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___offset1);
	__d_args[2] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___length2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.ZipInputStream/ReadDataHandler::EndInvoke(System.IAsyncResult)
extern "C"  int32_t ReadDataHandler_EndInvoke_m2292870872 (ReadDataHandler_t1814344764 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipNameTransform::.ctor()
extern "C"  void ZipNameTransform__ctor_m1825291376 (ZipNameTransform_t3311483194 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipNameTransform::.cctor()
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipNameTransform_t3311483194_il2cpp_TypeInfo_var;
extern const uint32_t ZipNameTransform__cctor_m2749763903_MetadataUsageId;
extern "C"  void ZipNameTransform__cctor_m2749763903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipNameTransform__cctor_m2749763903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_0 = Path_GetInvalidPathChars_m1312878345(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t1328083999* L_1 = V_0;
		NullCheck(L_1);
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))+(int32_t)2));
		int32_t L_2 = V_1;
		((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->set_InvalidEntryCharsRelaxed_1(((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		CharU5BU5D_t1328083999* L_3 = V_0;
		CharU5BU5D_t1328083999* L_4 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryCharsRelaxed_1();
		CharU5BU5D_t1328083999* L_5 = V_0;
		NullCheck(L_5);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, 0, (Il2CppArray *)(Il2CppArray *)L_4, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_6 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryCharsRelaxed_1();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_7-(int32_t)1))), (Il2CppChar)((int32_t)42));
		CharU5BU5D_t1328083999* L_8 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryCharsRelaxed_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_9-(int32_t)2))), (Il2CppChar)((int32_t)63));
		CharU5BU5D_t1328083999* L_10 = V_0;
		NullCheck(L_10);
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))+(int32_t)4));
		int32_t L_11 = V_1;
		((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->set_InvalidEntryChars_0(((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)L_11)));
		CharU5BU5D_t1328083999* L_12 = V_0;
		CharU5BU5D_t1328083999* L_13 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryChars_0();
		CharU5BU5D_t1328083999* L_14 = V_0;
		NullCheck(L_14);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_12, 0, (Il2CppArray *)(Il2CppArray *)L_13, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))), /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_15 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryChars_0();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_16-(int32_t)1))), (Il2CppChar)((int32_t)58));
		CharU5BU5D_t1328083999* L_17 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryChars_0();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_18-(int32_t)2))), (Il2CppChar)((int32_t)92));
		CharU5BU5D_t1328083999* L_19 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryChars_0();
		int32_t L_20 = V_1;
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_20-(int32_t)3))), (Il2CppChar)((int32_t)42));
		CharU5BU5D_t1328083999* L_21 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryChars_0();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_22-(int32_t)4))), (Il2CppChar)((int32_t)63));
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.ZipNameTransform::IsValidName(System.String,System.Boolean)
extern Il2CppClass* ZipNameTransform_t3311483194_il2cpp_TypeInfo_var;
extern const uint32_t ZipNameTransform_IsValidName_m4036463524_MetadataUsageId;
extern "C"  bool ZipNameTransform_IsValidName_m4036463524 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___relaxed1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZipNameTransform_IsValidName_m4036463524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B6_0 = 0;
	{
		String_t* L_0 = ___name0;
		V_0 = (bool)((((int32_t)((((Il2CppObject*)(String_t*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		bool L_2 = ___relaxed1;
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_3 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(ZipNameTransform_t3311483194_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_4 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryCharsRelaxed_1();
		NullCheck(L_3);
		int32_t L_5 = String_IndexOfAny_m2016554902(L_3, L_4, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_5) < ((int32_t)0))? 1 : 0);
		goto IL_003f;
	}

IL_001f:
	{
		String_t* L_6 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(ZipNameTransform_t3311483194_il2cpp_TypeInfo_var);
		CharU5BU5D_t1328083999* L_7 = ((ZipNameTransform_t3311483194_StaticFields*)ZipNameTransform_t3311483194_il2cpp_TypeInfo_var->static_fields)->get_InvalidEntryChars_0();
		NullCheck(L_6);
		int32_t L_8 = String_IndexOfAny_m2016554902(L_6, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_9 = ___name0;
		NullCheck(L_9);
		int32_t L_10 = String_IndexOf_m2358239236(L_9, ((int32_t)47), /*hidden argument*/NULL);
		G_B6_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B6_0 = 0;
	}

IL_003e:
	{
		V_0 = (bool)G_B6_0;
	}

IL_003f:
	{
		bool L_11 = V_0;
		return L_11;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipTestResultHandler__ctor_m1553451706 (ZipTestResultHandler_t2034591904 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::Invoke(ICSharpCode.SharpZipLib.Zip.TestStatus,System.String)
extern "C"  void ZipTestResultHandler_Invoke_m849185304 (ZipTestResultHandler_t2034591904 * __this, TestStatus_t3549853348 * ___status0, String_t* ___message1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ZipTestResultHandler_Invoke_m849185304((ZipTestResultHandler_t2034591904 *)__this->get_prev_9(),___status0, ___message1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, TestStatus_t3549853348 * ___status0, String_t* ___message1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___status0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, TestStatus_t3549853348 * ___status0, String_t* ___message1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___status0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___status0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::BeginInvoke(ICSharpCode.SharpZipLib.Zip.TestStatus,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipTestResultHandler_BeginInvoke_m3132147729 (ZipTestResultHandler_t2034591904 * __this, TestStatus_t3549853348 * ___status0, String_t* ___message1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___status0;
	__d_args[1] = ___message1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipTestResultHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ZipTestResultHandler_EndInvoke_m1080382356 (ZipTestResultHandler_t2034591904 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
