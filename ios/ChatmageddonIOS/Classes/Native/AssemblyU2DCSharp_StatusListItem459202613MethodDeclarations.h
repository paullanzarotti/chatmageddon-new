﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusListItem
struct StatusListItem_t459202613;
// LaunchedItem
struct LaunchedItem_t3670634427;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"

// System.Void StatusListItem::.ctor()
extern "C"  void StatusListItem__ctor_m3053766144 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::SetUpItem()
extern "C"  void StatusListItem_SetUpItem_m3779585706 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::PopulateItem(LaunchedItem,System.Boolean)
extern "C"  void StatusListItem_PopulateItem_m1884363801 (StatusListItem_t459202613 * __this, LaunchedItem_t3670634427 * ___launchItem0, bool ___outgoing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::PopulateItemInfo()
extern "C"  void StatusListItem_PopulateItemInfo_m3252815041 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::PopulateMissileItemInfo()
extern "C"  void StatusListItem_PopulateMissileItemInfo_m966745941 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::PopulateMineItemInfo()
extern "C"  void StatusListItem_PopulateMineItemInfo_m3898105536 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::UpdateAvatar()
extern "C"  void StatusListItem_UpdateAvatar_m4216732786 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::SetBaseItemColour(UnityEngine.Color)
extern "C"  void StatusListItem_SetBaseItemColour_m1432491270 (StatusListItem_t459202613 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::SetActionLabelColour(UnityEngine.Color)
extern "C"  void StatusListItem_SetActionLabelColour_m224856786 (StatusListItem_t459202613 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::SetPointsLabels(MissileTravelState,System.Single)
extern "C"  void StatusListItem_SetPointsLabels_m3951392500 (StatusListItem_t459202613 * __this, int32_t ___state0, float ___amount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::SetVisible()
extern "C"  void StatusListItem_SetVisible_m1542311084 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::SetInvisible()
extern "C"  void StatusListItem_SetInvisible_m3450221039 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusListItem::verifyVisibility()
extern "C"  bool StatusListItem_verifyVisibility_m1046245349 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::CheckVisibilty()
extern "C"  void StatusListItem_CheckVisibilty_m663575981 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::Update()
extern "C"  void StatusListItem_Update_m2341220353 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::OnEnable()
extern "C"  void StatusListItem_OnEnable_m1856598304 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::FindScrollView()
extern "C"  void StatusListItem_FindScrollView_m2205256821 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::OnPress(System.Boolean)
extern "C"  void StatusListItem_OnPress_m2315864945 (StatusListItem_t459202613 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::OnDrag()
extern "C"  void StatusListItem_OnDrag_m1582848089 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::Drag()
extern "C"  void StatusListItem_Drag_m4096880188 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::OnDragEnd()
extern "C"  void StatusListItem_OnDragEnd_m1773756140 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::OnScroll(System.Single)
extern "C"  void StatusListItem_OnScroll_m2208296445 (StatusListItem_t459202613 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::OnClick()
extern "C"  void StatusListItem_OnClick_m372686571 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusListItem::LoadResultModal()
extern "C"  void StatusListItem_LoadResultModal_m858381990 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StatusListItem::WaitToAllowToast()
extern "C"  Il2CppObject * StatusListItem_WaitToAllowToast_m898924086 (StatusListItem_t459202613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
