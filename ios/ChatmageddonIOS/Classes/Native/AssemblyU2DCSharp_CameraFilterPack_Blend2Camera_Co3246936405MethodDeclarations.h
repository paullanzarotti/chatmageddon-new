﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Color
struct CameraFilterPack_Blend2Camera_Color_t3246936405;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Color::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Color__ctor_m2616441696 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Color::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Color_get_material_m1736536265 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Color_Start_m1887331632 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Color_OnRenderImage_m716669056 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Color_OnValidate_m4058584491 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Color_Update_m2017009173 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Color_OnEnable_m1563766768 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Color_OnDisable_m3801040089 (CameraFilterPack_Blend2Camera_Color_t3246936405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
