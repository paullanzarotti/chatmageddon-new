﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<HomeLeftUIManager>::.ctor()
#define MonoSingleton_1__ctor_m4020040331(__this, method) ((  void (*) (MonoSingleton_1_t3683132453 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeLeftUIManager>::Awake()
#define MonoSingleton_1_Awake_m3393475598(__this, method) ((  void (*) (MonoSingleton_1_t3683132453 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<HomeLeftUIManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m3364618050(__this /* static, unused */, method) ((  HomeLeftUIManager_t3932466733 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<HomeLeftUIManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3130842560(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<HomeLeftUIManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1597604801(__this, method) ((  void (*) (MonoSingleton_1_t3683132453 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeLeftUIManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m1524627921(__this, method) ((  void (*) (MonoSingleton_1_t3683132453 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<HomeLeftUIManager>::.cctor()
#define MonoSingleton_1__cctor_m2679609368(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
