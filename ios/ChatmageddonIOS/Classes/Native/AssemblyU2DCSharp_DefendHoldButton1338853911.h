﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HoldMiniGame
struct HoldMiniGame_t1783545348;
// HoldBarController
struct HoldBarController_t3195732834;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendHoldButton
struct  DefendHoldButton_t1338853911  : public SFXButton_t792651341
{
public:
	// HoldMiniGame DefendHoldButton::miniGame
	HoldMiniGame_t1783545348 * ___miniGame_5;
	// HoldBarController DefendHoldButton::barController
	HoldBarController_t3195732834 * ___barController_6;
	// System.Single DefendHoldButton::totalHoldTime
	float ___totalHoldTime_7;
	// System.Boolean DefendHoldButton::heldDown
	bool ___heldDown_8;
	// System.Single DefendHoldButton::secondsHeld
	float ___secondsHeld_9;
	// UnityEngine.Coroutine DefendHoldButton::holdRoutine
	Coroutine_t2299508840 * ___holdRoutine_10;

public:
	inline static int32_t get_offset_of_miniGame_5() { return static_cast<int32_t>(offsetof(DefendHoldButton_t1338853911, ___miniGame_5)); }
	inline HoldMiniGame_t1783545348 * get_miniGame_5() const { return ___miniGame_5; }
	inline HoldMiniGame_t1783545348 ** get_address_of_miniGame_5() { return &___miniGame_5; }
	inline void set_miniGame_5(HoldMiniGame_t1783545348 * value)
	{
		___miniGame_5 = value;
		Il2CppCodeGenWriteBarrier(&___miniGame_5, value);
	}

	inline static int32_t get_offset_of_barController_6() { return static_cast<int32_t>(offsetof(DefendHoldButton_t1338853911, ___barController_6)); }
	inline HoldBarController_t3195732834 * get_barController_6() const { return ___barController_6; }
	inline HoldBarController_t3195732834 ** get_address_of_barController_6() { return &___barController_6; }
	inline void set_barController_6(HoldBarController_t3195732834 * value)
	{
		___barController_6 = value;
		Il2CppCodeGenWriteBarrier(&___barController_6, value);
	}

	inline static int32_t get_offset_of_totalHoldTime_7() { return static_cast<int32_t>(offsetof(DefendHoldButton_t1338853911, ___totalHoldTime_7)); }
	inline float get_totalHoldTime_7() const { return ___totalHoldTime_7; }
	inline float* get_address_of_totalHoldTime_7() { return &___totalHoldTime_7; }
	inline void set_totalHoldTime_7(float value)
	{
		___totalHoldTime_7 = value;
	}

	inline static int32_t get_offset_of_heldDown_8() { return static_cast<int32_t>(offsetof(DefendHoldButton_t1338853911, ___heldDown_8)); }
	inline bool get_heldDown_8() const { return ___heldDown_8; }
	inline bool* get_address_of_heldDown_8() { return &___heldDown_8; }
	inline void set_heldDown_8(bool value)
	{
		___heldDown_8 = value;
	}

	inline static int32_t get_offset_of_secondsHeld_9() { return static_cast<int32_t>(offsetof(DefendHoldButton_t1338853911, ___secondsHeld_9)); }
	inline float get_secondsHeld_9() const { return ___secondsHeld_9; }
	inline float* get_address_of_secondsHeld_9() { return &___secondsHeld_9; }
	inline void set_secondsHeld_9(float value)
	{
		___secondsHeld_9 = value;
	}

	inline static int32_t get_offset_of_holdRoutine_10() { return static_cast<int32_t>(offsetof(DefendHoldButton_t1338853911, ___holdRoutine_10)); }
	inline Coroutine_t2299508840 * get_holdRoutine_10() const { return ___holdRoutine_10; }
	inline Coroutine_t2299508840 ** get_address_of_holdRoutine_10() { return &___holdRoutine_10; }
	inline void set_holdRoutine_10(Coroutine_t2299508840 * value)
	{
		___holdRoutine_10 = value;
		Il2CppCodeGenWriteBarrier(&___holdRoutine_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
