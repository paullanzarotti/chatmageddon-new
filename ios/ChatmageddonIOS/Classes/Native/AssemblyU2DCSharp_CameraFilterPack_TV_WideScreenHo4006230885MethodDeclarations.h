﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_WideScreenHorizontal
struct CameraFilterPack_TV_WideScreenHorizontal_t4006230885;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_WideScreenHorizontal::.ctor()
extern "C"  void CameraFilterPack_TV_WideScreenHorizontal__ctor_m4236412922 (CameraFilterPack_TV_WideScreenHorizontal_t4006230885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_WideScreenHorizontal::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_WideScreenHorizontal_get_material_m4010630709 (CameraFilterPack_TV_WideScreenHorizontal_t4006230885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHorizontal::Start()
extern "C"  void CameraFilterPack_TV_WideScreenHorizontal_Start_m1629319282 (CameraFilterPack_TV_WideScreenHorizontal_t4006230885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHorizontal::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_WideScreenHorizontal_OnRenderImage_m4179764450 (CameraFilterPack_TV_WideScreenHorizontal_t4006230885 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHorizontal::OnValidate()
extern "C"  void CameraFilterPack_TV_WideScreenHorizontal_OnValidate_m36587267 (CameraFilterPack_TV_WideScreenHorizontal_t4006230885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHorizontal::Update()
extern "C"  void CameraFilterPack_TV_WideScreenHorizontal_Update_m2308695017 (CameraFilterPack_TV_WideScreenHorizontal_t4006230885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHorizontal::OnDisable()
extern "C"  void CameraFilterPack_TV_WideScreenHorizontal_OnDisable_m1025555221 (CameraFilterPack_TV_WideScreenHorizontal_t4006230885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
