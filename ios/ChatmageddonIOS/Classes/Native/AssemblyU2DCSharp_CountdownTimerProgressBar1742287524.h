﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_CircularProgressBar2980095063.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountdownTimerProgressBar
struct  CountdownTimerProgressBar_t1742287524  : public CircularProgressBar_t2980095063
{
public:
	// UILabel CountdownTimerProgressBar::countdownLabel
	UILabel_t1795115428 * ___countdownLabel_13;
	// System.Single CountdownTimerProgressBar::countdownTimeAmount
	float ___countdownTimeAmount_14;
	// System.Int32 CountdownTimerProgressBar::numberOn
	int32_t ___numberOn_15;

public:
	inline static int32_t get_offset_of_countdownLabel_13() { return static_cast<int32_t>(offsetof(CountdownTimerProgressBar_t1742287524, ___countdownLabel_13)); }
	inline UILabel_t1795115428 * get_countdownLabel_13() const { return ___countdownLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_countdownLabel_13() { return &___countdownLabel_13; }
	inline void set_countdownLabel_13(UILabel_t1795115428 * value)
	{
		___countdownLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___countdownLabel_13, value);
	}

	inline static int32_t get_offset_of_countdownTimeAmount_14() { return static_cast<int32_t>(offsetof(CountdownTimerProgressBar_t1742287524, ___countdownTimeAmount_14)); }
	inline float get_countdownTimeAmount_14() const { return ___countdownTimeAmount_14; }
	inline float* get_address_of_countdownTimeAmount_14() { return &___countdownTimeAmount_14; }
	inline void set_countdownTimeAmount_14(float value)
	{
		___countdownTimeAmount_14 = value;
	}

	inline static int32_t get_offset_of_numberOn_15() { return static_cast<int32_t>(offsetof(CountdownTimerProgressBar_t1742287524, ___numberOn_15)); }
	inline int32_t get_numberOn_15() const { return ___numberOn_15; }
	inline int32_t* get_address_of_numberOn_15() { return &___numberOn_15; }
	inline void set_numberOn_15(int32_t value)
	{
		___numberOn_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
