﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BasePurchaseManager`2<System.Object,System.Object>
struct BasePurchaseManager_2_t3825336974;
// PurchasableItem
struct PurchasableItem_t3963353899;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void BasePurchaseManager`2<System.Object,System.Object>::.ctor()
extern "C"  void BasePurchaseManager_2__ctor_m2801942166_gshared (BasePurchaseManager_2_t3825336974 * __this, const MethodInfo* method);
#define BasePurchaseManager_2__ctor_m2801942166(__this, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, const MethodInfo*))BasePurchaseManager_2__ctor_m2801942166_gshared)(__this, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::Init()
extern "C"  void BasePurchaseManager_2_Init_m4111095922_gshared (BasePurchaseManager_2_t3825336974 * __this, const MethodInfo* method);
#define BasePurchaseManager_2_Init_m4111095922(__this, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, const MethodInfo*))BasePurchaseManager_2_Init_m4111095922_gshared)(__this, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::PurchaseUnibillItem(PurchasableItem)
extern "C"  void BasePurchaseManager_2_PurchaseUnibillItem_m2896313322_gshared (BasePurchaseManager_2_t3825336974 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_PurchaseUnibillItem_m2896313322(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, PurchasableItem_t3963353899 *, const MethodInfo*))BasePurchaseManager_2_PurchaseUnibillItem_m2896313322_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::UnibillPurchaseSuccess(PurchasableItem,System.String)
extern "C"  void BasePurchaseManager_2_UnibillPurchaseSuccess_m2453143854_gshared (BasePurchaseManager_2_t3825336974 * __this, PurchasableItem_t3963353899 * ___item0, String_t* ___receipt1, const MethodInfo* method);
#define BasePurchaseManager_2_UnibillPurchaseSuccess_m2453143854(__this, ___item0, ___receipt1, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, PurchasableItem_t3963353899 *, String_t*, const MethodInfo*))BasePurchaseManager_2_UnibillPurchaseSuccess_m2453143854_gshared)(__this, ___item0, ___receipt1, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::UnibillPurchaseFailed(PurchasableItem)
extern "C"  void BasePurchaseManager_2_UnibillPurchaseFailed_m1481010802_gshared (BasePurchaseManager_2_t3825336974 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_UnibillPurchaseFailed_m1481010802(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, PurchasableItem_t3963353899 *, const MethodInfo*))BasePurchaseManager_2_UnibillPurchaseFailed_m1481010802_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::ProcessItem(System.String,System.String)
extern "C"  void BasePurchaseManager_2_ProcessItem_m3316610698_gshared (BasePurchaseManager_2_t3825336974 * __this, String_t* ___id0, String_t* ___receipt1, const MethodInfo* method);
#define BasePurchaseManager_2_ProcessItem_m3316610698(__this, ___id0, ___receipt1, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, String_t*, String_t*, const MethodInfo*))BasePurchaseManager_2_ProcessItem_m3316610698_gshared)(__this, ___id0, ___receipt1, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::ProcessFailedItem(System.String)
extern "C"  void BasePurchaseManager_2_ProcessFailedItem_m3688344421_gshared (BasePurchaseManager_2_t3825336974 * __this, String_t* ___id0, const MethodInfo* method);
#define BasePurchaseManager_2_ProcessFailedItem_m3688344421(__this, ___id0, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, String_t*, const MethodInfo*))BasePurchaseManager_2_ProcessFailedItem_m3688344421_gshared)(__this, ___id0, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::PurchaseItem(itemType)
extern "C"  void BasePurchaseManager_2_PurchaseItem_m782678101_gshared (BasePurchaseManager_2_t3825336974 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_PurchaseItem_m782678101(__this, ___item0, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, Il2CppObject *, const MethodInfo*))BasePurchaseManager_2_PurchaseItem_m782678101_gshared)(__this, ___item0, method)
// System.Void BasePurchaseManager`2<System.Object,System.Object>::PurchaseItemByID(System.String)
extern "C"  void BasePurchaseManager_2_PurchaseItemByID_m3644043766_gshared (BasePurchaseManager_2_t3825336974 * __this, String_t* ___itemID0, const MethodInfo* method);
#define BasePurchaseManager_2_PurchaseItemByID_m3644043766(__this, ___itemID0, method) ((  void (*) (BasePurchaseManager_2_t3825336974 *, String_t*, const MethodInfo*))BasePurchaseManager_2_PurchaseItemByID_m3644043766_gshared)(__this, ___itemID0, method)
// System.String BasePurchaseManager`2<System.Object,System.Object>::GetPurchaseLocalizedString(itemType)
extern "C"  String_t* BasePurchaseManager_2_GetPurchaseLocalizedString_m1961339405_gshared (BasePurchaseManager_2_t3825336974 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define BasePurchaseManager_2_GetPurchaseLocalizedString_m1961339405(__this, ___item0, method) ((  String_t* (*) (BasePurchaseManager_2_t3825336974 *, Il2CppObject *, const MethodInfo*))BasePurchaseManager_2_GetPurchaseLocalizedString_m1961339405_gshared)(__this, ___item0, method)
// System.String BasePurchaseManager`2<System.Object,System.Object>::GetPurchaseLocalizedStringByID(System.String)
extern "C"  String_t* BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m4171794686_gshared (BasePurchaseManager_2_t3825336974 * __this, String_t* ___itemID0, const MethodInfo* method);
#define BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m4171794686(__this, ___itemID0, method) ((  String_t* (*) (BasePurchaseManager_2_t3825336974 *, String_t*, const MethodInfo*))BasePurchaseManager_2_GetPurchaseLocalizedStringByID_m4171794686_gshared)(__this, ___itemID0, method)
