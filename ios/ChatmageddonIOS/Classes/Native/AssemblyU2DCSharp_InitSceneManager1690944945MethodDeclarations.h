﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InitSceneManager
struct InitSceneManager_t1690944945;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void InitSceneManager::.ctor()
extern "C"  void InitSceneManager__ctor_m1805251512 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::InitScene()
extern "C"  void InitSceneManager_InitScene_m3565125498 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InitSceneManager::WaitForScreenResize()
extern "C"  Il2CppObject * InitSceneManager_WaitForScreenResize_m2460592340 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InitSceneManager::WaitForSeconds()
extern "C"  Il2CppObject * InitSceneManager_WaitForSeconds_m3914468093 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::ContinueInit()
extern "C"  void InitSceneManager_ContinueInit_m1977631163 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::AppInit()
extern "C"  void InitSceneManager_AppInit_m903852911 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::LoadConnectionPU()
extern "C"  void InitSceneManager_LoadConnectionPU_m3995219773 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::LoadInternetManagers()
extern "C"  void InitSceneManager_LoadInternetManagers_m1496106341 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::LoadConfig()
extern "C"  void InitSceneManager_LoadConfig_m613549296 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::StartScene()
extern "C"  void InitSceneManager_StartScene_m2212686978 (InitSceneManager_t1690944945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::<AppInit>m__0(System.Boolean,System.String)
extern "C"  void InitSceneManager_U3CAppInitU3Em__0_m2117005097 (InitSceneManager_t1690944945 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneManager::<LoadConfig>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void InitSceneManager_U3CLoadConfigU3Em__1_m235441695 (InitSceneManager_t1690944945 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
