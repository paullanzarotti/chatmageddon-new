﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// QuestionUI
struct  QuestionUI_t3721436426  : public MonoBehaviour_t1158329972
{
public:
	// UILabel QuestionUI::questionText
	UILabel_t1795115428 * ___questionText_2;
	// UILabel QuestionUI::anwserText
	UILabel_t1795115428 * ___anwserText_3;
	// UnityEngine.BoxCollider QuestionUI::uiCollider
	BoxCollider_t22920061 * ___uiCollider_4;
	// System.Single QuestionUI::sectionSplit
	float ___sectionSplit_5;

public:
	inline static int32_t get_offset_of_questionText_2() { return static_cast<int32_t>(offsetof(QuestionUI_t3721436426, ___questionText_2)); }
	inline UILabel_t1795115428 * get_questionText_2() const { return ___questionText_2; }
	inline UILabel_t1795115428 ** get_address_of_questionText_2() { return &___questionText_2; }
	inline void set_questionText_2(UILabel_t1795115428 * value)
	{
		___questionText_2 = value;
		Il2CppCodeGenWriteBarrier(&___questionText_2, value);
	}

	inline static int32_t get_offset_of_anwserText_3() { return static_cast<int32_t>(offsetof(QuestionUI_t3721436426, ___anwserText_3)); }
	inline UILabel_t1795115428 * get_anwserText_3() const { return ___anwserText_3; }
	inline UILabel_t1795115428 ** get_address_of_anwserText_3() { return &___anwserText_3; }
	inline void set_anwserText_3(UILabel_t1795115428 * value)
	{
		___anwserText_3 = value;
		Il2CppCodeGenWriteBarrier(&___anwserText_3, value);
	}

	inline static int32_t get_offset_of_uiCollider_4() { return static_cast<int32_t>(offsetof(QuestionUI_t3721436426, ___uiCollider_4)); }
	inline BoxCollider_t22920061 * get_uiCollider_4() const { return ___uiCollider_4; }
	inline BoxCollider_t22920061 ** get_address_of_uiCollider_4() { return &___uiCollider_4; }
	inline void set_uiCollider_4(BoxCollider_t22920061 * value)
	{
		___uiCollider_4 = value;
		Il2CppCodeGenWriteBarrier(&___uiCollider_4, value);
	}

	inline static int32_t get_offset_of_sectionSplit_5() { return static_cast<int32_t>(offsetof(QuestionUI_t3721436426, ___sectionSplit_5)); }
	inline float get_sectionSplit_5() const { return ___sectionSplit_5; }
	inline float* get_address_of_sectionSplit_5() { return &___sectionSplit_5; }
	inline void set_sectionSplit_5(float value)
	{
		___sectionSplit_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
