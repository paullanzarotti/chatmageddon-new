﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OpenPUToast3407558498.h"
#include "AssemblyU2DCSharp_OpenSmallToastButton3751439810.h"
#include "AssemblyU2DCSharp_PUBread493011471.h"
#include "AssemblyU2DCSharp_SmallBread121884373.h"
#include "AssemblyU2DCSharp_ToastType1578101939.h"
#include "AssemblyU2DCSharp_ToastManager1002293494.h"
#include "AssemblyU2DCSharp_Toaster2428368812.h"
#include "AssemblyU2DCSharp_ClosePUToastButton2735970074.h"
#include "AssemblyU2DCSharp_PUToastUI4217418532.h"
#include "AssemblyU2DCSharp_PUToastUI_U3CDisplayWaitU3Ec__It4015913167.h"
#include "AssemblyU2DCSharp_SmallToastUI2064810232.h"
#include "AssemblyU2DCSharp_SmallToastUI_U3CDisplayWaitU3Ec_3130991769.h"
#include "AssemblyU2DCSharp_ToastUI270783635.h"
#include "AssemblyU2DCSharp_ToasterUIScaler2057419170.h"
#include "AssemblyU2DCSharp_ChatUpdateType1225593925.h"
#include "AssemblyU2DCSharp_ChatFeed701616454.h"
#include "AssemblyU2DCSharp_Feed3408144164.h"
#include "AssemblyU2DCSharp_Feed_U3CUpdateFeedU3Ec__Iterator075471835.h"
#include "AssemblyU2DCSharp_FeedManager2709865823.h"
#include "AssemblyU2DCSharp_UniversalUpdateType1629501440.h"
#include "AssemblyU2DCSharp_AutoFriendType103565391.h"
#include "AssemblyU2DCSharp_UniversalFeed2144776909.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "AssemblyU2DCSharp_Player1147783557.h"
#include "AssemblyU2DCSharp_Player_U3CUpdateInnerFriendsListU395413968.h"
#include "AssemblyU2DCSharp_Player_U3CUpdateInnerFriendsListU395413967.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_PrefabResource926725558.h"
#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_ChatContact3760700014.h"
#include "AssemblyU2DCSharp_ChatContactsILP1780035494.h"
#include "AssemblyU2DCSharp_ChatContactsListItem4002571236.h"
#include "AssemblyU2DCSharp_ChatListType844669534.h"
#include "AssemblyU2DCSharp_ChatManager2792590695.h"
#include "AssemblyU2DCSharp_ChatMessageILP4258019024.h"
#include "AssemblyU2DCSharp_ChatMessageListItem2425051210.h"
#include "AssemblyU2DCSharp_ChatNavigateBackwardButton516150148.h"
#include "AssemblyU2DCSharp_ChatNavigateForwardButton4025092154.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_ChatNavigationController880507216.h"
#include "AssemblyU2DCSharp_ChatSceneManager555354597.h"
#include "AssemblyU2DCSharp_ChatThreadSwipe397952508.h"
#include "AssemblyU2DCSharp_ChatThreadsILP2341903446.h"
#include "AssemblyU2DCSharp_ChatThreadsILP_U3CUpdateItemData1678493984.h"
#include "AssemblyU2DCSharp_ChatThreadsListItem303868848.h"
#include "AssemblyU2DCSharp_ContactsNavScreen1794286542.h"
#include "AssemblyU2DCSharp_ContactsSearchInput2789723005.h"
#include "AssemblyU2DCSharp_DeleteThreadButton3881568975.h"
#include "AssemblyU2DCSharp_KeyboardSendButton1707295269.h"
#include "AssemblyU2DCSharp_MessageKeyboardInput4224908926.h"
#include "AssemblyU2DCSharp_MessageNavScreen601131416.h"
#include "AssemblyU2DCSharp_ThreadsNavScreen4034840106.h"
#include "AssemblyU2DCSharp_ChatCenterUIScaler436229885.h"
#include "AssemblyU2DCSharp_ChatContactsListItemUIScaler2894863298.h"
#include "AssemblyU2DCSharp_ThreadListItemUIScaler2282969193.h"
#include "AssemblyU2DCSharp_ActivateUserKnockoutButton3686915466.h"
#include "AssemblyU2DCSharp_ActivateUserStealthModeButton2608034790.h"
#include "AssemblyU2DCSharp_DeactivateShield2656914733.h"
#include "AssemblyU2DCSharp_PingServer1218551355.h"
#include "AssemblyU2DCSharp_SetTotalPoints1575563713.h"
#include "AssemblyU2DCSharp_ChatmageddonGamestate808689860.h"
#include "AssemblyU2DCSharp_AttackListType2861798804.h"
#include "AssemblyU2DCSharp_AttackManager3475553125.h"
#include "AssemblyU2DCSharp_AttackManager_U3CSendFriendReque2978818532.h"
#include "AssemblyU2DCSharp_AttackManager_U3CAcceptFriendRequ658574309.h"
#include "AssemblyU2DCSharp_AttackManager_U3CDeclineFriendRe1271168958.h"
#include "AssemblyU2DCSharp_AttackNavigateBackwardsButton4019147023.h"
#include "AssemblyU2DCSharp_AttackNavigateForwardsButton1401782687.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_AttackNavigationController2145483014.h"
#include "AssemblyU2DCSharp_AttackNavigationController_U3CFi2076676339.h"
#include "AssemblyU2DCSharp_AttackTargetsButton1016182822.h"
#include "AssemblyU2DCSharp_AimQuadrent3168938451.h"
#include "AssemblyU2DCSharp_AimMarker327093161.h"
#include "AssemblyU2DCSharp_AimMarker_U3CUpdatePositionUIRout189874335.h"
#include "AssemblyU2DCSharp_AimMarker_U3CSpirographU3Ec__Ite3990386259.h"
#include "AssemblyU2DCSharp_AttackCancelGameButton2121269608.h"
#include "AssemblyU2DCSharp_AttackGameNavScreen4158424117.h"
#include "AssemblyU2DCSharp_AttackGameState4284764851.h"
#include "AssemblyU2DCSharp_AttackMiniGameController105133407.h"
#include "AssemblyU2DCSharp_AttackMiniGameController_U3CWait2230046844.h"
#include "AssemblyU2DCSharp_AttackMiniGameController_U3CLaunc265198302.h"
#include "AssemblyU2DCSharp_AttackMiniGameController_U3Cwait4214048921.h"
#include "AssemblyU2DCSharp_FireGuage4001636259.h"
#include "AssemblyU2DCSharp_FireGuage_U3CMoveFireGuageU3Ec__I520811751.h"
#include "AssemblyU2DCSharp_LaunchAccuracyController4092159714.h"
#include "AssemblyU2DCSharp_LaunchAccuracyController_U3CFlas3216582880.h"
#include "AssemblyU2DCSharp_HomeBottomUIScaler3171336006.h"
#include "AssemblyU2DCSharp_AttackAcceptButton1767177984.h"
#include "AssemblyU2DCSharp_AttackChatButton4176739546.h"
#include "AssemblyU2DCSharp_AttackDeclineButton304169658.h"
#include "AssemblyU2DCSharp_AttackFriendButton2212457944.h"
#include "AssemblyU2DCSharp_AttackHomeNavScreen2473299220.h"
#include "AssemblyU2DCSharp_AttackHomeNavigateForwardButton3562703987.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"
#include "AssemblyU2DCSharp_AttackHomeNavigationController3047766999.h"
#include "AssemblyU2DCSharp_AttackILP2111768551.h"
#include "AssemblyU2DCSharp_AttackItemAttackButton1001821753.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5500 = { sizeof (OpenPUToast_t3407558498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5500[6] = 
{
	OpenPUToast_t3407558498::get_offset_of_message_5(),
	OpenPUToast_t3407558498::get_offset_of_closeText_6(),
	OpenPUToast_t3407558498::get_offset_of_title_7(),
	OpenPUToast_t3407558498::get_offset_of_texture_8(),
	OpenPUToast_t3407558498::get_offset_of_showFade_9(),
	OpenPUToast_t3407558498::get_offset_of_sfx_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5501 = { sizeof (OpenSmallToastButton_t3751439810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5501[1] = 
{
	OpenSmallToastButton_t3751439810::get_offset_of_message_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5502 = { sizeof (PUBread_t493011471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5502[3] = 
{
	PUBread_t493011471::get_offset_of_toastTitle_4(),
	PUBread_t493011471::get_offset_of_toastTexture_5(),
	PUBread_t493011471::get_offset_of_toastButtonText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5503 = { sizeof (SmallBread_t121884373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5504 = { sizeof (ToastType_t1578101939)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5504[3] = 
{
	ToastType_t1578101939::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5505 = { sizeof (ToastManager_t1002293494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5505[3] = 
{
	ToastManager_t1002293494::get_offset_of_toaster_3(),
	ToastManager_t1002293494::get_offset_of_loaf_4(),
	ToastManager_t1002293494::get_offset_of_uiResourcePath_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5506 = { sizeof (Toaster_t2428368812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5506[5] = 
{
	Toaster_t2428368812::get_offset_of_uiResourcePath_2(),
	Toaster_t2428368812::get_offset_of_backgroundFade_3(),
	Toaster_t2428368812::get_offset_of_toastContentObject_4(),
	Toaster_t2428368812::get_offset_of_activeUI_5(),
	Toaster_t2428368812::get_offset_of_closingFade_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5507 = { sizeof (ClosePUToastButton_t2735970074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5507[1] = 
{
	ClosePUToastButton_t2735970074::get_offset_of_toastUI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5508 = { sizeof (PUToastUI_t4217418532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5508[16] = 
{
	PUToastUI_t4217418532::get_offset_of_content_6(),
	PUToastUI_t4217418532::get_offset_of_titleLabel_7(),
	PUToastUI_t4217418532::get_offset_of_puTexture_8(),
	PUToastUI_t4217418532::get_offset_of_buttonCollider_9(),
	PUToastUI_t4217418532::get_offset_of_buttonSprite_10(),
	PUToastUI_t4217418532::get_offset_of_buttonLabel_11(),
	PUToastUI_t4217418532::get_offset_of_puBread_12(),
	PUToastUI_t4217418532::get_offset_of_puPadding_13(),
	PUToastUI_t4217418532::get_offset_of_messagePadding_14(),
	PUToastUI_t4217418532::get_offset_of_titlePadding_15(),
	PUToastUI_t4217418532::get_offset_of_texturePadding_16(),
	PUToastUI_t4217418532::get_offset_of_buttonPadding_17(),
	PUToastUI_t4217418532::get_offset_of_buttonLabelPadding_18(),
	PUToastUI_t4217418532::get_offset_of_scaleTween_19(),
	PUToastUI_t4217418532::get_offset_of_alphaTweens_20(),
	PUToastUI_t4217418532::get_offset_of_closing_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5509 = { sizeof (U3CDisplayWaitU3Ec__Iterator0_t4015913167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5509[4] = 
{
	U3CDisplayWaitU3Ec__Iterator0_t4015913167::get_offset_of_U24this_0(),
	U3CDisplayWaitU3Ec__Iterator0_t4015913167::get_offset_of_U24current_1(),
	U3CDisplayWaitU3Ec__Iterator0_t4015913167::get_offset_of_U24disposing_2(),
	U3CDisplayWaitU3Ec__Iterator0_t4015913167::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5510 = { sizeof (SmallToastUI_t2064810232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5510[6] = 
{
	SmallToastUI_t2064810232::get_offset_of_positonOffset_6(),
	SmallToastUI_t2064810232::get_offset_of_verticlePadding_7(),
	SmallToastUI_t2064810232::get_offset_of_smallBread_8(),
	SmallToastUI_t2064810232::get_offset_of_scaleTween_9(),
	SmallToastUI_t2064810232::get_offset_of_alphaTweens_10(),
	SmallToastUI_t2064810232::get_offset_of_closing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5511 = { sizeof (U3CDisplayWaitU3Ec__Iterator0_t3130991769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5511[4] = 
{
	U3CDisplayWaitU3Ec__Iterator0_t3130991769::get_offset_of_U24this_0(),
	U3CDisplayWaitU3Ec__Iterator0_t3130991769::get_offset_of_U24current_1(),
	U3CDisplayWaitU3Ec__Iterator0_t3130991769::get_offset_of_U24disposing_2(),
	U3CDisplayWaitU3Ec__Iterator0_t3130991769::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5512 = { sizeof (ToastUI_t270783635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5512[4] = 
{
	ToastUI_t270783635::get_offset_of_toastID_2(),
	ToastUI_t270783635::get_offset_of_messageLabel_3(),
	ToastUI_t270783635::get_offset_of_backgroundSprite_4(),
	ToastUI_t270783635::get_offset_of_displaySeconds_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5513 = { sizeof (ToasterUIScaler_t2057419170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5513[2] = 
{
	ToasterUIScaler_t2057419170::get_offset_of_background_14(),
	ToasterUIScaler_t2057419170::get_offset_of_backgroundCollider_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5514 = { sizeof (ChatUpdateType_t1225593925)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5514[3] = 
{
	ChatUpdateType_t1225593925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5515 = { sizeof (ChatFeed_t701616454), -1, sizeof(ChatFeed_t701616454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5515[1] = 
{
	ChatFeed_t701616454_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5516 = { sizeof (Feed_t3408144164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5516[4] = 
{
	Feed_t3408144164::get_offset_of_feedActive_2(),
	Feed_t3408144164::get_offset_of_feedName_3(),
	Feed_t3408144164::get_offset_of_feedPollInterval_4(),
	Feed_t3408144164::get_offset_of_feedPolled_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5517 = { sizeof (U3CUpdateFeedU3Ec__Iterator0_t75471835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5517[5] = 
{
	U3CUpdateFeedU3Ec__Iterator0_t75471835::get_offset_of_U3CloopedU3E__0_0(),
	U3CUpdateFeedU3Ec__Iterator0_t75471835::get_offset_of_U24this_1(),
	U3CUpdateFeedU3Ec__Iterator0_t75471835::get_offset_of_U24current_2(),
	U3CUpdateFeedU3Ec__Iterator0_t75471835::get_offset_of_U24disposing_3(),
	U3CUpdateFeedU3Ec__Iterator0_t75471835::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5518 = { sizeof (FeedManager_t2709865823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5518[2] = 
{
	FeedManager_t2709865823::get_offset_of_connectionAvailable_3(),
	FeedManager_t2709865823::get_offset_of_feedStore_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5519 = { sizeof (UniversalUpdateType_t1629501440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5519[33] = 
{
	UniversalUpdateType_t1629501440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5520 = { sizeof (AutoFriendType_t103565391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5520[3] = 
{
	AutoFriendType_t103565391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5521 = { sizeof (UniversalFeed_t2144776909), -1, sizeof(UniversalFeed_t2144776909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5521[10] = 
{
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_10(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_11(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_12(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_13(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_14(),
	UniversalFeed_t2144776909_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5522 = { sizeof (Friend_t3555014108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5522[9] = 
{
	Friend_t3555014108::get_offset_of_friendRequest_41(),
	Friend_t3555014108::get_offset_of_incomingFriendRequest_42(),
	Friend_t3555014108::get_offset_of_outgoingFriendRequest_43(),
	Friend_t3555014108::get_offset_of_incomingBlocked_44(),
	Friend_t3555014108::get_offset_of_outgoingBlocked_45(),
	Friend_t3555014108::get_offset_of_globalFriend_46(),
	Friend_t3555014108::get_offset_of_innerFriend_47(),
	Friend_t3555014108::get_offset_of_innerFriendID_48(),
	Friend_t3555014108::get_offset_of_strikeFirstPoints_49(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5523 = { sizeof (Player_t1147783557), -1, sizeof(Player_t1147783557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5523[21] = 
{
	Player_t1147783557::get_offset_of_innerFriendsList_41(),
	Player_t1147783557::get_offset_of_outerFriendsList_42(),
	Player_t1147783557::get_offset_of_incPendingFriendsList_43(),
	Player_t1147783557::get_offset_of_outPendingFriendsList_44(),
	Player_t1147783557::get_offset_of_blockList_45(),
	Player_t1147783557::get_offset_of_incLaunchedItems_46(),
	Player_t1147783557::get_offset_of_outLaunchedItems_47(),
	Player_t1147783557::get_offset_of_chatThreadsList_48(),
	Player_t1147783557::get_offset_of_allowIGNotifications_49(),
	Player_t1147783557::get_offset_of_allowIGNSound_50(),
	Player_t1147783557::get_offset_of_allowIGNVibration_51(),
	Player_t1147783557::get_offset_of_bucks_52(),
	Player_t1147783557::get_offset_of_currentFuel_53(),
	Player_t1147783557::get_offset_of_latestInnerList_54(),
	Player_t1147783557::get_offset_of_mobileRegion_55(),
	Player_t1147783557::get_offset_of_nemesisID_56(),
	Player_t1147783557::get_offset_of_pointsReset_57(),
	Player_t1147783557_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_58(),
	Player_t1147783557_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_59(),
	Player_t1147783557_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_60(),
	Player_t1147783557_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5524 = { sizeof (U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5524[2] = 
{
	U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968::get_offset_of_pair_0(),
	U3CUpdateInnerFriendsListU3Ec__AnonStorey1_t395413968::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5525 = { sizeof (U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967), -1, sizeof(U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5525[3] = 
{
	U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967::get_offset_of_friend_0(),
	U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967::get_offset_of_U3CU3Ef__refU241_1(),
	U3CUpdateInnerFriendsListU3Ec__AnonStorey0_t395413967_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5526 = { sizeof (Gender_t3618070753)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5526[3] = 
{
	Gender_t3618070753::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5527 = { sizeof (Audience_t3183951146)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5527[4] = 
{
	Audience_t3183951146::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5528 = { sizeof (User_t719925459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5528[41] = 
{
	User_t719925459::get_offset_of_userName_0(),
	User_t719925459::get_offset_of_fullName_1(),
	User_t719925459::get_offset_of_firstName_2(),
	User_t719925459::get_offset_of_secondName_3(),
	User_t719925459::get_offset_of_email_4(),
	User_t719925459::get_offset_of_ID_5(),
	User_t719925459::get_offset_of_profileImageURL_6(),
	User_t719925459::get_offset_of_rankName_7(),
	User_t719925459::get_offset_of_rankLevel_8(),
	User_t719925459::get_offset_of_userGender_9(),
	User_t719925459::get_offset_of_userAvatar_10(),
	User_t719925459::get_offset_of_loadingAvatar_11(),
	User_t719925459::get_offset_of_userAudience_12(),
	User_t719925459::get_offset_of_allowLocation_13(),
	User_t719925459::get_offset_of_latitude_14(),
	User_t719925459::get_offset_of_longitude_15(),
	User_t719925459::get_offset_of_currentMissileShield_16(),
	User_t719925459::get_offset_of_currentMineShield_17(),
	User_t719925459::get_offset_of_inStealth_18(),
	User_t719925459::get_offset_of_stealthDuration_19(),
	User_t719925459::get_offset_of_stealthStartHour_20(),
	User_t719925459::get_offset_of_stealthStartMin_21(),
	User_t719925459::get_offset_of_deactivatesStealthAt_22(),
	User_t719925459::get_offset_of_activatesStealthAt_23(),
	User_t719925459::get_offset_of_lastStealthReset_24(),
	User_t719925459::get_offset_of_dailyPoints_25(),
	User_t719925459::get_offset_of_totalPoints_26(),
	User_t719925459::get_offset_of_knockedOut_27(),
	User_t719925459::get_offset_of_knockedOutUntil_28(),
	User_t719925459::get_offset_of_knockedOutByID_29(),
	User_t719925459::get_offset_of_knockedOutByName_30(),
	User_t719925459::get_offset_of_mobileHash_31(),
	User_t719925459::get_offset_of_missilesFired_32(),
	User_t719925459::get_offset_of_missilesSuccessRate_33(),
	User_t719925459::get_offset_of_attackDirectHits_34(),
	User_t719925459::get_offset_of_incomingAttacks_35(),
	User_t719925459::get_offset_of_hitBy_36(),
	User_t719925459::get_offset_of_missilesDefenceRate_37(),
	User_t719925459::get_offset_of_minesLaid_38(),
	User_t719925459::get_offset_of_minesTriggerred_39(),
	User_t719925459::get_offset_of_minesDefused_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5529 = { sizeof (PrefabResource_t926725558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5529[7] = 
{
	PrefabResource_t926725558::get_offset_of_basePath_3(),
	PrefabResource_t926725558::get_offset_of_clipPanel_4(),
	PrefabResource_t926725558::get_offset_of_attackPanel_5(),
	PrefabResource_t926725558::get_offset_of_friendsPanel_6(),
	PrefabResource_t926725558::get_offset_of_storePanel_7(),
	PrefabResource_t926725558::get_offset_of_statusPanel_8(),
	PrefabResource_t926725558::get_offset_of_leaderBoardPanel_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5530 = { sizeof (SFXButton_t792651341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5530[3] = 
{
	SFXButton_t792651341::get_offset_of_playSFX_2(),
	SFXButton_t792651341::get_offset_of_sfxToPlay_3(),
	SFXButton_t792651341::get_offset_of_collision_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5531 = { sizeof (ChatContact_t3760700014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5531[5] = 
{
	ChatContact_t3760700014::get_offset_of_alphaSeperator_0(),
	ChatContact_t3760700014::get_offset_of_contactFirstName_1(),
	ChatContact_t3760700014::get_offset_of_contactLastName_2(),
	ChatContact_t3760700014::get_offset_of_contactUsername_3(),
	ChatContact_t3760700014::get_offset_of_contactID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5532 = { sizeof (ChatContactsILP_t1780035494), -1, sizeof(ChatContactsILP_t1780035494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5532[5] = 
{
	ChatContactsILP_t1780035494::get_offset_of_listLoaded_33(),
	ChatContactsILP_t1780035494::get_offset_of_chatContactsArray_34(),
	ChatContactsILP_t1780035494::get_offset_of_seperatorCellHeight_35(),
	ChatContactsILP_t1780035494::get_offset_of_noResultsLabel_36(),
	ChatContactsILP_t1780035494_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5533 = { sizeof (ChatContactsListItem_t4002571236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5533[15] = 
{
	ChatContactsListItem_t4002571236::get_offset_of_listPopulator_10(),
	ChatContactsListItem_t4002571236::get_offset_of_contact_11(),
	ChatContactsListItem_t4002571236::get_offset_of_firstNameLabel_12(),
	ChatContactsListItem_t4002571236::get_offset_of_lastNameLabel_13(),
	ChatContactsListItem_t4002571236::get_offset_of_usernameLabel_14(),
	ChatContactsListItem_t4002571236::get_offset_of_seperator_15(),
	ChatContactsListItem_t4002571236::get_offset_of_seperatorPos_16(),
	ChatContactsListItem_t4002571236::get_offset_of_halfSeperatorPos_17(),
	ChatContactsListItem_t4002571236::get_offset_of_seperatorColour_18(),
	ChatContactsListItem_t4002571236::get_offset_of_scrollView_19(),
	ChatContactsListItem_t4002571236::get_offset_of_draggablePanel_20(),
	ChatContactsListItem_t4002571236::get_offset_of_mTrans_21(),
	ChatContactsListItem_t4002571236::get_offset_of_mScroll_22(),
	ChatContactsListItem_t4002571236::get_offset_of_mAutoFind_23(),
	ChatContactsListItem_t4002571236::get_offset_of_mStarted_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5534 = { sizeof (ChatListType_t844669534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5534[4] = 
{
	ChatListType_t844669534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5535 = { sizeof (ChatManager_t2792590695), -1, sizeof(ChatManager_t2792590695_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5535[15] = 
{
	ChatManager_t2792590695::get_offset_of_threadsList_3(),
	ChatManager_t2792590695::get_offset_of_messagesList_4(),
	ChatManager_t2792590695::get_offset_of_contatcsList_5(),
	ChatManager_t2792590695::get_offset_of_newMessage_6(),
	ChatManager_t2792590695::get_offset_of_additionalContact_7(),
	ChatManager_t2792590695::get_offset_of_activeThread_8(),
	ChatManager_t2792590695::get_offset_of_activeContacts_9(),
	ChatManager_t2792590695::get_offset_of_localUserSearchList_10(),
	ChatManager_t2792590695::get_offset_of_contactsListUpdated_11(),
	ChatManager_t2792590695::get_offset_of_threadsListUpdated_12(),
	ChatManager_t2792590695::get_offset_of_currentSearchTerm_13(),
	ChatManager_t2792590695_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
	ChatManager_t2792590695_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_15(),
	ChatManager_t2792590695_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_16(),
	ChatManager_t2792590695_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5536 = { sizeof (ChatMessageILP_t4258019024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5536[6] = 
{
	ChatMessageILP_t4258019024::get_offset_of_listLoaded_33(),
	ChatMessageILP_t4258019024::get_offset_of_chatMessageArray_34(),
	ChatMessageILP_t4258019024::get_offset_of_FriendColour_35(),
	ChatMessageILP_t4258019024::get_offset_of_playerColour_36(),
	ChatMessageILP_t4258019024::get_offset_of_latestDateTime_37(),
	ChatMessageILP_t4258019024::get_offset_of_maxBackgroundWidth_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5537 = { sizeof (ChatMessageListItem_t2425051210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5537[15] = 
{
	ChatMessageListItem_t2425051210::get_offset_of_friendItem_10(),
	ChatMessageListItem_t2425051210::get_offset_of_listPopulator_11(),
	ChatMessageListItem_t2425051210::get_offset_of_itemMessage_12(),
	ChatMessageListItem_t2425051210::get_offset_of_friendUser_13(),
	ChatMessageListItem_t2425051210::get_offset_of_friendUserLabel_14(),
	ChatMessageListItem_t2425051210::get_offset_of_friendUserUnderline_15(),
	ChatMessageListItem_t2425051210::get_offset_of_messageLabel_16(),
	ChatMessageListItem_t2425051210::get_offset_of_timeLabel_17(),
	ChatMessageListItem_t2425051210::get_offset_of_dateLabel_18(),
	ChatMessageListItem_t2425051210::get_offset_of_scrollView_19(),
	ChatMessageListItem_t2425051210::get_offset_of_draggablePanel_20(),
	ChatMessageListItem_t2425051210::get_offset_of_mTrans_21(),
	ChatMessageListItem_t2425051210::get_offset_of_mScroll_22(),
	ChatMessageListItem_t2425051210::get_offset_of_mAutoFind_23(),
	ChatMessageListItem_t2425051210::get_offset_of_mStarted_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5538 = { sizeof (ChatNavigateBackwardButton_t516150148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5538[2] = 
{
	ChatNavigateBackwardButton_t516150148::get_offset_of_sfxToPlay_2(),
	ChatNavigateBackwardButton_t516150148::get_offset_of_buttonSprite_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5539 = { sizeof (ChatNavigateForwardButton_t4025092154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5539[2] = 
{
	ChatNavigateForwardButton_t4025092154::get_offset_of_navScreen_5(),
	ChatNavigateForwardButton_t4025092154::get_offset_of_additionalContact_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5540 = { sizeof (ChatNavScreen_t42377261)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5540[5] = 
{
	ChatNavScreen_t42377261::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5541 = { sizeof (ChatNavigationController_t880507216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5541[9] = 
{
	ChatNavigationController_t880507216::get_offset_of_titleLabel_8(),
	ChatNavigationController_t880507216::get_offset_of_threadsObject_9(),
	ChatNavigationController_t880507216::get_offset_of_messageObject_10(),
	ChatNavigationController_t880507216::get_offset_of_contactsObject_11(),
	ChatNavigationController_t880507216::get_offset_of_mainNavigateBackwardButton_12(),
	ChatNavigationController_t880507216::get_offset_of_mainNavigateForwardButton_13(),
	ChatNavigationController_t880507216::get_offset_of_activePos_14(),
	ChatNavigationController_t880507216::get_offset_of_leftInactivePos_15(),
	ChatNavigationController_t880507216::get_offset_of_rightInactivePos_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5542 = { sizeof (ChatSceneManager_t555354597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5542[2] = 
{
	ChatSceneManager_t555354597::get_offset_of_ChatCenterUI_17(),
	ChatSceneManager_t555354597::get_offset_of_SceneLoader_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5543 = { sizeof (ChatThreadSwipe_t397952508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5543[5] = 
{
	ChatThreadSwipe_t397952508::get_offset_of_listItem_10(),
	ChatThreadSwipe_t397952508::get_offset_of_deleteContent_11(),
	ChatThreadSwipe_t397952508::get_offset_of_posTween_12(),
	ChatThreadSwipe_t397952508::get_offset_of_uiclosing_13(),
	ChatThreadSwipe_t397952508::get_offset_of_deleteOpen_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5544 = { sizeof (ChatThreadsILP_t2341903446), -1, sizeof(ChatThreadsILP_t2341903446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5544[7] = 
{
	ChatThreadsILP_t2341903446::get_offset_of_listLoaded_33(),
	ChatThreadsILP_t2341903446::get_offset_of_chatThreadArray_34(),
	ChatThreadsILP_t2341903446::get_offset_of_tableTween_35(),
	ChatThreadsILP_t2341903446::get_offset_of_newMessageColour_36(),
	ChatThreadsILP_t2341903446::get_offset_of_defaultColour_37(),
	ChatThreadsILP_t2341903446_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_38(),
	ChatThreadsILP_t2341903446_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5545 = { sizeof (U3CUpdateItemDataU3Ec__AnonStorey0_t1678493984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5545[1] = 
{
	U3CUpdateItemDataU3Ec__AnonStorey0_t1678493984::get_offset_of_newMessage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5546 = { sizeof (ChatThreadsListItem_t303868848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5546[17] = 
{
	ChatThreadsListItem_t303868848::get_offset_of_listPopulator_10(),
	ChatThreadsListItem_t303868848::get_offset_of_itemThread_11(),
	ChatThreadsListItem_t303868848::get_offset_of_contatcsLabel_12(),
	ChatThreadsListItem_t303868848::get_offset_of_contactsString_13(),
	ChatThreadsListItem_t303868848::get_offset_of_messageDateLabel_14(),
	ChatThreadsListItem_t303868848::get_offset_of_latestMessageLabel_15(),
	ChatThreadsListItem_t303868848::get_offset_of_rankLabel_16(),
	ChatThreadsListItem_t303868848::get_offset_of_newMessageObject_17(),
	ChatThreadsListItem_t303868848::get_offset_of_avatarUpdater_18(),
	ChatThreadsListItem_t303868848::get_offset_of_groupChatProfilePic_19(),
	ChatThreadsListItem_t303868848::get_offset_of_playerProfile_20(),
	ChatThreadsListItem_t303868848::get_offset_of_scrollView_21(),
	ChatThreadsListItem_t303868848::get_offset_of_draggablePanel_22(),
	ChatThreadsListItem_t303868848::get_offset_of_mTrans_23(),
	ChatThreadsListItem_t303868848::get_offset_of_mScroll_24(),
	ChatThreadsListItem_t303868848::get_offset_of_mAutoFind_25(),
	ChatThreadsListItem_t303868848::get_offset_of_mStarted_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5547 = { sizeof (ContactsNavScreen_t1794286542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5547[3] = 
{
	ContactsNavScreen_t1794286542::get_offset_of_contactsILP_3(),
	ContactsNavScreen_t1794286542::get_offset_of_searchInput_4(),
	ContactsNavScreen_t1794286542::get_offset_of_contactsUIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5548 = { sizeof (ContactsSearchInput_t2789723005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5548[1] = 
{
	ContactsSearchInput_t2789723005::get_offset_of_searchInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5549 = { sizeof (DeleteThreadButton_t3881568975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5549[2] = 
{
	DeleteThreadButton_t3881568975::get_offset_of_listItem_5(),
	DeleteThreadButton_t3881568975::get_offset_of_listItemSwipe_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5550 = { sizeof (KeyboardSendButton_t1707295269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5550[1] = 
{
	KeyboardSendButton_t1707295269::get_offset_of_keyboardInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5551 = { sizeof (MessageKeyboardInput_t4224908926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5551[1] = 
{
	MessageKeyboardInput_t4224908926::get_offset_of_keyboardInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5552 = { sizeof (MessageNavScreen_t601131416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5552[2] = 
{
	MessageNavScreen_t601131416::get_offset_of_messageContactsLabel_3(),
	MessageNavScreen_t601131416::get_offset_of_messageUIclosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5553 = { sizeof (ThreadsNavScreen_t4034840106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5553[3] = 
{
	ThreadsNavScreen_t4034840106::get_offset_of_tableTween_3(),
	ThreadsNavScreen_t4034840106::get_offset_of_threadsUIclosing_4(),
	ThreadsNavScreen_t4034840106::get_offset_of_ChatUIclosing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5554 = { sizeof (ChatCenterUIScaler_t436229885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5554[23] = 
{
	ChatCenterUIScaler_t436229885::get_offset_of_background_14(),
	ChatCenterUIScaler_t436229885::get_offset_of_titleSeperator_15(),
	ChatCenterUIScaler_t436229885::get_offset_of_navigateBackButton_16(),
	ChatCenterUIScaler_t436229885::get_offset_of_newChatButton_17(),
	ChatCenterUIScaler_t436229885::get_offset_of_threadsPanel_18(),
	ChatCenterUIScaler_t436229885::get_offset_of_messagePanel_19(),
	ChatCenterUIScaler_t436229885::get_offset_of_messageTable_20(),
	ChatCenterUIScaler_t436229885::get_offset_of_messageToLabel_21(),
	ChatCenterUIScaler_t436229885::get_offset_of_messageContactsLabel_22(),
	ChatCenterUIScaler_t436229885::get_offset_of_messagePlusButtonCollider_23(),
	ChatCenterUIScaler_t436229885::get_offset_of_messagePlusLabel_24(),
	ChatCenterUIScaler_t436229885::get_offset_of_messageSeperator_25(),
	ChatCenterUIScaler_t436229885::get_offset_of_keyboardCollider_26(),
	ChatCenterUIScaler_t436229885::get_offset_of_keyboardBackground_27(),
	ChatCenterUIScaler_t436229885::get_offset_of_keyboardForeground_28(),
	ChatCenterUIScaler_t436229885::get_offset_of_keyboardLabel_29(),
	ChatCenterUIScaler_t436229885::get_offset_of_sendCollider_30(),
	ChatCenterUIScaler_t436229885::get_offset_of_sendLabel_31(),
	ChatCenterUIScaler_t436229885::get_offset_of_searchCollider_32(),
	ChatCenterUIScaler_t436229885::get_offset_of_searchLabel_33(),
	ChatCenterUIScaler_t436229885::get_offset_of_searchButton_34(),
	ChatCenterUIScaler_t436229885::get_offset_of_contactsSeperator_35(),
	ChatCenterUIScaler_t436229885::get_offset_of_contactsPanel_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5555 = { sizeof (ChatContactsListItemUIScaler_t2894863298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5555[5] = 
{
	ChatContactsListItemUIScaler_t2894863298::get_offset_of_background_14(),
	ChatContactsListItemUIScaler_t2894863298::get_offset_of_backgroundCollider_15(),
	ChatContactsListItemUIScaler_t2894863298::get_offset_of_firstnameLabel_16(),
	ChatContactsListItemUIScaler_t2894863298::get_offset_of_usernameLabel_17(),
	ChatContactsListItemUIScaler_t2894863298::get_offset_of_seperator_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5556 = { sizeof (ThreadListItemUIScaler_t2282969193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5556[10] = 
{
	ThreadListItemUIScaler_t2282969193::get_offset_of_itemTween_14(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_background_15(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_backgroundCollider_16(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_profilePicture_17(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_contactsLabel_18(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_dateLabel_19(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_lastMessageLabel_20(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_rankLabel_21(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_newMessageObject_22(),
	ThreadListItemUIScaler_t2282969193::get_offset_of_deleteObject_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5557 = { sizeof (ActivateUserKnockoutButton_t3686915466), -1, sizeof(ActivateUserKnockoutButton_t3686915466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5557[3] = 
{
	ActivateUserKnockoutButton_t3686915466::get_offset_of_durationMins_5(),
	ActivateUserKnockoutButton_t3686915466::get_offset_of_userID_6(),
	ActivateUserKnockoutButton_t3686915466_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5558 = { sizeof (ActivateUserStealthModeButton_t2608034790), -1, sizeof(ActivateUserStealthModeButton_t2608034790_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5558[3] = 
{
	ActivateUserStealthModeButton_t2608034790::get_offset_of_delayMins_5(),
	ActivateUserStealthModeButton_t2608034790::get_offset_of_durationHours_6(),
	ActivateUserStealthModeButton_t2608034790_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5559 = { sizeof (DeactivateShield_t2656914733), -1, sizeof(DeactivateShield_t2656914733_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5559[3] = 
{
	DeactivateShield_t2656914733::get_offset_of_missileShield_5(),
	DeactivateShield_t2656914733_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	DeactivateShield_t2656914733_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5560 = { sizeof (PingServer_t1218551355), -1, sizeof(PingServer_t1218551355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5560[1] = 
{
	PingServer_t1218551355_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5561 = { sizeof (SetTotalPoints_t1575563713), -1, sizeof(SetTotalPoints_t1575563713_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5561[2] = 
{
	SetTotalPoints_t1575563713::get_offset_of_pointsToSet_5(),
	SetTotalPoints_t1575563713_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5562 = { sizeof (ChatmageddonGamestate_t808689860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5563 = { sizeof (AttackListType_t2861798804)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5563[6] = 
{
	AttackListType_t2861798804::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5564 = { sizeof (AttackManager_t3475553125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5564[11] = 
{
	AttackManager_t3475553125::get_offset_of_globalSearchList_3(),
	AttackManager_t3475553125::get_offset_of_friendsSearchList_4(),
	AttackManager_t3475553125::get_offset_of_worldFriendsList_5(),
	AttackManager_t3475553125::get_offset_of_globalUserSearchList_6(),
	AttackManager_t3475553125::get_offset_of_globalListUpdated_7(),
	AttackManager_t3475553125::get_offset_of_localUserSearchList_8(),
	AttackManager_t3475553125::get_offset_of_localListUpdated_9(),
	AttackManager_t3475553125::get_offset_of_innerListUpdated_10(),
	AttackManager_t3475553125::get_offset_of_outerListUpdated_11(),
	AttackManager_t3475553125::get_offset_of_worldListUpdated_12(),
	AttackManager_t3475553125::get_offset_of_phoneNumberUI_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5565 = { sizeof (U3CSendFriendRequestU3Ec__AnonStorey0_t2978818532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5565[2] = 
{
	U3CSendFriendRequestU3Ec__AnonStorey0_t2978818532::get_offset_of_friend_0(),
	U3CSendFriendRequestU3Ec__AnonStorey0_t2978818532::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5566 = { sizeof (U3CAcceptFriendRequestU3Ec__AnonStorey1_t658574309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5566[2] = 
{
	U3CAcceptFriendRequestU3Ec__AnonStorey1_t658574309::get_offset_of_friend_0(),
	U3CAcceptFriendRequestU3Ec__AnonStorey1_t658574309::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5567 = { sizeof (U3CDeclineFriendRequestU3Ec__AnonStorey2_t1271168958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5567[2] = 
{
	U3CDeclineFriendRequestU3Ec__AnonStorey2_t1271168958::get_offset_of_friend_0(),
	U3CDeclineFriendRequestU3Ec__AnonStorey2_t1271168958::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5568 = { sizeof (AttackNavigateBackwardsButton_t4019147023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5568[1] = 
{
	AttackNavigateBackwardsButton_t4019147023::get_offset_of_buttonSprite_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5569 = { sizeof (AttackNavigateForwardsButton_t1401782687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5569[4] = 
{
	AttackNavigateForwardsButton_t1401782687::get_offset_of_navScreen_5(),
	AttackNavigateForwardsButton_t1401782687::get_offset_of_activeColor_6(),
	AttackNavigateForwardsButton_t1401782687::get_offset_of_inActiveColor_7(),
	AttackNavigateForwardsButton_t1401782687::get_offset_of_buttonSprite_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5570 = { sizeof (AttackNavScreen_t36759459)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5570[7] = 
{
	AttackNavScreen_t36759459::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5571 = { sizeof (AttackNavigationController_t2145483014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5571[12] = 
{
	AttackNavigationController_t2145483014::get_offset_of_titleLabel_8(),
	AttackNavigationController_t2145483014::get_offset_of_homeObject_9(),
	AttackNavigationController_t2145483014::get_offset_of_inventoryObject_10(),
	AttackNavigationController_t2145483014::get_offset_of_reviewObject_11(),
	AttackNavigationController_t2145483014::get_offset_of_searchObject_12(),
	AttackNavigationController_t2145483014::get_offset_of_attackGameObject_13(),
	AttackNavigationController_t2145483014::get_offset_of_mainNavigateBackwardButton_14(),
	AttackNavigationController_t2145483014::get_offset_of_mainNavigateForwardButton_15(),
	AttackNavigationController_t2145483014::get_offset_of_activePos_16(),
	AttackNavigationController_t2145483014::get_offset_of_leftInactivePos_17(),
	AttackNavigationController_t2145483014::get_offset_of_rightInactivePos_18(),
	AttackNavigationController_t2145483014::get_offset_of_inputBlock_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5572 = { sizeof (U3CFirstLoadWaitU3Ec__Iterator0_t2076676339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5572[4] = 
{
	U3CFirstLoadWaitU3Ec__Iterator0_t2076676339::get_offset_of_U24this_0(),
	U3CFirstLoadWaitU3Ec__Iterator0_t2076676339::get_offset_of_U24current_1(),
	U3CFirstLoadWaitU3Ec__Iterator0_t2076676339::get_offset_of_U24disposing_2(),
	U3CFirstLoadWaitU3Ec__Iterator0_t2076676339::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5573 = { sizeof (AttackTargetsButton_t1016182822), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5574 = { sizeof (AimQuadrent_t3168938451)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5574[5] = 
{
	AimQuadrent_t3168938451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5575 = { sizeof (AimMarker_t327093161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5575[27] = 
{
	AimMarker_t327093161::get_offset_of_backfireRing_2(),
	AimMarker_t327093161::get_offset_of_perfectLaunchRing_3(),
	AimMarker_t327093161::get_offset_of_pointsAvailableLabel_4(),
	AimMarker_t327093161::get_offset_of_latLabel_5(),
	AimMarker_t327093161::get_offset_of_longLabel_6(),
	AimMarker_t327093161::get_offset_of_targetAimer_7(),
	AimMarker_t327093161::get_offset_of_parentRotator_8(),
	AimMarker_t327093161::get_offset_of_targetRadius_9(),
	AimMarker_t327093161::get_offset_of_playAnim_10(),
	AimMarker_t327093161::get_offset_of_m_Speed_11(),
	AimMarker_t327093161::get_offset_of_m_XScale_12(),
	AimMarker_t327093161::get_offset_of_m_YScale_13(),
	AimMarker_t327093161::get_offset_of_m_Pivot_14(),
	AimMarker_t327093161::get_offset_of_m_PivotOffset_15(),
	AimMarker_t327093161::get_offset_of_m_Phase_16(),
	AimMarker_t327093161::get_offset_of_m_Invert_17(),
	AimMarker_t327093161::get_offset_of_previousInvert_18(),
	AimMarker_t327093161::get_offset_of_m_2PI_19(),
	AimMarker_t327093161::get_offset_of_outsideRadius_20(),
	AimMarker_t327093161::get_offset_of_insideRadius_21(),
	AimMarker_t327093161::get_offset_of_insideOffset_22(),
	AimMarker_t327093161::get_offset_of_pointsPerDraw_23(),
	AimMarker_t327093161::get_offset_of_loopSpirograph_24(),
	AimMarker_t327093161::get_offset_of_smoothTimer_25(),
	AimMarker_t327093161::get_offset_of_spirographRoutine_26(),
	AimMarker_t327093161::get_offset_of_difference_27(),
	AimMarker_t327093161::get_offset_of_previousPercent_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5576 = { sizeof (U3CUpdatePositionUIRoutineU3Ec__Iterator0_t189874335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5576[4] = 
{
	U3CUpdatePositionUIRoutineU3Ec__Iterator0_t189874335::get_offset_of_U24this_0(),
	U3CUpdatePositionUIRoutineU3Ec__Iterator0_t189874335::get_offset_of_U24current_1(),
	U3CUpdatePositionUIRoutineU3Ec__Iterator0_t189874335::get_offset_of_U24disposing_2(),
	U3CUpdatePositionUIRoutineU3Ec__Iterator0_t189874335::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5577 = { sizeof (U3CSpirographU3Ec__Iterator1_t3990386259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5577[10] = 
{
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U3CtU3E__0_0(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U3CdtU3E__1_1(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U3Cmax_tU3E__2_2(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U3Cx1U3E__3_3(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U3Cy1U3E__4_4(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U3CpointU3E__5_5(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U24this_6(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U24current_7(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U24disposing_8(),
	U3CSpirographU3Ec__Iterator1_t3990386259::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5578 = { sizeof (AttackCancelGameButton_t2121269608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5579 = { sizeof (AttackGameNavScreen_t4158424117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5579[4] = 
{
	AttackGameNavScreen_t4158424117::get_offset_of_avatarUpdater_3(),
	AttackGameNavScreen_t4158424117::get_offset_of_gameController_4(),
	AttackGameNavScreen_t4158424117::get_offset_of_backgroundTexture_5(),
	AttackGameNavScreen_t4158424117::get_offset_of_gameUIClosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5580 = { sizeof (AttackGameState_t4284764851)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5580[4] = 
{
	AttackGameState_t4284764851::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5581 = { sizeof (AttackMiniGameController_t105133407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5581[10] = 
{
	AttackMiniGameController_t105133407::get_offset_of_currentState_4(),
	AttackMiniGameController_t105133407::get_offset_of_aimMarker_5(),
	AttackMiniGameController_t105133407::get_offset_of_stateButton_6(),
	AttackMiniGameController_t105133407::get_offset_of_aimAccuracy_7(),
	AttackMiniGameController_t105133407::get_offset_of_fireAccuracy_8(),
	AttackMiniGameController_t105133407::get_offset_of_totalAccuracy_9(),
	AttackMiniGameController_t105133407::get_offset_of_perfectLaunch_10(),
	AttackMiniGameController_t105133407::get_offset_of_backfireLaunch_11(),
	AttackMiniGameController_t105133407::get_offset_of_labelPopulator_12(),
	AttackMiniGameController_t105133407::get_offset_of_laController_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5582 = { sizeof (U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5582[5] = 
{
	U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844::get_offset_of_standard_0(),
	U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844::get_offset_of_U24this_1(),
	U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844::get_offset_of_U24current_2(),
	U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844::get_offset_of_U24disposing_3(),
	U3CWaitToLaunchMissileU3Ec__Iterator0_t2230046844::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5583 = { sizeof (U3CLaunchMissileU3Ec__AnonStorey2_t265198302), -1, sizeof(U3CLaunchMissileU3Ec__AnonStorey2_t265198302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5583[4] = 
{
	U3CLaunchMissileU3Ec__AnonStorey2_t265198302::get_offset_of_inventoryMissile_0(),
	U3CLaunchMissileU3Ec__AnonStorey2_t265198302::get_offset_of_standard_1(),
	U3CLaunchMissileU3Ec__AnonStorey2_t265198302::get_offset_of_U24this_2(),
	U3CLaunchMissileU3Ec__AnonStorey2_t265198302_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5584 = { sizeof (U3CwaitU3Ec__Iterator1_t4214048921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5584[3] = 
{
	U3CwaitU3Ec__Iterator1_t4214048921::get_offset_of_U24current_0(),
	U3CwaitU3Ec__Iterator1_t4214048921::get_offset_of_U24disposing_1(),
	U3CwaitU3Ec__Iterator1_t4214048921::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5585 = { sizeof (FireGuage_t4001636259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5585[5] = 
{
	FireGuage_t4001636259::get_offset_of_firePanel_2(),
	FireGuage_t4001636259::get_offset_of_fireBackground_3(),
	FireGuage_t4001636259::get_offset_of_fireRoutine_4(),
	FireGuage_t4001636259::get_offset_of_guageSpeed_5(),
	FireGuage_t4001636259::get_offset_of_forward_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5586 = { sizeof (U3CMoveFireGuageU3Ec__Iterator0_t520811751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5586[6] = 
{
	U3CMoveFireGuageU3Ec__Iterator0_t520811751::get_offset_of_U3CoffsetU3E__0_0(),
	U3CMoveFireGuageU3Ec__Iterator0_t520811751::get_offset_of_U3CvalueIncrementU3E__1_1(),
	U3CMoveFireGuageU3Ec__Iterator0_t520811751::get_offset_of_U24this_2(),
	U3CMoveFireGuageU3Ec__Iterator0_t520811751::get_offset_of_U24current_3(),
	U3CMoveFireGuageU3Ec__Iterator0_t520811751::get_offset_of_U24disposing_4(),
	U3CMoveFireGuageU3Ec__Iterator0_t520811751::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5587 = { sizeof (LaunchAccuracyController_t4092159714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5587[3] = 
{
	LaunchAccuracyController_t4092159714::get_offset_of_accuracyLabel_2(),
	LaunchAccuracyController_t4092159714::get_offset_of_posTween_3(),
	LaunchAccuracyController_t4092159714::get_offset_of_flash_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5588 = { sizeof (U3CFlashRoutineU3Ec__Iterator0_t3216582880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5588[4] = 
{
	U3CFlashRoutineU3Ec__Iterator0_t3216582880::get_offset_of_U24this_0(),
	U3CFlashRoutineU3Ec__Iterator0_t3216582880::get_offset_of_U24current_1(),
	U3CFlashRoutineU3Ec__Iterator0_t3216582880::get_offset_of_U24disposing_2(),
	U3CFlashRoutineU3Ec__Iterator0_t3216582880::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5589 = { sizeof (HomeBottomUIScaler_t3171336006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5589[1] = 
{
	HomeBottomUIScaler_t3171336006::get_offset_of_background_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5590 = { sizeof (AttackAcceptButton_t1767177984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5590[1] = 
{
	AttackAcceptButton_t1767177984::get_offset_of_item_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5591 = { sizeof (AttackChatButton_t4176739546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5591[1] = 
{
	AttackChatButton_t4176739546::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5592 = { sizeof (AttackDeclineButton_t304169658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5592[1] = 
{
	AttackDeclineButton_t304169658::get_offset_of_item_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5593 = { sizeof (AttackFriendButton_t2212457944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5593[1] = 
{
	AttackFriendButton_t2212457944::get_offset_of_item_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5594 = { sizeof (AttackHomeNavScreen_t2473299220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5594[1] = 
{
	AttackHomeNavScreen_t2473299220::get_offset_of_homeUIclosing_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5595 = { sizeof (AttackHomeNavigateForwardButton_t3562703987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5595[1] = 
{
	AttackHomeNavigateForwardButton_t3562703987::get_offset_of_nextScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5596 = { sizeof (AttackHomeNav_t3944890504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5596[5] = 
{
	AttackHomeNav_t3944890504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5597 = { sizeof (AttackHomeNavigationController_t3047766999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5597[9] = 
{
	AttackHomeNavigationController_t3047766999::get_offset_of_innerTab_8(),
	AttackHomeNavigationController_t3047766999::get_offset_of_outerTab_9(),
	AttackHomeNavigationController_t3047766999::get_offset_of_worldTab_10(),
	AttackHomeNavigationController_t3047766999::get_offset_of_innerObject_11(),
	AttackHomeNavigationController_t3047766999::get_offset_of_outerObject_12(),
	AttackHomeNavigationController_t3047766999::get_offset_of_worldObject_13(),
	AttackHomeNavigationController_t3047766999::get_offset_of_activePos_14(),
	AttackHomeNavigationController_t3047766999::get_offset_of_leftInactivePos_15(),
	AttackHomeNavigationController_t3047766999::get_offset_of_rightInactivePos_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5598 = { sizeof (AttackILP_t2111768551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5598[8] = 
{
	AttackILP_t2111768551::get_offset_of_noResultsLabel_33(),
	AttackILP_t2111768551::get_offset_of_maxNameLength_34(),
	AttackILP_t2111768551::get_offset_of_listLoaded_35(),
	AttackILP_t2111768551::get_offset_of_attackArray_36(),
	AttackILP_t2111768551::get_offset_of_outerTargetList_37(),
	AttackILP_t2111768551::get_offset_of_searchList_38(),
	AttackILP_t2111768551::get_offset_of_globalSearchList_39(),
	AttackILP_t2111768551::get_offset_of_resetSearch_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5599 = { sizeof (AttackItemAttackButton_t1001821753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5599[1] = 
{
	AttackItemAttackButton_t1001821753::get_offset_of_item_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
