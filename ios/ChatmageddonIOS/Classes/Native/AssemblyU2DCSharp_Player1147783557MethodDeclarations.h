﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player
struct Player_t1147783557;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// Friend
struct Friend_t3555014108;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;
// System.Collections.Generic.List`1<ChatThread>
struct List_1_t1763444614;
// ChatThread
struct ChatThread_t2394323482;
// LaunchedItem
struct LaunchedItem_t3670634427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "AssemblyU2DCSharp_ChatThread2394323482.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427.h"

// System.Void Player::.ctor(System.Collections.Hashtable)
extern "C"  void Player__ctor_m951383864 (Player_t1147783557 * __this, Hashtable_t909839986 * ___playerHash0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend Player::CheckIfPlayerFriend(System.String)
extern "C"  Friend_t3555014108 * Player_CheckIfPlayerFriend_m3839000879 (Player_t1147783557 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Friend> Player::GetInnerFriendsList()
extern "C"  List_1_t2924135240 * Player_GetInnerFriendsList_m1362749698 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::UpdateInnerFriendsList(System.Collections.Hashtable,System.Boolean)
extern "C"  void Player_UpdateInnerFriendsList_m2357477435 (Player_t1147783557 * __this, Hashtable_t909839986 * ___innerList0, bool ___firstUpdate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AddInnerFriend(Friend)
extern "C"  void Player_AddInnerFriend_m237927135 (Player_t1147783557 * __this, Friend_t3555014108 * ___newFriend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RemoveInnerFriend(Friend)
extern "C"  void Player_RemoveInnerFriend_m3947356392 (Player_t1147783557 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetTotalInnerFriends()
extern "C"  int32_t Player_GetTotalInnerFriends_m2668827129 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Friend> Player::GetOuterFriendsList()
extern "C"  List_1_t2924135240 * Player_GetOuterFriendsList_m365500427 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AddOuterFriend(Friend)
extern "C"  void Player_AddOuterFriend_m3393695944 (Player_t1147783557 * __this, Friend_t3555014108 * ___newFriend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RemoveOuterFriend(Friend)
extern "C"  void Player_RemoveOuterFriend_m1389787099 (Player_t1147783557 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetTotalOuterFriends()
extern "C"  int32_t Player_GetTotalOuterFriends_m1005014146 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Friend> Player::GetIncPendingFriendsList()
extern "C"  List_1_t2924135240 * Player_GetIncPendingFriendsList_m3279042481 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AddIncPendingFriend(Friend)
extern "C"  void Player_AddIncPendingFriend_m3433020360 (Player_t1147783557 * __this, Friend_t3555014108 * ___newFriend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RemoveIncPendingFriend(Friend)
extern "C"  void Player_RemoveIncPendingFriend_m283898205 (Player_t1147783557 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetIncTotalPendingFriends()
extern "C"  int32_t Player_GetIncTotalPendingFriends_m398791768 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::ClearIncPendingUsers()
extern "C"  void Player_ClearIncPendingUsers_m1361283704 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Friend> Player::GetOutPendingFriendsList()
extern "C"  List_1_t2924135240 * Player_GetOutPendingFriendsList_m3751407959 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AddOutPendingFriend(Friend)
extern "C"  void Player_AddOutPendingFriend_m2284448344 (Player_t1147783557 * __this, Friend_t3555014108 * ___newFriend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RemoveOutPendingFriend(Friend)
extern "C"  void Player_RemoveOutPendingFriend_m1308657843 (Player_t1147783557 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetOutTotalPendingFriends()
extern "C"  int32_t Player_GetOutTotalPendingFriends_m2178358016 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Friend> Player::GetBlockedFriendsList()
extern "C"  List_1_t2924135240 * Player_GetBlockedFriendsList_m102595438 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::BlockFriend(Friend)
extern "C"  void Player_BlockFriend_m2261499785 (Player_t1147783557 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::UnblockFriend(Friend)
extern "C"  void Player_UnblockFriend_m1978911392 (Player_t1147783557 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetTotalBlockedFriends()
extern "C"  int32_t Player_GetTotalBlockedFriends_m3106424431 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::ClearBlockedUsers()
extern "C"  void Player_ClearBlockedUsers_m4009519667 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<ChatThread> Player::GetChatThreadsList()
extern "C"  List_1_t1763444614 * Player_GetChatThreadsList_m4149013694 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ChatThread Player::CheckIfThreadExists(System.String)
extern "C"  ChatThread_t2394323482 * Player_CheckIfThreadExists_m3548298694 (Player_t1147783557 * __this, String_t* ___threadID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AddChatThread(ChatThread)
extern "C"  void Player_AddChatThread_m3129642333 (Player_t1147783557 * __this, ChatThread_t2394323482 * ___newThread0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RemoveChatThread(ChatThread)
extern "C"  void Player_RemoveChatThread_m2306530182 (Player_t1147783557 * __this, ChatThread_t2394323482 * ___thread0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::ClearChatThreads()
extern "C"  void Player_ClearChatThreads_m4244954850 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetTotalChatThreads()
extern "C"  int32_t Player_GetTotalChatThreads_m1297118329 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AddBucks(System.Int32)
extern "C"  void Player_AddBucks_m1842278886 (Player_t1147783557 * __this, int32_t ___bucksToAdd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RemoveBucks(System.Int32)
extern "C"  void Player_RemoveBucks_m1975077235 (Player_t1147783557 * __this, int32_t ___bucksToRemove0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::SetBucks(System.Int32)
extern "C"  void Player_SetBucks_m2591811967 (Player_t1147783557 * __this, int32_t ___bucksToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetBucks()
extern "C"  int32_t Player_GetBucks_m2104085516 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::SetFuel(System.Int32)
extern "C"  void Player_SetFuel_m2965782601 (Player_t1147783557 * __this, int32_t ___fuelToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetCurrentFuel()
extern "C"  int32_t Player_GetCurrentFuel_m695965243 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AddLaunchedItem(LaunchedItem)
extern "C"  void Player_AddLaunchedItem_m233124113 (Player_t1147783557 * __this, LaunchedItem_t3670634427 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RemovedLaunchedItem(LaunchedItem)
extern "C"  void Player_RemovedLaunchedItem_m2256642706 (Player_t1147783557 * __this, LaunchedItem_t3670634427 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetIncStatusAmount()
extern "C"  int32_t Player_GetIncStatusAmount_m4205052798 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::SetAllowIGN(System.Boolean)
extern "C"  void Player_SetAllowIGN_m2878950388 (Player_t1147783557 * __this, bool ___ignToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player::GetAllowIGN()
extern "C"  bool Player_GetAllowIGN_m2838283383 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::SetAllowIGNSound(System.Boolean)
extern "C"  void Player_SetAllowIGNSound_m3717886615 (Player_t1147783557 * __this, bool ___ignToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player::GetAllowIGNSound()
extern "C"  bool Player_GetAllowIGNSound_m4171521592 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::SetAllowIGNVibration(System.Boolean)
extern "C"  void Player_SetAllowIGNVibration_m345609414 (Player_t1147783557 * __this, bool ___ignToSet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player::GetAllowIGNVibration()
extern "C"  bool Player_GetAllowIGNVibration_m1336232429 (Player_t1147783557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::<UpdateInnerFriendsList>m__0(Friend)
extern "C"  int32_t Player_U3CUpdateInnerFriendsListU3Em__0_m382399559 (Il2CppObject * __this /* static, unused */, Friend_t3555014108 * ___innerF0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Player::<AddOuterFriend>m__1(Friend)
extern "C"  String_t* Player_U3CAddOuterFriendU3Em__1_m361937983 (Il2CppObject * __this /* static, unused */, Friend_t3555014108 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::<AddLaunchedItem>m__2(LaunchedItem,LaunchedItem)
extern "C"  int32_t Player_U3CAddLaunchedItemU3Em__2_m3192887023 (Il2CppObject * __this /* static, unused */, LaunchedItem_t3670634427 * ___x0, LaunchedItem_t3670634427 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::<AddLaunchedItem>m__3(LaunchedItem,LaunchedItem)
extern "C"  int32_t Player_U3CAddLaunchedItemU3Em__3_m1981836404 (Il2CppObject * __this /* static, unused */, LaunchedItem_t3670634427 * ___x0, LaunchedItem_t3670634427 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
