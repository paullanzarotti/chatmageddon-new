﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// QuestionUI
struct QuestionUI_t3721436426;
// ContactUsUI
struct ContactUsUI_t1917862594;
// InstructionsUI
struct InstructionsUI_t253378877;
// EULAUI
struct EULAUI_t3168190395;
// PrivacyPolicyUI
struct PrivacyPolicyUI_t524063172;
// TermsOfServiceUI
struct TermsOfServiceUI_t3223215579;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextPageNavScreen
struct  TextPageNavScreen_t1592130049  : public NavigationScreen_t2333230110
{
public:
	// UIPanel TextPageNavScreen::scrollViewPanel
	UIPanel_t1795085332 * ___scrollViewPanel_3;
	// QuestionUI TextPageNavScreen::questionUI
	QuestionUI_t3721436426 * ___questionUI_4;
	// ContactUsUI TextPageNavScreen::contactUI
	ContactUsUI_t1917862594 * ___contactUI_5;
	// InstructionsUI TextPageNavScreen::instructionsUI
	InstructionsUI_t253378877 * ___instructionsUI_6;
	// EULAUI TextPageNavScreen::eulaUI
	EULAUI_t3168190395 * ___eulaUI_7;
	// PrivacyPolicyUI TextPageNavScreen::ppUI
	PrivacyPolicyUI_t524063172 * ___ppUI_8;
	// TermsOfServiceUI TextPageNavScreen::tosUI
	TermsOfServiceUI_t3223215579 * ___tosUI_9;
	// System.Boolean TextPageNavScreen::UIclosing
	bool ___UIclosing_10;

public:
	inline static int32_t get_offset_of_scrollViewPanel_3() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___scrollViewPanel_3)); }
	inline UIPanel_t1795085332 * get_scrollViewPanel_3() const { return ___scrollViewPanel_3; }
	inline UIPanel_t1795085332 ** get_address_of_scrollViewPanel_3() { return &___scrollViewPanel_3; }
	inline void set_scrollViewPanel_3(UIPanel_t1795085332 * value)
	{
		___scrollViewPanel_3 = value;
		Il2CppCodeGenWriteBarrier(&___scrollViewPanel_3, value);
	}

	inline static int32_t get_offset_of_questionUI_4() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___questionUI_4)); }
	inline QuestionUI_t3721436426 * get_questionUI_4() const { return ___questionUI_4; }
	inline QuestionUI_t3721436426 ** get_address_of_questionUI_4() { return &___questionUI_4; }
	inline void set_questionUI_4(QuestionUI_t3721436426 * value)
	{
		___questionUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___questionUI_4, value);
	}

	inline static int32_t get_offset_of_contactUI_5() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___contactUI_5)); }
	inline ContactUsUI_t1917862594 * get_contactUI_5() const { return ___contactUI_5; }
	inline ContactUsUI_t1917862594 ** get_address_of_contactUI_5() { return &___contactUI_5; }
	inline void set_contactUI_5(ContactUsUI_t1917862594 * value)
	{
		___contactUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___contactUI_5, value);
	}

	inline static int32_t get_offset_of_instructionsUI_6() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___instructionsUI_6)); }
	inline InstructionsUI_t253378877 * get_instructionsUI_6() const { return ___instructionsUI_6; }
	inline InstructionsUI_t253378877 ** get_address_of_instructionsUI_6() { return &___instructionsUI_6; }
	inline void set_instructionsUI_6(InstructionsUI_t253378877 * value)
	{
		___instructionsUI_6 = value;
		Il2CppCodeGenWriteBarrier(&___instructionsUI_6, value);
	}

	inline static int32_t get_offset_of_eulaUI_7() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___eulaUI_7)); }
	inline EULAUI_t3168190395 * get_eulaUI_7() const { return ___eulaUI_7; }
	inline EULAUI_t3168190395 ** get_address_of_eulaUI_7() { return &___eulaUI_7; }
	inline void set_eulaUI_7(EULAUI_t3168190395 * value)
	{
		___eulaUI_7 = value;
		Il2CppCodeGenWriteBarrier(&___eulaUI_7, value);
	}

	inline static int32_t get_offset_of_ppUI_8() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___ppUI_8)); }
	inline PrivacyPolicyUI_t524063172 * get_ppUI_8() const { return ___ppUI_8; }
	inline PrivacyPolicyUI_t524063172 ** get_address_of_ppUI_8() { return &___ppUI_8; }
	inline void set_ppUI_8(PrivacyPolicyUI_t524063172 * value)
	{
		___ppUI_8 = value;
		Il2CppCodeGenWriteBarrier(&___ppUI_8, value);
	}

	inline static int32_t get_offset_of_tosUI_9() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___tosUI_9)); }
	inline TermsOfServiceUI_t3223215579 * get_tosUI_9() const { return ___tosUI_9; }
	inline TermsOfServiceUI_t3223215579 ** get_address_of_tosUI_9() { return &___tosUI_9; }
	inline void set_tosUI_9(TermsOfServiceUI_t3223215579 * value)
	{
		___tosUI_9 = value;
		Il2CppCodeGenWriteBarrier(&___tosUI_9, value);
	}

	inline static int32_t get_offset_of_UIclosing_10() { return static_cast<int32_t>(offsetof(TextPageNavScreen_t1592130049, ___UIclosing_10)); }
	inline bool get_UIclosing_10() const { return ___UIclosing_10; }
	inline bool* get_address_of_UIclosing_10() { return &___UIclosing_10; }
	inline void set_UIclosing_10(bool value)
	{
		___UIclosing_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
