﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumberButton
struct PhoneNumberButton_t1790400683;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumberButton::.ctor()
extern "C"  void PhoneNumberButton__ctor_m3849977044 (PhoneNumberButton_t1790400683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberButton::PopulatePhoneNumber(System.String)
extern "C"  void PhoneNumberButton_PopulatePhoneNumber_m2205052233 (PhoneNumberButton_t1790400683 * __this, String_t* ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberButton::ResetButton()
extern "C"  void PhoneNumberButton_ResetButton_m3233597689 (PhoneNumberButton_t1790400683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberButton::OnButtonClick()
extern "C"  void PhoneNumberButton_OnButtonClick_m2894205251 (PhoneNumberButton_t1790400683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
