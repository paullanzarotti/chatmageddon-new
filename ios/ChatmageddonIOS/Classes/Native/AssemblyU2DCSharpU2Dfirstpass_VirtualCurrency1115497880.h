﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Decimal>
struct Dictionary_2_t2639480339;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VirtualCurrency
struct  VirtualCurrency_t1115497880  : public Il2CppObject
{
public:
	// System.String VirtualCurrency::<currencyId>k__BackingField
	String_t* ___U3CcurrencyIdU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Decimal> VirtualCurrency::<mappings>k__BackingField
	Dictionary_2_t2639480339 * ___U3CmappingsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CcurrencyIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualCurrency_t1115497880, ___U3CcurrencyIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CcurrencyIdU3Ek__BackingField_0() const { return ___U3CcurrencyIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CcurrencyIdU3Ek__BackingField_0() { return &___U3CcurrencyIdU3Ek__BackingField_0; }
	inline void set_U3CcurrencyIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CcurrencyIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrencyIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CmappingsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualCurrency_t1115497880, ___U3CmappingsU3Ek__BackingField_1)); }
	inline Dictionary_2_t2639480339 * get_U3CmappingsU3Ek__BackingField_1() const { return ___U3CmappingsU3Ek__BackingField_1; }
	inline Dictionary_2_t2639480339 ** get_address_of_U3CmappingsU3Ek__BackingField_1() { return &___U3CmappingsU3Ek__BackingField_1; }
	inline void set_U3CmappingsU3Ek__BackingField_1(Dictionary_2_t2639480339 * value)
	{
		___U3CmappingsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmappingsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
