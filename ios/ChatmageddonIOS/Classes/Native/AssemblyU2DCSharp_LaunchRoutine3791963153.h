﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchStateController
struct LaunchStateController_t3361989840;
// LaunchCountdownTimer
struct LaunchCountdownTimer_t1840221825;
// TweenAlpha
struct TweenAlpha_t2421518635;
// LaunchSequenceReport
struct LaunchSequenceReport_t983071960;
// ItemInfoController
struct ItemInfoController_t4231956141;
// System.Collections.Generic.List`1<TweenAlpha>
struct List_1_t1790639767;
// UILabel
struct UILabel_t1795115428;
// TweenScale
struct TweenScale_t2697902175;
// LaunchAnimationController
struct LaunchAnimationController_t825305345;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchRoutine
struct  LaunchRoutine_t3791963153  : public MonoBehaviour_t1158329972
{
public:
	// LaunchStateController LaunchRoutine::stateController
	LaunchStateController_t3361989840 * ___stateController_2;
	// LaunchCountdownTimer LaunchRoutine::countdownTimer
	LaunchCountdownTimer_t1840221825 * ___countdownTimer_3;
	// TweenAlpha LaunchRoutine::coutdownTween
	TweenAlpha_t2421518635 * ___coutdownTween_4;
	// LaunchSequenceReport LaunchRoutine::sequenceReport
	LaunchSequenceReport_t983071960 * ___sequenceReport_5;
	// ItemInfoController LaunchRoutine::screenInfoController
	ItemInfoController_t4231956141 * ___screenInfoController_6;
	// System.Collections.Generic.List`1<TweenAlpha> LaunchRoutine::overlayTweens
	List_1_t1790639767 * ___overlayTweens_7;
	// ItemInfoController LaunchRoutine::successInfoController
	ItemInfoController_t4231956141 * ___successInfoController_8;
	// UILabel LaunchRoutine::backfireLabel
	UILabel_t1795115428 * ___backfireLabel_9;
	// TweenScale LaunchRoutine::backfireTween
	TweenScale_t2697902175 * ___backfireTween_10;
	// LaunchSequenceReport LaunchRoutine::finishSequenceReport
	LaunchSequenceReport_t983071960 * ___finishSequenceReport_11;
	// TweenScale LaunchRoutine::finishSequenceTween
	TweenScale_t2697902175 * ___finishSequenceTween_12;
	// TweenAlpha LaunchRoutine::successLabelTween
	TweenAlpha_t2421518635 * ___successLabelTween_13;
	// TweenScale LaunchRoutine::animRenderTween
	TweenScale_t2697902175 * ___animRenderTween_14;
	// TweenAlpha LaunchRoutine::whiteoutAlphaTween
	TweenAlpha_t2421518635 * ___whiteoutAlphaTween_15;
	// LaunchAnimationController LaunchRoutine::animationController
	LaunchAnimationController_t825305345 * ___animationController_16;
	// UnityEngine.Coroutine LaunchRoutine::timedGlitch
	Coroutine_t2299508840 * ___timedGlitch_17;
	// System.Boolean LaunchRoutine::engage
	bool ___engage_18;
	// System.Boolean LaunchRoutine::playingForward
	bool ___playingForward_19;
	// System.Boolean LaunchRoutine::step1
	bool ___step1_20;
	// UnityEngine.Coroutine LaunchRoutine::setp1Routine
	Coroutine_t2299508840 * ___setp1Routine_21;
	// System.Boolean LaunchRoutine::step2
	bool ___step2_22;
	// UnityEngine.Coroutine LaunchRoutine::setp2Routine
	Coroutine_t2299508840 * ___setp2Routine_23;
	// System.Boolean LaunchRoutine::closing
	bool ___closing_24;
	// UnityEngine.Coroutine LaunchRoutine::endAnimWaitRoutine
	Coroutine_t2299508840 * ___endAnimWaitRoutine_25;
	// UnityEngine.Coroutine LaunchRoutine::sceneTransitionRoutine
	Coroutine_t2299508840 * ___sceneTransitionRoutine_26;

public:
	inline static int32_t get_offset_of_stateController_2() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___stateController_2)); }
	inline LaunchStateController_t3361989840 * get_stateController_2() const { return ___stateController_2; }
	inline LaunchStateController_t3361989840 ** get_address_of_stateController_2() { return &___stateController_2; }
	inline void set_stateController_2(LaunchStateController_t3361989840 * value)
	{
		___stateController_2 = value;
		Il2CppCodeGenWriteBarrier(&___stateController_2, value);
	}

	inline static int32_t get_offset_of_countdownTimer_3() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___countdownTimer_3)); }
	inline LaunchCountdownTimer_t1840221825 * get_countdownTimer_3() const { return ___countdownTimer_3; }
	inline LaunchCountdownTimer_t1840221825 ** get_address_of_countdownTimer_3() { return &___countdownTimer_3; }
	inline void set_countdownTimer_3(LaunchCountdownTimer_t1840221825 * value)
	{
		___countdownTimer_3 = value;
		Il2CppCodeGenWriteBarrier(&___countdownTimer_3, value);
	}

	inline static int32_t get_offset_of_coutdownTween_4() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___coutdownTween_4)); }
	inline TweenAlpha_t2421518635 * get_coutdownTween_4() const { return ___coutdownTween_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_coutdownTween_4() { return &___coutdownTween_4; }
	inline void set_coutdownTween_4(TweenAlpha_t2421518635 * value)
	{
		___coutdownTween_4 = value;
		Il2CppCodeGenWriteBarrier(&___coutdownTween_4, value);
	}

	inline static int32_t get_offset_of_sequenceReport_5() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___sequenceReport_5)); }
	inline LaunchSequenceReport_t983071960 * get_sequenceReport_5() const { return ___sequenceReport_5; }
	inline LaunchSequenceReport_t983071960 ** get_address_of_sequenceReport_5() { return &___sequenceReport_5; }
	inline void set_sequenceReport_5(LaunchSequenceReport_t983071960 * value)
	{
		___sequenceReport_5 = value;
		Il2CppCodeGenWriteBarrier(&___sequenceReport_5, value);
	}

	inline static int32_t get_offset_of_screenInfoController_6() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___screenInfoController_6)); }
	inline ItemInfoController_t4231956141 * get_screenInfoController_6() const { return ___screenInfoController_6; }
	inline ItemInfoController_t4231956141 ** get_address_of_screenInfoController_6() { return &___screenInfoController_6; }
	inline void set_screenInfoController_6(ItemInfoController_t4231956141 * value)
	{
		___screenInfoController_6 = value;
		Il2CppCodeGenWriteBarrier(&___screenInfoController_6, value);
	}

	inline static int32_t get_offset_of_overlayTweens_7() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___overlayTweens_7)); }
	inline List_1_t1790639767 * get_overlayTweens_7() const { return ___overlayTweens_7; }
	inline List_1_t1790639767 ** get_address_of_overlayTweens_7() { return &___overlayTweens_7; }
	inline void set_overlayTweens_7(List_1_t1790639767 * value)
	{
		___overlayTweens_7 = value;
		Il2CppCodeGenWriteBarrier(&___overlayTweens_7, value);
	}

	inline static int32_t get_offset_of_successInfoController_8() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___successInfoController_8)); }
	inline ItemInfoController_t4231956141 * get_successInfoController_8() const { return ___successInfoController_8; }
	inline ItemInfoController_t4231956141 ** get_address_of_successInfoController_8() { return &___successInfoController_8; }
	inline void set_successInfoController_8(ItemInfoController_t4231956141 * value)
	{
		___successInfoController_8 = value;
		Il2CppCodeGenWriteBarrier(&___successInfoController_8, value);
	}

	inline static int32_t get_offset_of_backfireLabel_9() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___backfireLabel_9)); }
	inline UILabel_t1795115428 * get_backfireLabel_9() const { return ___backfireLabel_9; }
	inline UILabel_t1795115428 ** get_address_of_backfireLabel_9() { return &___backfireLabel_9; }
	inline void set_backfireLabel_9(UILabel_t1795115428 * value)
	{
		___backfireLabel_9 = value;
		Il2CppCodeGenWriteBarrier(&___backfireLabel_9, value);
	}

	inline static int32_t get_offset_of_backfireTween_10() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___backfireTween_10)); }
	inline TweenScale_t2697902175 * get_backfireTween_10() const { return ___backfireTween_10; }
	inline TweenScale_t2697902175 ** get_address_of_backfireTween_10() { return &___backfireTween_10; }
	inline void set_backfireTween_10(TweenScale_t2697902175 * value)
	{
		___backfireTween_10 = value;
		Il2CppCodeGenWriteBarrier(&___backfireTween_10, value);
	}

	inline static int32_t get_offset_of_finishSequenceReport_11() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___finishSequenceReport_11)); }
	inline LaunchSequenceReport_t983071960 * get_finishSequenceReport_11() const { return ___finishSequenceReport_11; }
	inline LaunchSequenceReport_t983071960 ** get_address_of_finishSequenceReport_11() { return &___finishSequenceReport_11; }
	inline void set_finishSequenceReport_11(LaunchSequenceReport_t983071960 * value)
	{
		___finishSequenceReport_11 = value;
		Il2CppCodeGenWriteBarrier(&___finishSequenceReport_11, value);
	}

	inline static int32_t get_offset_of_finishSequenceTween_12() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___finishSequenceTween_12)); }
	inline TweenScale_t2697902175 * get_finishSequenceTween_12() const { return ___finishSequenceTween_12; }
	inline TweenScale_t2697902175 ** get_address_of_finishSequenceTween_12() { return &___finishSequenceTween_12; }
	inline void set_finishSequenceTween_12(TweenScale_t2697902175 * value)
	{
		___finishSequenceTween_12 = value;
		Il2CppCodeGenWriteBarrier(&___finishSequenceTween_12, value);
	}

	inline static int32_t get_offset_of_successLabelTween_13() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___successLabelTween_13)); }
	inline TweenAlpha_t2421518635 * get_successLabelTween_13() const { return ___successLabelTween_13; }
	inline TweenAlpha_t2421518635 ** get_address_of_successLabelTween_13() { return &___successLabelTween_13; }
	inline void set_successLabelTween_13(TweenAlpha_t2421518635 * value)
	{
		___successLabelTween_13 = value;
		Il2CppCodeGenWriteBarrier(&___successLabelTween_13, value);
	}

	inline static int32_t get_offset_of_animRenderTween_14() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___animRenderTween_14)); }
	inline TweenScale_t2697902175 * get_animRenderTween_14() const { return ___animRenderTween_14; }
	inline TweenScale_t2697902175 ** get_address_of_animRenderTween_14() { return &___animRenderTween_14; }
	inline void set_animRenderTween_14(TweenScale_t2697902175 * value)
	{
		___animRenderTween_14 = value;
		Il2CppCodeGenWriteBarrier(&___animRenderTween_14, value);
	}

	inline static int32_t get_offset_of_whiteoutAlphaTween_15() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___whiteoutAlphaTween_15)); }
	inline TweenAlpha_t2421518635 * get_whiteoutAlphaTween_15() const { return ___whiteoutAlphaTween_15; }
	inline TweenAlpha_t2421518635 ** get_address_of_whiteoutAlphaTween_15() { return &___whiteoutAlphaTween_15; }
	inline void set_whiteoutAlphaTween_15(TweenAlpha_t2421518635 * value)
	{
		___whiteoutAlphaTween_15 = value;
		Il2CppCodeGenWriteBarrier(&___whiteoutAlphaTween_15, value);
	}

	inline static int32_t get_offset_of_animationController_16() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___animationController_16)); }
	inline LaunchAnimationController_t825305345 * get_animationController_16() const { return ___animationController_16; }
	inline LaunchAnimationController_t825305345 ** get_address_of_animationController_16() { return &___animationController_16; }
	inline void set_animationController_16(LaunchAnimationController_t825305345 * value)
	{
		___animationController_16 = value;
		Il2CppCodeGenWriteBarrier(&___animationController_16, value);
	}

	inline static int32_t get_offset_of_timedGlitch_17() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___timedGlitch_17)); }
	inline Coroutine_t2299508840 * get_timedGlitch_17() const { return ___timedGlitch_17; }
	inline Coroutine_t2299508840 ** get_address_of_timedGlitch_17() { return &___timedGlitch_17; }
	inline void set_timedGlitch_17(Coroutine_t2299508840 * value)
	{
		___timedGlitch_17 = value;
		Il2CppCodeGenWriteBarrier(&___timedGlitch_17, value);
	}

	inline static int32_t get_offset_of_engage_18() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___engage_18)); }
	inline bool get_engage_18() const { return ___engage_18; }
	inline bool* get_address_of_engage_18() { return &___engage_18; }
	inline void set_engage_18(bool value)
	{
		___engage_18 = value;
	}

	inline static int32_t get_offset_of_playingForward_19() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___playingForward_19)); }
	inline bool get_playingForward_19() const { return ___playingForward_19; }
	inline bool* get_address_of_playingForward_19() { return &___playingForward_19; }
	inline void set_playingForward_19(bool value)
	{
		___playingForward_19 = value;
	}

	inline static int32_t get_offset_of_step1_20() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___step1_20)); }
	inline bool get_step1_20() const { return ___step1_20; }
	inline bool* get_address_of_step1_20() { return &___step1_20; }
	inline void set_step1_20(bool value)
	{
		___step1_20 = value;
	}

	inline static int32_t get_offset_of_setp1Routine_21() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___setp1Routine_21)); }
	inline Coroutine_t2299508840 * get_setp1Routine_21() const { return ___setp1Routine_21; }
	inline Coroutine_t2299508840 ** get_address_of_setp1Routine_21() { return &___setp1Routine_21; }
	inline void set_setp1Routine_21(Coroutine_t2299508840 * value)
	{
		___setp1Routine_21 = value;
		Il2CppCodeGenWriteBarrier(&___setp1Routine_21, value);
	}

	inline static int32_t get_offset_of_step2_22() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___step2_22)); }
	inline bool get_step2_22() const { return ___step2_22; }
	inline bool* get_address_of_step2_22() { return &___step2_22; }
	inline void set_step2_22(bool value)
	{
		___step2_22 = value;
	}

	inline static int32_t get_offset_of_setp2Routine_23() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___setp2Routine_23)); }
	inline Coroutine_t2299508840 * get_setp2Routine_23() const { return ___setp2Routine_23; }
	inline Coroutine_t2299508840 ** get_address_of_setp2Routine_23() { return &___setp2Routine_23; }
	inline void set_setp2Routine_23(Coroutine_t2299508840 * value)
	{
		___setp2Routine_23 = value;
		Il2CppCodeGenWriteBarrier(&___setp2Routine_23, value);
	}

	inline static int32_t get_offset_of_closing_24() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___closing_24)); }
	inline bool get_closing_24() const { return ___closing_24; }
	inline bool* get_address_of_closing_24() { return &___closing_24; }
	inline void set_closing_24(bool value)
	{
		___closing_24 = value;
	}

	inline static int32_t get_offset_of_endAnimWaitRoutine_25() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___endAnimWaitRoutine_25)); }
	inline Coroutine_t2299508840 * get_endAnimWaitRoutine_25() const { return ___endAnimWaitRoutine_25; }
	inline Coroutine_t2299508840 ** get_address_of_endAnimWaitRoutine_25() { return &___endAnimWaitRoutine_25; }
	inline void set_endAnimWaitRoutine_25(Coroutine_t2299508840 * value)
	{
		___endAnimWaitRoutine_25 = value;
		Il2CppCodeGenWriteBarrier(&___endAnimWaitRoutine_25, value);
	}

	inline static int32_t get_offset_of_sceneTransitionRoutine_26() { return static_cast<int32_t>(offsetof(LaunchRoutine_t3791963153, ___sceneTransitionRoutine_26)); }
	inline Coroutine_t2299508840 * get_sceneTransitionRoutine_26() const { return ___sceneTransitionRoutine_26; }
	inline Coroutine_t2299508840 ** get_address_of_sceneTransitionRoutine_26() { return &___sceneTransitionRoutine_26; }
	inline void set_sceneTransitionRoutine_26(Coroutine_t2299508840 * value)
	{
		___sceneTransitionRoutine_26 = value;
		Il2CppCodeGenWriteBarrier(&___sceneTransitionRoutine_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
