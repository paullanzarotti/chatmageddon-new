﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.HelpCentre
struct HelpCentre_t214342444;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"

// System.Void Unibill.Impl.HelpCentre::.ctor(System.String)
extern "C"  void HelpCentre__ctor_m677441124 (HelpCentre_t214342444 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.HelpCentre::getMessage(UnibillError)
extern "C"  String_t* HelpCentre_getMessage_m825329859 (HelpCentre_t214342444 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
