﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPDatePicker/Date
struct  Date_t2675184608  : public Il2CppObject
{
public:
	// System.Int32 IPDatePicker/Date::day
	int32_t ___day_0;
	// System.Int32 IPDatePicker/Date::month
	int32_t ___month_1;
	// System.Int32 IPDatePicker/Date::year
	int32_t ___year_2;

public:
	inline static int32_t get_offset_of_day_0() { return static_cast<int32_t>(offsetof(Date_t2675184608, ___day_0)); }
	inline int32_t get_day_0() const { return ___day_0; }
	inline int32_t* get_address_of_day_0() { return &___day_0; }
	inline void set_day_0(int32_t value)
	{
		___day_0 = value;
	}

	inline static int32_t get_offset_of_month_1() { return static_cast<int32_t>(offsetof(Date_t2675184608, ___month_1)); }
	inline int32_t get_month_1() const { return ___month_1; }
	inline int32_t* get_address_of_month_1() { return &___month_1; }
	inline void set_month_1(int32_t value)
	{
		___month_1 = value;
	}

	inline static int32_t get_offset_of_year_2() { return static_cast<int32_t>(offsetof(Date_t2675184608, ___year_2)); }
	inline int32_t get_year_2() const { return ___year_2; }
	inline int32_t* get_address_of_year_2() { return &___year_2; }
	inline void set_year_2(int32_t value)
	{
		___year_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
