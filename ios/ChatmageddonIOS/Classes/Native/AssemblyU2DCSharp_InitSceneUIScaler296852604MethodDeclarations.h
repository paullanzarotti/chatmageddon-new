﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InitSceneUIScaler
struct InitSceneUIScaler_t296852604;

#include "codegen/il2cpp-codegen.h"

// System.Void InitSceneUIScaler::.ctor()
extern "C"  void InitSceneUIScaler__ctor_m1172016985 (InitSceneUIScaler_t296852604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitSceneUIScaler::GlobalUIScale()
extern "C"  void InitSceneUIScaler_GlobalUIScale_m2492238710 (InitSceneUIScaler_t296852604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
