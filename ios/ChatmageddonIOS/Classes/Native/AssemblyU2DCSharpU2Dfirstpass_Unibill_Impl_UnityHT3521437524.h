﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.UnityHTTPRequest
struct  UnityHTTPRequest_t3521437524  : public Il2CppObject
{
public:
	// UnityEngine.WWW Unibill.Impl.UnityHTTPRequest::w
	WWW_t2919945039 * ___w_0;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(UnityHTTPRequest_t3521437524, ___w_0)); }
	inline WWW_t2919945039 * get_w_0() const { return ___w_0; }
	inline WWW_t2919945039 ** get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(WWW_t2919945039 * value)
	{
		___w_0 = value;
		Il2CppCodeGenWriteBarrier(&___w_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
