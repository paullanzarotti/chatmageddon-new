﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UserRegistrationButton
struct UserRegistrationButton_t1399913110;

#include "codegen/il2cpp-codegen.h"

// System.Void UserRegistrationButton::.ctor()
extern "C"  void UserRegistrationButton__ctor_m2212632477 (UserRegistrationButton_t1399913110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UserRegistrationButton::OnButtonClick()
extern "C"  void UserRegistrationButton_OnButtonClick_m4271833294 (UserRegistrationButton_t1399913110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
