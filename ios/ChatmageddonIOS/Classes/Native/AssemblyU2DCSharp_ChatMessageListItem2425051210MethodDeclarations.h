﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatMessageListItem
struct ChatMessageListItem_t2425051210;
// ChatMessage
struct ChatMessage_t2384228687;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatMessage2384228687.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void ChatMessageListItem::.ctor()
extern "C"  void ChatMessageListItem__ctor_m2252682849 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::SetUpItem()
extern "C"  void ChatMessageListItem_SetUpItem_m2268486323 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::PopulateItem(ChatMessage)
extern "C"  void ChatMessageListItem_PopulateItem_m839090099 (ChatMessageListItem_t2425051210 * __this, ChatMessage_t2384228687 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::ActivateDateUI(System.Boolean)
extern "C"  void ChatMessageListItem_ActivateDateUI_m610009003 (ChatMessageListItem_t2425051210 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::SetMessageType(System.Boolean)
extern "C"  void ChatMessageListItem_SetMessageType_m628522027 (ChatMessageListItem_t2425051210 * __this, bool ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::ForceItemPosition()
extern "C"  void ChatMessageListItem_ForceItemPosition_m806332506 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::SetVisible()
extern "C"  void ChatMessageListItem_SetVisible_m770579485 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::SetInvisible()
extern "C"  void ChatMessageListItem_SetInvisible_m1222000978 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatMessageListItem::verifyVisibility()
extern "C"  bool ChatMessageListItem_verifyVisibility_m1471515414 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::CheckVisibilty()
extern "C"  void ChatMessageListItem_CheckVisibilty_m2359168178 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::FixedUpdate()
extern "C"  void ChatMessageListItem_FixedUpdate_m1646678918 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::OnEnable()
extern "C"  void ChatMessageListItem_OnEnable_m3693226513 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::FindScrollView()
extern "C"  void ChatMessageListItem_FindScrollView_m1089100506 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::OnPress(System.Boolean)
extern "C"  void ChatMessageListItem_OnPress_m1012328908 (ChatMessageListItem_t2425051210 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::OnDrag(UnityEngine.Vector2)
extern "C"  void ChatMessageListItem_OnDrag_m1226299174 (ChatMessageListItem_t2425051210 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::OnDragEnd()
extern "C"  void ChatMessageListItem_OnDragEnd_m461964787 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::OnScroll(System.Single)
extern "C"  void ChatMessageListItem_OnScroll_m638763954 (ChatMessageListItem_t2425051210 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatMessageListItem::OnClick()
extern "C"  void ChatMessageListItem_OnClick_m290664462 (ChatMessageListItem_t2425051210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
