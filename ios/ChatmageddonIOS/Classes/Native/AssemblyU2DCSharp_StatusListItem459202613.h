﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StatusILP
struct StatusILP_t1076408775;
// LaunchedItem
struct LaunchedItem_t3670634427;
// AvatarUpdater
struct AvatarUpdater_t2404165026;
// OpenPlayerProfileButton
struct OpenPlayerProfileButton_t3248004898;
// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;
// MissilePointsPopulator
struct MissilePointsPopulator_t2262625279;
// InactiveMissileUI
struct InactiveMissileUI_t409112257;
// PendingMissileUI
struct PendingMissileUI_t3261005045;
// ActiveMissileUI
struct ActiveMissileUI_t356948948;
// MineStatusUI
struct MineStatusUI_t2517460477;
// ItemFinishTimePopulator
struct ItemFinishTimePopulator_t2301874695;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen1234741977.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusListItem
struct  StatusListItem_t459202613  : public BaseInfiniteListItem_1_t1234741977
{
public:
	// StatusILP StatusListItem::listPopulator
	StatusILP_t1076408775 * ___listPopulator_10;
	// System.Boolean StatusListItem::outgoingItem
	bool ___outgoingItem_11;
	// LaunchedItem StatusListItem::launchedItem
	LaunchedItem_t3670634427 * ___launchedItem_12;
	// AvatarUpdater StatusListItem::avatar
	AvatarUpdater_t2404165026 * ___avatar_13;
	// OpenPlayerProfileButton StatusListItem::profileButton
	OpenPlayerProfileButton_t3248004898 * ___profileButton_14;
	// UILabel StatusListItem::nameLabel
	UILabel_t1795115428 * ___nameLabel_15;
	// UILabel StatusListItem::actionLabel
	UILabel_t1795115428 * ___actionLabel_16;
	// UISprite StatusListItem::itemGraphic
	UISprite_t603616735 * ___itemGraphic_17;
	// MissilePointsPopulator StatusListItem::pointsPopulator
	MissilePointsPopulator_t2262625279 * ___pointsPopulator_18;
	// InactiveMissileUI StatusListItem::missileInactiveUI
	InactiveMissileUI_t409112257 * ___missileInactiveUI_19;
	// PendingMissileUI StatusListItem::missilePendingUI
	PendingMissileUI_t3261005045 * ___missilePendingUI_20;
	// ActiveMissileUI StatusListItem::missileActiveUI
	ActiveMissileUI_t356948948 * ___missileActiveUI_21;
	// MineStatusUI StatusListItem::mineUI
	MineStatusUI_t2517460477 * ___mineUI_22;
	// ItemFinishTimePopulator StatusListItem::finishTime
	ItemFinishTimePopulator_t2301874695 * ___finishTime_23;
	// UIScrollView StatusListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_24;
	// UIScrollView StatusListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_25;
	// UnityEngine.Transform StatusListItem::mTrans
	Transform_t3275118058 * ___mTrans_26;
	// UIScrollView StatusListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_27;
	// System.Boolean StatusListItem::mAutoFind
	bool ___mAutoFind_28;
	// System.Boolean StatusListItem::mStarted
	bool ___mStarted_29;
	// System.Boolean StatusListItem::allowToast
	bool ___allowToast_30;
	// System.Single StatusListItem::toastWaitTime
	float ___toastWaitTime_31;

public:
	inline static int32_t get_offset_of_listPopulator_10() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___listPopulator_10)); }
	inline StatusILP_t1076408775 * get_listPopulator_10() const { return ___listPopulator_10; }
	inline StatusILP_t1076408775 ** get_address_of_listPopulator_10() { return &___listPopulator_10; }
	inline void set_listPopulator_10(StatusILP_t1076408775 * value)
	{
		___listPopulator_10 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_10, value);
	}

	inline static int32_t get_offset_of_outgoingItem_11() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___outgoingItem_11)); }
	inline bool get_outgoingItem_11() const { return ___outgoingItem_11; }
	inline bool* get_address_of_outgoingItem_11() { return &___outgoingItem_11; }
	inline void set_outgoingItem_11(bool value)
	{
		___outgoingItem_11 = value;
	}

	inline static int32_t get_offset_of_launchedItem_12() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___launchedItem_12)); }
	inline LaunchedItem_t3670634427 * get_launchedItem_12() const { return ___launchedItem_12; }
	inline LaunchedItem_t3670634427 ** get_address_of_launchedItem_12() { return &___launchedItem_12; }
	inline void set_launchedItem_12(LaunchedItem_t3670634427 * value)
	{
		___launchedItem_12 = value;
		Il2CppCodeGenWriteBarrier(&___launchedItem_12, value);
	}

	inline static int32_t get_offset_of_avatar_13() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___avatar_13)); }
	inline AvatarUpdater_t2404165026 * get_avatar_13() const { return ___avatar_13; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_13() { return &___avatar_13; }
	inline void set_avatar_13(AvatarUpdater_t2404165026 * value)
	{
		___avatar_13 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_13, value);
	}

	inline static int32_t get_offset_of_profileButton_14() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___profileButton_14)); }
	inline OpenPlayerProfileButton_t3248004898 * get_profileButton_14() const { return ___profileButton_14; }
	inline OpenPlayerProfileButton_t3248004898 ** get_address_of_profileButton_14() { return &___profileButton_14; }
	inline void set_profileButton_14(OpenPlayerProfileButton_t3248004898 * value)
	{
		___profileButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___profileButton_14, value);
	}

	inline static int32_t get_offset_of_nameLabel_15() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___nameLabel_15)); }
	inline UILabel_t1795115428 * get_nameLabel_15() const { return ___nameLabel_15; }
	inline UILabel_t1795115428 ** get_address_of_nameLabel_15() { return &___nameLabel_15; }
	inline void set_nameLabel_15(UILabel_t1795115428 * value)
	{
		___nameLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___nameLabel_15, value);
	}

	inline static int32_t get_offset_of_actionLabel_16() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___actionLabel_16)); }
	inline UILabel_t1795115428 * get_actionLabel_16() const { return ___actionLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_actionLabel_16() { return &___actionLabel_16; }
	inline void set_actionLabel_16(UILabel_t1795115428 * value)
	{
		___actionLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___actionLabel_16, value);
	}

	inline static int32_t get_offset_of_itemGraphic_17() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___itemGraphic_17)); }
	inline UISprite_t603616735 * get_itemGraphic_17() const { return ___itemGraphic_17; }
	inline UISprite_t603616735 ** get_address_of_itemGraphic_17() { return &___itemGraphic_17; }
	inline void set_itemGraphic_17(UISprite_t603616735 * value)
	{
		___itemGraphic_17 = value;
		Il2CppCodeGenWriteBarrier(&___itemGraphic_17, value);
	}

	inline static int32_t get_offset_of_pointsPopulator_18() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___pointsPopulator_18)); }
	inline MissilePointsPopulator_t2262625279 * get_pointsPopulator_18() const { return ___pointsPopulator_18; }
	inline MissilePointsPopulator_t2262625279 ** get_address_of_pointsPopulator_18() { return &___pointsPopulator_18; }
	inline void set_pointsPopulator_18(MissilePointsPopulator_t2262625279 * value)
	{
		___pointsPopulator_18 = value;
		Il2CppCodeGenWriteBarrier(&___pointsPopulator_18, value);
	}

	inline static int32_t get_offset_of_missileInactiveUI_19() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___missileInactiveUI_19)); }
	inline InactiveMissileUI_t409112257 * get_missileInactiveUI_19() const { return ___missileInactiveUI_19; }
	inline InactiveMissileUI_t409112257 ** get_address_of_missileInactiveUI_19() { return &___missileInactiveUI_19; }
	inline void set_missileInactiveUI_19(InactiveMissileUI_t409112257 * value)
	{
		___missileInactiveUI_19 = value;
		Il2CppCodeGenWriteBarrier(&___missileInactiveUI_19, value);
	}

	inline static int32_t get_offset_of_missilePendingUI_20() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___missilePendingUI_20)); }
	inline PendingMissileUI_t3261005045 * get_missilePendingUI_20() const { return ___missilePendingUI_20; }
	inline PendingMissileUI_t3261005045 ** get_address_of_missilePendingUI_20() { return &___missilePendingUI_20; }
	inline void set_missilePendingUI_20(PendingMissileUI_t3261005045 * value)
	{
		___missilePendingUI_20 = value;
		Il2CppCodeGenWriteBarrier(&___missilePendingUI_20, value);
	}

	inline static int32_t get_offset_of_missileActiveUI_21() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___missileActiveUI_21)); }
	inline ActiveMissileUI_t356948948 * get_missileActiveUI_21() const { return ___missileActiveUI_21; }
	inline ActiveMissileUI_t356948948 ** get_address_of_missileActiveUI_21() { return &___missileActiveUI_21; }
	inline void set_missileActiveUI_21(ActiveMissileUI_t356948948 * value)
	{
		___missileActiveUI_21 = value;
		Il2CppCodeGenWriteBarrier(&___missileActiveUI_21, value);
	}

	inline static int32_t get_offset_of_mineUI_22() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___mineUI_22)); }
	inline MineStatusUI_t2517460477 * get_mineUI_22() const { return ___mineUI_22; }
	inline MineStatusUI_t2517460477 ** get_address_of_mineUI_22() { return &___mineUI_22; }
	inline void set_mineUI_22(MineStatusUI_t2517460477 * value)
	{
		___mineUI_22 = value;
		Il2CppCodeGenWriteBarrier(&___mineUI_22, value);
	}

	inline static int32_t get_offset_of_finishTime_23() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___finishTime_23)); }
	inline ItemFinishTimePopulator_t2301874695 * get_finishTime_23() const { return ___finishTime_23; }
	inline ItemFinishTimePopulator_t2301874695 ** get_address_of_finishTime_23() { return &___finishTime_23; }
	inline void set_finishTime_23(ItemFinishTimePopulator_t2301874695 * value)
	{
		___finishTime_23 = value;
		Il2CppCodeGenWriteBarrier(&___finishTime_23, value);
	}

	inline static int32_t get_offset_of_scrollView_24() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___scrollView_24)); }
	inline UIScrollView_t3033954930 * get_scrollView_24() const { return ___scrollView_24; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_24() { return &___scrollView_24; }
	inline void set_scrollView_24(UIScrollView_t3033954930 * value)
	{
		___scrollView_24 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_24, value);
	}

	inline static int32_t get_offset_of_draggablePanel_25() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___draggablePanel_25)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_25() const { return ___draggablePanel_25; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_25() { return &___draggablePanel_25; }
	inline void set_draggablePanel_25(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_25 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_25, value);
	}

	inline static int32_t get_offset_of_mTrans_26() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___mTrans_26)); }
	inline Transform_t3275118058 * get_mTrans_26() const { return ___mTrans_26; }
	inline Transform_t3275118058 ** get_address_of_mTrans_26() { return &___mTrans_26; }
	inline void set_mTrans_26(Transform_t3275118058 * value)
	{
		___mTrans_26 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_26, value);
	}

	inline static int32_t get_offset_of_mScroll_27() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___mScroll_27)); }
	inline UIScrollView_t3033954930 * get_mScroll_27() const { return ___mScroll_27; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_27() { return &___mScroll_27; }
	inline void set_mScroll_27(UIScrollView_t3033954930 * value)
	{
		___mScroll_27 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_27, value);
	}

	inline static int32_t get_offset_of_mAutoFind_28() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___mAutoFind_28)); }
	inline bool get_mAutoFind_28() const { return ___mAutoFind_28; }
	inline bool* get_address_of_mAutoFind_28() { return &___mAutoFind_28; }
	inline void set_mAutoFind_28(bool value)
	{
		___mAutoFind_28 = value;
	}

	inline static int32_t get_offset_of_mStarted_29() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___mStarted_29)); }
	inline bool get_mStarted_29() const { return ___mStarted_29; }
	inline bool* get_address_of_mStarted_29() { return &___mStarted_29; }
	inline void set_mStarted_29(bool value)
	{
		___mStarted_29 = value;
	}

	inline static int32_t get_offset_of_allowToast_30() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___allowToast_30)); }
	inline bool get_allowToast_30() const { return ___allowToast_30; }
	inline bool* get_address_of_allowToast_30() { return &___allowToast_30; }
	inline void set_allowToast_30(bool value)
	{
		___allowToast_30 = value;
	}

	inline static int32_t get_offset_of_toastWaitTime_31() { return static_cast<int32_t>(offsetof(StatusListItem_t459202613, ___toastWaitTime_31)); }
	inline float get_toastWaitTime_31() const { return ___toastWaitTime_31; }
	inline float* get_address_of_toastWaitTime_31() { return &___toastWaitTime_31; }
	inline void set_toastWaitTime_31(float value)
	{
		___toastWaitTime_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
