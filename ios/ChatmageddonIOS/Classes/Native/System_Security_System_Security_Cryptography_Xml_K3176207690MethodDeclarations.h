﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyInfoName
struct KeyInfoName_t3176207690;
// System.String
struct String_t;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.KeyInfoName::.ctor()
extern "C"  void KeyInfoName__ctor_m3488526140 (KeyInfoName_t3176207690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoName::.ctor(System.String)
extern "C"  void KeyInfoName__ctor_m2861851006 (KeyInfoName_t3176207690 * __this, String_t* ___keyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.KeyInfoName::get_Value()
extern "C"  String_t* KeyInfoName_get_Value_m2364273111 (KeyInfoName_t3176207690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.KeyInfoName::GetXml()
extern "C"  XmlElement_t2877111883 * KeyInfoName_GetXml_m2904075703 (KeyInfoName_t3176207690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfoName::LoadXml(System.Xml.XmlElement)
extern "C"  void KeyInfoName_LoadXml_m1296158628 (KeyInfoName_t3176207690 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
