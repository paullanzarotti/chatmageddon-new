﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer
struct ChatmageddonServer_t594474938;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// System.String
struct String_t;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// Shield
struct Shield_t3327121081;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Action`3<System.Boolean,System.String,System.String>
struct Action_3_t3864773326;
// System.Action`3<System.Boolean,System.Collections.Generic.List`1<FAQ>,System.String>
struct Action_3_t1845567599;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_Gender3618070753.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"
#include "AssemblyU2DCSharp_KnockoutRemovalType4219541724.h"
#include "AssemblyU2DCSharp_Shield3327121081.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"
#include "AssemblyU2DCSharp_AboutAndHelpType4001655409.h"

// System.Void ChatmageddonServer::.ctor()
extern "C"  void ChatmageddonServer__ctor_m4230452579 (ChatmageddonServer_t594474938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::PingServer(System.Action`2<System.Boolean,System.String>)
extern "C"  void ChatmageddonServer_PingServer_m1337531634 (ChatmageddonServer_t594474938 * __this, Action_2_t1865222972 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::RegisterNewUser(System.String,System.String,System.String,System.String,System.String,System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void ChatmageddonServer_RegisterNewUser_m449641895 (ChatmageddonServer_t594474938 * __this, String_t* ___email0, String_t* ___username1, String_t* ___password2, String_t* ___passwordConfirm3, String_t* ___firstName4, String_t* ___secondName5, Action_2_t1865222972 * ___callBack6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::SubmitMobileNumber(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_SubmitMobileNumber_m249974985 (ChatmageddonServer_t594474938 * __this, String_t* ___countryCode0, String_t* ___mobileNumber1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::VerifyMobileNumber(System.String,System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_VerifyMobileNumber_m2287088762 (ChatmageddonServer_t594474938 * __this, String_t* ___mobileHash0, String_t* ___pinID1, String_t* ___pin2, Action_3_t3681841185 * ___callBack3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::SubmitMobilePin(System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void ChatmageddonServer_SubmitMobilePin_m2577933596 (ChatmageddonServer_t594474938 * __this, String_t* ___mobilePinID0, Action_2_t1865222972 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::LoginViaUsername(System.String,System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void ChatmageddonServer_LoginViaUsername_m2426370374 (ChatmageddonServer_t594474938 * __this, String_t* ___username0, String_t* ___password1, Action_2_t1865222972 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::NotifyAppStart(System.Action`2<System.Boolean,System.String>)
extern "C"  void ChatmageddonServer_NotifyAppStart_m3432782553 (ChatmageddonServer_t594474938 * __this, Action_2_t1865222972 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::ResetPoints(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_ResetPoints_m4188559892 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::MarkAllNotificationsRead(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_MarkAllNotificationsRead_m1089751932 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::RetrieveBlockedUsers(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_RetrieveBlockedUsers_m1608932176 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::BlockUser(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_BlockUser_m1363190330 (ChatmageddonServer_t594474938 * __this, String_t* ___UserID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UnblockUser(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UnblockUser_m3523721547 (ChatmageddonServer_t594474938 * __this, String_t* ___UserID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetLatestConfig(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetLatestConfig_m3630985913 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateProfileImage(UnityEngine.Texture2D,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateProfileImage_m2577585969 (ChatmageddonServer_t594474938 * __this, Texture2D_t3542995729 * ___image0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateURLProfileImage(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateURLProfileImage_m2912119752 (ChatmageddonServer_t594474938 * __this, String_t* ___imageURL0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserEmail(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserEmail_m234485714 (ChatmageddonServer_t594474938 * __this, String_t* ___email0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserUsername(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserUsername_m2930195306 (ChatmageddonServer_t594474938 * __this, String_t* ___username0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserPassword(System.String,System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserPassword_m3075323563 (ChatmageddonServer_t594474938 * __this, String_t* ___currentPassword0, String_t* ___password1, String_t* ___confirmPassword2, Action_3_t3681841185 * ___callBack3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserFullName(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserFullName_m2815998944 (ChatmageddonServer_t594474938 * __this, String_t* ___firstName0, String_t* ___lastName1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserFirstName(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserFirstName_m313605253 (ChatmageddonServer_t594474938 * __this, String_t* ___name0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserLastName(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserLastName_m524353673 (ChatmageddonServer_t594474938 * __this, String_t* ___name0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserGender(Gender,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserGender_m3253465000 (ChatmageddonServer_t594474938 * __this, int32_t ___gender0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserAudience(Audience,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserAudience_m2107381098 (ChatmageddonServer_t594474938 * __this, int32_t ___audience0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserDisplayLocation(System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserDisplayLocation_m409711626 (ChatmageddonServer_t594474938 * __this, bool ___displayLocation0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserAudioMusic(System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserAudioMusic_m3908265754 (ChatmageddonServer_t594474938 * __this, bool ___music0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserAudioSFX(System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserAudioSFX_m503973646 (ChatmageddonServer_t594474938 * __this, bool ___sfx0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserNotifications(System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserNotifications_m2649460575 (ChatmageddonServer_t594474938 * __this, bool ___enabled0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserNotificationSound(System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserNotificationSound_m4128026603 (ChatmageddonServer_t594474938 * __this, bool ___sound0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserNotificationVibrate(System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserNotificationVibrate_m2034468399 (ChatmageddonServer_t594474938 * __this, bool ___vibrate0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserLocation(System.Single,System.Single,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserLocation_m19342335 (ChatmageddonServer_t594474938 * __this, float ___longitude0, float ___latitude1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::UpdateUserStealthMode(System.Int32,System.Int32,System.Int32,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_UpdateUserStealthMode_m2273478283 (ChatmageddonServer_t594474938 * __this, int32_t ___startHour0, int32_t ___startMin1, int32_t ___duration2, Action_3_t3681841185 * ___callBack3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::DeactivateUserStealthMode(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_DeactivateUserStealthMode_m2892989331 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::DeactivateUserKnockout(KnockoutRemovalType,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_DeactivateUserKnockout_m82096427 (ChatmageddonServer_t594474938 * __this, int32_t ___removalType0, String_t* ___postID1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::ActivateShield(Shield,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_ActivateShield_m3473547795 (ChatmageddonServer_t594474938 * __this, Shield_t3327121081 * ___shield0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::DeactivateShield(Shield,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_DeactivateShield_m325168608 (ChatmageddonServer_t594474938 * __this, Shield_t3327121081 * ___shield0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::LaunchMissile(System.String,System.String,System.Single,System.Boolean,System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_LaunchMissile_m2205575210 (ChatmageddonServer_t594474938 * __this, String_t* ___missileID0, String_t* ___targetID1, float ___launchAccuracy2, bool ___perfectLaunch3, bool ___backfired4, Action_3_t3681841185 * ___callBack5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::DefendMissile(System.String,System.Boolean,System.DateTime,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_DefendMissile_m1907250073 (ChatmageddonServer_t594474938 * __this, String_t* ___missileID0, bool ___defended1, DateTime_t693205669  ___defendTime2, Action_3_t3681841185 * ___callBack3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetFullWarefareStatus(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetFullWarefareStatus_m992405678 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetMissileWarefareStatus(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetMissileWarefareStatus_m3758223651 (ChatmageddonServer_t594474938 * __this, String_t* ___missileID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetMineWarefareStatus(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetMineWarefareStatus_m2729867900 (ChatmageddonServer_t594474938 * __this, String_t* ___mineID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::ClearFullWarefareStatus(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_ClearFullWarefareStatus_m2365655985 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::ClearMissileWarefareStatus(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_ClearMissileWarefareStatus_m3980361508 (ChatmageddonServer_t594474938 * __this, String_t* ___missileID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::LayMine(System.String,System.Single,System.Single,MineAudience,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_LayMine_m1311365730 (ChatmageddonServer_t594474938 * __this, String_t* ___mineID0, float ___longitutde1, float ___latitude2, int32_t ___audience3, Action_3_t3681841185 * ___callBack4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::TriggerMine(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_TriggerMine_m2822577359 (ChatmageddonServer_t594474938 * __this, String_t* ___mineID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::DisarmMine(System.String,System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_DisarmMine_m3227938232 (ChatmageddonServer_t594474938 * __this, String_t* ___mineID0, bool ___success1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetUniversalFeed(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetUniversalFeed_m1393471569 (ChatmageddonServer_t594474938 * __this, String_t* ___url0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetChatFeed(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetChatFeed_m3147724702 (ChatmageddonServer_t594474938 * __this, String_t* ___url0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::SaveContactsToServer(System.Collections.Generic.List`1<System.String>,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_SaveContactsToServer_m3290342318 (ChatmageddonServer_t594474938 * __this, List_1_t1398341365 * ___contactHashList0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::DeleteContactsFromServer(System.Collections.Generic.List`1<System.String>,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_DeleteContactsFromServer_m4153862983 (ChatmageddonServer_t594474938 * __this, List_1_t1398341365 * ___contactHashList0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::ClearContactsFromServer(System.Action`2<System.Boolean,System.String>)
extern "C"  void ChatmageddonServer_ClearContactsFromServer_m3198107742 (ChatmageddonServer_t594474938 * __this, Action_2_t1865222972 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetPurchasableItems(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetPurchasableItems_m1767740296 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::PurchaseItem(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_PurchaseItem_m3452527244 (ChatmageddonServer_t594474938 * __this, String_t* ___id0, String_t* ___duration1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::VerifyInAppPurchase(System.String,System.Boolean,System.Action`2<System.Boolean,System.String>)
extern "C"  void ChatmageddonServer_VerifyInAppPurchase_m2462855012 (ChatmageddonServer_t594474938 * __this, String_t* ___receipt0, bool ___dev1, Action_2_t1865222972 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetFullInventory(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetFullInventory_m3635419637 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetMissileInventory(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetMissileInventory_m3532413764 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetShieldInventory(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetShieldInventory_m245227489 (ChatmageddonServer_t594474938 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetLeaderboardPage(System.Int32,System.Int32,System.Boolean,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_GetLeaderboardPage_m4034304593 (ChatmageddonServer_t594474938 * __this, int32_t ___page0, int32_t ___pageDataCount1, bool ___dailyPoints2, Action_3_t3681841185 * ___callBack3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatmageddonServer::TrimContent(System.String)
extern "C"  String_t* ChatmageddonServer_TrimContent_m464819667 (ChatmageddonServer_t594474938 * __this, String_t* ___contentString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatmageddonServer::DecodeHtmlChars(System.String)
extern "C"  String_t* ChatmageddonServer_DecodeHtmlChars_m144147400 (ChatmageddonServer_t594474938 * __this, String_t* ___aText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatmageddonServer::GetPostNumber(AboutAndHelpType)
extern "C"  String_t* ChatmageddonServer_GetPostNumber_m1437359210 (ChatmageddonServer_t594474938 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetAboutAndHelpInfo(AboutAndHelpType,System.Action`3<System.Boolean,System.String,System.String>)
extern "C"  void ChatmageddonServer_GetAboutAndHelpInfo_m3383754944 (ChatmageddonServer_t594474938 * __this, int32_t ___type0, Action_3_t3864773326 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::GetFAQInfo(System.Action`3<System.Boolean,System.Collections.Generic.List`1<FAQ>,System.String>)
extern "C"  void ChatmageddonServer_GetFAQInfo_m3842624096 (ChatmageddonServer_t594474938 * __this, Action_3_t1845567599 * ___callBack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatmageddonServer::ProcessErrorCode(System.String)
extern "C"  bool ChatmageddonServer_ProcessErrorCode_m298683935 (ChatmageddonServer_t594474938 * __this, String_t* ___errorCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::ActivateUserStealthMode(System.Int32,System.Int32,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_ActivateUserStealthMode_m2109104884 (ChatmageddonServer_t594474938 * __this, int32_t ___delayMins0, int32_t ___durationHours1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::ActivateUserKnockout(System.String,System.Int32,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_ActivateUserKnockout_m411310845 (ChatmageddonServer_t594474938 * __this, String_t* ___userID0, int32_t ___durationMins1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer::SetTotalPoints(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void ChatmageddonServer_SetTotalPoints_m3623681247 (ChatmageddonServer_t594474938 * __this, String_t* ___points0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
