﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTrue1672815575.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFalse2929064940.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLang718178243.h"
#include "System_Xml_System_Xml_XPath_XPathNumericFunction2367269690.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNumber1210619362.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSum2438242634.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionFloor2377969453.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCeil4011711386.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionRound711107915.h"
#include "System_Xml_System_Xml_XPath_CompiledExpression3686330919.h"
#include "System_Xml_System_Xml_XPath_XPathSortElement1040291071.h"
#include "System_Xml_System_Xml_XPath_XPathSorters4019574815.h"
#include "System_Xml_System_Xml_XPath_XPathSorter3491953490.h"
#include "System_Xml_System_Xml_XPath_Expression1283317256.h"
#include "System_Xml_System_Xml_XPath_ExprBinary2134514854.h"
#include "System_Xml_System_Xml_XPath_ExprBoolean2584142721.h"
#include "System_Xml_System_Xml_XPath_ExprOR3873281492.h"
#include "System_Xml_System_Xml_XPath_ExprAND3057430278.h"
#include "System_Xml_System_Xml_XPath_EqualityExpr2363998129.h"
#include "System_Xml_System_Xml_XPath_ExprEQ1097343967.h"
#include "System_Xml_System_Xml_XPath_ExprNE1144398114.h"
#include "System_Xml_System_Xml_XPath_RelationalExpr3066272106.h"
#include "System_Xml_System_Xml_XPath_ExprGT4229511846.h"
#include "System_Xml_System_Xml_XPath_ExprGE4229511829.h"
#include "System_Xml_System_Xml_XPath_ExprLT2307197545.h"
#include "System_Xml_System_Xml_XPath_ExprLE2307197528.h"
#include "System_Xml_System_Xml_XPath_ExprNumeric503612810.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS627624341.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS3273980157.h"
#include "System_Xml_System_Xml_XPath_ExprMULT1293289955.h"
#include "System_Xml_System_Xml_XPath_ExprDIV1827373722.h"
#include "System_Xml_System_Xml_XPath_ExprMOD3265849921.h"
#include "System_Xml_System_Xml_XPath_ExprNEG1881334439.h"
#include "System_Xml_System_Xml_XPath_NodeSet2895962396.h"
#include "System_Xml_System_Xml_XPath_ExprUNION1011953538.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH1432925936.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH21892232446.h"
#include "System_Xml_System_Xml_XPath_ExprRoot2449183281.h"
#include "System_Xml_System_Xml_XPath_Axes2027048019.h"
#include "System_Xml_System_Xml_XPath_AxisSpecifier201930955.h"
#include "System_Xml_System_Xml_XPath_NodeTest2072055152.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest383960256.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest972157009.h"
#include "System_Xml_System_Xml_XPath_ExprFilter1897694423.h"
#include "System_Xml_System_Xml_XPath_ExprNumber2687440122.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral1691835634.h"
#include "System_Xml_System_Xml_XPath_ExprVariable3362236065.h"
#include "System_Xml_System_Xml_XPath_ExprParens545584810.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments2900945452.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall1376373777.h"
#include "System_Xml_System_Xml_XPath_BaseIterator2454437973.h"
#include "System_Xml_System_Xml_XPath_WrapperIterator2718786873.h"
#include "System_Xml_System_Xml_XPath_SimpleIterator833624542.h"
#include "System_Xml_System_Xml_XPath_SelfIterator1886393192.h"
#include "System_Xml_System_Xml_XPath_NullIterator2539636145.h"
#include "System_Xml_System_Xml_XPath_ParensIterator1637647049.h"
#include "System_Xml_System_Xml_XPath_ParentIterator1638759120.h"
#include "System_Xml_System_Xml_XPath_ChildIterator1144511930.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterato681640159.h"
#include "System_Xml_System_Xml_XPath_PrecedingSiblingIterat3076405737.h"
#include "System_Xml_System_Xml_XPath_AncestorIterator4023303433.h"
#include "System_Xml_System_Xml_XPath_AncestorOrSelfIterator3792308554.h"
#include "System_Xml_System_Xml_XPath_DescendantIterator490680463.h"
#include "System_Xml_System_Xml_XPath_DescendantOrSelfIterat3468500080.h"
#include "System_Xml_System_Xml_XPath_FollowingIterator2758624297.h"
#include "System_Xml_System_Xml_XPath_PrecedingIterator3277790919.h"
#include "System_Xml_System_Xml_XPath_NamespaceIterator3683603305.h"
#include "System_Xml_System_Xml_XPath_AttributeIterator2403056388.h"
#include "System_Xml_System_Xml_XPath_AxisIterator3253336785.h"
#include "System_Xml_System_Xml_XPath_SimpleSlashIterator400959233.h"
#include "System_Xml_System_Xml_XPath_SortedIterator3199707209.h"
#include "System_Xml_System_Xml_XPath_SlashIterator3247429967.h"
#include "System_Xml_System_Xml_XPath_PredicateIterator2401536495.h"
#include "System_Xml_System_Xml_XPath_ListIterator3804705054.h"
#include "System_Xml_System_Xml_XPath_UnionIterator284038993.h"
#include "System_Xml_Mono_Xml_XPath_Tokenizer2998805977.h"
#include "System_Xml_System_Xml_Xsl_XsltContext2013960098.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet2758097827.h"
#include "System_Xml_Mono_Xml_Schema_XsdOrdering3741887935.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1096449895.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType3359210273.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic2904699188.h"
#include "System_Xml_Mono_Xml_Schema_XsdString263933896.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2132216291.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2902215462.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage3897851897.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken547135877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens1753890866.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1409945702.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName414304927.h"
#include "System_Xml_Mono_Xml_Schema_XsdID1067193160.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef1377311049.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs763119546.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity2664305022.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1103053540.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation720379093.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal2932251550.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (XPathFunctionTrue_t1672815575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (XPathFunctionFalse_t2929064940), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (XPathFunctionLang_t718178243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1302[1] = 
{
	XPathFunctionLang_t718178243::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (XPathNumericFunction_t2367269690), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (XPathFunctionNumber_t1210619362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1304[1] = 
{
	XPathFunctionNumber_t1210619362::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (XPathFunctionSum_t2438242634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1305[1] = 
{
	XPathFunctionSum_t2438242634::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (XPathFunctionFloor_t2377969453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1306[1] = 
{
	XPathFunctionFloor_t2377969453::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (XPathFunctionCeil_t4011711386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[1] = 
{
	XPathFunctionCeil_t4011711386::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (XPathFunctionRound_t711107915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[1] = 
{
	XPathFunctionRound_t711107915::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (CompiledExpression_t3686330919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1309[4] = 
{
	CompiledExpression_t3686330919::get_offset_of__nsm_0(),
	CompiledExpression_t3686330919::get_offset_of__expr_1(),
	CompiledExpression_t3686330919::get_offset_of__sorters_2(),
	CompiledExpression_t3686330919::get_offset_of_rawExpression_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (XPathSortElement_t1040291071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1310[2] = 
{
	XPathSortElement_t1040291071::get_offset_of_Navigator_0(),
	XPathSortElement_t1040291071::get_offset_of_Values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (XPathSorters_t4019574815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1311[1] = 
{
	XPathSorters_t4019574815::get_offset_of__rgSorters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (XPathSorter_t3491953490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1312[3] = 
{
	XPathSorter_t3491953490::get_offset_of__expr_0(),
	XPathSorter_t3491953490::get_offset_of__cmp_1(),
	XPathSorter_t3491953490::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (Expression_t1283317256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (ExprBinary_t2134514854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1314[2] = 
{
	ExprBinary_t2134514854::get_offset_of__left_0(),
	ExprBinary_t2134514854::get_offset_of__right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (ExprBoolean_t2584142721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (ExprOR_t3873281492), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (ExprAND_t3057430278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (EqualityExpr_t2363998129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1318[1] = 
{
	EqualityExpr_t2363998129::get_offset_of_trueVal_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (ExprEQ_t1097343967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (ExprNE_t1144398114), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (RelationalExpr_t3066272106), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (ExprGT_t4229511846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (ExprGE_t4229511829), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (ExprLT_t2307197545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (ExprLE_t2307197528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (ExprNumeric_t503612810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (ExprPLUS_t627624341), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (ExprMINUS_t3273980157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (ExprMULT_t1293289955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (ExprDIV_t1827373722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (ExprMOD_t3265849921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (ExprNEG_t1881334439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1332[1] = 
{
	ExprNEG_t1881334439::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (NodeSet_t2895962396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (ExprUNION_t1011953538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1334[2] = 
{
	ExprUNION_t1011953538::get_offset_of_left_0(),
	ExprUNION_t1011953538::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (ExprSLASH_t1432925936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1335[2] = 
{
	ExprSLASH_t1432925936::get_offset_of_left_0(),
	ExprSLASH_t1432925936::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (ExprSLASH2_t1892232446), -1, sizeof(ExprSLASH2_t1892232446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1336[3] = 
{
	ExprSLASH2_t1892232446::get_offset_of_left_0(),
	ExprSLASH2_t1892232446::get_offset_of_right_1(),
	ExprSLASH2_t1892232446_StaticFields::get_offset_of_DescendantOrSelfStar_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (ExprRoot_t2449183281), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (Axes_t2027048019)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1338[14] = 
{
	Axes_t2027048019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (AxisSpecifier_t201930955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1339[1] = 
{
	AxisSpecifier_t201930955::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (NodeTest_t2072055152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1340[1] = 
{
	NodeTest_t2072055152::get_offset_of__axis_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (NodeTypeTest_t383960256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1341[2] = 
{
	NodeTypeTest_t383960256::get_offset_of_type_1(),
	NodeTypeTest_t383960256::get_offset_of__param_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (NodeNameTest_t972157009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1342[2] = 
{
	NodeNameTest_t972157009::get_offset_of__name_1(),
	NodeNameTest_t972157009::get_offset_of_resolvedName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (ExprFilter_t1897694423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1343[2] = 
{
	ExprFilter_t1897694423::get_offset_of_expr_0(),
	ExprFilter_t1897694423::get_offset_of_pred_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (ExprNumber_t2687440122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1344[1] = 
{
	ExprNumber_t2687440122::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (ExprLiteral_t1691835634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1345[1] = 
{
	ExprLiteral_t1691835634::get_offset_of__value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (ExprVariable_t3362236065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1346[2] = 
{
	ExprVariable_t3362236065::get_offset_of__name_0(),
	ExprVariable_t3362236065::get_offset_of_resolvedName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (ExprParens_t545584810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1347[1] = 
{
	ExprParens_t545584810::get_offset_of__expr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (FunctionArguments_t2900945452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1348[2] = 
{
	FunctionArguments_t2900945452::get_offset_of__arg_0(),
	FunctionArguments_t2900945452::get_offset_of__tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (ExprFunctionCall_t1376373777), -1, sizeof(ExprFunctionCall_t1376373777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1349[4] = 
{
	ExprFunctionCall_t1376373777::get_offset_of__name_0(),
	ExprFunctionCall_t1376373777::get_offset_of_resolvedName_1(),
	ExprFunctionCall_t1376373777::get_offset_of__args_2(),
	ExprFunctionCall_t1376373777_StaticFields::get_offset_of_U3CU3Ef__switchU24map3C_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (BaseIterator_t2454437973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1350[2] = 
{
	BaseIterator_t2454437973::get_offset_of__nsm_1(),
	BaseIterator_t2454437973::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (WrapperIterator_t2718786873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1351[1] = 
{
	WrapperIterator_t2718786873::get_offset_of_iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (SimpleIterator_t833624542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1352[3] = 
{
	SimpleIterator_t833624542::get_offset_of__nav_3(),
	SimpleIterator_t833624542::get_offset_of__current_4(),
	SimpleIterator_t833624542::get_offset_of_skipfirst_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (SelfIterator_t1886393192), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (NullIterator_t2539636145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (ParensIterator_t1637647049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1355[1] = 
{
	ParensIterator_t1637647049::get_offset_of__iter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (ParentIterator_t1638759120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1356[1] = 
{
	ParentIterator_t1638759120::get_offset_of_canMove_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (ChildIterator_t1144511930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1357[1] = 
{
	ChildIterator_t1144511930::get_offset_of__nav_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (FollowingSiblingIterator_t681640159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (PrecedingSiblingIterator_t3076405737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1359[3] = 
{
	PrecedingSiblingIterator_t3076405737::get_offset_of_finished_6(),
	PrecedingSiblingIterator_t3076405737::get_offset_of_started_7(),
	PrecedingSiblingIterator_t3076405737::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (AncestorIterator_t4023303433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1360[3] = 
{
	AncestorIterator_t4023303433::get_offset_of_currentPosition_6(),
	AncestorIterator_t4023303433::get_offset_of_navigators_7(),
	AncestorIterator_t4023303433::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (AncestorOrSelfIterator_t3792308554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1361[3] = 
{
	AncestorOrSelfIterator_t3792308554::get_offset_of_currentPosition_6(),
	AncestorOrSelfIterator_t3792308554::get_offset_of_navigators_7(),
	AncestorOrSelfIterator_t3792308554::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (DescendantIterator_t490680463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1362[2] = 
{
	DescendantIterator_t490680463::get_offset_of__depth_6(),
	DescendantIterator_t490680463::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (DescendantOrSelfIterator_t3468500080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1363[2] = 
{
	DescendantOrSelfIterator_t3468500080::get_offset_of__depth_6(),
	DescendantOrSelfIterator_t3468500080::get_offset_of__finished_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (FollowingIterator_t2758624297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1364[1] = 
{
	FollowingIterator_t2758624297::get_offset_of__finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (PrecedingIterator_t3277790919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1365[3] = 
{
	PrecedingIterator_t3277790919::get_offset_of_finished_6(),
	PrecedingIterator_t3277790919::get_offset_of_started_7(),
	PrecedingIterator_t3277790919::get_offset_of_startPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (NamespaceIterator_t3683603305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (AttributeIterator_t2403056388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (AxisIterator_t3253336785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1368[2] = 
{
	AxisIterator_t3253336785::get_offset_of__iter_3(),
	AxisIterator_t3253336785::get_offset_of__test_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (SimpleSlashIterator_t400959233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1369[4] = 
{
	SimpleSlashIterator_t400959233::get_offset_of__expr_3(),
	SimpleSlashIterator_t400959233::get_offset_of__left_4(),
	SimpleSlashIterator_t400959233::get_offset_of__right_5(),
	SimpleSlashIterator_t400959233::get_offset_of__current_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (SortedIterator_t3199707209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1370[1] = 
{
	SortedIterator_t3199707209::get_offset_of_list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (SlashIterator_t3247429967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1371[6] = 
{
	SlashIterator_t3247429967::get_offset_of__iterLeft_3(),
	SlashIterator_t3247429967::get_offset_of__iterRight_4(),
	SlashIterator_t3247429967::get_offset_of__expr_5(),
	SlashIterator_t3247429967::get_offset_of__iterList_6(),
	SlashIterator_t3247429967::get_offset_of__finished_7(),
	SlashIterator_t3247429967::get_offset_of__nextIterRight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (PredicateIterator_t2401536495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1372[4] = 
{
	PredicateIterator_t2401536495::get_offset_of__iter_3(),
	PredicateIterator_t2401536495::get_offset_of__pred_4(),
	PredicateIterator_t2401536495::get_offset_of_resType_5(),
	PredicateIterator_t2401536495::get_offset_of_finished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (ListIterator_t3804705054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1373[1] = 
{
	ListIterator_t3804705054::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (UnionIterator_t284038993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1374[5] = 
{
	UnionIterator_t284038993::get_offset_of__left_3(),
	UnionIterator_t284038993::get_offset_of__right_4(),
	UnionIterator_t284038993::get_offset_of_keepLeft_5(),
	UnionIterator_t284038993::get_offset_of_keepRight_6(),
	UnionIterator_t284038993::get_offset_of__current_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (Tokenizer_t2998805977), -1, sizeof(Tokenizer_t2998805977_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1375[10] = 
{
	Tokenizer_t2998805977::get_offset_of_m_rgchInput_0(),
	Tokenizer_t2998805977::get_offset_of_m_ich_1(),
	Tokenizer_t2998805977::get_offset_of_m_cch_2(),
	Tokenizer_t2998805977::get_offset_of_m_iToken_3(),
	Tokenizer_t2998805977::get_offset_of_m_iTokenPrev_4(),
	Tokenizer_t2998805977::get_offset_of_m_objToken_5(),
	Tokenizer_t2998805977::get_offset_of_m_fPrevWasOperator_6(),
	Tokenizer_t2998805977::get_offset_of_m_fThisIsOperator_7(),
	Tokenizer_t2998805977_StaticFields::get_offset_of_s_mapTokens_8(),
	Tokenizer_t2998805977_StaticFields::get_offset_of_s_rgTokenMap_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (XsltContext_t2013960098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1380[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (XsdOrdering_t3741887935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1381[5] = 
{
	XsdOrdering_t3741887935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1382[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
