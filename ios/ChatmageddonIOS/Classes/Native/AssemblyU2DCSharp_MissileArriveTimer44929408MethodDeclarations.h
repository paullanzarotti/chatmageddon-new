﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileArriveTimer
struct MissileArriveTimer_t44929408;
// MissileArriveTimer/OnZeroHit
struct OnZeroHit_t3980744303;
// MissileArriveTimer/OnTimerUpdate
struct OnTimerUpdate_t2669454040;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MissileArriveTimer_OnZeroHit3980744303.h"
#include "AssemblyU2DCSharp_MissileArriveTimer_OnTimerUpdate2669454040.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void MissileArriveTimer::.ctor()
extern "C"  void MissileArriveTimer__ctor_m3443068339 (MissileArriveTimer_t44929408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::add_onZeroHit(MissileArriveTimer/OnZeroHit)
extern "C"  void MissileArriveTimer_add_onZeroHit_m3189935898 (MissileArriveTimer_t44929408 * __this, OnZeroHit_t3980744303 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::remove_onZeroHit(MissileArriveTimer/OnZeroHit)
extern "C"  void MissileArriveTimer_remove_onZeroHit_m996103233 (MissileArriveTimer_t44929408 * __this, OnZeroHit_t3980744303 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::add_onTimerUpdate(MissileArriveTimer/OnTimerUpdate)
extern "C"  void MissileArriveTimer_add_onTimerUpdate_m3587959840 (MissileArriveTimer_t44929408 * __this, OnTimerUpdate_t2669454040 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::remove_onTimerUpdate(MissileArriveTimer/OnTimerUpdate)
extern "C"  void MissileArriveTimer_remove_onTimerUpdate_m3136163845 (MissileArriveTimer_t44929408 * __this, OnTimerUpdate_t2669454040 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::StartCountdownTimer(LaunchedMissile)
extern "C"  void MissileArriveTimer_StartCountdownTimer_m3447530101 (MissileArriveTimer_t44929408 * __this, LaunchedMissile_t1542515810 * ___missile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::StopCountdownTimer()
extern "C"  void MissileArriveTimer_StopCountdownTimer_m2323016937 (MissileArriveTimer_t44929408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MissileArriveTimer::CalculateArriveTime()
extern "C"  Il2CppObject * MissileArriveTimer_CalculateArriveTime_m2809347471 (MissileArriveTimer_t44929408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::SetTimeLabel(System.TimeSpan)
extern "C"  void MissileArriveTimer_SetTimeLabel_m2368306268 (MissileArriveTimer_t44929408 * __this, TimeSpan_t3430258949  ___span0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::SetContentColour(UnityEngine.Color)
extern "C"  void MissileArriveTimer_SetContentColour_m1662676986 (MissileArriveTimer_t44929408 * __this, Color_t2020392075  ___colour0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::SendZeroHitEvent()
extern "C"  void MissileArriveTimer_SendZeroHitEvent_m3959433176 (MissileArriveTimer_t44929408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileArriveTimer::SendTimeUpdateEvent(System.TimeSpan)
extern "C"  void MissileArriveTimer_SendTimeUpdateEvent_m594480713 (MissileArriveTimer_t44929408 * __this, TimeSpan_t3430258949  ___timeDifference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
