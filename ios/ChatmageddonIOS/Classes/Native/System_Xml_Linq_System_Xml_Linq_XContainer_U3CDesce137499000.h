﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XNode>
struct IEnumerator_1_t183028041;
// System.Xml.Linq.XNode
struct XNode_t2707504214;
// System.Xml.Linq.XContainer
struct XContainer_t1445911831;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B
struct  U3CDescendantNodesU3Ec__Iterator1B_t137499000  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XNode> System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::<$s_45>__0
	Il2CppObject* ___U3CU24s_45U3E__0_0;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::<n>__1
	XNode_t2707504214 * ___U3CnU3E__1_1;
	// System.Xml.Linq.XContainer System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::<c>__2
	XContainer_t1445911831 * ___U3CcU3E__2_2;
	// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XNode> System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::<$s_46>__3
	Il2CppObject* ___U3CU24s_46U3E__3_3;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::<d>__4
	XNode_t2707504214 * ___U3CdU3E__4_4;
	// System.Int32 System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::$PC
	int32_t ___U24PC_5;
	// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::$current
	XNode_t2707504214 * ___U24current_6;
	// System.Xml.Linq.XContainer System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::<>f__this
	XContainer_t1445911831 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CU24s_45U3E__0_0() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U3CU24s_45U3E__0_0)); }
	inline Il2CppObject* get_U3CU24s_45U3E__0_0() const { return ___U3CU24s_45U3E__0_0; }
	inline Il2CppObject** get_address_of_U3CU24s_45U3E__0_0() { return &___U3CU24s_45U3E__0_0; }
	inline void set_U3CU24s_45U3E__0_0(Il2CppObject* value)
	{
		___U3CU24s_45U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_45U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CnU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U3CnU3E__1_1)); }
	inline XNode_t2707504214 * get_U3CnU3E__1_1() const { return ___U3CnU3E__1_1; }
	inline XNode_t2707504214 ** get_address_of_U3CnU3E__1_1() { return &___U3CnU3E__1_1; }
	inline void set_U3CnU3E__1_1(XNode_t2707504214 * value)
	{
		___U3CnU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CcU3E__2_2() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U3CcU3E__2_2)); }
	inline XContainer_t1445911831 * get_U3CcU3E__2_2() const { return ___U3CcU3E__2_2; }
	inline XContainer_t1445911831 ** get_address_of_U3CcU3E__2_2() { return &___U3CcU3E__2_2; }
	inline void set_U3CcU3E__2_2(XContainer_t1445911831 * value)
	{
		___U3CcU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_46U3E__3_3() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U3CU24s_46U3E__3_3)); }
	inline Il2CppObject* get_U3CU24s_46U3E__3_3() const { return ___U3CU24s_46U3E__3_3; }
	inline Il2CppObject** get_address_of_U3CU24s_46U3E__3_3() { return &___U3CU24s_46U3E__3_3; }
	inline void set_U3CU24s_46U3E__3_3(Il2CppObject* value)
	{
		___U3CU24s_46U3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_46U3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CdU3E__4_4() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U3CdU3E__4_4)); }
	inline XNode_t2707504214 * get_U3CdU3E__4_4() const { return ___U3CdU3E__4_4; }
	inline XNode_t2707504214 ** get_address_of_U3CdU3E__4_4() { return &___U3CdU3E__4_4; }
	inline void set_U3CdU3E__4_4(XNode_t2707504214 * value)
	{
		___U3CdU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U24current_6)); }
	inline XNode_t2707504214 * get_U24current_6() const { return ___U24current_6; }
	inline XNode_t2707504214 ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(XNode_t2707504214 * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CDescendantNodesU3Ec__Iterator1B_t137499000, ___U3CU3Ef__this_7)); }
	inline XContainer_t1445911831 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline XContainer_t1445911831 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(XContainer_t1445911831 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
