﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineModal
struct MineModal_t1141032720;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayMineButton
struct  LayMineButton_t2913210547  : public SFXButton_t792651341
{
public:
	// MineModal LayMineButton::modal
	MineModal_t1141032720 * ___modal_5;
	// MineAudience LayMineButton::mineAudience
	int32_t ___mineAudience_6;

public:
	inline static int32_t get_offset_of_modal_5() { return static_cast<int32_t>(offsetof(LayMineButton_t2913210547, ___modal_5)); }
	inline MineModal_t1141032720 * get_modal_5() const { return ___modal_5; }
	inline MineModal_t1141032720 ** get_address_of_modal_5() { return &___modal_5; }
	inline void set_modal_5(MineModal_t1141032720 * value)
	{
		___modal_5 = value;
		Il2CppCodeGenWriteBarrier(&___modal_5, value);
	}

	inline static int32_t get_offset_of_mineAudience_6() { return static_cast<int32_t>(offsetof(LayMineButton_t2913210547, ___mineAudience_6)); }
	inline int32_t get_mineAudience_6() const { return ___mineAudience_6; }
	inline int32_t* get_address_of_mineAudience_6() { return &___mineAudience_6; }
	inline void set_mineAudience_6(int32_t value)
	{
		___mineAudience_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
