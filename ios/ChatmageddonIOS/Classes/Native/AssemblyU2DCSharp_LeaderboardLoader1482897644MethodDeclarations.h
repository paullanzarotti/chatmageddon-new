﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardLoader
struct LeaderboardLoader_t1482897644;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardLoader::.ctor()
extern "C"  void LeaderboardLoader__ctor_m275486943 (LeaderboardLoader_t1482897644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardLoader::Start()
extern "C"  void LeaderboardLoader_Start_m687292083 (LeaderboardLoader_t1482897644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardLoader::Update()
extern "C"  void LeaderboardLoader_Update_m3594668426 (LeaderboardLoader_t1482897644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
