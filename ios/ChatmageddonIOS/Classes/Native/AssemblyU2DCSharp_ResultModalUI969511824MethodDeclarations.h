﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultModalUI
struct ResultModalUI_t969511824;
// LaunchedItem
struct LaunchedItem_t3670634427;
// Friend
struct Friend_t3555014108;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427.h"
#include "AssemblyU2DCSharp_LaunchedItemType186177771.h"
#include "AssemblyU2DCSharp_GameResult1017068805.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ResultModalUI::.ctor()
extern "C"  void ResultModalUI__ctor_m2926345057 (ResultModalUI_t969511824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::LoadResultData(LaunchedItem)
extern "C"  void ResultModalUI_LoadResultData_m3999815939 (ResultModalUI_t969511824 * __this, LaunchedItem_t3670634427 * ___itemResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::LoadItemUI(LaunchedItemType)
extern "C"  void ResultModalUI_LoadItemUI_m2920401035 (ResultModalUI_t969511824 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Friend ResultModalUI::GetItemFriend()
extern "C"  Friend_t3555014108 * ResultModalUI_GetItemFriend_m2898923097 (ResultModalUI_t969511824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::SetFriendData()
extern "C"  void ResultModalUI_SetFriendData_m818157677 (ResultModalUI_t969511824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::SetFlamesActive(System.Boolean)
extern "C"  void ResultModalUI_SetFlamesActive_m1656266204 (ResultModalUI_t969511824 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::SetFriendPtsLabel(GameResult,System.Int32)
extern "C"  void ResultModalUI_SetFriendPtsLabel_m1305264942 (ResultModalUI_t969511824 * __this, int32_t ___result0, int32_t ___ptsAmount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::SetActionLabel(System.String)
extern "C"  void ResultModalUI_SetActionLabel_m3906393281 (ResultModalUI_t969511824 * __this, String_t* ___actionText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::SetResultLabels(GameResult,System.Int32)
extern "C"  void ResultModalUI_SetResultLabels_m1285894421 (ResultModalUI_t969511824 * __this, int32_t ___result0, int32_t ___ptsAmount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::SetLockoutState()
extern "C"  void ResultModalUI_SetLockoutState_m3192721417 (ResultModalUI_t969511824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultModalUI::SetResultsStateLabel(GameResult,System.String)
extern "C"  void ResultModalUI_SetResultsStateLabel_m4142908921 (ResultModalUI_t969511824 * __this, int32_t ___result0, String_t* ___stateText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
