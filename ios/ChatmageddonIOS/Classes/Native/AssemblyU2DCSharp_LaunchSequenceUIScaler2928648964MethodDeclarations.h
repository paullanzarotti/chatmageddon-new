﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchSequenceUIScaler
struct LaunchSequenceUIScaler_t2928648964;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchSequenceUIScaler::.ctor()
extern "C"  void LaunchSequenceUIScaler__ctor_m245127855 (LaunchSequenceUIScaler_t2928648964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchSequenceUIScaler::GlobalUIScale()
extern "C"  void LaunchSequenceUIScaler_GlobalUIScale_m3983567958 (LaunchSequenceUIScaler_t2928648964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
