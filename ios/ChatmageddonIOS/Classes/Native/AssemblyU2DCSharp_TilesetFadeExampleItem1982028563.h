﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t193706927;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TilesetFadeExampleItem
struct  TilesetFadeExampleItem_t1982028563  : public Il2CppObject
{
public:
	// System.Boolean TilesetFadeExampleItem::finished
	bool ___finished_3;
	// UnityEngine.Material TilesetFadeExampleItem::material
	Material_t193706927 * ___material_4;
	// OnlineMapsTile TilesetFadeExampleItem::tile
	OnlineMapsTile_t21329940 * ___tile_5;
	// System.Single TilesetFadeExampleItem::alpha
	float ___alpha_6;
	// System.Int64 TilesetFadeExampleItem::startTicks
	int64_t ___startTicks_7;

public:
	inline static int32_t get_offset_of_finished_3() { return static_cast<int32_t>(offsetof(TilesetFadeExampleItem_t1982028563, ___finished_3)); }
	inline bool get_finished_3() const { return ___finished_3; }
	inline bool* get_address_of_finished_3() { return &___finished_3; }
	inline void set_finished_3(bool value)
	{
		___finished_3 = value;
	}

	inline static int32_t get_offset_of_material_4() { return static_cast<int32_t>(offsetof(TilesetFadeExampleItem_t1982028563, ___material_4)); }
	inline Material_t193706927 * get_material_4() const { return ___material_4; }
	inline Material_t193706927 ** get_address_of_material_4() { return &___material_4; }
	inline void set_material_4(Material_t193706927 * value)
	{
		___material_4 = value;
		Il2CppCodeGenWriteBarrier(&___material_4, value);
	}

	inline static int32_t get_offset_of_tile_5() { return static_cast<int32_t>(offsetof(TilesetFadeExampleItem_t1982028563, ___tile_5)); }
	inline OnlineMapsTile_t21329940 * get_tile_5() const { return ___tile_5; }
	inline OnlineMapsTile_t21329940 ** get_address_of_tile_5() { return &___tile_5; }
	inline void set_tile_5(OnlineMapsTile_t21329940 * value)
	{
		___tile_5 = value;
		Il2CppCodeGenWriteBarrier(&___tile_5, value);
	}

	inline static int32_t get_offset_of_alpha_6() { return static_cast<int32_t>(offsetof(TilesetFadeExampleItem_t1982028563, ___alpha_6)); }
	inline float get_alpha_6() const { return ___alpha_6; }
	inline float* get_address_of_alpha_6() { return &___alpha_6; }
	inline void set_alpha_6(float value)
	{
		___alpha_6 = value;
	}

	inline static int32_t get_offset_of_startTicks_7() { return static_cast<int32_t>(offsetof(TilesetFadeExampleItem_t1982028563, ___startTicks_7)); }
	inline int64_t get_startTicks_7() const { return ___startTicks_7; }
	inline int64_t* get_address_of_startTicks_7() { return &___startTicks_7; }
	inline void set_startTicks_7(int64_t value)
	{
		___startTicks_7 = value;
	}
};

struct TilesetFadeExampleItem_t1982028563_StaticFields
{
public:
	// UnityEngine.Color TilesetFadeExampleItem::fromColor
	Color_t2020392075  ___fromColor_1;
	// UnityEngine.Color TilesetFadeExampleItem::toColor
	Color_t2020392075  ___toColor_2;

public:
	inline static int32_t get_offset_of_fromColor_1() { return static_cast<int32_t>(offsetof(TilesetFadeExampleItem_t1982028563_StaticFields, ___fromColor_1)); }
	inline Color_t2020392075  get_fromColor_1() const { return ___fromColor_1; }
	inline Color_t2020392075 * get_address_of_fromColor_1() { return &___fromColor_1; }
	inline void set_fromColor_1(Color_t2020392075  value)
	{
		___fromColor_1 = value;
	}

	inline static int32_t get_offset_of_toColor_2() { return static_cast<int32_t>(offsetof(TilesetFadeExampleItem_t1982028563_StaticFields, ___toColor_2)); }
	inline Color_t2020392075  get_toColor_2() const { return ___toColor_2; }
	inline Color_t2020392075 * get_address_of_toColor_2() { return &___toColor_2; }
	inline void set_toColor_2(Color_t2020392075  value)
	{
		___toColor_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
