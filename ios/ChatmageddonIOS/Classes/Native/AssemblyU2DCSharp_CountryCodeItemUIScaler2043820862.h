﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t1795115428;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountryCodeItemUIScaler
struct  CountryCodeItemUIScaler_t2043820862  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UILabel CountryCodeItemUIScaler::itemlabel
	UILabel_t1795115428 * ___itemlabel_14;
	// UnityEngine.BoxCollider CountryCodeItemUIScaler::itemcollider
	BoxCollider_t22920061 * ___itemcollider_15;
	// UISprite CountryCodeItemUIScaler::itemDivider
	UISprite_t603616735 * ___itemDivider_16;

public:
	inline static int32_t get_offset_of_itemlabel_14() { return static_cast<int32_t>(offsetof(CountryCodeItemUIScaler_t2043820862, ___itemlabel_14)); }
	inline UILabel_t1795115428 * get_itemlabel_14() const { return ___itemlabel_14; }
	inline UILabel_t1795115428 ** get_address_of_itemlabel_14() { return &___itemlabel_14; }
	inline void set_itemlabel_14(UILabel_t1795115428 * value)
	{
		___itemlabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___itemlabel_14, value);
	}

	inline static int32_t get_offset_of_itemcollider_15() { return static_cast<int32_t>(offsetof(CountryCodeItemUIScaler_t2043820862, ___itemcollider_15)); }
	inline BoxCollider_t22920061 * get_itemcollider_15() const { return ___itemcollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_itemcollider_15() { return &___itemcollider_15; }
	inline void set_itemcollider_15(BoxCollider_t22920061 * value)
	{
		___itemcollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___itemcollider_15, value);
	}

	inline static int32_t get_offset_of_itemDivider_16() { return static_cast<int32_t>(offsetof(CountryCodeItemUIScaler_t2043820862, ___itemDivider_16)); }
	inline UISprite_t603616735 * get_itemDivider_16() const { return ___itemDivider_16; }
	inline UISprite_t603616735 ** get_address_of_itemDivider_16() { return &___itemDivider_16; }
	inline void set_itemDivider_16(UISprite_t603616735 * value)
	{
		___itemDivider_16 = value;
		Il2CppCodeGenWriteBarrier(&___itemDivider_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
