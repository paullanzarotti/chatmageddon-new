﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptedReference
struct EncryptedReference_t2364822029;
// System.String
struct String_t;
// System.Security.Cryptography.Xml.TransformChain
struct TransformChain_t251592321;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Security_System_Security_Cryptography_Xml_Tr251592321.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.EncryptedReference::.ctor()
extern "C"  void EncryptedReference__ctor_m3932354359 (EncryptedReference_t2364822029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedReference::get_ReferenceType()
extern "C"  String_t* EncryptedReference_get_ReferenceType_m4249932974 (EncryptedReference_t2364822029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedReference::set_ReferenceType(System.String)
extern "C"  void EncryptedReference_set_ReferenceType_m819825731 (EncryptedReference_t2364822029 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Xml.TransformChain System.Security.Cryptography.Xml.EncryptedReference::get_TransformChain()
extern "C"  TransformChain_t251592321 * EncryptedReference_get_TransformChain_m2375387337 (EncryptedReference_t2364822029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedReference::set_TransformChain(System.Security.Cryptography.Xml.TransformChain)
extern "C"  void EncryptedReference_set_TransformChain_m1430501462 (EncryptedReference_t2364822029 * __this, TransformChain_t251592321 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptedReference::get_Uri()
extern "C"  String_t* EncryptedReference_get_Uri_m374898485 (EncryptedReference_t2364822029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedReference::set_Uri(System.String)
extern "C"  void EncryptedReference_set_Uri_m1313297682 (EncryptedReference_t2364822029 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptedReference::GetXml()
extern "C"  XmlElement_t2877111883 * EncryptedReference_GetXml_m3287100648 (EncryptedReference_t2364822029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptedReference::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * EncryptedReference_GetXml_m2992645532 (EncryptedReference_t2364822029 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedReference::LoadXml(System.Xml.XmlElement)
extern "C"  void EncryptedReference_LoadXml_m2653210591 (EncryptedReference_t2364822029 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
