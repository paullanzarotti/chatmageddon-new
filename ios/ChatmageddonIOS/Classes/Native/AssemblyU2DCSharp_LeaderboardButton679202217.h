﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardButton
struct  LeaderboardButton_t679202217  : public SFXButton_t792651341
{
public:
	// UISprite LeaderboardButton::leaderboardIcon
	UISprite_t603616735 * ___leaderboardIcon_5;

public:
	inline static int32_t get_offset_of_leaderboardIcon_5() { return static_cast<int32_t>(offsetof(LeaderboardButton_t679202217, ___leaderboardIcon_5)); }
	inline UISprite_t603616735 * get_leaderboardIcon_5() const { return ___leaderboardIcon_5; }
	inline UISprite_t603616735 ** get_address_of_leaderboardIcon_5() { return &___leaderboardIcon_5; }
	inline void set_leaderboardIcon_5(UISprite_t603616735 * value)
	{
		___leaderboardIcon_5 = value;
		Il2CppCodeGenWriteBarrier(&___leaderboardIcon_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
