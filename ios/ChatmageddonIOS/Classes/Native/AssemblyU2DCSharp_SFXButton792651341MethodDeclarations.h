﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SFXButton
struct SFXButton_t792651341;

#include "codegen/il2cpp-codegen.h"

// System.Void SFXButton::.ctor()
extern "C"  void SFXButton__ctor_m538305182 (SFXButton_t792651341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXButton::OnClick()
extern "C"  void SFXButton_OnClick_m3633773659 (SFXButton_t792651341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXButton::OnButtonClick()
extern "C"  void SFXButton_OnButtonClick_m3012217745 (SFXButton_t792651341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXButton::PlayButtonSFX()
extern "C"  void SFXButton_PlayButtonSFX_m505889255 (SFXButton_t792651341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXButton::SetCollisionActive(System.Boolean)
extern "C"  void SFXButton_SetCollisionActive_m1987525299 (SFXButton_t792651341 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFXButton::SetButtonActive(System.Boolean)
extern "C"  void SFXButton_SetButtonActive_m1706716019 (SFXButton_t792651341 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
