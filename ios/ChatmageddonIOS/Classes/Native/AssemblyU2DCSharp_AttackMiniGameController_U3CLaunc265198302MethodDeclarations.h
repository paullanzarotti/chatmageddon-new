﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackMiniGameController/<LaunchMissile>c__AnonStorey2
struct U3CLaunchMissileU3Ec__AnonStorey2_t265198302;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AttackMiniGameController/<LaunchMissile>c__AnonStorey2::.ctor()
extern "C"  void U3CLaunchMissileU3Ec__AnonStorey2__ctor_m1120394603 (U3CLaunchMissileU3Ec__AnonStorey2_t265198302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController/<LaunchMissile>c__AnonStorey2::<>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void U3CLaunchMissileU3Ec__AnonStorey2_U3CU3Em__0_m1332181381 (U3CLaunchMissileU3Ec__AnonStorey2_t265198302 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameController/<LaunchMissile>c__AnonStorey2::<>m__1(System.Boolean)
extern "C"  void U3CLaunchMissileU3Ec__AnonStorey2_U3CU3Em__1_m470177126 (Il2CppObject * __this /* static, unused */, bool ___standardSuccess0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
