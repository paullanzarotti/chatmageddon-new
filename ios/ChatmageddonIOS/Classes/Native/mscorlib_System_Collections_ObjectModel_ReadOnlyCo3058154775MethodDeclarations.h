﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>
struct ReadOnlyCollection_1_t3058154775;
// System.Collections.Generic.IList`1<StatusNavScreen>
struct IList_1_t3413309684;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// StatusNavScreen[]
struct StatusNavScreenU5BU5D_t3698178810;
// System.Collections.Generic.IEnumerator`1<StatusNavScreen>
struct IEnumerator_1_t347892910;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_StatusNavScreen2872369083.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1889114669_gshared (ReadOnlyCollection_1_t3058154775 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1889114669(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1889114669_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m974135671_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m974135671(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m974135671_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m820805075_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m820805075(__this, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m820805075_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3953004600_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3953004600(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3953004600_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3424444782_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3424444782(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3424444782_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2941166716_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2941166716(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2941166716_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m925697162_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m925697162(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m925697162_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3152945819_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3152945819(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3152945819_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2519256871_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2519256871(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2519256871_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3303120834_gshared (ReadOnlyCollection_1_t3058154775 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3303120834(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3303120834_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3295417219_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3295417219(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3295417219_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3981167846_gshared (ReadOnlyCollection_1_t3058154775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3981167846(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3058154775 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3981167846_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1107815724_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1107815724(__this, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1107815724_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2768433552_gshared (ReadOnlyCollection_1_t3058154775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2768433552(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3058154775 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2768433552_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m973927020_gshared (ReadOnlyCollection_1_t3058154775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m973927020(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3058154775 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m973927020_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1764330515_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1764330515(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1764330515_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2837520051_gshared (ReadOnlyCollection_1_t3058154775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2837520051(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2837520051_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3206687573_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3206687573(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3206687573_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3061869414_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3061869414(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3061869414_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1084607778_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1084607778(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1084607778_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1070139451_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1070139451(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1070139451_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m629494482_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m629494482(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m629494482_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m383735579_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m383735579(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m383735579_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2180400968_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2180400968(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2180400968_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m4234237649_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m4234237649(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m4234237649_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m729707811_gshared (ReadOnlyCollection_1_t3058154775 * __this, StatusNavScreenU5BU5D_t3698178810* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m729707811(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3058154775 *, StatusNavScreenU5BU5D_t3698178810*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m729707811_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3141575746_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3141575746(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3141575746_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2666603399_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2666603399(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2666603399_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3342702662_gshared (ReadOnlyCollection_1_t3058154775 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3342702662(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3058154775 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3342702662_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<StatusNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m4223039702_gshared (ReadOnlyCollection_1_t3058154775 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m4223039702(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3058154775 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m4223039702_gshared)(__this, ___index0, method)
