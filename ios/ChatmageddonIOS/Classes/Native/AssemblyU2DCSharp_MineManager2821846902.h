﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchedMine
struct LaunchedMine_t3677282773;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MineMiniGameController
struct MineMiniGameController_t2791970086;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2572512622.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineManager
struct  MineManager_t2821846902  : public MonoSingleton_1_t2572512622
{
public:
	// LaunchedMine MineManager::activeMine
	LaunchedMine_t3677282773 * ___activeMine_3;
	// UnityEngine.GameObject MineManager::miniGameObject
	GameObject_t1756533147 * ___miniGameObject_4;
	// MineMiniGameController MineManager::gameController
	MineMiniGameController_t2791970086 * ___gameController_5;
	// System.String MineManager::uiResourcePath
	String_t* ___uiResourcePath_6;

public:
	inline static int32_t get_offset_of_activeMine_3() { return static_cast<int32_t>(offsetof(MineManager_t2821846902, ___activeMine_3)); }
	inline LaunchedMine_t3677282773 * get_activeMine_3() const { return ___activeMine_3; }
	inline LaunchedMine_t3677282773 ** get_address_of_activeMine_3() { return &___activeMine_3; }
	inline void set_activeMine_3(LaunchedMine_t3677282773 * value)
	{
		___activeMine_3 = value;
		Il2CppCodeGenWriteBarrier(&___activeMine_3, value);
	}

	inline static int32_t get_offset_of_miniGameObject_4() { return static_cast<int32_t>(offsetof(MineManager_t2821846902, ___miniGameObject_4)); }
	inline GameObject_t1756533147 * get_miniGameObject_4() const { return ___miniGameObject_4; }
	inline GameObject_t1756533147 ** get_address_of_miniGameObject_4() { return &___miniGameObject_4; }
	inline void set_miniGameObject_4(GameObject_t1756533147 * value)
	{
		___miniGameObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___miniGameObject_4, value);
	}

	inline static int32_t get_offset_of_gameController_5() { return static_cast<int32_t>(offsetof(MineManager_t2821846902, ___gameController_5)); }
	inline MineMiniGameController_t2791970086 * get_gameController_5() const { return ___gameController_5; }
	inline MineMiniGameController_t2791970086 ** get_address_of_gameController_5() { return &___gameController_5; }
	inline void set_gameController_5(MineMiniGameController_t2791970086 * value)
	{
		___gameController_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameController_5, value);
	}

	inline static int32_t get_offset_of_uiResourcePath_6() { return static_cast<int32_t>(offsetof(MineManager_t2821846902, ___uiResourcePath_6)); }
	inline String_t* get_uiResourcePath_6() const { return ___uiResourcePath_6; }
	inline String_t** get_address_of_uiResourcePath_6() { return &___uiResourcePath_6; }
	inline void set_uiResourcePath_6(String_t* value)
	{
		___uiResourcePath_6 = value;
		Il2CppCodeGenWriteBarrier(&___uiResourcePath_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
