﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// libphonenumber_csharp_portable.XDocumentLegacy/<GetElementsByTagName>c__AnonStorey0
struct  U3CGetElementsByTagNameU3Ec__AnonStorey0_t3380385869  : public Il2CppObject
{
public:
	// System.String libphonenumber_csharp_portable.XDocumentLegacy/<GetElementsByTagName>c__AnonStorey0::tagName
	String_t* ___tagName_0;

public:
	inline static int32_t get_offset_of_tagName_0() { return static_cast<int32_t>(offsetof(U3CGetElementsByTagNameU3Ec__AnonStorey0_t3380385869, ___tagName_0)); }
	inline String_t* get_tagName_0() const { return ___tagName_0; }
	inline String_t** get_address_of_tagName_0() { return &___tagName_0; }
	inline void set_tagName_0(String_t* value)
	{
		___tagName_0 = value;
		Il2CppCodeGenWriteBarrier(&___tagName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
