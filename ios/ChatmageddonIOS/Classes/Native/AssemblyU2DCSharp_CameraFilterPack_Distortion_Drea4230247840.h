﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Distortion_Dream2
struct  CameraFilterPack_Distortion_Dream2_t4230247840  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Distortion_Dream2::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Dream2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Dream2::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Dream2::SCMaterial
	Material_t193706927 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Dream2::Distortion
	float ___Distortion_6;
	// System.Single CameraFilterPack_Distortion_Dream2::Speed
	float ___Speed_7;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840, ___ScreenResolution_4)); }
	inline Vector4_t2243707581  get_ScreenResolution_4() const { return ___ScreenResolution_4; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_4() { return &___ScreenResolution_4; }
	inline void set_ScreenResolution_4(Vector4_t2243707581  value)
	{
		___ScreenResolution_4 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840, ___SCMaterial_5)); }
	inline Material_t193706927 * get_SCMaterial_5() const { return ___SCMaterial_5; }
	inline Material_t193706927 ** get_address_of_SCMaterial_5() { return &___SCMaterial_5; }
	inline void set_SCMaterial_5(Material_t193706927 * value)
	{
		___SCMaterial_5 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_5, value);
	}

	inline static int32_t get_offset_of_Distortion_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840, ___Distortion_6)); }
	inline float get_Distortion_6() const { return ___Distortion_6; }
	inline float* get_address_of_Distortion_6() { return &___Distortion_6; }
	inline void set_Distortion_6(float value)
	{
		___Distortion_6 = value;
	}

	inline static int32_t get_offset_of_Speed_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840, ___Speed_7)); }
	inline float get_Speed_7() const { return ___Speed_7; }
	inline float* get_address_of_Speed_7() { return &___Speed_7; }
	inline void set_Speed_7(float value)
	{
		___Speed_7 = value;
	}
};

struct CameraFilterPack_Distortion_Dream2_t4230247840_StaticFields
{
public:
	// System.Single CameraFilterPack_Distortion_Dream2::ChangeDistortion
	float ___ChangeDistortion_8;
	// System.Single CameraFilterPack_Distortion_Dream2::ChangeSpeed
	float ___ChangeSpeed_9;

public:
	inline static int32_t get_offset_of_ChangeDistortion_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840_StaticFields, ___ChangeDistortion_8)); }
	inline float get_ChangeDistortion_8() const { return ___ChangeDistortion_8; }
	inline float* get_address_of_ChangeDistortion_8() { return &___ChangeDistortion_8; }
	inline void set_ChangeDistortion_8(float value)
	{
		___ChangeDistortion_8 = value;
	}

	inline static int32_t get_offset_of_ChangeSpeed_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Distortion_Dream2_t4230247840_StaticFields, ___ChangeSpeed_9)); }
	inline float get_ChangeSpeed_9() const { return ___ChangeSpeed_9; }
	inline float* get_address_of_ChangeSpeed_9() { return &___ChangeSpeed_9; }
	inline void set_ChangeSpeed_9(float value)
	{
		___ChangeSpeed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
