﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen3248597474MethodDeclarations.h"

// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::.ctor()
#define BaseInfiniteListPopulator_1__ctor_m1323597682(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1__ctor_m2641845277_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::Awake()
#define BaseInfiniteListPopulator_1_Awake_m4020651763(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_Awake_m758246746_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::ResetPanel()
#define BaseInfiniteListPopulator_1_ResetPanel_m4210214821(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_ResetPanel_m2816930074_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m3997498017(__this, ___item0, ___dataIndex1, ___carouselIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, Transform_t3275118058 *, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_PopulateListItemWithIndex_m1398675422_gshared)(__this, ___item0, ___dataIndex1, ___carouselIndex2, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetStartIndex(System.Int32)
#define BaseInfiniteListPopulator_1_SetStartIndex_m3676971531(__this, ___inStartIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_SetStartIndex_m814882572_gshared)(__this, ___inStartIndex0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetOriginalData(System.Collections.ArrayList)
#define BaseInfiniteListPopulator_1_SetOriginalData_m3784333406(__this, ___inDataList0, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, ArrayList_t4252133567 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetOriginalData_m3487250981_gshared)(__this, ___inDataList0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetSectionIndices(System.Collections.Generic.List`1<System.Int32>)
#define BaseInfiniteListPopulator_1_SetSectionIndices_m3887250597(__this, ___inSectionsIndices0, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, List_1_t1440998580 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetSectionIndices_m4142448658_gshared)(__this, ___inSectionsIndices0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::RefreshTableView()
#define BaseInfiniteListPopulator_1_RefreshTableView_m2755251020(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshTableView_m3255066675_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::InitTableView(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableView_m209059624(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableView_m3216652517_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::InitTableViewImp(System.Collections.ArrayList,System.Collections.Generic.List`1<System.Int32>,System.Int32)
#define BaseInfiniteListPopulator_1_InitTableViewImp_m854884246(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, ArrayList_t4252133567 *, List_1_t1440998580 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_InitTableViewImp_m2195640693_gshared)(__this, ___inDataList0, ___inSectionsIndices1, ___inStartIndex2, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::RepositionList()
#define BaseInfiniteListPopulator_1_RepositionList_m3664481698(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_RepositionList_m111137877_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::UponPostRepositionReq()
#define BaseInfiniteListPopulator_1_UponPostRepositionReq_m3753824338(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_UponPostRepositionReq_m2774805037_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
#define BaseInfiniteListPopulator_1_InitListItemWithIndex_m3089967420(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, Transform_t3275118058 *, int32_t, int32_t, Nullable_1_t334943763 , const MethodInfo*))BaseInfiniteListPopulator_1_InitListItemWithIndex_m1903848259_gshared)(__this, ___item0, ___dataIndex1, ___poolIndex2, ___carouselIndex3, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::PrepareListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m3132061767(__this, ___item0, ___newIndex1, ___oldIndex2, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, Transform_t3275118058 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareListItemWithIndex_m996552464_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::PrepareCarouselListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Int32)
#define BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m3117504000(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, Transform_t3275118058 *, int32_t, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_PrepareCarouselListItemWithIndex_m657617729_gshared)(__this, ___item0, ___newIndex1, ___oldIndex2, ___actualIndex3, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::ItemIsInvisible(System.Int32)
#define BaseInfiniteListPopulator_1_ItemIsInvisible_m3984508291(__this, ___itemNumber0, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_ItemIsInvisible_m4214197614_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetItemInvisible(Item,System.Boolean)
#define BaseInfiniteListPopulator_1_SetItemInvisible_m2000003102(__this, ___item0, ___forward1, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, MultiPhoneNumberListItem_t177127779 *, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetItemInvisible_m3445369353_gshared)(__this, ___item0, ___forward1, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::UpdateList(System.Int32)
#define BaseInfiniteListPopulator_1_UpdateList_m2769019484(__this, ___itemNumber0, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateList_m2236403487_gshared)(__this, ___itemNumber0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::UpdateListDirection(BaseInfiniteListPopulator`1/ListMovementDirection<Item>,System.Int32)
#define BaseInfiniteListPopulator_1_UpdateListDirection_m1977440643(__this, ___direction0, ___itemNumber1, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_UpdateListDirection_m2578789970_gshared)(__this, ___direction0, ___itemNumber1, method)
// System.Int32 BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::GetJumpIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetJumpIndexForItem_m4208762973(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetJumpIndexForItem_m1184416498_gshared)(__this, ___itemDataIndex0, method)
// System.Int32 BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::GetRealIndexForItem(System.Int32)
#define BaseInfiniteListPopulator_1_GetRealIndexForItem_m3998183717(__this, ___itemDataIndex0, method) ((  int32_t (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetRealIndexForItem_m1748713794_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::itemIsPressed(System.Int32,System.Boolean)
#define BaseInfiniteListPopulator_1_itemIsPressed_m2140051233(__this, ___itemDataIndex0, ___isDown1, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, bool, const MethodInfo*))BaseInfiniteListPopulator_1_itemIsPressed_m852036530_gshared)(__this, ___itemDataIndex0, ___isDown1, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::itemClicked(System.Int32)
#define BaseInfiniteListPopulator_1_itemClicked_m2668398549(__this, ___itemDataIndex0, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_itemClicked_m3174637120_gshared)(__this, ___itemDataIndex0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::StopScrollViewMomentum()
#define BaseInfiniteListPopulator_1_StopScrollViewMomentum_m2336265318(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_StopScrollViewMomentum_m1874399289_gshared)(__this, method)
// UnityEngine.Transform BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::GetItemFromPool(System.Int32)
#define BaseInfiniteListPopulator_1_GetItemFromPool_m83387552(__this, ___i0, method) ((  Transform_t3275118058 * (*) (BaseInfiniteListPopulator_1_t736275958 *, int32_t, const MethodInfo*))BaseInfiniteListPopulator_1_GetItemFromPool_m3910532773_gshared)(__this, ___i0, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::RefreshPool()
#define BaseInfiniteListPopulator_1_RefreshPool_m256000895(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_RefreshPool_m4033485912_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetScrollToBottom()
#define BaseInfiniteListPopulator_1_SetScrollToBottom_m2681622009(__this, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollToBottom_m1776815970_gshared)(__this, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetScrollPosition(System.Single,System.Boolean)
#define BaseInfiniteListPopulator_1_SetScrollPosition_m354631320(__this, ___exactPosition0, ___up1, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, float, bool, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPosition_m3274440381_gshared)(__this, ___exactPosition0, ___up1, method)
// System.Void BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetScrollPositionOverTime(System.Single)
#define BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3751683424(__this, ___exactPosition0, method) ((  void (*) (BaseInfiniteListPopulator_1_t736275958 *, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetScrollPositionOverTime_m3264131221_gshared)(__this, ___exactPosition0, method)
// System.Collections.IEnumerator BaseInfiniteListPopulator`1<MultiPhoneNumberListItem>::SetPanelPosOverTime(System.Single,System.Single)
#define BaseInfiniteListPopulator_1_SetPanelPosOverTime_m1357129543(__this, ___seconds0, ___position1, method) ((  Il2CppObject * (*) (BaseInfiniteListPopulator_1_t736275958 *, float, float, const MethodInfo*))BaseInfiniteListPopulator_1_SetPanelPosOverTime_m476648792_gshared)(__this, ___seconds0, ___position1, method)
