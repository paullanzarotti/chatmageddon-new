﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookPlayer
struct FacebookPlayer_t2645999341;
// System.String
struct String_t;
// System.Collections.Generic.List`1<FacebookFriend>
struct List_1_t127391664;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FacebookPlayer::.ctor(System.String,System.String)
extern "C"  void FacebookPlayer__ctor_m3584626410 (FacebookPlayer_t2645999341 * __this, String_t* ___FirstName0, String_t* ___ID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookPlayer::SetFriendsList(System.Collections.Generic.List`1<FacebookFriend>)
extern "C"  void FacebookPlayer_SetFriendsList_m1238908729 (FacebookPlayer_t2645999341 * __this, List_1_t127391664 * ___newFriendsList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FacebookFriend> FacebookPlayer::GetFriendsList()
extern "C"  List_1_t127391664 * FacebookPlayer_GetFriendsList_m3369879414 (FacebookPlayer_t2645999341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
