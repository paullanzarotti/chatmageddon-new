﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VerificationScreen
struct VerificationScreen_t1800753931;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void VerificationScreen::.ctor()
extern "C"  void VerificationScreen__ctor_m4173238258 (VerificationScreen_t1800753931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerificationScreen::Start()
extern "C"  void VerificationScreen_Start_m3424584630 (VerificationScreen_t1800753931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerificationScreen::VerificationUIClosing()
extern "C"  void VerificationScreen_VerificationUIClosing_m2202126214 (VerificationScreen_t1800753931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerificationScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void VerificationScreen_ScreenLoad_m3599340954 (VerificationScreen_t1800753931 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VerificationScreen::ScreenUnload(NavigationDirection)
extern "C"  void VerificationScreen_ScreenUnload_m1706785742 (VerificationScreen_t1800753931 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VerificationScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool VerificationScreen_ValidateScreenNavigation_m2515162657 (VerificationScreen_t1800753931 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
