﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0
struct U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::.ctor()
extern "C"  void U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0__ctor_m3876208511 (U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::MoveNext()
extern "C"  bool U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_MoveNext_m3032575773 (U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3307444873 (U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1891832913 (U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::Dispose()
extern "C"  void U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_Dispose_m3658393832 (U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsControlBase/<DoReduceVelocityMoveOverTime>c__Iterator0::Reset()
extern "C"  void U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_Reset_m378826142 (U3CDoReduceVelocityMoveOverTimeU3Ec__Iterator0_t676239010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
