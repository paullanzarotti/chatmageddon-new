﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_IPPickerSpriteBase2462226553.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPSpritePicker
struct  IPSpritePicker_t2623787062  : public IPPickerSpriteBase_t2462226553
{
public:
	// System.Collections.Generic.List`1<System.String> IPSpritePicker::spriteNames
	List_1_t1398341365 * ___spriteNames_15;
	// System.Boolean IPSpritePicker::normalizeSprites
	bool ___normalizeSprites_16;
	// System.Single IPSpritePicker::normalizedMax
	float ___normalizedMax_17;
	// System.Int32 IPSpritePicker::initIndex
	int32_t ___initIndex_18;
	// System.String IPSpritePicker::<CurrentSpriteName>k__BackingField
	String_t* ___U3CCurrentSpriteNameU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of_spriteNames_15() { return static_cast<int32_t>(offsetof(IPSpritePicker_t2623787062, ___spriteNames_15)); }
	inline List_1_t1398341365 * get_spriteNames_15() const { return ___spriteNames_15; }
	inline List_1_t1398341365 ** get_address_of_spriteNames_15() { return &___spriteNames_15; }
	inline void set_spriteNames_15(List_1_t1398341365 * value)
	{
		___spriteNames_15 = value;
		Il2CppCodeGenWriteBarrier(&___spriteNames_15, value);
	}

	inline static int32_t get_offset_of_normalizeSprites_16() { return static_cast<int32_t>(offsetof(IPSpritePicker_t2623787062, ___normalizeSprites_16)); }
	inline bool get_normalizeSprites_16() const { return ___normalizeSprites_16; }
	inline bool* get_address_of_normalizeSprites_16() { return &___normalizeSprites_16; }
	inline void set_normalizeSprites_16(bool value)
	{
		___normalizeSprites_16 = value;
	}

	inline static int32_t get_offset_of_normalizedMax_17() { return static_cast<int32_t>(offsetof(IPSpritePicker_t2623787062, ___normalizedMax_17)); }
	inline float get_normalizedMax_17() const { return ___normalizedMax_17; }
	inline float* get_address_of_normalizedMax_17() { return &___normalizedMax_17; }
	inline void set_normalizedMax_17(float value)
	{
		___normalizedMax_17 = value;
	}

	inline static int32_t get_offset_of_initIndex_18() { return static_cast<int32_t>(offsetof(IPSpritePicker_t2623787062, ___initIndex_18)); }
	inline int32_t get_initIndex_18() const { return ___initIndex_18; }
	inline int32_t* get_address_of_initIndex_18() { return &___initIndex_18; }
	inline void set_initIndex_18(int32_t value)
	{
		___initIndex_18 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentSpriteNameU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(IPSpritePicker_t2623787062, ___U3CCurrentSpriteNameU3Ek__BackingField_19)); }
	inline String_t* get_U3CCurrentSpriteNameU3Ek__BackingField_19() const { return ___U3CCurrentSpriteNameU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CCurrentSpriteNameU3Ek__BackingField_19() { return &___U3CCurrentSpriteNameU3Ek__BackingField_19; }
	inline void set_U3CCurrentSpriteNameU3Ek__BackingField_19(String_t* value)
	{
		___U3CCurrentSpriteNameU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrentSpriteNameU3Ek__BackingField_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
