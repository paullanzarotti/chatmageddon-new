﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<UILabel>
struct List_1_t1164236560;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchSequenceReport
struct  LaunchSequenceReport_t983071960  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> LaunchSequenceReport::sequenceInfoParents
	List_1_t2644239190 * ___sequenceInfoParents_2;
	// System.Collections.Generic.List`1<UILabel> LaunchSequenceReport::sequenceInfoLabels
	List_1_t1164236560 * ___sequenceInfoLabels_3;
	// System.Single LaunchSequenceReport::height
	float ___height_4;
	// System.Single LaunchSequenceReport::startPosition
	float ___startPosition_5;

public:
	inline static int32_t get_offset_of_sequenceInfoParents_2() { return static_cast<int32_t>(offsetof(LaunchSequenceReport_t983071960, ___sequenceInfoParents_2)); }
	inline List_1_t2644239190 * get_sequenceInfoParents_2() const { return ___sequenceInfoParents_2; }
	inline List_1_t2644239190 ** get_address_of_sequenceInfoParents_2() { return &___sequenceInfoParents_2; }
	inline void set_sequenceInfoParents_2(List_1_t2644239190 * value)
	{
		___sequenceInfoParents_2 = value;
		Il2CppCodeGenWriteBarrier(&___sequenceInfoParents_2, value);
	}

	inline static int32_t get_offset_of_sequenceInfoLabels_3() { return static_cast<int32_t>(offsetof(LaunchSequenceReport_t983071960, ___sequenceInfoLabels_3)); }
	inline List_1_t1164236560 * get_sequenceInfoLabels_3() const { return ___sequenceInfoLabels_3; }
	inline List_1_t1164236560 ** get_address_of_sequenceInfoLabels_3() { return &___sequenceInfoLabels_3; }
	inline void set_sequenceInfoLabels_3(List_1_t1164236560 * value)
	{
		___sequenceInfoLabels_3 = value;
		Il2CppCodeGenWriteBarrier(&___sequenceInfoLabels_3, value);
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(LaunchSequenceReport_t983071960, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_startPosition_5() { return static_cast<int32_t>(offsetof(LaunchSequenceReport_t983071960, ___startPosition_5)); }
	inline float get_startPosition_5() const { return ___startPosition_5; }
	inline float* get_address_of_startPosition_5() { return &___startPosition_5; }
	inline void set_startPosition_5(float value)
	{
		___startPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
