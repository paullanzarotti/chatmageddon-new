﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AttemptTab>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2757068645(__this, ___l0, method) ((  void (*) (Enumerator_t2868559170 *, List_1_t3333829496 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttemptTab>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3826559125(__this, method) ((  void (*) (Enumerator_t2868559170 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AttemptTab>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2797361953(__this, method) ((  Il2CppObject * (*) (Enumerator_t2868559170 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttemptTab>::Dispose()
#define Enumerator_Dispose_m3684503148(__this, method) ((  void (*) (Enumerator_t2868559170 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AttemptTab>::VerifyState()
#define Enumerator_VerifyState_m134866619(__this, method) ((  void (*) (Enumerator_t2868559170 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AttemptTab>::MoveNext()
#define Enumerator_MoveNext_m86290013(__this, method) ((  bool (*) (Enumerator_t2868559170 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AttemptTab>::get_Current()
#define Enumerator_get_Current_m2606017532(__this, method) ((  AttemptTab_t3964708364 * (*) (Enumerator_t2868559170 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
