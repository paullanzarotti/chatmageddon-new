﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetFullInventory>c__AnonStorey35
struct U3CGetFullInventoryU3Ec__AnonStorey35_t3467193277;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetFullInventory>c__AnonStorey35::.ctor()
extern "C"  void U3CGetFullInventoryU3Ec__AnonStorey35__ctor_m528526188 (U3CGetFullInventoryU3Ec__AnonStorey35_t3467193277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetFullInventory>c__AnonStorey35::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetFullInventoryU3Ec__AnonStorey35_U3CU3Em__0_m932242961 (U3CGetFullInventoryU3Ec__AnonStorey35_t3467193277 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
