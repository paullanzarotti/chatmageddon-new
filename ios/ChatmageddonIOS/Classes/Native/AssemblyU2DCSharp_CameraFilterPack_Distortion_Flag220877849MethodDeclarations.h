﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Distortion_Flag
struct CameraFilterPack_Distortion_Flag_t220877849;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Distortion_Flag::.ctor()
extern "C"  void CameraFilterPack_Distortion_Flag__ctor_m1436722524 (CameraFilterPack_Distortion_Flag_t220877849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Flag::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Distortion_Flag_get_material_m1981774857 (CameraFilterPack_Distortion_Flag_t220877849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::Start()
extern "C"  void CameraFilterPack_Distortion_Flag_Start_m4067903428 (CameraFilterPack_Distortion_Flag_t220877849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Distortion_Flag_OnRenderImage_m1533574676 (CameraFilterPack_Distortion_Flag_t220877849 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::OnValidate()
extern "C"  void CameraFilterPack_Distortion_Flag_OnValidate_m2520311039 (CameraFilterPack_Distortion_Flag_t220877849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::Update()
extern "C"  void CameraFilterPack_Distortion_Flag_Update_m4068447269 (CameraFilterPack_Distortion_Flag_t220877849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::OnDisable()
extern "C"  void CameraFilterPack_Distortion_Flag_OnDisable_m2960259193 (CameraFilterPack_Distortion_Flag_t220877849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
