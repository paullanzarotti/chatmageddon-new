﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginBottomUIScaler
struct LoginBottomUIScaler_t3264146212;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginBottomUIScaler::.ctor()
extern "C"  void LoginBottomUIScaler__ctor_m3905081777 (LoginBottomUIScaler_t3264146212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginBottomUIScaler::GlobalUIScale()
extern "C"  void LoginBottomUIScaler_GlobalUIScale_m481841102 (LoginBottomUIScaler_t3264146212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
