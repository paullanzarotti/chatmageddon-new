﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIAnimation
struct NGUIAnimation_t2962118329;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// NGUIAnimation/OnAnimationStart
struct OnAnimationStart_t3313445917;
// NGUIAnimation/OnAnimationDrawn
struct OnAnimationDrawn_t3207005891;
// NGUIAnimation/OnAnimationUpdated
struct OnAnimationUpdated_t4231811366;
// NGUIAnimation/OnAnimationLooped
struct OnAnimationLooped_t2655604368;
// NGUIAnimation/OnAnimationFinished
struct OnAnimationFinished_t3859016523;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationStart3313445917.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationDrawn3207005891.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationUpdated4231811366.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationLooped2655604368.h"
#include "AssemblyU2DCSharp_NGUIAnimation_OnAnimationFinishe3859016523.h"

// System.Void NGUIAnimation::.ctor()
extern "C"  void NGUIAnimation__ctor_m1912070896 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::PlayAnimation(System.Boolean)
extern "C"  void NGUIAnimation_PlayAnimation_m2284872221 (NGUIAnimation_t2962118329 * __this, bool ___loop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::PauseAnimation(System.Boolean)
extern "C"  void NGUIAnimation_PauseAnimation_m2722619599 (NGUIAnimation_t2962118329 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::StopAnimation()
extern "C"  void NGUIAnimation_StopAnimation_m4067271006 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator NGUIAnimation::PlayAnim(System.Boolean)
extern "C"  Il2CppObject * NGUIAnimation_PlayAnim_m1821702432 (NGUIAnimation_t2962118329 * __this, bool ___loop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::ResetCurrents()
extern "C"  void NGUIAnimation_ResetCurrents_m441985683 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::OnResetCurrents()
extern "C"  void NGUIAnimation_OnResetCurrents_m1443560166 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::add_onAnimationStart(NGUIAnimation/OnAnimationStart)
extern "C"  void NGUIAnimation_add_onAnimationStart_m1649567706 (NGUIAnimation_t2962118329 * __this, OnAnimationStart_t3313445917 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::remove_onAnimationStart(NGUIAnimation/OnAnimationStart)
extern "C"  void NGUIAnimation_remove_onAnimationStart_m1516928961 (NGUIAnimation_t2962118329 * __this, OnAnimationStart_t3313445917 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::AnimStart()
extern "C"  void NGUIAnimation_AnimStart_m86907467 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::OnAnimStart()
extern "C"  void NGUIAnimation_OnAnimStart_m403698544 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::add_onAnimationDrawn(NGUIAnimation/OnAnimationDrawn)
extern "C"  void NGUIAnimation_add_onAnimationDrawn_m3521571706 (NGUIAnimation_t2962118329 * __this, OnAnimationDrawn_t3207005891 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::remove_onAnimationDrawn(NGUIAnimation/OnAnimationDrawn)
extern "C"  void NGUIAnimation_remove_onAnimationDrawn_m3228416853 (NGUIAnimation_t2962118329 * __this, OnAnimationDrawn_t3207005891 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::DrawFrame()
extern "C"  void NGUIAnimation_DrawFrame_m254274817 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::OnAnimFrameDraw()
extern "C"  void NGUIAnimation_OnAnimFrameDraw_m1718541551 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::add_onAnimationUpdated(NGUIAnimation/OnAnimationUpdated)
extern "C"  void NGUIAnimation_add_onAnimationUpdated_m340267176 (NGUIAnimation_t2962118329 * __this, OnAnimationUpdated_t4231811366 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::remove_onAnimationUpdated(NGUIAnimation/OnAnimationUpdated)
extern "C"  void NGUIAnimation_remove_onAnimationUpdated_m1358664949 (NGUIAnimation_t2962118329 * __this, OnAnimationUpdated_t4231811366 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::UpdateAnim()
extern "C"  void NGUIAnimation_UpdateAnim_m3272288258 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::OnAnimUpdate()
extern "C"  void NGUIAnimation_OnAnimUpdate_m2454875127 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::add_onAnimationLooped(NGUIAnimation/OnAnimationLooped)
extern "C"  void NGUIAnimation_add_onAnimationLooped_m3133292776 (NGUIAnimation_t2962118329 * __this, OnAnimationLooped_t2655604368 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::remove_onAnimationLooped(NGUIAnimation/OnAnimationLooped)
extern "C"  void NGUIAnimation_remove_onAnimationLooped_m821449625 (NGUIAnimation_t2962118329 * __this, OnAnimationLooped_t2655604368 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::LoopAnim()
extern "C"  void NGUIAnimation_LoopAnim_m1908965527 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::OnAnimLooped()
extern "C"  void NGUIAnimation_OnAnimLooped_m30459663 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::add_onAnimationFinished(NGUIAnimation/OnAnimationFinished)
extern "C"  void NGUIAnimation_add_onAnimationFinished_m447687752 (NGUIAnimation_t2962118329 * __this, OnAnimationFinished_t3859016523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::remove_onAnimationFinished(NGUIAnimation/OnAnimationFinished)
extern "C"  void NGUIAnimation_remove_onAnimationFinished_m2739207993 (NGUIAnimation_t2962118329 * __this, OnAnimationFinished_t3859016523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::FinishAnim()
extern "C"  void NGUIAnimation_FinishAnim_m3809826096 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIAnimation::OnAnimFinished()
extern "C"  void NGUIAnimation_OnAnimFinished_m3079210460 (NGUIAnimation_t2962118329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
