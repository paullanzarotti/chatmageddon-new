﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseModalUIScaler
struct BaseModalUIScaler_t2796740432;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseModalUIScaler::.ctor()
extern "C"  void BaseModalUIScaler__ctor_m2458772303 (BaseModalUIScaler_t2796740432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseModalUIScaler::GlobalUIScale()
extern "C"  void BaseModalUIScaler_GlobalUIScale_m3534163510 (BaseModalUIScaler_t2796740432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
