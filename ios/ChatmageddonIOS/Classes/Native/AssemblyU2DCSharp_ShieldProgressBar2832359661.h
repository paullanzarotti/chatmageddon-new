﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_UISliderProgressBar2494061391.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldProgressBar
struct  ShieldProgressBar_t2832359661  : public UISliderProgressBar_t2494061391
{
public:
	// UnityEngine.Color ShieldProgressBar::bronzeColour
	Color_t2020392075  ___bronzeColour_9;
	// UnityEngine.Color ShieldProgressBar::silverColour
	Color_t2020392075  ___silverColour_10;
	// UnityEngine.Color ShieldProgressBar::goldColour
	Color_t2020392075  ___goldColour_11;
	// UnityEngine.Color ShieldProgressBar::mineColour
	Color_t2020392075  ___mineColour_12;
	// UISprite ShieldProgressBar::foregroundBar
	UISprite_t603616735 * ___foregroundBar_13;
	// UILabel ShieldProgressBar::shieldLabel
	UILabel_t1795115428 * ___shieldLabel_14;
	// UILabel ShieldProgressBar::activeLabel
	UILabel_t1795115428 * ___activeLabel_15;

public:
	inline static int32_t get_offset_of_bronzeColour_9() { return static_cast<int32_t>(offsetof(ShieldProgressBar_t2832359661, ___bronzeColour_9)); }
	inline Color_t2020392075  get_bronzeColour_9() const { return ___bronzeColour_9; }
	inline Color_t2020392075 * get_address_of_bronzeColour_9() { return &___bronzeColour_9; }
	inline void set_bronzeColour_9(Color_t2020392075  value)
	{
		___bronzeColour_9 = value;
	}

	inline static int32_t get_offset_of_silverColour_10() { return static_cast<int32_t>(offsetof(ShieldProgressBar_t2832359661, ___silverColour_10)); }
	inline Color_t2020392075  get_silverColour_10() const { return ___silverColour_10; }
	inline Color_t2020392075 * get_address_of_silverColour_10() { return &___silverColour_10; }
	inline void set_silverColour_10(Color_t2020392075  value)
	{
		___silverColour_10 = value;
	}

	inline static int32_t get_offset_of_goldColour_11() { return static_cast<int32_t>(offsetof(ShieldProgressBar_t2832359661, ___goldColour_11)); }
	inline Color_t2020392075  get_goldColour_11() const { return ___goldColour_11; }
	inline Color_t2020392075 * get_address_of_goldColour_11() { return &___goldColour_11; }
	inline void set_goldColour_11(Color_t2020392075  value)
	{
		___goldColour_11 = value;
	}

	inline static int32_t get_offset_of_mineColour_12() { return static_cast<int32_t>(offsetof(ShieldProgressBar_t2832359661, ___mineColour_12)); }
	inline Color_t2020392075  get_mineColour_12() const { return ___mineColour_12; }
	inline Color_t2020392075 * get_address_of_mineColour_12() { return &___mineColour_12; }
	inline void set_mineColour_12(Color_t2020392075  value)
	{
		___mineColour_12 = value;
	}

	inline static int32_t get_offset_of_foregroundBar_13() { return static_cast<int32_t>(offsetof(ShieldProgressBar_t2832359661, ___foregroundBar_13)); }
	inline UISprite_t603616735 * get_foregroundBar_13() const { return ___foregroundBar_13; }
	inline UISprite_t603616735 ** get_address_of_foregroundBar_13() { return &___foregroundBar_13; }
	inline void set_foregroundBar_13(UISprite_t603616735 * value)
	{
		___foregroundBar_13 = value;
		Il2CppCodeGenWriteBarrier(&___foregroundBar_13, value);
	}

	inline static int32_t get_offset_of_shieldLabel_14() { return static_cast<int32_t>(offsetof(ShieldProgressBar_t2832359661, ___shieldLabel_14)); }
	inline UILabel_t1795115428 * get_shieldLabel_14() const { return ___shieldLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_shieldLabel_14() { return &___shieldLabel_14; }
	inline void set_shieldLabel_14(UILabel_t1795115428 * value)
	{
		___shieldLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___shieldLabel_14, value);
	}

	inline static int32_t get_offset_of_activeLabel_15() { return static_cast<int32_t>(offsetof(ShieldProgressBar_t2832359661, ___activeLabel_15)); }
	inline UILabel_t1795115428 * get_activeLabel_15() const { return ___activeLabel_15; }
	inline UILabel_t1795115428 ** get_address_of_activeLabel_15() { return &___activeLabel_15; }
	inline void set_activeLabel_15(UILabel_t1795115428 * value)
	{
		___activeLabel_15 = value;
		Il2CppCodeGenWriteBarrier(&___activeLabel_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
