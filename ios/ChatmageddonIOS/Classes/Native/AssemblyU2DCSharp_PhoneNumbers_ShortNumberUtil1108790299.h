﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.ShortNumberUtil
struct  ShortNumberUtil_t1108790299  : public Il2CppObject
{
public:

public:
};

struct ShortNumberUtil_t1108790299_StaticFields
{
public:
	// PhoneNumbers.PhoneNumberUtil PhoneNumbers.ShortNumberUtil::phoneUtil
	PhoneNumberUtil_t4155573397 * ___phoneUtil_0;

public:
	inline static int32_t get_offset_of_phoneUtil_0() { return static_cast<int32_t>(offsetof(ShortNumberUtil_t1108790299_StaticFields, ___phoneUtil_0)); }
	inline PhoneNumberUtil_t4155573397 * get_phoneUtil_0() const { return ___phoneUtil_0; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_phoneUtil_0() { return &___phoneUtil_0; }
	inline void set_phoneUtil_0(PhoneNumberUtil_t4155573397 * value)
	{
		___phoneUtil_0 = value;
		Il2CppCodeGenWriteBarrier(&___phoneUtil_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
