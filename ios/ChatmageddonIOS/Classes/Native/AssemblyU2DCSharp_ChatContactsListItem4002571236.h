﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatContactsILP
struct ChatContactsILP_t1780035494;
// ChatContact
struct ChatContact_t3760700014;
// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen483143304.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatContactsListItem
struct  ChatContactsListItem_t4002571236  : public BaseInfiniteListItem_1_t483143304
{
public:
	// ChatContactsILP ChatContactsListItem::listPopulator
	ChatContactsILP_t1780035494 * ___listPopulator_10;
	// ChatContact ChatContactsListItem::contact
	ChatContact_t3760700014 * ___contact_11;
	// UILabel ChatContactsListItem::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_12;
	// UILabel ChatContactsListItem::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_13;
	// UILabel ChatContactsListItem::usernameLabel
	UILabel_t1795115428 * ___usernameLabel_14;
	// UISprite ChatContactsListItem::seperator
	UISprite_t603616735 * ___seperator_15;
	// UnityEngine.Vector3 ChatContactsListItem::seperatorPos
	Vector3_t2243707580  ___seperatorPos_16;
	// UnityEngine.Vector3 ChatContactsListItem::halfSeperatorPos
	Vector3_t2243707580  ___halfSeperatorPos_17;
	// UnityEngine.Color ChatContactsListItem::seperatorColour
	Color_t2020392075  ___seperatorColour_18;
	// UIScrollView ChatContactsListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_19;
	// UIScrollView ChatContactsListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_20;
	// UnityEngine.Transform ChatContactsListItem::mTrans
	Transform_t3275118058 * ___mTrans_21;
	// UIScrollView ChatContactsListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_22;
	// System.Boolean ChatContactsListItem::mAutoFind
	bool ___mAutoFind_23;
	// System.Boolean ChatContactsListItem::mStarted
	bool ___mStarted_24;

public:
	inline static int32_t get_offset_of_listPopulator_10() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___listPopulator_10)); }
	inline ChatContactsILP_t1780035494 * get_listPopulator_10() const { return ___listPopulator_10; }
	inline ChatContactsILP_t1780035494 ** get_address_of_listPopulator_10() { return &___listPopulator_10; }
	inline void set_listPopulator_10(ChatContactsILP_t1780035494 * value)
	{
		___listPopulator_10 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_10, value);
	}

	inline static int32_t get_offset_of_contact_11() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___contact_11)); }
	inline ChatContact_t3760700014 * get_contact_11() const { return ___contact_11; }
	inline ChatContact_t3760700014 ** get_address_of_contact_11() { return &___contact_11; }
	inline void set_contact_11(ChatContact_t3760700014 * value)
	{
		___contact_11 = value;
		Il2CppCodeGenWriteBarrier(&___contact_11, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_12() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___firstNameLabel_12)); }
	inline UILabel_t1795115428 * get_firstNameLabel_12() const { return ___firstNameLabel_12; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_12() { return &___firstNameLabel_12; }
	inline void set_firstNameLabel_12(UILabel_t1795115428 * value)
	{
		___firstNameLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_12, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_13() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___lastNameLabel_13)); }
	inline UILabel_t1795115428 * get_lastNameLabel_13() const { return ___lastNameLabel_13; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_13() { return &___lastNameLabel_13; }
	inline void set_lastNameLabel_13(UILabel_t1795115428 * value)
	{
		___lastNameLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_13, value);
	}

	inline static int32_t get_offset_of_usernameLabel_14() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___usernameLabel_14)); }
	inline UILabel_t1795115428 * get_usernameLabel_14() const { return ___usernameLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_usernameLabel_14() { return &___usernameLabel_14; }
	inline void set_usernameLabel_14(UILabel_t1795115428 * value)
	{
		___usernameLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___usernameLabel_14, value);
	}

	inline static int32_t get_offset_of_seperator_15() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___seperator_15)); }
	inline UISprite_t603616735 * get_seperator_15() const { return ___seperator_15; }
	inline UISprite_t603616735 ** get_address_of_seperator_15() { return &___seperator_15; }
	inline void set_seperator_15(UISprite_t603616735 * value)
	{
		___seperator_15 = value;
		Il2CppCodeGenWriteBarrier(&___seperator_15, value);
	}

	inline static int32_t get_offset_of_seperatorPos_16() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___seperatorPos_16)); }
	inline Vector3_t2243707580  get_seperatorPos_16() const { return ___seperatorPos_16; }
	inline Vector3_t2243707580 * get_address_of_seperatorPos_16() { return &___seperatorPos_16; }
	inline void set_seperatorPos_16(Vector3_t2243707580  value)
	{
		___seperatorPos_16 = value;
	}

	inline static int32_t get_offset_of_halfSeperatorPos_17() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___halfSeperatorPos_17)); }
	inline Vector3_t2243707580  get_halfSeperatorPos_17() const { return ___halfSeperatorPos_17; }
	inline Vector3_t2243707580 * get_address_of_halfSeperatorPos_17() { return &___halfSeperatorPos_17; }
	inline void set_halfSeperatorPos_17(Vector3_t2243707580  value)
	{
		___halfSeperatorPos_17 = value;
	}

	inline static int32_t get_offset_of_seperatorColour_18() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___seperatorColour_18)); }
	inline Color_t2020392075  get_seperatorColour_18() const { return ___seperatorColour_18; }
	inline Color_t2020392075 * get_address_of_seperatorColour_18() { return &___seperatorColour_18; }
	inline void set_seperatorColour_18(Color_t2020392075  value)
	{
		___seperatorColour_18 = value;
	}

	inline static int32_t get_offset_of_scrollView_19() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___scrollView_19)); }
	inline UIScrollView_t3033954930 * get_scrollView_19() const { return ___scrollView_19; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_19() { return &___scrollView_19; }
	inline void set_scrollView_19(UIScrollView_t3033954930 * value)
	{
		___scrollView_19 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_19, value);
	}

	inline static int32_t get_offset_of_draggablePanel_20() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___draggablePanel_20)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_20() const { return ___draggablePanel_20; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_20() { return &___draggablePanel_20; }
	inline void set_draggablePanel_20(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_20 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_20, value);
	}

	inline static int32_t get_offset_of_mTrans_21() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___mTrans_21)); }
	inline Transform_t3275118058 * get_mTrans_21() const { return ___mTrans_21; }
	inline Transform_t3275118058 ** get_address_of_mTrans_21() { return &___mTrans_21; }
	inline void set_mTrans_21(Transform_t3275118058 * value)
	{
		___mTrans_21 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_21, value);
	}

	inline static int32_t get_offset_of_mScroll_22() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___mScroll_22)); }
	inline UIScrollView_t3033954930 * get_mScroll_22() const { return ___mScroll_22; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_22() { return &___mScroll_22; }
	inline void set_mScroll_22(UIScrollView_t3033954930 * value)
	{
		___mScroll_22 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_22, value);
	}

	inline static int32_t get_offset_of_mAutoFind_23() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___mAutoFind_23)); }
	inline bool get_mAutoFind_23() const { return ___mAutoFind_23; }
	inline bool* get_address_of_mAutoFind_23() { return &___mAutoFind_23; }
	inline void set_mAutoFind_23(bool value)
	{
		___mAutoFind_23 = value;
	}

	inline static int32_t get_offset_of_mStarted_24() { return static_cast<int32_t>(offsetof(ChatContactsListItem_t4002571236, ___mStarted_24)); }
	inline bool get_mStarted_24() const { return ___mStarted_24; }
	inline bool* get_address_of_mStarted_24() { return &___mStarted_24; }
	inline void set_mStarted_24(bool value)
	{
		___mStarted_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
