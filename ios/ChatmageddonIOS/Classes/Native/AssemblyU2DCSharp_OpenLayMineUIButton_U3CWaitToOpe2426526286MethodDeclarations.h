﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpenLayMineUIButton/<WaitToOpenModal>c__Iterator0
struct U3CWaitToOpenModalU3Ec__Iterator0_t2426526286;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OpenLayMineUIButton/<WaitToOpenModal>c__Iterator0::.ctor()
extern "C"  void U3CWaitToOpenModalU3Ec__Iterator0__ctor_m547706821 (U3CWaitToOpenModalU3Ec__Iterator0_t2426526286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OpenLayMineUIButton/<WaitToOpenModal>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitToOpenModalU3Ec__Iterator0_MoveNext_m3052324507 (U3CWaitToOpenModalU3Ec__Iterator0_t2426526286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OpenLayMineUIButton/<WaitToOpenModal>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitToOpenModalU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2148684127 (U3CWaitToOpenModalU3Ec__Iterator0_t2426526286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OpenLayMineUIButton/<WaitToOpenModal>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitToOpenModalU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2076589511 (U3CWaitToOpenModalU3Ec__Iterator0_t2426526286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenLayMineUIButton/<WaitToOpenModal>c__Iterator0::Dispose()
extern "C"  void U3CWaitToOpenModalU3Ec__Iterator0_Dispose_m2803101216 (U3CWaitToOpenModalU3Ec__Iterator0_t2426526286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpenLayMineUIButton/<WaitToOpenModal>c__Iterator0::Reset()
extern "C"  void U3CWaitToOpenModalU3Ec__Iterator0_Reset_m2969111698 (U3CWaitToOpenModalU3Ec__Iterator0_t2426526286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
