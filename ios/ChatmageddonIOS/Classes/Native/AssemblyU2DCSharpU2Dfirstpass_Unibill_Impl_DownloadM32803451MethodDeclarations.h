﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.DownloadManager
struct DownloadManager_t32803451;
// Uniject.IUtil
struct IUtil_t2188430191;
// Uniject.IStorage
struct IStorage_t1347868490;
// Uniject.IURLFetcher
struct IURLFetcher_t2002848911;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.String
struct String_t;
// System.Action`2<System.String,System.String>
struct Action_2_t4234541925;
// System.Action`2<System.String,System.Int32>
struct Action_2_t4277199140;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Uniject.IHTTPRequest
struct IHTTPRequest_t54590966;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.FileStream
struct FileStream_t1695958676;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.DownloadManager::.ctor(Uniject.IUtil,Uniject.IStorage,Uniject.IURLFetcher,Uniject.ILogger,Unibill.Impl.BillingPlatform,System.String,System.String)
extern "C"  void DownloadManager__ctor_m4016877151 (DownloadManager_t32803451 * __this, Il2CppObject * ___util0, Il2CppObject * ___storage1, Il2CppObject * ___fetcher2, Il2CppObject * ___logger3, int32_t ___platform4, String_t* ___appSecret5, String_t* ___appId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::add_onDownloadCompletedEvent(System.Action`2<System.String,System.String>)
extern "C"  void DownloadManager_add_onDownloadCompletedEvent_m3965736668 (DownloadManager_t32803451 * __this, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::remove_onDownloadCompletedEvent(System.Action`2<System.String,System.String>)
extern "C"  void DownloadManager_remove_onDownloadCompletedEvent_m1777951855 (DownloadManager_t32803451 * __this, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::add_onDownloadFailedEvent(System.Action`2<System.String,System.String>)
extern "C"  void DownloadManager_add_onDownloadFailedEvent_m72248366 (DownloadManager_t32803451 * __this, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::remove_onDownloadFailedEvent(System.Action`2<System.String,System.String>)
extern "C"  void DownloadManager_remove_onDownloadFailedEvent_m1216215309 (DownloadManager_t32803451 * __this, Action_2_t4234541925 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::add_onDownloadProgressedEvent(System.Action`2<System.String,System.Int32>)
extern "C"  void DownloadManager_add_onDownloadProgressedEvent_m2357197692 (DownloadManager_t32803451 * __this, Action_2_t4277199140 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::remove_onDownloadProgressedEvent(System.Action`2<System.String,System.Int32>)
extern "C"  void DownloadManager_remove_onDownloadProgressedEvent_m2244747465 (DownloadManager_t32803451 * __this, Action_2_t4277199140 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::setBufferSize(System.Int32)
extern "C"  void DownloadManager_setBufferSize_m605600029 (DownloadManager_t32803451 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::downloadContentFor(System.String,System.String)
extern "C"  void DownloadManager_downloadContentFor_m807870637 (DownloadManager_t32803451 * __this, String_t* ___fileBundleId0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.DownloadManager::isDownloadScheduled(System.String)
extern "C"  bool DownloadManager_isDownloadScheduled_m77594074 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Unibill.Impl.DownloadManager::checkDownloads()
extern "C"  Il2CppObject * DownloadManager_checkDownloads_m3692264024 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Unibill.Impl.DownloadManager::monitorDownloads()
extern "C"  Il2CppObject * DownloadManager_monitorDownloads_m2839278690 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Unibill.Impl.DownloadManager::getQueueSize()
extern "C"  int32_t DownloadManager_getQueueSize_m2058474063 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Unibill.Impl.DownloadManager::deserialiseDownloads()
extern "C"  List_1_t1398341365 * DownloadManager_deserialiseDownloads_m1834374297 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::serialiseDownloads()
extern "C"  void DownloadManager_serialiseDownloads_m1133316375 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Unibill.Impl.DownloadManager::download(System.String)
extern "C"  Il2CppObject * DownloadManager_download_m1164367911 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.DownloadManager::isContentNotFound(Uniject.IHTTPRequest)
extern "C"  bool DownloadManager_isContentNotFound_m1606020623 (DownloadManager_t32803451 * __this, Il2CppObject * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::Unpack(System.String)
extern "C"  void DownloadManager_Unpack_m730601627 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::DeleteIfExists(System.String)
extern "C"  void DownloadManager_DeleteIfExists_m1413154123 (DownloadManager_t32803451 * __this, String_t* ___folder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::onDownloadFailedPermanently(System.String,System.String)
extern "C"  void DownloadManager_onDownloadFailedPermanently_m77670622 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::removeDownloadFromQueues(System.String)
extern "C"  void DownloadManager_removeDownloadFromQueues_m212452419 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.DownloadManager::verifyDownload(System.String)
extern "C"  bool DownloadManager_verifyDownload_m245386188 (DownloadManager_t32803451 * __this, String_t* ___filepath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::DownloadFlusher()
extern "C"  void DownloadManager_DownloadFlusher_m3466162896 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Unibill.Impl.DownloadManager::decodeBase64String(System.String)
extern "C"  ByteU5BU5D_t3397334013* DownloadManager_decodeBase64String_m1201649123 (DownloadManager_t32803451 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream Unibill.Impl.DownloadManager::openDownload(System.String)
extern "C"  FileStream_t1695958676 * DownloadManager_openDownload_m192353659 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getContentPath(System.String)
extern "C"  String_t* DownloadManager_getContentPath_m4200784220 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getUnpackPath(System.String)
extern "C"  String_t* DownloadManager_getUnpackPath_m703174965 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getZipPath(System.String)
extern "C"  String_t* DownloadManager_getZipPath_m2643228746 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getPartialPath(System.String)
extern "C"  String_t* DownloadManager_getPartialPath_m1566548244 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::saveVersion(System.String,System.String)
extern "C"  void DownloadManager_saveVersion_m3365458864 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, String_t* ___version1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getVersionToDownload(System.String)
extern "C"  String_t* DownloadManager_getVersionToDownload_m161495039 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::saveReceipt(System.String,System.String)
extern "C"  void DownloadManager_saveReceipt_m3172593664 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, String_t* ___receipt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getReceipt(System.String)
extern "C"  String_t* DownloadManager_getReceipt_m3724744126 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getReceiptPath(System.String)
extern "C"  String_t* DownloadManager_getReceiptPath_m177535591 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getVersionPath(System.String)
extern "C"  String_t* DownloadManager_getVersionPath_m3794302861 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getRootContentPath()
extern "C"  String_t* DownloadManager_getRootContentPath_m1541671902 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.DownloadManager::getDataPath(System.String)
extern "C"  String_t* DownloadManager_getDataPath_m2778141675 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.DownloadManager::isDownloaded(System.String)
extern "C"  bool DownloadManager_isDownloaded_m1465134200 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::createDataPathIfNecessary(System.String)
extern "C"  void DownloadManager_createDataPathIfNecessary_m955875484 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager::deleteContent(System.String)
extern "C"  void DownloadManager_deleteContent_m1021747581 (DownloadManager_t32803451 * __this, String_t* ___bundleId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Unibill.Impl.DownloadManager::getRandomSleep()
extern "C"  Il2CppObject * DownloadManager_getRandomSleep_m999184828 (DownloadManager_t32803451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
