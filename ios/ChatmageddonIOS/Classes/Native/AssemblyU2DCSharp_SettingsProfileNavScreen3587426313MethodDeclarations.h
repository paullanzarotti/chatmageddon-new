﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsProfileNavScreen
struct SettingsProfileNavScreen_t3587426313;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void SettingsProfileNavScreen::.ctor()
extern "C"  void SettingsProfileNavScreen__ctor_m1147181602 (SettingsProfileNavScreen_t3587426313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileNavScreen::Start()
extern "C"  void SettingsProfileNavScreen_Start_m3306185578 (SettingsProfileNavScreen_t3587426313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileNavScreen::UIClosing()
extern "C"  void SettingsProfileNavScreen_UIClosing_m551332763 (SettingsProfileNavScreen_t3587426313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void SettingsProfileNavScreen_ScreenLoad_m1517871070 (SettingsProfileNavScreen_t3587426313 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SettingsProfileNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void SettingsProfileNavScreen_ScreenUnload_m2194357598 (SettingsProfileNavScreen_t3587426313 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SettingsProfileNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool SettingsProfileNavScreen_ValidateScreenNavigation_m2925991563 (SettingsProfileNavScreen_t3587426313 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
