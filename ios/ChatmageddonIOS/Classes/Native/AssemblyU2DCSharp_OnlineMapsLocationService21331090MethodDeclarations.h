﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsLocationService
struct OnlineMapsLocationService_t21331090;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"

// System.Void OnlineMapsLocationService::.ctor()
extern "C"  void OnlineMapsLocationService__ctor_m3410741099 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsLocationService OnlineMapsLocationService::get_instance()
extern "C"  OnlineMapsLocationService_t21331090 * OnlineMapsLocationService_get_instance_m2792835282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::OnChangePosition()
extern "C"  void OnlineMapsLocationService_OnChangePosition_m3471562985 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::OnEnable()
extern "C"  void OnlineMapsLocationService_OnEnable_m3305194807 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::OnGUI()
extern "C"  void OnlineMapsLocationService_OnGUI_m2641341177 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsXML OnlineMapsLocationService::Save(OnlineMapsXML)
extern "C"  OnlineMapsXML_t3341520387 * OnlineMapsLocationService_Save_m1502813901 (OnlineMapsLocationService_t21331090 * __this, OnlineMapsXML_t3341520387 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::Start()
extern "C"  void OnlineMapsLocationService_Start_m303971031 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::Update()
extern "C"  void OnlineMapsLocationService_Update_m2942318752 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::UpdateCompassFromEmulator(System.Boolean&)
extern "C"  void OnlineMapsLocationService_UpdateCompassFromEmulator_m4200669840 (OnlineMapsLocationService_t21331090 * __this, bool* ___compassChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::UpdateCompassFromInput(System.Boolean&)
extern "C"  void OnlineMapsLocationService_UpdateCompassFromInput_m1608663859 (OnlineMapsLocationService_t21331090 * __this, bool* ___compassChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::UpdateMarker()
extern "C"  void OnlineMapsLocationService_UpdateMarker_m280873498 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::UpdatePosition()
extern "C"  void OnlineMapsLocationService_UpdatePosition_m3269067335 (OnlineMapsLocationService_t21331090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::UpdatePositionFromEmulator(System.Boolean&)
extern "C"  void OnlineMapsLocationService_UpdatePositionFromEmulator_m4180888225 (OnlineMapsLocationService_t21331090 * __this, bool* ___positionChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMapsLocationService::UpdatePositionFromInput(System.Boolean&)
extern "C"  void OnlineMapsLocationService_UpdatePositionFromInput_m3887505698 (OnlineMapsLocationService_t21331090 * __this, bool* ___positionChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
