﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldManager/<TrackShield>c__Iterator0
struct U3CTrackShieldU3Ec__Iterator0_t85697528;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ShieldManager/<TrackShield>c__Iterator0::.ctor()
extern "C"  void U3CTrackShieldU3Ec__Iterator0__ctor_m2698975399 (U3CTrackShieldU3Ec__Iterator0_t85697528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShieldManager/<TrackShield>c__Iterator0::MoveNext()
extern "C"  bool U3CTrackShieldU3Ec__Iterator0_MoveNext_m612342977 (U3CTrackShieldU3Ec__Iterator0_t85697528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShieldManager/<TrackShield>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTrackShieldU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3626753545 (U3CTrackShieldU3Ec__Iterator0_t85697528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ShieldManager/<TrackShield>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTrackShieldU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3626540769 (U3CTrackShieldU3Ec__Iterator0_t85697528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldManager/<TrackShield>c__Iterator0::Dispose()
extern "C"  void U3CTrackShieldU3Ec__Iterator0_Dispose_m3522630058 (U3CTrackShieldU3Ec__Iterator0_t85697528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldManager/<TrackShield>c__Iterator0::Reset()
extern "C"  void U3CTrackShieldU3Ec__Iterator0_Reset_m4242535996 (U3CTrackShieldU3Ec__Iterator0_t85697528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
