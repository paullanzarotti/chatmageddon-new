﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaunchRoutine/<FinishAnimRenderTween>c__Iterator2
struct U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LaunchRoutine/<FinishAnimRenderTween>c__Iterator2::.ctor()
extern "C"  void U3CFinishAnimRenderTweenU3Ec__Iterator2__ctor_m2547747685 (U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaunchRoutine/<FinishAnimRenderTween>c__Iterator2::MoveNext()
extern "C"  bool U3CFinishAnimRenderTweenU3Ec__Iterator2_MoveNext_m2901938007 (U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<FinishAnimRenderTween>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFinishAnimRenderTweenU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1876691363 (U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LaunchRoutine/<FinishAnimRenderTween>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFinishAnimRenderTweenU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1428106571 (U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<FinishAnimRenderTween>c__Iterator2::Dispose()
extern "C"  void U3CFinishAnimRenderTweenU3Ec__Iterator2_Dispose_m4007905644 (U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaunchRoutine/<FinishAnimRenderTween>c__Iterator2::Reset()
extern "C"  void U3CFinishAnimRenderTweenU3Ec__Iterator2_Reset_m3924929946 (U3CFinishAnimRenderTweenU3Ec__Iterator2_t1156187826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
