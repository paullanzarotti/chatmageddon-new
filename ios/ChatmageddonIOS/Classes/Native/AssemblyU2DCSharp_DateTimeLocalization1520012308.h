﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_DateTimeLocalization_Language3679215353.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DateTimeLocalization
struct  DateTimeLocalization_t1520012308  : public Il2CppObject
{
public:

public:
};

struct DateTimeLocalization_t1520012308_StaticFields
{
public:
	// DateTimeLocalization/Language DateTimeLocalization::_currentLanguage
	int32_t ____currentLanguage_0;
	// System.String[] DateTimeLocalization::_languageCodes
	StringU5BU5D_t1642385972* ____languageCodes_1;
	// System.Globalization.CultureInfo DateTimeLocalization::<Culture>k__BackingField
	CultureInfo_t3500843524 * ___U3CCultureU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__currentLanguage_0() { return static_cast<int32_t>(offsetof(DateTimeLocalization_t1520012308_StaticFields, ____currentLanguage_0)); }
	inline int32_t get__currentLanguage_0() const { return ____currentLanguage_0; }
	inline int32_t* get_address_of__currentLanguage_0() { return &____currentLanguage_0; }
	inline void set__currentLanguage_0(int32_t value)
	{
		____currentLanguage_0 = value;
	}

	inline static int32_t get_offset_of__languageCodes_1() { return static_cast<int32_t>(offsetof(DateTimeLocalization_t1520012308_StaticFields, ____languageCodes_1)); }
	inline StringU5BU5D_t1642385972* get__languageCodes_1() const { return ____languageCodes_1; }
	inline StringU5BU5D_t1642385972** get_address_of__languageCodes_1() { return &____languageCodes_1; }
	inline void set__languageCodes_1(StringU5BU5D_t1642385972* value)
	{
		____languageCodes_1 = value;
		Il2CppCodeGenWriteBarrier(&____languageCodes_1, value);
	}

	inline static int32_t get_offset_of_U3CCultureU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DateTimeLocalization_t1520012308_StaticFields, ___U3CCultureU3Ek__BackingField_2)); }
	inline CultureInfo_t3500843524 * get_U3CCultureU3Ek__BackingField_2() const { return ___U3CCultureU3Ek__BackingField_2; }
	inline CultureInfo_t3500843524 ** get_address_of_U3CCultureU3Ek__BackingField_2() { return &___U3CCultureU3Ek__BackingField_2; }
	inline void set_U3CCultureU3Ek__BackingField_2(CultureInfo_t3500843524 * value)
	{
		___U3CCultureU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCultureU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
