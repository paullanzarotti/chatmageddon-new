﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Int16>
struct GenericComparer_1_t1779221418;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Int16>::.ctor()
extern "C"  void GenericComparer_1__ctor_m158989555_gshared (GenericComparer_1_t1779221418 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m158989555(__this, method) ((  void (*) (GenericComparer_1_t1779221418 *, const MethodInfo*))GenericComparer_1__ctor_m158989555_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int16>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m2317189868_gshared (GenericComparer_1_t1779221418 * __this, int16_t ___x0, int16_t ___y1, const MethodInfo* method);
#define GenericComparer_1_Compare_m2317189868(__this, ___x0, ___y1, method) ((  int32_t (*) (GenericComparer_1_t1779221418 *, int16_t, int16_t, const MethodInfo*))GenericComparer_1_Compare_m2317189868_gshared)(__this, ___x0, ___y1, method)
