﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<DeleteAllContacts>c__AnonStorey18<System.Object>
struct U3CDeleteAllContactsU3Ec__AnonStorey18_t2775103307;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<DeleteAllContacts>c__AnonStorey18<System.Object>::.ctor()
extern "C"  void U3CDeleteAllContactsU3Ec__AnonStorey18__ctor_m952853372_gshared (U3CDeleteAllContactsU3Ec__AnonStorey18_t2775103307 * __this, const MethodInfo* method);
#define U3CDeleteAllContactsU3Ec__AnonStorey18__ctor_m952853372(__this, method) ((  void (*) (U3CDeleteAllContactsU3Ec__AnonStorey18_t2775103307 *, const MethodInfo*))U3CDeleteAllContactsU3Ec__AnonStorey18__ctor_m952853372_gshared)(__this, method)
// System.Void BaseServer`1/<DeleteAllContacts>c__AnonStorey18<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CDeleteAllContactsU3Ec__AnonStorey18_U3CU3Em__0_m3897356927_gshared (U3CDeleteAllContactsU3Ec__AnonStorey18_t2775103307 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CDeleteAllContactsU3Ec__AnonStorey18_U3CU3Em__0_m3897356927(__this, ___request0, ___response1, method) ((  void (*) (U3CDeleteAllContactsU3Ec__AnonStorey18_t2775103307 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CDeleteAllContactsU3Ec__AnonStorey18_U3CU3Em__0_m3897356927_gshared)(__this, ___request0, ___response1, method)
