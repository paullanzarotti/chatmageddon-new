﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_Mirror
struct CameraFilterPack_FX_Mirror_t1330003333;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_Mirror::.ctor()
extern "C"  void CameraFilterPack_FX_Mirror__ctor_m1687685998 (CameraFilterPack_FX_Mirror_t1330003333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Mirror::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_Mirror_get_material_m1823116529 (CameraFilterPack_FX_Mirror_t1330003333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::Start()
extern "C"  void CameraFilterPack_FX_Mirror_Start_m2435503142 (CameraFilterPack_FX_Mirror_t1330003333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_Mirror_OnRenderImage_m2357626422 (CameraFilterPack_FX_Mirror_t1330003333 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::Update()
extern "C"  void CameraFilterPack_FX_Mirror_Update_m2683197765 (CameraFilterPack_FX_Mirror_t1330003333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::OnDisable()
extern "C"  void CameraFilterPack_FX_Mirror_OnDisable_m1639073777 (CameraFilterPack_FX_Mirror_t1330003333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
