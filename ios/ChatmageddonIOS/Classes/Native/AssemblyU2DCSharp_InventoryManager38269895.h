﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Missile>>
struct Dictionary_2_t2097845322;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Shield>>
struct Dictionary_2_t316054179;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Mine>>
struct Dictionary_2_t4013341671;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen4083902911.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryManager
struct  InventoryManager_t38269895  : public MonoSingleton_1_t4083902911
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Missile>> InventoryManager::missileInventory
	Dictionary_2_t2097845322 * ___missileInventory_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Shield>> InventoryManager::shieldInventory
	Dictionary_2_t316054179 * ___shieldInventory_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Mine>> InventoryManager::mineInventory
	Dictionary_2_t4013341671 * ___mineInventory_5;

public:
	inline static int32_t get_offset_of_missileInventory_3() { return static_cast<int32_t>(offsetof(InventoryManager_t38269895, ___missileInventory_3)); }
	inline Dictionary_2_t2097845322 * get_missileInventory_3() const { return ___missileInventory_3; }
	inline Dictionary_2_t2097845322 ** get_address_of_missileInventory_3() { return &___missileInventory_3; }
	inline void set_missileInventory_3(Dictionary_2_t2097845322 * value)
	{
		___missileInventory_3 = value;
		Il2CppCodeGenWriteBarrier(&___missileInventory_3, value);
	}

	inline static int32_t get_offset_of_shieldInventory_4() { return static_cast<int32_t>(offsetof(InventoryManager_t38269895, ___shieldInventory_4)); }
	inline Dictionary_2_t316054179 * get_shieldInventory_4() const { return ___shieldInventory_4; }
	inline Dictionary_2_t316054179 ** get_address_of_shieldInventory_4() { return &___shieldInventory_4; }
	inline void set_shieldInventory_4(Dictionary_2_t316054179 * value)
	{
		___shieldInventory_4 = value;
		Il2CppCodeGenWriteBarrier(&___shieldInventory_4, value);
	}

	inline static int32_t get_offset_of_mineInventory_5() { return static_cast<int32_t>(offsetof(InventoryManager_t38269895, ___mineInventory_5)); }
	inline Dictionary_2_t4013341671 * get_mineInventory_5() const { return ___mineInventory_5; }
	inline Dictionary_2_t4013341671 ** get_address_of_mineInventory_5() { return &___mineInventory_5; }
	inline void set_mineInventory_5(Dictionary_2_t4013341671 * value)
	{
		___mineInventory_5 = value;
		Il2CppCodeGenWriteBarrier(&___mineInventory_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
