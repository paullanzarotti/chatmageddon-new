﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<RegNavScreen>
struct Collection_1_t208918113;
// System.Collections.Generic.IList`1<RegNavScreen>
struct IList_1_t1208113960;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// RegNavScreen[]
struct RegNavScreenU5BU5D_t3187910006;
// System.Collections.Generic.IEnumerator`1<RegNavScreen>
struct IEnumerator_1_t2437664482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"

// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m3615815052_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3615815052(__this, method) ((  void (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1__ctor_m3615815052_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m2097389959_gshared (Collection_1_t208918113 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m2097389959(__this, ___list0, method) ((  void (*) (Collection_1_t208918113 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m2097389959_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m314361161_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m314361161(__this, method) ((  bool (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m314361161_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4175373604_gshared (Collection_1_t208918113 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m4175373604(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t208918113 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4175373604_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3796226969_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3796226969(__this, method) ((  Il2CppObject * (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3796226969_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1517408480_gshared (Collection_1_t208918113 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1517408480(__this, ___value0, method) ((  int32_t (*) (Collection_1_t208918113 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1517408480_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3796845418_gshared (Collection_1_t208918113 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3796845418(__this, ___value0, method) ((  bool (*) (Collection_1_t208918113 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3796845418_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2927748106_gshared (Collection_1_t208918113 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2927748106(__this, ___value0, method) ((  int32_t (*) (Collection_1_t208918113 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2927748106_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3969392821_gshared (Collection_1_t208918113 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3969392821(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t208918113 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3969392821_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2774982757_gshared (Collection_1_t208918113 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2774982757(__this, ___value0, method) ((  void (*) (Collection_1_t208918113 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2774982757_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2725743588_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2725743588(__this, method) ((  bool (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2725743588_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m492890154_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m492890154(__this, method) ((  Il2CppObject * (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m492890154_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2990391929_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2990391929(__this, method) ((  bool (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2990391929_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1898845464_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1898845464(__this, method) ((  bool (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1898845464_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m818521853_gshared (Collection_1_t208918113 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m818521853(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m818521853_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2465748198_gshared (Collection_1_t208918113 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2465748198(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t208918113 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2465748198_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m517896833_gshared (Collection_1_t208918113 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m517896833(__this, ___item0, method) ((  void (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_Add_m517896833_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m1755148333_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1755148333(__this, method) ((  void (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_Clear_m1755148333_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3717804243_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3717804243(__this, method) ((  void (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_ClearItems_m3717804243_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m264192855_gshared (Collection_1_t208918113 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m264192855(__this, ___item0, method) ((  bool (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_Contains_m264192855_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1559875657_gshared (Collection_1_t208918113 * __this, RegNavScreenU5BU5D_t3187910006* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1559875657(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t208918113 *, RegNavScreenU5BU5D_t3187910006*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1559875657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<RegNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2796653516_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2796653516(__this, method) ((  Il2CppObject* (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_GetEnumerator_m2796653516_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<RegNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1577265073_gshared (Collection_1_t208918113 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1577265073(__this, ___item0, method) ((  int32_t (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m1577265073_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3418807966_gshared (Collection_1_t208918113 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3418807966(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t208918113 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m3418807966_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2264609847_gshared (Collection_1_t208918113 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2264609847(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t208918113 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2264609847_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m1532041080_gshared (Collection_1_t208918113 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1532041080(__this, ___item0, method) ((  bool (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_Remove_m1532041080_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4198205018_gshared (Collection_1_t208918113 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4198205018(__this, ___index0, method) ((  void (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4198205018_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1832924920_gshared (Collection_1_t208918113 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1832924920(__this, ___index0, method) ((  void (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1832924920_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<RegNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3649547232_gshared (Collection_1_t208918113 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3649547232(__this, method) ((  int32_t (*) (Collection_1_t208918113 *, const MethodInfo*))Collection_1_get_Count_m3649547232_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<RegNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m247123382_gshared (Collection_1_t208918113 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m247123382(__this, ___index0, method) ((  int32_t (*) (Collection_1_t208918113 *, int32_t, const MethodInfo*))Collection_1_get_Item_m247123382_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2866343585_gshared (Collection_1_t208918113 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2866343585(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t208918113 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m2866343585_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1066719574_gshared (Collection_1_t208918113 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1066719574(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t208918113 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1066719574_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3194102011_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3194102011(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3194102011_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<RegNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m1854407135_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1854407135(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1854407135_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<RegNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3393822279_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3393822279(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3393822279_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1524359311_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1524359311(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1524359311_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<RegNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3923225274_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3923225274(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3923225274_gshared)(__this /* static, unused */, ___list0, method)
