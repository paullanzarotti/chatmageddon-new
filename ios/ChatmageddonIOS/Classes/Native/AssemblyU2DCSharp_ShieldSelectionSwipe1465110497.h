﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShieldModal
struct ShieldModal_t4049164426;

#include "AssemblyU2DCSharp_ItemSelectionSwipe2104393431.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldSelectionSwipe
struct  ShieldSelectionSwipe_t1465110497  : public ItemSelectionSwipe_t2104393431
{
public:
	// ShieldModal ShieldSelectionSwipe::modal
	ShieldModal_t4049164426 * ___modal_11;

public:
	inline static int32_t get_offset_of_modal_11() { return static_cast<int32_t>(offsetof(ShieldSelectionSwipe_t1465110497, ___modal_11)); }
	inline ShieldModal_t4049164426 * get_modal_11() const { return ___modal_11; }
	inline ShieldModal_t4049164426 ** get_address_of_modal_11() { return &___modal_11; }
	inline void set_modal_11(ShieldModal_t4049164426 * value)
	{
		___modal_11 = value;
		Il2CppCodeGenWriteBarrier(&___modal_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
