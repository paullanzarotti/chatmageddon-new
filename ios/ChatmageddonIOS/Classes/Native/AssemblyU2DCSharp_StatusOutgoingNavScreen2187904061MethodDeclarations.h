﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StatusOutgoingNavScreen
struct StatusOutgoingNavScreen_t2187904061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void StatusOutgoingNavScreen::.ctor()
extern "C"  void StatusOutgoingNavScreen__ctor_m596042604 (StatusOutgoingNavScreen_t2187904061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusOutgoingNavScreen::Start()
extern "C"  void StatusOutgoingNavScreen_Start_m3262633412 (StatusOutgoingNavScreen_t2187904061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusOutgoingNavScreen::UIClosing()
extern "C"  void StatusOutgoingNavScreen_UIClosing_m2693888711 (StatusOutgoingNavScreen_t2187904061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusOutgoingNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void StatusOutgoingNavScreen_ScreenLoad_m866147816 (StatusOutgoingNavScreen_t2187904061 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusOutgoingNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void StatusOutgoingNavScreen_ScreenUnload_m1889858656 (StatusOutgoingNavScreen_t2187904061 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StatusOutgoingNavScreen::UpdateUI()
extern "C"  void StatusOutgoingNavScreen_UpdateUI_m458661433 (StatusOutgoingNavScreen_t2187904061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StatusOutgoingNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool StatusOutgoingNavScreen_ValidateScreenNavigation_m2513246847 (StatusOutgoingNavScreen_t2187904061 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
