﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.TimeSpanValidator
struct TimeSpanValidator_t4078846089;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Configuration.TimeSpanValidator::.ctor(System.TimeSpan,System.TimeSpan)
extern "C"  void TimeSpanValidator__ctor_m401177130 (TimeSpanValidator_t4078846089 * __this, TimeSpan_t3430258949  ___minValue0, TimeSpan_t3430258949  ___maxValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.TimeSpanValidator::.ctor(System.TimeSpan,System.TimeSpan,System.Boolean)
extern "C"  void TimeSpanValidator__ctor_m3086080589 (TimeSpanValidator_t4078846089 * __this, TimeSpan_t3430258949  ___minValue0, TimeSpan_t3430258949  ___maxValue1, bool ___rangeIsExclusive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.TimeSpanValidator::.ctor(System.TimeSpan,System.TimeSpan,System.Boolean,System.Int64)
extern "C"  void TimeSpanValidator__ctor_m2175997863 (TimeSpanValidator_t4078846089 * __this, TimeSpan_t3430258949  ___minValue0, TimeSpan_t3430258949  ___maxValue1, bool ___rangeIsExclusive2, int64_t ___resolutionInSeconds3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.TimeSpanValidator::CanValidate(System.Type)
extern "C"  bool TimeSpanValidator_CanValidate_m4088519063 (TimeSpanValidator_t4078846089 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.TimeSpanValidator::Validate(System.Object)
extern "C"  void TimeSpanValidator_Validate_m2271454024 (TimeSpanValidator_t4078846089 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
