﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Fabric.Internal.Crashlytics.CrashlyticsInit
struct CrashlyticsInit_t2902165287;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t1916531888;
// UnityEngine.Application/LogCallback
struct LogCallback_t1867914413;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fabric.Internal.Crashlytics.CrashlyticsInit
struct  CrashlyticsInit_t2902165287  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct CrashlyticsInit_t2902165287_StaticFields
{
public:
	// System.String Fabric.Internal.Crashlytics.CrashlyticsInit::kitName
	String_t* ___kitName_2;
	// Fabric.Internal.Crashlytics.CrashlyticsInit Fabric.Internal.Crashlytics.CrashlyticsInit::instance
	CrashlyticsInit_t2902165287 * ___instance_3;
	// System.UnhandledExceptionEventHandler Fabric.Internal.Crashlytics.CrashlyticsInit::<>f__mg$cache0
	UnhandledExceptionEventHandler_t1916531888 * ___U3CU3Ef__mgU24cache0_4;
	// UnityEngine.Application/LogCallback Fabric.Internal.Crashlytics.CrashlyticsInit::<>f__mg$cache1
	LogCallback_t1867914413 * ___U3CU3Ef__mgU24cache1_5;

public:
	inline static int32_t get_offset_of_kitName_2() { return static_cast<int32_t>(offsetof(CrashlyticsInit_t2902165287_StaticFields, ___kitName_2)); }
	inline String_t* get_kitName_2() const { return ___kitName_2; }
	inline String_t** get_address_of_kitName_2() { return &___kitName_2; }
	inline void set_kitName_2(String_t* value)
	{
		___kitName_2 = value;
		Il2CppCodeGenWriteBarrier(&___kitName_2, value);
	}

	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(CrashlyticsInit_t2902165287_StaticFields, ___instance_3)); }
	inline CrashlyticsInit_t2902165287 * get_instance_3() const { return ___instance_3; }
	inline CrashlyticsInit_t2902165287 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CrashlyticsInit_t2902165287 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(CrashlyticsInit_t2902165287_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline UnhandledExceptionEventHandler_t1916531888 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline UnhandledExceptionEventHandler_t1916531888 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(UnhandledExceptionEventHandler_t1916531888 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_5() { return static_cast<int32_t>(offsetof(CrashlyticsInit_t2902165287_StaticFields, ___U3CU3Ef__mgU24cache1_5)); }
	inline LogCallback_t1867914413 * get_U3CU3Ef__mgU24cache1_5() const { return ___U3CU3Ef__mgU24cache1_5; }
	inline LogCallback_t1867914413 ** get_address_of_U3CU3Ef__mgU24cache1_5() { return &___U3CU3Ef__mgU24cache1_5; }
	inline void set_U3CU3Ef__mgU24cache1_5(LogCallback_t1867914413 * value)
	{
		___U3CU3Ef__mgU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
