﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnDrawTooltipExample
struct OnDrawTooltipExample_t3197201692;
// OnlineMapsMarker
struct OnlineMapsMarker_t3492166682;
// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3492166682.h"
#include "AssemblyU2DCSharp_OnlineMapsMarkerBase3900955221.h"

// System.Void OnDrawTooltipExample::.ctor()
extern "C"  void OnDrawTooltipExample__ctor_m3075035095 (OnDrawTooltipExample_t3197201692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnDrawTooltipExample::Start()
extern "C"  void OnDrawTooltipExample_Start_m4196609627 (OnDrawTooltipExample_t3197201692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnDrawTooltipExample::DrawBoxAroundMarker(OnlineMapsMarker)
extern "C"  void OnDrawTooltipExample_DrawBoxAroundMarker_m2898095053 (OnDrawTooltipExample_t3197201692 * __this, OnlineMapsMarker_t3492166682 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnDrawTooltipExample::OnDrawTooltip(OnlineMapsMarkerBase)
extern "C"  void OnDrawTooltipExample_OnDrawTooltip_m2522850828 (OnDrawTooltipExample_t3197201692 * __this, OnlineMapsMarkerBase_t3900955221 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnDrawTooltipExample::OnMarkerDrawTooltip(OnlineMapsMarkerBase)
extern "C"  void OnDrawTooltipExample_OnMarkerDrawTooltip_m513445070 (OnDrawTooltipExample_t3197201692 * __this, OnlineMapsMarkerBase_t3900955221 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
