﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatThreadsILP/<UpdateItemData>c__AnonStorey0
struct U3CUpdateItemDataU3Ec__AnonStorey0_t1678493984;
// ChatThread
struct ChatThread_t2394323482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ChatThread2394323482.h"

// System.Void ChatThreadsILP/<UpdateItemData>c__AnonStorey0::.ctor()
extern "C"  void U3CUpdateItemDataU3Ec__AnonStorey0__ctor_m2892008115 (U3CUpdateItemDataU3Ec__AnonStorey0_t1678493984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ChatThreadsILP/<UpdateItemData>c__AnonStorey0::<>m__0(ChatThread)
extern "C"  bool U3CUpdateItemDataU3Ec__AnonStorey0_U3CU3Em__0_m3869246578 (U3CUpdateItemDataU3Ec__AnonStorey0_t1678493984 * __this, ChatThread_t2394323482 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
