﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatNavigateForwardButton
struct ChatNavigateForwardButton_t4025092154;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatNavigateForwardButton::.ctor()
extern "C"  void ChatNavigateForwardButton__ctor_m962908911 (ChatNavigateForwardButton_t4025092154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigateForwardButton::OnButtonClick()
extern "C"  void ChatNavigateForwardButton_OnButtonClick_m989393566 (ChatNavigateForwardButton_t4025092154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
