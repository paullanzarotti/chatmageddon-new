﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.P31RestKit
struct P31RestKit_t1799911290;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Action`2<System.String,System.Object>
struct Action_2_t599803691;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_String2029220233.h"
#include "P31RestKit_Prime31_HTTPVerb1395544859.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"

// System.Void Prime31.P31RestKit::.ctor()
extern "C"  void P31RestKit__ctor_m1256707194 (P31RestKit_t1799911290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Prime31.P31RestKit::get_surrogateGameObject()
extern "C"  GameObject_t1756533147 * P31RestKit_get_surrogateGameObject_m3028491179 (P31RestKit_t1799911290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit::set_surrogateGameObject(UnityEngine.GameObject)
extern "C"  void P31RestKit_set_surrogateGameObject_m1855410268 (P31RestKit_t1799911290 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour Prime31.P31RestKit::get_surrogateMonobehaviour()
extern "C"  MonoBehaviour_t1158329972 * P31RestKit_get_surrogateMonobehaviour_m2184075163 (P31RestKit_t1799911290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit::set_surrogateMonobehaviour(UnityEngine.MonoBehaviour)
extern "C"  void P31RestKit_set_surrogateMonobehaviour_m3262828644 (P31RestKit_t1799911290 * __this, MonoBehaviour_t1158329972 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Prime31.P31RestKit::send(System.String,Prime31.HTTPVerb,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Action`2<System.String,System.Object>)
extern "C"  Il2CppObject * P31RestKit_send_m2163074132 (P31RestKit_t1799911290 * __this, String_t* ___path0, int32_t ___httpVerb1, Dictionary_2_t309261261 * ___parameters2, Action_2_t599803691 * ___onComplete3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW Prime31.P31RestKit::processRequest(System.String,Prime31.HTTPVerb,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  WWW_t2919945039 * P31RestKit_processRequest_m3974629712 (P31RestKit_t1799911290 * __this, String_t* ___path0, int32_t ___httpVerb1, Dictionary_2_t309261261 * ___parameters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Prime31.P31RestKit::headersForRequest(Prime31.HTTPVerb,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  Dictionary_2_t3943999495 * P31RestKit_headersForRequest_m2084627913 (P31RestKit_t1799911290 * __this, int32_t ___httpVerb0, Dictionary_2_t3943999495 * ___headers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit::processResponse(UnityEngine.WWW,System.Action`2<System.String,System.Object>)
extern "C"  void P31RestKit_processResponse_m3594510993 (P31RestKit_t1799911290 * __this, WWW_t2919945039 * ___www0, Action_2_t599803691 * ___onComplete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Prime31.P31RestKit::isResponseJson(UnityEngine.WWW)
extern "C"  bool P31RestKit_isResponseJson_m1235923117 (P31RestKit_t1799911290 * __this, WWW_t2919945039 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary Prime31.P31RestKit::getHeadersFromForm(UnityEngine.WWWForm)
extern "C"  Il2CppObject * P31RestKit_getHeadersFromForm_m3874259084 (P31RestKit_t1799911290 * __this, WWWForm_t3950226929 * ___form0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit::get(System.String,System.Action`2<System.String,System.Object>)
extern "C"  void P31RestKit_get_m1262824013 (P31RestKit_t1799911290 * __this, String_t* ___path0, Action_2_t599803691 * ___completionHandler1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit::get(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Action`2<System.String,System.Object>)
extern "C"  void P31RestKit_get_m908437476 (P31RestKit_t1799911290 * __this, String_t* ___path0, Dictionary_2_t309261261 * ___parameters1, Action_2_t599803691 * ___completionHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.P31RestKit::post(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Action`2<System.String,System.Object>)
extern "C"  void P31RestKit_post_m3971960840 (P31RestKit_t1799911290 * __this, String_t* ___path0, Dictionary_2_t309261261 * ___parameters1, Action_2_t599803691 * ___completionHandler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
