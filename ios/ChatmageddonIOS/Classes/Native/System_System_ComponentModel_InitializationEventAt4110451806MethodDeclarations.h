﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.InitializationEventAttribute
struct InitializationEventAttribute_t4110451806;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.ComponentModel.InitializationEventAttribute::.ctor(System.String)
extern "C"  void InitializationEventAttribute__ctor_m2193751881 (InitializationEventAttribute_t4110451806 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.InitializationEventAttribute::get_EventName()
extern "C"  String_t* InitializationEventAttribute_get_EventName_m374077446 (InitializationEventAttribute_t4110451806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
