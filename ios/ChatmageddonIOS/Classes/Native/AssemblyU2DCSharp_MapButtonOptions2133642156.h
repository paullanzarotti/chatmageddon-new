﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenRotation
struct TweenRotation_t1747194511;
// TweenAlpha
struct TweenAlpha_t2421518635;
// System.Collections.Generic.List`1<SwitchMapStateButton>
struct List_1_t2341171969;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapButtonOptions
struct  MapButtonOptions_t2133642156  : public SFXButton_t792651341
{
public:
	// TweenRotation MapButtonOptions::rotTween
	TweenRotation_t1747194511 * ___rotTween_5;
	// TweenAlpha MapButtonOptions::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_6;
	// TweenAlpha MapButtonOptions::activateAlphaTween
	TweenAlpha_t2421518635 * ___activateAlphaTween_7;
	// System.Boolean MapButtonOptions::optionsOpen
	bool ___optionsOpen_8;
	// System.Collections.Generic.List`1<SwitchMapStateButton> MapButtonOptions::mapOptions
	List_1_t2341171969 * ___mapOptions_9;

public:
	inline static int32_t get_offset_of_rotTween_5() { return static_cast<int32_t>(offsetof(MapButtonOptions_t2133642156, ___rotTween_5)); }
	inline TweenRotation_t1747194511 * get_rotTween_5() const { return ___rotTween_5; }
	inline TweenRotation_t1747194511 ** get_address_of_rotTween_5() { return &___rotTween_5; }
	inline void set_rotTween_5(TweenRotation_t1747194511 * value)
	{
		___rotTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___rotTween_5, value);
	}

	inline static int32_t get_offset_of_alphaTween_6() { return static_cast<int32_t>(offsetof(MapButtonOptions_t2133642156, ___alphaTween_6)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_6() const { return ___alphaTween_6; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_6() { return &___alphaTween_6; }
	inline void set_alphaTween_6(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_6, value);
	}

	inline static int32_t get_offset_of_activateAlphaTween_7() { return static_cast<int32_t>(offsetof(MapButtonOptions_t2133642156, ___activateAlphaTween_7)); }
	inline TweenAlpha_t2421518635 * get_activateAlphaTween_7() const { return ___activateAlphaTween_7; }
	inline TweenAlpha_t2421518635 ** get_address_of_activateAlphaTween_7() { return &___activateAlphaTween_7; }
	inline void set_activateAlphaTween_7(TweenAlpha_t2421518635 * value)
	{
		___activateAlphaTween_7 = value;
		Il2CppCodeGenWriteBarrier(&___activateAlphaTween_7, value);
	}

	inline static int32_t get_offset_of_optionsOpen_8() { return static_cast<int32_t>(offsetof(MapButtonOptions_t2133642156, ___optionsOpen_8)); }
	inline bool get_optionsOpen_8() const { return ___optionsOpen_8; }
	inline bool* get_address_of_optionsOpen_8() { return &___optionsOpen_8; }
	inline void set_optionsOpen_8(bool value)
	{
		___optionsOpen_8 = value;
	}

	inline static int32_t get_offset_of_mapOptions_9() { return static_cast<int32_t>(offsetof(MapButtonOptions_t2133642156, ___mapOptions_9)); }
	inline List_1_t2341171969 * get_mapOptions_9() const { return ___mapOptions_9; }
	inline List_1_t2341171969 ** get_address_of_mapOptions_9() { return &___mapOptions_9; }
	inline void set_mapOptions_9(List_1_t2341171969 * value)
	{
		___mapOptions_9 = value;
		Il2CppCodeGenWriteBarrier(&___mapOptions_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
