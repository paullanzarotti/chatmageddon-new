﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<RegNavScreen>
struct DefaultComparer_t2289103520;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<RegNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m1533865613_gshared (DefaultComparer_t2289103520 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1533865613(__this, method) ((  void (*) (DefaultComparer_t2289103520 *, const MethodInfo*))DefaultComparer__ctor_m1533865613_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<RegNavScreen>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m194650256_gshared (DefaultComparer_t2289103520 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m194650256(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2289103520 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m194650256_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<RegNavScreen>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2196618704_gshared (DefaultComparer_t2289103520 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2196618704(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2289103520 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2196618704_gshared)(__this, ___x0, ___y1, method)
