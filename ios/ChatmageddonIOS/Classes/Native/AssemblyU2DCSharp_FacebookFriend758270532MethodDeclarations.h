﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookFriend
struct FacebookFriend_t758270532;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FacebookFriend::.ctor(System.String,System.String,System.String)
extern "C"  void FacebookFriend__ctor_m156082579 (FacebookFriend_t758270532 * __this, String_t* ___FirstName0, String_t* ___ID1, String_t* ___appScore2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
