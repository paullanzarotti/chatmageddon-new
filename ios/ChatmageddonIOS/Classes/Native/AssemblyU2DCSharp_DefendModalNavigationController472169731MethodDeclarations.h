﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendModalNavigationController
struct DefendModalNavigationController_t472169731;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DefendNavScreen3838619703.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void DefendModalNavigationController::.ctor()
extern "C"  void DefendModalNavigationController__ctor_m375229426 (DefendModalNavigationController_t472169731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModalNavigationController::NavigateBackwards()
extern "C"  void DefendModalNavigationController_NavigateBackwards_m2912771489 (DefendModalNavigationController_t472169731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModalNavigationController::NavigateForwards(DefendNavScreen)
extern "C"  void DefendModalNavigationController_NavigateForwards_m2731686854 (DefendModalNavigationController_t472169731 * __this, int32_t ___nextScreen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DefendModalNavigationController::ValidateNavigation(DefendNavScreen,NavigationDirection)
extern "C"  bool DefendModalNavigationController_ValidateNavigation_m2148565834 (DefendModalNavigationController_t472169731 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModalNavigationController::LoadNavScreen(DefendNavScreen,NavigationDirection,System.Boolean)
extern "C"  void DefendModalNavigationController_LoadNavScreen_m1703725468 (DefendModalNavigationController_t472169731 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModalNavigationController::UnloadNavScreen(DefendNavScreen,NavigationDirection)
extern "C"  void DefendModalNavigationController_UnloadNavScreen_m557845106 (DefendModalNavigationController_t472169731 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendModalNavigationController::PopulateTitleBar(DefendNavScreen)
extern "C"  void DefendModalNavigationController_PopulateTitleBar_m219882386 (DefendModalNavigationController_t472169731 * __this, int32_t ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
