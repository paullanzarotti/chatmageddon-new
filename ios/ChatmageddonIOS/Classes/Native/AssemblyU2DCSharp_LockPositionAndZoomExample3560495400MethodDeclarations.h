﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LockPositionAndZoomExample
struct LockPositionAndZoomExample_t3560495400;

#include "codegen/il2cpp-codegen.h"

// System.Void LockPositionAndZoomExample::.ctor()
extern "C"  void LockPositionAndZoomExample__ctor_m19364333 (LockPositionAndZoomExample_t3560495400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockPositionAndZoomExample::Start()
extern "C"  void LockPositionAndZoomExample_Start_m1788578677 (LockPositionAndZoomExample_t3560495400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
