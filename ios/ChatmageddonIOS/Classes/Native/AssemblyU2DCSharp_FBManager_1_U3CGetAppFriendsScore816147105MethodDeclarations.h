﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3<System.Object>
struct U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105;
// Facebook.Unity.IGraphResult
struct IGraphResult_t3984946686;

#include "codegen/il2cpp-codegen.h"

// System.Void FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3<System.Object>::.ctor()
extern "C"  void U3CGetAppFriendsScoreListU3Ec__AnonStorey3__ctor_m184220960_gshared (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * __this, const MethodInfo* method);
#define U3CGetAppFriendsScoreListU3Ec__AnonStorey3__ctor_m184220960(__this, method) ((  void (*) (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 *, const MethodInfo*))U3CGetAppFriendsScoreListU3Ec__AnonStorey3__ctor_m184220960_gshared)(__this, method)
// System.Void FBManager`1/<GetAppFriendsScoreList>c__AnonStorey3<System.Object>::<>m__0(Facebook.Unity.IGraphResult)
extern "C"  void U3CGetAppFriendsScoreListU3Ec__AnonStorey3_U3CU3Em__0_m472877894_gshared (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define U3CGetAppFriendsScoreListU3Ec__AnonStorey3_U3CU3Em__0_m472877894(__this, ___result0, method) ((  void (*) (U3CGetAppFriendsScoreListU3Ec__AnonStorey3_t816147105 *, Il2CppObject *, const MethodInfo*))U3CGetAppFriendsScoreListU3Ec__AnonStorey3_U3CU3Em__0_m472877894_gshared)(__this, ___result0, method)
