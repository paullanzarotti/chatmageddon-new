﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Shield
struct Shield_t3327121081;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// UISprite
struct UISprite_t603616735;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldDisplayController
struct  ShieldDisplayController_t1783798351  : public MonoBehaviour_t1158329972
{
public:
	// Shield ShieldDisplayController::shield
	Shield_t3327121081 * ___shield_2;
	// LaunchedMissile ShieldDisplayController::missile
	LaunchedMissile_t1542515810 * ___missile_3;
	// UISprite ShieldDisplayController::shieldSprite
	UISprite_t603616735 * ___shieldSprite_4;
	// UnityEngine.Color ShieldDisplayController::bronzeColour
	Color_t2020392075  ___bronzeColour_5;
	// UnityEngine.Color ShieldDisplayController::silverColour
	Color_t2020392075  ___silverColour_6;
	// UnityEngine.Color ShieldDisplayController::goldColour
	Color_t2020392075  ___goldColour_7;

public:
	inline static int32_t get_offset_of_shield_2() { return static_cast<int32_t>(offsetof(ShieldDisplayController_t1783798351, ___shield_2)); }
	inline Shield_t3327121081 * get_shield_2() const { return ___shield_2; }
	inline Shield_t3327121081 ** get_address_of_shield_2() { return &___shield_2; }
	inline void set_shield_2(Shield_t3327121081 * value)
	{
		___shield_2 = value;
		Il2CppCodeGenWriteBarrier(&___shield_2, value);
	}

	inline static int32_t get_offset_of_missile_3() { return static_cast<int32_t>(offsetof(ShieldDisplayController_t1783798351, ___missile_3)); }
	inline LaunchedMissile_t1542515810 * get_missile_3() const { return ___missile_3; }
	inline LaunchedMissile_t1542515810 ** get_address_of_missile_3() { return &___missile_3; }
	inline void set_missile_3(LaunchedMissile_t1542515810 * value)
	{
		___missile_3 = value;
		Il2CppCodeGenWriteBarrier(&___missile_3, value);
	}

	inline static int32_t get_offset_of_shieldSprite_4() { return static_cast<int32_t>(offsetof(ShieldDisplayController_t1783798351, ___shieldSprite_4)); }
	inline UISprite_t603616735 * get_shieldSprite_4() const { return ___shieldSprite_4; }
	inline UISprite_t603616735 ** get_address_of_shieldSprite_4() { return &___shieldSprite_4; }
	inline void set_shieldSprite_4(UISprite_t603616735 * value)
	{
		___shieldSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___shieldSprite_4, value);
	}

	inline static int32_t get_offset_of_bronzeColour_5() { return static_cast<int32_t>(offsetof(ShieldDisplayController_t1783798351, ___bronzeColour_5)); }
	inline Color_t2020392075  get_bronzeColour_5() const { return ___bronzeColour_5; }
	inline Color_t2020392075 * get_address_of_bronzeColour_5() { return &___bronzeColour_5; }
	inline void set_bronzeColour_5(Color_t2020392075  value)
	{
		___bronzeColour_5 = value;
	}

	inline static int32_t get_offset_of_silverColour_6() { return static_cast<int32_t>(offsetof(ShieldDisplayController_t1783798351, ___silverColour_6)); }
	inline Color_t2020392075  get_silverColour_6() const { return ___silverColour_6; }
	inline Color_t2020392075 * get_address_of_silverColour_6() { return &___silverColour_6; }
	inline void set_silverColour_6(Color_t2020392075  value)
	{
		___silverColour_6 = value;
	}

	inline static int32_t get_offset_of_goldColour_7() { return static_cast<int32_t>(offsetof(ShieldDisplayController_t1783798351, ___goldColour_7)); }
	inline Color_t2020392075  get_goldColour_7() const { return ___goldColour_7; }
	inline Color_t2020392075 * get_address_of_goldColour_7() { return &___goldColour_7; }
	inline void set_goldColour_7(Color_t2020392075  value)
	{
		___goldColour_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
