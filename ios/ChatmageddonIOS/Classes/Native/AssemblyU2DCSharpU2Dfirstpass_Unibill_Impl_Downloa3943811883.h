﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Unibill.Impl.DownloadManager
struct DownloadManager_t32803451;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.DownloadManager/<onDownloadFailedPermanently>c__AnonStorey4
struct  U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883  : public Il2CppObject
{
public:
	// System.String Unibill.Impl.DownloadManager/<onDownloadFailedPermanently>c__AnonStorey4::bundleId
	String_t* ___bundleId_0;
	// System.String Unibill.Impl.DownloadManager/<onDownloadFailedPermanently>c__AnonStorey4::error
	String_t* ___error_1;
	// Unibill.Impl.DownloadManager Unibill.Impl.DownloadManager/<onDownloadFailedPermanently>c__AnonStorey4::$this
	DownloadManager_t32803451 * ___U24this_2;

public:
	inline static int32_t get_offset_of_bundleId_0() { return static_cast<int32_t>(offsetof(U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883, ___bundleId_0)); }
	inline String_t* get_bundleId_0() const { return ___bundleId_0; }
	inline String_t** get_address_of_bundleId_0() { return &___bundleId_0; }
	inline void set_bundleId_0(String_t* value)
	{
		___bundleId_0 = value;
		Il2CppCodeGenWriteBarrier(&___bundleId_0, value);
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883, ___error_1)); }
	inline String_t* get_error_1() const { return ___error_1; }
	inline String_t** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(String_t* value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier(&___error_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883, ___U24this_2)); }
	inline DownloadManager_t32803451 * get_U24this_2() const { return ___U24this_2; }
	inline DownloadManager_t32803451 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(DownloadManager_t32803451 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
