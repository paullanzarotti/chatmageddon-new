﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int16>
struct List_1_t3410367046;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2945096720.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int16>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2143606664_gshared (Enumerator_t2945096720 * __this, List_1_t3410367046 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2143606664(__this, ___l0, method) ((  void (*) (Enumerator_t2945096720 *, List_1_t3410367046 *, const MethodInfo*))Enumerator__ctor_m2143606664_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2817981434_gshared (Enumerator_t2945096720 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2817981434(__this, method) ((  void (*) (Enumerator_t2945096720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2817981434_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2462243140_gshared (Enumerator_t2945096720 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2462243140(__this, method) ((  Il2CppObject * (*) (Enumerator_t2945096720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2462243140_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int16>::Dispose()
extern "C"  void Enumerator_Dispose_m2629292449_gshared (Enumerator_t2945096720 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2629292449(__this, method) ((  void (*) (Enumerator_t2945096720 *, const MethodInfo*))Enumerator_Dispose_m2629292449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int16>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3775365198_gshared (Enumerator_t2945096720 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3775365198(__this, method) ((  void (*) (Enumerator_t2945096720 *, const MethodInfo*))Enumerator_VerifyState_m3775365198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int16>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1385894430_gshared (Enumerator_t2945096720 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1385894430(__this, method) ((  bool (*) (Enumerator_t2945096720 *, const MethodInfo*))Enumerator_MoveNext_m1385894430_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int16>::get_Current()
extern "C"  int16_t Enumerator_get_Current_m2613660929_gshared (Enumerator_t2945096720 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2613660929(__this, method) ((  int16_t (*) (Enumerator_t2945096720 *, const MethodInfo*))Enumerator_get_Current_m2613660929_gshared)(__this, method)
