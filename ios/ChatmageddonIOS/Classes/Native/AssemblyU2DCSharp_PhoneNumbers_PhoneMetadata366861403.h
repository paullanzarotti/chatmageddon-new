﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneMetadata
struct PhoneMetadata_t366861403;
// PhoneNumbers.PhoneNumberDesc
struct PhoneNumberDesc_t922391174;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PhoneNumbers.NumberFormat>
struct List_1_t4105827652;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneMetadata
struct  PhoneMetadata_t366861403  : public Il2CppObject
{
public:
	// System.Boolean PhoneNumbers.PhoneMetadata::hasGeneralDesc
	bool ___hasGeneralDesc_2;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::generalDesc_
	PhoneNumberDesc_t922391174 * ___generalDesc__3;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasFixedLine
	bool ___hasFixedLine_5;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::fixedLine_
	PhoneNumberDesc_t922391174 * ___fixedLine__6;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasMobile
	bool ___hasMobile_8;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::mobile_
	PhoneNumberDesc_t922391174 * ___mobile__9;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasTollFree
	bool ___hasTollFree_11;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::tollFree_
	PhoneNumberDesc_t922391174 * ___tollFree__12;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasPremiumRate
	bool ___hasPremiumRate_14;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::premiumRate_
	PhoneNumberDesc_t922391174 * ___premiumRate__15;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasSharedCost
	bool ___hasSharedCost_17;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::sharedCost_
	PhoneNumberDesc_t922391174 * ___sharedCost__18;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasPersonalNumber
	bool ___hasPersonalNumber_20;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::personalNumber_
	PhoneNumberDesc_t922391174 * ___personalNumber__21;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasVoip
	bool ___hasVoip_23;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::voip_
	PhoneNumberDesc_t922391174 * ___voip__24;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasPager
	bool ___hasPager_26;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::pager_
	PhoneNumberDesc_t922391174 * ___pager__27;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasUan
	bool ___hasUan_29;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::uan_
	PhoneNumberDesc_t922391174 * ___uan__30;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasEmergency
	bool ___hasEmergency_32;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::emergency_
	PhoneNumberDesc_t922391174 * ___emergency__33;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasVoicemail
	bool ___hasVoicemail_35;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::voicemail_
	PhoneNumberDesc_t922391174 * ___voicemail__36;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasNoInternationalDialling
	bool ___hasNoInternationalDialling_38;
	// PhoneNumbers.PhoneNumberDesc PhoneNumbers.PhoneMetadata::noInternationalDialling_
	PhoneNumberDesc_t922391174 * ___noInternationalDialling__39;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasId
	bool ___hasId_41;
	// System.String PhoneNumbers.PhoneMetadata::id_
	String_t* ___id__42;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasCountryCode
	bool ___hasCountryCode_44;
	// System.Int32 PhoneNumbers.PhoneMetadata::countryCode_
	int32_t ___countryCode__45;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasInternationalPrefix
	bool ___hasInternationalPrefix_47;
	// System.String PhoneNumbers.PhoneMetadata::internationalPrefix_
	String_t* ___internationalPrefix__48;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasPreferredInternationalPrefix
	bool ___hasPreferredInternationalPrefix_50;
	// System.String PhoneNumbers.PhoneMetadata::preferredInternationalPrefix_
	String_t* ___preferredInternationalPrefix__51;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasNationalPrefix
	bool ___hasNationalPrefix_53;
	// System.String PhoneNumbers.PhoneMetadata::nationalPrefix_
	String_t* ___nationalPrefix__54;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasPreferredExtnPrefix
	bool ___hasPreferredExtnPrefix_56;
	// System.String PhoneNumbers.PhoneMetadata::preferredExtnPrefix_
	String_t* ___preferredExtnPrefix__57;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasNationalPrefixForParsing
	bool ___hasNationalPrefixForParsing_59;
	// System.String PhoneNumbers.PhoneMetadata::nationalPrefixForParsing_
	String_t* ___nationalPrefixForParsing__60;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasNationalPrefixTransformRule
	bool ___hasNationalPrefixTransformRule_62;
	// System.String PhoneNumbers.PhoneMetadata::nationalPrefixTransformRule_
	String_t* ___nationalPrefixTransformRule__63;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasSameMobileAndFixedLinePattern
	bool ___hasSameMobileAndFixedLinePattern_65;
	// System.Boolean PhoneNumbers.PhoneMetadata::sameMobileAndFixedLinePattern_
	bool ___sameMobileAndFixedLinePattern__66;
	// System.Collections.Generic.List`1<PhoneNumbers.NumberFormat> PhoneNumbers.PhoneMetadata::numberFormat_
	List_1_t4105827652 * ___numberFormat__68;
	// System.Collections.Generic.List`1<PhoneNumbers.NumberFormat> PhoneNumbers.PhoneMetadata::intlNumberFormat_
	List_1_t4105827652 * ___intlNumberFormat__70;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasMainCountryForCode
	bool ___hasMainCountryForCode_72;
	// System.Boolean PhoneNumbers.PhoneMetadata::mainCountryForCode_
	bool ___mainCountryForCode__73;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasLeadingDigits
	bool ___hasLeadingDigits_75;
	// System.String PhoneNumbers.PhoneMetadata::leadingDigits_
	String_t* ___leadingDigits__76;
	// System.Boolean PhoneNumbers.PhoneMetadata::hasLeadingZeroPossible
	bool ___hasLeadingZeroPossible_78;
	// System.Boolean PhoneNumbers.PhoneMetadata::leadingZeroPossible_
	bool ___leadingZeroPossible__79;

public:
	inline static int32_t get_offset_of_hasGeneralDesc_2() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasGeneralDesc_2)); }
	inline bool get_hasGeneralDesc_2() const { return ___hasGeneralDesc_2; }
	inline bool* get_address_of_hasGeneralDesc_2() { return &___hasGeneralDesc_2; }
	inline void set_hasGeneralDesc_2(bool value)
	{
		___hasGeneralDesc_2 = value;
	}

	inline static int32_t get_offset_of_generalDesc__3() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___generalDesc__3)); }
	inline PhoneNumberDesc_t922391174 * get_generalDesc__3() const { return ___generalDesc__3; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_generalDesc__3() { return &___generalDesc__3; }
	inline void set_generalDesc__3(PhoneNumberDesc_t922391174 * value)
	{
		___generalDesc__3 = value;
		Il2CppCodeGenWriteBarrier(&___generalDesc__3, value);
	}

	inline static int32_t get_offset_of_hasFixedLine_5() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasFixedLine_5)); }
	inline bool get_hasFixedLine_5() const { return ___hasFixedLine_5; }
	inline bool* get_address_of_hasFixedLine_5() { return &___hasFixedLine_5; }
	inline void set_hasFixedLine_5(bool value)
	{
		___hasFixedLine_5 = value;
	}

	inline static int32_t get_offset_of_fixedLine__6() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___fixedLine__6)); }
	inline PhoneNumberDesc_t922391174 * get_fixedLine__6() const { return ___fixedLine__6; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_fixedLine__6() { return &___fixedLine__6; }
	inline void set_fixedLine__6(PhoneNumberDesc_t922391174 * value)
	{
		___fixedLine__6 = value;
		Il2CppCodeGenWriteBarrier(&___fixedLine__6, value);
	}

	inline static int32_t get_offset_of_hasMobile_8() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasMobile_8)); }
	inline bool get_hasMobile_8() const { return ___hasMobile_8; }
	inline bool* get_address_of_hasMobile_8() { return &___hasMobile_8; }
	inline void set_hasMobile_8(bool value)
	{
		___hasMobile_8 = value;
	}

	inline static int32_t get_offset_of_mobile__9() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___mobile__9)); }
	inline PhoneNumberDesc_t922391174 * get_mobile__9() const { return ___mobile__9; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_mobile__9() { return &___mobile__9; }
	inline void set_mobile__9(PhoneNumberDesc_t922391174 * value)
	{
		___mobile__9 = value;
		Il2CppCodeGenWriteBarrier(&___mobile__9, value);
	}

	inline static int32_t get_offset_of_hasTollFree_11() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasTollFree_11)); }
	inline bool get_hasTollFree_11() const { return ___hasTollFree_11; }
	inline bool* get_address_of_hasTollFree_11() { return &___hasTollFree_11; }
	inline void set_hasTollFree_11(bool value)
	{
		___hasTollFree_11 = value;
	}

	inline static int32_t get_offset_of_tollFree__12() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___tollFree__12)); }
	inline PhoneNumberDesc_t922391174 * get_tollFree__12() const { return ___tollFree__12; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_tollFree__12() { return &___tollFree__12; }
	inline void set_tollFree__12(PhoneNumberDesc_t922391174 * value)
	{
		___tollFree__12 = value;
		Il2CppCodeGenWriteBarrier(&___tollFree__12, value);
	}

	inline static int32_t get_offset_of_hasPremiumRate_14() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasPremiumRate_14)); }
	inline bool get_hasPremiumRate_14() const { return ___hasPremiumRate_14; }
	inline bool* get_address_of_hasPremiumRate_14() { return &___hasPremiumRate_14; }
	inline void set_hasPremiumRate_14(bool value)
	{
		___hasPremiumRate_14 = value;
	}

	inline static int32_t get_offset_of_premiumRate__15() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___premiumRate__15)); }
	inline PhoneNumberDesc_t922391174 * get_premiumRate__15() const { return ___premiumRate__15; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_premiumRate__15() { return &___premiumRate__15; }
	inline void set_premiumRate__15(PhoneNumberDesc_t922391174 * value)
	{
		___premiumRate__15 = value;
		Il2CppCodeGenWriteBarrier(&___premiumRate__15, value);
	}

	inline static int32_t get_offset_of_hasSharedCost_17() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasSharedCost_17)); }
	inline bool get_hasSharedCost_17() const { return ___hasSharedCost_17; }
	inline bool* get_address_of_hasSharedCost_17() { return &___hasSharedCost_17; }
	inline void set_hasSharedCost_17(bool value)
	{
		___hasSharedCost_17 = value;
	}

	inline static int32_t get_offset_of_sharedCost__18() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___sharedCost__18)); }
	inline PhoneNumberDesc_t922391174 * get_sharedCost__18() const { return ___sharedCost__18; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_sharedCost__18() { return &___sharedCost__18; }
	inline void set_sharedCost__18(PhoneNumberDesc_t922391174 * value)
	{
		___sharedCost__18 = value;
		Il2CppCodeGenWriteBarrier(&___sharedCost__18, value);
	}

	inline static int32_t get_offset_of_hasPersonalNumber_20() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasPersonalNumber_20)); }
	inline bool get_hasPersonalNumber_20() const { return ___hasPersonalNumber_20; }
	inline bool* get_address_of_hasPersonalNumber_20() { return &___hasPersonalNumber_20; }
	inline void set_hasPersonalNumber_20(bool value)
	{
		___hasPersonalNumber_20 = value;
	}

	inline static int32_t get_offset_of_personalNumber__21() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___personalNumber__21)); }
	inline PhoneNumberDesc_t922391174 * get_personalNumber__21() const { return ___personalNumber__21; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_personalNumber__21() { return &___personalNumber__21; }
	inline void set_personalNumber__21(PhoneNumberDesc_t922391174 * value)
	{
		___personalNumber__21 = value;
		Il2CppCodeGenWriteBarrier(&___personalNumber__21, value);
	}

	inline static int32_t get_offset_of_hasVoip_23() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasVoip_23)); }
	inline bool get_hasVoip_23() const { return ___hasVoip_23; }
	inline bool* get_address_of_hasVoip_23() { return &___hasVoip_23; }
	inline void set_hasVoip_23(bool value)
	{
		___hasVoip_23 = value;
	}

	inline static int32_t get_offset_of_voip__24() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___voip__24)); }
	inline PhoneNumberDesc_t922391174 * get_voip__24() const { return ___voip__24; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_voip__24() { return &___voip__24; }
	inline void set_voip__24(PhoneNumberDesc_t922391174 * value)
	{
		___voip__24 = value;
		Il2CppCodeGenWriteBarrier(&___voip__24, value);
	}

	inline static int32_t get_offset_of_hasPager_26() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasPager_26)); }
	inline bool get_hasPager_26() const { return ___hasPager_26; }
	inline bool* get_address_of_hasPager_26() { return &___hasPager_26; }
	inline void set_hasPager_26(bool value)
	{
		___hasPager_26 = value;
	}

	inline static int32_t get_offset_of_pager__27() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___pager__27)); }
	inline PhoneNumberDesc_t922391174 * get_pager__27() const { return ___pager__27; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_pager__27() { return &___pager__27; }
	inline void set_pager__27(PhoneNumberDesc_t922391174 * value)
	{
		___pager__27 = value;
		Il2CppCodeGenWriteBarrier(&___pager__27, value);
	}

	inline static int32_t get_offset_of_hasUan_29() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasUan_29)); }
	inline bool get_hasUan_29() const { return ___hasUan_29; }
	inline bool* get_address_of_hasUan_29() { return &___hasUan_29; }
	inline void set_hasUan_29(bool value)
	{
		___hasUan_29 = value;
	}

	inline static int32_t get_offset_of_uan__30() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___uan__30)); }
	inline PhoneNumberDesc_t922391174 * get_uan__30() const { return ___uan__30; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_uan__30() { return &___uan__30; }
	inline void set_uan__30(PhoneNumberDesc_t922391174 * value)
	{
		___uan__30 = value;
		Il2CppCodeGenWriteBarrier(&___uan__30, value);
	}

	inline static int32_t get_offset_of_hasEmergency_32() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasEmergency_32)); }
	inline bool get_hasEmergency_32() const { return ___hasEmergency_32; }
	inline bool* get_address_of_hasEmergency_32() { return &___hasEmergency_32; }
	inline void set_hasEmergency_32(bool value)
	{
		___hasEmergency_32 = value;
	}

	inline static int32_t get_offset_of_emergency__33() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___emergency__33)); }
	inline PhoneNumberDesc_t922391174 * get_emergency__33() const { return ___emergency__33; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_emergency__33() { return &___emergency__33; }
	inline void set_emergency__33(PhoneNumberDesc_t922391174 * value)
	{
		___emergency__33 = value;
		Il2CppCodeGenWriteBarrier(&___emergency__33, value);
	}

	inline static int32_t get_offset_of_hasVoicemail_35() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasVoicemail_35)); }
	inline bool get_hasVoicemail_35() const { return ___hasVoicemail_35; }
	inline bool* get_address_of_hasVoicemail_35() { return &___hasVoicemail_35; }
	inline void set_hasVoicemail_35(bool value)
	{
		___hasVoicemail_35 = value;
	}

	inline static int32_t get_offset_of_voicemail__36() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___voicemail__36)); }
	inline PhoneNumberDesc_t922391174 * get_voicemail__36() const { return ___voicemail__36; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_voicemail__36() { return &___voicemail__36; }
	inline void set_voicemail__36(PhoneNumberDesc_t922391174 * value)
	{
		___voicemail__36 = value;
		Il2CppCodeGenWriteBarrier(&___voicemail__36, value);
	}

	inline static int32_t get_offset_of_hasNoInternationalDialling_38() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasNoInternationalDialling_38)); }
	inline bool get_hasNoInternationalDialling_38() const { return ___hasNoInternationalDialling_38; }
	inline bool* get_address_of_hasNoInternationalDialling_38() { return &___hasNoInternationalDialling_38; }
	inline void set_hasNoInternationalDialling_38(bool value)
	{
		___hasNoInternationalDialling_38 = value;
	}

	inline static int32_t get_offset_of_noInternationalDialling__39() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___noInternationalDialling__39)); }
	inline PhoneNumberDesc_t922391174 * get_noInternationalDialling__39() const { return ___noInternationalDialling__39; }
	inline PhoneNumberDesc_t922391174 ** get_address_of_noInternationalDialling__39() { return &___noInternationalDialling__39; }
	inline void set_noInternationalDialling__39(PhoneNumberDesc_t922391174 * value)
	{
		___noInternationalDialling__39 = value;
		Il2CppCodeGenWriteBarrier(&___noInternationalDialling__39, value);
	}

	inline static int32_t get_offset_of_hasId_41() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasId_41)); }
	inline bool get_hasId_41() const { return ___hasId_41; }
	inline bool* get_address_of_hasId_41() { return &___hasId_41; }
	inline void set_hasId_41(bool value)
	{
		___hasId_41 = value;
	}

	inline static int32_t get_offset_of_id__42() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___id__42)); }
	inline String_t* get_id__42() const { return ___id__42; }
	inline String_t** get_address_of_id__42() { return &___id__42; }
	inline void set_id__42(String_t* value)
	{
		___id__42 = value;
		Il2CppCodeGenWriteBarrier(&___id__42, value);
	}

	inline static int32_t get_offset_of_hasCountryCode_44() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasCountryCode_44)); }
	inline bool get_hasCountryCode_44() const { return ___hasCountryCode_44; }
	inline bool* get_address_of_hasCountryCode_44() { return &___hasCountryCode_44; }
	inline void set_hasCountryCode_44(bool value)
	{
		___hasCountryCode_44 = value;
	}

	inline static int32_t get_offset_of_countryCode__45() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___countryCode__45)); }
	inline int32_t get_countryCode__45() const { return ___countryCode__45; }
	inline int32_t* get_address_of_countryCode__45() { return &___countryCode__45; }
	inline void set_countryCode__45(int32_t value)
	{
		___countryCode__45 = value;
	}

	inline static int32_t get_offset_of_hasInternationalPrefix_47() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasInternationalPrefix_47)); }
	inline bool get_hasInternationalPrefix_47() const { return ___hasInternationalPrefix_47; }
	inline bool* get_address_of_hasInternationalPrefix_47() { return &___hasInternationalPrefix_47; }
	inline void set_hasInternationalPrefix_47(bool value)
	{
		___hasInternationalPrefix_47 = value;
	}

	inline static int32_t get_offset_of_internationalPrefix__48() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___internationalPrefix__48)); }
	inline String_t* get_internationalPrefix__48() const { return ___internationalPrefix__48; }
	inline String_t** get_address_of_internationalPrefix__48() { return &___internationalPrefix__48; }
	inline void set_internationalPrefix__48(String_t* value)
	{
		___internationalPrefix__48 = value;
		Il2CppCodeGenWriteBarrier(&___internationalPrefix__48, value);
	}

	inline static int32_t get_offset_of_hasPreferredInternationalPrefix_50() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasPreferredInternationalPrefix_50)); }
	inline bool get_hasPreferredInternationalPrefix_50() const { return ___hasPreferredInternationalPrefix_50; }
	inline bool* get_address_of_hasPreferredInternationalPrefix_50() { return &___hasPreferredInternationalPrefix_50; }
	inline void set_hasPreferredInternationalPrefix_50(bool value)
	{
		___hasPreferredInternationalPrefix_50 = value;
	}

	inline static int32_t get_offset_of_preferredInternationalPrefix__51() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___preferredInternationalPrefix__51)); }
	inline String_t* get_preferredInternationalPrefix__51() const { return ___preferredInternationalPrefix__51; }
	inline String_t** get_address_of_preferredInternationalPrefix__51() { return &___preferredInternationalPrefix__51; }
	inline void set_preferredInternationalPrefix__51(String_t* value)
	{
		___preferredInternationalPrefix__51 = value;
		Il2CppCodeGenWriteBarrier(&___preferredInternationalPrefix__51, value);
	}

	inline static int32_t get_offset_of_hasNationalPrefix_53() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasNationalPrefix_53)); }
	inline bool get_hasNationalPrefix_53() const { return ___hasNationalPrefix_53; }
	inline bool* get_address_of_hasNationalPrefix_53() { return &___hasNationalPrefix_53; }
	inline void set_hasNationalPrefix_53(bool value)
	{
		___hasNationalPrefix_53 = value;
	}

	inline static int32_t get_offset_of_nationalPrefix__54() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___nationalPrefix__54)); }
	inline String_t* get_nationalPrefix__54() const { return ___nationalPrefix__54; }
	inline String_t** get_address_of_nationalPrefix__54() { return &___nationalPrefix__54; }
	inline void set_nationalPrefix__54(String_t* value)
	{
		___nationalPrefix__54 = value;
		Il2CppCodeGenWriteBarrier(&___nationalPrefix__54, value);
	}

	inline static int32_t get_offset_of_hasPreferredExtnPrefix_56() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasPreferredExtnPrefix_56)); }
	inline bool get_hasPreferredExtnPrefix_56() const { return ___hasPreferredExtnPrefix_56; }
	inline bool* get_address_of_hasPreferredExtnPrefix_56() { return &___hasPreferredExtnPrefix_56; }
	inline void set_hasPreferredExtnPrefix_56(bool value)
	{
		___hasPreferredExtnPrefix_56 = value;
	}

	inline static int32_t get_offset_of_preferredExtnPrefix__57() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___preferredExtnPrefix__57)); }
	inline String_t* get_preferredExtnPrefix__57() const { return ___preferredExtnPrefix__57; }
	inline String_t** get_address_of_preferredExtnPrefix__57() { return &___preferredExtnPrefix__57; }
	inline void set_preferredExtnPrefix__57(String_t* value)
	{
		___preferredExtnPrefix__57 = value;
		Il2CppCodeGenWriteBarrier(&___preferredExtnPrefix__57, value);
	}

	inline static int32_t get_offset_of_hasNationalPrefixForParsing_59() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasNationalPrefixForParsing_59)); }
	inline bool get_hasNationalPrefixForParsing_59() const { return ___hasNationalPrefixForParsing_59; }
	inline bool* get_address_of_hasNationalPrefixForParsing_59() { return &___hasNationalPrefixForParsing_59; }
	inline void set_hasNationalPrefixForParsing_59(bool value)
	{
		___hasNationalPrefixForParsing_59 = value;
	}

	inline static int32_t get_offset_of_nationalPrefixForParsing__60() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___nationalPrefixForParsing__60)); }
	inline String_t* get_nationalPrefixForParsing__60() const { return ___nationalPrefixForParsing__60; }
	inline String_t** get_address_of_nationalPrefixForParsing__60() { return &___nationalPrefixForParsing__60; }
	inline void set_nationalPrefixForParsing__60(String_t* value)
	{
		___nationalPrefixForParsing__60 = value;
		Il2CppCodeGenWriteBarrier(&___nationalPrefixForParsing__60, value);
	}

	inline static int32_t get_offset_of_hasNationalPrefixTransformRule_62() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasNationalPrefixTransformRule_62)); }
	inline bool get_hasNationalPrefixTransformRule_62() const { return ___hasNationalPrefixTransformRule_62; }
	inline bool* get_address_of_hasNationalPrefixTransformRule_62() { return &___hasNationalPrefixTransformRule_62; }
	inline void set_hasNationalPrefixTransformRule_62(bool value)
	{
		___hasNationalPrefixTransformRule_62 = value;
	}

	inline static int32_t get_offset_of_nationalPrefixTransformRule__63() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___nationalPrefixTransformRule__63)); }
	inline String_t* get_nationalPrefixTransformRule__63() const { return ___nationalPrefixTransformRule__63; }
	inline String_t** get_address_of_nationalPrefixTransformRule__63() { return &___nationalPrefixTransformRule__63; }
	inline void set_nationalPrefixTransformRule__63(String_t* value)
	{
		___nationalPrefixTransformRule__63 = value;
		Il2CppCodeGenWriteBarrier(&___nationalPrefixTransformRule__63, value);
	}

	inline static int32_t get_offset_of_hasSameMobileAndFixedLinePattern_65() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasSameMobileAndFixedLinePattern_65)); }
	inline bool get_hasSameMobileAndFixedLinePattern_65() const { return ___hasSameMobileAndFixedLinePattern_65; }
	inline bool* get_address_of_hasSameMobileAndFixedLinePattern_65() { return &___hasSameMobileAndFixedLinePattern_65; }
	inline void set_hasSameMobileAndFixedLinePattern_65(bool value)
	{
		___hasSameMobileAndFixedLinePattern_65 = value;
	}

	inline static int32_t get_offset_of_sameMobileAndFixedLinePattern__66() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___sameMobileAndFixedLinePattern__66)); }
	inline bool get_sameMobileAndFixedLinePattern__66() const { return ___sameMobileAndFixedLinePattern__66; }
	inline bool* get_address_of_sameMobileAndFixedLinePattern__66() { return &___sameMobileAndFixedLinePattern__66; }
	inline void set_sameMobileAndFixedLinePattern__66(bool value)
	{
		___sameMobileAndFixedLinePattern__66 = value;
	}

	inline static int32_t get_offset_of_numberFormat__68() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___numberFormat__68)); }
	inline List_1_t4105827652 * get_numberFormat__68() const { return ___numberFormat__68; }
	inline List_1_t4105827652 ** get_address_of_numberFormat__68() { return &___numberFormat__68; }
	inline void set_numberFormat__68(List_1_t4105827652 * value)
	{
		___numberFormat__68 = value;
		Il2CppCodeGenWriteBarrier(&___numberFormat__68, value);
	}

	inline static int32_t get_offset_of_intlNumberFormat__70() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___intlNumberFormat__70)); }
	inline List_1_t4105827652 * get_intlNumberFormat__70() const { return ___intlNumberFormat__70; }
	inline List_1_t4105827652 ** get_address_of_intlNumberFormat__70() { return &___intlNumberFormat__70; }
	inline void set_intlNumberFormat__70(List_1_t4105827652 * value)
	{
		___intlNumberFormat__70 = value;
		Il2CppCodeGenWriteBarrier(&___intlNumberFormat__70, value);
	}

	inline static int32_t get_offset_of_hasMainCountryForCode_72() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasMainCountryForCode_72)); }
	inline bool get_hasMainCountryForCode_72() const { return ___hasMainCountryForCode_72; }
	inline bool* get_address_of_hasMainCountryForCode_72() { return &___hasMainCountryForCode_72; }
	inline void set_hasMainCountryForCode_72(bool value)
	{
		___hasMainCountryForCode_72 = value;
	}

	inline static int32_t get_offset_of_mainCountryForCode__73() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___mainCountryForCode__73)); }
	inline bool get_mainCountryForCode__73() const { return ___mainCountryForCode__73; }
	inline bool* get_address_of_mainCountryForCode__73() { return &___mainCountryForCode__73; }
	inline void set_mainCountryForCode__73(bool value)
	{
		___mainCountryForCode__73 = value;
	}

	inline static int32_t get_offset_of_hasLeadingDigits_75() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasLeadingDigits_75)); }
	inline bool get_hasLeadingDigits_75() const { return ___hasLeadingDigits_75; }
	inline bool* get_address_of_hasLeadingDigits_75() { return &___hasLeadingDigits_75; }
	inline void set_hasLeadingDigits_75(bool value)
	{
		___hasLeadingDigits_75 = value;
	}

	inline static int32_t get_offset_of_leadingDigits__76() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___leadingDigits__76)); }
	inline String_t* get_leadingDigits__76() const { return ___leadingDigits__76; }
	inline String_t** get_address_of_leadingDigits__76() { return &___leadingDigits__76; }
	inline void set_leadingDigits__76(String_t* value)
	{
		___leadingDigits__76 = value;
		Il2CppCodeGenWriteBarrier(&___leadingDigits__76, value);
	}

	inline static int32_t get_offset_of_hasLeadingZeroPossible_78() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___hasLeadingZeroPossible_78)); }
	inline bool get_hasLeadingZeroPossible_78() const { return ___hasLeadingZeroPossible_78; }
	inline bool* get_address_of_hasLeadingZeroPossible_78() { return &___hasLeadingZeroPossible_78; }
	inline void set_hasLeadingZeroPossible_78(bool value)
	{
		___hasLeadingZeroPossible_78 = value;
	}

	inline static int32_t get_offset_of_leadingZeroPossible__79() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403, ___leadingZeroPossible__79)); }
	inline bool get_leadingZeroPossible__79() const { return ___leadingZeroPossible__79; }
	inline bool* get_address_of_leadingZeroPossible__79() { return &___leadingZeroPossible__79; }
	inline void set_leadingZeroPossible__79(bool value)
	{
		___leadingZeroPossible__79 = value;
	}
};

struct PhoneMetadata_t366861403_StaticFields
{
public:
	// PhoneNumbers.PhoneMetadata PhoneNumbers.PhoneMetadata::defaultInstance
	PhoneMetadata_t366861403 * ___defaultInstance_0;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(PhoneMetadata_t366861403_StaticFields, ___defaultInstance_0)); }
	inline PhoneMetadata_t366861403 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline PhoneMetadata_t366861403 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(PhoneMetadata_t366861403 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
