﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptionMethod
struct EncryptionMethod_t718466202;
// System.String
struct String_t;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.EncryptionMethod::.ctor()
extern "C"  void EncryptionMethod__ctor_m3114029894 (EncryptionMethod_t718466202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionMethod::.ctor(System.String)
extern "C"  void EncryptionMethod__ctor_m3203118972 (EncryptionMethod_t718466202 * __this, String_t* ___strAlgorithm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptionMethod::get_KeyAlgorithm()
extern "C"  String_t* EncryptionMethod_get_KeyAlgorithm_m4122664778 (EncryptionMethod_t718466202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionMethod::set_KeyAlgorithm(System.String)
extern "C"  void EncryptionMethod_set_KeyAlgorithm_m2067235869 (EncryptionMethod_t718466202 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.Xml.EncryptionMethod::get_KeySize()
extern "C"  int32_t EncryptionMethod_get_KeySize_m455652171 (EncryptionMethod_t718466202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionMethod::set_KeySize(System.Int32)
extern "C"  void EncryptionMethod_set_KeySize_m4272865346 (EncryptionMethod_t718466202 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptionMethod::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * EncryptionMethod_GetXml_m1312620955 (EncryptionMethod_t718466202 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionMethod::LoadXml(System.Xml.XmlElement)
extern "C"  void EncryptionMethod_LoadXml_m3374509974 (EncryptionMethod_t718466202 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
