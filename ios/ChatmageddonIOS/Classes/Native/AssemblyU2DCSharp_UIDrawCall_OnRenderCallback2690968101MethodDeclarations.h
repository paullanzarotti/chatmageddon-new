﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t2690968101;
// System.Object
struct Il2CppObject;
// UnityEngine.Material
struct Material_t193706927;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UIDrawCall/OnRenderCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnRenderCallback__ctor_m1758414226 (OnRenderCallback_t2690968101 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCall/OnRenderCallback::Invoke(UnityEngine.Material)
extern "C"  void OnRenderCallback_Invoke_m1034177226 (OnRenderCallback_t2690968101 * __this, Material_t193706927 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UIDrawCall/OnRenderCallback::BeginInvoke(UnityEngine.Material,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnRenderCallback_BeginInvoke_m1029584755 (OnRenderCallback_t2690968101 * __this, Material_t193706927 * ___mat0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCall/OnRenderCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnRenderCallback_EndInvoke_m2447804168 (OnRenderCallback_t2690968101 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
