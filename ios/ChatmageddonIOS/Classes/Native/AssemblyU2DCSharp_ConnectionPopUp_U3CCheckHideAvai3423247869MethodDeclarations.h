﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConnectionPopUp/<CheckHideAvailable>c__Iterator0
struct U3CCheckHideAvailableU3Ec__Iterator0_t3423247869;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ConnectionPopUp/<CheckHideAvailable>c__Iterator0::.ctor()
extern "C"  void U3CCheckHideAvailableU3Ec__Iterator0__ctor_m423408022 (U3CCheckHideAvailableU3Ec__Iterator0_t3423247869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConnectionPopUp/<CheckHideAvailable>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckHideAvailableU3Ec__Iterator0_MoveNext_m2168077250 (U3CCheckHideAvailableU3Ec__Iterator0_t3423247869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConnectionPopUp/<CheckHideAvailable>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckHideAvailableU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3347002742 (U3CCheckHideAvailableU3Ec__Iterator0_t3423247869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ConnectionPopUp/<CheckHideAvailable>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckHideAvailableU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1546919086 (U3CCheckHideAvailableU3Ec__Iterator0_t3423247869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp/<CheckHideAvailable>c__Iterator0::Dispose()
extern "C"  void U3CCheckHideAvailableU3Ec__Iterator0_Dispose_m808920887 (U3CCheckHideAvailableU3Ec__Iterator0_t3423247869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConnectionPopUp/<CheckHideAvailable>c__Iterator0::Reset()
extern "C"  void U3CCheckHideAvailableU3Ec__Iterator0_Reset_m1257808589 (U3CCheckHideAvailableU3Ec__Iterator0_t3423247869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
