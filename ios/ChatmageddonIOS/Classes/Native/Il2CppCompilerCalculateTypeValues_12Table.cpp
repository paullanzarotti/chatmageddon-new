﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlException4188277960.h"
#include "System_Xml_System_Xml_XmlImplementation1664517635.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"
#include "System_Xml_System_Xml_XmlDateTimeSerializationMode137774893.h"
#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"
#include "System_Xml_System_Xml_XmlNameEntry3745551716.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3855584002.h"
#include "System_Xml_System_Xml_XmlNameTable1345805268.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1718403287.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs4036174778.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"
#include "System_Xml_System_Xml_XmlNodeListChildren2811458520.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerato569056069.h"
#include "System_Xml_System_Xml_XmlNodeArrayList2454560084.h"
#include "System_Xml_System_Xml_XmlIteratorNodeList4166547161.h"
#include "System_Xml_System_Xml_XmlIteratorNodeList_XPathNod2835107459.h"
#include "System_Xml_System_Xml_XmlNodeOrder930453011.h"
#include "System_Xml_System_Xml_XmlNodeReader1022603664.h"
#include "System_Xml_System_Xml_XmlNodeReaderImpl2982135230.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlOutputMethod2267235953.h"
#include "System_Xml_System_Xml_XmlParserContext2728039553.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem1262420678.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction431557540.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport1548133672.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1644897369.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG1955031820.h"
#include "System_Xml_System_Xml_XmlReaderSettings1578612233.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "System_Xml_System_Xml_XmlSecureResolver2018216936.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1224054391.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"
#include "System_Xml_System_Xml_XmlText4111601336.h"
#include "System_Xml_Mono_Xml2_XmlTextReader511376973.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo254587324.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeTok3353594030.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3313602765.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt3023928423.h"
#include "System_Xml_System_Xml_XmlTextReader3514170725.h"
#include "System_Xml_System_Xml_XmlTokenizedType1619571710.h"
#include "System_Xml_System_Xml_XmlUrlResolver896669594.h"
#include "System_Xml_System_Xml_XmlValidatingReader3416770767.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_XmlWriterSettings924210539.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo3709371029.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil2068578019.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState3530111136.h"
#include "System_Xml_System_Xml_XmlStreamReader2725532304.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader3963211903.h"
#include "System_Xml_System_Xml_XmlInputStream2650744719.h"
#include "System_Xml_System_Xml_XmlParserInput2366782760.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInputS25800784.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator3981235968.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_Enumera1602910416.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator_U3CEnum1235609798.h"
#include "System_Xml_System_Xml_XPath_XPathExpression452251917.h"
#include "System_Xml_System_Xml_XPath_XPathItem3130801258.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope3601604274.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator3192332357.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator_U3CGe959253998.h"
#include "System_Xml_System_Xml_XPath_XPathResultType1521569578.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType817388867.h"
#include "System_Xml_System_Xml_XPath_XmlDataType315095065.h"
#include "System_Xml_System_Xml_XPath_XPathException1503722168.h"
#include "System_Xml_System_Xml_XPath_XPathIteratorComparer2522471076.h"
#include "System_Xml_System_Xml_XPath_XPathNavigatorComparer2670709129.h"
#include "System_Xml_System_Xml_XPath_XPathFunctions976633782.h"
#include "System_Xml_System_Xml_XPath_XPathFunction759167395.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLast2734600875.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionPosition3711683936.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionCount711123082.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionId2998162272.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionLocalName3935594891.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNamespaceU448035136.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionName1880977850.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionString3963481524.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionConcat2913460711.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStartsWit1855488236.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionContains4033230314.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1797727619.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring1560316700.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionSubstring2124841904.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionStringLen2821584396.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNormalize1288231156.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionTranslate2720664953.h"
#include "System_Xml_System_Xml_XPath_XPathBooleanFunction2511646183.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionBoolean2454599187.h"
#include "System_Xml_System_Xml_XPath_XPathFunctionNot1631674504.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1200[5] = 
{
	XmlException_t4188277960::get_offset_of_lineNumber_11(),
	XmlException_t4188277960::get_offset_of_linePosition_12(),
	XmlException_t4188277960::get_offset_of_sourceUri_13(),
	XmlException_t4188277960::get_offset_of_res_14(),
	XmlException_t4188277960::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1201[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1202[8] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1936105738_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_U3CU3Ef__switchU24map33_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (XmlDateTimeSerializationMode_t137774893)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1203[5] = 
{
	XmlDateTimeSerializationMode_t137774893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1204[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (XmlNameEntry_t3745551716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1205[5] = 
{
	XmlNameEntry_t3745551716::get_offset_of_Prefix_0(),
	XmlNameEntry_t3745551716::get_offset_of_LocalName_1(),
	XmlNameEntry_t3745551716::get_offset_of_NS_2(),
	XmlNameEntry_t3745551716::get_offset_of_Hash_3(),
	XmlNameEntry_t3745551716::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (XmlNameEntryCache_t3855584002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1206[4] = 
{
	XmlNameEntryCache_t3855584002::get_offset_of_table_0(),
	XmlNameEntryCache_t3855584002::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3855584002::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3855584002::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (XmlNamedNodeMap_t145210370), -1, sizeof(XmlNamedNodeMap_t145210370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1208[4] = 
{
	XmlNamedNodeMap_t145210370_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t145210370::get_offset_of_parent_1(),
	XmlNamedNodeMap_t145210370::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t145210370::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (XmlNamespaceManager_t486731501), -1, sizeof(XmlNamespaceManager_t486731501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1209[9] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_decls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_declPos_1(),
	XmlNamespaceManager_t486731501::get_offset_of_scopes_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t486731501::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t486731501::get_offset_of_count_5(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t486731501::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t486731501_StaticFields::get_offset_of_U3CU3Ef__switchU24map25_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (NsDecl_t3210081295)+ sizeof (Il2CppObject), sizeof(NsDecl_t3210081295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1210[2] = 
{
	NsDecl_t3210081295::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3210081295::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (NsScope_t2513625351)+ sizeof (Il2CppObject), sizeof(NsScope_t2513625351_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1211[2] = 
{
	NsScope_t2513625351::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t2513625351::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (XmlNode_t616554813), -1, sizeof(XmlNode_t616554813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1212[5] = 
{
	XmlNode_t616554813_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t616554813::get_offset_of_ownerDocument_1(),
	XmlNode_t616554813::get_offset_of_parentNode_2(),
	XmlNode_t616554813::get_offset_of_childNodes_3(),
	XmlNode_t616554813_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (EmptyNodeList_t1718403287), -1, sizeof(EmptyNodeList_t1718403287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1213[1] = 
{
	EmptyNodeList_t1718403287_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (XmlNodeChangedAction_t1188489541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1214[4] = 
{
	XmlNodeChangedAction_t1188489541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (XmlNodeChangedEventArgs_t4036174778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1215[6] = 
{
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (XmlNodeList_t497326455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (XmlNodeListChildren_t2811458520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1217[1] = 
{
	XmlNodeListChildren_t2811458520::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (Enumerator_t569056069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1218[3] = 
{
	Enumerator_t569056069::get_offset_of_parent_0(),
	Enumerator_t569056069::get_offset_of_currentChild_1(),
	Enumerator_t569056069::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (XmlNodeArrayList_t2454560084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1219[1] = 
{
	XmlNodeArrayList_t2454560084::get_offset_of__rgNodes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (XmlIteratorNodeList_t4166547161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1220[4] = 
{
	XmlIteratorNodeList_t4166547161::get_offset_of_source_0(),
	XmlIteratorNodeList_t4166547161::get_offset_of_iterator_1(),
	XmlIteratorNodeList_t4166547161::get_offset_of_list_2(),
	XmlIteratorNodeList_t4166547161::get_offset_of_finished_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (XPathNodeIteratorNodeListIterator_t2835107459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1221[2] = 
{
	XPathNodeIteratorNodeListIterator_t2835107459::get_offset_of_iter_0(),
	XPathNodeIteratorNodeListIterator_t2835107459::get_offset_of_source_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (XmlNodeOrder_t930453011)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1222[5] = 
{
	XmlNodeOrder_t930453011::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (XmlNodeReader_t1022603664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1223[4] = 
{
	XmlNodeReader_t1022603664::get_offset_of_entity_3(),
	XmlNodeReader_t1022603664::get_offset_of_source_4(),
	XmlNodeReader_t1022603664::get_offset_of_entityInsideAttribute_5(),
	XmlNodeReader_t1022603664::get_offset_of_insideAttribute_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (XmlNodeReaderImpl_t2982135230), -1, sizeof(XmlNodeReaderImpl_t2982135230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1224[12] = 
{
	XmlNodeReaderImpl_t2982135230::get_offset_of_document_3(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_startNode_4(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_current_5(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_ownerLinkedNode_6(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_state_7(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_depth_8(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_isEndElement_9(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_ignoreStartNode_10(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map34_11(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map35_12(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map36_13(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map37_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (XmlNodeType_t739504597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1225[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (XmlOutputMethod_t2267235953)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1226[5] = 
{
	XmlOutputMethod_t2267235953::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (XmlParserContext_t2728039553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1227[13] = 
{
	XmlParserContext_t2728039553::get_offset_of_baseURI_0(),
	XmlParserContext_t2728039553::get_offset_of_docTypeName_1(),
	XmlParserContext_t2728039553::get_offset_of_encoding_2(),
	XmlParserContext_t2728039553::get_offset_of_internalSubset_3(),
	XmlParserContext_t2728039553::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2728039553::get_offset_of_nameTable_5(),
	XmlParserContext_t2728039553::get_offset_of_publicID_6(),
	XmlParserContext_t2728039553::get_offset_of_systemID_7(),
	XmlParserContext_t2728039553::get_offset_of_xmlLang_8(),
	XmlParserContext_t2728039553::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2728039553::get_offset_of_contextItems_10(),
	XmlParserContext_t2728039553::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2728039553::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (ContextItem_t1262420678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1228[3] = 
{
	ContextItem_t1262420678::get_offset_of_BaseURI_0(),
	ContextItem_t1262420678::get_offset_of_XmlLang_1(),
	ContextItem_t1262420678::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (XmlProcessingInstruction_t431557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1229[2] = 
{
	XmlProcessingInstruction_t431557540::get_offset_of_target_6(),
	XmlProcessingInstruction_t431557540::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1230[4] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (XmlReader_t3675626668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1231[3] = 
{
	XmlReader_t3675626668::get_offset_of_readStringBuffer_0(),
	XmlReader_t3675626668::get_offset_of_binary_1(),
	XmlReader_t3675626668::get_offset_of_settings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (XmlReaderBinarySupport_t1548133672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1232[5] = 
{
	XmlReaderBinarySupport_t1548133672::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_state_2(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (CommandState_t1644897369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1233[6] = 
{
	CommandState_t1644897369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (CharGetter_t1955031820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (XmlReaderSettings_t1578612233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1235[16] = 
{
	XmlReaderSettings_t1578612233::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t1578612233::get_offset_of_closeInput_1(),
	XmlReaderSettings_t1578612233::get_offset_of_conformance_2(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreComments_3(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreProcessingInstructions_4(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreWhitespace_5(),
	XmlReaderSettings_t1578612233::get_offset_of_lineNumberOffset_6(),
	XmlReaderSettings_t1578612233::get_offset_of_linePositionOffset_7(),
	XmlReaderSettings_t1578612233::get_offset_of_prohibitDtd_8(),
	XmlReaderSettings_t1578612233::get_offset_of_nameTable_9(),
	XmlReaderSettings_t1578612233::get_offset_of_schemas_10(),
	XmlReaderSettings_t1578612233::get_offset_of_schemasNeedsInitialization_11(),
	XmlReaderSettings_t1578612233::get_offset_of_validationFlags_12(),
	XmlReaderSettings_t1578612233::get_offset_of_validationType_13(),
	XmlReaderSettings_t1578612233::get_offset_of_xmlResolver_14(),
	XmlReaderSettings_t1578612233::get_offset_of_ValidationEventHandler_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (XmlSecureResolver_t2018216936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1237[2] = 
{
	XmlSecureResolver_t2018216936::get_offset_of_resolver_0(),
	XmlSecureResolver_t2018216936::get_offset_of_permissionSet_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (XmlSignificantWhitespace_t1224054391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (XmlSpace_t2880376877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1239[4] = 
{
	XmlSpace_t2880376877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (XmlText_t4111601336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (XmlTextReader_t511376973), -1, sizeof(XmlTextReader_t511376973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1241[54] = 
{
	XmlTextReader_t511376973::get_offset_of_cursorToken_3(),
	XmlTextReader_t511376973::get_offset_of_currentToken_4(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeToken_5(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValueToken_6(),
	XmlTextReader_t511376973::get_offset_of_attributeTokens_7(),
	XmlTextReader_t511376973::get_offset_of_attributeValueTokens_8(),
	XmlTextReader_t511376973::get_offset_of_currentAttribute_9(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValue_10(),
	XmlTextReader_t511376973::get_offset_of_attributeCount_11(),
	XmlTextReader_t511376973::get_offset_of_parserContext_12(),
	XmlTextReader_t511376973::get_offset_of_nameTable_13(),
	XmlTextReader_t511376973::get_offset_of_nsmgr_14(),
	XmlTextReader_t511376973::get_offset_of_readState_15(),
	XmlTextReader_t511376973::get_offset_of_disallowReset_16(),
	XmlTextReader_t511376973::get_offset_of_depth_17(),
	XmlTextReader_t511376973::get_offset_of_elementDepth_18(),
	XmlTextReader_t511376973::get_offset_of_depthUp_19(),
	XmlTextReader_t511376973::get_offset_of_popScope_20(),
	XmlTextReader_t511376973::get_offset_of_elementNames_21(),
	XmlTextReader_t511376973::get_offset_of_elementNameStackPos_22(),
	XmlTextReader_t511376973::get_offset_of_allowMultipleRoot_23(),
	XmlTextReader_t511376973::get_offset_of_isStandalone_24(),
	XmlTextReader_t511376973::get_offset_of_returnEntityReference_25(),
	XmlTextReader_t511376973::get_offset_of_entityReferenceName_26(),
	XmlTextReader_t511376973::get_offset_of_valueBuffer_27(),
	XmlTextReader_t511376973::get_offset_of_reader_28(),
	XmlTextReader_t511376973::get_offset_of_peekChars_29(),
	XmlTextReader_t511376973::get_offset_of_peekCharsIndex_30(),
	XmlTextReader_t511376973::get_offset_of_peekCharsLength_31(),
	XmlTextReader_t511376973::get_offset_of_curNodePeekIndex_32(),
	XmlTextReader_t511376973::get_offset_of_preserveCurrentTag_33(),
	XmlTextReader_t511376973::get_offset_of_line_34(),
	XmlTextReader_t511376973::get_offset_of_column_35(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLineNumber_36(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLinePosition_37(),
	XmlTextReader_t511376973::get_offset_of_useProceedingLineInfo_38(),
	XmlTextReader_t511376973::get_offset_of_startNodeType_39(),
	XmlTextReader_t511376973::get_offset_of_currentState_40(),
	XmlTextReader_t511376973::get_offset_of_nestLevel_41(),
	XmlTextReader_t511376973::get_offset_of_readCharsInProgress_42(),
	XmlTextReader_t511376973::get_offset_of_binaryCharGetter_43(),
	XmlTextReader_t511376973::get_offset_of_namespaces_44(),
	XmlTextReader_t511376973::get_offset_of_whitespaceHandling_45(),
	XmlTextReader_t511376973::get_offset_of_resolver_46(),
	XmlTextReader_t511376973::get_offset_of_normalization_47(),
	XmlTextReader_t511376973::get_offset_of_checkCharacters_48(),
	XmlTextReader_t511376973::get_offset_of_prohibitDtd_49(),
	XmlTextReader_t511376973::get_offset_of_closeInput_50(),
	XmlTextReader_t511376973::get_offset_of_entityHandling_51(),
	XmlTextReader_t511376973::get_offset_of_whitespacePool_52(),
	XmlTextReader_t511376973::get_offset_of_whitespaceCache_53(),
	XmlTextReader_t511376973::get_offset_of_stateStack_54(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map38_55(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map39_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (XmlTokenInfo_t254587324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1242[13] = 
{
	XmlTokenInfo_t254587324::get_offset_of_valueCache_0(),
	XmlTokenInfo_t254587324::get_offset_of_Reader_1(),
	XmlTokenInfo_t254587324::get_offset_of_Name_2(),
	XmlTokenInfo_t254587324::get_offset_of_LocalName_3(),
	XmlTokenInfo_t254587324::get_offset_of_Prefix_4(),
	XmlTokenInfo_t254587324::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t254587324::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t254587324::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t254587324::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t254587324::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t254587324::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (XmlAttributeTokenInfo_t3353594030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1243[4] = 
{
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (TagName_t2340974457)+ sizeof (Il2CppObject), sizeof(TagName_t2340974457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1244[3] = 
{
	TagName_t2340974457::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (DtdInputState_t3313602765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1245[10] = 
{
	DtdInputState_t3313602765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (DtdInputStateStack_t3023928423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1246[1] = 
{
	DtdInputStateStack_t3023928423::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (XmlTextReader_t3514170725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1247[5] = 
{
	XmlTextReader_t3514170725::get_offset_of_entity_3(),
	XmlTextReader_t3514170725::get_offset_of_source_4(),
	XmlTextReader_t3514170725::get_offset_of_entityInsideAttribute_5(),
	XmlTextReader_t3514170725::get_offset_of_insideAttribute_6(),
	XmlTextReader_t3514170725::get_offset_of_entityNameStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1248[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (XmlUrlResolver_t896669594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1249[1] = 
{
	XmlUrlResolver_t896669594::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (XmlValidatingReader_t3416770767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1250[12] = 
{
	XmlValidatingReader_t3416770767::get_offset_of_entityHandling_3(),
	XmlValidatingReader_t3416770767::get_offset_of_sourceReader_4(),
	XmlValidatingReader_t3416770767::get_offset_of_xmlTextReader_5(),
	XmlValidatingReader_t3416770767::get_offset_of_validatingReader_6(),
	XmlValidatingReader_t3416770767::get_offset_of_resolver_7(),
	XmlValidatingReader_t3416770767::get_offset_of_resolverSpecified_8(),
	XmlValidatingReader_t3416770767::get_offset_of_validationType_9(),
	XmlValidatingReader_t3416770767::get_offset_of_schemas_10(),
	XmlValidatingReader_t3416770767::get_offset_of_dtdReader_11(),
	XmlValidatingReader_t3416770767::get_offset_of_schemaInfo_12(),
	XmlValidatingReader_t3416770767::get_offset_of_storedCharacters_13(),
	XmlValidatingReader_t3416770767::get_offset_of_ValidationEventHandler_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1252[1] = 
{
	XmlWriter_t1048088568::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (XmlWriterSettings_t924210539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1253[12] = 
{
	XmlWriterSettings_t924210539::get_offset_of_checkCharacters_0(),
	XmlWriterSettings_t924210539::get_offset_of_closeOutput_1(),
	XmlWriterSettings_t924210539::get_offset_of_conformance_2(),
	XmlWriterSettings_t924210539::get_offset_of_encoding_3(),
	XmlWriterSettings_t924210539::get_offset_of_indent_4(),
	XmlWriterSettings_t924210539::get_offset_of_indentChars_5(),
	XmlWriterSettings_t924210539::get_offset_of_newLineChars_6(),
	XmlWriterSettings_t924210539::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_t924210539::get_offset_of_newLineHandling_8(),
	XmlWriterSettings_t924210539::get_offset_of_omitXmlDeclaration_9(),
	XmlWriterSettings_t924210539::get_offset_of_outputMethod_10(),
	XmlWriterSettings_t924210539::get_offset_of_U3CNamespaceHandlingU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1254[35] = 
{
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_unmarked_utf8encoding_1(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_text_chars_2(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_attr_chars_3(),
	XmlTextWriter_t2527250655::get_offset_of_base_stream_4(),
	XmlTextWriter_t2527250655::get_offset_of_source_5(),
	XmlTextWriter_t2527250655::get_offset_of_writer_6(),
	XmlTextWriter_t2527250655::get_offset_of_preserver_7(),
	XmlTextWriter_t2527250655::get_offset_of_preserved_name_8(),
	XmlTextWriter_t2527250655::get_offset_of_is_preserved_xmlns_9(),
	XmlTextWriter_t2527250655::get_offset_of_allow_doc_fragment_10(),
	XmlTextWriter_t2527250655::get_offset_of_close_output_stream_11(),
	XmlTextWriter_t2527250655::get_offset_of_ignore_encoding_12(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_13(),
	XmlTextWriter_t2527250655::get_offset_of_xmldecl_state_14(),
	XmlTextWriter_t2527250655::get_offset_of_check_character_validity_15(),
	XmlTextWriter_t2527250655::get_offset_of_newline_handling_16(),
	XmlTextWriter_t2527250655::get_offset_of_is_document_entity_17(),
	XmlTextWriter_t2527250655::get_offset_of_state_18(),
	XmlTextWriter_t2527250655::get_offset_of_node_state_19(),
	XmlTextWriter_t2527250655::get_offset_of_nsmanager_20(),
	XmlTextWriter_t2527250655::get_offset_of_open_count_21(),
	XmlTextWriter_t2527250655::get_offset_of_elements_22(),
	XmlTextWriter_t2527250655::get_offset_of_new_local_namespaces_23(),
	XmlTextWriter_t2527250655::get_offset_of_explicit_nsdecls_24(),
	XmlTextWriter_t2527250655::get_offset_of_namespace_handling_25(),
	XmlTextWriter_t2527250655::get_offset_of_indent_26(),
	XmlTextWriter_t2527250655::get_offset_of_indent_count_27(),
	XmlTextWriter_t2527250655::get_offset_of_indent_char_28(),
	XmlTextWriter_t2527250655::get_offset_of_indent_string_29(),
	XmlTextWriter_t2527250655::get_offset_of_newline_30(),
	XmlTextWriter_t2527250655::get_offset_of_indent_attributes_31(),
	XmlTextWriter_t2527250655::get_offset_of_quote_char_32(),
	XmlTextWriter_t2527250655::get_offset_of_v2_33(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_34(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (XmlNodeInfo_t3709371029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1255[7] = 
{
	XmlNodeInfo_t3709371029::get_offset_of_Prefix_0(),
	XmlNodeInfo_t3709371029::get_offset_of_LocalName_1(),
	XmlNodeInfo_t3709371029::get_offset_of_NS_2(),
	XmlNodeInfo_t3709371029::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t3709371029::get_offset_of_HasElements_4(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (StringUtil_t2068578019), -1, sizeof(StringUtil_t2068578019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1256[2] = 
{
	StringUtil_t2068578019_StaticFields::get_offset_of_cul_0(),
	StringUtil_t2068578019_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (XmlDeclState_t3530111136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1257[5] = 
{
	XmlDeclState_t3530111136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1258[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_13(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1259[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_11(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1260[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_2(),
	XmlInputStream_t2650744719::get_offset_of_enc_3(),
	XmlInputStream_t2650744719::get_offset_of_stream_4(),
	XmlInputStream_t2650744719::get_offset_of_buffer_5(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_6(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_7(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (XmlParserInput_t2366782760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1261[5] = 
{
	XmlParserInput_t2366782760::get_offset_of_sourceStack_0(),
	XmlParserInput_t2366782760::get_offset_of_source_1(),
	XmlParserInput_t2366782760::get_offset_of_has_peek_2(),
	XmlParserInput_t2366782760::get_offset_of_peek_char_3(),
	XmlParserInput_t2366782760::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (XmlParserInputSource_t25800784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1262[6] = 
{
	XmlParserInputSource_t25800784::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t25800784::get_offset_of_reader_1(),
	XmlParserInputSource_t25800784::get_offset_of_state_2(),
	XmlParserInputSource_t25800784::get_offset_of_isPE_3(),
	XmlParserInputSource_t25800784::get_offset_of_line_4(),
	XmlParserInputSource_t25800784::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (XPathNavigator_t3981235968), -1, sizeof(XPathNavigator_t3981235968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1264[2] = 
{
	XPathNavigator_t3981235968_StaticFields::get_offset_of_escape_text_chars_0(),
	XPathNavigator_t3981235968_StaticFields::get_offset_of_escape_attr_chars_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (EnumerableIterator_t1602910416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1265[3] = 
{
	EnumerableIterator_t1602910416::get_offset_of_source_1(),
	EnumerableIterator_t1602910416::get_offset_of_e_2(),
	EnumerableIterator_t1602910416::get_offset_of_pos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (U3CEnumerateChildrenU3Ec__Iterator0_t1235609798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1266[8] = 
{
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_n_0(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3CnavU3E__0_1(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3Cnav2U3E__1_2(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_type_3(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U24PC_4(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U24current_5(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3CU24U3En_6(),
	U3CEnumerateChildrenU3Ec__Iterator0_t1235609798::get_offset_of_U3CU24U3Etype_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (XPathExpression_t452251917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (XPathItem_t3130801258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (XPathNamespaceScope_t3601604274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1269[4] = 
{
	XPathNamespaceScope_t3601604274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (XPathNodeIterator_t3192332357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1270[1] = 
{
	XPathNodeIterator_t3192332357::get_offset_of__count_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (U3CGetEnumeratorU3Ec__Iterator2_t959253998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1271[3] = 
{
	U3CGetEnumeratorU3Ec__Iterator2_t959253998::get_offset_of_U24PC_0(),
	U3CGetEnumeratorU3Ec__Iterator2_t959253998::get_offset_of_U24current_1(),
	U3CGetEnumeratorU3Ec__Iterator2_t959253998::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (XPathResultType_t1521569578)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1272[8] = 
{
	XPathResultType_t1521569578::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (XPathNodeType_t817388867)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1273[11] = 
{
	XPathNodeType_t817388867::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (XmlDataType_t315095065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1274[3] = 
{
	XmlDataType_t315095065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (XPathException_t1503722168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (XPathIteratorComparer_t2522471076), -1, sizeof(XPathIteratorComparer_t2522471076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1276[1] = 
{
	XPathIteratorComparer_t2522471076_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (XPathNavigatorComparer_t2670709129), -1, sizeof(XPathNavigatorComparer_t2670709129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1277[1] = 
{
	XPathNavigatorComparer_t2670709129_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (XPathFunctions_t976633782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (XPathFunction_t759167395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (XPathFunctionLast_t2734600875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (XPathFunctionPosition_t3711683936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (XPathFunctionCount_t711123082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1282[1] = 
{
	XPathFunctionCount_t711123082::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (XPathFunctionId_t2998162272), -1, sizeof(XPathFunctionId_t2998162272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1283[2] = 
{
	XPathFunctionId_t2998162272::get_offset_of_arg0_0(),
	XPathFunctionId_t2998162272_StaticFields::get_offset_of_rgchWhitespace_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (XPathFunctionLocalName_t3935594891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1284[1] = 
{
	XPathFunctionLocalName_t3935594891::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (XPathFunctionNamespaceUri_t448035136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1285[1] = 
{
	XPathFunctionNamespaceUri_t448035136::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (XPathFunctionName_t1880977850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1286[1] = 
{
	XPathFunctionName_t1880977850::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (XPathFunctionString_t3963481524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1287[1] = 
{
	XPathFunctionString_t3963481524::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (XPathFunctionConcat_t2913460711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1288[1] = 
{
	XPathFunctionConcat_t2913460711::get_offset_of_rgs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (XPathFunctionStartsWith_t1855488236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1289[2] = 
{
	XPathFunctionStartsWith_t1855488236::get_offset_of_arg0_0(),
	XPathFunctionStartsWith_t1855488236::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (XPathFunctionContains_t4033230314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1290[2] = 
{
	XPathFunctionContains_t4033230314::get_offset_of_arg0_0(),
	XPathFunctionContains_t4033230314::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (XPathFunctionSubstringBefore_t1797727619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1291[2] = 
{
	XPathFunctionSubstringBefore_t1797727619::get_offset_of_arg0_0(),
	XPathFunctionSubstringBefore_t1797727619::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (XPathFunctionSubstringAfter_t1560316700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1292[2] = 
{
	XPathFunctionSubstringAfter_t1560316700::get_offset_of_arg0_0(),
	XPathFunctionSubstringAfter_t1560316700::get_offset_of_arg1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (XPathFunctionSubstring_t2124841904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1293[3] = 
{
	XPathFunctionSubstring_t2124841904::get_offset_of_arg0_0(),
	XPathFunctionSubstring_t2124841904::get_offset_of_arg1_1(),
	XPathFunctionSubstring_t2124841904::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (XPathFunctionStringLength_t2821584396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1294[1] = 
{
	XPathFunctionStringLength_t2821584396::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (XPathFunctionNormalizeSpace_t1288231156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1295[1] = 
{
	XPathFunctionNormalizeSpace_t1288231156::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (XPathFunctionTranslate_t2720664953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1296[3] = 
{
	XPathFunctionTranslate_t2720664953::get_offset_of_arg0_0(),
	XPathFunctionTranslate_t2720664953::get_offset_of_arg1_1(),
	XPathFunctionTranslate_t2720664953::get_offset_of_arg2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (XPathBooleanFunction_t2511646183), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (XPathFunctionBoolean_t2454599187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1298[1] = 
{
	XPathFunctionBoolean_t2454599187::get_offset_of_arg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (XPathFunctionNot_t1631674504), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1299[1] = 
{
	XPathFunctionNot_t1631674504::get_offset_of_arg0_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
