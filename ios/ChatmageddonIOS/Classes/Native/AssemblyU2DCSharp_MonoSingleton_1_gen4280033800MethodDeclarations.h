﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ChatmageddonFBManager>::.ctor()
#define MonoSingleton_1__ctor_m336995510(__this, method) ((  void (*) (MonoSingleton_1_t4280033800 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonFBManager>::Awake()
#define MonoSingleton_1_Awake_m3997676307(__this, method) ((  void (*) (MonoSingleton_1_t4280033800 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ChatmageddonFBManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m2902283321(__this /* static, unused */, method) ((  ChatmageddonFBManager_t234400784 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ChatmageddonFBManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m2857797773(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ChatmageddonFBManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m4260801488(__this, method) ((  void (*) (MonoSingleton_1_t4280033800 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonFBManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m3839199320(__this, method) ((  void (*) (MonoSingleton_1_t4280033800 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatmageddonFBManager>::.cctor()
#define MonoSingleton_1__cctor_m3148581081(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
