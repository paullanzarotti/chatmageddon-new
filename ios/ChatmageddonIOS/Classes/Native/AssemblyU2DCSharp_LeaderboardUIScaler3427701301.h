﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// TweenPosition
struct TweenPosition_t1144714832;
// UIPanel
struct UIPanel_t1795085332;

#include "AssemblyU2DCSharp_LeaderboardLoaderUIScaler1071922518.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeaderboardUIScaler
struct  LeaderboardUIScaler_t3427701301  : public LeaderboardLoaderUIScaler_t1071922518
{
public:
	// UISprite LeaderboardUIScaler::background
	UISprite_t603616735 * ___background_27;
	// TweenPosition LeaderboardUIScaler::dailyScreenTween
	TweenPosition_t1144714832 * ___dailyScreenTween_28;
	// UIPanel LeaderboardUIScaler::dailyPanel
	UIPanel_t1795085332 * ___dailyPanel_29;
	// TweenPosition LeaderboardUIScaler::allTimeScreenTween
	TweenPosition_t1144714832 * ___allTimeScreenTween_30;
	// UIPanel LeaderboardUIScaler::allTimePanel
	UIPanel_t1795085332 * ___allTimePanel_31;

public:
	inline static int32_t get_offset_of_background_27() { return static_cast<int32_t>(offsetof(LeaderboardUIScaler_t3427701301, ___background_27)); }
	inline UISprite_t603616735 * get_background_27() const { return ___background_27; }
	inline UISprite_t603616735 ** get_address_of_background_27() { return &___background_27; }
	inline void set_background_27(UISprite_t603616735 * value)
	{
		___background_27 = value;
		Il2CppCodeGenWriteBarrier(&___background_27, value);
	}

	inline static int32_t get_offset_of_dailyScreenTween_28() { return static_cast<int32_t>(offsetof(LeaderboardUIScaler_t3427701301, ___dailyScreenTween_28)); }
	inline TweenPosition_t1144714832 * get_dailyScreenTween_28() const { return ___dailyScreenTween_28; }
	inline TweenPosition_t1144714832 ** get_address_of_dailyScreenTween_28() { return &___dailyScreenTween_28; }
	inline void set_dailyScreenTween_28(TweenPosition_t1144714832 * value)
	{
		___dailyScreenTween_28 = value;
		Il2CppCodeGenWriteBarrier(&___dailyScreenTween_28, value);
	}

	inline static int32_t get_offset_of_dailyPanel_29() { return static_cast<int32_t>(offsetof(LeaderboardUIScaler_t3427701301, ___dailyPanel_29)); }
	inline UIPanel_t1795085332 * get_dailyPanel_29() const { return ___dailyPanel_29; }
	inline UIPanel_t1795085332 ** get_address_of_dailyPanel_29() { return &___dailyPanel_29; }
	inline void set_dailyPanel_29(UIPanel_t1795085332 * value)
	{
		___dailyPanel_29 = value;
		Il2CppCodeGenWriteBarrier(&___dailyPanel_29, value);
	}

	inline static int32_t get_offset_of_allTimeScreenTween_30() { return static_cast<int32_t>(offsetof(LeaderboardUIScaler_t3427701301, ___allTimeScreenTween_30)); }
	inline TweenPosition_t1144714832 * get_allTimeScreenTween_30() const { return ___allTimeScreenTween_30; }
	inline TweenPosition_t1144714832 ** get_address_of_allTimeScreenTween_30() { return &___allTimeScreenTween_30; }
	inline void set_allTimeScreenTween_30(TweenPosition_t1144714832 * value)
	{
		___allTimeScreenTween_30 = value;
		Il2CppCodeGenWriteBarrier(&___allTimeScreenTween_30, value);
	}

	inline static int32_t get_offset_of_allTimePanel_31() { return static_cast<int32_t>(offsetof(LeaderboardUIScaler_t3427701301, ___allTimePanel_31)); }
	inline UIPanel_t1795085332 * get_allTimePanel_31() const { return ___allTimePanel_31; }
	inline UIPanel_t1795085332 ** get_address_of_allTimePanel_31() { return &___allTimePanel_31; }
	inline void set_allTimePanel_31(UIPanel_t1795085332 * value)
	{
		___allTimePanel_31 = value;
		Il2CppCodeGenWriteBarrier(&___allTimePanel_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
