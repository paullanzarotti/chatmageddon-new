﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<PrefabResource>::.ctor()
#define MonoSingleton_1__ctor_m469608530(__this, method) ((  void (*) (MonoSingleton_1_t677391278 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<PrefabResource>::Awake()
#define MonoSingleton_1_Awake_m1790777417(__this, method) ((  void (*) (MonoSingleton_1_t677391278 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<PrefabResource>::get_Instance()
#define MonoSingleton_1_get_Instance_m1627483127(__this /* static, unused */, method) ((  PrefabResource_t926725558 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<PrefabResource>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3089568235(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<PrefabResource>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m906101312(__this, method) ((  void (*) (MonoSingleton_1_t677391278 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<PrefabResource>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m4158419204(__this, method) ((  void (*) (MonoSingleton_1_t677391278 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<PrefabResource>::.cctor()
#define MonoSingleton_1__cctor_m1267483859(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
