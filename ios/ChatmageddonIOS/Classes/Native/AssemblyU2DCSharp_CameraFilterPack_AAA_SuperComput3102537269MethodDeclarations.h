﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_AAA_SuperComputer
struct CameraFilterPack_AAA_SuperComputer_t3102537269;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_AAA_SuperComputer::.ctor()
extern "C"  void CameraFilterPack_AAA_SuperComputer__ctor_m3901207422 (CameraFilterPack_AAA_SuperComputer_t3102537269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_AAA_SuperComputer::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_AAA_SuperComputer_get_material_m1432503889 (CameraFilterPack_AAA_SuperComputer_t3102537269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::Start()
extern "C"  void CameraFilterPack_AAA_SuperComputer_Start_m1272461654 (CameraFilterPack_AAA_SuperComputer_t3102537269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_AAA_SuperComputer_OnRenderImage_m2312141286 (CameraFilterPack_AAA_SuperComputer_t3102537269 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::OnValidate()
extern "C"  void CameraFilterPack_AAA_SuperComputer_OnValidate_m1369139771 (CameraFilterPack_AAA_SuperComputer_t3102537269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::Update()
extern "C"  void CameraFilterPack_AAA_SuperComputer_Update_m2782321301 (CameraFilterPack_AAA_SuperComputer_t3102537269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::OnDisable()
extern "C"  void CameraFilterPack_AAA_SuperComputer_OnDisable_m2989685665 (CameraFilterPack_AAA_SuperComputer_t3102537269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::.cctor()
extern "C"  void CameraFilterPack_AAA_SuperComputer__cctor_m4281294223 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
