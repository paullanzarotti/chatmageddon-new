﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Pixelisation_Dot
struct CameraFilterPack_Pixelisation_Dot_t214826704;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Pixelisation_Dot::.ctor()
extern "C"  void CameraFilterPack_Pixelisation_Dot__ctor_m3917259879 (CameraFilterPack_Pixelisation_Dot_t214826704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixelisation_Dot::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Pixelisation_Dot_get_material_m2900807872 (CameraFilterPack_Pixelisation_Dot_t214826704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::Start()
extern "C"  void CameraFilterPack_Pixelisation_Dot_Start_m3269671915 (CameraFilterPack_Pixelisation_Dot_t214826704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Pixelisation_Dot_OnRenderImage_m3066786659 (CameraFilterPack_Pixelisation_Dot_t214826704 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::OnValidate()
extern "C"  void CameraFilterPack_Pixelisation_Dot_OnValidate_m258208324 (CameraFilterPack_Pixelisation_Dot_t214826704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::Update()
extern "C"  void CameraFilterPack_Pixelisation_Dot_Update_m3021394 (CameraFilterPack_Pixelisation_Dot_t214826704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::OnDisable()
extern "C"  void CameraFilterPack_Pixelisation_Dot_OnDisable_m921516052 (CameraFilterPack_Pixelisation_Dot_t214826704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
