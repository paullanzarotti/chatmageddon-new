﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// PhoneNumbers.PhoneRegex
struct PhoneRegex_t3216508019;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// System.String
struct String_t;
// PhoneNumbers.PhoneNumberMatch
struct PhoneNumberMatch_t2163858580;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil_Len3057047663.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumberMatcher
struct  PhoneNumberMatcher_t3898172501  : public Il2CppObject
{
public:
	// PhoneNumbers.PhoneNumberUtil PhoneNumbers.PhoneNumberMatcher::phoneUtil
	PhoneNumberUtil_t4155573397 * ___phoneUtil_8;
	// System.String PhoneNumbers.PhoneNumberMatcher::text
	String_t* ___text_9;
	// System.String PhoneNumbers.PhoneNumberMatcher::preferredRegion
	String_t* ___preferredRegion_10;
	// PhoneNumbers.PhoneNumberUtil/Leniency PhoneNumbers.PhoneNumberMatcher::leniency
	int32_t ___leniency_11;
	// System.Int64 PhoneNumbers.PhoneNumberMatcher::maxTries
	int64_t ___maxTries_12;
	// PhoneNumbers.PhoneNumberMatch PhoneNumbers.PhoneNumberMatcher::lastMatch
	PhoneNumberMatch_t2163858580 * ___lastMatch_13;
	// System.Int32 PhoneNumbers.PhoneNumberMatcher::searchIndex
	int32_t ___searchIndex_14;

public:
	inline static int32_t get_offset_of_phoneUtil_8() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501, ___phoneUtil_8)); }
	inline PhoneNumberUtil_t4155573397 * get_phoneUtil_8() const { return ___phoneUtil_8; }
	inline PhoneNumberUtil_t4155573397 ** get_address_of_phoneUtil_8() { return &___phoneUtil_8; }
	inline void set_phoneUtil_8(PhoneNumberUtil_t4155573397 * value)
	{
		___phoneUtil_8 = value;
		Il2CppCodeGenWriteBarrier(&___phoneUtil_8, value);
	}

	inline static int32_t get_offset_of_text_9() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501, ___text_9)); }
	inline String_t* get_text_9() const { return ___text_9; }
	inline String_t** get_address_of_text_9() { return &___text_9; }
	inline void set_text_9(String_t* value)
	{
		___text_9 = value;
		Il2CppCodeGenWriteBarrier(&___text_9, value);
	}

	inline static int32_t get_offset_of_preferredRegion_10() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501, ___preferredRegion_10)); }
	inline String_t* get_preferredRegion_10() const { return ___preferredRegion_10; }
	inline String_t** get_address_of_preferredRegion_10() { return &___preferredRegion_10; }
	inline void set_preferredRegion_10(String_t* value)
	{
		___preferredRegion_10 = value;
		Il2CppCodeGenWriteBarrier(&___preferredRegion_10, value);
	}

	inline static int32_t get_offset_of_leniency_11() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501, ___leniency_11)); }
	inline int32_t get_leniency_11() const { return ___leniency_11; }
	inline int32_t* get_address_of_leniency_11() { return &___leniency_11; }
	inline void set_leniency_11(int32_t value)
	{
		___leniency_11 = value;
	}

	inline static int32_t get_offset_of_maxTries_12() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501, ___maxTries_12)); }
	inline int64_t get_maxTries_12() const { return ___maxTries_12; }
	inline int64_t* get_address_of_maxTries_12() { return &___maxTries_12; }
	inline void set_maxTries_12(int64_t value)
	{
		___maxTries_12 = value;
	}

	inline static int32_t get_offset_of_lastMatch_13() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501, ___lastMatch_13)); }
	inline PhoneNumberMatch_t2163858580 * get_lastMatch_13() const { return ___lastMatch_13; }
	inline PhoneNumberMatch_t2163858580 ** get_address_of_lastMatch_13() { return &___lastMatch_13; }
	inline void set_lastMatch_13(PhoneNumberMatch_t2163858580 * value)
	{
		___lastMatch_13 = value;
		Il2CppCodeGenWriteBarrier(&___lastMatch_13, value);
	}

	inline static int32_t get_offset_of_searchIndex_14() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501, ___searchIndex_14)); }
	inline int32_t get_searchIndex_14() const { return ___searchIndex_14; }
	inline int32_t* get_address_of_searchIndex_14() { return &___searchIndex_14; }
	inline void set_searchIndex_14(int32_t value)
	{
		___searchIndex_14 = value;
	}
};

struct PhoneNumberMatcher_t3898172501_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberMatcher::PATTERN
	Regex_t1803876613 * ___PATTERN_0;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberMatcher::PUB_PAGES
	Regex_t1803876613 * ___PUB_PAGES_1;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberMatcher::SLASH_SEPARATED_DATES
	Regex_t1803876613 * ___SLASH_SEPARATED_DATES_2;
	// System.Text.RegularExpressions.Regex PhoneNumbers.PhoneNumberMatcher::TIME_STAMPS
	Regex_t1803876613 * ___TIME_STAMPS_3;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberMatcher::TIME_STAMPS_SUFFIX
	PhoneRegex_t3216508019 * ___TIME_STAMPS_SUFFIX_4;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberMatcher::MATCHING_BRACKETS
	PhoneRegex_t3216508019 * ___MATCHING_BRACKETS_5;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberMatcher::LEAD_CLASS
	PhoneRegex_t3216508019 * ___LEAD_CLASS_6;
	// PhoneNumbers.PhoneRegex PhoneNumbers.PhoneNumberMatcher::GROUP_SEPARATOR
	PhoneRegex_t3216508019 * ___GROUP_SEPARATOR_7;

public:
	inline static int32_t get_offset_of_PATTERN_0() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___PATTERN_0)); }
	inline Regex_t1803876613 * get_PATTERN_0() const { return ___PATTERN_0; }
	inline Regex_t1803876613 ** get_address_of_PATTERN_0() { return &___PATTERN_0; }
	inline void set_PATTERN_0(Regex_t1803876613 * value)
	{
		___PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier(&___PATTERN_0, value);
	}

	inline static int32_t get_offset_of_PUB_PAGES_1() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___PUB_PAGES_1)); }
	inline Regex_t1803876613 * get_PUB_PAGES_1() const { return ___PUB_PAGES_1; }
	inline Regex_t1803876613 ** get_address_of_PUB_PAGES_1() { return &___PUB_PAGES_1; }
	inline void set_PUB_PAGES_1(Regex_t1803876613 * value)
	{
		___PUB_PAGES_1 = value;
		Il2CppCodeGenWriteBarrier(&___PUB_PAGES_1, value);
	}

	inline static int32_t get_offset_of_SLASH_SEPARATED_DATES_2() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___SLASH_SEPARATED_DATES_2)); }
	inline Regex_t1803876613 * get_SLASH_SEPARATED_DATES_2() const { return ___SLASH_SEPARATED_DATES_2; }
	inline Regex_t1803876613 ** get_address_of_SLASH_SEPARATED_DATES_2() { return &___SLASH_SEPARATED_DATES_2; }
	inline void set_SLASH_SEPARATED_DATES_2(Regex_t1803876613 * value)
	{
		___SLASH_SEPARATED_DATES_2 = value;
		Il2CppCodeGenWriteBarrier(&___SLASH_SEPARATED_DATES_2, value);
	}

	inline static int32_t get_offset_of_TIME_STAMPS_3() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___TIME_STAMPS_3)); }
	inline Regex_t1803876613 * get_TIME_STAMPS_3() const { return ___TIME_STAMPS_3; }
	inline Regex_t1803876613 ** get_address_of_TIME_STAMPS_3() { return &___TIME_STAMPS_3; }
	inline void set_TIME_STAMPS_3(Regex_t1803876613 * value)
	{
		___TIME_STAMPS_3 = value;
		Il2CppCodeGenWriteBarrier(&___TIME_STAMPS_3, value);
	}

	inline static int32_t get_offset_of_TIME_STAMPS_SUFFIX_4() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___TIME_STAMPS_SUFFIX_4)); }
	inline PhoneRegex_t3216508019 * get_TIME_STAMPS_SUFFIX_4() const { return ___TIME_STAMPS_SUFFIX_4; }
	inline PhoneRegex_t3216508019 ** get_address_of_TIME_STAMPS_SUFFIX_4() { return &___TIME_STAMPS_SUFFIX_4; }
	inline void set_TIME_STAMPS_SUFFIX_4(PhoneRegex_t3216508019 * value)
	{
		___TIME_STAMPS_SUFFIX_4 = value;
		Il2CppCodeGenWriteBarrier(&___TIME_STAMPS_SUFFIX_4, value);
	}

	inline static int32_t get_offset_of_MATCHING_BRACKETS_5() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___MATCHING_BRACKETS_5)); }
	inline PhoneRegex_t3216508019 * get_MATCHING_BRACKETS_5() const { return ___MATCHING_BRACKETS_5; }
	inline PhoneRegex_t3216508019 ** get_address_of_MATCHING_BRACKETS_5() { return &___MATCHING_BRACKETS_5; }
	inline void set_MATCHING_BRACKETS_5(PhoneRegex_t3216508019 * value)
	{
		___MATCHING_BRACKETS_5 = value;
		Il2CppCodeGenWriteBarrier(&___MATCHING_BRACKETS_5, value);
	}

	inline static int32_t get_offset_of_LEAD_CLASS_6() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___LEAD_CLASS_6)); }
	inline PhoneRegex_t3216508019 * get_LEAD_CLASS_6() const { return ___LEAD_CLASS_6; }
	inline PhoneRegex_t3216508019 ** get_address_of_LEAD_CLASS_6() { return &___LEAD_CLASS_6; }
	inline void set_LEAD_CLASS_6(PhoneRegex_t3216508019 * value)
	{
		___LEAD_CLASS_6 = value;
		Il2CppCodeGenWriteBarrier(&___LEAD_CLASS_6, value);
	}

	inline static int32_t get_offset_of_GROUP_SEPARATOR_7() { return static_cast<int32_t>(offsetof(PhoneNumberMatcher_t3898172501_StaticFields, ___GROUP_SEPARATOR_7)); }
	inline PhoneRegex_t3216508019 * get_GROUP_SEPARATOR_7() const { return ___GROUP_SEPARATOR_7; }
	inline PhoneRegex_t3216508019 ** get_address_of_GROUP_SEPARATOR_7() { return &___GROUP_SEPARATOR_7; }
	inline void set_GROUP_SEPARATOR_7(PhoneRegex_t3216508019 * value)
	{
		___GROUP_SEPARATOR_7 = value;
		Il2CppCodeGenWriteBarrier(&___GROUP_SEPARATOR_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
