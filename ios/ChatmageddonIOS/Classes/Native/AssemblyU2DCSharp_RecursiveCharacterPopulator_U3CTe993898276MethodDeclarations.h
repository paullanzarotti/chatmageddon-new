﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RecursiveCharacterPopulator/<TextPopulator>c__Iterator0
struct U3CTextPopulatorU3Ec__Iterator0_t993898276;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::.ctor()
extern "C"  void U3CTextPopulatorU3Ec__Iterator0__ctor_m709648601 (U3CTextPopulatorU3Ec__Iterator0_t993898276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::MoveNext()
extern "C"  bool U3CTextPopulatorU3Ec__Iterator0_MoveNext_m4176975723 (U3CTextPopulatorU3Ec__Iterator0_t993898276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTextPopulatorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3810491327 (U3CTextPopulatorU3Ec__Iterator0_t993898276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTextPopulatorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3374101799 (U3CTextPopulatorU3Ec__Iterator0_t993898276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::Dispose()
extern "C"  void U3CTextPopulatorU3Ec__Iterator0_Dispose_m855690934 (U3CTextPopulatorU3Ec__Iterator0_t993898276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RecursiveCharacterPopulator/<TextPopulator>c__Iterator0::Reset()
extern "C"  void U3CTextPopulatorU3Ec__Iterator0_Reset_m1177148964 (U3CTextPopulatorU3Ec__Iterator0_t993898276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
