﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPPickerSpriteBase
struct IPPickerSpriteBase_t2462226553;
// UIWidget
struct UIWidget_t1453041918;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void IPPickerSpriteBase::.ctor()
extern "C"  void IPPickerSpriteBase__ctor_m161698042 (IPPickerSpriteBase_t2462226553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget IPPickerSpriteBase::GetCenterWidget()
extern "C"  UIWidget_t1453041918 * IPPickerSpriteBase_GetCenterWidget_m231214676 (IPPickerSpriteBase_t2462226553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget IPPickerSpriteBase::GetWidgetAtScreenPos(UnityEngine.Vector2)
extern "C"  UIWidget_t1453041918 * IPPickerSpriteBase_GetWidgetAtScreenPos_m3167860992 (IPPickerSpriteBase_t2462226553 * __this, Vector2_t2243707579  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerSpriteBase::InitWidgets()
extern "C"  void IPPickerSpriteBase_InitWidgets_m2697823801 (IPPickerSpriteBase_t2462226553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerSpriteBase::MakeWidgetComponents()
extern "C"  void IPPickerSpriteBase_MakeWidgetComponents_m1894035946 (IPPickerSpriteBase_t2462226553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPPickerSpriteBase::EnableWidgets(System.Boolean)
extern "C"  void IPPickerSpriteBase_EnableWidgets_m161239929 (IPPickerSpriteBase_t2462226553 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPPickerSpriteBase::WidgetsNeedRebuild()
extern "C"  bool IPPickerSpriteBase_WidgetsNeedRebuild_m602249318 (IPPickerSpriteBase_t2462226553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
