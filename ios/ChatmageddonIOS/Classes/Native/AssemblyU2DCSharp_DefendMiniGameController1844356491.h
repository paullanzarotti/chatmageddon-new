﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefendModal
struct DefendModal_t2645228609;
// DefendGameNavScreen
struct DefendGameNavScreen_t2935542205;

#include "AssemblyU2DCSharp_MiniGameController3067676539.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendMiniGameController
struct  DefendMiniGameController_t1844356491  : public MiniGameController_t3067676539
{
public:
	// DefendModal DefendMiniGameController::modal
	DefendModal_t2645228609 * ___modal_4;
	// DefendGameNavScreen DefendMiniGameController::navScreen
	DefendGameNavScreen_t2935542205 * ___navScreen_5;

public:
	inline static int32_t get_offset_of_modal_4() { return static_cast<int32_t>(offsetof(DefendMiniGameController_t1844356491, ___modal_4)); }
	inline DefendModal_t2645228609 * get_modal_4() const { return ___modal_4; }
	inline DefendModal_t2645228609 ** get_address_of_modal_4() { return &___modal_4; }
	inline void set_modal_4(DefendModal_t2645228609 * value)
	{
		___modal_4 = value;
		Il2CppCodeGenWriteBarrier(&___modal_4, value);
	}

	inline static int32_t get_offset_of_navScreen_5() { return static_cast<int32_t>(offsetof(DefendMiniGameController_t1844356491, ___navScreen_5)); }
	inline DefendGameNavScreen_t2935542205 * get_navScreen_5() const { return ___navScreen_5; }
	inline DefendGameNavScreen_t2935542205 ** get_address_of_navScreen_5() { return &___navScreen_5; }
	inline void set_navScreen_5(DefendGameNavScreen_t2935542205 * value)
	{
		___navScreen_5 = value;
		Il2CppCodeGenWriteBarrier(&___navScreen_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
