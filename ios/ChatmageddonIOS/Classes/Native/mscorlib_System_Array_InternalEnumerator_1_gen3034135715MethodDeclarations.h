﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3034135715.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"

// System.Void System.Array/InternalEnumerator`1<FriendNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4150477014_gshared (InternalEnumerator_1_t3034135715 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4150477014(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3034135715 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4150477014_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FriendNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3871634594_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3871634594(__this, method) ((  void (*) (InternalEnumerator_1_t3034135715 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3871634594_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FriendNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1492489908_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1492489908(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3034135715 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1492489908_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FriendNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3485547609_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3485547609(__this, method) ((  void (*) (InternalEnumerator_1_t3034135715 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3485547609_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FriendNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2243210150_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2243210150(__this, method) ((  bool (*) (InternalEnumerator_1_t3034135715 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2243210150_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FriendNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m685573937_gshared (InternalEnumerator_1_t3034135715 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m685573937(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3034135715 *, const MethodInfo*))InternalEnumerator_1_get_Current_m685573937_gshared)(__this, method)
