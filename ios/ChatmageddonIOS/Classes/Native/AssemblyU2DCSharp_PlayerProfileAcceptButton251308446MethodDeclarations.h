﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerProfileAcceptButton
struct PlayerProfileAcceptButton_t251308446;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerProfileAcceptButton::.ctor()
extern "C"  void PlayerProfileAcceptButton__ctor_m3518570305 (PlayerProfileAcceptButton_t251308446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerProfileAcceptButton::OnButtonClick()
extern "C"  void PlayerProfileAcceptButton_OnButtonClick_m535141382 (PlayerProfileAcceptButton_t251308446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
