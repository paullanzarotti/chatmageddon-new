﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsFindDirectionAdvanced
struct OnlineMapsFindDirectionAdvanced_t3669500598;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirectionMode1690934541.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirectionAvoid3411768659.h"
#include "AssemblyU2DCSharp_OnlineMapsFindDirectionUnits4069044639.h"
#include "AssemblyU2DCSharp_OnlineMapsQueryType3894089788.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void OnlineMapsFindDirectionAdvanced::.ctor(System.String,System.String,OnlineMapsFindDirectionMode,System.String[],System.Boolean,OnlineMapsFindDirectionAvoid,OnlineMapsFindDirectionUnits,System.String,System.Int64,System.Int64,System.String)
extern "C"  void OnlineMapsFindDirectionAdvanced__ctor_m2093112401 (OnlineMapsFindDirectionAdvanced_t3669500598 * __this, String_t* ___origin0, String_t* ___destination1, int32_t ___mode2, StringU5BU5D_t1642385972* ___waypoints3, bool ___alternatives4, int32_t ___avoid5, int32_t ___units6, String_t* ___region7, int64_t ___departure_time8, int64_t ___arrival_time9, String_t* ___language10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsQueryType OnlineMapsFindDirectionAdvanced::get_type()
extern "C"  int32_t OnlineMapsFindDirectionAdvanced_get_type_m2357129961 (OnlineMapsFindDirectionAdvanced_t3669500598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindDirectionAdvanced OnlineMapsFindDirectionAdvanced::Find(UnityEngine.Vector2,UnityEngine.Vector2,OnlineMapsFindDirectionMode,System.String[],System.Boolean,OnlineMapsFindDirectionAvoid,OnlineMapsFindDirectionUnits,System.String,System.Int64,System.Int64,System.String)
extern "C"  OnlineMapsFindDirectionAdvanced_t3669500598 * OnlineMapsFindDirectionAdvanced_Find_m1697277665 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___destination1, int32_t ___mode2, StringU5BU5D_t1642385972* ___waypoints3, bool ___alternatives4, int32_t ___avoid5, int32_t ___units6, String_t* ___region7, int64_t ___departure_time8, int64_t ___arrival_time9, String_t* ___language10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindDirectionAdvanced OnlineMapsFindDirectionAdvanced::Find(UnityEngine.Vector2,System.String,OnlineMapsFindDirectionMode,System.String[],System.Boolean,OnlineMapsFindDirectionAvoid,OnlineMapsFindDirectionUnits,System.String,System.Int64,System.Int64,System.String)
extern "C"  OnlineMapsFindDirectionAdvanced_t3669500598 * OnlineMapsFindDirectionAdvanced_Find_m2712693115 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, String_t* ___destination1, int32_t ___mode2, StringU5BU5D_t1642385972* ___waypoints3, bool ___alternatives4, int32_t ___avoid5, int32_t ___units6, String_t* ___region7, int64_t ___departure_time8, int64_t ___arrival_time9, String_t* ___language10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindDirectionAdvanced OnlineMapsFindDirectionAdvanced::Find(System.String,UnityEngine.Vector2,OnlineMapsFindDirectionMode,System.String[],System.Boolean,OnlineMapsFindDirectionAvoid,OnlineMapsFindDirectionUnits,System.String,System.Int64,System.Int64,System.String)
extern "C"  OnlineMapsFindDirectionAdvanced_t3669500598 * OnlineMapsFindDirectionAdvanced_Find_m3318398651 (Il2CppObject * __this /* static, unused */, String_t* ___origin0, Vector2_t2243707579  ___destination1, int32_t ___mode2, StringU5BU5D_t1642385972* ___waypoints3, bool ___alternatives4, int32_t ___avoid5, int32_t ___units6, String_t* ___region7, int64_t ___departure_time8, int64_t ___arrival_time9, String_t* ___language10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindDirectionAdvanced OnlineMapsFindDirectionAdvanced::Find(System.String,System.String,OnlineMapsFindDirectionMode,System.String[],System.Boolean,OnlineMapsFindDirectionAvoid,OnlineMapsFindDirectionUnits,System.String,System.Int64,System.Int64,System.String)
extern "C"  OnlineMapsFindDirectionAdvanced_t3669500598 * OnlineMapsFindDirectionAdvanced_Find_m3525789793 (Il2CppObject * __this /* static, unused */, String_t* ___origin0, String_t* ___destination1, int32_t ___mode2, StringU5BU5D_t1642385972* ___waypoints3, bool ___alternatives4, int32_t ___avoid5, int32_t ___units6, String_t* ___region7, int64_t ___departure_time8, int64_t ___arrival_time9, String_t* ___language10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
