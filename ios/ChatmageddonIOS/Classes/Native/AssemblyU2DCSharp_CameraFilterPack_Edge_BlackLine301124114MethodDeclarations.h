﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Edge_BlackLine
struct CameraFilterPack_Edge_BlackLine_t301124114;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Edge_BlackLine::.ctor()
extern "C"  void CameraFilterPack_Edge_BlackLine__ctor_m3208865229 (CameraFilterPack_Edge_BlackLine_t301124114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_BlackLine::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Edge_BlackLine_get_material_m719150570 (CameraFilterPack_Edge_BlackLine_t301124114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::Start()
extern "C"  void CameraFilterPack_Edge_BlackLine_Start_m2347036525 (CameraFilterPack_Edge_BlackLine_t301124114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Edge_BlackLine_OnRenderImage_m203811669 (CameraFilterPack_Edge_BlackLine_t301124114 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::Update()
extern "C"  void CameraFilterPack_Edge_BlackLine_Update_m3495150256 (CameraFilterPack_Edge_BlackLine_t301124114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::OnDisable()
extern "C"  void CameraFilterPack_Edge_BlackLine_OnDisable_m4192652702 (CameraFilterPack_Edge_BlackLine_t301124114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
