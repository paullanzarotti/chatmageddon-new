﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.ProductDefinition
struct ProductDefinition_t1519653988;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseType1639241305.h"

// System.Void Unibill.ProductDefinition::.ctor(System.String,PurchaseType)
extern "C"  void ProductDefinition__ctor_m327385889 (ProductDefinition_t1519653988 * __this, String_t* ___platformSpecificId0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.ProductDefinition::get_PlatformSpecificId()
extern "C"  String_t* ProductDefinition_get_PlatformSpecificId_m330161718 (ProductDefinition_t1519653988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.ProductDefinition::set_PlatformSpecificId(System.String)
extern "C"  void ProductDefinition_set_PlatformSpecificId_m538945865 (ProductDefinition_t1519653988 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchaseType Unibill.ProductDefinition::get_Type()
extern "C"  int32_t ProductDefinition_get_Type_m3674054549 (ProductDefinition_t1519653988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.ProductDefinition::set_Type(PurchaseType)
extern "C"  void ProductDefinition_set_Type_m4117069810 (ProductDefinition_t1519653988 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
