﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.String>
struct Dictionary_2_t3917891754;
// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Dictionary_2_t2197932782;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseType1639241305.h"
#include "mscorlib_System_Decimal724701077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PurchasableItem
struct  PurchasableItem_t3963353899  : public Il2CppObject
{
public:
	// System.Boolean PurchasableItem::<AvailableToPurchase>k__BackingField
	bool ___U3CAvailableToPurchaseU3Ek__BackingField_0;
	// System.String PurchasableItem::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_1;
	// PurchaseType PurchasableItem::<PurchaseType>k__BackingField
	int32_t ___U3CPurchaseTypeU3Ek__BackingField_2;
	// System.String PurchasableItem::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_3;
	// System.String PurchasableItem::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_4;
	// System.Decimal PurchasableItem::<localizedPrice>k__BackingField
	Decimal_t724701077  ___U3ClocalizedPriceU3Ek__BackingField_5;
	// System.String PurchasableItem::<localizedPriceString>k__BackingField
	String_t* ___U3ClocalizedPriceStringU3Ek__BackingField_6;
	// System.String PurchasableItem::<localizedTitle>k__BackingField
	String_t* ___U3ClocalizedTitleU3Ek__BackingField_7;
	// System.String PurchasableItem::<localizedDescription>k__BackingField
	String_t* ___U3ClocalizedDescriptionU3Ek__BackingField_8;
	// System.String PurchasableItem::<isoCurrencySymbol>k__BackingField
	String_t* ___U3CisoCurrencySymbolU3Ek__BackingField_9;
	// System.Decimal PurchasableItem::<priceInLocalCurrency>k__BackingField
	Decimal_t724701077  ___U3CpriceInLocalCurrencyU3Ek__BackingField_10;
	// System.String PurchasableItem::<downloadableContentId>k__BackingField
	String_t* ___U3CdownloadableContentIdU3Ek__BackingField_11;
	// System.String PurchasableItem::<receipt>k__BackingField
	String_t* ___U3CreceiptU3Ek__BackingField_12;
	// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.String> PurchasableItem::<LocalIds>k__BackingField
	Dictionary_2_t3917891754 * ___U3CLocalIdsU3Ek__BackingField_13;
	// System.Collections.Generic.Dictionary`2<Unibill.Impl.BillingPlatform,System.Collections.Generic.Dictionary`2<System.String,System.Object>> PurchasableItem::platformBundles
	Dictionary_2_t2197932782 * ___platformBundles_14;
	// Unibill.Impl.BillingPlatform PurchasableItem::platform
	int32_t ___platform_15;

public:
	inline static int32_t get_offset_of_U3CAvailableToPurchaseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CAvailableToPurchaseU3Ek__BackingField_0)); }
	inline bool get_U3CAvailableToPurchaseU3Ek__BackingField_0() const { return ___U3CAvailableToPurchaseU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CAvailableToPurchaseU3Ek__BackingField_0() { return &___U3CAvailableToPurchaseU3Ek__BackingField_0; }
	inline void set_U3CAvailableToPurchaseU3Ek__BackingField_0(bool value)
	{
		___U3CAvailableToPurchaseU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CPurchaseTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CPurchaseTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CPurchaseTypeU3Ek__BackingField_2() const { return ___U3CPurchaseTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CPurchaseTypeU3Ek__BackingField_2() { return &___U3CPurchaseTypeU3Ek__BackingField_2; }
	inline void set_U3CPurchaseTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CPurchaseTypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CnameU3Ek__BackingField_3)); }
	inline String_t* get_U3CnameU3Ek__BackingField_3() const { return ___U3CnameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_3() { return &___U3CnameU3Ek__BackingField_3; }
	inline void set_U3CnameU3Ek__BackingField_3(String_t* value)
	{
		___U3CnameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CdescriptionU3Ek__BackingField_4)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_4() const { return ___U3CdescriptionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_4() { return &___U3CdescriptionU3Ek__BackingField_4; }
	inline void set_U3CdescriptionU3Ek__BackingField_4(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdescriptionU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3ClocalizedPriceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3ClocalizedPriceU3Ek__BackingField_5)); }
	inline Decimal_t724701077  get_U3ClocalizedPriceU3Ek__BackingField_5() const { return ___U3ClocalizedPriceU3Ek__BackingField_5; }
	inline Decimal_t724701077 * get_address_of_U3ClocalizedPriceU3Ek__BackingField_5() { return &___U3ClocalizedPriceU3Ek__BackingField_5; }
	inline void set_U3ClocalizedPriceU3Ek__BackingField_5(Decimal_t724701077  value)
	{
		___U3ClocalizedPriceU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3ClocalizedPriceStringU3Ek__BackingField_6)); }
	inline String_t* get_U3ClocalizedPriceStringU3Ek__BackingField_6() const { return ___U3ClocalizedPriceStringU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3ClocalizedPriceStringU3Ek__BackingField_6() { return &___U3ClocalizedPriceStringU3Ek__BackingField_6; }
	inline void set_U3ClocalizedPriceStringU3Ek__BackingField_6(String_t* value)
	{
		___U3ClocalizedPriceStringU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalizedPriceStringU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3ClocalizedTitleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3ClocalizedTitleU3Ek__BackingField_7)); }
	inline String_t* get_U3ClocalizedTitleU3Ek__BackingField_7() const { return ___U3ClocalizedTitleU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3ClocalizedTitleU3Ek__BackingField_7() { return &___U3ClocalizedTitleU3Ek__BackingField_7; }
	inline void set_U3ClocalizedTitleU3Ek__BackingField_7(String_t* value)
	{
		___U3ClocalizedTitleU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalizedTitleU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3ClocalizedDescriptionU3Ek__BackingField_8)); }
	inline String_t* get_U3ClocalizedDescriptionU3Ek__BackingField_8() const { return ___U3ClocalizedDescriptionU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3ClocalizedDescriptionU3Ek__BackingField_8() { return &___U3ClocalizedDescriptionU3Ek__BackingField_8; }
	inline void set_U3ClocalizedDescriptionU3Ek__BackingField_8(String_t* value)
	{
		___U3ClocalizedDescriptionU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClocalizedDescriptionU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CisoCurrencySymbolU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CisoCurrencySymbolU3Ek__BackingField_9)); }
	inline String_t* get_U3CisoCurrencySymbolU3Ek__BackingField_9() const { return ___U3CisoCurrencySymbolU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CisoCurrencySymbolU3Ek__BackingField_9() { return &___U3CisoCurrencySymbolU3Ek__BackingField_9; }
	inline void set_U3CisoCurrencySymbolU3Ek__BackingField_9(String_t* value)
	{
		___U3CisoCurrencySymbolU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CisoCurrencySymbolU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CpriceInLocalCurrencyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CpriceInLocalCurrencyU3Ek__BackingField_10)); }
	inline Decimal_t724701077  get_U3CpriceInLocalCurrencyU3Ek__BackingField_10() const { return ___U3CpriceInLocalCurrencyU3Ek__BackingField_10; }
	inline Decimal_t724701077 * get_address_of_U3CpriceInLocalCurrencyU3Ek__BackingField_10() { return &___U3CpriceInLocalCurrencyU3Ek__BackingField_10; }
	inline void set_U3CpriceInLocalCurrencyU3Ek__BackingField_10(Decimal_t724701077  value)
	{
		___U3CpriceInLocalCurrencyU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CdownloadableContentIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CdownloadableContentIdU3Ek__BackingField_11)); }
	inline String_t* get_U3CdownloadableContentIdU3Ek__BackingField_11() const { return ___U3CdownloadableContentIdU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CdownloadableContentIdU3Ek__BackingField_11() { return &___U3CdownloadableContentIdU3Ek__BackingField_11; }
	inline void set_U3CdownloadableContentIdU3Ek__BackingField_11(String_t* value)
	{
		___U3CdownloadableContentIdU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdownloadableContentIdU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CreceiptU3Ek__BackingField_12)); }
	inline String_t* get_U3CreceiptU3Ek__BackingField_12() const { return ___U3CreceiptU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CreceiptU3Ek__BackingField_12() { return &___U3CreceiptU3Ek__BackingField_12; }
	inline void set_U3CreceiptU3Ek__BackingField_12(String_t* value)
	{
		___U3CreceiptU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreceiptU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CLocalIdsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___U3CLocalIdsU3Ek__BackingField_13)); }
	inline Dictionary_2_t3917891754 * get_U3CLocalIdsU3Ek__BackingField_13() const { return ___U3CLocalIdsU3Ek__BackingField_13; }
	inline Dictionary_2_t3917891754 ** get_address_of_U3CLocalIdsU3Ek__BackingField_13() { return &___U3CLocalIdsU3Ek__BackingField_13; }
	inline void set_U3CLocalIdsU3Ek__BackingField_13(Dictionary_2_t3917891754 * value)
	{
		___U3CLocalIdsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLocalIdsU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_platformBundles_14() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___platformBundles_14)); }
	inline Dictionary_2_t2197932782 * get_platformBundles_14() const { return ___platformBundles_14; }
	inline Dictionary_2_t2197932782 ** get_address_of_platformBundles_14() { return &___platformBundles_14; }
	inline void set_platformBundles_14(Dictionary_2_t2197932782 * value)
	{
		___platformBundles_14 = value;
		Il2CppCodeGenWriteBarrier(&___platformBundles_14, value);
	}

	inline static int32_t get_offset_of_platform_15() { return static_cast<int32_t>(offsetof(PurchasableItem_t3963353899, ___platform_15)); }
	inline int32_t get_platform_15() const { return ___platform_15; }
	inline int32_t* get_address_of_platform_15() { return &___platform_15; }
	inline void set_platform_15(int32_t value)
	{
		___platform_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
