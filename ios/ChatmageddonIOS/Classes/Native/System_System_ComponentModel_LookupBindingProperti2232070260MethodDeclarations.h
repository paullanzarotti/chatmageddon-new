﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.LookupBindingPropertiesAttribute
struct LookupBindingPropertiesAttribute_t2232070260;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.LookupBindingPropertiesAttribute::.ctor(System.String,System.String,System.String,System.String)
extern "C"  void LookupBindingPropertiesAttribute__ctor_m1427036893 (LookupBindingPropertiesAttribute_t2232070260 * __this, String_t* ___dataSource0, String_t* ___displayMember1, String_t* ___valueMember2, String_t* ___lookupMember3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.LookupBindingPropertiesAttribute::.ctor()
extern "C"  void LookupBindingPropertiesAttribute__ctor_m3140621701 (LookupBindingPropertiesAttribute_t2232070260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.LookupBindingPropertiesAttribute::.cctor()
extern "C"  void LookupBindingPropertiesAttribute__cctor_m3965399802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.LookupBindingPropertiesAttribute::GetHashCode()
extern "C"  int32_t LookupBindingPropertiesAttribute_GetHashCode_m1821414738 (LookupBindingPropertiesAttribute_t2232070260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.LookupBindingPropertiesAttribute::Equals(System.Object)
extern "C"  bool LookupBindingPropertiesAttribute_Equals_m3912052682 (LookupBindingPropertiesAttribute_t2232070260 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.LookupBindingPropertiesAttribute::get_DataSource()
extern "C"  String_t* LookupBindingPropertiesAttribute_get_DataSource_m3123732762 (LookupBindingPropertiesAttribute_t2232070260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.LookupBindingPropertiesAttribute::get_DisplayMember()
extern "C"  String_t* LookupBindingPropertiesAttribute_get_DisplayMember_m4049564359 (LookupBindingPropertiesAttribute_t2232070260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.LookupBindingPropertiesAttribute::get_LookupMember()
extern "C"  String_t* LookupBindingPropertiesAttribute_get_LookupMember_m1752815891 (LookupBindingPropertiesAttribute_t2232070260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.LookupBindingPropertiesAttribute::get_ValueMember()
extern "C"  String_t* LookupBindingPropertiesAttribute_get_ValueMember_m2191062684 (LookupBindingPropertiesAttribute_t2232070260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
