﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.CodeGenerator
struct CodeGenerator_t1245030040;

#include "codegen/il2cpp-codegen.h"

// System.Void System.CodeDom.Compiler.CodeGenerator::.ctor()
extern "C"  void CodeGenerator__ctor_m1054665543 (CodeGenerator_t1245030040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CodeGenerator::.cctor()
extern "C"  void CodeGenerator__cctor_m2674835028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
