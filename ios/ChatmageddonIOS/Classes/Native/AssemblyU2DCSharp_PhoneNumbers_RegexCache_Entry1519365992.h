﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneRegex
struct PhoneRegex_t3216508019;
// System.Collections.Generic.LinkedListNode`1<System.String>
struct LinkedListNode_1_t925326146;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.RegexCache/Entry
struct  Entry_t1519365992  : public Il2CppObject
{
public:
	// PhoneNumbers.PhoneRegex PhoneNumbers.RegexCache/Entry::Regex
	PhoneRegex_t3216508019 * ___Regex_0;
	// System.Collections.Generic.LinkedListNode`1<System.String> PhoneNumbers.RegexCache/Entry::Node
	LinkedListNode_1_t925326146 * ___Node_1;

public:
	inline static int32_t get_offset_of_Regex_0() { return static_cast<int32_t>(offsetof(Entry_t1519365992, ___Regex_0)); }
	inline PhoneRegex_t3216508019 * get_Regex_0() const { return ___Regex_0; }
	inline PhoneRegex_t3216508019 ** get_address_of_Regex_0() { return &___Regex_0; }
	inline void set_Regex_0(PhoneRegex_t3216508019 * value)
	{
		___Regex_0 = value;
		Il2CppCodeGenWriteBarrier(&___Regex_0, value);
	}

	inline static int32_t get_offset_of_Node_1() { return static_cast<int32_t>(offsetof(Entry_t1519365992, ___Node_1)); }
	inline LinkedListNode_1_t925326146 * get_Node_1() const { return ___Node_1; }
	inline LinkedListNode_1_t925326146 ** get_address_of_Node_1() { return &___Node_1; }
	inline void set_Node_1(LinkedListNode_1_t925326146 * value)
	{
		___Node_1 = value;
		Il2CppCodeGenWriteBarrier(&___Node_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
