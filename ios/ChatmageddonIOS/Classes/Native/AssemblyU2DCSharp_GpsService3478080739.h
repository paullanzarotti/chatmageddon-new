﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GpsService
struct  GpsService_t3478080739  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GpsService::gpsDisabled
	bool ___gpsDisabled_2;
	// UnityEngine.LocationInfo GpsService::startLocation
	LocationInfo_t1364725149  ___startLocation_3;
	// UnityEngine.LocationInfo GpsService::lastLocation
	LocationInfo_t1364725149  ___lastLocation_4;
	// System.Single GpsService::sphereRadius
	float ___sphereRadius_5;
	// System.Single GpsService::distanceBetweenPoints
	float ___distanceBetweenPoints_6;

public:
	inline static int32_t get_offset_of_gpsDisabled_2() { return static_cast<int32_t>(offsetof(GpsService_t3478080739, ___gpsDisabled_2)); }
	inline bool get_gpsDisabled_2() const { return ___gpsDisabled_2; }
	inline bool* get_address_of_gpsDisabled_2() { return &___gpsDisabled_2; }
	inline void set_gpsDisabled_2(bool value)
	{
		___gpsDisabled_2 = value;
	}

	inline static int32_t get_offset_of_startLocation_3() { return static_cast<int32_t>(offsetof(GpsService_t3478080739, ___startLocation_3)); }
	inline LocationInfo_t1364725149  get_startLocation_3() const { return ___startLocation_3; }
	inline LocationInfo_t1364725149 * get_address_of_startLocation_3() { return &___startLocation_3; }
	inline void set_startLocation_3(LocationInfo_t1364725149  value)
	{
		___startLocation_3 = value;
	}

	inline static int32_t get_offset_of_lastLocation_4() { return static_cast<int32_t>(offsetof(GpsService_t3478080739, ___lastLocation_4)); }
	inline LocationInfo_t1364725149  get_lastLocation_4() const { return ___lastLocation_4; }
	inline LocationInfo_t1364725149 * get_address_of_lastLocation_4() { return &___lastLocation_4; }
	inline void set_lastLocation_4(LocationInfo_t1364725149  value)
	{
		___lastLocation_4 = value;
	}

	inline static int32_t get_offset_of_sphereRadius_5() { return static_cast<int32_t>(offsetof(GpsService_t3478080739, ___sphereRadius_5)); }
	inline float get_sphereRadius_5() const { return ___sphereRadius_5; }
	inline float* get_address_of_sphereRadius_5() { return &___sphereRadius_5; }
	inline void set_sphereRadius_5(float value)
	{
		___sphereRadius_5 = value;
	}

	inline static int32_t get_offset_of_distanceBetweenPoints_6() { return static_cast<int32_t>(offsetof(GpsService_t3478080739, ___distanceBetweenPoints_6)); }
	inline float get_distanceBetweenPoints_6() const { return ___distanceBetweenPoints_6; }
	inline float* get_address_of_distanceBetweenPoints_6() { return &___distanceBetweenPoints_6; }
	inline void set_distanceBetweenPoints_6(float value)
	{
		___distanceBetweenPoints_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
