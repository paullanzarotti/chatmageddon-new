﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DefendMiniGameController
struct DefendMiniGameController_t1844356491;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendMiniGameController/<SendDefendMissileServerCall>c__AnonStorey1
struct  U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571  : public Il2CppObject
{
public:
	// System.Boolean DefendMiniGameController/<SendDefendMissileServerCall>c__AnonStorey1::defendSuccess
	bool ___defendSuccess_0;
	// System.DateTime DefendMiniGameController/<SendDefendMissileServerCall>c__AnonStorey1::defendTime
	DateTime_t693205669  ___defendTime_1;
	// DefendMiniGameController DefendMiniGameController/<SendDefendMissileServerCall>c__AnonStorey1::$this
	DefendMiniGameController_t1844356491 * ___U24this_2;

public:
	inline static int32_t get_offset_of_defendSuccess_0() { return static_cast<int32_t>(offsetof(U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571, ___defendSuccess_0)); }
	inline bool get_defendSuccess_0() const { return ___defendSuccess_0; }
	inline bool* get_address_of_defendSuccess_0() { return &___defendSuccess_0; }
	inline void set_defendSuccess_0(bool value)
	{
		___defendSuccess_0 = value;
	}

	inline static int32_t get_offset_of_defendTime_1() { return static_cast<int32_t>(offsetof(U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571, ___defendTime_1)); }
	inline DateTime_t693205669  get_defendTime_1() const { return ___defendTime_1; }
	inline DateTime_t693205669 * get_address_of_defendTime_1() { return &___defendTime_1; }
	inline void set_defendTime_1(DateTime_t693205669  value)
	{
		___defendTime_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571, ___U24this_2)); }
	inline DefendMiniGameController_t1844356491 * get_U24this_2() const { return ___U24this_2; }
	inline DefendMiniGameController_t1844356491 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(DefendMiniGameController_t1844356491 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
