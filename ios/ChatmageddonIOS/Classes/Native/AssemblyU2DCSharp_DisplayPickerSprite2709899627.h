﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPSpritePicker
struct IPSpritePicker_t2623787062;
// UISprite
struct UISprite_t603616735;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_DisplayPickerSprite_DisplayMode481136365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisplayPickerSprite
struct  DisplayPickerSprite_t2709899627  : public MonoBehaviour_t1158329972
{
public:
	// IPSpritePicker DisplayPickerSprite::picker
	IPSpritePicker_t2623787062 * ___picker_2;
	// DisplayPickerSprite/DisplayMode DisplayPickerSprite::displayMode
	int32_t ___displayMode_3;
	// System.Single DisplayPickerSprite::normalizedMax
	float ___normalizedMax_4;
	// UISprite DisplayPickerSprite::_sprite
	UISprite_t603616735 * ____sprite_5;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(DisplayPickerSprite_t2709899627, ___picker_2)); }
	inline IPSpritePicker_t2623787062 * get_picker_2() const { return ___picker_2; }
	inline IPSpritePicker_t2623787062 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(IPSpritePicker_t2623787062 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier(&___picker_2, value);
	}

	inline static int32_t get_offset_of_displayMode_3() { return static_cast<int32_t>(offsetof(DisplayPickerSprite_t2709899627, ___displayMode_3)); }
	inline int32_t get_displayMode_3() const { return ___displayMode_3; }
	inline int32_t* get_address_of_displayMode_3() { return &___displayMode_3; }
	inline void set_displayMode_3(int32_t value)
	{
		___displayMode_3 = value;
	}

	inline static int32_t get_offset_of_normalizedMax_4() { return static_cast<int32_t>(offsetof(DisplayPickerSprite_t2709899627, ___normalizedMax_4)); }
	inline float get_normalizedMax_4() const { return ___normalizedMax_4; }
	inline float* get_address_of_normalizedMax_4() { return &___normalizedMax_4; }
	inline void set_normalizedMax_4(float value)
	{
		___normalizedMax_4 = value;
	}

	inline static int32_t get_offset_of__sprite_5() { return static_cast<int32_t>(offsetof(DisplayPickerSprite_t2709899627, ____sprite_5)); }
	inline UISprite_t603616735 * get__sprite_5() const { return ____sprite_5; }
	inline UISprite_t603616735 ** get_address_of__sprite_5() { return &____sprite_5; }
	inline void set__sprite_5(UISprite_t603616735 * value)
	{
		____sprite_5 = value;
		Il2CppCodeGenWriteBarrier(&____sprite_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
