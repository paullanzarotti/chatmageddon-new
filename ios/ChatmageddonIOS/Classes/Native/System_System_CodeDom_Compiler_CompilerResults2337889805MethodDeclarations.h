﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CodeDom.Compiler.CompilerResults
struct CompilerResults_t2337889805;
// System.CodeDom.Compiler.TempFileCollection
struct TempFileCollection_t3377240462;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.CodeDom.Compiler.CompilerErrorCollection
struct CompilerErrorCollection_t2852289537;
// System.Collections.Specialized.StringCollection
struct StringCollection_t352985975;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_CodeDom_Compiler_TempFileCollection3377240462.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.CodeDom.Compiler.CompilerResults::.ctor(System.CodeDom.Compiler.TempFileCollection)
extern "C"  void CompilerResults__ctor_m2496397175 (CompilerResults_t2337889805 * __this, TempFileCollection_t3377240462 * ___tempFiles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.CodeDom.Compiler.CompilerResults::get_CompiledAssembly()
extern "C"  Assembly_t4268412390 * CompilerResults_get_CompiledAssembly_m947517984 (CompilerResults_t2337889805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerResults::set_CompiledAssembly(System.Reflection.Assembly)
extern "C"  void CompilerResults_set_CompiledAssembly_m1370679830 (CompilerResults_t2337889805 * __this, Assembly_t4268412390 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.CodeDom.Compiler.CompilerErrorCollection System.CodeDom.Compiler.CompilerResults::get_Errors()
extern "C"  CompilerErrorCollection_t2852289537 * CompilerResults_get_Errors_m1865176316 (CompilerResults_t2337889805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerResults::set_NativeCompilerReturnValue(System.Int32)
extern "C"  void CompilerResults_set_NativeCompilerReturnValue_m2193652023 (CompilerResults_t2337889805 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerResults::set_Output(System.Collections.Specialized.StringCollection)
extern "C"  void CompilerResults_set_Output_m3465224780 (CompilerResults_t2337889805 * __this, StringCollection_t352985975 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CodeDom.Compiler.CompilerResults::set_PathToAssembly(System.String)
extern "C"  void CompilerResults_set_PathToAssembly_m3848053287 (CompilerResults_t2337889805 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
