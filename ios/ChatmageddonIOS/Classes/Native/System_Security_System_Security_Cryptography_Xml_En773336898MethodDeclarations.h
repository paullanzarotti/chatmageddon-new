﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptionProperty
struct EncryptionProperty_t773336898;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"

// System.Void System.Security.Cryptography.Xml.EncryptionProperty::.ctor(System.Xml.XmlElement)
extern "C"  void EncryptionProperty__ctor_m2675174237 (EncryptionProperty_t773336898 * __this, XmlElement_t2877111883 * ___elemProp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptionProperty::get_Id()
extern "C"  String_t* EncryptionProperty_get_Id_m383272859 (EncryptionProperty_t773336898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.Xml.EncryptionProperty::get_Target()
extern "C"  String_t* EncryptionProperty_get_Target_m3749930055 (EncryptionProperty_t773336898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptionProperty::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * EncryptionProperty_GetXml_m1125636387 (EncryptionProperty_t773336898 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptionProperty::LoadXml(System.Xml.XmlElement)
extern "C"  void EncryptionProperty_LoadXml_m3485821982 (EncryptionProperty_t773336898 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
