﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InternetReachability/<InternetPollCheck>c__Iterator0/<InternetPollCheck>c__AnonStorey1
struct U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void InternetReachability/<InternetPollCheck>c__Iterator0/<InternetPollCheck>c__AnonStorey1::.ctor()
extern "C"  void U3CInternetPollCheckU3Ec__AnonStorey1__ctor_m1317522796 (U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability/<InternetPollCheck>c__Iterator0/<InternetPollCheck>c__AnonStorey1::<>m__0(System.Boolean,System.String)
extern "C"  void U3CInternetPollCheckU3Ec__AnonStorey1_U3CU3Em__0_m935134766 (U3CInternetPollCheckU3Ec__AnonStorey1_t3087003569 * __this, bool ___isConnected0, String_t* ___errorCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
