﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatContactsILP
struct ChatContactsILP_t1780035494;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// ChatContact
struct ChatContact_t3760700014;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "AssemblyU2DCSharp_ChatContact3760700014.h"

// System.Void ChatContactsILP::.ctor()
extern "C"  void ChatContactsILP__ctor_m3595186635 (ChatContactsILP_t1780035494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsILP::StartIL(System.Boolean)
extern "C"  void ChatContactsILP_StartIL_m4029191105 (ChatContactsILP_t1780035494 * __this, bool ___search0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsILP::SetDataArray(System.Boolean)
extern "C"  void ChatContactsILP_SetDataArray_m639510815 (ChatContactsILP_t1780035494 * __this, bool ___search0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsILP::RepositionList()
extern "C"  void ChatContactsILP_RepositionList_m543584611 (ChatContactsILP_t1780035494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void ChatContactsILP_InitListItemWithIndex_m4044601865 (ChatContactsILP_t1780035494 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatContactsILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void ChatContactsILP_PopulateListItemWithIndex_m3195890366 (ChatContactsILP_t1780035494 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ChatContactsILP::<SetDataArray>m__0(ChatContact)
extern "C"  String_t* ChatContactsILP_U3CSetDataArrayU3Em__0_m1878000340 (Il2CppObject * __this /* static, unused */, ChatContact_t3760700014 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
