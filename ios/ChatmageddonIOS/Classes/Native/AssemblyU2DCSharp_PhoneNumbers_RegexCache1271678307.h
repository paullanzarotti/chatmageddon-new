﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.LinkedList`1<System.String>
struct LinkedList_1_t2333928462;
// System.Collections.Generic.Dictionary`2<System.String,PhoneNumbers.RegexCache/Entry>
struct Dictionary_2_t3434145254;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.RegexCache
struct  RegexCache_t1271678307  : public Il2CppObject
{
public:
	// System.Int32 PhoneNumbers.RegexCache::size_
	int32_t ___size__0;
	// System.Collections.Generic.LinkedList`1<System.String> PhoneNumbers.RegexCache::lru_
	LinkedList_1_t2333928462 * ___lru__1;
	// System.Collections.Generic.Dictionary`2<System.String,PhoneNumbers.RegexCache/Entry> PhoneNumbers.RegexCache::cache_
	Dictionary_2_t3434145254 * ___cache__2;

public:
	inline static int32_t get_offset_of_size__0() { return static_cast<int32_t>(offsetof(RegexCache_t1271678307, ___size__0)); }
	inline int32_t get_size__0() const { return ___size__0; }
	inline int32_t* get_address_of_size__0() { return &___size__0; }
	inline void set_size__0(int32_t value)
	{
		___size__0 = value;
	}

	inline static int32_t get_offset_of_lru__1() { return static_cast<int32_t>(offsetof(RegexCache_t1271678307, ___lru__1)); }
	inline LinkedList_1_t2333928462 * get_lru__1() const { return ___lru__1; }
	inline LinkedList_1_t2333928462 ** get_address_of_lru__1() { return &___lru__1; }
	inline void set_lru__1(LinkedList_1_t2333928462 * value)
	{
		___lru__1 = value;
		Il2CppCodeGenWriteBarrier(&___lru__1, value);
	}

	inline static int32_t get_offset_of_cache__2() { return static_cast<int32_t>(offsetof(RegexCache_t1271678307, ___cache__2)); }
	inline Dictionary_2_t3434145254 * get_cache__2() const { return ___cache__2; }
	inline Dictionary_2_t3434145254 ** get_address_of_cache__2() { return &___cache__2; }
	inline void set_cache__2(Dictionary_2_t3434145254 * value)
	{
		___cache__2 = value;
		Il2CppCodeGenWriteBarrier(&___cache__2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
