﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonGamestate
struct ChatmageddonGamestate_t808689860;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatmageddonGamestate::.ctor()
extern "C"  void ChatmageddonGamestate__ctor_m754701287 (ChatmageddonGamestate_t808689860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonGamestate::PreLevelLoad()
extern "C"  void ChatmageddonGamestate_PreLevelLoad_m3114820190 (ChatmageddonGamestate_t808689860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonGamestate::DontDestroyOnLoad()
extern "C"  void ChatmageddonGamestate_DontDestroyOnLoad_m2983885875 (ChatmageddonGamestate_t808689860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
