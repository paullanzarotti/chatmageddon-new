﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<StatusManager>::.ctor()
#define MonoSingleton_1__ctor_m3811902387(__this, method) ((  void (*) (MonoSingleton_1_t2470912757 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<StatusManager>::Awake()
#define MonoSingleton_1_Awake_m3179915984(__this, method) ((  void (*) (MonoSingleton_1_t2470912757 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<StatusManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m821239464(__this /* static, unused */, method) ((  StatusManager_t2720247037 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<StatusManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1135699226(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<StatusManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m507328713(__this, method) ((  void (*) (MonoSingleton_1_t2470912757 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<StatusManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m3049275753(__this, method) ((  void (*) (MonoSingleton_1_t2470912757 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<StatusManager>::.cctor()
#define MonoSingleton_1__cctor_m1632214258(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
