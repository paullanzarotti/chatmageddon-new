﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// QuestionUI
struct QuestionUI_t3721436426;

#include "codegen/il2cpp-codegen.h"

// System.Void QuestionUI::.ctor()
extern "C"  void QuestionUI__ctor_m3352419881 (QuestionUI_t3721436426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void QuestionUI::LoadUI()
extern "C"  void QuestionUI_LoadUI_m142751027 (QuestionUI_t3721436426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
