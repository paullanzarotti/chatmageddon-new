﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Material193706927.h"

// System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_fogColor_m3716873294 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2591134422 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m2565496707 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
