﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatThread
struct ChatThread_t2394323482;
// Friend
struct Friend_t3555014108;
// Missile
struct Missile_t813944928;
// LaunchedMissile
struct LaunchedMissile_t1542515810;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1861297139.h"
#include "AssemblyU2DCSharp_Scene4200804392.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"
#include "AssemblyU2DCSharp_AttackNavScreen36759459.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApplicationManager
struct  ApplicationManager_t2110631419  : public MonoSingleton_1_t1861297139
{
public:
	// System.Boolean ApplicationManager::forcedLogout
	bool ___forcedLogout_3;
	// System.Boolean ApplicationManager::firstGameStart
	bool ___firstGameStart_4;
	// Scene ApplicationManager::sceneLoadingFrom
	int32_t ___sceneLoadingFrom_5;
	// ChatNavScreen ApplicationManager::chatStartScreen
	int32_t ___chatStartScreen_6;
	// ChatThread ApplicationManager::chatStartThread
	ChatThread_t2394323482 * ___chatStartThread_7;
	// Friend ApplicationManager::chatStartFriend
	Friend_t3555014108 * ___chatStartFriend_8;
	// AttackNavScreen ApplicationManager::startAttackNav
	int32_t ___startAttackNav_9;
	// AttackHomeNav ApplicationManager::startAttackHomeNav
	int32_t ___startAttackHomeNav_10;
	// Missile ApplicationManager::activeMissile
	Missile_t813944928 * ___activeMissile_11;
	// Friend ApplicationManager::activeTarget
	Friend_t3555014108 * ___activeTarget_12;
	// LaunchedMissile ApplicationManager::firedMissile
	LaunchedMissile_t1542515810 * ___firedMissile_13;
	// System.Int32 ApplicationManager::firedAccuracy
	int32_t ___firedAccuracy_14;

public:
	inline static int32_t get_offset_of_forcedLogout_3() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___forcedLogout_3)); }
	inline bool get_forcedLogout_3() const { return ___forcedLogout_3; }
	inline bool* get_address_of_forcedLogout_3() { return &___forcedLogout_3; }
	inline void set_forcedLogout_3(bool value)
	{
		___forcedLogout_3 = value;
	}

	inline static int32_t get_offset_of_firstGameStart_4() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___firstGameStart_4)); }
	inline bool get_firstGameStart_4() const { return ___firstGameStart_4; }
	inline bool* get_address_of_firstGameStart_4() { return &___firstGameStart_4; }
	inline void set_firstGameStart_4(bool value)
	{
		___firstGameStart_4 = value;
	}

	inline static int32_t get_offset_of_sceneLoadingFrom_5() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___sceneLoadingFrom_5)); }
	inline int32_t get_sceneLoadingFrom_5() const { return ___sceneLoadingFrom_5; }
	inline int32_t* get_address_of_sceneLoadingFrom_5() { return &___sceneLoadingFrom_5; }
	inline void set_sceneLoadingFrom_5(int32_t value)
	{
		___sceneLoadingFrom_5 = value;
	}

	inline static int32_t get_offset_of_chatStartScreen_6() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___chatStartScreen_6)); }
	inline int32_t get_chatStartScreen_6() const { return ___chatStartScreen_6; }
	inline int32_t* get_address_of_chatStartScreen_6() { return &___chatStartScreen_6; }
	inline void set_chatStartScreen_6(int32_t value)
	{
		___chatStartScreen_6 = value;
	}

	inline static int32_t get_offset_of_chatStartThread_7() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___chatStartThread_7)); }
	inline ChatThread_t2394323482 * get_chatStartThread_7() const { return ___chatStartThread_7; }
	inline ChatThread_t2394323482 ** get_address_of_chatStartThread_7() { return &___chatStartThread_7; }
	inline void set_chatStartThread_7(ChatThread_t2394323482 * value)
	{
		___chatStartThread_7 = value;
		Il2CppCodeGenWriteBarrier(&___chatStartThread_7, value);
	}

	inline static int32_t get_offset_of_chatStartFriend_8() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___chatStartFriend_8)); }
	inline Friend_t3555014108 * get_chatStartFriend_8() const { return ___chatStartFriend_8; }
	inline Friend_t3555014108 ** get_address_of_chatStartFriend_8() { return &___chatStartFriend_8; }
	inline void set_chatStartFriend_8(Friend_t3555014108 * value)
	{
		___chatStartFriend_8 = value;
		Il2CppCodeGenWriteBarrier(&___chatStartFriend_8, value);
	}

	inline static int32_t get_offset_of_startAttackNav_9() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___startAttackNav_9)); }
	inline int32_t get_startAttackNav_9() const { return ___startAttackNav_9; }
	inline int32_t* get_address_of_startAttackNav_9() { return &___startAttackNav_9; }
	inline void set_startAttackNav_9(int32_t value)
	{
		___startAttackNav_9 = value;
	}

	inline static int32_t get_offset_of_startAttackHomeNav_10() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___startAttackHomeNav_10)); }
	inline int32_t get_startAttackHomeNav_10() const { return ___startAttackHomeNav_10; }
	inline int32_t* get_address_of_startAttackHomeNav_10() { return &___startAttackHomeNav_10; }
	inline void set_startAttackHomeNav_10(int32_t value)
	{
		___startAttackHomeNav_10 = value;
	}

	inline static int32_t get_offset_of_activeMissile_11() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___activeMissile_11)); }
	inline Missile_t813944928 * get_activeMissile_11() const { return ___activeMissile_11; }
	inline Missile_t813944928 ** get_address_of_activeMissile_11() { return &___activeMissile_11; }
	inline void set_activeMissile_11(Missile_t813944928 * value)
	{
		___activeMissile_11 = value;
		Il2CppCodeGenWriteBarrier(&___activeMissile_11, value);
	}

	inline static int32_t get_offset_of_activeTarget_12() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___activeTarget_12)); }
	inline Friend_t3555014108 * get_activeTarget_12() const { return ___activeTarget_12; }
	inline Friend_t3555014108 ** get_address_of_activeTarget_12() { return &___activeTarget_12; }
	inline void set_activeTarget_12(Friend_t3555014108 * value)
	{
		___activeTarget_12 = value;
		Il2CppCodeGenWriteBarrier(&___activeTarget_12, value);
	}

	inline static int32_t get_offset_of_firedMissile_13() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___firedMissile_13)); }
	inline LaunchedMissile_t1542515810 * get_firedMissile_13() const { return ___firedMissile_13; }
	inline LaunchedMissile_t1542515810 ** get_address_of_firedMissile_13() { return &___firedMissile_13; }
	inline void set_firedMissile_13(LaunchedMissile_t1542515810 * value)
	{
		___firedMissile_13 = value;
		Il2CppCodeGenWriteBarrier(&___firedMissile_13, value);
	}

	inline static int32_t get_offset_of_firedAccuracy_14() { return static_cast<int32_t>(offsetof(ApplicationManager_t2110631419, ___firedAccuracy_14)); }
	inline int32_t get_firedAccuracy_14() const { return ___firedAccuracy_14; }
	inline int32_t* get_address_of_firedAccuracy_14() { return &___firedAccuracy_14; }
	inline void set_firedAccuracy_14(int32_t value)
	{
		___firedAccuracy_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
