﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPTextPicker
struct IPTextPicker_t3074051364;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IPTextPicker::.ctor()
extern "C"  void IPTextPicker__ctor_m1373586821 (IPTextPicker_t3074051364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IPTextPicker::get_CurrentLabelText()
extern "C"  String_t* IPTextPicker_get_CurrentLabelText_m3582367775 (IPTextPicker_t3074051364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::set_CurrentLabelText(System.String)
extern "C"  void IPTextPicker_set_CurrentLabelText_m3860710630 (IPTextPicker_t3074051364 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::InsertElement(System.String)
extern "C"  void IPTextPicker_InsertElement_m682701454 (IPTextPicker_t3074051364 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::InsertElementAtIndex(System.String,System.Int32,System.Boolean)
extern "C"  void IPTextPicker_InsertElementAtIndex_m3274588559 (IPTextPicker_t3074051364 * __this, String_t* ___text0, int32_t ___index1, bool ___resetPicker2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::RemoveElementAtIndex(System.Int32,System.Boolean)
extern "C"  void IPTextPicker_RemoveElementAtIndex_m3053762312 (IPTextPicker_t3074051364 * __this, int32_t ___index0, bool ___resetPicker1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::ResetPickerAtText(System.String)
extern "C"  void IPTextPicker_ResetPickerAtText_m197490378 (IPTextPicker_t3074051364 * __this, String_t* ___labelText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::ResetPickerAtContentIndex(System.Int32)
extern "C"  void IPTextPicker_ResetPickerAtContentIndex_m1360300657 (IPTextPicker_t3074051364 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPTextPicker::GetInitIndex()
extern "C"  int32_t IPTextPicker_GetInitIndex_m1885330421 (IPTextPicker_t3074051364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::UpdateCurrentValue()
extern "C"  void IPTextPicker_UpdateCurrentValue_m1833224064 (IPTextPicker_t3074051364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::UpdateWidget(System.Int32,System.Int32)
extern "C"  void IPTextPicker_UpdateWidget_m1035974146 (IPTextPicker_t3074051364 * __this, int32_t ___widgetIndex0, int32_t ___contentIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPTextPicker::UpdateVirtualElementsCount()
extern "C"  void IPTextPicker_UpdateVirtualElementsCount_m844811911 (IPTextPicker_t3074051364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
