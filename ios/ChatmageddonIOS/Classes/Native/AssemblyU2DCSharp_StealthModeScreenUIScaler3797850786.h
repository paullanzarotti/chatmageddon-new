﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StealthModeScreenUIScaler
struct  StealthModeScreenUIScaler_t3797850786  : public ChatmageddonUIScaler_t3374094653
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> StealthModeScreenUIScaler::leftTransforms
	List_1_t2644239190 * ___leftTransforms_14;

public:
	inline static int32_t get_offset_of_leftTransforms_14() { return static_cast<int32_t>(offsetof(StealthModeScreenUIScaler_t3797850786, ___leftTransforms_14)); }
	inline List_1_t2644239190 * get_leftTransforms_14() const { return ___leftTransforms_14; }
	inline List_1_t2644239190 ** get_address_of_leftTransforms_14() { return &___leftTransforms_14; }
	inline void set_leftTransforms_14(List_1_t2644239190 * value)
	{
		___leftTransforms_14 = value;
		Il2CppCodeGenWriteBarrier(&___leftTransforms_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
