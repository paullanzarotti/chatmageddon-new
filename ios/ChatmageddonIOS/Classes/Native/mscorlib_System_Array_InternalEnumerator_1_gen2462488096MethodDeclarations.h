﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2462488096.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21603735834.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1029876120_gshared (InternalEnumerator_1_t2462488096 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1029876120(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2462488096 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1029876120_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2888069968_gshared (InternalEnumerator_1_t2462488096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2888069968(__this, method) ((  void (*) (InternalEnumerator_1_t2462488096 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2888069968_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2221339356_gshared (InternalEnumerator_1_t2462488096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2221339356(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2462488096 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2221339356_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m517160127_gshared (InternalEnumerator_1_t2462488096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m517160127(__this, method) ((  void (*) (InternalEnumerator_1_t2462488096 *, const MethodInfo*))InternalEnumerator_1_Dispose_m517160127_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2147424464_gshared (InternalEnumerator_1_t2462488096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2147424464(__this, method) ((  bool (*) (InternalEnumerator_1_t2462488096 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2147424464_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PanelType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1603735834  InternalEnumerator_1_get_Current_m2701637559_gshared (InternalEnumerator_1_t2462488096 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2701637559(__this, method) ((  KeyValuePair_2_t1603735834  (*) (InternalEnumerator_1_t2462488096 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2701637559_gshared)(__this, method)
