﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>
struct ReadOnlyCollection_1_t2452912926;
// System.Collections.Generic.IList`1<SettingsNavScreen>
struct IList_1_t2808067835;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// SettingsNavScreen[]
struct SettingsNavScreenU5BU5D_t655014135;
// System.Collections.Generic.IEnumerator`1<SettingsNavScreen>
struct IEnumerator_1_t4037618357;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1023363810_gshared (ReadOnlyCollection_1_t2452912926 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1023363810(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1023363810_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1239948270_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1239948270(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1239948270_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1889985234_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1889985234(__this, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1889985234_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2333052877_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2333052877(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2333052877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1638105853_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1638105853(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1638105853_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3352873321_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3352873321(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3352873321_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2131685061_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2131685061(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2131685061_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m321731796_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m321731796(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m321731796_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m411998852_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m411998852(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m411998852_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1523496835_gshared (ReadOnlyCollection_1_t2452912926 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1523496835(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1523496835_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3812656616_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3812656616(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3812656616_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1802462595_gshared (ReadOnlyCollection_1_t2452912926 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1802462595(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2452912926 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1802462595_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m64801187_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m64801187(__this, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m64801187_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1449246043_gshared (ReadOnlyCollection_1_t2452912926 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1449246043(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2452912926 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1449246043_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3733607741_gshared (ReadOnlyCollection_1_t2452912926 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3733607741(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2452912926 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3733607741_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3531449490_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3531449490(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3531449490_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m444332188_gshared (ReadOnlyCollection_1_t2452912926 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m444332188(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m444332188_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4118132624_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4118132624(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4118132624_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1604456931_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1604456931(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1604456931_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m36007043_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m36007043(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m36007043_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2703714286_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2703714286(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2703714286_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1631160087_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1631160087(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1631160087_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2797005866_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2797005866(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2797005866_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m4047446429_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4047446429(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4047446429_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3076902456_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3076902456(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3076902456_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2370221090_gshared (ReadOnlyCollection_1_t2452912926 * __this, SettingsNavScreenU5BU5D_t655014135* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2370221090(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2452912926 *, SettingsNavScreenU5BU5D_t655014135*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2370221090_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2229639907_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2229639907(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2229639907_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m717710724_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m717710724(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m717710724_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m804185507_gshared (ReadOnlyCollection_1_t2452912926 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m804185507(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2452912926 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m804185507_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SettingsNavScreen>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m3149353625_gshared (ReadOnlyCollection_1_t2452912926 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3149353625(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2452912926 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3149353625_gshared)(__this, ___index0, method)
