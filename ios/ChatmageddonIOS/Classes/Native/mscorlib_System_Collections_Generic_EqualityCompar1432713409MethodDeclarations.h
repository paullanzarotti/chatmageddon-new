﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<Vectrosity.Vector3Pair>
struct EqualityComparer_1_t1432713409;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Vectrosity.Vector3Pair>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m729848249_gshared (EqualityComparer_1_t1432713409 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m729848249(__this, method) ((  void (*) (EqualityComparer_1_t1432713409 *, const MethodInfo*))EqualityComparer_1__ctor_m729848249_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Vectrosity.Vector3Pair>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m3812664474_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m3812664474(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m3812664474_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vectrosity.Vector3Pair>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1789220516_gshared (EqualityComparer_1_t1432713409 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1789220516(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t1432713409 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1789220516_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vectrosity.Vector3Pair>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1591829546_gshared (EqualityComparer_1_t1432713409 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1591829546(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t1432713409 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1591829546_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vectrosity.Vector3Pair>::get_Default()
extern "C"  EqualityComparer_1_t1432713409 * EqualityComparer_1_get_Default_m2974302485_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m2974302485(__this /* static, unused */, method) ((  EqualityComparer_1_t1432713409 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m2974302485_gshared)(__this /* static, unused */, method)
