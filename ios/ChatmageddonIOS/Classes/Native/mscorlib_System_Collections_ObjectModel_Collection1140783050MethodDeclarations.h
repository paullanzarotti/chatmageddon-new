﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>
struct Collection_1_t1140783050;
// System.Collections.Generic.IList`1<FriendHomeNavScreen>
struct IList_1_t2139978897;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FriendHomeNavScreen[]
struct FriendHomeNavScreenU5BU5D_t1773644873;
// System.Collections.Generic.IEnumerator`1<FriendHomeNavScreen>
struct IEnumerator_1_t3369529419;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"

// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m3164418529_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3164418529(__this, method) ((  void (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1__ctor_m3164418529_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m3861591978_gshared (Collection_1_t1140783050 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m3861591978(__this, ___list0, method) ((  void (*) (Collection_1_t1140783050 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3861591978_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3382535220_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3382535220(__this, method) ((  bool (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3382535220_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m191176977_gshared (Collection_1_t1140783050 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m191176977(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1140783050 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m191176977_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2188079400_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2188079400(__this, method) ((  Il2CppObject * (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2188079400_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m510416717_gshared (Collection_1_t1140783050 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m510416717(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1140783050 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m510416717_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2445320833_gshared (Collection_1_t1140783050 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2445320833(__this, ___value0, method) ((  bool (*) (Collection_1_t1140783050 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2445320833_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2798937311_gshared (Collection_1_t1140783050 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2798937311(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1140783050 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2798937311_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m471547230_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m471547230(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m471547230_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2174926796_gshared (Collection_1_t1140783050 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2174926796(__this, ___value0, method) ((  void (*) (Collection_1_t1140783050 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2174926796_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3632946645_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3632946645(__this, method) ((  bool (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3632946645_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1814908481_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1814908481(__this, method) ((  Il2CppObject * (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1814908481_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1724789030_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1724789030(__this, method) ((  bool (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1724789030_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3804061745_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3804061745(__this, method) ((  bool (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3804061745_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2444500330_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2444500330(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2444500330_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m4196799095_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m4196799095(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m4196799095_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m3770995282_gshared (Collection_1_t1140783050 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m3770995282(__this, ___item0, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_Add_m3770995282_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m1736118350_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1736118350(__this, method) ((  void (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_Clear_m1736118350_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1814637136_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1814637136(__this, method) ((  void (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_ClearItems_m1814637136_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m353438232_gshared (Collection_1_t1140783050 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m353438232(__this, ___item0, method) ((  bool (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_Contains_m353438232_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m4160828322_gshared (Collection_1_t1140783050 * __this, FriendHomeNavScreenU5BU5D_t1773644873* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m4160828322(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1140783050 *, FriendHomeNavScreenU5BU5D_t1773644873*, int32_t, const MethodInfo*))Collection_1_CopyTo_m4160828322_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2903188509_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2903188509(__this, method) ((  Il2CppObject* (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_GetEnumerator_m2903188509_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m480551136_gshared (Collection_1_t1140783050 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m480551136(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m480551136_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3245707287_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3245707287(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m3245707287_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m423121228_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m423121228(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m423121228_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m4095407979_gshared (Collection_1_t1140783050 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m4095407979(__this, ___item0, method) ((  bool (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_Remove_m4095407979_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3780903411_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3780903411(__this, ___index0, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3780903411_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4198057599_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4198057599(__this, ___index0, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4198057599_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2259740189_gshared (Collection_1_t1140783050 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2259740189(__this, method) ((  int32_t (*) (Collection_1_t1140783050 *, const MethodInfo*))Collection_1_get_Count_m2259740189_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m535214587_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m535214587(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1140783050 *, int32_t, const MethodInfo*))Collection_1_get_Item_m535214587_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3779075720_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3779075720(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m3779075720_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4089029463_gshared (Collection_1_t1140783050 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4089029463(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1140783050 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m4089029463_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m692701418_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m692701418(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m692701418_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m799393408_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m799393408(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m799393408_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m4035283090_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m4035283090(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m4035283090_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1287229448_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1287229448(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1287229448_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendHomeNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m346799351_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m346799351(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m346799351_gshared)(__this /* static, unused */, ___list0, method)
