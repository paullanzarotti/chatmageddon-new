﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeactivateShield
struct  DeactivateShield_t2656914733  : public SFXButton_t792651341
{
public:
	// System.Boolean DeactivateShield::missileShield
	bool ___missileShield_5;

public:
	inline static int32_t get_offset_of_missileShield_5() { return static_cast<int32_t>(offsetof(DeactivateShield_t2656914733, ___missileShield_5)); }
	inline bool get_missileShield_5() const { return ___missileShield_5; }
	inline bool* get_address_of_missileShield_5() { return &___missileShield_5; }
	inline void set_missileShield_5(bool value)
	{
		___missileShield_5 = value;
	}
};

struct DeactivateShield_t2656914733_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> DeactivateShield::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_6;
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> DeactivateShield::<>f__am$cache1
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(DeactivateShield_t2656914733_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(DeactivateShield_t2656914733_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
