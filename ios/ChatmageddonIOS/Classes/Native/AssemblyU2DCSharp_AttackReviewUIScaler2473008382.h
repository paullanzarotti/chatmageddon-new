﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3275118058;
// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackReviewUIScaler
struct  AttackReviewUIScaler_t2473008382  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.Transform AttackReviewUIScaler::itemScaleMeasure
	Transform_t3275118058 * ___itemScaleMeasure_14;
	// UISprite AttackReviewUIScaler::launchButton
	UISprite_t603616735 * ___launchButton_15;
	// UnityEngine.BoxCollider AttackReviewUIScaler::launchCollider
	BoxCollider_t22920061 * ___launchCollider_16;
	// UISprite AttackReviewUIScaler::buyMoreButton
	UISprite_t603616735 * ___buyMoreButton_17;
	// UILabel AttackReviewUIScaler::buyMoreLabel
	UILabel_t1795115428 * ___buyMoreLabel_18;
	// UnityEngine.BoxCollider AttackReviewUIScaler::buyMoreCollider
	BoxCollider_t22920061 * ___buyMoreCollider_19;
	// UISprite AttackReviewUIScaler::targetsButton
	UISprite_t603616735 * ___targetsButton_20;
	// UILabel AttackReviewUIScaler::targetsLabel
	UILabel_t1795115428 * ___targetsLabel_21;
	// UnityEngine.BoxCollider AttackReviewUIScaler::targetsCollider
	BoxCollider_t22920061 * ___targetsCollider_22;
	// UISprite AttackReviewUIScaler::topHorizLine
	UISprite_t603616735 * ___topHorizLine_23;
	// UISprite AttackReviewUIScaler::botHorizLine
	UISprite_t603616735 * ___botHorizLine_24;

public:
	inline static int32_t get_offset_of_itemScaleMeasure_14() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___itemScaleMeasure_14)); }
	inline Transform_t3275118058 * get_itemScaleMeasure_14() const { return ___itemScaleMeasure_14; }
	inline Transform_t3275118058 ** get_address_of_itemScaleMeasure_14() { return &___itemScaleMeasure_14; }
	inline void set_itemScaleMeasure_14(Transform_t3275118058 * value)
	{
		___itemScaleMeasure_14 = value;
		Il2CppCodeGenWriteBarrier(&___itemScaleMeasure_14, value);
	}

	inline static int32_t get_offset_of_launchButton_15() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___launchButton_15)); }
	inline UISprite_t603616735 * get_launchButton_15() const { return ___launchButton_15; }
	inline UISprite_t603616735 ** get_address_of_launchButton_15() { return &___launchButton_15; }
	inline void set_launchButton_15(UISprite_t603616735 * value)
	{
		___launchButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___launchButton_15, value);
	}

	inline static int32_t get_offset_of_launchCollider_16() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___launchCollider_16)); }
	inline BoxCollider_t22920061 * get_launchCollider_16() const { return ___launchCollider_16; }
	inline BoxCollider_t22920061 ** get_address_of_launchCollider_16() { return &___launchCollider_16; }
	inline void set_launchCollider_16(BoxCollider_t22920061 * value)
	{
		___launchCollider_16 = value;
		Il2CppCodeGenWriteBarrier(&___launchCollider_16, value);
	}

	inline static int32_t get_offset_of_buyMoreButton_17() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___buyMoreButton_17)); }
	inline UISprite_t603616735 * get_buyMoreButton_17() const { return ___buyMoreButton_17; }
	inline UISprite_t603616735 ** get_address_of_buyMoreButton_17() { return &___buyMoreButton_17; }
	inline void set_buyMoreButton_17(UISprite_t603616735 * value)
	{
		___buyMoreButton_17 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreButton_17, value);
	}

	inline static int32_t get_offset_of_buyMoreLabel_18() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___buyMoreLabel_18)); }
	inline UILabel_t1795115428 * get_buyMoreLabel_18() const { return ___buyMoreLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_buyMoreLabel_18() { return &___buyMoreLabel_18; }
	inline void set_buyMoreLabel_18(UILabel_t1795115428 * value)
	{
		___buyMoreLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreLabel_18, value);
	}

	inline static int32_t get_offset_of_buyMoreCollider_19() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___buyMoreCollider_19)); }
	inline BoxCollider_t22920061 * get_buyMoreCollider_19() const { return ___buyMoreCollider_19; }
	inline BoxCollider_t22920061 ** get_address_of_buyMoreCollider_19() { return &___buyMoreCollider_19; }
	inline void set_buyMoreCollider_19(BoxCollider_t22920061 * value)
	{
		___buyMoreCollider_19 = value;
		Il2CppCodeGenWriteBarrier(&___buyMoreCollider_19, value);
	}

	inline static int32_t get_offset_of_targetsButton_20() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___targetsButton_20)); }
	inline UISprite_t603616735 * get_targetsButton_20() const { return ___targetsButton_20; }
	inline UISprite_t603616735 ** get_address_of_targetsButton_20() { return &___targetsButton_20; }
	inline void set_targetsButton_20(UISprite_t603616735 * value)
	{
		___targetsButton_20 = value;
		Il2CppCodeGenWriteBarrier(&___targetsButton_20, value);
	}

	inline static int32_t get_offset_of_targetsLabel_21() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___targetsLabel_21)); }
	inline UILabel_t1795115428 * get_targetsLabel_21() const { return ___targetsLabel_21; }
	inline UILabel_t1795115428 ** get_address_of_targetsLabel_21() { return &___targetsLabel_21; }
	inline void set_targetsLabel_21(UILabel_t1795115428 * value)
	{
		___targetsLabel_21 = value;
		Il2CppCodeGenWriteBarrier(&___targetsLabel_21, value);
	}

	inline static int32_t get_offset_of_targetsCollider_22() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___targetsCollider_22)); }
	inline BoxCollider_t22920061 * get_targetsCollider_22() const { return ___targetsCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_targetsCollider_22() { return &___targetsCollider_22; }
	inline void set_targetsCollider_22(BoxCollider_t22920061 * value)
	{
		___targetsCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___targetsCollider_22, value);
	}

	inline static int32_t get_offset_of_topHorizLine_23() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___topHorizLine_23)); }
	inline UISprite_t603616735 * get_topHorizLine_23() const { return ___topHorizLine_23; }
	inline UISprite_t603616735 ** get_address_of_topHorizLine_23() { return &___topHorizLine_23; }
	inline void set_topHorizLine_23(UISprite_t603616735 * value)
	{
		___topHorizLine_23 = value;
		Il2CppCodeGenWriteBarrier(&___topHorizLine_23, value);
	}

	inline static int32_t get_offset_of_botHorizLine_24() { return static_cast<int32_t>(offsetof(AttackReviewUIScaler_t2473008382, ___botHorizLine_24)); }
	inline UISprite_t603616735 * get_botHorizLine_24() const { return ___botHorizLine_24; }
	inline UISprite_t603616735 ** get_address_of_botHorizLine_24() { return &___botHorizLine_24; }
	inline void set_botHorizLine_24(UISprite_t603616735 * value)
	{
		___botHorizLine_24 = value;
		Il2CppCodeGenWriteBarrier(&___botHorizLine_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
