﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.PhoneNumberMatcher/CheckGroups
struct CheckGroups_t2785688338;
// System.Object
struct Il2CppObject;
// PhoneNumbers.PhoneNumberUtil
struct PhoneNumberUtil_t4155573397;
// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumberUtil4155573397.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber814071929.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void PhoneNumbers.PhoneNumberMatcher/CheckGroups::.ctor(System.Object,System.IntPtr)
extern "C"  void CheckGroups__ctor_m282312187 (CheckGroups_t2785688338 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher/CheckGroups::Invoke(PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumber,System.Text.StringBuilder,System.String[])
extern "C"  bool CheckGroups_Invoke_m456958787 (CheckGroups_t2785688338 * __this, PhoneNumberUtil_t4155573397 * ___util0, PhoneNumber_t814071929 * ___number1, StringBuilder_t1221177846 * ___normalizedCandidate2, StringU5BU5D_t1642385972* ___expectedNumberGroups3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult PhoneNumbers.PhoneNumberMatcher/CheckGroups::BeginInvoke(PhoneNumbers.PhoneNumberUtil,PhoneNumbers.PhoneNumber,System.Text.StringBuilder,System.String[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CheckGroups_BeginInvoke_m3949516274 (CheckGroups_t2785688338 * __this, PhoneNumberUtil_t4155573397 * ___util0, PhoneNumber_t814071929 * ___number1, StringBuilder_t1221177846 * ___normalizedCandidate2, StringU5BU5D_t1642385972* ___expectedNumberGroups3, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.PhoneNumberMatcher/CheckGroups::EndInvoke(System.IAsyncResult)
extern "C"  bool CheckGroups_EndInvoke_m1962132225 (CheckGroups_t2785688338 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
