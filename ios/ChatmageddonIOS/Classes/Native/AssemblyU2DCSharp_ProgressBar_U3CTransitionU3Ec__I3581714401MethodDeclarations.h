﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProgressBar/<Transition>c__Iterator0
struct U3CTransitionU3Ec__Iterator0_t3581714401;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ProgressBar/<Transition>c__Iterator0::.ctor()
extern "C"  void U3CTransitionU3Ec__Iterator0__ctor_m2991904220 (U3CTransitionU3Ec__Iterator0_t3581714401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProgressBar/<Transition>c__Iterator0::MoveNext()
extern "C"  bool U3CTransitionU3Ec__Iterator0_MoveNext_m2212755744 (U3CTransitionU3Ec__Iterator0_t3581714401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProgressBar/<Transition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTransitionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1114750164 (U3CTransitionU3Ec__Iterator0_t3581714401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProgressBar/<Transition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTransitionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m450979084 (U3CTransitionU3Ec__Iterator0_t3581714401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar/<Transition>c__Iterator0::Dispose()
extern "C"  void U3CTransitionU3Ec__Iterator0_Dispose_m2196749003 (U3CTransitionU3Ec__Iterator0_t3581714401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProgressBar/<Transition>c__Iterator0::Reset()
extern "C"  void U3CTransitionU3Ec__Iterator0_Reset_m418904949 (U3CTransitionU3Ec__Iterator0_t3581714401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
