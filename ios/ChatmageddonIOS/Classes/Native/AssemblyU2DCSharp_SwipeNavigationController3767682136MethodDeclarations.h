﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwipeNavigationController
struct SwipeNavigationController_t3767682136;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"

// System.Void SwipeNavigationController::.ctor()
extern "C"  void SwipeNavigationController__ctor_m1325390501 (SwipeNavigationController_t3767682136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeNavigationController::Start()
extern "C"  void SwipeNavigationController_Start_m3792709021 (SwipeNavigationController_t3767682136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeNavigationController::Setup()
extern "C"  void SwipeNavigationController_Setup_m987678898 (SwipeNavigationController_t3767682136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeNavigationController::CalcPopulationIndices()
extern "C"  void SwipeNavigationController_CalcPopulationIndices_m1273525396 (SwipeNavigationController_t3767682136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeNavigationController::JumpToItem(PurchaseableCategory)
extern "C"  void SwipeNavigationController_JumpToItem_m3633435000 (SwipeNavigationController_t3767682136 * __this, int32_t ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeNavigationController::JumpToIndex(System.Int32)
extern "C"  void SwipeNavigationController_JumpToIndex_m4227375631 (SwipeNavigationController_t3767682136 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeNavigationController::SnapToClosestPoint()
extern "C"  void SwipeNavigationController_SnapToClosestPoint_m3096176815 (SwipeNavigationController_t3767682136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeNavigationController::TweenNavItem(System.Boolean)
extern "C"  void SwipeNavigationController_TweenNavItem_m11147363 (SwipeNavigationController_t3767682136 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
