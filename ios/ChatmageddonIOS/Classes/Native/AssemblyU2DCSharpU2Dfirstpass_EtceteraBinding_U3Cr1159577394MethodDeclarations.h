﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3
struct U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3::.ctor()
extern "C"  void U3CregisterDeviceWithGameThriveU3Ec__Iterator3__ctor_m3146485695 (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3::MoveNext()
extern "C"  bool U3CregisterDeviceWithGameThriveU3Ec__Iterator3_MoveNext_m1999696229 (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CregisterDeviceWithGameThriveU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3074310341 (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CregisterDeviceWithGameThriveU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m445494029 (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3::Dispose()
extern "C"  void U3CregisterDeviceWithGameThriveU3Ec__Iterator3_Dispose_m2900946302 (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3::Reset()
extern "C"  void U3CregisterDeviceWithGameThriveU3Ec__Iterator3_Reset_m1700913976 (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraBinding/<registerDeviceWithGameThrive>c__Iterator3::<>__Finally0()
extern "C"  void U3CregisterDeviceWithGameThriveU3Ec__Iterator3_U3CU3E__Finally0_m3213453694 (U3CregisterDeviceWithGameThriveU3Ec__Iterator3_t1159577394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
