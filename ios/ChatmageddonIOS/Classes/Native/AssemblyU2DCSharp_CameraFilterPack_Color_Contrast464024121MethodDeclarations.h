﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Color_Contrast
struct CameraFilterPack_Color_Contrast_t464024121;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Color_Contrast::.ctor()
extern "C"  void CameraFilterPack_Color_Contrast__ctor_m977435422 (CameraFilterPack_Color_Contrast_t464024121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Contrast::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Color_Contrast_get_material_m2764014469 (CameraFilterPack_Color_Contrast_t464024121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::Start()
extern "C"  void CameraFilterPack_Color_Contrast_Start_m2267841726 (CameraFilterPack_Color_Contrast_t464024121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Color_Contrast_OnRenderImage_m2113177134 (CameraFilterPack_Color_Contrast_t464024121 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::OnValidate()
extern "C"  void CameraFilterPack_Color_Contrast_OnValidate_m3637958231 (CameraFilterPack_Color_Contrast_t464024121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::Update()
extern "C"  void CameraFilterPack_Color_Contrast_Update_m942817697 (CameraFilterPack_Color_Contrast_t464024121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::OnDisable()
extern "C"  void CameraFilterPack_Color_Contrast_OnDisable_m3725632813 (CameraFilterPack_Color_Contrast_t464024121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
