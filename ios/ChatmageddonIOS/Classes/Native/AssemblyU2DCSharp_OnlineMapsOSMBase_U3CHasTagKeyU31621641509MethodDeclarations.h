﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMapsOSMBase/<HasTagKey>c__AnonStorey2
struct U3CHasTagKeyU3Ec__AnonStorey2_t1621641509;
// OnlineMapsOSMTag
struct OnlineMapsOSMTag_t3629071465;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsOSMTag3629071465.h"

// System.Void OnlineMapsOSMBase/<HasTagKey>c__AnonStorey2::.ctor()
extern "C"  void U3CHasTagKeyU3Ec__AnonStorey2__ctor_m3464090700 (U3CHasTagKeyU3Ec__AnonStorey2_t1621641509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMapsOSMBase/<HasTagKey>c__AnonStorey2::<>m__0(OnlineMapsOSMTag)
extern "C"  bool U3CHasTagKeyU3Ec__AnonStorey2_U3CU3Em__0_m3038844108 (U3CHasTagKeyU3Ec__AnonStorey2_t1621641509 * __this, OnlineMapsOSMTag_t3629071465 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
