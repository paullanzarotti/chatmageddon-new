﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Overlay
struct CameraFilterPack_Blend2Camera_Overlay_t1002672026;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Overlay::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Overlay__ctor_m1034045883 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Overlay::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Overlay_get_material_m2793037338 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Overlay_Start_m2484568927 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Overlay_OnRenderImage_m3484963511 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Overlay_OnValidate_m1474667226 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Overlay_Update_m1017355848 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Overlay_OnEnable_m1744331327 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Overlay_OnDisable_m757792830 (CameraFilterPack_Blend2Camera_Overlay_t1002672026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
