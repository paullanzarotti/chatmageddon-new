﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonPanelTransitionManager
struct ChatmageddonPanelTransitionManager_t1399015365;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PanelType482769230.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ChatmageddonPanelTransitionManager::.ctor()
extern "C"  void ChatmageddonPanelTransitionManager__ctor_m1969001852 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::Awake()
extern "C"  void ChatmageddonPanelTransitionManager_Awake_m3852674241 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::ToggleUI()
extern "C"  void ChatmageddonPanelTransitionManager_ToggleUI_m3476922444 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::TogglePanel(PanelType,System.Boolean)
extern "C"  void ChatmageddonPanelTransitionManager_TogglePanel_m357776969 (ChatmageddonPanelTransitionManager_t1399015365 * __this, int32_t ___panel0, bool ___switching1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ChatmageddonPanelTransitionManager::SwitchPanels()
extern "C"  Il2CppObject * ChatmageddonPanelTransitionManager_SwitchPanels_m3472482757 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::DestroyPanel(PanelType)
extern "C"  void ChatmageddonPanelTransitionManager_DestroyPanel_m3502792802 (ChatmageddonPanelTransitionManager_t1399015365 * __this, int32_t ___panel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::getBottomUI(System.String)
extern "C"  void ChatmageddonPanelTransitionManager_getBottomUI_m176439887 (ChatmageddonPanelTransitionManager_t1399015365 * __this, String_t* ___methodName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::ShowBottomUI(System.Single)
extern "C"  void ChatmageddonPanelTransitionManager_ShowBottomUI_m1084300817 (ChatmageddonPanelTransitionManager_t1399015365 * __this, float ___delayTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::HideBottomUI()
extern "C"  void ChatmageddonPanelTransitionManager_HideBottomUI_m1609030635 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::getTopTabs(System.String)
extern "C"  void ChatmageddonPanelTransitionManager_getTopTabs_m3499276599 (ChatmageddonPanelTransitionManager_t1399015365 * __this, String_t* ___methodName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::ShowTopTabs()
extern "C"  void ChatmageddonPanelTransitionManager_ShowTopTabs_m1730468720 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::HideTopTabs()
extern "C"  void ChatmageddonPanelTransitionManager_HideTopTabs_m1364607819 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::CheckContainerPanel()
extern "C"  void ChatmageddonPanelTransitionManager_CheckContainerPanel_m1466327099 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonPanelTransitionManager::SetupContainerPanel()
extern "C"  void ChatmageddonPanelTransitionManager_SetupContainerPanel_m1953339278 (ChatmageddonPanelTransitionManager_t1399015365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
