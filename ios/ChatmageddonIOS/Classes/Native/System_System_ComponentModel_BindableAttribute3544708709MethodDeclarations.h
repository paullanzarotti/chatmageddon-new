﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.BindableAttribute
struct BindableAttribute_t3544708709;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_BindableSupport1735231582.h"
#include "System_System_ComponentModel_BindingDirection2271780566.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.ComponentModel.BindableAttribute::.ctor(System.ComponentModel.BindableSupport)
extern "C"  void BindableAttribute__ctor_m4079693729 (BindableAttribute_t3544708709 * __this, int32_t ___flags0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.BindableAttribute::.ctor(System.Boolean)
extern "C"  void BindableAttribute__ctor_m3269036555 (BindableAttribute_t3544708709 * __this, bool ___bindable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.BindableAttribute::.ctor(System.Boolean,System.ComponentModel.BindingDirection)
extern "C"  void BindableAttribute__ctor_m1594704626 (BindableAttribute_t3544708709 * __this, bool ___bindable0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.BindableAttribute::.ctor(System.ComponentModel.BindableSupport,System.ComponentModel.BindingDirection)
extern "C"  void BindableAttribute__ctor_m791681766 (BindableAttribute_t3544708709 * __this, int32_t ___flags0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.BindableAttribute::.cctor()
extern "C"  void BindableAttribute__cctor_m1453284639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.BindingDirection System.ComponentModel.BindableAttribute::get_Direction()
extern "C"  int32_t BindableAttribute_get_Direction_m963904548 (BindableAttribute_t3544708709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.BindableAttribute::get_Bindable()
extern "C"  bool BindableAttribute_get_Bindable_m2597330870 (BindableAttribute_t3544708709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.BindableAttribute::Equals(System.Object)
extern "C"  bool BindableAttribute_Equals_m3372731997 (BindableAttribute_t3544708709 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.BindableAttribute::GetHashCode()
extern "C"  int32_t BindableAttribute_GetHashCode_m3135004243 (BindableAttribute_t3544708709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.BindableAttribute::IsDefaultAttribute()
extern "C"  bool BindableAttribute_IsDefaultAttribute_m21532877 (BindableAttribute_t3544708709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
