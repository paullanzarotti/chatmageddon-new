﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraGUIManager
struct EtceteraGUIManager_t3776717143;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EtceteraGUIManager::.ctor()
extern "C"  void EtceteraGUIManager__ctor_m3115713022 (EtceteraGUIManager_t3776717143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManager::Start()
extern "C"  void EtceteraGUIManager_Start_m3081892662 (EtceteraGUIManager_t3776717143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManager::OnGUI()
extern "C"  void EtceteraGUIManager_OnGUI_m199713170 (EtceteraGUIManager_t3776717143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManager::<OnGUI>m__0(System.String)
extern "C"  void EtceteraGUIManager_U3COnGUIU3Em__0_m3570933795 (Il2CppObject * __this /* static, unused */, String_t* ___imagePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
