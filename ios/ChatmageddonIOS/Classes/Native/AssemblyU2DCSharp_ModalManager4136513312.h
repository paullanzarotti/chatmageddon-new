﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// ModalPanel
struct ModalPanel_t3020707755;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3887179032.h"
#include "AssemblyU2DCSharp_ModalType1526568337.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalManager
struct  ModalManager_t4136513312  : public MonoSingleton_1_t3887179032
{
public:
	// ModalType ModalManager::activeModal
	int32_t ___activeModal_3;
	// System.Boolean ModalManager::modalActive
	bool ___modalActive_4;
	// UnityEngine.GameObject ModalManager::modalPanelObject
	GameObject_t1756533147 * ___modalPanelObject_5;
	// ModalPanel ModalManager::modalPanel
	ModalPanel_t3020707755 * ___modalPanel_6;
	// UnityEngine.GameObject ModalManager::currentModalUI
	GameObject_t1756533147 * ___currentModalUI_7;
	// System.Boolean ModalManager::modalclosing
	bool ___modalclosing_8;

public:
	inline static int32_t get_offset_of_activeModal_3() { return static_cast<int32_t>(offsetof(ModalManager_t4136513312, ___activeModal_3)); }
	inline int32_t get_activeModal_3() const { return ___activeModal_3; }
	inline int32_t* get_address_of_activeModal_3() { return &___activeModal_3; }
	inline void set_activeModal_3(int32_t value)
	{
		___activeModal_3 = value;
	}

	inline static int32_t get_offset_of_modalActive_4() { return static_cast<int32_t>(offsetof(ModalManager_t4136513312, ___modalActive_4)); }
	inline bool get_modalActive_4() const { return ___modalActive_4; }
	inline bool* get_address_of_modalActive_4() { return &___modalActive_4; }
	inline void set_modalActive_4(bool value)
	{
		___modalActive_4 = value;
	}

	inline static int32_t get_offset_of_modalPanelObject_5() { return static_cast<int32_t>(offsetof(ModalManager_t4136513312, ___modalPanelObject_5)); }
	inline GameObject_t1756533147 * get_modalPanelObject_5() const { return ___modalPanelObject_5; }
	inline GameObject_t1756533147 ** get_address_of_modalPanelObject_5() { return &___modalPanelObject_5; }
	inline void set_modalPanelObject_5(GameObject_t1756533147 * value)
	{
		___modalPanelObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___modalPanelObject_5, value);
	}

	inline static int32_t get_offset_of_modalPanel_6() { return static_cast<int32_t>(offsetof(ModalManager_t4136513312, ___modalPanel_6)); }
	inline ModalPanel_t3020707755 * get_modalPanel_6() const { return ___modalPanel_6; }
	inline ModalPanel_t3020707755 ** get_address_of_modalPanel_6() { return &___modalPanel_6; }
	inline void set_modalPanel_6(ModalPanel_t3020707755 * value)
	{
		___modalPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___modalPanel_6, value);
	}

	inline static int32_t get_offset_of_currentModalUI_7() { return static_cast<int32_t>(offsetof(ModalManager_t4136513312, ___currentModalUI_7)); }
	inline GameObject_t1756533147 * get_currentModalUI_7() const { return ___currentModalUI_7; }
	inline GameObject_t1756533147 ** get_address_of_currentModalUI_7() { return &___currentModalUI_7; }
	inline void set_currentModalUI_7(GameObject_t1756533147 * value)
	{
		___currentModalUI_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentModalUI_7, value);
	}

	inline static int32_t get_offset_of_modalclosing_8() { return static_cast<int32_t>(offsetof(ModalManager_t4136513312, ___modalclosing_8)); }
	inline bool get_modalclosing_8() const { return ___modalclosing_8; }
	inline bool* get_address_of_modalclosing_8() { return &___modalclosing_8; }
	inline void set_modalclosing_8(bool value)
	{
		___modalclosing_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
