﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// IPSpritePicker
struct IPSpritePicker_t2623787062;
// TweenAlpha
struct TweenAlpha_t2421518635;
// UIDragObject
struct UIDragObject_t1520449903;
// IPUserInteraction
struct IPUserInteraction_t4192479194;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragPickerSprite
struct  DragPickerSprite_t1100164761  : public MonoBehaviour_t1158329972
{
public:
	// UISprite DragPickerSprite::draggedSprite
	UISprite_t603616735 * ___draggedSprite_2;
	// IPSpritePicker DragPickerSprite::picker
	IPSpritePicker_t2623787062 * ___picker_3;
	// TweenAlpha DragPickerSprite::tweenAlpha
	TweenAlpha_t2421518635 * ___tweenAlpha_4;
	// System.Boolean DragPickerSprite::dragSelectedItemOnly
	bool ___dragSelectedItemOnly_5;
	// System.Single DragPickerSprite::delayAfterExit
	float ___delayAfterExit_6;
	// UIDragObject DragPickerSprite::_dragObject
	UIDragObject_t1520449903 * ____dragObject_7;
	// IPUserInteraction DragPickerSprite::_userInteraction
	IPUserInteraction_t4192479194 * ____userInteraction_8;

public:
	inline static int32_t get_offset_of_draggedSprite_2() { return static_cast<int32_t>(offsetof(DragPickerSprite_t1100164761, ___draggedSprite_2)); }
	inline UISprite_t603616735 * get_draggedSprite_2() const { return ___draggedSprite_2; }
	inline UISprite_t603616735 ** get_address_of_draggedSprite_2() { return &___draggedSprite_2; }
	inline void set_draggedSprite_2(UISprite_t603616735 * value)
	{
		___draggedSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___draggedSprite_2, value);
	}

	inline static int32_t get_offset_of_picker_3() { return static_cast<int32_t>(offsetof(DragPickerSprite_t1100164761, ___picker_3)); }
	inline IPSpritePicker_t2623787062 * get_picker_3() const { return ___picker_3; }
	inline IPSpritePicker_t2623787062 ** get_address_of_picker_3() { return &___picker_3; }
	inline void set_picker_3(IPSpritePicker_t2623787062 * value)
	{
		___picker_3 = value;
		Il2CppCodeGenWriteBarrier(&___picker_3, value);
	}

	inline static int32_t get_offset_of_tweenAlpha_4() { return static_cast<int32_t>(offsetof(DragPickerSprite_t1100164761, ___tweenAlpha_4)); }
	inline TweenAlpha_t2421518635 * get_tweenAlpha_4() const { return ___tweenAlpha_4; }
	inline TweenAlpha_t2421518635 ** get_address_of_tweenAlpha_4() { return &___tweenAlpha_4; }
	inline void set_tweenAlpha_4(TweenAlpha_t2421518635 * value)
	{
		___tweenAlpha_4 = value;
		Il2CppCodeGenWriteBarrier(&___tweenAlpha_4, value);
	}

	inline static int32_t get_offset_of_dragSelectedItemOnly_5() { return static_cast<int32_t>(offsetof(DragPickerSprite_t1100164761, ___dragSelectedItemOnly_5)); }
	inline bool get_dragSelectedItemOnly_5() const { return ___dragSelectedItemOnly_5; }
	inline bool* get_address_of_dragSelectedItemOnly_5() { return &___dragSelectedItemOnly_5; }
	inline void set_dragSelectedItemOnly_5(bool value)
	{
		___dragSelectedItemOnly_5 = value;
	}

	inline static int32_t get_offset_of_delayAfterExit_6() { return static_cast<int32_t>(offsetof(DragPickerSprite_t1100164761, ___delayAfterExit_6)); }
	inline float get_delayAfterExit_6() const { return ___delayAfterExit_6; }
	inline float* get_address_of_delayAfterExit_6() { return &___delayAfterExit_6; }
	inline void set_delayAfterExit_6(float value)
	{
		___delayAfterExit_6 = value;
	}

	inline static int32_t get_offset_of__dragObject_7() { return static_cast<int32_t>(offsetof(DragPickerSprite_t1100164761, ____dragObject_7)); }
	inline UIDragObject_t1520449903 * get__dragObject_7() const { return ____dragObject_7; }
	inline UIDragObject_t1520449903 ** get_address_of__dragObject_7() { return &____dragObject_7; }
	inline void set__dragObject_7(UIDragObject_t1520449903 * value)
	{
		____dragObject_7 = value;
		Il2CppCodeGenWriteBarrier(&____dragObject_7, value);
	}

	inline static int32_t get_offset_of__userInteraction_8() { return static_cast<int32_t>(offsetof(DragPickerSprite_t1100164761, ____userInteraction_8)); }
	inline IPUserInteraction_t4192479194 * get__userInteraction_8() const { return ____userInteraction_8; }
	inline IPUserInteraction_t4192479194 ** get_address_of__userInteraction_8() { return &____userInteraction_8; }
	inline void set__userInteraction_8(IPUserInteraction_t4192479194 * value)
	{
		____userInteraction_8 = value;
		Il2CppCodeGenWriteBarrier(&____userInteraction_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
