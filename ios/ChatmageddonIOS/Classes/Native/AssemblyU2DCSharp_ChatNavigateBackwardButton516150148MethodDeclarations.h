﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatNavigateBackwardButton
struct ChatNavigateBackwardButton_t516150148;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"

// System.Void ChatNavigateBackwardButton::.ctor()
extern "C"  void ChatNavigateBackwardButton__ctor_m4172363087 (ChatNavigateBackwardButton_t516150148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigateBackwardButton::OnClick()
extern "C"  void ChatNavigateBackwardButton_OnClick_m3293918760 (ChatNavigateBackwardButton_t516150148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigateBackwardButton::PlaySfx()
extern "C"  void ChatNavigateBackwardButton_PlaySfx_m3115953366 (ChatNavigateBackwardButton_t516150148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatNavigateBackwardButton::SetButtonDirection(ButtonDirection)
extern "C"  void ChatNavigateBackwardButton_SetButtonDirection_m935878853 (ChatNavigateBackwardButton_t516150148 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
