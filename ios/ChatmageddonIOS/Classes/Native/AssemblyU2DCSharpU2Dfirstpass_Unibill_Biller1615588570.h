﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// TransactionDatabase
struct TransactionDatabase_t201476183;
// Uniject.ILogger
struct ILogger_t2858843691;
// Unibill.Impl.HelpCentre
struct HelpCentre_t214342444;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.CurrencyManager
struct CurrencyManager_t3009003778;
// Unibill.Impl.IBillingService
struct IBillingService_t403880577;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<PurchaseEvent>
struct Action_1_t545353811;
// System.Action`1<PurchasableItem>
struct Action_1_t3765153281;
// System.Collections.Generic.List`1<UnibillError>
struct List_1_t1122980919;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillerS4181920983.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Biller
struct  Biller_t1615588570  : public Il2CppObject
{
public:
	// Unibill.Impl.UnibillConfiguration Unibill.Biller::<InventoryDatabase>k__BackingField
	UnibillConfiguration_t2915611853 * ___U3CInventoryDatabaseU3Ek__BackingField_0;
	// TransactionDatabase Unibill.Biller::transactionDatabase
	TransactionDatabase_t201476183 * ___transactionDatabase_1;
	// Uniject.ILogger Unibill.Biller::logger
	Il2CppObject * ___logger_2;
	// Unibill.Impl.HelpCentre Unibill.Biller::help
	HelpCentre_t214342444 * ___help_3;
	// Unibill.Impl.ProductIdRemapper Unibill.Biller::remapper
	ProductIdRemapper_t3313438456 * ___remapper_4;
	// Unibill.Impl.CurrencyManager Unibill.Biller::currencyManager
	CurrencyManager_t3009003778 * ___currencyManager_5;
	// Unibill.Impl.IBillingService Unibill.Biller::<billingSubsystem>k__BackingField
	Il2CppObject * ___U3CbillingSubsystemU3Ek__BackingField_6;
	// System.Action`1<System.Boolean> Unibill.Biller::onBillerReady
	Action_1_t3627374100 * ___onBillerReady_7;
	// System.Action`1<PurchaseEvent> Unibill.Biller::onPurchaseComplete
	Action_1_t545353811 * ___onPurchaseComplete_8;
	// System.Action`1<System.Boolean> Unibill.Biller::onTransactionRestoreBegin
	Action_1_t3627374100 * ___onTransactionRestoreBegin_9;
	// System.Action`1<System.Boolean> Unibill.Biller::onTransactionsRestored
	Action_1_t3627374100 * ___onTransactionsRestored_10;
	// System.Action`1<PurchasableItem> Unibill.Biller::onPurchaseCancelled
	Action_1_t3765153281 * ___onPurchaseCancelled_11;
	// System.Action`1<PurchasableItem> Unibill.Biller::onPurchaseRefunded
	Action_1_t3765153281 * ___onPurchaseRefunded_12;
	// System.Action`1<PurchasableItem> Unibill.Biller::onPurchaseFailed
	Action_1_t3765153281 * ___onPurchaseFailed_13;
	// System.Action`1<PurchasableItem> Unibill.Biller::onPurchaseDeferred
	Action_1_t3765153281 * ___onPurchaseDeferred_14;
	// Unibill.Impl.BillerState Unibill.Biller::<State>k__BackingField
	int32_t ___U3CStateU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<UnibillError> Unibill.Biller::<Errors>k__BackingField
	List_1_t1122980919 * ___U3CErrorsU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CInventoryDatabaseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___U3CInventoryDatabaseU3Ek__BackingField_0)); }
	inline UnibillConfiguration_t2915611853 * get_U3CInventoryDatabaseU3Ek__BackingField_0() const { return ___U3CInventoryDatabaseU3Ek__BackingField_0; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_U3CInventoryDatabaseU3Ek__BackingField_0() { return &___U3CInventoryDatabaseU3Ek__BackingField_0; }
	inline void set_U3CInventoryDatabaseU3Ek__BackingField_0(UnibillConfiguration_t2915611853 * value)
	{
		___U3CInventoryDatabaseU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInventoryDatabaseU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_transactionDatabase_1() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___transactionDatabase_1)); }
	inline TransactionDatabase_t201476183 * get_transactionDatabase_1() const { return ___transactionDatabase_1; }
	inline TransactionDatabase_t201476183 ** get_address_of_transactionDatabase_1() { return &___transactionDatabase_1; }
	inline void set_transactionDatabase_1(TransactionDatabase_t201476183 * value)
	{
		___transactionDatabase_1 = value;
		Il2CppCodeGenWriteBarrier(&___transactionDatabase_1, value);
	}

	inline static int32_t get_offset_of_logger_2() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___logger_2)); }
	inline Il2CppObject * get_logger_2() const { return ___logger_2; }
	inline Il2CppObject ** get_address_of_logger_2() { return &___logger_2; }
	inline void set_logger_2(Il2CppObject * value)
	{
		___logger_2 = value;
		Il2CppCodeGenWriteBarrier(&___logger_2, value);
	}

	inline static int32_t get_offset_of_help_3() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___help_3)); }
	inline HelpCentre_t214342444 * get_help_3() const { return ___help_3; }
	inline HelpCentre_t214342444 ** get_address_of_help_3() { return &___help_3; }
	inline void set_help_3(HelpCentre_t214342444 * value)
	{
		___help_3 = value;
		Il2CppCodeGenWriteBarrier(&___help_3, value);
	}

	inline static int32_t get_offset_of_remapper_4() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___remapper_4)); }
	inline ProductIdRemapper_t3313438456 * get_remapper_4() const { return ___remapper_4; }
	inline ProductIdRemapper_t3313438456 ** get_address_of_remapper_4() { return &___remapper_4; }
	inline void set_remapper_4(ProductIdRemapper_t3313438456 * value)
	{
		___remapper_4 = value;
		Il2CppCodeGenWriteBarrier(&___remapper_4, value);
	}

	inline static int32_t get_offset_of_currencyManager_5() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___currencyManager_5)); }
	inline CurrencyManager_t3009003778 * get_currencyManager_5() const { return ___currencyManager_5; }
	inline CurrencyManager_t3009003778 ** get_address_of_currencyManager_5() { return &___currencyManager_5; }
	inline void set_currencyManager_5(CurrencyManager_t3009003778 * value)
	{
		___currencyManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___currencyManager_5, value);
	}

	inline static int32_t get_offset_of_U3CbillingSubsystemU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___U3CbillingSubsystemU3Ek__BackingField_6)); }
	inline Il2CppObject * get_U3CbillingSubsystemU3Ek__BackingField_6() const { return ___U3CbillingSubsystemU3Ek__BackingField_6; }
	inline Il2CppObject ** get_address_of_U3CbillingSubsystemU3Ek__BackingField_6() { return &___U3CbillingSubsystemU3Ek__BackingField_6; }
	inline void set_U3CbillingSubsystemU3Ek__BackingField_6(Il2CppObject * value)
	{
		___U3CbillingSubsystemU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbillingSubsystemU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_onBillerReady_7() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onBillerReady_7)); }
	inline Action_1_t3627374100 * get_onBillerReady_7() const { return ___onBillerReady_7; }
	inline Action_1_t3627374100 ** get_address_of_onBillerReady_7() { return &___onBillerReady_7; }
	inline void set_onBillerReady_7(Action_1_t3627374100 * value)
	{
		___onBillerReady_7 = value;
		Il2CppCodeGenWriteBarrier(&___onBillerReady_7, value);
	}

	inline static int32_t get_offset_of_onPurchaseComplete_8() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onPurchaseComplete_8)); }
	inline Action_1_t545353811 * get_onPurchaseComplete_8() const { return ___onPurchaseComplete_8; }
	inline Action_1_t545353811 ** get_address_of_onPurchaseComplete_8() { return &___onPurchaseComplete_8; }
	inline void set_onPurchaseComplete_8(Action_1_t545353811 * value)
	{
		___onPurchaseComplete_8 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseComplete_8, value);
	}

	inline static int32_t get_offset_of_onTransactionRestoreBegin_9() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onTransactionRestoreBegin_9)); }
	inline Action_1_t3627374100 * get_onTransactionRestoreBegin_9() const { return ___onTransactionRestoreBegin_9; }
	inline Action_1_t3627374100 ** get_address_of_onTransactionRestoreBegin_9() { return &___onTransactionRestoreBegin_9; }
	inline void set_onTransactionRestoreBegin_9(Action_1_t3627374100 * value)
	{
		___onTransactionRestoreBegin_9 = value;
		Il2CppCodeGenWriteBarrier(&___onTransactionRestoreBegin_9, value);
	}

	inline static int32_t get_offset_of_onTransactionsRestored_10() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onTransactionsRestored_10)); }
	inline Action_1_t3627374100 * get_onTransactionsRestored_10() const { return ___onTransactionsRestored_10; }
	inline Action_1_t3627374100 ** get_address_of_onTransactionsRestored_10() { return &___onTransactionsRestored_10; }
	inline void set_onTransactionsRestored_10(Action_1_t3627374100 * value)
	{
		___onTransactionsRestored_10 = value;
		Il2CppCodeGenWriteBarrier(&___onTransactionsRestored_10, value);
	}

	inline static int32_t get_offset_of_onPurchaseCancelled_11() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onPurchaseCancelled_11)); }
	inline Action_1_t3765153281 * get_onPurchaseCancelled_11() const { return ___onPurchaseCancelled_11; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseCancelled_11() { return &___onPurchaseCancelled_11; }
	inline void set_onPurchaseCancelled_11(Action_1_t3765153281 * value)
	{
		___onPurchaseCancelled_11 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseCancelled_11, value);
	}

	inline static int32_t get_offset_of_onPurchaseRefunded_12() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onPurchaseRefunded_12)); }
	inline Action_1_t3765153281 * get_onPurchaseRefunded_12() const { return ___onPurchaseRefunded_12; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseRefunded_12() { return &___onPurchaseRefunded_12; }
	inline void set_onPurchaseRefunded_12(Action_1_t3765153281 * value)
	{
		___onPurchaseRefunded_12 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseRefunded_12, value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_13() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onPurchaseFailed_13)); }
	inline Action_1_t3765153281 * get_onPurchaseFailed_13() const { return ___onPurchaseFailed_13; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseFailed_13() { return &___onPurchaseFailed_13; }
	inline void set_onPurchaseFailed_13(Action_1_t3765153281 * value)
	{
		___onPurchaseFailed_13 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseFailed_13, value);
	}

	inline static int32_t get_offset_of_onPurchaseDeferred_14() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___onPurchaseDeferred_14)); }
	inline Action_1_t3765153281 * get_onPurchaseDeferred_14() const { return ___onPurchaseDeferred_14; }
	inline Action_1_t3765153281 ** get_address_of_onPurchaseDeferred_14() { return &___onPurchaseDeferred_14; }
	inline void set_onPurchaseDeferred_14(Action_1_t3765153281 * value)
	{
		___onPurchaseDeferred_14 = value;
		Il2CppCodeGenWriteBarrier(&___onPurchaseDeferred_14, value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___U3CStateU3Ek__BackingField_15)); }
	inline int32_t get_U3CStateU3Ek__BackingField_15() const { return ___U3CStateU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CStateU3Ek__BackingField_15() { return &___U3CStateU3Ek__BackingField_15; }
	inline void set_U3CStateU3Ek__BackingField_15(int32_t value)
	{
		___U3CStateU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CErrorsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Biller_t1615588570, ___U3CErrorsU3Ek__BackingField_16)); }
	inline List_1_t1122980919 * get_U3CErrorsU3Ek__BackingField_16() const { return ___U3CErrorsU3Ek__BackingField_16; }
	inline List_1_t1122980919 ** get_address_of_U3CErrorsU3Ek__BackingField_16() { return &___U3CErrorsU3Ek__BackingField_16; }
	inline void set_U3CErrorsU3Ek__BackingField_16(List_1_t1122980919 * value)
	{
		___U3CErrorsU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorsU3Ek__BackingField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
