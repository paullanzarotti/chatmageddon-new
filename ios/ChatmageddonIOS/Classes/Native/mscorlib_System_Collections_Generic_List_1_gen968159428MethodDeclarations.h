﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<FriendHomeNavScreen>
struct List_1_t968159428;
// System.Collections.Generic.IEnumerable`1<FriendHomeNavScreen>
struct IEnumerable_1_t1891165341;
// FriendHomeNavScreen[]
struct FriendHomeNavScreenU5BU5D_t1773644873;
// System.Collections.Generic.IEnumerator`1<FriendHomeNavScreen>
struct IEnumerator_1_t3369529419;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<FriendHomeNavScreen>
struct ICollection_1_t2551113601;
// System.Collections.ObjectModel.ReadOnlyCollection`1<FriendHomeNavScreen>
struct ReadOnlyCollection_1_t1784823988;
// System.Predicate`1<FriendHomeNavScreen>
struct Predicate_1_t42008411;
// System.Action`1<FriendHomeNavScreen>
struct Action_1_t1400837678;
// System.Collections.Generic.IComparer`1<FriendHomeNavScreen>
struct IComparer_1_t3848468714;
// System.Comparison`1<FriendHomeNavScreen>
struct Comparison_1_t2860777147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FriendHomeNavScreen1599038296.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat502889102.h"

// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::.ctor()
extern "C"  void List_1__ctor_m4182775444_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1__ctor_m4182775444(__this, method) ((  void (*) (List_1_t968159428 *, const MethodInfo*))List_1__ctor_m4182775444_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2214389417_gshared (List_1_t968159428 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2214389417(__this, ___collection0, method) ((  void (*) (List_1_t968159428 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2214389417_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m79194723_gshared (List_1_t968159428 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m79194723(__this, ___capacity0, method) ((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1__ctor_m79194723_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m1306197869_gshared (List_1_t968159428 * __this, FriendHomeNavScreenU5BU5D_t1773644873* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m1306197869(__this, ___data0, ___size1, method) ((  void (*) (List_1_t968159428 *, FriendHomeNavScreenU5BU5D_t1773644873*, int32_t, const MethodInfo*))List_1__ctor_m1306197869_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::.cctor()
extern "C"  void List_1__cctor_m4130761785_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4130761785(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4130761785_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m808871154_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m808871154(__this, method) ((  Il2CppObject* (*) (List_1_t968159428 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m808871154_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m947183240_gshared (List_1_t968159428 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m947183240(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t968159428 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m947183240_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2390847463_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2390847463(__this, method) ((  Il2CppObject * (*) (List_1_t968159428 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2390847463_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1892800004_gshared (List_1_t968159428 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1892800004(__this, ___item0, method) ((  int32_t (*) (List_1_t968159428 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1892800004_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m873545530_gshared (List_1_t968159428 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m873545530(__this, ___item0, method) ((  bool (*) (List_1_t968159428 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m873545530_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1642022686_gshared (List_1_t968159428 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1642022686(__this, ___item0, method) ((  int32_t (*) (List_1_t968159428 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1642022686_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m313541559_gshared (List_1_t968159428 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m313541559(__this, ___index0, ___item1, method) ((  void (*) (List_1_t968159428 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m313541559_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m585487959_gshared (List_1_t968159428 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m585487959(__this, ___item0, method) ((  void (*) (List_1_t968159428 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m585487959_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3942422755_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3942422755(__this, method) ((  bool (*) (List_1_t968159428 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3942422755_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m568937764_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m568937764(__this, method) ((  bool (*) (List_1_t968159428 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m568937764_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2729136136_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2729136136(__this, method) ((  Il2CppObject * (*) (List_1_t968159428 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2729136136_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m375209295_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m375209295(__this, method) ((  bool (*) (List_1_t968159428 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m375209295_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1750684312_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1750684312(__this, method) ((  bool (*) (List_1_t968159428 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1750684312_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3318421807_gshared (List_1_t968159428 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3318421807(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3318421807_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m189987202_gshared (List_1_t968159428 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m189987202(__this, ___index0, ___value1, method) ((  void (*) (List_1_t968159428 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m189987202_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Add(T)
extern "C"  void List_1_Add_m3586055427_gshared (List_1_t968159428 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3586055427(__this, ___item0, method) ((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_Add_m3586055427_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2657065050_gshared (List_1_t968159428 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2657065050(__this, ___newCount0, method) ((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2657065050_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m141972875_gshared (List_1_t968159428 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m141972875(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t968159428 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m141972875_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3897737042_gshared (List_1_t968159428 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3897737042(__this, ___collection0, method) ((  void (*) (List_1_t968159428 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3897737042_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1668431634_gshared (List_1_t968159428 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1668431634(__this, ___enumerable0, method) ((  void (*) (List_1_t968159428 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1668431634_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2558313779_gshared (List_1_t968159428 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2558313779(__this, ___collection0, method) ((  void (*) (List_1_t968159428 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2558313779_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<FriendHomeNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1784823988 * List_1_AsReadOnly_m2119180432_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2119180432(__this, method) ((  ReadOnlyCollection_1_t1784823988 * (*) (List_1_t968159428 *, const MethodInfo*))List_1_AsReadOnly_m2119180432_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Clear()
extern "C"  void List_1_Clear_m161757359_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_Clear_m161757359(__this, method) ((  void (*) (List_1_t968159428 *, const MethodInfo*))List_1_Clear_m161757359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m1149464345_gshared (List_1_t968159428 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m1149464345(__this, ___item0, method) ((  bool (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_Contains_m1149464345_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m41978620_gshared (List_1_t968159428 * __this, FriendHomeNavScreenU5BU5D_t1773644873* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m41978620(__this, ___array0, method) ((  void (*) (List_1_t968159428 *, FriendHomeNavScreenU5BU5D_t1773644873*, const MethodInfo*))List_1_CopyTo_m41978620_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m235834695_gshared (List_1_t968159428 * __this, FriendHomeNavScreenU5BU5D_t1773644873* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m235834695(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t968159428 *, FriendHomeNavScreenU5BU5D_t1773644873*, int32_t, const MethodInfo*))List_1_CopyTo_m235834695_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m3167719979_gshared (List_1_t968159428 * __this, int32_t ___index0, FriendHomeNavScreenU5BU5D_t1773644873* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m3167719979(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t968159428 *, int32_t, FriendHomeNavScreenU5BU5D_t1773644873*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m3167719979_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m2953183723_gshared (List_1_t968159428 * __this, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_Exists_m2953183723(__this, ___match0, method) ((  bool (*) (List_1_t968159428 *, Predicate_1_t42008411 *, const MethodInfo*))List_1_Exists_m2953183723_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<FriendHomeNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m2110724129_gshared (List_1_t968159428 * __this, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_Find_m2110724129(__this, ___match0, method) ((  int32_t (*) (List_1_t968159428 *, Predicate_1_t42008411 *, const MethodInfo*))List_1_Find_m2110724129_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m93661016_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m93661016(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t42008411 *, const MethodInfo*))List_1_CheckMatch_m93661016_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendHomeNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t968159428 * List_1_FindAll_m2318135414_gshared (List_1_t968159428 * __this, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m2318135414(__this, ___match0, method) ((  List_1_t968159428 * (*) (List_1_t968159428 *, Predicate_1_t42008411 *, const MethodInfo*))List_1_FindAll_m2318135414_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendHomeNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t968159428 * List_1_FindAllStackBits_m1576367106_gshared (List_1_t968159428 * __this, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1576367106(__this, ___match0, method) ((  List_1_t968159428 * (*) (List_1_t968159428 *, Predicate_1_t42008411 *, const MethodInfo*))List_1_FindAllStackBits_m1576367106_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<FriendHomeNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t968159428 * List_1_FindAllList_m3660725506_gshared (List_1_t968159428 * __this, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m3660725506(__this, ___match0, method) ((  List_1_t968159428 * (*) (List_1_t968159428 *, Predicate_1_t42008411 *, const MethodInfo*))List_1_FindAllList_m3660725506_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m2177456042_gshared (List_1_t968159428 * __this, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m2177456042(__this, ___match0, method) ((  int32_t (*) (List_1_t968159428 *, Predicate_1_t42008411 *, const MethodInfo*))List_1_FindIndex_m2177456042_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1325248109_gshared (List_1_t968159428 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t42008411 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1325248109(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t968159428 *, int32_t, int32_t, Predicate_1_t42008411 *, const MethodInfo*))List_1_GetIndex_m1325248109_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m2716234280_gshared (List_1_t968159428 * __this, Action_1_t1400837678 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m2716234280(__this, ___action0, method) ((  void (*) (List_1_t968159428 *, Action_1_t1400837678 *, const MethodInfo*))List_1_ForEach_m2716234280_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<FriendHomeNavScreen>::GetEnumerator()
extern "C"  Enumerator_t502889102  List_1_GetEnumerator_m1035513708_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1035513708(__this, method) ((  Enumerator_t502889102  (*) (List_1_t968159428 *, const MethodInfo*))List_1_GetEnumerator_m1035513708_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1658812547_gshared (List_1_t968159428 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1658812547(__this, ___item0, method) ((  int32_t (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_IndexOf_m1658812547_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1894898516_gshared (List_1_t968159428 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1894898516(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t968159428 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1894898516_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m534496495_gshared (List_1_t968159428 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m534496495(__this, ___index0, method) ((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_CheckIndex_m534496495_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2483345166_gshared (List_1_t968159428 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m2483345166(__this, ___index0, ___item1, method) ((  void (*) (List_1_t968159428 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2483345166_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3561269257_gshared (List_1_t968159428 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3561269257(__this, ___collection0, method) ((  void (*) (List_1_t968159428 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3561269257_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<FriendHomeNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m387662496_gshared (List_1_t968159428 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m387662496(__this, ___item0, method) ((  bool (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_Remove_m387662496_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2577357852_gshared (List_1_t968159428 * __this, Predicate_1_t42008411 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2577357852(__this, ___match0, method) ((  int32_t (*) (List_1_t968159428 *, Predicate_1_t42008411 *, const MethodInfo*))List_1_RemoveAll_m2577357852_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1597741866_gshared (List_1_t968159428 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1597741866(__this, ___index0, method) ((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1597741866_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2913267635_gshared (List_1_t968159428 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2913267635(__this, ___index0, ___count1, method) ((  void (*) (List_1_t968159428 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2913267635_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m735688370_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_Reverse_m735688370(__this, method) ((  void (*) (List_1_t968159428 *, const MethodInfo*))List_1_Reverse_m735688370_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Sort()
extern "C"  void List_1_Sort_m2059280246_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_Sort_m2059280246(__this, method) ((  void (*) (List_1_t968159428 *, const MethodInfo*))List_1_Sort_m2059280246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3287221050_gshared (List_1_t968159428 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3287221050(__this, ___comparer0, method) ((  void (*) (List_1_t968159428 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3287221050_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3078527713_gshared (List_1_t968159428 * __this, Comparison_1_t2860777147 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3078527713(__this, ___comparison0, method) ((  void (*) (List_1_t968159428 *, Comparison_1_t2860777147 *, const MethodInfo*))List_1_Sort_m3078527713_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<FriendHomeNavScreen>::ToArray()
extern "C"  FriendHomeNavScreenU5BU5D_t1773644873* List_1_ToArray_m735758433_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_ToArray_m735758433(__this, method) ((  FriendHomeNavScreenU5BU5D_t1773644873* (*) (List_1_t968159428 *, const MethodInfo*))List_1_ToArray_m735758433_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2876147939_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2876147939(__this, method) ((  void (*) (List_1_t968159428 *, const MethodInfo*))List_1_TrimExcess_m2876147939_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m727495793_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m727495793(__this, method) ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))List_1_get_Capacity_m727495793_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m228166994_gshared (List_1_t968159428 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m228166994(__this, ___value0, method) ((  void (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_set_Capacity_m228166994_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<FriendHomeNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m2202191812_gshared (List_1_t968159428 * __this, const MethodInfo* method);
#define List_1_get_Count_m2202191812(__this, method) ((  int32_t (*) (List_1_t968159428 *, const MethodInfo*))List_1_get_Count_m2202191812_gshared)(__this, method)
// T System.Collections.Generic.List`1<FriendHomeNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2523458676_gshared (List_1_t968159428 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2523458676(__this, ___index0, method) ((  int32_t (*) (List_1_t968159428 *, int32_t, const MethodInfo*))List_1_get_Item_m2523458676_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<FriendHomeNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1364122083_gshared (List_1_t968159428 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1364122083(__this, ___index0, ___value1, method) ((  void (*) (List_1_t968159428 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1364122083_gshared)(__this, ___index0, ___value1, method)
