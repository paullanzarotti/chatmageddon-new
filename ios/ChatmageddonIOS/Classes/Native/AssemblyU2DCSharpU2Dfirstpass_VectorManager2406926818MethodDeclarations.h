﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VectorManager
struct VectorManager_t2406926818;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Vectrosity.VectorLine
struct VectorLine_t3390220087;
// RefInt
struct RefInt_t2938871354;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// UnityEngine.Mesh
struct Mesh_t1356156583;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_VectorLin3390220087.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Visibilit4053239146.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Vectrosity_Brightnes2731075545.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RefInt2938871354.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"

// System.Void VectorManager::.ctor()
extern "C"  void VectorManager__ctor_m1859383891 (VectorManager_t2406926818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::SetBrightnessParameters(System.Single,System.Single,System.Int32,System.Single,UnityEngine.Color)
extern "C"  void VectorManager_SetBrightnessParameters_m3958862304 (Il2CppObject * __this /* static, unused */, float ___fadeOutDistance0, float ___fullBrightDistance1, int32_t ___levels2, float ___frequency3, Color_t2020392075  ___color4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VectorManager::GetBrightnessValue(UnityEngine.Vector3)
extern "C"  float VectorManager_GetBrightnessValue_m1489464314 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness)
extern "C"  void VectorManager_ObjectSetup_m1342365885 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, VectorLine_t3390220087 * ___line1, int32_t ___visibility2, int32_t ___brightness3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::ObjectSetup(UnityEngine.GameObject,Vectrosity.VectorLine,Vectrosity.Visibility,Vectrosity.Brightness,System.Boolean)
extern "C"  void VectorManager_ObjectSetup_m2458206478 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, VectorLine_t3390220087 * ___line1, int32_t ___visibility2, int32_t ___brightness3, bool ___makeBounds4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VectorManager::get_arrayCount()
extern "C"  int32_t VectorManager_get_arrayCount_m3789925832 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::VisibilityStaticSetup(Vectrosity.VectorLine,RefInt&)
extern "C"  void VectorManager_VisibilityStaticSetup_m596260549 (Il2CppObject * __this /* static, unused */, VectorLine_t3390220087 * ___line0, RefInt_t2938871354 ** ___objectNum1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::VisibilityStaticRemove(System.Int32)
extern "C"  void VectorManager_VisibilityStaticRemove_m1191842836 (Il2CppObject * __this /* static, unused */, int32_t ___objectNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VectorManager::get_arrayCount2()
extern "C"  int32_t VectorManager_get_arrayCount2_m1469161172 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::VisibilitySetup(UnityEngine.Transform,Vectrosity.VectorLine,RefInt&)
extern "C"  void VectorManager_VisibilitySetup_m285381700 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___thisTransform0, VectorLine_t3390220087 * ___line1, RefInt_t2938871354 ** ___objectNum2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::VisibilityRemove(System.Int32)
extern "C"  void VectorManager_VisibilityRemove_m1572259214 (Il2CppObject * __this /* static, unused */, int32_t ___objectNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::CheckDistanceSetup(UnityEngine.Transform,Vectrosity.VectorLine,UnityEngine.Color,RefInt)
extern "C"  void VectorManager_CheckDistanceSetup_m3662292433 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___thisTransform0, VectorLine_t3390220087 * ___line1, Color_t2020392075  ___color2, RefInt_t2938871354 * ___objectNum3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::DistanceRemove(System.Int32)
extern "C"  void VectorManager_DistanceRemove_m19544115 (Il2CppObject * __this /* static, unused */, int32_t ___objectNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::CheckDistance()
extern "C"  void VectorManager_CheckDistance_m1442234416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::SetOldDistance(System.Int32,System.Int32)
extern "C"  void VectorManager_SetOldDistance_m4053046985 (Il2CppObject * __this /* static, unused */, int32_t ___objectNumber0, int32_t ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::SetDistanceColor(System.Int32)
extern "C"  void VectorManager_SetDistanceColor_m200134312 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::DrawArrayLine(System.Int32)
extern "C"  void VectorManager_DrawArrayLine_m2449407401 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::DrawArrayLine2(System.Int32)
extern "C"  void VectorManager_DrawArrayLine2_m2175998375 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::DrawArrayLines()
extern "C"  void VectorManager_DrawArrayLines_m2411388651 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::DrawArrayLines2()
extern "C"  void VectorManager_DrawArrayLines2_m3921499561 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds VectorManager::GetBounds(Vectrosity.VectorLine)
extern "C"  Bounds_t3033363703  VectorManager_GetBounds_m1975135940 (Il2CppObject * __this /* static, unused */, VectorLine_t3390220087 * ___line0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds VectorManager::GetBounds(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  Bounds_t3033363703  VectorManager_GetBounds_m1065878562 (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___points30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh VectorManager::MakeBoundsMesh(UnityEngine.Bounds)
extern "C"  Mesh_t1356156583 * VectorManager_MakeBoundsMesh_m332377664 (Il2CppObject * __this /* static, unused */, Bounds_t3033363703  ___bounds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::SetupBoundsMesh(UnityEngine.GameObject,Vectrosity.VectorLine)
extern "C"  void VectorManager_SetupBoundsMesh_m3782295237 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, VectorLine_t3390220087 * ___line1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VectorManager::.cctor()
extern "C"  void VectorManager__cctor_m2541023598 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
