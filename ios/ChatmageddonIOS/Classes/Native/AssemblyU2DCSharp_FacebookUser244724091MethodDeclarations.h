﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacebookUser
struct FacebookUser_t244724091;
// System.String
struct String_t;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// System.Void FacebookUser::.ctor(System.String,System.String)
extern "C"  void FacebookUser__ctor_m4198019848 (FacebookUser_t244724091 * __this, String_t* ___FirstName0, String_t* ___ID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookUser::SetUserProfilePicture(UnityEngine.Texture)
extern "C"  void FacebookUser_SetUserProfilePicture_m3527551772 (FacebookUser_t244724091 * __this, Texture_t2243626319 * ___profileImage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture FacebookUser::GetUserProfilePicture()
extern "C"  Texture_t2243626319 * FacebookUser_GetUserProfilePicture_m262333321 (FacebookUser_t244724091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookUser::SetUserFirstName(System.String)
extern "C"  void FacebookUser_SetUserFirstName_m1631350954 (FacebookUser_t244724091 * __this, String_t* ___newFirstName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FacebookUser::GetUserFirstName()
extern "C"  String_t* FacebookUser_GetUserFirstName_m638477057 (FacebookUser_t244724091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookUser::SetUserLastName(System.String)
extern "C"  void FacebookUser_SetUserLastName_m668642400 (FacebookUser_t244724091 * __this, String_t* ___newLastName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FacebookUser::GetUserLastName()
extern "C"  String_t* FacebookUser_GetUserLastName_m3845462181 (FacebookUser_t244724091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookUser::SetUserEmail(System.String)
extern "C"  void FacebookUser_SetUserEmail_m1442261011 (FacebookUser_t244724091 * __this, String_t* ___newEmail0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FacebookUser::GetUserEmail()
extern "C"  String_t* FacebookUser_GetUserEmail_m512110872 (FacebookUser_t244724091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FacebookUser::SetUserID(System.String)
extern "C"  void FacebookUser_SetUserID_m2787758146 (FacebookUser_t244724091 * __this, String_t* ___newID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FacebookUser::GetUserID()
extern "C"  String_t* FacebookUser_GetUserID_m683715385 (FacebookUser_t244724091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
