﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragDropItem/<EnableDragScrollView>c__Iterator0
struct U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIDragDropItem/<EnableDragScrollView>c__Iterator0::.ctor()
extern "C"  void U3CEnableDragScrollViewU3Ec__Iterator0__ctor_m3756020344 (U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragDropItem/<EnableDragScrollView>c__Iterator0::MoveNext()
extern "C"  bool U3CEnableDragScrollViewU3Ec__Iterator0_MoveNext_m2854585028 (U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIDragDropItem/<EnableDragScrollView>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnableDragScrollViewU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m235554402 (U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIDragDropItem/<EnableDragScrollView>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnableDragScrollViewU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m205536138 (U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem/<EnableDragScrollView>c__Iterator0::Dispose()
extern "C"  void U3CEnableDragScrollViewU3Ec__Iterator0_Dispose_m1676664169 (U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem/<EnableDragScrollView>c__Iterator0::Reset()
extern "C"  void U3CEnableDragScrollViewU3Ec__Iterator0_Reset_m1789403851 (U3CEnableDragScrollViewU3Ec__Iterator0_t3584381351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
