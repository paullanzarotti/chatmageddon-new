﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsLoaderUIScaler
struct  SettingsLoaderUIScaler_t190890564  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite SettingsLoaderUIScaler::titleSeperator
	UISprite_t603616735 * ___titleSeperator_14;
	// UnityEngine.Transform SettingsLoaderUIScaler::navigateBackButton
	Transform_t3275118058 * ___navigateBackButton_15;

public:
	inline static int32_t get_offset_of_titleSeperator_14() { return static_cast<int32_t>(offsetof(SettingsLoaderUIScaler_t190890564, ___titleSeperator_14)); }
	inline UISprite_t603616735 * get_titleSeperator_14() const { return ___titleSeperator_14; }
	inline UISprite_t603616735 ** get_address_of_titleSeperator_14() { return &___titleSeperator_14; }
	inline void set_titleSeperator_14(UISprite_t603616735 * value)
	{
		___titleSeperator_14 = value;
		Il2CppCodeGenWriteBarrier(&___titleSeperator_14, value);
	}

	inline static int32_t get_offset_of_navigateBackButton_15() { return static_cast<int32_t>(offsetof(SettingsLoaderUIScaler_t190890564, ___navigateBackButton_15)); }
	inline Transform_t3275118058 * get_navigateBackButton_15() const { return ___navigateBackButton_15; }
	inline Transform_t3275118058 ** get_address_of_navigateBackButton_15() { return &___navigateBackButton_15; }
	inline void set_navigateBackButton_15(Transform_t3275118058 * value)
	{
		___navigateBackButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___navigateBackButton_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
