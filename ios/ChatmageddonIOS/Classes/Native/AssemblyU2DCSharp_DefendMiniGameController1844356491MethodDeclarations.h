﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendMiniGameController
struct DefendMiniGameController_t1844356491;
// LaunchedMissile
struct LaunchedMissile_t1542515810;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DefendMiniGameController::.ctor()
extern "C"  void DefendMiniGameController__ctor_m3813445852 (DefendMiniGameController_t1844356491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController::TimerUpdated(System.TimeSpan)
extern "C"  void DefendMiniGameController_TimerUpdated_m2392437024 (DefendMiniGameController_t1844356491 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController::DefendMissile(System.Boolean)
extern "C"  void DefendMiniGameController_DefendMissile_m2020968673 (DefendMiniGameController_t1844356491 * __this, bool ___defendSuccess0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController::SendDefendMissileServerCall(System.Boolean,System.DateTime)
extern "C"  void DefendMiniGameController_SendDefendMissileServerCall_m3805245504 (DefendMiniGameController_t1844356491 * __this, bool ___defendSuccess0, DateTime_t693205669  ___defendTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController::DisplayMissileDefendedToast(LaunchedMissile,System.Boolean)
extern "C"  void DefendMiniGameController_DisplayMissileDefendedToast_m3291472947 (DefendMiniGameController_t1844356491 * __this, LaunchedMissile_t1542515810 * ___missile0, bool ___defendSuccess1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController::GetMissileStatusFromServer(System.String)
extern "C"  void DefendMiniGameController_GetMissileStatusFromServer_m3887033409 (DefendMiniGameController_t1844356491 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendMiniGameController::SendFlurryEvent(System.Boolean)
extern "C"  void DefendMiniGameController_SendFlurryEvent_m2530309103 (DefendMiniGameController_t1844356491 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DefendMiniGameController::WaitToUnloadModal(System.Single)
extern "C"  Il2CppObject * DefendMiniGameController_WaitToUnloadModal_m2260267007 (DefendMiniGameController_t1844356491 * __this, float ___seconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
