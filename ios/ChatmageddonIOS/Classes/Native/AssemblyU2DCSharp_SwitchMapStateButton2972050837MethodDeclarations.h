﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwitchMapStateButton
struct SwitchMapStateButton_t2972050837;
// UITweener
struct UITweener_t2986641582;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UITweener2986641582.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"

// System.Void SwitchMapStateButton::.ctor()
extern "C"  void SwitchMapStateButton__ctor_m2339583474 (SwitchMapStateButton_t2972050837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchMapStateButton::Awake()
extern "C"  void SwitchMapStateButton_Awake_m1705679173 (SwitchMapStateButton_t2972050837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchMapStateButton::SetButtonState(System.Boolean)
extern "C"  void SwitchMapStateButton_SetButtonState_m4049156416 (SwitchMapStateButton_t2972050837 * __this, bool ___display0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchMapStateButton::OnTweenFinishedHandler(UITweener)
extern "C"  void SwitchMapStateButton_OnTweenFinishedHandler_m2518867410 (SwitchMapStateButton_t2972050837 * __this, UITweener_t2986641582 * ___tweener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchMapStateButton::OnButtonClick()
extern "C"  void SwitchMapStateButton_OnButtonClick_m949571513 (SwitchMapStateButton_t2972050837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchMapStateButton::SendFlurryEvent(MapState)
extern "C"  void SwitchMapStateButton_SendFlurryEvent_m1772063089 (SwitchMapStateButton_t2972050837 * __this, int32_t ___mapState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchMapStateButton::SetActiveState(System.Boolean)
extern "C"  void SwitchMapStateButton_SetActiveState_m1661917890 (SwitchMapStateButton_t2972050837 * __this, bool ___activeState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
