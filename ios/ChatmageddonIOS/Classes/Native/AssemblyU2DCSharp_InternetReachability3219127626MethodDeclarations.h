﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InternetReachability
struct InternetReachability_t3219127626;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void InternetReachability::.ctor()
extern "C"  void InternetReachability__ctor_m2979610571 (InternetReachability_t3219127626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability::Init(System.String,System.Int32)
extern "C"  void InternetReachability_Init_m2787521246 (InternetReachability_t3219127626 * __this, String_t* ___CheckUrl0, int32_t ___timeoutSeconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InternetReachability::CheckForInternetConnection()
extern "C"  bool InternetReachability_CheckForInternetConnection_m1437318007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability::AsyncCallback()
extern "C"  void InternetReachability_AsyncCallback_m2677030694 (InternetReachability_t3219127626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability::StartInternetPoll(System.Single)
extern "C"  void InternetReachability_StartInternetPoll_m310774546 (InternetReachability_t3219127626 * __this, float ___pollTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability::StopInternetPoll()
extern "C"  void InternetReachability_StopInternetPoll_m341775135 (InternetReachability_t3219127626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InternetReachability::InternetPollCheck(System.Single)
extern "C"  Il2CppObject * InternetReachability_InternetPollCheck_m479972790 (InternetReachability_t3219127626 * __this, float ___pollTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability::OnInternetPollComplete(System.Boolean)
extern "C"  void InternetReachability_OnInternetPollComplete_m3532945402 (InternetReachability_t3219127626 * __this, bool ___connected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability::.cctor()
extern "C"  void InternetReachability__cctor_m2827515516 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
