﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchSequenceReport
struct LaunchSequenceReport_t983071960;
// UILabel
struct UILabel_t1795115428;
// TweenAlpha
struct TweenAlpha_t2421518635;

#include "AssemblyU2DCSharp_SceneLoaderUI1806459353.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LauncherLoader
struct  LauncherLoader_t1212054665  : public SceneLoaderUI_t1806459353
{
public:
	// LaunchSequenceReport LauncherLoader::sequenceReport
	LaunchSequenceReport_t983071960 * ___sequenceReport_2;
	// LaunchSequenceReport LauncherLoader::finishSequenceReport
	LaunchSequenceReport_t983071960 * ___finishSequenceReport_3;
	// UILabel LauncherLoader::backfireLabel
	UILabel_t1795115428 * ___backfireLabel_4;
	// TweenAlpha LauncherLoader::initBulb
	TweenAlpha_t2421518635 * ___initBulb_5;
	// TweenAlpha LauncherLoader::engageBulb
	TweenAlpha_t2421518635 * ___engageBulb_6;
	// TweenAlpha LauncherLoader::launchBulb
	TweenAlpha_t2421518635 * ___launchBulb_7;

public:
	inline static int32_t get_offset_of_sequenceReport_2() { return static_cast<int32_t>(offsetof(LauncherLoader_t1212054665, ___sequenceReport_2)); }
	inline LaunchSequenceReport_t983071960 * get_sequenceReport_2() const { return ___sequenceReport_2; }
	inline LaunchSequenceReport_t983071960 ** get_address_of_sequenceReport_2() { return &___sequenceReport_2; }
	inline void set_sequenceReport_2(LaunchSequenceReport_t983071960 * value)
	{
		___sequenceReport_2 = value;
		Il2CppCodeGenWriteBarrier(&___sequenceReport_2, value);
	}

	inline static int32_t get_offset_of_finishSequenceReport_3() { return static_cast<int32_t>(offsetof(LauncherLoader_t1212054665, ___finishSequenceReport_3)); }
	inline LaunchSequenceReport_t983071960 * get_finishSequenceReport_3() const { return ___finishSequenceReport_3; }
	inline LaunchSequenceReport_t983071960 ** get_address_of_finishSequenceReport_3() { return &___finishSequenceReport_3; }
	inline void set_finishSequenceReport_3(LaunchSequenceReport_t983071960 * value)
	{
		___finishSequenceReport_3 = value;
		Il2CppCodeGenWriteBarrier(&___finishSequenceReport_3, value);
	}

	inline static int32_t get_offset_of_backfireLabel_4() { return static_cast<int32_t>(offsetof(LauncherLoader_t1212054665, ___backfireLabel_4)); }
	inline UILabel_t1795115428 * get_backfireLabel_4() const { return ___backfireLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_backfireLabel_4() { return &___backfireLabel_4; }
	inline void set_backfireLabel_4(UILabel_t1795115428 * value)
	{
		___backfireLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___backfireLabel_4, value);
	}

	inline static int32_t get_offset_of_initBulb_5() { return static_cast<int32_t>(offsetof(LauncherLoader_t1212054665, ___initBulb_5)); }
	inline TweenAlpha_t2421518635 * get_initBulb_5() const { return ___initBulb_5; }
	inline TweenAlpha_t2421518635 ** get_address_of_initBulb_5() { return &___initBulb_5; }
	inline void set_initBulb_5(TweenAlpha_t2421518635 * value)
	{
		___initBulb_5 = value;
		Il2CppCodeGenWriteBarrier(&___initBulb_5, value);
	}

	inline static int32_t get_offset_of_engageBulb_6() { return static_cast<int32_t>(offsetof(LauncherLoader_t1212054665, ___engageBulb_6)); }
	inline TweenAlpha_t2421518635 * get_engageBulb_6() const { return ___engageBulb_6; }
	inline TweenAlpha_t2421518635 ** get_address_of_engageBulb_6() { return &___engageBulb_6; }
	inline void set_engageBulb_6(TweenAlpha_t2421518635 * value)
	{
		___engageBulb_6 = value;
		Il2CppCodeGenWriteBarrier(&___engageBulb_6, value);
	}

	inline static int32_t get_offset_of_launchBulb_7() { return static_cast<int32_t>(offsetof(LauncherLoader_t1212054665, ___launchBulb_7)); }
	inline TweenAlpha_t2421518635 * get_launchBulb_7() const { return ___launchBulb_7; }
	inline TweenAlpha_t2421518635 ** get_address_of_launchBulb_7() { return &___launchBulb_7; }
	inline void set_launchBulb_7(TweenAlpha_t2421518635 * value)
	{
		___launchBulb_7 = value;
		Il2CppCodeGenWriteBarrier(&___launchBulb_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
