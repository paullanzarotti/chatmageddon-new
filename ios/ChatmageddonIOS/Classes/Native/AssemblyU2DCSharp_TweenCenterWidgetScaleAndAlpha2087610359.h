﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IPPickerBase
struct IPPickerBase_t4159478266;
// TweenScale
struct TweenScale_t2697902175;
// TweenAlpha
struct TweenAlpha_t2421518635;
// UIWidget
struct UIWidget_t1453041918;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TweenCenterWidgetScaleAndAlpha
struct  TweenCenterWidgetScaleAndAlpha_t2087610359  : public MonoBehaviour_t1158329972
{
public:
	// IPPickerBase TweenCenterWidgetScaleAndAlpha::picker
	IPPickerBase_t4159478266 * ___picker_2;
	// System.Single TweenCenterWidgetScaleAndAlpha::scaleFactor
	float ___scaleFactor_3;
	// System.Single TweenCenterWidgetScaleAndAlpha::duration
	float ___duration_4;
	// TweenScale TweenCenterWidgetScaleAndAlpha::scaleTween
	TweenScale_t2697902175 * ___scaleTween_5;
	// TweenAlpha TweenCenterWidgetScaleAndAlpha::alphaTween
	TweenAlpha_t2421518635 * ___alphaTween_6;
	// UIWidget TweenCenterWidgetScaleAndAlpha::currentWidget
	UIWidget_t1453041918 * ___currentWidget_7;

public:
	inline static int32_t get_offset_of_picker_2() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScaleAndAlpha_t2087610359, ___picker_2)); }
	inline IPPickerBase_t4159478266 * get_picker_2() const { return ___picker_2; }
	inline IPPickerBase_t4159478266 ** get_address_of_picker_2() { return &___picker_2; }
	inline void set_picker_2(IPPickerBase_t4159478266 * value)
	{
		___picker_2 = value;
		Il2CppCodeGenWriteBarrier(&___picker_2, value);
	}

	inline static int32_t get_offset_of_scaleFactor_3() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScaleAndAlpha_t2087610359, ___scaleFactor_3)); }
	inline float get_scaleFactor_3() const { return ___scaleFactor_3; }
	inline float* get_address_of_scaleFactor_3() { return &___scaleFactor_3; }
	inline void set_scaleFactor_3(float value)
	{
		___scaleFactor_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScaleAndAlpha_t2087610359, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_scaleTween_5() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScaleAndAlpha_t2087610359, ___scaleTween_5)); }
	inline TweenScale_t2697902175 * get_scaleTween_5() const { return ___scaleTween_5; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_5() { return &___scaleTween_5; }
	inline void set_scaleTween_5(TweenScale_t2697902175 * value)
	{
		___scaleTween_5 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_5, value);
	}

	inline static int32_t get_offset_of_alphaTween_6() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScaleAndAlpha_t2087610359, ___alphaTween_6)); }
	inline TweenAlpha_t2421518635 * get_alphaTween_6() const { return ___alphaTween_6; }
	inline TweenAlpha_t2421518635 ** get_address_of_alphaTween_6() { return &___alphaTween_6; }
	inline void set_alphaTween_6(TweenAlpha_t2421518635 * value)
	{
		___alphaTween_6 = value;
		Il2CppCodeGenWriteBarrier(&___alphaTween_6, value);
	}

	inline static int32_t get_offset_of_currentWidget_7() { return static_cast<int32_t>(offsetof(TweenCenterWidgetScaleAndAlpha_t2087610359, ___currentWidget_7)); }
	inline UIWidget_t1453041918 * get_currentWidget_7() const { return ___currentWidget_7; }
	inline UIWidget_t1453041918 ** get_address_of_currentWidget_7() { return &___currentWidget_7; }
	inline void set_currentWidget_7(UIWidget_t1453041918 * value)
	{
		___currentWidget_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentWidget_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
