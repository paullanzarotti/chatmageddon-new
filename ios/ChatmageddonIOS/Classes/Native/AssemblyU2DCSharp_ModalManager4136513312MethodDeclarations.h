﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModalManager
struct ModalManager_t4136513312;
// System.String
struct String_t;
// User
struct User_t719925459;
// LaunchedItem
struct LaunchedItem_t3670634427;
// LevelAward
struct LevelAward_t2605567013;
// System.Collections.Generic.List`1<Friend>
struct List_1_t2924135240;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_ModalType1526568337.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_LaunchedItem3670634427.h"
#include "AssemblyU2DCSharp_LevelAward2605567013.h"

// System.Void ModalManager::.ctor()
extern "C"  void ModalManager__ctor_m287943955 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::Init()
extern "C"  void ModalManager_Init_m4026461229 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadModalPanel()
extern "C"  void ModalManager_LoadModalPanel_m4267751610 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadModalFromResource()
extern "C"  void ModalManager_LoadModalFromResource_m3625686898 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadBaseModal()
extern "C"  void ModalManager_LoadBaseModal_m3096662003 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::UnloadModal()
extern "C"  void ModalManager_UnloadModal_m581591513 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::ClearModal()
extern "C"  void ModalManager_ClearModal_m3866349097 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::OnModalCleared()
extern "C"  void ModalManager_OnModalCleared_m3078865513 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::SetModalTitle(System.String)
extern "C"  void ModalManager_SetModalTitle_m192211224 (ModalManager_t4136513312 * __this, String_t* ___title0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::SetModalTitleLayer(System.Int32)
extern "C"  void ModalManager_SetModalTitleLayer_m660991968 (ModalManager_t4136513312 * __this, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::SetActiveModalType(ModalType)
extern "C"  void ModalManager_SetActiveModalType_m1631721045 (ModalManager_t4136513312 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::SetUpCurrentModal(System.String)
extern "C"  void ModalManager_SetUpCurrentModal_m1101057638 (ModalManager_t4136513312 * __this, String_t* ___uiName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadActivateShieldModal()
extern "C"  void ModalManager_LoadActivateShieldModal_m559335730 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadActivateMineModal()
extern "C"  void ModalManager_LoadActivateMineModal_m2048412084 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadPlayerProfileModal(User,System.Boolean)
extern "C"  void ModalManager_LoadPlayerProfileModal_m2361723656 (ModalManager_t4136513312 * __this, User_t719925459 * ___user0, bool ___switchTo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::SetupPlayerProfileModal(User)
extern "C"  void ModalManager_SetupPlayerProfileModal_m937693636 (ModalManager_t4136513312 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadDefendModal(LaunchedItem)
extern "C"  void ModalManager_LoadDefendModal_m1178097663 (ModalManager_t4136513312 * __this, LaunchedItem_t3670634427 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadMissileResultsModal(LaunchedItem)
extern "C"  void ModalManager_LoadMissileResultsModal_m2998790773 (ModalManager_t4136513312 * __this, LaunchedItem_t3670634427 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadAwardModal(LevelAward)
extern "C"  void ModalManager_LoadAwardModal_m1283904096 (ModalManager_t4136513312 * __this, LevelAward_t2605567013 * ___award0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadNotificationModal()
extern "C"  void ModalManager_LoadNotificationModal_m2289633815 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::ClearNotificationModal()
extern "C"  void ModalManager_ClearNotificationModal_m1449784788 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::LoadMapGroupModal(System.Collections.Generic.List`1<Friend>)
extern "C"  void ModalManager_LoadMapGroupModal_m2045662989 (ModalManager_t4136513312 * __this, List_1_t2924135240 * ___groupFriends0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModalManager::ClearMapGroupModal()
extern "C"  void ModalManager_ClearMapGroupModal_m3781750706 (ModalManager_t4136513312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
