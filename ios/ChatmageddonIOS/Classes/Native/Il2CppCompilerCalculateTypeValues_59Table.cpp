﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LeaderboardILP694823370.h"
#include "AssemblyU2DCSharp_LeaderboardILP_U3CwaitToRestartI1138547282.h"
#include "AssemblyU2DCSharp_LeaderboardILP_U3CWaitForWorldLe4267966070.h"
#include "AssemblyU2DCSharp_LeaderboardListItem3112309392.h"
#include "AssemblyU2DCSharp_LeaderboardManager1534320874.h"
#include "AssemblyU2DCSharp_LeaderboardManager_U3CLoadLeader2783613432.h"
#include "AssemblyU2DCSharp_LeaderboardNavigateForwardsButto1837924610.h"
#include "AssemblyU2DCSharp_LeaderboardNavScreen2356043712.h"
#include "AssemblyU2DCSharp_LeaderboardNavigationController1680977333.h"
#include "AssemblyU2DCSharp_LeaderboardPlayerItem2673913181.h"
#include "AssemblyU2DCSharp_LeaderboardSceneManager2883297036.h"
#include "AssemblyU2DCSharp_LeaderboardItemUIScaler1432927178.h"
#include "AssemblyU2DCSharp_LeaderboardUIScaler3427701301.h"
#include "AssemblyU2DCSharp_UpdateActiveLeaderboardButton3273818106.h"
#include "AssemblyU2DCSharp_LockoutState173023818.h"
#include "AssemblyU2DCSharp_LockoutManager4122842952.h"
#include "AssemblyU2DCSharp_LockoutUI1337294023.h"
#include "AssemblyU2DCSharp_NoNetworkUI1712168161.h"
#include "AssemblyU2DCSharp_NoNetworkUIScaler2051622931.h"
#include "AssemblyU2DCSharp_ConfirmPopUpCancelButton953518780.h"
#include "AssemblyU2DCSharp_ExitConfirmPopUp674592142.h"
#include "AssemblyU2DCSharp_ExitStealthButton3936490483.h"
#include "AssemblyU2DCSharp_OpenExitConfirmPopUp3055608070.h"
#include "AssemblyU2DCSharp_StealthModeUI2601479126.h"
#include "AssemblyU2DCSharp_StealthModeUIScaler786227838.h"
#include "AssemblyU2DCSharp_NoPointsBribeButton319339654.h"
#include "AssemblyU2DCSharp_NoPointsFacebookButton2783514292.h"
#include "AssemblyU2DCSharp_NoPointsInviteFriendButton2823702333.h"
#include "AssemblyU2DCSharp_NoPointsUIScaler4253144202.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton1249750581.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton_OnZeroHit3971658986.h"
#include "AssemblyU2DCSharp_NoPointsWaitButton_U3CCalculateK2972959588.h"
#include "AssemblyU2DCSharp_ZeroPointsUI1285366563.h"
#include "AssemblyU2DCSharp_ConnectionPopUp470643858.h"
#include "AssemblyU2DCSharp_ConnectionPopUp_U3CCheckHideAvai3423247869.h"
#include "AssemblyU2DCSharp_ConnectionPopUp_U3CWaitToHideCon3462675709.h"
#include "AssemblyU2DCSharp_ErrorMessageCloseButton1031829045.h"
#include "AssemblyU2DCSharp_ErrorMessagePopUp1211465891.h"
#include "AssemblyU2DCSharp_ErrorMessagePopUp_U3CWaitForView3227356967.h"
#include "AssemblyU2DCSharp_CloseLoginUIButton3048161229.h"
#include "AssemblyU2DCSharp_FacebookLoginButton2856354003.h"
#include "AssemblyU2DCSharp_ForgotPasswordButton3035499862.h"
#include "AssemblyU2DCSharp_LoginBottomUIManager3738095009.h"
#include "AssemblyU2DCSharp_UserLoginButton3500827568.h"
#include "AssemblyU2DCSharp_UsernameLoginButton2274508449.h"
#include "AssemblyU2DCSharp_LoginCenterUI890505802.h"
#include "AssemblyU2DCSharp_LoginRegistrationSceneManager2327503985.h"
#include "AssemblyU2DCSharp_NonTimedErrorMessage4057427095.h"
#include "AssemblyU2DCSharp_AccountDetailsScreen3027915693.h"
#include "AssemblyU2DCSharp_ClosecountryPicker1622155624.h"
#include "AssemblyU2DCSharp_CountryCodeData1765037603.h"
#include "AssemblyU2DCSharp_CountryCodeILP3216166666.h"
#include "AssemblyU2DCSharp_CountryCodeListItem2467463620.h"
#include "AssemblyU2DCSharp_CountryCodeListManager203952622.h"
#include "AssemblyU2DCSharp_CountryPickerButton1162517224.h"
#include "AssemblyU2DCSharp_ButtonDirection2892178281.h"
#include "AssemblyU2DCSharp_NavigateBackwardButton1567151050.h"
#include "AssemblyU2DCSharp_NavigateForwardButton3075171100.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"
#include "AssemblyU2DCSharp_PhoneNumberButton1790400683.h"
#include "AssemblyU2DCSharp_RegImportFBProfile542324572.h"
#include "AssemblyU2DCSharp_RegInput2119160278.h"
#include "AssemblyU2DCSharp_RegistrationCompleteScreen222205918.h"
#include "AssemblyU2DCSharp_RegistrationManager1533517116.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "AssemblyU2DCSharp_RegistrationNavigationcontroller539766269.h"
#include "AssemblyU2DCSharp_ResendVerificationButton1304384308.h"
#include "AssemblyU2DCSharp_UserRegistrationButton1399913110.h"
#include "AssemblyU2DCSharp_VerificationCodeInput924401746.h"
#include "AssemblyU2DCSharp_VerificationScreen1800753931.h"
#include "AssemblyU2DCSharp_VerifyCodeButton1959864684.h"
#include "AssemblyU2DCSharp_YNContinueButton318956450.h"
#include "AssemblyU2DCSharp_YNContinueButton_U3COnButtonClic1978033145.h"
#include "AssemblyU2DCSharp_YourNumberScreen2760251358.h"
#include "AssemblyU2DCSharp_RetryConnectionButton1348204850.h"
#include "AssemblyU2DCSharp_AccountDetailsUIScaler3070489523.h"
#include "AssemblyU2DCSharp_CountryCodeItemUIScaler2043820862.h"
#include "AssemblyU2DCSharp_CountryCodeUIScaler3177948777.h"
#include "AssemblyU2DCSharp_ErrorMessageUIScaler1585671755.h"
#include "AssemblyU2DCSharp_LoginBottomUIScaler3264146212.h"
#include "AssemblyU2DCSharp_LoginCenterUIScaler3891368654.h"
#include "AssemblyU2DCSharp_VerificationUIScaler217609153.h"
#include "AssemblyU2DCSharp_LoginLoadingIndicator3051072036.h"
#include "AssemblyU2DCSharp_LoginSuccessSceneManager2204684339.h"
#include "AssemblyU2DCSharp_LoginSuccessSceneManager_U3CChec1167711508.h"
#include "AssemblyU2DCSharp_LoginSuccessSceneManager_U3CCont3024659562.h"
#include "AssemblyU2DCSharp_LoginSuccessSceneManager_U3CWait3768918776.h"
#include "AssemblyU2DCSharp_LoginSuccessUIScaler10393798.h"
#include "AssemblyU2DCSharp_AttackMiniGameStateButton3439103466.h"
#include "AssemblyU2DCSharp_DefendMiniGameController1844356491.h"
#include "AssemblyU2DCSharp_DefendMiniGameController_U3CSendDe36876571.h"
#include "AssemblyU2DCSharp_DefendMiniGameController_U3CGetM3057462542.h"
#include "AssemblyU2DCSharp_DefendMiniGameController_U3CWait1093209425.h"
#include "AssemblyU2DCSharp_DefendHoldButton1338853911.h"
#include "AssemblyU2DCSharp_DefendHoldButton_U3CUpdateHoldPr1153919390.h"
#include "AssemblyU2DCSharp_HoldBarController3195732834.h"
#include "AssemblyU2DCSharp_HoldMiniGame1783545348.h"
#include "AssemblyU2DCSharp_CloseMineMiniGameButton3701522082.h"
#include "AssemblyU2DCSharp_MineCountdownProgressBar1258896602.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5900 = { sizeof (LeaderboardILP_t694823370), -1, sizeof(LeaderboardILP_t694823370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5900[25] = 
{
	LeaderboardILP_t694823370::get_offset_of_listLoadingIndicator_33(),
	LeaderboardILP_t694823370::get_offset_of_allTimeLeaderboard_34(),
	LeaderboardILP_t694823370::get_offset_of_currentLoadedType_35(),
	LeaderboardILP_t694823370::get_offset_of_leaderboardScaleTween_36(),
	LeaderboardILP_t694823370::get_offset_of_maxNameLength_37(),
	LeaderboardILP_t694823370::get_offset_of_listLoaded_38(),
	LeaderboardILP_t694823370::get_offset_of_leadersArray_39(),
	LeaderboardILP_t694823370::get_offset_of_playerUIItem_40(),
	LeaderboardILP_t694823370::get_offset_of_playerIncluded_41(),
	LeaderboardILP_t694823370::get_offset_of_playerRank_42(),
	LeaderboardILP_t694823370::get_offset_of_activatingType_43(),
	LeaderboardILP_t694823370::get_offset_of_waitRoutine_44(),
	LeaderboardILP_t694823370::get_offset_of_worldWaitRoutine_45(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_46(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_47(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_48(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_49(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_50(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_51(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_52(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_53(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_54(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_55(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_56(),
	LeaderboardILP_t694823370_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5901 = { sizeof (U3CwaitToRestartILU3Ec__Iterator0_t1138547282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5901[4] = 
{
	U3CwaitToRestartILU3Ec__Iterator0_t1138547282::get_offset_of_U24this_0(),
	U3CwaitToRestartILU3Ec__Iterator0_t1138547282::get_offset_of_U24current_1(),
	U3CwaitToRestartILU3Ec__Iterator0_t1138547282::get_offset_of_U24disposing_2(),
	U3CwaitToRestartILU3Ec__Iterator0_t1138547282::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5902 = { sizeof (U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5902[5] = 
{
	U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070::get_offset_of_allTime_0(),
	U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070::get_offset_of_U24this_1(),
	U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070::get_offset_of_U24current_2(),
	U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070::get_offset_of_U24disposing_3(),
	U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5903 = { sizeof (LeaderboardListItem_t3112309392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5903[12] = 
{
	LeaderboardListItem_t3112309392::get_offset_of_user_10(),
	LeaderboardListItem_t3112309392::get_offset_of_listPopulator_11(),
	LeaderboardListItem_t3112309392::get_offset_of_rankLabel_12(),
	LeaderboardListItem_t3112309392::get_offset_of_nameLabel_13(),
	LeaderboardListItem_t3112309392::get_offset_of_userRankLabel_14(),
	LeaderboardListItem_t3112309392::get_offset_of_scoreLabel_15(),
	LeaderboardListItem_t3112309392::get_offset_of_scrollView_16(),
	LeaderboardListItem_t3112309392::get_offset_of_draggablePanel_17(),
	LeaderboardListItem_t3112309392::get_offset_of_mTrans_18(),
	LeaderboardListItem_t3112309392::get_offset_of_mScroll_19(),
	LeaderboardListItem_t3112309392::get_offset_of_mAutoFind_20(),
	LeaderboardListItem_t3112309392::get_offset_of_mStarted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5904 = { sizeof (LeaderboardManager_t1534320874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5904[5] = 
{
	LeaderboardManager_t1534320874::get_offset_of_pageDataCount_3(),
	LeaderboardManager_t1534320874::get_offset_of_dailyWorldLeaderboard_4(),
	LeaderboardManager_t1534320874::get_offset_of_playerDailyRank_5(),
	LeaderboardManager_t1534320874::get_offset_of_allTimeWorldLeaderboard_6(),
	LeaderboardManager_t1534320874::get_offset_of_playerAllTimeRank_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5905 = { sizeof (U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5905[3] = 
{
	U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432::get_offset_of_daily_0(),
	U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432::get_offset_of_page_1(),
	U3CLoadLeaderboardPageU3Ec__AnonStorey0_t2783613432::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5906 = { sizeof (LeaderboardNavigateForwardsButton_t1837924610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5906[1] = 
{
	LeaderboardNavigateForwardsButton_t1837924610::get_offset_of_nextScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5907 = { sizeof (LeaderboardNavScreen_t2356043712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5907[4] = 
{
	LeaderboardNavScreen_t2356043712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5908 = { sizeof (LeaderboardNavigationController_t1680977333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5908[11] = 
{
	LeaderboardNavigationController_t1680977333::get_offset_of_dailyLabel_8(),
	LeaderboardNavigationController_t1680977333::get_offset_of_allTimeLabel_9(),
	LeaderboardNavigationController_t1680977333::get_offset_of_innerLabel_10(),
	LeaderboardNavigationController_t1680977333::get_offset_of_outerLabel_11(),
	LeaderboardNavigationController_t1680977333::get_offset_of_worldLabel_12(),
	LeaderboardNavigationController_t1680977333::get_offset_of_dailyObject_13(),
	LeaderboardNavigationController_t1680977333::get_offset_of_allTimeObject_14(),
	LeaderboardNavigationController_t1680977333::get_offset_of_activeLeaderboardType_15(),
	LeaderboardNavigationController_t1680977333::get_offset_of_activePos_16(),
	LeaderboardNavigationController_t1680977333::get_offset_of_leftInactivePos_17(),
	LeaderboardNavigationController_t1680977333::get_offset_of_rightInactivePos_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5909 = { sizeof (LeaderboardPlayerItem_t2673913181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5909[8] = 
{
	LeaderboardPlayerItem_t2673913181::get_offset_of_user_2(),
	LeaderboardPlayerItem_t2673913181::get_offset_of_maxNameLength_3(),
	LeaderboardPlayerItem_t2673913181::get_offset_of_rankLabel_4(),
	LeaderboardPlayerItem_t2673913181::get_offset_of_nameLabel_5(),
	LeaderboardPlayerItem_t2673913181::get_offset_of_userRankLabel_6(),
	LeaderboardPlayerItem_t2673913181::get_offset_of_scoreLabel_7(),
	LeaderboardPlayerItem_t2673913181::get_offset_of_panelTween_8(),
	LeaderboardPlayerItem_t2673913181::get_offset_of_hidingPanel_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5910 = { sizeof (LeaderboardSceneManager_t2883297036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5910[1] = 
{
	LeaderboardSceneManager_t2883297036::get_offset_of_leaderboardCenterUI_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5911 = { sizeof (LeaderboardItemUIScaler_t1432927178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5911[6] = 
{
	LeaderboardItemUIScaler_t1432927178::get_offset_of_background_14(),
	LeaderboardItemUIScaler_t1432927178::get_offset_of_backgroundCollider_15(),
	LeaderboardItemUIScaler_t1432927178::get_offset_of_rankLabel_16(),
	LeaderboardItemUIScaler_t1432927178::get_offset_of_nameLabel_17(),
	LeaderboardItemUIScaler_t1432927178::get_offset_of_userRankLabel_18(),
	LeaderboardItemUIScaler_t1432927178::get_offset_of_scoreLabel_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5912 = { sizeof (LeaderboardUIScaler_t3427701301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5912[5] = 
{
	LeaderboardUIScaler_t3427701301::get_offset_of_background_27(),
	LeaderboardUIScaler_t3427701301::get_offset_of_dailyScreenTween_28(),
	LeaderboardUIScaler_t3427701301::get_offset_of_dailyPanel_29(),
	LeaderboardUIScaler_t3427701301::get_offset_of_allTimeScreenTween_30(),
	LeaderboardUIScaler_t3427701301::get_offset_of_allTimePanel_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5913 = { sizeof (UpdateActiveLeaderboardButton_t3273818106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5913[1] = 
{
	UpdateActiveLeaderboardButton_t3273818106::get_offset_of_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5914 = { sizeof (LockoutState_t173023818)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5914[4] = 
{
	LockoutState_t173023818::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5915 = { sizeof (LockoutManager_t4122842952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5915[3] = 
{
	LockoutManager_t4122842952::get_offset_of_noPointsUI_3(),
	LockoutManager_t4122842952::get_offset_of_stealthModeUI_4(),
	LockoutManager_t4122842952::get_offset_of_noNetworkUI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5916 = { sizeof (LockoutUI_t1337294023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5916[1] = 
{
	LockoutUI_t1337294023::get_offset_of_unloading_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5917 = { sizeof (NoNetworkUI_t1712168161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5917[6] = 
{
	NoNetworkUI_t1712168161::get_offset_of_backgroundSprite_3(),
	NoNetworkUI_t1712168161::get_offset_of_backgroundTween_4(),
	NoNetworkUI_t1712168161::get_offset_of_backgroundFlashTween_5(),
	NoNetworkUI_t1712168161::get_offset_of_backgroundColourTween_6(),
	NoNetworkUI_t1712168161::get_offset_of_contentTween_7(),
	NoNetworkUI_t1712168161::get_offset_of_uiPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5918 = { sizeof (NoNetworkUIScaler_t2051622931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5918[2] = 
{
	NoNetworkUIScaler_t2051622931::get_offset_of_background_14(),
	NoNetworkUIScaler_t2051622931::get_offset_of_backgroundCollider_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5919 = { sizeof (ConfirmPopUpCancelButton_t953518780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5919[1] = 
{
	ConfirmPopUpCancelButton_t953518780::get_offset_of_popUp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5920 = { sizeof (ExitConfirmPopUp_t674592142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5920[3] = 
{
	ExitConfirmPopUp_t674592142::get_offset_of_backgroundAlphaTween_2(),
	ExitConfirmPopUp_t674592142::get_offset_of_contentScaleTween_3(),
	ExitConfirmPopUp_t674592142::get_offset_of_popUpClosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5921 = { sizeof (ExitStealthButton_t3936490483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5921[2] = 
{
	ExitStealthButton_t3936490483::get_offset_of_popUp_5(),
	ExitStealthButton_t3936490483::get_offset_of_cancelButton_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5922 = { sizeof (OpenExitConfirmPopUp_t3055608070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5922[1] = 
{
	OpenExitConfirmPopUp_t3055608070::get_offset_of_popUp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5923 = { sizeof (StealthModeUI_t2601479126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5923[4] = 
{
	StealthModeUI_t2601479126::get_offset_of_backgroundTween_3(),
	StealthModeUI_t2601479126::get_offset_of_exitButtonTween_4(),
	StealthModeUI_t2601479126::get_offset_of_contentTween_5(),
	StealthModeUI_t2601479126::get_offset_of_calculator_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5924 = { sizeof (StealthModeUIScaler_t786227838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5924[6] = 
{
	StealthModeUIScaler_t786227838::get_offset_of_background_14(),
	StealthModeUIScaler_t786227838::get_offset_of_backgroundCollider_15(),
	StealthModeUIScaler_t786227838::get_offset_of_exitButton_16(),
	StealthModeUIScaler_t786227838::get_offset_of_exitButtonDivider_17(),
	StealthModeUIScaler_t786227838::get_offset_of_confirmBackground_18(),
	StealthModeUIScaler_t786227838::get_offset_of_confirmBackgroundCollider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5925 = { sizeof (NoPointsBribeButton_t319339654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5925[1] = 
{
	NoPointsBribeButton_t319339654::get_offset_of_bucksAmountLabel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5926 = { sizeof (NoPointsFacebookButton_t2783514292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5927 = { sizeof (NoPointsInviteFriendButton_t2823702333), -1, sizeof(NoPointsInviteFriendButton_t2823702333_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5927[1] = 
{
	NoPointsInviteFriendButton_t2823702333_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5928 = { sizeof (NoPointsUIScaler_t4253144202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5928[2] = 
{
	NoPointsUIScaler_t4253144202::get_offset_of_background_14(),
	NoPointsUIScaler_t4253144202::get_offset_of_backgroundCollider_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5929 = { sizeof (NoPointsWaitButton_t1249750581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5929[6] = 
{
	NoPointsWaitButton_t1249750581::get_offset_of_timeLabel_5(),
	NoPointsWaitButton_t1249750581::get_offset_of_waitTimer_6(),
	NoPointsWaitButton_t1249750581::get_offset_of_exitButton_7(),
	NoPointsWaitButton_t1249750581::get_offset_of_allowExit_8(),
	NoPointsWaitButton_t1249750581::get_offset_of_calcRoutine_9(),
	NoPointsWaitButton_t1249750581::get_offset_of_onZeroHit_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5930 = { sizeof (OnZeroHit_t3971658986), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5931 = { sizeof (U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5931[6] = 
{
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588::get_offset_of_user_0(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588::get_offset_of_U3CdifferenceU3E__0_1(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588::get_offset_of_U24this_2(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588::get_offset_of_U24current_3(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588::get_offset_of_U24disposing_4(),
	U3CCalculateKnockoutTimeU3Ec__Iterator0_t2972959588::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5932 = { sizeof (ZeroPointsUI_t1285366563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5932[4] = 
{
	ZeroPointsUI_t1285366563::get_offset_of_backgroundTween_3(),
	ZeroPointsUI_t1285366563::get_offset_of_contentTween_4(),
	ZeroPointsUI_t1285366563::get_offset_of_attackerNameLabel_5(),
	ZeroPointsUI_t1285366563::get_offset_of_waitTimer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5933 = { sizeof (ConnectionPopUp_t470643858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5933[18] = 
{
	ConnectionPopUp_t470643858::get_offset_of_UI_3(),
	ConnectionPopUp_t470643858::get_offset_of_backgroundAlphaTween_4(),
	ConnectionPopUp_t470643858::get_offset_of_loadingIndicator_5(),
	ConnectionPopUp_t470643858::get_offset_of_indicatorAlphaTween_6(),
	ConnectionPopUp_t470643858::get_offset_of_reconnectLabel_7(),
	ConnectionPopUp_t470643858::get_offset_of_reconnectLabelAlphaTween_8(),
	ConnectionPopUp_t470643858::get_offset_of_messageLabel_9(),
	ConnectionPopUp_t470643858::get_offset_of_messageLabelAlphaTween_10(),
	ConnectionPopUp_t470643858::get_offset_of_buttonSpriteAlphaTween_11(),
	ConnectionPopUp_t470643858::get_offset_of_buttonLabelAlphaTween_12(),
	ConnectionPopUp_t470643858::get_offset_of_retryButton_13(),
	ConnectionPopUp_t470643858::get_offset_of_backgroundUIClosing_14(),
	ConnectionPopUp_t470643858::get_offset_of_attempt_15(),
	ConnectionPopUp_t470643858::get_offset_of_closeAttempt_16(),
	ConnectionPopUp_t470643858::get_offset_of_checkRoutine_17(),
	ConnectionPopUp_t470643858::get_offset_of_waitRoutine_18(),
	ConnectionPopUp_t470643858::get_offset_of_currentTime_19(),
	ConnectionPopUp_t470643858::get_offset_of_showTime_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5934 = { sizeof (U3CCheckHideAvailableU3Ec__Iterator0_t3423247869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5934[4] = 
{
	U3CCheckHideAvailableU3Ec__Iterator0_t3423247869::get_offset_of_U24this_0(),
	U3CCheckHideAvailableU3Ec__Iterator0_t3423247869::get_offset_of_U24current_1(),
	U3CCheckHideAvailableU3Ec__Iterator0_t3423247869::get_offset_of_U24disposing_2(),
	U3CCheckHideAvailableU3Ec__Iterator0_t3423247869::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5935 = { sizeof (U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5935[4] = 
{
	U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709::get_offset_of_U24this_0(),
	U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709::get_offset_of_U24current_1(),
	U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709::get_offset_of_U24disposing_2(),
	U3CWaitToHideConnectingUIU3Ec__Iterator1_t3462675709::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5936 = { sizeof (ErrorMessageCloseButton_t1031829045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5937 = { sizeof (ErrorMessagePopUp_t1211465891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5937[9] = 
{
	ErrorMessagePopUp_t1211465891::get_offset_of_panel_3(),
	ErrorMessagePopUp_t1211465891::get_offset_of_background_4(),
	ErrorMessagePopUp_t1211465891::get_offset_of_backgroundTexture_5(),
	ErrorMessagePopUp_t1211465891::get_offset_of_backgroundCollider_6(),
	ErrorMessagePopUp_t1211465891::get_offset_of_errorLabel_7(),
	ErrorMessagePopUp_t1211465891::get_offset_of_alphaTween_8(),
	ErrorMessagePopUp_t1211465891::get_offset_of_errorViewTime_9(),
	ErrorMessagePopUp_t1211465891::get_offset_of_timed_10(),
	ErrorMessagePopUp_t1211465891::get_offset_of_unloadingMessage_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5938 = { sizeof (U3CWaitForViewTimeU3Ec__Iterator0_t3227356967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5938[4] = 
{
	U3CWaitForViewTimeU3Ec__Iterator0_t3227356967::get_offset_of_U24this_0(),
	U3CWaitForViewTimeU3Ec__Iterator0_t3227356967::get_offset_of_U24current_1(),
	U3CWaitForViewTimeU3Ec__Iterator0_t3227356967::get_offset_of_U24disposing_2(),
	U3CWaitForViewTimeU3Ec__Iterator0_t3227356967::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5939 = { sizeof (CloseLoginUIButton_t3048161229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5940 = { sizeof (FacebookLoginButton_t2856354003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5941 = { sizeof (ForgotPasswordButton_t3035499862), -1, sizeof(ForgotPasswordButton_t3035499862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5941[1] = 
{
	ForgotPasswordButton_t3035499862_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5942 = { sizeof (LoginBottomUIManager_t3738095009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5942[5] = 
{
	LoginBottomUIManager_t3738095009::get_offset_of_startPage_3(),
	LoginBottomUIManager_t3738095009::get_offset_of_startTween_4(),
	LoginBottomUIManager_t3738095009::get_offset_of_loginButton_5(),
	LoginBottomUIManager_t3738095009::get_offset_of_menuLoginButton_6(),
	LoginBottomUIManager_t3738095009::get_offset_of_registerationButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5943 = { sizeof (UserLoginButton_t3500827568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5943[3] = 
{
	UserLoginButton_t3500827568::get_offset_of_startPage_5(),
	UserLoginButton_t3500827568::get_offset_of_loaded_6(),
	UserLoginButton_t3500827568::get_offset_of_facebookLogin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5944 = { sizeof (UsernameLoginButton_t2274508449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5944[6] = 
{
	UsernameLoginButton_t2274508449::get_offset_of_loginUI_5(),
	UsernameLoginButton_t2274508449::get_offset_of_closeLoginButton_6(),
	UsernameLoginButton_t2274508449::get_offset_of_usernameInput_7(),
	UsernameLoginButton_t2274508449::get_offset_of_passwordInput_8(),
	UsernameLoginButton_t2274508449::get_offset_of_unloaded_9(),
	UsernameLoginButton_t2274508449::get_offset_of_userLoginSuccess_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5945 = { sizeof (LoginCenterUI_t890505802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5945[1] = 
{
	LoginCenterUI_t890505802::get_offset_of_background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5946 = { sizeof (LoginRegistrationSceneManager_t2327503985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5946[6] = 
{
	LoginRegistrationSceneManager_t2327503985::get_offset_of_LoginBottomUI_17(),
	LoginRegistrationSceneManager_t2327503985::get_offset_of_LoginTopUI_18(),
	LoginRegistrationSceneManager_t2327503985::get_offset_of_LoginCenterUI_19(),
	LoginRegistrationSceneManager_t2327503985::get_offset_of_DeviceCameraUI_20(),
	LoginRegistrationSceneManager_t2327503985::get_offset_of_ConnectionPopUpUI_21(),
	LoginRegistrationSceneManager_t2327503985::get_offset_of_ErrorMessageUI_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5947 = { sizeof (NonTimedErrorMessage_t4057427095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5947[1] = 
{
	NonTimedErrorMessage_t4057427095::get_offset_of_messageContent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5948 = { sizeof (AccountDetailsScreen_t3027915693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5948[2] = 
{
	AccountDetailsScreen_t3027915693::get_offset_of_pnButton_3(),
	AccountDetailsScreen_t3027915693::get_offset_of_ADUIclosing_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5949 = { sizeof (ClosecountryPicker_t1622155624), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5950 = { sizeof (CountryCodeData_t1765037603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5950[3] = 
{
	CountryCodeData_t1765037603::get_offset_of_countryName_0(),
	CountryCodeData_t1765037603::get_offset_of_countryShortName_1(),
	CountryCodeData_t1765037603::get_offset_of_countryCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5951 = { sizeof (CountryCodeILP_t3216166666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5951[2] = 
{
	CountryCodeILP_t3216166666::get_offset_of_listLoaded_33(),
	CountryCodeILP_t3216166666::get_offset_of_ccArray_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5952 = { sizeof (CountryCodeListItem_t2467463620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5952[10] = 
{
	CountryCodeListItem_t2467463620::get_offset_of_uiScaler_10(),
	CountryCodeListItem_t2467463620::get_offset_of_listPopulator_11(),
	CountryCodeListItem_t2467463620::get_offset_of_data_12(),
	CountryCodeListItem_t2467463620::get_offset_of_codeLabel_13(),
	CountryCodeListItem_t2467463620::get_offset_of_scrollView_14(),
	CountryCodeListItem_t2467463620::get_offset_of_draggablePanel_15(),
	CountryCodeListItem_t2467463620::get_offset_of_mTrans_16(),
	CountryCodeListItem_t2467463620::get_offset_of_mScroll_17(),
	CountryCodeListItem_t2467463620::get_offset_of_mAutoFind_18(),
	CountryCodeListItem_t2467463620::get_offset_of_mStarted_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5953 = { sizeof (CountryCodeListManager_t203952622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5953[8] = 
{
	CountryCodeListManager_t203952622::get_offset_of_posTween_3(),
	CountryCodeListManager_t203952622::get_offset_of_codeILP_4(),
	CountryCodeListManager_t203952622::get_offset_of_backgroundPanel_5(),
	CountryCodeListManager_t203952622::get_offset_of_topContent_6(),
	CountryCodeListManager_t203952622::get_offset_of_bottomContent_7(),
	CountryCodeListManager_t203952622::get_offset_of_titleBar_8(),
	CountryCodeListManager_t203952622::get_offset_of_background_9(),
	CountryCodeListManager_t203952622::get_offset_of_UIClosing_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5954 = { sizeof (CountryPickerButton_t1162517224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5955 = { sizeof (ButtonDirection_t2892178281)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5955[5] = 
{
	ButtonDirection_t2892178281::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5956 = { sizeof (NavigateBackwardButton_t1567151050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5956[1] = 
{
	NavigateBackwardButton_t1567151050::get_offset_of_buttonSprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5957 = { sizeof (NavigateForwardButton_t3075171100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5957[2] = 
{
	NavigateForwardButton_t3075171100::get_offset_of_nextScreen_5(),
	NavigateForwardButton_t3075171100::get_offset_of_buttonLabel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5958 = { sizeof (NavigationDirection_t2114074025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5958[3] = 
{
	NavigationDirection_t2114074025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5959 = { sizeof (NavigationScreen_t2333230110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5959[1] = 
{
	NavigationScreen_t2333230110::get_offset_of_loading_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5960 = { sizeof (PhoneNumberButton_t1790400683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5960[2] = 
{
	PhoneNumberButton_t1790400683::get_offset_of_buttonLabel_5(),
	PhoneNumberButton_t1790400683::get_offset_of_buttonColour_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5961 = { sizeof (RegImportFBProfile_t542324572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5962 = { sizeof (RegInput_t2119160278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5962[3] = 
{
	RegInput_t2119160278::get_offset_of_inputActive_5(),
	RegInput_t2119160278::get_offset_of_topBar_6(),
	RegInput_t2119160278::get_offset_of_bottomBar_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5963 = { sizeof (RegistrationCompleteScreen_t222205918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5963[6] = 
{
	RegistrationCompleteScreen_t222205918::get_offset_of_firstNameInput_3(),
	RegistrationCompleteScreen_t222205918::get_offset_of_lastNameInput_4(),
	RegistrationCompleteScreen_t222205918::get_offset_of_usernameInput_5(),
	RegistrationCompleteScreen_t222205918::get_offset_of_emailInput_6(),
	RegistrationCompleteScreen_t222205918::get_offset_of_passwordInput_7(),
	RegistrationCompleteScreen_t222205918::get_offset_of_confirmPasswordInput_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5964 = { sizeof (RegistrationManager_t1533517116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5964[31] = 
{
	RegistrationManager_t1533517116::get_offset_of_facebookRegStart_3(),
	RegistrationManager_t1533517116::get_offset_of_numberVerified_4(),
	RegistrationManager_t1533517116::get_offset_of_registrationComplete_5(),
	RegistrationManager_t1533517116::get_offset_of_selectedCountryCode_6(),
	RegistrationManager_t1533517116::get_offset_of_countryCode_7(),
	RegistrationManager_t1533517116::get_offset_of_phoneNumber_8(),
	RegistrationManager_t1533517116::get_offset_of_fullPhoneNumber_9(),
	RegistrationManager_t1533517116::get_offset_of_mobileHash_10(),
	RegistrationManager_t1533517116::get_offset_of_mobilePinID_11(),
	RegistrationManager_t1533517116::get_offset_of_mobilePin_12(),
	RegistrationManager_t1533517116::get_offset_of_profileImageSelected_13(),
	RegistrationManager_t1533517116::get_offset_of_profileImageURL_14(),
	RegistrationManager_t1533517116::get_offset_of_countryNameLabel_15(),
	RegistrationManager_t1533517116::get_offset_of_countryCodeLabel_16(),
	RegistrationManager_t1533517116::get_offset_of_validEmail_17(),
	RegistrationManager_t1533517116::get_offset_of_validUsername_18(),
	RegistrationManager_t1533517116::get_offset_of_validPassword_19(),
	RegistrationManager_t1533517116::get_offset_of_validFirstName_20(),
	RegistrationManager_t1533517116::get_offset_of_validLastName_21(),
	RegistrationManager_t1533517116::get_offset_of_regContentTween_22(),
	RegistrationManager_t1533517116::get_offset_of_reginputs_23(),
	RegistrationManager_t1533517116::get_offset_of_regInputActive_24(),
	RegistrationManager_t1533517116::get_offset_of_regInputInactive_25(),
	RegistrationManager_t1533517116::get_offset_of_yourNumberLabel_26(),
	RegistrationManager_t1533517116::get_offset_of_verifyCodeLabel_27(),
	RegistrationManager_t1533517116::get_offset_of_ADPhonenumberLabel_28(),
	RegistrationManager_t1533517116::get_offset_of_regComplete_29(),
	RegistrationManager_t1533517116::get_offset_of_firstNameInput_30(),
	RegistrationManager_t1533517116::get_offset_of_lastNameInput_31(),
	RegistrationManager_t1533517116::get_offset_of_emailInput_32(),
	RegistrationManager_t1533517116::get_offset_of_profileTexture_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5965 = { sizeof (RegNavScreen_t667173359)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5965[6] = 
{
	RegNavScreen_t667173359::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5966 = { sizeof (RegistrationNavigationcontroller_t539766269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5966[12] = 
{
	RegistrationNavigationcontroller_t539766269::get_offset_of_activePos_8(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_leftInactivePos_9(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_rightInactivePos_10(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_startPage_11(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_contentObject_12(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_titleLabel_13(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_yourNumberObject_14(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_verificationObject_15(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_accountDetailsObject_16(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_registrationCompleteObject_17(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_mainNavigateForwardButton_18(),
	RegistrationNavigationcontroller_t539766269::get_offset_of_mainNavigateBackwardButton_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5967 = { sizeof (ResendVerificationButton_t1304384308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5967[1] = 
{
	ResendVerificationButton_t1304384308::get_offset_of_codeInput_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5968 = { sizeof (UserRegistrationButton_t1399913110), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5969 = { sizeof (VerificationCodeInput_t924401746), -1, sizeof(VerificationCodeInput_t924401746_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5969[2] = 
{
	VerificationCodeInput_t924401746::get_offset_of_codeinput_5(),
	VerificationCodeInput_t924401746_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5970 = { sizeof (VerificationScreen_t1800753931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5970[4] = 
{
	VerificationScreen_t1800753931::get_offset_of_phonenumLabel_3(),
	VerificationScreen_t1800753931::get_offset_of_codeinput_4(),
	VerificationScreen_t1800753931::get_offset_of_reSendButton_5(),
	VerificationScreen_t1800753931::get_offset_of_verificationUIclosing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5971 = { sizeof (VerifyCodeButton_t1959864684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5971[2] = 
{
	VerifyCodeButton_t1959864684::get_offset_of_input_5(),
	VerifyCodeButton_t1959864684::get_offset_of_buttonBackground_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5972 = { sizeof (YNContinueButton_t318956450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5972[2] = 
{
	YNContinueButton_t318956450::get_offset_of_countryCodeLabel_7(),
	YNContinueButton_t318956450::get_offset_of_phoneNumberLabel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5973 = { sizeof (U3COnButtonClickU3Ec__AnonStorey0_t1978033145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5973[2] = 
{
	U3COnButtonClickU3Ec__AnonStorey0_t1978033145::get_offset_of_countyCode_0(),
	U3COnButtonClickU3Ec__AnonStorey0_t1978033145::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5974 = { sizeof (YourNumberScreen_t2760251358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5974[5] = 
{
	YourNumberScreen_t2760251358::get_offset_of_contentObject_3(),
	YourNumberScreen_t2760251358::get_offset_of_phoneNumLabel_4(),
	YourNumberScreen_t2760251358::get_offset_of_continueButton_5(),
	YourNumberScreen_t2760251358::get_offset_of_registerUIclosing_6(),
	YourNumberScreen_t2760251358::get_offset_of_yourNumUIclosing_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5975 = { sizeof (RetryConnectionButton_t1348204850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5975[1] = 
{
	RetryConnectionButton_t1348204850::get_offset_of_buttonTween_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5976 = { sizeof (AccountDetailsUIScaler_t3070489523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5976[25] = 
{
	AccountDetailsUIScaler_t3070489523::get_offset_of_profilePhoto_14(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_names_15(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_setPhotoSeperator_16(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_setPhotoLabel_17(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_setPhotoCollider_18(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_fbSeperator_19(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_fbIcon_20(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_fbLabel_21(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_fbCollider_22(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_emailSeperator_23(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_emailLabel_24(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_emailCollider_25(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_usernameSeperator_26(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_usernameLabel_27(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_usernameCollider_28(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_passwordSeperator_29(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_passwordLabel_30(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_passwordCollider_31(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_confirmPasswordSeperator_32(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_confirmPasswordLabel_33(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_confirmCollider_34(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_phoneSeperator_35(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_phoneLabel_36(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_phoneCollider_37(),
	AccountDetailsUIScaler_t3070489523::get_offset_of_phoneBottomSeperator_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5977 = { sizeof (CountryCodeItemUIScaler_t2043820862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5977[3] = 
{
	CountryCodeItemUIScaler_t2043820862::get_offset_of_itemlabel_14(),
	CountryCodeItemUIScaler_t2043820862::get_offset_of_itemcollider_15(),
	CountryCodeItemUIScaler_t2043820862::get_offset_of_itemDivider_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5978 = { sizeof (CountryCodeUIScaler_t3177948777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5978[4] = 
{
	CountryCodeUIScaler_t3177948777::get_offset_of_background_14(),
	CountryCodeUIScaler_t3177948777::get_offset_of_navBackButton_15(),
	CountryCodeUIScaler_t3177948777::get_offset_of_titleDivider_16(),
	CountryCodeUIScaler_t3177948777::get_offset_of_scrollViewPanel_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5979 = { sizeof (ErrorMessageUIScaler_t1585671755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5979[1] = 
{
	ErrorMessageUIScaler_t1585671755::get_offset_of_errorCollider_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5980 = { sizeof (LoginBottomUIScaler_t3264146212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5980[28] = 
{
	LoginBottomUIScaler_t3264146212::get_offset_of_loginButtonCollider_14(),
	LoginBottomUIScaler_t3264146212::get_offset_of_loginButtonBackground_15(),
	LoginBottomUIScaler_t3264146212::get_offset_of_closeUICollider_16(),
	LoginBottomUIScaler_t3264146212::get_offset_of_usernameButtonCollider_17(),
	LoginBottomUIScaler_t3264146212::get_offset_of_usernameButtonBackground_18(),
	LoginBottomUIScaler_t3264146212::get_offset_of_usernameButtonDivider_19(),
	LoginBottomUIScaler_t3264146212::get_offset_of_usernameButtonTopDivider_20(),
	LoginBottomUIScaler_t3264146212::get_offset_of_usernameButtonLabel_21(),
	LoginBottomUIScaler_t3264146212::get_offset_of_passwordButtonCollider_22(),
	LoginBottomUIScaler_t3264146212::get_offset_of_passwordButtonBackground_23(),
	LoginBottomUIScaler_t3264146212::get_offset_of_passwordButtonDivider_24(),
	LoginBottomUIScaler_t3264146212::get_offset_of_passwordButtonLabel_25(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regBackground_26(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regTitleDivider_27(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regNavBackButton_28(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regNavForwardButton_29(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regYNTopDivider_30(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regYNMiddleDivider_31(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regYNBottomDivider_32(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regYNContinueButtonCollider_33(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regYNContinueButton_34(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regCountryPickerCollider_35(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regCountryPickerButton_36(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regCountryPickerLabel_37(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regCountryCodeLabel_38(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regCountryCodeSeperator_39(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regYNPhoneNumberCollider_40(),
	LoginBottomUIScaler_t3264146212::get_offset_of_regYNPhoneNumberLabel_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5981 = { sizeof (LoginCenterUIScaler_t3891368654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5981[1] = 
{
	LoginCenterUIScaler_t3891368654::get_offset_of_backgroundTexture_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5982 = { sizeof (VerificationUIScaler_t217609153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5982[6] = 
{
	VerificationUIScaler_t217609153::get_offset_of_topSeperator_14(),
	VerificationUIScaler_t217609153::get_offset_of_bottomSeperator_15(),
	VerificationUIScaler_t217609153::get_offset_of_codeCollider_16(),
	VerificationUIScaler_t217609153::get_offset_of_resendCollider_17(),
	VerificationUIScaler_t217609153::get_offset_of_verifyButton_18(),
	VerificationUIScaler_t217609153::get_offset_of_verifyCollider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5983 = { sizeof (LoginLoadingIndicator_t3051072036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5983[6] = 
{
	LoginLoadingIndicator_t3051072036::get_offset_of_rocketTween1_3(),
	LoginLoadingIndicator_t3051072036::get_offset_of_rocketTween2_4(),
	LoginLoadingIndicator_t3051072036::get_offset_of_rocketTween3_5(),
	LoginLoadingIndicator_t3051072036::get_offset_of_rocketTween4_6(),
	LoginLoadingIndicator_t3051072036::get_offset_of_rocketTween5_7(),
	LoginLoadingIndicator_t3051072036::get_offset_of_messageLabel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5984 = { sizeof (LoginSuccessSceneManager_t2204684339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5984[3] = 
{
	LoginSuccessSceneManager_t2204684339::get_offset_of_loginSuccessCenterUI_17(),
	LoginSuccessSceneManager_t2204684339::get_offset_of_connectionPopUp_18(),
	LoginSuccessSceneManager_t2204684339::get_offset_of_LoadFunctionToRecall_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5985 = { sizeof (U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5985[2] = 
{
	U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508::get_offset_of_loadFunction_0(),
	U3CCheckInternetConnectionU3Ec__AnonStorey2_t1167711508::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5986 = { sizeof (U3CContactWaitU3Ec__Iterator0_t3024659562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5986[4] = 
{
	U3CContactWaitU3Ec__Iterator0_t3024659562::get_offset_of_U24this_0(),
	U3CContactWaitU3Ec__Iterator0_t3024659562::get_offset_of_U24current_1(),
	U3CContactWaitU3Ec__Iterator0_t3024659562::get_offset_of_U24disposing_2(),
	U3CContactWaitU3Ec__Iterator0_t3024659562::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5987 = { sizeof (U3CWaitForAnimationU3Ec__Iterator1_t3768918776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5987[3] = 
{
	U3CWaitForAnimationU3Ec__Iterator1_t3768918776::get_offset_of_U24current_0(),
	U3CWaitForAnimationU3Ec__Iterator1_t3768918776::get_offset_of_U24disposing_1(),
	U3CWaitForAnimationU3Ec__Iterator1_t3768918776::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5988 = { sizeof (LoginSuccessUIScaler_t10393798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5988[1] = 
{
	LoginSuccessUIScaler_t10393798::get_offset_of_backgroundTexture_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5989 = { sizeof (AttackMiniGameStateButton_t3439103466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5989[7] = 
{
	AttackMiniGameStateButton_t3439103466::get_offset_of_gameController_5(),
	AttackMiniGameStateButton_t3439103466::get_offset_of_topLine_6(),
	AttackMiniGameStateButton_t3439103466::get_offset_of_fireGuage_7(),
	AttackMiniGameStateButton_t3439103466::get_offset_of_buttonsTween_8(),
	AttackMiniGameStateButton_t3439103466::get_offset_of_colourTween_9(),
	AttackMiniGameStateButton_t3439103466::get_offset_of_buttonlabel_10(),
	AttackMiniGameStateButton_t3439103466::get_offset_of_buttonsClosing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5990 = { sizeof (DefendMiniGameController_t1844356491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5990[2] = 
{
	DefendMiniGameController_t1844356491::get_offset_of_modal_4(),
	DefendMiniGameController_t1844356491::get_offset_of_navScreen_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5991 = { sizeof (U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5991[3] = 
{
	U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571::get_offset_of_defendSuccess_0(),
	U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571::get_offset_of_defendTime_1(),
	U3CSendDefendMissileServerCallU3Ec__AnonStorey1_t36876571::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5992 = { sizeof (U3CGetMissileStatusFromServerU3Ec__AnonStorey2_t3057462542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5992[2] = 
{
	U3CGetMissileStatusFromServerU3Ec__AnonStorey2_t3057462542::get_offset_of_id_0(),
	U3CGetMissileStatusFromServerU3Ec__AnonStorey2_t3057462542::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5993 = { sizeof (U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5993[4] = 
{
	U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425::get_offset_of_seconds_0(),
	U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425::get_offset_of_U24current_1(),
	U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425::get_offset_of_U24disposing_2(),
	U3CWaitToUnloadModalU3Ec__Iterator0_t1093209425::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5994 = { sizeof (DefendHoldButton_t1338853911), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5994[6] = 
{
	DefendHoldButton_t1338853911::get_offset_of_miniGame_5(),
	DefendHoldButton_t1338853911::get_offset_of_barController_6(),
	DefendHoldButton_t1338853911::get_offset_of_totalHoldTime_7(),
	DefendHoldButton_t1338853911::get_offset_of_heldDown_8(),
	DefendHoldButton_t1338853911::get_offset_of_secondsHeld_9(),
	DefendHoldButton_t1338853911::get_offset_of_holdRoutine_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5995 = { sizeof (U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5995[4] = 
{
	U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390::get_offset_of_U24this_0(),
	U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390::get_offset_of_U24current_1(),
	U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390::get_offset_of_U24disposing_2(),
	U3CUpdateHoldProgressU3Ec__Iterator0_t1153919390::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5996 = { sizeof (HoldBarController_t3195732834), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5997 = { sizeof (HoldMiniGame_t1783545348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5997[6] = 
{
	HoldMiniGame_t1783545348::get_offset_of_holdbutton_6(),
	HoldMiniGame_t1783545348::get_offset_of_modelParent_7(),
	HoldMiniGame_t1783545348::get_offset_of_currentModel_8(),
	HoldMiniGame_t1783545348::get_offset_of_itemLight_9(),
	HoldMiniGame_t1783545348::get_offset_of_leftMarker_10(),
	HoldMiniGame_t1783545348::get_offset_of_rightMarker_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5998 = { sizeof (CloseMineMiniGameButton_t3701522082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5999 = { sizeof (MineCountdownProgressBar_t1258896602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5999[4] = 
{
	MineCountdownProgressBar_t1258896602::get_offset_of_infoController_13(),
	MineCountdownProgressBar_t1258896602::get_offset_of_countdownLabel_14(),
	MineCountdownProgressBar_t1258896602::get_offset_of_countdownTimeAmount_15(),
	MineCountdownProgressBar_t1258896602::get_offset_of_numberOn_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
