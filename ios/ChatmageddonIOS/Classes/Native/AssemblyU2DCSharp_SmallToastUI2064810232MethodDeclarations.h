﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmallToastUI
struct SmallToastUI_t2064810232;
// Bread
struct Bread_t457172030;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Bread457172030.h"

// System.Void SmallToastUI::.ctor()
extern "C"  void SmallToastUI__ctor_m3577154257 (SmallToastUI_t2064810232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmallToastUI::OnAwake()
extern "C"  void SmallToastUI_OnAwake_m1389589825 (SmallToastUI_t2064810232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmallToastUI::LoadToastUI(Bread)
extern "C"  void SmallToastUI_LoadToastUI_m923834538 (SmallToastUI_t2064810232 * __this, Bread_t457172030 * ___bread0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmallToastUI::OnScaleTweenFinished()
extern "C"  void SmallToastUI_OnScaleTweenFinished_m3650703559 (SmallToastUI_t2064810232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SmallToastUI::DisplayWait()
extern "C"  Il2CppObject * SmallToastUI_DisplayWait_m1352742948 (SmallToastUI_t2064810232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmallToastUI::UnloadToastUI()
extern "C"  void SmallToastUI_UnloadToastUI_m1601413049 (SmallToastUI_t2064810232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmallToastUI::OnAlphaFadeComplete()
extern "C"  void SmallToastUI_OnAlphaFadeComplete_m2033713823 (SmallToastUI_t2064810232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
