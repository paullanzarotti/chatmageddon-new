﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapManager
struct MapManager_t3593696545;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Mine
struct Mine_t2729441277;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MapState1547378305.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_MapManager_LatLon3749872052.h"
#include "AssemblyU2DCSharp_MapManager_DistanceUnit2764741455.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MapManager::.ctor()
extern "C"  void MapManager__ctor_m4263344916 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::InitMap()
extern "C"  void MapManager_InitMap_m582314344 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::SetMapState(MapState,System.Boolean)
extern "C"  void MapManager_SetMapState_m1833492461 (MapManager_t3593696545 * __this, int32_t ___state0, bool ___transition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::ForceCurrentMapState()
extern "C"  void MapManager_ForceCurrentMapState_m4268007775 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::ShowMapUI(MapState)
extern "C"  void MapManager_ShowMapUI_m452962086 (MapManager_t3593696545 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MapManager::WaitToUpdate()
extern "C"  Il2CppObject * MapManager_WaitToUpdate_m2850381771 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::ShowInnerFriendsUI()
extern "C"  void MapManager_ShowInnerFriendsUI_m3354809676 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::ShowOuterFriendsUI()
extern "C"  void MapManager_ShowOuterFriendsUI_m1643072983 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::ShowMinesUI()
extern "C"  void MapManager_ShowMinesUI_m2328260655 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::ShowPlayerUI()
extern "C"  void MapManager_ShowPlayerUI_m4187728388 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::RemoveMarkers()
extern "C"  void MapManager_RemoveMarkers_m3172716671 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::UpdateMarkers()
extern "C"  void MapManager_UpdateMarkers_m1616873208 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::GroupMarkers()
extern "C"  void MapManager_GroupMarkers_m4121749464 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::UnGroupMarkers()
extern "C"  void MapManager_UnGroupMarkers_m3157303137 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::SetMapUIActive(System.Boolean)
extern "C"  void MapManager_SetMapUIActive_m4077652589 (MapManager_t3593696545 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::SetMapMeshActive(System.Boolean)
extern "C"  void MapManager_SetMapMeshActive_m2409759898 (MapManager_t3593696545 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::BlurMap(System.Boolean)
extern "C"  void MapManager_BlurMap_m3197330212 (MapManager_t3593696545 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::SetRadarActive(System.Boolean)
extern "C"  void MapManager_SetRadarActive_m4055092745 (MapManager_t3593696545 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::ActivateMapCollision(System.Boolean)
extern "C"  void MapManager_ActivateMapCollision_m1063091672 (MapManager_t3593696545 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::CentreOnGroup()
extern "C"  void MapManager_CentreOnGroup_m71342537 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::CenterOnPlayerPosition(System.Boolean)
extern "C"  void MapManager_CenterOnPlayerPosition_m2858239295 (MapManager_t3593696545 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::StopCenterVelocityRoutine()
extern "C"  void MapManager_StopCenterVelocityRoutine_m3918130558 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::CenterOnPosition(UnityEngine.Vector2)
extern "C"  void MapManager_CenterOnPosition_m3534112583 (MapManager_t3593696545 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::SetZoomLevel(System.Int32,System.Boolean)
extern "C"  void MapManager_SetZoomLevel_m3019132709 (MapManager_t3593696545 * __this, int32_t ___zoomLevel0, bool ___playSFX1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 MapManager::GetCenterOffsetForLayer()
extern "C"  Vector2_t2243707579  MapManager_GetCenterOffsetForLayer_m4250204763 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MapManager::CheckMineInTriggerRange(Mine)
extern "C"  bool MapManager_CheckMineInTriggerRange_m279616690 (MapManager_t3593696545 * __this, Mine_t2729441277 * ___surroundingMine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MapManager::AllowMineTrigger()
extern "C"  bool MapManager_AllowMineTrigger_m2294612850 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::TriggerMine(Mine)
extern "C"  void MapManager_TriggerMine_m45962698 (MapManager_t3593696545 * __this, Mine_t2729441277 * ___mine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double MapManager::ToRad(System.Double)
extern "C"  double MapManager_ToRad_m3871562647 (Il2CppObject * __this /* static, unused */, double ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double MapManager::GetDistanceKM(MapManager/LatLon,MapManager/LatLon)
extern "C"  double MapManager_GetDistanceKM_m1107405674 (Il2CppObject * __this /* static, unused */, LatLon_t3749872052  ___point10, LatLon_t3749872052  ___point21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double MapManager::DistanceTo(System.Double,System.Double,System.Double,System.Double,MapManager/DistanceUnit)
extern "C"  double MapManager_DistanceTo_m2177895300 (Il2CppObject * __this /* static, unused */, double ___lat10, double ___lon11, double ___lat22, double ___lon23, int32_t ___unit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::OnChangeZoom()
extern "C"  void MapManager_OnChangeZoom_m2166204828 (MapManager_t3593696545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::OnLocationChanged(UnityEngine.Vector2)
extern "C"  void MapManager_OnLocationChanged_m1023306374 (MapManager_t3593696545 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::<TriggerMine>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void MapManager_U3CTriggerMineU3Em__0_m2137271653 (Il2CppObject * __this /* static, unused */, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapManager::<OnLocationChanged>m__1(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void MapManager_U3COnLocationChangedU3Em__1_m940447497 (MapManager_t3593696545 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
