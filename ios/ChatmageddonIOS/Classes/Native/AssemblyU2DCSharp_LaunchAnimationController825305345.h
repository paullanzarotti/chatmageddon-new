﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Skybox
struct Skybox_t2033495038;
// GlitchEffect
struct GlitchEffect_t4241067900;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchAnimationController
struct  LaunchAnimationController_t825305345  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject LaunchAnimationController::animationObject
	GameObject_t1756533147 * ___animationObject_2;
	// UnityEngine.GameObject LaunchAnimationController::standardMissile
	GameObject_t1756533147 * ___standardMissile_3;
	// UnityEngine.GameObject LaunchAnimationController::stealthMissile
	GameObject_t1756533147 * ___stealthMissile_4;
	// UnityEngine.GameObject LaunchAnimationController::shakerMissile
	GameObject_t1756533147 * ___shakerMissile_5;
	// UnityEngine.GameObject LaunchAnimationController::screamerMissile
	GameObject_t1756533147 * ___screamerMissile_6;
	// UnityEngine.GameObject LaunchAnimationController::heatSeekerMissile
	GameObject_t1756533147 * ___heatSeekerMissile_7;
	// UnityEngine.Color LaunchAnimationController::dayFogColour
	Color_t2020392075  ___dayFogColour_8;
	// UnityEngine.Color LaunchAnimationController::duskFogColour
	Color_t2020392075  ___duskFogColour_9;
	// UnityEngine.Color LaunchAnimationController::nightFogColour
	Color_t2020392075  ___nightFogColour_10;
	// UnityEngine.GameObject LaunchAnimationController::dayLight
	GameObject_t1756533147 * ___dayLight_11;
	// UnityEngine.GameObject LaunchAnimationController::duskLight
	GameObject_t1756533147 * ___duskLight_12;
	// UnityEngine.GameObject LaunchAnimationController::nightLight
	GameObject_t1756533147 * ___nightLight_13;
	// UnityEngine.Skybox LaunchAnimationController::daySky
	Skybox_t2033495038 * ___daySky_14;
	// UnityEngine.Skybox LaunchAnimationController::duskSky
	Skybox_t2033495038 * ___duskSky_15;
	// UnityEngine.Skybox LaunchAnimationController::nightSky
	Skybox_t2033495038 * ___nightSky_16;
	// GlitchEffect LaunchAnimationController::cameraGlitch
	GlitchEffect_t4241067900 * ___cameraGlitch_17;

public:
	inline static int32_t get_offset_of_animationObject_2() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___animationObject_2)); }
	inline GameObject_t1756533147 * get_animationObject_2() const { return ___animationObject_2; }
	inline GameObject_t1756533147 ** get_address_of_animationObject_2() { return &___animationObject_2; }
	inline void set_animationObject_2(GameObject_t1756533147 * value)
	{
		___animationObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___animationObject_2, value);
	}

	inline static int32_t get_offset_of_standardMissile_3() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___standardMissile_3)); }
	inline GameObject_t1756533147 * get_standardMissile_3() const { return ___standardMissile_3; }
	inline GameObject_t1756533147 ** get_address_of_standardMissile_3() { return &___standardMissile_3; }
	inline void set_standardMissile_3(GameObject_t1756533147 * value)
	{
		___standardMissile_3 = value;
		Il2CppCodeGenWriteBarrier(&___standardMissile_3, value);
	}

	inline static int32_t get_offset_of_stealthMissile_4() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___stealthMissile_4)); }
	inline GameObject_t1756533147 * get_stealthMissile_4() const { return ___stealthMissile_4; }
	inline GameObject_t1756533147 ** get_address_of_stealthMissile_4() { return &___stealthMissile_4; }
	inline void set_stealthMissile_4(GameObject_t1756533147 * value)
	{
		___stealthMissile_4 = value;
		Il2CppCodeGenWriteBarrier(&___stealthMissile_4, value);
	}

	inline static int32_t get_offset_of_shakerMissile_5() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___shakerMissile_5)); }
	inline GameObject_t1756533147 * get_shakerMissile_5() const { return ___shakerMissile_5; }
	inline GameObject_t1756533147 ** get_address_of_shakerMissile_5() { return &___shakerMissile_5; }
	inline void set_shakerMissile_5(GameObject_t1756533147 * value)
	{
		___shakerMissile_5 = value;
		Il2CppCodeGenWriteBarrier(&___shakerMissile_5, value);
	}

	inline static int32_t get_offset_of_screamerMissile_6() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___screamerMissile_6)); }
	inline GameObject_t1756533147 * get_screamerMissile_6() const { return ___screamerMissile_6; }
	inline GameObject_t1756533147 ** get_address_of_screamerMissile_6() { return &___screamerMissile_6; }
	inline void set_screamerMissile_6(GameObject_t1756533147 * value)
	{
		___screamerMissile_6 = value;
		Il2CppCodeGenWriteBarrier(&___screamerMissile_6, value);
	}

	inline static int32_t get_offset_of_heatSeekerMissile_7() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___heatSeekerMissile_7)); }
	inline GameObject_t1756533147 * get_heatSeekerMissile_7() const { return ___heatSeekerMissile_7; }
	inline GameObject_t1756533147 ** get_address_of_heatSeekerMissile_7() { return &___heatSeekerMissile_7; }
	inline void set_heatSeekerMissile_7(GameObject_t1756533147 * value)
	{
		___heatSeekerMissile_7 = value;
		Il2CppCodeGenWriteBarrier(&___heatSeekerMissile_7, value);
	}

	inline static int32_t get_offset_of_dayFogColour_8() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___dayFogColour_8)); }
	inline Color_t2020392075  get_dayFogColour_8() const { return ___dayFogColour_8; }
	inline Color_t2020392075 * get_address_of_dayFogColour_8() { return &___dayFogColour_8; }
	inline void set_dayFogColour_8(Color_t2020392075  value)
	{
		___dayFogColour_8 = value;
	}

	inline static int32_t get_offset_of_duskFogColour_9() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___duskFogColour_9)); }
	inline Color_t2020392075  get_duskFogColour_9() const { return ___duskFogColour_9; }
	inline Color_t2020392075 * get_address_of_duskFogColour_9() { return &___duskFogColour_9; }
	inline void set_duskFogColour_9(Color_t2020392075  value)
	{
		___duskFogColour_9 = value;
	}

	inline static int32_t get_offset_of_nightFogColour_10() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___nightFogColour_10)); }
	inline Color_t2020392075  get_nightFogColour_10() const { return ___nightFogColour_10; }
	inline Color_t2020392075 * get_address_of_nightFogColour_10() { return &___nightFogColour_10; }
	inline void set_nightFogColour_10(Color_t2020392075  value)
	{
		___nightFogColour_10 = value;
	}

	inline static int32_t get_offset_of_dayLight_11() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___dayLight_11)); }
	inline GameObject_t1756533147 * get_dayLight_11() const { return ___dayLight_11; }
	inline GameObject_t1756533147 ** get_address_of_dayLight_11() { return &___dayLight_11; }
	inline void set_dayLight_11(GameObject_t1756533147 * value)
	{
		___dayLight_11 = value;
		Il2CppCodeGenWriteBarrier(&___dayLight_11, value);
	}

	inline static int32_t get_offset_of_duskLight_12() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___duskLight_12)); }
	inline GameObject_t1756533147 * get_duskLight_12() const { return ___duskLight_12; }
	inline GameObject_t1756533147 ** get_address_of_duskLight_12() { return &___duskLight_12; }
	inline void set_duskLight_12(GameObject_t1756533147 * value)
	{
		___duskLight_12 = value;
		Il2CppCodeGenWriteBarrier(&___duskLight_12, value);
	}

	inline static int32_t get_offset_of_nightLight_13() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___nightLight_13)); }
	inline GameObject_t1756533147 * get_nightLight_13() const { return ___nightLight_13; }
	inline GameObject_t1756533147 ** get_address_of_nightLight_13() { return &___nightLight_13; }
	inline void set_nightLight_13(GameObject_t1756533147 * value)
	{
		___nightLight_13 = value;
		Il2CppCodeGenWriteBarrier(&___nightLight_13, value);
	}

	inline static int32_t get_offset_of_daySky_14() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___daySky_14)); }
	inline Skybox_t2033495038 * get_daySky_14() const { return ___daySky_14; }
	inline Skybox_t2033495038 ** get_address_of_daySky_14() { return &___daySky_14; }
	inline void set_daySky_14(Skybox_t2033495038 * value)
	{
		___daySky_14 = value;
		Il2CppCodeGenWriteBarrier(&___daySky_14, value);
	}

	inline static int32_t get_offset_of_duskSky_15() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___duskSky_15)); }
	inline Skybox_t2033495038 * get_duskSky_15() const { return ___duskSky_15; }
	inline Skybox_t2033495038 ** get_address_of_duskSky_15() { return &___duskSky_15; }
	inline void set_duskSky_15(Skybox_t2033495038 * value)
	{
		___duskSky_15 = value;
		Il2CppCodeGenWriteBarrier(&___duskSky_15, value);
	}

	inline static int32_t get_offset_of_nightSky_16() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___nightSky_16)); }
	inline Skybox_t2033495038 * get_nightSky_16() const { return ___nightSky_16; }
	inline Skybox_t2033495038 ** get_address_of_nightSky_16() { return &___nightSky_16; }
	inline void set_nightSky_16(Skybox_t2033495038 * value)
	{
		___nightSky_16 = value;
		Il2CppCodeGenWriteBarrier(&___nightSky_16, value);
	}

	inline static int32_t get_offset_of_cameraGlitch_17() { return static_cast<int32_t>(offsetof(LaunchAnimationController_t825305345, ___cameraGlitch_17)); }
	inline GlitchEffect_t4241067900 * get_cameraGlitch_17() const { return ___cameraGlitch_17; }
	inline GlitchEffect_t4241067900 ** get_address_of_cameraGlitch_17() { return &___cameraGlitch_17; }
	inline void set_cameraGlitch_17(GlitchEffect_t4241067900 * value)
	{
		___cameraGlitch_17 = value;
		Il2CppCodeGenWriteBarrier(&___cameraGlitch_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
