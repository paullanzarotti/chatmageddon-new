﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonAudioManager
struct ChatmageddonAudioManager_t766956996;

#include "codegen/il2cpp-codegen.h"

// System.Void ChatmageddonAudioManager::.ctor()
extern "C"  void ChatmageddonAudioManager__ctor_m1341269935 (ChatmageddonAudioManager_t766956996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonAudioManager::Awake()
extern "C"  void ChatmageddonAudioManager_Awake_m707126788 (ChatmageddonAudioManager_t766956996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonAudioManager::LoadAudioSettings()
extern "C"  void ChatmageddonAudioManager_LoadAudioSettings_m3356213512 (ChatmageddonAudioManager_t766956996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonAudioManager::SetMusicActive(System.Boolean)
extern "C"  void ChatmageddonAudioManager_SetMusicActive_m465843119 (ChatmageddonAudioManager_t766956996 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonAudioManager::SetSFXActive(System.Boolean)
extern "C"  void ChatmageddonAudioManager_SetSFXActive_m4091151227 (ChatmageddonAudioManager_t766956996 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
