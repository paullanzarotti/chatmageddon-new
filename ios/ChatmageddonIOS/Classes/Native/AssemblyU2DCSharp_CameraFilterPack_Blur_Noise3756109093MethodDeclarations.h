﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Noise
struct CameraFilterPack_Blur_Noise_t3756109093;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Noise::.ctor()
extern "C"  void CameraFilterPack_Blur_Noise__ctor_m66715480 (CameraFilterPack_Blur_Noise_t3756109093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Noise::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Noise_get_material_m3521028985 (CameraFilterPack_Blur_Noise_t3756109093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::Start()
extern "C"  void CameraFilterPack_Blur_Noise_Start_m910499072 (CameraFilterPack_Blur_Noise_t3756109093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Noise_OnRenderImage_m3338218480 (CameraFilterPack_Blur_Noise_t3756109093 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::OnValidate()
extern "C"  void CameraFilterPack_Blur_Noise_OnValidate_m613162035 (CameraFilterPack_Blur_Noise_t3756109093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::Update()
extern "C"  void CameraFilterPack_Blur_Noise_Update_m4224741957 (CameraFilterPack_Blur_Noise_t3756109093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::OnDisable()
extern "C"  void CameraFilterPack_Blur_Noise_OnDisable_m40772737 (CameraFilterPack_Blur_Noise_t3756109093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
