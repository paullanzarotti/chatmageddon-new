﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineModal
struct MineModal_t1141032720;
// Mine
struct Mine_t2729441277;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"
#include "AssemblyU2DCSharp_MineAudience3529940549.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MineModal::.ctor()
extern "C"  void MineModal__ctor_m1812982299 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnUIActivate()
extern "C"  void MineModal_OnUIActivate_m672356691 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnUIDeactivate()
extern "C"  void MineModal_OnUIDeactivate_m1872346874 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnUICleared()
extern "C"  void MineModal_OnUICleared_m3306531922 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MineModal::SetExternalLightsActive(System.Boolean)
extern "C"  bool MineModal_SetExternalLightsActive_m1712770292 (MineModal_t1141032720 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::LoadMineInventory()
extern "C"  void MineModal_LoadMineInventory_m1661767334 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MineModal::MineItemsContains(Mine)
extern "C"  bool MineModal_MineItemsContains_m2104389872 (MineModal_t1141032720 * __this, Mine_t2729441277 * ___mine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::LoadDefaultItem()
extern "C"  void MineModal_LoadDefaultItem_m3816343059 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MineModal::LoadDefaultModel()
extern "C"  Il2CppObject * MineModal_LoadDefaultModel_m4222765355 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnDefaultItemShow(System.Boolean)
extern "C"  void MineModal_OnDefaultItemShow_m2754635674 (MineModal_t1141032720 * __this, bool ___closed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::LoadNextItem()
extern "C"  void MineModal_LoadNextItem_m3534873131 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::LoadPreviousItem()
extern "C"  void MineModal_LoadPreviousItem_m2808612997 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::TweenItem(System.Boolean)
extern "C"  void MineModal_TweenItem_m2338560958 (MineModal_t1141032720 * __this, bool ___right0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnTweenItemComplete()
extern "C"  void MineModal_OnTweenItemComplete_m3804780999 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::LoadItem(Mine)
extern "C"  void MineModal_LoadItem_m3927394107 (MineModal_t1141032720 * __this, Mine_t2729441277 * ___mine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::AlphaTweenLabels(System.Boolean)
extern "C"  void MineModal_AlphaTweenLabels_m3502363130 (MineModal_t1141032720 * __this, bool ___forward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::ShowItemDescription(System.Boolean)
extern "C"  void MineModal_ShowItemDescription_m1819772318 (MineModal_t1141032720 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::ShowAuidencePU(System.Boolean)
extern "C"  void MineModal_ShowAuidencePU_m2061720864 (MineModal_t1141032720 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::ChooseAudience(MineAudience)
extern "C"  void MineModal_ChooseAudience_m743355077 (MineModal_t1141032720 * __this, int32_t ___audience0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnAuidencePUTweenFinished()
extern "C"  void MineModal_OnAuidencePUTweenFinished_m1392311048 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::ActivateCurrentMine(MineAudience)
extern "C"  void MineModal_ActivateCurrentMine_m797533431 (MineModal_t1141032720 * __this, int32_t ___audience0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnPanelAlphaFinished()
extern "C"  void MineModal_OnPanelAlphaFinished_m3696496262 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::OnItemHideComplete(System.Boolean)
extern "C"  void MineModal_OnItemHideComplete_m167931413 (MineModal_t1141032720 * __this, bool ___closed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::DisplayAudienceToast()
extern "C"  void MineModal_DisplayAudienceToast_m290550480 (MineModal_t1141032720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::PlayMineActivationsSFX(Mine)
extern "C"  void MineModal_PlayMineActivationsSFX_m1611268073 (MineModal_t1141032720 * __this, Mine_t2729441277 * ___mine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineModal::<ActivateCurrentMine>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void MineModal_U3CActivateCurrentMineU3Em__0_m1708386162 (MineModal_t1141032720 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
