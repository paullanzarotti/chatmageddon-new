﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatmageddonTwitterManager
struct ChatmageddonTwitterManager_t3244794565;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoSingleton`1<ChatmageddonTwitterManager>
struct  MonoSingleton_1_t2995460285  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct MonoSingleton_1_t2995460285_StaticFields
{
public:
	// T MonoSingleton`1::m_Instance
	ChatmageddonTwitterManager_t3244794565 * ___m_Instance_2;

public:
	inline static int32_t get_offset_of_m_Instance_2() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t2995460285_StaticFields, ___m_Instance_2)); }
	inline ChatmageddonTwitterManager_t3244794565 * get_m_Instance_2() const { return ___m_Instance_2; }
	inline ChatmageddonTwitterManager_t3244794565 ** get_address_of_m_Instance_2() { return &___m_Instance_2; }
	inline void set_m_Instance_2(ChatmageddonTwitterManager_t3244794565 * value)
	{
		___m_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
