﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// MultiPhoneNumberILP
struct MultiPhoneNumberILP_t3109982769;

#include "AssemblyU2DCSharp_BaseInfiniteListPopulator_1_gen2475730487.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OtherFriendsILP
struct  OtherFriendsILP_t671720694  : public BaseInfiniteListPopulator_1_t2475730487
{
public:
	// UnityEngine.Color OtherFriendsILP::sepeartorColour
	Color_t2020392075  ___sepeartorColour_33;
	// System.Int32 OtherFriendsILP::maxNameLength
	int32_t ___maxNameLength_34;
	// System.Boolean OtherFriendsILP::listLoaded
	bool ___listLoaded_35;
	// System.Collections.ArrayList OtherFriendsILP::friendsArray
	ArrayList_t4252133567 * ___friendsArray_36;
	// MultiPhoneNumberILP OtherFriendsILP::multiNumberUI
	MultiPhoneNumberILP_t3109982769 * ___multiNumberUI_37;
	// System.Boolean OtherFriendsILP::attackList
	bool ___attackList_38;

public:
	inline static int32_t get_offset_of_sepeartorColour_33() { return static_cast<int32_t>(offsetof(OtherFriendsILP_t671720694, ___sepeartorColour_33)); }
	inline Color_t2020392075  get_sepeartorColour_33() const { return ___sepeartorColour_33; }
	inline Color_t2020392075 * get_address_of_sepeartorColour_33() { return &___sepeartorColour_33; }
	inline void set_sepeartorColour_33(Color_t2020392075  value)
	{
		___sepeartorColour_33 = value;
	}

	inline static int32_t get_offset_of_maxNameLength_34() { return static_cast<int32_t>(offsetof(OtherFriendsILP_t671720694, ___maxNameLength_34)); }
	inline int32_t get_maxNameLength_34() const { return ___maxNameLength_34; }
	inline int32_t* get_address_of_maxNameLength_34() { return &___maxNameLength_34; }
	inline void set_maxNameLength_34(int32_t value)
	{
		___maxNameLength_34 = value;
	}

	inline static int32_t get_offset_of_listLoaded_35() { return static_cast<int32_t>(offsetof(OtherFriendsILP_t671720694, ___listLoaded_35)); }
	inline bool get_listLoaded_35() const { return ___listLoaded_35; }
	inline bool* get_address_of_listLoaded_35() { return &___listLoaded_35; }
	inline void set_listLoaded_35(bool value)
	{
		___listLoaded_35 = value;
	}

	inline static int32_t get_offset_of_friendsArray_36() { return static_cast<int32_t>(offsetof(OtherFriendsILP_t671720694, ___friendsArray_36)); }
	inline ArrayList_t4252133567 * get_friendsArray_36() const { return ___friendsArray_36; }
	inline ArrayList_t4252133567 ** get_address_of_friendsArray_36() { return &___friendsArray_36; }
	inline void set_friendsArray_36(ArrayList_t4252133567 * value)
	{
		___friendsArray_36 = value;
		Il2CppCodeGenWriteBarrier(&___friendsArray_36, value);
	}

	inline static int32_t get_offset_of_multiNumberUI_37() { return static_cast<int32_t>(offsetof(OtherFriendsILP_t671720694, ___multiNumberUI_37)); }
	inline MultiPhoneNumberILP_t3109982769 * get_multiNumberUI_37() const { return ___multiNumberUI_37; }
	inline MultiPhoneNumberILP_t3109982769 ** get_address_of_multiNumberUI_37() { return &___multiNumberUI_37; }
	inline void set_multiNumberUI_37(MultiPhoneNumberILP_t3109982769 * value)
	{
		___multiNumberUI_37 = value;
		Il2CppCodeGenWriteBarrier(&___multiNumberUI_37, value);
	}

	inline static int32_t get_offset_of_attackList_38() { return static_cast<int32_t>(offsetof(OtherFriendsILP_t671720694, ___attackList_38)); }
	inline bool get_attackList_38() const { return ___attackList_38; }
	inline bool* get_address_of_attackList_38() { return &___attackList_38; }
	inline void set_attackList_38(bool value)
	{
		___attackList_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
