﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FBManager`1/LoadPictureCallback<System.Object>
struct LoadPictureCallback_t2854975038;
// System.Object
struct Il2CppObject;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void FBManager`1/LoadPictureCallback<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void LoadPictureCallback__ctor_m3122573841_gshared (LoadPictureCallback_t2854975038 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define LoadPictureCallback__ctor_m3122573841(__this, ___object0, ___method1, method) ((  void (*) (LoadPictureCallback_t2854975038 *, Il2CppObject *, IntPtr_t, const MethodInfo*))LoadPictureCallback__ctor_m3122573841_gshared)(__this, ___object0, ___method1, method)
// System.Void FBManager`1/LoadPictureCallback<System.Object>::Invoke(UnityEngine.Texture)
extern "C"  void LoadPictureCallback_Invoke_m2615178341_gshared (LoadPictureCallback_t2854975038 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method);
#define LoadPictureCallback_Invoke_m2615178341(__this, ___texture0, method) ((  void (*) (LoadPictureCallback_t2854975038 *, Texture_t2243626319 *, const MethodInfo*))LoadPictureCallback_Invoke_m2615178341_gshared)(__this, ___texture0, method)
// System.IAsyncResult FBManager`1/LoadPictureCallback<System.Object>::BeginInvoke(UnityEngine.Texture,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoadPictureCallback_BeginInvoke_m3362398610_gshared (LoadPictureCallback_t2854975038 * __this, Texture_t2243626319 * ___texture0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define LoadPictureCallback_BeginInvoke_m3362398610(__this, ___texture0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (LoadPictureCallback_t2854975038 *, Texture_t2243626319 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))LoadPictureCallback_BeginInvoke_m3362398610_gshared)(__this, ___texture0, ___callback1, ___object2, method)
// System.Void FBManager`1/LoadPictureCallback<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void LoadPictureCallback_EndInvoke_m2740201835_gshared (LoadPictureCallback_t2854975038 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define LoadPictureCallback_EndInvoke_m2740201835(__this, ___result0, method) ((  void (*) (LoadPictureCallback_t2854975038 *, Il2CppObject *, const MethodInfo*))LoadPictureCallback_EndInvoke_m2740201835_gshared)(__this, ___result0, method)
