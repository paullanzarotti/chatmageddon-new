﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B
struct U3CDescendantNodesU3Ec__Iterator1B_t137499000;
// System.Xml.Linq.XNode
struct XNode_t2707504214;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XNode>
struct IEnumerator_1_t183028041;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::.ctor()
extern "C"  void U3CDescendantNodesU3Ec__Iterator1B__ctor_m3492840563 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XNode System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::System.Collections.Generic.IEnumerator<System.Xml.Linq.XNode>.get_Current()
extern "C"  XNode_t2707504214 * U3CDescendantNodesU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XNodeU3E_get_Current_m305205224 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDescendantNodesU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m2181652417 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CDescendantNodesU3Ec__Iterator1B_System_Collections_IEnumerable_GetEnumerator_m3787816320 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XNode> System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator()
extern "C"  Il2CppObject* U3CDescendantNodesU3Ec__Iterator1B_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XNodeU3E_GetEnumerator_m3995460915 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::MoveNext()
extern "C"  bool U3CDescendantNodesU3Ec__Iterator1B_MoveNext_m2968166021 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::Dispose()
extern "C"  void U3CDescendantNodesU3Ec__Iterator1B_Dispose_m54141840 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XContainer/<DescendantNodes>c__Iterator1B::Reset()
extern "C"  void U3CDescendantNodesU3Ec__Iterator1B_Reset_m2295028990 (U3CDescendantNodesU3Ec__Iterator1B_t137499000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
