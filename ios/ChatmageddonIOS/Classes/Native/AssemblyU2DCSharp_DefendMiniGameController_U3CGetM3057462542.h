﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DefendMiniGameController
struct DefendMiniGameController_t1844356491;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefendMiniGameController/<GetMissileStatusFromServer>c__AnonStorey2
struct  U3CGetMissileStatusFromServerU3Ec__AnonStorey2_t3057462542  : public Il2CppObject
{
public:
	// System.String DefendMiniGameController/<GetMissileStatusFromServer>c__AnonStorey2::id
	String_t* ___id_0;
	// DefendMiniGameController DefendMiniGameController/<GetMissileStatusFromServer>c__AnonStorey2::$this
	DefendMiniGameController_t1844356491 * ___U24this_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CGetMissileStatusFromServerU3Ec__AnonStorey2_t3057462542, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetMissileStatusFromServerU3Ec__AnonStorey2_t3057462542, ___U24this_1)); }
	inline DefendMiniGameController_t1844356491 * get_U24this_1() const { return ___U24this_1; }
	inline DefendMiniGameController_t1844356491 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DefendMiniGameController_t1844356491 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
