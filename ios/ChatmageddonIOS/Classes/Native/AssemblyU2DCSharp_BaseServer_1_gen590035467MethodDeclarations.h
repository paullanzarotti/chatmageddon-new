﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1<System.Object>
struct BaseServer_1_t590035467;
// System.String
struct String_t;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;
// User
struct User_t719925459;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void BaseServer`1<System.Object>::.ctor()
extern "C"  void BaseServer_1__ctor_m1812252370_gshared (BaseServer_1_t590035467 * __this, const MethodInfo* method);
#define BaseServer_1__ctor_m1812252370(__this, method) ((  void (*) (BaseServer_1_t590035467 *, const MethodInfo*))BaseServer_1__ctor_m1812252370_gshared)(__this, method)
// System.Void BaseServer`1<System.Object>::Awake()
extern "C"  void BaseServer_1_Awake_m3052512987_gshared (BaseServer_1_t590035467 * __this, const MethodInfo* method);
#define BaseServer_1_Awake_m3052512987(__this, method) ((  void (*) (BaseServer_1_t590035467 *, const MethodInfo*))BaseServer_1_Awake_m3052512987_gshared)(__this, method)
// System.Void BaseServer`1<System.Object>::Init(System.String,System.Int32)
extern "C"  void BaseServer_1_Init_m545638547_gshared (BaseServer_1_t590035467 * __this, String_t* ___serverURL0, int32_t ___timeoutSeconds1, const MethodInfo* method);
#define BaseServer_1_Init_m545638547(__this, ___serverURL0, ___timeoutSeconds1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, int32_t, const MethodInfo*))BaseServer_1_Init_m545638547_gshared)(__this, ___serverURL0, ___timeoutSeconds1, method)
// System.Void BaseServer`1<System.Object>::SetServerURL(System.String)
extern "C"  void BaseServer_1_SetServerURL_m3996693198_gshared (BaseServer_1_t590035467 * __this, String_t* ___newURL0, const MethodInfo* method);
#define BaseServer_1_SetServerURL_m3996693198(__this, ___newURL0, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, const MethodInfo*))BaseServer_1_SetServerURL_m3996693198_gshared)(__this, ___newURL0, method)
// System.Void BaseServer`1<System.Object>::ReSetServerURL()
extern "C"  void BaseServer_1_ReSetServerURL_m891984549_gshared (BaseServer_1_t590035467 * __this, const MethodInfo* method);
#define BaseServer_1_ReSetServerURL_m891984549(__this, method) ((  void (*) (BaseServer_1_t590035467 *, const MethodInfo*))BaseServer_1_ReSetServerURL_m891984549_gshared)(__this, method)
// System.Void BaseServer`1<System.Object>::ClearCookies()
extern "C"  void BaseServer_1_ClearCookies_m227966080_gshared (BaseServer_1_t590035467 * __this, const MethodInfo* method);
#define BaseServer_1_ClearCookies_m227966080(__this, method) ((  void (*) (BaseServer_1_t590035467 *, const MethodInfo*))BaseServer_1_ClearCookies_m227966080_gshared)(__this, method)
// System.Void BaseServer`1<System.Object>::SetDefaultConnectionTimeout(System.Int32)
extern "C"  void BaseServer_1_SetDefaultConnectionTimeout_m1299164955_gshared (BaseServer_1_t590035467 * __this, int32_t ___timeoutSeconds0, const MethodInfo* method);
#define BaseServer_1_SetDefaultConnectionTimeout_m1299164955(__this, ___timeoutSeconds0, method) ((  void (*) (BaseServer_1_t590035467 *, int32_t, const MethodInfo*))BaseServer_1_SetDefaultConnectionTimeout_m1299164955_gshared)(__this, ___timeoutSeconds0, method)
// System.Void BaseServer`1<System.Object>::SetConnectionTimeout(System.Int32)
extern "C"  void BaseServer_1_SetConnectionTimeout_m3327895726_gshared (BaseServer_1_t590035467 * __this, int32_t ___timeoutSeconds0, const MethodInfo* method);
#define BaseServer_1_SetConnectionTimeout_m3327895726(__this, ___timeoutSeconds0, method) ((  void (*) (BaseServer_1_t590035467 *, int32_t, const MethodInfo*))BaseServer_1_SetConnectionTimeout_m3327895726_gshared)(__this, ___timeoutSeconds0, method)
// System.Void BaseServer`1<System.Object>::CheckPersistantLogin(System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_CheckPersistantLogin_m2558463858_gshared (BaseServer_1_t590035467 * __this, Action_2_t1865222972 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_CheckPersistantLogin_m2558463858(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_CheckPersistantLogin_m2558463858_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::RetrieveAllActiveAuth(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RetrieveAllActiveAuth_m891934412_gshared (BaseServer_1_t590035467 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_RetrieveAllActiveAuth_m891934412(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RetrieveAllActiveAuth_m891934412_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::RetrieveUserFacebookAuth(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RetrieveUserFacebookAuth_m2071742404_gshared (BaseServer_1_t590035467 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_RetrieveUserFacebookAuth_m2071742404(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RetrieveUserFacebookAuth_m2071742404_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::LoginViaTwitter(System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_LoginViaTwitter_m2679094843_gshared (BaseServer_1_t590035467 * __this, String_t* ___accessToken0, String_t* ___accessTokenSecret1, Action_3_t3681841185 * ___callBack2, const MethodInfo* method);
#define BaseServer_1_LoginViaTwitter_m2679094843(__this, ___accessToken0, ___accessTokenSecret1, ___callBack2, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_LoginViaTwitter_m2679094843_gshared)(__this, ___accessToken0, ___accessTokenSecret1, ___callBack2, method)
// System.Void BaseServer`1<System.Object>::LoginViaGoogle(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_LoginViaGoogle_m3066647405_gshared (BaseServer_1_t590035467 * __this, String_t* ___otCode0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_LoginViaGoogle_m3066647405(__this, ___otCode0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_LoginViaGoogle_m3066647405_gshared)(__this, ___otCode0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::LoginViaFacebook(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_LoginViaFacebook_m2987038918_gshared (BaseServer_1_t590035467 * __this, String_t* ___accessToken0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_LoginViaFacebook_m2987038918(__this, ___accessToken0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_LoginViaFacebook_m2987038918_gshared)(__this, ___accessToken0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::LoginViaUsername(System.String,System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_LoginViaUsername_m3365051993_gshared (BaseServer_1_t590035467 * __this, String_t* ___username0, String_t* ___password1, Action_2_t1865222972 * ___callBack2, const MethodInfo* method);
#define BaseServer_1_LoginViaUsername_m3365051993(__this, ___username0, ___password1, ___callBack2, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, String_t*, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_LoginViaUsername_m3365051993_gshared)(__this, ___username0, ___password1, ___callBack2, method)
// System.Void BaseServer`1<System.Object>::DeleteAuthViaID(System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_DeleteAuthViaID_m1377852468_gshared (BaseServer_1_t590035467 * __this, String_t* ___userDefiner0, Action_2_t1865222972 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_DeleteAuthViaID_m1377852468(__this, ___userDefiner0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_DeleteAuthViaID_m1377852468_gshared)(__this, ___userDefiner0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::DeleteAuthViaUDID(System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_DeleteAuthViaUDID_m3854546131_gshared (BaseServer_1_t590035467 * __this, String_t* ___udid0, Action_2_t1865222972 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_DeleteAuthViaUDID_m3854546131(__this, ___udid0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_DeleteAuthViaUDID_m3854546131_gshared)(__this, ___udid0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::DeleteTwitterAuth(System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_DeleteTwitterAuth_m3130978530_gshared (BaseServer_1_t590035467 * __this, String_t* ___userDefiner0, Action_2_t1865222972 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_DeleteTwitterAuth_m3130978530(__this, ___userDefiner0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_DeleteTwitterAuth_m3130978530_gshared)(__this, ___userDefiner0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::DeleteFacebookAuth(System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_DeleteFacebookAuth_m1662953999_gshared (BaseServer_1_t590035467 * __this, Action_2_t1865222972 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_DeleteFacebookAuth_m1662953999(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_DeleteFacebookAuth_m1662953999_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::Logout(System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_Logout_m2396197790_gshared (BaseServer_1_t590035467 * __this, Action_2_t1865222972 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_Logout_m2396197790(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_Logout_m2396197790_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::ResetPassword(System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_ResetPassword_m3312894212_gshared (BaseServer_1_t590035467 * __this, String_t* ___email0, Action_2_t1865222972 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_ResetPassword_m3312894212(__this, ___email0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_ResetPassword_m3312894212_gshared)(__this, ___email0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::RetrieveUsersFriendsList(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RetrieveUsersFriendsList_m1204918984_gshared (BaseServer_1_t590035467 * __this, String_t* ___userDefiner0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_RetrieveUsersFriendsList_m1204918984(__this, ___userDefiner0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RetrieveUsersFriendsList_m1204918984_gshared)(__this, ___userDefiner0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::RetrieveUsersFriendsListPlusPending(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RetrieveUsersFriendsListPlusPending_m2743533615_gshared (BaseServer_1_t590035467 * __this, String_t* ___userDefiner0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_RetrieveUsersFriendsListPlusPending_m2743533615(__this, ___userDefiner0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RetrieveUsersFriendsListPlusPending_m2743533615_gshared)(__this, ___userDefiner0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::RetrieveUsersPendindFriendRequests(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RetrieveUsersPendindFriendRequests_m3612388967_gshared (BaseServer_1_t590035467 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_RetrieveUsersPendindFriendRequests_m3612388967(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RetrieveUsersPendindFriendRequests_m3612388967_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::RetrieveUsersIncomingPendindFriendRequests(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RetrieveUsersIncomingPendindFriendRequests_m2950427705_gshared (BaseServer_1_t590035467 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_RetrieveUsersIncomingPendindFriendRequests_m2950427705(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RetrieveUsersIncomingPendindFriendRequests_m2950427705_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::RetrieveUsersOutgoingPendindFriendRequests(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RetrieveUsersOutgoingPendindFriendRequests_m3249156329_gshared (BaseServer_1_t590035467 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_RetrieveUsersOutgoingPendindFriendRequests_m3249156329(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RetrieveUsersOutgoingPendindFriendRequests_m3249156329_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::AddNewFriend(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_AddNewFriend_m2178336882_gshared (BaseServer_1_t590035467 * __this, String_t* ___userID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_AddNewFriend_m2178336882(__this, ___userID0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_AddNewFriend_m2178336882_gshared)(__this, ___userID0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::RemoveExistingFriend(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RemoveExistingFriend_m1547708528_gshared (BaseServer_1_t590035467 * __this, String_t* ___userID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_RemoveExistingFriend_m1547708528(__this, ___userID0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RemoveExistingFriend_m1547708528_gshared)(__this, ___userID0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::AcceptFriendRequest(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_AcceptFriendRequest_m1960025982_gshared (BaseServer_1_t590035467 * __this, String_t* ___userID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_AcceptFriendRequest_m1960025982(__this, ___userID0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_AcceptFriendRequest_m1960025982_gshared)(__this, ___userID0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::DeclineFriendRequest(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_DeclineFriendRequest_m3023733210_gshared (BaseServer_1_t590035467 * __this, String_t* ___userID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_DeclineFriendRequest_m3023733210(__this, ___userID0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_DeclineFriendRequest_m3023733210_gshared)(__this, ___userID0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::RevokeFriendRequest(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_RevokeFriendRequest_m3665995492_gshared (BaseServer_1_t590035467 * __this, String_t* ___userID0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_RevokeFriendRequest_m3665995492(__this, ___userID0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_RevokeFriendRequest_m3665995492_gshared)(__this, ___userID0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::ImportContatcs(System.Collections.Generic.List`1<System.String>,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_ImportContatcs_m598203441_gshared (BaseServer_1_t590035467 * __this, List_1_t1398341365 * ___hashesList0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_ImportContatcs_m598203441(__this, ___hashesList0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, List_1_t1398341365 *, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_ImportContatcs_m598203441_gshared)(__this, ___hashesList0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::DeleteAllContacts(System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_DeleteAllContacts_m206297020_gshared (BaseServer_1_t590035467 * __this, Action_3_t3681841185 * ___callBack0, const MethodInfo* method);
#define BaseServer_1_DeleteAllContacts_m206297020(__this, ___callBack0, method) ((  void (*) (BaseServer_1_t590035467 *, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_DeleteAllContacts_m206297020_gshared)(__this, ___callBack0, method)
// System.Void BaseServer`1<System.Object>::DeleteSingleContact(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_DeleteSingleContact_m1200481326_gshared (BaseServer_1_t590035467 * __this, String_t* ___hashToDelete0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_DeleteSingleContact_m1200481326(__this, ___hashToDelete0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_DeleteSingleContact_m1200481326_gshared)(__this, ___hashToDelete0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::SearchExistingUserBase(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_SearchExistingUserBase_m4137465982_gshared (BaseServer_1_t590035467 * __this, String_t* ___searchContent0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_SearchExistingUserBase_m4137465982(__this, ___searchContent0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_SearchExistingUserBase_m4137465982_gshared)(__this, ___searchContent0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::RetrieveAuthenticatedUser(System.String)
extern "C"  void BaseServer_1_RetrieveAuthenticatedUser_m556691508_gshared (BaseServer_1_t590035467 * __this, String_t* ___username0, const MethodInfo* method);
#define BaseServer_1_RetrieveAuthenticatedUser_m556691508(__this, ___username0, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, const MethodInfo*))BaseServer_1_RetrieveAuthenticatedUser_m556691508_gshared)(__this, ___username0, method)
// System.Void BaseServer`1<System.Object>::PostRetrieveAuthenticatedUser(System.String)
extern "C"  void BaseServer_1_PostRetrieveAuthenticatedUser_m440460942_gshared (BaseServer_1_t590035467 * __this, String_t* ___username0, const MethodInfo* method);
#define BaseServer_1_PostRetrieveAuthenticatedUser_m440460942(__this, ___username0, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, const MethodInfo*))BaseServer_1_PostRetrieveAuthenticatedUser_m440460942_gshared)(__this, ___username0, method)
// System.Void BaseServer`1<System.Object>::OnPostRetrieveAuthenticatedUserFinished(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void BaseServer_1_OnPostRetrieveAuthenticatedUserFinished_m285119069_gshared (BaseServer_1_t590035467 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define BaseServer_1_OnPostRetrieveAuthenticatedUserFinished_m285119069(__this, ___request0, ___response1, method) ((  void (*) (BaseServer_1_t590035467 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))BaseServer_1_OnPostRetrieveAuthenticatedUserFinished_m285119069_gshared)(__this, ___request0, ___response1, method)
// System.Void BaseServer`1<System.Object>::GetExistingUser(System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_GetExistingUser_m1533161585_gshared (BaseServer_1_t590035467 * __this, String_t* ___userDefiner0, Action_3_t3681841185 * ___callBack1, const MethodInfo* method);
#define BaseServer_1_GetExistingUser_m1533161585(__this, ___userDefiner0, ___callBack1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_GetExistingUser_m1533161585_gshared)(__this, ___userDefiner0, ___callBack1, method)
// System.Void BaseServer`1<System.Object>::RegisterNewUser(System.String,System.String,System.String,System.String,System.String,System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_RegisterNewUser_m3187373608_gshared (BaseServer_1_t590035467 * __this, String_t* ___email0, String_t* ___username1, String_t* ___password2, String_t* ___passwordConfirm3, String_t* ___firstName4, String_t* ___secondName5, Action_2_t1865222972 * ___callBack6, const MethodInfo* method);
#define BaseServer_1_RegisterNewUser_m3187373608(__this, ___email0, ___username1, ___password2, ___passwordConfirm3, ___firstName4, ___secondName5, ___callBack6, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_RegisterNewUser_m3187373608_gshared)(__this, ___email0, ___username1, ___password2, ___passwordConfirm3, ___firstName4, ___secondName5, ___callBack6, method)
// System.Void BaseServer`1<System.Object>::UpdateExistingUser(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Action`2<System.Boolean,System.String>)
extern "C"  void BaseServer_1_UpdateExistingUser_m3769795227_gshared (BaseServer_1_t590035467 * __this, String_t* ___currentUsername0, String_t* ___email1, String_t* ___username2, String_t* ___password3, String_t* ___passwordConfirm4, String_t* ___firstName5, String_t* ___secondName6, Action_2_t1865222972 * ___callBack7, const MethodInfo* method);
#define BaseServer_1_UpdateExistingUser_m3769795227(__this, ___currentUsername0, ___email1, ___username2, ___password3, ___passwordConfirm4, ___firstName5, ___secondName6, ___callBack7, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, String_t*, Action_2_t1865222972 *, const MethodInfo*))BaseServer_1_UpdateExistingUser_m3769795227_gshared)(__this, ___currentUsername0, ___email1, ___username2, ___password3, ___passwordConfirm4, ___firstName5, ___secondName6, ___callBack7, method)
// System.Void BaseServer`1<System.Object>::SavePushToken(System.String,System.String,System.String,System.String,System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>)
extern "C"  void BaseServer_1_SavePushToken_m686693303_gshared (BaseServer_1_t590035467 * __this, String_t* ___deviceID0, String_t* ___deviceType1, String_t* ___token2, String_t* ___isDev3, Action_3_t3681841185 * ___callBack4, const MethodInfo* method);
#define BaseServer_1_SavePushToken_m686693303(__this, ___deviceID0, ___deviceType1, ___token2, ___isDev3, ___callBack4, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, String_t*, String_t*, String_t*, Action_3_t3681841185 *, const MethodInfo*))BaseServer_1_SavePushToken_m686693303_gshared)(__this, ___deviceID0, ___deviceType1, ___token2, ___isDev3, ___callBack4, method)
// System.Void BaseServer`1<System.Object>::LoadImageFromURL(System.String,User)
extern "C"  void BaseServer_1_LoadImageFromURL_m1277818253_gshared (BaseServer_1_t590035467 * __this, String_t* ___URL0, User_t719925459 * ___user1, const MethodInfo* method);
#define BaseServer_1_LoadImageFromURL_m1277818253(__this, ___URL0, ___user1, method) ((  void (*) (BaseServer_1_t590035467 *, String_t*, User_t719925459 *, const MethodInfo*))BaseServer_1_LoadImageFromURL_m1277818253_gshared)(__this, ___URL0, ___user1, method)
// System.Boolean BaseServer`1<System.Object>::ProcessErrorCode(System.String)
extern "C"  bool BaseServer_1_ProcessErrorCode_m1092000164_gshared (BaseServer_1_t590035467 * __this, String_t* ___errorCode0, const MethodInfo* method);
#define BaseServer_1_ProcessErrorCode_m1092000164(__this, ___errorCode0, method) ((  bool (*) (BaseServer_1_t590035467 *, String_t*, const MethodInfo*))BaseServer_1_ProcessErrorCode_m1092000164_gshared)(__this, ___errorCode0, method)
// System.Void BaseServer`1<System.Object>::ProcessHeader(BestHTTP.HTTPResponse)
extern "C"  void BaseServer_1_ProcessHeader_m1787211967_gshared (BaseServer_1_t590035467 * __this, HTTPResponse_t62748825 * ___response0, const MethodInfo* method);
#define BaseServer_1_ProcessHeader_m1787211967(__this, ___response0, method) ((  void (*) (BaseServer_1_t590035467 *, HTTPResponse_t62748825 *, const MethodInfo*))BaseServer_1_ProcessHeader_m1787211967_gshared)(__this, ___response0, method)
// System.Void BaseServer`1<System.Object>::SetServerTimeOffset(System.DateTime)
extern "C"  void BaseServer_1_SetServerTimeOffset_m1487323239_gshared (BaseServer_1_t590035467 * __this, DateTime_t693205669  ___serverTime0, const MethodInfo* method);
#define BaseServer_1_SetServerTimeOffset_m1487323239(__this, ___serverTime0, method) ((  void (*) (BaseServer_1_t590035467 *, DateTime_t693205669 , const MethodInfo*))BaseServer_1_SetServerTimeOffset_m1487323239_gshared)(__this, ___serverTime0, method)
// System.DateTime BaseServer`1<System.Object>::ApplyServerTimeOffset(System.DateTime)
extern "C"  DateTime_t693205669  BaseServer_1_ApplyServerTimeOffset_m3345860896_gshared (BaseServer_1_t590035467 * __this, DateTime_t693205669  ___dateTime0, const MethodInfo* method);
#define BaseServer_1_ApplyServerTimeOffset_m3345860896(__this, ___dateTime0, method) ((  DateTime_t693205669  (*) (BaseServer_1_t590035467 *, DateTime_t693205669 , const MethodInfo*))BaseServer_1_ApplyServerTimeOffset_m3345860896_gshared)(__this, ___dateTime0, method)
// System.Int32 BaseServer`1<System.Object>::GetServerTimeOffSet(System.Boolean)
extern "C"  int32_t BaseServer_1_GetServerTimeOffSet_m1015512072_gshared (BaseServer_1_t590035467 * __this, bool ___seconds0, const MethodInfo* method);
#define BaseServer_1_GetServerTimeOffSet_m1015512072(__this, ___seconds0, method) ((  int32_t (*) (BaseServer_1_t590035467 *, bool, const MethodInfo*))BaseServer_1_GetServerTimeOffSet_m1015512072_gshared)(__this, ___seconds0, method)
