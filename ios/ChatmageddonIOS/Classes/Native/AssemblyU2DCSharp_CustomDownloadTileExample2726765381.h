﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMaps
struct OnlineMaps_t1893290312;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomDownloadTileExample
struct  CustomDownloadTileExample_t2726765381  : public MonoBehaviour_t1158329972
{
public:
	// OnlineMaps CustomDownloadTileExample::api
	OnlineMaps_t1893290312 * ___api_2;

public:
	inline static int32_t get_offset_of_api_2() { return static_cast<int32_t>(offsetof(CustomDownloadTileExample_t2726765381, ___api_2)); }
	inline OnlineMaps_t1893290312 * get_api_2() const { return ___api_2; }
	inline OnlineMaps_t1893290312 ** get_address_of_api_2() { return &___api_2; }
	inline void set_api_2(OnlineMaps_t1893290312 * value)
	{
		___api_2 = value;
		Il2CppCodeGenWriteBarrier(&___api_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
