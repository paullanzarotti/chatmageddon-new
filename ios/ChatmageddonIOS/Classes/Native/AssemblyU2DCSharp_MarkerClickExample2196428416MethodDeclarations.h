﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerClickExample
struct MarkerClickExample_t2196428416;
// OnlineMapsMarkerBase
struct OnlineMapsMarkerBase_t3900955221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarkerBase3900955221.h"

// System.Void MarkerClickExample::.ctor()
extern "C"  void MarkerClickExample__ctor_m3604426431 (MarkerClickExample_t2196428416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerClickExample::Start()
extern "C"  void MarkerClickExample_Start_m1798829483 (MarkerClickExample_t2196428416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerClickExample::OnMarkerClick(OnlineMapsMarkerBase)
extern "C"  void MarkerClickExample_OnMarkerClick_m1125772905 (MarkerClickExample_t2196428416 * __this, OnlineMapsMarkerBase_t3900955221 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
