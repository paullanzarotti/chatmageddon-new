﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColorCorrectionCurves
struct ColorCorrectionCurves_t1552509009;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void ColorCorrectionCurves::.ctor()
extern "C"  void ColorCorrectionCurves__ctor_m2136290235 (ColorCorrectionCurves_t1552509009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::Start()
extern "C"  void ColorCorrectionCurves_Start_m325796911 (ColorCorrectionCurves_t1552509009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::Awake()
extern "C"  void ColorCorrectionCurves_Awake_m2763133360 (ColorCorrectionCurves_t1552509009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ColorCorrectionCurves::CheckResources()
extern "C"  bool ColorCorrectionCurves_CheckResources_m201875690 (ColorCorrectionCurves_t1552509009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::UpdateParameters()
extern "C"  void ColorCorrectionCurves_UpdateParameters_m220928710 (ColorCorrectionCurves_t1552509009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::UpdateTextures()
extern "C"  void ColorCorrectionCurves_UpdateTextures_m1564017502 (ColorCorrectionCurves_t1552509009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ColorCorrectionCurves_OnRenderImage_m2528311431 (ColorCorrectionCurves_t1552509009 * __this, RenderTexture_t2666733923 * ___source0, RenderTexture_t2666733923 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColorCorrectionCurves::Main()
extern "C"  void ColorCorrectionCurves_Main_m1962987030 (ColorCorrectionCurves_t1552509009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
