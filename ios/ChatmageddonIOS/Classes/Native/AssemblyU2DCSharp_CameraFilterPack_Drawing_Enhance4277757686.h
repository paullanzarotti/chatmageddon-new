﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_Drawing_EnhancedComics
struct  CameraFilterPack_Drawing_EnhancedComics_t4277757686  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_Drawing_EnhancedComics::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_Drawing_EnhancedComics::SCMaterial
	Material_t193706927 * ___SCMaterial_4;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::DotSize
	float ___DotSize_5;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_ColorR
	float ____ColorR_6;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_ColorG
	float ____ColorG_7;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_ColorB
	float ____ColorB_8;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_Blood
	float ____Blood_9;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_SmoothStart
	float ____SmoothStart_10;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_SmoothEnd
	float ____SmoothEnd_11;
	// UnityEngine.Color CameraFilterPack_Drawing_EnhancedComics::ColorRGB
	Color_t2020392075  ___ColorRGB_12;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of_TimeX_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ___TimeX_3)); }
	inline float get_TimeX_3() const { return ___TimeX_3; }
	inline float* get_address_of_TimeX_3() { return &___TimeX_3; }
	inline void set_TimeX_3(float value)
	{
		___TimeX_3 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ___SCMaterial_4)); }
	inline Material_t193706927 * get_SCMaterial_4() const { return ___SCMaterial_4; }
	inline Material_t193706927 ** get_address_of_SCMaterial_4() { return &___SCMaterial_4; }
	inline void set_SCMaterial_4(Material_t193706927 * value)
	{
		___SCMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_4, value);
	}

	inline static int32_t get_offset_of_DotSize_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ___DotSize_5)); }
	inline float get_DotSize_5() const { return ___DotSize_5; }
	inline float* get_address_of_DotSize_5() { return &___DotSize_5; }
	inline void set_DotSize_5(float value)
	{
		___DotSize_5 = value;
	}

	inline static int32_t get_offset_of__ColorR_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ____ColorR_6)); }
	inline float get__ColorR_6() const { return ____ColorR_6; }
	inline float* get_address_of__ColorR_6() { return &____ColorR_6; }
	inline void set__ColorR_6(float value)
	{
		____ColorR_6 = value;
	}

	inline static int32_t get_offset_of__ColorG_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ____ColorG_7)); }
	inline float get__ColorG_7() const { return ____ColorG_7; }
	inline float* get_address_of__ColorG_7() { return &____ColorG_7; }
	inline void set__ColorG_7(float value)
	{
		____ColorG_7 = value;
	}

	inline static int32_t get_offset_of__ColorB_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ____ColorB_8)); }
	inline float get__ColorB_8() const { return ____ColorB_8; }
	inline float* get_address_of__ColorB_8() { return &____ColorB_8; }
	inline void set__ColorB_8(float value)
	{
		____ColorB_8 = value;
	}

	inline static int32_t get_offset_of__Blood_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ____Blood_9)); }
	inline float get__Blood_9() const { return ____Blood_9; }
	inline float* get_address_of__Blood_9() { return &____Blood_9; }
	inline void set__Blood_9(float value)
	{
		____Blood_9 = value;
	}

	inline static int32_t get_offset_of__SmoothStart_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ____SmoothStart_10)); }
	inline float get__SmoothStart_10() const { return ____SmoothStart_10; }
	inline float* get_address_of__SmoothStart_10() { return &____SmoothStart_10; }
	inline void set__SmoothStart_10(float value)
	{
		____SmoothStart_10 = value;
	}

	inline static int32_t get_offset_of__SmoothEnd_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ____SmoothEnd_11)); }
	inline float get__SmoothEnd_11() const { return ____SmoothEnd_11; }
	inline float* get_address_of__SmoothEnd_11() { return &____SmoothEnd_11; }
	inline void set__SmoothEnd_11(float value)
	{
		____SmoothEnd_11 = value;
	}

	inline static int32_t get_offset_of_ColorRGB_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686, ___ColorRGB_12)); }
	inline Color_t2020392075  get_ColorRGB_12() const { return ___ColorRGB_12; }
	inline Color_t2020392075 * get_address_of_ColorRGB_12() { return &___ColorRGB_12; }
	inline void set_ColorRGB_12(Color_t2020392075  value)
	{
		___ColorRGB_12 = value;
	}
};

struct CameraFilterPack_Drawing_EnhancedComics_t4277757686_StaticFields
{
public:
	// System.Single CameraFilterPack_Drawing_EnhancedComics::ChangeDotSize
	float ___ChangeDotSize_13;

public:
	inline static int32_t get_offset_of_ChangeDotSize_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_Drawing_EnhancedComics_t4277757686_StaticFields, ___ChangeDotSize_13)); }
	inline float get_ChangeDotSize_13() const { return ___ChangeDotSize_13; }
	inline float* get_address_of_ChangeDotSize_13() { return &___ChangeDotSize_13; }
	inline void set_ChangeDotSize_13(float value)
	{
		___ChangeDotSize_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
