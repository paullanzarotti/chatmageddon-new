﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.Configuration.ExeConfigurationFileMap
struct ExeConfigurationFileMap_t1419586304;
// System.Configuration.ConfigurationFileMap
struct ConfigurationFileMap_t2625210096;
// System.Configuration.Internal.IInternalConfigConfigurationFactory
struct IInternalConfigConfigurationFactory_t2261973434;
// System.Configuration.Internal.IInternalConfigSystem
struct IInternalConfigSystem_t344849519;
// System.Object
struct Il2CppObject;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Configuration.ConnectionStringSettingsCollection
struct ConnectionStringSettingsCollection_t2042791570;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "System_Configuration_System_Configuration_Configur1204907851.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_ExeConfi1419586304.h"
#include "System_Configuration_System_Configuration_Configur2625210096.h"

// System.Void System.Configuration.ConfigurationManager::.cctor()
extern "C"  void ConfigurationManager__cctor_m145104057 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ConfigurationManager::GetAssemblyInfo(System.Reflection.Assembly)
extern "C"  String_t* ConfigurationManager_GetAssemblyInfo_m1959334153 (Il2CppObject * __this /* static, unused */, Assembly_t4268412390 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.ConfigurationManager::OpenExeConfigurationInternal(System.Configuration.ConfigurationUserLevel,System.Reflection.Assembly,System.String)
extern "C"  Configuration_t3335372970 * ConfigurationManager_OpenExeConfigurationInternal_m2337857263 (Il2CppObject * __this /* static, unused */, int32_t ___userLevel0, Assembly_t4268412390 * ___calling_assembly1, String_t* ___exePath2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.ConfigurationManager::OpenExeConfiguration(System.Configuration.ConfigurationUserLevel)
extern "C"  Configuration_t3335372970 * ConfigurationManager_OpenExeConfiguration_m3390350096 (Il2CppObject * __this /* static, unused */, int32_t ___userLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.ConfigurationManager::OpenExeConfiguration(System.String)
extern "C"  Configuration_t3335372970 * ConfigurationManager_OpenExeConfiguration_m704693324 (Il2CppObject * __this /* static, unused */, String_t* ___exePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.ConfigurationManager::OpenMappedExeConfiguration(System.Configuration.ExeConfigurationFileMap,System.Configuration.ConfigurationUserLevel)
extern "C"  Configuration_t3335372970 * ConfigurationManager_OpenMappedExeConfiguration_m2697890586 (Il2CppObject * __this /* static, unused */, ExeConfigurationFileMap_t1419586304 * ___fileMap0, int32_t ___userLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.ConfigurationManager::OpenMachineConfiguration()
extern "C"  Configuration_t3335372970 * ConfigurationManager_OpenMachineConfiguration_m2616221981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Configuration System.Configuration.ConfigurationManager::OpenMappedMachineConfiguration(System.Configuration.ConfigurationFileMap)
extern "C"  Configuration_t3335372970 * ConfigurationManager_OpenMappedMachineConfiguration_m3147858555 (Il2CppObject * __this /* static, unused */, ConfigurationFileMap_t2625210096 * ___fileMap0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Internal.IInternalConfigConfigurationFactory System.Configuration.ConfigurationManager::get_ConfigurationFactory()
extern "C"  Il2CppObject * ConfigurationManager_get_ConfigurationFactory_m113132422 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Internal.IInternalConfigSystem System.Configuration.ConfigurationManager::get_ConfigurationSystem()
extern "C"  Il2CppObject * ConfigurationManager_get_ConfigurationSystem_m1820628092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ConfigurationManager::GetSection(System.String)
extern "C"  Il2CppObject * ConfigurationManager_GetSection_m1187558274 (Il2CppObject * __this /* static, unused */, String_t* ___sectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.ConfigurationManager::RefreshSection(System.String)
extern "C"  void ConfigurationManager_RefreshSection_m2414402770 (Il2CppObject * __this /* static, unused */, String_t* ___sectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.NameValueCollection System.Configuration.ConfigurationManager::get_AppSettings()
extern "C"  NameValueCollection_t3047564564 * ConfigurationManager_get_AppSettings_m2815980311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConnectionStringSettingsCollection System.Configuration.ConfigurationManager::get_ConnectionStrings()
extern "C"  ConnectionStringSettingsCollection_t2042791570 * ConfigurationManager_get_ConnectionStrings_m1294369269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Internal.IInternalConfigSystem System.Configuration.ConfigurationManager::ChangeConfigurationSystem(System.Configuration.Internal.IInternalConfigSystem)
extern "C"  Il2CppObject * ConfigurationManager_ChangeConfigurationSystem_m1541904010 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___newSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
