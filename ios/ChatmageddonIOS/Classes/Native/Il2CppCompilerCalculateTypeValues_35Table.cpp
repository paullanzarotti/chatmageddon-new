﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppleAppStoreCallbac2011772955.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_OSXStor3020431434.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_StoreKit529087534.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_GoogleP2494001613.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GooglePlayCallbackMo3452313744.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_RawGoog2949402238.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_RawSams3420523086.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Samsung2236131154.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SamsungAppsCallbackM2214262867.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_WP8Billi394118855.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WP8Eventhook2441977752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Win8_1B3938749894.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Win8Eventhook3380410125.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Analyti4061074961.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Analyti4278177091.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillerS4181920983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Biller1615588570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillerF1450757922.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Currenc3009003778.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_DownloadM32803451.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Downloa2765085661.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Downloa2301959070.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Downloa3409902454.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Downloa2573312903.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Downloa3943811883.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Tests_FakeBillingServ881210075.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_HTTPCli1865772921.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_HTTPCli4132869030.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_HTTPClie873001920.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_HelpCent214342444.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_PostPara415229629.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseType1639241305.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchaseEvent743554429.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem_Writ3585946155.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Writabl2700975424.h"
#include "AssemblyU2DCSharpU2Dfirstpass_VirtualCurrency1115497880.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_MiniJSON333594015.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_MiniJson615358805.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_ProductDefin1519653988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Product3313438456.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_RawBill1857346121.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RemoteConfigFetcher2652256879.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RemoteConfigFetcher_2771239707.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_RemoteC2693772795.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TransactionDatabase201476183.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Samsung2745005863.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill1264162701.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_UnibillC469792186.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2627465703.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillError1753859787.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnibillState4272135008.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibiller1392804696.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibiller_U3C_intern3773954316.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_UnityHT3521437524.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_UnityURL313076257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Util3420519310.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_ZipUtil2047198994.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_Authenti1276453517.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_Credenti3762395084.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_Digest59399582.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_DigestSto674131537.h"
#include "AssemblyU2DCSharp_BestHTTP_Authentication_DigestSt3001516118.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheFileIn2858191078.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheFileLoc673754655.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheMaintan558833957.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheServic1690789871.h"
#include "AssemblyU2DCSharp_BestHTTP_Caching_HTTPCacheServic2391743479.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPConnectionRecycledD3354195806.h"
#include "AssemblyU2DCSharp_BestHTTP_ConnectionBase2782190729.h"
#include "AssemblyU2DCSharp_BestHTTP_StreamList3066109018.h"
#include "AssemblyU2DCSharp_BestHTTP_FileConnection3022110980.h"
#include "AssemblyU2DCSharp_BestHTTP_Cookies_Cookie4162804382.h"
#include "AssemblyU2DCSharp_BestHTTP_Cookies_CookieJar756201495.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Crc_CRC322268741539.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Bloc2541392848.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl1224730655.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl1983720200.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl4191606113.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl3381668151.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Defl2274450459.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_GZip1683233742.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_InfT1475751651.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl3437229943.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl2450294045.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Inte3809646427.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infla996093859.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl3102396736.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_Infl2519418742.h"
#include "AssemblyU2DCSharp_BestHTTP_Decompression_Zlib_ZTre1042194920.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (AppleAppStoreCallbackMonoBehaviour_t2011772955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3500[1] = 
{
	AppleAppStoreCallbackMonoBehaviour_t2011772955::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (OSXStoreKitPluginImpl_t3020431434), -1, sizeof(OSXStoreKitPluginImpl_t3020431434_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3502[2] = 
{
	OSXStoreKitPluginImpl_t3020431434_StaticFields::get_offset_of_callback_0(),
	OSXStoreKitPluginImpl_t3020431434_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (StoreKitPluginImpl_t529087534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (GooglePlayBillingService_t2494001613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3504[7] = 
{
	GooglePlayBillingService_t2494001613::get_offset_of_publicKey_0(),
	GooglePlayBillingService_t2494001613::get_offset_of_rawInterface_1(),
	GooglePlayBillingService_t2494001613::get_offset_of_callback_2(),
	GooglePlayBillingService_t2494001613::get_offset_of_remapper_3(),
	GooglePlayBillingService_t2494001613::get_offset_of_db_4(),
	GooglePlayBillingService_t2494001613::get_offset_of_logger_5(),
	GooglePlayBillingService_t2494001613::get_offset_of_unknownAmazonProducts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (GooglePlayCallbackMonoBehaviour_t3452313744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3505[1] = 
{
	GooglePlayCallbackMonoBehaviour_t3452313744::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (RawGooglePlayInterface_t2949402238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (RawSamsungAppsBillingInterface_t3420523086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (SamsungAppsBillingService_t2236131154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3510[6] = 
{
	SamsungAppsBillingService_t2236131154::get_offset_of_callback_0(),
	SamsungAppsBillingService_t2236131154::get_offset_of_remapper_1(),
	SamsungAppsBillingService_t2236131154::get_offset_of_config_2(),
	SamsungAppsBillingService_t2236131154::get_offset_of_rawSamsung_3(),
	SamsungAppsBillingService_t2236131154::get_offset_of_logger_4(),
	SamsungAppsBillingService_t2236131154::get_offset_of_unknownSamsungProducts_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (SamsungAppsCallbackMonoBehaviour_t2214262867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3511[1] = 
{
	SamsungAppsCallbackMonoBehaviour_t2214262867::get_offset_of_samsung_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (WP8BillingService_t394118855), -1, sizeof(WP8BillingService_t394118855_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3512[8] = 
{
	WP8BillingService_t394118855::get_offset_of_wp8_0(),
	WP8BillingService_t394118855::get_offset_of_callback_1(),
	WP8BillingService_t394118855::get_offset_of_db_2(),
	WP8BillingService_t394118855::get_offset_of_tDb_3(),
	WP8BillingService_t394118855::get_offset_of_remapper_4(),
	WP8BillingService_t394118855::get_offset_of_logger_5(),
	WP8BillingService_t394118855::get_offset_of_unknownProducts_6(),
	WP8BillingService_t394118855_StaticFields::get_offset_of_count_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (WP8Eventhook_t2441977752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3513[1] = 
{
	WP8Eventhook_t2441977752::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (Win8_1BillingService_t3938749894), -1, sizeof(Win8_1BillingService_t3938749894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3514[1] = 
{
	Win8_1BillingService_t3938749894_StaticFields::get_offset_of_count_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (Win8Eventhook_t3380410125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3515[1] = 
{
	Win8Eventhook_t3380410125::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (AnalyticsReporter_t4061074961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3516[10] = 
{
	0,
	0,
	0,
	AnalyticsReporter_t4061074961::get_offset_of_config_3(),
	AnalyticsReporter_t4061074961::get_offset_of_client_4(),
	AnalyticsReporter_t4061074961::get_offset_of_util_5(),
	AnalyticsReporter_t4061074961::get_offset_of_userId_6(),
	AnalyticsReporter_t4061074961::get_offset_of_restoreInProgress_7(),
	AnalyticsReporter_t4061074961::get_offset_of_levelName_8(),
	AnalyticsReporter_t4061074961::get_offset_of_levelLoadTime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (EventType_t4278177091)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3517[8] = 
{
	EventType_t4278177091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (BillerState_t4181920983)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3518[5] = 
{
	BillerState_t4181920983::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (Biller_t1615588570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3519[17] = 
{
	Biller_t1615588570::get_offset_of_U3CInventoryDatabaseU3Ek__BackingField_0(),
	Biller_t1615588570::get_offset_of_transactionDatabase_1(),
	Biller_t1615588570::get_offset_of_logger_2(),
	Biller_t1615588570::get_offset_of_help_3(),
	Biller_t1615588570::get_offset_of_remapper_4(),
	Biller_t1615588570::get_offset_of_currencyManager_5(),
	Biller_t1615588570::get_offset_of_U3CbillingSubsystemU3Ek__BackingField_6(),
	Biller_t1615588570::get_offset_of_onBillerReady_7(),
	Biller_t1615588570::get_offset_of_onPurchaseComplete_8(),
	Biller_t1615588570::get_offset_of_onTransactionRestoreBegin_9(),
	Biller_t1615588570::get_offset_of_onTransactionsRestored_10(),
	Biller_t1615588570::get_offset_of_onPurchaseCancelled_11(),
	Biller_t1615588570::get_offset_of_onPurchaseRefunded_12(),
	Biller_t1615588570::get_offset_of_onPurchaseFailed_13(),
	Biller_t1615588570::get_offset_of_onPurchaseDeferred_14(),
	Biller_t1615588570::get_offset_of_U3CStateU3Ek__BackingField_15(),
	Biller_t1615588570::get_offset_of_U3CErrorsU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (BillerFactory_t1450757922), -1, sizeof(BillerFactory_t1450757922_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3520[12] = 
{
	BillerFactory_t1450757922::get_offset_of_loader_0(),
	BillerFactory_t1450757922::get_offset_of_logger_1(),
	BillerFactory_t1450757922::get_offset_of_storage_2(),
	BillerFactory_t1450757922::get_offset_of_platformProvider_3(),
	BillerFactory_t1450757922::get_offset_of_util_4(),
	BillerFactory_t1450757922::get_offset_of_config_5(),
	BillerFactory_t1450757922::get_offset_of__currencyManager_6(),
	BillerFactory_t1450757922::get_offset_of__tDb_7(),
	BillerFactory_t1450757922::get_offset_of__helpCentre_8(),
	BillerFactory_t1450757922::get_offset_of__remapper_9(),
	BillerFactory_t1450757922_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	BillerFactory_t1450757922_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (CurrencyManager_t3009003778), -1, sizeof(CurrencyManager_t3009003778_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3521[4] = 
{
	CurrencyManager_t3009003778::get_offset_of_storage_0(),
	CurrencyManager_t3009003778::get_offset_of_config_1(),
	CurrencyManager_t3009003778::get_offset_of_U3CCurrenciesU3Ek__BackingField_2(),
	CurrencyManager_t3009003778_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (DownloadManager_t32803451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3522[24] = 
{
	DownloadManager_t32803451::get_offset_of_util_0(),
	DownloadManager_t32803451::get_offset_of_storage_1(),
	DownloadManager_t32803451::get_offset_of_fetcher_2(),
	DownloadManager_t32803451::get_offset_of_logger_3(),
	DownloadManager_t32803451::get_offset_of_persistentDataPath_4(),
	DownloadManager_t32803451::get_offset_of_scheduledDownloads_5(),
	DownloadManager_t32803451::get_offset_of_bufferSize_6(),
	0,
	0,
	0,
	DownloadManager_t32803451::get_offset_of_BUFFER_10(),
	DownloadManager_t32803451::get_offset_of_DATA_READY_11(),
	DownloadManager_t32803451::get_offset_of_UNPACK_FINISHED_12(),
	DownloadManager_t32803451::get_offset_of_DATA_FLUSHED_13(),
	DownloadManager_t32803451::get_offset_of_fileStream_14(),
	DownloadManager_t32803451::get_offset_of_bytesReceived_15(),
	DownloadManager_t32803451::get_offset_of_platform_16(),
	DownloadManager_t32803451::get_offset_of_appSecret_17(),
	DownloadManager_t32803451::get_offset_of_appId_18(),
	DownloadManager_t32803451::get_offset_of_onDownloadCompletedEvent_19(),
	DownloadManager_t32803451::get_offset_of_onDownloadFailedEvent_20(),
	DownloadManager_t32803451::get_offset_of_onDownloadProgressedEvent_21(),
	DownloadManager_t32803451::get_offset_of_waiter_22(),
	DownloadManager_t32803451::get_offset_of_rand_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (U3CcheckDownloadsU3Ec__Iterator0_t2765085661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3523[6] = 
{
	U3CcheckDownloadsU3Ec__Iterator0_t2765085661::get_offset_of_U3CtU3E__0_0(),
	U3CcheckDownloadsU3Ec__Iterator0_t2765085661::get_offset_of_U3CscheduledDownloadU3E__1_1(),
	U3CcheckDownloadsU3Ec__Iterator0_t2765085661::get_offset_of_U24this_2(),
	U3CcheckDownloadsU3Ec__Iterator0_t2765085661::get_offset_of_U24current_3(),
	U3CcheckDownloadsU3Ec__Iterator0_t2765085661::get_offset_of_U24disposing_4(),
	U3CcheckDownloadsU3Ec__Iterator0_t2765085661::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (U3CmonitorDownloadsU3Ec__Iterator1_t2301959070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3524[4] = 
{
	U3CmonitorDownloadsU3Ec__Iterator1_t2301959070::get_offset_of_U24this_0(),
	U3CmonitorDownloadsU3Ec__Iterator1_t2301959070::get_offset_of_U24current_1(),
	U3CmonitorDownloadsU3Ec__Iterator1_t2301959070::get_offset_of_U24disposing_2(),
	U3CmonitorDownloadsU3Ec__Iterator1_t2301959070::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (U3CdownloadU3Ec__Iterator2_t3409902454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[23] = 
{
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_bundleId_0(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CdownloadTokenU3E__0_1(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CparametersU3E__1_2(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CresponseU3E__2_3(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CdownloadTokenHashU3E__3_4(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CsuccessU3E__4_5(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CversionU3E__5_6(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CheadersU3E__6_7(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CresponseU3E__7_8(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CcontentRangeU3E__8_9(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CcontentLengthU3E__9_10(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U24locvar0_11(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CrangeStartU3E__A_12(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CrangeEndU3E__B_13(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3ClastProgressU3E__C_14(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CheaderU3E__D_15(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CresponseU3E__E_16(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U3CprogressU3E__F_17(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U24this_18(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U24current_19(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U24disposing_20(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U24PC_21(),
	U3CdownloadU3Ec__Iterator2_t3409902454::get_offset_of_U24locvar1_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (U3CdownloadU3Ec__AnonStorey3_t2573312903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3526[2] = 
{
	U3CdownloadU3Ec__AnonStorey3_t2573312903::get_offset_of_bundleId_0(),
	U3CdownloadU3Ec__AnonStorey3_t2573312903::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3527[3] = 
{
	U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883::get_offset_of_bundleId_0(),
	U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883::get_offset_of_error_1(),
	U3ConDownloadFailedPermanentlyU3Ec__AnonStorey4_t3943811883::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (FakeBillingService_t881210075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3528[7] = 
{
	FakeBillingService_t881210075::get_offset_of_biller_0(),
	FakeBillingService_t881210075::get_offset_of_purchasedItems_1(),
	FakeBillingService_t881210075::get_offset_of_remapper_2(),
	FakeBillingService_t881210075::get_offset_of_reportError_3(),
	FakeBillingService_t881210075::get_offset_of_reportCriticalError_4(),
	FakeBillingService_t881210075::get_offset_of_purchaseCalled_5(),
	FakeBillingService_t881210075::get_offset_of_restoreCalled_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (HTTPClient_t1865772921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3529[2] = 
{
	HTTPClient_t1865772921::get_offset_of_events_0(),
	HTTPClient_t1865772921::get_offset_of_wait_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (PostRequest_t4132869030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3530[2] = 
{
	PostRequest_t4132869030::get_offset_of_url_0(),
	PostRequest_t4132869030::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (U3CpumpU3Ec__Iterator0_t873001920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3531[7] = 
{
	U3CpumpU3Ec__Iterator0_t873001920::get_offset_of_U3CeU3E__0_0(),
	U3CpumpU3Ec__Iterator0_t873001920::get_offset_of_U3CformU3E__1_1(),
	U3CpumpU3Ec__Iterator0_t873001920::get_offset_of_U3CwU3E__2_2(),
	U3CpumpU3Ec__Iterator0_t873001920::get_offset_of_U24this_3(),
	U3CpumpU3Ec__Iterator0_t873001920::get_offset_of_U24current_4(),
	U3CpumpU3Ec__Iterator0_t873001920::get_offset_of_U24disposing_5(),
	U3CpumpU3Ec__Iterator0_t873001920::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (HelpCentre_t214342444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3532[1] = 
{
	HelpCentre_t214342444::get_offset_of_helpMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (PostParameter_t415229629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3535[2] = 
{
	PostParameter_t415229629::get_offset_of_U3CnameU3Ek__BackingField_0(),
	PostParameter_t415229629::get_offset_of_U3CvalueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (PurchaseType_t1639241305)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3538[4] = 
{
	PurchaseType_t1639241305::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (PurchaseEvent_t743554429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3539[2] = 
{
	PurchaseEvent_t743554429::get_offset_of_U3CPurchasedItemU3Ek__BackingField_0(),
	PurchaseEvent_t743554429::get_offset_of_U3CReceiptU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (PurchasableItem_t3963353899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3540[16] = 
{
	PurchasableItem_t3963353899::get_offset_of_U3CAvailableToPurchaseU3Ek__BackingField_0(),
	PurchasableItem_t3963353899::get_offset_of_U3CIdU3Ek__BackingField_1(),
	PurchasableItem_t3963353899::get_offset_of_U3CPurchaseTypeU3Ek__BackingField_2(),
	PurchasableItem_t3963353899::get_offset_of_U3CnameU3Ek__BackingField_3(),
	PurchasableItem_t3963353899::get_offset_of_U3CdescriptionU3Ek__BackingField_4(),
	PurchasableItem_t3963353899::get_offset_of_U3ClocalizedPriceU3Ek__BackingField_5(),
	PurchasableItem_t3963353899::get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_6(),
	PurchasableItem_t3963353899::get_offset_of_U3ClocalizedTitleU3Ek__BackingField_7(),
	PurchasableItem_t3963353899::get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_8(),
	PurchasableItem_t3963353899::get_offset_of_U3CisoCurrencySymbolU3Ek__BackingField_9(),
	PurchasableItem_t3963353899::get_offset_of_U3CpriceInLocalCurrencyU3Ek__BackingField_10(),
	PurchasableItem_t3963353899::get_offset_of_U3CdownloadableContentIdU3Ek__BackingField_11(),
	PurchasableItem_t3963353899::get_offset_of_U3CreceiptU3Ek__BackingField_12(),
	PurchasableItem_t3963353899::get_offset_of_U3CLocalIdsU3Ek__BackingField_13(),
	PurchasableItem_t3963353899::get_offset_of_platformBundles_14(),
	PurchasableItem_t3963353899::get_offset_of_platform_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (Writer_t3585946155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (WritablePurchasable_t2700975424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[1] = 
{
	WritablePurchasable_t2700975424::get_offset_of_U3CitemU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (VirtualCurrency_t1115497880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3543[2] = 
{
	VirtualCurrency_t1115497880::get_offset_of_U3CcurrencyIdU3Ek__BackingField_0(),
	VirtualCurrency_t1115497880::get_offset_of_U3CmappingsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (MiniJSON_t333594015), -1, sizeof(MiniJSON_t333594015_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3544[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MiniJSON_t333594015_StaticFields::get_offset_of_lastErrorIndex_13(),
	MiniJSON_t333594015_StaticFields::get_offset_of_lastDecode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (MiniJsonExtensions_t615358805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (ProductDefinition_t1519653988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3546[2] = 
{
	ProductDefinition_t1519653988::get_offset_of_U3CPlatformSpecificIdU3Ek__BackingField_0(),
	ProductDefinition_t1519653988::get_offset_of_U3CTypeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (ProductIdRemapper_t3313438456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3547[3] = 
{
	ProductIdRemapper_t3313438456::get_offset_of_genericToPlatformSpecificIds_0(),
	ProductIdRemapper_t3313438456::get_offset_of_platformSpecificToGenericIds_1(),
	ProductIdRemapper_t3313438456::get_offset_of_db_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (RawBillingPlatformProvider_t1857346121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[4] = 
{
	RawBillingPlatformProvider_t1857346121::get_offset_of_config_0(),
	RawBillingPlatformProvider_t1857346121::get_offset_of_gameObject_1(),
	RawBillingPlatformProvider_t1857346121::get_offset_of_listener_2(),
	RawBillingPlatformProvider_t1857346121::get_offset_of_client_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (RemoteConfigFetcher_t2652256879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (U3CfetchU3Ec__Iterator0_t2771239707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3550[8] = 
{
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_url_0(),
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_U3CrequestU3E__0_1(),
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_storage_2(),
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_key_3(),
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_U24this_4(),
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_U24current_5(),
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_U24disposing_6(),
	U3CfetchU3Ec__Iterator0_t2771239707::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (RemoteConfigManager_t2693772795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3551[4] = 
{
	0,
	RemoteConfigManager_t2693772795::get_offset_of_storage_1(),
	RemoteConfigManager_t2693772795::get_offset_of_U3CConfigU3Ek__BackingField_2(),
	RemoteConfigManager_t2693772795::get_offset_of_XML_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (TransactionDatabase_t201476183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3552[3] = 
{
	TransactionDatabase_t201476183::get_offset_of_storage_0(),
	TransactionDatabase_t201476183::get_offset_of_logger_1(),
	TransactionDatabase_t201476183::get_offset_of_U3CUserIdU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (BillingPlatform_t552059234)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3553[9] = 
{
	BillingPlatform_t552059234::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (SamsungAppsMode_t2745005863)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3554[4] = 
{
	SamsungAppsMode_t2745005863::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (UnibillConfiguration_t2915611853), -1, sizeof(UnibillConfiguration_t2915611853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3555[21] = 
{
	UnibillConfiguration_t2915611853::get_offset_of_logger_0(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CCurrentPlatformU3Ek__BackingField_1(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CiOSSKUU3Ek__BackingField_2(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CmacAppStoreSKUU3Ek__BackingField_3(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CAndroidBillingPlatformU3Ek__BackingField_4(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CGooglePlayPublicKeyU3Ek__BackingField_5(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CAmazonSandboxEnabledU3Ek__BackingField_6(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CWP8SandboxEnabledU3Ek__BackingField_7(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CUseHostedConfigU3Ek__BackingField_8(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CHostedConfigUrlU3Ek__BackingField_9(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CUnibillAnalyticsAppIdU3Ek__BackingField_10(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CUnibillAnalyticsAppSecretU3Ek__BackingField_11(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CUseWin8_1SandboxU3Ek__BackingField_12(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CSamsungAppsModeU3Ek__BackingField_13(),
	UnibillConfiguration_t2915611853::get_offset_of_U3CSamsungItemGroupIdU3Ek__BackingField_14(),
	UnibillConfiguration_t2915611853::get_offset_of_inventory_15(),
	UnibillConfiguration_t2915611853::get_offset_of_currencies_16(),
	UnibillConfiguration_t2915611853_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
	UnibillConfiguration_t2915611853_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_18(),
	UnibillConfiguration_t2915611853_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_19(),
	UnibillConfiguration_t2915611853_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (U3CUnibillConfigurationU3Ec__AnonStorey0_t1264162701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3556[1] = 
{
	U3CUnibillConfigurationU3Ec__AnonStorey0_t1264162701::get_offset_of_item_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (U3CgetItemByIdU3Ec__AnonStorey1_t469792186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3557[1] = 
{
	U3CgetItemByIdU3Ec__AnonStorey1_t469792186::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (U3CgetCurrencyU3Ec__AnonStorey2_t2627465703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3558[1] = 
{
	U3CgetCurrencyU3Ec__AnonStorey2_t2627465703::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (UnibillError_t1753859787)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3559[33] = 
{
	UnibillError_t1753859787::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (UnibillState_t4272135008)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3560[4] = 
{
	UnibillState_t4272135008::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (Unibiller_t1392804696), -1, sizeof(Unibiller_t1392804696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3561[21] = 
{
	Unibiller_t1392804696_StaticFields::get_offset_of_biller_0(),
	Unibiller_t1392804696_StaticFields::get_offset_of_downloadManager_1(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onBillerReady_2(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onPurchaseCancelled_3(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onPurchaseCompleteEvent_4(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onPurchaseComplete_5(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onPurchaseFailed_6(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onPurchaseDeferred_7(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onPurchaseRefunded_8(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onDownloadCompletedEvent_9(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onDownloadCompletedEventString_10(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onDownloadProgressedEvent_11(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onDownloadFailedEvent_12(),
	Unibiller_t1392804696_StaticFields::get_offset_of_onTransactionsRestored_13(),
	Unibiller_t1392804696_StaticFields::get_offset_of_DownloadManager_14(),
	Unibiller_t1392804696_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
	Unibiller_t1392804696_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_16(),
	Unibiller_t1392804696_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_17(),
	Unibiller_t1392804696_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_18(),
	Unibiller_t1392804696_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_19(),
	Unibiller_t1392804696_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316), -1, sizeof(U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3562[3] = 
{
	U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316::get_offset_of_factory_0(),
	U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316::get_offset_of_biller_1(),
	U3C_internal_hook_eventsU3Ec__AnonStorey0_t3773954316_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (UnityHTTPRequest_t3521437524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3563[1] = 
{
	UnityHTTPRequest_t3521437524::get_offset_of_w_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (UnityURLFetcher_t313076257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3564[1] = 
{
	UnityURLFetcher_t313076257::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (Util_t3420519310), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (ZipUtils_t2047198994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (U3CModuleU3E_t3783534235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (AuthenticationTypes_t1276453517)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3568[4] = 
{
	AuthenticationTypes_t1276453517::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (Credentials_t3762395084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3569[3] = 
{
	Credentials_t3762395084::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	Credentials_t3762395084::get_offset_of_U3CUserNameU3Ek__BackingField_1(),
	Credentials_t3762395084::get_offset_of_U3CPasswordU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (Digest_t59399582), -1, sizeof(Digest_t59399582_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3570[12] = 
{
	Digest_t59399582::get_offset_of_U3CUriU3Ek__BackingField_0(),
	Digest_t59399582::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Digest_t59399582::get_offset_of_U3CRealmU3Ek__BackingField_2(),
	Digest_t59399582::get_offset_of_U3CStaleU3Ek__BackingField_3(),
	Digest_t59399582::get_offset_of_U3CNonceU3Ek__BackingField_4(),
	Digest_t59399582::get_offset_of_U3COpaqueU3Ek__BackingField_5(),
	Digest_t59399582::get_offset_of_U3CAlgorithmU3Ek__BackingField_6(),
	Digest_t59399582::get_offset_of_U3CProtectedUrisU3Ek__BackingField_7(),
	Digest_t59399582::get_offset_of_U3CQualityOfProtectionsU3Ek__BackingField_8(),
	Digest_t59399582::get_offset_of_U3CNonceCountU3Ek__BackingField_9(),
	Digest_t59399582::get_offset_of_U3CHA1SessU3Ek__BackingField_10(),
	Digest_t59399582_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (DigestStore_t674131537), -1, sizeof(DigestStore_t674131537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3571[3] = 
{
	DigestStore_t674131537_StaticFields::get_offset_of_Digests_0(),
	DigestStore_t674131537_StaticFields::get_offset_of_Locker_1(),
	DigestStore_t674131537_StaticFields::get_offset_of_SupportedAlgorithms_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (U3CFindBestU3Ec__AnonStorey0_t3001516118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3572[1] = 
{
	U3CFindBestU3Ec__AnonStorey0_t3001516118::get_offset_of_i_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (HTTPCacheFileInfo_t2858191078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3573[13] = 
{
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CUriU3Ek__BackingField_0(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CLastAccessU3Ek__BackingField_1(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CBodyLengthU3Ek__BackingField_2(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CETagU3Ek__BackingField_3(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CLastModifiedU3Ek__BackingField_4(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CExpiresU3Ek__BackingField_5(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CAgeU3Ek__BackingField_6(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CMaxAgeU3Ek__BackingField_7(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CDateU3Ek__BackingField_8(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CMustRevalidateU3Ek__BackingField_9(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CReceivedU3Ek__BackingField_10(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CConstructedPathU3Ek__BackingField_11(),
	HTTPCacheFileInfo_t2858191078::get_offset_of_U3CMappedNameIDXU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (HTTPCacheFileLock_t673754655), -1, sizeof(HTTPCacheFileLock_t673754655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3574[2] = 
{
	HTTPCacheFileLock_t673754655_StaticFields::get_offset_of_FileLocks_0(),
	HTTPCacheFileLock_t673754655_StaticFields::get_offset_of_SyncRoot_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (HTTPCacheMaintananceParams_t558833957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[2] = 
{
	HTTPCacheMaintananceParams_t558833957::get_offset_of_U3CDeleteOlderU3Ek__BackingField_0(),
	HTTPCacheMaintananceParams_t558833957::get_offset_of_U3CMaxCacheSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (HTTPCacheService_t1690789871), -1, sizeof(HTTPCacheService_t1690789871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3576[13] = 
{
	0,
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_isSupported_1(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_IsSupportCheckDone_2(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_library_3(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_UsedIndexes_4(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CCacheFolderU3Ek__BackingField_5(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CLibraryPathU3Ek__BackingField_6(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_InClearThread_7(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_InMaintainenceThread_8(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_NextNameIDX_9(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
	HTTPCacheService_t1690789871_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (U3CBeginMaintainenceU3Ec__AnonStorey0_t2391743479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[1] = 
{
	U3CBeginMaintainenceU3Ec__AnonStorey0_t2391743479::get_offset_of_maintananceParam_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (HTTPConnectionRecycledDelegate_t3354195806), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (ConnectionBase_t2782190729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[11] = 
{
	ConnectionBase_t2782190729::get_offset_of_U3CServerAddressU3Ek__BackingField_0(),
	ConnectionBase_t2782190729::get_offset_of_U3CStateU3Ek__BackingField_1(),
	ConnectionBase_t2782190729::get_offset_of_U3CCurrentRequestU3Ek__BackingField_2(),
	ConnectionBase_t2782190729::get_offset_of_U3CStartTimeU3Ek__BackingField_3(),
	ConnectionBase_t2782190729::get_offset_of_U3CTimedOutStartU3Ek__BackingField_4(),
	ConnectionBase_t2782190729::get_offset_of_U3CProxyU3Ek__BackingField_5(),
	ConnectionBase_t2782190729::get_offset_of_U3CLastProcessedUriU3Ek__BackingField_6(),
	ConnectionBase_t2782190729::get_offset_of_LastProcessTime_7(),
	ConnectionBase_t2782190729::get_offset_of_OnConnectionRecycled_8(),
	ConnectionBase_t2782190729::get_offset_of_IsThreaded_9(),
	ConnectionBase_t2782190729::get_offset_of_U3CIsDisposedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (StreamList_t3066109018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3580[2] = 
{
	StreamList_t3066109018::get_offset_of_Streams_2(),
	StreamList_t3066109018::get_offset_of_CurrentIdx_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (FileConnection_t3022110980), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (Cookie_t4162804382), -1, sizeof(Cookie_t4162804382_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3582[13] = 
{
	0,
	Cookie_t4162804382::get_offset_of_U3CNameU3Ek__BackingField_1(),
	Cookie_t4162804382::get_offset_of_U3CValueU3Ek__BackingField_2(),
	Cookie_t4162804382::get_offset_of_U3CDateU3Ek__BackingField_3(),
	Cookie_t4162804382::get_offset_of_U3CLastAccessU3Ek__BackingField_4(),
	Cookie_t4162804382::get_offset_of_U3CExpiresU3Ek__BackingField_5(),
	Cookie_t4162804382::get_offset_of_U3CMaxAgeU3Ek__BackingField_6(),
	Cookie_t4162804382::get_offset_of_U3CIsSessionU3Ek__BackingField_7(),
	Cookie_t4162804382::get_offset_of_U3CDomainU3Ek__BackingField_8(),
	Cookie_t4162804382::get_offset_of_U3CPathU3Ek__BackingField_9(),
	Cookie_t4162804382::get_offset_of_U3CIsSecureU3Ek__BackingField_10(),
	Cookie_t4162804382::get_offset_of_U3CIsHttpOnlyU3Ek__BackingField_11(),
	Cookie_t4162804382_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (CookieJar_t756201495), -1, sizeof(CookieJar_t756201495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3583[8] = 
{
	0,
	CookieJar_t756201495_StaticFields::get_offset_of_Cookies_1(),
	CookieJar_t756201495_StaticFields::get_offset_of_U3CCookieFolderU3Ek__BackingField_2(),
	CookieJar_t756201495_StaticFields::get_offset_of_U3CLibraryPathU3Ek__BackingField_3(),
	CookieJar_t756201495_StaticFields::get_offset_of_Locker_4(),
	CookieJar_t756201495_StaticFields::get_offset_of__isSavingSupported_5(),
	CookieJar_t756201495_StaticFields::get_offset_of_IsSupportCheckDone_6(),
	CookieJar_t756201495_StaticFields::get_offset_of_Loaded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (CRC32_t2268741539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3584[6] = 
{
	CRC32_t2268741539::get_offset_of_dwPolynomial_0(),
	CRC32_t2268741539::get_offset_of__TotalBytesRead_1(),
	CRC32_t2268741539::get_offset_of_reverseBits_2(),
	CRC32_t2268741539::get_offset_of_crc32Table_3(),
	0,
	CRC32_t2268741539::get_offset_of__register_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (BlockState_t2541392848)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3585[5] = 
{
	BlockState_t2541392848::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (DeflateFlavor_t1224730655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3586[4] = 
{
	DeflateFlavor_t1224730655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (DeflateManager_t1983720200), -1, sizeof(DeflateManager_t1983720200_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3587[74] = 
{
	DeflateManager_t1983720200_StaticFields::get_offset_of_MEM_LEVEL_MAX_0(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MEM_LEVEL_DEFAULT_1(),
	DeflateManager_t1983720200::get_offset_of_DeflateFunction_2(),
	DeflateManager_t1983720200_StaticFields::get_offset_of__ErrorMessage_3(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_PRESET_DICT_4(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_INIT_STATE_5(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_BUSY_STATE_6(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_FINISH_STATE_7(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_DEFLATED_8(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_STORED_BLOCK_9(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_STATIC_TREES_10(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_DYN_TREES_11(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_BINARY_12(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_ASCII_13(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Z_UNKNOWN_14(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_Buf_size_15(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MIN_MATCH_16(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MAX_MATCH_17(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_MIN_LOOKAHEAD_18(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_HEAP_SIZE_19(),
	DeflateManager_t1983720200_StaticFields::get_offset_of_END_BLOCK_20(),
	DeflateManager_t1983720200::get_offset_of__codec_21(),
	DeflateManager_t1983720200::get_offset_of_status_22(),
	DeflateManager_t1983720200::get_offset_of_pending_23(),
	DeflateManager_t1983720200::get_offset_of_nextPending_24(),
	DeflateManager_t1983720200::get_offset_of_pendingCount_25(),
	DeflateManager_t1983720200::get_offset_of_data_type_26(),
	DeflateManager_t1983720200::get_offset_of_last_flush_27(),
	DeflateManager_t1983720200::get_offset_of_w_size_28(),
	DeflateManager_t1983720200::get_offset_of_w_bits_29(),
	DeflateManager_t1983720200::get_offset_of_w_mask_30(),
	DeflateManager_t1983720200::get_offset_of_window_31(),
	DeflateManager_t1983720200::get_offset_of_window_size_32(),
	DeflateManager_t1983720200::get_offset_of_prev_33(),
	DeflateManager_t1983720200::get_offset_of_head_34(),
	DeflateManager_t1983720200::get_offset_of_ins_h_35(),
	DeflateManager_t1983720200::get_offset_of_hash_size_36(),
	DeflateManager_t1983720200::get_offset_of_hash_bits_37(),
	DeflateManager_t1983720200::get_offset_of_hash_mask_38(),
	DeflateManager_t1983720200::get_offset_of_hash_shift_39(),
	DeflateManager_t1983720200::get_offset_of_block_start_40(),
	DeflateManager_t1983720200::get_offset_of_config_41(),
	DeflateManager_t1983720200::get_offset_of_match_length_42(),
	DeflateManager_t1983720200::get_offset_of_prev_match_43(),
	DeflateManager_t1983720200::get_offset_of_match_available_44(),
	DeflateManager_t1983720200::get_offset_of_strstart_45(),
	DeflateManager_t1983720200::get_offset_of_match_start_46(),
	DeflateManager_t1983720200::get_offset_of_lookahead_47(),
	DeflateManager_t1983720200::get_offset_of_prev_length_48(),
	DeflateManager_t1983720200::get_offset_of_compressionLevel_49(),
	DeflateManager_t1983720200::get_offset_of_compressionStrategy_50(),
	DeflateManager_t1983720200::get_offset_of_dyn_ltree_51(),
	DeflateManager_t1983720200::get_offset_of_dyn_dtree_52(),
	DeflateManager_t1983720200::get_offset_of_bl_tree_53(),
	DeflateManager_t1983720200::get_offset_of_treeLiterals_54(),
	DeflateManager_t1983720200::get_offset_of_treeDistances_55(),
	DeflateManager_t1983720200::get_offset_of_treeBitLengths_56(),
	DeflateManager_t1983720200::get_offset_of_bl_count_57(),
	DeflateManager_t1983720200::get_offset_of_heap_58(),
	DeflateManager_t1983720200::get_offset_of_heap_len_59(),
	DeflateManager_t1983720200::get_offset_of_heap_max_60(),
	DeflateManager_t1983720200::get_offset_of_depth_61(),
	DeflateManager_t1983720200::get_offset_of__lengthOffset_62(),
	DeflateManager_t1983720200::get_offset_of_lit_bufsize_63(),
	DeflateManager_t1983720200::get_offset_of_last_lit_64(),
	DeflateManager_t1983720200::get_offset_of__distanceOffset_65(),
	DeflateManager_t1983720200::get_offset_of_opt_len_66(),
	DeflateManager_t1983720200::get_offset_of_static_len_67(),
	DeflateManager_t1983720200::get_offset_of_matches_68(),
	DeflateManager_t1983720200::get_offset_of_last_eob_len_69(),
	DeflateManager_t1983720200::get_offset_of_bi_buf_70(),
	DeflateManager_t1983720200::get_offset_of_bi_valid_71(),
	DeflateManager_t1983720200::get_offset_of_Rfc1950BytesEmitted_72(),
	DeflateManager_t1983720200::get_offset_of__WantRfc1950HeaderBytes_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (CompressFunc_t4191606113), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (Config_t3381668151), -1, sizeof(Config_t3381668151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3589[6] = 
{
	Config_t3381668151::get_offset_of_GoodLength_0(),
	Config_t3381668151::get_offset_of_MaxLazy_1(),
	Config_t3381668151::get_offset_of_NiceLength_2(),
	Config_t3381668151::get_offset_of_MaxChainLength_3(),
	Config_t3381668151::get_offset_of_Flavor_4(),
	Config_t3381668151_StaticFields::get_offset_of_Table_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (DeflateStream_t2274450459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3590[3] = 
{
	DeflateStream_t2274450459::get_offset_of__baseStream_2(),
	DeflateStream_t2274450459::get_offset_of__innerStream_3(),
	DeflateStream_t2274450459::get_offset_of__disposed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (GZipStream_t1683233742), -1, sizeof(GZipStream_t1683233742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3591[10] = 
{
	GZipStream_t1683233742::get_offset_of_LastModified_2(),
	GZipStream_t1683233742::get_offset_of__headerByteCount_3(),
	GZipStream_t1683233742::get_offset_of__baseStream_4(),
	GZipStream_t1683233742::get_offset_of__disposed_5(),
	GZipStream_t1683233742::get_offset_of__firstReadDone_6(),
	GZipStream_t1683233742::get_offset_of__FileName_7(),
	GZipStream_t1683233742::get_offset_of__Comment_8(),
	GZipStream_t1683233742::get_offset_of__Crc32_9(),
	GZipStream_t1683233742_StaticFields::get_offset_of__unixEpoch_10(),
	GZipStream_t1683233742_StaticFields::get_offset_of_iso8859dash1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (InfTree_t1475751651), -1, sizeof(InfTree_t1475751651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3592[25] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InfTree_t1475751651_StaticFields::get_offset_of_fixed_tl_12(),
	InfTree_t1475751651_StaticFields::get_offset_of_fixed_td_13(),
	InfTree_t1475751651_StaticFields::get_offset_of_cplens_14(),
	InfTree_t1475751651_StaticFields::get_offset_of_cplext_15(),
	InfTree_t1475751651_StaticFields::get_offset_of_cpdist_16(),
	InfTree_t1475751651_StaticFields::get_offset_of_cpdext_17(),
	0,
	InfTree_t1475751651::get_offset_of_hn_19(),
	InfTree_t1475751651::get_offset_of_v_20(),
	InfTree_t1475751651::get_offset_of_c_21(),
	InfTree_t1475751651::get_offset_of_r_22(),
	InfTree_t1475751651::get_offset_of_u_23(),
	InfTree_t1475751651::get_offset_of_x_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (InflateBlocks_t3437229943), -1, sizeof(InflateBlocks_t3437229943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3593[22] = 
{
	0,
	InflateBlocks_t3437229943_StaticFields::get_offset_of_border_1(),
	InflateBlocks_t3437229943::get_offset_of_mode_2(),
	InflateBlocks_t3437229943::get_offset_of_left_3(),
	InflateBlocks_t3437229943::get_offset_of_table_4(),
	InflateBlocks_t3437229943::get_offset_of_index_5(),
	InflateBlocks_t3437229943::get_offset_of_blens_6(),
	InflateBlocks_t3437229943::get_offset_of_bb_7(),
	InflateBlocks_t3437229943::get_offset_of_tb_8(),
	InflateBlocks_t3437229943::get_offset_of_codes_9(),
	InflateBlocks_t3437229943::get_offset_of_last_10(),
	InflateBlocks_t3437229943::get_offset_of__codec_11(),
	InflateBlocks_t3437229943::get_offset_of_bitk_12(),
	InflateBlocks_t3437229943::get_offset_of_bitb_13(),
	InflateBlocks_t3437229943::get_offset_of_hufts_14(),
	InflateBlocks_t3437229943::get_offset_of_window_15(),
	InflateBlocks_t3437229943::get_offset_of_end_16(),
	InflateBlocks_t3437229943::get_offset_of_readAt_17(),
	InflateBlocks_t3437229943::get_offset_of_writeAt_18(),
	InflateBlocks_t3437229943::get_offset_of_checkfn_19(),
	InflateBlocks_t3437229943::get_offset_of_check_20(),
	InflateBlocks_t3437229943::get_offset_of_inftree_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (InflateBlockMode_t2450294045)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3594[11] = 
{
	InflateBlockMode_t2450294045::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (InternalInflateConstants_t3809646427), -1, sizeof(InternalInflateConstants_t3809646427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3595[1] = 
{
	InternalInflateConstants_t3809646427_StaticFields::get_offset_of_InflateMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (InflateCodes_t996093859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3596[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	InflateCodes_t996093859::get_offset_of_mode_10(),
	InflateCodes_t996093859::get_offset_of_len_11(),
	InflateCodes_t996093859::get_offset_of_tree_12(),
	InflateCodes_t996093859::get_offset_of_tree_index_13(),
	InflateCodes_t996093859::get_offset_of_need_14(),
	InflateCodes_t996093859::get_offset_of_lit_15(),
	InflateCodes_t996093859::get_offset_of_bitsToGet_16(),
	InflateCodes_t996093859::get_offset_of_dist_17(),
	InflateCodes_t996093859::get_offset_of_lbits_18(),
	InflateCodes_t996093859::get_offset_of_dbits_19(),
	InflateCodes_t996093859::get_offset_of_ltree_20(),
	InflateCodes_t996093859::get_offset_of_ltree_index_21(),
	InflateCodes_t996093859::get_offset_of_dtree_22(),
	InflateCodes_t996093859::get_offset_of_dtree_index_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (InflateManager_t3102396736), -1, sizeof(InflateManager_t3102396736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3597[12] = 
{
	0,
	0,
	InflateManager_t3102396736::get_offset_of_mode_2(),
	InflateManager_t3102396736::get_offset_of__codec_3(),
	InflateManager_t3102396736::get_offset_of_method_4(),
	InflateManager_t3102396736::get_offset_of_computedCheck_5(),
	InflateManager_t3102396736::get_offset_of_expectedCheck_6(),
	InflateManager_t3102396736::get_offset_of_marker_7(),
	InflateManager_t3102396736::get_offset_of__handleRfc1950HeaderBytes_8(),
	InflateManager_t3102396736::get_offset_of_wbits_9(),
	InflateManager_t3102396736::get_offset_of_blocks_10(),
	InflateManager_t3102396736_StaticFields::get_offset_of_mark_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (InflateManagerMode_t2519418742)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3598[15] = 
{
	InflateManagerMode_t2519418742::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (ZTree_t1042194920), -1, sizeof(ZTree_t1042194920_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3599[13] = 
{
	ZTree_t1042194920_StaticFields::get_offset_of_HEAP_SIZE_0(),
	ZTree_t1042194920_StaticFields::get_offset_of_ExtraLengthBits_1(),
	ZTree_t1042194920_StaticFields::get_offset_of_ExtraDistanceBits_2(),
	ZTree_t1042194920_StaticFields::get_offset_of_extra_blbits_3(),
	ZTree_t1042194920_StaticFields::get_offset_of_bl_order_4(),
	0,
	ZTree_t1042194920_StaticFields::get_offset_of__dist_code_6(),
	ZTree_t1042194920_StaticFields::get_offset_of_LengthCode_7(),
	ZTree_t1042194920_StaticFields::get_offset_of_LengthBase_8(),
	ZTree_t1042194920_StaticFields::get_offset_of_DistanceBase_9(),
	ZTree_t1042194920::get_offset_of_dyn_tree_10(),
	ZTree_t1042194920::get_offset_of_max_code_11(),
	ZTree_t1042194920::get_offset_of_staticTree_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
