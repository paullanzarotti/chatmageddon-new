﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectAudienceSwitch
struct SelectAudienceSwitch_t3539699246;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_Audience3183951146.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectAudienceSwitchButton
struct  SelectAudienceSwitchButton_t3845187086  : public SFXButton_t792651341
{
public:
	// SelectAudienceSwitch SelectAudienceSwitchButton::enumSwitch
	SelectAudienceSwitch_t3539699246 * ___enumSwitch_5;
	// Audience SelectAudienceSwitchButton::switchSelection
	int32_t ___switchSelection_6;

public:
	inline static int32_t get_offset_of_enumSwitch_5() { return static_cast<int32_t>(offsetof(SelectAudienceSwitchButton_t3845187086, ___enumSwitch_5)); }
	inline SelectAudienceSwitch_t3539699246 * get_enumSwitch_5() const { return ___enumSwitch_5; }
	inline SelectAudienceSwitch_t3539699246 ** get_address_of_enumSwitch_5() { return &___enumSwitch_5; }
	inline void set_enumSwitch_5(SelectAudienceSwitch_t3539699246 * value)
	{
		___enumSwitch_5 = value;
		Il2CppCodeGenWriteBarrier(&___enumSwitch_5, value);
	}

	inline static int32_t get_offset_of_switchSelection_6() { return static_cast<int32_t>(offsetof(SelectAudienceSwitchButton_t3845187086, ___switchSelection_6)); }
	inline int32_t get_switchSelection_6() const { return ___switchSelection_6; }
	inline int32_t* get_address_of_switchSelection_6() { return &___switchSelection_6; }
	inline void set_switchSelection_6(int32_t value)
	{
		___switchSelection_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
