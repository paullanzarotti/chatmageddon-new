﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackSearchNavigateForwardsButton
struct AttackSearchNavigateForwardsButton_t3074088129;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackSearchNavigateForwardsButton::.ctor()
extern "C"  void AttackSearchNavigateForwardsButton__ctor_m2965565116 (AttackSearchNavigateForwardsButton_t3074088129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackSearchNavigateForwardsButton::OnButtonClick()
extern "C"  void AttackSearchNavigateForwardsButton_OnButtonClick_m1417860053 (AttackSearchNavigateForwardsButton_t3074088129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
