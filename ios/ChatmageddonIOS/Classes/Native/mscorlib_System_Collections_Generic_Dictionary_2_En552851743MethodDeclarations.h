﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2028690842(__this, ___dictionary0, method) ((  void (*) (Enumerator_t552851743 *, Dictionary_2_t3527794337 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4233861355(__this, method) ((  Il2CppObject * (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3132626931(__this, method) ((  void (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3103142298(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1794486929(__this, method) ((  Il2CppObject * (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1648701497(__this, method) ((  Il2CppObject * (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::MoveNext()
#define Enumerator_MoveNext_m2739960759(__this, method) ((  bool (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::get_Current()
#define Enumerator_get_Current_m1007274167(__this, method) ((  KeyValuePair_2_t1285139559  (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2924291514(__this, method) ((  String_t* (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3066796434(__this, method) ((  XNamespace_t1613015075 * (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::Reset()
#define Enumerator_Reset_m1086782204(__this, method) ((  void (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::VerifyState()
#define Enumerator_VerifyState_m3076268369(__this, method) ((  void (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3588646083(__this, method) ((  void (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Xml.Linq.XNamespace>::Dispose()
#define Enumerator_Dispose_m696673342(__this, method) ((  void (*) (Enumerator_t552851743 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
