﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollWheelEvents
struct ScrollWheelEvents_t2022027795;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollWheelEvents::.ctor()
extern "C"  void ScrollWheelEvents__ctor_m1827656812 (ScrollWheelEvents_t2022027795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollWheelEvents::Awake()
extern "C"  void ScrollWheelEvents_Awake_m130972563 (ScrollWheelEvents_t2022027795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollWheelEvents::CheckInstance()
extern "C"  void ScrollWheelEvents_CheckInstance_m1164483521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollWheelEvents::Update()
extern "C"  void ScrollWheelEvents_Update_m2077909279 (ScrollWheelEvents_t2022027795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
