﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LaunchRoutine
struct LaunchRoutine_t3791963153;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseLaunchSequenceButton
struct  CloseLaunchSequenceButton_t1852628538  : public SFXButton_t792651341
{
public:
	// LaunchRoutine CloseLaunchSequenceButton::routine
	LaunchRoutine_t3791963153 * ___routine_5;

public:
	inline static int32_t get_offset_of_routine_5() { return static_cast<int32_t>(offsetof(CloseLaunchSequenceButton_t1852628538, ___routine_5)); }
	inline LaunchRoutine_t3791963153 * get_routine_5() const { return ___routine_5; }
	inline LaunchRoutine_t3791963153 ** get_address_of_routine_5() { return &___routine_5; }
	inline void set_routine_5(LaunchRoutine_t3791963153 * value)
	{
		___routine_5 = value;
		Il2CppCodeGenWriteBarrier(&___routine_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
