﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FAQ
struct FAQ_t2837997648;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FAQ::.ctor(System.String,System.String)
extern "C"  void FAQ__ctor_m1573075101 (FAQ_t2837997648 * __this, String_t* ___newQuestion0, String_t* ___newAnwser1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
