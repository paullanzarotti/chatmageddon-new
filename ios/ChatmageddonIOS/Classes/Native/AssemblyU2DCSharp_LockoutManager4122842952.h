﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3873508672.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LockoutManager
struct  LockoutManager_t4122842952  : public MonoSingleton_1_t3873508672
{
public:
	// UnityEngine.GameObject LockoutManager::noPointsUI
	GameObject_t1756533147 * ___noPointsUI_3;
	// UnityEngine.GameObject LockoutManager::stealthModeUI
	GameObject_t1756533147 * ___stealthModeUI_4;
	// UnityEngine.GameObject LockoutManager::noNetworkUI
	GameObject_t1756533147 * ___noNetworkUI_5;

public:
	inline static int32_t get_offset_of_noPointsUI_3() { return static_cast<int32_t>(offsetof(LockoutManager_t4122842952, ___noPointsUI_3)); }
	inline GameObject_t1756533147 * get_noPointsUI_3() const { return ___noPointsUI_3; }
	inline GameObject_t1756533147 ** get_address_of_noPointsUI_3() { return &___noPointsUI_3; }
	inline void set_noPointsUI_3(GameObject_t1756533147 * value)
	{
		___noPointsUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___noPointsUI_3, value);
	}

	inline static int32_t get_offset_of_stealthModeUI_4() { return static_cast<int32_t>(offsetof(LockoutManager_t4122842952, ___stealthModeUI_4)); }
	inline GameObject_t1756533147 * get_stealthModeUI_4() const { return ___stealthModeUI_4; }
	inline GameObject_t1756533147 ** get_address_of_stealthModeUI_4() { return &___stealthModeUI_4; }
	inline void set_stealthModeUI_4(GameObject_t1756533147 * value)
	{
		___stealthModeUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___stealthModeUI_4, value);
	}

	inline static int32_t get_offset_of_noNetworkUI_5() { return static_cast<int32_t>(offsetof(LockoutManager_t4122842952, ___noNetworkUI_5)); }
	inline GameObject_t1756533147 * get_noNetworkUI_5() const { return ___noNetworkUI_5; }
	inline GameObject_t1756533147 ** get_address_of_noNetworkUI_5() { return &___noNetworkUI_5; }
	inline void set_noNetworkUI_5(GameObject_t1756533147 * value)
	{
		___noNetworkUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___noNetworkUI_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
