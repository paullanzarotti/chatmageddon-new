﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MineMarker
struct MineMarker_t2486518043;
// OnlineMapsMarker3D
struct OnlineMapsMarker3D_t576815539;
// Mine
struct Mine_t2729441277;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3D576815539.h"
#include "AssemblyU2DCSharp_Mine2729441277.h"

// System.Void MineMarker::.ctor()
extern "C"  void MineMarker__ctor_m3750064496 (MineMarker_t2486518043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMarker::InitMineMarker(OnlineMapsMarker3D,Mine)
extern "C"  void MineMarker_InitMineMarker_m375117335 (MineMarker_t2486518043 * __this, OnlineMapsMarker3D_t576815539 * ___marker0, Mine_t2729441277 * ___initMine1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMarker::UpdateMarker(System.Int32)
extern "C"  void MineMarker_UpdateMarker_m2868826832 (MineMarker_t2486518043 * __this, int32_t ___zoomLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMarker::OnAlphaTweenComplete()
extern "C"  void MineMarker_OnAlphaTweenComplete_m2662424119 (MineMarker_t2486518043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMarker::TriggerEntered()
extern "C"  void MineMarker_TriggerEntered_m1833426763 (MineMarker_t2486518043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MineMarker::TriggerExited()
extern "C"  void MineMarker_TriggerExited_m2911907319 (MineMarker_t2486518043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
