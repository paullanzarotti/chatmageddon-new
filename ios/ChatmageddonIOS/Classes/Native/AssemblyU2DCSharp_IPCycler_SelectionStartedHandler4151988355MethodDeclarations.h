﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPCycler/SelectionStartedHandler
struct SelectionStartedHandler_t4151988355;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void IPCycler/SelectionStartedHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SelectionStartedHandler__ctor_m2714758806 (SelectionStartedHandler_t4151988355 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler/SelectionStartedHandler::Invoke()
extern "C"  void SelectionStartedHandler_Invoke_m2986396524 (SelectionStartedHandler_t4151988355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult IPCycler/SelectionStartedHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SelectionStartedHandler_BeginInvoke_m3816985421 (SelectionStartedHandler_t4151988355 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPCycler/SelectionStartedHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SelectionStartedHandler_EndInvoke_m1928734060 (SelectionStartedHandler_t4151988355 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
