﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetUniversalFeed>c__AnonStorey2D
struct U3CGetUniversalFeedU3Ec__AnonStorey2D_t2160789767;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetUniversalFeed>c__AnonStorey2D::.ctor()
extern "C"  void U3CGetUniversalFeedU3Ec__AnonStorey2D__ctor_m730179424 (U3CGetUniversalFeedU3Ec__AnonStorey2D_t2160789767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetUniversalFeed>c__AnonStorey2D::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetUniversalFeedU3Ec__AnonStorey2D_U3CU3Em__0_m3225730203 (U3CGetUniversalFeedU3Ec__AnonStorey2D_t2160789767 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
