﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatContactsListItemUIScaler
struct  ChatContactsListItemUIScaler_t2894863298  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite ChatContactsListItemUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider ChatContactsListItemUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UnityEngine.Transform ChatContactsListItemUIScaler::firstnameLabel
	Transform_t3275118058 * ___firstnameLabel_16;
	// UnityEngine.Transform ChatContactsListItemUIScaler::usernameLabel
	Transform_t3275118058 * ___usernameLabel_17;
	// UISprite ChatContactsListItemUIScaler::seperator
	UISprite_t603616735 * ___seperator_18;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(ChatContactsListItemUIScaler_t2894863298, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(ChatContactsListItemUIScaler_t2894863298, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_firstnameLabel_16() { return static_cast<int32_t>(offsetof(ChatContactsListItemUIScaler_t2894863298, ___firstnameLabel_16)); }
	inline Transform_t3275118058 * get_firstnameLabel_16() const { return ___firstnameLabel_16; }
	inline Transform_t3275118058 ** get_address_of_firstnameLabel_16() { return &___firstnameLabel_16; }
	inline void set_firstnameLabel_16(Transform_t3275118058 * value)
	{
		___firstnameLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___firstnameLabel_16, value);
	}

	inline static int32_t get_offset_of_usernameLabel_17() { return static_cast<int32_t>(offsetof(ChatContactsListItemUIScaler_t2894863298, ___usernameLabel_17)); }
	inline Transform_t3275118058 * get_usernameLabel_17() const { return ___usernameLabel_17; }
	inline Transform_t3275118058 ** get_address_of_usernameLabel_17() { return &___usernameLabel_17; }
	inline void set_usernameLabel_17(Transform_t3275118058 * value)
	{
		___usernameLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___usernameLabel_17, value);
	}

	inline static int32_t get_offset_of_seperator_18() { return static_cast<int32_t>(offsetof(ChatContactsListItemUIScaler_t2894863298, ___seperator_18)); }
	inline UISprite_t603616735 * get_seperator_18() const { return ___seperator_18; }
	inline UISprite_t603616735 ** get_address_of_seperator_18() { return &___seperator_18; }
	inline void set_seperator_18(UISprite_t603616735 * value)
	{
		___seperator_18 = value;
		Il2CppCodeGenWriteBarrier(&___seperator_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
