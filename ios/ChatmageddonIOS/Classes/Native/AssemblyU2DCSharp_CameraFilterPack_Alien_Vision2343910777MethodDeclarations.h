﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Alien_Vision
struct CameraFilterPack_Alien_Vision_t2343910777;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Alien_Vision::.ctor()
extern "C"  void CameraFilterPack_Alien_Vision__ctor_m3866334364 (CameraFilterPack_Alien_Vision_t2343910777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Alien_Vision::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Alien_Vision_get_material_m4083032285 (CameraFilterPack_Alien_Vision_t2343910777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::Start()
extern "C"  void CameraFilterPack_Alien_Vision_Start_m523788308 (CameraFilterPack_Alien_Vision_t2343910777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Alien_Vision_OnRenderImage_m1174075820 (CameraFilterPack_Alien_Vision_t2343910777 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::OnValidate()
extern "C"  void CameraFilterPack_Alien_Vision_OnValidate_m621406415 (CameraFilterPack_Alien_Vision_t2343910777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::Update()
extern "C"  void CameraFilterPack_Alien_Vision_Update_m2006984409 (CameraFilterPack_Alien_Vision_t2343910777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::OnDisable()
extern "C"  void CameraFilterPack_Alien_Vision_OnDisable_m246414061 (CameraFilterPack_Alien_Vision_t2343910777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
