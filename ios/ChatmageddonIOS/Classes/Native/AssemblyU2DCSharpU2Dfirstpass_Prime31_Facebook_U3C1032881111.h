﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<System.String,Prime31.FacebookMeResult>
struct Action_2_t2393018193;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.Facebook/<getMe>c__AnonStorey2
struct  U3CgetMeU3Ec__AnonStorey2_t1032881111  : public Il2CppObject
{
public:
	// System.Action`2<System.String,Prime31.FacebookMeResult> Prime31.Facebook/<getMe>c__AnonStorey2::completionHandler
	Action_2_t2393018193 * ___completionHandler_0;

public:
	inline static int32_t get_offset_of_completionHandler_0() { return static_cast<int32_t>(offsetof(U3CgetMeU3Ec__AnonStorey2_t1032881111, ___completionHandler_0)); }
	inline Action_2_t2393018193 * get_completionHandler_0() const { return ___completionHandler_0; }
	inline Action_2_t2393018193 ** get_address_of_completionHandler_0() { return &___completionHandler_0; }
	inline void set_completionHandler_0(Action_2_t2393018193 * value)
	{
		___completionHandler_0 = value;
		Il2CppCodeGenWriteBarrier(&___completionHandler_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
