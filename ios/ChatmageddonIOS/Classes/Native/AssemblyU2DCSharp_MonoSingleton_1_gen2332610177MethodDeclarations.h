﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<SettingsNavigationController>::.ctor()
#define MonoSingleton_1__ctor_m1375771915(__this, method) ((  void (*) (MonoSingleton_1_t2332610177 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<SettingsNavigationController>::Awake()
#define MonoSingleton_1_Awake_m879010220(__this, method) ((  void (*) (MonoSingleton_1_t2332610177 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<SettingsNavigationController>::get_Instance()
#define MonoSingleton_1_get_Instance_m3872251226(__this /* static, unused */, method) ((  SettingsNavigationController_t2581944457 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<SettingsNavigationController>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m4021740914(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<SettingsNavigationController>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m3483522685(__this, method) ((  void (*) (MonoSingleton_1_t2332610177 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<SettingsNavigationController>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m663432813(__this, method) ((  void (*) (MonoSingleton_1_t2332610177 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<SettingsNavigationController>::.cctor()
#define MonoSingleton_1__cctor_m2803642426(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
