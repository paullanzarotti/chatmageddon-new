﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsHomeContent
struct SettingsHomeContent_t249573869;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsHomeContent::.ctor()
extern "C"  void SettingsHomeContent__ctor_m3365240358 (SettingsHomeContent_t249573869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
