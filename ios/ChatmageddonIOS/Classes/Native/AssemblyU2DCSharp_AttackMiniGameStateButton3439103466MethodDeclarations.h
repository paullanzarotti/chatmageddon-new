﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackMiniGameStateButton
struct AttackMiniGameStateButton_t3439103466;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackGameState4284764851.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AttackMiniGameStateButton::.ctor()
extern "C"  void AttackMiniGameStateButton__ctor_m2185587231 (AttackMiniGameStateButton_t3439103466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameStateButton::Start()
extern "C"  void AttackMiniGameStateButton_Start_m3982733403 (AttackMiniGameStateButton_t3439103466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameStateButton::SetButtonState(AttackGameState)
extern "C"  void AttackMiniGameStateButton_SetButtonState_m3321893577 (AttackMiniGameStateButton_t3439103466 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameStateButton::OnButtonClick()
extern "C"  void AttackMiniGameStateButton_OnButtonClick_m4107764942 (AttackMiniGameStateButton_t3439103466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameStateButton::SetLabelText(System.String)
extern "C"  void AttackMiniGameStateButton_SetLabelText_m2915853314 (AttackMiniGameStateButton_t3439103466 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackMiniGameStateButton::OnButtonsTweenFinished()
extern "C"  void AttackMiniGameStateButton_OnButtonsTweenFinished_m3496876366 (AttackMiniGameStateButton_t3439103466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
