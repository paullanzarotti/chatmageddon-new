﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// NotificationManager
struct NotificationManager_t2388475022;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationManager/<CheckUnreadNotifications>c__Iterator1
struct  U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442  : public Il2CppObject
{
public:
	// System.Boolean NotificationManager/<CheckUnreadNotifications>c__Iterator1::<checkAvailability>__0
	bool ___U3CcheckAvailabilityU3E__0_0;
	// NotificationManager NotificationManager/<CheckUnreadNotifications>c__Iterator1::$this
	NotificationManager_t2388475022 * ___U24this_1;
	// System.Object NotificationManager/<CheckUnreadNotifications>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean NotificationManager/<CheckUnreadNotifications>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 NotificationManager/<CheckUnreadNotifications>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcheckAvailabilityU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442, ___U3CcheckAvailabilityU3E__0_0)); }
	inline bool get_U3CcheckAvailabilityU3E__0_0() const { return ___U3CcheckAvailabilityU3E__0_0; }
	inline bool* get_address_of_U3CcheckAvailabilityU3E__0_0() { return &___U3CcheckAvailabilityU3E__0_0; }
	inline void set_U3CcheckAvailabilityU3E__0_0(bool value)
	{
		___U3CcheckAvailabilityU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442, ___U24this_1)); }
	inline NotificationManager_t2388475022 * get_U24this_1() const { return ___U24this_1; }
	inline NotificationManager_t2388475022 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(NotificationManager_t2388475022 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCheckUnreadNotificationsU3Ec__Iterator1_t2482171442, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
