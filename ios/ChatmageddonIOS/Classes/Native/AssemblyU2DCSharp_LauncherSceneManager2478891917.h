﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_BaseSceneManager_1_gen1857718269.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LauncherSceneManager
struct  LauncherSceneManager_t2478891917  : public BaseSceneManager_1_t1857718269
{
public:
	// UnityEngine.GameObject LauncherSceneManager::launcherCenterUI
	GameObject_t1756533147 * ___launcherCenterUI_17;
	// UnityEngine.GameObject LauncherSceneManager::launchAnimation
	GameObject_t1756533147 * ___launchAnimation_18;

public:
	inline static int32_t get_offset_of_launcherCenterUI_17() { return static_cast<int32_t>(offsetof(LauncherSceneManager_t2478891917, ___launcherCenterUI_17)); }
	inline GameObject_t1756533147 * get_launcherCenterUI_17() const { return ___launcherCenterUI_17; }
	inline GameObject_t1756533147 ** get_address_of_launcherCenterUI_17() { return &___launcherCenterUI_17; }
	inline void set_launcherCenterUI_17(GameObject_t1756533147 * value)
	{
		___launcherCenterUI_17 = value;
		Il2CppCodeGenWriteBarrier(&___launcherCenterUI_17, value);
	}

	inline static int32_t get_offset_of_launchAnimation_18() { return static_cast<int32_t>(offsetof(LauncherSceneManager_t2478891917, ___launchAnimation_18)); }
	inline GameObject_t1756533147 * get_launchAnimation_18() const { return ___launchAnimation_18; }
	inline GameObject_t1756533147 ** get_address_of_launchAnimation_18() { return &___launchAnimation_18; }
	inline void set_launchAnimation_18(GameObject_t1756533147 * value)
	{
		___launchAnimation_18 = value;
		Il2CppCodeGenWriteBarrier(&___launchAnimation_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
