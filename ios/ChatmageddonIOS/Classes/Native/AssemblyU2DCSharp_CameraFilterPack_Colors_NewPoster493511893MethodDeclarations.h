﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_NewPosterize
struct CameraFilterPack_Colors_NewPosterize_t493511893;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_NewPosterize::.ctor()
extern "C"  void CameraFilterPack_Colors_NewPosterize__ctor_m1448049484 (CameraFilterPack_Colors_NewPosterize_t493511893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_NewPosterize::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_NewPosterize_get_material_m389416785 (CameraFilterPack_Colors_NewPosterize_t493511893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::Start()
extern "C"  void CameraFilterPack_Colors_NewPosterize_Start_m1218004 (CameraFilterPack_Colors_NewPosterize_t493511893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_NewPosterize_OnRenderImage_m1301141996 (CameraFilterPack_Colors_NewPosterize_t493511893 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::OnValidate()
extern "C"  void CameraFilterPack_Colors_NewPosterize_OnValidate_m1340458955 (CameraFilterPack_Colors_NewPosterize_t493511893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::Update()
extern "C"  void CameraFilterPack_Colors_NewPosterize_Update_m3747106493 (CameraFilterPack_Colors_NewPosterize_t493511893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::OnDisable()
extern "C"  void CameraFilterPack_Colors_NewPosterize_OnDisable_m1600679065 (CameraFilterPack_Colors_NewPosterize_t493511893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
