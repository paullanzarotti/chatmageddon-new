﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeviceCameraUIScaler
struct  DeviceCameraUIScaler_t3087545317  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UISprite DeviceCameraUIScaler::background
	UISprite_t603616735 * ___background_14;
	// UnityEngine.BoxCollider DeviceCameraUIScaler::backgroundCollider
	BoxCollider_t22920061 * ___backgroundCollider_15;
	// UISprite DeviceCameraUIScaler::takePhotoButton
	UISprite_t603616735 * ___takePhotoButton_16;
	// UnityEngine.BoxCollider DeviceCameraUIScaler::takePhotoCollider
	BoxCollider_t22920061 * ___takePhotoCollider_17;
	// UISprite DeviceCameraUIScaler::divider
	UISprite_t603616735 * ___divider_18;
	// UISprite DeviceCameraUIScaler::chooseExistingButton
	UISprite_t603616735 * ___chooseExistingButton_19;
	// UnityEngine.BoxCollider DeviceCameraUIScaler::chooseExistingCollider
	BoxCollider_t22920061 * ___chooseExistingCollider_20;
	// UISprite DeviceCameraUIScaler::cancelButton
	UISprite_t603616735 * ___cancelButton_21;
	// UnityEngine.BoxCollider DeviceCameraUIScaler::cancelCollider
	BoxCollider_t22920061 * ___cancelCollider_22;

public:
	inline static int32_t get_offset_of_background_14() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___background_14)); }
	inline UISprite_t603616735 * get_background_14() const { return ___background_14; }
	inline UISprite_t603616735 ** get_address_of_background_14() { return &___background_14; }
	inline void set_background_14(UISprite_t603616735 * value)
	{
		___background_14 = value;
		Il2CppCodeGenWriteBarrier(&___background_14, value);
	}

	inline static int32_t get_offset_of_backgroundCollider_15() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___backgroundCollider_15)); }
	inline BoxCollider_t22920061 * get_backgroundCollider_15() const { return ___backgroundCollider_15; }
	inline BoxCollider_t22920061 ** get_address_of_backgroundCollider_15() { return &___backgroundCollider_15; }
	inline void set_backgroundCollider_15(BoxCollider_t22920061 * value)
	{
		___backgroundCollider_15 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundCollider_15, value);
	}

	inline static int32_t get_offset_of_takePhotoButton_16() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___takePhotoButton_16)); }
	inline UISprite_t603616735 * get_takePhotoButton_16() const { return ___takePhotoButton_16; }
	inline UISprite_t603616735 ** get_address_of_takePhotoButton_16() { return &___takePhotoButton_16; }
	inline void set_takePhotoButton_16(UISprite_t603616735 * value)
	{
		___takePhotoButton_16 = value;
		Il2CppCodeGenWriteBarrier(&___takePhotoButton_16, value);
	}

	inline static int32_t get_offset_of_takePhotoCollider_17() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___takePhotoCollider_17)); }
	inline BoxCollider_t22920061 * get_takePhotoCollider_17() const { return ___takePhotoCollider_17; }
	inline BoxCollider_t22920061 ** get_address_of_takePhotoCollider_17() { return &___takePhotoCollider_17; }
	inline void set_takePhotoCollider_17(BoxCollider_t22920061 * value)
	{
		___takePhotoCollider_17 = value;
		Il2CppCodeGenWriteBarrier(&___takePhotoCollider_17, value);
	}

	inline static int32_t get_offset_of_divider_18() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___divider_18)); }
	inline UISprite_t603616735 * get_divider_18() const { return ___divider_18; }
	inline UISprite_t603616735 ** get_address_of_divider_18() { return &___divider_18; }
	inline void set_divider_18(UISprite_t603616735 * value)
	{
		___divider_18 = value;
		Il2CppCodeGenWriteBarrier(&___divider_18, value);
	}

	inline static int32_t get_offset_of_chooseExistingButton_19() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___chooseExistingButton_19)); }
	inline UISprite_t603616735 * get_chooseExistingButton_19() const { return ___chooseExistingButton_19; }
	inline UISprite_t603616735 ** get_address_of_chooseExistingButton_19() { return &___chooseExistingButton_19; }
	inline void set_chooseExistingButton_19(UISprite_t603616735 * value)
	{
		___chooseExistingButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___chooseExistingButton_19, value);
	}

	inline static int32_t get_offset_of_chooseExistingCollider_20() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___chooseExistingCollider_20)); }
	inline BoxCollider_t22920061 * get_chooseExistingCollider_20() const { return ___chooseExistingCollider_20; }
	inline BoxCollider_t22920061 ** get_address_of_chooseExistingCollider_20() { return &___chooseExistingCollider_20; }
	inline void set_chooseExistingCollider_20(BoxCollider_t22920061 * value)
	{
		___chooseExistingCollider_20 = value;
		Il2CppCodeGenWriteBarrier(&___chooseExistingCollider_20, value);
	}

	inline static int32_t get_offset_of_cancelButton_21() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___cancelButton_21)); }
	inline UISprite_t603616735 * get_cancelButton_21() const { return ___cancelButton_21; }
	inline UISprite_t603616735 ** get_address_of_cancelButton_21() { return &___cancelButton_21; }
	inline void set_cancelButton_21(UISprite_t603616735 * value)
	{
		___cancelButton_21 = value;
		Il2CppCodeGenWriteBarrier(&___cancelButton_21, value);
	}

	inline static int32_t get_offset_of_cancelCollider_22() { return static_cast<int32_t>(offsetof(DeviceCameraUIScaler_t3087545317, ___cancelCollider_22)); }
	inline BoxCollider_t22920061 * get_cancelCollider_22() const { return ___cancelCollider_22; }
	inline BoxCollider_t22920061 ** get_address_of_cancelCollider_22() { return &___cancelCollider_22; }
	inline void set_cancelCollider_22(BoxCollider_t22920061 * value)
	{
		___cancelCollider_22 = value;
		Il2CppCodeGenWriteBarrier(&___cancelCollider_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
