﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ContactsPhoneNumberManager>::.ctor()
#define MonoSingleton_1__ctor_m3539894059(__this, method) ((  void (*) (MonoSingleton_1_t3918856289 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ContactsPhoneNumberManager>::Awake()
#define MonoSingleton_1_Awake_m2497126074(__this, method) ((  void (*) (MonoSingleton_1_t3918856289 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ContactsPhoneNumberManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m1581338760(__this /* static, unused */, method) ((  ContactsPhoneNumberManager_t4168190569 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ContactsPhoneNumberManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1787148600(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ContactsPhoneNumberManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m1653702605(__this, method) ((  void (*) (MonoSingleton_1_t3918856289 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ContactsPhoneNumberManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m2349065597(__this, method) ((  void (*) (MonoSingleton_1_t3918856289 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ContactsPhoneNumberManager>::.cctor()
#define MonoSingleton_1__cctor_m253037536(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
