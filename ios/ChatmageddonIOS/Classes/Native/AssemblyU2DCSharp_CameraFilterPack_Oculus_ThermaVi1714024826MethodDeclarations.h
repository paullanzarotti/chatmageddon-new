﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Oculus_ThermaVision
struct CameraFilterPack_Oculus_ThermaVision_t1714024826;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Oculus_ThermaVision::.ctor()
extern "C"  void CameraFilterPack_Oculus_ThermaVision__ctor_m3380607079 (CameraFilterPack_Oculus_ThermaVision_t1714024826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_ThermaVision::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Oculus_ThermaVision_get_material_m3663466024 (CameraFilterPack_Oculus_ThermaVision_t1714024826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::Start()
extern "C"  void CameraFilterPack_Oculus_ThermaVision_Start_m4213832987 (CameraFilterPack_Oculus_ThermaVision_t1714024826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Oculus_ThermaVision_OnRenderImage_m814097331 (CameraFilterPack_Oculus_ThermaVision_t1714024826 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::OnValidate()
extern "C"  void CameraFilterPack_Oculus_ThermaVision_OnValidate_m1222539606 (CameraFilterPack_Oculus_ThermaVision_t1714024826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::Update()
extern "C"  void CameraFilterPack_Oculus_ThermaVision_Update_m3321784392 (CameraFilterPack_Oculus_ThermaVision_t1714024826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::OnDisable()
extern "C"  void CameraFilterPack_Oculus_ThermaVision_OnDisable_m159738894 (CameraFilterPack_Oculus_ThermaVision_t1714024826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
