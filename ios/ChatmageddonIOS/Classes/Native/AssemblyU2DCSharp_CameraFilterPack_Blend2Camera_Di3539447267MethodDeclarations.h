﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_Divide
struct CameraFilterPack_Blend2Camera_Divide_t3539447267;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_Divide::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_Divide__ctor_m3537053150 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Divide::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_Divide_get_material_m3837478779 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::Start()
extern "C"  void CameraFilterPack_Blend2Camera_Divide_Start_m72367138 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_Divide_OnRenderImage_m2197990874 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_Divide_OnValidate_m3360154589 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::Update()
extern "C"  void CameraFilterPack_Blend2Camera_Divide_Update_m805587407 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_Divide_OnEnable_m993327778 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_Divide_OnDisable_m3962732807 (CameraFilterPack_Blend2Camera_Divide_t3539447267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
