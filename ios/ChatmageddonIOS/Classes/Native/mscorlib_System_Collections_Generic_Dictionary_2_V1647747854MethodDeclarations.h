﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2549450455MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3841066438(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1647747854 *, Dictionary_2_t2944688011 *, const MethodInfo*))ValueCollection__ctor_m2130100390_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1105242676(__this, ___item0, method) ((  void (*) (ValueCollection_t1647747854 *, Panel_t1787746694 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3438430544_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2605332759(__this, method) ((  void (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1058040647_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1240170560(__this, ___item0, method) ((  bool (*) (ValueCollection_t1647747854 *, Panel_t1787746694 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m694834480_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3871834311(__this, ___item0, method) ((  bool (*) (ValueCollection_t1647747854 *, Panel_t1787746694 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m979932759_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2896947905(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1918810493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2619798789(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1647747854 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2215146529_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m584613316(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m908029848_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3013304899(__this, method) ((  bool (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3483533139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m320098569(__this, method) ((  bool (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3925226765_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2706333509(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3186168593_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3666504263(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1647747854 *, PanelU5BU5D_t2729894307*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1621950423_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::GetEnumerator()
#define ValueCollection_GetEnumerator_m233089836(__this, method) ((  Enumerator_t336253479  (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_GetEnumerator_m271368956_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<PanelType,Panel>::get_Count()
#define ValueCollection_get_Count_m248746785(__this, method) ((  int32_t (*) (ValueCollection_t1647747854 *, const MethodInfo*))ValueCollection_get_Count_m250972581_gshared)(__this, method)
