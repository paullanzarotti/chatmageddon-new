﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumberContinueButton
struct PhoneNumberContinueButton_t1818508624;

#include "codegen/il2cpp-codegen.h"

// System.Void PhoneNumberContinueButton::.ctor()
extern "C"  void PhoneNumberContinueButton__ctor_m4205411471 (PhoneNumberContinueButton_t1818508624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberContinueButton::OnButtonClick()
extern "C"  void PhoneNumberContinueButton_OnButtonClick_m3695868776 (PhoneNumberContinueButton_t1818508624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumberContinueButton::ValidateInput()
extern "C"  bool PhoneNumberContinueButton_ValidateInput_m657593359 (PhoneNumberContinueButton_t1818508624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhoneNumberContinueButton::<OnButtonClick>__BaseCallProxy0()
extern "C"  void PhoneNumberContinueButton_U3COnButtonClickU3E__BaseCallProxy0_m201674555 (PhoneNumberContinueButton_t1818508624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
