﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2122219512.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_PhoneNumberNavScreen1263467250.h"

// System.Void System.Array/InternalEnumerator`1<PhoneNumberNavScreen>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1038882613_gshared (InternalEnumerator_1_t2122219512 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1038882613(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2122219512 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1038882613_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PhoneNumberNavScreen>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1585961217_gshared (InternalEnumerator_1_t2122219512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1585961217(__this, method) ((  void (*) (InternalEnumerator_1_t2122219512 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1585961217_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PhoneNumberNavScreen>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625284369_gshared (InternalEnumerator_1_t2122219512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625284369(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2122219512 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625284369_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PhoneNumberNavScreen>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m254708054_gshared (InternalEnumerator_1_t2122219512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m254708054(__this, method) ((  void (*) (InternalEnumerator_1_t2122219512 *, const MethodInfo*))InternalEnumerator_1_Dispose_m254708054_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PhoneNumberNavScreen>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2063903841_gshared (InternalEnumerator_1_t2122219512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2063903841(__this, method) ((  bool (*) (InternalEnumerator_1_t2122219512 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2063903841_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PhoneNumberNavScreen>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1437581580_gshared (InternalEnumerator_1_t2122219512 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1437581580(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2122219512 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1437581580_gshared)(__this, method)
