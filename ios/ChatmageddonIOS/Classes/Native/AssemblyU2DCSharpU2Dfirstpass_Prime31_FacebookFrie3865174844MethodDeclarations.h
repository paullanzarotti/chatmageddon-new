﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookFriend
struct FacebookFriend_t3865174844;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookFriend::.ctor()
extern "C"  void FacebookFriend__ctor_m1533935012 (FacebookFriend_t3865174844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
