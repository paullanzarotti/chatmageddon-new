﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Unibill.Impl.IRawGooglePlayInterface
struct IRawGooglePlayInterface_t4240979409;
// Unibill.Impl.IBillingServiceCallback
struct IBillingServiceCallback_t1876287372;
// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.GooglePlayBillingService
struct  GooglePlayBillingService_t2494001613  : public Il2CppObject
{
public:
	// System.String Unibill.Impl.GooglePlayBillingService::publicKey
	String_t* ___publicKey_0;
	// Unibill.Impl.IRawGooglePlayInterface Unibill.Impl.GooglePlayBillingService::rawInterface
	Il2CppObject * ___rawInterface_1;
	// Unibill.Impl.IBillingServiceCallback Unibill.Impl.GooglePlayBillingService::callback
	Il2CppObject * ___callback_2;
	// Unibill.Impl.ProductIdRemapper Unibill.Impl.GooglePlayBillingService::remapper
	ProductIdRemapper_t3313438456 * ___remapper_3;
	// Unibill.Impl.UnibillConfiguration Unibill.Impl.GooglePlayBillingService::db
	UnibillConfiguration_t2915611853 * ___db_4;
	// Uniject.ILogger Unibill.Impl.GooglePlayBillingService::logger
	Il2CppObject * ___logger_5;
	// System.Collections.Generic.HashSet`1<System.String> Unibill.Impl.GooglePlayBillingService::unknownAmazonProducts
	HashSet_1_t362681087 * ___unknownAmazonProducts_6;

public:
	inline static int32_t get_offset_of_publicKey_0() { return static_cast<int32_t>(offsetof(GooglePlayBillingService_t2494001613, ___publicKey_0)); }
	inline String_t* get_publicKey_0() const { return ___publicKey_0; }
	inline String_t** get_address_of_publicKey_0() { return &___publicKey_0; }
	inline void set_publicKey_0(String_t* value)
	{
		___publicKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___publicKey_0, value);
	}

	inline static int32_t get_offset_of_rawInterface_1() { return static_cast<int32_t>(offsetof(GooglePlayBillingService_t2494001613, ___rawInterface_1)); }
	inline Il2CppObject * get_rawInterface_1() const { return ___rawInterface_1; }
	inline Il2CppObject ** get_address_of_rawInterface_1() { return &___rawInterface_1; }
	inline void set_rawInterface_1(Il2CppObject * value)
	{
		___rawInterface_1 = value;
		Il2CppCodeGenWriteBarrier(&___rawInterface_1, value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(GooglePlayBillingService_t2494001613, ___callback_2)); }
	inline Il2CppObject * get_callback_2() const { return ___callback_2; }
	inline Il2CppObject ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Il2CppObject * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___callback_2, value);
	}

	inline static int32_t get_offset_of_remapper_3() { return static_cast<int32_t>(offsetof(GooglePlayBillingService_t2494001613, ___remapper_3)); }
	inline ProductIdRemapper_t3313438456 * get_remapper_3() const { return ___remapper_3; }
	inline ProductIdRemapper_t3313438456 ** get_address_of_remapper_3() { return &___remapper_3; }
	inline void set_remapper_3(ProductIdRemapper_t3313438456 * value)
	{
		___remapper_3 = value;
		Il2CppCodeGenWriteBarrier(&___remapper_3, value);
	}

	inline static int32_t get_offset_of_db_4() { return static_cast<int32_t>(offsetof(GooglePlayBillingService_t2494001613, ___db_4)); }
	inline UnibillConfiguration_t2915611853 * get_db_4() const { return ___db_4; }
	inline UnibillConfiguration_t2915611853 ** get_address_of_db_4() { return &___db_4; }
	inline void set_db_4(UnibillConfiguration_t2915611853 * value)
	{
		___db_4 = value;
		Il2CppCodeGenWriteBarrier(&___db_4, value);
	}

	inline static int32_t get_offset_of_logger_5() { return static_cast<int32_t>(offsetof(GooglePlayBillingService_t2494001613, ___logger_5)); }
	inline Il2CppObject * get_logger_5() const { return ___logger_5; }
	inline Il2CppObject ** get_address_of_logger_5() { return &___logger_5; }
	inline void set_logger_5(Il2CppObject * value)
	{
		___logger_5 = value;
		Il2CppCodeGenWriteBarrier(&___logger_5, value);
	}

	inline static int32_t get_offset_of_unknownAmazonProducts_6() { return static_cast<int32_t>(offsetof(GooglePlayBillingService_t2494001613, ___unknownAmazonProducts_6)); }
	inline HashSet_1_t362681087 * get_unknownAmazonProducts_6() const { return ___unknownAmazonProducts_6; }
	inline HashSet_1_t362681087 ** get_address_of_unknownAmazonProducts_6() { return &___unknownAmazonProducts_6; }
	inline void set_unknownAmazonProducts_6(HashSet_1_t362681087 * value)
	{
		___unknownAmazonProducts_6 = value;
		Il2CppCodeGenWriteBarrier(&___unknownAmazonProducts_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
