﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/GetAnyKeyFunc
struct GetAnyKeyFunc_t2213225645;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UICamera/GetAnyKeyFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAnyKeyFunc__ctor_m3808252580 (GetAnyKeyFunc_t2213225645 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera/GetAnyKeyFunc::Invoke()
extern "C"  bool GetAnyKeyFunc_Invoke_m83437692 (GetAnyKeyFunc_t2213225645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UICamera/GetAnyKeyFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAnyKeyFunc_BeginInvoke_m854863075 (GetAnyKeyFunc_t2213225645 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera/GetAnyKeyFunc::EndInvoke(System.IAsyncResult)
extern "C"  bool GetAnyKeyFunc_EndInvoke_m261936136 (GetAnyKeyFunc_t2213225645 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
