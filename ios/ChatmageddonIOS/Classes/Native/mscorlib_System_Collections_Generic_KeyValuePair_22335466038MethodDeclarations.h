﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22335466038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1535297478_gshared (KeyValuePair_2_t2335466038 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1535297478(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2335466038 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1535297478_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3206954976_gshared (KeyValuePair_2_t2335466038 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3206954976(__this, method) ((  int32_t (*) (KeyValuePair_2_t2335466038 *, const MethodInfo*))KeyValuePair_2_get_Key_m3206954976_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3634558371_gshared (KeyValuePair_2_t2335466038 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3634558371(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2335466038 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3634558371_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m4245955624_gshared (KeyValuePair_2_t2335466038 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m4245955624(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2335466038 *, const MethodInfo*))KeyValuePair_2_get_Value_m4245955624_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m241734939_gshared (KeyValuePair_2_t2335466038 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m241734939(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2335466038 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m241734939_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Unibill.Impl.BillingPlatform,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m191268585_gshared (KeyValuePair_2_t2335466038 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m191268585(__this, method) ((  String_t* (*) (KeyValuePair_2_t2335466038 *, const MethodInfo*))KeyValuePair_2_ToString_m191268585_gshared)(__this, method)
