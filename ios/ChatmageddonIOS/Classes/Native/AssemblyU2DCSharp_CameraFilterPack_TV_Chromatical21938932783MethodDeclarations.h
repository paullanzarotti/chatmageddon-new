﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_TV_Chromatical2
struct CameraFilterPack_TV_Chromatical2_t1938932783;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_TV_Chromatical2::.ctor()
extern "C"  void CameraFilterPack_TV_Chromatical2__ctor_m3125150182 (CameraFilterPack_TV_Chromatical2_t1938932783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Chromatical2::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_TV_Chromatical2_get_material_m2936708211 (CameraFilterPack_TV_Chromatical2_t1938932783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::Start()
extern "C"  void CameraFilterPack_TV_Chromatical2_Start_m1459747482 (CameraFilterPack_TV_Chromatical2_t1938932783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_TV_Chromatical2_OnRenderImage_m2251830034 (CameraFilterPack_TV_Chromatical2_t1938932783 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::OnValidate()
extern "C"  void CameraFilterPack_TV_Chromatical2_OnValidate_m2001239441 (CameraFilterPack_TV_Chromatical2_t1938932783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::Update()
extern "C"  void CameraFilterPack_TV_Chromatical2_Update_m1820660855 (CameraFilterPack_TV_Chromatical2_t1938932783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::OnDisable()
extern "C"  void CameraFilterPack_TV_Chromatical2_OnDisable_m643988127 (CameraFilterPack_TV_Chromatical2_t1938932783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
