﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "AssemblyU2DCSharp_ChatNavScreen42377261.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatNavigateForwardButton
struct  ChatNavigateForwardButton_t4025092154  : public SFXButton_t792651341
{
public:
	// ChatNavScreen ChatNavigateForwardButton::navScreen
	int32_t ___navScreen_5;
	// System.Boolean ChatNavigateForwardButton::additionalContact
	bool ___additionalContact_6;

public:
	inline static int32_t get_offset_of_navScreen_5() { return static_cast<int32_t>(offsetof(ChatNavigateForwardButton_t4025092154, ___navScreen_5)); }
	inline int32_t get_navScreen_5() const { return ___navScreen_5; }
	inline int32_t* get_address_of_navScreen_5() { return &___navScreen_5; }
	inline void set_navScreen_5(int32_t value)
	{
		___navScreen_5 = value;
	}

	inline static int32_t get_offset_of_additionalContact_6() { return static_cast<int32_t>(offsetof(ChatNavigateForwardButton_t4025092154, ___additionalContact_6)); }
	inline bool get_additionalContact_6() const { return ___additionalContact_6; }
	inline bool* get_address_of_additionalContact_6() { return &___additionalContact_6; }
	inline void set_additionalContact_6(bool value)
	{
		___additionalContact_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
