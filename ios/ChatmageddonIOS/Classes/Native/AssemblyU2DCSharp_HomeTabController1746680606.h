﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TweenPosition
struct TweenPosition_t1144714832;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen1497346326.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeTabController
struct  HomeTabController_t1746680606  : public MonoSingleton_1_t1497346326
{
public:
	// TweenPosition HomeTabController::posTween
	TweenPosition_t1144714832 * ___posTween_3;
	// UILabel HomeTabController::incomingLabel
	UILabel_t1795115428 * ___incomingLabel_4;
	// UILabel HomeTabController::incomingAmountLabel
	UILabel_t1795115428 * ___incomingAmountLabel_5;
	// System.Boolean HomeTabController::tabClosing
	bool ___tabClosing_6;

public:
	inline static int32_t get_offset_of_posTween_3() { return static_cast<int32_t>(offsetof(HomeTabController_t1746680606, ___posTween_3)); }
	inline TweenPosition_t1144714832 * get_posTween_3() const { return ___posTween_3; }
	inline TweenPosition_t1144714832 ** get_address_of_posTween_3() { return &___posTween_3; }
	inline void set_posTween_3(TweenPosition_t1144714832 * value)
	{
		___posTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___posTween_3, value);
	}

	inline static int32_t get_offset_of_incomingLabel_4() { return static_cast<int32_t>(offsetof(HomeTabController_t1746680606, ___incomingLabel_4)); }
	inline UILabel_t1795115428 * get_incomingLabel_4() const { return ___incomingLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_incomingLabel_4() { return &___incomingLabel_4; }
	inline void set_incomingLabel_4(UILabel_t1795115428 * value)
	{
		___incomingLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___incomingLabel_4, value);
	}

	inline static int32_t get_offset_of_incomingAmountLabel_5() { return static_cast<int32_t>(offsetof(HomeTabController_t1746680606, ___incomingAmountLabel_5)); }
	inline UILabel_t1795115428 * get_incomingAmountLabel_5() const { return ___incomingAmountLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_incomingAmountLabel_5() { return &___incomingAmountLabel_5; }
	inline void set_incomingAmountLabel_5(UILabel_t1795115428 * value)
	{
		___incomingAmountLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___incomingAmountLabel_5, value);
	}

	inline static int32_t get_offset_of_tabClosing_6() { return static_cast<int32_t>(offsetof(HomeTabController_t1746680606, ___tabClosing_6)); }
	inline bool get_tabClosing_6() const { return ___tabClosing_6; }
	inline bool* get_address_of_tabClosing_6() { return &___tabClosing_6; }
	inline void set_tabClosing_6(bool value)
	{
		___tabClosing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
