﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Colors_Adjust_ColorRGB
struct CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::.ctor()
extern "C"  void CameraFilterPack_Colors_Adjust_ColorRGB__ctor_m163381917 (CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Adjust_ColorRGB::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Colors_Adjust_ColorRGB_get_material_m3964332456 (CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::Start()
extern "C"  void CameraFilterPack_Colors_Adjust_ColorRGB_Start_m3135262629 (CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Colors_Adjust_ColorRGB_OnRenderImage_m785376469 (CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::OnValidate()
extern "C"  void CameraFilterPack_Colors_Adjust_ColorRGB_OnValidate_m717544664 (CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::Update()
extern "C"  void CameraFilterPack_Colors_Adjust_ColorRGB_Update_m3289444298 (CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::OnDisable()
extern "C"  void CameraFilterPack_Colors_Adjust_ColorRGB_OnDisable_m3098717436 (CameraFilterPack_Colors_Adjust_ColorRGB_t2744619808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
