﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetLatestConfig>c__AnonStoreyC
struct U3CGetLatestConfigU3Ec__AnonStoreyC_t3154160566;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetLatestConfig>c__AnonStoreyC::.ctor()
extern "C"  void U3CGetLatestConfigU3Ec__AnonStoreyC__ctor_m3786990525 (U3CGetLatestConfigU3Ec__AnonStoreyC_t3154160566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetLatestConfig>c__AnonStoreyC::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetLatestConfigU3Ec__AnonStoreyC_U3CU3Em__0_m1704637722 (U3CGetLatestConfigU3Ec__AnonStoreyC_t3154160566 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
