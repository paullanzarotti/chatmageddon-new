﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardILP
struct LeaderboardILP_t694823370;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform
struct Transform_t3275118058;
// User
struct User_t719925459;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LeaderboardType1980945291.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "AssemblyU2DCSharp_User719925459.h"

// System.Void LeaderboardILP::.ctor()
extern "C"  void LeaderboardILP__ctor_m3125659627 (LeaderboardILP_t694823370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::Awake()
extern "C"  void LeaderboardILP_Awake_m718620854 (LeaderboardILP_t694823370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::StartIL(LeaderboardType,System.Boolean)
extern "C"  void LeaderboardILP_StartIL_m3107776078 (LeaderboardILP_t694823370 * __this, int32_t ___type0, bool ___reset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::OnScaleTweenFinished()
extern "C"  void LeaderboardILP_OnScaleTweenFinished_m593113197 (LeaderboardILP_t694823370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LeaderboardILP::waitToRestartIL()
extern "C"  Il2CppObject * LeaderboardILP_waitToRestartIL_m2407390359 (LeaderboardILP_t694823370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::DrawIL()
extern "C"  void LeaderboardILP_DrawIL_m3198862018 (LeaderboardILP_t694823370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::SetPlayerUIVisible(System.Boolean)
extern "C"  void LeaderboardILP_SetPlayerUIVisible_m2322966107 (LeaderboardILP_t694823370 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::SetDataArray(LeaderboardType)
extern "C"  void LeaderboardILP_SetDataArray_m1823368241 (LeaderboardILP_t694823370 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LeaderboardILP::WaitForWorldLeaderboardLoad(System.Boolean)
extern "C"  Il2CppObject * LeaderboardILP_WaitForWorldLeaderboardLoad_m3417720583 (LeaderboardILP_t694823370 * __this, bool ___allTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::UpdateListData()
extern "C"  void LeaderboardILP_UpdateListData_m580531104 (LeaderboardILP_t694823370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::InitListItemWithIndex(UnityEngine.Transform,System.Int32,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void LeaderboardILP_InitListItemWithIndex_m3218334189 (LeaderboardILP_t694823370 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, int32_t ___poolIndex2, Nullable_1_t334943763  ___carouselIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP::PopulateListItemWithIndex(UnityEngine.Transform,System.Int32,System.Nullable`1<System.Int32>)
extern "C"  void LeaderboardILP_PopulateListItemWithIndex_m332251926 (LeaderboardILP_t694823370 * __this, Transform_t3275118058 * ___item0, int32_t ___dataIndex1, Nullable_1_t334943763  ___carouselIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__0(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__0_m4173111904 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___tp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__1(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__1_m4173078145 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___rl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LeaderboardILP::<SetDataArray>m__2(User)
extern "C"  String_t* LeaderboardILP_U3CSetDataArrayU3Em__2_m3343229713 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___un0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__3(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__3_m4173145663 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___tp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__4(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__4_m4172976604 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___rl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LeaderboardILP::<SetDataArray>m__5(User)
extern "C"  String_t* LeaderboardILP_U3CSetDataArrayU3Em__5_m2852880180 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___un0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__6(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__6_m4173044122 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___tp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__7(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__7_m4173010363 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___rl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LeaderboardILP::<SetDataArray>m__8(User)
extern "C"  String_t* LeaderboardILP_U3CSetDataArrayU3Em__8_m128038723 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___un0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__9(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__9_m4172790665 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___tp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LeaderboardILP::<SetDataArray>m__A(User)
extern "C"  int32_t LeaderboardILP_U3CSetDataArrayU3Em__A_m4177209681 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___rl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LeaderboardILP::<SetDataArray>m__B(User)
extern "C"  String_t* LeaderboardILP_U3CSetDataArrayU3Em__B_m978138913 (Il2CppObject * __this /* static, unused */, User_t719925459 * ___un0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
