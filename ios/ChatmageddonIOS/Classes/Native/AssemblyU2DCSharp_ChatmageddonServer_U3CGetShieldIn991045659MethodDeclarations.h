﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<GetShieldInventory>c__AnonStorey37
struct U3CGetShieldInventoryU3Ec__AnonStorey37_t991045659;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<GetShieldInventory>c__AnonStorey37::.ctor()
extern "C"  void U3CGetShieldInventoryU3Ec__AnonStorey37__ctor_m2285154170 (U3CGetShieldInventoryU3Ec__AnonStorey37_t991045659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<GetShieldInventory>c__AnonStorey37::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CGetShieldInventoryU3Ec__AnonStorey37_U3CU3Em__0_m1948138591 (U3CGetShieldInventoryU3Ec__AnonStorey37_t991045659 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
