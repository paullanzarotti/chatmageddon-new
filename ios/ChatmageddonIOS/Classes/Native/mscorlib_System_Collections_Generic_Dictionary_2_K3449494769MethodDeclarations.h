﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Char,System.Char>
struct Dictionary_2_t759991331;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3449494769.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1087450417_gshared (Enumerator_t3449494769 * __this, Dictionary_2_t759991331 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1087450417(__this, ___host0, method) ((  void (*) (Enumerator_t3449494769 *, Dictionary_2_t759991331 *, const MethodInfo*))Enumerator__ctor_m1087450417_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3863856756_gshared (Enumerator_t3449494769 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3863856756(__this, method) ((  Il2CppObject * (*) (Enumerator_t3449494769 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3863856756_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m71575220_gshared (Enumerator_t3449494769 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m71575220(__this, method) ((  void (*) (Enumerator_t3449494769 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m71575220_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m49196121_gshared (Enumerator_t3449494769 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m49196121(__this, method) ((  void (*) (Enumerator_t3449494769 *, const MethodInfo*))Enumerator_Dispose_m49196121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m618764807_gshared (Enumerator_t3449494769 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m618764807(__this, method) ((  bool (*) (Enumerator_t3449494769 *, const MethodInfo*))Enumerator_MoveNext_m618764807_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Char,System.Char>::get_Current()
extern "C"  Il2CppChar Enumerator_get_Current_m1348508795_gshared (Enumerator_t3449494769 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1348508795(__this, method) ((  Il2CppChar (*) (Enumerator_t3449494769 *, const MethodInfo*))Enumerator_get_Current_m1348508795_gshared)(__this, method)
