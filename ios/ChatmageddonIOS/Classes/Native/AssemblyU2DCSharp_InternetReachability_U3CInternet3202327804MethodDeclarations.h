﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InternetReachability/<InternetPollCheck>c__Iterator0
struct U3CInternetPollCheckU3Ec__Iterator0_t3202327804;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void InternetReachability/<InternetPollCheck>c__Iterator0::.ctor()
extern "C"  void U3CInternetPollCheckU3Ec__Iterator0__ctor_m1598389005 (U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InternetReachability/<InternetPollCheck>c__Iterator0::MoveNext()
extern "C"  bool U3CInternetPollCheckU3Ec__Iterator0_MoveNext_m3304391031 (U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InternetReachability/<InternetPollCheck>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInternetPollCheckU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3964723167 (U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InternetReachability/<InternetPollCheck>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInternetPollCheckU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1053460519 (U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability/<InternetPollCheck>c__Iterator0::Dispose()
extern "C"  void U3CInternetPollCheckU3Ec__Iterator0_Dispose_m539243670 (U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InternetReachability/<InternetPollCheck>c__Iterator0::Reset()
extern "C"  void U3CInternetPollCheckU3Ec__Iterator0_Reset_m527426444 (U3CInternetPollCheckU3Ec__Iterator0_t3202327804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
