﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AvatarUpdater
struct AvatarUpdater_t2404165026;
// UILabel
struct UILabel_t1795115428;
// DisplayLocationSwitch
struct DisplayLocationSwitch_t413816511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsProfileContent
struct  SettingsProfileContent_t3280006997  : public MonoBehaviour_t1158329972
{
public:
	// AvatarUpdater SettingsProfileContent::avatar
	AvatarUpdater_t2404165026 * ___avatar_2;
	// UILabel SettingsProfileContent::firstNameLabel
	UILabel_t1795115428 * ___firstNameLabel_3;
	// UILabel SettingsProfileContent::lastNameLabel
	UILabel_t1795115428 * ___lastNameLabel_4;
	// UILabel SettingsProfileContent::userNameLabel
	UILabel_t1795115428 * ___userNameLabel_5;
	// UILabel SettingsProfileContent::genderLabel
	UILabel_t1795115428 * ___genderLabel_6;
	// UILabel SettingsProfileContent::audienceLabel
	UILabel_t1795115428 * ___audienceLabel_7;
	// DisplayLocationSwitch SettingsProfileContent::locationSwitch
	DisplayLocationSwitch_t413816511 * ___locationSwitch_8;

public:
	inline static int32_t get_offset_of_avatar_2() { return static_cast<int32_t>(offsetof(SettingsProfileContent_t3280006997, ___avatar_2)); }
	inline AvatarUpdater_t2404165026 * get_avatar_2() const { return ___avatar_2; }
	inline AvatarUpdater_t2404165026 ** get_address_of_avatar_2() { return &___avatar_2; }
	inline void set_avatar_2(AvatarUpdater_t2404165026 * value)
	{
		___avatar_2 = value;
		Il2CppCodeGenWriteBarrier(&___avatar_2, value);
	}

	inline static int32_t get_offset_of_firstNameLabel_3() { return static_cast<int32_t>(offsetof(SettingsProfileContent_t3280006997, ___firstNameLabel_3)); }
	inline UILabel_t1795115428 * get_firstNameLabel_3() const { return ___firstNameLabel_3; }
	inline UILabel_t1795115428 ** get_address_of_firstNameLabel_3() { return &___firstNameLabel_3; }
	inline void set_firstNameLabel_3(UILabel_t1795115428 * value)
	{
		___firstNameLabel_3 = value;
		Il2CppCodeGenWriteBarrier(&___firstNameLabel_3, value);
	}

	inline static int32_t get_offset_of_lastNameLabel_4() { return static_cast<int32_t>(offsetof(SettingsProfileContent_t3280006997, ___lastNameLabel_4)); }
	inline UILabel_t1795115428 * get_lastNameLabel_4() const { return ___lastNameLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_lastNameLabel_4() { return &___lastNameLabel_4; }
	inline void set_lastNameLabel_4(UILabel_t1795115428 * value)
	{
		___lastNameLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___lastNameLabel_4, value);
	}

	inline static int32_t get_offset_of_userNameLabel_5() { return static_cast<int32_t>(offsetof(SettingsProfileContent_t3280006997, ___userNameLabel_5)); }
	inline UILabel_t1795115428 * get_userNameLabel_5() const { return ___userNameLabel_5; }
	inline UILabel_t1795115428 ** get_address_of_userNameLabel_5() { return &___userNameLabel_5; }
	inline void set_userNameLabel_5(UILabel_t1795115428 * value)
	{
		___userNameLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___userNameLabel_5, value);
	}

	inline static int32_t get_offset_of_genderLabel_6() { return static_cast<int32_t>(offsetof(SettingsProfileContent_t3280006997, ___genderLabel_6)); }
	inline UILabel_t1795115428 * get_genderLabel_6() const { return ___genderLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_genderLabel_6() { return &___genderLabel_6; }
	inline void set_genderLabel_6(UILabel_t1795115428 * value)
	{
		___genderLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___genderLabel_6, value);
	}

	inline static int32_t get_offset_of_audienceLabel_7() { return static_cast<int32_t>(offsetof(SettingsProfileContent_t3280006997, ___audienceLabel_7)); }
	inline UILabel_t1795115428 * get_audienceLabel_7() const { return ___audienceLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_audienceLabel_7() { return &___audienceLabel_7; }
	inline void set_audienceLabel_7(UILabel_t1795115428 * value)
	{
		___audienceLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___audienceLabel_7, value);
	}

	inline static int32_t get_offset_of_locationSwitch_8() { return static_cast<int32_t>(offsetof(SettingsProfileContent_t3280006997, ___locationSwitch_8)); }
	inline DisplayLocationSwitch_t413816511 * get_locationSwitch_8() const { return ___locationSwitch_8; }
	inline DisplayLocationSwitch_t413816511 ** get_address_of_locationSwitch_8() { return &___locationSwitch_8; }
	inline void set_locationSwitch_8(DisplayLocationSwitch_t413816511 * value)
	{
		___locationSwitch_8 = value;
		Il2CppCodeGenWriteBarrier(&___locationSwitch_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
