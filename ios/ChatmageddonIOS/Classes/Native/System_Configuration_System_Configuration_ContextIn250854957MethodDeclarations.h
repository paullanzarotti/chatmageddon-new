﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ContextInformation
struct ContextInformation_t250854957;
// System.Configuration.Configuration
struct Configuration_t3335372970;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.ContextInformation::.ctor(System.Configuration.Configuration,System.Object)
extern "C"  void ContextInformation__ctor_m622941153 (ContextInformation_t250854957 * __this, Configuration_t3335372970 * ___config0, Il2CppObject * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ContextInformation::GetSection(System.String)
extern "C"  Il2CppObject * ContextInformation_GetSection_m2858340796 (ContextInformation_t250854957 * __this, String_t* ___sectionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Configuration.ContextInformation::get_HostingContext()
extern "C"  Il2CppObject * ContextInformation_get_HostingContext_m2336616081 (ContextInformation_t250854957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.ContextInformation::get_IsMachineLevel()
extern "C"  bool ContextInformation_get_IsMachineLevel_m4019820316 (ContextInformation_t250854957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
