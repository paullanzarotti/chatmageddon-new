﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwitchPanelButton
struct SwitchPanelButton_t319351498;

#include "codegen/il2cpp-codegen.h"

// System.Void SwitchPanelButton::.ctor()
extern "C"  void SwitchPanelButton__ctor_m1305364161 (SwitchPanelButton_t319351498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwitchPanelButton::OnButtonClick()
extern "C"  void SwitchPanelButton_OnButtonClick_m4185321710 (SwitchPanelButton_t319351498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
