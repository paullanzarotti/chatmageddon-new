﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIColorPicker
struct UIColorPicker_t954141641;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void UIColorPicker::.ctor()
extern "C"  void UIColorPicker__ctor_m3074106442 (UIColorPicker_t954141641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::Start()
extern "C"  void UIColorPicker_Start_m3492607290 (UIColorPicker_t954141641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnDestroy()
extern "C"  void UIColorPicker_OnDestroy_m1105189223 (UIColorPicker_t954141641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnPress(System.Boolean)
extern "C"  void UIColorPicker_OnPress_m3798751021 (UIColorPicker_t954141641 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnDrag(UnityEngine.Vector2)
extern "C"  void UIColorPicker_OnDrag_m1071655109 (UIColorPicker_t954141641 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::OnPan(UnityEngine.Vector2)
extern "C"  void UIColorPicker_OnPan_m844102414 (UIColorPicker_t954141641 * __this, Vector2_t2243707579  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::Sample()
extern "C"  void UIColorPicker_Sample_m2411124810 (UIColorPicker_t954141641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIColorPicker::Select(UnityEngine.Vector2)
extern "C"  void UIColorPicker_Select_m4190342200 (UIColorPicker_t954141641 * __this, Vector2_t2243707579  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIColorPicker::Select(UnityEngine.Color)
extern "C"  Vector2_t2243707579  UIColorPicker_Select_m1770306629 (UIColorPicker_t954141641 * __this, Color_t2020392075  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIColorPicker::Sample(System.Single,System.Single)
extern "C"  Color_t2020392075  UIColorPicker_Sample_m977188067 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
