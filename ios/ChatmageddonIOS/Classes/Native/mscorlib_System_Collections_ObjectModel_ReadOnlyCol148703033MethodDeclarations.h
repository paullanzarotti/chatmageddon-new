﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>
struct ReadOnlyCollection_1_t148703033;
// System.Collections.Generic.IList`1<AttackSearchNav>
struct IList_1_t503857942;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// AttackSearchNav[]
struct AttackSearchNavU5BU5D_t406956560;
// System.Collections.Generic.IEnumerator`1<AttackSearchNav>
struct IEnumerator_1_t1733408464;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackSearchNav4257884637.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2838772327_gshared (ReadOnlyCollection_1_t148703033 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2838772327(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2838772327_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1556132649_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1556132649(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1556132649_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m814245685_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m814245685(__this, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m814245685_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4148521166_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4148521166(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4148521166_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2190622156_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2190622156(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2190622156_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m118211090_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m118211090(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m118211090_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1661974436_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1661974436(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1661974436_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3346566169_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3346566169(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3346566169_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m930763901_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m930763901(__this, method) ((  bool (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m930763901_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1971479244_gshared (ReadOnlyCollection_1_t148703033 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1971479244(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1971479244_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m895457257_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m895457257(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m895457257_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3522808132_gshared (ReadOnlyCollection_1_t148703033 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3522808132(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t148703033 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3522808132_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2497759582_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2497759582(__this, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2497759582_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m525453378_gshared (ReadOnlyCollection_1_t148703033 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m525453378(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t148703033 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m525453378_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1200825342_gshared (ReadOnlyCollection_1_t148703033 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1200825342(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t148703033 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1200825342_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3287977017_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3287977017(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3287977017_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3787074377_gshared (ReadOnlyCollection_1_t148703033 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3787074377(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3787074377_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2612265259_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2612265259(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2612265259_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1320515036_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1320515036(__this, method) ((  bool (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1320515036_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1348286172_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1348286172(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1348286172_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1274999741_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1274999741(__this, method) ((  bool (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1274999741_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3432120000_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3432120000(__this, method) ((  bool (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3432120000_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2057918133_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2057918133(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2057918133_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2333237626_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2333237626(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2333237626_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2538518719_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2538518719(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m2538518719_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3448582621_gshared (ReadOnlyCollection_1_t148703033 * __this, AttackSearchNavU5BU5D_t406956560* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3448582621(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t148703033 *, AttackSearchNavU5BU5D_t406956560*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3448582621_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m4041246008_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m4041246008(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m4041246008_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m825766521_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m825766521(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m825766521_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3424407316_gshared (ReadOnlyCollection_1_t148703033 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3424407316(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t148703033 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3424407316_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AttackSearchNav>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m1430692984_gshared (ReadOnlyCollection_1_t148703033 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1430692984(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t148703033 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1430692984_gshared)(__this, ___index0, method)
