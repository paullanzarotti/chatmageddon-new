﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<DefendNavScreen>
struct EqualityComparer_1_t2412254974;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<DefendNavScreen>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m90980342_gshared (EqualityComparer_1_t2412254974 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m90980342(__this, method) ((  void (*) (EqualityComparer_1_t2412254974 *, const MethodInfo*))EqualityComparer_1__ctor_m90980342_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<DefendNavScreen>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m1598280791_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m1598280791(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m1598280791_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<DefendNavScreen>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2002872259_gshared (EqualityComparer_1_t2412254974 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2002872259(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2412254974 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2002872259_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<DefendNavScreen>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2531743717_gshared (EqualityComparer_1_t2412254974 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2531743717(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2412254974 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2531743717_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<DefendNavScreen>::get_Default()
extern "C"  EqualityComparer_1_t2412254974 * EqualityComparer_1_get_Default_m1748315530_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1748315530(__this /* static, unused */, method) ((  EqualityComparer_1_t2412254974 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1748315530_gshared)(__this /* static, unused */, method)
