﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.RemoteConfigManager
struct RemoteConfigManager_t2693772795;
// Uniject.IResourceLoader
struct IResourceLoader_t2860850570;
// Uniject.IStorage
struct IStorage_t1347868490;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.Collections.Generic.List`1<Unibill.ProductDefinition>
struct List_1_t888775120;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.RemoteConfigManager::.ctor(Uniject.IResourceLoader,Uniject.IStorage,Uniject.ILogger,UnityEngine.RuntimePlatform,System.Collections.Generic.List`1<Unibill.ProductDefinition>)
extern "C"  void RemoteConfigManager__ctor_m3011166985 (RemoteConfigManager_t2693772795 * __this, Il2CppObject * ___loader0, Il2CppObject * ___storage1, Il2CppObject * ___logger2, int32_t ___platform3, List_1_t888775120 * ___runtimeProducts4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Unibill.Impl.UnibillConfiguration Unibill.Impl.RemoteConfigManager::get_Config()
extern "C"  UnibillConfiguration_t2915611853 * RemoteConfigManager_get_Config_m3881307277 (RemoteConfigManager_t2693772795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RemoteConfigManager::set_Config(Unibill.Impl.UnibillConfiguration)
extern "C"  void RemoteConfigManager_set_Config_m471832702 (RemoteConfigManager_t2693772795 * __this, UnibillConfiguration_t2915611853 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.RemoteConfigManager::refreshCachedConfig(System.String,Uniject.ILogger)
extern "C"  void RemoteConfigManager_refreshCachedConfig_m2148756477 (RemoteConfigManager_t2693772795 * __this, String_t* ___url0, Il2CppObject * ___logger1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
