﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3420759743;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.UInt32 ICSharpCode.SharpZipLib.Checksums.Crc32::ComputeCrc32(System.UInt32,System.Byte)
extern "C"  uint32_t Crc32_ComputeCrc32_m1449573370 (Il2CppObject * __this /* static, unused */, uint32_t ___oldCrc0, uint8_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ICSharpCode.SharpZipLib.Checksums.Crc32::get_Value()
extern "C"  int64_t Crc32_get_Value_m1420497589 (Crc32_t3420759743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Reset()
extern "C"  void Crc32_Reset_m2645532975 (Crc32_t3420759743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[],System.Int32,System.Int32)
extern "C"  void Crc32_Update_m3476396462 (Crc32_t3420759743 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.ctor()
extern "C"  void Crc32__ctor_m2818634674 (Crc32_t3420759743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.cctor()
extern "C"  void Crc32__cctor_m578332849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
