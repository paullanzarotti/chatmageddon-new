﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPDatePicker
struct IPDatePicker_t1471736889;
// IPDatePicker/Date
struct Date_t2675184608;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_IPDatePicker_Date2675184608.h"
#include "AssemblyU2DCSharp_DateTimeLocalization_Language3679215353.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IPDatePicker::.ctor()
extern "C"  void IPDatePicker__ctor_m701965394 (IPDatePicker_t1471736889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime IPDatePicker::get_CurrentDate()
extern "C"  DateTime_t693205669  IPDatePicker_get_CurrentDate_m1257829033 (IPDatePicker_t1471736889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDatePicker::set_CurrentDate(System.DateTime)
extern "C"  void IPDatePicker_set_CurrentDate_m702119970 (IPDatePicker_t1471736889 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPDatePicker::TryResetPickerAtDate(IPDatePicker/Date)
extern "C"  bool IPDatePicker_TryResetPickerAtDate_m3030961925 (IPDatePicker_t1471736889 * __this, Date_t2675184608 * ___date0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IPDatePicker::TryResetPickerAtDateTime(System.DateTime)
extern "C"  bool IPDatePicker_TryResetPickerAtDateTime_m258675618 (IPDatePicker_t1471736889 * __this, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDatePicker::SetNewLanguage(DateTimeLocalization/Language)
extern "C"  void IPDatePicker_SetNewLanguage_m1059819991 (IPDatePicker_t1471736889 * __this, int32_t ___newLanguage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDatePicker::SetNewLanguage(System.String)
extern "C"  void IPDatePicker_SetNewLanguage_m2315884034 (IPDatePicker_t1471736889 * __this, String_t* ___languageString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDatePicker::Init()
extern "C"  void IPDatePicker_Init_m1092690594 (IPDatePicker_t1471736889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPDatePicker::GetInitIndex()
extern "C"  int32_t IPDatePicker_GetInitIndex_m3871186572 (IPDatePicker_t1471736889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDatePicker::UpdateCurrentValue()
extern "C"  void IPDatePicker_UpdateCurrentValue_m2992460667 (IPDatePicker_t1471736889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDatePicker::UpdateVirtualElementsCount()
extern "C"  void IPDatePicker_UpdateVirtualElementsCount_m1858136324 (IPDatePicker_t1471736889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPDatePicker::UpdateWidget(System.Int32,System.Int32)
extern "C"  void IPDatePicker_UpdateWidget_m1982731549 (IPDatePicker_t1471736889 * __this, int32_t ___widgetIndex0, int32_t ___contentIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime IPDatePicker::GetDateTimeForIndex(System.Int32)
extern "C"  DateTime_t693205669  IPDatePicker_GetDateTimeForIndex_m1176118244 (IPDatePicker_t1471736889 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 IPDatePicker::GetIndexForDateTime(System.DateTime)
extern "C"  int32_t IPDatePicker_GetIndexForDateTime_m1202651388 (IPDatePicker_t1471736889 * __this, DateTime_t693205669  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
