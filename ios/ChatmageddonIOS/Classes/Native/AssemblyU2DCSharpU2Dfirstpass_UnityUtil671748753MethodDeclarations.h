﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityUtil
struct UnityUtil_t671748753;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_DeviceType2044541946.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "System_Core_System_Action3226471752.h"

// System.Void UnityUtil::.ctor()
extern "C"  void UnityUtil__ctor_m1276321058 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil::Start()
extern "C"  void UnityUtil_Start_m4144837230 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityUtil::get_currentTime()
extern "C"  DateTime_t693205669  UnityUtil_get_currentTime_m885959372 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityUtil::get_persistentDataPath()
extern "C"  String_t* UnityUtil_get_persistentDataPath_m644522340 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityUtil::loadedLevelName()
extern "C"  String_t* UnityUtil_loadedLevelName_m3221429449 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityUtil::get_Platform()
extern "C"  int32_t UnityUtil_get_Platform_m1436510839 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityUtil::get_IsEditor()
extern "C"  bool UnityUtil_get_IsEditor_m1681823694 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityUtil::get_DeviceModel()
extern "C"  String_t* UnityUtil_get_DeviceModel_m147212701 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityUtil::get_DeviceName()
extern "C"  String_t* UnityUtil_get_DeviceName_m2735798581 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DeviceType UnityUtil::get_DeviceType()
extern "C"  int32_t UnityUtil_get_DeviceType_m641556583 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityUtil::get_OperatingSystem()
extern "C"  String_t* UnityUtil_get_OperatingSystem_m697846814 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityUtil::pcPlatform()
extern "C"  bool UnityUtil_pcPlatform_m49480088 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil::DebugLog(System.String,System.Object[])
extern "C"  void UnityUtil_DebugLog_m3365762461 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityUtil::getFrustumBoundaries(UnityEngine.Camera)
extern "C"  SingleU5BU5D_t577127397* UnityUtil_getFrustumBoundaries_m3017814326 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___camera0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator)
extern "C"  Il2CppObject * UnityUtil_Uniject_IUtil_InitiateCoroutine_m4152587294 (UnityUtil_t671748753 * __this, Il2CppObject * ___start0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil::Uniject.IUtil.InitiateCoroutine(System.Collections.IEnumerator,System.Int32)
extern "C"  void UnityUtil_Uniject_IUtil_InitiateCoroutine_m351413528 (UnityUtil_t671748753 * __this, Il2CppObject * ___start0, int32_t ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityUtil::delayedCoroutine(System.Collections.IEnumerator,System.Int32)
extern "C"  Il2CppObject * UnityUtil_delayedCoroutine_m1140815218 (UnityUtil_t671748753 * __this, Il2CppObject * ___coroutine0, int32_t ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil::RunOnThreadPool(System.Action)
extern "C"  void UnityUtil_RunOnThreadPool_m3457023993 (UnityUtil_t671748753 * __this, Action_t3226471752 * ___runnable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil::Update()
extern "C"  void UnityUtil_Update_m1099942151 (UnityUtil_t671748753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil::RunOnMainThread(System.Action)
extern "C"  void UnityUtil_RunOnMainThread_m3176238374 (UnityUtil_t671748753 * __this, Action_t3226471752 * ___runnable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityUtil::getWaitForSeconds(System.Int32)
extern "C"  Il2CppObject * UnityUtil_getWaitForSeconds_m3851112887 (UnityUtil_t671748753 * __this, int32_t ___seconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityUtil::.cctor()
extern "C"  void UnityUtil__cctor_m1755325213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
