﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>
struct ReadOnlyCollection_1_t4227031606;
// System.Collections.Generic.IList`1<System.Int16>
struct IList_1_t287219219;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Collections.Generic.IEnumerator`1<System.Int16>
struct IEnumerator_1_t1516769741;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3410044699_gshared (ReadOnlyCollection_1_t4227031606 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3410044699(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3410044699_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m836770477_gshared (ReadOnlyCollection_1_t4227031606 * __this, int16_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m836770477(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, int16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m836770477_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3093891273_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3093891273(__this, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3093891273_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m573248966_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, int16_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m573248966(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, int16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m573248966_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3542413908_gshared (ReadOnlyCollection_1_t4227031606 * __this, int16_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3542413908(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t4227031606 *, int16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3542413908_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1375184090_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1375184090(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1375184090_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int16_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2459908638_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2459908638(__this, ___index0, method) ((  int16_t (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2459908638_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m874501173_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, int16_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m874501173(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, int16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m874501173_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1683980513_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1683980513(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1683980513_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3413064828_gshared (ReadOnlyCollection_1_t4227031606 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3413064828(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3413064828_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1020204433_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1020204433(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1020204433_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m681047728_gshared (ReadOnlyCollection_1_t4227031606 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m681047728(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4227031606 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m681047728_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m42316318_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m42316318(__this, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m42316318_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3417954994_gshared (ReadOnlyCollection_1_t4227031606 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3417954994(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4227031606 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3417954994_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1663729854_gshared (ReadOnlyCollection_1_t4227031606 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1663729854(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4227031606 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1663729854_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m834583437_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m834583437(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m834583437_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2641307301_gshared (ReadOnlyCollection_1_t4227031606 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2641307301(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2641307301_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m41798519_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m41798519(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m41798519_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m800980508_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m800980508(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m800980508_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1601592946_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1601592946(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1601592946_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932856313_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932856313(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932856313_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3525461168_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3525461168(__this, method) ((  bool (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3525461168_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3938416741_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3938416741(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3938416741_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3193612146_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3193612146(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3193612146_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m393672499_gshared (ReadOnlyCollection_1_t4227031606 * __this, int16_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m393672499(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t4227031606 *, int16_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m393672499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1974574865_gshared (ReadOnlyCollection_1_t4227031606 * __this, Int16U5BU5D_t3104283263* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1974574865(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t4227031606 *, Int16U5BU5D_t3104283263*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1974574865_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2419288476_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2419288476(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2419288476_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2365109489_gshared (ReadOnlyCollection_1_t4227031606 * __this, int16_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2365109489(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t4227031606 *, int16_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2365109489_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1537197536_gshared (ReadOnlyCollection_1_t4227031606 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1537197536(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t4227031606 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1537197536_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int16>::get_Item(System.Int32)
extern "C"  int16_t ReadOnlyCollection_1_get_Item_m2458186450_gshared (ReadOnlyCollection_1_t4227031606 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2458186450(__this, ___index0, method) ((  int16_t (*) (ReadOnlyCollection_1_t4227031606 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2458186450_gshared)(__this, ___index0, method)
