﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Bucks>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m465342499(__this, ___l0, method) ((  void (*) (Enumerator_t2835866526 *, List_1_t3301136852 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Bucks>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1938620271(__this, method) ((  void (*) (Enumerator_t2835866526 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Bucks>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3217879227(__this, method) ((  Il2CppObject * (*) (Enumerator_t2835866526 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Bucks>::Dispose()
#define Enumerator_Dispose_m3471966240(__this, method) ((  void (*) (Enumerator_t2835866526 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Bucks>::VerifyState()
#define Enumerator_VerifyState_m1245723497(__this, method) ((  void (*) (Enumerator_t2835866526 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Bucks>::MoveNext()
#define Enumerator_MoveNext_m2990209931(__this, method) ((  bool (*) (Enumerator_t2835866526 *, const MethodInfo*))Enumerator_MoveNext_m1245242307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Bucks>::get_Current()
#define Enumerator_get_Current_m1264924150(__this, method) ((  Bucks_t3932015720 * (*) (Enumerator_t2835866526 *, const MethodInfo*))Enumerator_get_Current_m4088623743_gshared)(__this, method)
