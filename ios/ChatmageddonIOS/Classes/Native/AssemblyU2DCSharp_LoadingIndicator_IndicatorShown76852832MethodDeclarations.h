﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingIndicator/IndicatorShown
struct IndicatorShown_t76852832;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void LoadingIndicator/IndicatorShown::.ctor(System.Object,System.IntPtr)
extern "C"  void IndicatorShown__ctor_m1786184175 (IndicatorShown_t76852832 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator/IndicatorShown::Invoke()
extern "C"  void IndicatorShown_Invoke_m158906819 (IndicatorShown_t76852832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult LoadingIndicator/IndicatorShown::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * IndicatorShown_BeginInvoke_m2926499130 (IndicatorShown_t76852832 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingIndicator/IndicatorShown::EndInvoke(System.IAsyncResult)
extern "C"  void IndicatorShown_EndInvoke_m66013441 (IndicatorShown_t76852832 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
