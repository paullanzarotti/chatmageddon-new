﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.RegexStringValidatorAttribute
struct RegexStringValidatorAttribute_t2980974044;
// System.String
struct String_t;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t210547623;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.RegexStringValidatorAttribute::.ctor(System.String)
extern "C"  void RegexStringValidatorAttribute__ctor_m987182821 (RegexStringValidatorAttribute_t2980974044 * __this, String_t* ___regex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.RegexStringValidatorAttribute::get_Regex()
extern "C"  String_t* RegexStringValidatorAttribute_get_Regex_m3900445448 (RegexStringValidatorAttribute_t2980974044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationValidatorBase System.Configuration.RegexStringValidatorAttribute::get_ValidatorInstance()
extern "C"  ConfigurationValidatorBase_t210547623 * RegexStringValidatorAttribute_get_ValidatorInstance_m2972947178 (RegexStringValidatorAttribute_t2980974044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
