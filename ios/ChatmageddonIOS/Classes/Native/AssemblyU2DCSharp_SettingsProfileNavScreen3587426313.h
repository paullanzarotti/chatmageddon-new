﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SettingsProfileScreenList
struct SettingsProfileScreenList_t677344198;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsProfileNavScreen
struct  SettingsProfileNavScreen_t3587426313  : public NavigationScreen_t2333230110
{
public:
	// SettingsProfileScreenList SettingsProfileNavScreen::profileList
	SettingsProfileScreenList_t677344198 * ___profileList_3;
	// System.Boolean SettingsProfileNavScreen::UIclosing
	bool ___UIclosing_4;

public:
	inline static int32_t get_offset_of_profileList_3() { return static_cast<int32_t>(offsetof(SettingsProfileNavScreen_t3587426313, ___profileList_3)); }
	inline SettingsProfileScreenList_t677344198 * get_profileList_3() const { return ___profileList_3; }
	inline SettingsProfileScreenList_t677344198 ** get_address_of_profileList_3() { return &___profileList_3; }
	inline void set_profileList_3(SettingsProfileScreenList_t677344198 * value)
	{
		___profileList_3 = value;
		Il2CppCodeGenWriteBarrier(&___profileList_3, value);
	}

	inline static int32_t get_offset_of_UIclosing_4() { return static_cast<int32_t>(offsetof(SettingsProfileNavScreen_t3587426313, ___UIclosing_4)); }
	inline bool get_UIclosing_4() const { return ___UIclosing_4; }
	inline bool* get_address_of_UIclosing_4() { return &___UIclosing_4; }
	inline void set_UIclosing_4(bool value)
	{
		___UIclosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
