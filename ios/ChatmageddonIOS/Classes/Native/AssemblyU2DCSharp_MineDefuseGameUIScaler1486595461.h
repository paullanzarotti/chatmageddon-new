﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// UISprite
struct UISprite_t603616735;
// UITexture
struct UITexture_t2537039969;

#include "AssemblyU2DCSharp_ChatmageddonUIScaler3374094653.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineDefuseGameUIScaler
struct  MineDefuseGameUIScaler_t1486595461  : public ChatmageddonUIScaler_t3374094653
{
public:
	// UnityEngine.BoxCollider MineDefuseGameUIScaler::panelCollider
	BoxCollider_t22920061 * ___panelCollider_14;
	// UISprite MineDefuseGameUIScaler::background
	UISprite_t603616735 * ___background_15;
	// UISprite MineDefuseGameUIScaler::titleSeperator
	UISprite_t603616735 * ___titleSeperator_16;
	// UITexture MineDefuseGameUIScaler::mineAnim
	UITexture_t2537039969 * ___mineAnim_17;
	// UnityEngine.BoxCollider MineDefuseGameUIScaler::swipeCollider1
	BoxCollider_t22920061 * ___swipeCollider1_18;
	// UnityEngine.BoxCollider MineDefuseGameUIScaler::swipeCollider2
	BoxCollider_t22920061 * ___swipeCollider2_19;
	// UnityEngine.BoxCollider MineDefuseGameUIScaler::swipeCollider3
	BoxCollider_t22920061 * ___swipeCollider3_20;

public:
	inline static int32_t get_offset_of_panelCollider_14() { return static_cast<int32_t>(offsetof(MineDefuseGameUIScaler_t1486595461, ___panelCollider_14)); }
	inline BoxCollider_t22920061 * get_panelCollider_14() const { return ___panelCollider_14; }
	inline BoxCollider_t22920061 ** get_address_of_panelCollider_14() { return &___panelCollider_14; }
	inline void set_panelCollider_14(BoxCollider_t22920061 * value)
	{
		___panelCollider_14 = value;
		Il2CppCodeGenWriteBarrier(&___panelCollider_14, value);
	}

	inline static int32_t get_offset_of_background_15() { return static_cast<int32_t>(offsetof(MineDefuseGameUIScaler_t1486595461, ___background_15)); }
	inline UISprite_t603616735 * get_background_15() const { return ___background_15; }
	inline UISprite_t603616735 ** get_address_of_background_15() { return &___background_15; }
	inline void set_background_15(UISprite_t603616735 * value)
	{
		___background_15 = value;
		Il2CppCodeGenWriteBarrier(&___background_15, value);
	}

	inline static int32_t get_offset_of_titleSeperator_16() { return static_cast<int32_t>(offsetof(MineDefuseGameUIScaler_t1486595461, ___titleSeperator_16)); }
	inline UISprite_t603616735 * get_titleSeperator_16() const { return ___titleSeperator_16; }
	inline UISprite_t603616735 ** get_address_of_titleSeperator_16() { return &___titleSeperator_16; }
	inline void set_titleSeperator_16(UISprite_t603616735 * value)
	{
		___titleSeperator_16 = value;
		Il2CppCodeGenWriteBarrier(&___titleSeperator_16, value);
	}

	inline static int32_t get_offset_of_mineAnim_17() { return static_cast<int32_t>(offsetof(MineDefuseGameUIScaler_t1486595461, ___mineAnim_17)); }
	inline UITexture_t2537039969 * get_mineAnim_17() const { return ___mineAnim_17; }
	inline UITexture_t2537039969 ** get_address_of_mineAnim_17() { return &___mineAnim_17; }
	inline void set_mineAnim_17(UITexture_t2537039969 * value)
	{
		___mineAnim_17 = value;
		Il2CppCodeGenWriteBarrier(&___mineAnim_17, value);
	}

	inline static int32_t get_offset_of_swipeCollider1_18() { return static_cast<int32_t>(offsetof(MineDefuseGameUIScaler_t1486595461, ___swipeCollider1_18)); }
	inline BoxCollider_t22920061 * get_swipeCollider1_18() const { return ___swipeCollider1_18; }
	inline BoxCollider_t22920061 ** get_address_of_swipeCollider1_18() { return &___swipeCollider1_18; }
	inline void set_swipeCollider1_18(BoxCollider_t22920061 * value)
	{
		___swipeCollider1_18 = value;
		Il2CppCodeGenWriteBarrier(&___swipeCollider1_18, value);
	}

	inline static int32_t get_offset_of_swipeCollider2_19() { return static_cast<int32_t>(offsetof(MineDefuseGameUIScaler_t1486595461, ___swipeCollider2_19)); }
	inline BoxCollider_t22920061 * get_swipeCollider2_19() const { return ___swipeCollider2_19; }
	inline BoxCollider_t22920061 ** get_address_of_swipeCollider2_19() { return &___swipeCollider2_19; }
	inline void set_swipeCollider2_19(BoxCollider_t22920061 * value)
	{
		___swipeCollider2_19 = value;
		Il2CppCodeGenWriteBarrier(&___swipeCollider2_19, value);
	}

	inline static int32_t get_offset_of_swipeCollider3_20() { return static_cast<int32_t>(offsetof(MineDefuseGameUIScaler_t1486595461, ___swipeCollider3_20)); }
	inline BoxCollider_t22920061 * get_swipeCollider3_20() const { return ___swipeCollider3_20; }
	inline BoxCollider_t22920061 ** get_address_of_swipeCollider3_20() { return &___swipeCollider3_20; }
	inline void set_swipeCollider3_20(BoxCollider_t22920061 * value)
	{
		___swipeCollider3_20 = value;
		Il2CppCodeGenWriteBarrier(&___swipeCollider3_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
