﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackManager
struct AttackManager_t3475553125;
// System.String
struct String_t;
// Friend
struct Friend_t3555014108;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AttackListType2861798804.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Friend3555014108.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void AttackManager::.ctor()
extern "C"  void AttackManager__ctor_m2825667342 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::Awake()
extern "C"  void AttackManager_Awake_m2966055381 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::StartList(AttackListType)
extern "C"  void AttackManager_StartList_m2595340934 (AttackManager_t3475553125 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::SetListDrawn(AttackListType)
extern "C"  void AttackManager_SetListDrawn_m2058262432 (AttackManager_t3475553125 * __this, int32_t ___listType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::SearchGlobalUserBase(System.String)
extern "C"  void AttackManager_SearchGlobalUserBase_m1475989467 (AttackManager_t3475553125 * __this, String_t* ___searchTerm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::SendFriendRequest(Friend)
extern "C"  void AttackManager_SendFriendRequest_m2351950725 (AttackManager_t3475553125 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::AcceptFriendRequest(Friend)
extern "C"  void AttackManager_AcceptFriendRequest_m3030328217 (AttackManager_t3475553125 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::DeclineFriendRequest(Friend)
extern "C"  void AttackManager_DeclineFriendRequest_m1835824209 (AttackManager_t3475553125 * __this, Friend_t3555014108 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::FriendProfileUpdate()
extern "C"  void AttackManager_FriendProfileUpdate_m4052334046 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::InnerFriendUpdate()
extern "C"  void AttackManager_InnerFriendUpdate_m1031136153 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::IncomingFriendRequestUpdate()
extern "C"  void AttackManager_IncomingFriendRequestUpdate_m2795462850 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::ContactFriendAddition()
extern "C"  void AttackManager_ContactFriendAddition_m1355861958 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::OutgoingFriendRequestUpdate(System.Boolean,System.String)
extern "C"  void AttackManager_OutgoingFriendRequestUpdate_m2823455769 (AttackManager_t3475553125 * __this, bool ___accepted0, String_t* ___recieverID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::OpenPhoneNumberUI()
extern "C"  void AttackManager_OpenPhoneNumberUI_m1077732395 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::RemovePhoneNumberUI()
extern "C"  void AttackManager_RemovePhoneNumberUI_m1111979053 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::PhoneNumberSubmit()
extern "C"  void AttackManager_PhoneNumberSubmit_m3963120867 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::ContactsUpdated()
extern "C"  void AttackManager_ContactsUpdated_m34453702 (AttackManager_t3475553125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackManager::<SearchGlobalUserBase>m__0(System.Boolean,System.Collections.Hashtable,System.String)
extern "C"  void AttackManager_U3CSearchGlobalUserBaseU3Em__0_m1432439675 (AttackManager_t3475553125 * __this, bool ___success0, Hashtable_t909839986 * ___info1, String_t* ___errorCode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
