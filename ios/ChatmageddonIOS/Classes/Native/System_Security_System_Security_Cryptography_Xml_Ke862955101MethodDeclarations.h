﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.KeyInfo
struct KeyInfo_t862955101;
// System.String
struct String_t;
// System.Security.Cryptography.Xml.KeyInfoClause
struct KeyInfoClause_t279567104;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Xml.XmlElement
struct XmlElement_t2877111883;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Security_System_Security_Cryptography_Xml_Ke279567104.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.KeyInfo::.ctor()
extern "C"  void KeyInfo__ctor_m493196015 (KeyInfo_t862955101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfo::set_Id(System.String)
extern "C"  void KeyInfo_set_Id_m4244984977 (KeyInfo_t862955101 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfo::AddClause(System.Security.Cryptography.Xml.KeyInfoClause)
extern "C"  void KeyInfo_AddClause_m455609295 (KeyInfo_t862955101 * __this, KeyInfoClause_t279567104 * ___clause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Cryptography.Xml.KeyInfo::GetEnumerator()
extern "C"  Il2CppObject * KeyInfo_GetEnumerator_m2659599399 (KeyInfo_t862955101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.KeyInfo::GetXml()
extern "C"  XmlElement_t2877111883 * KeyInfo_GetXml_m2390433924 (KeyInfo_t862955101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.KeyInfo::LoadXml(System.Xml.XmlElement)
extern "C"  void KeyInfo_LoadXml_m2811617871 (KeyInfo_t862955101 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
