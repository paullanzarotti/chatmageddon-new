﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LongNotificationModal
struct LongNotificationModal_t834782082;

#include "codegen/il2cpp-codegen.h"

// System.Void LongNotificationModal::.ctor()
extern "C"  void LongNotificationModal__ctor_m552307791 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::OnEnable()
extern "C"  void LongNotificationModal_OnEnable_m2989285011 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::Awake()
extern "C"  void LongNotificationModal_Awake_m3939318318 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::OnUIActivate()
extern "C"  void LongNotificationModal_OnUIActivate_m1656354999 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::OnUIDeactivate()
extern "C"  void LongNotificationModal_OnUIDeactivate_m2934216912 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::OnFadeTweenFinished()
extern "C"  void LongNotificationModal_OnFadeTweenFinished_m3929580123 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::SetUIScene()
extern "C"  void LongNotificationModal_SetUIScene_m3298438659 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::CheckNextNotification()
extern "C"  void LongNotificationModal_CheckNextNotification_m4255222715 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::UpdateCountLabel()
extern "C"  void LongNotificationModal_UpdateCountLabel_m2445971287 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LongNotificationModal::SetUISceneSprites()
extern "C"  void LongNotificationModal_SetUISceneSprites_m232176273 (LongNotificationModal_t834782082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
