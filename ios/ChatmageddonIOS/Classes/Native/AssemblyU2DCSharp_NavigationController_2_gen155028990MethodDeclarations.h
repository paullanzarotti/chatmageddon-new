﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,SettingsNavScreen>
struct NavigationController_2_t155028990;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SettingsNavScreen2267127234.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,SettingsNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m3184207255_gshared (NavigationController_2_t155028990 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m3184207255(__this, method) ((  void (*) (NavigationController_2_t155028990 *, const MethodInfo*))NavigationController_2__ctor_m3184207255_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m402874753_gshared (NavigationController_2_t155028990 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m402874753(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t155028990 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m402874753_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m3978899914_gshared (NavigationController_2_t155028990 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m3978899914(__this, method) ((  void (*) (NavigationController_2_t155028990 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m3978899914_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m3977220309_gshared (NavigationController_2_t155028990 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m3977220309(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t155028990 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m3977220309_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,SettingsNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m1643361941_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m1643361941(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t155028990 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m1643361941_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,SettingsNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m708187624_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m708187624(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t155028990 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m708187624_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m4096629085_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m4096629085(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t155028990 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m4096629085_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m126515209_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m126515209(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t155028990 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m126515209_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,SettingsNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m1326194473_gshared (NavigationController_2_t155028990 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m1326194473(__this, ___screen0, method) ((  void (*) (NavigationController_2_t155028990 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m1326194473_gshared)(__this, ___screen0, method)
