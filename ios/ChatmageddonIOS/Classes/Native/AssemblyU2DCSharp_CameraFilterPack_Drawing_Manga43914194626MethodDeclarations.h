﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Drawing_Manga4
struct CameraFilterPack_Drawing_Manga4_t3914194626;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Drawing_Manga4::.ctor()
extern "C"  void CameraFilterPack_Drawing_Manga4__ctor_m86594651 (CameraFilterPack_Drawing_Manga4_t3914194626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga4::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Drawing_Manga4_get_material_m810079122 (CameraFilterPack_Drawing_Manga4_t3914194626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::Start()
extern "C"  void CameraFilterPack_Drawing_Manga4_Start_m3573806791 (CameraFilterPack_Drawing_Manga4_t3914194626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Drawing_Manga4_OnRenderImage_m814824111 (CameraFilterPack_Drawing_Manga4_t3914194626 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::OnValidate()
extern "C"  void CameraFilterPack_Drawing_Manga4_OnValidate_m3412171130 (CameraFilterPack_Drawing_Manga4_t3914194626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::Update()
extern "C"  void CameraFilterPack_Drawing_Manga4_Update_m3669886896 (CameraFilterPack_Drawing_Manga4_t3914194626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::OnDisable()
extern "C"  void CameraFilterPack_Drawing_Manga4_OnDisable_m1895407998 (CameraFilterPack_Drawing_Manga4_t3914194626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
