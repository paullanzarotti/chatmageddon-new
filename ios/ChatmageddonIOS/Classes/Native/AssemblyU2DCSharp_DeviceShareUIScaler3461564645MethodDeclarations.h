﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DeviceShareUIScaler
struct DeviceShareUIScaler_t3461564645;

#include "codegen/il2cpp-codegen.h"

// System.Void DeviceShareUIScaler::.ctor()
extern "C"  void DeviceShareUIScaler__ctor_m1168070042 (DeviceShareUIScaler_t3461564645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DeviceShareUIScaler::GlobalUIScale()
extern "C"  void DeviceShareUIScaler_GlobalUIScale_m2938645495 (DeviceShareUIScaler_t3461564645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
