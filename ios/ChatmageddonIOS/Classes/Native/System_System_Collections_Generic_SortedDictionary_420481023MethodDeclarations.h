﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Object>
struct SortedDictionary_2_t3346886819;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.SortedDictionary`2/Node<System.Int32,System.Object>
struct Node_t1929215751;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_420481023.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"

// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3766456554_gshared (Enumerator_t420481023 * __this, SortedDictionary_2_t3346886819 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m3766456554(__this, ___dic0, method) ((  void (*) (Enumerator_t420481023 *, SortedDictionary_2_t3346886819 *, const MethodInfo*))Enumerator__ctor_m3766456554_gshared)(__this, ___dic0, method)
// System.Collections.DictionaryEntry System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3694502715_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3694502715(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3694502715_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m917593492_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m917593492(__this, method) ((  Il2CppObject * (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m917593492_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2383712954_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2383712954(__this, method) ((  Il2CppObject * (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2383712954_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2855845382_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2855845382(__this, method) ((  Il2CppObject * (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2855845382_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2384313008_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2384313008(__this, method) ((  void (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2384313008_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m1200532472_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1200532472(__this, method) ((  KeyValuePair_2_t3749587448  (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_get_Current_m1200532472_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4279185496_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4279185496(__this, method) ((  bool (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_MoveNext_m4279185496_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3555749825_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3555749825(__this, method) ((  void (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_Dispose_m3555749825_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/Node<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentNode()
extern "C"  Node_t1929215751 * Enumerator_get_CurrentNode_m1248168108_gshared (Enumerator_t420481023 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentNode_m1248168108(__this, method) ((  Node_t1929215751 * (*) (Enumerator_t420481023 *, const MethodInfo*))Enumerator_get_CurrentNode_m1248168108_gshared)(__this, method)
