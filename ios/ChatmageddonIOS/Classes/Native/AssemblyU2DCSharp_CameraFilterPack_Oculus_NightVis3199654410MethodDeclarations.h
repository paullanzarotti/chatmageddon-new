﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Oculus_NightVision1
struct CameraFilterPack_Oculus_NightVision1_t3199654410;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Oculus_NightVision1::.ctor()
extern "C"  void CameraFilterPack_Oculus_NightVision1__ctor_m408623293 (CameraFilterPack_Oculus_NightVision1_t3199654410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision1::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Oculus_NightVision1_get_material_m2862155204 (CameraFilterPack_Oculus_NightVision1_t3199654410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::Start()
extern "C"  void CameraFilterPack_Oculus_NightVision1_Start_m615910581 (CameraFilterPack_Oculus_NightVision1_t3199654410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Oculus_NightVision1_OnRenderImage_m3064081085 (CameraFilterPack_Oculus_NightVision1_t3199654410 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::Update()
extern "C"  void CameraFilterPack_Oculus_NightVision1_Update_m422629324 (CameraFilterPack_Oculus_NightVision1_t3199654410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::OnDisable()
extern "C"  void CameraFilterPack_Oculus_NightVision1_OnDisable_m2760157234 (CameraFilterPack_Oculus_NightVision1_t3199654410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
