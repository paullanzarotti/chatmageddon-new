﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.AreaCodeMap>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m803363071(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2902883856 *, String_t*, AreaCodeMap_t3230759372 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.AreaCodeMap>::get_Key()
#define KeyValuePair_2_get_Key_m2290572545(__this, method) ((  String_t* (*) (KeyValuePair_2_t2902883856 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.AreaCodeMap>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1792893290(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2902883856 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.AreaCodeMap>::get_Value()
#define KeyValuePair_2_get_Value_m2530934921(__this, method) ((  AreaCodeMap_t3230759372 * (*) (KeyValuePair_2_t2902883856 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.AreaCodeMap>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m867891778(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2902883856 *, AreaCodeMap_t3230759372 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,PhoneNumbers.AreaCodeMap>::ToString()
#define KeyValuePair_2_ToString_m2971025836(__this, method) ((  String_t* (*) (KeyValuePair_2_t2902883856 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
