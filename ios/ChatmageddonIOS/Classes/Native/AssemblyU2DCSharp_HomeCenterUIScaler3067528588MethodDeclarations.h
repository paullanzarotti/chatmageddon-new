﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HomeCenterUIScaler
struct HomeCenterUIScaler_t3067528588;

#include "codegen/il2cpp-codegen.h"

// System.Void HomeCenterUIScaler::.ctor()
extern "C"  void HomeCenterUIScaler__ctor_m2540857941 (HomeCenterUIScaler_t3067528588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HomeCenterUIScaler::GlobalUIScale()
extern "C"  void HomeCenterUIScaler_GlobalUIScale_m1886936062 (HomeCenterUIScaler_t3067528588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
