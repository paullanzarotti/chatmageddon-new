﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fabric.Crashlytics.Internal.IOSImpl
struct IOSImpl_t2874644713;
// System.String
struct String_t;
// System.Diagnostics.StackTrace
struct StackTrace_t2500644597;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Diagnostics_StackTrace2500644597.h"

// System.Void Fabric.Crashlytics.Internal.IOSImpl::.ctor()
extern "C"  void IOSImpl__ctor_m402727725 (IOSImpl_t2874644713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLUCrash()
extern "C"  void IOSImpl_CLUCrash_m3441073310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLUSetDebugMode(System.Int32)
extern "C"  void IOSImpl_CLUSetDebugMode_m388311232 (Il2CppObject * __this /* static, unused */, int32_t ___debugMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLULog(System.String)
extern "C"  void IOSImpl_CLULog_m2901338093 (Il2CppObject * __this /* static, unused */, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLUSetKeyValue(System.String,System.String)
extern "C"  void IOSImpl_CLUSetKeyValue_m1699926581 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLUSetUserIdentifier(System.String)
extern "C"  void IOSImpl_CLUSetUserIdentifier_m2495209373 (Il2CppObject * __this /* static, unused */, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLUSetUserEmail(System.String)
extern "C"  void IOSImpl_CLUSetUserEmail_m1725182298 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLUSetUserName(System.String)
extern "C"  void IOSImpl_CLUSetUserName_m3512341881 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::CLURecordCustomException(System.String,System.String,System.String)
extern "C"  void IOSImpl_CLURecordCustomException_m3927844160 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___reason1, String_t* ___stackTrace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::SetDebugMode(System.Boolean)
extern "C"  void IOSImpl_SetDebugMode_m3712845934 (IOSImpl_t2874644713 * __this, bool ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::Crash()
extern "C"  void IOSImpl_Crash_m1357284704 (IOSImpl_t2874644713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::Log(System.String)
extern "C"  void IOSImpl_Log_m1172680539 (IOSImpl_t2874644713 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::SetKeyValue(System.String,System.String)
extern "C"  void IOSImpl_SetKeyValue_m4195877255 (IOSImpl_t2874644713 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::SetUserIdentifier(System.String)
extern "C"  void IOSImpl_SetUserIdentifier_m4063531515 (IOSImpl_t2874644713 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::SetUserEmail(System.String)
extern "C"  void IOSImpl_SetUserEmail_m4169963668 (IOSImpl_t2874644713 * __this, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::SetUserName(System.String)
extern "C"  void IOSImpl_SetUserName_m2112603967 (IOSImpl_t2874644713 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::RecordCustomException(System.String,System.String,System.Diagnostics.StackTrace)
extern "C"  void IOSImpl_RecordCustomException_m2581659228 (IOSImpl_t2874644713 * __this, String_t* ___name0, String_t* ___reason1, StackTrace_t2500644597 * ___stackTrace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fabric.Crashlytics.Internal.IOSImpl::RecordCustomException(System.String,System.String,System.String)
extern "C"  void IOSImpl_RecordCustomException_m4052253638 (IOSImpl_t2874644713 * __this, String_t* ___name0, String_t* ___reason1, String_t* ___stackTraceString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
