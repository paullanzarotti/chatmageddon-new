﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ChatMessageILP
struct ChatMessageILP_t4258019024;
// ChatMessage
struct ChatMessage_t2384228687;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// UISprite
struct UISprite_t603616735;
// UIScrollView
struct UIScrollView_t3033954930;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "AssemblyU2DCSharp_BaseInfiniteListItem_1_gen3200590574.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatMessageListItem
struct  ChatMessageListItem_t2425051210  : public BaseInfiniteListItem_1_t3200590574
{
public:
	// System.Boolean ChatMessageListItem::friendItem
	bool ___friendItem_10;
	// ChatMessageILP ChatMessageListItem::listPopulator
	ChatMessageILP_t4258019024 * ___listPopulator_11;
	// ChatMessage ChatMessageListItem::itemMessage
	ChatMessage_t2384228687 * ___itemMessage_12;
	// UnityEngine.GameObject ChatMessageListItem::friendUser
	GameObject_t1756533147 * ___friendUser_13;
	// UILabel ChatMessageListItem::friendUserLabel
	UILabel_t1795115428 * ___friendUserLabel_14;
	// UISprite ChatMessageListItem::friendUserUnderline
	UISprite_t603616735 * ___friendUserUnderline_15;
	// UILabel ChatMessageListItem::messageLabel
	UILabel_t1795115428 * ___messageLabel_16;
	// UILabel ChatMessageListItem::timeLabel
	UILabel_t1795115428 * ___timeLabel_17;
	// UILabel ChatMessageListItem::dateLabel
	UILabel_t1795115428 * ___dateLabel_18;
	// UIScrollView ChatMessageListItem::scrollView
	UIScrollView_t3033954930 * ___scrollView_19;
	// UIScrollView ChatMessageListItem::draggablePanel
	UIScrollView_t3033954930 * ___draggablePanel_20;
	// UnityEngine.Transform ChatMessageListItem::mTrans
	Transform_t3275118058 * ___mTrans_21;
	// UIScrollView ChatMessageListItem::mScroll
	UIScrollView_t3033954930 * ___mScroll_22;
	// System.Boolean ChatMessageListItem::mAutoFind
	bool ___mAutoFind_23;
	// System.Boolean ChatMessageListItem::mStarted
	bool ___mStarted_24;

public:
	inline static int32_t get_offset_of_friendItem_10() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___friendItem_10)); }
	inline bool get_friendItem_10() const { return ___friendItem_10; }
	inline bool* get_address_of_friendItem_10() { return &___friendItem_10; }
	inline void set_friendItem_10(bool value)
	{
		___friendItem_10 = value;
	}

	inline static int32_t get_offset_of_listPopulator_11() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___listPopulator_11)); }
	inline ChatMessageILP_t4258019024 * get_listPopulator_11() const { return ___listPopulator_11; }
	inline ChatMessageILP_t4258019024 ** get_address_of_listPopulator_11() { return &___listPopulator_11; }
	inline void set_listPopulator_11(ChatMessageILP_t4258019024 * value)
	{
		___listPopulator_11 = value;
		Il2CppCodeGenWriteBarrier(&___listPopulator_11, value);
	}

	inline static int32_t get_offset_of_itemMessage_12() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___itemMessage_12)); }
	inline ChatMessage_t2384228687 * get_itemMessage_12() const { return ___itemMessage_12; }
	inline ChatMessage_t2384228687 ** get_address_of_itemMessage_12() { return &___itemMessage_12; }
	inline void set_itemMessage_12(ChatMessage_t2384228687 * value)
	{
		___itemMessage_12 = value;
		Il2CppCodeGenWriteBarrier(&___itemMessage_12, value);
	}

	inline static int32_t get_offset_of_friendUser_13() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___friendUser_13)); }
	inline GameObject_t1756533147 * get_friendUser_13() const { return ___friendUser_13; }
	inline GameObject_t1756533147 ** get_address_of_friendUser_13() { return &___friendUser_13; }
	inline void set_friendUser_13(GameObject_t1756533147 * value)
	{
		___friendUser_13 = value;
		Il2CppCodeGenWriteBarrier(&___friendUser_13, value);
	}

	inline static int32_t get_offset_of_friendUserLabel_14() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___friendUserLabel_14)); }
	inline UILabel_t1795115428 * get_friendUserLabel_14() const { return ___friendUserLabel_14; }
	inline UILabel_t1795115428 ** get_address_of_friendUserLabel_14() { return &___friendUserLabel_14; }
	inline void set_friendUserLabel_14(UILabel_t1795115428 * value)
	{
		___friendUserLabel_14 = value;
		Il2CppCodeGenWriteBarrier(&___friendUserLabel_14, value);
	}

	inline static int32_t get_offset_of_friendUserUnderline_15() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___friendUserUnderline_15)); }
	inline UISprite_t603616735 * get_friendUserUnderline_15() const { return ___friendUserUnderline_15; }
	inline UISprite_t603616735 ** get_address_of_friendUserUnderline_15() { return &___friendUserUnderline_15; }
	inline void set_friendUserUnderline_15(UISprite_t603616735 * value)
	{
		___friendUserUnderline_15 = value;
		Il2CppCodeGenWriteBarrier(&___friendUserUnderline_15, value);
	}

	inline static int32_t get_offset_of_messageLabel_16() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___messageLabel_16)); }
	inline UILabel_t1795115428 * get_messageLabel_16() const { return ___messageLabel_16; }
	inline UILabel_t1795115428 ** get_address_of_messageLabel_16() { return &___messageLabel_16; }
	inline void set_messageLabel_16(UILabel_t1795115428 * value)
	{
		___messageLabel_16 = value;
		Il2CppCodeGenWriteBarrier(&___messageLabel_16, value);
	}

	inline static int32_t get_offset_of_timeLabel_17() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___timeLabel_17)); }
	inline UILabel_t1795115428 * get_timeLabel_17() const { return ___timeLabel_17; }
	inline UILabel_t1795115428 ** get_address_of_timeLabel_17() { return &___timeLabel_17; }
	inline void set_timeLabel_17(UILabel_t1795115428 * value)
	{
		___timeLabel_17 = value;
		Il2CppCodeGenWriteBarrier(&___timeLabel_17, value);
	}

	inline static int32_t get_offset_of_dateLabel_18() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___dateLabel_18)); }
	inline UILabel_t1795115428 * get_dateLabel_18() const { return ___dateLabel_18; }
	inline UILabel_t1795115428 ** get_address_of_dateLabel_18() { return &___dateLabel_18; }
	inline void set_dateLabel_18(UILabel_t1795115428 * value)
	{
		___dateLabel_18 = value;
		Il2CppCodeGenWriteBarrier(&___dateLabel_18, value);
	}

	inline static int32_t get_offset_of_scrollView_19() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___scrollView_19)); }
	inline UIScrollView_t3033954930 * get_scrollView_19() const { return ___scrollView_19; }
	inline UIScrollView_t3033954930 ** get_address_of_scrollView_19() { return &___scrollView_19; }
	inline void set_scrollView_19(UIScrollView_t3033954930 * value)
	{
		___scrollView_19 = value;
		Il2CppCodeGenWriteBarrier(&___scrollView_19, value);
	}

	inline static int32_t get_offset_of_draggablePanel_20() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___draggablePanel_20)); }
	inline UIScrollView_t3033954930 * get_draggablePanel_20() const { return ___draggablePanel_20; }
	inline UIScrollView_t3033954930 ** get_address_of_draggablePanel_20() { return &___draggablePanel_20; }
	inline void set_draggablePanel_20(UIScrollView_t3033954930 * value)
	{
		___draggablePanel_20 = value;
		Il2CppCodeGenWriteBarrier(&___draggablePanel_20, value);
	}

	inline static int32_t get_offset_of_mTrans_21() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___mTrans_21)); }
	inline Transform_t3275118058 * get_mTrans_21() const { return ___mTrans_21; }
	inline Transform_t3275118058 ** get_address_of_mTrans_21() { return &___mTrans_21; }
	inline void set_mTrans_21(Transform_t3275118058 * value)
	{
		___mTrans_21 = value;
		Il2CppCodeGenWriteBarrier(&___mTrans_21, value);
	}

	inline static int32_t get_offset_of_mScroll_22() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___mScroll_22)); }
	inline UIScrollView_t3033954930 * get_mScroll_22() const { return ___mScroll_22; }
	inline UIScrollView_t3033954930 ** get_address_of_mScroll_22() { return &___mScroll_22; }
	inline void set_mScroll_22(UIScrollView_t3033954930 * value)
	{
		___mScroll_22 = value;
		Il2CppCodeGenWriteBarrier(&___mScroll_22, value);
	}

	inline static int32_t get_offset_of_mAutoFind_23() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___mAutoFind_23)); }
	inline bool get_mAutoFind_23() const { return ___mAutoFind_23; }
	inline bool* get_address_of_mAutoFind_23() { return &___mAutoFind_23; }
	inline void set_mAutoFind_23(bool value)
	{
		___mAutoFind_23 = value;
	}

	inline static int32_t get_offset_of_mStarted_24() { return static_cast<int32_t>(offsetof(ChatMessageListItem_t2425051210, ___mStarted_24)); }
	inline bool get_mStarted_24() const { return ___mStarted_24; }
	inline bool* get_address_of_mStarted_24() { return &___mStarted_24; }
	inline void set_mStarted_24(bool value)
	{
		___mStarted_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
