﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoginSuccessSceneManager/<ContactWait>c__Iterator0
struct U3CContactWaitU3Ec__Iterator0_t3024659562;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LoginSuccessSceneManager/<ContactWait>c__Iterator0::.ctor()
extern "C"  void U3CContactWaitU3Ec__Iterator0__ctor_m3222860573 (U3CContactWaitU3Ec__Iterator0_t3024659562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoginSuccessSceneManager/<ContactWait>c__Iterator0::MoveNext()
extern "C"  bool U3CContactWaitU3Ec__Iterator0_MoveNext_m3202446355 (U3CContactWaitU3Ec__Iterator0_t3024659562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoginSuccessSceneManager/<ContactWait>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CContactWaitU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3686357331 (U3CContactWaitU3Ec__Iterator0_t3024659562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoginSuccessSceneManager/<ContactWait>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CContactWaitU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3469456331 (U3CContactWaitU3Ec__Iterator0_t3024659562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager/<ContactWait>c__Iterator0::Dispose()
extern "C"  void U3CContactWaitU3Ec__Iterator0_Dispose_m317854676 (U3CContactWaitU3Ec__Iterator0_t3024659562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoginSuccessSceneManager/<ContactWait>c__Iterator0::Reset()
extern "C"  void U3CContactWaitU3Ec__Iterator0_Reset_m2430970782 (U3CContactWaitU3Ec__Iterator0_t3024659562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
