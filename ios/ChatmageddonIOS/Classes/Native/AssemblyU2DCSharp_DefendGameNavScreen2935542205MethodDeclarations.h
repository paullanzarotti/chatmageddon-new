﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefendGameNavScreen
struct DefendGameNavScreen_t2935542205;
// LaunchedMissile
struct LaunchedMissile_t1542515810;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "AssemblyU2DCSharp_LaunchedMissile1542515810.h"

// System.Void DefendGameNavScreen::.ctor()
extern "C"  void DefendGameNavScreen__ctor_m2014374874 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::OnEnable()
extern "C"  void DefendGameNavScreen_OnEnable_m2321695714 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::OnDisable()
extern "C"  void DefendGameNavScreen_OnDisable_m871827169 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::Start()
extern "C"  void DefendGameNavScreen_Start_m1786735426 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::UIClosing()
extern "C"  void DefendGameNavScreen_UIClosing_m1209530855 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void DefendGameNavScreen_ScreenLoad_m3069594206 (DefendGameNavScreen_t2935542205 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void DefendGameNavScreen_ScreenUnload_m1342701870 (DefendGameNavScreen_t2935542205 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::PopulateTopUI()
extern "C"  void DefendGameNavScreen_PopulateTopUI_m2178354273 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::CalculatorUpdated(System.TimeSpan)
extern "C"  void DefendGameNavScreen_CalculatorUpdated_m162371307 (DefendGameNavScreen_t2935542205 * __this, TimeSpan_t3430258949  ___difference0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::SetColour()
extern "C"  void DefendGameNavScreen_SetColour_m3120466266 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::CalculatorHitZero()
extern "C"  void DefendGameNavScreen_CalculatorHitZero_m716638271 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::DisplayMissileDefendFailedToast(LaunchedMissile)
extern "C"  void DefendGameNavScreen_DisplayMissileDefendFailedToast_m142486508 (DefendGameNavScreen_t2935542205 * __this, LaunchedMissile_t1542515810 * ___missile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::PopulateGameUI()
extern "C"  void DefendGameNavScreen_PopulateGameUI_m203798582 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::StartMiniGame()
extern "C"  void DefendGameNavScreen_StartMiniGame_m1251702141 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::StopMiniGame()
extern "C"  void DefendGameNavScreen_StopMiniGame_m3153714057 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefendGameNavScreen::ModalClosing()
extern "C"  void DefendGameNavScreen_ModalClosing_m3036899176 (DefendGameNavScreen_t2935542205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
