﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.Xml.EncryptedData
struct EncryptedData_t1564008194;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"

// System.Void System.Security.Cryptography.Xml.EncryptedData::.ctor()
extern "C"  void EncryptedData__ctor_m4008134596 (EncryptedData_t1564008194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptedData::GetXml()
extern "C"  XmlElement_t2877111883 * EncryptedData_GetXml_m8490607 (EncryptedData_t1564008194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlElement System.Security.Cryptography.Xml.EncryptedData::GetXml(System.Xml.XmlDocument)
extern "C"  XmlElement_t2877111883 * EncryptedData_GetXml_m870161341 (EncryptedData_t1564008194 * __this, XmlDocument_t3649534162 * ___document0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.Xml.EncryptedData::LoadXml(System.Xml.XmlElement)
extern "C"  void EncryptedData_LoadXml_m626120220 (EncryptedData_t1564008194 * __this, XmlElement_t2877111883 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
