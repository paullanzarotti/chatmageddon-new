﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CacheTilesExample
struct CacheTilesExample_t3121978397;
// System.String
struct String_t;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"

// System.Void CacheTilesExample::.ctor()
extern "C"  void CacheTilesExample__ctor_m2030710966 (CacheTilesExample_t3121978397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CacheTilesExample::GetTilePath(OnlineMapsTile)
extern "C"  String_t* CacheTilesExample_GetTilePath_m2979308056 (Il2CppObject * __this /* static, unused */, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CacheTilesExample::OnStartDownloadTile(OnlineMapsTile)
extern "C"  void CacheTilesExample_OnStartDownloadTile_m3829534357 (CacheTilesExample_t3121978397 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CacheTilesExample::OnTileDownloaded(OnlineMapsTile)
extern "C"  void CacheTilesExample_OnTileDownloaded_m1018733648 (CacheTilesExample_t3121978397 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CacheTilesExample::Start()
extern "C"  void CacheTilesExample_Start_m1054219918 (CacheTilesExample_t3121978397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
