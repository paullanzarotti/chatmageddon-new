﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnlineMapsControlBase
struct OnlineMapsControlBase_t473237564;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1
struct  U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301  : public Il2CppObject
{
public:
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<pointX>__0
	double ___U3CpointXU3E__0_0;
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<pointY>__1
	double ___U3CpointYU3E__1_1;
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::distanceX
	double ___distanceX_2;
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::distanceY
	double ___distanceY_3;
	// System.Single OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<totalTime>__2
	float ___U3CtotalTimeU3E__2_4;
	// System.Single OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::durationTime
	float ___durationTime_5;
	// System.Single OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<percentageIncrease>__3
	float ___U3CpercentageIncreaseU3E__3_6;
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<offsetX>__4
	double ___U3CoffsetXU3E__4_7;
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<offsetY>__5
	double ___U3CoffsetYU3E__5_8;
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<px>__6
	double ___U3CpxU3E__6_9;
	// System.Double OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::<py>__7
	double ___U3CpyU3E__7_10;
	// OnlineMapsControlBase OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::$this
	OnlineMapsControlBase_t473237564 * ___U24this_11;
	// System.Object OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::$current
	Il2CppObject * ___U24current_12;
	// System.Boolean OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::$disposing
	bool ___U24disposing_13;
	// System.Int32 OnlineMapsControlBase/<MoveToPositionWithConstantVelocity>c__Iterator1::$PC
	int32_t ___U24PC_14;

public:
	inline static int32_t get_offset_of_U3CpointXU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CpointXU3E__0_0)); }
	inline double get_U3CpointXU3E__0_0() const { return ___U3CpointXU3E__0_0; }
	inline double* get_address_of_U3CpointXU3E__0_0() { return &___U3CpointXU3E__0_0; }
	inline void set_U3CpointXU3E__0_0(double value)
	{
		___U3CpointXU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpointYU3E__1_1() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CpointYU3E__1_1)); }
	inline double get_U3CpointYU3E__1_1() const { return ___U3CpointYU3E__1_1; }
	inline double* get_address_of_U3CpointYU3E__1_1() { return &___U3CpointYU3E__1_1; }
	inline void set_U3CpointYU3E__1_1(double value)
	{
		___U3CpointYU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_distanceX_2() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___distanceX_2)); }
	inline double get_distanceX_2() const { return ___distanceX_2; }
	inline double* get_address_of_distanceX_2() { return &___distanceX_2; }
	inline void set_distanceX_2(double value)
	{
		___distanceX_2 = value;
	}

	inline static int32_t get_offset_of_distanceY_3() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___distanceY_3)); }
	inline double get_distanceY_3() const { return ___distanceY_3; }
	inline double* get_address_of_distanceY_3() { return &___distanceY_3; }
	inline void set_distanceY_3(double value)
	{
		___distanceY_3 = value;
	}

	inline static int32_t get_offset_of_U3CtotalTimeU3E__2_4() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CtotalTimeU3E__2_4)); }
	inline float get_U3CtotalTimeU3E__2_4() const { return ___U3CtotalTimeU3E__2_4; }
	inline float* get_address_of_U3CtotalTimeU3E__2_4() { return &___U3CtotalTimeU3E__2_4; }
	inline void set_U3CtotalTimeU3E__2_4(float value)
	{
		___U3CtotalTimeU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_durationTime_5() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___durationTime_5)); }
	inline float get_durationTime_5() const { return ___durationTime_5; }
	inline float* get_address_of_durationTime_5() { return &___durationTime_5; }
	inline void set_durationTime_5(float value)
	{
		___durationTime_5 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageIncreaseU3E__3_6() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CpercentageIncreaseU3E__3_6)); }
	inline float get_U3CpercentageIncreaseU3E__3_6() const { return ___U3CpercentageIncreaseU3E__3_6; }
	inline float* get_address_of_U3CpercentageIncreaseU3E__3_6() { return &___U3CpercentageIncreaseU3E__3_6; }
	inline void set_U3CpercentageIncreaseU3E__3_6(float value)
	{
		___U3CpercentageIncreaseU3E__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CoffsetXU3E__4_7() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CoffsetXU3E__4_7)); }
	inline double get_U3CoffsetXU3E__4_7() const { return ___U3CoffsetXU3E__4_7; }
	inline double* get_address_of_U3CoffsetXU3E__4_7() { return &___U3CoffsetXU3E__4_7; }
	inline void set_U3CoffsetXU3E__4_7(double value)
	{
		___U3CoffsetXU3E__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CoffsetYU3E__5_8() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CoffsetYU3E__5_8)); }
	inline double get_U3CoffsetYU3E__5_8() const { return ___U3CoffsetYU3E__5_8; }
	inline double* get_address_of_U3CoffsetYU3E__5_8() { return &___U3CoffsetYU3E__5_8; }
	inline void set_U3CoffsetYU3E__5_8(double value)
	{
		___U3CoffsetYU3E__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CpxU3E__6_9() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CpxU3E__6_9)); }
	inline double get_U3CpxU3E__6_9() const { return ___U3CpxU3E__6_9; }
	inline double* get_address_of_U3CpxU3E__6_9() { return &___U3CpxU3E__6_9; }
	inline void set_U3CpxU3E__6_9(double value)
	{
		___U3CpxU3E__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CpyU3E__7_10() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U3CpyU3E__7_10)); }
	inline double get_U3CpyU3E__7_10() const { return ___U3CpyU3E__7_10; }
	inline double* get_address_of_U3CpyU3E__7_10() { return &___U3CpyU3E__7_10; }
	inline void set_U3CpyU3E__7_10(double value)
	{
		___U3CpyU3E__7_10 = value;
	}

	inline static int32_t get_offset_of_U24this_11() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U24this_11)); }
	inline OnlineMapsControlBase_t473237564 * get_U24this_11() const { return ___U24this_11; }
	inline OnlineMapsControlBase_t473237564 ** get_address_of_U24this_11() { return &___U24this_11; }
	inline void set_U24this_11(OnlineMapsControlBase_t473237564 * value)
	{
		___U24this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_11, value);
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U24disposing_13() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U24disposing_13)); }
	inline bool get_U24disposing_13() const { return ___U24disposing_13; }
	inline bool* get_address_of_U24disposing_13() { return &___U24disposing_13; }
	inline void set_U24disposing_13(bool value)
	{
		___U24disposing_13 = value;
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CMoveToPositionWithConstantVelocityU3Ec__Iterator1_t1503452301, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
