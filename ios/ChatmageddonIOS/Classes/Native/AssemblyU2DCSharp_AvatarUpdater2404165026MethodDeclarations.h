﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AvatarUpdater
struct AvatarUpdater_t2404165026;
// User
struct User_t719925459;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_User719925459.h"

// System.Void AvatarUpdater::.ctor()
extern "C"  void AvatarUpdater__ctor_m3410125653 (AvatarUpdater_t2404165026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::Awake()
extern "C"  void AvatarUpdater_Awake_m3517745038 (AvatarUpdater_t2404165026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::UpdateAvatar(User)
extern "C"  void AvatarUpdater_UpdateAvatar_m3974128416 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::UpdateUserAvatar(User)
extern "C"  void AvatarUpdater_UpdateUserAvatar_m3319627601 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::SetAsInitials(User)
extern "C"  void AvatarUpdater_SetAsInitials_m2628905981 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AvatarUpdater::CheckAvatarLoaded(User)
extern "C"  Il2CppObject * AvatarUpdater_CheckAvatarLoaded_m357756720 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::SetAsProfilePicture(User)
extern "C"  void AvatarUpdater_SetAsProfilePicture_m3563140371 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::CheckNemesis(User)
extern "C"  void AvatarUpdater_CheckNemesis_m1023585996 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::SetNemesis(System.Boolean)
extern "C"  void AvatarUpdater_SetNemesis_m1128278460 (AvatarUpdater_t2404165026 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::CheckFirstStrike(User)
extern "C"  void AvatarUpdater_CheckFirstStrike_m2906530580 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::SetChatFriend(User)
extern "C"  void AvatarUpdater_SetChatFriend_m4246073982 (AvatarUpdater_t2404165026 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarUpdater::CheckForNewChatMessage()
extern "C"  void AvatarUpdater_CheckForNewChatMessage_m1141872981 (AvatarUpdater_t2404165026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
