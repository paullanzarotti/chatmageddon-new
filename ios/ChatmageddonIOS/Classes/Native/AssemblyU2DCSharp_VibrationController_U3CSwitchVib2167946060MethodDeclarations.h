﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VibrationController/<SwitchVibrate>c__Iterator3
struct U3CSwitchVibrateU3Ec__Iterator3_t2167946060;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void VibrationController/<SwitchVibrate>c__Iterator3::.ctor()
extern "C"  void U3CSwitchVibrateU3Ec__Iterator3__ctor_m573071091 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VibrationController/<SwitchVibrate>c__Iterator3::MoveNext()
extern "C"  bool U3CSwitchVibrateU3Ec__Iterator3_MoveNext_m2197227717 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<SwitchVibrate>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSwitchVibrateU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1698350205 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VibrationController/<SwitchVibrate>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSwitchVibrateU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1779214549 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<SwitchVibrate>c__Iterator3::Dispose()
extern "C"  void U3CSwitchVibrateU3Ec__Iterator3_Dispose_m1190319694 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationController/<SwitchVibrate>c__Iterator3::Reset()
extern "C"  void U3CSwitchVibrateU3Ec__Iterator3_Reset_m2081466768 (U3CSwitchVibrateU3Ec__Iterator3_t2167946060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
