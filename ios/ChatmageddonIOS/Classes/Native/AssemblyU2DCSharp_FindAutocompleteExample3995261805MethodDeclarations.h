﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FindAutocompleteExample
struct FindAutocompleteExample_t3995261805;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FindAutocompleteExample::.ctor()
extern "C"  void FindAutocompleteExample__ctor_m1786130066 (FindAutocompleteExample_t3995261805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FindAutocompleteExample::Start()
extern "C"  void FindAutocompleteExample_Start_m2547887474 (FindAutocompleteExample_t3995261805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FindAutocompleteExample::OnComplete(System.String)
extern "C"  void FindAutocompleteExample_OnComplete_m4270047292 (FindAutocompleteExample_t3995261805 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
