﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharpU2Dfirstpass_Prime31_FacebookBaseDT79607460.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.FacebookResultsPaging
struct  FacebookResultsPaging_t3277319338  : public FacebookBaseDTO_t79607460
{
public:
	// System.String Prime31.FacebookResultsPaging::next
	String_t* ___next_0;
	// System.String Prime31.FacebookResultsPaging::previous
	String_t* ___previous_1;

public:
	inline static int32_t get_offset_of_next_0() { return static_cast<int32_t>(offsetof(FacebookResultsPaging_t3277319338, ___next_0)); }
	inline String_t* get_next_0() const { return ___next_0; }
	inline String_t** get_address_of_next_0() { return &___next_0; }
	inline void set_next_0(String_t* value)
	{
		___next_0 = value;
		Il2CppCodeGenWriteBarrier(&___next_0, value);
	}

	inline static int32_t get_offset_of_previous_1() { return static_cast<int32_t>(offsetof(FacebookResultsPaging_t3277319338, ___previous_1)); }
	inline String_t* get_previous_1() const { return ___previous_1; }
	inline String_t** get_address_of_previous_1() { return &___previous_1; }
	inline void set_previous_1(String_t* value)
	{
		___previous_1 = value;
		Il2CppCodeGenWriteBarrier(&___previous_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
