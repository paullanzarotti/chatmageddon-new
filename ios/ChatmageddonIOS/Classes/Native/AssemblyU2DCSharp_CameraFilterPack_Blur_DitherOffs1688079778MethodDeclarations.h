﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_DitherOffset
struct CameraFilterPack_Blur_DitherOffset_t1688079778;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_DitherOffset::.ctor()
extern "C"  void CameraFilterPack_Blur_DitherOffset__ctor_m297422981 (CameraFilterPack_Blur_DitherOffset_t1688079778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_DitherOffset::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_DitherOffset_get_material_m3674109676 (CameraFilterPack_Blur_DitherOffset_t1688079778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::Start()
extern "C"  void CameraFilterPack_Blur_DitherOffset_Start_m3764813837 (CameraFilterPack_Blur_DitherOffset_t1688079778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_DitherOffset_OnRenderImage_m3608092533 (CameraFilterPack_Blur_DitherOffset_t1688079778 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::OnValidate()
extern "C"  void CameraFilterPack_Blur_DitherOffset_OnValidate_m2934491638 (CameraFilterPack_Blur_DitherOffset_t1688079778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::Update()
extern "C"  void CameraFilterPack_Blur_DitherOffset_Update_m2487059460 (CameraFilterPack_Blur_DitherOffset_t1688079778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::OnDisable()
extern "C"  void CameraFilterPack_Blur_DitherOffset_OnDisable_m2501102458 (CameraFilterPack_Blur_DitherOffset_t1688079778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
