﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FireGuage
struct FireGuage_t4001636259;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void FireGuage::.ctor()
extern "C"  void FireGuage__ctor_m573030854 (FireGuage_t4001636259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FireGuage::StartFireGuage()
extern "C"  void FireGuage_StartFireGuage_m2959054407 (FireGuage_t4001636259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FireGuage::StopFireGuage()
extern "C"  void FireGuage_StopFireGuage_m3316255543 (FireGuage_t4001636259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FireGuage::ResetFireGuage()
extern "C"  void FireGuage_ResetFireGuage_m3768575142 (FireGuage_t4001636259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FireGuage::MoveFireGuage()
extern "C"  Il2CppObject * FireGuage_MoveFireGuage_m3072146062 (FireGuage_t4001636259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FireGuage::GetGuagePercent()
extern "C"  float FireGuage_GetGuagePercent_m39026244 (FireGuage_t4001636259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FireGuage::SetGuageLevelSpeed()
extern "C"  void FireGuage_SetGuageLevelSpeed_m2891077934 (FireGuage_t4001636259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
