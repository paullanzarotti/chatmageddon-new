﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CountryCodeData
struct  CountryCodeData_t1765037603  : public Il2CppObject
{
public:
	// System.String CountryCodeData::countryName
	String_t* ___countryName_0;
	// System.String CountryCodeData::countryShortName
	String_t* ___countryShortName_1;
	// System.String CountryCodeData::countryCode
	String_t* ___countryCode_2;

public:
	inline static int32_t get_offset_of_countryName_0() { return static_cast<int32_t>(offsetof(CountryCodeData_t1765037603, ___countryName_0)); }
	inline String_t* get_countryName_0() const { return ___countryName_0; }
	inline String_t** get_address_of_countryName_0() { return &___countryName_0; }
	inline void set_countryName_0(String_t* value)
	{
		___countryName_0 = value;
		Il2CppCodeGenWriteBarrier(&___countryName_0, value);
	}

	inline static int32_t get_offset_of_countryShortName_1() { return static_cast<int32_t>(offsetof(CountryCodeData_t1765037603, ___countryShortName_1)); }
	inline String_t* get_countryShortName_1() const { return ___countryShortName_1; }
	inline String_t** get_address_of_countryShortName_1() { return &___countryShortName_1; }
	inline void set_countryShortName_1(String_t* value)
	{
		___countryShortName_1 = value;
		Il2CppCodeGenWriteBarrier(&___countryShortName_1, value);
	}

	inline static int32_t get_offset_of_countryCode_2() { return static_cast<int32_t>(offsetof(CountryCodeData_t1765037603, ___countryCode_2)); }
	inline String_t* get_countryCode_2() const { return ___countryCode_2; }
	inline String_t** get_address_of_countryCode_2() { return &___countryCode_2; }
	inline void set_countryCode_2(String_t* value)
	{
		___countryCode_2 = value;
		Il2CppCodeGenWriteBarrier(&___countryCode_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
