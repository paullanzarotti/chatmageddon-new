﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryController
struct FlurryController_t2385863972;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlurryEvent3996959420.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void FlurryController::.ctor()
extern "C"  void FlurryController__ctor_m2407934461 (FlurryController_t2385863972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryController::StartFlurrySession()
extern "C"  void FlurryController_StartFlurrySession_m2162821549 (FlurryController_t2385863972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryController::SendEvent(FlurryEvent,System.Boolean)
extern "C"  void FlurryController_SendEvent_m2773676096 (FlurryController_t2385863972 * __this, int32_t ___eventName0, bool ___timedEvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryController::SendEvent(System.String)
extern "C"  void FlurryController_SendEvent_m3837079907 (FlurryController_t2385863972 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryController::SendEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FlurryController_SendEvent_m1111498304 (FlurryController_t2385863972 * __this, String_t* ___eventName0, Dictionary_2_t3943999495 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryController::GKIMLogs(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void FlurryController_GKIMLogs_m163716539 (FlurryController_t2385863972 * __this, String_t* ___eventName0, Dictionary_2_t3943999495 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryController::<GKIMLogs>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void FlurryController_U3CGKIMLogsU3Em__0_m3516784827 (Il2CppObject * __this /* static, unused */, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
