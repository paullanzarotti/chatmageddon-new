﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhoneNumbers.RegexCache
struct RegexCache_t1271678307;
// PhoneNumbers.PhoneRegex
struct PhoneRegex_t3216508019;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PhoneNumbers.RegexCache::.ctor(System.Int32)
extern "C"  void RegexCache__ctor_m2288487425 (RegexCache_t1271678307 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PhoneNumbers.PhoneRegex PhoneNumbers.RegexCache::GetPatternForRegex(System.String)
extern "C"  PhoneRegex_t3216508019 * RegexCache_GetPatternForRegex_m1558366426 (RegexCache_t1271678307 * __this, String_t* ___regex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PhoneNumbers.RegexCache::ContainsRegex(System.String)
extern "C"  bool RegexCache_ContainsRegex_m3928154114 (RegexCache_t1271678307 * __this, String_t* ___regex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
