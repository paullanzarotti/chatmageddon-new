﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen829260555MethodDeclarations.h"

// System.Void System.Func`2<ChatMessage,System.DateTime>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1634460065(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t925466379 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2995884364_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<ChatMessage,System.DateTime>::Invoke(T)
#define Func_2_Invoke_m3380195139(__this, ___arg10, method) ((  DateTime_t693205669  (*) (Func_2_t925466379 *, ChatMessage_t2384228687 *, const MethodInfo*))Func_2_Invoke_m1345562794_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<ChatMessage,System.DateTime>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4060809446(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t925466379 *, ChatMessage_t2384228687 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m962406237_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<ChatMessage,System.DateTime>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3595513529(__this, ___result0, method) ((  DateTime_t693205669  (*) (Func_2_t925466379 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3619721516_gshared)(__this, ___result0, method)
