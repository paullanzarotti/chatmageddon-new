﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<FriendNavScreen>
struct Collection_1_t1717128207;
// System.Collections.Generic.IList`1<FriendNavScreen>
struct IList_1_t2716324054;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// FriendNavScreen[]
struct FriendNavScreenU5BU5D_t3451088208;
// System.Collections.Generic.IEnumerator`1<FriendNavScreen>
struct IEnumerator_1_t3945874576;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"

// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::.ctor()
extern "C"  void Collection_1__ctor_m3720976342_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3720976342(__this, method) ((  void (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1__ctor_m3720976342_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m1020263533_gshared (Collection_1_t1717128207 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m1020263533(__this, ___list0, method) ((  void (*) (Collection_1_t1717128207 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m1020263533_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1924864039_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1924864039(__this, method) ((  bool (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1924864039_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2147761826_gshared (Collection_1_t1717128207 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2147761826(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1717128207 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2147761826_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2122527587_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2122527587(__this, method) ((  Il2CppObject * (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2122527587_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m777373670_gshared (Collection_1_t1717128207 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m777373670(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1717128207 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m777373670_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2227661616_gshared (Collection_1_t1717128207 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2227661616(__this, ___value0, method) ((  bool (*) (Collection_1_t1717128207 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2227661616_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m4058216588_gshared (Collection_1_t1717128207 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m4058216588(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1717128207 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m4058216588_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m591205715_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m591205715(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m591205715_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3512705715_gshared (Collection_1_t1717128207 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3512705715(__this, ___value0, method) ((  void (*) (Collection_1_t1717128207 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3512705715_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2309112422_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2309112422(__this, method) ((  bool (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2309112422_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4234747490_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4234747490(__this, method) ((  Il2CppObject * (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4234747490_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m4174655355_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4174655355(__this, method) ((  bool (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m4174655355_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3291796274_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3291796274(__this, method) ((  bool (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3291796274_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1136321531_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1136321531(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1136321531_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3137802280_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3137802280(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3137802280_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::Add(T)
extern "C"  void Collection_1_Add_m463983847_gshared (Collection_1_t1717128207 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m463983847(__this, ___item0, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_Add_m463983847_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::Clear()
extern "C"  void Collection_1_Clear_m3113896795_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3113896795(__this, method) ((  void (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_Clear_m3113896795_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1500443857_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1500443857(__this, method) ((  void (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_ClearItems_m1500443857_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::Contains(T)
extern "C"  bool Collection_1_Contains_m4111800305_gshared (Collection_1_t1717128207 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4111800305(__this, ___item0, method) ((  bool (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_Contains_m4111800305_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2939857379_gshared (Collection_1_t1717128207 * __this, FriendNavScreenU5BU5D_t3451088208* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2939857379(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1717128207 *, FriendNavScreenU5BU5D_t3451088208*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2939857379_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<FriendNavScreen>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2918114626_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2918114626(__this, method) ((  Il2CppObject* (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_GetEnumerator_m2918114626_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendNavScreen>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2764067815_gshared (Collection_1_t1717128207 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2764067815(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m2764067815_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4004045852_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4004045852(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m4004045852_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2699677241_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2699677241(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2699677241_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::Remove(T)
extern "C"  bool Collection_1_Remove_m1124379370_gshared (Collection_1_t1717128207 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1124379370(__this, ___item0, method) ((  bool (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_Remove_m1124379370_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2734562144_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2734562144(__this, ___index0, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2734562144_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1384632622_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1384632622(__this, ___index0, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1384632622_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<FriendNavScreen>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1049231142_gshared (Collection_1_t1717128207 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1049231142(__this, method) ((  int32_t (*) (Collection_1_t1717128207 *, const MethodInfo*))Collection_1_get_Count_m1049231142_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<FriendNavScreen>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3044544118_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3044544118(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1717128207 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3044544118_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1978683847_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1978683847(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1978683847_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3914582936_gshared (Collection_1_t1717128207 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3914582936(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1717128207 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m3914582936_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1510493869_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1510493869(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1510493869_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<FriendNavScreen>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m455136457_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m455136457(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m455136457_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<FriendNavScreen>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3359907757_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3359907757(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3359907757_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3475891421_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3475891421(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3475891421_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<FriendNavScreen>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2295808040_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2295808040(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2295808040_gshared)(__this /* static, unused */, ___list0, method)
