﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Atmosphere_Snow_8bits
struct CameraFilterPack_Atmosphere_Snow_8bits_t346296162;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Atmosphere_Snow_8bits::.ctor()
extern "C"  void CameraFilterPack_Atmosphere_Snow_8bits__ctor_m3840621895 (CameraFilterPack_Atmosphere_Snow_8bits_t346296162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Atmosphere_Snow_8bits::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Atmosphere_Snow_8bits_get_material_m2065751184 (CameraFilterPack_Atmosphere_Snow_8bits_t346296162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::Start()
extern "C"  void CameraFilterPack_Atmosphere_Snow_8bits_Start_m2155143875 (CameraFilterPack_Atmosphere_Snow_8bits_t346296162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Atmosphere_Snow_8bits_OnRenderImage_m4082523915 (CameraFilterPack_Atmosphere_Snow_8bits_t346296162 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::OnValidate()
extern "C"  void CameraFilterPack_Atmosphere_Snow_8bits_OnValidate_m1168959382 (CameraFilterPack_Atmosphere_Snow_8bits_t346296162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::Update()
extern "C"  void CameraFilterPack_Atmosphere_Snow_8bits_Update_m3550594928 (CameraFilterPack_Atmosphere_Snow_8bits_t346296162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::OnDisable()
extern "C"  void CameraFilterPack_Atmosphere_Snow_8bits_OnDisable_m2510468846 (CameraFilterPack_Atmosphere_Snow_8bits_t346296162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
