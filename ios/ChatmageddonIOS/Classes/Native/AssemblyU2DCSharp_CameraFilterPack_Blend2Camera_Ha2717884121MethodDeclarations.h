﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blend2Camera_HardLight
struct CameraFilterPack_Blend2Camera_HardLight_t2717884121;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blend2Camera_HardLight::.ctor()
extern "C"  void CameraFilterPack_Blend2Camera_HardLight__ctor_m1363141948 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_HardLight::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blend2Camera_HardLight_get_material_m1051655245 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::Start()
extern "C"  void CameraFilterPack_Blend2Camera_HardLight_Start_m3489157044 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blend2Camera_HardLight_OnRenderImage_m2102459404 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnValidate()
extern "C"  void CameraFilterPack_Blend2Camera_HardLight_OnValidate_m1876869231 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::Update()
extern "C"  void CameraFilterPack_Blend2Camera_HardLight_Update_m562613689 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnEnable()
extern "C"  void CameraFilterPack_Blend2Camera_HardLight_OnEnable_m2916991220 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnDisable()
extern "C"  void CameraFilterPack_Blend2Camera_HardLight_OnDisable_m1331122829 (CameraFilterPack_Blend2Camera_HardLight_t2717884121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
