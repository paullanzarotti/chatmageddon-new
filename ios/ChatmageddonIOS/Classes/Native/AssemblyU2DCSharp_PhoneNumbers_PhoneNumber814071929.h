﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.PhoneNumber
struct PhoneNumber_t814071929;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PhoneNumbers_PhoneNumber_Types_C2831594294.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.PhoneNumber
struct  PhoneNumber_t814071929  : public Il2CppObject
{
public:
	// System.Boolean PhoneNumbers.PhoneNumber::hasCountryCode
	bool ___hasCountryCode_2;
	// System.Int32 PhoneNumbers.PhoneNumber::countryCode_
	int32_t ___countryCode__3;
	// System.Boolean PhoneNumbers.PhoneNumber::hasNationalNumber
	bool ___hasNationalNumber_5;
	// System.UInt64 PhoneNumbers.PhoneNumber::nationalNumber_
	uint64_t ___nationalNumber__6;
	// System.Boolean PhoneNumbers.PhoneNumber::hasExtension
	bool ___hasExtension_8;
	// System.String PhoneNumbers.PhoneNumber::extension_
	String_t* ___extension__9;
	// System.Boolean PhoneNumbers.PhoneNumber::hasItalianLeadingZero
	bool ___hasItalianLeadingZero_11;
	// System.Boolean PhoneNumbers.PhoneNumber::italianLeadingZero_
	bool ___italianLeadingZero__12;
	// System.Boolean PhoneNumbers.PhoneNumber::hasRawInput
	bool ___hasRawInput_14;
	// System.String PhoneNumbers.PhoneNumber::rawInput_
	String_t* ___rawInput__15;
	// System.Boolean PhoneNumbers.PhoneNumber::hasCountryCodeSource
	bool ___hasCountryCodeSource_17;
	// PhoneNumbers.PhoneNumber/Types/CountryCodeSource PhoneNumbers.PhoneNumber::countryCodeSource_
	int32_t ___countryCodeSource__18;
	// System.Boolean PhoneNumbers.PhoneNumber::hasPreferredDomesticCarrierCode
	bool ___hasPreferredDomesticCarrierCode_20;
	// System.String PhoneNumbers.PhoneNumber::preferredDomesticCarrierCode_
	String_t* ___preferredDomesticCarrierCode__21;

public:
	inline static int32_t get_offset_of_hasCountryCode_2() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___hasCountryCode_2)); }
	inline bool get_hasCountryCode_2() const { return ___hasCountryCode_2; }
	inline bool* get_address_of_hasCountryCode_2() { return &___hasCountryCode_2; }
	inline void set_hasCountryCode_2(bool value)
	{
		___hasCountryCode_2 = value;
	}

	inline static int32_t get_offset_of_countryCode__3() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___countryCode__3)); }
	inline int32_t get_countryCode__3() const { return ___countryCode__3; }
	inline int32_t* get_address_of_countryCode__3() { return &___countryCode__3; }
	inline void set_countryCode__3(int32_t value)
	{
		___countryCode__3 = value;
	}

	inline static int32_t get_offset_of_hasNationalNumber_5() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___hasNationalNumber_5)); }
	inline bool get_hasNationalNumber_5() const { return ___hasNationalNumber_5; }
	inline bool* get_address_of_hasNationalNumber_5() { return &___hasNationalNumber_5; }
	inline void set_hasNationalNumber_5(bool value)
	{
		___hasNationalNumber_5 = value;
	}

	inline static int32_t get_offset_of_nationalNumber__6() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___nationalNumber__6)); }
	inline uint64_t get_nationalNumber__6() const { return ___nationalNumber__6; }
	inline uint64_t* get_address_of_nationalNumber__6() { return &___nationalNumber__6; }
	inline void set_nationalNumber__6(uint64_t value)
	{
		___nationalNumber__6 = value;
	}

	inline static int32_t get_offset_of_hasExtension_8() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___hasExtension_8)); }
	inline bool get_hasExtension_8() const { return ___hasExtension_8; }
	inline bool* get_address_of_hasExtension_8() { return &___hasExtension_8; }
	inline void set_hasExtension_8(bool value)
	{
		___hasExtension_8 = value;
	}

	inline static int32_t get_offset_of_extension__9() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___extension__9)); }
	inline String_t* get_extension__9() const { return ___extension__9; }
	inline String_t** get_address_of_extension__9() { return &___extension__9; }
	inline void set_extension__9(String_t* value)
	{
		___extension__9 = value;
		Il2CppCodeGenWriteBarrier(&___extension__9, value);
	}

	inline static int32_t get_offset_of_hasItalianLeadingZero_11() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___hasItalianLeadingZero_11)); }
	inline bool get_hasItalianLeadingZero_11() const { return ___hasItalianLeadingZero_11; }
	inline bool* get_address_of_hasItalianLeadingZero_11() { return &___hasItalianLeadingZero_11; }
	inline void set_hasItalianLeadingZero_11(bool value)
	{
		___hasItalianLeadingZero_11 = value;
	}

	inline static int32_t get_offset_of_italianLeadingZero__12() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___italianLeadingZero__12)); }
	inline bool get_italianLeadingZero__12() const { return ___italianLeadingZero__12; }
	inline bool* get_address_of_italianLeadingZero__12() { return &___italianLeadingZero__12; }
	inline void set_italianLeadingZero__12(bool value)
	{
		___italianLeadingZero__12 = value;
	}

	inline static int32_t get_offset_of_hasRawInput_14() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___hasRawInput_14)); }
	inline bool get_hasRawInput_14() const { return ___hasRawInput_14; }
	inline bool* get_address_of_hasRawInput_14() { return &___hasRawInput_14; }
	inline void set_hasRawInput_14(bool value)
	{
		___hasRawInput_14 = value;
	}

	inline static int32_t get_offset_of_rawInput__15() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___rawInput__15)); }
	inline String_t* get_rawInput__15() const { return ___rawInput__15; }
	inline String_t** get_address_of_rawInput__15() { return &___rawInput__15; }
	inline void set_rawInput__15(String_t* value)
	{
		___rawInput__15 = value;
		Il2CppCodeGenWriteBarrier(&___rawInput__15, value);
	}

	inline static int32_t get_offset_of_hasCountryCodeSource_17() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___hasCountryCodeSource_17)); }
	inline bool get_hasCountryCodeSource_17() const { return ___hasCountryCodeSource_17; }
	inline bool* get_address_of_hasCountryCodeSource_17() { return &___hasCountryCodeSource_17; }
	inline void set_hasCountryCodeSource_17(bool value)
	{
		___hasCountryCodeSource_17 = value;
	}

	inline static int32_t get_offset_of_countryCodeSource__18() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___countryCodeSource__18)); }
	inline int32_t get_countryCodeSource__18() const { return ___countryCodeSource__18; }
	inline int32_t* get_address_of_countryCodeSource__18() { return &___countryCodeSource__18; }
	inline void set_countryCodeSource__18(int32_t value)
	{
		___countryCodeSource__18 = value;
	}

	inline static int32_t get_offset_of_hasPreferredDomesticCarrierCode_20() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___hasPreferredDomesticCarrierCode_20)); }
	inline bool get_hasPreferredDomesticCarrierCode_20() const { return ___hasPreferredDomesticCarrierCode_20; }
	inline bool* get_address_of_hasPreferredDomesticCarrierCode_20() { return &___hasPreferredDomesticCarrierCode_20; }
	inline void set_hasPreferredDomesticCarrierCode_20(bool value)
	{
		___hasPreferredDomesticCarrierCode_20 = value;
	}

	inline static int32_t get_offset_of_preferredDomesticCarrierCode__21() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929, ___preferredDomesticCarrierCode__21)); }
	inline String_t* get_preferredDomesticCarrierCode__21() const { return ___preferredDomesticCarrierCode__21; }
	inline String_t** get_address_of_preferredDomesticCarrierCode__21() { return &___preferredDomesticCarrierCode__21; }
	inline void set_preferredDomesticCarrierCode__21(String_t* value)
	{
		___preferredDomesticCarrierCode__21 = value;
		Il2CppCodeGenWriteBarrier(&___preferredDomesticCarrierCode__21, value);
	}
};

struct PhoneNumber_t814071929_StaticFields
{
public:
	// PhoneNumbers.PhoneNumber PhoneNumbers.PhoneNumber::defaultInstance
	PhoneNumber_t814071929 * ___defaultInstance_0;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(PhoneNumber_t814071929_StaticFields, ___defaultInstance_0)); }
	inline PhoneNumber_t814071929 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline PhoneNumber_t814071929 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(PhoneNumber_t814071929 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___defaultInstance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
