﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// RunMiniGame
struct RunMiniGame_t569217060;
// GpsService
struct GpsService_t3478080739;
// RunDistanceDisplay
struct RunDistanceDisplay_t1358936964;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocationDistanceController
struct  LocationDistanceController_t4191421708  : public MonoBehaviour_t1158329972
{
public:
	// RunMiniGame LocationDistanceController::miniGame
	RunMiniGame_t569217060 * ___miniGame_2;
	// GpsService LocationDistanceController::gps
	GpsService_t3478080739 * ___gps_3;
	// System.Single LocationDistanceController::maxDistance
	float ___maxDistance_4;
	// RunDistanceDisplay LocationDistanceController::distanceDisplay
	RunDistanceDisplay_t1358936964 * ___distanceDisplay_5;
	// UILabel LocationDistanceController::remainingDistanceLabel
	UILabel_t1795115428 * ___remainingDistanceLabel_6;
	// UnityEngine.Coroutine LocationDistanceController::distanceCheckRoutine
	Coroutine_t2299508840 * ___distanceCheckRoutine_7;

public:
	inline static int32_t get_offset_of_miniGame_2() { return static_cast<int32_t>(offsetof(LocationDistanceController_t4191421708, ___miniGame_2)); }
	inline RunMiniGame_t569217060 * get_miniGame_2() const { return ___miniGame_2; }
	inline RunMiniGame_t569217060 ** get_address_of_miniGame_2() { return &___miniGame_2; }
	inline void set_miniGame_2(RunMiniGame_t569217060 * value)
	{
		___miniGame_2 = value;
		Il2CppCodeGenWriteBarrier(&___miniGame_2, value);
	}

	inline static int32_t get_offset_of_gps_3() { return static_cast<int32_t>(offsetof(LocationDistanceController_t4191421708, ___gps_3)); }
	inline GpsService_t3478080739 * get_gps_3() const { return ___gps_3; }
	inline GpsService_t3478080739 ** get_address_of_gps_3() { return &___gps_3; }
	inline void set_gps_3(GpsService_t3478080739 * value)
	{
		___gps_3 = value;
		Il2CppCodeGenWriteBarrier(&___gps_3, value);
	}

	inline static int32_t get_offset_of_maxDistance_4() { return static_cast<int32_t>(offsetof(LocationDistanceController_t4191421708, ___maxDistance_4)); }
	inline float get_maxDistance_4() const { return ___maxDistance_4; }
	inline float* get_address_of_maxDistance_4() { return &___maxDistance_4; }
	inline void set_maxDistance_4(float value)
	{
		___maxDistance_4 = value;
	}

	inline static int32_t get_offset_of_distanceDisplay_5() { return static_cast<int32_t>(offsetof(LocationDistanceController_t4191421708, ___distanceDisplay_5)); }
	inline RunDistanceDisplay_t1358936964 * get_distanceDisplay_5() const { return ___distanceDisplay_5; }
	inline RunDistanceDisplay_t1358936964 ** get_address_of_distanceDisplay_5() { return &___distanceDisplay_5; }
	inline void set_distanceDisplay_5(RunDistanceDisplay_t1358936964 * value)
	{
		___distanceDisplay_5 = value;
		Il2CppCodeGenWriteBarrier(&___distanceDisplay_5, value);
	}

	inline static int32_t get_offset_of_remainingDistanceLabel_6() { return static_cast<int32_t>(offsetof(LocationDistanceController_t4191421708, ___remainingDistanceLabel_6)); }
	inline UILabel_t1795115428 * get_remainingDistanceLabel_6() const { return ___remainingDistanceLabel_6; }
	inline UILabel_t1795115428 ** get_address_of_remainingDistanceLabel_6() { return &___remainingDistanceLabel_6; }
	inline void set_remainingDistanceLabel_6(UILabel_t1795115428 * value)
	{
		___remainingDistanceLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___remainingDistanceLabel_6, value);
	}

	inline static int32_t get_offset_of_distanceCheckRoutine_7() { return static_cast<int32_t>(offsetof(LocationDistanceController_t4191421708, ___distanceCheckRoutine_7)); }
	inline Coroutine_t2299508840 * get_distanceCheckRoutine_7() const { return ___distanceCheckRoutine_7; }
	inline Coroutine_t2299508840 ** get_address_of_distanceCheckRoutine_7() { return &___distanceCheckRoutine_7; }
	inline void set_distanceCheckRoutine_7(Coroutine_t2299508840 * value)
	{
		___distanceCheckRoutine_7 = value;
		Il2CppCodeGenWriteBarrier(&___distanceCheckRoutine_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
