﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransactionDatabase
struct TransactionDatabase_t201476183;
// Uniject.IStorage
struct IStorage_t1347868490;
// Uniject.ILogger
struct ILogger_t2858843691;
// System.String
struct String_t;
// PurchasableItem
struct PurchasableItem_t3963353899;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"

// System.Void TransactionDatabase::.ctor(Uniject.IStorage,Uniject.ILogger)
extern "C"  void TransactionDatabase__ctor_m94891645 (TransactionDatabase_t201476183 * __this, Il2CppObject * ___storage0, Il2CppObject * ___logger1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TransactionDatabase::get_UserId()
extern "C"  String_t* TransactionDatabase_get_UserId_m1464449984 (TransactionDatabase_t201476183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransactionDatabase::set_UserId(System.String)
extern "C"  void TransactionDatabase_set_UserId_m1852118883 (TransactionDatabase_t201476183 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TransactionDatabase::getPurchaseHistory(PurchasableItem)
extern "C"  int32_t TransactionDatabase_getPurchaseHistory_m3194211682 (TransactionDatabase_t201476183 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransactionDatabase::onPurchase(PurchasableItem)
extern "C"  void TransactionDatabase_onPurchase_m1695046091 (TransactionDatabase_t201476183 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransactionDatabase::clearPurchases(PurchasableItem)
extern "C"  void TransactionDatabase_clearPurchases_m1773946422 (TransactionDatabase_t201476183 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransactionDatabase::onRefunded(PurchasableItem)
extern "C"  void TransactionDatabase_onRefunded_m1109733367 (TransactionDatabase_t201476183 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TransactionDatabase::getKey(System.String)
extern "C"  String_t* TransactionDatabase_getKey_m3815701122 (TransactionDatabase_t201476183 * __this, String_t* ___fragment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
