﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIFont
struct UIFont_t389944949;
// UILabel[]
struct UILabelU5BU5D_t4012230477;

#include "AssemblyU2DCSharp_IPPickerBase4159478266.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPPickerLabelBase
struct  IPPickerLabelBase_t2669006230  : public IPPickerBase_t4159478266
{
public:
	// UIFont IPPickerLabelBase::font
	UIFont_t389944949 * ___font_13;
	// UILabel[] IPPickerLabelBase::uiLabels
	UILabelU5BU5D_t4012230477* ___uiLabels_14;

public:
	inline static int32_t get_offset_of_font_13() { return static_cast<int32_t>(offsetof(IPPickerLabelBase_t2669006230, ___font_13)); }
	inline UIFont_t389944949 * get_font_13() const { return ___font_13; }
	inline UIFont_t389944949 ** get_address_of_font_13() { return &___font_13; }
	inline void set_font_13(UIFont_t389944949 * value)
	{
		___font_13 = value;
		Il2CppCodeGenWriteBarrier(&___font_13, value);
	}

	inline static int32_t get_offset_of_uiLabels_14() { return static_cast<int32_t>(offsetof(IPPickerLabelBase_t2669006230, ___uiLabels_14)); }
	inline UILabelU5BU5D_t4012230477* get_uiLabels_14() const { return ___uiLabels_14; }
	inline UILabelU5BU5D_t4012230477** get_address_of_uiLabels_14() { return &___uiLabels_14; }
	inline void set_uiLabels_14(UILabelU5BU5D_t4012230477* value)
	{
		___uiLabels_14 = value;
		Il2CppCodeGenWriteBarrier(&___uiLabels_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
