﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<ImportContatcs>c__AnonStorey17<System.Object>
struct U3CImportContatcsU3Ec__AnonStorey17_t3406229105;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<ImportContatcs>c__AnonStorey17<System.Object>::.ctor()
extern "C"  void U3CImportContatcsU3Ec__AnonStorey17__ctor_m987895952_gshared (U3CImportContatcsU3Ec__AnonStorey17_t3406229105 * __this, const MethodInfo* method);
#define U3CImportContatcsU3Ec__AnonStorey17__ctor_m987895952(__this, method) ((  void (*) (U3CImportContatcsU3Ec__AnonStorey17_t3406229105 *, const MethodInfo*))U3CImportContatcsU3Ec__AnonStorey17__ctor_m987895952_gshared)(__this, method)
// System.Void BaseServer`1/<ImportContatcs>c__AnonStorey17<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CImportContatcsU3Ec__AnonStorey17_U3CU3Em__0_m1973698945_gshared (U3CImportContatcsU3Ec__AnonStorey17_t3406229105 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CImportContatcsU3Ec__AnonStorey17_U3CU3Em__0_m1973698945(__this, ___request0, ___response1, method) ((  void (*) (U3CImportContatcsU3Ec__AnonStorey17_t3406229105 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CImportContatcsU3Ec__AnonStorey17_U3CU3Em__0_m1973698945_gshared)(__this, ___request0, ___response1, method)
