﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.RsaProtectedConfigurationProvider
struct RsaProtectedConfigurationProvider_t2799276815;
// System.Security.Cryptography.RSACryptoServiceProvider
struct RSACryptoServiceProvider_t4229286967;
// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.String
struct String_t;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "mscorlib_System_Security_Cryptography_RSAParameter1462703416.h"

// System.Void System.Configuration.RsaProtectedConfigurationProvider::.ctor()
extern "C"  void RsaProtectedConfigurationProvider__ctor_m777948794 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSACryptoServiceProvider System.Configuration.RsaProtectedConfigurationProvider::GetProvider()
extern "C"  RSACryptoServiceProvider_t4229286967 * RsaProtectedConfigurationProvider_GetProvider_m3628527682 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Configuration.RsaProtectedConfigurationProvider::Decrypt(System.Xml.XmlNode)
extern "C"  XmlNode_t616554813 * RsaProtectedConfigurationProvider_Decrypt_m1477387774 (RsaProtectedConfigurationProvider_t2799276815 * __this, XmlNode_t616554813 * ___encrypted_node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Configuration.RsaProtectedConfigurationProvider::Encrypt(System.Xml.XmlNode)
extern "C"  XmlNode_t616554813 * RsaProtectedConfigurationProvider_Encrypt_m4266463110 (RsaProtectedConfigurationProvider_t2799276815 * __this, XmlNode_t616554813 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.RsaProtectedConfigurationProvider::Initialize(System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  void RsaProtectedConfigurationProvider_Initialize_m233976695 (RsaProtectedConfigurationProvider_t2799276815 * __this, String_t* ___name0, NameValueCollection_t3047564564 * ___configurationValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.RsaProtectedConfigurationProvider::AddKey(System.Int32,System.Boolean)
extern "C"  void RsaProtectedConfigurationProvider_AddKey_m1910991426 (RsaProtectedConfigurationProvider_t2799276815 * __this, int32_t ___keySize0, bool ___exportable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.RsaProtectedConfigurationProvider::DeleteKey()
extern "C"  void RsaProtectedConfigurationProvider_DeleteKey_m2189134304 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.RsaProtectedConfigurationProvider::ExportKey(System.String,System.Boolean)
extern "C"  void RsaProtectedConfigurationProvider_ExportKey_m1703729426 (RsaProtectedConfigurationProvider_t2799276815 * __this, String_t* ___xmlFileName0, bool ___includePrivateParameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.RsaProtectedConfigurationProvider::ImportKey(System.String,System.Boolean)
extern "C"  void RsaProtectedConfigurationProvider_ImportKey_m414528881 (RsaProtectedConfigurationProvider_t2799276815 * __this, String_t* ___xmlFileName0, bool ___exportable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.RsaProtectedConfigurationProvider::get_CspProviderName()
extern "C"  String_t* RsaProtectedConfigurationProvider_get_CspProviderName_m2172146006 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.RsaProtectedConfigurationProvider::get_KeyContainerName()
extern "C"  String_t* RsaProtectedConfigurationProvider_get_KeyContainerName_m2338822033 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSAParameters System.Configuration.RsaProtectedConfigurationProvider::get_RsaPublicKey()
extern "C"  RSAParameters_t1462703416  RsaProtectedConfigurationProvider_get_RsaPublicKey_m313278219 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.RsaProtectedConfigurationProvider::get_UseMachineContainer()
extern "C"  bool RsaProtectedConfigurationProvider_get_UseMachineContainer_m3368001862 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Configuration.RsaProtectedConfigurationProvider::get_UseOAEP()
extern "C"  bool RsaProtectedConfigurationProvider_get_UseOAEP_m1177085035 (RsaProtectedConfigurationProvider_t2799276815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
