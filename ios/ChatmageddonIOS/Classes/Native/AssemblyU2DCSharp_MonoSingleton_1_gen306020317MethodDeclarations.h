﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ChatSceneManager>::.ctor()
#define MonoSingleton_1__ctor_m28604591(__this, method) ((  void (*) (MonoSingleton_1_t306020317 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatSceneManager>::Awake()
#define MonoSingleton_1_Awake_m1937093230(__this, method) ((  void (*) (MonoSingleton_1_t306020317 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ChatSceneManager>::get_Instance()
#define MonoSingleton_1_get_Instance_m879634880(__this /* static, unused */, method) ((  ChatSceneManager_t555354597 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ChatSceneManager>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m1137298536(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ChatSceneManager>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2343385469(__this, method) ((  void (*) (MonoSingleton_1_t306020317 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatSceneManager>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m107842061(__this, method) ((  void (*) (MonoSingleton_1_t306020317 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ChatSceneManager>::.cctor()
#define MonoSingleton_1__cctor_m1893662304(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
