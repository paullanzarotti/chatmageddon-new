﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhoneNumbers.NumberFormat
struct NumberFormat_t441739224;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhoneNumbers.NumberFormat/Builder
struct  Builder_t3378404562  : public Il2CppObject
{
public:
	// PhoneNumbers.NumberFormat PhoneNumbers.NumberFormat/Builder::result
	NumberFormat_t441739224 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(Builder_t3378404562, ___result_0)); }
	inline NumberFormat_t441739224 * get_result_0() const { return ___result_0; }
	inline NumberFormat_t441739224 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(NumberFormat_t441739224 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
