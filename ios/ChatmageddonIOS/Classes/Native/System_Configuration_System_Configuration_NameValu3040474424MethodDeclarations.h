﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.NameValueConfigurationElement
struct NameValueConfigurationElement_t3040474424;
// System.String
struct String_t;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3473514151;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Configuration.NameValueConfigurationElement::.ctor(System.String,System.String)
extern "C"  void NameValueConfigurationElement__ctor_m564809271 (NameValueConfigurationElement_t3040474424 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationElement::.cctor()
extern "C"  void NameValueConfigurationElement__cctor_m4218779648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.NameValueConfigurationElement::get_Name()
extern "C"  String_t* NameValueConfigurationElement_get_Name_m3459236058 (NameValueConfigurationElement_t3040474424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.NameValueConfigurationElement::get_Value()
extern "C"  String_t* NameValueConfigurationElement_get_Value_m2375864624 (NameValueConfigurationElement_t3040474424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Configuration.NameValueConfigurationElement::set_Value(System.String)
extern "C"  void NameValueConfigurationElement_set_Value_m3642882011 (NameValueConfigurationElement_t3040474424 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationPropertyCollection System.Configuration.NameValueConfigurationElement::get_Properties()
extern "C"  ConfigurationPropertyCollection_t3473514151 * NameValueConfigurationElement_get_Properties_m3475725168 (NameValueConfigurationElement_t3040474424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
