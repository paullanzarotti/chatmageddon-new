﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AvatarChatUI
struct AvatarChatUI_t2524884989;
// User
struct User_t719925459;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_User719925459.h"

// System.Void AvatarChatUI::.ctor()
extern "C"  void AvatarChatUI__ctor_m3380724228 (AvatarChatUI_t2524884989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarChatUI::SetAvatarFriend(User)
extern "C"  void AvatarChatUI_SetAvatarFriend_m2076331776 (AvatarChatUI_t2524884989 * __this, User_t719925459 * ___user0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarChatUI::CheckForNewMessage()
extern "C"  void AvatarChatUI_CheckForNewMessage_m766129336 (AvatarChatUI_t2524884989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AvatarChatUI::SetChatUIActive()
extern "C"  void AvatarChatUI_SetChatUIActive_m2342553874 (AvatarChatUI_t2524884989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
