﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BaseSceneManager_1_gen2068275647MethodDeclarations.h"

// System.Void BaseSceneManager`1<InitSceneManager>::.ctor()
#define BaseSceneManager_1__ctor_m3835583015(__this, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1__ctor_m53509762_gshared)(__this, method)
// System.Void BaseSceneManager`1<InitSceneManager>::Awake()
#define BaseSceneManager_1_Awake_m167214116(__this, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1_Awake_m1676433883_gshared)(__this, method)
// System.Void BaseSceneManager`1<InitSceneManager>::Start()
#define BaseSceneManager_1_Start_m3428414499(__this, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1_Start_m2010791446_gshared)(__this, method)
// System.Void BaseSceneManager`1<InitSceneManager>::InitScene()
#define BaseSceneManager_1_InitScene_m1732851291(__this, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1_InitScene_m147665072_gshared)(__this, method)
// System.Void BaseSceneManager`1<InitSceneManager>::AddObjectToScene(AnchorPoint,UnityEngine.GameObject)
#define BaseSceneManager_1_AddObjectToScene_m1952115619(__this, ___anchor0, ___objectToAdd1, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, int32_t, GameObject_t1756533147 *, const MethodInfo*))BaseSceneManager_1_AddObjectToScene_m1741167544_gshared)(__this, ___anchor0, ___objectToAdd1, method)
// System.Void BaseSceneManager`1<InitSceneManager>::StartScene()
#define BaseSceneManager_1_StartScene_m3675050769(__this, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1_StartScene_m1996485408_gshared)(__this, method)
// System.Collections.IEnumerator BaseSceneManager`1<InitSceneManager>::SceneLoad()
#define BaseSceneManager_1_SceneLoad_m3421804929(__this, method) ((  Il2CppObject * (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1_SceneLoad_m614703994_gshared)(__this, method)
// System.Void BaseSceneManager`1<InitSceneManager>::UnloadScene()
#define BaseSceneManager_1_UnloadScene_m3336327198(__this, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1_UnloadScene_m2008479001_gshared)(__this, method)
// System.Void BaseSceneManager`1<InitSceneManager>::ExitScene()
#define BaseSceneManager_1_ExitScene_m8906597(__this, method) ((  void (*) (BaseSceneManager_1_t1069771297 *, const MethodInfo*))BaseSceneManager_1_ExitScene_m2148921902_gshared)(__this, method)
