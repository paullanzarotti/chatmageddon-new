﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShieldBarController
struct ShieldBarController_t264539450;

#include "AssemblyU2DCSharp_SFXButton792651341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldButton
struct  ShieldButton_t2669703779  : public SFXButton_t792651341
{
public:
	// ShieldBarController ShieldButton::barController
	ShieldBarController_t264539450 * ___barController_5;

public:
	inline static int32_t get_offset_of_barController_5() { return static_cast<int32_t>(offsetof(ShieldButton_t2669703779, ___barController_5)); }
	inline ShieldBarController_t264539450 * get_barController_5() const { return ___barController_5; }
	inline ShieldBarController_t264539450 ** get_address_of_barController_5() { return &___barController_5; }
	inline void set_barController_5(ShieldBarController_t264539450 * value)
	{
		___barController_5 = value;
		Il2CppCodeGenWriteBarrier(&___barController_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
