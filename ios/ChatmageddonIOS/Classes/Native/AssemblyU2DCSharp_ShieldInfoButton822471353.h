﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShieldModal
struct ShieldModal_t4049164426;

#include "AssemblyU2DCSharp_RotateButton2565449149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldInfoButton
struct  ShieldInfoButton_t822471353  : public RotateButton_t2565449149
{
public:
	// ShieldModal ShieldInfoButton::modal
	ShieldModal_t4049164426 * ___modal_10;

public:
	inline static int32_t get_offset_of_modal_10() { return static_cast<int32_t>(offsetof(ShieldInfoButton_t822471353, ___modal_10)); }
	inline ShieldModal_t4049164426 * get_modal_10() const { return ___modal_10; }
	inline ShieldModal_t4049164426 ** get_address_of_modal_10() { return &___modal_10; }
	inline void set_modal_10(ShieldModal_t4049164426 * value)
	{
		___modal_10 = value;
		Il2CppCodeGenWriteBarrier(&___modal_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
