﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AttackUIScaler
struct AttackUIScaler_t1212281222;

#include "codegen/il2cpp-codegen.h"

// System.Void AttackUIScaler::.ctor()
extern "C"  void AttackUIScaler__ctor_m1880832705 (AttackUIScaler_t1212281222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AttackUIScaler::GlobalUIScale()
extern "C"  void AttackUIScaler_GlobalUIScale_m456587880 (AttackUIScaler_t1212281222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
