﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWrapContent/OnInitializeItem
struct OnInitializeItem_t2948699028;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void UIWrapContent/OnInitializeItem::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInitializeItem__ctor_m2088558765 (OnInitializeItem_t2948699028 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent/OnInitializeItem::Invoke(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void OnInitializeItem_Invoke_m286694789 (OnInitializeItem_t2948699028 * __this, GameObject_t1756533147 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult UIWrapContent/OnInitializeItem::BeginInvoke(UnityEngine.GameObject,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnInitializeItem_BeginInvoke_m3324777424 (OnInitializeItem_t2948699028 * __this, GameObject_t1756533147 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWrapContent/OnInitializeItem::EndInvoke(System.IAsyncResult)
extern "C"  void OnInitializeItem_EndInvoke_m1518335515 (OnInitializeItem_t2948699028 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
