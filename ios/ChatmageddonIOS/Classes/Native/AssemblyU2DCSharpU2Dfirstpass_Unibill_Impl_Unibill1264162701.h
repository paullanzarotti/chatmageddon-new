﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PurchasableItem
struct PurchasableItem_t3963353899;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unibill.Impl.UnibillConfiguration/<UnibillConfiguration>c__AnonStorey0
struct  U3CUnibillConfigurationU3Ec__AnonStorey0_t1264162701  : public Il2CppObject
{
public:
	// PurchasableItem Unibill.Impl.UnibillConfiguration/<UnibillConfiguration>c__AnonStorey0::item
	PurchasableItem_t3963353899 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CUnibillConfigurationU3Ec__AnonStorey0_t1264162701, ___item_0)); }
	inline PurchasableItem_t3963353899 * get_item_0() const { return ___item_0; }
	inline PurchasableItem_t3963353899 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(PurchasableItem_t3963353899 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier(&___item_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
