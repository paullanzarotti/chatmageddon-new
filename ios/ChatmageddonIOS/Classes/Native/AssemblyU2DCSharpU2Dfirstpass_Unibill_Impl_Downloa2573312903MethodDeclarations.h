﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3
struct U3CdownloadU3Ec__AnonStorey3_t2573312903;

#include "codegen/il2cpp-codegen.h"

// System.Void Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3::.ctor()
extern "C"  void U3CdownloadU3Ec__AnonStorey3__ctor_m3203538060 (U3CdownloadU3Ec__AnonStorey3_t2573312903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.DownloadManager/<download>c__Iterator2/<download>c__AnonStorey3::<>m__0()
extern "C"  void U3CdownloadU3Ec__AnonStorey3_U3CU3Em__0_m202156855 (U3CdownloadU3Ec__AnonStorey3_t2573312903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
