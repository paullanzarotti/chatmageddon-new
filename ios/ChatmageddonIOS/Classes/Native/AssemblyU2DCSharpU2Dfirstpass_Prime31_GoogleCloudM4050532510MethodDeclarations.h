﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.GoogleCloudMessagingEventListener
struct GoogleCloudMessagingEventListener_t4050532510;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.GoogleCloudMessagingEventListener::.ctor()
extern "C"  void GoogleCloudMessagingEventListener__ctor_m1717019714 (GoogleCloudMessagingEventListener_t4050532510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
