﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SelectAudienceSwitch
struct SelectAudienceSwitch_t3539699246;

#include "AssemblyU2DCSharp_NavigationScreen2333230110.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectAudienceNavScreen
struct  SelectAudienceNavScreen_t4274980323  : public NavigationScreen_t2333230110
{
public:
	// SelectAudienceSwitch SelectAudienceNavScreen::selectAudienceSwitch
	SelectAudienceSwitch_t3539699246 * ___selectAudienceSwitch_3;
	// System.Boolean SelectAudienceNavScreen::UIclosing
	bool ___UIclosing_4;

public:
	inline static int32_t get_offset_of_selectAudienceSwitch_3() { return static_cast<int32_t>(offsetof(SelectAudienceNavScreen_t4274980323, ___selectAudienceSwitch_3)); }
	inline SelectAudienceSwitch_t3539699246 * get_selectAudienceSwitch_3() const { return ___selectAudienceSwitch_3; }
	inline SelectAudienceSwitch_t3539699246 ** get_address_of_selectAudienceSwitch_3() { return &___selectAudienceSwitch_3; }
	inline void set_selectAudienceSwitch_3(SelectAudienceSwitch_t3539699246 * value)
	{
		___selectAudienceSwitch_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectAudienceSwitch_3, value);
	}

	inline static int32_t get_offset_of_UIclosing_4() { return static_cast<int32_t>(offsetof(SelectAudienceNavScreen_t4274980323, ___UIclosing_4)); }
	inline bool get_UIclosing_4() const { return ___UIclosing_4; }
	inline bool* get_address_of_UIclosing_4() { return &___UIclosing_4; }
	inline void set_UIclosing_4(bool value)
	{
		___UIclosing_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
