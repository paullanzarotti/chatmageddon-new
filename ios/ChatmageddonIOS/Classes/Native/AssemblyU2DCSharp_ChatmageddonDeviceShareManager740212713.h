﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// LanguageModel
struct LanguageModel_t2428340347;

#include "AssemblyU2DCSharp_DeviceShareManager_2_gen4204038203.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatmageddonDeviceShareManager
struct  ChatmageddonDeviceShareManager_t740212713  : public DeviceShareManager_2_t4204038203
{
public:
	// UnityEngine.GameObject ChatmageddonDeviceShareManager::panel
	GameObject_t1756533147 * ___panel_4;
	// UnityEngine.GameObject ChatmageddonDeviceShareManager::background
	GameObject_t1756533147 * ___background_5;
	// UnityEngine.GameObject ChatmageddonDeviceShareManager::UI
	GameObject_t1756533147 * ___UI_6;
	// LanguageModel ChatmageddonDeviceShareManager::linkModel
	LanguageModel_t2428340347 * ___linkModel_7;
	// System.Boolean ChatmageddonDeviceShareManager::shareUIClosing
	bool ___shareUIClosing_8;

public:
	inline static int32_t get_offset_of_panel_4() { return static_cast<int32_t>(offsetof(ChatmageddonDeviceShareManager_t740212713, ___panel_4)); }
	inline GameObject_t1756533147 * get_panel_4() const { return ___panel_4; }
	inline GameObject_t1756533147 ** get_address_of_panel_4() { return &___panel_4; }
	inline void set_panel_4(GameObject_t1756533147 * value)
	{
		___panel_4 = value;
		Il2CppCodeGenWriteBarrier(&___panel_4, value);
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(ChatmageddonDeviceShareManager_t740212713, ___background_5)); }
	inline GameObject_t1756533147 * get_background_5() const { return ___background_5; }
	inline GameObject_t1756533147 ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(GameObject_t1756533147 * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier(&___background_5, value);
	}

	inline static int32_t get_offset_of_UI_6() { return static_cast<int32_t>(offsetof(ChatmageddonDeviceShareManager_t740212713, ___UI_6)); }
	inline GameObject_t1756533147 * get_UI_6() const { return ___UI_6; }
	inline GameObject_t1756533147 ** get_address_of_UI_6() { return &___UI_6; }
	inline void set_UI_6(GameObject_t1756533147 * value)
	{
		___UI_6 = value;
		Il2CppCodeGenWriteBarrier(&___UI_6, value);
	}

	inline static int32_t get_offset_of_linkModel_7() { return static_cast<int32_t>(offsetof(ChatmageddonDeviceShareManager_t740212713, ___linkModel_7)); }
	inline LanguageModel_t2428340347 * get_linkModel_7() const { return ___linkModel_7; }
	inline LanguageModel_t2428340347 ** get_address_of_linkModel_7() { return &___linkModel_7; }
	inline void set_linkModel_7(LanguageModel_t2428340347 * value)
	{
		___linkModel_7 = value;
		Il2CppCodeGenWriteBarrier(&___linkModel_7, value);
	}

	inline static int32_t get_offset_of_shareUIClosing_8() { return static_cast<int32_t>(offsetof(ChatmageddonDeviceShareManager_t740212713, ___shareUIClosing_8)); }
	inline bool get_shareUIClosing_8() const { return ___shareUIClosing_8; }
	inline bool* get_address_of_shareUIClosing_8() { return &___shareUIClosing_8; }
	inline void set_shareUIClosing_8(bool value)
	{
		___shareUIClosing_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
