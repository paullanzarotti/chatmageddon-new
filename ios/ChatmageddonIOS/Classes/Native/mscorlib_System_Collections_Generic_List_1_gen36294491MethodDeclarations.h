﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<RegNavScreen>
struct List_1_t36294491;
// System.Collections.Generic.IEnumerable`1<RegNavScreen>
struct IEnumerable_1_t959300404;
// RegNavScreen[]
struct RegNavScreenU5BU5D_t3187910006;
// System.Collections.Generic.IEnumerator`1<RegNavScreen>
struct IEnumerator_1_t2437664482;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<RegNavScreen>
struct ICollection_1_t1619248664;
// System.Collections.ObjectModel.ReadOnlyCollection`1<RegNavScreen>
struct ReadOnlyCollection_1_t852959051;
// System.Predicate`1<RegNavScreen>
struct Predicate_1_t3405110770;
// System.Action`1<RegNavScreen>
struct Action_1_t468972741;
// System.Collections.Generic.IComparer`1<RegNavScreen>
struct IComparer_1_t2916603777;
// System.Comparison`1<RegNavScreen>
struct Comparison_1_t1928912210;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3865991461.h"

// System.Void System.Collections.Generic.List`1<RegNavScreen>::.ctor()
extern "C"  void List_1__ctor_m2717616963_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1__ctor_m2717616963(__this, method) ((  void (*) (List_1_t36294491 *, const MethodInfo*))List_1__ctor_m2717616963_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3338424286_gshared (List_1_t36294491 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3338424286(__this, ___collection0, method) ((  void (*) (List_1_t36294491 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3338424286_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3940589376_gshared (List_1_t36294491 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3940589376(__this, ___capacity0, method) ((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1__ctor_m3940589376_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::.ctor(T[],System.Int32)
extern "C"  void List_1__ctor_m3236546224_gshared (List_1_t36294491 * __this, RegNavScreenU5BU5D_t3187910006* ___data0, int32_t ___size1, const MethodInfo* method);
#define List_1__ctor_m3236546224(__this, ___data0, ___size1, method) ((  void (*) (List_1_t36294491 *, RegNavScreenU5BU5D_t3187910006*, int32_t, const MethodInfo*))List_1__ctor_m3236546224_gshared)(__this, ___data0, ___size1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::.cctor()
extern "C"  void List_1__cctor_m2251989748_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2251989748(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2251989748_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<RegNavScreen>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3269971707_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3269971707(__this, method) ((  Il2CppObject* (*) (List_1_t36294491 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3269971707_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1826756575_gshared (List_1_t36294491 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1826756575(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t36294491 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1826756575_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2219544310_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2219544310(__this, method) ((  Il2CppObject * (*) (List_1_t36294491 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2219544310_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2059474591_gshared (List_1_t36294491 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2059474591(__this, ___item0, method) ((  int32_t (*) (List_1_t36294491 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2059474591_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m644119031_gshared (List_1_t36294491 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m644119031(__this, ___item0, method) ((  bool (*) (List_1_t36294491 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m644119031_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3644688653_gshared (List_1_t36294491 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3644688653(__this, ___item0, method) ((  int32_t (*) (List_1_t36294491 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3644688653_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1978602188_gshared (List_1_t36294491 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1978602188(__this, ___index0, ___item1, method) ((  void (*) (List_1_t36294491 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1978602188_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1977212674_gshared (List_1_t36294491 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1977212674(__this, ___item0, method) ((  void (*) (List_1_t36294491 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1977212674_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3668450962_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3668450962(__this, method) ((  bool (*) (List_1_t36294491 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3668450962_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3341958263_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3341958263(__this, method) ((  bool (*) (List_1_t36294491 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3341958263_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<RegNavScreen>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m923685363_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m923685363(__this, method) ((  Il2CppObject * (*) (List_1_t36294491 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m923685363_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3317864120_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3317864120(__this, method) ((  bool (*) (List_1_t36294491 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3317864120_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3491398267_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3491398267(__this, method) ((  bool (*) (List_1_t36294491 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3491398267_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m328232726_gshared (List_1_t36294491 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m328232726(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m328232726_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2708502817_gshared (List_1_t36294491 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2708502817(__this, ___index0, ___value1, method) ((  void (*) (List_1_t36294491 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2708502817_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Add(T)
extern "C"  void List_1_Add_m982005176_gshared (List_1_t36294491 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m982005176(__this, ___item0, method) ((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_Add_m982005176_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1801996083_gshared (List_1_t36294491 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1801996083(__this, ___newCount0, method) ((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1801996083_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2951222928_gshared (List_1_t36294491 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2951222928(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t36294491 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2951222928_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3729816467_gshared (List_1_t36294491 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3729816467(__this, ___collection0, method) ((  void (*) (List_1_t36294491 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3729816467_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3287971363_gshared (List_1_t36294491 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3287971363(__this, ___enumerable0, method) ((  void (*) (List_1_t36294491 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3287971363_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m213629894_gshared (List_1_t36294491 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m213629894(__this, ___collection0, method) ((  void (*) (List_1_t36294491 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m213629894_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<RegNavScreen>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t852959051 * List_1_AsReadOnly_m2392576435_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2392576435(__this, method) ((  ReadOnlyCollection_1_t852959051 * (*) (List_1_t36294491 *, const MethodInfo*))List_1_AsReadOnly_m2392576435_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Clear()
extern "C"  void List_1_Clear_m1007088316_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_Clear_m1007088316(__this, method) ((  void (*) (List_1_t36294491 *, const MethodInfo*))List_1_Clear_m1007088316_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::Contains(T)
extern "C"  bool List_1_Contains_m3773348210_gshared (List_1_t36294491 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3773348210(__this, ___item0, method) ((  bool (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_Contains_m3773348210_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1930128563_gshared (List_1_t36294491 * __this, RegNavScreenU5BU5D_t3187910006* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1930128563(__this, ___array0, method) ((  void (*) (List_1_t36294491 *, RegNavScreenU5BU5D_t3187910006*, const MethodInfo*))List_1_CopyTo_m1930128563_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m13977940_gshared (List_1_t36294491 * __this, RegNavScreenU5BU5D_t3187910006* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m13977940(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t36294491 *, RegNavScreenU5BU5D_t3187910006*, int32_t, const MethodInfo*))List_1_CopyTo_m13977940_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C"  void List_1_CopyTo_m1349085942_gshared (List_1_t36294491 * __this, int32_t ___index0, RegNavScreenU5BU5D_t3187910006* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const MethodInfo* method);
#define List_1_CopyTo_m1349085942(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method) ((  void (*) (List_1_t36294491 *, int32_t, RegNavScreenU5BU5D_t3187910006*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1349085942_gshared)(__this, ___index0, ___array1, ___arrayIndex2, ___count3, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::Exists(System.Predicate`1<T>)
extern "C"  bool List_1_Exists_m3185591002_gshared (List_1_t36294491 * __this, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_Exists_m3185591002(__this, ___match0, method) ((  bool (*) (List_1_t36294491 *, Predicate_1_t3405110770 *, const MethodInfo*))List_1_Exists_m3185591002_gshared)(__this, ___match0, method)
// T System.Collections.Generic.List`1<RegNavScreen>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m3631078222_gshared (List_1_t36294491 * __this, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_Find_m3631078222(__this, ___match0, method) ((  int32_t (*) (List_1_t36294491 *, Predicate_1_t3405110770 *, const MethodInfo*))List_1_Find_m3631078222_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3650574151_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3650574151(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3405110770 *, const MethodInfo*))List_1_CheckMatch_m3650574151_gshared)(__this /* static, unused */, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<RegNavScreen>::FindAll(System.Predicate`1<T>)
extern "C"  List_1_t36294491 * List_1_FindAll_m1749214439_gshared (List_1_t36294491 * __this, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_FindAll_m1749214439(__this, ___match0, method) ((  List_1_t36294491 * (*) (List_1_t36294491 *, Predicate_1_t3405110770 *, const MethodInfo*))List_1_FindAll_m1749214439_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<RegNavScreen>::FindAllStackBits(System.Predicate`1<T>)
extern "C"  List_1_t36294491 * List_1_FindAllStackBits_m1538680761_gshared (List_1_t36294491 * __this, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_FindAllStackBits_m1538680761(__this, ___match0, method) ((  List_1_t36294491 * (*) (List_1_t36294491 *, Predicate_1_t3405110770 *, const MethodInfo*))List_1_FindAllStackBits_m1538680761_gshared)(__this, ___match0, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<RegNavScreen>::FindAllList(System.Predicate`1<T>)
extern "C"  List_1_t36294491 * List_1_FindAllList_m2104256457_gshared (List_1_t36294491 * __this, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_FindAllList_m2104256457(__this, ___match0, method) ((  List_1_t36294491 * (*) (List_1_t36294491 *, Predicate_1_t3405110770 *, const MethodInfo*))List_1_FindAllList_m2104256457_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::FindIndex(System.Predicate`1<T>)
extern "C"  int32_t List_1_FindIndex_m2586315127_gshared (List_1_t36294491 * __this, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_FindIndex_m2586315127(__this, ___match0, method) ((  int32_t (*) (List_1_t36294491 *, Predicate_1_t3405110770 *, const MethodInfo*))List_1_FindIndex_m2586315127_gshared)(__this, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1370444472_gshared (List_1_t36294491 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3405110770 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1370444472(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t36294491 *, int32_t, int32_t, Predicate_1_t3405110770 *, const MethodInfo*))List_1_GetIndex_m1370444472_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m4049081921_gshared (List_1_t36294491 * __this, Action_1_t468972741 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m4049081921(__this, ___action0, method) ((  void (*) (List_1_t36294491 *, Action_1_t468972741 *, const MethodInfo*))List_1_ForEach_m4049081921_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<RegNavScreen>::GetEnumerator()
extern "C"  Enumerator_t3865991461  List_1_GetEnumerator_m1237501075_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1237501075(__this, method) ((  Enumerator_t3865991461  (*) (List_1_t36294491 *, const MethodInfo*))List_1_GetEnumerator_m1237501075_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3086265106_gshared (List_1_t36294491 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3086265106(__this, ___item0, method) ((  int32_t (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_IndexOf_m3086265106_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2649271619_gshared (List_1_t36294491 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2649271619(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t36294491 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2649271619_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1634093908_gshared (List_1_t36294491 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1634093908(__this, ___index0, method) ((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1634093908_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1646219109_gshared (List_1_t36294491 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1646219109(__this, ___index0, ___item1, method) ((  void (*) (List_1_t36294491 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m1646219109_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m802263214_gshared (List_1_t36294491 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m802263214(__this, ___collection0, method) ((  void (*) (List_1_t36294491 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m802263214_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<RegNavScreen>::Remove(T)
extern "C"  bool List_1_Remove_m3411737909_gshared (List_1_t36294491 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m3411737909(__this, ___item0, method) ((  bool (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_Remove_m3411737909_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1475539903_gshared (List_1_t36294491 * __this, Predicate_1_t3405110770 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1475539903(__this, ___match0, method) ((  int32_t (*) (List_1_t36294491 *, Predicate_1_t3405110770 *, const MethodInfo*))List_1_RemoveAll_m1475539903_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2096091561_gshared (List_1_t36294491 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2096091561(__this, ___index0, method) ((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2096091561_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m2582408768_gshared (List_1_t36294491 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m2582408768(__this, ___index0, ___count1, method) ((  void (*) (List_1_t36294491 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m2582408768_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Reverse()
extern "C"  void List_1_Reverse_m747707131_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_Reverse_m747707131(__this, method) ((  void (*) (List_1_t36294491 *, const MethodInfo*))List_1_Reverse_m747707131_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Sort()
extern "C"  void List_1_Sort_m1847208349_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_Sort_m1847208349(__this, method) ((  void (*) (List_1_t36294491 *, const MethodInfo*))List_1_Sort_m1847208349_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m622084529_gshared (List_1_t36294491 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m622084529(__this, ___comparer0, method) ((  void (*) (List_1_t36294491 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m622084529_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3649598508_gshared (List_1_t36294491 * __this, Comparison_1_t1928912210 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3649598508(__this, ___comparison0, method) ((  void (*) (List_1_t36294491 *, Comparison_1_t1928912210 *, const MethodInfo*))List_1_Sort_m3649598508_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<RegNavScreen>::ToArray()
extern "C"  RegNavScreenU5BU5D_t3187910006* List_1_ToArray_m2973885618_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_ToArray_m2973885618(__this, method) ((  RegNavScreenU5BU5D_t3187910006* (*) (List_1_t36294491 *, const MethodInfo*))List_1_ToArray_m2973885618_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2847577606_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2847577606(__this, method) ((  void (*) (List_1_t36294491 *, const MethodInfo*))List_1_TrimExcess_m2847577606_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2238586236_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2238586236(__this, method) ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))List_1_get_Capacity_m2238586236_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3858923475_gshared (List_1_t36294491 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3858923475(__this, ___value0, method) ((  void (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3858923475_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<RegNavScreen>::get_Count()
extern "C"  int32_t List_1_get_Count_m4260937583_gshared (List_1_t36294491 * __this, const MethodInfo* method);
#define List_1_get_Count_m4260937583(__this, method) ((  int32_t (*) (List_1_t36294491 *, const MethodInfo*))List_1_get_Count_m4260937583_gshared)(__this, method)
// T System.Collections.Generic.List`1<RegNavScreen>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m2841589173_gshared (List_1_t36294491 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2841589173(__this, ___index0, method) ((  int32_t (*) (List_1_t36294491 *, int32_t, const MethodInfo*))List_1_get_Item_m2841589173_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<RegNavScreen>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m70260406_gshared (List_1_t36294491 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m70260406(__this, ___index0, ___value1, method) ((  void (*) (List_1_t36294491 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m70260406_gshared)(__this, ___index0, ___value1, method)
