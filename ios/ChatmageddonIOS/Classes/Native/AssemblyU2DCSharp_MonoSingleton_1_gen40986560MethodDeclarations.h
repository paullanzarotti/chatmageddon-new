﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MonoSingleton_1_gen2440115015MethodDeclarations.h"

// System.Void MonoSingleton`1<ColourStyleSheet>::.ctor()
#define MonoSingleton_1__ctor_m1685208872(__this, method) ((  void (*) (MonoSingleton_1_t40986560 *, const MethodInfo*))MonoSingleton_1__ctor_m437210476_gshared)(__this, method)
// System.Void MonoSingleton`1<ColourStyleSheet>::Awake()
#define MonoSingleton_1_Awake_m349498185(__this, method) ((  void (*) (MonoSingleton_1_t40986560 *, const MethodInfo*))MonoSingleton_1_Awake_m2844725271_gshared)(__this, method)
// T MonoSingleton`1<ColourStyleSheet>::get_Instance()
#define MonoSingleton_1_get_Instance_m564399611(__this /* static, unused */, method) ((  ColourStyleSheet_t290320840 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_get_Instance_m4076012193_gshared)(__this /* static, unused */, method)
// System.Boolean MonoSingleton`1<ColourStyleSheet>::IsAvailable()
#define MonoSingleton_1_IsAvailable_m3350290583(__this /* static, unused */, method) ((  bool (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1_IsAvailable_m527113581_gshared)(__this /* static, unused */, method)
// System.Void MonoSingleton`1<ColourStyleSheet>::ForceInstanceInit()
#define MonoSingleton_1_ForceInstanceInit_m2947578586(__this, method) ((  void (*) (MonoSingleton_1_t40986560 *, const MethodInfo*))MonoSingleton_1_ForceInstanceInit_m3222864930_gshared)(__this, method)
// System.Void MonoSingleton`1<ColourStyleSheet>::OnApplicationQuit()
#define MonoSingleton_1_OnApplicationQuit_m337198030(__this, method) ((  void (*) (MonoSingleton_1_t40986560 *, const MethodInfo*))MonoSingleton_1_OnApplicationQuit_m750638130_gshared)(__this, method)
// System.Void MonoSingleton`1<ColourStyleSheet>::.cctor()
#define MonoSingleton_1__cctor_m408311(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))MonoSingleton_1__cctor_m590081433_gshared)(__this /* static, unused */, method)
