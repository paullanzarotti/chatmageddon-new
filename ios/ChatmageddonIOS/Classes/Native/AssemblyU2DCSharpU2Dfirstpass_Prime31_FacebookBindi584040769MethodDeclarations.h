﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.FacebookBinding/<graphRequest>c__AnonStorey2
struct U3CgraphRequestU3Ec__AnonStorey2_t584040769;

#include "codegen/il2cpp-codegen.h"

// System.Void Prime31.FacebookBinding/<graphRequest>c__AnonStorey2::.ctor()
extern "C"  void U3CgraphRequestU3Ec__AnonStorey2__ctor_m2659152870 (U3CgraphRequestU3Ec__AnonStorey2_t584040769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.FacebookBinding/<graphRequest>c__AnonStorey2::<>m__0()
extern "C"  void U3CgraphRequestU3Ec__AnonStorey2_U3CU3Em__0_m4013195137 (U3CgraphRequestU3Ec__AnonStorey2_t584040769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
