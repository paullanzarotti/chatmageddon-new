﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseAudioSource/<FadeInSource>c__Iterator1
struct U3CFadeInSourceU3Ec__Iterator1_t4157317973;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BaseAudioSource/<FadeInSource>c__Iterator1::.ctor()
extern "C"  void U3CFadeInSourceU3Ec__Iterator1__ctor_m4008783210 (U3CFadeInSourceU3Ec__Iterator1_t4157317973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BaseAudioSource/<FadeInSource>c__Iterator1::MoveNext()
extern "C"  bool U3CFadeInSourceU3Ec__Iterator1_MoveNext_m701682994 (U3CFadeInSourceU3Ec__Iterator1_t4157317973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BaseAudioSource/<FadeInSource>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFadeInSourceU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1687919838 (U3CFadeInSourceU3Ec__Iterator1_t4157317973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BaseAudioSource/<FadeInSource>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFadeInSourceU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m592909350 (U3CFadeInSourceU3Ec__Iterator1_t4157317973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource/<FadeInSource>c__Iterator1::Dispose()
extern "C"  void U3CFadeInSourceU3Ec__Iterator1_Dispose_m20969927 (U3CFadeInSourceU3Ec__Iterator1_t4157317973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BaseAudioSource/<FadeInSource>c__Iterator1::Reset()
extern "C"  void U3CFadeInSourceU3Ec__Iterator1_Reset_m3178988977 (U3CFadeInSourceU3Ec__Iterator1_t4157317973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
