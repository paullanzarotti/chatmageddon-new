﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChangeMapTextureExample
struct ChangeMapTextureExample_t833836899;

#include "codegen/il2cpp-codegen.h"

// System.Void ChangeMapTextureExample::.ctor()
extern "C"  void ChangeMapTextureExample__ctor_m3723965104 (ChangeMapTextureExample_t833836899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeMapTextureExample::ChangeMapTexture()
extern "C"  void ChangeMapTextureExample_ChangeMapTexture_m3668759303 (ChangeMapTextureExample_t833836899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeMapTextureExample::OnGUI()
extern "C"  void ChangeMapTextureExample_OnGUI_m3609357232 (ChangeMapTextureExample_t833836899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChangeMapTextureExample::Start()
extern "C"  void ChangeMapTextureExample_Start_m314906388 (ChangeMapTextureExample_t833836899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
