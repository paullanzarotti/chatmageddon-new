﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_Blur_Movie
struct CameraFilterPack_Blur_Movie_t2949542203;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_Blur_Movie::.ctor()
extern "C"  void CameraFilterPack_Blur_Movie__ctor_m3790076878 (CameraFilterPack_Blur_Movie_t2949542203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Movie::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_Blur_Movie_get_material_m919394723 (CameraFilterPack_Blur_Movie_t2949542203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::Start()
extern "C"  void CameraFilterPack_Blur_Movie_Start_m2499649090 (CameraFilterPack_Blur_Movie_t2949542203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_Blur_Movie_OnRenderImage_m853610362 (CameraFilterPack_Blur_Movie_t2949542203 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::OnValidate()
extern "C"  void CameraFilterPack_Blur_Movie_OnValidate_m478272977 (CameraFilterPack_Blur_Movie_t2949542203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::Update()
extern "C"  void CameraFilterPack_Blur_Movie_Update_m1929542075 (CameraFilterPack_Blur_Movie_t2949542203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::OnDisable()
extern "C"  void CameraFilterPack_Blur_Movie_OnDisable_m1341970699 (CameraFilterPack_Blur_Movie_t2949542203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
