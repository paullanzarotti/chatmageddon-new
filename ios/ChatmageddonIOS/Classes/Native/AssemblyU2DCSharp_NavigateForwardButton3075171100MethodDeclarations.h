﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigateForwardButton
struct NavigateForwardButton_t3075171100;

#include "codegen/il2cpp-codegen.h"

// System.Void NavigateForwardButton::.ctor()
extern "C"  void NavigateForwardButton__ctor_m95191469 (NavigateForwardButton_t3075171100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigateForwardButton::OnButtonClick()
extern "C"  void NavigateForwardButton_OnButtonClick_m263286112 (NavigateForwardButton_t3075171100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NavigateForwardButton::SetButtonActive(System.Boolean)
extern "C"  void NavigateForwardButton_SetButtonActive_m99539748 (NavigateForwardButton_t3075171100 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
