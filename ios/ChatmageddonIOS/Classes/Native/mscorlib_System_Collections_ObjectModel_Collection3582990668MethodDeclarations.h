﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Int16>
struct Collection_1_t3582990668;
// System.Collections.Generic.IList`1<System.Int16>
struct IList_1_t287219219;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int16[]
struct Int16U5BU5D_t3104283263;
// System.Collections.Generic.IEnumerator`1<System.Int16>
struct IEnumerator_1_t1516769741;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::.ctor()
extern "C"  void Collection_1__ctor_m2821085430_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2821085430(__this, method) ((  void (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1__ctor_m2821085430_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m953336721_gshared (Collection_1_t3582990668 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m953336721(__this, ___list0, method) ((  void (*) (Collection_1_t3582990668 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m953336721_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1096858203_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1096858203(__this, method) ((  bool (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1096858203_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2160670482_gshared (Collection_1_t3582990668 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2160670482(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3582990668 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2160670482_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3876924719_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3876924719(__this, method) ((  Il2CppObject * (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3876924719_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2163364142_gshared (Collection_1_t3582990668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2163364142(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3582990668 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2163364142_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1850840976_gshared (Collection_1_t3582990668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1850840976(__this, ___value0, method) ((  bool (*) (Collection_1_t3582990668 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1850840976_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1691984480_gshared (Collection_1_t3582990668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1691984480(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3582990668 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1691984480_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3391585751_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3391585751(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3391585751_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m4238604335_gshared (Collection_1_t3582990668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m4238604335(__this, ___value0, method) ((  void (*) (Collection_1_t3582990668 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m4238604335_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4186479126_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4186479126(__this, method) ((  bool (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4186479126_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4016878248_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4016878248(__this, method) ((  Il2CppObject * (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4016878248_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3475218423_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3475218423(__this, method) ((  bool (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3475218423_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3140430866_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3140430866(__this, method) ((  bool (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3140430866_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m686955195_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m686955195(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3582990668 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m686955195_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2636622896_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2636622896(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2636622896_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::Add(T)
extern "C"  void Collection_1_Add_m1176410571_gshared (Collection_1_t3582990668 * __this, int16_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1176410571(__this, ___item0, method) ((  void (*) (Collection_1_t3582990668 *, int16_t, const MethodInfo*))Collection_1_Add_m1176410571_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::Clear()
extern "C"  void Collection_1_Clear_m3478615199_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3478615199(__this, method) ((  void (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_Clear_m3478615199_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2757561437_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2757561437(__this, method) ((  void (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_ClearItems_m2757561437_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::Contains(T)
extern "C"  bool Collection_1_Contains_m2677631701_gshared (Collection_1_t3582990668 * __this, int16_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2677631701(__this, ___item0, method) ((  bool (*) (Collection_1_t3582990668 *, int16_t, const MethodInfo*))Collection_1_Contains_m2677631701_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m27964615_gshared (Collection_1_t3582990668 * __this, Int16U5BU5D_t3104283263* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m27964615(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3582990668 *, Int16U5BU5D_t3104283263*, int32_t, const MethodInfo*))Collection_1_CopyTo_m27964615_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int16>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m882763962_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m882763962(__this, method) ((  Il2CppObject* (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_GetEnumerator_m882763962_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int16>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1729471571_gshared (Collection_1_t3582990668 * __this, int16_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1729471571(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3582990668 *, int16_t, const MethodInfo*))Collection_1_IndexOf_m1729471571_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1531032532_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, int16_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1531032532(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, int16_t, const MethodInfo*))Collection_1_Insert_m1531032532_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2731761781_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, int16_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2731761781(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, int16_t, const MethodInfo*))Collection_1_InsertItem_m2731761781_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::Remove(T)
extern "C"  bool Collection_1_Remove_m2199085426_gshared (Collection_1_t3582990668 * __this, int16_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2199085426(__this, ___item0, method) ((  bool (*) (Collection_1_t3582990668 *, int16_t, const MethodInfo*))Collection_1_Remove_m2199085426_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m237753320_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m237753320(__this, ___index0, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m237753320_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2905118214_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2905118214(__this, ___index0, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2905118214_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int16>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1971160830_gshared (Collection_1_t3582990668 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1971160830(__this, method) ((  int32_t (*) (Collection_1_t3582990668 *, const MethodInfo*))Collection_1_get_Count_m1971160830_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int16>::get_Item(System.Int32)
extern "C"  int16_t Collection_1_get_Item_m3748079036_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3748079036(__this, ___index0, method) ((  int16_t (*) (Collection_1_t3582990668 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3748079036_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3405687107_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, int16_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3405687107(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, int16_t, const MethodInfo*))Collection_1_set_Item_m3405687107_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4061974224_gshared (Collection_1_t3582990668 * __this, int32_t ___index0, int16_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4061974224(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3582990668 *, int32_t, int16_t, const MethodInfo*))Collection_1_SetItem_m4061974224_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m371437049_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m371437049(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m371437049_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Int16>::ConvertItem(System.Object)
extern "C"  int16_t Collection_1_ConvertItem_m688133573_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m688133573(__this /* static, unused */, ___item0, method) ((  int16_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m688133573_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int16>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1469276017_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1469276017(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1469276017_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1120999073_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1120999073(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1120999073_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int16>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m793270432_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m793270432(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m793270432_gshared)(__this /* static, unused */, ___list0, method)
