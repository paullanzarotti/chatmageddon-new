﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DataReceivedEventHandler
struct DataReceivedEventHandler_t426056147;
// System.Object
struct Il2CppObject;
// System.Diagnostics.DataReceivedEventArgs
struct DataReceivedEventArgs_t3302745294;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_System_Diagnostics_DataReceivedEventArgs3302745294.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Diagnostics.DataReceivedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DataReceivedEventHandler__ctor_m971090156 (DataReceivedEventHandler_t426056147 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DataReceivedEventHandler::Invoke(System.Object,System.Diagnostics.DataReceivedEventArgs)
extern "C"  void DataReceivedEventHandler_Invoke_m3597138139 (DataReceivedEventHandler_t426056147 * __this, Il2CppObject * ___sender0, DataReceivedEventArgs_t3302745294 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Diagnostics.DataReceivedEventHandler::BeginInvoke(System.Object,System.Diagnostics.DataReceivedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DataReceivedEventHandler_BeginInvoke_m1825477090 (DataReceivedEventHandler_t426056147 * __this, Il2CppObject * ___sender0, DataReceivedEventArgs_t3302745294 * ___e1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DataReceivedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void DataReceivedEventHandler_EndInvoke_m396886262 (DataReceivedEventHandler_t426056147 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
