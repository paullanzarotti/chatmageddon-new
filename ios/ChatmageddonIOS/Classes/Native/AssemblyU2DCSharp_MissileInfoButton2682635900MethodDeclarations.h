﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissileInfoButton
struct MissileInfoButton_t2682635900;

#include "codegen/il2cpp-codegen.h"

// System.Void MissileInfoButton::.ctor()
extern "C"  void MissileInfoButton__ctor_m3042700411 (MissileInfoButton_t2682635900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileInfoButton::OnButtonReset()
extern "C"  void MissileInfoButton_OnButtonReset_m2864234669 (MissileInfoButton_t2682635900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissileInfoButton::OnButtonRotate()
extern "C"  void MissileInfoButton_OnButtonRotate_m3348844461 (MissileInfoButton_t2682635900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
