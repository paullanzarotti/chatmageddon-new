﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPUserInteraction
struct IPUserInteraction_t4192479194;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void IPUserInteraction::.ctor()
extern "C"  void IPUserInteraction__ctor_m1073565605 (IPUserInteraction_t4192479194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction::OnPress(System.Boolean)
extern "C"  void IPUserInteraction_OnPress_m3064362168 (IPUserInteraction_t4192479194 * __this, bool ___press0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction::OnScroll(System.Single)
extern "C"  void IPUserInteraction_OnScroll_m2113508510 (IPUserInteraction_t4192479194 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction::ScrollStartOrStop(System.Boolean)
extern "C"  void IPUserInteraction_ScrollStartOrStop_m3193876326 (IPUserInteraction_t4192479194 * __this, bool ___start0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction::OnClick()
extern "C"  void IPUserInteraction_OnClick_m3078506358 (IPUserInteraction_t4192479194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPUserInteraction::OnDrag()
extern "C"  void IPUserInteraction_OnDrag_m553603330 (IPUserInteraction_t4192479194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator IPUserInteraction::DelayedDragExit()
extern "C"  Il2CppObject * IPUserInteraction_DelayedDragExit_m852311919 (IPUserInteraction_t4192479194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
