﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_CryptoEx2532638102.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_DataLengt677974397.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_InvalidCi962650396.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_KeyGenera650995725.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_MaxBytes2161670937.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_OutputLe3343085798.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_PbeParame148045164.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Agreemen3319300478.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Agreemen2812231168.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_3607278510.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_1877917918.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_2852052464.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_M144774215.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_M144985097.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_M144945066.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_N368343707.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_2796683682.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_3851284614.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_R951608910.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_2794586136.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_2812698497.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_3187179275.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_2023181968.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_2397707127.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_1326945845.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_4198631692.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_3671869576.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_1492537033.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Digests_1623533772.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo4042549922.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom554492283.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom807343051.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo4096517136.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo4096104871.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo3567932696.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo3251660359.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo3250774544.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1968921233.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1968508968.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo2797881180.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo2797536565.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1079655843.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1554171410.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom711183863.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1239356038.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1541260227.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo2092188222.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom710268829.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom709856564.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1238028739.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo2417012719.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo2416676552.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom822399437.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo2406823542.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo2405878525.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom343708266.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custom343304449.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1705265402.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_EC_Custo1704929235.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Encodings408740108.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Encodings419752442.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Encoding2136560189.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1294214581.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2923690633.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1837061725.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2369378670.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1360378068.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_4087146868.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2024773626.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2024946905.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2060435280.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2644224304.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_4248411376.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1307100856.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_E291645553.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2806286145.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1442846786.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1873260450.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_2343780579.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_1284744109.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_4198474181.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3848943229.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_R276302965.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3849006527.h"
#include "AssemblyU2DCSharp_Org_BouncyCastle_Crypto_Engines_3517208315.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (CryptoException_t2532638102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (DataLengthException_t677974397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (InvalidCipherTextException_t962650396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (KeyGenerationParameters_t650995725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3918[2] = 
{
	KeyGenerationParameters_t650995725::get_offset_of_random_0(),
	KeyGenerationParameters_t650995725::get_offset_of_strength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (MaxBytesExceededException_t2161670937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (OutputLengthException_t3343085798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (PbeParametersGenerator_t148045164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3921[3] = 
{
	PbeParametersGenerator_t148045164::get_offset_of_mPassword_0(),
	PbeParametersGenerator_t148045164::get_offset_of_mSalt_1(),
	PbeParametersGenerator_t148045164::get_offset_of_mIterationCount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (DHBasicAgreement_t3319300478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3922[2] = 
{
	DHBasicAgreement_t3319300478::get_offset_of_key_0(),
	DHBasicAgreement_t3319300478::get_offset_of_dhParams_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (ECDHBasicAgreement_t2812231168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3923[1] = 
{
	ECDHBasicAgreement_t2812231168::get_offset_of_privKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (Gost3411Digest_t3607278510), -1, sizeof(Gost3411Digest_t3607278510_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3924[20] = 
{
	0,
	Gost3411Digest_t3607278510::get_offset_of_H_1(),
	Gost3411Digest_t3607278510::get_offset_of_L_2(),
	Gost3411Digest_t3607278510::get_offset_of_M_3(),
	Gost3411Digest_t3607278510::get_offset_of_Sum_4(),
	Gost3411Digest_t3607278510::get_offset_of_C_5(),
	Gost3411Digest_t3607278510::get_offset_of_xBuf_6(),
	Gost3411Digest_t3607278510::get_offset_of_xBufOff_7(),
	Gost3411Digest_t3607278510::get_offset_of_byteCount_8(),
	Gost3411Digest_t3607278510::get_offset_of_cipher_9(),
	Gost3411Digest_t3607278510::get_offset_of_sBox_10(),
	Gost3411Digest_t3607278510::get_offset_of_K_11(),
	Gost3411Digest_t3607278510::get_offset_of_a_12(),
	Gost3411Digest_t3607278510::get_offset_of_wS_13(),
	Gost3411Digest_t3607278510::get_offset_of_w_S_14(),
	Gost3411Digest_t3607278510::get_offset_of_S_15(),
	Gost3411Digest_t3607278510::get_offset_of_U_16(),
	Gost3411Digest_t3607278510::get_offset_of_V_17(),
	Gost3411Digest_t3607278510::get_offset_of_W_18(),
	Gost3411Digest_t3607278510_StaticFields::get_offset_of_C2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (GeneralDigest_t1877917918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[4] = 
{
	0,
	GeneralDigest_t1877917918::get_offset_of_xBuf_1(),
	GeneralDigest_t1877917918::get_offset_of_xBufOff_2(),
	GeneralDigest_t1877917918::get_offset_of_byteCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (LongDigest_t2852052464), -1, sizeof(LongDigest_t2852052464_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3926[16] = 
{
	LongDigest_t2852052464::get_offset_of_MyByteLength_0(),
	LongDigest_t2852052464::get_offset_of_xBuf_1(),
	LongDigest_t2852052464::get_offset_of_xBufOff_2(),
	LongDigest_t2852052464::get_offset_of_byteCount1_3(),
	LongDigest_t2852052464::get_offset_of_byteCount2_4(),
	LongDigest_t2852052464::get_offset_of_H1_5(),
	LongDigest_t2852052464::get_offset_of_H2_6(),
	LongDigest_t2852052464::get_offset_of_H3_7(),
	LongDigest_t2852052464::get_offset_of_H4_8(),
	LongDigest_t2852052464::get_offset_of_H5_9(),
	LongDigest_t2852052464::get_offset_of_H6_10(),
	LongDigest_t2852052464::get_offset_of_H7_11(),
	LongDigest_t2852052464::get_offset_of_H8_12(),
	LongDigest_t2852052464::get_offset_of_W_13(),
	LongDigest_t2852052464::get_offset_of_wOff_14(),
	LongDigest_t2852052464_StaticFields::get_offset_of_K_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (MD2Digest_t144774215), -1, sizeof(MD2Digest_t144774215_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3927[9] = 
{
	0,
	0,
	MD2Digest_t144774215::get_offset_of_X_2(),
	MD2Digest_t144774215::get_offset_of_xOff_3(),
	MD2Digest_t144774215::get_offset_of_M_4(),
	MD2Digest_t144774215::get_offset_of_mOff_5(),
	MD2Digest_t144774215::get_offset_of_C_6(),
	MD2Digest_t144774215::get_offset_of_COff_7(),
	MD2Digest_t144774215_StaticFields::get_offset_of_S_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (MD4Digest_t144985097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3928[19] = 
{
	0,
	MD4Digest_t144985097::get_offset_of_H1_5(),
	MD4Digest_t144985097::get_offset_of_H2_6(),
	MD4Digest_t144985097::get_offset_of_H3_7(),
	MD4Digest_t144985097::get_offset_of_H4_8(),
	MD4Digest_t144985097::get_offset_of_X_9(),
	MD4Digest_t144985097::get_offset_of_xOff_10(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (MD5Digest_t144945066), -1, sizeof(MD5Digest_t144945066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3929[23] = 
{
	0,
	MD5Digest_t144945066::get_offset_of_H1_5(),
	MD5Digest_t144945066::get_offset_of_H2_6(),
	MD5Digest_t144945066::get_offset_of_H3_7(),
	MD5Digest_t144945066::get_offset_of_H4_8(),
	MD5Digest_t144945066::get_offset_of_X_9(),
	MD5Digest_t144945066::get_offset_of_xOff_10(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S11_11(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S12_12(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S13_13(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S14_14(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S21_15(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S22_16(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S23_17(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S24_18(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S31_19(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S32_20(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S33_21(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S34_22(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S41_23(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S42_24(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S43_25(),
	MD5Digest_t144945066_StaticFields::get_offset_of_S44_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (NullDigest_t368343707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3930[1] = 
{
	NullDigest_t368343707::get_offset_of_bOut_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (RipeMD128Digest_t2796683682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3931[7] = 
{
	0,
	RipeMD128Digest_t2796683682::get_offset_of_H0_5(),
	RipeMD128Digest_t2796683682::get_offset_of_H1_6(),
	RipeMD128Digest_t2796683682::get_offset_of_H2_7(),
	RipeMD128Digest_t2796683682::get_offset_of_H3_8(),
	RipeMD128Digest_t2796683682::get_offset_of_X_9(),
	RipeMD128Digest_t2796683682::get_offset_of_xOff_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (RipeMD160Digest_t3851284614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3932[8] = 
{
	0,
	RipeMD160Digest_t3851284614::get_offset_of_H0_5(),
	RipeMD160Digest_t3851284614::get_offset_of_H1_6(),
	RipeMD160Digest_t3851284614::get_offset_of_H2_7(),
	RipeMD160Digest_t3851284614::get_offset_of_H3_8(),
	RipeMD160Digest_t3851284614::get_offset_of_H4_9(),
	RipeMD160Digest_t3851284614::get_offset_of_X_10(),
	RipeMD160Digest_t3851284614::get_offset_of_xOff_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (RipeMD256Digest_t951608910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3933[11] = 
{
	0,
	RipeMD256Digest_t951608910::get_offset_of_H0_5(),
	RipeMD256Digest_t951608910::get_offset_of_H1_6(),
	RipeMD256Digest_t951608910::get_offset_of_H2_7(),
	RipeMD256Digest_t951608910::get_offset_of_H3_8(),
	RipeMD256Digest_t951608910::get_offset_of_H4_9(),
	RipeMD256Digest_t951608910::get_offset_of_H5_10(),
	RipeMD256Digest_t951608910::get_offset_of_H6_11(),
	RipeMD256Digest_t951608910::get_offset_of_H7_12(),
	RipeMD256Digest_t951608910::get_offset_of_X_13(),
	RipeMD256Digest_t951608910::get_offset_of_xOff_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (RipeMD320Digest_t2794586136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3934[13] = 
{
	0,
	RipeMD320Digest_t2794586136::get_offset_of_H0_5(),
	RipeMD320Digest_t2794586136::get_offset_of_H1_6(),
	RipeMD320Digest_t2794586136::get_offset_of_H2_7(),
	RipeMD320Digest_t2794586136::get_offset_of_H3_8(),
	RipeMD320Digest_t2794586136::get_offset_of_H4_9(),
	RipeMD320Digest_t2794586136::get_offset_of_H5_10(),
	RipeMD320Digest_t2794586136::get_offset_of_H6_11(),
	RipeMD320Digest_t2794586136::get_offset_of_H7_12(),
	RipeMD320Digest_t2794586136::get_offset_of_H8_13(),
	RipeMD320Digest_t2794586136::get_offset_of_H9_14(),
	RipeMD320Digest_t2794586136::get_offset_of_X_15(),
	RipeMD320Digest_t2794586136::get_offset_of_xOff_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (Sha3Digest_t2812698497), -1, sizeof(Sha3Digest_t2812698497_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3935[14] = 
{
	Sha3Digest_t2812698497_StaticFields::get_offset_of_KeccakRoundConstants_0(),
	Sha3Digest_t2812698497_StaticFields::get_offset_of_KeccakRhoOffsets_1(),
	Sha3Digest_t2812698497::get_offset_of_state_2(),
	Sha3Digest_t2812698497::get_offset_of_dataQueue_3(),
	Sha3Digest_t2812698497::get_offset_of_rate_4(),
	Sha3Digest_t2812698497::get_offset_of_bitsInQueue_5(),
	Sha3Digest_t2812698497::get_offset_of_fixedOutputLength_6(),
	Sha3Digest_t2812698497::get_offset_of_squeezing_7(),
	Sha3Digest_t2812698497::get_offset_of_bitsAvailableForSqueezing_8(),
	Sha3Digest_t2812698497::get_offset_of_chunk_9(),
	Sha3Digest_t2812698497::get_offset_of_oneByte_10(),
	Sha3Digest_t2812698497::get_offset_of_C_11(),
	Sha3Digest_t2812698497::get_offset_of_tempA_12(),
	Sha3Digest_t2812698497::get_offset_of_chiC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (Sha1Digest_t3187179275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3936[12] = 
{
	0,
	Sha1Digest_t3187179275::get_offset_of_H1_5(),
	Sha1Digest_t3187179275::get_offset_of_H2_6(),
	Sha1Digest_t3187179275::get_offset_of_H3_7(),
	Sha1Digest_t3187179275::get_offset_of_H4_8(),
	Sha1Digest_t3187179275::get_offset_of_H5_9(),
	Sha1Digest_t3187179275::get_offset_of_X_10(),
	Sha1Digest_t3187179275::get_offset_of_xOff_11(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (Sha224Digest_t2023181968), -1, sizeof(Sha224Digest_t2023181968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3937[12] = 
{
	0,
	Sha224Digest_t2023181968::get_offset_of_H1_5(),
	Sha224Digest_t2023181968::get_offset_of_H2_6(),
	Sha224Digest_t2023181968::get_offset_of_H3_7(),
	Sha224Digest_t2023181968::get_offset_of_H4_8(),
	Sha224Digest_t2023181968::get_offset_of_H5_9(),
	Sha224Digest_t2023181968::get_offset_of_H6_10(),
	Sha224Digest_t2023181968::get_offset_of_H7_11(),
	Sha224Digest_t2023181968::get_offset_of_H8_12(),
	Sha224Digest_t2023181968::get_offset_of_X_13(),
	Sha224Digest_t2023181968::get_offset_of_xOff_14(),
	Sha224Digest_t2023181968_StaticFields::get_offset_of_K_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (Sha256Digest_t2397707127), -1, sizeof(Sha256Digest_t2397707127_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3938[12] = 
{
	0,
	Sha256Digest_t2397707127::get_offset_of_H1_5(),
	Sha256Digest_t2397707127::get_offset_of_H2_6(),
	Sha256Digest_t2397707127::get_offset_of_H3_7(),
	Sha256Digest_t2397707127::get_offset_of_H4_8(),
	Sha256Digest_t2397707127::get_offset_of_H5_9(),
	Sha256Digest_t2397707127::get_offset_of_H6_10(),
	Sha256Digest_t2397707127::get_offset_of_H7_11(),
	Sha256Digest_t2397707127::get_offset_of_H8_12(),
	Sha256Digest_t2397707127::get_offset_of_X_13(),
	Sha256Digest_t2397707127::get_offset_of_xOff_14(),
	Sha256Digest_t2397707127_StaticFields::get_offset_of_K_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (Sha384Digest_t1326945845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3939[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (Sha512Digest_t4198631692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3940[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (Sha512tDigest_t3671869576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[10] = 
{
	0,
	Sha512tDigest_t3671869576::get_offset_of_digestLength_17(),
	Sha512tDigest_t3671869576::get_offset_of_H1t_18(),
	Sha512tDigest_t3671869576::get_offset_of_H2t_19(),
	Sha512tDigest_t3671869576::get_offset_of_H3t_20(),
	Sha512tDigest_t3671869576::get_offset_of_H4t_21(),
	Sha512tDigest_t3671869576::get_offset_of_H5t_22(),
	Sha512tDigest_t3671869576::get_offset_of_H6t_23(),
	Sha512tDigest_t3671869576::get_offset_of_H7t_24(),
	Sha512tDigest_t3671869576::get_offset_of_H8t_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (TigerDigest_t1492537033), -1, sizeof(TigerDigest_t1492537033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3942[14] = 
{
	0,
	TigerDigest_t1492537033_StaticFields::get_offset_of_t1_1(),
	TigerDigest_t1492537033_StaticFields::get_offset_of_t2_2(),
	TigerDigest_t1492537033_StaticFields::get_offset_of_t3_3(),
	TigerDigest_t1492537033_StaticFields::get_offset_of_t4_4(),
	0,
	TigerDigest_t1492537033::get_offset_of_a_6(),
	TigerDigest_t1492537033::get_offset_of_b_7(),
	TigerDigest_t1492537033::get_offset_of_c_8(),
	TigerDigest_t1492537033::get_offset_of_byteCount_9(),
	TigerDigest_t1492537033::get_offset_of_Buffer_10(),
	TigerDigest_t1492537033::get_offset_of_bOff_11(),
	TigerDigest_t1492537033::get_offset_of_x_12(),
	TigerDigest_t1492537033::get_offset_of_xOff_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (WhirlpoolDigest_t1623533772), -1, sizeof(WhirlpoolDigest_t1623533772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3943[24] = 
{
	0,
	0,
	0,
	0,
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_SBOX_4(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C0_5(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C1_6(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C2_7(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C3_8(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C4_9(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C5_10(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C6_11(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_C7_12(),
	WhirlpoolDigest_t1623533772::get_offset_of__rc_13(),
	WhirlpoolDigest_t1623533772_StaticFields::get_offset_of_EIGHT_14(),
	0,
	WhirlpoolDigest_t1623533772::get_offset_of__buffer_16(),
	WhirlpoolDigest_t1623533772::get_offset_of__bufferPos_17(),
	WhirlpoolDigest_t1623533772::get_offset_of__bitCount_18(),
	WhirlpoolDigest_t1623533772::get_offset_of__hash_19(),
	WhirlpoolDigest_t1623533772::get_offset_of__K_20(),
	WhirlpoolDigest_t1623533772::get_offset_of__L_21(),
	WhirlpoolDigest_t1623533772::get_offset_of__block_22(),
	WhirlpoolDigest_t1623533772::get_offset_of__state_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (CustomNamedCurves_t4042549922), -1, sizeof(CustomNamedCurves_t4042549922_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3944[5] = 
{
	CustomNamedCurves_t4042549922_StaticFields::get_offset_of_nameToCurve_0(),
	CustomNamedCurves_t4042549922_StaticFields::get_offset_of_nameToOid_1(),
	CustomNamedCurves_t4042549922_StaticFields::get_offset_of_oidToCurve_2(),
	CustomNamedCurves_t4042549922_StaticFields::get_offset_of_oidToName_3(),
	CustomNamedCurves_t4042549922_StaticFields::get_offset_of_names_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (Curve25519Holder_t554492283), -1, sizeof(Curve25519Holder_t554492283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3945[1] = 
{
	Curve25519Holder_t554492283_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (SecP128R1Holder_t807343051), -1, sizeof(SecP128R1Holder_t807343051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3946[1] = 
{
	SecP128R1Holder_t807343051_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (SecP160K1Holder_t4096517136), -1, sizeof(SecP160K1Holder_t4096517136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3947[1] = 
{
	SecP160K1Holder_t4096517136_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (SecP160R1Holder_t4096104871), -1, sizeof(SecP160R1Holder_t4096104871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3948[1] = 
{
	SecP160R1Holder_t4096104871_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (SecP160R2Holder_t3567932696), -1, sizeof(SecP160R2Holder_t3567932696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3949[1] = 
{
	SecP160R2Holder_t3567932696_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (SecP192K1Holder_t3251660359), -1, sizeof(SecP192K1Holder_t3251660359_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3950[1] = 
{
	SecP192K1Holder_t3251660359_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (SecP192R1Holder_t3250774544), -1, sizeof(SecP192R1Holder_t3250774544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3951[1] = 
{
	SecP192R1Holder_t3250774544_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (SecP224K1Holder_t1968921233), -1, sizeof(SecP224K1Holder_t1968921233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3952[1] = 
{
	SecP224K1Holder_t1968921233_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (SecP224R1Holder_t1968508968), -1, sizeof(SecP224R1Holder_t1968508968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3953[1] = 
{
	SecP224R1Holder_t1968508968_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (SecP256K1Holder_t2797881180), -1, sizeof(SecP256K1Holder_t2797881180_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3954[1] = 
{
	SecP256K1Holder_t2797881180_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (SecP256R1Holder_t2797536565), -1, sizeof(SecP256R1Holder_t2797536565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3955[1] = 
{
	SecP256R1Holder_t2797536565_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (SecP384R1Holder_t1079655843), -1, sizeof(SecP384R1Holder_t1079655843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3956[1] = 
{
	SecP384R1Holder_t1079655843_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (SecP521R1Holder_t1554171410), -1, sizeof(SecP521R1Holder_t1554171410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3957[1] = 
{
	SecP521R1Holder_t1554171410_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (SecT113R1Holder_t711183863), -1, sizeof(SecT113R1Holder_t711183863_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3958[1] = 
{
	SecT113R1Holder_t711183863_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (SecT113R2Holder_t1239356038), -1, sizeof(SecT113R2Holder_t1239356038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3959[1] = 
{
	SecT113R2Holder_t1239356038_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (SecT131R1Holder_t1541260227), -1, sizeof(SecT131R1Holder_t1541260227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3960[1] = 
{
	SecT131R1Holder_t1541260227_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (SecT131R2Holder_t2092188222), -1, sizeof(SecT131R2Holder_t2092188222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3961[1] = 
{
	SecT131R2Holder_t2092188222_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (SecT163K1Holder_t710268829), -1, sizeof(SecT163K1Holder_t710268829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3962[1] = 
{
	SecT163K1Holder_t710268829_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (SecT163R1Holder_t709856564), -1, sizeof(SecT163R1Holder_t709856564_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3963[1] = 
{
	SecT163R1Holder_t709856564_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (SecT163R2Holder_t1238028739), -1, sizeof(SecT163R2Holder_t1238028739_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3964[1] = 
{
	SecT163R2Holder_t1238028739_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (SecT233K1Holder_t2417012719), -1, sizeof(SecT233K1Holder_t2417012719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3965[1] = 
{
	SecT233K1Holder_t2417012719_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (SecT233R1Holder_t2416676552), -1, sizeof(SecT233R1Holder_t2416676552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3966[1] = 
{
	SecT233R1Holder_t2416676552_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (SecT239K1Holder_t822399437), -1, sizeof(SecT239K1Holder_t822399437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3967[1] = 
{
	SecT239K1Holder_t822399437_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (SecT283K1Holder_t2406823542), -1, sizeof(SecT283K1Holder_t2406823542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3968[1] = 
{
	SecT283K1Holder_t2406823542_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (SecT283R1Holder_t2405878525), -1, sizeof(SecT283R1Holder_t2405878525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3969[1] = 
{
	SecT283R1Holder_t2405878525_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (SecT409K1Holder_t343708266), -1, sizeof(SecT409K1Holder_t343708266_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3970[1] = 
{
	SecT409K1Holder_t343708266_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (SecT409R1Holder_t343304449), -1, sizeof(SecT409R1Holder_t343304449_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3971[1] = 
{
	SecT409R1Holder_t343304449_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (SecT571K1Holder_t1705265402), -1, sizeof(SecT571K1Holder_t1705265402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3972[1] = 
{
	SecT571K1Holder_t1705265402_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (SecT571R1Holder_t1704929235), -1, sizeof(SecT571R1Holder_t1704929235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3973[1] = 
{
	SecT571R1Holder_t1704929235_StaticFields::get_offset_of_Instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (ISO9796d1Encoding_t408740108), -1, sizeof(ISO9796d1Encoding_t408740108_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3974[9] = 
{
	ISO9796d1Encoding_t408740108_StaticFields::get_offset_of_Sixteen_0(),
	ISO9796d1Encoding_t408740108_StaticFields::get_offset_of_Six_1(),
	ISO9796d1Encoding_t408740108_StaticFields::get_offset_of_shadows_2(),
	ISO9796d1Encoding_t408740108_StaticFields::get_offset_of_inverse_3(),
	ISO9796d1Encoding_t408740108::get_offset_of_engine_4(),
	ISO9796d1Encoding_t408740108::get_offset_of_forEncryption_5(),
	ISO9796d1Encoding_t408740108::get_offset_of_bitSize_6(),
	ISO9796d1Encoding_t408740108::get_offset_of_padBits_7(),
	ISO9796d1Encoding_t408740108::get_offset_of_modulus_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (OaepEncoding_t419752442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3975[6] = 
{
	OaepEncoding_t419752442::get_offset_of_defHash_0(),
	OaepEncoding_t419752442::get_offset_of_hash_1(),
	OaepEncoding_t419752442::get_offset_of_mgf1Hash_2(),
	OaepEncoding_t419752442::get_offset_of_engine_3(),
	OaepEncoding_t419752442::get_offset_of_random_4(),
	OaepEncoding_t419752442::get_offset_of_forEncryption_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (Pkcs1Encoding_t2136560189), -1, sizeof(Pkcs1Encoding_t2136560189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3976[10] = 
{
	0,
	0,
	Pkcs1Encoding_t2136560189_StaticFields::get_offset_of_strictLengthEnabled_2(),
	Pkcs1Encoding_t2136560189::get_offset_of_random_3(),
	Pkcs1Encoding_t2136560189::get_offset_of_engine_4(),
	Pkcs1Encoding_t2136560189::get_offset_of_forEncryption_5(),
	Pkcs1Encoding_t2136560189::get_offset_of_forPrivateKey_6(),
	Pkcs1Encoding_t2136560189::get_offset_of_useStrictLength_7(),
	Pkcs1Encoding_t2136560189::get_offset_of_pLen_8(),
	Pkcs1Encoding_t2136560189::get_offset_of_fallback_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (AesEngine_t1294214581), -1, sizeof(AesEngine_t1294214581_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3977[16] = 
{
	AesEngine_t1294214581_StaticFields::get_offset_of_S_0(),
	AesEngine_t1294214581_StaticFields::get_offset_of_Si_1(),
	AesEngine_t1294214581_StaticFields::get_offset_of_rcon_2(),
	AesEngine_t1294214581_StaticFields::get_offset_of_T0_3(),
	AesEngine_t1294214581_StaticFields::get_offset_of_Tinv0_4(),
	0,
	0,
	0,
	AesEngine_t1294214581::get_offset_of_ROUNDS_8(),
	AesEngine_t1294214581::get_offset_of_WorkingKey_9(),
	AesEngine_t1294214581::get_offset_of_C0_10(),
	AesEngine_t1294214581::get_offset_of_C1_11(),
	AesEngine_t1294214581::get_offset_of_C2_12(),
	AesEngine_t1294214581::get_offset_of_C3_13(),
	AesEngine_t1294214581::get_offset_of_forEncryption_14(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (AesFastEngine_t2923690633), -1, sizeof(AesFastEngine_t2923690633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3978[22] = 
{
	AesFastEngine_t2923690633_StaticFields::get_offset_of_S_0(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_Si_1(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_rcon_2(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_T0_3(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_T1_4(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_T2_5(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_T3_6(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_Tinv0_7(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_Tinv1_8(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_Tinv2_9(),
	AesFastEngine_t2923690633_StaticFields::get_offset_of_Tinv3_10(),
	0,
	0,
	0,
	AesFastEngine_t2923690633::get_offset_of_ROUNDS_14(),
	AesFastEngine_t2923690633::get_offset_of_WorkingKey_15(),
	AesFastEngine_t2923690633::get_offset_of_C0_16(),
	AesFastEngine_t2923690633::get_offset_of_C1_17(),
	AesFastEngine_t2923690633::get_offset_of_C2_18(),
	AesFastEngine_t2923690633::get_offset_of_C3_19(),
	AesFastEngine_t2923690633::get_offset_of_forEncryption_20(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (AesWrapEngine_t1837061725), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (BlowfishEngine_t2369378670), -1, sizeof(BlowfishEngine_t2369378670_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3980[16] = 
{
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_KP_0(),
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_KS0_1(),
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_KS1_2(),
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_KS2_3(),
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_KS3_4(),
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_ROUNDS_5(),
	0,
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_SBOX_SK_7(),
	BlowfishEngine_t2369378670_StaticFields::get_offset_of_P_SZ_8(),
	BlowfishEngine_t2369378670::get_offset_of_S0_9(),
	BlowfishEngine_t2369378670::get_offset_of_S1_10(),
	BlowfishEngine_t2369378670::get_offset_of_S2_11(),
	BlowfishEngine_t2369378670::get_offset_of_S3_12(),
	BlowfishEngine_t2369378670::get_offset_of_P_13(),
	BlowfishEngine_t2369378670::get_offset_of_encrypting_14(),
	BlowfishEngine_t2369378670::get_offset_of_workingKey_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (CamelliaEngine_t1360378068), -1, sizeof(CamelliaEngine_t1360378068_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3981[12] = 
{
	CamelliaEngine_t1360378068::get_offset_of_initialised_0(),
	CamelliaEngine_t1360378068::get_offset_of__keyIs128_1(),
	0,
	CamelliaEngine_t1360378068::get_offset_of_subkey_3(),
	CamelliaEngine_t1360378068::get_offset_of_kw_4(),
	CamelliaEngine_t1360378068::get_offset_of_ke_5(),
	CamelliaEngine_t1360378068::get_offset_of_state_6(),
	CamelliaEngine_t1360378068_StaticFields::get_offset_of_SIGMA_7(),
	CamelliaEngine_t1360378068_StaticFields::get_offset_of_SBOX1_1110_8(),
	CamelliaEngine_t1360378068_StaticFields::get_offset_of_SBOX4_4404_9(),
	CamelliaEngine_t1360378068_StaticFields::get_offset_of_SBOX2_0222_10(),
	CamelliaEngine_t1360378068_StaticFields::get_offset_of_SBOX3_3033_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (CamelliaWrapEngine_t4087146868), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (Cast5Engine_t2024773626), -1, sizeof(Cast5Engine_t2024773626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3983[16] = 
{
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S1_0(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S2_1(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S3_2(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S4_3(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S5_4(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S6_5(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S7_6(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_S8_7(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_MAX_ROUNDS_8(),
	Cast5Engine_t2024773626_StaticFields::get_offset_of_RED_ROUNDS_9(),
	0,
	Cast5Engine_t2024773626::get_offset_of__Kr_11(),
	Cast5Engine_t2024773626::get_offset_of__Km_12(),
	Cast5Engine_t2024773626::get_offset_of__encrypting_13(),
	Cast5Engine_t2024773626::get_offset_of__workingKey_14(),
	Cast5Engine_t2024773626::get_offset_of__rounds_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (Cast6Engine_t2024946905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3984[7] = 
{
	0,
	0,
	Cast6Engine_t2024946905::get_offset_of__Kr_18(),
	Cast6Engine_t2024946905::get_offset_of__Km_19(),
	Cast6Engine_t2024946905::get_offset_of__Tr_20(),
	Cast6Engine_t2024946905::get_offset_of__Tm_21(),
	Cast6Engine_t2024946905::get_offset_of__workingKey_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (ChaChaEngine_t2060435280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (DesEdeEngine_t2644224304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3986[4] = 
{
	DesEdeEngine_t2644224304::get_offset_of_workingKey1_15(),
	DesEdeEngine_t2644224304::get_offset_of_workingKey2_16(),
	DesEdeEngine_t2644224304::get_offset_of_workingKey3_17(),
	DesEdeEngine_t2644224304::get_offset_of_forEncryption_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (DesEdeWrapEngine_t4248411376), -1, sizeof(DesEdeWrapEngine_t4248411376_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3987[8] = 
{
	DesEdeWrapEngine_t4248411376::get_offset_of_engine_0(),
	DesEdeWrapEngine_t4248411376::get_offset_of_param_1(),
	DesEdeWrapEngine_t4248411376::get_offset_of_paramPlusIV_2(),
	DesEdeWrapEngine_t4248411376::get_offset_of_iv_3(),
	DesEdeWrapEngine_t4248411376::get_offset_of_forWrapping_4(),
	DesEdeWrapEngine_t4248411376_StaticFields::get_offset_of_IV2_5(),
	DesEdeWrapEngine_t4248411376::get_offset_of_sha1_6(),
	DesEdeWrapEngine_t4248411376::get_offset_of_digest_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (DesEngine_t1307100856), -1, sizeof(DesEngine_t1307100856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3988[15] = 
{
	0,
	DesEngine_t1307100856::get_offset_of_workingKey_1(),
	DesEngine_t1307100856_StaticFields::get_offset_of_bytebit_2(),
	DesEngine_t1307100856_StaticFields::get_offset_of_bigbyte_3(),
	DesEngine_t1307100856_StaticFields::get_offset_of_pc1_4(),
	DesEngine_t1307100856_StaticFields::get_offset_of_totrot_5(),
	DesEngine_t1307100856_StaticFields::get_offset_of_pc2_6(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP1_7(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP2_8(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP3_9(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP4_10(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP5_11(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP6_12(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP7_13(),
	DesEngine_t1307100856_StaticFields::get_offset_of_SP8_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (ElGamalEngine_t291645553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3989[4] = 
{
	ElGamalEngine_t291645553::get_offset_of_key_0(),
	ElGamalEngine_t291645553::get_offset_of_random_1(),
	ElGamalEngine_t291645553::get_offset_of_forEncryption_2(),
	ElGamalEngine_t291645553::get_offset_of_bitSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (Gost28147Engine_t2806286145), -1, sizeof(Gost28147Engine_t2806286145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3990[13] = 
{
	0,
	Gost28147Engine_t2806286145::get_offset_of_workingKey_1(),
	Gost28147Engine_t2806286145::get_offset_of_forEncryption_2(),
	Gost28147Engine_t2806286145::get_offset_of_S_3(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_Sbox_Default_4(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_ESbox_Test_5(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_ESbox_A_6(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_ESbox_B_7(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_ESbox_C_8(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_ESbox_D_9(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_DSbox_Test_10(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_DSbox_A_11(),
	Gost28147Engine_t2806286145_StaticFields::get_offset_of_sBoxes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (HC128Engine_t1442846786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3991[8] = 
{
	HC128Engine_t1442846786::get_offset_of_p_0(),
	HC128Engine_t1442846786::get_offset_of_q_1(),
	HC128Engine_t1442846786::get_offset_of_cnt_2(),
	HC128Engine_t1442846786::get_offset_of_key_3(),
	HC128Engine_t1442846786::get_offset_of_iv_4(),
	HC128Engine_t1442846786::get_offset_of_initialised_5(),
	HC128Engine_t1442846786::get_offset_of_buf_6(),
	HC128Engine_t1442846786::get_offset_of_idx_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (HC256Engine_t1873260450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3992[8] = 
{
	HC256Engine_t1873260450::get_offset_of_p_0(),
	HC256Engine_t1873260450::get_offset_of_q_1(),
	HC256Engine_t1873260450::get_offset_of_cnt_2(),
	HC256Engine_t1873260450::get_offset_of_key_3(),
	HC256Engine_t1873260450::get_offset_of_iv_4(),
	HC256Engine_t1873260450::get_offset_of_initialised_5(),
	HC256Engine_t1873260450::get_offset_of_buf_6(),
	HC256Engine_t1873260450::get_offset_of_idx_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { sizeof (IdeaEngine_t2343780579), -1, sizeof(IdeaEngine_t2343780579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3993[4] = 
{
	0,
	IdeaEngine_t2343780579::get_offset_of_workingKey_1(),
	IdeaEngine_t2343780579_StaticFields::get_offset_of_MASK_2(),
	IdeaEngine_t2343780579_StaticFields::get_offset_of_BASE_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (IesEngine_t1284744109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3994[9] = 
{
	IesEngine_t1284744109::get_offset_of_agree_0(),
	IesEngine_t1284744109::get_offset_of_kdf_1(),
	IesEngine_t1284744109::get_offset_of_mac_2(),
	IesEngine_t1284744109::get_offset_of_cipher_3(),
	IesEngine_t1284744109::get_offset_of_macBuf_4(),
	IesEngine_t1284744109::get_offset_of_forEncryption_5(),
	IesEngine_t1284744109::get_offset_of_privParam_6(),
	IesEngine_t1284744109::get_offset_of_pubParam_7(),
	IesEngine_t1284744109::get_offset_of_param_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (NoekeonEngine_t4198474181), -1, sizeof(NoekeonEngine_t4198474181_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3995[8] = 
{
	0,
	NoekeonEngine_t4198474181_StaticFields::get_offset_of_nullVector_1(),
	NoekeonEngine_t4198474181_StaticFields::get_offset_of_roundConstants_2(),
	NoekeonEngine_t4198474181::get_offset_of_state_3(),
	NoekeonEngine_t4198474181::get_offset_of_subKeys_4(),
	NoekeonEngine_t4198474181::get_offset_of_decryptKeys_5(),
	NoekeonEngine_t4198474181::get_offset_of__initialised_6(),
	NoekeonEngine_t4198474181::get_offset_of__forEncryption_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (RC2Engine_t3848943229), -1, sizeof(RC2Engine_t3848943229_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3996[4] = 
{
	RC2Engine_t3848943229_StaticFields::get_offset_of_piTable_0(),
	0,
	RC2Engine_t3848943229::get_offset_of_workingKey_2(),
	RC2Engine_t3848943229::get_offset_of_encrypting_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (RC2WrapEngine_t276302965), -1, sizeof(RC2WrapEngine_t276302965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3997[9] = 
{
	RC2WrapEngine_t276302965::get_offset_of_engine_0(),
	RC2WrapEngine_t276302965::get_offset_of_parameters_1(),
	RC2WrapEngine_t276302965::get_offset_of_paramPlusIV_2(),
	RC2WrapEngine_t276302965::get_offset_of_iv_3(),
	RC2WrapEngine_t276302965::get_offset_of_forWrapping_4(),
	RC2WrapEngine_t276302965::get_offset_of_sr_5(),
	RC2WrapEngine_t276302965_StaticFields::get_offset_of_IV2_6(),
	RC2WrapEngine_t276302965::get_offset_of_sha1_7(),
	RC2WrapEngine_t276302965::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (RC4Engine_t3849006527), -1, sizeof(RC4Engine_t3849006527_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3998[5] = 
{
	RC4Engine_t3849006527_StaticFields::get_offset_of_STATE_LENGTH_0(),
	RC4Engine_t3849006527::get_offset_of_engineState_1(),
	RC4Engine_t3849006527::get_offset_of_x_2(),
	RC4Engine_t3849006527::get_offset_of_y_3(),
	RC4Engine_t3849006527::get_offset_of_workingKey_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (RC532Engine_t3517208315), -1, sizeof(RC532Engine_t3517208315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3999[5] = 
{
	RC532Engine_t3517208315::get_offset_of__noRounds_0(),
	RC532Engine_t3517208315::get_offset_of__S_1(),
	RC532Engine_t3517208315_StaticFields::get_offset_of_P32_2(),
	RC532Engine_t3517208315_StaticFields::get_offset_of_Q32_3(),
	RC532Engine_t3517208315::get_offset_of_forEncryption_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
