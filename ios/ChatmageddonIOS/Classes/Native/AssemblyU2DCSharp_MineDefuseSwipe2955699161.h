﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MineMiniGameController
struct MineMiniGameController_t2791970086;

#include "AssemblyU2DCSharp_NGUISwipe4135300327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineDefuseSwipe
struct  MineDefuseSwipe_t2955699161  : public NGUISwipe_t4135300327
{
public:
	// MineMiniGameController MineDefuseSwipe::gamecontroller
	MineMiniGameController_t2791970086 * ___gamecontroller_10;
	// System.Int32 MineDefuseSwipe::swipeNumber
	int32_t ___swipeNumber_11;

public:
	inline static int32_t get_offset_of_gamecontroller_10() { return static_cast<int32_t>(offsetof(MineDefuseSwipe_t2955699161, ___gamecontroller_10)); }
	inline MineMiniGameController_t2791970086 * get_gamecontroller_10() const { return ___gamecontroller_10; }
	inline MineMiniGameController_t2791970086 ** get_address_of_gamecontroller_10() { return &___gamecontroller_10; }
	inline void set_gamecontroller_10(MineMiniGameController_t2791970086 * value)
	{
		___gamecontroller_10 = value;
		Il2CppCodeGenWriteBarrier(&___gamecontroller_10, value);
	}

	inline static int32_t get_offset_of_swipeNumber_11() { return static_cast<int32_t>(offsetof(MineDefuseSwipe_t2955699161, ___swipeNumber_11)); }
	inline int32_t get_swipeNumber_11() const { return ___swipeNumber_11; }
	inline int32_t* get_address_of_swipeNumber_11() { return &___swipeNumber_11; }
	inline void set_swipeNumber_11(int32_t value)
	{
		___swipeNumber_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
