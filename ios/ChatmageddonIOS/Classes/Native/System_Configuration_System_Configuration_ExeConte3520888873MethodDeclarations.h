﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Configuration.ExeContext
struct ExeContext_t3520888873;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_Configuration_System_Configuration_Configur1204907851.h"

// System.Void System.Configuration.ExeContext::.ctor(System.String,System.Configuration.ConfigurationUserLevel)
extern "C"  void ExeContext__ctor_m4126525196 (ExeContext_t3520888873 * __this, String_t* ___path0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Configuration.ExeContext::get_ExePath()
extern "C"  String_t* ExeContext_get_ExePath_m2130101443 (ExeContext_t3520888873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.ConfigurationUserLevel System.Configuration.ExeContext::get_UserLevel()
extern "C"  int32_t ExeContext_get_UserLevel_m2478101141 (ExeContext_t3520888873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
