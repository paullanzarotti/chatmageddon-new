﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1636785907MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3310637601(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3959505041 *, Dictionary_2_t2639480339 *, const MethodInfo*))Enumerator__ctor_m426532933_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2363721780(__this, method) ((  Il2CppObject * (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1826349162_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3681930058(__this, method) ((  void (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3415398420_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4068257399(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1344016491_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1652130022(__this, method) ((  Il2CppObject * (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4146714620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2257904576(__this, method) ((  Il2CppObject * (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1681872826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::MoveNext()
#define Enumerator_MoveNext_m623304049(__this, method) ((  bool (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_MoveNext_m2045677544_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::get_Current()
#define Enumerator_get_Current_m156704782(__this, method) ((  KeyValuePair_2_t396825561  (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_get_Current_m2539709128_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1588536125(__this, method) ((  String_t* (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_get_CurrentKey_m3992455305_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3250266741(__this, method) ((  Decimal_t724701077  (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_get_CurrentValue_m599508961_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::Reset()
#define Enumerator_Reset_m3209543735(__this, method) ((  void (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_Reset_m652953371_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::VerifyState()
#define Enumerator_VerifyState_m3964723082(__this, method) ((  void (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_VerifyState_m3710756016_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m780674618(__this, method) ((  void (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_VerifyCurrent_m2312053988_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Decimal>::Dispose()
#define Enumerator_Dispose_m3441601561(__this, method) ((  void (*) (Enumerator_t3959505041 *, const MethodInfo*))Enumerator_Dispose_m1228939197_gshared)(__this, method)
