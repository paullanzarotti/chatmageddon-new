﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApplicationManager/<ScaleAndShow>c__Iterator1
struct  U3CScaleAndShowU3Ec__Iterator1_t3962984058  : public Il2CppObject
{
public:
	// UnityEngine.GameObject[] ApplicationManager/<ScaleAndShow>c__Iterator1::gameobjects
	GameObjectU5BU5D_t3057952154* ___gameobjects_0;
	// UnityEngine.GameObject[] ApplicationManager/<ScaleAndShow>c__Iterator1::$locvar0
	GameObjectU5BU5D_t3057952154* ___U24locvar0_1;
	// System.Int32 ApplicationManager/<ScaleAndShow>c__Iterator1::$locvar1
	int32_t ___U24locvar1_2;
	// UnityEngine.GameObject[] ApplicationManager/<ScaleAndShow>c__Iterator1::$locvar2
	GameObjectU5BU5D_t3057952154* ___U24locvar2_3;
	// System.Int32 ApplicationManager/<ScaleAndShow>c__Iterator1::$locvar3
	int32_t ___U24locvar3_4;
	// System.Single ApplicationManager/<ScaleAndShow>c__Iterator1::scaleTime
	float ___scaleTime_5;
	// System.Object ApplicationManager/<ScaleAndShow>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean ApplicationManager/<ScaleAndShow>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 ApplicationManager/<ScaleAndShow>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_gameobjects_0() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___gameobjects_0)); }
	inline GameObjectU5BU5D_t3057952154* get_gameobjects_0() const { return ___gameobjects_0; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_gameobjects_0() { return &___gameobjects_0; }
	inline void set_gameobjects_0(GameObjectU5BU5D_t3057952154* value)
	{
		___gameobjects_0 = value;
		Il2CppCodeGenWriteBarrier(&___gameobjects_0, value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___U24locvar0_1)); }
	inline GameObjectU5BU5D_t3057952154* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(GameObjectU5BU5D_t3057952154* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24locvar2_3() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___U24locvar2_3)); }
	inline GameObjectU5BU5D_t3057952154* get_U24locvar2_3() const { return ___U24locvar2_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_U24locvar2_3() { return &___U24locvar2_3; }
	inline void set_U24locvar2_3(GameObjectU5BU5D_t3057952154* value)
	{
		___U24locvar2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar2_3, value);
	}

	inline static int32_t get_offset_of_U24locvar3_4() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___U24locvar3_4)); }
	inline int32_t get_U24locvar3_4() const { return ___U24locvar3_4; }
	inline int32_t* get_address_of_U24locvar3_4() { return &___U24locvar3_4; }
	inline void set_U24locvar3_4(int32_t value)
	{
		___U24locvar3_4 = value;
	}

	inline static int32_t get_offset_of_scaleTime_5() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___scaleTime_5)); }
	inline float get_scaleTime_5() const { return ___scaleTime_5; }
	inline float* get_address_of_scaleTime_5() { return &___scaleTime_5; }
	inline void set_scaleTime_5(float value)
	{
		___scaleTime_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CScaleAndShowU3Ec__Iterator1_t3962984058, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
