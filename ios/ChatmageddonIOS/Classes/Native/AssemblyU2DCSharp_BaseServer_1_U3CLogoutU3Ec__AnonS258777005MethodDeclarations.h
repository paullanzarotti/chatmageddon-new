﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BaseServer`1/<Logout>c__AnonStoreyB<System.Object>
struct U3CLogoutU3Ec__AnonStoreyB_t258777005;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void BaseServer`1/<Logout>c__AnonStoreyB<System.Object>::.ctor()
extern "C"  void U3CLogoutU3Ec__AnonStoreyB__ctor_m2206561200_gshared (U3CLogoutU3Ec__AnonStoreyB_t258777005 * __this, const MethodInfo* method);
#define U3CLogoutU3Ec__AnonStoreyB__ctor_m2206561200(__this, method) ((  void (*) (U3CLogoutU3Ec__AnonStoreyB_t258777005 *, const MethodInfo*))U3CLogoutU3Ec__AnonStoreyB__ctor_m2206561200_gshared)(__this, method)
// System.Void BaseServer`1/<Logout>c__AnonStoreyB<System.Object>::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CLogoutU3Ec__AnonStoreyB_U3CU3Em__0_m2111821013_gshared (U3CLogoutU3Ec__AnonStoreyB_t258777005 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method);
#define U3CLogoutU3Ec__AnonStoreyB_U3CU3Em__0_m2111821013(__this, ___request0, ___response1, method) ((  void (*) (U3CLogoutU3Ec__AnonStoreyB_t258777005 *, HTTPRequest_t138485887 *, HTTPResponse_t62748825 *, const MethodInfo*))U3CLogoutU3Ec__AnonStoreyB_U3CU3Em__0_m2111821013_gshared)(__this, ___request0, ___response1, method)
