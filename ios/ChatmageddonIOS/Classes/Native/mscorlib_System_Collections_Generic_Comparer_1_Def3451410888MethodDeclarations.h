﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<RegNavScreen>
struct DefaultComparer_t3451410888;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_RegNavScreen667173359.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<RegNavScreen>::.ctor()
extern "C"  void DefaultComparer__ctor_m968540629_gshared (DefaultComparer_t3451410888 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m968540629(__this, method) ((  void (*) (DefaultComparer_t3451410888 *, const MethodInfo*))DefaultComparer__ctor_m968540629_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<RegNavScreen>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1044579394_gshared (DefaultComparer_t3451410888 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1044579394(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3451410888 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m1044579394_gshared)(__this, ___x0, ___y1, method)
