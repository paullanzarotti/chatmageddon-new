﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<TilesetFadeExampleItem>
struct List_1_t1351149695;
// System.Predicate`1<TilesetFadeExampleItem>
struct Predicate_1_t424998678;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TilesetFadeExample
struct  TilesetFadeExample_t3521105452  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<TilesetFadeExampleItem> TilesetFadeExample::items
	List_1_t1351149695 * ___items_2;

public:
	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(TilesetFadeExample_t3521105452, ___items_2)); }
	inline List_1_t1351149695 * get_items_2() const { return ___items_2; }
	inline List_1_t1351149695 ** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(List_1_t1351149695 * value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier(&___items_2, value);
	}
};

struct TilesetFadeExample_t3521105452_StaticFields
{
public:
	// System.Predicate`1<TilesetFadeExampleItem> TilesetFadeExample::<>f__am$cache0
	Predicate_1_t424998678 * ___U3CU3Ef__amU24cache0_3;
	// System.Predicate`1<TilesetFadeExampleItem> TilesetFadeExample::<>f__am$cache1
	Predicate_1_t424998678 * ___U3CU3Ef__amU24cache1_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(TilesetFadeExample_t3521105452_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Predicate_1_t424998678 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Predicate_1_t424998678 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Predicate_1_t424998678 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_4() { return static_cast<int32_t>(offsetof(TilesetFadeExample_t3521105452_StaticFields, ___U3CU3Ef__amU24cache1_4)); }
	inline Predicate_1_t424998678 * get_U3CU3Ef__amU24cache1_4() const { return ___U3CU3Ef__amU24cache1_4; }
	inline Predicate_1_t424998678 ** get_address_of_U3CU3Ef__amU24cache1_4() { return &___U3CU3Ef__amU24cache1_4; }
	inline void set_U3CU3Ef__amU24cache1_4(Predicate_1_t424998678 * value)
	{
		___U3CU3Ef__amU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
