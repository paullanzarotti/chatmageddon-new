﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SwipeNavigationController
struct SwipeNavigationController_t3767682136;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UILabel
struct UILabel_t1795115428;
// InventoryItemButton
struct InventoryItemButton_t3515382031;
// ItemInfoController
struct ItemInfoController_t4231956141;
// ItemSwipeController
struct ItemSwipeController_t3011021799;
// ShieldSlider
struct ShieldSlider_t1483508804;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Collections.Generic.List`1<PurchaseableItem>
struct List_1_t2720244128;
// UISprite
struct UISprite_t603616735;
// BottomButtonsController
struct BottomButtonsController_t3250435864;
// BuyItemButton
struct BuyItemButton_t2593913003;
// StoreTargetsButton
struct StoreTargetsButton_t3560946605;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// TweenScale
struct TweenScale_t2697902175;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen401442244.h"
#include "AssemblyU2DCSharp_PurchaseableCategory1933007175.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StoreManager
struct  StoreManager_t650776524  : public MonoSingleton_1_t401442244
{
public:
	// SwipeNavigationController StoreManager::categoryNavController
	SwipeNavigationController_t3767682136 * ___categoryNavController_3;
	// UnityEngine.GameObject StoreManager::infoObject
	GameObject_t1756533147 * ___infoObject_4;
	// UnityEngine.GameObject StoreManager::itemObject
	GameObject_t1756533147 * ___itemObject_5;
	// UnityEngine.GameObject StoreManager::lockedBackground
	GameObject_t1756533147 * ___lockedBackground_6;
	// UILabel StoreManager::lockedLabel
	UILabel_t1795115428 * ___lockedLabel_7;
	// UILabel StoreManager::bucksLabel
	UILabel_t1795115428 * ___bucksLabel_8;
	// InventoryItemButton StoreManager::infoButton
	InventoryItemButton_t3515382031 * ___infoButton_9;
	// ItemInfoController StoreManager::infoController
	ItemInfoController_t4231956141 * ___infoController_10;
	// ItemSwipeController StoreManager::swipeController
	ItemSwipeController_t3011021799 * ___swipeController_11;
	// ShieldSlider StoreManager::currentShieldSlider
	ShieldSlider_t1483508804 * ___currentShieldSlider_12;
	// UnityEngine.AnimationCurve StoreManager::curve
	AnimationCurve_t3306541151 * ___curve_13;
	// UnityEngine.GameObject[] StoreManager::missileSpecificItems
	GameObjectU5BU5D_t3057952154* ___missileSpecificItems_14;
	// UnityEngine.GameObject[] StoreManager::shieldHourSpecificItems
	GameObjectU5BU5D_t3057952154* ___shieldHourSpecificItems_15;
	// UnityEngine.GameObject[] StoreManager::shieldDaySpecificItems
	GameObjectU5BU5D_t3057952154* ___shieldDaySpecificItems_16;
	// UnityEngine.GameObject[] StoreManager::mineSpecificItems
	GameObjectU5BU5D_t3057952154* ___mineSpecificItems_17;
	// UnityEngine.GameObject[] StoreManager::fuelSpecificItems
	GameObjectU5BU5D_t3057952154* ___fuelSpecificItems_18;
	// UnityEngine.GameObject[] StoreManager::bundleSpecificItems
	GameObjectU5BU5D_t3057952154* ___bundleSpecificItems_19;
	// UnityEngine.GameObject[] StoreManager::bucksSpecificItems
	GameObjectU5BU5D_t3057952154* ___bucksSpecificItems_20;
	// UnityEngine.GameObject[] StoreManager::newItems
	GameObjectU5BU5D_t3057952154* ___newItems_21;
	// UnityEngine.GameObject[] StoreManager::currentItems
	GameObjectU5BU5D_t3057952154* ___currentItems_22;
	// PurchaseableCategory StoreManager::currentCategory
	int32_t ___currentCategory_23;
	// System.Collections.Generic.List`1<PurchaseableItem> StoreManager::categoryItems
	List_1_t2720244128 * ___categoryItems_24;
	// System.Int32 StoreManager::currentItemIndex
	int32_t ___currentItemIndex_25;
	// UILabel StoreManager::itemNameLabel
	UILabel_t1795115428 * ___itemNameLabel_26;
	// UILabel StoreManager::itemPriceLabel
	UILabel_t1795115428 * ___itemPriceLabel_27;
	// UILabel StoreManager::itemAmountLabel
	UILabel_t1795115428 * ___itemAmountLabel_28;
	// UISprite StoreManager::infinitySprite
	UISprite_t603616735 * ___infinitySprite_29;
	// BottomButtonsController StoreManager::buttonsController
	BottomButtonsController_t3250435864 * ___buttonsController_30;
	// BuyItemButton StoreManager::buyButton
	BuyItemButton_t2593913003 * ___buyButton_31;
	// StoreTargetsButton StoreManager::targetsButton
	StoreTargetsButton_t3560946605 * ___targetsButton_32;
	// System.Boolean StoreManager::showingInfo
	bool ___showingInfo_33;
	// UnityEngine.Light StoreManager::itemLight
	Light_t494725636 * ___itemLight_34;
	// UnityEngine.Coroutine StoreManager::startCheck
	Coroutine_t2299508840 * ___startCheck_35;
	// UnityEngine.Color StoreManager::bucksLightColour
	Color_t2020392075  ___bucksLightColour_36;
	// TweenScale StoreManager::scaleTween
	TweenScale_t2697902175 * ___scaleTween_37;
	// System.Boolean StoreManager::firstTimeLoading
	bool ___firstTimeLoading_38;

public:
	inline static int32_t get_offset_of_categoryNavController_3() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___categoryNavController_3)); }
	inline SwipeNavigationController_t3767682136 * get_categoryNavController_3() const { return ___categoryNavController_3; }
	inline SwipeNavigationController_t3767682136 ** get_address_of_categoryNavController_3() { return &___categoryNavController_3; }
	inline void set_categoryNavController_3(SwipeNavigationController_t3767682136 * value)
	{
		___categoryNavController_3 = value;
		Il2CppCodeGenWriteBarrier(&___categoryNavController_3, value);
	}

	inline static int32_t get_offset_of_infoObject_4() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___infoObject_4)); }
	inline GameObject_t1756533147 * get_infoObject_4() const { return ___infoObject_4; }
	inline GameObject_t1756533147 ** get_address_of_infoObject_4() { return &___infoObject_4; }
	inline void set_infoObject_4(GameObject_t1756533147 * value)
	{
		___infoObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___infoObject_4, value);
	}

	inline static int32_t get_offset_of_itemObject_5() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___itemObject_5)); }
	inline GameObject_t1756533147 * get_itemObject_5() const { return ___itemObject_5; }
	inline GameObject_t1756533147 ** get_address_of_itemObject_5() { return &___itemObject_5; }
	inline void set_itemObject_5(GameObject_t1756533147 * value)
	{
		___itemObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___itemObject_5, value);
	}

	inline static int32_t get_offset_of_lockedBackground_6() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___lockedBackground_6)); }
	inline GameObject_t1756533147 * get_lockedBackground_6() const { return ___lockedBackground_6; }
	inline GameObject_t1756533147 ** get_address_of_lockedBackground_6() { return &___lockedBackground_6; }
	inline void set_lockedBackground_6(GameObject_t1756533147 * value)
	{
		___lockedBackground_6 = value;
		Il2CppCodeGenWriteBarrier(&___lockedBackground_6, value);
	}

	inline static int32_t get_offset_of_lockedLabel_7() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___lockedLabel_7)); }
	inline UILabel_t1795115428 * get_lockedLabel_7() const { return ___lockedLabel_7; }
	inline UILabel_t1795115428 ** get_address_of_lockedLabel_7() { return &___lockedLabel_7; }
	inline void set_lockedLabel_7(UILabel_t1795115428 * value)
	{
		___lockedLabel_7 = value;
		Il2CppCodeGenWriteBarrier(&___lockedLabel_7, value);
	}

	inline static int32_t get_offset_of_bucksLabel_8() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___bucksLabel_8)); }
	inline UILabel_t1795115428 * get_bucksLabel_8() const { return ___bucksLabel_8; }
	inline UILabel_t1795115428 ** get_address_of_bucksLabel_8() { return &___bucksLabel_8; }
	inline void set_bucksLabel_8(UILabel_t1795115428 * value)
	{
		___bucksLabel_8 = value;
		Il2CppCodeGenWriteBarrier(&___bucksLabel_8, value);
	}

	inline static int32_t get_offset_of_infoButton_9() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___infoButton_9)); }
	inline InventoryItemButton_t3515382031 * get_infoButton_9() const { return ___infoButton_9; }
	inline InventoryItemButton_t3515382031 ** get_address_of_infoButton_9() { return &___infoButton_9; }
	inline void set_infoButton_9(InventoryItemButton_t3515382031 * value)
	{
		___infoButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___infoButton_9, value);
	}

	inline static int32_t get_offset_of_infoController_10() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___infoController_10)); }
	inline ItemInfoController_t4231956141 * get_infoController_10() const { return ___infoController_10; }
	inline ItemInfoController_t4231956141 ** get_address_of_infoController_10() { return &___infoController_10; }
	inline void set_infoController_10(ItemInfoController_t4231956141 * value)
	{
		___infoController_10 = value;
		Il2CppCodeGenWriteBarrier(&___infoController_10, value);
	}

	inline static int32_t get_offset_of_swipeController_11() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___swipeController_11)); }
	inline ItemSwipeController_t3011021799 * get_swipeController_11() const { return ___swipeController_11; }
	inline ItemSwipeController_t3011021799 ** get_address_of_swipeController_11() { return &___swipeController_11; }
	inline void set_swipeController_11(ItemSwipeController_t3011021799 * value)
	{
		___swipeController_11 = value;
		Il2CppCodeGenWriteBarrier(&___swipeController_11, value);
	}

	inline static int32_t get_offset_of_currentShieldSlider_12() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___currentShieldSlider_12)); }
	inline ShieldSlider_t1483508804 * get_currentShieldSlider_12() const { return ___currentShieldSlider_12; }
	inline ShieldSlider_t1483508804 ** get_address_of_currentShieldSlider_12() { return &___currentShieldSlider_12; }
	inline void set_currentShieldSlider_12(ShieldSlider_t1483508804 * value)
	{
		___currentShieldSlider_12 = value;
		Il2CppCodeGenWriteBarrier(&___currentShieldSlider_12, value);
	}

	inline static int32_t get_offset_of_curve_13() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___curve_13)); }
	inline AnimationCurve_t3306541151 * get_curve_13() const { return ___curve_13; }
	inline AnimationCurve_t3306541151 ** get_address_of_curve_13() { return &___curve_13; }
	inline void set_curve_13(AnimationCurve_t3306541151 * value)
	{
		___curve_13 = value;
		Il2CppCodeGenWriteBarrier(&___curve_13, value);
	}

	inline static int32_t get_offset_of_missileSpecificItems_14() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___missileSpecificItems_14)); }
	inline GameObjectU5BU5D_t3057952154* get_missileSpecificItems_14() const { return ___missileSpecificItems_14; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_missileSpecificItems_14() { return &___missileSpecificItems_14; }
	inline void set_missileSpecificItems_14(GameObjectU5BU5D_t3057952154* value)
	{
		___missileSpecificItems_14 = value;
		Il2CppCodeGenWriteBarrier(&___missileSpecificItems_14, value);
	}

	inline static int32_t get_offset_of_shieldHourSpecificItems_15() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___shieldHourSpecificItems_15)); }
	inline GameObjectU5BU5D_t3057952154* get_shieldHourSpecificItems_15() const { return ___shieldHourSpecificItems_15; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_shieldHourSpecificItems_15() { return &___shieldHourSpecificItems_15; }
	inline void set_shieldHourSpecificItems_15(GameObjectU5BU5D_t3057952154* value)
	{
		___shieldHourSpecificItems_15 = value;
		Il2CppCodeGenWriteBarrier(&___shieldHourSpecificItems_15, value);
	}

	inline static int32_t get_offset_of_shieldDaySpecificItems_16() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___shieldDaySpecificItems_16)); }
	inline GameObjectU5BU5D_t3057952154* get_shieldDaySpecificItems_16() const { return ___shieldDaySpecificItems_16; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_shieldDaySpecificItems_16() { return &___shieldDaySpecificItems_16; }
	inline void set_shieldDaySpecificItems_16(GameObjectU5BU5D_t3057952154* value)
	{
		___shieldDaySpecificItems_16 = value;
		Il2CppCodeGenWriteBarrier(&___shieldDaySpecificItems_16, value);
	}

	inline static int32_t get_offset_of_mineSpecificItems_17() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___mineSpecificItems_17)); }
	inline GameObjectU5BU5D_t3057952154* get_mineSpecificItems_17() const { return ___mineSpecificItems_17; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_mineSpecificItems_17() { return &___mineSpecificItems_17; }
	inline void set_mineSpecificItems_17(GameObjectU5BU5D_t3057952154* value)
	{
		___mineSpecificItems_17 = value;
		Il2CppCodeGenWriteBarrier(&___mineSpecificItems_17, value);
	}

	inline static int32_t get_offset_of_fuelSpecificItems_18() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___fuelSpecificItems_18)); }
	inline GameObjectU5BU5D_t3057952154* get_fuelSpecificItems_18() const { return ___fuelSpecificItems_18; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_fuelSpecificItems_18() { return &___fuelSpecificItems_18; }
	inline void set_fuelSpecificItems_18(GameObjectU5BU5D_t3057952154* value)
	{
		___fuelSpecificItems_18 = value;
		Il2CppCodeGenWriteBarrier(&___fuelSpecificItems_18, value);
	}

	inline static int32_t get_offset_of_bundleSpecificItems_19() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___bundleSpecificItems_19)); }
	inline GameObjectU5BU5D_t3057952154* get_bundleSpecificItems_19() const { return ___bundleSpecificItems_19; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_bundleSpecificItems_19() { return &___bundleSpecificItems_19; }
	inline void set_bundleSpecificItems_19(GameObjectU5BU5D_t3057952154* value)
	{
		___bundleSpecificItems_19 = value;
		Il2CppCodeGenWriteBarrier(&___bundleSpecificItems_19, value);
	}

	inline static int32_t get_offset_of_bucksSpecificItems_20() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___bucksSpecificItems_20)); }
	inline GameObjectU5BU5D_t3057952154* get_bucksSpecificItems_20() const { return ___bucksSpecificItems_20; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_bucksSpecificItems_20() { return &___bucksSpecificItems_20; }
	inline void set_bucksSpecificItems_20(GameObjectU5BU5D_t3057952154* value)
	{
		___bucksSpecificItems_20 = value;
		Il2CppCodeGenWriteBarrier(&___bucksSpecificItems_20, value);
	}

	inline static int32_t get_offset_of_newItems_21() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___newItems_21)); }
	inline GameObjectU5BU5D_t3057952154* get_newItems_21() const { return ___newItems_21; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_newItems_21() { return &___newItems_21; }
	inline void set_newItems_21(GameObjectU5BU5D_t3057952154* value)
	{
		___newItems_21 = value;
		Il2CppCodeGenWriteBarrier(&___newItems_21, value);
	}

	inline static int32_t get_offset_of_currentItems_22() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___currentItems_22)); }
	inline GameObjectU5BU5D_t3057952154* get_currentItems_22() const { return ___currentItems_22; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_currentItems_22() { return &___currentItems_22; }
	inline void set_currentItems_22(GameObjectU5BU5D_t3057952154* value)
	{
		___currentItems_22 = value;
		Il2CppCodeGenWriteBarrier(&___currentItems_22, value);
	}

	inline static int32_t get_offset_of_currentCategory_23() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___currentCategory_23)); }
	inline int32_t get_currentCategory_23() const { return ___currentCategory_23; }
	inline int32_t* get_address_of_currentCategory_23() { return &___currentCategory_23; }
	inline void set_currentCategory_23(int32_t value)
	{
		___currentCategory_23 = value;
	}

	inline static int32_t get_offset_of_categoryItems_24() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___categoryItems_24)); }
	inline List_1_t2720244128 * get_categoryItems_24() const { return ___categoryItems_24; }
	inline List_1_t2720244128 ** get_address_of_categoryItems_24() { return &___categoryItems_24; }
	inline void set_categoryItems_24(List_1_t2720244128 * value)
	{
		___categoryItems_24 = value;
		Il2CppCodeGenWriteBarrier(&___categoryItems_24, value);
	}

	inline static int32_t get_offset_of_currentItemIndex_25() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___currentItemIndex_25)); }
	inline int32_t get_currentItemIndex_25() const { return ___currentItemIndex_25; }
	inline int32_t* get_address_of_currentItemIndex_25() { return &___currentItemIndex_25; }
	inline void set_currentItemIndex_25(int32_t value)
	{
		___currentItemIndex_25 = value;
	}

	inline static int32_t get_offset_of_itemNameLabel_26() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___itemNameLabel_26)); }
	inline UILabel_t1795115428 * get_itemNameLabel_26() const { return ___itemNameLabel_26; }
	inline UILabel_t1795115428 ** get_address_of_itemNameLabel_26() { return &___itemNameLabel_26; }
	inline void set_itemNameLabel_26(UILabel_t1795115428 * value)
	{
		___itemNameLabel_26 = value;
		Il2CppCodeGenWriteBarrier(&___itemNameLabel_26, value);
	}

	inline static int32_t get_offset_of_itemPriceLabel_27() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___itemPriceLabel_27)); }
	inline UILabel_t1795115428 * get_itemPriceLabel_27() const { return ___itemPriceLabel_27; }
	inline UILabel_t1795115428 ** get_address_of_itemPriceLabel_27() { return &___itemPriceLabel_27; }
	inline void set_itemPriceLabel_27(UILabel_t1795115428 * value)
	{
		___itemPriceLabel_27 = value;
		Il2CppCodeGenWriteBarrier(&___itemPriceLabel_27, value);
	}

	inline static int32_t get_offset_of_itemAmountLabel_28() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___itemAmountLabel_28)); }
	inline UILabel_t1795115428 * get_itemAmountLabel_28() const { return ___itemAmountLabel_28; }
	inline UILabel_t1795115428 ** get_address_of_itemAmountLabel_28() { return &___itemAmountLabel_28; }
	inline void set_itemAmountLabel_28(UILabel_t1795115428 * value)
	{
		___itemAmountLabel_28 = value;
		Il2CppCodeGenWriteBarrier(&___itemAmountLabel_28, value);
	}

	inline static int32_t get_offset_of_infinitySprite_29() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___infinitySprite_29)); }
	inline UISprite_t603616735 * get_infinitySprite_29() const { return ___infinitySprite_29; }
	inline UISprite_t603616735 ** get_address_of_infinitySprite_29() { return &___infinitySprite_29; }
	inline void set_infinitySprite_29(UISprite_t603616735 * value)
	{
		___infinitySprite_29 = value;
		Il2CppCodeGenWriteBarrier(&___infinitySprite_29, value);
	}

	inline static int32_t get_offset_of_buttonsController_30() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___buttonsController_30)); }
	inline BottomButtonsController_t3250435864 * get_buttonsController_30() const { return ___buttonsController_30; }
	inline BottomButtonsController_t3250435864 ** get_address_of_buttonsController_30() { return &___buttonsController_30; }
	inline void set_buttonsController_30(BottomButtonsController_t3250435864 * value)
	{
		___buttonsController_30 = value;
		Il2CppCodeGenWriteBarrier(&___buttonsController_30, value);
	}

	inline static int32_t get_offset_of_buyButton_31() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___buyButton_31)); }
	inline BuyItemButton_t2593913003 * get_buyButton_31() const { return ___buyButton_31; }
	inline BuyItemButton_t2593913003 ** get_address_of_buyButton_31() { return &___buyButton_31; }
	inline void set_buyButton_31(BuyItemButton_t2593913003 * value)
	{
		___buyButton_31 = value;
		Il2CppCodeGenWriteBarrier(&___buyButton_31, value);
	}

	inline static int32_t get_offset_of_targetsButton_32() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___targetsButton_32)); }
	inline StoreTargetsButton_t3560946605 * get_targetsButton_32() const { return ___targetsButton_32; }
	inline StoreTargetsButton_t3560946605 ** get_address_of_targetsButton_32() { return &___targetsButton_32; }
	inline void set_targetsButton_32(StoreTargetsButton_t3560946605 * value)
	{
		___targetsButton_32 = value;
		Il2CppCodeGenWriteBarrier(&___targetsButton_32, value);
	}

	inline static int32_t get_offset_of_showingInfo_33() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___showingInfo_33)); }
	inline bool get_showingInfo_33() const { return ___showingInfo_33; }
	inline bool* get_address_of_showingInfo_33() { return &___showingInfo_33; }
	inline void set_showingInfo_33(bool value)
	{
		___showingInfo_33 = value;
	}

	inline static int32_t get_offset_of_itemLight_34() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___itemLight_34)); }
	inline Light_t494725636 * get_itemLight_34() const { return ___itemLight_34; }
	inline Light_t494725636 ** get_address_of_itemLight_34() { return &___itemLight_34; }
	inline void set_itemLight_34(Light_t494725636 * value)
	{
		___itemLight_34 = value;
		Il2CppCodeGenWriteBarrier(&___itemLight_34, value);
	}

	inline static int32_t get_offset_of_startCheck_35() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___startCheck_35)); }
	inline Coroutine_t2299508840 * get_startCheck_35() const { return ___startCheck_35; }
	inline Coroutine_t2299508840 ** get_address_of_startCheck_35() { return &___startCheck_35; }
	inline void set_startCheck_35(Coroutine_t2299508840 * value)
	{
		___startCheck_35 = value;
		Il2CppCodeGenWriteBarrier(&___startCheck_35, value);
	}

	inline static int32_t get_offset_of_bucksLightColour_36() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___bucksLightColour_36)); }
	inline Color_t2020392075  get_bucksLightColour_36() const { return ___bucksLightColour_36; }
	inline Color_t2020392075 * get_address_of_bucksLightColour_36() { return &___bucksLightColour_36; }
	inline void set_bucksLightColour_36(Color_t2020392075  value)
	{
		___bucksLightColour_36 = value;
	}

	inline static int32_t get_offset_of_scaleTween_37() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___scaleTween_37)); }
	inline TweenScale_t2697902175 * get_scaleTween_37() const { return ___scaleTween_37; }
	inline TweenScale_t2697902175 ** get_address_of_scaleTween_37() { return &___scaleTween_37; }
	inline void set_scaleTween_37(TweenScale_t2697902175 * value)
	{
		___scaleTween_37 = value;
		Il2CppCodeGenWriteBarrier(&___scaleTween_37, value);
	}

	inline static int32_t get_offset_of_firstTimeLoading_38() { return static_cast<int32_t>(offsetof(StoreManager_t650776524, ___firstTimeLoading_38)); }
	inline bool get_firstTimeLoading_38() const { return ___firstTimeLoading_38; }
	inline bool* get_address_of_firstTimeLoading_38() { return &___firstTimeLoading_38; }
	inline void set_firstTimeLoading_38(bool value)
	{
		___firstTimeLoading_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
