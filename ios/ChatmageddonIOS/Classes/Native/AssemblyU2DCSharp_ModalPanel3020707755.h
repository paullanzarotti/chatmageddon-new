﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIPanel
struct UIPanel_t1795085332;
// TweenAlpha
struct TweenAlpha_t2421518635;
// UILabel
struct UILabel_t1795115428;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UISprite
struct UISprite_t603616735;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModalPanel
struct  ModalPanel_t3020707755  : public MonoBehaviour_t1158329972
{
public:
	// UIPanel ModalPanel::panel
	UIPanel_t1795085332 * ___panel_2;
	// TweenAlpha ModalPanel::panelAlphaTween
	TweenAlpha_t2421518635 * ___panelAlphaTween_3;
	// UILabel ModalPanel::titleLabel
	UILabel_t1795115428 * ___titleLabel_4;
	// UnityEngine.GameObject ModalPanel::backgroundFade
	GameObject_t1756533147 * ___backgroundFade_5;
	// UISprite ModalPanel::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_6;
	// UISprite ModalPanel::closeSprite
	UISprite_t603616735 * ___closeSprite_7;
	// UnityEngine.BoxCollider ModalPanel::closeCollider
	BoxCollider_t22920061 * ___closeCollider_8;
	// UISprite ModalPanel::dividerSprite
	UISprite_t603616735 * ___dividerSprite_9;
	// UnityEngine.GameObject ModalPanel::baseModal
	GameObject_t1756533147 * ___baseModal_10;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___panel_2)); }
	inline UIPanel_t1795085332 * get_panel_2() const { return ___panel_2; }
	inline UIPanel_t1795085332 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(UIPanel_t1795085332 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_panelAlphaTween_3() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___panelAlphaTween_3)); }
	inline TweenAlpha_t2421518635 * get_panelAlphaTween_3() const { return ___panelAlphaTween_3; }
	inline TweenAlpha_t2421518635 ** get_address_of_panelAlphaTween_3() { return &___panelAlphaTween_3; }
	inline void set_panelAlphaTween_3(TweenAlpha_t2421518635 * value)
	{
		___panelAlphaTween_3 = value;
		Il2CppCodeGenWriteBarrier(&___panelAlphaTween_3, value);
	}

	inline static int32_t get_offset_of_titleLabel_4() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___titleLabel_4)); }
	inline UILabel_t1795115428 * get_titleLabel_4() const { return ___titleLabel_4; }
	inline UILabel_t1795115428 ** get_address_of_titleLabel_4() { return &___titleLabel_4; }
	inline void set_titleLabel_4(UILabel_t1795115428 * value)
	{
		___titleLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___titleLabel_4, value);
	}

	inline static int32_t get_offset_of_backgroundFade_5() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___backgroundFade_5)); }
	inline GameObject_t1756533147 * get_backgroundFade_5() const { return ___backgroundFade_5; }
	inline GameObject_t1756533147 ** get_address_of_backgroundFade_5() { return &___backgroundFade_5; }
	inline void set_backgroundFade_5(GameObject_t1756533147 * value)
	{
		___backgroundFade_5 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundFade_5, value);
	}

	inline static int32_t get_offset_of_backgroundSprite_6() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___backgroundSprite_6)); }
	inline UISprite_t603616735 * get_backgroundSprite_6() const { return ___backgroundSprite_6; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_6() { return &___backgroundSprite_6; }
	inline void set_backgroundSprite_6(UISprite_t603616735 * value)
	{
		___backgroundSprite_6 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_6, value);
	}

	inline static int32_t get_offset_of_closeSprite_7() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___closeSprite_7)); }
	inline UISprite_t603616735 * get_closeSprite_7() const { return ___closeSprite_7; }
	inline UISprite_t603616735 ** get_address_of_closeSprite_7() { return &___closeSprite_7; }
	inline void set_closeSprite_7(UISprite_t603616735 * value)
	{
		___closeSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___closeSprite_7, value);
	}

	inline static int32_t get_offset_of_closeCollider_8() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___closeCollider_8)); }
	inline BoxCollider_t22920061 * get_closeCollider_8() const { return ___closeCollider_8; }
	inline BoxCollider_t22920061 ** get_address_of_closeCollider_8() { return &___closeCollider_8; }
	inline void set_closeCollider_8(BoxCollider_t22920061 * value)
	{
		___closeCollider_8 = value;
		Il2CppCodeGenWriteBarrier(&___closeCollider_8, value);
	}

	inline static int32_t get_offset_of_dividerSprite_9() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___dividerSprite_9)); }
	inline UISprite_t603616735 * get_dividerSprite_9() const { return ___dividerSprite_9; }
	inline UISprite_t603616735 ** get_address_of_dividerSprite_9() { return &___dividerSprite_9; }
	inline void set_dividerSprite_9(UISprite_t603616735 * value)
	{
		___dividerSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___dividerSprite_9, value);
	}

	inline static int32_t get_offset_of_baseModal_10() { return static_cast<int32_t>(offsetof(ModalPanel_t3020707755, ___baseModal_10)); }
	inline GameObject_t1756533147 * get_baseModal_10() const { return ___baseModal_10; }
	inline GameObject_t1756533147 ** get_address_of_baseModal_10() { return &___baseModal_10; }
	inline void set_baseModal_10(GameObject_t1756533147 * value)
	{
		___baseModal_10 = value;
		Il2CppCodeGenWriteBarrier(&___baseModal_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
