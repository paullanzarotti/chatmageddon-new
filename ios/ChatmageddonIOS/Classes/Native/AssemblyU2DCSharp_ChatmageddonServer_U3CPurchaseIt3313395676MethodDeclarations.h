﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChatmageddonServer/<PurchaseItem>c__AnonStorey33
struct U3CPurchaseItemU3Ec__AnonStorey33_t3313395676;
// BestHTTP.HTTPRequest
struct HTTPRequest_t138485887;
// BestHTTP.HTTPResponse
struct HTTPResponse_t62748825;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPRequest138485887.h"
#include "AssemblyU2DCSharp_BestHTTP_HTTPResponse62748825.h"

// System.Void ChatmageddonServer/<PurchaseItem>c__AnonStorey33::.ctor()
extern "C"  void U3CPurchaseItemU3Ec__AnonStorey33__ctor_m3068999311 (U3CPurchaseItemU3Ec__AnonStorey33_t3313395676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChatmageddonServer/<PurchaseItem>c__AnonStorey33::<>m__0(BestHTTP.HTTPRequest,BestHTTP.HTTPResponse)
extern "C"  void U3CPurchaseItemU3Ec__AnonStorey33_U3CU3Em__0_m410692276 (U3CPurchaseItemU3Ec__AnonStorey33_t3313395676 * __this, HTTPRequest_t138485887 * ___request0, HTTPResponse_t62748825 * ___response1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
