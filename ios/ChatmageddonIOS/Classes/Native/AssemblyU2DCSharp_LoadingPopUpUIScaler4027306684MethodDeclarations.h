﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingPopUpUIScaler
struct LoadingPopUpUIScaler_t4027306684;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadingPopUpUIScaler::.ctor()
extern "C"  void LoadingPopUpUIScaler__ctor_m3051907021 (LoadingPopUpUIScaler_t4027306684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingPopUpUIScaler::GlobalUIScale()
extern "C"  void LoadingPopUpUIScaler_GlobalUIScale_m3077387558 (LoadingPopUpUIScaler_t4027306684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
