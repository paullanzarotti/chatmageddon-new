﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Coroutine
struct Coroutine_t2299508840;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen3302319956.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShieldManager
struct  ShieldManager_t3551654236  : public MonoSingleton_1_t3302319956
{
public:
	// UnityEngine.Coroutine ShieldManager::missileShieldRoutine
	Coroutine_t2299508840 * ___missileShieldRoutine_3;
	// UnityEngine.Coroutine ShieldManager::mineShieldRoutine
	Coroutine_t2299508840 * ___mineShieldRoutine_4;

public:
	inline static int32_t get_offset_of_missileShieldRoutine_3() { return static_cast<int32_t>(offsetof(ShieldManager_t3551654236, ___missileShieldRoutine_3)); }
	inline Coroutine_t2299508840 * get_missileShieldRoutine_3() const { return ___missileShieldRoutine_3; }
	inline Coroutine_t2299508840 ** get_address_of_missileShieldRoutine_3() { return &___missileShieldRoutine_3; }
	inline void set_missileShieldRoutine_3(Coroutine_t2299508840 * value)
	{
		___missileShieldRoutine_3 = value;
		Il2CppCodeGenWriteBarrier(&___missileShieldRoutine_3, value);
	}

	inline static int32_t get_offset_of_mineShieldRoutine_4() { return static_cast<int32_t>(offsetof(ShieldManager_t3551654236, ___mineShieldRoutine_4)); }
	inline Coroutine_t2299508840 * get_mineShieldRoutine_4() const { return ___mineShieldRoutine_4; }
	inline Coroutine_t2299508840 ** get_address_of_mineShieldRoutine_4() { return &___mineShieldRoutine_4; }
	inline void set_mineShieldRoutine_4(Coroutine_t2299508840 * value)
	{
		___mineShieldRoutine_4 = value;
		Il2CppCodeGenWriteBarrier(&___mineShieldRoutine_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
