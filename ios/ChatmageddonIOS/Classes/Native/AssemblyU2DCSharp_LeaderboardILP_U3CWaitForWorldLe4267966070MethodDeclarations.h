﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LeaderboardILP/<WaitForWorldLeaderboardLoad>c__Iterator1
struct U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LeaderboardILP/<WaitForWorldLeaderboardLoad>c__Iterator1::.ctor()
extern "C"  void U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1__ctor_m1514338771 (U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LeaderboardILP/<WaitForWorldLeaderboardLoad>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_MoveNext_m219408025 (U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LeaderboardILP/<WaitForWorldLeaderboardLoad>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2882843869 (U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LeaderboardILP/<WaitForWorldLeaderboardLoad>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3725001749 (U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP/<WaitForWorldLeaderboardLoad>c__Iterator1::Dispose()
extern "C"  void U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_Dispose_m587694068 (U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LeaderboardILP/<WaitForWorldLeaderboardLoad>c__Iterator1::Reset()
extern "C"  void U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_Reset_m4127400490 (U3CWaitForWorldLeaderboardLoadU3Ec__Iterator1_t4267966070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
