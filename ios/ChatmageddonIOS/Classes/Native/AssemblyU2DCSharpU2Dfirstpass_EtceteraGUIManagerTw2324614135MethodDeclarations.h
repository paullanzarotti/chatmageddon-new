﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EtceteraGUIManagerTwo/<hideActivityView>c__Iterator0
struct U3ChideActivityViewU3Ec__Iterator0_t2324614135;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EtceteraGUIManagerTwo/<hideActivityView>c__Iterator0::.ctor()
extern "C"  void U3ChideActivityViewU3Ec__Iterator0__ctor_m2863336732 (U3ChideActivityViewU3Ec__Iterator0_t2324614135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EtceteraGUIManagerTwo/<hideActivityView>c__Iterator0::MoveNext()
extern "C"  bool U3ChideActivityViewU3Ec__Iterator0_MoveNext_m632146196 (U3ChideActivityViewU3Ec__Iterator0_t2324614135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraGUIManagerTwo/<hideActivityView>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ChideActivityViewU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1829838864 (U3ChideActivityViewU3Ec__Iterator0_t2324614135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EtceteraGUIManagerTwo/<hideActivityView>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ChideActivityViewU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4286714824 (U3ChideActivityViewU3Ec__Iterator0_t2324614135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo/<hideActivityView>c__Iterator0::Dispose()
extern "C"  void U3ChideActivityViewU3Ec__Iterator0_Dispose_m214000839 (U3ChideActivityViewU3Ec__Iterator0_t2324614135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EtceteraGUIManagerTwo/<hideActivityView>c__Iterator0::Reset()
extern "C"  void U3ChideActivityViewU3Ec__Iterator0_Reset_m3699617181 (U3ChideActivityViewU3Ec__Iterator0_t2324614135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
