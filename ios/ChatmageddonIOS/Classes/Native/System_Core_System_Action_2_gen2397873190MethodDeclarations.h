﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"

// System.Void System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m619474708(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t2397873190 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m2967680750_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]>::Invoke(T1,T2)
#define Action_2_Invoke_m4210274227(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2397873190 *, WebSocketResponse_t3376763264 *, ByteU5BU5D_t3397334013*, const MethodInfo*))Action_2_Invoke_m715425719_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m333026348(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t2397873190 *, WebSocketResponse_t3376763264 *, ByteU5BU5D_t3397334013*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1914861552_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<BestHTTP.WebSocket.WebSocketResponse,System.Byte[]>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m2994731240(__this, ___result0, method) ((  void (*) (Action_2_t2397873190 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3956733788_gshared)(__this, ___result0, method)
