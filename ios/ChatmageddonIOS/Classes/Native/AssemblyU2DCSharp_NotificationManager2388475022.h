﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String>
struct Action_3_t3681841185;

#include "AssemblyU2DCSharp_MonoSingleton_1_gen2139140742.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationManager
struct  NotificationManager_t2388475022  : public MonoSingleton_1_t2139140742
{
public:
	// System.Boolean NotificationManager::initialised
	bool ___initialised_3;
	// System.Boolean NotificationManager::tokenSent
	bool ___tokenSent_4;
	// System.String NotificationManager::activeToken
	String_t* ___activeToken_5;
	// System.Collections.Generic.List`1<System.String> NotificationManager::unreadNotifications
	List_1_t1398341365 * ___unreadNotifications_6;
	// UnityEngine.Coroutine NotificationManager::checkRoutine
	Coroutine_t2299508840 * ___checkRoutine_7;
	// UnityEngine.Coroutine NotificationManager::clearOverTimeRoutine
	Coroutine_t2299508840 * ___clearOverTimeRoutine_8;

public:
	inline static int32_t get_offset_of_initialised_3() { return static_cast<int32_t>(offsetof(NotificationManager_t2388475022, ___initialised_3)); }
	inline bool get_initialised_3() const { return ___initialised_3; }
	inline bool* get_address_of_initialised_3() { return &___initialised_3; }
	inline void set_initialised_3(bool value)
	{
		___initialised_3 = value;
	}

	inline static int32_t get_offset_of_tokenSent_4() { return static_cast<int32_t>(offsetof(NotificationManager_t2388475022, ___tokenSent_4)); }
	inline bool get_tokenSent_4() const { return ___tokenSent_4; }
	inline bool* get_address_of_tokenSent_4() { return &___tokenSent_4; }
	inline void set_tokenSent_4(bool value)
	{
		___tokenSent_4 = value;
	}

	inline static int32_t get_offset_of_activeToken_5() { return static_cast<int32_t>(offsetof(NotificationManager_t2388475022, ___activeToken_5)); }
	inline String_t* get_activeToken_5() const { return ___activeToken_5; }
	inline String_t** get_address_of_activeToken_5() { return &___activeToken_5; }
	inline void set_activeToken_5(String_t* value)
	{
		___activeToken_5 = value;
		Il2CppCodeGenWriteBarrier(&___activeToken_5, value);
	}

	inline static int32_t get_offset_of_unreadNotifications_6() { return static_cast<int32_t>(offsetof(NotificationManager_t2388475022, ___unreadNotifications_6)); }
	inline List_1_t1398341365 * get_unreadNotifications_6() const { return ___unreadNotifications_6; }
	inline List_1_t1398341365 ** get_address_of_unreadNotifications_6() { return &___unreadNotifications_6; }
	inline void set_unreadNotifications_6(List_1_t1398341365 * value)
	{
		___unreadNotifications_6 = value;
		Il2CppCodeGenWriteBarrier(&___unreadNotifications_6, value);
	}

	inline static int32_t get_offset_of_checkRoutine_7() { return static_cast<int32_t>(offsetof(NotificationManager_t2388475022, ___checkRoutine_7)); }
	inline Coroutine_t2299508840 * get_checkRoutine_7() const { return ___checkRoutine_7; }
	inline Coroutine_t2299508840 ** get_address_of_checkRoutine_7() { return &___checkRoutine_7; }
	inline void set_checkRoutine_7(Coroutine_t2299508840 * value)
	{
		___checkRoutine_7 = value;
		Il2CppCodeGenWriteBarrier(&___checkRoutine_7, value);
	}

	inline static int32_t get_offset_of_clearOverTimeRoutine_8() { return static_cast<int32_t>(offsetof(NotificationManager_t2388475022, ___clearOverTimeRoutine_8)); }
	inline Coroutine_t2299508840 * get_clearOverTimeRoutine_8() const { return ___clearOverTimeRoutine_8; }
	inline Coroutine_t2299508840 ** get_address_of_clearOverTimeRoutine_8() { return &___clearOverTimeRoutine_8; }
	inline void set_clearOverTimeRoutine_8(Coroutine_t2299508840 * value)
	{
		___clearOverTimeRoutine_8 = value;
		Il2CppCodeGenWriteBarrier(&___clearOverTimeRoutine_8, value);
	}
};

struct NotificationManager_t2388475022_StaticFields
{
public:
	// System.Action`3<System.Boolean,System.Collections.Hashtable,System.String> NotificationManager::<>f__am$cache0
	Action_3_t3681841185 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(NotificationManager_t2388475022_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_3_t3681841185 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_3_t3681841185 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_3_t3681841185 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
