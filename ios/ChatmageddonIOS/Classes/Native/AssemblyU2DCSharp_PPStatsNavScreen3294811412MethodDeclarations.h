﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PPStatsNavScreen
struct PPStatsNavScreen_t3294811412;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void PPStatsNavScreen::.ctor()
extern "C"  void PPStatsNavScreen__ctor_m1884556383 (PPStatsNavScreen_t3294811412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPStatsNavScreen::Start()
extern "C"  void PPStatsNavScreen_Start_m1531703635 (PPStatsNavScreen_t3294811412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPStatsNavScreen::UIClosing()
extern "C"  void PPStatsNavScreen_UIClosing_m3953401118 (PPStatsNavScreen_t3294811412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPStatsNavScreen::ScreenLoad(NavigationDirection,System.Boolean)
extern "C"  void PPStatsNavScreen_ScreenLoad_m4290284793 (PPStatsNavScreen_t3294811412 * __this, int32_t ___direction0, bool ___restartScreen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPStatsNavScreen::UpdateValueLabels()
extern "C"  void PPStatsNavScreen_UpdateValueLabels_m131158410 (PPStatsNavScreen_t3294811412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPStatsNavScreen::ScreenUnload(NavigationDirection)
extern "C"  void PPStatsNavScreen_ScreenUnload_m3237167253 (PPStatsNavScreen_t3294811412 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PPStatsNavScreen::ValidateScreenNavigation(NavigationDirection)
extern "C"  bool PPStatsNavScreen_ValidateScreenNavigation_m890383244 (PPStatsNavScreen_t3294811412 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PPStatsNavScreen::SetUIScene()
extern "C"  void PPStatsNavScreen_SetUIScene_m1892484963 (PPStatsNavScreen_t3294811412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
