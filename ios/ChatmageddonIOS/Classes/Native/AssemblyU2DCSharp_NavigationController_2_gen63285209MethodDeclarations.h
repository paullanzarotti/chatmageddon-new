﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NavigationController`2<System.Object,FriendNavScreen>
struct NavigationController_2_t63285209;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FriendNavScreen2175383453.h"
#include "AssemblyU2DCSharp_NavigationDirection2114074025.h"

// System.Void NavigationController`2<System.Object,FriendNavScreen>::.ctor()
extern "C"  void NavigationController_2__ctor_m254826646_gshared (NavigationController_2_t63285209 * __this, const MethodInfo* method);
#define NavigationController_2__ctor_m254826646(__this, method) ((  void (*) (NavigationController_2_t63285209 *, const MethodInfo*))NavigationController_2__ctor_m254826646_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,FriendNavScreen>::SetupStartScreen(NavScreenType)
extern "C"  void NavigationController_2_SetupStartScreen_m2217178148_gshared (NavigationController_2_t63285209 * __this, int32_t ___startScreen0, const MethodInfo* method);
#define NavigationController_2_SetupStartScreen_m2217178148(__this, ___startScreen0, method) ((  void (*) (NavigationController_2_t63285209 *, int32_t, const MethodInfo*))NavigationController_2_SetupStartScreen_m2217178148_gshared)(__this, ___startScreen0, method)
// System.Void NavigationController`2<System.Object,FriendNavScreen>::NavigateBackwards()
extern "C"  void NavigationController_2_NavigateBackwards_m2984498779_gshared (NavigationController_2_t63285209 * __this, const MethodInfo* method);
#define NavigationController_2_NavigateBackwards_m2984498779(__this, method) ((  void (*) (NavigationController_2_t63285209 *, const MethodInfo*))NavigationController_2_NavigateBackwards_m2984498779_gshared)(__this, method)
// System.Void NavigationController`2<System.Object,FriendNavScreen>::NavigateForwards(NavScreenType)
extern "C"  void NavigationController_2_NavigateForwards_m3329785178_gshared (NavigationController_2_t63285209 * __this, int32_t ___nextScreen0, const MethodInfo* method);
#define NavigationController_2_NavigateForwards_m3329785178(__this, ___nextScreen0, method) ((  void (*) (NavigationController_2_t63285209 *, int32_t, const MethodInfo*))NavigationController_2_NavigateForwards_m3329785178_gshared)(__this, ___nextScreen0, method)
// System.Boolean NavigationController`2<System.Object,FriendNavScreen>::ValidateNavigation(NavScreenType,NavigationDirection)
extern "C"  bool NavigationController_2_ValidateNavigation_m710670682_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_ValidateNavigation_m710670682(__this, ___screen0, ___direction1, method) ((  bool (*) (NavigationController_2_t63285209 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_ValidateNavigation_m710670682_gshared)(__this, ___screen0, ___direction1, method)
// System.Collections.IEnumerator NavigationController`2<System.Object,FriendNavScreen>::SaveScreenContent(NavScreenType,NavigationDirection,System.Action`1<System.Boolean>)
extern "C"  Il2CppObject * NavigationController_2_SaveScreenContent_m635772033_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, Action_1_t3627374100 * ___savedAction2, const MethodInfo* method);
#define NavigationController_2_SaveScreenContent_m635772033(__this, ___screen0, ___direction1, ___savedAction2, method) ((  Il2CppObject * (*) (NavigationController_2_t63285209 *, int32_t, int32_t, Action_1_t3627374100 *, const MethodInfo*))NavigationController_2_SaveScreenContent_m635772033_gshared)(__this, ___screen0, ___direction1, ___savedAction2, method)
// System.Void NavigationController`2<System.Object,FriendNavScreen>::LoadNavScreen(NavScreenType,NavigationDirection,System.Boolean)
extern "C"  void NavigationController_2_LoadNavScreen_m1954343124_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, bool ___restartScreen2, const MethodInfo* method);
#define NavigationController_2_LoadNavScreen_m1954343124(__this, ___screen0, ___direction1, ___restartScreen2, method) ((  void (*) (NavigationController_2_t63285209 *, int32_t, int32_t, bool, const MethodInfo*))NavigationController_2_LoadNavScreen_m1954343124_gshared)(__this, ___screen0, ___direction1, ___restartScreen2, method)
// System.Void NavigationController`2<System.Object,FriendNavScreen>::UnloadNavScreen(NavScreenType,NavigationDirection)
extern "C"  void NavigationController_2_UnloadNavScreen_m2758172006_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, int32_t ___direction1, const MethodInfo* method);
#define NavigationController_2_UnloadNavScreen_m2758172006(__this, ___screen0, ___direction1, method) ((  void (*) (NavigationController_2_t63285209 *, int32_t, int32_t, const MethodInfo*))NavigationController_2_UnloadNavScreen_m2758172006_gshared)(__this, ___screen0, ___direction1, method)
// System.Void NavigationController`2<System.Object,FriendNavScreen>::PopulateTitleBar(NavScreenType)
extern "C"  void NavigationController_2_PopulateTitleBar_m1720819378_gshared (NavigationController_2_t63285209 * __this, int32_t ___screen0, const MethodInfo* method);
#define NavigationController_2_PopulateTitleBar_m1720819378(__this, ___screen0, method) ((  void (*) (NavigationController_2_t63285209 *, int32_t, const MethodInfo*))NavigationController_2_PopulateTitleBar_m1720819378_gshared)(__this, ___screen0, method)
