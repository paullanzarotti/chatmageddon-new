﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResultModalUI
struct ResultModalUI_t969511824;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MineResultsModal
struct  MineResultsModal_t4210040424  : public MonoBehaviour_t1158329972
{
public:
	// ResultModalUI MineResultsModal::modal
	ResultModalUI_t969511824 * ___modal_2;

public:
	inline static int32_t get_offset_of_modal_2() { return static_cast<int32_t>(offsetof(MineResultsModal_t4210040424, ___modal_2)); }
	inline ResultModalUI_t969511824 * get_modal_2() const { return ___modal_2; }
	inline ResultModalUI_t969511824 ** get_address_of_modal_2() { return &___modal_2; }
	inline void set_modal_2(ResultModalUI_t969511824 * value)
	{
		___modal_2 = value;
		Il2CppCodeGenWriteBarrier(&___modal_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
