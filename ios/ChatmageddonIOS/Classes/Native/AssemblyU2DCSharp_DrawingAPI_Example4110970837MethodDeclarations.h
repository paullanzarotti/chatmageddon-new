﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DrawingAPI_Example
struct DrawingAPI_Example_t4110970837;

#include "codegen/il2cpp-codegen.h"

// System.Void DrawingAPI_Example::.ctor()
extern "C"  void DrawingAPI_Example__ctor_m3150758386 (DrawingAPI_Example_t4110970837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DrawingAPI_Example::Start()
extern "C"  void DrawingAPI_Example_Start_m1705454754 (DrawingAPI_Example_t4110970837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
