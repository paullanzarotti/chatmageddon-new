﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XElement/<Attributes>c__Iterator20
struct U3CAttributesU3Ec__Iterator20_t3474816789;
// System.Xml.Linq.XAttribute
struct XAttribute_t3858477518;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XAttribute>
struct IEnumerator_1_t1334001345;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Linq.XElement/<Attributes>c__Iterator20::.ctor()
extern "C"  void U3CAttributesU3Ec__Iterator20__ctor_m1308961820 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Linq.XAttribute System.Xml.Linq.XElement/<Attributes>c__Iterator20::System.Collections.Generic.IEnumerator<System.Xml.Linq.XAttribute>.get_Current()
extern "C"  XAttribute_t3858477518 * U3CAttributesU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XAttributeU3E_get_Current_m340769163 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.Linq.XElement/<Attributes>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAttributesU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m3003661324 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.Linq.XElement/<Attributes>c__Iterator20::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAttributesU3Ec__Iterator20_System_Collections_IEnumerable_GetEnumerator_m255454415 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XAttribute> System.Xml.Linq.XElement/<Attributes>c__Iterator20::System.Collections.Generic.IEnumerable<System.Xml.Linq.XAttribute>.GetEnumerator()
extern "C"  Il2CppObject* U3CAttributesU3Ec__Iterator20_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XAttributeU3E_GetEnumerator_m558085440 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XElement/<Attributes>c__Iterator20::MoveNext()
extern "C"  bool U3CAttributesU3Ec__Iterator20_MoveNext_m1568743152 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement/<Attributes>c__Iterator20::Dispose()
extern "C"  void U3CAttributesU3Ec__Iterator20_Dispose_m2040226533 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.Linq.XElement/<Attributes>c__Iterator20::Reset()
extern "C"  void U3CAttributesU3Ec__Iterator20_Reset_m238223563 (U3CAttributesU3Ec__Iterator20_t3474816789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
