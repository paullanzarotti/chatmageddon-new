﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t603616735;
// UILabel
struct UILabel_t1795115428;

#include "AssemblyU2DCSharp_SFXButton792651341.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoubleStateButton
struct  DoubleStateButton_t1032633262  : public SFXButton_t792651341
{
public:
	// UnityEngine.Color DoubleStateButton::activeForegroundColor
	Color_t2020392075  ___activeForegroundColor_5;
	// UnityEngine.Color DoubleStateButton::activeBackgroundColor
	Color_t2020392075  ___activeBackgroundColor_6;
	// UnityEngine.Color DoubleStateButton::inactiveForegroundColor
	Color_t2020392075  ___inactiveForegroundColor_7;
	// UnityEngine.Color DoubleStateButton::inactiveBackgroundColor
	Color_t2020392075  ___inactiveBackgroundColor_8;
	// UISprite DoubleStateButton::forgroundSprite
	UISprite_t603616735 * ___forgroundSprite_9;
	// UISprite DoubleStateButton::backgroundSprite
	UISprite_t603616735 * ___backgroundSprite_10;
	// UILabel DoubleStateButton::buttonLabel
	UILabel_t1795115428 * ___buttonLabel_11;
	// System.Boolean DoubleStateButton::buttonActive
	bool ___buttonActive_12;

public:
	inline static int32_t get_offset_of_activeForegroundColor_5() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___activeForegroundColor_5)); }
	inline Color_t2020392075  get_activeForegroundColor_5() const { return ___activeForegroundColor_5; }
	inline Color_t2020392075 * get_address_of_activeForegroundColor_5() { return &___activeForegroundColor_5; }
	inline void set_activeForegroundColor_5(Color_t2020392075  value)
	{
		___activeForegroundColor_5 = value;
	}

	inline static int32_t get_offset_of_activeBackgroundColor_6() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___activeBackgroundColor_6)); }
	inline Color_t2020392075  get_activeBackgroundColor_6() const { return ___activeBackgroundColor_6; }
	inline Color_t2020392075 * get_address_of_activeBackgroundColor_6() { return &___activeBackgroundColor_6; }
	inline void set_activeBackgroundColor_6(Color_t2020392075  value)
	{
		___activeBackgroundColor_6 = value;
	}

	inline static int32_t get_offset_of_inactiveForegroundColor_7() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___inactiveForegroundColor_7)); }
	inline Color_t2020392075  get_inactiveForegroundColor_7() const { return ___inactiveForegroundColor_7; }
	inline Color_t2020392075 * get_address_of_inactiveForegroundColor_7() { return &___inactiveForegroundColor_7; }
	inline void set_inactiveForegroundColor_7(Color_t2020392075  value)
	{
		___inactiveForegroundColor_7 = value;
	}

	inline static int32_t get_offset_of_inactiveBackgroundColor_8() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___inactiveBackgroundColor_8)); }
	inline Color_t2020392075  get_inactiveBackgroundColor_8() const { return ___inactiveBackgroundColor_8; }
	inline Color_t2020392075 * get_address_of_inactiveBackgroundColor_8() { return &___inactiveBackgroundColor_8; }
	inline void set_inactiveBackgroundColor_8(Color_t2020392075  value)
	{
		___inactiveBackgroundColor_8 = value;
	}

	inline static int32_t get_offset_of_forgroundSprite_9() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___forgroundSprite_9)); }
	inline UISprite_t603616735 * get_forgroundSprite_9() const { return ___forgroundSprite_9; }
	inline UISprite_t603616735 ** get_address_of_forgroundSprite_9() { return &___forgroundSprite_9; }
	inline void set_forgroundSprite_9(UISprite_t603616735 * value)
	{
		___forgroundSprite_9 = value;
		Il2CppCodeGenWriteBarrier(&___forgroundSprite_9, value);
	}

	inline static int32_t get_offset_of_backgroundSprite_10() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___backgroundSprite_10)); }
	inline UISprite_t603616735 * get_backgroundSprite_10() const { return ___backgroundSprite_10; }
	inline UISprite_t603616735 ** get_address_of_backgroundSprite_10() { return &___backgroundSprite_10; }
	inline void set_backgroundSprite_10(UISprite_t603616735 * value)
	{
		___backgroundSprite_10 = value;
		Il2CppCodeGenWriteBarrier(&___backgroundSprite_10, value);
	}

	inline static int32_t get_offset_of_buttonLabel_11() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___buttonLabel_11)); }
	inline UILabel_t1795115428 * get_buttonLabel_11() const { return ___buttonLabel_11; }
	inline UILabel_t1795115428 ** get_address_of_buttonLabel_11() { return &___buttonLabel_11; }
	inline void set_buttonLabel_11(UILabel_t1795115428 * value)
	{
		___buttonLabel_11 = value;
		Il2CppCodeGenWriteBarrier(&___buttonLabel_11, value);
	}

	inline static int32_t get_offset_of_buttonActive_12() { return static_cast<int32_t>(offsetof(DoubleStateButton_t1032633262, ___buttonActive_12)); }
	inline bool get_buttonActive_12() const { return ___buttonActive_12; }
	inline bool* get_address_of_buttonActive_12() { return &___buttonActive_12; }
	inline void set_buttonActive_12(bool value)
	{
		___buttonActive_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
