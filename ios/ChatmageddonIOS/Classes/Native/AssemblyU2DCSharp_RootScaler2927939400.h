﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIRoot
struct UIRoot_t389944298;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RootScaler
struct  RootScaler_t2927939400  : public MonoBehaviour_t1158329972
{
public:
	// UIRoot RootScaler::root
	UIRoot_t389944298 * ___root_2;

public:
	inline static int32_t get_offset_of_root_2() { return static_cast<int32_t>(offsetof(RootScaler_t2927939400, ___root_2)); }
	inline UIRoot_t389944298 * get_root_2() const { return ___root_2; }
	inline UIRoot_t389944298 ** get_address_of_root_2() { return &___root_2; }
	inline void set_root_2(UIRoot_t389944298 * value)
	{
		___root_2 = value;
		Il2CppCodeGenWriteBarrier(&___root_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
