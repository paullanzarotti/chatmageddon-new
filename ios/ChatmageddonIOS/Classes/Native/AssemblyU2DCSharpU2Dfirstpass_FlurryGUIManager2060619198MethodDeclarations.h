﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlurryGUIManager
struct FlurryGUIManager_t2060619198;

#include "codegen/il2cpp-codegen.h"

// System.Void FlurryGUIManager::.ctor()
extern "C"  void FlurryGUIManager__ctor_m2756177315 (FlurryGUIManager_t2060619198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlurryGUIManager::OnGUI()
extern "C"  void FlurryGUIManager_OnGUI_m2034006765 (FlurryGUIManager_t2060619198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
