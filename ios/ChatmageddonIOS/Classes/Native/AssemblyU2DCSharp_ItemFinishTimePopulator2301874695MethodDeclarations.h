﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ItemFinishTimePopulator
struct ItemFinishTimePopulator_t2301874695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void ItemFinishTimePopulator::.ctor()
extern "C"  void ItemFinishTimePopulator__ctor_m811998064 (ItemFinishTimePopulator_t2301874695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemFinishTimePopulator::SetFinishTimeLabels(System.DateTime)
extern "C"  void ItemFinishTimePopulator_SetFinishTimeLabels_m214226709 (ItemFinishTimePopulator_t2301874695 * __this, DateTime_t693205669  ___finishTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ItemFinishTimePopulator::SetFinishTimeActive(System.Boolean)
extern "C"  void ItemFinishTimePopulator_SetFinishTimeActive_m1960899201 (ItemFinishTimePopulator_t2301874695 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
