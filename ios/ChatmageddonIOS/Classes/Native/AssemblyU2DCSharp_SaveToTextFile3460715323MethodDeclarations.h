﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveToTextFile
struct SaveToTextFile_t3460715323;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SaveToTextFile::.ctor()
extern "C"  void SaveToTextFile__ctor_m925340388 (SaveToTextFile_t3460715323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveToTextFile::WriteToTextFile(System.String,System.String)
extern "C"  void SaveToTextFile_WriteToTextFile_m871572787 (SaveToTextFile_t3460715323 * __this, String_t* ___path0, String_t* ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
