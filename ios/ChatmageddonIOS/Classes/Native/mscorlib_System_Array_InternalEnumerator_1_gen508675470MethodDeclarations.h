﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen508675470.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_AttackHomeNav3944890504.h"

// System.Void System.Array/InternalEnumerator`1<AttackHomeNav>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2321939449_gshared (InternalEnumerator_1_t508675470 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2321939449(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t508675470 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2321939449_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AttackHomeNav>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4040966061_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4040966061(__this, method) ((  void (*) (InternalEnumerator_1_t508675470 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4040966061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AttackHomeNav>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3095401865_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3095401865(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t508675470 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3095401865_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AttackHomeNav>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4240110220_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4240110220(__this, method) ((  void (*) (InternalEnumerator_1_t508675470 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4240110220_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AttackHomeNav>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2803236661_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2803236661(__this, method) ((  bool (*) (InternalEnumerator_1_t508675470 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2803236661_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AttackHomeNav>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m4113124508_gshared (InternalEnumerator_1_t508675470 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4113124508(__this, method) ((  int32_t (*) (InternalEnumerator_1_t508675470 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4113124508_gshared)(__this, method)
