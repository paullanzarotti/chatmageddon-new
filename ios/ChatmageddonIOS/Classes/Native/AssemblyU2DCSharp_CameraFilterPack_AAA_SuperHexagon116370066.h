﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t2430389951;
// UnityEngine.Material
struct Material_t193706927;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFilterPack_AAA_SuperHexagon
struct  CameraFilterPack_AAA_SuperHexagon_t116370066  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Shader CameraFilterPack_AAA_SuperHexagon::SCShader
	Shader_t2430389951 * ___SCShader_2;
	// System.Single CameraFilterPack_AAA_SuperHexagon::_AlphaHexa
	float ____AlphaHexa_3;
	// System.Single CameraFilterPack_AAA_SuperHexagon::TimeX
	float ___TimeX_4;
	// UnityEngine.Vector4 CameraFilterPack_AAA_SuperHexagon::ScreenResolution
	Vector4_t2243707581  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_AAA_SuperHexagon::SCMaterial
	Material_t193706927 * ___SCMaterial_6;
	// System.Single CameraFilterPack_AAA_SuperHexagon::HexaSize
	float ___HexaSize_7;
	// System.Single CameraFilterPack_AAA_SuperHexagon::_BorderSize
	float ____BorderSize_8;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::_BorderColor
	Color_t2020392075  ____BorderColor_9;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::_HexaColor
	Color_t2020392075  ____HexaColor_10;
	// System.Single CameraFilterPack_AAA_SuperHexagon::_SpotSize
	float ____SpotSize_11;
	// UnityEngine.Vector2 CameraFilterPack_AAA_SuperHexagon::center
	Vector2_t2243707579  ___center_18;
	// System.Single CameraFilterPack_AAA_SuperHexagon::Radius
	float ___Radius_19;

public:
	inline static int32_t get_offset_of_SCShader_2() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ___SCShader_2)); }
	inline Shader_t2430389951 * get_SCShader_2() const { return ___SCShader_2; }
	inline Shader_t2430389951 ** get_address_of_SCShader_2() { return &___SCShader_2; }
	inline void set_SCShader_2(Shader_t2430389951 * value)
	{
		___SCShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___SCShader_2, value);
	}

	inline static int32_t get_offset_of__AlphaHexa_3() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ____AlphaHexa_3)); }
	inline float get__AlphaHexa_3() const { return ____AlphaHexa_3; }
	inline float* get_address_of__AlphaHexa_3() { return &____AlphaHexa_3; }
	inline void set__AlphaHexa_3(float value)
	{
		____AlphaHexa_3 = value;
	}

	inline static int32_t get_offset_of_TimeX_4() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ___TimeX_4)); }
	inline float get_TimeX_4() const { return ___TimeX_4; }
	inline float* get_address_of_TimeX_4() { return &___TimeX_4; }
	inline void set_TimeX_4(float value)
	{
		___TimeX_4 = value;
	}

	inline static int32_t get_offset_of_ScreenResolution_5() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ___ScreenResolution_5)); }
	inline Vector4_t2243707581  get_ScreenResolution_5() const { return ___ScreenResolution_5; }
	inline Vector4_t2243707581 * get_address_of_ScreenResolution_5() { return &___ScreenResolution_5; }
	inline void set_ScreenResolution_5(Vector4_t2243707581  value)
	{
		___ScreenResolution_5 = value;
	}

	inline static int32_t get_offset_of_SCMaterial_6() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ___SCMaterial_6)); }
	inline Material_t193706927 * get_SCMaterial_6() const { return ___SCMaterial_6; }
	inline Material_t193706927 ** get_address_of_SCMaterial_6() { return &___SCMaterial_6; }
	inline void set_SCMaterial_6(Material_t193706927 * value)
	{
		___SCMaterial_6 = value;
		Il2CppCodeGenWriteBarrier(&___SCMaterial_6, value);
	}

	inline static int32_t get_offset_of_HexaSize_7() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ___HexaSize_7)); }
	inline float get_HexaSize_7() const { return ___HexaSize_7; }
	inline float* get_address_of_HexaSize_7() { return &___HexaSize_7; }
	inline void set_HexaSize_7(float value)
	{
		___HexaSize_7 = value;
	}

	inline static int32_t get_offset_of__BorderSize_8() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ____BorderSize_8)); }
	inline float get__BorderSize_8() const { return ____BorderSize_8; }
	inline float* get_address_of__BorderSize_8() { return &____BorderSize_8; }
	inline void set__BorderSize_8(float value)
	{
		____BorderSize_8 = value;
	}

	inline static int32_t get_offset_of__BorderColor_9() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ____BorderColor_9)); }
	inline Color_t2020392075  get__BorderColor_9() const { return ____BorderColor_9; }
	inline Color_t2020392075 * get_address_of__BorderColor_9() { return &____BorderColor_9; }
	inline void set__BorderColor_9(Color_t2020392075  value)
	{
		____BorderColor_9 = value;
	}

	inline static int32_t get_offset_of__HexaColor_10() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ____HexaColor_10)); }
	inline Color_t2020392075  get__HexaColor_10() const { return ____HexaColor_10; }
	inline Color_t2020392075 * get_address_of__HexaColor_10() { return &____HexaColor_10; }
	inline void set__HexaColor_10(Color_t2020392075  value)
	{
		____HexaColor_10 = value;
	}

	inline static int32_t get_offset_of__SpotSize_11() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ____SpotSize_11)); }
	inline float get__SpotSize_11() const { return ____SpotSize_11; }
	inline float* get_address_of__SpotSize_11() { return &____SpotSize_11; }
	inline void set__SpotSize_11(float value)
	{
		____SpotSize_11 = value;
	}

	inline static int32_t get_offset_of_center_18() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ___center_18)); }
	inline Vector2_t2243707579  get_center_18() const { return ___center_18; }
	inline Vector2_t2243707579 * get_address_of_center_18() { return &___center_18; }
	inline void set_center_18(Vector2_t2243707579  value)
	{
		___center_18 = value;
	}

	inline static int32_t get_offset_of_Radius_19() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066, ___Radius_19)); }
	inline float get_Radius_19() const { return ___Radius_19; }
	inline float* get_address_of_Radius_19() { return &___Radius_19; }
	inline void set_Radius_19(float value)
	{
		___Radius_19 = value;
	}
};

struct CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields
{
public:
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeBorderSize
	float ___ChangeBorderSize_12;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::ChangeBorderColor
	Color_t2020392075  ___ChangeBorderColor_13;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::ChangeHexaColor
	Color_t2020392075  ___ChangeHexaColor_14;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeSpotSize
	float ___ChangeSpotSize_15;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeAlphaHexa
	float ___ChangeAlphaHexa_16;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeValue
	float ___ChangeValue_17;
	// UnityEngine.Vector2 CameraFilterPack_AAA_SuperHexagon::Changecenter
	Vector2_t2243707579  ___Changecenter_20;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeRadius
	float ___ChangeRadius_21;

public:
	inline static int32_t get_offset_of_ChangeBorderSize_12() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___ChangeBorderSize_12)); }
	inline float get_ChangeBorderSize_12() const { return ___ChangeBorderSize_12; }
	inline float* get_address_of_ChangeBorderSize_12() { return &___ChangeBorderSize_12; }
	inline void set_ChangeBorderSize_12(float value)
	{
		___ChangeBorderSize_12 = value;
	}

	inline static int32_t get_offset_of_ChangeBorderColor_13() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___ChangeBorderColor_13)); }
	inline Color_t2020392075  get_ChangeBorderColor_13() const { return ___ChangeBorderColor_13; }
	inline Color_t2020392075 * get_address_of_ChangeBorderColor_13() { return &___ChangeBorderColor_13; }
	inline void set_ChangeBorderColor_13(Color_t2020392075  value)
	{
		___ChangeBorderColor_13 = value;
	}

	inline static int32_t get_offset_of_ChangeHexaColor_14() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___ChangeHexaColor_14)); }
	inline Color_t2020392075  get_ChangeHexaColor_14() const { return ___ChangeHexaColor_14; }
	inline Color_t2020392075 * get_address_of_ChangeHexaColor_14() { return &___ChangeHexaColor_14; }
	inline void set_ChangeHexaColor_14(Color_t2020392075  value)
	{
		___ChangeHexaColor_14 = value;
	}

	inline static int32_t get_offset_of_ChangeSpotSize_15() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___ChangeSpotSize_15)); }
	inline float get_ChangeSpotSize_15() const { return ___ChangeSpotSize_15; }
	inline float* get_address_of_ChangeSpotSize_15() { return &___ChangeSpotSize_15; }
	inline void set_ChangeSpotSize_15(float value)
	{
		___ChangeSpotSize_15 = value;
	}

	inline static int32_t get_offset_of_ChangeAlphaHexa_16() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___ChangeAlphaHexa_16)); }
	inline float get_ChangeAlphaHexa_16() const { return ___ChangeAlphaHexa_16; }
	inline float* get_address_of_ChangeAlphaHexa_16() { return &___ChangeAlphaHexa_16; }
	inline void set_ChangeAlphaHexa_16(float value)
	{
		___ChangeAlphaHexa_16 = value;
	}

	inline static int32_t get_offset_of_ChangeValue_17() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___ChangeValue_17)); }
	inline float get_ChangeValue_17() const { return ___ChangeValue_17; }
	inline float* get_address_of_ChangeValue_17() { return &___ChangeValue_17; }
	inline void set_ChangeValue_17(float value)
	{
		___ChangeValue_17 = value;
	}

	inline static int32_t get_offset_of_Changecenter_20() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___Changecenter_20)); }
	inline Vector2_t2243707579  get_Changecenter_20() const { return ___Changecenter_20; }
	inline Vector2_t2243707579 * get_address_of_Changecenter_20() { return &___Changecenter_20; }
	inline void set_Changecenter_20(Vector2_t2243707579  value)
	{
		___Changecenter_20 = value;
	}

	inline static int32_t get_offset_of_ChangeRadius_21() { return static_cast<int32_t>(offsetof(CameraFilterPack_AAA_SuperHexagon_t116370066_StaticFields, ___ChangeRadius_21)); }
	inline float get_ChangeRadius_21() const { return ___ChangeRadius_21; }
	inline float* get_address_of_ChangeRadius_21() { return &___ChangeRadius_21; }
	inline void set_ChangeRadius_21(float value)
	{
		___ChangeRadius_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
