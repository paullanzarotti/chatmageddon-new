﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_IPPickerLabelBase2669006230.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IPNumberPicker
struct  IPNumberPicker_t1469221158  : public IPPickerLabelBase_t2669006230
{
public:
	// System.Int32 IPNumberPicker::min
	int32_t ___min_15;
	// System.Int32 IPNumberPicker::max
	int32_t ___max_16;
	// System.Int32 IPNumberPicker::step
	int32_t ___step_17;
	// System.Int32 IPNumberPicker::initValue
	int32_t ___initValue_18;
	// System.String IPNumberPicker::toStringFormat
	String_t* ___toStringFormat_19;
	// System.Int32 IPNumberPicker::<CurrentValue>k__BackingField
	int32_t ___U3CCurrentValueU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_min_15() { return static_cast<int32_t>(offsetof(IPNumberPicker_t1469221158, ___min_15)); }
	inline int32_t get_min_15() const { return ___min_15; }
	inline int32_t* get_address_of_min_15() { return &___min_15; }
	inline void set_min_15(int32_t value)
	{
		___min_15 = value;
	}

	inline static int32_t get_offset_of_max_16() { return static_cast<int32_t>(offsetof(IPNumberPicker_t1469221158, ___max_16)); }
	inline int32_t get_max_16() const { return ___max_16; }
	inline int32_t* get_address_of_max_16() { return &___max_16; }
	inline void set_max_16(int32_t value)
	{
		___max_16 = value;
	}

	inline static int32_t get_offset_of_step_17() { return static_cast<int32_t>(offsetof(IPNumberPicker_t1469221158, ___step_17)); }
	inline int32_t get_step_17() const { return ___step_17; }
	inline int32_t* get_address_of_step_17() { return &___step_17; }
	inline void set_step_17(int32_t value)
	{
		___step_17 = value;
	}

	inline static int32_t get_offset_of_initValue_18() { return static_cast<int32_t>(offsetof(IPNumberPicker_t1469221158, ___initValue_18)); }
	inline int32_t get_initValue_18() const { return ___initValue_18; }
	inline int32_t* get_address_of_initValue_18() { return &___initValue_18; }
	inline void set_initValue_18(int32_t value)
	{
		___initValue_18 = value;
	}

	inline static int32_t get_offset_of_toStringFormat_19() { return static_cast<int32_t>(offsetof(IPNumberPicker_t1469221158, ___toStringFormat_19)); }
	inline String_t* get_toStringFormat_19() const { return ___toStringFormat_19; }
	inline String_t** get_address_of_toStringFormat_19() { return &___toStringFormat_19; }
	inline void set_toStringFormat_19(String_t* value)
	{
		___toStringFormat_19 = value;
		Il2CppCodeGenWriteBarrier(&___toStringFormat_19, value);
	}

	inline static int32_t get_offset_of_U3CCurrentValueU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(IPNumberPicker_t1469221158, ___U3CCurrentValueU3Ek__BackingField_20)); }
	inline int32_t get_U3CCurrentValueU3Ek__BackingField_20() const { return ___U3CCurrentValueU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CCurrentValueU3Ek__BackingField_20() { return &___U3CCurrentValueU3Ek__BackingField_20; }
	inline void set_U3CCurrentValueU3Ek__BackingField_20(int32_t value)
	{
		___U3CCurrentValueU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
