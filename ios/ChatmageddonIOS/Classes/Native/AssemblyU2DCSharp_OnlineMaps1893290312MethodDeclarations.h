﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnlineMaps
struct OnlineMaps_t1893290312;
// System.String[]
struct StringU5BU5D_t1642385972;
// OnlineMapsBuffer
struct OnlineMapsBuffer_t2643049474;
// System.Collections.Generic.List`1<OnlineMapsGoogleAPIQuery>
struct List_1_t4020097581;
// OnlineMapsDrawingElement
struct OnlineMapsDrawingElement_t539447654;
// OnlineMapsGoogleAPIQuery
struct OnlineMapsGoogleAPIQuery_t356009153;
// OnlineMapsMarker
struct OnlineMapsMarker_t3492166682;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// OnlineMapsMarker[]
struct OnlineMapsMarkerU5BU5D_t3424771647;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// OnlineMapsEvents[]
struct OnlineMapsEventsU5BU5D_t2569217244;
// OnlineMapsFindLocation
struct OnlineMapsFindLocation_t1727203720;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// OnlineMapsXML
struct OnlineMapsXML_t3341520387;
// OnlineMapsTile
struct OnlineMapsTile_t21329940;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_OnlineMapsBufferStatus201731616.h"
#include "AssemblyU2DCSharp_OnlineMapsDrawingElement539447654.h"
#include "AssemblyU2DCSharp_OnlineMapsGoogleAPIQuery356009153.h"
#include "AssemblyU2DCSharp_OnlineMapsMarker3492166682.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "AssemblyU2DCSharp_OnlineMapsXML3341520387.h"
#include "AssemblyU2DCSharp_OnlineMapsTile21329940.h"

// System.Void OnlineMaps::.ctor()
extern "C"  void OnlineMaps__ctor_m828907723 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMaps OnlineMaps::get_instance()
extern "C"  OnlineMaps_t1893290312 * OnlineMaps_get_instance_m491170226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps::get_availableLabels()
extern "C"  bool OnlineMaps_get_availableLabels_m1870663502 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] OnlineMaps::get_availableTypes()
extern "C"  StringU5BU5D_t1642385972* OnlineMaps_get_availableTypes_m2896871087 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps::get_availableLanguage()
extern "C"  bool OnlineMaps_get_availableLanguage_m1385787329 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMaps::get_bottomRightPosition()
extern "C"  Vector2_t2243707579  OnlineMaps_get_bottomRightPosition_m2988512599 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsBuffer OnlineMaps::get_buffer()
extern "C"  OnlineMapsBuffer_t2643049474 * OnlineMaps_get_buffer_m1225980505 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsBufferStatus OnlineMaps::get_bufferStatus()
extern "C"  int32_t OnlineMaps_get_bufferStatus_m2062978249 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps::get_enabledLabels()
extern "C"  bool OnlineMaps_get_enabledLabels_m3216455376 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<OnlineMapsGoogleAPIQuery> OnlineMaps::get_googleQueries()
extern "C"  List_1_t4020097581 * OnlineMaps_get_googleQueries_m915813585 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMaps::get_position()
extern "C"  Vector2_t2243707579  OnlineMaps_get_position_m3150611202 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::set_position(UnityEngine.Vector2)
extern "C"  void OnlineMaps_set_position_m2426567487 (OnlineMaps_t1893290312 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 OnlineMaps::get_topLeftPosition()
extern "C"  Vector2_t2243707579  OnlineMaps_get_topLeftPosition_m2656658004 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OnlineMaps::get_zoom()
extern "C"  int32_t OnlineMaps_get_zoom_m3999767999 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::set_zoom(System.Int32)
extern "C"  void OnlineMaps_set_zoom_m4029497604 (OnlineMaps_t1893290312 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::AddDrawingElement(OnlineMapsDrawingElement)
extern "C"  void OnlineMaps_AddDrawingElement_m877163200 (OnlineMaps_t1893290312 * __this, OnlineMapsDrawingElement_t539447654 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::AddGoogleAPIQuery(OnlineMapsGoogleAPIQuery)
extern "C"  void OnlineMaps_AddGoogleAPIQuery_m2648537714 (OnlineMaps_t1893290312 * __this, OnlineMapsGoogleAPIQuery_t356009153 * ___query0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsMarker OnlineMaps::AddMarker(OnlineMapsMarker)
extern "C"  OnlineMapsMarker_t3492166682 * OnlineMaps_AddMarker_m497705663 (OnlineMaps_t1893290312 * __this, OnlineMapsMarker_t3492166682 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsMarker OnlineMaps::AddMarker(UnityEngine.Vector2,System.String)
extern "C"  OnlineMapsMarker_t3492166682 * OnlineMaps_AddMarker_m3936489783 (OnlineMaps_t1893290312 * __this, Vector2_t2243707579  ___markerPosition0, String_t* ___label1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsMarker OnlineMaps::AddMarker(UnityEngine.Vector2,UnityEngine.Texture2D,System.String)
extern "C"  OnlineMapsMarker_t3492166682 * OnlineMaps_AddMarker_m3212593581 (OnlineMaps_t1893290312 * __this, Vector2_t2243707579  ___markerPosition0, Texture2D_t3542995729 * ___markerTexture1, String_t* ___label2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::AddMarkers(OnlineMapsMarker[])
extern "C"  void OnlineMaps_AddMarkers_m75381143 (OnlineMaps_t1893290312 * __this, OnlineMapsMarkerU5BU5D_t3424771647* ___newMarkers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::Awake()
extern "C"  void OnlineMaps_Awake_m195094808 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::CheckBaseProps()
extern "C"  void OnlineMaps_CheckBaseProps_m3710271542 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::CheckBufferComplete()
extern "C"  void OnlineMaps_CheckBufferComplete_m4072295234 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::CheckDownloadComplete()
extern "C"  void OnlineMaps_CheckDownloadComplete_m1803028036 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::CheckGoogleAPIQuery()
extern "C"  void OnlineMaps_CheckGoogleAPIQuery_m2767297604 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OnlineMaps::CheckMapSize(System.Int32)
extern "C"  int32_t OnlineMaps_CheckMapSize_m1126147359 (OnlineMaps_t1893290312 * __this, int32_t ___z0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::CheckRedrawType()
extern "C"  void OnlineMaps_CheckRedrawType_m1452697328 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::CheckServerConnection(System.Action`1<System.Boolean>)
extern "C"  void OnlineMaps_CheckServerConnection_m2377067151 (OnlineMaps_t1893290312 * __this, Action_1_t3627374100 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::DispatchEvent(OnlineMapsEvents[])
extern "C"  void OnlineMaps_DispatchEvent_m2546826238 (OnlineMaps_t1893290312 * __this, OnlineMapsEventsU5BU5D_t2569217244* ___evs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMaps::FindDirection(System.String,System.String,System.Boolean)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMaps_FindDirection_m1039045344 (OnlineMaps_t1893290312 * __this, String_t* ___origin0, String_t* ___destination1, bool ___alternatives2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsGoogleAPIQuery OnlineMaps::FindLocation(System.String)
extern "C"  OnlineMapsGoogleAPIQuery_t356009153 * OnlineMaps_FindLocation_m1559757305 (OnlineMaps_t1893290312 * __this, String_t* ___search0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsFindLocation OnlineMaps::FindLocation(System.String,System.Action`1<System.String>)
extern "C"  OnlineMapsFindLocation_t1727203720 * OnlineMaps_FindLocation_m1943525118 (OnlineMaps_t1893290312 * __this, String_t* ___search0, Action_1_t1831019615 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::GCCollect()
extern "C"  void OnlineMaps_GCCollect_m1829470341 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::GetBottomRightPosition(System.Double&,System.Double&)
extern "C"  void OnlineMaps_GetBottomRightPosition_m945706163 (OnlineMaps_t1893290312 * __this, double* ___lng0, double* ___lat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsDrawingElement OnlineMaps::GetDrawingElement(UnityEngine.Vector2)
extern "C"  OnlineMapsDrawingElement_t539447654 * OnlineMaps_GetDrawingElement_m562658214 (OnlineMaps_t1893290312 * __this, Vector2_t2243707579  ___screenPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsMarker OnlineMaps::GetMarkerFromScreen(UnityEngine.Vector2)
extern "C"  OnlineMapsMarker_t3492166682 * OnlineMaps_GetMarkerFromScreen_m2691745866 (OnlineMaps_t1893290312 * __this, Vector2_t2243707579  ___screenPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::GetPosition(System.Double&,System.Double&)
extern "C"  void OnlineMaps_GetPosition_m2404551726 (OnlineMaps_t1893290312 * __this, double* ___lng0, double* ___lat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::GetTopLeftPosition(System.Double&,System.Double&)
extern "C"  void OnlineMaps_GetTopLeftPosition_m1966083608 (OnlineMaps_t1893290312 * __this, double* ___lng0, double* ___lat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::LateUpdate()
extern "C"  void OnlineMaps_LateUpdate_m4238656462 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::OnDestroy()
extern "C"  void OnlineMaps_OnDestroy_m1796255910 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::OnDisable()
extern "C"  void OnlineMaps_OnDisable_m49658068 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::OnEnable()
extern "C"  void OnlineMaps_OnEnable_m2166483079 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::OnGUI()
extern "C"  void OnlineMaps_OnGUI_m104230597 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::OnGUITooltip(UnityEngine.GUIStyle,System.String,UnityEngine.Vector2)
extern "C"  void OnlineMaps_OnGUITooltip_m3175932761 (OnlineMaps_t1893290312 * __this, GUIStyle_t1799908754 * ___style0, String_t* ___text1, Vector2_t2243707579  ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::Redraw()
extern "C"  void OnlineMaps_Redraw_m3352703156 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::RedrawImmediately()
extern "C"  void OnlineMaps_RedrawImmediately_m935989472 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::RemoveAllDrawingElements()
extern "C"  void OnlineMaps_RemoveAllDrawingElements_m691216833 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::RemoveAllMarkers()
extern "C"  void OnlineMaps_RemoveAllMarkers_m3102614897 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::RemoveDrawingElement(OnlineMapsDrawingElement)
extern "C"  void OnlineMaps_RemoveDrawingElement_m2659807977 (OnlineMaps_t1893290312 * __this, OnlineMapsDrawingElement_t539447654 * ___element0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::RemoveDrawingElementAt(System.Int32)
extern "C"  void OnlineMaps_RemoveDrawingElementAt_m1788926355 (OnlineMaps_t1893290312 * __this, int32_t ___elementIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::RemoveMarker(OnlineMapsMarker)
extern "C"  void OnlineMaps_RemoveMarker_m2757288137 (OnlineMaps_t1893290312 * __this, OnlineMapsMarker_t3492166682 * ___marker0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::RemoveMarkerAt(System.Int32)
extern "C"  void OnlineMaps_RemoveMarkerAt_m4068167303 (OnlineMaps_t1893290312 * __this, int32_t ___markerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::Save()
extern "C"  void OnlineMaps_Save_m326981402 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::SaveMarkers(OnlineMapsXML)
extern "C"  void OnlineMaps_SaveMarkers_m1740572432 (OnlineMaps_t1893290312 * __this, OnlineMapsXML_t3341520387 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// OnlineMapsXML OnlineMaps::SaveSettings(OnlineMapsXML)
extern "C"  OnlineMapsXML_t3341520387 * OnlineMaps_SaveSettings_m1567567810 (OnlineMaps_t1893290312 * __this, OnlineMapsXML_t3341520387 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::SetPosition(System.Double,System.Double)
extern "C"  void OnlineMaps_SetPosition_m3884821578 (OnlineMaps_t1893290312 * __this, double ___lng0, double ___lat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::SetPositionAndZoom(System.Single,System.Single,System.Int32)
extern "C"  void OnlineMaps_SetPositionAndZoom_m3796886349 (OnlineMaps_t1893290312 * __this, float ___lng0, float ___lat1, int32_t ___ZOOM2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::SetTexture(UnityEngine.Texture2D)
extern "C"  void OnlineMaps_SetTexture_m1050189490 (OnlineMaps_t1893290312 * __this, Texture2D_t3542995729 * ___newTexture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::ShowMarkersTooltip(UnityEngine.Vector2)
extern "C"  void OnlineMaps_ShowMarkersTooltip_m1023579684 (OnlineMaps_t1893290312 * __this, Vector2_t2243707579  ___screenPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::Start()
extern "C"  void OnlineMaps_Start_m3354382023 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::StartDownloading()
extern "C"  void OnlineMaps_StartDownloading_m616997839 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::StartDownloadTile(OnlineMapsTile)
extern "C"  void OnlineMaps_StartDownloadTile_m271605211 (OnlineMaps_t1893290312 * __this, OnlineMapsTile_t21329940 * ___tile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::StartBuffer()
extern "C"  void OnlineMaps_StartBuffer_m485942659 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::Update()
extern "C"  void OnlineMaps_Update_m1937654250 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::UpdateBottonRightPosition()
extern "C"  void OnlineMaps_UpdateBottonRightPosition_m162609147 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::UpdateTopLeftPosition()
extern "C"  void OnlineMaps_UpdateTopLeftPosition_m2755599707 (OnlineMaps_t1893290312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnlineMaps::.cctor()
extern "C"  void OnlineMaps__cctor_m2121127866 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps::<CheckGoogleAPIQuery>m__0(OnlineMapsGoogleAPIQuery)
extern "C"  bool OnlineMaps_U3CCheckGoogleAPIQueryU3Em__0_m82248464 (Il2CppObject * __this /* static, unused */, OnlineMapsGoogleAPIQuery_t356009153 * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps::<StartDownloading>m__1(OnlineMapsTile)
extern "C"  bool OnlineMaps_U3CStartDownloadingU3Em__1_m2895568701 (Il2CppObject * __this /* static, unused */, OnlineMapsTile_t21329940 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnlineMaps::<StartDownloading>m__2(OnlineMapsTile)
extern "C"  bool OnlineMaps_U3CStartDownloadingU3Em__2_m3555225406 (Il2CppObject * __this /* static, unused */, OnlineMapsTile_t21329940 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 OnlineMaps::<StartDownloading>m__3(OnlineMapsTile)
extern "C"  int32_t OnlineMaps_U3CStartDownloadingU3Em__3_m2932421205 (Il2CppObject * __this /* static, unused */, OnlineMapsTile_t21329940 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
