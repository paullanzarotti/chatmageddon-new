﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Unibill.Impl.ProductIdRemapper
struct ProductIdRemapper_t3313438456;
// Unibill.Impl.UnibillConfiguration
struct UnibillConfiguration_t2915611853;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// PurchasableItem
struct PurchasableItem_t3963353899;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_Unibill2915611853.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Unibill_Impl_BillingP552059234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PurchasableItem3963353899.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Unibill.Impl.ProductIdRemapper::.ctor(Unibill.Impl.UnibillConfiguration)
extern "C"  void ProductIdRemapper__ctor_m475892330 (ProductIdRemapper_t3313438456 * __this, UnibillConfiguration_t2915611853 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Unibill.Impl.ProductIdRemapper::initialiseForPlatform(Unibill.Impl.BillingPlatform)
extern "C"  void ProductIdRemapper_initialiseForPlatform_m1233317846 (ProductIdRemapper_t3313438456 * __this, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Unibill.Impl.ProductIdRemapper::getAllPlatformSpecificProductIds()
extern "C"  StringU5BU5D_t1642385972* ProductIdRemapper_getAllPlatformSpecificProductIds_m1672566550 (ProductIdRemapper_t3313438456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Unibill.Impl.ProductIdRemapper::mapItemIdToPlatformSpecificId(PurchasableItem)
extern "C"  String_t* ProductIdRemapper_mapItemIdToPlatformSpecificId_m2202714655 (ProductIdRemapper_t3313438456 * __this, PurchasableItem_t3963353899 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PurchasableItem Unibill.Impl.ProductIdRemapper::getPurchasableItemFromPlatformSpecificId(System.String)
extern "C"  PurchasableItem_t3963353899 * ProductIdRemapper_getPurchasableItemFromPlatformSpecificId_m1406358765 (ProductIdRemapper_t3313438456 * __this, String_t* ___platformSpecificId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Unibill.Impl.ProductIdRemapper::canMapProductSpecificId(System.String)
extern "C"  bool ProductIdRemapper_canMapProductSpecificId_m83156700 (ProductIdRemapper_t3313438456 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
