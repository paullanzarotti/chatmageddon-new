﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BaseInfiniteListPopulator`1<System.Object>
struct BaseInfiniteListPopulator_1_t3248597474;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1<System.Object>
struct  U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206  : public Il2CppObject
{
public:
	// System.Boolean BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::<sectionCompleted>__0
	bool ___U3CsectionCompletedU3E__0_0;
	// System.Single BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::<sectionTime>__1
	float ___U3CsectionTimeU3E__1_1;
	// System.Single BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::seconds
	float ___seconds_2;
	// System.Single BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::position
	float ___position_3;
	// BaseInfiniteListPopulator`1<Item> BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::$this
	BaseInfiniteListPopulator_1_t3248597474 * ___U24this_4;
	// System.Object BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 BaseInfiniteListPopulator`1/<SetPanelPosOverTime>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CsectionCompletedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___U3CsectionCompletedU3E__0_0)); }
	inline bool get_U3CsectionCompletedU3E__0_0() const { return ___U3CsectionCompletedU3E__0_0; }
	inline bool* get_address_of_U3CsectionCompletedU3E__0_0() { return &___U3CsectionCompletedU3E__0_0; }
	inline void set_U3CsectionCompletedU3E__0_0(bool value)
	{
		___U3CsectionCompletedU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CsectionTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___U3CsectionTimeU3E__1_1)); }
	inline float get_U3CsectionTimeU3E__1_1() const { return ___U3CsectionTimeU3E__1_1; }
	inline float* get_address_of_U3CsectionTimeU3E__1_1() { return &___U3CsectionTimeU3E__1_1; }
	inline void set_U3CsectionTimeU3E__1_1(float value)
	{
		___U3CsectionTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_seconds_2() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___seconds_2)); }
	inline float get_seconds_2() const { return ___seconds_2; }
	inline float* get_address_of_seconds_2() { return &___seconds_2; }
	inline void set_seconds_2(float value)
	{
		___seconds_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___position_3)); }
	inline float get_position_3() const { return ___position_3; }
	inline float* get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(float value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___U24this_4)); }
	inline BaseInfiniteListPopulator_1_t3248597474 * get_U24this_4() const { return ___U24this_4; }
	inline BaseInfiniteListPopulator_1_t3248597474 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(BaseInfiniteListPopulator_1_t3248597474 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSetPanelPosOverTimeU3Ec__Iterator1_t3711079206, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
