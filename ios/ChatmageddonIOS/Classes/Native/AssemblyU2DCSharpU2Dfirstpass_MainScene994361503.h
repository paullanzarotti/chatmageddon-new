﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ELANNotification
struct ELANNotification_t786863861;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainScene
struct  MainScene_t994361503  : public MonoBehaviour_t1158329972
{
public:
	// ELANNotification MainScene::notification
	ELANNotification_t786863861 * ___notification_2;
	// ELANNotification MainScene::parametrizedNotification1
	ELANNotification_t786863861 * ___parametrizedNotification1_3;
	// ELANNotification MainScene::parametrizedNotification2
	ELANNotification_t786863861 * ___parametrizedNotification2_4;
	// ELANNotification MainScene::parametrizedNotification3
	ELANNotification_t786863861 * ___parametrizedNotification3_5;
	// ELANNotification MainScene::parametrizedNotification4
	ELANNotification_t786863861 * ___parametrizedNotification4_6;
	// ELANNotification MainScene::parametrizedNotification5
	ELANNotification_t786863861 * ___parametrizedNotification5_7;
	// System.Boolean MainScene::sendParametrizedNotification1
	bool ___sendParametrizedNotification1_8;
	// System.Boolean MainScene::sendParametrizedNotification2
	bool ___sendParametrizedNotification2_9;
	// System.Boolean MainScene::sendParametrizedNotification3
	bool ___sendParametrizedNotification3_10;
	// System.Boolean MainScene::sendParametrizedNotification4
	bool ___sendParametrizedNotification4_11;
	// System.Boolean MainScene::sendParametrizedNotification5
	bool ___sendParametrizedNotification5_12;
	// System.String MainScene::delay
	String_t* ___delay_13;
	// System.String MainScene::rep
	String_t* ___rep_14;
	// System.String MainScene::message
	String_t* ___message_15;
	// System.String MainScene::errorRepetition
	String_t* ___errorRepetition_16;
	// System.String MainScene::errorDelay
	String_t* ___errorDelay_17;
	// System.Int32 MainScene::errorType
	int32_t ___errorType_18;
	// System.String MainScene::logMessage
	String_t* ___logMessage_19;

public:
	inline static int32_t get_offset_of_notification_2() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___notification_2)); }
	inline ELANNotification_t786863861 * get_notification_2() const { return ___notification_2; }
	inline ELANNotification_t786863861 ** get_address_of_notification_2() { return &___notification_2; }
	inline void set_notification_2(ELANNotification_t786863861 * value)
	{
		___notification_2 = value;
		Il2CppCodeGenWriteBarrier(&___notification_2, value);
	}

	inline static int32_t get_offset_of_parametrizedNotification1_3() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___parametrizedNotification1_3)); }
	inline ELANNotification_t786863861 * get_parametrizedNotification1_3() const { return ___parametrizedNotification1_3; }
	inline ELANNotification_t786863861 ** get_address_of_parametrizedNotification1_3() { return &___parametrizedNotification1_3; }
	inline void set_parametrizedNotification1_3(ELANNotification_t786863861 * value)
	{
		___parametrizedNotification1_3 = value;
		Il2CppCodeGenWriteBarrier(&___parametrizedNotification1_3, value);
	}

	inline static int32_t get_offset_of_parametrizedNotification2_4() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___parametrizedNotification2_4)); }
	inline ELANNotification_t786863861 * get_parametrizedNotification2_4() const { return ___parametrizedNotification2_4; }
	inline ELANNotification_t786863861 ** get_address_of_parametrizedNotification2_4() { return &___parametrizedNotification2_4; }
	inline void set_parametrizedNotification2_4(ELANNotification_t786863861 * value)
	{
		___parametrizedNotification2_4 = value;
		Il2CppCodeGenWriteBarrier(&___parametrizedNotification2_4, value);
	}

	inline static int32_t get_offset_of_parametrizedNotification3_5() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___parametrizedNotification3_5)); }
	inline ELANNotification_t786863861 * get_parametrizedNotification3_5() const { return ___parametrizedNotification3_5; }
	inline ELANNotification_t786863861 ** get_address_of_parametrizedNotification3_5() { return &___parametrizedNotification3_5; }
	inline void set_parametrizedNotification3_5(ELANNotification_t786863861 * value)
	{
		___parametrizedNotification3_5 = value;
		Il2CppCodeGenWriteBarrier(&___parametrizedNotification3_5, value);
	}

	inline static int32_t get_offset_of_parametrizedNotification4_6() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___parametrizedNotification4_6)); }
	inline ELANNotification_t786863861 * get_parametrizedNotification4_6() const { return ___parametrizedNotification4_6; }
	inline ELANNotification_t786863861 ** get_address_of_parametrizedNotification4_6() { return &___parametrizedNotification4_6; }
	inline void set_parametrizedNotification4_6(ELANNotification_t786863861 * value)
	{
		___parametrizedNotification4_6 = value;
		Il2CppCodeGenWriteBarrier(&___parametrizedNotification4_6, value);
	}

	inline static int32_t get_offset_of_parametrizedNotification5_7() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___parametrizedNotification5_7)); }
	inline ELANNotification_t786863861 * get_parametrizedNotification5_7() const { return ___parametrizedNotification5_7; }
	inline ELANNotification_t786863861 ** get_address_of_parametrizedNotification5_7() { return &___parametrizedNotification5_7; }
	inline void set_parametrizedNotification5_7(ELANNotification_t786863861 * value)
	{
		___parametrizedNotification5_7 = value;
		Il2CppCodeGenWriteBarrier(&___parametrizedNotification5_7, value);
	}

	inline static int32_t get_offset_of_sendParametrizedNotification1_8() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___sendParametrizedNotification1_8)); }
	inline bool get_sendParametrizedNotification1_8() const { return ___sendParametrizedNotification1_8; }
	inline bool* get_address_of_sendParametrizedNotification1_8() { return &___sendParametrizedNotification1_8; }
	inline void set_sendParametrizedNotification1_8(bool value)
	{
		___sendParametrizedNotification1_8 = value;
	}

	inline static int32_t get_offset_of_sendParametrizedNotification2_9() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___sendParametrizedNotification2_9)); }
	inline bool get_sendParametrizedNotification2_9() const { return ___sendParametrizedNotification2_9; }
	inline bool* get_address_of_sendParametrizedNotification2_9() { return &___sendParametrizedNotification2_9; }
	inline void set_sendParametrizedNotification2_9(bool value)
	{
		___sendParametrizedNotification2_9 = value;
	}

	inline static int32_t get_offset_of_sendParametrizedNotification3_10() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___sendParametrizedNotification3_10)); }
	inline bool get_sendParametrizedNotification3_10() const { return ___sendParametrizedNotification3_10; }
	inline bool* get_address_of_sendParametrizedNotification3_10() { return &___sendParametrizedNotification3_10; }
	inline void set_sendParametrizedNotification3_10(bool value)
	{
		___sendParametrizedNotification3_10 = value;
	}

	inline static int32_t get_offset_of_sendParametrizedNotification4_11() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___sendParametrizedNotification4_11)); }
	inline bool get_sendParametrizedNotification4_11() const { return ___sendParametrizedNotification4_11; }
	inline bool* get_address_of_sendParametrizedNotification4_11() { return &___sendParametrizedNotification4_11; }
	inline void set_sendParametrizedNotification4_11(bool value)
	{
		___sendParametrizedNotification4_11 = value;
	}

	inline static int32_t get_offset_of_sendParametrizedNotification5_12() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___sendParametrizedNotification5_12)); }
	inline bool get_sendParametrizedNotification5_12() const { return ___sendParametrizedNotification5_12; }
	inline bool* get_address_of_sendParametrizedNotification5_12() { return &___sendParametrizedNotification5_12; }
	inline void set_sendParametrizedNotification5_12(bool value)
	{
		___sendParametrizedNotification5_12 = value;
	}

	inline static int32_t get_offset_of_delay_13() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___delay_13)); }
	inline String_t* get_delay_13() const { return ___delay_13; }
	inline String_t** get_address_of_delay_13() { return &___delay_13; }
	inline void set_delay_13(String_t* value)
	{
		___delay_13 = value;
		Il2CppCodeGenWriteBarrier(&___delay_13, value);
	}

	inline static int32_t get_offset_of_rep_14() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___rep_14)); }
	inline String_t* get_rep_14() const { return ___rep_14; }
	inline String_t** get_address_of_rep_14() { return &___rep_14; }
	inline void set_rep_14(String_t* value)
	{
		___rep_14 = value;
		Il2CppCodeGenWriteBarrier(&___rep_14, value);
	}

	inline static int32_t get_offset_of_message_15() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___message_15)); }
	inline String_t* get_message_15() const { return ___message_15; }
	inline String_t** get_address_of_message_15() { return &___message_15; }
	inline void set_message_15(String_t* value)
	{
		___message_15 = value;
		Il2CppCodeGenWriteBarrier(&___message_15, value);
	}

	inline static int32_t get_offset_of_errorRepetition_16() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___errorRepetition_16)); }
	inline String_t* get_errorRepetition_16() const { return ___errorRepetition_16; }
	inline String_t** get_address_of_errorRepetition_16() { return &___errorRepetition_16; }
	inline void set_errorRepetition_16(String_t* value)
	{
		___errorRepetition_16 = value;
		Il2CppCodeGenWriteBarrier(&___errorRepetition_16, value);
	}

	inline static int32_t get_offset_of_errorDelay_17() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___errorDelay_17)); }
	inline String_t* get_errorDelay_17() const { return ___errorDelay_17; }
	inline String_t** get_address_of_errorDelay_17() { return &___errorDelay_17; }
	inline void set_errorDelay_17(String_t* value)
	{
		___errorDelay_17 = value;
		Il2CppCodeGenWriteBarrier(&___errorDelay_17, value);
	}

	inline static int32_t get_offset_of_errorType_18() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___errorType_18)); }
	inline int32_t get_errorType_18() const { return ___errorType_18; }
	inline int32_t* get_address_of_errorType_18() { return &___errorType_18; }
	inline void set_errorType_18(int32_t value)
	{
		___errorType_18 = value;
	}

	inline static int32_t get_offset_of_logMessage_19() { return static_cast<int32_t>(offsetof(MainScene_t994361503, ___logMessage_19)); }
	inline String_t* get_logMessage_19() const { return ___logMessage_19; }
	inline String_t** get_address_of_logMessage_19() { return &___logMessage_19; }
	inline void set_logMessage_19(String_t* value)
	{
		___logMessage_19 = value;
		Il2CppCodeGenWriteBarrier(&___logMessage_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
