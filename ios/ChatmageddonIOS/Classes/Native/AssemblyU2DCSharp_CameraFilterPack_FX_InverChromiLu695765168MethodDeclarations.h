﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraFilterPack_FX_InverChromiLum
struct CameraFilterPack_FX_InverChromiLum_t695765168;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"

// System.Void CameraFilterPack_FX_InverChromiLum::.ctor()
extern "C"  void CameraFilterPack_FX_InverChromiLum__ctor_m1463164933 (CameraFilterPack_FX_InverChromiLum_t695765168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_InverChromiLum::get_material()
extern "C"  Material_t193706927 * CameraFilterPack_FX_InverChromiLum_get_material_m1275978726 (CameraFilterPack_FX_InverChromiLum_t695765168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::Start()
extern "C"  void CameraFilterPack_FX_InverChromiLum_Start_m3227239165 (CameraFilterPack_FX_InverChromiLum_t695765168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void CameraFilterPack_FX_InverChromiLum_OnRenderImage_m2527525261 (CameraFilterPack_FX_InverChromiLum_t695765168 * __this, RenderTexture_t2666733923 * ___sourceTexture0, RenderTexture_t2666733923 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::Update()
extern "C"  void CameraFilterPack_FX_InverChromiLum_Update_m2079989246 (CameraFilterPack_FX_InverChromiLum_t695765168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::OnDisable()
extern "C"  void CameraFilterPack_FX_InverChromiLum_OnDisable_m1265490720 (CameraFilterPack_FX_InverChromiLum_t695765168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
