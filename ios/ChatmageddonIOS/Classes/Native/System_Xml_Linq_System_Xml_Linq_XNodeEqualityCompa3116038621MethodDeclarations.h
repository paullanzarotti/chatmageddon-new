﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Linq.XNodeEqualityComparer
struct XNodeEqualityComparer_t3116038621;
// System.Object
struct Il2CppObject;
// System.Xml.Linq.XNode
struct XNode_t2707504214;
// System.Xml.Linq.XAttribute
struct XAttribute_t3858477518;
// System.Xml.Linq.XDeclaration
struct XDeclaration_t3367285402;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Xml_Linq_System_Xml_Linq_XNode2707504214.h"
#include "System_Xml_Linq_System_Xml_Linq_XAttribute3858477518.h"
#include "System_Xml_Linq_System_Xml_Linq_XDeclaration3367285402.h"

// System.Void System.Xml.Linq.XNodeEqualityComparer::.ctor()
extern "C"  void XNodeEqualityComparer__ctor_m2149745472 (XNodeEqualityComparer_t3116038621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XNodeEqualityComparer::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool XNodeEqualityComparer_System_Collections_IEqualityComparer_Equals_m3901301221 (XNodeEqualityComparer_t3116038621 * __this, Il2CppObject * ___n10, Il2CppObject * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XNodeEqualityComparer::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t XNodeEqualityComparer_System_Collections_IEqualityComparer_GetHashCode_m1716004019 (XNodeEqualityComparer_t3116038621 * __this, Il2CppObject * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XNodeEqualityComparer::Equals(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern "C"  bool XNodeEqualityComparer_Equals_m3412641323 (XNodeEqualityComparer_t3116038621 * __this, XNode_t2707504214 * ___n10, XNode_t2707504214 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XNodeEqualityComparer::Equals(System.Xml.Linq.XAttribute,System.Xml.Linq.XAttribute)
extern "C"  bool XNodeEqualityComparer_Equals_m163327531 (XNodeEqualityComparer_t3116038621 * __this, XAttribute_t3858477518 * ___a10, XAttribute_t3858477518 * ___a21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Linq.XNodeEqualityComparer::Equals(System.Xml.Linq.XDeclaration,System.Xml.Linq.XDeclaration)
extern "C"  bool XNodeEqualityComparer_Equals_m2405644035 (XNodeEqualityComparer_t3116038621 * __this, XDeclaration_t3367285402 * ___d10, XDeclaration_t3367285402 * ___d21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XNodeEqualityComparer::GetHashCode(System.Xml.Linq.XDeclaration)
extern "C"  int32_t XNodeEqualityComparer_GetHashCode_m1024769467 (XNodeEqualityComparer_t3116038621 * __this, XDeclaration_t3367285402 * ___d0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.Linq.XNodeEqualityComparer::GetHashCode(System.Xml.Linq.XNode)
extern "C"  int32_t XNodeEqualityComparer_GetHashCode_m2349713071 (XNodeEqualityComparer_t3116038621 * __this, XNode_t2707504214 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
