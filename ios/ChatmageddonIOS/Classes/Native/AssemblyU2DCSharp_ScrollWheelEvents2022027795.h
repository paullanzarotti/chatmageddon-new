﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ScrollWheelEvents
struct ScrollWheelEvents_t2022027795;
// ScrollWheelEvents/ScrollStartStopHandler
struct ScrollStartStopHandler_t1032307121;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollWheelEvents
struct  ScrollWheelEvents_t2022027795  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ScrollWheelEvents::_isScrolling
	bool ____isScrolling_4;

public:
	inline static int32_t get_offset_of__isScrolling_4() { return static_cast<int32_t>(offsetof(ScrollWheelEvents_t2022027795, ____isScrolling_4)); }
	inline bool get__isScrolling_4() const { return ____isScrolling_4; }
	inline bool* get_address_of__isScrolling_4() { return &____isScrolling_4; }
	inline void set__isScrolling_4(bool value)
	{
		____isScrolling_4 = value;
	}
};

struct ScrollWheelEvents_t2022027795_StaticFields
{
public:
	// ScrollWheelEvents ScrollWheelEvents::_instance
	ScrollWheelEvents_t2022027795 * ____instance_2;
	// ScrollWheelEvents/ScrollStartStopHandler ScrollWheelEvents::onScrollStartOrStop
	ScrollStartStopHandler_t1032307121 * ___onScrollStartOrStop_3;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(ScrollWheelEvents_t2022027795_StaticFields, ____instance_2)); }
	inline ScrollWheelEvents_t2022027795 * get__instance_2() const { return ____instance_2; }
	inline ScrollWheelEvents_t2022027795 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ScrollWheelEvents_t2022027795 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of_onScrollStartOrStop_3() { return static_cast<int32_t>(offsetof(ScrollWheelEvents_t2022027795_StaticFields, ___onScrollStartOrStop_3)); }
	inline ScrollStartStopHandler_t1032307121 * get_onScrollStartOrStop_3() const { return ___onScrollStartOrStop_3; }
	inline ScrollStartStopHandler_t1032307121 ** get_address_of_onScrollStartOrStop_3() { return &___onScrollStartOrStop_3; }
	inline void set_onScrollStartOrStop_3(ScrollStartStopHandler_t1032307121 * value)
	{
		___onScrollStartOrStop_3 = value;
		Il2CppCodeGenWriteBarrier(&___onScrollStartOrStop_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
