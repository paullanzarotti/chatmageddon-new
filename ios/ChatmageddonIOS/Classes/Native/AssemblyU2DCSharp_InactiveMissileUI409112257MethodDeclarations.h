﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InactiveMissileUI
struct InactiveMissileUI_t409112257;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MissileTravelState1612660365.h"

// System.Void InactiveMissileUI::.ctor()
extern "C"  void InactiveMissileUI__ctor_m2902782416 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::OnEnable()
extern "C"  void InactiveMissileUI_OnEnable_m1195485832 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::OnDisable()
extern "C"  void InactiveMissileUI_OnDisable_m1388333609 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::LoadUI(System.Boolean)
extern "C"  void InactiveMissileUI_LoadUI_m2159229129 (InactiveMissileUI_t409112257 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::UnloadUI(System.Boolean)
extern "C"  void InactiveMissileUI_UnloadUI_m479743038 (InactiveMissileUI_t409112257 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::OnScaleTweenFinished()
extern "C"  void InactiveMissileUI_OnScaleTweenFinished_m1852632116 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::SetMissileHit()
extern "C"  void InactiveMissileUI_SetMissileHit_m1317737559 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::SetMissileDefended()
extern "C"  void InactiveMissileUI_SetMissileDefended_m4138528619 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::SetMissileBlocked()
extern "C"  void InactiveMissileUI_SetMissileBlocked_m1357983620 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::SetMissileBackfired()
extern "C"  void InactiveMissileUI_SetMissileBackfired_m1737004909 (InactiveMissileUI_t409112257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::SetFlamesActive(System.Boolean,System.Boolean)
extern "C"  void InactiveMissileUI_SetFlamesActive_m287530378 (InactiveMissileUI_t409112257 * __this, bool ___active0, bool ___withAnim1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InactiveMissileUI::SetPointsLabels(MissileTravelState)
extern "C"  void InactiveMissileUI_SetPointsLabels_m3953403979 (InactiveMissileUI_t409112257 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
